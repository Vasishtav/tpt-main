﻿using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.DTO.Source;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    /// <summary>
    /// Source
    /// </summary>
    [RoutePrefix("api/Source")]
    public class SourceController : BaseController
    {
        SourceManager sourcemgr = new SourceManager();

        /// <summary>
        /// get all source to Tp-Source
        /// </summary>
        /// <returns></returns>
        [HttpGet,Route("GetSourcesTP")]
        public IHttpActionResult GetSourcesTP()
        {
            try
            {
                var result = sourcemgr.GetSourcesTP();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// get all source to Tp-Source including in-active data
        /// </summary>
        /// <returns></returns>
        [HttpGet,Route("GetInactiveSourcesTP")]
        public IHttpActionResult GetInactiveSourcesTP()
        {
            try
            {
                var result = sourcemgr.GetInactiveSources();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }
       
        /// <summary>
        /// get owner list data for Tp-Source
        /// </summary>
        /// <returns></returns>
        [HttpGet,Route("GetOwnerSources")]
        public IHttpActionResult GetOwnerSources()
        {
            try
            {
                var result = sourcemgr.GetOwnerSources();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// get channel list for Tp-Source
        /// </summary>
        /// <returns></returns>
        [HttpGet,Route("GetChannelSources")]
        public IHttpActionResult GetChannelSources()
        {
            try
            {
                var result = sourcemgr.GetChannelSources();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }
        
        /// <summary>
        /// get source list for Tp-Source
        /// </summary>
        /// <returns></returns>
        [HttpGet,Route("GetSource")]
        public IHttpActionResult GetSource()
        {
            try
            {
                var result = sourcemgr.GetSource();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get Sources HFC
        /// </summary>
        /// <returns></returns>
        [HttpGet,Route("GetSourcesHFC")]
        public IHttpActionResult GetSourcesHFC()
        {
            try
            {
                var result = sourcemgr.GetSourcesHFC();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }
        
        /// <summary>
        /// get all data to HFC-Source
        /// </summary>
        /// <returns></returns>
        [HttpGet,Route("GetSourcesHFCmapTP")]
        public IHttpActionResult GetSourcesHFCmapTP()
        {
            try
            {
                var result = sourcemgr.GetSourcesHFCmapTP();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }
        
        /// <summary>
        /// get all data including in-active data to HFC-Source
        /// </summary>
        /// <returns></returns>
        [HttpGet,Route("GetInactiveHFCSources")]
        public IHttpActionResult GetInactiveHFCSources()
        {
            try
            {
                var result = sourcemgr.GetInactiveSourcesHFCmapTP();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }
        
        /// <summary>
        /// get TPTSource List for HFC-Source
        /// </summary>
        /// <returns></returns>
        [HttpGet,Route("GetTPTSource")]
        public IHttpActionResult GetTPTSource()
        {
            try
            {
                var result = sourcemgr.GetTPTSources();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }
        
        /// <summary>
        /// get owner list data for HFC-Source
        /// </summary>
        /// <returns></returns>
        [HttpGet,Route("GetTPTOwnerSources")]
        public IHttpActionResult GetTPTOwnerSources()
        {
            try
            {
                var result = sourcemgr.GetTPTOwnerSources();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }
        
        /// <summary>
        /// get channel list for HFC-Source
        /// </summary>
        /// <returns></returns>
        [HttpGet,Route("GetTPTChannelSources")]
        public IHttpActionResult GetTPTChannelSources()
        {
            try
            {
                var result = sourcemgr.GetTPTChannelSources();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }
        
        /// <summary>
        /// get source list for HFC-Source
        /// </summary>
        /// <returns></returns>
        [HttpGet,Route("GetTPTSourceList")]
        public IHttpActionResult GetTPTSourceList()
        {
            try
            {
                var result = sourcemgr.GetTPTListSource();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }
        /// <summary>
        /// To Add or update SourcesHFC
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost,Route("PostSourcesHFC")]
        public IHttpActionResult PostSourcesHFC(SourcesHFC model)
        {

            try
            {
                var result = "";
                if (model!= null)
                {
                    
                    if (model.SourcesHFCId > 0)
                    {
                        result = sourcemgr.UpdateSourceHFC(model);
                    }
                    else
                        result = sourcemgr.AddSourceHFC(model);
                }
                //return Json(result);
                return ResponseResult(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// To Add or update SourcesTP
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost,Route("PostSourcesTP")]
        public IHttpActionResult PostSourcesTP(SourcesTP model)
        {

            try
            {
                if (model!=null)
                {
                    if (model.SourcesTPId > 0)
                    {
                        sourcemgr.UpdateSourceTP(model);
                    }
                    else
                        sourcemgr.AddSourceTP(model);
                }
                return ResponseResult("Success");
            }
            catch (Exception ex)
            {
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Post Sources
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost,Route("PostSources")]
        public IHttpActionResult PostSources([FromBody]List<SourceDTO> model)
        {

            try
            {
                var result = "";
                foreach (var m in model)
                {
                    if (m.SourceId > 0)
                    {
                        result = sourcemgr.UpdateSource(m);
                        if(result== "Source Name Exists Already")
                        {
                            return Json(result);
                        }
                    }
                    else
                        result=sourcemgr.AddSource(m);
                }
                return Json(result);
                //return ResponseResult("Success");
            }
            catch (Exception ex)
            {
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Post TypeChannel
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost,Route("PostTypeChannel")]
        public IHttpActionResult PostTypeChannel([FromBody]List<Type_Channel> model)
        {

            try
            {
                var result = "";
                foreach (var m in model)
                {
                    if (m.ChannelId > 0)
                    {
                         result = sourcemgr.UpdateType_Channel(m);
                        if (result == "Channel Name Exists Already")
                        {
                            return Json(result);
                        }
                    }
                    else
                     result = sourcemgr.AddType_Channel(m);
                }
                return Json(result);
                //return ResponseResult("Success");
            }
            catch (Exception ex)
            {
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Post TypeOwner
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost,Route("PostTypeOwner")]
        public IHttpActionResult PostTypeOwner([FromBody]List<Type_Owner> model)
        {

            try
            {
                foreach (var m in model)
                {
                    if (m.OwnerId > 0)
                    {
                        sourcemgr.UpdateType_Owner(m);
                    }
                    else
                        sourcemgr.AddType_Owner(m);
                }
                return ResponseResult("Success");
            }
            catch (Exception ex)
            {
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get Lead Source Mapping
        /// </summary>
        /// <returns></returns>
        [HttpGet,Route("GetLeadSourceMapping")]
        public IHttpActionResult GetLeadSourceMapping()
       {
            try
            {
                var result = sourcemgr.GetLeadSourcesmapTP(0);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Post UTM Campaign
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost,Route("PostUTMCampaign")]
        public IHttpActionResult PostUTMCampaign(UTMCampaign model)
        {
            try
            {
                if (model != null)
                {
                    var result = sourcemgr.SaveUtmCampgn(model);
                    return ResponseResult(result);
                }
                else
                    return ResponseResult("Failed");
            }
            catch(Exception exp)
            {
                EventLogger.LogEvent(exp);
                return ResponseResult(exp);
            }
            
        }

        /// <summary>
        /// Get UTM Mapping
        /// </summary>
        /// <returns></returns>
        [HttpGet,Route("GetUTMMapping")]
        public IHttpActionResult GetUTMMapping()
        {
            try
            {
                var result = sourcemgr.GetUTMSourcesmap(0);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Source LeadType
        /// </summary>
        /// <returns></returns>
        [HttpGet,Route("SourceLeadType")]
        public IHttpActionResult SourceLeadType()
        {
            List<sourceLeadType> response = new List<sourceLeadType>();
            response.Add(new sourceLeadType
            {
                Key = "In Home Consultation",
                Value = "In Home Consultation"
            });
            response.Add(new sourceLeadType
            {
                Key = "Commercial",
                Value = "Commercial"
            });
            response.Add(new sourceLeadType
            {
                Key = "Design Guide",
                Value = "Design Guide"
            });
            response.Add(new sourceLeadType
            {
                Key = "Swatch",
                Value = "Swatch"
            });
            
            return Json(response);
        }
    }
}
