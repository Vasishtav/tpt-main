﻿using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using System;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    [RoutePrefix("api/Vendor")]
    public class VendorController : BaseController
    {

        /// <summary>
        /// Save
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult Post([FromBody]FranchiseVendor model)
        {
            if (model.VendorName != null || model.VendorName != "")
            {

                if (model.VendorType == 0)
                {
                    if (model.VendorName == "My Vendor")
                    {
                        model.VendorType = 3;
                    }
                    else if (model.VendorName == "Alliance Vendor")
                    {
                        model.VendorType = 1;
                    }
                    else
                    {
                        model.VendorType = 2;
                    }
                }
                if (model.VendorType == 1 && model.IsAlliance == true)
                {
                    model.OrderingMethod = ""; model.OrderingName = ""; model.ContactDetails = "";
                }
                if (model.VendorType != 1 && model.VendorType != 2)
                {
                    if (model.Currency == 0 && (model.CurrencyValueCode != null || model.CurrencyValueCode != ""))
                    {
                        if (model.CurrencyValueCode == "USD")
                        {
                            model.Currency = 223;
                        }
                        else if (model.CurrencyValueCode == "CAD")
                        {
                            model.Currency = 39;
                        }
                    }
                }

                //if (model.CreditLimit != "")
                //{
                //    //var value = Convert.ToString(model.CreditLimit);

                //    model.CreditLimit = model.CreditLimit.Replace("$0", "");
                //    model.CreditLimit = model.CreditLimit.Replace(".00", "");
                //    model.CreditLimit = model.CreditLimit.Replace("$", "");
                //    model.CreditLimit = model.CreditLimit.Replace(",", "");
                //}
                VendorManager VendorMngr = new VendorManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var Status = VendorMngr.Save(model);
                return Json(new { data = Status });
                //return ResponseResult("Success");
            }

            return ResponseResult("Failed");
        }

        /// <summary>
        /// spVendor_New
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("spVendor_New")]
        public Nullable<int> spVendor_New()
        {
            VendorManager VendorMngr = new VendorManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var vendorNumber = VendorMngr.spVendor_New();
            return vendorNumber;
        }

        /// <summary>
        /// Displays all value in list kendo
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetVendors")]
        public IHttpActionResult GetVendors()
        {
            VendorManager VendorMngr = new VendorManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var result = VendorMngr.GetVendors();
            return Json(result);
        }

        /// <summary>
        /// Get vendors details to view
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Route("Get/{id}")]
        public IHttpActionResult Get(int id)
        {
            VendorManager VendorMngr = new VendorManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var result = VendorMngr.GetLocalvendor(id);
            return Json(result);

        }

        /// <summary>
        /// Get vendors details to view
        /// </summary>
        /// <param name="id"></param>
        /// <param name="FranchiseID"></param>
        /// <returns></returns>
        [HttpGet, Route("Get/{id}/{FranchiseID}")]
        public IHttpActionResult Get(int id, int FranchiseID)
        {
            VendorManager VendorMngr = new VendorManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var result = VendorMngr.GetLocalvendor(id, FranchiseID);
            return Json(result);

        }

        /// <summary>
        /// To get value based on option selected(drop-down)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Route("VendorOption/{id}")]
        public IHttpActionResult VendorOption(int id)
        {
            VendorManager VendorMngr = new VendorManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var result = VendorMngr.Getvalue(id);
            return Json(result);
        }

        /// <summary>
        /// To fetch product details to vendor view page
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Route("GetProductValue/{id}")]
        public IHttpActionResult GetProductValue(int id)
        {
            VendorManager VendorMngr = new VendorManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var result = VendorMngr.GetPrdtValue(id);
            return Json(result);
        }

        /// <summary>
        /// on clicking on checkbox value display(in-active)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="Dropvalue"></param>
        /// <returns></returns>
        [HttpGet, Route("GetVendorsClick/{id}/{Dropvalue}")]
        public IHttpActionResult GetVendorsClick(int id, int Dropvalue)
        {
            VendorManager VendorMngr = new VendorManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var result = VendorMngr.GetVendorsDatas(id, Dropvalue);
            return Json(result);
        }

        /// <summary>
        /// Get Account Related Vendor
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Route("GetAccountRelVendor/{id}")]
        public IHttpActionResult GetAccountRelVendor(int id)
        {
            VendorManager VendorMngr = new VendorManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var result = VendorMngr.GetRelatedAccount(id);
            return Json(result);
        }

    }
}
