﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    [RoutePrefix("api/AdditionalContact")]
    public class AdditionalContactController : BaseController
    {
        /// <summary>
        /// Get the Lead Contacts
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="showDeleted">showDeleted</param>
        /// <returns></returns>
        [HttpGet, Route("GetLeadContacts")]
        public IHttpActionResult GetLeadContacts(int id, bool showDeleted)
        {
            try
            {
                AdditionalContactManager contactManager = new AdditionalContactManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = contactManager.GetLeadContacts(id, showDeleted);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get the Account Contacts
        /// </summary>
        /// <param name="id">Account Id</param>
        /// <param name="showDeleted">showDeleted</param>
        /// <returns></returns>
        [HttpGet, Route("GetAccountContacts")]
        public IHttpActionResult GetAccountContacts(int id, bool showDeleted)
        {
            try
            {
                AdditionalContactManager contactManager = new AdditionalContactManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = contactManager.GetAccountContacts(id, showDeleted);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Add New Contact
        /// </summary>
        /// <remarks>
        /// Notes
        /// 
        ///     POST 
        ///     {
        ///        "AssociatedSource": 1, //Lead = 1,Account = 2,Opportunity = 3
        ///     }
        ///     
        /// </remarks>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, Route("AddNewContact")]
        public IHttpActionResult AddNewContact(CustomerTP model)
        {
            try
            {
                AdditionalContactManager contactManager = new AdditionalContactManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var status = "Save Failed. Please try again";
                if (model != null)
                {
                    model.LeadId = (model.LeadId > 0) ? model.LeadId : null;
                    model.AccountId = (model.AccountId > 0) ? model.AccountId : null;
                    model.OpportunityId = (model.OpportunityId > 0) ? model.OpportunityId : null;
                    model.CreatedOn = DateTime.Now;
                    model.CreatedBy = SessionManager.CurrentUser.PersonId;
                    status = contactManager.Add(model); // noteMgr.Add(model);
                }
                return ResponseResult(status);

            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Update Contact
        /// </summary>
        /// <remarks>
        /// Notes
        /// 
        ///     POST 
        ///     {
        ///        "AssociatedSource": 1, //Lead = 1,Account = 2,Opportunity = 3
        ///     }
        ///     
        /// </remarks>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, Route("UpdateContact")]
        public IHttpActionResult UpdateContact(CustomerTP model)
        {

            try
            {
                AdditionalContactManager contactManager = new AdditionalContactManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var status = "Update Failed. Please try again";
                if (model != null)
                {
                    model.UpdatedOn = DateTime.Now;
                    model.UpdatedBy = SessionManager.CurrentUser.PersonId;
                    status = contactManager.Update(model);  //noteMgr.Update(model);
                }
                return ResponseResult(status);

            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Can be Deleted
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Route("CanbeDeleted/{id}")]
        public IHttpActionResult CanbeDeleted(int id)
        {
            try
            {
                AdditionalContactManager contactManager = new AdditionalContactManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = contactManager.CanbeDeleted(id);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {

            try
            {
                AdditionalContactManager contactManager = new AdditionalContactManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var status = "Update Failed. Please try again";
                status = contactManager.Delete(id);

                return ResponseResult(status);

            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
    }
}
