﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    /// <summary>
    /// News and Updates
    /// </summary>
    public class NewsUpdatesController : BaseController
    {
        /// <summary>
        /// Add/Create News
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, Route("api/NewsUpdates/AddNews")]
        public IHttpActionResult AddNews(NewsUpdates model)
        {
            try
            {
                var user = Getuser();
                NewsUpdatesManager numanager = new NewsUpdatesManager(user);
                var newsId = 0;
                var status = "Invalid data. Save Failed. Please try again";
                if (model != null)
                {
                    model.CreatedBy = user.PersonId;
                    status = numanager.CreateNews(out newsId, model);
                }
                return ResponseResult(status, Json(new { NewsUpdatesId = newsId }));
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Update News
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, Route("api/NewsUpdates/UpdateNews")]
        public IHttpActionResult UpdateNews(NewsUpdates model)
        {

            try
            {
                var user = Getuser();
                NewsUpdatesManager numanager = new NewsUpdatesManager(user);
                var status = "Invalid data. Update Failed. Please try again";
                if (model != null)
                {
                    model.LastUpdatedBy = user.PersonId;
                    status = numanager.Update(model);
                }
                return ResponseResult(status, Json(new { NewsUpdatesId = model.NewsUpdatesId }));

            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Activate News
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        [HttpPost, Route("api/NewsUpdates/ActivateNews/{newsId}")]
        public IHttpActionResult ActivateNews(int newsId)
        {

            try
            {
                var user = Getuser();
                NewsUpdatesManager numanager = new NewsUpdatesManager(user);
                var status = "Invalid data. Activation Failed. Please try again";
                if (newsId > 0)
                {
                    status = numanager.Activate(newsId);
                }
                return ResponseResult(status, Json(new { NewsUpdatesId = newsId }));
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// MoveUp
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        [HttpPost, Route("api/NewsUpdates/MoveUp/{newsId}")]
        public IHttpActionResult MoveUp(int newsId)
        {

            try
            {
                var user = Getuser();
                NewsUpdatesManager numanager = new NewsUpdatesManager(user);
                var status = "Invalid data. Move up or down Failed. Please try again";
                if (newsId > 0)
                {
                    status = numanager.MoveUp(newsId);
                }
                return ResponseResult(status, Json(new { NewsUpdatesId = newsId }));
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// MoveDown
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        [HttpPost, Route("api/NewsUpdates/MoveDown/{newsId}")]
        public IHttpActionResult MoveDown(int newsId)
        {
            try
            {
                var user = Getuser();
                NewsUpdatesManager numanager = new NewsUpdatesManager(user);
                var status = "Invalid data. Move up or down Failed. Please try again";
                if (newsId > 0)
                {
                    status = numanager.MoveDown(newsId);
                }
                return ResponseResult(status, Json(new { NewsUpdatesId = newsId }));
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Change Sorting
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="move"></param>
        /// <returns></returns>
        [HttpPost, Route("api/NewsUpdates/ChangeSorting/{newsId}/{move}")]
        public IHttpActionResult ChangeSorting(int newsId, int move)
        {
            try
            {
                if (newsId <= 0 || move == 0)
                    return Json("Failed, newsId <= 0 (or) move == 0");

                NewsUpdatesManager numanager = new NewsUpdatesManager(Getuser());
                var status = numanager.ChangeSorting(newsId, move);
                return Json(ContantStrings.Success);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// DeactivateNews
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        [HttpPost, Route("api/NewsUpdates/DeactivateNews/{newsId}")]
        public IHttpActionResult DeactivateNews(int newsId)
        {
            try
            {
                var user = Getuser();
                NewsUpdatesManager numanager = new NewsUpdatesManager(user);
                var status = "Invalid data. Deactivation Failed. Please try again";
                if (newsId > 0)
                {
                    status = numanager.Deactivate(newsId);
                }
                return ResponseResult(status, Json(new { NewsUpdatesId = newsId }));
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// GetNews  
        /// if newsId >0 then return the particular newsid info else return all the news
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        [HttpGet, Route("api/NewsUpdates/GetNews/{newsId}")]
        public IHttpActionResult GetNews(int newsId)
        {
            try
            {
                var user = Getuser();
                NewsUpdatesManager numanager = new NewsUpdatesManager(user);
                NewsUpdates news = null;
                IList<NewsUpdates> allNews = new List<NewsUpdates>();
                if (newsId > 0)
                {
                    news = numanager.Get(newsId);
                    allNews.Add(news);
                }
                else
                    allNews = numanager.GetAllNews();
                return Json(allNews);

            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// GetCurrentNews for Franchise
        /// This api can be used only in Franchise login
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("api/NewsUpdates/GetCurrentNews")]
        public IHttpActionResult GetCurrentNews()
        {
            try
            {
                var user = Getuser();
                var Franchise = GetFranchise();
                NewsUpdatesManager numanager = new NewsUpdatesManager(user);
                var news = numanager.GetCurrentNews(Franchise.BrandId);
                return Json(news);

            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

    }
}
