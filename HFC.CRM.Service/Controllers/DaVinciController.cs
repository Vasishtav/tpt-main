﻿using HFC.CRM.Data;
using HFC.CRM.DTO;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Linq;
using System.Web.Http.Description;
using HFC.CRM.Service.Models;
using System.Text;
using HFC.CRM.Core.Logs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace HFC.CRM.Service.Controllers
{
    /// <summary>
    /// DaVinci API methods
    /// </summary>
    public class DaVinciController : BaseController
    {
        /// <summary>
        /// Get Opportunity List
        /// </summary>
        /// <remarks>

        /// <returns></returns>
        [HttpGet, Route("api/GetOpportunityList")]
        [ResponseType(typeof(Response<List<DavinciOpportunityInfo>>))]
        public IHttpActionResult GetOpportunityList()
        {
            try
            {
                var user = Getuser();
                var franchise = GetFranchise();

                EventLogger.LogEvent(user.UserName, "DaVinciController GetOpportunityList call started", LogSeverity.Information);

                DaVinciManager mgr = new DaVinciManager(user, franchise);
                var lstopp = mgr.GetOpportunityList();

                EventLogger.LogEvent(user.UserName + " | " + JsonConvert.SerializeObject(lstopp), "DaVinciController GetOpportunityList call ended", LogSeverity.Information);
                var response = new Response(true, "", lstopp);
                return Ok(response);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Update Opportunity Status
        /// </summary>
        /// <returns></returns>
        [HttpPut, Route("api/UpdateOpportunityStatus/{OpportunityId}/{StatusId}")]
        [ResponseType(typeof(Response))]
        public IHttpActionResult UpdateOpportunityStatus(int OpportunityId, int StatusId)
        {
            try
            {
                var user = Getuser();
                var franchise = GetFranchise();
                EventLogger.LogEvent(user.UserName + " | " + OpportunityId + " | " + StatusId, "DaVinciController UpdateOpportunityStatus call started", LogSeverity.Information);

                DaVinciManager mgr = new DaVinciManager(user, franchise);
                var res = mgr.UpdateOpportunitiestatus(OpportunityId, StatusId);

                EventLogger.LogEvent(user.UserName, "DaVinciController UpdateOpportunityStatus call ended", LogSeverity.Information);
                var response = new Response(true, "Status Updated Successfully");
                return Ok(response);
            }
            catch (Exception ex)
            {
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get the list of opportunity status in TPT
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("api/GetOpportunityStatus")]
        [ResponseType(typeof(Response<List<OpportunityStatusModel>>))]
        public IHttpActionResult GetOpportunityStatus()
        {
            try
            {
                var user = Getuser();
                var franchise = GetFranchise();
                EventLogger.LogEvent(user.UserName, "DaVinciController GetOpportunityStatus call started", LogSeverity.Information);

                DaVinciManager mgr = new DaVinciManager(user, franchise);
                var oppstatus = mgr.GetOpportunityStatus();

                var res = (from op in oppstatus
                           select new OpportunityStatusModel
                           {
                               StatusId = op.OpportunityStatusId,
                               Name = op.Name,
                               Description = op.Description
                           }).ToList();

                EventLogger.LogEvent(user.UserName, "DaVinciController GetOpportunityStatus call ended", LogSeverity.Information);

                var response = new Response(true, "", res);
                return Ok(response);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Create Quote
        /// </summary>
        /// <remarks>
        /// **Error Codes** <br />
        /// Code: 1000	Description: Quote object is empty                     <br />
        /// Code: 1001	Description: Invalid OpportunityId                     <br />
        /// Code: 1002	Description: DVI file is missing                       <br />
        /// Code: 1003	Description: Vendor is Required                        <br />
        /// Code: 1004	Description: ProductCategory is Required               <br />
        /// Code: 1005	Description: SubCategory is Required                   <br />
        /// Code: 1006	Description: Cost should not empty or less than zero   <br />
        /// Code: 1007	Description: Quantity is Required                      <br />
        /// Code: 1008	Description: Minimun one Quote line per Quote          <br />
        /// Code: 1009	Description: RoomName is Required                      <br />
        /// Code: 1010	Description: ProductName is Required                   <br />
        /// Code: 1011	Description: Description is Required                   <br />
        /// Code: 1012	Description: UnitPrice is Required                     <br />
        /// Code: 1013	Description: Minimum one image per Quote line          <br />
        /// Code: 1014	Description: Minimum one PartsDetail per Quote line    <br />
        /// Code: 1015	Description: Totalcost  is Required                    <br />
        /// Code: 1016	Description: Totalmargin  is Required                  <br />
        /// Code: 1017	Description: Totalprice  is Required                   <br />
        /// Code: 1018	Description: Filename is Required                      <br />
        /// Code: 1019	Description: Invalid ProductCategory                   <br />
        /// Code: 1020	Description: Invalid Subcategory                       <br />
        /// Code: 1021	Description: Invalid Part Id                           <br />
        /// Code: 1022	Description: Image file is missing                     <br />
        /// </remarks>
        /// <returns></returns>
        [HttpPost, Route("api/CreateQuote")]
        [ResponseType(typeof(Response<QuoteCreateResponse>))]
        public IHttpActionResult CreateQuote(QuoteDavinci Quote)
        {
            try
            {
                var user = Getuser();
                var franchise = GetFranchise();
                EventLogger.LogEvent(user.UserName + " | " + JsonConvert.SerializeObject(Quote), "DaVinciController CreateQuote call started", LogSeverity.Information);

                string validationmessage = QuoteValidation(user, franchise, Quote);
                if (validationmessage != "")
                {
                    JObject obj = JObject.Parse(validationmessage);
                    var eresponse = new Response(false, "", obj);
                    return Ok<Response>(eresponse);
                }

                DaVinciManager mgr = new DaVinciManager(user, franchise);
                int QuoteKey = mgr.CreateQuote(Quote);
                EventLogger.LogEvent(user.UserName + " | " + QuoteKey, "DaVinciController CreateQuote call ended", LogSeverity.Information);
                var response = new Response(true, "Quote Created Successfully", new { quoteid = QuoteKey });
                return Ok(response);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Update Quote
        /// </summary>
        /// <param name="quoteid"></param>
        /// <param name="Quote"></param>
        /// <remarks>
        /// **Error Codes** <br />
        /// Code: 1001	Description: Invalid OpportunityId                     <br />
        /// Code: 1002	Description: DVI file is missing                       <br />
        /// Code: 1003	Description: Vendor is Required                        <br />
        /// Code: 1004	Description: ProductCategory is Required               <br />
        /// Code: 1005	Description: SubCategory is Required                   <br />
        /// Code: 1006	Description: Cost should not empty or less than zero   <br />
        /// Code: 1007	Description: Quantity is Required                      <br />
        /// Code: 1008	Description: Minimun one Quote line per Quote          <br />
        /// Code: 1009	Description: RoomName is Required                      <br />
        /// Code: 1010	Description: ProductName is Required                   <br />
        /// Code: 1011	Description: Description is Required                   <br />
        /// Code: 1012	Description: UnitPrice is Required                     <br />
        /// Code: 1013	Description: Minimum one image per Quote line          <br />
        /// Code: 1014	Description: Minimum one PartsDetail per Quote line    <br />
        /// Code: 1015	Description: Totalcost  is Required                    <br />
        /// Code: 1016	Description: Totalmargin  is Required                  <br />
        /// Code: 1017	Description: Totalprice  is Required                   <br />
        /// Code: 1018	Description: Filename is Required                      <br />
        /// Code: 1019	Description: Invalid ProductCategory                   <br />
        /// Code: 1020	Description: Invalid Subcategory                       <br />
        /// Code: 1021	Description: Invalid Part Id                           <br />
        /// Code: 1022	Description: Image file is missing                     <br />
        /// </remarks>
        /// <returns></returns>
        [HttpPut, Route("api/UpdateQuote")]
        [ResponseType(typeof(Response))]
        public IHttpActionResult UpdateQuote(int quoteid, QuoteDavinci Quote)
        {
            try
            {
                var user = Getuser();
                var franchise = GetFranchise();
                EventLogger.LogEvent(user.UserName + " | " + JsonConvert.SerializeObject(Quote), "DaVinciController UpdateQuote call started", LogSeverity.Information);

                if (quoteid <= 0)
                {
                    var eresponse = new Response(false, "quoteid should greater than zero");
                    return Ok<Response>(eresponse);
                }

                string validationmessage = QuoteValidation(user, franchise, Quote);
                if (validationmessage != "")
                {
                    var eresponse = new Response(false, validationmessage);
                    return Ok<Response>(eresponse);
                }

                DaVinciManager mgr = new DaVinciManager(user, franchise);
                mgr.UpdateQuote(quoteid, Quote);
                EventLogger.LogEvent(user.UserName + " | " + quoteid, "DaVinciController UpdateQuote call ended", LogSeverity.Information);
                var response = new Response(true, "Quote Updated Successfully");
                return Ok(response);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="franchise"></param>
        /// <param name="Quote"></param>
        /// <returns></returns>
        private string QuoteValidation(User user, Franchise franchise, QuoteDavinci Quote)
        {
            DaVinciManager mgr = new DaVinciManager(user, franchise);
            Dictionary<Int32, string> error = new Dictionary<Int32, string>();

            if (Quote != null)
            {
                // validate opportunity id
                if (Quote.OpportunityId <= 0)
                    error.Add(1001, GetErrorMessage(1001));
                else
                {
                    var res = mgr.checkOpportunityIdExist(Quote.OpportunityId, franchise.FranchiseId);
                    if (res == false)
                        error.Add(1001, GetErrorMessage(1001));
                }

                // validate dvi file and file name
                if (Quote.File == null)
                    error.Add(1002, GetErrorMessage(1002));
                else
                {
                    if (Quote.File.File_Base64String == null || Quote.File.File_Base64String.Length == 0)
                        error.Add(1002, GetErrorMessage(1002));

                    if (string.IsNullOrEmpty(Quote.File.FileName) || Quote.File.FileName == "")
                        error.Add(1018, GetErrorMessage(1018));
                }

                //totalcost
                if (Quote.totalcost <= 0)
                    error.Add(1015, GetErrorMessage(1015));

                //totalmargin
                //if (Quote.totalmargin <= 0)
                //    error.Add(1016, GetErrorMessage(1016));

                //totalmargin
                if (Quote.totalprice <= 0)
                    error.Add(1017, GetErrorMessage(1017));

                #region Header level part detail check

                if (Quote.PartsDetail != null && Quote.PartsDetail.Length > 0)
                {
                    foreach (var pd in Quote.PartsDetail)
                    {
                        //check part id
                        if (string.IsNullOrEmpty(pd.partid) || pd.partid == "")
                            if (!error.ContainsKey(1021))
                                error.Add(1021, GetErrorMessage(1021));

                        //check qty
                        if (pd.qty <= 0)
                            if (!error.ContainsKey(1007))
                                error.Add(1007, GetErrorMessage(1007));


                        //check vendor
                        if (string.IsNullOrEmpty(pd.vendor) || pd.vendor == "")
                            if (!error.ContainsKey(1003))
                                error.Add(1003, GetErrorMessage(1003));

                        //check productcategory
                        if (string.IsNullOrEmpty(pd.productcategory) || pd.productcategory == "")
                        {
                            if (!error.ContainsKey(1004))
                                error.Add(1004, GetErrorMessage(1004));
                        }
                        else
                        {
                            //var res = mgr.checkProductCategoryExist(pd.productcategory);
                            //if (res == false)
                            //    if (!error.ContainsKey(1004))
                            //        error.Add(1019, pd.productcategory + " - " + GetErrorMessage(1019));
                        }

                        // check subcategory
                        if (string.IsNullOrEmpty(pd.subcategory) || pd.subcategory == "")
                        {
                            if (!error.ContainsKey(1005))
                                error.Add(1005, GetErrorMessage(1005));
                        }
                        else
                        {
                            //var res = mgr.checkProductCategoryExist(pd.subcategory);
                            //if (res == false)
                            //    if (!error.ContainsKey(1020))
                            //        error.Add(1020, pd.subcategory + " - " + GetErrorMessage(1020));
                        }

                        //check cost
                        if (pd.cost <= 0)
                            if (!error.ContainsKey(1006))
                                error.Add(1006, GetErrorMessage(1006));
                    }
                }
                #endregion

                #region Quote Lines 

                if (Quote.QuoteLines != null && Quote.QuoteLines.Length > 0)
                {
                    foreach (var ql in Quote.QuoteLines)
                    {
                        //Room name
                        if (string.IsNullOrEmpty(ql.RoomName) || ql.RoomName == "")
                            if (!error.ContainsKey(1009))
                                error.Add(1009, GetErrorMessage(1009));

                        //ProductName
                        if (string.IsNullOrEmpty(ql.ProductName) || ql.ProductName == "")
                            if (!error.ContainsKey(1010))
                                error.Add(1010, GetErrorMessage(1010));

                        //Description
                        //if (string.IsNullOrEmpty(ql.Description) || ql.Description == "")
                        //    if (!error.ContainsKey(1011))
                        //        error.Add(1011, GetErrorMessage(1011));

                        //UnitPrice
                        if (ql.UnitPrice <= 0)
                            if (!error.ContainsKey(1012))
                                error.Add(1012, GetErrorMessage(1012));

                        //totalcost
                        if (ql.totalcost <= 0)
                            if (!error.ContainsKey(1015))
                                error.Add(1015, GetErrorMessage(1015));

                        //totalmargin
                        //if (ql.totalmargin <= 0)
                        //    if (!error.ContainsKey(1016))
                        //        error.Add(1016, GetErrorMessage(1016));

                        //totalmargin
                        if (ql.totalprice <= 0)
                            if (!error.ContainsKey(1017))
                                error.Add(1017, GetErrorMessage(1017));

                        //image file
                        if (ql.image != null && ql.image.Count >= 0)
                        {
                            foreach (var im in ql.image)
                            {
                                if (im == null)
                                {
                                    if (!error.ContainsKey(1022))
                                        error.Add(1022, GetErrorMessage(1022));
                                }
                                else
                                {
                                    if (im.File_Base64String == null || im.File_Base64String.Length == 0)
                                        if (!error.ContainsKey(1022))
                                            error.Add(1022, GetErrorMessage(1022));

                                    if (string.IsNullOrEmpty(im.FileName) || im.FileName == "")
                                        if (!error.ContainsKey(1018))
                                            error.Add(1018, GetErrorMessage(1018));
                                }
                            }
                        }
                        //else
                        //{
                        //    if (!error.ContainsKey(1013))
                        //        error.Add(1013, GetErrorMessage(1013));
                        //}



                        #region Line level part detail check

                        if (ql.PartsDetail != null && ql.PartsDetail.Length > 0)
                        {
                            foreach (var pd in Quote.PartsDetail)
                            {
                                //check part id
                                if (string.IsNullOrEmpty(pd.partid) || pd.partid == "")
                                    if (!error.ContainsKey(1021))
                                        error.Add(1021, GetErrorMessage(1021));

                                //check qty
                                if (pd.qty <= 0)
                                    if (!error.ContainsKey(1007))
                                        error.Add(1007, GetErrorMessage(1007));

                                //check vendor
                                if (string.IsNullOrEmpty(pd.vendor) || pd.vendor == "")
                                    if (!error.ContainsKey(1003))
                                        error.Add(1003, GetErrorMessage(1003));

                                //check productcategory
                                if (string.IsNullOrEmpty(pd.productcategory) || pd.productcategory == "")
                                {
                                    if (!error.ContainsKey(1004))
                                        error.Add(1004, GetErrorMessage(1004));
                                }
                                else
                                {
                                    //var res = mgr.checkProductCategoryExist(pd.productcategory);
                                    //if (res == false)
                                    //    if (!error.ContainsKey(1019))
                                    //        error.Add(1019, pd.productcategory + " - " + GetErrorMessage(1019));
                                }

                                // check subcategory
                                if (string.IsNullOrEmpty(pd.subcategory) || pd.subcategory == "")
                                {
                                    if (!error.ContainsKey(1005))
                                        error.Add(1005, GetErrorMessage(1005));
                                }
                                else
                                {
                                    //var res = mgr.checkProductCategoryExist(pd.subcategory);
                                    //if (res == false)
                                    //    if (!error.ContainsKey(1020))
                                    //        error.Add(1020, pd.subcategory + " - " + GetErrorMessage(1020));
                                }

                                //check cost
                                if (pd.cost <= 0)
                                    if (!error.ContainsKey(1006))
                                        error.Add(1006, GetErrorMessage(1006));
                            }
                        }
                        else
                        {
                            if (!error.ContainsKey(1014))
                                error.Add(1014, GetErrorMessage(1014));
                        }
                        #endregion
                    }
                }
                else
                {
                    if (!error.ContainsKey(1008))
                        error.Add(1008, GetErrorMessage(1008));
                }
                #endregion
            }
            else
                error.Add(1000, "Quote object is empty");

            return error.Count > 0 ? JsonConvert.SerializeObject(error) : "";
        }

        private string GetErrorMessage(int code)
        {
            switch (code)
            {
                default: return "";
                case 1000:
                    return "Quote object is empty ";
                case 1001:
                    return "Invalid OpportunityId ";
                case 1002:
                    return "DVI file is missing ";
                case 1003:
                    return "Vendor is Required ";
                case 1004:
                    return "ProductCategory is Required ";
                case 1005:
                    return "SubCategory is Required ";
                case 1006:
                    return "Cost should not empty or less than zero ";
                case 1007:
                    return "Quantity is Required ";
                case 1008:
                    return "Minimun one Quote line per Quote ";
                case 1009:
                    return "RoomName is Required ";
                case 1010:
                    return "ProductName is Required ";
                case 1011:
                    return "Description is Required ";
                case 1012:
                    return "UnitPrice is Required ";
                case 1013:
                    return "Minimum one image per Quote line ";
                case 1014:
                    return "Minimum one PartsDetail per Quote line ";
                case 1015:
                    return "Totalcost  is Required ";
                case 1016:
                    return "Totalmargin  is Required ";
                case 1017:
                    return "Totalprice  is Required ";
                case 1018:
                    return "Filename  is Required ";
                case 1019:
                    return "Invalid ProductCategory ";
                case 1020:
                    return "Invalid Subcategory ";
                case 1021:
                    return "Invalid Part Id ";
                case 1022:
                    return "Image file is missing ";

            }
        }

    }
}
