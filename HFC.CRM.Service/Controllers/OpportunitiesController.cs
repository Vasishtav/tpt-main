﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using HFC.CRM.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    [RoutePrefix("api/OpportunitiesTP")]
    public class OpportunitiesController : BaseController
    {
        /// <summary>
        /// Get Opportunity by OpportunityId
        /// </summary>
        /// <param name="id">OpportunityId</param>
        /// <returns></returns>
        [HttpGet, Route("Get/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                OpportunitiesManager OpportunityMgr = new OpportunitiesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                AccountsManager actMgr = new AccountsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                CommunicationManager communication_Mangr = new CommunicationManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                if (id <= 0)
                {
                    throw new Exception("Opportunity Id is required");
                }

                int totalRecords = 0;

                var opportunity = OpportunityMgr.Get(id);

                var FranchiseAddress = SessionManager.CurrentFranchise.Address.ToString();
                var FranchiseAddressObject = SessionManager.CurrentFranchise.Address;
                var Addresses = OpportunityMgr.GetAddress(new List<int>() { id });
                var States = CacheManager.StatesCollection.Select(s => new { s.StateAbv, s.StateName, s.CountryISO2 });
                var Countries = CacheManager.StatesCollection.Select(s => new { s.Type_Country.ISOCode2Digits, s.Type_Country.Country, s.Type_Country.ZipCodeMask }).Distinct();
                var OpportunityNotes = OpportunityMgr.GetNotes(new List<int>() { opportunity.OpportunityId }, false).Where(n => n.TypeEnum != NoteTypeEnum.Primary);
                //var NoteTypeEnum = NoteTypeEnum.Internal.ToDictionary<byte>((byte)NoteTypeEnum.Primary);
                var PurchaseOrdersCount = totalRecords;
                var SelectedOpportunityStatus = opportunity.OpportunityStatus.Name;
                var selectedCampaign = opportunity.CampaignName;
                var OpportunityQuestionAns = opportunity.OpportunityQuestionAns;
                var PrimarySource = opportunity.SourcesList.Where(x => x.IsPrimarySource == true).FirstOrDefault();
                var SecondarySource = opportunity.SourcesList.Where(x => x.IsPrimarySource == false).ToList();
                opportunity.AccountTP = actMgr.Get(opportunity.AccountId);

                return
                    Ok(
                        new
                        {
                            Opportunity = opportunity,
                            OpportunityPrimaryNotes = opportunity.OpportunityNotes.FirstOrDefault(n => n.TypeEnum == NoteTypeEnum.Primary),
                            //OpportunityStatuses = SessionManager.CurrentFranchise.OpportunityStatusTypeCollection(),
                            FranchiseAddress = SessionManager.CurrentFranchise.Address.ToString(),
                            FranchiseAddressObject = SessionManager.CurrentFranchise.Address,
                            Addresses = OpportunityMgr.GetAddress(new List<int>() { id }),
                            States = CacheManager.StatesCollection.Select(s => new { s.StateAbv, s.StateName, s.CountryISO2 }),
                            Countries = CacheManager.StatesCollection.Select(s => new { s.Type_Country.ISOCode2Digits, s.Type_Country.Country, s.Type_Country.ZipCodeMask }).Distinct(),
                            OpportunityNotes = OpportunityMgr.GetNotes(new List<int>() { opportunity.OpportunityId }, false).Where(n => n.TypeEnum != NoteTypeEnum.Primary),
                            NoteTypeEnum = NoteTypeEnum.Internal.ToDictionary<byte>((byte)NoteTypeEnum.Primary),
                            PurchaseOrdersCount = totalRecords,
                            SelectedOpportunityStatus = opportunity.OpportunityStatus.Name,
                            selectedCampaign = opportunity.CampaignName,
                            OpportunityQuestionAns = opportunity.OpportunityQuestionAns,  //Added for Qualification/Question Answers
                            PrimarySource = opportunity.SourcesList.Where(x => x.IsPrimarySource == true).FirstOrDefault(),
                            SecondarySource = opportunity.SourcesList.Where(x => x.IsPrimarySource == false).ToList(),
                            FranciseLevelTexting = SessionManager.CurrentFranchise.EnableTexting,
                            TextStatus = communication_Mangr.GetOptingInfo(opportunity.AccountTP.PrimCustomer.CellPhone)
                        });
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Sales Agents
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetSalesAgents")]
        public IHttpActionResult GetSalesAgents()
        {
            try
            {
                OpportunitiesManager OpportunityMgr = new OpportunitiesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = OpportunityMgr.GetSalesAgents();
                if (result == null)
                    return OkTP();
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }

        }

        /// <summary>
        /// Get Opportunity Access
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("checkOpportunityAccess")]
        public IHttpActionResult checkOpportunityAccess()
        {
            try
            {
                PermissionManager permissionManager = new PermissionManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var GetPermission = permissionManager.GetUserRolesPermissions();
                var ModuleData = GetPermission.Find(x => x.ModuleCode == "Opportunity");
                var specialPermission = ModuleData.SpecialPermission.Find(x => x.PermissionCode == "OpportunityAllRecordAccess").CanAccess;
                return Json(specialPermission);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get the Installer List
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetInstallers")]
        public IHttpActionResult GetInstallers()
        {
            try
            {
                OpportunitiesManager OpportunityMgr = new OpportunitiesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = OpportunityMgr.GetInstallers();
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get the Opportunity Status list
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("OpportunitiesStatus")]
        public IHttpActionResult OpportunitiesStatus()
        {
            try
            {
                OpportunitiesManager OpportunityMgr = new OpportunitiesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = OpportunityMgr.OpportunitiesStatus();
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get the Account list
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetAccounts")]
        public IHttpActionResult GetAccounts()
        {
            try
            {
                OpportunitiesManager OpportunityMgr = new OpportunitiesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = OpportunityMgr.GetAccounts();
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Gets a single Opportunity, will include a lot of opportunity detail
        /// </summary>
        /// <param name="id">opportunity Id</param>
        /// <returns></returns>
        [HttpGet, Route("getAccountName/{id}")]
        public IHttpActionResult getAccountName(int id)
        {
            try
            {
                OpportunitiesManager OpportunityMgr = new OpportunitiesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = OpportunityMgr.getAccountName(id);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Sales Agent Name
        /// </summary>
        /// <param name="id">PersonId</param>
        /// <returns></returns>
        [HttpGet, Route("getSalesAgentName/{id}")]
        public IHttpActionResult getSalesAgentName(int id)
        {
            try
            {
                if (id == 0)
                {
                    return Json("SalesAgent Not Found");
                }
                OpportunitiesManager OpportunityMgr = new OpportunitiesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = OpportunityMgr.getSalesAgentName(id);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Gets the Installtion Address Name
        /// </summary>
        /// <param name="id">InstallAddressId</param>
        /// <returns></returns>
        [HttpGet, Route("getInstalltionAddressName/{id}")]
        public IHttpActionResult getInstalltionAddressName(int id)
        {
            try
            {
                if (id == 0)
                {
                    return Json("InstallationAddress Not exist");
                }
                OpportunitiesManager OpportunityMgr = new OpportunitiesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = OpportunityMgr.getInstalltionAddressName(id);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get the Installtion Addresses list
        /// </summary>
        /// <param name="id">AddressId</param>
        /// <returns></returns>
        [HttpGet, Route("InstallationAddresses/{id}")]
        public IHttpActionResult InstallationAddresses(int id)
        {
            try
            {
                OpportunitiesManager OpportunityMgr = new OpportunitiesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = OpportunityMgr.InstallationAddresses(id);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Primary Phone and Email Details
        /// </summary>
        /// <param name="id">OpportunityId</param>
        /// <returns></returns>
        [HttpGet, Route("getPhoneEmail/{id}")]
        public IHttpActionResult getPhoneEmail(int id)
        {
            try
            {
                OpportunitiesManager OpportunityMgr = new OpportunitiesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = OpportunityMgr.getPhoneEmail(id);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get the Phone and Email for Order
        /// </summary>
        /// <param name="id">Order Id</param>
        /// <returns></returns>
        [HttpGet, Route("getPhoneEmailforOrder/{id}")]
        public IHttpActionResult getPhoneEmailforOrder(int id)
        {
            try
            {
                OpportunitiesManager OpportunityMgr = new OpportunitiesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = OpportunityMgr.getPhoneEmailforOrder(id);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Save Opportunity
        /// </summary>
        /// <param name="viewmodel"></param>
        /// <returns></returns>
        [HttpPost, Route("")]
        public IHttpActionResult Post([FromBody]OpportunityViewModel viewmodel)
        {
            Opportunity model = OpportunityViewModel.MapFrom(viewmodel);
            OpportunitiesManager OpportunityMgr = new OpportunitiesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            AccountsManager actMgr = new AccountsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            CommunicationManager communication_Mangr = new CommunicationManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

            int opportunityId = 0;

            //texting
            var CountryCode = SessionManager.CurrentFranchise.CountryCode;
            var BrandId = SessionManager.CurrentFranchise.BrandId;
            var Account = actMgr.Get(model.AccountId);
            if (model.SendMsg)
            {
                var SendOptinMessage = communication_Mangr.ForceOptinMessage(Account.PrimCustomer.CellPhone, BrandId, CountryCode, null, model.AccountId);
            }
            if (model.UncheckNotifyviaText)
            {
                var unchecknotify = actMgr.UpdateNotifyText(model.AccountId);
            }

            if (model.SourcesTPIdFromCampaign > 0) model.SourcesTPId = model.SourcesTPIdFromCampaign;

            if (model.OpportunityId > 0)
            {
                var updateStatus = OpportunityMgr.Update(model);
                return ResponseResult(updateStatus, Json(new { OpportunityId = model.OpportunityId }));
            }
            if (model.SkipDuplicateCheck == false)
            {
                var dulpicateList = OpportunityMgr.GetDuplicateOpportunitys(SearchDuplicateOpportunityEnum.All, string.Empty, model);
                if (dulpicateList.Count > 0)
                {
                    var dtos = dulpicateList.Select(v => new DuplicateOpportunityModel()
                    {
                        Address = v.Addresses.FirstOrDefault() != null ? v.Addresses.FirstOrDefault().Address1 : "",
                        OpportunityId = v.OpportunityId,
                        CellPhone = TelBuilder(v.PrimCustomer.CellPhone),
                        HomePhone = TelBuilder(v.PrimCustomer.HomePhone),
                        FaxPhone = TelBuilder(v.PrimCustomer.FaxPhone),
                        WorkPhone = TelBuilder(v.PrimCustomer.WorkPhone),
                        PrimaryEmail = v.PrimCustomer.PrimaryEmail,
                        SecondaryEmail = v.PrimCustomer.SecondaryEmail,
                        FullName = v.PrimCustomer.FullName,
                        OpportunityNumber = v.OpportunityNumber
                    }).Take(3);

                    return ResponseResult("Success", Json(new { lstDuplicates = dtos }));
                }
            }

            var status = OpportunityMgr.CreateOpportunity(out opportunityId, model, 0);

            return ResponseResult(status, Json(new { OpportunityId = opportunityId }));
        }

        private string TelBuilder(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return string.Empty;
            }
            str = "(" + str.Insert(3, ")");
            return str.Insert(8, "-");
        }
    }
}
