﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    public class AddressController : BaseController
    {
        public IHttpActionResult Get()
        {
            try
            {
                return Ok(new
                {
                    States = CacheManager.StatesCollection.Select(s => new
                    {
                        s.StateAbv,
                        s.StateName,
                        s.CountryISO2
                    }),
                    Countries = CacheManager.StatesCollection.Select(s => new
                    {
                        s.Type_Country.ISOCode2Digits,
                        s.Type_Country.Country,
                        s.Type_Country.ZipCodeMask
                    }).Distinct()
                });
            }

            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }
    }
}
