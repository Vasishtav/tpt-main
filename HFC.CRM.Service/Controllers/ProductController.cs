﻿using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using HFC.CRM.Service.Models;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    /// <summary>
    /// Admin - Core Product Management
    /// </summary>
    public class ProductController : BaseController
    {
        /// <summary>
        /// Get Get Core Product Models
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("api/Product/GetCoreProductModels")]
        public IHttpActionResult GetCoreProductModels()
        {
            try
            {
                var user = Getuser();
                ProductsManager productsManager = new ProductsManager(null, user);
                var modelList = productsManager.GetCoreProductModels();
                return Json(modelList);
            }
            catch (System.Exception ex)
            {
                return Json(new { error = "An error occurred, Please try again later." });
            }
        }

        /// <summary>
        /// Update Core Product Models
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, Route("api/Product/UpdateCoreProductModel")]
        public IHttpActionResult UpdateCoreProductModel([FromBody] HFCModels model)
        {
            try
            {
                var user = Getuser();
                ProductsManager productsManager = new ProductsManager(null, user);
                var status = productsManager.UpdateCoreProductModel(model);
                var modelList = productsManager.GetCoreProductModels();
                return Json(new { error = "", modelData = modelList });
            }
            catch (System.Exception ex)
            {
                return Json(new { error = "An error occurred, Please try again later." });
            }
        }
    }
}
