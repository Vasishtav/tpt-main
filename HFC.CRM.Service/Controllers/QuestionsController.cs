﻿using System;
using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using System.Web.Http;
using System.Collections.Generic;
using HFC.CRM.Core.Logs;
using HFC.CRM.Service.Models;

namespace HFC.CRM.Service.Controllers
{
    /// <summary>
    /// Questions
    /// </summary>
    [RoutePrefix("api/Questions")]
    public class QuestionsController : BaseController
    {
        /// <summary>
        /// Gets a single lead, will include a lot of lead detail
        /// </summary>
        /// <param name="id">Lead Id</param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            try
            {
                //Added for Qualification/Question Answers
                int BrandId = (int)SessionManager.BrandId;
                List<Type_Question> qualifyQuestion = HFC.CRM.Managers.TypeQuestionManager.GetTypeQuestionsById(BrandId, id);
                var questionList = qualifyQuestion;

                var LeadsDetails = HFC.CRM.Managers.TypeQuestionManager.GetLeadQuestions(id);

                return
                    Ok(
                        new
                        {
                            QuestionList = questionList
                            ,
                            LeadDetails = LeadsDetails
                        });
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Adds the question answer.
        /// </summary>
        /// <returns>IHttpActionResult.</returns>
        [HttpGet, Route("GetQuestionTypes")]
        public IHttpActionResult GetQuestionTypes()
        {
            //Added for Qualification/Question Answers - Gets the Type_Question
            int BrandId = (int)SessionManager.BrandId;
            return Json(HFC.CRM.Managers.TypeQuestionManager.GetTypeQuestions(BrandId));
        }

        /// <summary>
        /// Adds the question answer.
        /// </summary>
        /// <param name="leadId">The identifier.</param>
        /// <returns>IHttpActionResult.</returns>
        [HttpGet, Route("GetQuestionTypesAns/{leadId}")]
        public IHttpActionResult GetQuestionTypesAns(int leadId)
        {
            //Added for Qualification/Question Answers - Gets the Type_Question
            int BrandId = (int)SessionManager.BrandId;
            return Json(HFC.CRM.Managers.TypeQuestionManager.GetTypeQuestionsById(BrandId, leadId));
        }

        /// <summary>
        /// Adds the question answer.
        /// </summary>
        /// <param name="id">The identifier. leadId</param>
        /// <param name="model">The answer.</param>
        /// <returns>IHttpActionResult.</returns>
        [HttpPost, Route("SaveAllQuestionAnswer")]
        public IHttpActionResult SaveAllQuestionAnswer(int id, [FromBody]List<Type_QuestionViewModel> model)
        {
            var listAnswer = Type_QuestionViewModel.MapFrom(model);
            TypeQuestionManager QansMgr = new TypeQuestionManager(SessionManager.CurrentUser.PersonId, SessionManager.CurrentFranchise.FranchiseId);
            int answerId = 0;
            string status = "";
            if (listAnswer != null && id > 0)
            {
                status = QansMgr.SaveQuestionAnswers(id, listAnswer, out answerId);
            }
            //Return the reponse
            return ResponseResult("Success", Json(new { AnswerId = answerId }));
        }
    }
}
