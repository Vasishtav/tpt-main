﻿using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Core.Membership;
using HFC.CRM.Data;
using HFC.CRM.Data.Constants;
using HFC.CRM.DTO;
using HFC.CRM.DTO.Person;
using HFC.CRM.Managers;
using HFC.CRM.Service.Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    /// <summary>
    /// User Related operation 
    /// </summary>
    public class UserController : BaseController
    {
        #region UserInfo

        /// <summary>
        /// Get The User Information
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("api/GetUserInfo")]
        public IHttpActionResult GetUserInfo()
        {
            try
            {
                var user = Getuser();
                TimeZoneEnum feTimeZone = GetFranchiseTimezone();

                dynamic o = new ExpandoObject();

                dynamic Userinfo = new ExpandoObject();
                Userinfo.UserEmail = user.Email;
                Userinfo.UserName = user.Person.FirstName + " " + user.Person.LastName;

                o.Userinfo = Userinfo;
                return Ok<Response>(new Response(true, "", o));
            }
            catch (Exception ex)
            {
                return ResponseResult(ex);
            }
        }

        #endregion

        /// <summary>
        /// Get all User detail with roles
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("api/User/Get")]
        public IHttpActionResult Get()
        {
            try
            {
                FranchiseManager _franMgr = new FranchiseManager(SessionManager.SecurityUser);
                Franchise CurrentFranchise = SessionManager.CurrentFranchise;
                User CurrentUser = SessionManager.CurrentUser;

                if (CurrentUser == null)
                    throw new Exception("CurrentUser is null");

                if (CurrentUser.IsAdminInRole())
                    return Ok();

                if (CurrentFranchise == null && (CurrentUser.IsInRole(AppConfigManager.AppProductStrgMang.RoleId) ||
                    CurrentUser.IsInRole(AppConfigManager.AppPicRole.RoleId)) || CurrentUser.IsInRole(AppConfigManager.AppVendorRole.RoleId))
                {
                    List<Role> roles = SessionManager.CurrentUser.Roles.ToList();
                    return Ok(roles);
                }
                else if (CurrentFranchise == null)
                    throw new Exception("CurrentFranchise is null <br/>");

                var UserCollection = CurrentFranchise.UserCollection();
                if (UserCollection == null || UserCollection.Count == 0)
                    throw new Exception("UserCollection is null");

                // Check QBO is enabled in HFC Admin
                bool isQBEnabled = false;
                var qbapp = _franMgr.GetQbinfo();
                if (qbapp != null) isQBEnabled = qbapp.IsActive;

                List<UserCollectionDTO> users = new List<UserCollectionDTO>();
                foreach (var uc in UserCollection)
                {
                    try
                    {
                        string BGColorToRGB = "rbg(0,0,255)", FGColorToRGB = "rbg(0,0,255)";
                        UserCollectionDTO user = new UserCollectionDTO();
                        user.UserId = uc.UserId;
                        user.FullName = uc.Person.FullName;
                        user.PersonId = uc.PersonId;
                        user.ColorId = uc.ColorId;
                        user.Email = uc.Email;
                        user.AvatarSrc = uc.AvatarSrc ?? AppConfigManager.DefaultAvatarSrc;
                        user.EmailSignature = uc.EmailSignature;
                        user.InSales = uc.IsInRole(AppConfigManager.DefaultSalesRole.RoleId, AppConfigManager.DefaultSalesNoMargineRole.RoleId);
                        user.InInstall = uc.IsInRole(AppConfigManager.DefaultInstallRole.RoleId);
                        user.IsDisabled = uc.IsDisabled;
                        user.SelectColor = uc.UserId == CurrentUser.UserId ? "list-group-item active" : "list-group-item ng-binding ng-hide";
                        if (CacheManager.ColorPalettes != null)
                        {
                            if (CacheManager.ColorPalettes.Where(f => f.ColorId == uc.ColorId).FirstOrDefault() != null)
                                BGColorToRGB = CacheManager.ColorPalettes.Where(f => f.ColorId == uc.ColorId).FirstOrDefault().BGColorToRGB();
                            if (CacheManager.ColorPalettes.Where(f => f.ColorId == uc.ColorId).FirstOrDefault() != null)
                                FGColorToRGB = CacheManager.ColorPalettes.Where(f => f.ColorId == uc.ColorId).FirstOrDefault().FGColorToRGB();
                            user.color = string.Format("background-color:{1}{3};border-color:{1}{3};color:{2}{3}",
                                uc.ColorId, BGColorToRGB, FGColorToRGB, " !important");
                        }
                        else
                        {
                            user.color = string.Format(@"background-color:{1}{3};
                            border-color:{1}{3};color:{2}{3}",
                                uc.ColorId, BGColorToRGB, FGColorToRGB, " !important");
                        }

                        user.FirstName = uc.Person.FirstName;
                        user.Domain = uc.Domain;
                        user.Roles = uc.Roles;
                        users.Add(user);
                    }
                    catch (Exception ex)
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.Append("Environment: " + System.Environment.MachineName);
                        sb.Append("<br /><br />");
                        sb.Append("Loggedin User: " + SessionManager.SecurityUser.UserName + " (Id: " + SessionManager.SecurityUser.UserId + ")");
                        sb.Append("<br /><br />");
                        sb.Append("Error user Usercollection: " + uc.UserName + " (Id: " + uc.UserId + ")");
                        sb.Append("<br /><br />");
                        if (SessionManager.CurrentUser != null)
                        {
                            sb.Append("Current User: " + SessionManager.CurrentUser.UserName + " (Id: " + SessionManager.CurrentUser.UserId + ")");
                            sb.Append("<br /><br />");
                        }
                        if (SessionManager.CurrentFranchise != null)
                        {
                            sb.Append("Current Franchise: " + SessionManager.CurrentFranchise.Name + " (Id: " + SessionManager.CurrentFranchise.FranchiseId + ")");
                            sb.Append("<br /><br />");
                        }
                        sb.Append("Error Datetime: " + DateTime.UtcNow.ToString("MM/dd/yyyy HH:mm:ss"));
                        sb.Append("<br /><br />");
                        sb.Append("Error Message: " + ex.Message.ToString());
                        sb.Append("<br /><br />");
                        sb.Append("Error StackTrace: " + ex.StackTrace.ToString());
                        sb.Append("<br /><br />");

                        ExchangeManager mgr = new ExchangeManager();
                        List<string> toAddresses = AppConfigManager.GetConfig<string>("EmailId", "System Settings", "Error Tracking") != null ? AppConfigManager.GetConfig<string>("EmailId", "System Settings", "Error Tracking").Split(',').ToList() : null;
                        if (toAddresses != null && toAddresses.Count > 0)
                            mgr.SendEmail("bbtestus@budgetblinds.com", "Touchpoint Error: Franchise User Controller", sb.ToString(), toAddresses, null, null);

                    }
                }

                if (users != null && users.Count > 0)
                {
                    return Ok(new
                    {
                        Users = users.Where(u => u.IsDisabled != true).ToList(),
                        CurrentUser = new { PersonId = CurrentUser.PersonId, user = CurrentUser },
                        DisabledUsers = users.Where(u => u.IsDisabled == true).ToList(),
                        FranchiseIsSolaTechEnabled = CurrentFranchise.isSolaTechEnabled,
                        FranchiseCountryCode = CurrentFranchise.CountryCode,
                        BrandId = CurrentFranchise.BrandId,
                        FranchiseName = CurrentFranchise.Name,
                        IsQBEnabled = isQBEnabled,
                        FranciseLevelTexting = CurrentFranchise.EnableTexting,
                        FranciseLevelCase = CurrentFranchise.EnableCase,
                        FranciseLevelVendorMng = CurrentFranchise.EnableVendorManagement,
                        AdminElectronicSign = CurrentFranchise.EnableElectronicSignAdmin,
                        FranciseLevelElectronicSign = CurrentFranchise.EnableElectronicSignFranchise,
                        CurrentUser.Person.EnableEmailSignature,
                        CurrentUser.Person.AddEmailSignAllEmails
                    });
                }
                return Ok();
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("Environment: " + System.Environment.MachineName);
                sb.Append("<br /><br />");
                sb.Append("Loggedin User: " + SessionManager.SecurityUser.UserName + " (Id: " + SessionManager.SecurityUser.UserId + ")");
                sb.Append("<br /><br />");
                if (SessionManager.CurrentUser != null)
                {
                    sb.Append("Current User: " + SessionManager.CurrentUser.UserName + " (Id: " + SessionManager.CurrentUser.UserId + ")");
                    sb.Append("<br /><br />");
                }
                if (SessionManager.CurrentFranchise != null)
                {
                    sb.Append("Current Franchise: " + SessionManager.CurrentFranchise.Name + " (Id: " + SessionManager.CurrentFranchise.FranchiseId + ")");
                    sb.Append("<br /><br />");
                }

                sb.Append("Error Datetime: " + DateTime.UtcNow.ToString("MM/dd/yyyy HH:mm:ss"));
                sb.Append("<br /><br />");
                sb.Append("Error Message: " + ex.Message.ToString());
                sb.Append("<br /><br />");
                sb.Append("Error StackTrace: " + ex.StackTrace.ToString());
                sb.Append("<br /><br />");

                ExchangeManager mgr = new ExchangeManager();
                List<string> toAddresses = AppConfigManager.GetConfig<string>("EmailId", "System Settings", "Error Tracking") != null ? AppConfigManager.GetConfig<string>("EmailId", "System Settings", "Error Tracking").Split(',').ToList() : null;
                if (toAddresses != null && toAddresses.Count > 0)
                    mgr.SendEmail("bbtestus@budgetblinds.com", "Touchpoint Error: Franchise User Controller", sb.ToString(), toAddresses, null, null);
                EventLogger.LogEvent(ex);
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Users
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("api/User/GetUsersInfo")]
        public IHttpActionResult GetUsers()
        {
            var user = Getuser();
            if (user.IsAdminInRole())
            {
                UserManager usrmgr = new UserManager(null, null);
                var Users = usrmgr.GetUsersInfo();
                return Json(Users.OrderBy(o => o.Person.FullName).ToList());
            }
            else
            {
                var CurrentFranchise = GetFranchiseDetail();
                if (CurrentFranchise != null)
                {
                    var owners = CurrentFranchise.FranchiseOwners.Select(v => v.UserId);
                    var model = LocalMembership.GetUsers(CurrentFranchise.FranchiseId, "Person");
                    model.RemoveAll(v => owners.Contains(v.UserId));
                    return Json(model.OrderBy(o => o.Person.FullName).ToList());
                }
                else
                {
                    return Json("");
                }

            }
        }

        /// <summary>
        /// Get User
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        [HttpGet, Route("api/User/Get/{userid}")]
        public IHttpActionResult Get(string userid)
        {
            try
            {
                UserManager usermgr = new UserManager(null, null);
                var model = usermgr.GetUser(userid);
                return Json(model);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Add User
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost, Route("api/User")]
        public IHttpActionResult Post([FromBody]UserViewModel data)
        {
            var currentuser = Getuser();
            var currentfranchise = GetFranchise();
            UserManager usermgr = new UserManager(null, null);
            var status = "Invalid user data";
            if (data.PreferredTFN == "") data.PreferredTFN = " ";
            Guid userId = Guid.Empty;
            if (data != null)
            {
                var usrPrimaryEmail = usermgr.GetADDetails(data.UserName);
                if (usrPrimaryEmail == "")
                    return Json("Invalid HFC Username.  Please check the spelling of the username issued by HFC Tech Support and contact them if the problem persists.");

                var person = new Person
                {
                    FirstName = data.FirstName,
                    LastName = data.LastName,
                    CellPhone = data.CellPhone,
                    HomePhone = data.HomePhone,
                    PreferredTFN = data.PreferredTFN,
                    PrimaryEmail = usrPrimaryEmail,
                    TimezoneCode = Convert.ToInt32(data.TimezoneCode) != 0 ? data.TimezoneCode : TimeZoneEnum.UTC
                };

                Address address = new Address();
                if (currentuser.IsAdminInRole())
                    address = null;
                else
                {
                    address = new Address
                    {
                        ZipCode = data.Zipcode,
                        CountryCode2Digits = data.CountryCode2Digits
                    };
                }

                var user = new User()
                {
                    CreationDateUtc = DateTime.Now,
                    UserName = data.UserName,
                    Person = person,
                    Email = person.PrimaryEmail,
                    Address = address,
                    FranchiseId = currentfranchise?.FranchiseId,
                    Domain = "bbi.corp",
                    DomainPath = @"LDAP://bbi.corp/DC=bbi,DC=corp",
                    Password = string.Empty,
                    IsApproved = true,
                    CalSync = data.CalSync,
                    Comment = data.Comments,
                    CalendarFirstHour = data.Calendar1sthr,
                    SyncStartsOnUtc = DateTime.UtcNow,
                    Roles = data.RoleIds != null ? CacheManager.RoleCollection.Where(w => data.RoleIds.Contains(w.RoleId)).ToList() : new List<Role>(),
                    PermissionSets = data.PermissionSetIds != null ? data.PermissionSetIds.Select(int.Parse).ToList() : null
                };

                var mgr = new FranchiseManager(SessionManager.CurrentUser);
                int colorId = mgr.GetOrInsertColor(data.BGColor, data.FGColor);
                if (colorId > 0)
                {
                    user.ColorId = colorId;
                    if (!currentuser.IsAdminInRole() && user.Roles.All(a => a.RoleId != AppConfigManager.DefaultRole.RoleId))
                        user.Roles.Add(AppConfigManager.DefaultRole);
                    status = LocalMembership.CreateUser(user, true, out userId);
                }
                else
                    status = "Unable to retrieve color value";

            }
            var result = new { status, UserId = userId };
            return Json(result);

        }

        /// <summary>
        /// Update User
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut, Route("api/User")]
        public IHttpActionResult Put(Guid id, [FromBody]UserModel model)
        {
            var currentuser = Getuser();
            var currentfranchise = GetFranchise();
            UserManager usermgr = new UserManager(null, null);
            var status = "Invalid user data";

            if (id != Guid.Empty && model != null)
            {
                var usrPrimaryEmail = usermgr.GetADDetails(model.UserName);
                if (usrPrimaryEmail == "")
                    return Json("Invalid HFC Username.  Please check the spelling of the username issued by HFC Tech Support and contact them if the problem persists.");

                model.Person.TimezoneCode = Convert.ToInt32(model.Person.TimezoneCode) != 0 ? model.Person.TimezoneCode : TimeZoneEnum.UTC;

                model.IsADUser = true;
                model.Person.PrimaryEmail = usrPrimaryEmail;
                FranchiseManager mgr = new FranchiseManager(currentuser);

                //var oldUser = CacheManager.UserCollection.FirstOrDefault(u => u.UserId == id);
                var user = new User()
                {
                    UserId = id,
                    UserName = model.UserName,
                    PersonId = model.PersonId,
                    Email = usrPrimaryEmail,
                    Person = model.Person != null ? PersonViewModel.MapFrom(model.Person) : null,
                    Address = model.Address,
                    FranchiseId = SessionManager.CurrentFranchise?.FranchiseId,
                    CalSync = model.CalSync,
                    IsLockedOut = SessionManager.CurrentUser.UserId != id && model.IsLockedOut,
                    IsDisabled = model.IsDisabled,
                    CalendarFirstHour = model.CalendarFirstHour,
                    //OAuthUsers = model.OAuthAccounts,
                    Roles = model.RoleIds != null ? model.RoleIds.Select(s => new Role { RoleId = s }).ToList() : new List<Role>(),
                    Comment = model.Comment,
                    Domain = "bbi.corp",
                    DomainPath = @"LDAP://bbi.corp/DC=bbi,DC=corp",
                    Password = string.Empty,
                    PermissionSets = model.PermissionSetIds != null ? model.PermissionSetIds.Select(int.Parse).ToList() : null
                };


                var colorId = mgr.GetOrInsertColor(model.BGColor, model.FGColor);
                if (colorId > 0)
                {
                    user.ColorId = colorId;
                    if (!currentuser.IsAdminInRole() && user.Roles.All(a => a.RoleId != AppConfigManager.DefaultRole.RoleId))
                        user.Roles.Add(AppConfigManager.DefaultRole);
                    status = LocalMembership.UpdateUser(user, false, true, true);
                }
                else
                    status = "Unable to retrieve color value";


                //}
                //else
                //    status = FranchiseManager.Unauthorized;

                //}
            }

            return Json(status);
            //return ResponseResult(status);
        }

        /// <summary>
        /// Enable user
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        [HttpGet, Route("api/User/Enable/{userid}")]
        public IHttpActionResult Enable(Guid userid)
        {
            var status = "Invalid user id";
            if (userid != Guid.Empty)
            {
                var franchiseid = new FranchiseManager(SessionManager.CurrentUser);
                status = LocalMembership.ActivateUser(userid);

            }

            return ResponseResult(status);
        }

        /// <summary>
        /// Desable user
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        [HttpDelete, Route("api/User/{userid}")]
        public IHttpActionResult Delete(Guid userid)
        {
            var status = "Invalid user id";
            if (userid != Guid.Empty)
            {
                var mgr = new FranchiseManager(SessionManager.CurrentUser);
                //commented the line as using exsist permission code which is not needed
                //if (mgr.User_BasePermission.CanDelete)
                status = LocalMembership.DeleteUser(userid);
            }

            return ResponseResult(status);
        }

        /// <summary>
        /// Impersonate
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost, Route("api/User/Impersonate")]
        public IHttpActionResult Impersonate(Guid id)
        {
            var status = "Invalid user id";

            if (id != Guid.Empty)
            {
                // TODO : Verify whether this will not break anything - murugan 
                //if (SessionManager.SecurityUser.IsInRole(AppConfigManager.DefaultOperatorRole.RoleId, AppConfigManager.AppAdminRole.RoleId))
                if (SessionManager.SecurityUser.IsAdminInRole())
                {
                    CookieManager.ImpersonateUserId = id;
                    status = IMessageConstants.Success;
                }
                else
                    status = IMessageConstants.Unauthorized;
            }


            return ResponseResult(status);
        }

        /// <summary>
        /// Get Roles
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("api/User/GetRoles")]
        public IHttpActionResult GetRoles()
        {
            var listroles = CacheManager.RoleCollection;
            if (SessionManager.CurrentFranchise != null)
                listroles = listroles.Where(x => x.RoleType == 2).ToList();
            else
                listroles = listroles.Where(x => x.RoleType != 2).ToList();
            return Json(listroles);
        }
    }
}
