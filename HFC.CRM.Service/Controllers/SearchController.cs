﻿using HFC.CRM.Core.Common;
using HFC.CRM.DTO.Search;
using HFC.CRM.Managers;
using HFC.CRM.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [RoutePrefix("api/Search")]
    public class SearchController : BaseController
    {
        /// <summary>
        /// Get Lead List
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetSearchLeads")]
        public async Task<IHttpActionResult> GetSearchLeads()
        {
            try
            {
                SearchManager _searchMgr = new SearchManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var leads = await _searchMgr.GetSearchLeads(false);
                if (leads.Count() > 0)
                {
                    foreach (var item in leads)
                    {
                        if (item.iscommercial == 1)
                        {
                            if (item.CompanyName != null && item.CompanyName != "")
                                item.LeadFullName = item.CompanyName + " " + "/" + " " + item.FirstName + " " + item.LastName;
                            else
                                item.LeadFullName = item.FirstName + " " + item.LastName;
                        }
                        else if (item.CompanyName != null && item.CompanyName != "")
                            item.LeadFullName = item.FirstName + " " + item.LastName + " " + "/" + " " + item.CompanyName;
                        else
                            item.LeadFullName = item.FirstName + " " + item.LastName;
                        item.CreatedOnUtc = TimeZoneManager.ToLocal(item.CreatedOnUtc).Date;
                    }
                }

                var leadSeachResultModels = leads as LeadSeachResultNewModel[] ?? leads.ToArray();

                return Json(leadSeachResultModels);
            }
            catch (Exception ex)
            {
                return ResponseResult(ex.Message);
            }
        }

        /// <summary>
        /// Get SearchLeads for check
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetSearchLeads_forcheck")]
        public async Task<IHttpActionResult> GetSearchLeads_forcheck()
        {
            try
            {
                SearchManager _searchMgr = new SearchManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                //IEnumerable<LeadSeachResultNewModel> leads = Enumerable.Empty<LeadSeachResultNewModel>();
                var leads = await _searchMgr.GetSearchLeads(true);
                if (leads.Count() > 0)
                {
                    foreach (var item in leads)
                    {
                        if (item.iscommercial == 1)
                        {
                            if (item.CompanyName != null && item.CompanyName != "")
                                item.LeadFullName = item.CompanyName + " " + "/" + " " + item.FirstName + " " + item.LastName;
                            else
                                item.LeadFullName = item.FirstName + " " + item.LastName;
                        }
                        else if (item.CompanyName != null && item.CompanyName != "")
                            item.LeadFullName = item.FirstName + " " + item.LastName + " " + "/" + " " + item.CompanyName;
                        else
                            item.LeadFullName = item.FirstName + " " + item.LastName;
                        item.CreatedOnUtc = TimeZoneManager.ToLocal(item.CreatedOnUtc).Date;
                    }
                }

                var leadSeachResultModels = leads as LeadSeachResultNewModel[] ?? leads.ToArray();

                return Json(leadSeachResultModels);
            }
            catch (Exception ex)
            {
                return ResponseResult(ex.Message);
            }
        }

        /// <summary>
        /// Get Lead Matching Accounts
        /// </summary>
        /// <param name="id">leadid</param>
        /// <returns></returns>
        [HttpGet, Route("GetLeadMatchingAccounts/{id}")]
        public async Task<IHttpActionResult> GetLeadMatchingAccounts(int id)
        {
            try
            {
                SearchManager _searchMgr = new SearchManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                IEnumerable<AccountSeachResultModel> accounts = Enumerable.Empty<AccountSeachResultModel>();
                accounts = await _searchMgr.GetLeadMatchingAccounts(id);


                foreach (var item in accounts)
                {
                    if (item.CompanyName != null && item.CompanyName != "")
                    {
                        item.AccountFullName = item.FirstName + " " + item.LastName + " " + "/" + " " + item.CompanyName;
                    }
                    else
                    {
                        item.AccountFullName = item.FirstName + " " + item.LastName;
                    }
                }
                var accountSeachResultModels = accounts as AccountSeachResultModel[] ?? accounts.ToArray();

                return Json(accountSeachResultModels);
            }
            catch (Exception ex)
            {
                return ResponseResult(ex.Message);
            }
        }

        /// <summary>
        /// Get Account List
        /// </summary>
        /// <param name="id">includeInactive</param>
        /// if id=0 then return Inactive account
        /// if id=1 then return active account
        /// <returns></returns>
        [HttpGet, Route("GetSearchAccounts/{id}")]
        public async Task<IHttpActionResult> GetSearchAccounts(int id)
        {
            // When id = 0 we want only active records,
            // otherwise we want all the records including inactive and merged.
            var includeInactiveMerged = id == 0 ? false : true;
            try
            {
                SearchManager _searchMgr = new SearchManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var accounts = await _searchMgr.GetSearchAccounts(includeInactiveMerged);
                if (accounts.Count() > 0)
                {
                    foreach (var item in accounts)
                    {
                        if (item.IsCommercial == 1)
                        {
                            if (item.CompanyName != null && item.CompanyName != "")
                                item.AccountFullName = item.CompanyName + " / " + item.FirstName + " " + item.LastName;
                            else
                                item.AccountFullName = item.FirstName + " " + item.LastName;
                        }

                        else if (item.CompanyName != null && item.CompanyName != "")
                            item.AccountFullName = item.FirstName + " " + item.LastName + " " + "/" + " " + item.CompanyName;
                        else
                            item.AccountFullName = item.FirstName + " " + item.LastName;
                        item.CreatedOnUtc = TimeZoneManager.ToLocal(item.CreatedOnUtc).Date;
                    }
                }
                return Json(accounts.ToList());

                // var accountSeachResultModels = accounts as AccountSeachResultModel[] ?? accounts.ToArray();
                //var accountSeachResultModels1 = accountSeachResultModels.OrderByDescending(t => t.CreatedOnUtc).ToList();
                // return Json(accountSeachResultModels1);
            }

            catch (Exception ex)
            {
                return ResponseResult(ex.Message);
            }
        }

        /// <summary>
        /// Get Search Opportunitys
        /// </summary>
        /// <param name="Id"></param>
        /// If Id=0 then return all the Opportunity by FranchiseId
        /// If Id>0 (AccountId) then return all the Opportunity by AccountId
        /// <param name="filter"></param>
        /// <param name="opportunityId"></param>
        /// <returns></returns>
        [HttpGet, Route("GetSearchOpportunitys")]
        public async Task<IHttpActionResult> GetSearchOpportunitys(int Id, [FromUri] SearchFilter filter, int opportunityId = 0)
        {
            try
            {
                SearchManager _searchMgr = new SearchManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                PermissionManager permissionManager = new PermissionManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

                IEnumerable<OpportunitySeachResultModel> Opportunitys = Enumerable.Empty<OpportunitySeachResultModel>();

                Opportunitys = await _searchMgr.GetSearchOpportunitys(Id, opportunityId);

                // TP-1698: CLONE - Opportunities and related need record level permissions
                // Opportunity need a special permission to access all the
                // records. only certain roles has that permission. otherwise
                // If the logged in user is a Sales person or installation 
                // person, the can see their records alone.
                var GetPermission = permissionManager.GetUserRolesPermissions();
                var ModuleData = GetPermission.Find(x => x.ModuleCode == "Opportunity");
                var specialPermission = ModuleData.SpecialPermission.Find(x => x.PermissionCode == "OpportunityAllRecordAccess").CanAccess;
                if (specialPermission == false)
                {
                    var filtered = Opportunitys.Where(x => (x.SalesAgentId ==
                                       SessionManager.CurrentUser.PersonId ||
                                       x.InstallerId == SessionManager.CurrentUser.PersonId)
                                       ).ToList();

                    Opportunitys = filtered;
                }

                var opportunitySeachResultModelsSortById = Opportunitys.Where(o => o.AccountName != "").OrderByDescending(p => p.OpportunityID).ToList();

                // for move quote opportunity filter
                if (opportunityId != 0)
                    opportunitySeachResultModelsSortById = opportunitySeachResultModelsSortById.Where(o => o.OpportunityID != opportunityId && o.OpportunityStatus != "Ordering" && o.OpportunityStatus != "Order Accepted").ToList();

                return Json(opportunitySeachResultModelsSortById);

            }
            catch (Exception ex)
            {
                return ResponseResult(ex.Message);
            }
        }

        /// <summary>
        /// Get Order
        /// </summary>
        /// <param name="Id"></param>
        /// If Id=0 then return all the Order by FranchiseId
        /// If Id>0 (AccountId) then return all the Order by AccountId
        /// <param name="orderStatusListValue"></param>
        /// <param name="dateRangeValue"></param>
        /// <returns></returns>
        [HttpGet, Route("GetSearchOrders")]
        public async Task<IHttpActionResult> GetSearchOrders(int Id, string orderStatusListValue, string dateRangeValue)
        {
            try
            {
                SearchManager _searchMgr = new SearchManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                PermissionManager permissionManager = new PermissionManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

                IEnumerable<OrderSeachResultModel> orders = Enumerable.Empty<OrderSeachResultModel>();
                orders = await _searchMgr.GetSearchOrders(Id, orderStatusListValue, dateRangeValue);

                // TP-1698: CLONE - Opportunities and related need record level permissions
                // Opportunity need a special permission to access all the
                // records. only certain roles has that permission. otherwise
                // If the logged in user is a Sales person or installation 
                // person, the can see their records alone.
                var GetPermission = permissionManager.GetUserRolesPermissions();
                var ModuleData = GetPermission.Find(x => x.ModuleCode == "SalesOrder");
                var specialPermission = ModuleData.SpecialPermission.Find(x => x.PermissionCode == "SalesOrderAllRecordAccess").CanAccess;
                if (specialPermission == false)
                {
                    // Need to apply the filter for SalesAgent/Installation.
                    var filtered = orders.Where(x => (x.SalesAgentId ==
                                        SessionManager.CurrentUser.PersonId ||
                                        x.InstallerId == SessionManager.CurrentUser.PersonId)
                                        ).ToList();

                    orders = filtered;
                }


                var orderSeachResultModels = orders as OrderSeachResultModel[] ?? orders.ToArray();

                return Json(orderSeachResultModels);
            }
            catch (Exception ex)
            {
                return ResponseResult(ex.Message);
            }
        }

        /// <summary>
        /// Get Search Account Payments
        /// </summary>
        /// <param name="Id">AccountId</param>
        /// <returns></returns>
        [HttpGet, Route("GetSearchAccountPayments/{Id}")]
        public async Task<IHttpActionResult> GetSearchAccountPayments(int Id)
        {
            try
            {
                SearchManager _searchMgr = new SearchManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                IEnumerable<PaymentSeachResultModel> payments = Enumerable.Empty<PaymentSeachResultModel>();
                payments = await _searchMgr.GetSearchAccountPayments(Id);

                var orderSeachResultModels = payments as PaymentSeachResultModel[] ?? payments.ToArray();

                return Json(orderSeachResultModels);
            }
            catch (Exception ex)
            {
                return ResponseResult(ex.Message);
            }
        }
    }
}
