﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.DTO.Source;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    [RoutePrefix("api/MyCRMSourceMapping")]
    public class MyCRMSourceMappingController : BaseController
    {

        /// <summary>
        /// Get myCRM Source
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetmyCRMSource")]
        public IHttpActionResult GetmyCRMSource()
        {
            try
            {
                MyCRMSourceMappingManager MyCRMSourceMappingManager = new MyCRMSourceMappingManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                return Json(MyCRMSourceMappingManager.GetmyCRMSource());
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get TPT Source
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetTPTSource")]
        public IHttpActionResult GetTPTSource()
        {
            try
            {
                MyCRMSourceMappingManager MyCRMSourceMappingManager = new MyCRMSourceMappingManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                return Json(MyCRMSourceMappingManager.GetTPTSource());
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Save myCRM Sourcemapping
        /// </summary>
        /// <param name="MyCRMSourceMap"></param>
        /// <returns></returns>
        [HttpPost, Route("SavemyCRMSourcemapping")]
        public IHttpActionResult SavemyCRMSourcemapping(List<MyCRMSourceMap> MyCRMSourceMap)
        {
            try
            {
                MyCRMSourceMappingManager MyCRMSourceMappingManager = new MyCRMSourceMappingManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                MyCRMSourceMappingManager.SavemyCRMSourcemapping(MyCRMSourceMap);
                return Json("Success");
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Confirm MyCRM TPT SourceMap
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("ConfirmMyCRM_TPT_SourceMap")]
        public IHttpActionResult ConfirmMyCRM_TPT_SourceMap()
        {
            try
            {
                MyCRMSourceMappingManager MyCRMSourceMappingManager = new MyCRMSourceMappingManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                return Json(MyCRMSourceMappingManager.ConfirmMyCRM_TPT_SourceMap());
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get Confirm MyCRM TPT SourceMap
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetConfirmMyCRM_TPT_SourceMap")]
        public IHttpActionResult GetConfirmMyCRM_TPT_SourceMap()
        {
            try
            {
                MyCRMSourceMappingManager MyCRMSourceMappingManager = new MyCRMSourceMappingManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var res = MyCRMSourceMappingManager.GetConfirmMyCRM_TPT_SourceMap();
                dynamic MyDynamic = new ExpandoObject();
                MyDynamic.Status = res;
                return Json(MyDynamic);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

    }
}
