﻿using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Data.Constants;
using HFC.CRM.DTO;
using HFC.CRM.DTO.Calandar;
using HFC.CRM.Managers;
using HFC.CRM.Managers.AdvancedEmailManager;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Results;

namespace HFC.CRM.Service.Controllers
{
    /// <summary>
    /// Event API
    /// </summary>
    public class EventController : BaseController
    {
        #region Event List
        /// <summary>
        /// Return the List of Events based on Date
        /// </summary>
        /// <param name="date">Pass Date input</param>
        /// <remarks>
        /// ### Note ###
        /// Please use Date format as yyyy-MM-dd while checking in swagger        
        /// </remarks>
        /// <returns>
        /// List of event for the day
        /// </returns>
        //[HttpGet, Route("api/EventList/{date}")]
        //public IHttpActionResult EventList(DateTime date)
        //{
        //    try
        //    {
        //        var user = Getuser();
        //        TimeZoneEnum feTimeZone = GetFranchiseTimezone();

        //        EventLogger.LogEvent(user.UserName + " | " + date.ToString(), "EventController EventList call started", LogSeverity.Information);

        //        // Get Event data from DB
        //        CalendarManager cmgr = new CalendarManager(user);
        //        var EventListData = cmgr.GetEventListDataTPConnect(user.Person.PersonId, TimeZoneManager.GetUTCstartdatetime(date, feTimeZone), TimeZoneManager.GetUTCenddatetime(date, feTimeZone));

        //        List<EventListDataTPConnect> lstevent = new List<EventListDataTPConnect>();

        //        foreach (var eld in EventListData)
        //        {
        //            EventListDataTPConnect e = new EventListDataTPConnect();
        //            e.CalendarId = eld.CalendarId;
        //            e.StartDate = TimeZoneManager.ToLocal(eld.StartDate.DateTime, feTimeZone);
        //            e.EndDate = TimeZoneManager.ToLocal(eld.EndDate.DateTime, feTimeZone);
        //            e.Account = eld.AccountName;
        //            e.Location = eld.Location;
        //            e.Subject = eld.Subject;
        //            e.AppointmentType = eld.Type;
        //            lstevent.Add(e);
        //        }

        //        EventListTPConnect el = new EventListTPConnect();
        //        el.EventList = lstevent;

        //        EventLogger.LogEvent(JsonConvert.SerializeObject(el), "EventController EventList call ended", LogSeverity.Information);
        //        var responce = new Response(true, "", el);
        //        return Ok<Response>(responce);
        //    }
        //    catch (Exception ex)
        //    {
        //        EventLogger.LogEvent(ex);
        //        return ResponseResult(ex);
        //    }
        //}

        /// <summary>
        /// Return the List of Events based on Date range
        /// </summary>
        /// <remarks>
        /// ### Note ###
        /// Please use Date format as yyyy-MM-dd while checking in swagger        
        /// </remarks>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <returns></returns>
        [HttpGet, Route("api/EventList/{StartDate}/{EndDate}")]
        public IHttpActionResult EventList(DateTime StartDate, DateTime EndDate)
        {
            try
            {
                var user = Getuser();
                TimeZoneEnum feTimeZone = GetFranchiseTimezone();

                EventLogger.LogEvent(StartDate.ToString() + " | " + EndDate.ToString(), "EventController EventList call ended", LogSeverity.Information);

                // Get Event data from DB
                CalendarManager cmgr = new CalendarManager(user);
                var EventListData = cmgr.GetEventListDataTPConnect(user.Person.PersonId, TimeZoneManager.GetUTCstartdatetime(StartDate, feTimeZone), TimeZoneManager.GetUTCenddatetime(EndDate, feTimeZone));

                DateTime tmpStartDate = StartDate;
                while (tmpStartDate <= EndDate)
                {
                    var recdata = cmgr.GetRecurringEventsTPConnect(user.Person.PersonId, tmpStartDate, feTimeZone);
                    if (recdata != null && recdata.Count > 0)
                        EventListData.AddRange(recdata);
                    tmpStartDate = tmpStartDate.AddDays(1);
                }
                List<EventListDataTPConnect> lstevent = new List<EventListDataTPConnect>();

                foreach (var eld in EventListData)
                {
                    EventListDataTPConnect e = new EventListDataTPConnect();
                    e.CalendarId = eld.CalendarId;
                    e.StartDate = TimeZoneManager.ToLocal(eld.StartDate.DateTime, feTimeZone);
                    e.EndDate = TimeZoneManager.ToLocal(eld.EndDate.DateTime, feTimeZone);

                    dynamic objAcc = new ExpandoObject();
                    objAcc.AccountId = eld.AccountId;
                    objAcc.Name = eld.AccountName;
                    e.Account = objAcc;

                    //e.Account = eld.AccountName;
                    e.Location = eld.Location;
                    e.Subject = eld.Subject;

                    dynamic objapptype = new ExpandoObject();
                    objapptype.AppointmentTypeId = (int)eld.AptTypeEnum;
                    objapptype.Name = eld.Type;
                    e.AppointmentType = objapptype;
                    // e.AppointmentType = eld.Type;
                    e.IsAllDay = eld.IsAllDay;
                    lstevent.Add(e);
                }

                EventListTPConnect el = new EventListTPConnect();
                el.EventList = lstevent;

                EventLogger.LogEvent(JsonConvert.SerializeObject(el), "EventController EventList call ended", LogSeverity.Information);
                var responce = new Response(true, "", el);
                return Ok<Response>(responce);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        #endregion

        #region Add Event

        /// <summary>
        /// This Method Used to Add the Event
        /// </summary>
        /// /// <remarks>
        /// ### Note ###
        /// 1. Reminder value - Convert to Minutes like (hour and week) and Post the data       
        /// </remarks>
        /// <param name="Event"> </param>
        /// <returns></returns>
        [HttpPost, Route("api/AddEvent")]
        public IHttpActionResult AddEvent([FromBody]EventTPConnect Event)
        {
            try
            {
                // user info
                var user = Getuser();
                TimeZoneEnum feTimeZone = GetFranchiseTimezone();

                EventLogger.LogEvent(JsonConvert.SerializeObject(Event), "EventController AddEvent call started", LogSeverity.Information);

                if (Event.AllDay)
                {
                    Event.StartDate = Event.StartDate.Date;
                    Event.EndDate = Event.EndDate.Date.AddHours(23).AddMinutes(59);
                }

                // Map the posted data to calendar model
                var calendar = ToCalendar(Event, user);


                CalendarManager cmgr = new CalendarManager(user);
                ExchangeManager excmgr = new ExchangeManager();
                CommunicationManager Communication = new CommunicationManager(user, GetFranchise());

                int calId = 0; string warning;

                // Insert the event
                var status = cmgr.InsertEvent(out calId, calendar, out warning, null, feTimeZone);

                // Get Calendar info in Calendar Id exist
                CalendarVM calendardata = new CalendarVM();
                calendardata = cmgr.GetCalendar(calId, calendar.FranchiseId);

                excmgr.SyncChannel_Up(calId);

                // Sending email - appointment confirmation email
                //Sent email to account/lead/opportunity
                if (Event.AppointmentType == AppointmentTypeEnumTP.Installation)
                {
                    calendar.CalendarId = calId;

                    cmgr.SendEmail(calendar, EmailType.Installation); //, 5);

                    var smsdata = cmgr.GetTextingModal(calendar);
                    Communication.SendTextSms(smsdata, EmailType.Installation);  //, 5);

                }
                else if (Event.AppointmentType == AppointmentTypeEnumTP.Appointment)
                {
                    if ((calendar.AccountId != null && calendar.AccountId > 0) || (calendar.LeadId != null && calendar.LeadId > 0))
                    {
                        calendar.CalendarId = calId;
                        cmgr.SendEmail(calendar, EmailType.AppointmentConfirmation); //, 2);
                        var smsdata = cmgr.GetTextingModal(calendar);
                        Communication.SendTextSms(smsdata, EmailType.AppointmentConfirmation);  //, 2);
                    }
                }

                EventLogger.LogEvent("", "EventController AddEvent call ended", LogSeverity.Information);

                return Ok<Response>(new Response(true, "Event Added Successfully"));
            }
            catch (Exception ex)
            {
                return ResponseResult(ex);
            }
        }

        #endregion



        #region Update Event
        ///// <summary>
        ///// This Method used to get the Event details for Update Event based on CalendarId
        ///// </summary>
        ///// <param name="CalendarId"></param>
        ///// <returns></returns>

        //[HttpGet, Route("api/event/detail/{CalendarId}")]
        //public IHttpActionResult UpdateEvent(int CalendarId)
        //{
        //    try
        //    {
        //        UpdateEventTPConnect uec = new UpdateEventTPConnect();

        //        // User info
        //        var user = Getuser();
        //        TimeZoneEnum feTimeZone = GetFranchiseTimezone();

        //        CalendarManager cmgr = new CalendarManager(user);
        //        var EventDetails = cmgr.GetUpdateEventDataTPConnect(CalendarId);

        //        if (EventDetails != null)
        //        {
        //            EventTPConnect ed = new EventTPConnect();
        //            ed.CalendarId = EventDetails.CalendarId;
        //            ed.Subject = EventDetails.Subject;
        //            ed.Location = EventDetails.Location;
        //            ed.LeadId = EventDetails.LeadId;
        //            ed.AccountId = EventDetails.AccountId;
        //            ed.OpportunityId = EventDetails.OpportunityId;
        //            ed.OrderId = EventDetails.OrderId;
        //            ed.Message = EventDetails.Message;
        //            ed.AppointmentType = EventDetails.AptTypeEnum;
        //            ed.Attendees = EventDetails.Attendees.Split(',').Select(int.Parse).ToList();
        //            ed.StartDate = TimeZoneManager.ToLocal(EventDetails.StartDate.DateTime, feTimeZone);
        //            ed.EndDate = TimeZoneManager.ToLocal(EventDetails.EndDate.DateTime, feTimeZone);
        //            ed.Reminder = EventDetails.ReminderMinute;
        //            ed.IsRecurring = EventDetails.Recurring;

        //            if (ed.IsRecurring == true && EventDetails.RecurringEventId != null)
        //            {
        //                var RecurringDetails = cmgr.GetRecurrenceEventDataTPConnect((int)EventDetails.RecurringEventId);

        //                RecurringTPConnect rd = new RecurringTPConnect();
        //                rd.RecurringEventId = RecurringDetails.RecurringEventId;
        //                rd.StartDate = TimeZoneManager.ToLocal(RecurringDetails.StartDate.DateTime, feTimeZone);
        //                rd.EndDate = TimeZoneManager.ToLocal(RecurringDetails.EndDate.DateTime, feTimeZone);
        //                rd.EndsOn = TimeZoneManager.ToLocal(Convert.ToDateTime(RecurringDetails.EndsOn), feTimeZone);
        //                rd.EndsAfterXOccurrences = RecurringDetails.EndsAfterXOccurrences;
        //                rd.RecursEvery = RecurringDetails.RecursEvery;
        //                rd.DayOfWeekEnum = RecurringDetails.DayOfWeekEnum;
        //                rd.MonthEnum = RecurringDetails.MonthEnum;
        //                rd.DayOfMonth = RecurringDetails.DayOfMonth;
        //                rd.PatternEnum = RecurringDetails.PatternEnum;
        //                rd.DayOfWeekIndex = RecurringDetails.DayOfWeekIndex;

        //                ed.Recurring = rd;
        //            }
        //            uec.EditEventDetails = ed;
        //        }

        //        return Ok<Response>(new Response(true, "", uec));
        //    }
        //    catch (Exception ex)
        //    {
        //        return ResponseResult(ex);
        //    }
        //}

        /// <summary>
        /// This Method is used to Updated the Event based on CalendarId
        /// </summary>
        /// <param name="Event"></param>
        /// <returns></returns>
        [HttpPost, Route("api/UpdateEvent")]
        public IHttpActionResult UpdateEvent([FromBody]EventTPConnect Event)
        {
            try
            {
                var user = Getuser();
                TimeZoneEnum feTimeZone = GetFranchiseTimezone();

                EventLogger.LogEvent(JsonConvert.SerializeObject(Event), "EventController UpdateEvent call started", LogSeverity.Information);

                // Map the posted data to calendar model
                var calendar = ToCalendar(Event, user);

                CalendarManager cmgr = new CalendarManager(user);
                ExchangeManager excmgr = new ExchangeManager();

                int calId = 0; string warning;

                // Update the event
                var status = cmgr.InsertEvent(out calId, calendar, out warning, null, feTimeZone);

                excmgr.SyncChannel_Up(calendar.CalendarId);

                EventLogger.LogEvent("", "EventController UpdateEvent call ended", LogSeverity.Information);
                return Ok<Response>(new Response(true, "Event Updated Successfully"));
            }
            catch (Exception ex)
            {
                return ResponseResult(ex);
            }
        }

        #endregion

        #region View Event
        /// <summary>
        /// This Method used to get the Event details for Details page based on CalendarId
        /// </summary>
        /// <param name="CalendarId"></param>
        /// <returns></returns>
        [HttpGet, Route("api/ViewEventDetails/{CalendarId}")]
        public IHttpActionResult ViewEventDetails(int CalendarId)
        {
            try
            {
                var user = Getuser();
                TimeZoneEnum feTimeZone = GetFranchiseTimezone();

                EventLogger.LogEvent(CalendarId.ToString(), "EventController ViewEventDetails call started", LogSeverity.Information);

                // Get Event data from DB
                CalendarManager cmgr = new CalendarManager(user);
                var EventDetails = cmgr.GetViewEventDataTPConnect(CalendarId);

                if (EventDetails != null)
                {
                    EventDetailsTPConnect ed = new EventDetailsTPConnect();
                    ed.CalendarId = EventDetails.CalendarId;
                    ed.Subject = EventDetails.Subject;
                    ed.Location = EventDetails.Location;
                    // Todo
                    ed.CloseType = "Open";

                    if (EventDetails.LeadId != null)
                    {
                        dynamic objLead = new ExpandoObject();
                        objLead.LeadId = EventDetails.LeadId;
                        objLead.Name = EventDetails.Lead;
                        ed.Lead = objLead;
                    }

                    if (EventDetails.AccountId != null)
                    {
                        dynamic objAcc = new ExpandoObject();
                        objAcc.AccountId = EventDetails.AccountId;
                        objAcc.Name = EventDetails.Account;
                        ed.Account = objAcc;
                    }

                    if (EventDetails.OpportunityId != null)
                    {
                        dynamic objopp = new ExpandoObject();
                        objopp.OpportunityId = EventDetails.OpportunityId;
                        objopp.Name = EventDetails.Opportunity;
                        ed.Opportunity = objopp;
                    }

                    if (EventDetails.OrderId != null)
                    {
                        dynamic objorder = new ExpandoObject();
                        objorder.OrderId = EventDetails.OrderId;
                        objorder.Name = EventDetails.Order;
                        ed.Order = objorder;
                    }


                    var AttendeesId = EventDetails.AttendeesId.Split(',').Select(int.Parse).ToArray();
                    var Attendees = EventDetails.Attendees.Split(',');

                    var objAttendees = new List<dynamic>();

                    int i = 0;
                    foreach (var attid in AttendeesId)
                    {
                        dynamic objAtt = new ExpandoObject();
                        objAtt.AttendeesId = attid;
                        objAtt.Name = Attendees[i];
                        objAttendees.Add(objAtt);
                        i++;
                    }

                    ed.Attendees = objAttendees;

                    if (!string.IsNullOrEmpty(EventDetails.Type))
                    {
                        dynamic objapptype = new ExpandoObject();
                        objapptype.AppointmentTypeId = (int)EventDetails.AptTypeEnum;
                        objapptype.Name = EventDetails.Type;
                        ed.AppointmentType = objapptype;
                    }
                    //ed.Lead = EventDetails.Lead;
                    //ed.Account = EventDetails.Account;
                    //ed.Opportunity = EventDetails.Opportunity;
                    //ed.Order = EventDetails.Order;
                    //ed.AppointmentType = EventDetails.Type;
                    //ed.Attendees = EventDetails.Attendees;
                    ed.Message = EventDetails.Message;
                    ed.StartDate = TimeZoneManager.ToLocal(EventDetails.StartDate.DateTime, feTimeZone);
                    ed.EndDate = TimeZoneManager.ToLocal(EventDetails.EndDate.DateTime, feTimeZone);
                    ed.IsPrivate = EventDetails.IsPrivate;
                    ed.IsRecurring = EventDetails.Recurring;
                    if (EventDetails.CreatedOnUtc != null)
                        ed.CreatedByDate = TimeZoneManager.ToLocal((DateTime)EventDetails.CreatedOnUtc, feTimeZone);
                    if (EventDetails.LastUpdatedUtc != null)
                        ed.ModifiedByDate = TimeZoneManager.ToLocal((DateTime)EventDetails.LastUpdatedUtc, feTimeZone);

                    ed.CreatedBy = EventDetails.CreatedBy;
                    ed.ModifiedBy = EventDetails.LastUpdatedBy;

                    //ed.LeadId = EventDetails.LeadId;
                    //ed.AccountId = EventDetails.AccountId;
                    //ed.OpportunityId = EventDetails.OpportunityId;
                    //ed.OrderId = EventDetails.OrderId;


                    //ed.AppointmentTypeId = (int)EventDetails.AptTypeEnum;

                    //string Reminder = "";
                    //TimeSpan Reminderts = TimeSpan.FromMinutes(EventDetails.ReminderMinute);
                    //if (Reminderts.Days > 0)
                    //    Reminder = String.Format("{0} Day(s)", Reminderts.Days);
                    //else if (Reminderts.Hours > 0)
                    //    Reminder = String.Format("{0} Hour(s)", Reminderts.Hours);
                    //else
                    //    Reminder = String.Format("{0} Minutes", Reminderts.Minutes);

                    ed.Reminder = EventDetails.ReminderMinute;
                    ed.IsAllDay = EventDetails.IsAllDay;

                    if (ed.IsRecurring == true && EventDetails.RecurringEventId != null)
                    {
                        var RecurringDetails = cmgr.GetRecurrenceEventDataTPConnect((int)EventDetails.RecurringEventId);

                        RecurringTPConnect rd = new RecurringTPConnect();
                        rd.RecurringEventId = RecurringDetails.RecurringEventId;
                        rd.StartDate = TimeZoneManager.ToLocal(RecurringDetails.StartDate.DateTime, feTimeZone);
                        rd.EndDate = TimeZoneManager.ToLocal(RecurringDetails.EndDate.DateTime, feTimeZone);
                        rd.EndsOn = TimeZoneManager.ToLocal(RecurringDetails.EndsOn == null ? (DateTime?)null : RecurringDetails.EndsOn.Value.DateTime, feTimeZone);
                        rd.EndsAfterXOccurrences = RecurringDetails.EndsAfterXOccurrences;
                        rd.RecursEvery = RecurringDetails.RecursEvery;
                        rd.DayOfWeekEnum = RecurringDetails.DayOfWeekEnum;
                        rd.MonthEnum = RecurringDetails.MonthEnum;
                        rd.DayOfMonth = RecurringDetails.DayOfMonth;
                        rd.PatternEnum = RecurringDetails.PatternEnum;
                        rd.DayOfWeekIndex = RecurringDetails.DayOfWeekIndex;

                        ed.RecurringDetails = rd;
                    }

                    EventDetailTPConnect edtp = new EventDetailTPConnect();
                    edtp.EventDetails = ed;

                    EventLogger.LogEvent(JsonConvert.SerializeObject(edtp), "EventController ViewEventDetails call ended", LogSeverity.Information);
                    return Ok<Response>(new Response(true, "", edtp));
                }
                else
                    return Ok<Response>(new Response(false, "Invalid CalendarId"));

            }
            catch (Exception ex)
            {
                return ResponseResult(ex);
            }
        }

        #endregion

        #region Delete Event
        /// <summary>
        /// Delete the event
        /// </summary>
        /// <param name="event_id"></param>
        /// <returns></returns>
        [HttpDelete, Route("api/event/{event_id}/delete")]
        public IHttpActionResult DeleteEvent(int event_id)
        {
            var user = Getuser();
            EventLogger.LogEvent(event_id.ToString(), "EventController DeleteEvent call started", LogSeverity.Information);
            CalendarManager cmgr = new CalendarManager(user);
            var status = "";
            if (event_id > 0)
            {
                status = cmgr.DeleteEvent(event_id, DateTime.Now);
            }
            EventLogger.LogEvent(event_id.ToString(), "EventController DeleteEvent call ended", LogSeverity.Information);
            return Ok<Response>(new Response(true, "Event Deleted Successfully"));

        }
        #endregion

        #region Cancel Event
        /// <summary>
        /// Cancel the event
        /// </summary>
        /// <param name="event_id"></param>
        /// <returns></returns>
        [HttpPost, Route("api/event/{event_id}/cancel")]
        public IHttpActionResult CancelEvent(int event_id)
        {
            var user = Getuser();
            CalendarManager cmgr = new CalendarManager(user);
            var status = "";
            if (event_id > 0)
            {
                status = cmgr.CancelEvent(event_id, DateTime.Now);
            }
            return Ok<Response>(new Response(true, "Event Cancelled Successfully"));
        }
        #endregion

        #region LookUp's
        ///// <summary>
        ///// Get Lead LookUp Data
        ///// </summary>
        ///// <returns></returns>
        //[HttpGet, Route("api/EventLeadLookUp")]
        //public IHttpActionResult EventLeadLookUp()
        //{
        //    var user = Getuser();

        //    CalendarManager cmgr = new CalendarManager(user);
        //    var EventLeadLookUp = cmgr.GetEventLeadLookUp((int)user.FranchiseId);

        //    return Ok<Response>(new Response(true, "", EventLeadLookUp));
        //}

        ///// <summary>
        ///// Get LookUp Data for Account, Opportunity, Order
        ///// </summary>
        ///// <param name="filter"></param>
        ///// <returns></returns>
        //[HttpPost, Route("api/EventLookUp")]
        //public IHttpActionResult EventLookUp([FromBody]LookUpFilter filter)
        //{
        //    var user = Getuser();

        //    CalendarManager cmgr = new CalendarManager(user);
        //    var EventLookUp = cmgr.GetEventLookUp((int)user.FranchiseId, filter);

        //    return Ok<Response>(new Response(true, "", EventLookUp));
        //}

        /// <summary>
        /// Get leads, accounts, opportunities, orders, types, attendees Lookup
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("api/lookups")]
        public IHttpActionResult EventLookUpAll()
        {
            try
            {
                var user = Getuser();

                CalendarManager cmgr = new CalendarManager(user);
                var EventLookUp = cmgr.GetEventLookUp((int)user.FranchiseId);

                return Ok<Response>(new Response(true, "", EventLookUp));
            }
            catch (Exception ex)
            {
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get Attendees LookUp Data
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("api/EventAttendeesLookUp")]
        public IHttpActionResult EventAttendeesLookUp()
        {
            try
            {
                var user = Getuser();

                CalendarManager cmgr = new CalendarManager(user);
                var EventAttendeesLookUp = cmgr.GetEventAttendeesLookUp((int)user.FranchiseId);

                return Ok<Response>(new Response(true, "", EventAttendeesLookUp));
            }
            catch (Exception ex)
            {
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get Appointment Type LookUp Data
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("api/EventAppointmentTypeLookUp")]
        public IHttpActionResult EventAppointmentTypeLookUp()
        {
            try
            {
                var user = Getuser();

                CalendarManager cmgr = new CalendarManager(user);
                var EventAppointmentTypeLookUp = cmgr.GetEventAppointmentTypeLookUp();

                return Ok<Response>(new Response(true, "", EventAppointmentTypeLookUp));
            }
            catch (Exception ex)
            {
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get Lead LookUp Data by key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        [HttpGet, Route("api/leads")]
        public IHttpActionResult GetEventLeadLookUp(string key)
        {
            try
            {
                var user = Getuser();

                CalendarManager cmgr = new CalendarManager(user);
                var EventLookUp = cmgr.GetEventLeadLookUp((int)user.FranchiseId, key);

                dynamic o = new ExpandoObject();
                o.Lead = EventLookUp;

                return Ok<Response>(new Response(true, "", o));
            }
            catch (Exception ex)
            {
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get Account LookUp Data by key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        [HttpGet, Route("api/accounts")]
        public IHttpActionResult GetEventAccountLookUp(string key)
        {
            try
            {
                var user = Getuser();

                CalendarManager cmgr = new CalendarManager(user);
                var EventLookUp = cmgr.GetEventAccountLookUp((int)user.FranchiseId, key);

                dynamic o = new ExpandoObject();
                o.Account = EventLookUp;

                return Ok<Response>(new Response(true, "", o));
            }
            catch (Exception ex)
            {
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get Opportunity LookUp Data by key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        [HttpGet, Route("api/opportunities")]
        public IHttpActionResult GetEventOpportunityLookUp(string key)
        {
            try
            {
                var user = Getuser();

                CalendarManager cmgr = new CalendarManager(user);
                var EventLookUp = cmgr.GetEventOpportunitiesLookUp((int)user.FranchiseId, key);

                dynamic o = new ExpandoObject();
                o.Opportunity = EventLookUp;

                return Ok<Response>(new Response(true, "", o));
            }
            catch (Exception ex)
            {
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get Order LookUp Data by key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        [HttpGet, Route("api/orders")]
        public IHttpActionResult GetEventOrderLookUp(string key)
        {
            try
            {
                var user = Getuser();

                CalendarManager cmgr = new CalendarManager(user);
                var EventLookUp = cmgr.GetEventOrderLookUp((int)user.FranchiseId, key);

                dynamic o = new ExpandoObject();
                o.Order = EventLookUp;

                return Ok<Response>(new Response(true, "", o));
            }
            catch (Exception ex)
            {
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get Event LookUp by AccountId
        /// </summary>
        /// <param name="account_id"></param>
        /// <returns></returns>
        [HttpGet, Route("api/accounts/{account_id}")]
        public IHttpActionResult GetEventLookUpbyAccountId(int account_id)
        {
            try
            {
                var user = Getuser();

                CalendarManager cmgr = new CalendarManager(user);
                var EventLookUp = cmgr.GetEventLookUpbyAccountId((int)user.FranchiseId, account_id);

                return Ok<Response>(new Response(true, "", EventLookUp));
            }
            catch (Exception ex)
            {
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get Event LookUp by OpportunityId
        /// </summary>
        /// <param name="opportunity_id"></param>
        /// <returns></returns>
        [HttpGet, Route("api/opportunities/{opportunity_id}")]
        public IHttpActionResult GetEventLookUpbyOpportunityId(int opportunity_id)
        {
            try
            {
                var user = Getuser();

                CalendarManager cmgr = new CalendarManager(user);
                var EventLookUp = cmgr.GetEventLookUpbyOpportunityId((int)user.FranchiseId, opportunity_id);

                return Ok<Response>(new Response(true, "", EventLookUp));
            }
            catch (Exception ex)
            {
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get Event LookUp by OrderId
        /// </summary>
        /// <param name="order_id"></param>
        /// <returns></returns>
        [HttpGet, Route("api/order/{order_id}")]
        public IHttpActionResult GetEventLookUpbyOrderId(int order_id)
        {
            try
            {
                var user = Getuser();

                CalendarManager cmgr = new CalendarManager(user);
                var EventLookUp = cmgr.GetEventLookUpbyOrderId((int)user.FranchiseId, order_id);

                return Ok<Response>(new Response(true, "", EventLookUp));
            }
            catch (Exception ex)
            {
                return ResponseResult(ex);
            }
        }

        #endregion

        #region Map Calendar model
        /// <summary>
        /// Map EventTPConnect to Calendar Model
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        private CalendarVM ToCalendar(EventTPConnect model, User user)
        {
            int apptypeid = (int)model.AppointmentType;
            // map calendar info
            var calendar = new CalendarVM
            {
                CalendarId = model.CalendarId,
                Subject = model.Subject,
                Location = model.Location,
                LeadId = model.LeadId,
                AccountId = model.AccountId,
                OpportunityId = model.OpportunityId,
                OrderId = model.OrderId,
                Message = model.Message,
                AptTypeEnum = (AppointmentTypeEnumTP)apptypeid, //model.AppointmentType,
                StartDate = model.StartDate,
                EndDate = model.EndDate,
                IsAllDay = model.AllDay,
                AddRecurrence = (bool)model.IsRecurring,
                ReminderMinute = (short)model.Reminder,

                OrganizerPersonId = user.Person.PersonId,
                AssignedPersonId = model.Attendees[0],
                //RemindMethodEnum = model.ReminderMethods,             
                FranchiseId = (int)user.FranchiseId,
                //IsCancelled = model.IsCancelled,
                RecurringEventId = model.RecurringEventId,
                //RevisionSequence = model.RevisionSequence,
                CreatedOnUtc = DateTime.UtcNow,
                CreatedByPersonId = user.Person.PersonId,
                // Todo
                EventTypeEnum = EventTypeEnum.Single,
                IsPrivate = model.IsPrivate
            };

            // map recurring
            if (model.IsRecurring == true && model.Recurring != null)
            {
                EventRecurring er = new EventRecurring();

                er.RecurringEventId = model.Recurring.RecurringEventId;
                if (DateTime.MinValue != model.Recurring.StartDate)
                    er.StartDate = model.Recurring.StartDate;
                if (DateTime.MinValue != model.Recurring.EndDate)
                    er.EndDate = model.Recurring.EndDate;
                if (DateTime.MinValue != model.Recurring.EndsOn)
                    er.EndsOn = model.Recurring.EndsOn;
                er.EndsAfterXOccurrences = model.Recurring.EndsAfterXOccurrences;
                er.RecursEvery = model.Recurring.RecursEvery;
                er.DayOfWeekEnum = model.Recurring.DayOfWeekEnum;
                er.MonthEnum = model.Recurring.MonthEnum;
                if (model.Recurring.DayOfMonth != null)
                    er.DayOfMonth = (short)model.Recurring.DayOfMonth;
                er.PatternEnum = model.Recurring.PatternEnum;
                er.DayOfWeekIndex = model.Recurring.DayOfWeekIndex;
                er.CreatedOnUtc = DateTime.UtcNow;
                er.CreatedByPersonId = user.Person.PersonId;

                calendar.EventRecurring = er;
            }

            // Attendees info
            CalendarManager cmgr = new CalendarManager(user);
            foreach (var person in model.Attendees)
            {
                var per = cmgr.Getpersoninfo(person);
                calendar.Attendees.Add(new EventToPerson
                {
                    PersonId = per.PersonId,
                    PersonEmail = per.PrimaryEmail,
                    PersonName = per.FirstName + " " + per.LastName
                });
            }
            return calendar;
        }
        #endregion

        #region Time Entries
        /// <summary>
        /// Get Time Entires
        /// </summary>
        /// <param name="CalendarId"></param>
        /// <returns></returns>
        [HttpGet, Route("api/GetTimeEntires/{CalendarId}")]
        public IHttpActionResult GetTimeEntires(int CalendarId)
        {
            try
            {
                var user = Getuser();
                TimeZoneEnum feTimeZone = GetFranchiseTimezone();

                EventLogger.LogEvent(user.UserName + " | " + CalendarId.ToString(), "EventController GetTimeEntires call started", LogSeverity.Information);

                // Get Event data from DB
                CalendarManager cmgr = new CalendarManager(user);
                var res = cmgr.GetEventTimeEntries(CalendarId);

                foreach (var r in res)
                {
                    r.StartDate = r.StartDate != null ? TimeZoneManager.ToLocal(r.StartDate, feTimeZone) : (DateTime?)null;
                    r.EndDate = r.StartDate != null ? TimeZoneManager.ToLocal(r.EndDate, feTimeZone) : (DateTime?)null;
                }

                EventLogger.LogEvent(JsonConvert.SerializeObject(res), "EventController EventList call ended", LogSeverity.Information);

                dynamic el = new ExpandoObject();
                el.TimeEntires = res;
                var responce = new Response(true, "", el);
                return Ok<Response>(responce);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Update Time Entries
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, Route("api/UpdateTimeEntires")]
        public IHttpActionResult UpdateTimeEntires(CalendarTimeEntries model)
        {
            try
            {
                var user = Getuser();
                TimeZoneEnum feTimeZone = GetFranchiseTimezone();

                EventLogger.LogEvent(JsonConvert.SerializeObject(model), "EventController UpdateTimeEntires call started", LogSeverity.Information);

                model.StartDate = model.StartDate != null ? TimeZoneManager.ToUTC(model.StartDate, feTimeZone) : (DateTime?)null;
                model.EndDate = model.StartDate != null ? TimeZoneManager.ToUTC(model.EndDate, feTimeZone) : (DateTime?)null;

                // Get Event data from DB
                CalendarManager cmgr = new CalendarManager(user);
                var res = cmgr.UpdateEventTimeEntries(model);

                EventLogger.LogEvent(JsonConvert.SerializeObject(model), "EventController UpdateTimeEntires call ended", LogSeverity.Information);
                var responce = new Response(true, "");
                return Ok<Response>(responce);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }


        #endregion

        #region Get Subject and Location
        [HttpGet, Route("api/GetSubjectLocation")]
        public IHttpActionResult GetSubjectLocation(int id, string type)
        {
            try
            {
                var user = Getuser();

                CalendarManager cmgr = new CalendarManager(user);
                var EventLookUp = cmgr.GetSubjectLocation(id, type);

                return Ok<Response>(new Response(true, "", EventLookUp));
            }
            catch (Exception ex)
            {
                return ResponseResult(ex);
            }
        }
        #endregion
    }
}
