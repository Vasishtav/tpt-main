﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.DTO.Order;
using HFC.CRM.Managers;
using HFC.CRM.Serializer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    [RoutePrefix("api/OrdersTP")]
    public class OrdersController : BaseController
    {
        /// <summary>
        /// Get Order Payment Methods
        /// </summary>
        /// If Id=0 then return all the Order Payment Methods
        /// If Id>0 then return particular Order Payment Method
        /// <param name="id">Id</param>
        /// <returns></returns>
        [HttpGet, Route("OrderPaymentMethods")]
        public IHttpActionResult OrderPaymentMethods(int id)
        {
            try
            {
                OrderManager ordermgr = new OrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = ordermgr.OrderPaymentMethods(id);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Order Reversal Reason
        /// </summary>
        /// If Id=0 then return all the Order Reversal Reason
        /// If Id>0 then return particular Order Reversal Reason
        /// <param name="id">Id</param>
        /// <returns></returns>
        [HttpGet, Route("OrderReversalReason")]
        public IHttpActionResult OrderReversalReason(int id)
        {
            try
            {
                OrderManager ordermgr = new OrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = ordermgr.OrderReversal(id);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get the Order Payments
        /// </summary>
        /// <param name="id">Payment Id</param>
        /// <param name="orderId">order Id</param>
        /// <returns></returns>
        [HttpGet, Route("OrderPaymentsByPaymentId")]
        public IHttpActionResult OrderPaymentsByPaymentId(int id, int orderId)
        {
            try
            {
                OrderManager ordermgr = new OrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = ordermgr.OrderPaymentsByPaymentId(id, orderId);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Add Payment
        /// </summary>
        /// <param name="model">model</param>
        /// <returns></returns>
        [HttpPut, Route("AddPayment")]
        public IHttpActionResult AddPayment(PaymentDTO model)
        {
            try
            {
                OrderManager ordermgr = new OrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = ordermgr.UpdateReversePayment(model);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get the Order By Opportunity
        /// </summary>
        /// <param name="id">Opportunity id</param>
        /// <returns></returns>
        [HttpGet, Route("GetOrderByOpportunity/{id}")]
        public IHttpActionResult GetOrderByOpportunity(int id)
        {
            try
            {
                OrderManager ordermgr = new OrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = ordermgr.GetOrderByOpportunityId(id);
                return Json(new { error = "", data = result });
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get the Phone and Email of an Order
        /// </summary>
        /// <param name="id">Order ID</param>
        /// <returns></returns>
        [HttpGet, Route("getPhoneEmailOrder /{id}")]
        public IHttpActionResult getPhoneEmailOrder(int id)
        {
            try
            {
                OrderManager ordermgr = new OrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = ordermgr.getPhoneEmailOrder(id);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// List of Order Status
        /// </summary>
        /// If id=0 then it will return all order status
        /// If id>0 then it will return particular order status
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Route("OrderStatus/{id}")]
        public IHttpActionResult OrderStatus(int id)
        {
            try
            {
                OrderManager ordermgr = new OrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = ordermgr.GetOrderStatues(id);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get the order details
        /// </summary>
        /// <param name="id">order id</param>
        /// <returns></returns>
        [HttpGet, Route("Get/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                OrderManager ordermgr = new OrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                var result = ordermgr.GetOrder(id, FranchiseId);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Update Contracted Date
        /// </summary>
        /// <param name="id">Order id</param>
        /// <param name="date">date</param>
        /// <param name="quotekey">quotekey</param>
        /// <returns></returns>
        [HttpPost, Route("updateContractedDate")]
        public IHttpActionResult updateContractedDate(int id, DateTime date, int quotekey)
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                OrderManager ordermgr = new OrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var quotesdetail = QuotesMgr.GetDetails(quotekey);
                if (quotesdetail.PreviousSaleDate == null)
                {
                    var savesale = QuotesMgr.SaveSale(quotekey, date);
                }
                var result = ordermgr.updateContractedDate(id, date);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Convert Order To MPO
        /// </summary>
        /// <param name="id">Order Id</param>
        /// <returns></returns>
        [HttpGet, Route("ConvertToMPO/{id}")]
        public IHttpActionResult ConvertToMPO(int id)
        {
            try
            {
                PurchaseOrderManager PurchaseOrderMgr = new PurchaseOrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                OrderManager ordermgr = new OrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var order = ordermgr.GetOrderByOrderId(id);
                var validQuote = QuotesMgr.ValidateQuote(order.QuoteKey, true);
                if (!string.IsNullOrEmpty(validQuote))
                {
                    return Json(new { data = "", error = validQuote });
                }
                if (!PurchaseOrderMgr.CheckIfMPOExists(id))
                {
                    var result = PurchaseOrderMgr.ConvertOrderToMasterPurchaseOrder(id);
                    return Json(new { data = result, error = "" });
                }
                else
                {
                    return Json(new { data = "", error = "A MPO already exists." });
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return Json(new { data = "", error = "The system is currently busy, please try again later." });
            }
        }

        /// <summary>
        /// Order Payments
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Route("OrderPayments/{id}")]
        public IHttpActionResult OrderPayments(int id)
        {
            try
            {
                OrderManager ordermgr = new OrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = ordermgr.OrderPayments(id);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Post the Order
        /// </summary>
        /// <param name="model">model</param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult Post([FromBody]OrderDTO model)
        {
            try
            {
                OrderManager ordermgr = new OrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                OpportunitiesManager oppMgr = new OpportunitiesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                CommunicationManager Communication = new CommunicationManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                
                var order = ordermgr.GetOrder(model.OrderID, SessionManager.CurrentFranchise.FranchiseId);
                var result = ordermgr.OrderUpdate(model);

                if (model.OrderStatus == 5)
                {
                    if (order.SendReviewEmail == true)
                    {
                        return Json(result);
                    }
                    else
                    {
                        var InvoiceHistory = ordermgr.GetInvoiceHistory(model.OrderID);
                        if (model.SendReviewEmail == true && model.IsNotifyemails == true)
                        {
                            if (InvoiceHistory != null)
                                SendEmail(model.OrderID, EmailType.ThankyouReview, model.SendReviewEmail, InvoiceHistory.Streamid.ToString());
                            else
                                SendEmail(model.OrderID, EmailType.ThankyouReview, model.SendReviewEmail, "");
                        }

                        //text sms
                        if (model.SendReviewEmail == true && model.IsNotifyText == true)
                            Communication.SendOrderTextSms(model.OrderID, EmailType.ThankyouReview);
                    }


                }
                else if (model.OrderStatus == 6)
                {
                    QuotesMgr.UpdateQuotesStatus(model.QuoteKey, 4);
                    oppMgr.UpdateOpportunityStatus(model.OpportunityId, 3);
                }
                else if (model.OrderStatus == 9)
                {
                    QuotesMgr.UpdateQuotesStatus(model.QuoteKey, 2);
                    oppMgr.UpdateOpportunityStatus(model.OpportunityId, 3);
                }

                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Update Order Status to Open
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("UpdateOrderStatustoOpen")]
        public IHttpActionResult UpdateOrderStatustoOpen()
        {
            try
            {
                OrderManager ordermgr = new OrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                ordermgr.UpdateOrderStatustoOpen();
                return Json("success");
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Set Installation Appointment
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut, Route("SetInstallationAppointment")]
        public IHttpActionResult SetInstallationAppointment(OrderDTO model)
        {
            try
            {
                OrderManager ordermgr = new OrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = ordermgr.UpdateOrderStatus(model.OrderID);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Create the Invoice
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut, Route("CreateInvoice")]
        public IHttpActionResult CreateInvoice(Orders model)
        {
            try
            {
                OrderManager ordermgr = new OrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                InvoiceSheetReport pdf = new InvoiceSheetReport();
                int invoiceId = ordermgr.CreateInvoice(model);
                Guid filepdf = pdf.createInvoicePDF(invoiceId);
                var result = ordermgr.CreateInvoice(invoiceId, filepdf);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Send the Invoice Email
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut, Route("SendInvoiceEmail")]
        public IHttpActionResult SendInvoiceEmail(EmailUrlParam model)
        {
            try
            {
                var result = EmailManager.CreateMessageWithAttachment(0, model);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Master PO List
        /// </summary>
        /// <param name="id">opprtunityId</param>
        /// <returns></returns>
        [HttpGet, Route("GetMasterPOList/{id}")]
        public IHttpActionResult GetMasterPOList(int id)
        {
            try
            {
                PurchaseOrderManager PurchaseOrderMgr = new PurchaseOrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var res = PurchaseOrderMgr.GetMPOList();

                // TP-1698: CLONE - Opportunities and related need record level permissions
                // Opportunity need a special permission to access all the
                // records. only certain roles has that permission. otherwise
                // If the logged in user is a Sales person or installation
                // person, the can see their records alone.
                if (!this.CanAccess("Opportunity All Record Access"))
                {
                    // Need to apply the filter for SalesAgent/Installation.
                    var filtered = res.Where(x => (x.SalesAgentId ==
                                        SessionManager.CurrentUser.PersonId ||
                                        x.InstallerId == SessionManager.CurrentUser.PersonId)
                                        ).ToList();
                    res = filtered;
                }
                OrderManager ordermgr = new OrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = ordermgr.GetOrderByOpportunityId(id);
                return Json(new { error = "", data = result });
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }

        /// <summary>
        /// Send Thank You Review Email
        /// </summary>
        /// <param name="id">OrderId</param>
        /// <returns></returns>
        [HttpGet, Route("SendThankYouReviewEmail/{id}")]
        public IHttpActionResult SendThankYouReviewEmail(int id)
        {
            try 
            {
                OrderManager ordermgr = new OrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var order = ordermgr.GetOrder(id, SessionManager.CurrentFranchise.FranchiseId);
                if (order.OrderStatus == 5)
                {
                    if (order.SendReviewEmail == false)
                    {
                        var InvoiceHistory = ordermgr.GetInvoiceHistory(order.OrderID);
                        var temp = InvoiceHistory != null ? InvoiceHistory.Streamid.ToString() : "";
                        SendEmail(order.OrderID, EmailType.ThankyouReview, true, temp);
                    }
                }
                var result = ordermgr.UpdateOrderValue(id);
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }

        private string SendEmail(int OrderId, EmailType emailType, bool typestatus, string Streamid = "")
        {
            try
            {
                OrderManager ordermgr = new OrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var SendOrderMail = ordermgr.SendOrdermail(OrderId, emailType, Streamid, typestatus, "");
                return SendOrderMail;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return "Failure";
            }
        }
    }
}
