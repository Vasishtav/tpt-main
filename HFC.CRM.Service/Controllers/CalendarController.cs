﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Data.Constants;
using HFC.CRM.DTO.Calandar;
using HFC.CRM.Managers;
using HFC.CRM.Managers.AdvancedEmailManager;
using HFC.CRM.Service.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    [RoutePrefix("api/Calendar")]
    public class CalendarController : BaseController
    {
        #region Dasboard section
        /// <summary>
        /// Get All Events
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetAllEvents")]
        public IHttpActionResult GetAllEvents()
        {
            try
            {
                var manager = new CalendarManager(SessionManager.CurrentUser);
                var result = manager.GetAllEvents();
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get My Events
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetMyEvents")]
        public IHttpActionResult GetMyEvents()
        {
            try
            {
                var manager = new CalendarManager(SessionManager.CurrentUser);
                var result = manager.GetMyEvents();
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get All Tasks
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetAllTasks")]
        public IHttpActionResult GetAllTasks()
        {
            try
            {
                var manager = new CalendarManager(SessionManager.CurrentUser);
                var result = manager.GetAllTasks();
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get My Tasks
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetMyTasks")]
        public IHttpActionResult GetMyTasks()
        {
            try
            {
                var manager = new CalendarManager(SessionManager.CurrentUser);
                var result = manager.GetMyTasks();
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        #endregion

        #region Appointment Grid Event

        /// <summary>
        /// Get Lead Events
        /// </summary>
        /// <param name="id">LeadId</param>
        /// <returns></returns>
        [HttpGet, Route("GetLeadEvents/{id}")]
        public IHttpActionResult GetLeadEvents(int id)
        {
            try
            {
                var manager = new CalendarManager(SessionManager.CurrentUser);
                var result = manager.GetEventsForLead(id);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Account Events
        /// </summary>
        /// <param name="id">AccountId</param>
        /// <returns></returns>
        [HttpGet, Route("GetAccountEvents/{id}")]
        public IHttpActionResult GetAccountEvents(int id)
        {
            try
            {
                var manager = new CalendarManager(SessionManager.CurrentUser);
                var result = manager.GetEventsForAccount(id);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Opportunity Events
        /// </summary>
        /// <param name="id">OpportunityId</param>
        /// <returns></returns>
        [HttpGet, Route("GetOpportunityEvents/{id}")]
        public IHttpActionResult GetOpportunityEvents(int id)
        {
            try
            {
                var manager = new CalendarManager(SessionManager.CurrentUser);
                var result = manager.GetEventsForOpportunity(id);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
        #endregion

        #region Appointment Grid Task
        /// <summary>
        /// Get Lead Tasks
        /// </summary>
        /// <param name="id">LeadId</param>
        /// <returns></returns>
        [HttpGet, Route("GetLeadTasks/{id}")]
        public IHttpActionResult GetLeadTasks(int id)
        {
            try
            {
                var manager = new CalendarManager(SessionManager.CurrentUser);
                var result = manager.GetTasksForLead(id);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Account Tasks
        /// </summary>
        /// <param name="id">AccountId</param>
        /// <returns></returns>
        [HttpGet, Route("GetAccountTasks/{id}")]
        public IHttpActionResult GetAccountTasks(int id)
        {
            try
            {
                var manager = new CalendarManager(SessionManager.CurrentUser);
                var result = manager.GetTasksForAccount(id);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Opportunity Tasks
        /// </summary>
        /// <param name="id">OpportunityId</param>
        /// <returns></returns>
        [HttpGet, Route("GetOpportunityTasks/{id}")]
        public IHttpActionResult GetOpportunityTasks(int id)
        {
            try
            {
                var manager = new CalendarManager(SessionManager.CurrentUser);
                var result = manager.GetTasksForOpportunity(id);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
        #endregion

        /// <summary>
        /// Get Calendar Data based on date range
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet, Route("GetCalendarDetails")]
        public IHttpActionResult Get([FromUri]CalendarFilter filter)
        {

            if (filter == null)
                return ResponseResult("Invalid parameters");


            List<CalendarModel> events = null, recurMasters = null;
            List<TaskModel> tasks = null;

            var status = filter.GetCalendar(out events, out recurMasters);

            filter.GetTasks(out tasks);

            var appointmentDic = new Dictionary<string, byte>();
            foreach (var type in SessionManager.CurrentFranchise.AppointmentTypesCollection())
            {
                appointmentDic.Add(type.Name, Convert.ToByte(type.AppointmentTypeId));
            }

            if (SessionManager.CurrentFranchise.AppTypeColor == true)
            {
                var appColorMgr = new AppointmentTypeColorManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

                var appColors = appColorMgr.GetColors();
                foreach (var evnt in events)
                {
                    evnt.EnableBorders = appColors.EnableBorders;
                    switch ((int)evnt.AptTypeEnum)
                    {
                        case 1:
                            evnt.AppointmentTypeColor = appColors.Appointment1;
                            break;
                        case 2:
                            evnt.AppointmentTypeColor = appColors.Appointment2;
                            break;
                        case 3:
                            evnt.AppointmentTypeColor = appColors.Appointment3;
                            break;
                        case 4:
                            evnt.AppointmentTypeColor = appColors.Appointment4;
                            break;
                        case 5:
                            evnt.AppointmentTypeColor = appColors.Installation;
                            break;
                        case 6:
                            evnt.AppointmentTypeColor = appColors.Service;
                            break;
                        case 7:
                            evnt.AppointmentTypeColor = appColors.Other;
                            break;
                        case 8:
                            evnt.AppointmentTypeColor = appColors.Personal;
                            break;
                        case 9:
                            evnt.AppointmentTypeColor = appColors.Vacation;
                            break;
                        case 10:
                            evnt.AppointmentTypeColor = appColors.Holiday;
                            break;
                        case 11:
                            evnt.AppointmentTypeColor = appColors.DayOff;
                            break;
                        case 12:
                            evnt.AppointmentTypeColor = appColors.Meeting_Training;
                            break;
                        case 13:
                            evnt.AppointmentTypeColor = appColors.Followup;
                            break;
                        default:
                            break;
                    }

                }
            }

            var reminders = Reminders();
            var recurrancepattern = Recurrancepattern();
            var dayoftheMonth = DayoftheMonth();
            var weekOfTheMonth = WeekOfTheMonth();
            var dayOfWeek = DayOfWeek();
            var yearlyMontRepeatsOn = YearlyMontRepeatsOn();

            if (status == IMessageConstants.Success)
            {
                if (filter.IsMainCalendar == true)
                {
                    return Json(tasks);
                }
                //if (filter.CalendarId > 0)
                //{
                //    CalendarManager calmgr = new CalendarManager(SessionManager.CurrentUser);
                //    var AccountOpportunity = calmgr.GetCalendarAppointmentsByCalendarId(Int32.Parse(filter.CalendarId.ToString()));
                //    if (AccountOpportunity != null)
                //    {
                //    }
                //}
                return Json(new
                {
                    Events = events,
                    RecurringMaster = recurMasters,
                    Reminder = reminders,
                    Recurrancepattern = recurrancepattern,
                    DayoftheMonth = dayoftheMonth,
                    WeekOfTheMonth = weekOfTheMonth,
                    DayOfWeek = dayOfWeek,
                    yearlyMontRepeatsOn = yearlyMontRepeatsOn,
                    Tasks = tasks,
                    AppointmentTypeEnum = filter.includeLookup ? appointmentDic : null,
                    RecurringPatternEnum = filter.includeLookup ? RecurringPatternEnum.Daily.ToDictionary<byte>() : null,
                    DayOfWeekEnum = filter.includeLookup ? DayOfWeekEnum.Friday.ToDictionary<byte>() : null,
                    DayOfWeekIndexEnum = filter.includeLookup ? DayOfWeekIndexEnum.First.ToDictionary<byte>() : null,
                    EventTypeEnum = filter.includeLookup ? EventTypeEnum.Occurrence.ToDictionary<byte>() : null
                });
            }
            else
                return ResponseResult(status);

        }

        /// <summary>
        /// Save Calendar
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult Post([FromBody]CalendarModel model)
        {
            CalendarManager CalendarMgr = new CalendarManager(SessionManager.CurrentUser);
            ExchangeManager excmgr = new ExchangeManager();
            CommunicationManager Communication = new CommunicationManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);


            EventLogger.LogEvent(JsonConvert.SerializeObject(model), "CalendarController Post call started", LogSeverity.Information);
            // return ResponseResult("Invalid calendar data");
            if (model == null)
                return ResponseResult("Invalid calendar data");

            //if (model.EventRecurring != null && model.EventRecurring.EndsOn != null)
            //    model.EventRecurring.EndsOn = ((DateTimeOffset)model.EventRecurring.EndsOn).Date.AddHours(23).AddMinutes(59);

            if (model != null && model.EventRecurring != null && model.EventRecurring.EndDate != null && model.EventRecurring.EndsOn != null)  //   && model.EventRecurring.EndsOn < model.EventRecurring.EndDate
            {
                if (((DateTimeOffset)model.EventRecurring.EndsOn).Date.AddHours(23).AddMinutes(59) < model.EventRecurring.EndDate)
                    return ResponseResult("Event / Task to date cannot be after Repeat Ends On Date.");
            }

            if (model.allDay)
            {
                model.start = model.start.Date;
                model.end = model.end.Date.AddHours(23).AddMinutes(59);
            }

            try
            {
                int calId = 0;

                var calendar = model.ToCalendar(SessionManager.CurrentFranchise.FranchiseId);

                EventLogger.LogEvent(JsonConvert.SerializeObject(calendar), "CalendarController map to calendar model", LogSeverity.Information);

                calendar.LastUpdatedUtc = DateTime.UtcNow;
                calendar.CreatedByPersonId = calendar.LastUpdatedByPersonId = SessionManager.CurrentUser.PersonId;
                if (calendar.EventRecurring != null)
                    calendar.EventRecurring.CreatedByPersonId = SessionManager.CurrentUser.PersonId;

                string warning;
                CalendarMgr = new CalendarManager(SessionManager.CurrentUser);

                if (calendar.CalendarId > 0)
                {
                    if (calendar.RecurringEventId != null)
                    {
                        if (calendar.EventRecurring != null)
                        {
                            calendar.EventRecurring.RecurringEventId = Int32.Parse(calendar.RecurringEventId.ToString());
                        }

                    }

                }
                // Get Calendar info in Calendar Id exist
                CalendarVM calendardata = new CalendarVM();
                if (calendar.CalendarId > 0)
                    calendardata = CalendarMgr.GetCalendar(calendar.CalendarId, calendar.FranchiseId);

                var status = CalendarMgr.InsertEvent(out calId, calendar, out warning, model.OriginalStartDate);

                if (excmgr == null)
                {
                    excmgr = new ExchangeManager();
                }
                if (calendar.CalendarId > 0)
                {
                    excmgr.SyncChannel_Up(calendar.CalendarId);
                }
                else
                {
                    excmgr.SyncChannel_Up(calId);
                }

                //Sending email - appointment confirmation email
                //Sent email to account/lead/opportunity
                string SendType = ""; var Status = "";
                if (model.AptTypeEnum == AppointmentTypeEnumTP.Installation && model.emailType != "OrderAppointment")
                {
                    if (calendar.CalendarId > 0)
                    {
                        if (model.SendEmail && model.IsNotifyemails)
                        {
                            SendType = "Email";
                            Status = SendMailOrText(calendardata, calendar, EmailType.Installation, SendType);
                        }
                        if (model.SendEmail && model.IsNotifyText)
                        {
                            SendType = "TextSms";
                            Status = SendMailOrText(calendardata, calendar, EmailType.Installation, SendType);
                        }
                    }
                    else
                    {
                        calendar.CalendarId = calId;
                        if (model.SendEmail && model.IsNotifyemails)
                        {
                            CalendarMgr.SendEmail(calendar, EmailType.Installation); //, 5);
                        }
                        if (model.SendEmail && model.IsNotifyText)
                        {
                            var smsdata = CalendarMgr.GetTextingModal(calendar);
                            Communication.SendTextSms(smsdata, EmailType.Installation);  //, 5);
                        }

                    }

                }
                else if (model.AptTypeEnum == AppointmentTypeEnumTP.Appointment)
                {
                    if ((model.AccountId != null && model.AccountId > 0) || (model.LeadId != null && model.LeadId > 0))
                    {
                        if (calendar.CalendarId > 0)
                        {
                            if (model.SendEmail && model.IsNotifyemails)
                            {
                                SendType = "Email";
                                Status = SendMailOrText(calendardata, calendar, EmailType.AppointmentConfirmation, SendType);
                            }
                            if (model.SendEmail && model.IsNotifyText)
                            {
                                SendType = "TextSms";
                                Status = SendMailOrText(calendardata, calendar, EmailType.AppointmentConfirmation, SendType);
                            }

                        }
                        else
                        {
                            calendar.CalendarId = calId;
                            if (model.SendEmail && model.IsNotifyemails)
                            {
                                CalendarMgr.SendEmail(calendar, EmailType.AppointmentConfirmation); //, 2);
                            }

                            if (model.SendEmail && model.IsNotifyText)
                            {
                                var smsdata = CalendarMgr.GetTextingModal(calendar);
                                Communication.SendTextSms(smsdata, EmailType.AppointmentConfirmation);  //, 2);
                            }
                        }

                    }
                }

                if (model.OrderId != null && model.emailType == "OrderAppointment")
                {
                    //"Installation Scheduled" is set when Procurement is Complete and an Installation appointment has been scheduled 
                    //change order status to Installation scheduled
                    if (model.AptTypeEnum == AppointmentTypeEnumTP.Installation)
                    {
                        OrderManager ordmgr = new OrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                        ordmgr.UpdateOrderStatus(model.OrderId);
                    }
                }
                return ResponseResult(status, Json(new { Id = calId, warning }));
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return BadRequest(ex.Message);
            }

        }

        private string SendMailOrText(CalendarVM calendardata, CalendarVM calendar, EmailType type, string SendType)
        {
            try
            {
                CalendarManager CalendarMgr = new CalendarManager(SessionManager.CurrentUser);
                CommunicationManager Communication = new CommunicationManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

                DateTime dbSD = calendardata.StartDate;
                DateTime dbED = calendardata.EndDate;
                bool prevAllday = calendardata.IsAllDay;
                var Attendees = calendardata.Attendees;


                DateTime upSD = calendar.StartDate;
                DateTime upED = calendar.EndDate;
                bool currAllday = calendar.IsAllDay;
                var Attendee = calendar.Attendees;


                bool Check = false;
                foreach (var item in Attendee)
                {
                    var result = Attendees.Where(x => x.PersonId == item.PersonId).FirstOrDefault();
                    if (result == null)
                    {
                        Check = true;
                        break;
                    }

                }
                if (Check == false && Attendees.Count != Attendee.Count)
                {
                    Check = true;
                }

                if (type == EmailType.AppointmentConfirmation)
                {
                    if (SendType == "Email")
                    {
                        if (dbSD != upSD || dbED != upED || Check == true || calendardata.Location != calendar.Location || prevAllday != currAllday)
                            CalendarMgr.SendEmail(calendar, EmailType.AppointmentConfirmation); //, 2);
                    }
                    else
                    {
                        if (dbSD != upSD || dbED != upED || Check == true || calendardata.Location != calendar.Location || prevAllday != currAllday)
                        {
                            var smsdata = CalendarMgr.GetTextingModal(calendar);
                            Communication.SendTextSms(smsdata, EmailType.AppointmentConfirmation);
                        }
                    }
                }
                else
                {
                    if (SendType == "Email")
                    {
                        if (dbSD != upSD || dbED != upED || calendardata.Subject != calendar.Subject || prevAllday != currAllday)
                            CalendarMgr.SendEmail(calendar, EmailType.Installation); //, 5);
                    }
                    else
                    {
                        if (dbSD != upSD || dbED != upED || calendardata.Subject != calendar.Subject || prevAllday != currAllday)
                        {
                            var smsdata = CalendarMgr.GetTextingModal(calendar);
                            Communication.SendTextSms(smsdata, EmailType.Installation);
                        }
                    }
                }
                return "Success";
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return (ex.Message);
            }

        }

        /// <summary>
        /// Delete a calendar event
        /// </summary>
        /// <param name="Id">Event Id</param>
        /// <param name="occurStartDate">occurStartDate</param>
        [HttpDelete, Route("Delete/{Id}")]
        public IHttpActionResult Delete(int Id, DateTime? occurStartDate = null)
        {
            try
            {
                var status = "";
                if (Id > 0)
                {
                    CalendarManager CalendarMgr = new CalendarManager(SessionManager.CurrentUser);
                    status = CalendarMgr.DeleteEvent(Id, DateTime.UtcNow, occurStartDate);
                }
                return Json(status);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get the Events For Case
        /// </summary>
        /// <param name="id">CaseId</param>
        /// <returns></returns>
        [HttpGet, Route("GetEventsForCase/{id}")]
        public IHttpActionResult GetEventsForCase(int id)
        {
            try
            {
                var manager = new CalendarManager(SessionManager.CurrentUser);
                var result = manager.GetEventsForCase(id);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get the Tasks For Case
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Route("GetTasksForCase/{id}")]
        public IHttpActionResult GetTasksForCase(int id)
        {
            try
            {
                var manager = new CalendarManager(SessionManager.CurrentUser);
                var result = manager.GetTasksForCase(id);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Cancel Event
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet, Route("CancelEvent/{Id}")]
        public IHttpActionResult CancelEvent(int Id)
        {
            var CalendarMgr = new CalendarManager(SessionManager.CurrentUser);
            var status = "";
            if (Id > 0)
            {
                status = CalendarMgr.CancelEvent(Id, DateTime.Now);
            }
            return Json(status);
        }

        //Get Reminder List
        protected Dictionary<string, int> Reminders()
        {
            var reminders = new Dictionary<string, int>(16);
            reminders.Add("Disabled", 0);
            reminders.Add("5 minutes", 5);
            reminders.Add("10 minutes", 10);
            reminders.Add("15 minutes", 15);
            reminders.Add("30 minutes", 30);
            reminders.Add("45 minutes", 45);
            reminders.Add("1 hour", 60);
            reminders.Add("2 hours", 120);
            reminders.Add("4 hours", 240);
            reminders.Add("8 hours", 480);
            reminders.Add("1 day", 1440);
            reminders.Add("2 days", 2880);
            reminders.Add("3 days", 4320);
            reminders.Add("4 days", 5760);
            reminders.Add("1 week", 10080);
            reminders.Add("2 weeks", 20160);
            return reminders;
        }
        protected Dictionary<string, int> Recurrancepattern()
        {
            var recurrancepattern = new Dictionary<string, int>(4);
            recurrancepattern.Add("Daily", 1);
            recurrancepattern.Add("Weekly", 2);
            recurrancepattern.Add("Monthly", 3);
            recurrancepattern.Add("Yearly", 4);
            return recurrancepattern;
        }
        protected Dictionary<string, int> DayoftheMonth()
        {
            var dayoftheMonth = new Dictionary<string, int>(31);
            dayoftheMonth.Add("1", 1);
            dayoftheMonth.Add("2", 2);
            dayoftheMonth.Add("3", 3);
            dayoftheMonth.Add("4", 4);
            dayoftheMonth.Add("5", 5);
            dayoftheMonth.Add("6", 6);
            dayoftheMonth.Add("7", 7);
            dayoftheMonth.Add("8", 8);
            dayoftheMonth.Add("9", 9);
            dayoftheMonth.Add("10", 10);
            dayoftheMonth.Add("11", 11);
            dayoftheMonth.Add("12", 12);
            dayoftheMonth.Add("13", 13);
            dayoftheMonth.Add("14", 14);
            dayoftheMonth.Add("15", 15);
            dayoftheMonth.Add("16", 16);
            dayoftheMonth.Add("17", 17);
            dayoftheMonth.Add("18", 18);
            dayoftheMonth.Add("19", 19);
            dayoftheMonth.Add("20", 20);
            dayoftheMonth.Add("21", 21);
            dayoftheMonth.Add("22", 22);
            dayoftheMonth.Add("23", 23);
            dayoftheMonth.Add("24", 24);
            dayoftheMonth.Add("25", 25);
            dayoftheMonth.Add("26", 26);
            dayoftheMonth.Add("27", 27);
            dayoftheMonth.Add("28", 28);
            dayoftheMonth.Add("29", 29);
            dayoftheMonth.Add("30", 30);
            dayoftheMonth.Add("31", 31);
            return dayoftheMonth;
        }
        protected Dictionary<string, string> WeekOfTheMonth()
        {
            var WeekOfTheMonth = new Dictionary<string, string>(5);
            WeekOfTheMonth.Add("First", "First");
            WeekOfTheMonth.Add("Second", "Second");
            WeekOfTheMonth.Add("Third", "Third");
            WeekOfTheMonth.Add("Fourth", "Fourth");
            WeekOfTheMonth.Add("Last", "Last");
            return WeekOfTheMonth;
        }
        protected Dictionary<string, string> DayOfWeek()
        {
            var DayOfWeek = new Dictionary<string, string>(7);
            DayOfWeek.Add("Sunday", "Sunday");
            DayOfWeek.Add("Monday", "Monday");
            DayOfWeek.Add("Tuesday", "Tuesday");
            DayOfWeek.Add("Wednesday", "Wednesday");
            DayOfWeek.Add("Thursday", "Thursday");
            DayOfWeek.Add("Friday", "Friday");
            DayOfWeek.Add("Saturday", "Saturday");
            return DayOfWeek;
        }
        protected Dictionary<string, string> YearlyMontRepeatsOn()
        {
            var yearlyMontRepeatsOn = new Dictionary<string, string>(12);
            yearlyMontRepeatsOn.Add("January", "January");
            yearlyMontRepeatsOn.Add("February", "February");
            yearlyMontRepeatsOn.Add("March", "March");
            yearlyMontRepeatsOn.Add("April", "April");
            yearlyMontRepeatsOn.Add("May", "May");
            yearlyMontRepeatsOn.Add("June", "June");
            yearlyMontRepeatsOn.Add("July", "July");
            yearlyMontRepeatsOn.Add("August", "August");
            yearlyMontRepeatsOn.Add("September", "September");
            yearlyMontRepeatsOn.Add("October", "October");
            yearlyMontRepeatsOn.Add("November", "November");
            yearlyMontRepeatsOn.Add("December", "December");
            return yearlyMontRepeatsOn;
        }


    }
}
