﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HFC.CRM.Core.Logs;
using HFC.CRM.Managers;
using HFC.CRM.Service.Models;

namespace HFC.CRM.Service.Controllers
{
    public class AdminSystemDocsController : BaseController
    {
        [HttpGet, Route("api/SystemDocs/GetGlobaldocList")]
        public IHttpActionResult GetGlobaldocList()
        {

            try
            {
                SourceManager sourcemgr = new SourceManager();
                List<globaldoc> gd = new List<globaldoc>();
                var result = sourcemgr.GetGlobaldocList();

                foreach (var q in result)
                {
                    globaldoc gdoc = new globaldoc();
                    gdoc.Id = q.Id;
                    gdoc.Title = q.Title;
                    gdoc.Name = q.Name;
                    gdoc.Category = q.Category;
                    gdoc.Concept = q.Concept;
                    gdoc.streamId = q.streamId;
                    gdoc.IsEnabled = q.IsEnabled;
                    gdoc.CreatedOn = q.CreatedOn.ToString("MM'/'dd'/'yyyy");


                    if (q.Concept == 1)
                        gdoc.ConceptTitle = "Budget Blinds";
                    else if (q.Concept == 2)
                        gdoc.ConceptTitle = "Tailored Living";
                    else if (q.Concept == 3)
                        gdoc.ConceptTitle = "Concrete Crafts";

                    if (q.Category == 1)
                        gdoc.CategoryTitle = "Sales";
                    else if (q.Category == 2)
                        gdoc.CategoryTitle = "Install";
                    else if (q.Category == 3)
                        gdoc.CategoryTitle = "Product Guides";
                    else if (q.Category == 4)
                        gdoc.CategoryTitle = "Help";
                    //  gdoc.Id = q.Id;
                    gd.Add(gdoc);

                }
                return Json(gd);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        [HttpPost, Route("api/SystemDocs/DeleteGlobalDocument/{Id}")]
        public IHttpActionResult DeleteGlobalDocument(int Id)
        {
            try
            {
                NoteManager noteMgr = new NoteManager(Getuser(), GetFranchise());
                return Json(noteMgr.DeleteGlobalDocument(Id));
            }
            catch (Exception e)
            {
                return ResponseResult(EventLogger.LogEvent(e));
            }
        }

    }
}
