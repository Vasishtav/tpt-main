﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    public class BulkPurchaseController : BaseController
    {
        /// <summary>
        /// Get Orders
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetOrders")]
        public IHttpActionResult GetOrders()
        {
            try
            {
                BulkPurchaseManager bpManager = new BulkPurchaseManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = bpManager.GetOrders();

                var orderIds = result.Where(x => x.QuoteLines.Count > 3).Select(y => new { id = y.OrderID }).ToList();

                return Json(result);

            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Post Convert Sales Order To xVpo
        /// </summary>
        /// <param name="models">List of OrderId</param>
        /// <returns></returns>
        [HttpPost, Route("PostConvertSalesOrderToxVpo")]
        public IHttpActionResult PostConvertSalesOrderToxVpo(List<int> models)
        {
            try
            {
                PurchaseOrderManager poManager = new PurchaseOrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                poManager.ConvertOrderToXMasterPurchaseOrders(models);

                return Json("Success");
            }
            catch (Exception ex)
            {
                return Json(new { data = "", error = ex.Message });
            }
        }
    }
}