﻿using HFC.CRM.Managers;


namespace HFC.CRM.Service.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Http;

    using Core.Common;
    using Data;
    using Managers.AdvancedEmailManager;
    using System.Data.Entity;
    using System;
    using Core.Membership;
    using Core;
    using System.Net.Mail;
    using Core.Logs;
    using DTO.SentEmail;
    using Serializer.Leads;
    using SelectPdf;
    using Serializer;
    using Data.Context;
    using System.IO;
    using static Core.Common.ContantStrings;
    using System.Net.Http;
    using System.Net;
    using System.Net.Http.Headers;
    using HFC.CRM.Serializer.Shipment;
    using HFC.CRM.Core.Extensions;
    using HFC.CRM.Serializer.PurchaseOrder;
    using HFC.CRM.DTO;
    using System.Data;

    public class PrintAndEmailController : BaseController
    {
        #region Reminder email job

        /// <summary>
        /// Reminder email job
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet, Route("api/Email/SendEmailByIds")]
        public IHttpActionResult SendEmailByIds()
        {
            try
            {
                EmailManager emailMgr = new EmailManager(null, null);
                CommunicationManager Communication = new CommunicationManager(null, null);

                var result = emailMgr.SendRemaindarEmail();
                result = Communication.SendRemainderTextSms();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return Json("Error");
            }
        }
        #endregion

        #region Send Email
        /// <summary>
        /// Send Email
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost, Route("api/Email/SendEmail")]
        public IHttpActionResult SendEmail(EmailData data)
        {
            try
            {
                List<string> ccAddresses = new List<string>();
                List<string> bccAddresses = new List<string>();
                string fileDownloadName = "";

                if (data.ccAddresses != "")
                    ccAddresses = data.ccAddresses.Split(',').ToList();

                if (data.bccAddresses != "")
                    bccAddresses = data.bccAddresses.Split(',').ToList();

                int? LeadId = null, AccountId = null, OpportunityId = null, OrderId = null;

                ExchangeManager exchange = new ExchangeManager();
                EmailManager emailMgr = new EmailManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

                if (data.Associatedwith == "Lead")
                    LeadId = data.id;
                if (data.Associatedwith == "Opportunity")
                {
                    var oppInfo = emailMgr.GetOpportunityInfo(data.id);
                    OpportunityId = data.id;
                    AccountId = oppInfo.AccountId;
                }

                if (data.Associatedwith == "Order")
                {
                    var orderInfo = emailMgr.GetOrderInfo(data.id);
                    OrderId = data.id;
                    OpportunityId = orderInfo.OpportunityId;
                    AccountId = orderInfo.AccountId;
                }

                byte[] pdf = null;

                if (data.packetType == "SalesPacket" && data.printOptions != "")
                {
                    pdf = PrintSalesPacket(data.id, data.Associatedwith, data.printOptions, data.salesPacketOption, out fileDownloadName);
                }
                if (data.packetType == "InstallationPacket" && data.printOptions != "")
                {
                    pdf = InstallationPacket(data.id, data.printOptions, data.installPacketOptions, out fileDownloadName);
                }
                var emailtemplate = emailMgr.GetHtmlBody(EmailType.GlobalEmail, SessionManager.CurrentFranchise.BrandId); //10, SessionManager.CurrentFranchise.BrandId);
                var body = emailtemplate.TemplateLayout;
                data.subject = HttpUtility.HtmlDecode(data.subject);
                data.body = HttpUtility.HtmlDecode(data.body);

                string[] BrandNameAbbr = { "BB", "TL", "CC" };
                //To get base server url to send with image 
                HttpContext context = HttpContext.Current;
                string baseUrl = context.Request.Url.Authority + context.Request.ApplicationPath.TrimEnd('/');
                string ServerImgPath = "http://" + baseUrl + "/Images/brand/" + BrandNameAbbr[SessionManager.CurrentFranchise.BrandId - 1] + "/";

                body = body.Replace("{IMAGEPATH}", ServerImgPath).Replace("{bodycontent}", data.body);

                exchange.SendEmailMultipleAttachment(SessionManager.CurrentUser.Person.PrimaryEmail, data.subject, body,
                    data.toAddresses.Split(',').ToList(), ccAddresses,
                     bccAddresses, data.streamid, pdf, fileDownloadName);


                emailMgr.AddSentEmail(data.toAddresses, data.subject, body, true, "", ccAddresses, bccAddresses, SessionManager.CurrentUser.Person.PrimaryEmail, LeadId, AccountId, OpportunityId, null, emailtemplate.EmailTemplateId, OrderId);
                return Json(true);

            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex.Message);
            }
        }
        #endregion

        #region Print
        private string error = @"An error occurred.If the problem persists, please contact your system administrator.";

        /// <summary>
        /// Print Sales Packet
        /// </summary>
        /// <remarks>
        /// Notes
        /// 
        ///        In Swagger, pdf couldn't able to view properly, So for testing use chrome extension "ModHeader"
        ///        
        ///        //id
        ///        if sourceType=Lead then leadId 
        ///        if sourceType=Account then accountId 
        ///        if sourceType=Opportunity then opportunityId
        ///        if sourceType=Quote then opportunityId
        ///        if sourceType=Order then orderId
        ///        
        ///        sourceType = Lead/Account/Opportunity/Quote/Order
        ///        
        ///        //Sample values, slip symbols(/|)
        ///        printoptions = leadDetails/internalNotes/externalNotes/directionNotes/googleMap/quotePrintFormat=/quoteKey|/mydocuments_230
        ///        
        ///        //Sample values, slip symbols(/|)
        ///        salesPacketOption = 18f377d3-bef9-e811-910a-9418827bb241|1/833959cc-f5f3-e911-9bd6-ace2d3633531
        ///     
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="sourceType"></param>
        /// <param name="printoptions"></param>
        /// <param name="salesPacketOption"></param>
        /// <returns></returns>
        [HttpGet, Route("api/Print/PrintSalesPacket/{id}")]
        public HttpResponseMessage PrintSalesPacket(int id, string sourceType = "Lead", string printoptions = null, string salesPacketOption = null)
        {
            ExportTypeEnum type = ExportTypeEnum.PDF;
            string fileDownloadName = "";
            string data = string.Empty;
            var options = printoptions.ToString().Split(new char[] { '/' }).ToList();

            if (id <= 0 || string.IsNullOrEmpty(printoptions))
                throw new Exception(error);

            try
            {

                //// https://selectpdf.com/docs/ModifyExistingPdf.htm
                bool IsPacket = false;
                int quotekey = 0;
                PdfDocument doc = new PdfDocument();

                var leadDetails = options.ToList().Where(c => c.Contains("leadDetails"))
                .FirstOrDefault();
                var internalNotes = options.ToList().Where(c => c.Contains("internalNotes"))
                  .FirstOrDefault();
                var externalNotes = options.ToList().Where(c => c.Contains("externalNotes"))
                   .FirstOrDefault();
                var directionNotes = options.ToList().Where(c => c.Contains("directionNotes"))
                  .FirstOrDefault();
                var googleMap = options.ToList().Where(c => c.Contains("googleMap"))
                    .FirstOrDefault();
                var existingMeasurements = options.ToList().Where(c => c.Contains("existingMeasurements"))
                   .FirstOrDefault();
                var bbmeasurementForm = options.ToList()
                    .Where(c => c.Contains("bbmeasurementForm"))
                    .FirstOrDefault();
                var ccmeasurementForm = options.ToList()
                   .Where(c => c.Contains("ccmeasurementForm"))
                   .FirstOrDefault();
                var garagemeasurementForm = options.ToList()
                   .Where(c => c.Contains("garagemeasurementForm"))
                   .FirstOrDefault();
                var closetmeasurementForm = options.ToList()
                   .Where(c => c.Contains("closetmeasurementForm"))
                   .FirstOrDefault();
                var officemeasurementForm = options.ToList()
                   .Where(c => c.Contains("officemeasurementForm"))
                   .FirstOrDefault();
                var floormeasurementForm = options.ToList()
                  .Where(c => c.Contains("floormeasurementForm"))
                  .FirstOrDefault();

                if (!string.IsNullOrEmpty(leadDetails) && !string.IsNullOrEmpty(googleMap) && !string.IsNullOrEmpty(existingMeasurements) && !string.IsNullOrEmpty(salesPacketOption))
                    IsPacket = true;

                if (!string.IsNullOrEmpty(leadDetails))
                {
                    data = LeadSheetReport.GetLeadCustomerContent(id, options, sourceType);

                    var leadSheet = GetLeadSheetPdf(data);
                    if (leadSheet != null)
                    {
                        var notes = GetNotes(id, internalNotes, externalNotes, directionNotes, sourceType);
                        if (notes != null)
                        {
                            leadSheet.Append(notes);
                        }
                        doc.Append(leadSheet);
                    }
                }

                var google = GetGooglemap(id, googleMap, sourceType);
                if (google != null)
                {
                    doc.Append(google);
                }

                var em = GetExistingMeasurements(id, existingMeasurements, sourceType);
                if (em != null)
                {
                    doc.Append(em);
                }

                var bbm = GetbbmeasurementForm(bbmeasurementForm);
                if (bbm != null)
                {
                    doc.Append(bbm);
                }

                var ccm = GetccmeasurementForm(ccmeasurementForm);
                if (ccm != null) doc.Append(bbm);

                var garage = GetgaragemeasurementForm(garagemeasurementForm);
                if (garage != null) doc.Append(garage);

                var closet = GetclosetmeasurementForm(closetmeasurementForm);
                if (closet != null) doc.Append(closet);

                var office = GetofficemeasurementForm(officemeasurementForm);
                if (office != null) doc.Append(office);

                var floor = GetfloormeasurementForm(floormeasurementForm);
                if (floor != null) doc.Append(floor);

                var quote = options.ToList().Where(c => c == "quote").SingleOrDefault();
                //moved the code to get the quotekey
                var quotekeyvalue = options.ToList().Where(c => c.Contains("quoteKey")).FirstOrDefault();
                string[] arrquotekey = new string[] { };
                if (!string.IsNullOrEmpty(quotekeyvalue))
                {
                    arrquotekey = quotekeyvalue.Split('|');
                    if (arrquotekey != null && arrquotekey.Length == 2 && !string.IsNullOrEmpty(arrquotekey[1]))
                        int.TryParse(arrquotekey[1], out quotekey);//quotekey = Convert.ToInt32(arrquotekey[1]);
                }

                if ((sourceType == "Opportunity" || sourceType == "Quote") && !string.IsNullOrEmpty(quote))
                {
                    var quotePrintFormat = options.ToList().Where(c => c.Contains("quotePrintFormat")).SingleOrDefault();

                    PrintVersion printVersion = PrintVersion.ExcludeDiscount;

                    if (!string.IsNullOrEmpty(quotePrintFormat))
                    {
                        var temp = quotePrintFormat.Split('=');
                        if (temp.Length > 1 && temp[1] == "IncludeDiscount")
                            printVersion = PrintVersion.IncludeDiscount;
                        else if (temp.Length > 1 && temp[1] == "CondensedVersion")
                            printVersion = PrintVersion.CondensedVersion;
                        else
                            printVersion = PrintVersion.ExcludeDiscount;
                    }

                    if (quotekey != 0)
                    {
                        QuoteSheetReport quotesheet = new QuoteSheetReport();
                        var docQuote = quotesheet.createQuotePDFDoc(quotekey, printVersion);
                        doc.Append(docQuote);
                    }
                    else
                    {
                        var oppmgr = new OpportunitiesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                        var opportunity = oppmgr.Get(id);
                        var quotemgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                        var quoteobj = quotemgr.GetQuote(id).Where(q => q.PrimaryQuote == true).FirstOrDefault();
                        if (quoteobj != null) quotekey = quoteobj.QuoteKey;

                        if (quotekey != 0)
                        {
                            QuoteSheetReport quotesheet = new QuoteSheetReport();
                            var docQuote = quotesheet.createQuotePDFDoc(quotekey, printVersion);
                            doc.Append(docQuote);
                        }
                    }

                }

                if (salesPacketOption != null)
                {
                    var salesOptions = salesPacketOption.ToString().Split(new char[] { '/' }).ToList();
                    var sp = GetSalesPacketdocumentsPdf(salesOptions.ToList());
                    if (sp != null)
                        doc.Append(sp);
                }

                var mydocuments = options.Where(c => c.Contains("mydocuments_")).ToList();

                var md = GetMydocumentsPdf(mydocuments);
                if (md != null && md.Pages.Count > 0) doc.Append(md);

                // save pdf document
                byte[] pdf = doc.Save();

                doc.Close();

                if (sourceType == "Quote" && quotekey != 0)
                    fileDownloadName = GetFileDownloadName(quotekey, sourceType, type, IsPacket);
                else if (sourceType == "Opportunity" && quotekey != 0 && !string.IsNullOrEmpty(quote))
                    fileDownloadName = GetFileDownloadName(quotekey, "Quote", type, false);
                else
                    fileDownloadName = GetFileDownloadName(id, sourceType, type, IsPacket);

                var result = new HttpResponseMessage(HttpStatusCode.OK);
                var stream = new MemoryStream(pdf);
                result.Content = new StreamContent(stream);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(fileDownloadName));
                return result;

            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw ex;
            }
        }

        /// <summary>
        /// Installation Packet
        /// </summary>
        /// <remarks>
        /// Notes
        /// 
        ///        In Swagger, pdf couldn't able to view properly, So for testing use chrome extension "ModHeader"
        ///        
        ///        //id
        ///        if sourceType=Lead then leadId 
        ///        if sourceType=Account then accountId 
        ///        if sourceType=Opportunity then opportunityId
        ///        if sourceType=Quote then opportunityId
        ///        if sourceType=Order then orderId
        ///        
        ///        sourceType = Lead/Account/Opportunity/Quote/Order
        ///        
        ///        //Sample values, slip symbols(/|)
        ///        printoptions = leadDetails/internalNotes/externalNotes/directionNotes/googleMap/quotePrintFormat=/quoteKey|/mydocuments_230
        ///        
        ///        //Sample values, slip symbols(/|)
        ///        salesPacketOption = 18f377d3-bef9-e811-910a-9418827bb241|1/833959cc-f5f3-e911-9bd6-ace2d3633531
        ///     
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="inline"></param>
        /// <param name="printoptions"></param>
        /// <param name="installPacketOptions"></param>
        /// <returns></returns>
        [HttpGet, Route("api/Print/InstallationPacket/{id}")]
        public HttpResponseMessage InstallationPacket(int id, bool inline = false, string printoptions = null, string installPacketOptions = null)
        {
            QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            OrderManager orderMgr = new OrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

            ExportTypeEnum type = ExportTypeEnum.PDF;
            if (id <= 0 || string.IsNullOrEmpty(printoptions))
            {
                throw new Exception("Invalid Order id");
            }

            var options = printoptions.ToString().Split(new char[] { '/' }).ToList();
            string fileDownloadName = "";
            string data = string.Empty;
            string leaddata = string.Empty;
            string measurementdata = string.Empty;
            string googledata = string.Empty;
            string customerdata = string.Empty;
            string orderdata = string.Empty;

            try
            {
                var leadDetails = options.ToList().Where(c => c.Contains("AddleadCustomer"))
                .FirstOrDefault();
                var googleMap = options.ToList().Where(c => c.ToLower().Contains("googlemap"))
                    .FirstOrDefault();
                var existingMeasurements = options.ToList().Where(c => c.Contains("existingMeasurements"))
                   .FirstOrDefault();

                var AddOrderCustomerInvoice = options.ToList().Where(c => c.Contains("AddOrderCustomerInvoice")).SingleOrDefault().Contains("true");
                var AddOrderInstallerInvoice = options.ToList().Where(c => c.Contains("AddOrderInstallerInvoice")).SingleOrDefault().Contains("true");
                var order = orderMgr.GetOrderByOrderId(id);
                var Details = QuotesMgr.GetSidemarkOppId(order.QuoteKey);

                if (!string.IsNullOrEmpty(leadDetails) && leadDetails.Contains("true"))
                {
                    data = LeadSheetReport.GetLeadCustomerContentForInstallationPacket(order.OpportunityId);
                }

                //for checking conditions for order packet
                var IsleadDetails = options.ToList().Where(c => c.Contains("AddleadCustomer")).SingleOrDefault().Contains("true");
                var IsgoogleMap = options.ToList().Where(c => c.ToLower().Contains("googlemap")).SingleOrDefault().Contains("true");
                if (IsleadDetails && IsgoogleMap && AddOrderCustomerInvoice && !string.IsNullOrEmpty(installPacketOptions))
                    fileDownloadName = string.Format("OPP {0} - SO {1} - {2} - Order Packet.pdf", Details.OpportunityId, order.OrderNumber, Details.SideMark.Replace("/", "-"));
                else
                    fileDownloadName = string.Format("OPP {0} - SO {1} - {2}.pdf", Details.OpportunityId, order.OrderNumber, Details.SideMark.Replace("/", "-"));

                // instantiate a html to pdf converter object
                HtmlToPdf converter = new HtmlToPdf();

                converter.Options.MarginTop = 20;
                converter.Options.MarginBottom = 30;
                converter.Options.MarginLeft = 10;
                converter.Options.MarginRight = 10;

                converter.Options.DisplayHeader = true;
                converter.Header.DisplayOnFirstPage = true;
                converter.Header.DisplayOnOddPages = true;
                converter.Header.DisplayOnEvenPages = true;

                converter.Options.DisplayFooter = true;
                converter.Footer.DisplayOnFirstPage = true;
                converter.Footer.DisplayOnEvenPages = true;
                converter.Footer.DisplayOnOddPages = true;

                converter.Options.PdfPageSize = PdfPageSize.Letter;

                // page numbers can be added using a PdfTextSection object
                //PdfTextSection text = new PdfTextSection(0, 10, "Page {page_number} of {total_pages}", new System.Drawing.Font("Arial", 8));
                //text.HorizontalAlign = PdfTextHorizontalAlign.Right;
                //converter.Footer.Add(text);

                string baseUrl = Serializer.Leads.LeadSheetReport.TemplateImageFilePath();
                PdfDocument doc = converter.ConvertHtmlString(data, baseUrl);

                if (string.IsNullOrEmpty(data))
                {
                    doc.RemovePageAt(0);
                }

                //// https://selectpdf.com/docs/ModifyExistingPdf.htm

                var internalNotes = options.ToList().Where(c => c.Contains("AddPrintInternalNotes")) //"internalNotes"))
                   .FirstOrDefault();
                var externalNotes = options.ToList().Where(c => c.Contains("AddPrintExternalNotes")) // "externalNotes"))
                   .FirstOrDefault();

                var directionNotes = options.ToList().Where(c => c.Contains("AddPrintDirectionNotes")) // "externalNotes"))
                   .FirstOrDefault();

                var installChecklist = options.ToList()
                    .Where(c => c.Contains("AddInstallationChecklist"))
                    .FirstOrDefault();
                //var ccmeasurementForm = options.ToList()
                //   .Where(c => c.Contains("ccmeasurementForm"))
                //   .FirstOrDefault();
                //var garagemeasurementForm = options.ToList()
                //   .Where(c => c.Contains("garagemeasurementForm"))
                //   .FirstOrDefault();
                //var closetmeasurementForm = options.ToList()
                //   .Where(c => c.Contains("closetmeasurementForm"))
                //   .FirstOrDefault();
                //var officemeasurementForm = options.ToList()
                //   .Where(c => c.Contains("officemeasurementForm"))
                //   .FirstOrDefault();
                //var floormeasurementForm = options.ToList()
                //  .Where(c => c.Contains("floormeasurementForm"))
                //  .FirstOrDefault();
                //var quote = options.ToList()
                //    .Where(c => c == "quote")
                //    .SingleOrDefault();

                if (!string.IsNullOrEmpty(leadDetails))
                {
                    internalNotes = internalNotes.Contains("true") ? "internalNotes" : "";
                    externalNotes = externalNotes.Contains("true") ? "externalNotes" : "";
                    directionNotes = directionNotes.Contains("true") ? "directionNotes" : "";
                    var notes = GetNotes(id, internalNotes, externalNotes, directionNotes, "Order");
                    if (notes != null) doc.Append(notes);
                }

                if (!string.IsNullOrEmpty(googleMap) && googleMap.ToLower().Contains("true"))
                {
                    var google = GetGooglemap(order.OpportunityId, googleMap, "Opportunity");
                    if (google != null) doc.Append(google);
                }

                //  var AddInstallationChecklist = options.ToList().Where(c => c.Contains("AddInstallationChecklist")).SingleOrDefault().Contains("true");
                //  var AddInstallationProcess = options.ToList().Where(c => c.Contains("AddInstallationProcess")).SingleOrDefault().Contains("true");
                //  var AddWarrantyInformation = options.ToList().Where(c => c.Contains("AddWarrantyInformation")).SingleOrDefault().Contains("true");
                var OrderPrintFormat = options.ToList().Where(c => c.Contains("OrderPrintFormat")).SingleOrDefault();
                PdfDocument docinvoice = new PdfDocument();
                PdfDocument docinstaller = new PdfDocument();

                //for naming install doc
                if (AddOrderInstallerInvoice == true)
                    fileDownloadName = string.Format("OPP {0} - SO {1} - {2} - INSTALL DOC.pdf", Details.OpportunityId, order.OrderNumber, Details.SideMark.Replace("/", "-"));

                if (id > 0)
                {
                    if (AddOrderCustomerInvoice)  //Customer Invoice
                    {
                        PrintVersion printVersion = PrintVersion.ExcludeDiscount;

                        if (!string.IsNullOrEmpty(OrderPrintFormat))
                        {
                            var temp = OrderPrintFormat.Split('=');
                            if (temp.Length > 1 && temp[1] == "IncludeDiscount")
                                printVersion = PrintVersion.IncludeDiscount;
                            else if (temp.Length > 1 && temp[1] == "CondensedVersion")
                                printVersion = PrintVersion.CondensedVersion;
                            else
                                printVersion = PrintVersion.ExcludeDiscount;
                        }

                        InvoiceSheetReport InvoiceSheet = new InvoiceSheetReport();
                        docinvoice = InvoiceSheet.createInvoicePDFDoc(id, printVersion);
                        doc.Append(docinvoice);
                    }

                    if (AddOrderInstallerInvoice) //Installer invoice
                    {
                        InstallerDocument installerDocument = new InstallerDocument();
                        docinstaller = installerDocument.createInstallerPDFDoc(id);
                        doc.Append(docinstaller);
                    }
                }

                //installChecklist = installChecklist.Contains("true") ? "installChecklist" : "";
                //var icl = GetInstallationChecklist(installChecklist);
                //if (icl != null) doc.Append(icl);

                var installOptions = installPacketOptions.ToString().Split(new char[] { '/' }).ToList();
                var sp = GetSalesPacketdocumentsPdf(installOptions.ToList());
                if (sp != null)
                    doc.Append(sp);

                var mydocuments = options.Where(c => c.Contains("mydocuments_")).ToList();
                var md = GetMydocumentsPdf(mydocuments);
                if (md != null) doc.Append(md);

                //var installationGuide = GetMydocumentsPdf(mydocuments);
                //if (md != null) doc.Append(md);

                // save pdf document
                byte[] pdf = doc.Save();
                doc.Close();

                var result = new HttpResponseMessage(HttpStatusCode.OK);
                var stream = new MemoryStream(pdf);
                result.Content = new StreamContent(stream);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(fileDownloadName));
                return result;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw ex;
            }
        }

        /// <summary>
        /// Print Quote
        /// </summary>
        /// <param name="id">QuoteKey</param>
        /// <param name="printOptions">IncludeDiscount/ExcludeDiscount/CondensedVersion</param>
        /// <returns></returns>
        [HttpGet, Route("api/Print/PrintQuote/{id}")]
        public HttpResponseMessage PrintQuote(int id, string printOptions = "ExcludeDiscount")
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                //for naming pdf
                string fileDownloadName = QuotesMgr.GetQuotebyQuoteKey(id);
                ExportTypeEnum type = ExportTypeEnum.PDF;
                PdfDocument doc = new PdfDocument();

                PrintVersion printVersion = PrintVersion.ExcludeDiscount;

                if (printOptions == "IncludeDiscount")
                    printVersion = PrintVersion.IncludeDiscount;
                else if (printOptions == "ExcludeDiscount")
                    printVersion = PrintVersion.ExcludeDiscount;
                else if (printOptions == "CondensedVersion")
                    printVersion = PrintVersion.CondensedVersion;

                QuoteSheetReport quotesheet = new QuoteSheetReport();
                var docQuote = quotesheet.createQuotePDFDoc(id, printVersion);
                doc.Append(docQuote);

                // save pdf document
                byte[] pdf = doc.Save();

                doc.Close();

                var result = new HttpResponseMessage(HttpStatusCode.OK);
                var stream = new MemoryStream(pdf);
                result.Content = new StreamContent(stream);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(fileDownloadName));
                return result;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw ex;
            }
        }

        /// <summary>
        /// Print MPO
        /// </summary>
        /// <param name="id">purchaseOrderId</param>
        /// <param name="printVPO"></param>
        /// <returns></returns>
        [HttpGet, Route("api/Print/PrintMPO/{id}")]
        public HttpResponseMessage PrintMPO(int id, bool printVPO)
        {
            if (id <= 0)
                throw new Exception(error);

            try
            {
                PdfDocument doc = new PdfDocument();
                PurchaseOrderSheet poSheet = new PurchaseOrderSheet();

                var docMpo = poSheet.createMPOPDFDoc(id);
                doc.Append(docMpo);
                if (printVPO)
                {
                    var docvpo = poSheet.createVPOPDFDoc(id, 0);
                    doc.Append(docvpo);
                }

                // save pdf document
                byte[] pdf = doc.Save();

                doc.Close();

                //fileDownloadName = "masterPurchaseOrder"; //GetFileDownloadName(id, sourceType, type);
                PurchaseOrderManager PurchaseOrderMgr = new PurchaseOrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                string fileDownloadName = PurchaseOrderMgr.GetMpobyId(id);

                var result = new HttpResponseMessage(HttpStatusCode.OK);
                var stream = new MemoryStream(pdf);
                result.Content = new StreamContent(stream);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(fileDownloadName));
                return result;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw ex;
            }
        }

        /// <summary>
        /// Print VPO
        /// </summary>
        /// <param name="id">orderid</param>
        /// <param name="vpoId"></param>
        /// <returns></returns>
        [HttpGet, Route("api/Print/PrintVPO/{id}/{vpoId}")]
        public HttpResponseMessage PrintVPO(int id, int vpoId )
        {
            ExportTypeEnum type = ExportTypeEnum.PDF;
            string fileDownloadName = "";
            string data = string.Empty;

            if (id <= 0)
                throw new Exception(error);

            try
            {
                PdfDocument doc = new PdfDocument();
                PurchaseOrderSheet poSheet = new PurchaseOrderSheet();
                var docMpo = poSheet.createVPOPDFDoc(id, vpoId);
                doc.Append(docMpo);
                // save pdf document
                byte[] pdf = doc.Save();
                doc.Close();
                
                fileDownloadName = "masterPurchaseOrder"; //GetFileDownloadName(id, sourceType, type);

                var result = new HttpResponseMessage(HttpStatusCode.OK);
                var stream = new MemoryStream(pdf);
                result.Content = new StreamContent(stream);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(fileDownloadName));
                return result;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw ex;
            }
        }

        /// <summary>
        /// Print Shipnotice
        /// </summary>
        /// <param name="id">ShipNoticeId</param>
        /// <returns></returns>
        [HttpGet, Route("api/Print/PrintShipnotice/{id}")]
        public HttpResponseMessage PrintShipnotice(int id)
        {
            if (id <= 0)
                throw new Exception(error);

            try
            {
                PdfDocument doc = new PdfDocument();
                ShipmentSheet shipSheet = new ShipmentSheet();
                var shipdoc = shipSheet.createShipmentDoc(id);
                if (shipSheet != null)
                {
                    doc.Append(shipdoc);
                }
                byte[] pdf = doc.Save();

                doc.Close();

                //setting name for print doc
                string date = TimeZoneManager.ToLocal(DateTime.UtcNow).GlobalDateFormat();
                string fileDownloadName = "VPO_xVPO_ShipmentID" + id + "_GoodReceipt_" + date;

                var result = new HttpResponseMessage(HttpStatusCode.OK);
                var stream = new MemoryStream(pdf);
                result.Content = new StreamContent(stream);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(fileDownloadName));
                return result;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw ex;
            }
        }

        /// <summary>
        /// Print VendorInvoice
        /// </summary>
        /// <param name="invoiceNumber"></param>
        /// <returns></returns>
        [HttpGet, Route("api/Print/PrintVendorInvoice")]
        public HttpResponseMessage PrintVendorInvoice(string invoiceNumber)
        {
            try
            {
                PdfDocument doc = new PdfDocument();
                VendorInvoiceSheet viSheet = new VendorInvoiceSheet();
                var docVI = viSheet.createVendorInvoiceDoc(invoiceNumber);
                doc.Append(docVI);
                // save pdf document
                byte[] pdf = doc.Save();
                doc.Close();

                var result = new HttpResponseMessage(HttpStatusCode.OK);
                var stream = new MemoryStream(pdf);
                result.Content = new StreamContent(stream);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(viSheet.VendorInvoiceFiledownloadname(invoiceNumber)));
                return result;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw ex;
            }
        }
        #endregion

        #region Download Rawdata

        /// <summary>
        /// Download RawData Excel
        /// </summary>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <returns></returns>
        [HttpGet, Route("api/Print/DownloadRawDataExcel")]
        public HttpResponseMessage DownloadRawDataExcel(DateTime StartDate, DateTime EndDate)
        {
            try
            {
                ReportManager reportMgr = new ReportManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                ReportFilter data = new ReportFilter();
                data.StartDate = StartDate;
                data.EndDate = EndDate;
                //var result = reportMgr.getRawDataDetail(data);

                DataTable result = reportMgr.getRawDataDetailDT(data);
                result.TableName = "RawData";

                MemoryStream stream = Serializer.Reports.Excel.ExportDataTable(result);

                string filename = string.Format("RawData_{0:MMddyy}-{1:MMddyy}.xlsx", data.StartDate, data.EndDate);

                byte[] bytes = stream.ToArray();

                var result1 = Request.CreateResponse(HttpStatusCode.OK);
                var stream1 =  new MemoryStream(bytes);
                result1.Content = new StreamContent(stream1);
                result1.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result1.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = filename
                };

                return result1;

            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw ex;
            }
        }
        #endregion

        #region Print Touchpoint provate methods


        private byte[] InstallationPacket(int id, string printoptions, string installPacketOptions, out string fileDownloadName)
        {
            QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            OrderManager orderMgr = new OrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

            var options = printoptions.ToString().Split(new char[] { '/' }).ToList();
            fileDownloadName = string.Format("PrintDocuments.pdf");
            string data = string.Empty;
            string leaddata = string.Empty;
            string measurementdata = string.Empty;
            string googledata = string.Empty;
            string customerdata = string.Empty;
            string orderdata = string.Empty;

            try
            {
                var leadDetails = options.ToList().Where(c => c.Contains("AddleadCustomer"))
                .FirstOrDefault();
                var googleMap = options.ToList().Where(c => c.ToLower().Contains("googlemap"))
                    .FirstOrDefault();
                var existingMeasurements = options.ToList().Where(c => c.Contains("existingMeasurements"))
                   .FirstOrDefault();

                var order = orderMgr.GetOrderByOrderId(id);
                var Details = QuotesMgr.GetSidemarkOppId(order.QuoteKey);

                if (!string.IsNullOrEmpty(leadDetails) && leadDetails.Contains("true"))
                {
                    data = LeadSheetReport.GetLeadCustomerContentForInstallationPacket(order.OpportunityId);
                }

                // instantiate a html to pdf converter object 
                HtmlToPdf converter = new HtmlToPdf();

                converter.Options.MarginTop = 20;
                converter.Options.MarginBottom = 30;
                converter.Options.MarginLeft = 10;
                converter.Options.MarginRight = 10;

                converter.Options.DisplayHeader = true;
                converter.Header.DisplayOnFirstPage = true;
                converter.Header.DisplayOnOddPages = true;
                converter.Header.DisplayOnEvenPages = true;

                converter.Options.DisplayFooter = true;
                converter.Footer.DisplayOnFirstPage = true;
                converter.Footer.DisplayOnEvenPages = true;
                converter.Footer.DisplayOnOddPages = true;

                converter.Options.PdfPageSize = PdfPageSize.Letter;

                // page numbers can be added using a PdfTextSection object
                //PdfTextSection text = new PdfTextSection(0, 10, "Page {page_number} of {total_pages}", new System.Drawing.Font("Arial", 8));
                //text.HorizontalAlign = PdfTextHorizontalAlign.Right;
                //converter.Footer.Add(text);

                string baseUrl = Serializer.Leads.LeadSheetReport.TemplateImageFilePath();
                PdfDocument doc = converter.ConvertHtmlString(data, baseUrl);

                if (string.IsNullOrEmpty(data))
                {
                    doc.RemovePageAt(0);
                }

                //// https://selectpdf.com/docs/ModifyExistingPdf.htm

                var internalNotes = options.ToList().Where(c => c.Contains("AddPrintInternalNotes")) //"internalNotes"))
                   .FirstOrDefault();
                var externalNotes = options.ToList().Where(c => c.Contains("AddPrintExternalNotes")) // "externalNotes"))
                   .FirstOrDefault();

                var directionNotes = options.ToList().Where(c => c.Contains("AddPrintDirectionNotes")) // "externalNotes"))
                  .FirstOrDefault();

                var installChecklist = options.ToList()
                    .Where(c => c.Contains("AddInstallationChecklist"))
                    .FirstOrDefault();

                if (!string.IsNullOrEmpty(leadDetails))
                {
                    internalNotes = internalNotes.Contains("true") ? "internalNotes" : "";
                    externalNotes = externalNotes.Contains("true") ? "externalNotes" : "";
                    directionNotes = directionNotes.Contains("true") ? "directionNotes" : "";
                    var notes = GetNotes(id, internalNotes, externalNotes, directionNotes, "Order");
                    if (notes != null) doc.Append(notes);
                }

                if (!string.IsNullOrEmpty(googleMap) && googleMap.ToLower().Contains("true"))
                {
                    var google = GetGooglemap(order.OpportunityId, googleMap, "Opportunity");
                    if (google != null) doc.Append(google);
                }

                var AddOrderCustomerInvoice = options.ToList().Where(c => c.Contains("AddOrderCustomerInvoice")).SingleOrDefault().Contains("true");
                var AddOrderInstallerInvoice = options.ToList().Where(c => c.Contains("AddOrderInstallerInvoice")).SingleOrDefault().Contains("true");
                var OrderPrintFormat = options.ToList().Where(c => c.Contains("OrderPrintFormat")).SingleOrDefault();
                PdfDocument docinvoice = new PdfDocument();
                PdfDocument docinstaller = new PdfDocument();
                if (id > 0)
                {
                    if (AddOrderCustomerInvoice)  //Customer Invoice
                    {
                        PrintVersion printVersion = PrintVersion.ExcludeDiscount;
                        if (!string.IsNullOrEmpty(OrderPrintFormat))
                        {
                            var temp = OrderPrintFormat.Split('=');
                            if (temp.Length > 1 && temp[1] == "IncludeDiscount")
                                printVersion = PrintVersion.IncludeDiscount;
                            else if (temp.Length > 1 && temp[1] == "CondensedVersion")
                                printVersion = PrintVersion.CondensedVersion;
                            else
                                printVersion = PrintVersion.ExcludeDiscount;
                        }

                        InvoiceSheetReport InvoiceSheet = new InvoiceSheetReport();
                        docinvoice = InvoiceSheet.createInvoicePDFDoc(id, printVersion);
                        doc.Append(docinvoice);
                    }

                    if (AddOrderInstallerInvoice) //Installer invoice
                    {
                        InstallerDocument installerDocument = new InstallerDocument();
                        docinstaller = installerDocument.createInstallerPDFDoc(id);
                        doc.Append(docinstaller);
                    }
                }

                var installOptions = installPacketOptions.ToString().Split(new char[] { '/' }).ToList();
                var sp = GetSalesPacketdocumentsPdf(installOptions.ToList());
                if (sp != null)
                    doc.Append(sp);


                var mydocuments = options.Where(c => c.Contains("mydocuments_")).ToList();
                var md = GetMydocumentsPdf(mydocuments);
                if (md != null) doc.Append(md);

                //var installationGuide = GetMydocumentsPdf(mydocuments);
                //if (md != null) doc.Append(md);


                // save pdf document 
                //byte[] pdf = doc.Save();

                //added the code to remove pdf. instead of sending blank pdf 
                byte[] pdf = null;
                if (doc.Pages.Count != 0)
                {
                    pdf = doc.Save();
                }


                doc.Close();

                //for naming pdf
                var IsleadDetails = options.ToList().Where(c => c.Contains("AddleadCustomer")).SingleOrDefault().Contains("true");
                var IsgoogleMap = options.ToList().Where(c => c.ToLower().Contains("googlemap")).SingleOrDefault().Contains("true");
                if (AddOrderInstallerInvoice == true)
                    fileDownloadName = string.Format("OPP {0} - SO {1} - {2} - INSTALL DOC.pdf", Details.OpportunityId, order.OrderNumber, Details.SideMark.Replace("/", "-"));
                else if (IsleadDetails && IsgoogleMap && AddOrderCustomerInvoice && !string.IsNullOrEmpty(installPacketOptions))
                    fileDownloadName = string.Format("OPP {0} - SO {1} - {2} - Order Packet.pdf", Details.OpportunityId, order.OrderNumber, Details.SideMark.Replace("/", "-"));
                else
                    fileDownloadName = string.Format("OPP {0} - SO {1} - {2}.pdf", Details.OpportunityId, order.OrderNumber, Details.SideMark.Replace("/", "-"));

                return pdf;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return null;
            }
        }

        private byte[] PrintSalesPacket(int id, string sourceType, string printoptions, string salesPacketOption, out string fileDownloadName)
        {
            OpportunitiesManager OpportunityMgr = new OpportunitiesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

            string data = string.Empty;
            fileDownloadName = string.Format("PrintDocuments.pdf");
            bool IsPacket = false;
            var options = printoptions.ToString().Split(new char[] { '/' }).ToList();

            try
            {

                PdfDocument doc = new PdfDocument();

                var leadDetails = options.ToList().Where(c => c.Contains("leadDetails"))
                .FirstOrDefault();
                var internalNotes = options.ToList().Where(c => c.Contains("internalNotes"))
                  .FirstOrDefault();
                var externalNotes = options.ToList().Where(c => c.Contains("externalNotes"))
                   .FirstOrDefault();
                var directionNotes = options.ToList().Where(c => c.Contains("directionNotes"))
                   .FirstOrDefault();

                var googleMap = options.ToList().Where(c => c.Contains("googleMap"))
                    .FirstOrDefault();
                var existingMeasurements = options.ToList().Where(c => c.Contains("existingMeasurements"))
                   .FirstOrDefault();
                var bbmeasurementForm = options.ToList()
                    .Where(c => c.Contains("bbmeasurementForm"))
                    .FirstOrDefault();
                var ccmeasurementForm = options.ToList()
                   .Where(c => c.Contains("ccmeasurementForm"))
                   .FirstOrDefault();
                var garagemeasurementForm = options.ToList()
                   .Where(c => c.Contains("garagemeasurementForm"))
                   .FirstOrDefault();
                var closetmeasurementForm = options.ToList()
                   .Where(c => c.Contains("closetmeasurementForm"))
                   .FirstOrDefault();
                var officemeasurementForm = options.ToList()
                   .Where(c => c.Contains("officemeasurementForm"))
                   .FirstOrDefault();
                var floormeasurementForm = options.ToList()
                  .Where(c => c.Contains("floormeasurementForm"))
                  .FirstOrDefault();

                if (!string.IsNullOrEmpty(leadDetails) && !string.IsNullOrEmpty(googleMap) && !string.IsNullOrEmpty(existingMeasurements) && !string.IsNullOrEmpty(salesPacketOption))
                    IsPacket = true;

                if (!string.IsNullOrEmpty(leadDetails))
                {
                    data = LeadSheetReport.GetLeadCustomerContent(id, options, sourceType);

                    var leadSheet = GetLeadSheetPdf(data);
                    if (leadSheet != null)
                    {
                        var notes = GetNotes(id, internalNotes, externalNotes, directionNotes, sourceType);
                        if (notes != null)
                        {
                            leadSheet.Append(notes);
                        }
                        doc.Append(leadSheet);
                    }
                }

                var google = GetGooglemap(id, googleMap, sourceType);
                if (google != null)
                {
                    doc.Append(google);
                }

                var em = GetExistingMeasurements(id, existingMeasurements, sourceType);
                if (em != null)
                {
                    doc.Append(em);
                }

                var bbm = GetbbmeasurementForm(bbmeasurementForm);
                if (bbm != null)
                {
                    doc.Append(bbm);
                }

                var ccm = GetccmeasurementForm(ccmeasurementForm);
                if (ccm != null) doc.Append(bbm);

                var garage = GetgaragemeasurementForm(garagemeasurementForm);
                if (garage != null) doc.Append(garage);

                var closet = GetclosetmeasurementForm(closetmeasurementForm);
                if (closet != null) doc.Append(closet);

                var office = GetofficemeasurementForm(officemeasurementForm);
                if (office != null) doc.Append(office);

                var floor = GetfloormeasurementForm(floormeasurementForm);
                if (floor != null) doc.Append(floor);


                var quote = options.ToList().Where(c => c == "quote").SingleOrDefault();

                //moved the code to get the quotekey
                int quotekey = 0;
                var quotekeyvalue = options.ToList().Where(c => c.Contains("quoteKey")).FirstOrDefault();
                if (!string.IsNullOrEmpty(quotekeyvalue))
                {
                    string[] arrquotekey = quotekeyvalue.Split('|');
                    if (arrquotekey != null && arrquotekey.Length == 2 && !string.IsNullOrEmpty(arrquotekey[1]))
                        int.TryParse(arrquotekey[1], out quotekey);//quotekey = Convert.ToInt32(arrquotekey[1]);
                }

                if (sourceType == "Opportunity" && !string.IsNullOrEmpty(quote))
                {
                    var quotePrintFormat = options.ToList().Where(c => c.Contains("quotePrintFormat")).SingleOrDefault();

                    PrintVersion printVersion = PrintVersion.ExcludeDiscount;

                    if (!string.IsNullOrEmpty(quotePrintFormat))
                    {
                        var temp = quotePrintFormat.Split('=');
                        if (temp.Length > 1 && temp[1] == "IncludeDiscount")
                            printVersion = PrintVersion.IncludeDiscount;
                        else if (temp.Length > 1 && temp[1] == "CondensedVersion")
                            printVersion = PrintVersion.CondensedVersion;
                        else
                            printVersion = PrintVersion.ExcludeDiscount;
                    }

                    if (quotekey == 0)
                    {
                        var oppmgr = new OpportunitiesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                        var opportunity = oppmgr.Get(id);
                        var quotemgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                        var quoteobj = quotemgr.GetQuote(id).Where(q => q.PrimaryQuote == true).FirstOrDefault();
                        if (quoteobj != null) quotekey = quoteobj.QuoteKey;
                    }

                    if (quotekey != 0)
                    {
                        QuoteSheetReport quotesheet = new QuoteSheetReport();
                        var docQuote = quotesheet.createQuotePDFDoc(quotekey, printVersion);
                        doc.Append(docQuote);
                    }

                }
                if (salesPacketOption != "")
                {
                    var salesOptions = salesPacketOption.ToString().Split(new char[] { '/' }).ToList();
                    var sp = GetSalesPacketdocumentsPdf(salesOptions.ToList());
                    if (sp != null)
                        doc.Append(sp);
                }
                var mydocuments = options.Where(c => c.Contains("mydocuments_")).ToList();


                var md = GetMydocumentsPdf(mydocuments);
                if (md != null) doc.Append(md);

                // save pdf document 
                //added the code to remove pdf. instead of sending blank pdf 
                byte[] pdf = null;
                if (doc.Pages.Count != 0)
                {
                    pdf = doc.Save();
                }

                doc.Close();

                //for pdf name in email
                if (sourceType == "Lead")
                    fileDownloadName = string.Format("LeadSheet.pdf");
                else if (sourceType == "Opportunity" && quotekey != 0)
                    fileDownloadName = QuotesMgr.GetQuotebyQuoteKey(quotekey);
                else if (sourceType == "Opportunity")
                    fileDownloadName = OpportunityMgr.GetOppbyOppId(id, IsPacket);

                return pdf;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return null;
            }
        }

        private string GetFileDownloadName(int id, string sourceType, ExportTypeEnum type, bool IsPacket)
        {
            OpportunitiesManager OpportunityMgr = new OpportunitiesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

            using (var dblead = ContextFactory.Create())
            {
                string fileDownloadName = string.Empty;
                if (sourceType == "Lead")
                {
                    var Lead = dblead.Leads.AsNoTracking().SingleOrDefault(l => l.LeadId == id);
                    if (type == ExportTypeEnum.PDF)
                    {
                        //fileDownloadName = string.Format("LeadSheet {0}-{1}.pdf", Lead.LeadId, Lead.LeadNumber);
                        fileDownloadName = string.Format("LeadSheet.pdf");
                    }
                    else if (type == ExportTypeEnum.Word)
                    {
                        fileDownloadName = string.Format("LeadSheet {0}-{1}.docx", Lead.LeadId, Lead.LeadNumber);
                    }
                }
                else if (sourceType == "Opportunity")
                {
                    fileDownloadName = OpportunityMgr.GetOppbyOppId(id, IsPacket);
                    // fileDownloadName = string.Format("LeadSheet-opportunity-{0}.pdf", id);
                }
                else if (sourceType == "Quote")
                {
                    fileDownloadName = QuotesMgr.GetQuotebyQuoteKey(id);
                }
                else
                {
                    fileDownloadName = string.Format("LeadSheet-unknown-{0}.pdf", id);
                }

                return fileDownloadName;
            }
        }

        private PdfDocument GetExistingMeasurements(int id, string existingMeasurements, string sourceType = "Lead")
        {
            if (id == 0 || string.IsNullOrEmpty(existingMeasurements) ||
                sourceType != "Opportunity") return null;

            var result = LeadSheetReport.GetExistingMeasurements(id);
            if (string.IsNullOrEmpty(result)) return null;

            string baseUrl = Serializer.Leads.LeadSheetReport.TemplateImageFilePath();

            //HtmlToPdf converter = new HtmlToPdf();
            var converter = GetPdfConverter(false);
            var doc = converter.ConvertHtmlString(result, baseUrl);
            return doc;
        }

        private HtmlToPdf GetPdfConverter(bool addPageNumber = true)
        {
            // instantiate a html to pdf converter object 
            HtmlToPdf converter = new HtmlToPdf();

            converter.Options.MarginTop = 20;
            converter.Options.MarginBottom = 30;
            converter.Options.MarginLeft = 10;
            converter.Options.MarginRight = 10;

            converter.Options.DisplayHeader = true;
            converter.Header.DisplayOnFirstPage = true;
            converter.Header.DisplayOnOddPages = true;
            converter.Header.DisplayOnEvenPages = true;

            converter.Options.DisplayFooter = true;
            converter.Footer.DisplayOnFirstPage = true;
            converter.Footer.DisplayOnEvenPages = true;
            converter.Footer.DisplayOnOddPages = true;

            converter.Options.PdfPageSize = PdfPageSize.Letter;

            // page numbers can be added using a PdfTextSection object
            if (addPageNumber)
            {
                PdfTextSection text = new PdfTextSection(0, 10, "Page {page_number} of {total_pages}", new System.Drawing.Font("Arial", 8));
                text.HorizontalAlign = PdfTextHorizontalAlign.Right;
                converter.Footer.Add(text);
            }

            return converter;
        }

        private PdfDocument GetLeadSheetPdf(string data)
        {
            if (string.IsNullOrEmpty(data)) return null;

            //HtmlToPdf converter = new HtmlToPdf();
            var converter = GetPdfConverter();

            string baseUrl = Serializer.Leads.LeadSheetReport.TemplateImageFilePath();
            var doc = converter.ConvertHtmlString(data, baseUrl);
            return doc;
        }

        private PdfDocument GetGooglemap(int id, string googleMap, string sourceType = "Lead")
        {
            try
            {
                EventLogger.LogEvent(id + " " + sourceType, "GetGooglemap", LogSeverity.Information);
                if (id == 0 || string.IsNullOrEmpty(googleMap)) return null;

                string result = "";

                if (sourceType == "Lead")
                {
                    result = LeadSheetReport.GetGoogleMapForLead(id);
                }
                else if (sourceType == "Opportunity")
                {
                    result = LeadSheetReport.GetGoogleMapForOpportunity(id);
                }
                else if (sourceType == "Order")
                {
                    result = LeadSheetReport.GetGoogleMapForOpportunity(id);
                }

                if (string.IsNullOrEmpty(result)) return null;

                //HtmlToPdf converter = new HtmlToPdf();

                string baseUrl = Serializer.Leads.LeadSheetReport.TemplateImageFilePath();

                var converter = GetPdfConverter(false);
                converter.Options.MinPageLoadTime = 3;
                var doc = converter.ConvertHtmlString(result, baseUrl);
                return doc;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }
        }

        private PdfDocument GetNotes(int id, string internalNotes
            , string externalNotes, string directionNotes
            , string sourceType = "Lead")
        {
            if (id == 0 || (string.IsNullOrEmpty(internalNotes)
                && string.IsNullOrEmpty(externalNotes)
                && string.IsNullOrEmpty(directionNotes))) return null;

            string result = "";
            if (sourceType == "Lead")
            {
                result = LeadSheetReport.printLeadNotesTP(id
                    , !string.IsNullOrEmpty(internalNotes)
                    , !string.IsNullOrEmpty(externalNotes)
                    , !string.IsNullOrEmpty(directionNotes));
            }
            else if (sourceType == "Opportunity")
            {
                result = LeadSheetReport.printOpportunityNotesTP(id
                    , !string.IsNullOrEmpty(internalNotes)
                    , !string.IsNullOrEmpty(externalNotes)
                    , !string.IsNullOrEmpty(directionNotes));

            }
            else if (sourceType == "Account")
            {
                result = LeadSheetReport.printAccountNotesTP(id
                    , !string.IsNullOrEmpty(internalNotes)
                    , !string.IsNullOrEmpty(externalNotes)
                    , !string.IsNullOrEmpty(directionNotes));

            }
            else if (sourceType == "Order")
            {
                result = LeadSheetReport.printOrderNotesTP(id
                    , !string.IsNullOrEmpty(internalNotes)
                    , !string.IsNullOrEmpty(externalNotes)
                    , !string.IsNullOrEmpty(directionNotes));
            }

            if (string.IsNullOrEmpty(result)) return null;

            // We have notes content, so we can create the pdf document with teh notes content.
            string filename = HttpContext.Current.Server.MapPath("/Templates/base/notes.html");
            var data = System.IO.File.ReadAllText(filename);

            if (string.IsNullOrEmpty(data)) return null;

            data = data.Replace("{{notes}}", result);

            string baseUrl = Serializer.Leads.LeadSheetReport.TemplateImageFilePath();

            var converter = GetPdfConverter(false);
            var doc = converter.ConvertHtmlString(data, baseUrl);
            return doc;
        }

        private PdfDocument GetbbmeasurementForm(string value)
        {
            SourceManager sourcemgr = new SourceManager();

            if (string.IsNullOrEmpty(value)) return null;

            var temp = value.Split('|');
            var count = temp.Length > 1 ? int.Parse(temp[1]) : 0;

            if (count == 0) return null;
            //  string filename = Server.MapPath("/Templates/Base/Brand/BB/BB Measurement Form.pdf");

            var result = sourcemgr.GetStreamIdForDocument(102);
            if (result == null) return null;

            PdfDocument doc = new PdfDocument();

            var data = FileTable.GetFileDataSync(result[0].stream_id.ToString());
            var stream = new MemoryStream(data.bytes);

            for (int index = 0; index < count; index++)
            {
                var obj = new PdfDocument(stream);
                doc.Append(obj);
            }

            return doc;
        }

        private PdfDocument GetccmeasurementForm(string value)
        {

            // TODO: the Pdf is not yet defined;
            //https://hfcprojects.atlassian.net/wiki/spaces/TOUC/pages/84902001/Print+Measurement+Forms+-+CC
            return null;

        }

        private PdfDocument GetgaragemeasurementForm(string value)
        {
            SourceManager sourcemgr = new SourceManager();

            if (string.IsNullOrEmpty(value)) return null;

            var temp = value.Split('|');
            var count = temp.Length > 1 ? int.Parse(temp[1]) : 0;

            if (count == 0) return null;

            //string filename = Server.MapPath("/Templates/Base/Brand/TL/TL 3 Garage Measurement Form.pdf");
            //PdfDocument pdf = new PdfDocument(filename);

            var result = sourcemgr.GetStreamIdForDocument(203);

            if (result == null) return null;


            PdfDocument doc = new PdfDocument();

            var data = FileTable.GetFileDataSync(result[0].stream_id.ToString());
            var stream = new MemoryStream(data.bytes);

            for (int index = 0; index < count; index++)
            {
                var obj = new PdfDocument(stream);
                doc.Append(obj);
            }

            return doc;
        }

        private PdfDocument GetclosetmeasurementForm(string value)
        {
            SourceManager sourcemgr = new SourceManager();

            if (string.IsNullOrEmpty(value)) return null;

            var temp = value.Split('|');
            var count = temp.Length > 1 ? int.Parse(temp[1]) : 0;

            if (count == 0) return null;

            //string filename = Server.MapPath("/Templates/Base/Brand/TL/TL 1 Closet Measurement Form.pdf");
            //PdfDocument pdf = new PdfDocument(filename);

            var result = sourcemgr.GetStreamIdForDocument(201);

            if (result == null) return null;


            PdfDocument doc = new PdfDocument();

            var data = FileTable.GetFileDataSync(result[0].stream_id.ToString());
            var stream = new MemoryStream(data.bytes);


            for (int index = 0; index < count; index++)
            {
                var obj = new PdfDocument(stream);
                doc.Append(obj);
            }

            return doc;
        }

        private PdfDocument GetofficemeasurementForm(string value)
        {
            SourceManager sourcemgr = new SourceManager();

            if (string.IsNullOrEmpty(value)) return null;

            var temp = value.Split('|');
            var count = temp.Length > 1 ? int.Parse(temp[1]) : 0;

            if (count == 0) return null;

            //string filename = Server.MapPath("/Templates/Base/Brand/TL/TL 2 Office Measurement Form.pdf");
            //PdfDocument pdf = new PdfDocument(filename);


            var result = sourcemgr.GetStreamIdForDocument(202);

            if (result == null) return null;

            PdfDocument doc = new PdfDocument();

            var data = FileTable.GetFileDataSync(result[0].stream_id.ToString());
            var stream = new MemoryStream(data.bytes);

            for (int index = 0; index < count; index++)
            {
                var obj = new PdfDocument(stream);
                doc.Append(obj);
            }

            return doc;
        }

        private PdfDocument GetfloormeasurementForm(string value)
        {
            SourceManager sourcemgr = new SourceManager();

            if (string.IsNullOrEmpty(value)) return null;

            var temp = value.Split('|');
            var count = temp.Length > 1 ? int.Parse(temp[1]) : 0;

            if (count == 0) return null;

            //string filename = Server.MapPath("/Templates/Base/Brand/TL/TL 4 Floor Measurement Form.pdf");
            //PdfDocument pdf = new PdfDocument(filename);


            var result = sourcemgr.GetStreamIdForDocument(204);

            if (result == null) return null;


            PdfDocument doc = new PdfDocument();

            var data = FileTable.GetFileDataSync(result[0].stream_id.ToString());
            var stream = new MemoryStream(data.bytes);

            for (int index = 0; index < count; index++)
            {
                var obj = new PdfDocument(stream);
                doc.Append(obj);
            }

            return doc;
        }

        private PdfDocument GetMydocumentsPdf(List<string> values)
        {
            NoteManager noteMgr = new NoteManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

            if (values == null || values.Count == 0) return null;

            PdfDocument doc = new PdfDocument();

            foreach (var item in values)
            {
                if (string.IsNullOrEmpty(item)) continue;

                var temp = item.Split('|');
                var count = temp.Length > 1 ? int.Parse(temp[1]) : 0;

                if (count == 0) continue;

                var temp2 = temp[0].Split('_');
                var noteid = temp2.Length > 1 ? int.Parse(temp2[1]) : 0;
                if (noteid == 0) continue;

                var note = noteMgr.Get(noteid);
                var data = FileTable.GetFileDataSync(note.AttachmentStreamId.ToString());
                var stream = new MemoryStream(data.bytes);

                //string filename = Server.MapPath("/Templates/Base/Brand/TL/TL 4 Floor Measurement Form.pdf");
                // PdfDocument pdf = new PdfDocument(stream);

                for (int index = 0; index < count; index++)
                {
                    var obj = new PdfDocument(stream); //PdfDocument(filename);
                    doc.Append(obj);
                }
            }

            return doc;
        }

        private PdfDocument GetSalesPacketdocumentsPdf(List<string> values)
        {
            if (values == null || values.Count == 0) return null;

            PdfDocument doc = new PdfDocument();

            foreach (var item in values)
            {
                if (string.IsNullOrEmpty(item)) continue;

                var temp = item.Split('|');
                var count = temp.Length > 1 ? int.Parse(temp[1]) : 0;

                if (count == 0) continue;

                var streamId = temp[0];
                //  var noteid = temp2.Length > 1 ? int.Parse(temp2[1]) : 0;
                //  if (noteid == 0) continue;

                // var note = noteMgr.Get(noteid);
                var data = FileTable.GetFileDataSync(streamId.ToString());
                var stream = new MemoryStream(data.bytes);

                //string filename = Server.MapPath("/Templates/Base/Brand/TL/TL 4 Floor Measurement Form.pdf");
                // PdfDocument pdf = new PdfDocument(stream);

                for (int index = 0; index < count; index++)
                {
                    var obj = new PdfDocument(stream); //PdfDocument(filename);
                    doc.Append(obj);
                }
            }

            return doc;
        }

        #endregion
    }
}
