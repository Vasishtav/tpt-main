﻿using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    /// <summary>
    /// Timezones Controller
    /// </summary>
    [RoutePrefix("api/Timezones")]
    public class TimezonesController : BaseController
    {
        /// <summary>
        /// Get TimeZone names
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult Get()
        {
            string[] names = Enum.GetNames(typeof(TimeZoneEnum));
            var values = names.Select(x => new { id = (int)Enum.Parse(typeof(TimeZoneEnum), x), name = x }).Where(y => y.id != 0).ToList();
            return ResponseResult("Success", Json(values));
        }
    }
}
