﻿using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    [RoutePrefix("api/LeadSources")]
    public class LeadSourcesController : BaseController
    {
        /// <summary>
        /// Add New Campaign
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, Route("AddNewCampaign")]
        public IHttpActionResult AddNewCampaign(Campaign model)
        {
            var status = "Invalid data. Save Failed. Please try again";
            if (model != null)
            {
                FranchiseManager FranMgr = new FranchiseManager(SessionManager.CurrentUser);
                if (SessionManager.CurrentFranchise != null)
                {
                    model.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                }
                model.LastUpdatedOn = DateTime.Now;
                model.LastUpdatedBy = SessionManager.CurrentUser.PersonId;
                status = FranMgr.AddNewCampaign(model);
            }
            return ResponseResult(status);
        }

        /// <summary>
        /// Update Campaign
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut, Route("UpdateCampaign")]
        public IHttpActionResult UpdateCampaign(Campaign model)
        {
            var status = "Invalid data. Update Failed. Plese try again.";
            FranchiseManager FranMgr = new FranchiseManager(SessionManager.CurrentUser);
            if (model != null)
            {
                if (SessionManager.CurrentFranchise != null)
                {
                    model.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                }
                model.LastUpdatedOn = DateTime.Now;
                model.LastUpdatedBy = SessionManager.CurrentUser.PersonId;
                status = FranMgr.UpdateCampaign(model);
            }
            return ResponseResult(status);
        }
    }
}
