﻿using HFC.CRM.Cache.Cache;
using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.DTO.Lookup;
using HFC.CRM.DTO.Source;
using HFC.CRM.Managers;
using HFC.CRM.Service.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    [RoutePrefix("api/Lookup")]
    public class LookupController : BaseController
    {
        /// <summary>
        /// Get Lov from common table
        /// </summary>
        /// <param name="tableId"></param>
        /// <returns></returns>
        [HttpGet, Route("Type_LookUpValues/{tableId}")]
        public IHttpActionResult Type_LookUpValues(int tableId)
        {
            try
            {
                LookupManager Lookup_manager = new LookupManager();
                var res = Lookup_manager.Type_LookUpValues(tableId);
                return Json(res);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Type_DateTime
        /// </summary>
        /// <param name="tableId"></param>
        /// <returns></returns>
        [HttpGet, Route("Type_DateTime/{tableId}")]
        public IHttpActionResult Type_DateTime(int tableId)
        {
            try
            {
                LookupManager Lookup_manager = new LookupManager();
                var res = Lookup_manager.Type_DateTime(tableId);
                return Json(res);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Gets list of Dispositions or a speicifc Disposition
        /// </summary>
        /// <param name="dispositionid">Dispoition Id, when 0 all the recoreds returned
        /// otherwise the speicified record</param>
        /// <returns></returns>
        [HttpGet, Route("GetDispositionsTP/{dispositionid}")]
        public IHttpActionResult GetDispositionsTP(int dispositionid)
        {
            try
            {
                LookupManager Lookup_manager = new LookupManager();
                var res = Lookup_manager.GetDispositionsTP(dispositionid);
                return Json(res);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Campaigns
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("Campaigns")]
        public IHttpActionResult Campaigns()
        {
            try
            {
                LookupManager Lookup_manager = new LookupManager();
                var res = Lookup_manager.Campaigns();
                return Json(res);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// SourceTP
        /// </summary>
        /// <param name="CampaignId"></param>
        /// <returns></returns>
        [HttpGet, Route("SourceTp/{CampaignId}")]
        public IHttpActionResult SourceTp(int CampaignId)
        {
            try
            {
                LookupManager Lookup_manager = new LookupManager();
                var res = Lookup_manager.SourceTp(CampaignId);
                return Json(res);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// ReferralType
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Route("ReferralType/{id}")]
        public IHttpActionResult ReferralType(int id)
        {
            try
            {
                LookupManager Lookup_manager = new LookupManager();
                var res = Lookup_manager.ReferralType(id);
                return Json(res);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// ReferralType
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Route("ChannelBySource/{id}")]
        public IHttpActionResult ChannelBySource(int id)
        {
            try
            {
                LookupManager Lookup_manager = new LookupManager();
                var res = Lookup_manager.ChannelBySource(id);
                return Json(res);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Lead Status
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Route("LeadStatus/{id}")]
        public IHttpActionResult LeadStatus(int id)
        {
            StatusChangesManager statusChangesMgr = new StatusChangesManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
            var statuses = SessionManager.CurrentFranchise.LeadStatusTypeCollection();
            var lstStatusId = statusChangesMgr.ValidStatus(1, id);
            var validStatusList = statuses.Where(x => lstStatusId.Contains(x.Id)).ToList();

            var finalStatusValue = validStatusList.Select(s => new { s.Id, s.Name }).ToList();
            finalStatusValue.RemoveAll(x => x.Id == 31422);
            return Json(finalStatusValue);
        }

        /// <summary>
        /// Get LeadStatus Lookup
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetLeadStatusLookup")]
        public IHttpActionResult GetLeadStatusLookup()
        {
            var result = new LookupDTO<ICacheable>()
            {
                Added = CacheManager.LeadStatusTypeCollection.Where(ls => (ls.FranchiseId == null || ls.FranchiseId == SessionManager.CurrentFranchise.FranchiseId)),
            };

            return Json(result);
        }

        /// <summary>
        /// Gets zip code or postal code
        /// </summary>
        /// <param name="zipcode">Search query</param>
        /// <returns></returns>
        [HttpGet, Route("ZipCodes/{zipcode}")]
        public IHttpActionResult ZipCodes(string zipcode)
        {
            try
            {
                LookupManager Lookup_manager = new LookupManager();
                var res = Lookup_manager.ZipCodes(zipcode);
                return Json(new { zipcodes = res });
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Gets the Related Accounts
        /// </summary>
        /// <param name="id">Account Id</param>
        /// <returns></returns>
        [HttpGet, Route("GetRelatedAccounts/{id}")]
        public IHttpActionResult GetRelatedAccounts(int id)
        {
            try
            {
                LookupManager Lookup_manager = new LookupManager();
                var res = Lookup_manager.GetRelatedAccounts(id);
                return Json(res);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Gets the Account status
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("AccountStatus")]
        public IHttpActionResult AccountStatus()
        {
            try
            {
                LookupManager Lookup_manager = new LookupManager();
                var res = Lookup_manager.AccountStatus();
                return Json(res);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Gets the Quick Dispositions
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("QuickDispositions")]
        public IHttpActionResult QuickDispositions()
        {
            List<QuickDispositionVM> result = new List<QuickDispositionVM>();
            result.Add(new QuickDispositionVM
            {
                name = "Qualified - Set Appointment",
                Value = "7|0|1"
            });
            result.Add(new QuickDispositionVM
            {
                name = "Lost Lead - Outside Service Area",
                Value = "2|2|0"
            });
            result.Add(new QuickDispositionVM
            {
                name = "Lost Lead - DIY",
                Value = "2|1|0"
            });
            result.Add(new QuickDispositionVM
            {
                name = "Lost Lead - Incorrect Lead Data",
                Value = "2|3|0"
            });
            result.Add(new QuickDispositionVM
            {
                name = "Lost Lead - Product Scope - Brand",
                Value = "2|4|0"
            });

            result.Add(new QuickDispositionVM
            {
                name = "Lost Lead - Unresponsive",
                Value = "2|7|0"
            });

            result.Add(new QuickDispositionVM
            {
                name = "Lost Lead - Product Scope - Local",
                Value = "2|5|0"
            });
            result.Add(new QuickDispositionVM
            {
                name = "Lost Lead - Cost Gap",
                Value = "2|6|0"
            });
            result.Add(new QuickDispositionVM
            {
                name = "Nurture - Budget Reallocation",
                Value = "5|8|2"
            });
            result.Add(new QuickDispositionVM
            {
                name = "Nurture - Competitive Shopping",
                Value = "5|9|2"
            });
            result.Add(new QuickDispositionVM
            {
                name = "Nurture - Researching",
                Value = "5|10|2"
            });
            result.Add(new QuickDispositionVM
            {
                name = "Nurture - Project Hold",
                Value = "5|11|2"
            });
            result.Add(new QuickDispositionVM
            {
                name = "Nurture - Unresponsive",
                Value = "5|12|2"
            });
            result.Add(new QuickDispositionVM
            {
                name = "Inactive - Bad Data",
                Value = "13|13|0"
            });
            result.Add(new QuickDispositionVM
            {
                name = "Inactive - Duplicate",
                Value = "13|14|0"
            });
            return Json(result);
        }

        /// <summary>
        /// List of Room Location
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetRoomLocation")]
        public IHttpActionResult GetRoomLocation()
        {
            try
            {
                LookupManager Lookup_manager = new LookupManager();
                var res = Lookup_manager.GetRoomLocation();
                return Json(res);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// List of Window Location
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetWindowLocation")]
        public IHttpActionResult GetWindowLocation()
        {
            try
            {
                LookupManager Lookup_manager = new LookupManager();
                var res = Lookup_manager.GetWindowLocation();
                return Json(res);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// List of Mount Type
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetMountType")]
        public IHttpActionResult GetMountType()
        {
            try
            {
                LookupManager Lookup_manager = new LookupManager();
                var res = Lookup_manager.GetMountType();
                return Json(res);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// List of Attendees
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetAttendees")]
        public IHttpActionResult GetAttendees()
        {
            try
            {
                LookupManager lookup_manager = new LookupManager();
                var result = lookup_manager.GetAttendees();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// List of Type Appointments
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("Type_Appointments")]
        public IHttpActionResult Type_Appointments()
        {
            try
            {
                var appointmentDic = new Dictionary<string, byte>();
                foreach (var type in SessionManager.CurrentFranchise.AppointmentTypesCollection())
                {
                    appointmentDic.Add(type.Name, Convert.ToByte(type.AppointmentTypeId));
                }
                return Json(new { AppointmentTypeEnum = appointmentDic });
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }

        }

        /// <summary>
        /// List of Reminders
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("Reminders")]
        public IHttpActionResult Reminders()
        {
            try
            {
                var reminders = new Dictionary<string, int>(16);
                reminders.Add("Disabled", 0);
                reminders.Add("5 minutes", 5);
                reminders.Add("10 minutes", 10);
                reminders.Add("15 minutes", 15);
                reminders.Add("30 minutes", 30);
                reminders.Add("45 minutes", 45);
                reminders.Add("1 hour", 60);
                reminders.Add("2 hours", 120);
                reminders.Add("4 hours", 240);
                reminders.Add("8 hours", 480);
                reminders.Add("1 day", 1440);
                reminders.Add("2 days", 2880);
                reminders.Add("3 days", 4320);
                reminders.Add("4 days", 5760);
                reminders.Add("1 week", 10080);
                reminders.Add("2 weeks", 20160);
                return Json(reminders);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get All Dropdown Values
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetAllDropdownValues")]
        public IHttpActionResult GetAllDropdownValues()
        {
            try
            {
                LookupManager lookup_manager = new LookupManager();
                var result = lookup_manager.GetAllDropdownValues();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get Emails
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetEmails")]
        public IHttpActionResult GetEmails()
        {
            try
            {
                LookupManager lookup_manager = new LookupManager();
                var result = lookup_manager.GetEmails();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get the Product Status
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("ProductStatus")]
        public IHttpActionResult ProductStatus()
        {
            try
            {
                LookupManager lookup_manager = new LookupManager();
                var result = lookup_manager.GetStatus();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get the Vendor Status
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("VendorStatus")]
        public IHttpActionResult VendorStatus()
        {
            try
            {
                LookupManager lookup_manager = new LookupManager();
                var result = lookup_manager.GetVendorStatus();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get the Source Lookup
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetSourceLookup")]
        public IHttpActionResult GetSourceLookup()
        {
            try
            {
                var sw = new Stopwatch();
                sw.Start();

                var list = new List<SourceDTO>();
                var result = new LookupDTO<ICacheable>()
                {
                    Added = list,
                    Took = sw.Elapsed.Milliseconds
                };

                sw.Stop();

                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }

        }

        /// <summary>
        /// Get the Concept Lookup
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetConceptLookup")]
        public IHttpActionResult GetConceptLookup()
        {
            try
            {
                var result = new LookupDTO<ICacheable>()
                {
                    Added = CacheManager.ConceptCollection,
                };

                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }
        /// <summary>
        /// Get the Job Status Lookup
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetJobStatusLookup")]
        public IHttpActionResult GetJobStatusLookup()
        {
            try
            {
                var result = new LookupDTO<ICacheable>()
                {
                    Added = CacheManager.JobStatusTypeCollection.Where(ls => (ls.FranchiseId == null || ls.FranchiseId == SessionManager.CurrentFranchise.FranchiseId)),
                };

                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get All Ship To Locations
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("Type_ShipToLocation")]
        public IHttpActionResult Type_ShipToLocation()
        {
            try
            {
                LookupManager lookup_manager = new LookupManager();
                var result = lookup_manager.Type_ShipToLocation();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Gets a list of territory or specific territory by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Route("Territory/{id}")]
        public IHttpActionResult Territory(int id)
        {
            try
            {
                if (id > 0)
                {
                    var ter = SessionManager.CurrentFranchise.Territories.SingleOrDefault(w => w.TerritoryId == id);
                    return Json(ter != null ? new { Id = ter.TerritoryId, Name = ter.Name } : null);
                }
                else
                    return Json(SessionManager.CurrentFranchise.Territories.Select(s => new { Id = s.TerritoryId, Name = s.Name }));
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }

        }

        /// <summary>
        /// Gets a list of Campaign By Franchise
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetCampaignByFranchise")]
        public IHttpActionResult GetCampaignByFranchise()
        {
            try
            {
                LookupManager lookup_manager = new LookupManager();
                var result = lookup_manager.GetCampaignByFranchise();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Gets a list of all Sources
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetAllSources")]
        public IHttpActionResult GetAllSources()
        {
            try
            {
                LookupManager lookup_manager = new LookupManager();
                var result = lookup_manager.GetAllSources();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get the list of Quote Status or particular status details
        /// </summary>
        /// If id=0, it will return list of Quote Status
        /// If id>0, it will return particular status details 
        /// <returns></returns>
        /// <param name="id">QuoteStatusId</param>
        [HttpGet, Route("GetQuoteStatus/{id}")]
        public IHttpActionResult GetQuoteStatus(int id)
        {
            try
            {
                LookupManager lookup_manager = new LookupManager();
                var result = lookup_manager.GetQuoteStatus(id);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get the list of MyProduct Categories
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetMyProductCategories")]
        public IHttpActionResult GetMyProductCategories()
        {
            try
            {
                LookupManager Lookup_manager = new LookupManager();
                var categories = Lookup_manager.GetMyProductCategories();
                var products = Lookup_manager.GetProdcustTL();
                var discounts = Lookup_manager.GetDiscountTL();

                var disclist = new List<FranchiseProducts>();
                foreach (var item in discounts)
                {
                    var disc = new FranchiseProducts();
                    disc.Discount = item.DiscountValue;
                    disc.Description = item.Description;
                    disc.ProductName = item.Name;
                    disc.ProductID = item.DiscountId;
                    disc.ProductKey = item.DiscountId;
                    disc.ProductType = 4;
                    disc.DiscountType = item.DiscountFactor;
                    products.Add(disc);
                }

                var result = new
                {
                    Categories = categories,
                    Products = products,
                    Discounts = discounts
                };
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get the StorageType
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetStorageType")]
        public IHttpActionResult GetStorageType()
        {
            try
            {
                LookupManager lookup_manager = new LookupManager();
                var result = lookup_manager.GetStorageType();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get the System values
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetSystem")]
        public IHttpActionResult GetSystem()
        {
            var response = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("Closet","Closet"),
                new KeyValuePair<string, string>("Office","Office"),
                new KeyValuePair<string, string>("Garage","Garage"),
                new KeyValuePair<string, string>("Floor","Floor")
            };
            return Json(response);
        }

        /// <summary>
        /// Get the Login User Details
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("LoginUserDetails")]
        public IHttpActionResult LoginUserDetails()
        {
            UserVM user = new UserVM();
            user.FullName = SessionManager.SecurityUser.Person.FullName;
            user.RoleName = SessionManager.SecurityUser.Roles.First().RoleName;
            user.PersonId = SessionManager.SecurityUser.PersonId;
            return Json(user);
        }

        /// <summary>
        /// Get FE Sales Person list
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetFESalesPerson")]
        public IHttpActionResult GetFESalesPerson()
        {
            try
            {
                LookupManager lookup_manager = new LookupManager();
                var result = lookup_manager.GetFESalesPerson();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get the FE Source list
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetFESource")]
        public IHttpActionResult GetFESource()
        {
            try
            {
                LookupManager lookup_manager = new LookupManager();
                var result = lookup_manager.GetFESource();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get Touchpoint Version
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet, Route("GetTPVersion")]
        public IHttpActionResult GetTPVersion()
        {
            var result = "Touchpoint Version " + AppConfigManager.AppVersion;
            return Json(result);
        }
    }
}
