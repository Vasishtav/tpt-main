﻿using System;
using HFC.CRM.Managers;
using HFC.CRM.Core.Common;
using System.Web.Http;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.DTO.PurchaseOrder;
using HFC.CRM.DTO.BulkPurchase;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace HFC.CRM.Service.Controllers
{
    public class PurchaseOrdersController : BaseController
    {
        /// <summary>
        /// Check the MPO Cancelled
        /// </summary>
        /// <param name="id">id</param>
        /// <returns></returns>
        [HttpGet, Route("CheckMPOCancelled/{id}")]
        public IHttpActionResult CheckMPOCancelled(int id)
        {
            try
            {
                PurchaseOrderManager PurchaseOrderMgr = new PurchaseOrderManager(SessionManager.CurrentUser,
                                                                                 SessionManager.CurrentFranchise);
                var result = PurchaseOrderMgr.CheckMPOCancelled(id);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return Json(new { error = ex.Message, data = "" });
            }
        }

        /// <summary>
        /// Get the MPO
        /// </summary>
        /// <param name="id">ID</param>
        /// <returns></returns>
        [HttpGet, Route("GetMPO/{id}")]
        public IHttpActionResult GetMPO(int id)
        {
            MasterPurchaseOrder result = new MasterPurchaseOrder();
            try
            {
                PurchaseOrderManager PurchaseOrderMgr = new PurchaseOrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                result = PurchaseOrderMgr.GetMPO(id);
                return Json(new { error = "", data = result });
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return Json(new { error = ex.Message, data = result });
            }
        }

        /// <summary>
        /// Get the MPO RelatedIds
        /// </summary>
        /// <param name="id">ID</param>
        /// <returns></returns>
        [HttpGet, Route("GetMPORelatedIds/{id}")]
        public IHttpActionResult GetMPORelatedIds(int id)
        {
            try
            {
                PurchaseOrderManager PurchaseOrderMgr = new PurchaseOrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = PurchaseOrderMgr.GetMPORelatedIds(id);
                if (result != null)
                {
                    return Json(new { error = "", data = result });
                }
                else
                {
                    return Json(new { error = "Error occured. Contact Administration.", data = "" });
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return Json(new { error = ex.Message, data = "" });
            }
        }

        /// <summary>
        /// Save the MyProduct Inline VPO
        /// </summary>
        /// <param name="model">VPODetail</param>
        /// <returns></returns>
        [HttpPost, Route("SaveMyProductInlineVPO")]
        public IHttpActionResult SaveMyProductInlineVPO(VPODetail model)
        {
            try
            {
                PurchaseOrderManager PurchaseOrderMgr = new PurchaseOrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var response = PurchaseOrderMgr.SaveMyProductInlineVPO(model);
                var result = PurchaseOrderMgr.GetMPO(model.PurchaseOrderId);
                if (response)
                {
                    return Json(new { error = "", data = result });
                }
                else
                {
                    return Json(new { error = "Error occured. Contact Administration.", data = "" });
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return Json(new { error = ex.Message, data = "" });
            }
        }

        /// <summary>
        /// Get the XMpo
        /// </summary>
        /// <param name="id">ID</param>
        /// <returns></returns>
        [HttpGet,Route("GetXMpo/{id}")]
        public IHttpActionResult GetXMpo(int id)
        {
            XMpoDTO result = new XMpoDTO();
            try
            {
                PurchaseOrderManager PurchaseOrderMgr = new PurchaseOrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                result = PurchaseOrderMgr.GetXmpo(id);
                return Json(new { error = "", data = result });
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return Json(new { error = ex.Message, data = result });
            }
        }

        /// <summary>
        /// Get the SalesOrders By XMpo
        /// </summary>
        /// <param name="id">ID</param>
        /// <returns></returns>
        [HttpGet, Route("GetSalesOrdersByXMpo/{id}")]
        public IHttpActionResult GetSalesOrdersByXMpo(int id)
        {
            // XMpoDTO result = new XMpoDTO();
            IList<OrderBulkPurchaseDTO> result = new List<OrderBulkPurchaseDTO>();
            try
            {
                PurchaseOrderManager PurchaseOrderMgr = new PurchaseOrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                result = PurchaseOrderMgr.GetSalesOrdersByXMpo(id);

                return Json(new { error = "", data = result });
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return Json(new { error = ex.Message, data = result });
            }
        }

        /// <summary>
        /// Save Notes to Vendor
        /// </summary>
        /// <param name="model">model</param>
        /// <returns></returns>
        [HttpPost, Route("SaveNotestoVendor")]
        public IHttpActionResult SaveNotestoVendor(VPODetail model)
        {
            try
            {
                PurchaseOrderManager PurchaseOrderMgr = new PurchaseOrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var response = PurchaseOrderMgr.SaveNotesToVendor(model);

                if (response)
                {
                    return Json(new { error = "" });
                }
                else
                {
                    return Json(new { error = "Error occured. Contact Administration.", data = "" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message, data = "" });
            }
        }

        /// <summary>
        /// Cancel VPO
        /// </summary>
        /// <param name="id">Purchase Order Id</param>
        /// <param name="VPO">VPO Id</param>
        /// <param name="cancelReason">cancel Reason</param>
        /// <returns></returns>
        [HttpGet, Route("CancelVPO")]
        public IHttpActionResult CancelVPO(int id, string VPO, string cancelReason)
        {
            var response = new MasterPurchaseOrder();

            try
            {
                PurchaseOrderManager PurchaseOrderMgr = new PurchaseOrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                if (!string.IsNullOrEmpty(VPO))
                {
                    var vpos = JsonConvert.DeserializeObject<List<int>>(VPO);
                    if (vpos.Count > 0)
                    {
                        foreach (var item in vpos)
                        {
                            var result = PurchaseOrderMgr.CancelVPO(item, id, cancelReason);
                        }
                    }
                }
                response = PurchaseOrderMgr.GetMPO(id);
                return Json(new { error = "", data = response });
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return Json(new { error = ex.Message, data = "" });
            }
        }

        /// <summary>
        /// Update and confirm ship address
        /// </summary>
        /// <param name="model">Model</param>
        /// <returns></returns>
        [HttpPost, Route("AddUpdateShipAddress")]
        public IHttpActionResult AddUpdateShipAddress(MasterPurchaseOrder model)
        {
            try
            {
                PurchaseOrderManager PurchaseOrderMgr = new PurchaseOrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var response = PurchaseOrderMgr.AddUpdateShipAddress(model);
                var result = PurchaseOrderMgr.GetMPO(model.PurchaseOrderId);
                if (response)
                {
                    return Json(new { error = "", data = result });
                }
                else
                {
                    return Json(new { error = "Error occured. Contact Administration.", data = "" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message, data = "" });
            }
        }

        /// <summary>
        /// OnCancel Update Status
        /// </summary>
        /// <param name="model">Model</param>
        /// <returns></returns>
        [HttpPost, Route("OnCancelUpdateStatus")]
        public IHttpActionResult OnCancelUpdateStatus(MasterPurchaseOrder model)
        {
            try
            {
                OrderManager ordermgr = new OrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                PurchaseOrderManager PurchaseOrderMgr = new PurchaseOrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                PurchaseOrderMgr.UpdateMasterPOCancelReason(model);
                PurchaseOrderMgr.CancelMPO(model.PurchaseOrderId);

                //When an mpo/xmpo cancelled if all the orderline items are in Open Status, 
                //the order status also become Open otherwise it it may be in Purchase Order.
                ordermgr.UpdateOrderStatusToOpen(model.OrderId); //7 - Open
                
                var orderData = ordermgr.GetOrderByOrderId(model.OrderId);

                var mpo = PurchaseOrderMgr.GetMPO(model.PurchaseOrderId);

                return Json(new { error = "", data = orderData, MPO = mpo });
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message, data = "" });
            }
        }

        /// <summary>
        /// Submit MPO to PIC
        /// </summary>
        /// <param name="id">order id</param>
        /// <param name="mpoId">mpo id</param>
        /// <returns></returns>
        [HttpGet, Route("SubmitMPO/{id}/{mpoId}")]
        public IHttpActionResult SubmitMPO(int id, int mpoId)
        {
            var response = new MasterPurchaseOrder();
            try
            {
                PurchaseOrderManager PurchaseOrderMgr = new PurchaseOrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = PurchaseOrderMgr.SubmitMPO(id, mpoId);

                if (result != "Success")
                {
                    return Json(new { error = result, data = "" });
                }
                else
                {
                    response = PurchaseOrderMgr.GetMPO(mpoId);
                    return Json(new { error = "", data = response });
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return Json(new { error = ex.Message, data = "" });
            }
        }
    }
}