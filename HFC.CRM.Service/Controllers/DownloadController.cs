﻿using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    /// <summary>
    /// Download files
    /// </summary>
    [RoutePrefix("api/Download")]
    public class DownloadController : BaseController
    {
        /// <summary>
        /// Download file based on Sreamid
        /// </summary>
        /// <param name="streamid"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<HttpResponseMessage> Get(string streamid)
        {
            var data = await FileTable.GetFileDataAsync(streamid);
            if (data != null)
            {
                var result = new HttpResponseMessage(HttpStatusCode.OK);
                var stream = new MemoryStream(data.bytes);// new FileStream(data.physicalfilePath, FileMode.Open);
                result.Content = new StreamContent(stream);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(data.fileName));
                return result;
            }
            else
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }

        }
    }
}
