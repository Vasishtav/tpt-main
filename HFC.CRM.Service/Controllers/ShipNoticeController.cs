﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    [RoutePrefix("api/ShipNotice")]
    public class ShipNoticeController : BaseController
    {
        /// <summary>
        /// Get the Shipment Notice
        /// </summary>
        /// <param name="id">vendorId</param>
        /// <param name="listStatus">Status</param>
        /// <param name="dateFilter">Date</param>
        /// <returns></returns>
        [HttpGet, Route("Get/{id}/{listStatus}/{dateFilter}")]
        public IHttpActionResult Get(int id, string listStatus, string dateFilter)
        {
            try
            {
                ShipNoticeManager snMgr = new ShipNoticeManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                PermissionManager permissionManager = new PermissionManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                List<int> value = new List<int>();

                if (!string.IsNullOrEmpty(listStatus))
                {
                    var v1 = listStatus.Split(',').ToArray().ToList();
                    value = v1.Select(int.Parse).ToList();
                }

                var result = snMgr.Get(id, value, dateFilter);

                var GetPermission = permissionManager.GetUserRolesPermissions();
                var ModuleData = GetPermission.Find(x => x.ModuleCode == "ProcurementDashboard");
                var specialPermission = ModuleData.SpecialPermission.Find(x => x.PermissionCode == "ProcurementDashboardAllRecordAccess").CanAccess;

                if (result != null)
                {
                    if (specialPermission == false)
                    {
                        // Need to apply the filter for SalesAgent/Installation.
                        var filtered = result.Where(x => (x.SalesAgentId ==
                                            SessionManager.CurrentUser.PersonId ||
                                            x.InstallerId == SessionManager.CurrentUser.PersonId)
                                            ).ToList();

                        result = filtered;
                    }
                    return Json(result);
                }
                return OkTP();
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
    }
}