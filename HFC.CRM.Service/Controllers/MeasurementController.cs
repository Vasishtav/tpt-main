﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using HFC.CRM.Managers;
using HFC.CRM.Core.Logs;
using HFC.CRM.Core.Common;
using System.Web;
using HFC.CRM.Data;
using Newtonsoft.Json;

namespace HFC.CRM.Service.Controllers
{
    [RoutePrefix("api/Measurement")]
    public class MeasurementController : BaseController
    {
        /// <summary>
        /// List of Installation Addresses
        /// </summary>
        /// <param name="id">Account Id</param>
        /// <returns></returns>
        [HttpGet, Route("InstallationAddresses/{id}")]
        public IHttpActionResult InstallationAddresses(int id)
        {
            try
            {
                MeasurementManager measurement_manager = new MeasurementManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = measurement_manager.InstallationAddresses(id);
                if (result == null)
                {
                    return Json(new { valid = false, error = "No records found.", data = "" });
                }

                return Json(new { valid = true, error = "", data = result });
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return Json(new { valid = false, error = ex.Message, data = "" });
            }
        }
        /// <summary>
        /// Get the Measurement lines
        /// </summary>
        /// <param name="id">InstalladdressId</param>
        /// <returns></returns>
        [HttpGet, Route("Get/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                int brandId = SessionManager.BrandId;
                MeasurementManager MeasurementMgr = new MeasurementManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = MeasurementMgr.Get(id);
                if (result != null)
                {
                    if (brandId == 1)
                    {
                        var response = result.MeasurementBB;

                        if (response.Count > 0)
                            return Json(response);
                        else
                            return OkTP();
                    }
                    else if (brandId == 2)
                    {
                        var response = result.MeasurementTL;
                        return Json(
                            response
                        );
                    }
                    else if (brandId == 3)
                    {
                        var response = result.MeasurementCC;
                        return Json(
                            response
                        );
                    }
                }
                return OkTP();
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return Json(ex);
            }
        }

        /// <summary>
        /// Post the Measurement lines
        /// </summary>
        /// <remarks>
        /// Notes
        /// 
        ///     dto: {"MeasurementBB": [
        ///     {
        ///         "id": 1,
        ///         "editable": true,
        ///         "WindowLocation": "",
        ///         "RoomLocation": "",
        ///         "RoomName": "",
        ///         "MountTypeName": "",
        ///         "fileName": "",
        ///         "Stream_id": null,
        ///         "thumb_Stream_id": "",
        ///         "QuoteLineId": 0,
        ///         "QuoteLineNumber": 0,
        ///         "isAdded": false,
        ///         "NotSave": false,
        ///         "Width": 0,
        ///         "FranctionalValueWidth": null,
        ///         "Height": 0,
        ///         "FranctionalValueHeight": null,
        ///         "Comments": "test"
        ///     }
        ///     ],
        ///     "InstallationAddressId": 0
        ///     }
        ///     Files: //If there is no attachment file, then you need to pass empty array[]. 
        ///     [
        ///     {
        ///         "id":1,
        ///         "name":"",
        ///         "base64":""
        ///     }]
        /// </remarks>
        /// <returns></returns>
        [HttpPost, Route("Post")]
        public IHttpActionResult Post()
        {
            try
            {
                var httpRequest = HttpContext.Current.Request;
                var dto = httpRequest.Form["dto"];
                var postedFiles = httpRequest.Form["Files"];

                MeasurementHeader model = new MeasurementHeader();
                model = JsonConvert.DeserializeObject<MeasurementHeader>(dto);

                List<measurementfiles> m_files = new List<measurementfiles>();
                if (postedFiles == null) { postedFiles ="[]"; }
                m_files = JsonConvert.DeserializeObject<List<measurementfiles>>(postedFiles);
                MeasurementManager MeasurementMgr = new MeasurementManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                if (model.MeasurementBB != null)
                    foreach (measurementfiles mf in m_files)
                        foreach (MeasurementLineItemBB mb in model.MeasurementBB)
                        {
                            if (mb.id == mf.id)
                            {
                                var liststr = MeasurementMgr.UploadFile(mf);
                                mb.Stream_id = liststr[0];
                                mb.thumb_Stream_id = liststr[1];
                            }
                        }
                if (model.MeasurementTL != null)
                    foreach (measurementfiles mf in m_files)
                        foreach (MeasurementLineItemTL mb in model.MeasurementTL)
                        {
                            if (mb.id == mf.id)
                            {
                                var liststr = MeasurementMgr.UploadFile(mf);
                                mb.Stream_id = liststr[0];
                                mb.thumb_Stream_id = liststr[1];
                            }
                        }
                if (model.MeasurementCC != null)
                    foreach (measurementfiles mf in m_files)
                        foreach (MeasurementLineItemCC mb in model.MeasurementCC)
                        {
                            if (mb.id == mf.id)
                            {
                                var liststr = MeasurementMgr.UploadFile(mf);
                                mb.Stream_id = liststr[0];
                                mb.thumb_Stream_id = liststr[1];
                            }
                        }
                if (model != null)
                {
                    MeasurementMgr.Update(model);

                }
                var result = MeasurementMgr.Get(model.InstallationAddressId.Value);
                int brandId = SessionManager.BrandId;
                if (result != null)
                {
                    if (brandId == 1)
                    {
                        var response = result.MeasurementBB;
                        return Json(
                            response
                        );
                    }
                    else if (brandId == 2)
                    {
                        var response = result.MeasurementTL;
                        return Json(
                            response
                        );
                    }
                    else if (brandId == 3)
                    {
                        var response = result.MeasurementCC;
                        return Json(
                            response
                        );
                    }
                }
                return OkTP();
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return Json(ex);
            }
        }

        /// <summary>
        /// Get the Opportunity Measurement Details
        /// </summary>
        /// <param name="id">Opportunity id</param>
        /// <returns></returns>
        [HttpGet, Route("GetOpportunityMeasurement/{id}")]
        public IHttpActionResult GetOpportunityMeasurement(int id)
        {
            try
            {
                MeasurementManager measurement_manager = new MeasurementManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = measurement_manager.GetOpportunityMeasurement(id);
                if (result == null)
                {
                    return Json(new { valid = false, error = "No records found.", data = "" });
                }
                return Json(new { valid = true, error = "", data = result });
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return Json(new { valid = false, error = ex.Message, data = "" });
            }
        }

        /// <summary>
        /// save the Internal Notes From Configurator
        /// </summary>
        /// <param name="id"></param>
        /// <param name="valuee"></param>
        /// <returns></returns>
        [HttpPost, Route("saveInternalNotesFromConfigurator")]
        public IHttpActionResult saveInternalNotesFromConfigurator(int id, string valuee)
        {
            try 
            {
                MeasurementManager measurement_manager = new MeasurementManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = measurement_manager.saveInternalNotesFromConfigurator(id, valuee);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return Json(new { valid = false, error = ex.Message, data = "" });
            }
        }

        /// <summary>
        /// Save the Measurements From Quote Line To Account
        /// </summary>
        /// <param name="id">QuoteKey</param>
        /// <param name="measurements">measurements</param>
        /// <returns></returns>
        [HttpPost, Route("SaveMeasurementsFromQuoteLineToAccount")]
        public IHttpActionResult SaveMeasurementsFromQuoteLineToAccount(int id, List<MeasurementLineItemBB> measurements)
        {
            try
            {
                MeasurementManager measurement_manager = new MeasurementManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = measurement_manager.SaveMeasurementsFromQuoteLineToAccount(id, measurements);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return Json(new { valid = false, error = ex.Message, data = "" });
            }
        }
    }
}