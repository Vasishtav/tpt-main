﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    /// <summary>
    /// Franchise Settings
    /// </summary>
    [RoutePrefix("api/FranchiseSettings")]
    public class FranchiseSettingsController : BaseController
    {
        /// <summary>
        /// Get Emails Configuration
        /// </summary>
        /// <returns></returns>
        [HttpGet,Route("GetEmailsConfiguration")]
        public IHttpActionResult GetEmailsConfiguration()
        {
            EmailManager emailMgr = new EmailManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var result = emailMgr.GetConfiguredEmails();
            return Json(result);
        }

        /// <summary>
        /// Update Email Configuration
        /// </summary>
        /// <param name="DisplayTerritoryId"></param>
        /// <param name="lstEmailSettings"></param>
        /// <returns></returns>
        [HttpPost,Route("UpdateEmailsConfiguration")]
        public IHttpActionResult UpdateEmailsConfiguration(int DisplayTerritoryId, List<EmailSettings> lstEmailSettings)
        {
            EmailManager emailMgr = new EmailManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var result = emailMgr.UpdateConfiguredEmails(DisplayTerritoryId, lstEmailSettings);
            return Json(result);
        }

        /// <summary>
        /// Get Display TerritoryId
        /// </summary>
        /// <returns></returns>
        [HttpGet,Route("GetDisplayTerritoryId")]
        public IHttpActionResult GetDisplayTerritoryId()
        {
            EmailManager emailMgr = new EmailManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var result = emailMgr.GetDisplayTerritoryId();
            return Json(result);
        }

        /// <summary>
        /// Get BuyerRemorse value in franchise table
        /// </summary>
        /// <returns></returns>
        [HttpGet,Route("BuyerRemorse")]
        public IHttpActionResult BuyerRemorse()
        {
            FranchiseManager _franMgr = new FranchiseManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var value = _franMgr.GetRemorseData();
            return Json(value);
        }

        /// <summary>
        /// Update BuyerRemorse value in franchise table
        /// </summary>
        /// <param name="id">QuoteExpiryDays</param>
        /// <param name="BuyerremorseDay"></param>
        /// <returns></returns>
        [HttpGet,Route("SaveBuyerRemose/{id}/{BuyerremorseDay}")]
        public IHttpActionResult SaveBuyerRemose(int id, int BuyerremorseDay)
        {
            try
            {
                FranchiseManager _franMgr = new FranchiseManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = _franMgr.SaveRemorse(SessionManager.CurrentFranchise.FranchiseId, BuyerremorseDay, id);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

    }
}
