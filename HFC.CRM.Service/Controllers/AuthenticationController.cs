﻿using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Membership;
using HFC.CRM.Data;
using HFC.CRM.DTO;
using HFC.CRM.Managers;
using HFC.CRM.Service.Models;
using Microsoft.Owin.Infrastructure;
using Microsoft.Owin.Security;
using System;
using System.Security.Claims;
using System.Web.Http;
using WebMatrix.WebData;

namespace HFC.CRM.Service.Controllers
{
    /// <summary>
    /// Authentication API
    /// </summary>
    public class AuthenticationController : ApiController
    {
        /// <summary>
        /// Generate Token
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost, Route("Token")]
        public IHttpActionResult Token(LoginViewModel login)
        {
            try
            {
                CookieManager.ImpersonateUserId = null;
                string fullUsername = login.UserName;
                ClaimsIdentity identity;

                // WebSecurity Login - Active diractory Login
                if (WebSecurity.Login(fullUsername, login.Password))
                {
                    // Get User info
                    var aUser = LocalMembership.GetUser(fullUsername, "Person", "Roles", "OAuthUsers");

                    // check if the user is franchise role and the user is suspended or not
                    if (!aUser.IsAdminInRole() && !aUser.IsInRole(AppConfigManager.AppVendorRole.RoleId) && aUser.FranchiseId.HasValue)
                    {
                        var frnManager = new FranchiseManager(aUser);
                        var isSuspended = frnManager.IsSuspended(aUser.FranchiseId.Value);
                        if (isSuspended)
                        {
                            var responce = new LoginAccessViewModel
                            {
                                status = false,
                                message = "Login Suspended, Please contact customer service."
                            };
                            return Ok<LoginAccessViewModel>(responce);
                        }
                    }

                    Franchise franchise = null;

                    // Get Franchise info if the User is franchise user
                    if (aUser.FranchiseId != null && aUser.FranchiseId > 0)
                        franchise = new FranchiseManager(aUser).GetFEinfo((int)aUser.FranchiseId);

                    //Get Logined in User Permissions
                    PermissionManager permissionManager = new PermissionManager(aUser, franchise);
                    var userRolesPermissions = permissionManager.GetUserRolesPermissions();

                    //Initiate OAuth
                    identity = new ClaimsIdentity(Startup.OAuthOptions.AuthenticationType);
                    identity.AddClaim(new Claim(ClaimTypes.Name, fullUsername));

                    var ticket = new AuthenticationTicket(identity, new AuthenticationProperties());
                    var currentUtc = new SystemClock().UtcNow;
                    ticket.Properties.IssuedUtc = currentUtc;
                    ticket.Properties.ExpiresUtc = currentUtc.Add(TimeSpan.FromHours(24));


                    return Ok(new LoginAccessViewModel
                    {
                        access_token = Startup.OAuthOptions.AccessTokenFormat.Protect(ticket),
                        expires_in = 3600,
                        token_type = "Bearer",
                        scope = "api",
                        expires = DateTime.UtcNow.AddHours(1),
                        issued = DateTime.UtcNow,
                        status = true,
                        user = aUser,
                        userPermission = userRolesPermissions,
                        franchise = franchise
                    });
                }
                else
                {
                    var responce = new LoginAccessViewModel
                    {
                        status = false,
                        message = "The user name or password provided is incorrect."
                    };
                    return Ok<LoginAccessViewModel>(responce);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

    }
}
