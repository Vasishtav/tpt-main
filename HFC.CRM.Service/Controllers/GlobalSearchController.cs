﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using HFC.CRM.Managers.DTO;

namespace HFC.CRM.Service.Controllers
{
    /// <summary>
    /// Global Search
    /// </summary>
    [RoutePrefix("api/GlobalSearch")]
    public class GlobalSearchController : BaseController
    {
        /// <summary>
        /// Get Leads
        /// </summary>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpGet, Route("GetLeads")]
        public IHttpActionResult GetLeads(string type, string value)
        {

            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(value))
                return this.BadRequest("Search type or value is missing");

            try
            {
                SearchManagerTP searchManager = new SearchManagerTP(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                List<LeadAccountDTO> result = null;
                switch (type.ToLower())
                {
                    case "email":
                    case "phone":
                    case "name":
                    case "address":
                    case "city":
                    case "all":
                        result = searchManager.GetLeads(type.ToLower(), value);
                        break;
                    default:
                        result = new List<LeadAccountDTO>();
                        break;
                }

                return Json(result);

            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Accounts
        /// </summary>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpGet, Route("GetAccounts")]
        public IHttpActionResult GetAccounts(string type, string value)
        {

            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(value))
                return this.BadRequest("Search type or value is missing");

            try
            {
                SearchManagerTP searchManager = new SearchManagerTP(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                IList<LeadAccountDTO> result = null;

                switch (type.ToLower())
                {
                    case "email":
                    case "phone":
                    case "name":
                    case "address":
                    case "city":
                    case "all":
                        result = searchManager.GetAccounts(type.ToLower(), value);
                        break;
                    default:
                        result = new List<LeadAccountDTO>();
                        break;
                }

                return Json(result);

            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Opportunities
        /// </summary>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpGet, Route("GetOpportunities")]
        public IHttpActionResult GetOpportunities(string type, string value)
        {

            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(value))
                return this.BadRequest("Search type or value is missing");

            try
            {
                SearchManagerTP searchManager = new SearchManagerTP(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                IList<OpportunityDTO> result = null;


                switch (type.ToLower())
                {

                    case "name":
                    case "address":
                    case "city":
                    case "id":
                    case "all":
                        result = searchManager.GetOpportunities(type.ToLower(), value);
                        break;
                    default:
                        result = new List<OpportunityDTO>();
                        break;
                }

                return Json(result);

            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Vendors
        /// </summary>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpGet, Route("GetVendors")]
        public IHttpActionResult GetVendors(string type, string value)
        {

            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(value))
                return this.BadRequest("Search type or value is missing");

            try
            {
                SearchManagerTP searchManager = new SearchManagerTP(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                IList<VendorsDTO> result = null;

                switch (type.ToLower())
                {
                    case "email":
                    case "phone":
                    case "address":
                    case "city":
                    case "id":
                    case "name":
                    case "all":
                        result = searchManager.GetVendors(type.ToLower(), value);
                        break;
                    default:
                        result = new List<VendorsDTO>();
                        break;
                }

                return Json(result);

            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Contacts
        /// </summary>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpGet, Route("GetContacts")]
        public IHttpActionResult GetContacts(string type, string value)
        {

            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(value))
                return this.BadRequest("Search type or value is missing");

            try
            {
                SearchManagerTP searchManager = new SearchManagerTP(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                IList<ContactsDTO> result = null;

                switch (type.ToLower())
                {
                    case "email":
                    case "phone":
                    case "name":
                    case "all":
                        result = searchManager.GetContacts(type.ToLower(), value);
                        break;
                    default:
                        result = new List<ContactsDTO>();
                        break;
                }

                return Json(result);

            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Appointments
        /// </summary>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpGet, Route("GetAppointments")]
        public IHttpActionResult GetAppointments(string type, string value)
        {

            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(value))
                return this.BadRequest("Search type or value is missing");

            try
            {
                SearchManagerTP searchManager = new SearchManagerTP(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                IList<AppointmentDTO> result = null;
                switch (type.ToLower())
                {

                    case "name":
                    case "all":
                        result = searchManager.GetAppointments(type.ToLower(), value);
                        break;
                    default:
                        result = new List<AppointmentDTO>();
                        break;
                }
                return Json(result);

            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Tasks
        /// </summary>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpGet, Route("GetTasks")]
        public IHttpActionResult GetTasks(string type, string value)
        {

            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(value))
                return this.BadRequest("Search type or value is missing");

            try
            {
                SearchManagerTP searchManager = new SearchManagerTP(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                IList<TaskDTO> result = null;
                switch (type.ToLower())
                {
                    case "name":
                    case "all":
                        result = searchManager.GetTasks(type.ToLower(), value);
                        break;
                    default:
                        result = new List<TaskDTO>();
                        break;
                }
                return Json(result);

            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Quotes
        /// </summary>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpGet, Route("GetQuotes")]
        public IHttpActionResult GetQuotes(string type, string value)
        {

            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(value))
                return this.BadRequest("Search type or value is missing");

            try
            {
                SearchManagerTP searchManager = new SearchManagerTP(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                IList<QuoteDTO> result = null;
                switch (type.ToLower())
                {

                    case "name":
                    case "id":
                    case "all":
                        result = searchManager.GetQuotes(type.ToLower(), value);
                        break;
                    default:
                        result = new List<QuoteDTO>();
                        break;
                }
                return Json(result);

            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Orders
        /// </summary>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpGet, Route("GetOrders")]
        public IHttpActionResult GetOrders(string type, string value)
        {

            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(value))
                return this.BadRequest("Search type or value is missing");

            try
            {
                SearchManagerTP searchManager = new SearchManagerTP(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                IList<OrderDTO> result = null;
                switch (type.ToLower())
                {

                    case "name":
                    case "id":
                    case "all":
                        result = searchManager.GetOrders(type.ToLower(), value);
                        break;
                    default:
                        result = new List<OrderDTO>();
                        break;
                }
                return Json(result);

            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Payments
        /// </summary>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpGet, Route("GetPayments")]
        public IHttpActionResult GetPayments(string type, string value)
        {

            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(value))
                return this.BadRequest("Search type or value is missing");

            try
            {
                SearchManagerTP searchManager = new SearchManagerTP(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                IList<PaymentDTO> result = null;
                switch (type.ToLower())
                {
                    case "name":
                    case "id":
                    case "all":
                        result = searchManager.GetPayments(type.ToLower(), value);
                        break;
                    default:
                        result = new List<PaymentDTO>();
                        break;
                }
                return Json(result);

            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Notes
        /// </summary>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpGet, Route("GetNotes")]
        public IHttpActionResult GetNotes(string type, string value)
        {

            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(value))
                return this.BadRequest("Search type or value is missing");

            try
            {
                SearchManagerTP searchManager = new SearchManagerTP(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                IList<NoteDTO> result = null;
                switch (type.ToLower())
                {

                    case "name":
                    case "all":
                        result = searchManager.GetNotes(type.ToLower(), value);
                        break;
                    default:
                        result = new List<NoteDTO>();
                        break;
                }
                return Json(result);

            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Mpo Vpos
        /// </summary>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpGet, Route("GetMpoVpos")]
        public IHttpActionResult GetMpoVpos(string type, string value)
        {

            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(value))
                return this.BadRequest("Search type or value is missing");

            try
            {
                SearchManagerTP searchManager = new SearchManagerTP(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                IList<MpoVpoDTO> result = null;
                switch (type.ToLower())
                {
                    case "id":
                    case "all":
                        result = searchManager.GetMposVpos(type.ToLower(), value);
                        break;
                    default:
                        result = new List<MpoVpoDTO>();
                        break;
                }
                return Json(result);

            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Shipments
        /// </summary>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpGet, Route("GetShipments")]
        public IHttpActionResult GetShipments(string type, string value)
        {

            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(value))
                return this.BadRequest("Search type or value is missing");

            try
            {
                SearchManagerTP searchManager = new SearchManagerTP(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                IList<ShipmentDTO> result = null;
                switch (type.ToLower())
                {
                    case "id":
                    case "all":
                        result = searchManager.GetShipments(type.ToLower(), value);
                        break;
                    default:
                        result = new List<ShipmentDTO>();
                        break;
                }
                return Json(result);

            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Cases
        /// </summary>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpGet, Route("GetCases")]
        public IHttpActionResult GetCases(string type, string value)
        {

            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(value))
                return this.BadRequest("Search type or value is missing");

            try
            {
                SearchManagerTP searchManager = new SearchManagerTP(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                IList<CaseDTO> result = null;
                switch (type.ToLower())
                {
                    case "id":
                    case "all":
                        result = searchManager.GetCases(type.ToLower(), value);
                        break;
                    default:
                        result = new List<CaseDTO>();
                        break;
                }
                return Json(result);

            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Address
        /// </summary>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpGet, Route("GetAddress")]
        public IHttpActionResult GetAddress(string type, string value)
        {

            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(value))
                return this.BadRequest("Search type or value is missing");

            try
            {
                SearchManagerTP searchManager = new SearchManagerTP(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                List<AddressDTO> result = null;
                switch (type.ToLower())
                {
                    case "address":
                    case "city":
                    case "all":
                    case "name":
                        result = searchManager.GetAddresses(type.ToLower(), value);
                        break;
                    default:
                        result = new List<AddressDTO>();
                        break;
                }

                return Json(result);

            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
    }
}
