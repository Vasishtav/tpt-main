﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Extensions;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using Newtonsoft.Json;

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net;
//using System.Net.Http;
//using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    /// <summary>
    /// Case management related operations
    /// </summary>
    public class CaseManagementController : BaseController
    {

        /// <summary>
        /// Returns list of all Franchise name and Id's
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/GetFranchiseList")]
        public IHttpActionResult GetFranchiseList()
        {
            CaseAddEditManager _caseManager = new CaseAddEditManager(GetFranchise(), Getuser());
            var result = _caseManager.GetFranchiseList();

            return Json(result);
        }

        /// <summary>
        /// Returns list of all Vendor name and Id's
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/GetVendorList")]
        public IHttpActionResult GetVendorList()
        {
            CaseAddEditManager _caseManager = new CaseAddEditManager(GetFranchise(), Getuser());
            var result = _caseManager.GetVendorList();

            return Json(result);
        }

        /// <summary>
        /// Returns list of franchise Cases -  FranchiseId and VendorID are optional fields here.
        /// </summary>
        /// <param name="SelectedCaseType"></param>
        /// <param name="Closed"></param>
        /// <param name="FranchiseID">optional - null</param>
        /// <param name="VendorID">optional - null</param>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/GetCaseList/{SelectedCaseType}/{Closed}/{FranchiseID?}/{VendorID?}")]
        public IHttpActionResult GetCaseList(int SelectedCaseType, bool Closed, int? FranchiseID = null , int? VendorID = null)
        {
            CaseAddEditManager _caseManager = new CaseAddEditManager(GetFranchise(), Getuser());
            try
            {
                var result = _caseManager.GetFranchiseCaseList(SelectedCaseType, Closed, FranchiseID, VendorID);               

                if (result != null)
                {
                    foreach (FranchiseCaseList fcl in result)
                    {
                        fcl.CreatedOn = TimeZoneManager.ToLocal(fcl.CreatedOn);
                    }
                    return Json(result);
                }
                else
                    return Json(new List<FranchiseCaseList>());
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Returns the line id's available in a case.
        /// </summary>
        /// <param name="CaseId"></param>
        /// <param name="Module"></param>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/GetlineIdList/{CaseId}/{Module}")]
        public IHttpActionResult GetlineIdList(int CaseId, string Module)
        {
            CaseAddEditManager _caseManager = new CaseAddEditManager(GetFranchise(), Getuser());
            try
            {
                var res = _caseManager.getlineIdList(CaseId, Module);
                return Json(res);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }

            //  return null;
        }


        /// <summary>
        /// Returns the attachment details
        /// </summary>
        /// <param name="CaseId"></param>
        /// <param name="Module"></param>
        /// <param name="flag"></param>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/GetFranchiseCaseDocList/{CaseId}/{Module}/{flag}")]
        public IHttpActionResult GetFranchiseCaseDocList(int CaseId, string Module, bool flag)
        {
            CaseAddEditManager _caseManager = new CaseAddEditManager(GetFranchise(), Getuser());
            try
            {
                var res = _caseManager.getfranchiseCaseDocList(CaseId, Module);

                if (flag == true)
                {
                    var tz = _caseManager.getFranchiseTimeZone(CaseId).TimezoneCode;
                    foreach (var r in res)
                    {
                        if (r.CreatedOn == null)
                        {
                            r.fileSize = r.fileSize.Insert(10, " " + tz.ToString() + " ");
                        }
                        else
                        {
                            DateTime lt = TimeZoneManager.ToLocal((DateTime)r.CreatedOn, tz);
                            r.fileSize = r.fileSize.Substring(10, r.fileSize.Length - 10);
                            r.fileSize = lt.Date.GlobalDateFormat() + " " + tz.ToString() + r.fileSize;
                        }
                    }
                }
                else
                {
                    var tz = _caseManager.getFranchiseTimeZone(CaseId).TimezoneCode;
                    foreach (var r in res)
                    {
                        if (r.CreatedOn != null)
                        {
                            DateTime lt = TimeZoneManager.ToLocal((DateTime)r.CreatedOn, tz);
                            r.fileSize = r.fileSize.Substring(10, r.fileSize.Length - 10);
                            r.fileSize = lt.Date.GlobalDateFormat() + r.fileSize;
                        }
                    }

                }
                return Json(res);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }


        /// <summary>
        /// Returns the user following the case or not
        /// </summary>
        /// <param name="CaseId"></param>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/GetFollowOrNot/{CaseId}")]
        public IHttpActionResult GetFollowOrNot(int CaseId)
        {
            CaseAddEditManager _caseManager = new CaseAddEditManager(GetFranchise(), Getuser());
            try
            {
                var res = _caseManager.GetFollowOrNot(CaseId);
                return Json(res);
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }


        /// <summary>
        /// Returns the case details
        /// </summary>
        /// <param name="CaseId"></param>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/GetCaseDetails/{CaseId}")]
        public IHttpActionResult GetCaseDetails(int CaseId)
        {
            CaseAddEditManager _caseManager = new CaseAddEditManager(GetFranchise(), Getuser());
            try
            {
                var GetDetails = _caseManager.GetDetailes(CaseId);
                return Json(GetDetails);
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }



        /// <summary>
        /// Returns events linked with the case
        /// </summary>
        /// <param name="CaseId"></param>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/GetEventsForCase/{CaseId}")]
        public IHttpActionResult GetEventsForCase(int CaseId)
        {
            try
            {
              //  var manager = new CalendarManager(Getuser());
                var result = new CalendarManager(Getuser()).GetEventsForCase(CaseId);
                return Json(result);
            }            
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }



        /// <summary>
        /// Returns tasks linked with the case
        /// </summary>
        /// <param name="CaseId"></param>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/GetTasksForCase/{CaseId}")]
        public IHttpActionResult GetTasksForCase(int CaseId)
        {
            try
            {
               // var manager = new CalendarManager(Getuser());
                var result = new CalendarManager(Getuser()).GetTasksForCase(CaseId);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Returns history of case
        /// </summary>
        /// <param name="CaseId"></param>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/GetCaseHistoryForCase/{CaseId}")]
        public IHttpActionResult GetCaseHistoryForCase(int CaseId)
        {
            CaseAddEditManager _caseManager = new CaseAddEditManager(GetFranchise(), Getuser());
            try
            {
                var res = _caseManager.GetCaseHistoryForCase(CaseId);

                foreach (HistoryTable ht in res)
                {
                    ht.Date = TimeZoneManager.ToLocal(ht.Date);
                    ht.LastUpdatedOn = TimeZoneManager.ToLocal(ht.LastUpdatedOn);
                }
                return Json(res);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }

        }


        /// <summary>
        /// Returns list of line items are in a case.
        /// </summary>
        /// <param name="CaseId"></param>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/GetCase/{CaseId}")]
        public IHttpActionResult GetCase(int CaseId)
        {
            CaseAddEditManager _caseManager = new CaseAddEditManager(GetFranchise(), Getuser());
            try
            {
                var Value = _caseManager.GetData(0, CaseId);
                if (Value != null)
                {
                    foreach (FranchiseCaseAddInfoModel fcai in Value)
                    {
                        fcai.CreatedOn = TimeZoneManager.ToLocal(fcai.CreatedOn);
                        fcai.LastUpdatedOn = TimeZoneManager.ToLocal(fcai.LastUpdatedOn);
                        //fcai.date = TimeZoneManager.ToLocal(fcai.LastUpdatedOn);
                        //fcai.LastUpdatedOn = TimeZoneManager.ToLocal(fcai.LastUpdatedOn);
                    }
                    return Json(Value);
                }
                else return null;
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }

        }


        /// <summary>
        /// Returns list of comments on page load
        /// </summary>
        /// <param name="id"> </param>
        /// <param name="module"></param>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/LoadInitialComments/{id}/{module}")]
        public IHttpActionResult LoadInitialComments(int id, int module)
        {
            CaseAddEditManager _caseManager = new CaseAddEditManager(GetFranchise(), Getuser());
            try
            {
                var res = _caseManager.loadInitialComments(id, module);
                return Json(res);
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }



        /// <summary>
        /// Returns list of users linked with the case
        /// </summary>
        /// <param name="id"></param>
        /// <param name="module"></param>
        /// <returns></returns>

        [HttpGet, Route("api/CaseManagement/LoadUserList/{id}/{module}")]
        public IHttpActionResult LoadUserList(int id, int module)
        {
            CaseAddEditManager _caseManager = new CaseAddEditManager(GetFranchise(), Getuser());
            try
            {
                var res = _caseManager.LoadUserList(id, module);
                return Json(res);
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }


        /// <summary>
        /// Returns the franchise time zone of case
        /// </summary>
        /// <param name="id">Case Id</param>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/FindFranchiseTimeZone/{id}")]
        public IHttpActionResult FindFranchiseTimeZone(int id)
        {
            CaseAddEditManager _caseManager = new CaseAddEditManager(GetFranchise(), Getuser());
            try
            {
                var res = _caseManager.FindFranchiseTimeZone(id);
                return Json(res);
            }
            catch (Exception e)
            {
                EventLogger.LogEvent(e);
                return this.ResponseResult(e);

            }

        }

        /// <summary>
        /// Post the initial comment or parent comment
        /// </summary>
        /// <param name="module"></param>
        /// <param name="moduleId"></param>
        /// <param name="Comments"></param>
        /// <param name="unique"> Optional - null </param>
        /// <returns></returns>
        [HttpPost, Route("api/CaseManagement/SaveParentComment/{module}/{moduleId}/{unique?}")]
        public IHttpActionResult SaveParentComment( int module, int moduleId, [FromBody]Comments Comments, string unique = null)
        {
            CaseAddEditManager _caseManager = new CaseAddEditManager(GetFranchise(), Getuser());
            try
            {
                var res = _caseManager.SaveParentComment(unique, module, moduleId, Comments);
                return Json(res);
            }
            catch (Exception e)
            {
                EventLogger.LogEvent(e);
                return this.ResponseResult(e);

            }
        }


       /// <summary>
       /// Post comment which is the reply for parent comment
       /// </summary>
       /// <param name="module"></param>
       /// <param name="moduleId"></param>
       /// <param name="Comments"></param>
       /// <param name="unique">Optional - null</param>
       /// <returns></returns>
        [HttpPost, Route("api/CaseManagement/SaveReplyComment/{module}/{moduleId}/{unique?}")]
        public IHttpActionResult SaveReplyComment(  int module, int moduleId, [FromBody]Comments Comments, string unique = null)
        {
            CaseAddEditManager _caseManager = new CaseAddEditManager(GetFranchise(), Getuser());
            try
            {
                var res = _caseManager.SaveReplyComment(unique, module, moduleId, Comments);
                return Json(res);
            }
            catch (Exception e)
            {
                EventLogger.LogEvent(e);
                return this.ResponseResult(e);

            }
        }


        /// <summary>
        /// Remove comment
        /// </summary>
        /// <param name="id"></param>
        /// <param name="parentId">optional - 0 </param>
        /// <returns></returns>
        [HttpPost, Route("api/CaseManagement/DeleteComment/{id}/{parentId?}")]
        public IHttpActionResult DeleteComment(int id, int parentId=0)
        {
            CaseAddEditManager _caseManager = new CaseAddEditManager(GetFranchise(), Getuser());
            try
            {
                var res = _caseManager.DeleteComment(id,parentId);
                return Json(res);
            }
            catch (Exception e)
            {
                EventLogger.LogEvent(e);
                return this.ResponseResult(e);

            }
        }



       /// <summary>
       /// updates parent comment
       /// </summary>
       /// <param name="id">Comment Id</param>
       /// <param name="module"></param>
       /// <param name="moduleId"></param>
       /// <param name="Comments"></param>
       /// <param name="unique">Optional - null</param>
       /// <returns></returns>
        [HttpPost, Route("api/CaseManagement/UpdateParentComment/{id}/{module}/{moduleId}/{unique?}")]
        public IHttpActionResult UpdateParentComment(int id, int module, int moduleId, [FromBody]Comments Comments, string unique = null)
        {
            CaseAddEditManager _caseManager = new CaseAddEditManager(GetFranchise(), Getuser());
            try
            {
                var res = _caseManager.UpdateParentComment(id, unique, module, moduleId, Comments);
                return Json(res);
            }
            catch (Exception e)
            {
                EventLogger.LogEvent(e);
                return this.ResponseResult(e);

            }
        }


        /// <summary>
        /// Returns current user name and id
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/GetUseridAndName")]
        public IHttpActionResult GetUseridAndName()
        {
            
            return Json(new { id = Getuser().PersonId, name = Getuser().UserName });
        }


        /// <summary>
        /// This will stop the user from following the case
        /// </summary>
        /// <param name="CaseId"></param>
        /// <param name="Module">Module number is 4 for Admin</param>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/UnfollowThisCase/{CaseId}/{Module}")]
        public IHttpActionResult UnfollowThisCase(int CaseId, int Module)
        {
            CaseAddEditManager _caseManager = new CaseAddEditManager(GetFranchise(), Getuser());
            try
            {
                var res = _caseManager.UnfollowThisCase(CaseId, Module);
                return Json(res);
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }


        /// <summary>
        /// THis will enable the user to follow the case
        /// </summary>
        /// <param name="CaseId"></param>
        /// <param name="Module">Module number is 4 for Admin</param>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/FollowThisCase/{CaseId}/{Module}")]
        public IHttpActionResult FollowThisCase(int CaseId, int Module)
        {
            CaseAddEditManager _caseManager = new CaseAddEditManager(GetFranchise(), Getuser());
            try
            {
                var res = _caseManager.FollowThisCase(CaseId, Module);
                return Json(res);
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        /// <summary>
        /// Returns the MPO is valid or not cancelled.
        /// </summary>
        /// <param name="PurchaseOrderId"></param>
        /// <returns></returns>

        [HttpGet, Route("api/CaseManagement/CheckMPOCancelled/{PurchaseOrderId}")]
        public IHttpActionResult CheckMPOCancelled(int PurchaseOrderId)
        {
            PurchaseOrderManager PurchaseOrderMgr = new PurchaseOrderManager(Getuser(), GetFranchise());
            try
            {
                var result = PurchaseOrderMgr.CheckMPOCancelled(PurchaseOrderId);

                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message, data = "" });
            }
        }


        /// <summary>
        /// Returns the MPO details
        /// </summary>
        /// <param name="purchaseOrderId"></param>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/GetMPO/{purchaseOrderId}")]
        public IHttpActionResult GetMPO(int purchaseOrderId)
        {
            PurchaseOrderManager PurchaseOrderMgr = new PurchaseOrderManager(Getuser(), GetFranchise());
            MasterPurchaseOrder result = new MasterPurchaseOrder();
            try
            {
                result = PurchaseOrderMgr.GetMPO(purchaseOrderId);
                return Json(result);
            }            
            catch (Exception ex)
            {
                return Json(new { error = ex.Message, data = result });
            }
        }

        /// <summary>
        /// Returns the available states
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/GetState")]
        public IHttpActionResult GetState()
        {
            try
            {
                var result = CacheManager.StatesCollection.Select(s => new { s.StateAbv, s.StateName, s.CountryISO2 });
                return Json(result);
            }            
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Returns Vendor details
        /// </summary>
        /// <param name="VendorId"></param>
        /// <param name="FranchiseID"></param>
        /// <param name="Status"></param>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/VendorDetails/{VendorId}/{FranchiseID}/{Status}")]
        public IHttpActionResult VendorDetails(int VendorId, int FranchiseID, string Status)
        {
            ShippingManager ShippingMgr = new ShippingManager(Getuser(), GetFranchise());
            var ships = ShippingMgr.GetVendor(0, VendorId, FranchiseID, Status);
            return Json(ships);
        }
       

        /// <summary>
        /// Returns vendor statuses
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/VendorStatus")]
        public IHttpActionResult VendorStatus()
        {
            LookupManager Lookup_manager = new LookupManager();
            var result = Lookup_manager.GetVendorStatus();
            return Json(result);
        }


        /// <summary>
        /// Returns products with details
        /// </summary>
        /// <param name="VendorID"></param>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/GetProductValue/{VendorID}")]
        public IHttpActionResult GetProductValue(int VendorID)
        {
            VendorManager VendorMngr = new VendorManager(Getuser(), GetFranchise());
            var result = VendorMngr.GetPrdtValue(VendorID);
            return Json(result);
        }


        /// <summary>
        /// Returns the vendor detils
        /// </summary>
        /// <param name="VendorId"></param>
        /// <param name="FranchiseID"></param>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/GetVendorDetails/{VendorId}/{FranchiseID}")]
        public IHttpActionResult GetVendorDetails(int VendorId, int FranchiseID)
        {
            VendorManager VendorMngr = new VendorManager(Getuser(), GetFranchise());
            var result = VendorMngr.GetLocalvendor(VendorId, FranchiseID);
            return Json(result);

        }


        /// <summary>
        /// Return Case full Details
        /// </summary>
        /// <param name="CaseId"></param>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/GetCaseDetailsForHO/{CaseId}")]
        public IHttpActionResult GetCaseDetailsForHO(int CaseId)
        {
            CaseAddEditManager _caseManager = new CaseAddEditManager(GetFranchise(), Getuser());
            try
            {
                var GetDetails = _caseManager.GetDetailesHO(CaseId);
                return Json(GetDetails);
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get the Franchise Case List
        /// </summary>
        /// <param name="SelectedCaseType">CaseType</param>
        /// <param name="Closed">IsClosed</param>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/GetFranchiseCaseList/{SelectedCaseType}/{Closed}")]
        public IHttpActionResult GetFranchiseCaseList(int SelectedCaseType, bool Closed)
        {
            try
            {
                CaseAddEditManager Case_Mangr = new CaseAddEditManager(SessionManager.CurrentFranchise,
                                                                       SessionManager.CurrentUser);
                PermissionManager permissionManager = new PermissionManager(SessionManager.CurrentUser,
                                                                       SessionManager.CurrentFranchise);
                var result = Case_Mangr.GetFranchiseCaseList(SelectedCaseType, Closed, SessionManager.CurrentFranchise.FranchiseId, null);
                var GetPermission = permissionManager.GetUserRolesPermissions();
                var ModuleData = GetPermission.Find(x => x.ModuleCode == "CaseManagement ");
                var specialPermission = ModuleData.SpecialPermission.Find(x => x.PermissionCode == "CaseManagementAllRecordAccess").CanAccess;
                if (result != null)
                {
                    if (specialPermission == false)
                    {
                        // Need to apply the filter for SalesAgent/Installation.
                        var filtered = result.Where(x => (x.SalesAgentId ==
                                            SessionManager.CurrentUser.PersonId ||
                                            x.InstallerId == SessionManager.CurrentUser.PersonId)
                                            ).ToList();

                        result = filtered;
                    }
                }

                if (result != null)
                {
                    foreach (FranchiseCaseList fcl in result)
                    {
                        fcl.CreatedOn = TimeZoneManager.ToLocal(fcl.CreatedOn);
                    }
                    return Json(result);
                }
                else
                    return Json(new List<FranchiseCaseList>());
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get the Data for Kanban View
        /// </summary>
        /// <param name="SelectedCaseType">Case Type</param>
        /// <param name="Closed">Is Closed</param>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/GetKanbanData/{SelectedCaseType}/{Closed}")]
        public IHttpActionResult GetKanbanData(int SelectedCaseType, bool Closed)
        {
            try
            {
                CaseAddEditManager Case_Mangr = new CaseAddEditManager(SessionManager.CurrentFranchise,
                                                                       SessionManager.CurrentUser);
                var res = Case_Mangr.GetFranchiseKanbanCaseList(SelectedCaseType, Closed, 0);
                foreach (var item in res)
                {
                    item.UserColor = Case_Mangr.GetUserTextColor(SessionManager.CurrentUser.PersonId);
                }
                return Json(res);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get the Saved Value
        /// </summary>
        /// <param name="Id">CaseID</param>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/GetSavedValue/{Id}")]
        public IHttpActionResult GetSavedValue(int Id)
        {
            try
            {
                CaseAddEditManager Case_Mangr = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                var GetDetails = Case_Mangr.GetDetailes(Id);
                return Json(GetDetails);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get the VPO Data for dropdown
        /// </summary>
        /// if id and orderid = null, then get the full list of VPO.
        /// <param name="id">Id</param>
        /// <param name="orderid">orderid</param>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/GetVPOData/{id}/{orderid}")]
        public IHttpActionResult GetVPOData(int? id, int? orderid)
        {
            try
            {
                CaseAddEditManager Case_Mangr = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                var GetDetails = Case_Mangr.GetVPOData(id,orderid);
                return Json(GetDetails);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get the MPO Data for dropdown
        /// </summary>
        /// if orderid = null, then get the full list of MPO.
        /// <param name="id">orderid</param>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/GetMPOData/{id}")]
        public IHttpActionResult GetMPOData(int id)
        {
            try
            {
                CaseAddEditManager Case_Mangr = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                var GetDetails = Case_Mangr.GetMPOData(id);
                return Json(GetDetails);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get Sales Order Data for dropdown
        /// </summary>
        /// if id = null, then get the full list of SalesOrder.
        /// <param name="id">PurchaseOrderId</param>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/GetSalesOrderData/{id}")]
        public IHttpActionResult GetSalesOrderData(int id)
        {
            try
            {
                CaseAddEditManager Case_Mangr = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                var GetDetails = Case_Mangr.GetSalesOrderData(id);
                return Json(GetDetails);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get the list of CaseType
        /// </summary>
        /// <param name="Tableid"></param>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/GetCaseType/{Tableid}")]
        public IHttpActionResult GetCaseType(int Tableid)
        {
            try
            {
                CaseAddEditManager Case_Mangr = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                var GetDetails = Case_Mangr.GetCaseType(Tableid);
                return Json(GetDetails);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get the CaseReason for dropdownlist
        /// </summary>
        /// <param name="Tableid"></param>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/GetCaseReason/{Tableid}")]
        public IHttpActionResult GetCaseReason(int Tableid)
        {
            try
            {
                CaseAddEditManager Case_Mangr = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                var GetDetails = Case_Mangr.GetCaseReason(Tableid);
                return Json(GetDetails);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get quote-line based on option selected to grid
        /// </summary>
        /// <param name="id"></param>
        /// <param name="Mpoid"></param>
        /// <param name="linenos"></param>
        /// <param name="lineNo"></param>
        /// <param name="OrderId"></param>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/GetLineData/{id}/{Mpoid}/{linenos}/{lineNo}/{OrderId}")]
        public IHttpActionResult GetLineData(int id, int Mpoid, string linenos, string lineNo, string OrderId)
        {
            try
            {
                CaseAddEditManager Case_Mangr = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                if (linenos == null) linenos = "0";
                var Value = Case_Mangr.GetLineData(id, Mpoid, linenos, lineNo, OrderId);
                return Json(Value);
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get the list of Case Status for dropdownlist
        /// </summary>
        /// <param name="Tableid"></param>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/GetCaseStatus/{Tableid}")]
        public IHttpActionResult GetCaseStatus(int Tableid)
        {
            try
            {
                CaseAddEditManager Case_Mangr = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                var GetDetails = Case_Mangr.GetCaseStatus(Tableid);
                return Json(GetDetails);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        //
        /// <summary>
        /// After selecting quoteline in grid, fetch value for display
        /// </summary>
        /// <param name="id"></param>
        /// <param name="OrderId"></param>
        /// <param name="POID"></param>
        /// <param name="PODID"></param>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/GetFullDetails/{id}/{OrderId}/{POID}/{PODID}")]
        public IHttpActionResult GetFullDetails(int id, int OrderId, int POID = 0, int PODID = 0)
        {
            try
            {
                CaseAddEditManager Case_Mangr = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                var Value = Case_Mangr.GetOtherLineData(id, OrderId, POID, PODID);
                return Json(Value);
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        /// <summary>
        /// Save the Vendor Case
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, Route("api/CaseManagement/SaveVendorCaseComplete")]
        public IHttpActionResult SaveVendorCaseComplete([FromBody]List<VendorCaseAddInfoModel> model)
        {
            try
            {
                CaseAddEditManager Case_Mangr = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                var result = Case_Mangr.Complete_Convert(model);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// get account, order, and opportunity information
        /// </summary>
        /// <param name="id">CaseId</param>
        /// <returns></returns>
        [HttpGet, Route("api/CaseManagement/getAccOrderOpportunity/{id}")]
        public IHttpActionResult getAccOrderOpportunity(int id)
        {
            try
            {
                CaseAddEditManager Case_Mangr = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                var res = Case_Mangr.getAccOrderOpportunity(id);
                return Json(res);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
    }
}
