﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using HFC.CRM.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    /// <summary>
    /// Accounts
    /// </summary>
    [RoutePrefix("api/AccountsTP")]
    public class AccountsController : BaseController
    {

        /// <summary>
        /// Gets a single Account, will include a lot of Account detail
        /// </summary>
        /// <param name="id">Account Id</param>
        /// <returns></returns>
        [HttpGet, Route("Get/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                AccountsManager AccountMgr = new AccountsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                CommunicationManager communication_Mangr = new CommunicationManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

                var Account = AccountMgr.Get(id);
                if (Account == null)
                {

                    throw new Exception("Account Id is required");
                }
                int totalRecords = 0;

                return
                    Ok(
                        new
                        {
                            Account = Account,
                            FranchiseAddress = SessionManager.CurrentFranchise.Address.ToString(),
                            FranchiseAddressObject = SessionManager.CurrentFranchise.Address,
                            Addresses = AccountMgr.GetAddress(new List<int>() { id }),
                            States = CacheManager.StatesCollection.Select(s => new { s.StateAbv, s.StateName, s.CountryISO2 }),
                            Countries = CacheManager.StatesCollection.Select(s => new { s.Type_Country.ISOCode2Digits, s.Type_Country.Country, s.Type_Country.ZipCodeMask }).Distinct(),
                            PurchaseOrdersCount = totalRecords,
                            SelectedAccountStatus = Account.Type_AccountStatus.Name,
                            selectedCampaign = Account.CampaignName,
                            AccountQuestionAns = Account.AccountQuestionAns,  //Added for Qualification/Question Answers
                            PrimarySource = Account.SourcesList.Where(x => x.IsPrimarySource == true).FirstOrDefault(),
                            SecondarySource = Account.SourcesList.Where(x => x.IsPrimarySource == false).ToList(),
                            FranciseLevelTexting = SessionManager.CurrentFranchise.EnableTexting,
                            TextStatus = communication_Mangr.GetOptingInfo(Account.PrimCustomer.CellPhone)
                        });
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// get account details based on email-id entered in lead
        /// </summary>
        /// <param name="value">email-id</param>
        /// <returns></returns>
        [HttpGet, Route("GetAcctDetails")]
        public IHttpActionResult GetAcctDetails(string value)
        {
            try
            {
                if (value != null)
                {
                    AccountsManager AccountMgr = new AccountsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                    var AcctDet = AccountMgr.GetMatchDetails(value);
                    return Json(AcctDet);
                }
                else return null;
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }

        }

        /// <summary>
        /// get account details based on (Phone-number) entered in lead
        /// </summary>
        /// <param name="value">Phone-number</param>
        /// <returns></returns>
        [HttpGet, Route("GetAcctPhneDetails")]
        public IHttpActionResult GetAcctPhneDetails(string value)
        {
            try
            {
                if (value != null)
                {
                    AccountsManager AccountMgr = new AccountsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                    var AcctDet = AccountMgr.GetMatchPhneDetails(value);
                    return Json(AcctDet);
                }
                else return null;
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }

        }

        /// <summary>
        /// get account details based on Address1 entered in lead
        /// </summary>
        /// <param name="Addrvalue">Address1</param>
        /// <returns></returns>
        [HttpGet, Route("GetAcctAddrDetails")]
        public IHttpActionResult GetAcctAddrDetails(string Addrvalue)
        {
            try
            {
                if (Addrvalue != "")
                {
                    AccountsManager AccountMgr = new AccountsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                    var AcctDet = AccountMgr.GetMatchAddrDetails(Addrvalue);
                    return Json(AcctDet);
                }
                else return null;
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get Territory For Franchise
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("getTerritoryForFranchise")]
        public IHttpActionResult getTerritoryForFranchise()
        {
            try
            {
                AccountsManager AccountMgr = new AccountsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var res = AccountMgr.getTerritoryForFranchise();
                return Json(res);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get Account Status
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetAccountTypeStatus")]
        public IHttpActionResult GetAccountTypeStatus()
        {
            try
            {
                AccountsManager AccountMgr = new AccountsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = AccountMgr.GetAccountType();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get the Account QuoteNames
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="OpportunityId">OpportunityId</param>
        /// <returns></returns>
        [HttpGet, Route("GetAccQuoteNames/{id}/{OpportunityId}")]
        public IHttpActionResult GetAccQuoteNames(int id, int OpportunityId)
        {
            try
            {

                AccountsManager AccountMgr = new AccountsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                string[] res = AccountMgr.GetAccQuoteNames(id, OpportunityId);
                return Json(res);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        /// <summary>
        /// Account add/update
        /// </summary>
        /// <param name="viewmodel"></param>
        /// <returns></returns>
        [HttpPost, Route("SaveAccount")]
        public IHttpActionResult Post(AccountTPViewModel viewmodel)
        {
            AccountTP model = AccountTPViewModel.MapFrom(viewmodel);
            AccountsManager AccountMgr = new AccountsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            CommunicationManager communication_Mangr = new CommunicationManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

            int accountId = 0;
            //texting
            var FranciseLevelTexting = SessionManager.CurrentFranchise.EnableTexting;
            var CountryCode = SessionManager.CurrentFranchise.CountryCode;
            var BrandId = SessionManager.CurrentFranchise.BrandId;
            if (model.PrimCustomer.CellPhone != null)
            {
                var TextStatus = communication_Mangr.GetOptingInfo(model.PrimCustomer.CellPhone);
                if (FranciseLevelTexting && model.IsNotifyText && (TextStatus == null || TextStatus.IsOptinmessagesent == false))
                {
                    if (model.SendText && TextStatus == null)
                        communication_Mangr.SendOptinMessage(model.PrimCustomer.CellPhone, BrandId, CountryCode, null, model.AccountId);
                    else if (model.SendText && TextStatus.IsOptinmessagesent == false)
                        communication_Mangr.ForceOptinMessage(model.PrimCustomer.CellPhone, BrandId, CountryCode, null, model.AccountId);
                }
            }

            //return ResponseResult("Success", Json(new { AccountId = 33 }));

            if (model.SourcesTPIdFromCampaign > 0) model.SourcesTPId = model.SourcesTPIdFromCampaign;

            if (model.AccountId > 0)
            {
                // TP-3904: Lead Created date has changed to past while change the lead status.
                var acct = AccountMgr.GetAccount(model.AccountId);
                model.CreatedOnUtc = acct.CreatedOnUtc;
                model.CreatedByPersonId = acct.CreatedByPersonId;

                var updateStatus = AccountMgr.Update(model);
                return ResponseResult(updateStatus, Json(new { accountId = model.AccountId }));
            }
            if (model.SkipDuplicateCheck == false)
            {

                var dulpicateList = AccountMgr.GetDuplicateAccount(SearchDuplicateAccountEnum.All, string.Empty, model);
                if (dulpicateList.Count > 0)
                {

                    var dtos = dulpicateList.Select(v => new DuplicateOpportunityModel()
                    {
                        Address = v.Addresses.FirstOrDefault() != null ? v.Addresses.FirstOrDefault().Address1 : "",
                        OpportunityId = v.OpportunityId,
                        CellPhone = TelBuilder(v.PrimCustomer.CellPhone),
                        HomePhone = TelBuilder(v.PrimCustomer.HomePhone),
                        FaxPhone = TelBuilder(v.PrimCustomer.FaxPhone),
                        WorkPhone = TelBuilder(v.PrimCustomer.WorkPhone),
                        PrimaryEmail = v.PrimCustomer.PrimaryEmail,
                        SecondaryEmail = v.PrimCustomer.SecondaryEmail,
                        FullName = v.PrimCustomer.FullName,
                        OpportunityNumber = v.AccountNumber
                    }).Take(3);

                    return ResponseResult("Success", Json(new { lstDuplicates = dtos }));
                }
            }

            accountId = AccountMgr.CreateAccount(out accountId, model, 0);

            return ResponseResult("Success", Json(new { accountId = accountId }));
        }

        private string TelBuilder(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return string.Empty;
            }
            str = "(" + str.Insert(3, ")");
            return str.Insert(8, "-");
        }

    }
}
