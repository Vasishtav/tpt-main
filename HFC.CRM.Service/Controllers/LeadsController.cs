﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.DTO.Search;
using HFC.CRM.Managers;
using HFC.CRM.Managers.DTO;
using HFC.CRM.Service.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    /// <summary>
    /// Lead
    /// </summary>
    [RoutePrefix("api/LeadsTP")]
    public class LeadsController : BaseController
    {

        /// <summary>
        /// Get Lead info based in Leadid
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            try
            {
                LeadsManager LeadMgr = new LeadsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                CommunicationManager communication_Mangr = new CommunicationManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

                var lead = LeadMgr.Get(id);

                if (lead != null && lead.FranchiseId != SessionManager.CurrentFranchise.FranchiseId)
                {
                    throw new AccessViolationException("Sorry, you do not have permission");
                }

                return
                    Ok(
                        new
                        {
                            Lead = lead,
                            //LeadPrimaryNotes = lead.LeadNotes.FirstOrDefault(n => n.TypeEnum == NoteTypeEnum.Primary),

                            FranchiseAddress = SessionManager.CurrentFranchise.Address.ToString(),
                            FranchiseAddressObject = SessionManager.CurrentFranchise.Address,
                            Addresses = LeadMgr.GetAddressTP(id),  //LeadMgr.GetAddress(new List<int>() { id }),
                            States = CacheManager.StatesCollection.Select(s => new { s.StateAbv, s.StateName, s.CountryISO2 }),
                            Countries = CacheManager.StatesCollection.Select(s => new { s.Type_Country.ISOCode2Digits, s.Type_Country.Country, s.Type_Country.ZipCodeMask }).Distinct(),

                            NoteTypeEnum = NoteTypeEnum.Internal.ToDictionary<byte>((byte)NoteTypeEnum.Primary),

                            SelectedLeadStatus = lead.LeadStatus.Name,
                            selectedCampaign = lead.CampaignName,

                            PrimarySource = lead.SourcesList.Where(x => x.IsPrimarySource == true).FirstOrDefault(),
                            SecondarySource = lead.SourcesList.Where(x => x.IsPrimarySource == false).ToList(),
                            FranciseLevelTexting = SessionManager.CurrentFranchise.EnableTexting,
                            TextStatus = communication_Mangr.GetOptingInfo(lead.PrimCustomer.CellPhone)
                        });
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Save Lead
        /// </summary>
        /// <param name="viewmodel"></param>
        /// <returns></returns>
        [HttpPost,Route("SaveLead")]
        public IHttpActionResult Post([FromBody]LeadViewModel viewmodel)
        {
            var model = LeadViewModel.MapFrom(viewmodel);
            LeadsManager LeadMgr = new LeadsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            CommunicationManager communication_Mangr = new CommunicationManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            
            int leadId = 0;
            var ld = LeadMgr.GetLeadByLeadId(model.LeadId);
            //texting
            var FranciseLevelTexting = SessionManager.CurrentFranchise.EnableTexting;
            var CountryCode = SessionManager.CurrentFranchise.CountryCode;
            var BrandId = SessionManager.CurrentFranchise.BrandId;
            if (model.PrimCustomer.CellPhone != null)
            {
                var TextStatus = communication_Mangr.GetOptingInfo(model.PrimCustomer.CellPhone);
                if (FranciseLevelTexting && model.IsNotifyText && (TextStatus == null || TextStatus.IsOptinmessagesent == false))
                {
                    if (model.SendText && TextStatus == null)
                        communication_Mangr.SendOptinMessage(model.PrimCustomer.CellPhone, BrandId, CountryCode, model.LeadId, null);
                    else if (model.SendText && TextStatus.IsOptinmessagesent == false)
                        communication_Mangr.ForceOptinMessage(model.PrimCustomer.CellPhone, BrandId, CountryCode, model.LeadId, null);
                }
            }
            //check for saving empty cell phone
            if (ld != null)
            {
                model.LeadGuid = ld.LeadGuid;
            }

            if (model.SourcesTPIdFromCampaign > 0) model.SourcesTPId = model.SourcesTPIdFromCampaign;

            if (model.LeadId > 0)
            {
                // TP-3904: Lead Created date has changed to past while change the lead status.
                // The created on date should not be modifed by based what sent from the UI.
                model.CreatedOnUtc = ld.CreatedOnUtc;
                model.CreatedByPersonId = ld.CreatedByPersonId;

                var updateStatus = LeadMgr.Update(model);
                return ResponseResult(updateStatus, Json(new { LeadId = model.LeadId }));
            }
            if (model.SkipDuplicateCheck == false)
            {
                var dulpicateList = LeadMgr.GetDuplicateLeads(SearchDuplicateLeadEnum.All, string.Empty, model);
                if (dulpicateList.Count > 0)
                {
                    var dtos = dulpicateList.Select(v => new DuplicateLeadModel()
                    {
                        Address = v.Addresses.FirstOrDefault() != null ? v.Addresses.FirstOrDefault().Address1 : "",
                        LeadId = v.LeadId,
                        CellPhone = TelBuilder(v.PrimCustomer.CellPhone),
                        HomePhone = TelBuilder(v.PrimCustomer.HomePhone),
                        FaxPhone = TelBuilder(v.PrimCustomer.FaxPhone),
                        WorkPhone = TelBuilder(v.PrimCustomer.WorkPhone),
                        PrimaryEmail = v.PrimCustomer.PrimaryEmail,
                        SecondaryEmail = v.PrimCustomer.SecondaryEmail,
                        FullName = v.PrimCustomer.FullName,
                        LeadNumber = v.LeadNumber
                    }).Take(3);

                    return ResponseResult("Success", Json(new { lstDuplicates = dtos }));
                }
            }

            var status = LeadMgr.CreateLead(out leadId, model);

            //Sending email - Thanks Email After save Lead
            //SendEmail(leadId, 1);

            return ResponseResult(status, Json(new { LeadId = leadId }));
        }

        private string TelBuilder(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return string.Empty;
            }
            str = "(" + str.Insert(3, ")");
            return str.Insert(8, "-");
        }

        /// <summary>
        /// Convert Select Lead
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut, Route("ConvertSelectLead")]
        public IHttpActionResult ConvertSelectLead([FromBody]ConvertLead request)
        {
            try
            {
                AccountsManager AccountMgr = new AccountsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                OpportunitiesManager OpportunityMgr = new OpportunitiesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

                int brandId = (int)SessionManager.BrandId;
                int newaccountId = 0;
                int sourceAccountId = 0;
                if (request.selectedaccountid > 0)
                {
                    // merge lead to existing account
                    AccountMgr.MergeLeadToAccount(brandId, request.leadId, request.selectedaccountid, "");
                    sourceAccountId = request.selectedaccountid;
                }
                else
                {
                    // create new account for the lead
                    newaccountId = AccountMgr.ConvertLeadToAccount(out newaccountId, brandId, request.leadId, "");
                    sourceAccountId = newaccountId;
                }

                if (request.addopportunity > 0 && (request.selectedaccountid > 0 || newaccountId > 0))
                {
                    // add opportunity for the account
                    OpportunityMgr.ConvertLeadToOpportunity(brandId, request.leadId, ""
                        , request.opportunityName, request.SalesAgentId, sourceAccountId);
                }

                var status = "Success";
                return ResponseResult(status, Json(new { status, newaccountId }));
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }


        /// <summary>
        /// Validate Duplicate Lead
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, Route("ValidateDuplicateLeadTP")]
        public IHttpActionResult ValidateDuplicateLeadTP([FromBody]LeadDuplicationDTO model)
        {
            LeadsManager LeadMgr = new LeadsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var result = LeadMgr.ValidateLeadDuplication(model);
            return Json(result);
        }

        /// <summary>
        /// Get Email Status
        /// </summary>
        /// <param name="leadid"></param>
        /// <returns></returns>
        [HttpGet, Route("GetEmailStatus/{leadid}")]
        public IHttpActionResult GetEmailStatus(int leadid)
        {
            try
            {
                LeadsManager LeadMgr = new LeadsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = LeadMgr.GetLeadEmail(leadid);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Send Email to Lead
        /// </summary>
        /// <param name="leadid"></param>
        /// <returns></returns>
        [HttpGet, Route("SendEmailToLead/{leadid}")]
        public IHttpActionResult SendEmailToLead(int leadid)
        {
            try
            {
                LeadsManager LeadMgr = new LeadsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var lead = LeadMgr.Get(leadid); var result = "";
                if (lead.IsNotifyemails == true)
                {
                    result = LeadMgr.SendThankYouEmail(leadid, EmailType.ThankyouInquiry); //, 1);
                    return Json(result);
                }
                else
                {
                    result = "Email option is not enabled";
                }

                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Customer Lead Update
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, Route("CustomerLeadUpdate")]
        public IHttpActionResult CustomerLeadUpdate(int id, [FromBody]QuickDispositionVM model)
        {
            LeadsManager LeadMgr = new LeadsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            string[] splitValue = model.Value.Split('|');
            var leadStatusId = Convert.ToInt16(splitValue[0]);
            var dispositionId = Convert.ToInt16(splitValue[1]);
            var status = LeadMgr.UpdateLead(id, leadStatusId, dispositionId);
            return ResponseResult(status);
        }

    }
}
