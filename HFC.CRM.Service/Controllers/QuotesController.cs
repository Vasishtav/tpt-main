﻿using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.DTO.Opportunity;
using HFC.CRM.Managers;
using HFC.CRM.Serializer;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    [RoutePrefix("api/Quotes")]
    public class QuotesController : BaseController
    {
        /// <summary>
        /// Get All Quote
        /// </summary>
        /// <param name="quoteStatusListValue"></param>
        /// <param name="dateRangeValue"></param>
        /// <param name="PrimaryQuote"></param>
        /// <returns></returns>
        [HttpGet, Route("GetAllQuote")]
        public IHttpActionResult GetAllQuote(string quoteStatusListValue, string dateRangeValue, bool PrimaryQuote = false)
        {
            QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            PermissionManager permissionManager = new PermissionManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var result = QuotesMgr.GetAllQuote(quoteStatusListValue, dateRangeValue, PrimaryQuote);

            if (result != null)
            {
                var GetPermission = permissionManager.GetUserRolesPermissions();
                var ModuleData = GetPermission.Find(x => x.ModuleCode == "Quote");
                var specialPermission = ModuleData.SpecialPermission.Find(x => x.PermissionCode == "QuotesAllRecordAccess").CanAccess;
                if (specialPermission == false)
                {
                    var filtered = result.Where(x => (x.SalesAgentId ==
                                       SessionManager.CurrentUser.PersonId ||
                                       x.InstallerId == SessionManager.CurrentUser.PersonId)
                                       ).ToList();
                    result = filtered;
                }
                return Json(result);
            }
            return OkTP();
        }

        /// <summary>
        /// Get Quote List by opportunity Id
        /// </summary>
        /// <param name="id">opportunity Id</param>
        /// <returns></returns>
        [HttpGet, Route("GetList/{id}")]
        public IHttpActionResult GetList(int id)
        {
            QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            OpportunitiesManager oppMgr = new OpportunitiesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var result = QuotesMgr.GetQuote(id);
            var opportunity = oppMgr.Get(id);
            if (result != null)
            {
                return Json(new { quoteList = result, Opportunities = opportunity });
            }
            return OkTP();
        }

        /// <summary>
        /// Get Quote Details
        /// </summary>
        /// <param name="id">Quote Key</param>
        /// <param name="oppurtunityId"></param>
        /// <returns></returns>
        [HttpGet, Route("GetDetails/{id}/{oppurtunityId}")]
        public IHttpActionResult GetDetails(int id, int oppurtunityId)
        {
            QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            if (id == 0)
            {
                var result = QuotesMgr.NewQuote(id, oppurtunityId);
                if (result != null)
                {
                    return Json(
                        result
                    );
                }
            }
            else
            {
                var result = QuotesMgr.GetDetails(id);
                if (result != null)
                {
                    return Json(
                        result
                    );
                }
            }

            return OkTP();
        }

        /// <summary>
        /// Copy Quote
        /// </summary>
        /// <param name="id">Opportunityid</param>
        /// <param name="quoteKey">quoteKey</param>
        /// <param name="copyTaxExempt">copyTaxExempt</param>
        /// <returns></returns>
        [HttpGet, Route("CopyQuote")]
        public IHttpActionResult CopyQuote(int id, int quoteKey, bool copyTaxExempt)
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var res = QuotesMgr.CopyQuote(quoteKey, copyTaxExempt);
                var data = QuotesMgr.GetQuote(id);

                return Json(new { data = data, error = "" });
            }
            catch (Exception ex)
            {
                return Json(new { data = "", error = ex.Message });
            }
        }

        /// <summary>
        /// Get the Quote Lines
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Route("GetQuoteLines/{id}")]
        public IHttpActionResult GetQuoteLines(int id)
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var res = QuotesMgr.GetQuoteLine(id);
                var discount = QuotesMgr.GetDiscountAndPromo(id);
                return Json(new { quoteLines = res, discountAndPromo = discount });
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get the Quote And Measurement Details
        /// </summary>
        /// <param name="id"></param>
        /// <param name="poId"></param>
        /// <returns></returns>
        [HttpGet, Route("GetQuoteAndMeasurementDetails/{id}/{poId}")]
        public IHttpActionResult GetQuoteAndMeasurementDetails(int id, int poId = 0)
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var res = QuotesMgr.GetQuoteDetailsforCoreConfiguration(id);
                var QuoteLinesVM = QuotesMgr.GetQuoteLine(id);
                if (poId != 0)
                {
                    PurchaseOrderManager poMgr = new PurchaseOrderManager();
                    var poDetails = poMgr.GetPurchaseOrderDetailsLines(poId);
                    if (poDetails != null && poDetails.Count > 0)
                    {
                        foreach (var item in poDetails)
                        {
                            QuoteLinesVM.Where(x => x.QuoteLineId == item.QuoteLineId).FirstOrDefault().ConfigErrors = item.Errors;
                            if (item.PICPO.HasValue)
                            {
                                QuoteLinesVM.Where(x => x.QuoteLineId == item.QuoteLineId).FirstOrDefault().PICPO = item.PICPO.Value; ;
                            }
                        }
                    }
                }
                if (QuoteLinesVM != null && QuoteLinesVM.Count > 0)
                {
                    res.QuoteLinesVM = QuoteLinesVM.Where(x => x.ProductTypeId == 1).ToList();
                }
                List<MeasurementLineItemBB> mh = new List<MeasurementLineItemBB>();
                if (res.Measurements != null)
                    mh = JsonConvert.DeserializeObject<List<MeasurementLineItemBB>>(res.Measurements);

                foreach (var qq in mh)
                {
                    qq.isAdded = true;
                }

                if (res.QuoteLinesVM != null)
                    foreach (var q in res.QuoteLinesVM)
                    {
                        //if((mh.Where(x => x.Height == q.Height.ToString() && x.Width == q.Width.ToString() && x.FranctionalValueHeight == q.FranctionalValueHeight &&
                        //x.FranctionalValueWidth == q.FranctionalValueWidth).FirstOrDefault()) == null)
                        if (Convert.ToInt32(q.Width) > 0 || Convert.ToInt32(q.Height) > 0)
                        {
                            //if (q.MeasurementsetId == null || q.MeasurementsetId == 0)
                            //{
                            MeasurementLineItemBB mbb = new MeasurementLineItemBB();
                            mbb.id = 0;
                            mbb.RoomLocation = null;// q.RoomLocation;
                            mbb.RoomName = q.RoomName;
                            mbb.WindowLocation = null;// q.WindowLocation;
                            mbb.MountTypeName = q.MountType;
                            mbb.fileName = "";
                            mbb.Stream_id = "";
                            mbb.QuoteLineId = 0;
                            //mbb.QuoteLineNumber = q.QuoteLineNumber;
                            mbb.isAdded = false;
                            mbb.Width = q.Width.ToString();
                            mbb.FranctionalValueWidth = q.FranctionalValueWidth.ToString();
                            mbb.Height = q.Height.ToString();
                            mbb.FranctionalValueHeight = q.FranctionalValueHeight;

                            mh.Add(mbb);
                        }
                    }
                foreach (var it in mh)
                {
                    it.FranctionalValueHeight = !string.IsNullOrEmpty(it.FranctionalValueHeight) ? it.FranctionalValueHeight : "";
                    it.FranctionalValueWidth = !string.IsNullOrEmpty(it.FranctionalValueWidth) ? it.FranctionalValueWidth : "";
                }
                var mhlist = (from m in mh
                                  //orderby m.RoomName, m.Height, m.FranctionalValueHeight, m.Width, m.FranctionalValueWidth, m.MountTypeName
                              group m by new { m.RoomName, m.Height, m.FranctionalValueHeight, m.Width, m.FranctionalValueWidth, m.MountTypeName } into m1
                              select m1.First()).ToList();
                res.Measurements = JsonConvert.SerializeObject(mhlist);
                return Json(res);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get the Vendor Names
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("getVendorNames")]
        public IHttpActionResult getVendorNames()
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var res = QuotesMgr.getVendorNames();
                return Json(res);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return Json(new { data = "", error = ex.Message });
            }
        }

        /// <summary>
        /// Get the My Products or Service Discount
        /// </summary>
        /// <param name="productType"></param>
        /// <returns></returns>
        [HttpGet, Route("GetMyProductsOrServiceDiscount/{productType}")]
        public IHttpActionResult GetMyProductsOrServiceDiscount(int productType)
        {
            try
            {
                var res = "";
                var prdType = new List<int>();
                if (productType == 2)
                {
                    prdType.Add(2);
                }
                else
                {
                    prdType.Add(3);
                    prdType.Add(4);
                }
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                res = QuotesMgr.GetMyProductsOrServiceDiscount(prdType);
                return Json(new { data = res, error = "" });
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return Json(new { data = "", error = ex.Message });
            }
        }

        /// <summary>
        /// Save the My Vendor Configuration List
        /// </summary>
        /// <param name="id">quoteKey</param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        [HttpPost, Route("SaveMyVendorConfigurationList")]
        public IHttpActionResult SaveMyVendorConfigurationList(int id, [FromBody]List<MyVendorProductVM> configuration)
        {
            try
            {
                foreach (var qq in configuration)
                {
                    if (qq.ProductTypeId == 2)
                        qq.ProductCategoryId = 99997;
                    if (qq.ProductTypeId == 3)
                        qq.ProductCategoryId = 99998;
                    if (qq.ProductTypeId == 4)
                        qq.ProductCategoryId = 99999;
                }
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var res = QuotesMgr.InsertBatchMyProductConfiguration(id, configuration);

                if (res)
                {
                    return Json(new { data = "Successful", error = "" });
                }
                else
                {
                    return Json(new { data = "", error = "Failed to Save." });
                }
            }
            catch (Exception e)
            {
                EventLogger.LogEvent(e);
                return Json(new { data = "", error = e.Message });
            }
        }

        /// <summary>
        /// Get the MyProducts
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetMyProducts")]
        public IHttpActionResult GetMyProducts()
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var res = QuotesMgr.GetMyProducts();
                return Json(new { data = res, error = "" });
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return Json(new { data = "", error = ex.Message });
            }
        }

        /// <summary>
        /// Get the Quote Line By Quote Line Id
        /// </summary>
        /// <param name="quoteLineId"></param>
        /// <returns></returns>
        [HttpGet, Route("GetQuoteLineByQuoteLineId/{quoteLineId}")]
        public IHttpActionResult GetQuoteLineByQuoteLineId(int quoteLineId)
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var res = QuotesMgr.GetQuoteLineByQuoteLineId(quoteLineId);
                return Json(new { data = res, error = "" });
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return Json(new { data = "", error = ex.Message });
            }
        }

        /// <summary>
        /// Save the My Vendor Configuration
        /// </summary>
        /// <param name="id"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        [HttpPost, Route("SaveMyVendorConfiguration")]
        public IHttpActionResult SaveMyVendorConfiguration(int id, [FromBody]MyVendorProductVM configuration)
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var oldQuoteLine = QuotesMgr.getQuoteLineWithGroupDiscount(configuration.QuotelineId);
                var res = QuotesMgr.UpdateQuoteConfiguration(configuration);

                if (res == "Success")
                {
                    QuotesMgr.ApplyGroupDiscount(oldQuoteLine.QuoteKey);
                    QuotesMgr.RefreshGroupDiscountTable(oldQuoteLine.QuoteKey);
                    return Json(new { data = "Sucessful", error = "" });
                }
                else
                {
                    return Json(new { data = "", error = "Failed to Save." });
                }
            }
            catch (Exception e)
            {
                EventLogger.LogEvent(e);
                return Json(new { data = "", error = e.Message });
            }
        }

        /// <summary>
        /// Copy or Delete QuoteLine
        /// </summary>
        /// <param name="id">quoteLineId</param>
        /// <param name="copyorDelete">'{"copyOrDelete":"COPY","QuoteKey":0}'</param>
        /// <returns></returns>
        [HttpPost, Route("CopyDeleteQuoteLine")]
        public IHttpActionResult CopyDeleteQuoteLine(int id, [FromBody]string copyorDelete)
        {
            try
            {
                var quotelineobj = new
                {
                    QuoteKey = 0,
                    copyOrDelete = ""
                };

                var obj = JsonConvert.DeserializeAnonymousType(copyorDelete, quotelineobj);
                var qlds = new List<int>();
                qlds.Add(id);
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var res = QuotesMgr.CopyDeleteQuoteline(qlds, obj.copyOrDelete);
                var response = QuotesMgr.RecalculateQuoteLines(obj.QuoteKey);
                if (obj.copyOrDelete == "DELETE")
                    QuotesMgr.RefreshGroupDiscountTable(obj.QuoteKey);

                var quote = QuotesMgr.GetDetails(obj.QuoteKey);

                if (res)
                {
                    var result = QuotesMgr.GetQuoteLine(obj.QuoteKey);
                    return Json(new { data = result, error = "", quote = quote });
                }
                else
                {
                    return Json(new { data = "", error = "Failed to Save." });
                }
            }
            catch (Exception e)
            {
                EventLogger.LogEvent(e);
                return Json(new { data = "", error = e.Message });
            }
        }

        /// <summary>
        /// Update Quote Note Lines
        /// </summary>
        /// <param name="quoteVM"></param>
        /// <returns></returns>
        [HttpPost, Route("UpdateQuoteNoteLines")]
        public IHttpActionResult UpdateQuoteNoteLines(QuoteLinesVM quoteVM)
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = QuotesMgr.UpdateQuoteLineNotes(quoteVM);

                return OkTP();
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return Json(new { data = "", error = ex.Message });
            }
        }

        /// <summary>
        /// Change the Primary Quote
        /// </summary>
        /// <param name="id">quoteKey</param>
        /// <param name="opportunityId">opportunityId</param>
        /// <returns></returns>
        [HttpGet, Route("ChangePrimaryQuote")]
        public IHttpActionResult ChangePrimaryQuote(int id, int opportunityId)
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var checkorder = QuotesMgr.CheckOrderExisitbyOpportunity(opportunityId);
                if (checkorder != null && checkorder.QuoteKey != id)
                {
                    return Json(new { data = "", error = "Quote \"" + checkorder.QuoteName + "\" is already converted to Sales Order." });
                }

                var response = QuotesMgr.UpdatePrimaryQuote(opportunityId, id);
                if (response)
                {
                    return Json(new { data = "Sucessful", error = "" });
                }
                else
                {
                    return Json(new { data = "", error = "Error changing the primary quote." });
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return Json(new { data = "", error = ex.Message });
            }
        }

        /// <summary>
        /// Move the Quote to another opportunity
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, Route("MoveQuote")]
        public IHttpActionResult MoveQuote(MoveOpportunityDTO model)
        {
            try
            {
                if (model == null)
                    throw new Exception("Object should not empty");

                int opportunityId = 0;
                int QuoteKey = 0;
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                OpportunitiesManager oppMgr = new OpportunitiesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                AccountsManager actMgr = new AccountsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                CommunicationManager communication_Mangr = new CommunicationManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                if (model.OpportunityId == 0)
                {
                    var status = oppMgr.CreateOpportunity(out opportunityId, CopyMoveOpportunityModelToOpportunity(model), 0);
                    model.OpportunityId = opportunityId;
                    //texting
                    var CountryCode = SessionManager.CurrentFranchise.CountryCode;
                    var BrandId = SessionManager.CurrentFranchise.BrandId;
                    var Account = actMgr.Get(model.AccountId);
                    if (model.SendMsg)
                    {
                        var SendOptinMessage = communication_Mangr.ForceOptinMessage(Account.PrimCustomer.CellPhone, BrandId, CountryCode, null, model.AccountId);
                    }
                    if (model.UncheckNotifyviaText)
                    {
                        var unchecknotify = actMgr.UpdateNotifyText(model.AccountId);
                    }
                }
                QuoteKey = QuotesMgr.SaveMoveQuote(model);
                return Json(new { model.OpportunityId, QuoteKey });
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Post the Group Discount
        /// </summary>
        /// <param name="id"></param>
        /// <param name="lstGroupDiscount"></param>
        /// <returns></returns>
        [HttpPost, Route("ApplyGroupDiscount")]
        public IHttpActionResult ApplyGroupDiscount(int id, List<QuoteGroupDiscount> lstGroupDiscount)
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = QuotesMgr.SaveGroupDiscount(id, lstGroupDiscount);
                if (result != null)
                {
                    var QuoteLines = QuotesMgr.ApplyGroupDiscount(id);
                    var quote = QuotesMgr.GetDetails(id);
                    return Json(new { quoteKey = id, data = quote, error = "" });
                }
                else
                {
                    return Json(new { quoteKey = id, data = result, error = "An Error Occured while applying Group Discount" });
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return Json(new { quoteKey = id, data = "", error = "An Error Occured while applying Group Discount" });
            }
        }

        /// <summary>
        /// Get the Margin Details
        /// </summary>
        /// <param name="id">Quote Key</param>
        /// <returns></returns>
        [HttpGet, Route("GetMarginDetails/{id}")]
        public IHttpActionResult GetMarginDetails(int id)
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var quote = QuotesMgr.GetDetails(id);
                var quoteLines = QuotesMgr.GetQuoteMarginDetails(id);
                var discount = QuotesMgr.GetDiscountAndPromo(id);
                var costAudit = QuotesMgr.GetCostChangeAudit(id);

                return Json(new { data = new { quote = quote, quoteLines = quoteLines, DiscountAndPromo = discount, costAudit = costAudit }, error = "" });
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return Json(new { data = "", error = ex.Message });
            }
        }

        /// <summary>
        /// Update the quote Line Margin
        /// </summary>
        /// <param name="id">quote key</param>
        /// <param name="model">model</param>
        /// <returns></returns>
        [HttpPost, Route("UpdatequoteLineMargin")]
        public IHttpActionResult UpdatequoteLineMargin(int id, QuoteMarginReviewVM model)
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var response = QuotesMgr.UpdatequotMargin(id, model);

                var quote = QuotesMgr.GetDetails(id);
                var quoteLines = QuotesMgr.GetQuoteMarginDetails(id);
                var discount = QuotesMgr.GetDiscountAndPromo(id);

                var costAudit = QuotesMgr.GetCostChangeAudit(id);

                return Json(new { data = new { quote = quote, quoteLines = quoteLines, DiscountAndPromo = discount, costAudit = costAudit }, error = "" });
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return Json(new { data = "", error = ex.Message });
            }
        }

        private Opportunity CopyMoveOpportunityModelToOpportunity(MoveOpportunityDTO Model)
        {
            Opportunity opportunitymodel = new Opportunity();

            opportunitymodel.OpportunityName = Model.OpportunityName;
            opportunitymodel.SideMark = Model.SideMark;
            opportunitymodel.OpportunityStatusId = Model.OpportunityStatusId;
            opportunitymodel.SalesAgentId = Model.SalesAgentId;

            opportunitymodel.InstallationAddressId = Model.InstallationAddressId;
            opportunitymodel.BillingAddressId = Model.BillingAddressId;
            opportunitymodel.InstallationAddress = "";
            opportunitymodel.Description = Model.Description;

            opportunitymodel.SourcesTPId = Model.SourcesTPId;
            opportunitymodel.AccountId = Model.AccountId;
            opportunitymodel.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;

            //Set Questions Answers
            opportunitymodel.OpportunityQuestionAns = new List<Type_Question>();

            return opportunitymodel;
        }

        /// <summary>
        /// Get the QuoteLines for TL
        /// </summary>
        /// <param name="id">QuoteId</param>
        /// <returns></returns>
        [HttpGet, Route("GetQuoteLinesTL/{id}")]
        public IHttpActionResult GetQuoteLinesTL(int id)
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var res = QuotesMgr.GetQuoteLine(id);
                LookupManager lkupMgr = new LookupManager();
                var prdCateg = lkupMgr.GetMyProductCategories();

                foreach (var qlitem in res)
                {
                    if (qlitem.ProductCategoryId != null && qlitem.ProductCategoryId > 0)
                    {
                        qlitem.ProductCategory = prdCateg.Where(x => x.ProductCategoryId
                           == qlitem.ProductCategoryId)
                               .FirstOrDefault().ProductCategory;
                    }

                    if (qlitem.ProductSubCategoryId != null && qlitem.ProductSubCategoryId > 0)
                    {
                        qlitem.ProductSubCategory = prdCateg.Where(x => x.ProductCategoryId
                          == qlitem.ProductSubCategoryId)
                               .FirstOrDefault().ProductCategory;
                    }

                    if (qlitem.ProductId.HasValue && qlitem.ProductId > 0)
                    {
                        var products = lkupMgr.GetProdcustTL();
                        var discounts = lkupMgr.GetDiscountTL();
                        var p = products.Where(x => x.ProductKey
                            == qlitem.ProductId).FirstOrDefault();
                        if (p != null)
                        {
                            qlitem.ProductName = p.ProductName;
                        }
                    }
                }
                var discount = QuotesMgr.GetDiscountAndPromo(id);
                return Json(new { quoteLines = res, discountAndPromo = discount });
            }
            catch (Exception ex)
            {
                return Json(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Save the QuoteLines
        /// </summary>
        /// <param name="id">QuoteId</param>
        /// <param name="quoteLines">'{"QuoteID":"0","QuoteLineList":[{"measurmentSetId":0,"selected":true,
        /// "RoomLocation":"","RoomName":"","MountTypeName":"",
        /// "AddemptyLine":"Y","WindowLocation":"",
        /// "InternalNotes":""}]}'</param>
        /// <returns></returns>
        [HttpPost, Route("SaveQuoteLines")]
        public IHttpActionResult SaveQuoteLines(int id, [FromBody]string quoteLines)
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var res = QuotesMgr.insertQuoteLines(quoteLines);
                if (res == "Sucessful")
                {
                    var result = QuotesMgr.GetQuoteLine(id);
                    return Json(new { data = result, error = "" });
                }
                else
                {
                    return Json(new { data = "", error = "Failed to Save." });
                }
            }
            catch (Exception e)
            {
                return Json(new { data = "", error = e.Message });
            }
        }

        /// <summary>
        /// update the Quoteline
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, Route("updateQuoteline")]
        public IHttpActionResult updateQuoteline(QuoteLinesVM model)
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var quoteLines_old = QuotesMgr.GetQuoteLine(model.QuoteKey);
                var ql_old = quoteLines_old.Where(x => x.QuoteLineId == model.QuoteLineId).FirstOrDefault();
                var qld_old = QuotesMgr.GetMargin(model.QuoteLineId).FirstOrDefault();

                if (model.ProductTypeId == 4)
                {
                    if (model.DiscountType == "%")
                    {
                        model.Discount = model.Discount * 100;
                    }
                }

                if (qld_old != null)
                {
                    if (!qld_old.ManualUnitPrice)
                    {
                    }
                    else
                    {
                        model.ManualUnitPrice = qld_old.ManualUnitPrice;
                    }
                }
                var res = QuotesMgr.updatequoteinlineEdit(model);
                //Recalcualte the unit price if there is a change in unitprice or discount if changed by user
                if (qld_old.ManualUnitPrice && (ql_old.Discount != model.Discount || model.UnitPrice != ql_old.UnitPrice))
                {
                    var response = QuotesMgr.RecalculateQuoteLines(model.QuoteKey, model.QuoteLineId);
                }
                else
                {
                    var response = QuotesMgr.RecalculateQuoteLines(model.QuoteKey, 0);
                }

                var quote = QuotesMgr.GetDetails(model.QuoteKey);
                var quoteLines = QuotesMgr.GetQuoteLine(model.QuoteKey);
                var discount = QuotesMgr.GetDiscountAndPromo(model.QuoteKey);
                return Json(new
                {
                    data = new { quote = quote, quoteLines = quoteLines, discountAndPromo = discount },
                    error = ""
                });
            }
            catch (Exception ex)
            {
                return Json(new { data = "", error = ex.Message });
            }
        }

        /// <summary>
        /// update Quoteline for TL
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, Route("updateQuotelineTL")]
        public IHttpActionResult updateQuotelineTL(QuoteLinesVM model)
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                LookupManager lkupMgr = new LookupManager();
                var res = "Success";
                var quoteLines_old = QuotesMgr.GetQuoteLine(model.QuoteKey);
                var ql_old = QuotesMgr.getQuoteLinebyId(model.QuoteLineId);
                var details_old = QuotesMgr.GetMargin(model.QuoteLineId).FirstOrDefault();
                if (model.ProductTypeId == 4)
                {
                    if (model.DiscountType == "%")
                    {
                        model.Discount = model.Discount;
                    }

                }
                if (model.ManualUnitPrice)
                {
                    details_old.ManualUnitPrice = true;
                }
                else
                {
                    if (ql_old.ProductTypeId != model.ProductTypeId)
                    {
                        details_old.ManualUnitPrice = false;
                    }
                    else
                    {
                        if (details_old.unitPrice == model.UnitPrice)
                        {
                            if (details_old.ManualUnitPrice)
                            {
                                details_old.ManualUnitPrice = true;
                            }
                            else
                            {
                                details_old.ManualUnitPrice = false;
                            }
                        }
                        else
                        {
                            details_old.ManualUnitPrice = true;
                        }
                    }
                }

                if ((model.ProductTypeId == 2 || model.ProductTypeId == 3)
                    && model.ProductId.HasValue)
                {
                    var prdOrSvc = new MyVendorProductVM();
                    prdOrSvc.ManualUnitPrice = details_old.ManualUnitPrice;
                    prdOrSvc.SuggestedResale = model.SuggestedResale;
                    var myprdOrService = QuotesMgr.GetMyProductById(model.ProductId.Value);
                    prdOrSvc.Cost = myprdOrService.Cost.HasValue ? myprdOrService.Cost.Value : 0;
                    prdOrSvc.Description = model.Description;
                    prdOrSvc.PICGroupId = myprdOrService.TypeProductCategory.PICGroupId.HasValue
                        ? myprdOrService.TypeProductCategory.PICGroupId.Value
                        : 0;
                    prdOrSvc.PICProductId = myprdOrService.TypeProductCategory.PICProductId.HasValue
                        ? myprdOrService.TypeProductCategory.PICProductId.Value
                        : 0;
                    prdOrSvc.ProductCategory = myprdOrService.TypeProductCategory.ProductCategory;

                    // TODO: This should not be the case, now we are not using the
                    // the old way sujjected by Vasishta.
                    //prdOrSvc.ProductId = model.ProductSubCategoryId.HasValue ? model.ProductSubCategoryId.Value : 0;
                    //prdOrSvc.ProductKey = model.ProductSubCategoryId.HasValue ? model.ProductSubCategoryId.Value : 0;

                    //prdOrSvc.ProductId = myprdOrService.ProductID;
                    // TODO: why are we using product key and productid interchangeably
                    // it is very confusing.
                    prdOrSvc.ProductId = myprdOrService.ProductKey;
                    prdOrSvc.ProductKey = myprdOrService.ProductKey;

                    prdOrSvc.ProductName = myprdOrService.ProductName;
                    prdOrSvc.ProductTypeId = model.ProductTypeId;
                    prdOrSvc.Quantity = model.Quantity.HasValue ? model.Quantity.Value : 0;
                    prdOrSvc.QuotelineId = model.QuoteLineId;
                    if (details_old.ManualUnitPrice)
                    {
                        prdOrSvc.UnitPrice = model.UnitPrice.HasValue ? model.UnitPrice.Value : 0;
                    }
                    else
                    {
                        prdOrSvc.UnitPrice = myprdOrService.SalePrice.HasValue ? myprdOrService.SalePrice.Value : 0;
                    }
                    prdOrSvc.Vendor = myprdOrService.VendorName;
                    prdOrSvc.VendorId = myprdOrService.VendorID.HasValue ? myprdOrService.VendorID.Value : 0;
                    prdOrSvc.VendorProductSKU = myprdOrService.VendorProductSKU;
                    prdOrSvc.ProductCategoryId = model.ProductCategoryId.HasValue ? model.ProductCategoryId.Value : 0;
                    prdOrSvc.ProductSubcategoryId =
                        model.ProductSubCategoryId.HasValue ? model.ProductSubCategoryId.Value : 0;
                    prdOrSvc.Discount = model.Discount.HasValue ? model.Discount.Value : 0;

                    res = QuotesMgr.UpdateQuoteConfigurationTL(prdOrSvc);
                }
                else if (model.ProductTypeId == 4)
                {
                    ql_old.ProductTypeId = model.ProductTypeId;
                    ql_old.Discount = model.Discount.HasValue ? model.Discount.Value : 0;
                    ql_old.Quantity = 0;
                    ql_old.ProductCategoryId = model.ProductCategoryId.HasValue ? model.ProductCategoryId.Value : 0;
                    ql_old.ProductSubCategoryId =
                        model.ProductSubCategoryId.HasValue ? model.ProductSubCategoryId.Value : 0;
                    ql_old.ProductId = model.ProductId;
                    ql_old.ProductName = model.ProductName;
                    ql_old.Description = model.Description;
                    ql_old.DiscountType = model.DiscountType;
                    details_old.Discount = model.Discount.HasValue ? model.Discount.Value : 0;
                    ql_old.DiscountType = model.DiscountType;
                    QuotesMgr.UpdateAndInsertDiscountRow(ql_old, details_old);
                }
                else
                {
                    var prdCateg = lkupMgr.GetMyProductCategories();
                    var prdOrSvc = new MyVendorProductVM();
                    prdOrSvc.ManualUnitPrice = details_old.ManualUnitPrice;
                    prdOrSvc.SuggestedResale = model.SuggestedResale;
                    prdOrSvc.Cost = model.Cost.HasValue ? model.Cost.Value : 0;
                    prdOrSvc.Description = model.Description;
                    prdOrSvc.PICGroupId = 1000000;
                    prdOrSvc.PICProductId = 10000;
                    if (model.ProductCategoryId != 0)
                        prdOrSvc.ProductCategory = prdCateg.Where(x => x.ProductCategoryId == model.ProductCategoryId)
                            .FirstOrDefault().ProductCategory;
                    prdOrSvc.ProductName = model.ProductName;
                    prdOrSvc.ProductTypeId = model.ProductTypeId;
                    prdOrSvc.Quantity = model.Quantity.Value;
                    prdOrSvc.BaseResalePriceWOPromos = model.BaseResalePriceWOPromos;
                    prdOrSvc.QuotelineId = model.QuoteLineId;
                    prdOrSvc.UnitPrice = model.UnitPrice.HasValue ? model.UnitPrice.Value : 0;
                    prdOrSvc.ProductCategoryId = model.ProductCategoryId.HasValue ? model.ProductCategoryId.Value : 0;
                    prdOrSvc.ProductSubcategoryId =
                        model.ProductSubCategoryId.HasValue ? model.ProductSubCategoryId.Value : 0;
                    prdOrSvc.Discount = model.Discount.HasValue ? model.Discount.Value : 0;

                    res = QuotesMgr.UpdateQuoteConfigurationTL(prdOrSvc);
                }

                var quote = QuotesMgr.GetDetails(model.QuoteKey);
                var quoteLines = QuotesMgr.GetQuoteLine(model.QuoteKey);
                var discount = QuotesMgr.GetDiscountAndPromo(model.QuoteKey);
                return Json(new
                {
                    data = new { quote = quote, quoteLines = quoteLines, discountAndPromo = discount },
                    error = ""
                });
            }
            catch (Exception ex)
            {
                return Json(new { data = "", error = ex.Message });
            }
        }

        /// <summary>
        /// Save Quote
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, Route("SaveQuote")]
        public IHttpActionResult SaveQuote([FromBody]Quote model)
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var quote_org = QuotesMgr.GetQuoteData(model.QuoteKey);
                if (model.PrimaryQuote.HasValue && model.PrimaryQuote.Value)
                {
                    if (quote_org != null && quote_org.PrimaryQuote.Value != model.PrimaryQuote.Value)
                    {
                        var primquote = QuotesMgr.GetQuoteDataforPrimary(model.OpportunityId);
                        return Json(new { quoteKey = model.QuoteKey, error = "Duplicate Primary Quote Check", data = primquote });
                    }
                }

                var Status = QuotesMgr.Save(model);

                if (Status != 0)
                {
                    var result = QuotesMgr.GetDetails(Status);
                    return Json(new { quoteKey = Status, error = "", data = result });
                }
                return Json(new { quoteKey = Status, error = "" });
            }
            catch (Exception ex)
            {
                return Json(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// get the Configurator Save Statuses
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("getConfigSaveStatuses")]
        public IHttpActionResult getConfigSaveStatuses()
        {
            try
            {
                return Json(SessionManager.GetConfigstatus(SessionManager.CurrentUser.PersonId.ToString()) != null ? SessionManager.GetConfigstatus(SessionManager.CurrentUser.PersonId.ToString()) : "");
            }
            catch (Exception ex)
            {
                var data = SessionManager.GetConfigstatus(SessionManager.CurrentUser.PersonId.ToString());
                if (data != null)
                    EventLogger.LogEvent(data.ToString());
                EventLogger.LogEvent(ex);
                throw ex;
            }
        }

        /// <summary>
        /// validate And Save Configurator
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost, Route("validateAndSaveConfigurator")]
        public IHttpActionResult validateAndSaveConfigurator([FromBody] CoreProductVM obj)
        {
            string content = "";
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                SessionManager.SetConfigstatus(SessionManager.CurrentUser.PersonId.ToString(), "Step 1 of 4: Validating Product Data");
                string response = ValidatePromptAnswersforPICConfigurator(obj);
                dynamic responseObj = JsonConvert.DeserializeObject<ExpandoObject>(response);

                if (Convert.ToBoolean(responseObj.valid) == true && responseObj.messages.errors != null && responseObj.messages.errors.Count == 0)
                {
                    if (Convert.ToDecimal(responseObj.cost) == 0 || Convert.ToDecimal(responseObj.list) == 0)
                    {
                        return Json(new
                        {
                            valid = false,
                            message = "Price Validation has failed on this product configuration. Please contact your system administrator.",
                            data = responseObj,
                            QuoteKey = obj.QuoteKey,
                            OpportunityId = obj.OpportunityId
                        });
                    }
                    else
                    {
                        var qlVM = new QuoteLinesVM();
                        if (obj.QuotelineId == 0)
                        {
                            var ql = QuotesMgr.InsertEmptyQuoteline(obj.QuoteKey);
                            obj.QuotelineId = ql.QuoteLineId;
                            qlVM.QuoteLineId = ql.QuoteLineId;
                        }
                        else
                        {
                            qlVM.QuoteLineId = obj.QuotelineId;
                        }

                        var QuoteLine = QuotesMgr.getQuoteLineWithGroupDiscount(obj.QuotelineId);
                        if (QuoteLine == null)
                            QuoteLine = qlVM;
                        var res = QuotesMgr.UpdateCoreProductQuoteconfiguration(obj, responseObj);
                        QuotesMgr.ApplyGroupDiscount(obj.QuoteKey);
                        QuotesMgr.RefreshGroupDiscountTable(obj.QuoteKey);
                    }
                }
                var qData = QuotesMgr.GetQuoteDetailsforCoreConfiguration(obj.QuoteKey);
                return Json(new
                {
                    valid = true,
                    data = responseObj,
                    QuoteKey = obj.QuoteKey,
                    OpportunityId = obj.OpportunityId,
                    QuoteData = qData
                });
            }
            catch (Exception ex)
            {
                SessionManager.SetConfigstatus(SessionManager.CurrentUser.PersonId.ToString(), "");
                string msg = "";
                if (ex.Message ==
                    "Unexpected character encountered while parsing value: <. Path '', line 0, position 0.")
                {
                    msg = content;
                }
                else
                {
                    msg = ex.Message;
                }
                return Json(new { valid = false, message = msg });
            }
        }

        /// <summary>
        /// clear the Configurator Save Statuses
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("clearConfigSaveStatuses")]
        public IHttpActionResult clearConfigSaveStatuses()
        {
            SessionManager.SetConfigstatus(SessionManager.CurrentUser.PersonId.ToString(), "");
            return Json(true);
        }

        /// <summary>
        /// Rearrange Quote lines
        /// </summary>
        /// <param name="LineModel"></param>
        /// <returns></returns>
        [HttpPost, Route("rearrangeQuotelines")]
        public bool rearrangeQuotelines(List<QuoteLinesVM> LineModel)
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var res = QuotesMgr.updatequoteLineNumber(LineModel);
                return true;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return false;
            }
        }

        /// <summary>
        /// change Expiry To Draft
        /// </summary>
        /// <param name="id">quoteKey</param>
        /// <returns></returns>
        [HttpPost, Route("changeExpiryToDraft")]
        public IHttpActionResult changeExpiryToDraft(int id)
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var res = QuotesMgr.changeExpiryToDraft(id);
                if (res == true)
                    return Json(true);
                else return Json(false);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return Json(false);
            }

        }

        /// <summary>
        /// Estimate the SalesTax
        /// </summary>
        /// <param name="id">quoteKey</param>
        /// <returns></returns>
        [HttpGet, Route("EstimateSalesTax/{id}")]
        public IHttpActionResult EstimateSalesTax(int id)
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var res = QuotesMgr.DeleteTax(id);
                var picTaxErr = false;
                var tax = QuotesMgr.GetAndUpdateTax(id);
                dynamic responseObj = JsonConvert.DeserializeObject(tax);

                if (!Convert.ToBoolean(responseObj.valid.Value))
                {
                    var err = string.Empty;
                    foreach (var item in responseObj.error)
                    {
                        if (item.Value.ToString().ToLower().Contains("jurisdictionnotfounderror: "))
                        {
                            picTaxErr = true;
                        }
                        err = err + "<li>" + item.Value + "</li>";
                    }
                    return Json(new { data = "", error = err, PICTaxerror = picTaxErr });
                }
                else
                {
                    var taxInfo = QuotesMgr.GetTaxinfo(id);
                    return Json(new { data = new { Taxinfo = taxInfo }, error = "", PICTaxerror = picTaxErr });
                }
            }
            catch (Exception ex)
            {
                return Json(new { data = "", error = ex.Message, PICTaxerror = false });
            }
        }

        /// <summary>
        /// Update Discount And Promos
        /// </summary>
        /// <param name="id">quoteKey</param>
        /// <param name="models"></param>
        /// <returns></returns>
        [HttpPost, Route("UpdateDiscountAndPromos")]
        public IHttpActionResult UpdateDiscountAndPromos(int id, List<DiscountsAndPromo> models)
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                foreach (var item in models)
                {
                    var response = QuotesMgr.UpdateDiscountAndPromo(item);
                }

                QuotesMgr.RecalculateQuoteLines(id);
                var quote = QuotesMgr.GetDetails(id);
                var quoteLines = QuotesMgr.GetQuoteMarginDetails(id);
                var discount = QuotesMgr.GetDiscountAndPromo(id);
                return Json(new { data = new { quote = quote, quoteLines = quoteLines, DiscountAndPromo = discount }, error = "" });
            }
            catch (Exception ex)
            {
                return Json(new { data = "", error = ex.Message });
            }
        }

        /// <summary>
        /// Get the Discount Details
        /// </summary>
        /// <param name="id">quoteKey</param>
        /// <returns></returns>
        [HttpGet, Route("GetDiscountDetails/{id}")]
        public IHttpActionResult GetDiscountDetails(int id)
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var discount = QuotesMgr.GetDiscountAndPromo(id);
                return Json(discount);
            }
            catch (Exception ex)
            {
                return Json(new { data = "", error = ex.Message });
            }
        }

        /// <summary>
        /// Update the Quote Line Item
        /// </summary>
        /// <param name="id">quoteKey</param>
        /// <param name="qld">Model</param>
        /// <returns></returns>
        [HttpPost, Route("UpdateQuoteLineItem")]
        public IHttpActionResult UpdateQuoteLineItem(int id, List<QuoteLineDetail> qld)
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = QuotesMgr.UpdateQuoteLine(id, qld);
                return null;
            }
            catch (Exception ex)
            {
                return Json(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Margin details
        /// </summary>
        /// <param name="id">quoteKey</param>
        /// <returns></returns>
        [HttpGet, Route("GetMargin/{id}")]
        public IHttpActionResult GetMargin(int id)
        {
            QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var result = QuotesMgr.GetMargin(id);
            if (result != null)
            {
                return Json(
                    result
                );
            }
            return OkTP();
        }

        /// <summary>
        /// Get Vendor Promo Details
        /// </summary>
        /// <param name="id">quoteKey</param>
        /// <returns></returns>
        [HttpGet, Route("GetVendorPromoDetails/{id}")]
        public IHttpActionResult GetVendorPromoDetails(int id)
        {
            QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var result = QuotesMgr.GetDiscountAndProm(id);
            if (result != null)
            {
                return Json(
                    result
                );
            }
            return OkTP();
        }

        /// <summary>
        /// Save the Sale Date
        /// </summary>
        /// <param name="id">quoteKey</param>
        /// <param name="SaleDate">SaleDate</param>
        /// <returns></returns>
        [HttpPost, Route("SaveSaleDate")]
        public IHttpActionResult SaveSaleDate(int id, DateTime SaleDate)
        {
            try 
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var Result = QuotesMgr.SaveSale(id, SaleDate);

                if (Result == null)
                {
                    return Json(new { data = "", error = "Failed to Save." });
                }
                return Json(new { data = Result, error = "" });
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return Json(new { data = "", error = "Failed to Save." });
            }
        }

        /// <summary>
        /// Save Quote Notes
        /// </summary>
        /// <param name="id">quoteKey</param>
        /// <returns></returns>
        [HttpPost, Route("SaveQuoteNotes")]
        public IHttpActionResult SaveQuoteNotes(int id)
        {
            try 
            {
                var httpRequest = HttpContext.Current.Request;
                var files = httpRequest.Files;
                var QuoteLevelNotes = httpRequest.Form["QuoteLevelNotes"];
                var OrderInvoiceLevelNotes = httpRequest.Form["OrderInvoiceLevelNotes"];
                var InstallerInvoiceNotes = httpRequest.Form["InstallerInvoiceNotes"];
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                QuotesMgr.SaveQuoteNotes(id, QuoteLevelNotes, OrderInvoiceLevelNotes, InstallerInvoiceNotes);

                return Json(true);
            }
            catch (Exception ex)
            {
                return Json(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// update Quote line From Config
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, Route("updateQuotelineFromConfig")]
        public IHttpActionResult updateQuotelineFromConfig(QuoteLinesVM model)
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var res = QuotesMgr.updateQuotelineFromConfig(model);

                return Json(new { error = "", QuoteKey = model.QuoteKey, OpportunityId = model.OpportunityId });
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return Json(new { error = ex.Message });
            }
        }

        /// <summary>
        /// Save Quote Lines Memo
        /// </summary>
        /// <param name="id">quoteKey</param>
        /// <param name="memo">Model</param>
        /// <returns></returns>
        [HttpPost, Route("SaveQuoteLinesMemo")]
        public IHttpActionResult SaveQuoteLinesMemo(int id, [FromBody]string memo)
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var res = QuotesMgr.updateQuotelineMemo(id, memo);
                if (res == "Sucessful")
                {
                    var result = QuotesMgr.GetQuoteLine(id);
                    return Json(new { data = "", error = "" });
                }
                else
                {
                    return Json(new { data = "", error = "Failed to Save." });
                }
            }
            catch (Exception e)
            {
                EventLogger.LogEvent(e);
                return Json(new { data = "", error = e.Message });
            }
        }

        /// <summary>
        /// Erase Quote Line
        /// </summary>
        /// <param name="id">quoteKey</param>
        /// <param name="arrObj">List</param>
        /// <returns></returns>
        [HttpPost, Route("EraseQuoteLine")]
        public IHttpActionResult EraseQuoteLine(int id, List<dynamic> arrObj)
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                int QuoteKey = id;
                var qIds = new List<int>();
                foreach (var obj in arrObj)
                {
                    qIds.Add(Convert.ToInt32(obj.QuoteLineId.Value));
                }
                var res = QuotesMgr.CopyDeleteQuoteline(qIds, "DELETE");
                var response = QuotesMgr.RecalculateQuoteLines(QuoteKey);
                QuotesMgr.RefreshGroupDiscountTable(QuoteKey);
                var quote = QuotesMgr.GetDetails(QuoteKey);
                var result = QuotesMgr.GetQuoteLine(QuoteKey);
                return Json(new { data = result, error = "", quote = quote });
            }
            catch (Exception e)
            {
                EventLogger.LogEvent(e);
                return Json(new { data = "", error = e.Message });
            }
        }

        /// <summary>
        /// Mark Quote Lines as TaxExempt
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isTaxExempt"></param>
        /// <param name="arrObj"></param>
        /// <returns></returns>
        [HttpPost, Route("MarkQuoteLinesTaxExempt")]
        public IHttpActionResult MarkQuoteLinesTaxExempt(int id, bool isTaxExempt, List<dynamic> arrObj)
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                int QuoteKey = id;
                var qIds = new List<int>();
                foreach (var obj in arrObj)
                {
                    qIds.Add(Convert.ToInt32(obj.QuoteLineId.Value));
                }
                QuotesMgr.MarkQuotelinesTaxExempt(qIds, isTaxExempt);
                var response = QuotesMgr.RecalculateQuoteLines(QuoteKey);
                var quote = QuotesMgr.GetDetails(QuoteKey);

                var result = QuotesMgr.GetQuoteLine(QuoteKey);
                return Json(new { data = result, error = "", quote = quote });

            }
            catch (Exception e)
            {
                EventLogger.LogEvent(e);
                return Json(new { data = "", error = e.Message });
            }
        }

        /// <summary>
        /// Convert Quote To Order Tax Override
        /// </summary>
        /// <param name="id">quoteKey</param>
        /// <returns></returns>
        [HttpGet, Route("ConvertQuoteToOrderTaxOverride/{id}")]
        public IHttpActionResult ConvertQuoteToOrderTaxOverride(int id)
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                if (!QuotesMgr.QuoteOrderExists(id))
                {
                    int OrderId;
                    OrderId = QuotesMgr.CreateOrder(id);
                    CreateInvoiceFromConvertOrder(OrderId);

                    return Json(new { data = OrderId, error = "" });
                }
                else
                {
                    return Json(new { data = "", error = "An order already exists for this opportunity." });
                }
            }
            catch (Exception ex)
            {
                return Json(new { data = "", error = ex.Message });
            }
        }

        /// <summary>
        /// Convert Quote To Order
        /// </summary>
        /// <param name="id">Quotekey</param>
        /// <returns></returns>
        [HttpGet, Route("ConvertQuoteToOrder/{id}")]
        public IHttpActionResult ConvertQuoteToOrder(int id)
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var validQuote = QuotesMgr.ValidateQuote(id, false);
                if (!string.IsNullOrEmpty(validQuote))
                {
                    return Json(new { data = "", error = validQuote });
                }
                var picTaxErr = false;
                var quote = QuotesMgr.GetDetails(id);
                if (!QuotesMgr.QuoteOrderExists(id))
                {
                    int OrderId;
                    if (quote != null && quote.IsTaxExempt)
                    {
                        QuotesMgr.DeleteTax(id);
                        OrderId = QuotesMgr.CreateOrder(id);
                        CreateInvoiceFromConvertOrder(OrderId);
                    }
                    else
                    {
                        var response = QuotesMgr.CreateTaxOnSalesOrder(id);
                        dynamic responseObj = JsonConvert.DeserializeObject(response);

                        if (Convert.ToBoolean(responseObj.valid.Value))
                        {
                            OrderId = QuotesMgr.CreateOrder(id);
                            CreateInvoiceFromConvertOrder(OrderId);
                        }
                        else
                        {
                            var err = string.Empty;
                            foreach (var item in responseObj.error)
                            {
                                if (item.Value.ToString().ToLower().Contains("jurisdictionnotfounderror: "))
                                {
                                    picTaxErr = true;
                                }

                                if (item.Value.ToString().ToLower().Contains("input missing required element 'items'."))
                                {
                                    err = "There are no line items to Create Sales Order.";
                                }
                                else
                                {
                                    err = err + " " + item.Value + " ";
                                }
                            }
                            return Json(new { data = "", error = err, PICTaxerror = picTaxErr });
                        }
                    }

                    return Json(new { data = OrderId, error = "", PICTaxerror = picTaxErr });
                }
                else
                {
                    return Json(new { data = "", error = "Quote is already Converted.", PICTaxerror = picTaxErr });
                }
            }
            catch (Exception ex)
            {
                return Json(new { data = "", error = ex.Message });
            }
        }

        /// <summary>
        /// Clear Quotes lines
        /// </summary>
        /// <param name="Id">QuoteStatusId</param>
        /// <param name="QuoteKey">QuoteKey</param>
        /// <returns></returns>
        [HttpGet, Route("ClearQuoteslines/{Id}/{QuoteKey}")]
        public IHttpActionResult ClearQuoteslines(int Id, int QuoteKey)
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var res = QuotesMgr.ClearQuoteslines(Id, QuoteKey);
                var resultGroupDiscount = QuotesMgr.RefreshGroupDiscountTable(QuoteKey);
                var result = QuotesMgr.GetDetails(QuoteKey);

                return Json(new { quoteKey = QuoteKey, error = "", data = result });
            }
            catch (Exception ex)
            {
                return Json(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// get Tax Exempt
        /// </summary>
        /// <param name="Id">QuoteStatusId</param>
        /// <param name="OpportunityId">OpportunityId</param>
        /// <returns></returns>
        [HttpGet, Route("getTaxExempt/{Id}/{OpportunityId}")]
        public IHttpActionResult getTaxExempt(int Id, int OpportunityId)
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var res = QuotesMgr.getTaxExempt(Id, OpportunityId);

                return Json(res);
            }
            catch (Exception ex)
            {
                return Json(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// change IsTaxExempt
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="QuoteId"></param>
        /// <param name="OpportunityId"></param>
        /// <returns></returns>
        [HttpGet, Route("changeIsTaxExempt/{Id}/{QuoteId}/{OpportunityId}")]
        public IHttpActionResult changeIsTaxExempt(int Id, int QuoteId, int OpportunityId)
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var res = QuotesMgr.changeIsTaxExempt(Id, QuoteId, OpportunityId);
                return Json(res);
            }
            catch (Exception ex)
            {
                return Json(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Update Quote Status
        /// </summary>
        /// <param name="id">quoteKey</param>
        /// <param name="quoteStatus">quoteStatus</param>
        /// <returns></returns>
        [HttpPost, Route("UpdateQuoteStatus")]
        public IHttpActionResult UpdateQuoteStatus(int id, int quoteStatus)
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var res = QuotesMgr.UpdateQuotesStatus(id, quoteStatus);
                return Json(res);
            }
            catch (Exception ex)
            {
                return Json(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// validate Prompt Answers
        /// </summary>
        /// <param name="obj">Model</param>
        /// <returns></returns>
        [HttpPost, Route("validatePromptAnswers")]
        public IHttpActionResult validatePromptAnswers([FromBody] CoreProductVM obj)
        {
            string content = "";
            try
            {
                string response = ValidatePromptAnswersforPICConfigurator(obj);
                dynamic responseObj = JsonConvert.DeserializeObject<ExpandoObject>(response);
                if (Convert.ToBoolean(responseObj.valid) == true && responseObj.messages.errors != null &&
                    responseObj.messages.errors.Count > 0)
                {
                    if (Convert.ToDecimal(responseObj.cost) == 0 || Convert.ToDecimal(responseObj.list) == 0)
                    {
                        return Json(new
                        {
                            valid = false,
                            message = "Price Validation has failed on this product configuration. Please contact your system administrator.",
                            data = responseObj,
                            QuoteKey = obj.QuoteKey,
                            OpportunityId = obj.OpportunityId
                        });
                    }
                }
                else
                {
                    return Json(new
                    {
                        valid = true,
                        message = "",
                        data = responseObj,
                        QuoteKey = obj.QuoteKey,
                        OpportunityId = obj.OpportunityId
                    });
                }

                return Json(new
                {
                    valid = true,
                    data = responseObj,
                    QuoteKey = obj.QuoteKey,
                    OpportunityId = obj.OpportunityId
                });
            }
            catch (Exception ex)
            {
                SessionManager.SetConfigstatus(SessionManager.CurrentUser.PersonId.ToString(), "");
                string msg = "";
                if (ex.Message ==
                    "Unexpected character encountered while parsing value: <. Path '', line 0, position 0.")
                {
                    msg = content;
                }
                else
                {
                    msg = ex.Message;
                }

                return Json(new { valid = false, message = msg });
            }
        }

        /// <summary>
        /// UpdatequoteLineDescription
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("UpdatequoteLineDescription")]
        public IHttpActionResult UpdatequoteLineDescription()
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = QuotesMgr.UpdateBulkQuotelineDescription();

                return OkTP();
            }
            catch (Exception ex)
            {
                return Json(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Crew Size
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetCrewSize")]
        public IHttpActionResult GetCrewSize()
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = QuotesMgr.GetJobCrewSize();

                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Validate All Quotes
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("ValidateAllQuotes")]
        public IHttpActionResult ValidateAllQuotes()
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = QuotesMgr.ValidateAllQuotes();

                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Update the Quote description for all quotes updated in last 30days
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("UpdatequoteLineDescriptioninBatch")]
        public IHttpActionResult UpdatequoteLineDescriptioninBatch()
        {
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = QuotesMgr.UpdateQuoteLineDescription();

                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(EventLogger.LogEvent(ex));
            }
        }


        private string ValidatePromptAnswersforPICConfigurator(CoreProductVM obj)
        {
            string content = "";
            try
            {
                Stopwatch start = new Stopwatch();
                string url = AppConfigManager.PICUrl;
                // Configurator / PromptAnswers / Validate /{ customerId}/{ productId}/{ modelId}/{ colorId}
                //TODO: for now hard code the franchisecode as these will be later changed as Franchisecode by PIC
                string FranchiseCode = SessionManager.CurrentFranchise.Code;
                var api = url + "/Configurator/PromptAnswers/Validate/" + FranchiseCode + "/" + obj.PicProduct + "/" +
                          obj.ModelId + "?access_token=" + PICManager.PICToken();
                var client = new RestClient(api);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");

                request.AddHeader("content-type", "application/x-www-form-urlencoded");
                request.AddParameter("application/x-www-form-urlencoded", "promptAnswers=" + obj.PromptAnswers,
                    ParameterType.RequestBody);
                start.Start();
                IRestResponse response = client.Execute(request);
                start.Stop();
                EventLogger.LogRequest(request, response, start.ElapsedMilliseconds, "PIC", "Validate");
                return response.Content;
            }
            catch (Exception ex)
            {
                string msg = "";
                if (ex.Message ==
                    "Unexpected character encountered while parsing value: <. Path '', line 0, position 0.")
                {
                    msg = content;
                }
                else
                {
                    msg = ex.Message;
                }

                EventLogger.LogEvent(ex);
                throw ex;
            }
        }

        private bool CreateInvoiceFromConvertOrder(int orderId)
        {
            OrderManager ordermgr = new OrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            InvoiceSheetReport pdf = new InvoiceSheetReport();
            var order = ordermgr.GetOrderByOrderId(orderId);
            int invoiceId = ordermgr.CreateInvoice(order);
            Guid filepdf = pdf.createInvoicePDF(invoiceId);
            var result = ordermgr.CreateInvoice(invoiceId, filepdf);

            return true;
        }
    }
}
