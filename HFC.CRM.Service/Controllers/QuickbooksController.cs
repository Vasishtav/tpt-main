﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Web.Http;
using System.Linq;
using System.Net;
using System.Web;
using Intuit.Ipp.Core.Configuration;

namespace HFC.CRM.Service.Controllers
{
    using HFC.CRM.Core.Common;
    using HFC.CRM.Data;
    using HFC.CRM.Managers.Quickbooks;
    using HFC.CRM.Managers.QBSync;
    using Core.Logs;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using Intuit.Ipp.OAuth2PlatformClient;
    using System.Threading.Tasks;
    using Intuit.Ipp.Security;
    using Intuit.Ipp.Core;
    using Intuit.Ipp.DataService;
    using Intuit.Ipp.QueryFilter;
    using System.Web.Http.Controllers;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Net.Http.Formatting;
    using DTO.Lookup;
    using System.Dynamic;

    [RoutePrefix("api/Quickbooks")]
    public class QuickbooksController : BaseController
    {
        public static string clientid = ConfigurationManager.AppSettings["clientid"].ToString();
        public static string clientsecret = ConfigurationManager.AppSettings["clientsecret"].ToString();
        public static string redirectUrl = ConfigurationManager.AppSettings["redirectUrl"].ToString();
        public static string environment = ConfigurationManager.AppSettings["appEnvironment"].ToString();
        public static string redirectpath = ConfigurationManager.AppSettings["redirectTPTpath"].ToString();

        #region QBO Signin
        /// <summary>
        /// Generate authorize url to get the OAuth2 code
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("InitiateAuth")]
        public IHttpActionResult InitiateAuth()
        {
            try
            {
                //EventLogger.LogEvent("Call Started FranchiseId: " + SessionManager.CurrentFranchise.FranchiseId.ToString(), "InitiateAuth");
                var baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);
                Intuit.Ipp.OAuth2PlatformClient.OAuth2Client auth2Client = new Intuit.Ipp.OAuth2PlatformClient.OAuth2Client(clientid, clientsecret, baseUrl + redirectUrl, environment);

                //EventLogger.LogEvent(clientid + ", " + clientsecret + ", " + baseUrl + redirectUrl + ", " + environment, "Keys");
                //EventLogger.LogEvent(JsonConvert.SerializeObject(auth2Client), "auth2Client");

                List<OidcScopes> scopes = new List<OidcScopes>();
                scopes.Add(OidcScopes.Accounting);
                scopes.Add(OidcScopes.OpenId);
                scopes.Add(OidcScopes.Email);
                scopes.Add(OidcScopes.Profile);
                string authorizeUrl = auth2Client.GetAuthorizationURL(scopes);
                //EventLogger.LogEvent(authorizeUrl, "authorizeUrl");
                //return Redirect(authorizeUrl);

                string[] separateURL = authorizeUrl.Split('?');
                NameValueCollection queryString = System.Web.HttpUtility.ParseQueryString(separateURL[1]);
                queryString["state"] = SessionManager.CurrentFranchise.FranchiseId.ToString();
                return Json(separateURL[0] + "?" + queryString.ToString());
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }
        }

        /// <summary>
        /// OAuthcallback
        /// </summary>
        /// <param name="state"></param>
        /// <param name="code"></param>
        /// <param name="realmId"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet, Route("OAuthcallback")]
        public async Task<IHttpActionResult> OAuthcallback(string state = null, string code = null, string realmId = null)
        {

            var baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);

            if (code == null || realmId == null)
                return Redirect(baseUrl + redirectpath);

            //var QBApp = new QBManager().GetQBApp(Convert.ToInt32(state));
            //EventLogger.LogEvent(state, "OAuthcallback");

            var profile = new Profile();
            profile.code = code;
            profile.RealmId = realmId;

            Intuit.Ipp.OAuth2PlatformClient.OAuth2Client auth2Client = new Intuit.Ipp.OAuth2PlatformClient.OAuth2Client(clientid, clientsecret, baseUrl + redirectUrl, environment);

            var tokenResponse = await auth2Client.GetBearerTokenAsync(code);
            if (tokenResponse == null)
                return Redirect(baseUrl + redirectpath);

            profile.accesstoken = tokenResponse.AccessToken;
            profile.accesstokenexpiresat = tokenResponse.AccessTokenExpiresIn;

            profile.refreshtoken = tokenResponse.RefreshToken;
            profile.refreshtokenexpiresat = tokenResponse.RefreshTokenExpiresIn;

            var userResponse = await auth2Client.GetUserInfoAsync(tokenResponse.AccessToken);
            if (userResponse == null)
                return Redirect(baseUrl + redirectpath);

            if (state != null && state != "")
            {
                new QBManager().UpdateQBOSessioninfo(Convert.ToInt32(state), JsonConvert.SerializeObject(profile));
            }

            //SessionManager.Profile = profile;
            //SessionManager.QbEmail = userResponse.Claims.Where(x => x.Type == "email").Select(x => x.Value).FirstOrDefault();

            return Redirect(baseUrl + redirectpath);
        }

        /// <summary>
        /// QBO Sessioninfo
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("QBOSessioninfo")]
        public HttpResponseMessage QBOSessioninfo()
        {
            HttpResponseMessage respMessage = new HttpResponseMessage();
            respMessage.Content = new ObjectContent<string>(DateTimeOffset.Now.AddHours(1).ToString(), new JsonMediaTypeFormatter());

            QBApps app = new QBManager().GetQBApp(SessionManager.CurrentFranchise.FranchiseId);

            if (app.AppToken != null && app.AppToken != "")
            {
                Profile profile = JsonConvert.DeserializeObject<Profile>(app.AppToken);
                respMessage.Headers.AddCookies(new CookieHeaderValue[] { setQBOsession(profile) });
            }
            return respMessage;
        }

        private Profile getProfile()
        {
            Profile profile = new Profile();
            CookieHeaderValue cookie = Request.Headers.GetCookies("QBOsession").FirstOrDefault();
            if (cookie != null)
            {
                profile.code = cookie["QBOsession"].Values["code"];
                profile.RealmId = cookie["QBOsession"].Values["RealmId"];
                profile.accesstoken = cookie["QBOsession"].Values["accesstoken"];
                profile.accesstokenexpiresat = cookie["QBOsession"].Values["accesstokenexpiresat"] != null ? Convert.ToInt64(cookie["QBOsession"].Values["accesstokenexpiresat"]) : 0;
            }
            return profile;
        }

        private CookieHeaderValue setQBOsession(Profile profile = null)
        {
            if (profile != null)
            {
                var values = new NameValueCollection();
                values["code"] = profile.code;
                values["RealmId"] = profile.RealmId;
                values["accesstoken"] = profile.accesstoken;
                values["accesstokenexpiresat"] = profile.accesstokenexpiresat.ToString();

                var cookie = new CookieHeaderValue("QBOsession", values);
                cookie.Expires = DateTimeOffset.Now.AddHours(1);
                cookie.Domain = Request.RequestUri.Host;
                cookie.Path = "/";
                return cookie;
            }
            else
            {
                var cookie = new CookieHeaderValue("QBOsession", "")
                {
                    Expires = DateTimeOffset.Now.AddDays(-1),
                    Domain = Request.RequestUri.Host,
                    Path = "/"
                };
                return cookie;
            }
        }


        #endregion

        #region Insert / Update / Get QB.apps table

        /// <summary>
        /// Return QB app info
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Route("GetQBApp/{id}")]
        public IHttpActionResult GetQBApp(int id)
        {
            try
            {
                EventLogger.LogEvent(id.ToString(), "QuickbooksController GetQBApp call started", LogSeverity.Information);
                var QBApp = new QBManager().GetQBApp(SessionManager.CurrentFranchise.FranchiseId);
                QBApp.BrandId = SessionManager.CurrentFranchise.BrandId;
                QBApp.SynchronizationStatusUrl = string.Format(Core.Common.Configuration.SynchronizationStatusUrl, QBApp.OwnerID);

                Franchise fe = new QBManager().GetFranchise(SessionManager.CurrentFranchise.FranchiseId);
                if (fe != null)
                {
                    AddressTP AddressTP = new QBManager().GetAddress((int)fe.AddressId);
                    fe.Region = AddressTP.CountryCode2Digits;

                    if (fe.Region == "CA")
                        QBApp.dispalyTaxCode = true;
                    else
                        QBApp.dispalyTaxCode = false;
                }

                int brandid = SessionManager.CurrentFranchise.BrandId;
                string suffix = ContantStrings.GetBrandName(brandid);

                if (QBApp.AR == null || QBApp.AR == "")
                    QBApp.AR = string.Format("{0} {1}", ContantStrings.ChartOfAccounts.AR, suffix);
                if (QBApp.Sales == null || QBApp.Sales == "")
                    QBApp.Sales = string.Format("{0} {1}", ContantStrings.ChartOfAccounts.Sales, suffix);
                if (QBApp.COG == null || QBApp.COG == "")
                    QBApp.COG = string.Format("{0} {1}", ContantStrings.ChartOfAccounts.COG, suffix);

                EventLogger.LogEvent(JsonConvert.SerializeObject(QBApp), "QuickbooksController GetQBApp call ended", LogSeverity.Information);
                return ResponseResult(ContantStrings.Success, Json<QBApps>(QBApp));
            }
            catch (Exception ex)
            {
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Save QB app info
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        [HttpPost, Route("SaveQBApp")]
        public IHttpActionResult SaveQBApp(QBApps app)
        {
            try
            {
                EventLogger.LogEvent(JsonConvert.SerializeObject(app), "QuickbooksController SaveQBApp call started", LogSeverity.Information);
                new QBManager().SaveQBApp(app);
                return ResponseResult(ContantStrings.Success);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        #endregion

        #region GET / Update Tax group and Taxrate

        /// <summary>
        /// Get Tax Name from Database [QB].[TaxRateMapping]
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Route("GetTaxName")]
        public IHttpActionResult GetTaxName()
        {
            try
            {
                var data = new QBManager().GetTaxName(SessionManager.CurrentFranchise.FranchiseId);
                return Json(data);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get TaxCode for the Quickbook online Logged in / Signin user
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetTaxCode")]
        public IHttpActionResult GetTaxCode()
        {
            try
            {
                //var profile = SessionManager.Profile;
                Profile profile = getProfile();

                if (String.IsNullOrEmpty(profile.accesstoken))
                {
                    throw new QBSyncValidationException("Neet to authorize to QBO!");
                }

                QBApps app = new QBManager().GetQBApp(SessionManager.CurrentFranchise.FranchiseId);
                Franchise fe = new QBManager().GetFranchise(SessionManager.CurrentFranchise.FranchiseId);
                if (fe != null)
                {
                    AddressTP AddressTP = new QBManager().GetAddress((int)fe.AddressId);
                    fe.Region = AddressTP.CountryCode2Digits;
                }

                if (app != null)
                {
                    var task = new QBSyncTask(app, SessionManager.QbEmail, new QBSyncInfoManager(app.OwnerID), profile, fe);
                    var data = task.GetALLTaxCode();
                    return Json(data);
                }
                else
                {
                    return ResponseResult(ContantStrings.SyncNotEnabled);
                }

            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get TaxCode for the Quickbook online Logged in / Signin user
        /// </summary>
        /// <param name="TaxCode"></param>
        /// <returns></returns>
        [HttpGet, Route("GetTaxRatebyTaxCode/{TaxCode}")]
        public IHttpActionResult GetTaxRatebyTaxCode(string TaxCode)
        {
            try
            {
                Profile profile = getProfile();
                if (String.IsNullOrEmpty(profile.accesstoken))
                {
                    throw new QBSyncValidationException("Neet to authorize to QBO!");
                }

                QBApps app = new QBManager().GetQBApp(SessionManager.CurrentFranchise.FranchiseId);
                Franchise fe = new QBManager().GetFranchise(SessionManager.CurrentFranchise.FranchiseId);
                if (fe != null)
                {
                    AddressTP AddressTP = new QBManager().GetAddress((int)fe.AddressId);
                    fe.Region = AddressTP.CountryCode2Digits;
                }

                if (app != null)
                {
                    var task = new QBSyncTask(app, SessionManager.QbEmail, new QBSyncInfoManager(app.OwnerID), profile, fe);
                    var data = task.GetTaxRatebyTaxCode(TaxCode);
                    return Json(data);
                }
                else
                {
                    return ResponseResult(ContantStrings.SyncNotEnabled);
                }

            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Update TaxCode info in QB.Apps table
        /// </summary>
        /// <param name="TaxCode"></param>
        /// <returns></returns>
        [HttpGet, Route("UpdateTaxCode/{TaxCode}")]
        public IHttpActionResult UpdateTaxCode(string TaxCode)
        {
            try
            {
                new QBManager().UpdateTaxCode(SessionManager.CurrentFranchise.FranchiseId, TaxCode);
                return ResponseResult(ContantStrings.Success);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        /// <summary>
        /// Update QBO Taxname [QB].[TaxRateMapping]
        /// </summary>
        /// <param name="TaxRateMapping"></param>
        /// <returns></returns>
        [HttpPost, Route("UpdateTaxName")]
        public IHttpActionResult UpdateTaxName(List<TaxRateMapping> TaxRateMapping)
        {
            try
            {
                new QBManager().UpdateTaxName(TaxRateMapping);
                return ResponseResult(ContantStrings.Success);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        #endregion

        #region Get Chart of Accounts for the Quickbook online Logged in / Signin user
        /// <summary>
        /// Get Account for the Quickbook online Logged in / Signin user
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetAccount")]
        public IHttpActionResult GetAccount()
        {
            try
            {
                //var profile = SessionManager.Profile;
                Profile profile = getProfile();
                QBApps app = new QBManager().GetQBApp(SessionManager.CurrentFranchise.FranchiseId);

                int brandid = SessionManager.CurrentFranchise.BrandId;
                string suffix = ContantStrings.GetBrandName(brandid);

                if (profile == null || String.IsNullOrEmpty(profile.accesstoken))
                {
                    dynamic objCOA = new ExpandoObject();
                    List<LookupBaseDTO> AR = new List<LookupBaseDTO>();
                    List<LookupBaseDTO> Sales = new List<LookupBaseDTO>();
                    List<LookupBaseDTO> COG = new List<LookupBaseDTO>();

                    if (app.AR == null || app.AR == "")
                        AR.Add(new LookupBaseDTO { Name = string.Format("{0} {1}", ContantStrings.ChartOfAccounts.AR, suffix) });
                    else
                        AR.Add(new LookupBaseDTO { Name = app.AR });

                    if (app.Sales == null || app.Sales == "")
                        Sales.Add(new LookupBaseDTO { Name = string.Format("{0} {1}", ContantStrings.ChartOfAccounts.Sales, suffix) });
                    else
                        Sales.Add(new LookupBaseDTO { Name = app.Sales });

                    if (app.COG == null || app.COG == "")
                        COG.Add(new LookupBaseDTO { Name = string.Format("{0} {1}", ContantStrings.ChartOfAccounts.COG, suffix) });
                    else
                        COG.Add(new LookupBaseDTO { Name = app.COG });

                    objCOA.AR = AR;
                    objCOA.Sales = Sales;
                    objCOA.COG = COG;
                    return Json(objCOA);
                }


                Franchise fe = new QBManager().GetFranchise(SessionManager.CurrentFranchise.FranchiseId);
                if (fe != null)
                {
                    AddressTP AddressTP = new QBManager().GetAddress((int)fe.AddressId);
                    fe.Region = AddressTP.CountryCode2Digits;
                }

                if (app != null)
                {
                    var task = new QBSyncTask(app, SessionManager.QbEmail, new QBSyncInfoManager(app.OwnerID), profile, fe);
                    var data = task.GetAllAccount();

                    var AR = data.Where(x => x.accountType == "AccountsReceivable").Select(x => new LookupBaseDTO { Name = x.Name }).ToList();
                    var Sales = data.Where(x => x.accountType == "Income").Select(x => new LookupBaseDTO { Name = x.Name }).ToList();
                    var COG = data.Where(x => x.accountType == "CostofGoodsSold").Select(x => new LookupBaseDTO { Name = x.Name }).ToList();

                    if (!AR.Any(x => x.Name.Contains(string.Format("{0} {1}", ContantStrings.ChartOfAccounts.AR, suffix))))
                        AR.Add(new LookupBaseDTO { Name = string.Format("{0} {1}", ContantStrings.ChartOfAccounts.AR, suffix) });
                    if (!Sales.Any(x => x.Name.Contains(string.Format("{0} {1}", ContantStrings.ChartOfAccounts.Sales, suffix))))
                        Sales.Add(new LookupBaseDTO { Name = string.Format("{0} {1}", ContantStrings.ChartOfAccounts.Sales, suffix) });
                    if (!COG.Any(x => x.Name.Contains(string.Format("{0} {1}", ContantStrings.ChartOfAccounts.COG, suffix))))
                        COG.Add(new LookupBaseDTO { Name = string.Format("{0} {1}", ContantStrings.ChartOfAccounts.COG, suffix) });

                    dynamic objCOA = new ExpandoObject();
                    objCOA.AR = AR;
                    objCOA.Sales = Sales;
                    objCOA.COG = COG;

                    return Json(objCOA);
                }
                else
                {
                    return ResponseResult(ContantStrings.SyncNotEnabled);
                }

            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
        #endregion

        #region PreSyncCheck
        /// <summary>
        /// QB Presync check if all the invoice have valid data
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("PreSyncCheck")]
        public IHttpActionResult PreSyncCheck()
        {
            try
            {
                EventLogger.LogEvent(SessionManager.CurrentFranchise.FranchiseId.ToString(), "QuickbooksController PreSyncCheck call started", LogSeverity.Information);
                QBApps app = new QBManager().GetQBApp(SessionManager.CurrentFranchise.FranchiseId);
                if (app != null)
                {
                    EventLogger.LogEvent(JsonConvert.SerializeObject(app), "PreSyncCheck app check", LogSeverity.Information);
                    var log = QBSyncManager.PreSyncCheck(SessionManager.CurrentFranchise.FranchiseId, app.OwnerID);
                    EventLogger.LogEvent(JsonConvert.SerializeObject(log), "QuickbooksController PreSyncCheck call ended", LogSeverity.Information);
                    return ResponseResult(ContantStrings.Success, Json(log));
                }
                else
                {
                    return ResponseResult(ContantStrings.SyncNotEnabled);
                }

            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }

        }
        #endregion

        #region Online Sync
        /// <summary>
        /// QB Online Sync
        /// </summary>
        /// <returns></returns>
        [HttpPost, Route("Sync")]
        public IHttpActionResult Sync()
        {
            try
            {
                EventLogger.LogEvent(SessionManager.CurrentFranchise.FranchiseId.ToString(), "QB Online Sync call started", LogSeverity.Information);

                //var profile = SessionManager.Profile;
                Profile profile = getProfile();
                if (String.IsNullOrEmpty(profile.accesstoken))
                {
                    throw new QBSyncValidationException("Neet to authorize to QBO!");
                }

                QBApps app = new QBManager().GetQBApp(SessionManager.CurrentFranchise.FranchiseId);
                Franchise fe = new QBManager().GetFranchise(SessionManager.CurrentFranchise.FranchiseId);
                if (fe != null)
                {
                    AddressTP AddressTP = new QBManager().GetAddress((int)fe.AddressId);
                    fe.Region = AddressTP.CountryCode2Digits;

                    if (fe.Region == "CA")
                    {
                        app.TaxRateMapping = new QBManager().GetTaxName(SessionManager.CurrentFranchise.FranchiseId).Where(x => x.QBOTaxName != "").ToList();
                    }

                }

                if (app != null)
                {
                    var task = new QBSyncTask(app, SessionManager.QbEmail, new QBSyncInfoManager(app.OwnerID), profile, fe);
                    task.Start();
                    EventLogger.LogEvent(SessionManager.CurrentFranchise.FranchiseId.ToString(), "QB Online Sync call ended", LogSeverity.Information);
                    return ResponseResult(ContantStrings.Success);
                }
                else
                {
                    return ResponseResult(ContantStrings.SyncNotEnabled);
                }

            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
        #endregion

        #region GetState - Sync Progress and Check the QBO connection
        /// <summary>
        /// Get Current state when QB online syncing
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetState")]
        public IHttpActionResult GetState()
        {
            try
            {
                Profile profile = getProfile();
                //EventLogger.LogEvent(id.ToString(), "QuickbooksController GetState call started", LogSeverity.Information);
                QBApps app = new QBManager().GetQBApp(SessionManager.CurrentFranchise.FranchiseId);
                if (app != null)
                {
                    var jsonResult = Json(QBSyncManager.GetSyncState(app.OwnerID, profile));
                    return ResponseResult(ContantStrings.Success, jsonResult);
                }
                else
                {
                    throw new Exception("Need to Enable QB Sync for this franchise!");
                }

            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
        #endregion

        #region QBO Disconnect - Remove API keys
        /// <summary>
        /// Disconnect QB online
        /// </summary>
        /// <returns></returns>
        [HttpPost, Route("Disconect")]
        public HttpResponseMessage Disconect()
        {
            try
            {
                new QBManager().UpdateQBOSessioninfo(SessionManager.CurrentFranchise.FranchiseId, "");

                HttpResponseMessage response = new HttpResponseMessage();
                response.Content = new ObjectContent<string>(ContantStrings.Success, new JsonMediaTypeFormatter());
                response.Headers.AddCookies(new[] { setQBOsession() });

                //SessionManager.QbEmail = null;
                //SessionManager.Profile = null;
                //return ResponseResult(ContantStrings.Success);
                return response;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }
        #endregion

        #region QBD Get API return sync data
        /// <summary>
        /// QB desktop sync data return
        /// </summary>
        /// <param name="credentials"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost, Route("QBSync")]
        public IHttpActionResult QBSync(QBCredential credentials)
        {
            try
            {
                EventLogger.LogEvent(JsonConvert.SerializeObject(credentials), "QuickbooksController QB desktop sync call started", LogSeverity.Information);
                using (QBSyncManager handler = QBSyncManager.For(credentials))
                {
                    try
                    {
                        object syncResult = handler.Sync();

                        if (syncResult == null)
                            return ResponseResult(ContantStrings.Unauthorized);

                        EventLogger.LogEvent(JsonConvert.SerializeObject(syncResult), "QuickbooksController QB desktop sync call ended", LogSeverity.Information);
                        return ResponseResult(ContantStrings.Success, Json(syncResult));
                    }
                    catch (Exception exception)
                    {
                        EventLogger.LogEvent(exception);
                        return ResponseResult(ContantStrings.FailedByError);
                    }
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
        #endregion

        #region QBO SyncLogs
        /// <summary>
        /// Get Sync Logs
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("SyncLogs")]
        public IHttpActionResult SyncLogs()
        {
            QBApps qbApp = new QBManager().GetQBApp(SessionManager.CurrentFranchise.FranchiseId);

            var synclog = new QBManager().GetSyncLog(qbApp.OwnerID);
            foreach (var s in synclog)
                s.ObjectRef = s.ToString();
            return ResponseResult(ContantStrings.Success, Json(synclog));

        }
        #endregion
    }
}