﻿using HFC.CRM.Data;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    [RoutePrefix("api/Disclaimer")]
    public class DisclaimerController : BaseController
    {
        DisclaimerManager Disclaim_Mangr = new DisclaimerManager();
        /// <summary>
        /// save
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult Post([FromBody]DisclaimerModel model)
        {
            if (model != null)
            {
                var result = Disclaim_Mangr.Save(model);
                return ResponseResult("Success");
            }
            return ResponseResult("Failed");
        }

        /// <summary>
        /// To fetch the value for the view
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("Getvalue")]
        public IHttpActionResult Getvalue()
        {
            var result = Disclaim_Mangr.GetDisclaimer();
            return Json(result);
        }

        /// <summary>
        /// To get value for the edit page
        /// </summary>
        /// <param name="Id">Identifier</param>
        /// <returns></returns>
        [HttpGet, Route("GetDataEdit/{Id}")]
        public IHttpActionResult GetDataEdit(int Id)
        {
            var result = Disclaim_Mangr.GetEdit(0, Id);
            return Json(result);
        }


    }
}
