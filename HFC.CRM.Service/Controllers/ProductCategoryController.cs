﻿using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    /// <summary>
    /// Admin - Product Category Management
    /// </summary>
    public class ProductCategoryController : BaseController
    {


        /// <summary>
        /// Get Product Category if id=0 return all category only active if id>0 return that particular category
        /// </summary>
        /// <param name="ProductCategoryId"></param>
        /// <returns></returns>
        [HttpGet, Route("api/ProductCategory/{ProductCategoryId}")]
        public IHttpActionResult Get(int ProductCategoryId)
        {
            try
            {
                if (ProductCategoryId > 0)
                {
                    var user = Getuser();
                    ProductCategoryManagerTP ProductCategoryMgr = new ProductCategoryManagerTP(user);
                    var result = ProductCategoryMgr.Get((int)ProductCategoryId);
                    return Json(result);
                }
                else
                {
                    throw new Exception("ProductCategory Id should be greater than zero");
                }
            }
            catch (Exception ex)
            {
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// If Inactive = true return all records else only active records
        /// </summary>
        /// <param name="Inactive"></param>
        /// <param name="brandId"></param>
        /// <returns></returns>
        [HttpGet, Route("api/ProductCategory/GetBasedOnBrandAndInactiveFlag/{Inactive}/{brandId}")]
        public IHttpActionResult GetBasedOnBrandAndInactiveFlag(bool Inactive, int brandId)
        {
            try
            {
                if (brandId == 0)
                    throw new Exception("brandId should be greater than zero");

                var user = Getuser();
                ProductCategoryManagerTP ProductCategoryMgr = new ProductCategoryManagerTP(user);

                if (Inactive)
                {
                    var result = ProductCategoryMgr.GetAllProductCategory(brandId);
                    return Json(result);
                }
                else
                {
                    var result = ProductCategoryMgr.GetAllActiveProductCategory(brandId);
                    return Json(result);
                }
            }
            catch (Exception ex)
            {
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// To Add or update product category
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, Route("api/ProductCategory/Post")]
        public IHttpActionResult Post([FromBody]List<Type_ProductCategory> model)
        {

            try
            {
                var user = Getuser();
                ProductCategoryManagerTP ProductCategoryMgr = new ProductCategoryManagerTP(user);
                foreach (var m in model)
                {
                    if (m.ProductCategoryId > 0)
                        ProductCategoryMgr.Update(m);
                    else
                    {
                        if (!string.IsNullOrEmpty(m.Parant_temp) && (m.Parant_temp!="0"))
                        {
                            var parent = model.Where(x => x.temp_id == m.Parant_temp).FirstOrDefault();
                            if (parent == null)
                            {
                                m.Parant = m.Parant;
                            }
                            else
                            //todo null check parent
                            m.Parant = parent.ProductCategoryId;
                            
                        }
                        ProductCategoryMgr.Add(m);
                    }
                        
                }
                return ResponseResult("Success");
            }
            catch (Exception ex)
            {
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Delete Product category soft delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete, Route("api/ProductCategory/Delete")]
        public IHttpActionResult Delete(int id)
        {
            var user = Getuser();
            ProductCategoryManagerTP ProductCategoryMgr = new ProductCategoryManagerTP(user);
            var result = ProductCategoryMgr.Delete((int)id);
            return Json(result);
        }

    }
}
