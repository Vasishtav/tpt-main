﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.DTO;
using HFC.CRM.DTO.SentEmail;
using HFC.CRM.Managers;
using SelectPdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;


namespace HFC.CRM.Service.Controllers
{
    [RoutePrefix("api/Reports")]
    public class ReportsController : BaseController
    {

        /// <summary>
        /// Sales and PurchasingDetail Report
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost, Route("getSalesandPurchasingDetail")]
        public IHttpActionResult getSalesandPurchasingDetail(ReportFilter data)
        {
            try
            {
                ReportManager reportMgr = new ReportManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = reportMgr.getSalesandPurchasingDetail(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// FranchisePerformance SalesAgent Report
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost, Route("getFranchisePerformance_SalesAgent")]
        public IHttpActionResult getFranchisePerformance_SalesAgent(ReportFilter data)
        {
            try
            {
                ReportManager reportMgr = new ReportManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = reportMgr.getFranchisePerformance_SalesAgent(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// FranchisePerformance Territory Report
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost, Route("getFranchisePerformance_Territory")]
        public IHttpActionResult getFranchisePerformance_Territory(ReportFilter data)
        {
            try
            {
                ReportManager reportMgr = new ReportManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = reportMgr.getFranchisePerformance_Territory(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// FranchisePerformance Franchise Report
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost, Route("getFranchisePerformance_Franchise")]
        public IHttpActionResult getFranchisePerformance_Franchise(ReportFilter data)
        {
            try
            {
                ReportManager reportMgr = new ReportManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = reportMgr.getFranchisePerformance_Franchise(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// FranchisePerformance Zip Report
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost, Route("getFranchisePerformance_Zip")]
        public IHttpActionResult getFranchisePerformance_Zip(ReportFilter data)
        {
            try
            {
                ReportManager reportMgr = new ReportManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = reportMgr.getFranchisePerformance_Zip(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// FranchisePerformance Vendor Report
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost, Route("getFranchisePerformance_Vendor")]
        public IHttpActionResult getFranchisePerformance_Vendor(ReportFilter data)
        {
            try
            {
                ReportManager reportMgr = new ReportManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = reportMgr.getFranchisePerformance_Vendor(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// FranchisePerformance ProductCategory Report
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost, Route("getFranchisePerformance_ProductCategory")]
        public IHttpActionResult getFranchisePerformance_ProductCategory(ReportFilter data)
        {
            try
            {
                ReportManager reportMgr = new ReportManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = reportMgr.getFranchisePerformance_ProductCategory(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// SalesSummaryByMonth Report
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost, Route("getSalesSummaryByMonth")]
        public IHttpActionResult getSalesSummaryByMonth(ReportFilter data)
        {
            try
            {
                ReportManager reportMgr = new ReportManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = reportMgr.getSalesSummaryByMonth(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get PIC ProductGroup For Configurator
        /// </summary>
        /// <param name="franchiseCode"></param>
        /// <param name="vendorId"></param>
        /// <returns></returns>
        [HttpGet, Route("GetPICProductGroupForConfigurator")]
        public IHttpActionResult GetPICProductGroupForConfigurator(string franchiseCode, int? vendorId = null)
        {

            if (string.IsNullOrEmpty(franchiseCode))
            {
                franchiseCode = "19001001";
            }
            ReportManager reportMgr = new ReportManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var res = reportMgr.GetPICProductGroupForConfigurator(franchiseCode, vendorId);
            return Json(res);
            //return Json(new { valid = true, PICItems = res });
        }

        /// <summary>
        /// VendorSalesPerformance Report
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost, Route("getVendorSalesPerformance")]
        public IHttpActionResult getVendorSalesPerformance(ReportFilter data)
        {
            try
            {
                ReportManager reportMgr = new ReportManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = reportMgr.getVendorSalesPerformance(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// MarketingPerformance Report
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost, Route("getMarketingPerformance")]
        public IHttpActionResult getMarketingPerformance(ReportFilter data)
        {
            try
            {
                ReportManager reportMgr = new ReportManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = reportMgr.getMarketingPerformance(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// SalesTax Report
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost, Route("getSalesTax")]
        public IHttpActionResult getSalesTax(ReportFilter data)
        {
            try
            {
                ReportManager reportMgr = new ReportManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = reportMgr.getSalesTax(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// SalesTaxSummary Report
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost, Route("getSalesTaxSummary")]
        public IHttpActionResult getSalesTaxSummary(ReportFilter data)
        {
            try
            {
                ReportManager reportMgr = new ReportManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = reportMgr.getSalesTaxSummary(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// SalesSummaryDetail Report
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost, Route("getSalesSummaryDetail")]
        public IHttpActionResult getSalesSummaryDetail(ReportFilter data)
        {
            try
            {
                ReportManager reportMgr = new ReportManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = reportMgr.getSalesSummaryDetail(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// OpenArDetail Report
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost, Route("getOpenArDetail")]
        public IHttpActionResult getOpenArDetail(ReportFilter data)
        {
            try
            {
                ReportManager reportMgr = new ReportManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = reportMgr.getOpenArDetail(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// SalesJournalDetail Report
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost, Route("getSalesJournalDetail")]
        public IHttpActionResult getSalesJournalDetail(ReportFilter data)
        {
            try
            {
                ReportManager reportMgr = new ReportManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = reportMgr.getSalesJournalDetail(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// PPCMatchBack Report
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost, Route("getPPCMatchBack")]
        public IHttpActionResult getPPCMatchBack(ReportFilter data)
        {
            try
            {
                ReportManager reportMgr = new ReportManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = reportMgr.getPPCMatchBack(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// PaymentReport Report
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost, Route("getPaymentReport")]
        public IHttpActionResult getPaymentReport(ReportFilter data)
        {
            try
            {
                ReportManager reportMgr = new ReportManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = reportMgr.getPaymentReport(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// IncomeSummaryReport Report
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost, Route("getIncomeSummaryReport")]
        public IHttpActionResult getIncomeSummaryReport(ReportFilter data)
        {
            try
            {
                ReportManager reportMgr = new ReportManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = reportMgr.getIncomeSummaryReport(data);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        public byte[] MSRReport_PDF(int TerritoryID, DateTime Mth)
        {
            ReportManager reportMgr = new ReportManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var data = reportMgr.getMSRHTML(TerritoryID, Mth);

            HtmlToPdf converter = new HtmlToPdf();

            converter.Options.MarginTop = 20;
            converter.Options.MarginBottom = 30;
            converter.Options.MarginLeft = 10;
            converter.Options.MarginRight = 10;

            converter.Options.DisplayHeader = true;
            converter.Header.DisplayOnFirstPage = true;
            converter.Header.DisplayOnOddPages = true;
            converter.Header.DisplayOnEvenPages = true;

            converter.Options.DisplayFooter = true;
            converter.Footer.DisplayOnFirstPage = true;
            converter.Footer.DisplayOnEvenPages = true;
            converter.Footer.DisplayOnOddPages = true;

            converter.Options.PdfPageSize = PdfPageSize.Letter;

            string baseUrl = HostingEnvironment.MapPath("/Templates/Base/");

            PdfDocument doc = converter.ConvertHtmlString(data, baseUrl);

            // save pdf document
            byte[] pdf = doc.Save();

            // close pdf document
            doc.Close();

            return pdf;
        }

        public string MSRReport_Filename(int TerritoryID, DateTime Mth)
        {
            ReportManager reportMgr = new ReportManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            List<MSR_DM> msrData = reportMgr.getMSRData(TerritoryID, Mth);
            string filename = "";
            if (msrData != null && msrData.Count > 0)
                filename = msrData.Select(x => x.Territory).FirstOrDefault().Split('-').Last().Trim() + "_" + Mth.ToString("MMMyyyy") + "_MSR.pdf";
            else
                filename = Mth.ToString("MMMyyyy") + "_MSR.pdf";
            return filename;
        }

        /// <summary>
        /// MSRReport_PDF
        /// </summary>
        /// <param name="TerritoryID"></param>
        /// <param name="Mth"></param>
        /// <returns></returns>
        [HttpGet, Route("getMSRReport_PDF")]
        public HttpResponseMessage getMSRReport_PDF(int TerritoryID, DateTime Mth)
        {
            try
            {
                var result = new HttpResponseMessage(HttpStatusCode.OK);
                var stream = new MemoryStream(MSRReport_PDF(TerritoryID, Mth));// new FileStream(data.physicalfilePath, FileMode.Open);
                result.Content = new StreamContent(stream);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping("MSR_Report.pdf"));
                return result;

            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }
        }

        /// <summary>
        /// MSRReport_PDFDownload
        /// </summary>
        /// <param name="TerritoryID"></param>
        /// <param name="Mth"></param>
        /// <returns></returns>
        [HttpGet, Route("getMSRReport_PDFDownload")]
        public HttpResponseMessage getMSRReport_PDFDownload(int TerritoryID, DateTime Mth)
        {
            try
            {
                var stream = new MemoryStream(MSRReport_PDF(TerritoryID, Mth));// new FileStream(data.physicalfilePath, FileMode.Open);
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StreamContent(stream);
                response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = MSRReport_Filename(TerritoryID, Mth);
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                return response;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }
        }

        /// <summary>
        /// MSRReport_Email
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost, Route("getMSRReport_Email")]
        public IHttpActionResult getMSRReport_Email(EmailData_MSR data)
        {
            try
            {
                List<string> ccAddresses = new List<string>();
                List<string> bccAddresses = new List<string>();

                if (data.ccAddresses != "")
                    ccAddresses = data.ccAddresses.Split(',').ToList();

                if (data.bccAddresses != "")
                    bccAddresses = data.bccAddresses.Split(',').ToList();

                int? LeadId = null, AccountId = null, OpportunityId = null, OrderId = null;

                ExchangeManager exchange = new ExchangeManager();
                EmailManager emailMgr = new EmailManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

                byte[] pdf = MSRReport_PDF(data.TerritoryID, data.Mth);

                data.subject = HttpUtility.HtmlDecode(data.subject);
                data.body = HttpUtility.HtmlDecode(data.body);

                exchange.SendEmailMultipleAttachment(SessionManager.CurrentUser.Person.PrimaryEmail, data.subject, data.body,
                    data.toAddresses.Split(',').ToList(), ccAddresses,
                     bccAddresses, null, pdf, MSRReport_Filename(data.TerritoryID, data.Mth));


                emailMgr.AddSentEmail(data.toAddresses, data.subject, data.body, true, "", ccAddresses, bccAddresses, SessionManager.CurrentUser.Person.PrimaryEmail, LeadId, AccountId, OpportunityId, null, null, OrderId);
                return Json(true);

            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex.Message);
            }
        }

        /// <summary>
        /// Aging Report
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Route("GetAgingReport")]
        public IHttpActionResult GetAgingReport(int id)
        {
            try
            {
                ReportManager reportMgr = new ReportManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = reportMgr.GetAgingReport();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }
    }
}
