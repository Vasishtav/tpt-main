﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Data.Constants;
using HFC.CRM.Data.SavedCalendar;
using HFC.CRM.Managers;
using HFC.CRM.Managers.AdvancedEmailManager;
using HFC.CRM.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    [RoutePrefix("api/CalTasks")]
    public class CalTasksController : BaseController
    {
        /// <summary>
        /// Read Saved calendar
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("ReadSavedcalendar")]
        public IHttpActionResult ReadSavedcalendar()
        {
            try
            {
                var FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                CalendarManager calmgr = new CalendarManager(SessionManager.CurrentUser);
                var data = calmgr.GetSavedCalendar(FranchiseId);
                return Json(data);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Read Selected Calendar
        /// </summary>
        /// <param name="PersonIds">PersonIds with comma seperator</param>
        /// <param name="start">MM/dd/yyyy</param>
        /// <param name="end">MM/dd/yyyy</param>
        /// <returns></returns>
        [HttpGet, Route("ReadSelection")]
        public IHttpActionResult ReadSelection(string PersonIds, DateTime? start = null, DateTime? end = null)
        {
            try
            {
                if (PersonIds == null || PersonIds == "")
                    return Json(new List<TaskModel>());

                CalendarFilter filter = new CalendarFilter();

                filter.start = start;
                filter.end = end;

                if (filter.personIds == null)
                {
                    filter.personIds = new List<int>();
                    filter.personIds = PersonIds.Split(',').Select(int.Parse).ToList();
                };
                List<CalendarModel> events = null, recurMasters = null;
                List<TaskModel> tasks = new List<TaskModel>();

                string status = "";
                if (!String.IsNullOrEmpty(PersonIds))
                    status = filter.GetCalendar(out events, out recurMasters);

                if (!String.IsNullOrEmpty(PersonIds))
                    filter.GetTasks(out tasks, PersonIds);

                if (tasks.Count() != 0)
                {
                    tasks = tasks.OrderBy(o => o.CreatedOnUtc).ToList();
                }

                var appointmentDic = new Dictionary<string, byte>();
                foreach (var type in SessionManager.CurrentFranchise.AppointmentTypesCollection())
                {
                    appointmentDic.Add(type.Name, Convert.ToByte(type.AppointmentTypeId));
                }

                var appColorMgr = new AppointmentTypeColorManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var appColors = appColorMgr.GetColors();
                if (appColors != null)
                {
                    foreach (var evnt in events)
                    {
                        switch ((int)evnt.AptTypeEnum)
                        {
                            case 1:
                                evnt.AppointmentTypeColor = appColors.Sales_Design;
                                break;
                            case 5:
                                evnt.AppointmentTypeColor = appColors.Installation;
                                break;
                            case 6:
                                evnt.AppointmentTypeColor = appColors.Service;
                                break;
                            case 7:
                                evnt.AppointmentTypeColor = appColors.Followup;
                                break;
                            case 8:
                                evnt.AppointmentTypeColor = appColors.Personal;
                                break;
                            case 9:
                                evnt.AppointmentTypeColor = appColors.Vacation;
                                break;
                            case 10:
                                evnt.AppointmentTypeColor = appColors.Holiday;
                                break;
                            case 11:
                                evnt.AppointmentTypeColor = appColors.Meeting_Training;
                                break;
                            case 12:
                                evnt.AppointmentTypeColor = appColors.TimeBlock;
                                break;
                            default:
                                break;
                        }

                    }
                }

                CalendarManager calmgr = new CalendarManager(SessionManager.CurrentUser);
                if (status == IMessageConstants.Success)
                {
                    {
                        IQueryable<TaskViewModel> caltasks = null;
                        if (tasks != null && tasks.Count > 0)
                        {
                            CalendarManager calmgr1 = new CalendarManager(SessionManager.CurrentUser);
                            caltasks = tasks.ToList().Select(task => new TaskViewModel
                            {
                                TaskID = task.id,
                                PersonId = task.PersonId,
                                Title = task.title,
                                Start = Convert.ToDateTime(task.start),
                                End = Convert.ToDateTime(task.start),
                                StartTimezone = null,
                                EndTimezone = null,
                                Description = task.Message,
                                IsAllDay = task.allDay,
                                RecurrenceRule = null,
                                RecurrenceException = null,
                                RecurrenceID = null,
                                eventType = 1,
                                IsPrivate = task.IsPrivate,
                                ReminderId = task.ReminderMinute,
                                IsTask = true,
                                Location = "Edit Task",
                                LeadId = task.LeadId,
                                AccountId = task.AccountId == null ? 0 : (int)task.AccountId,
                                OpportunityId = task.OpportunityId == null ? 0 : (int)task.OpportunityId,
                                OrderId = task.OrderId == null ? 0 : (int)task.OrderId,
                                CompletedDateUtc = task.CompletedDateUtc,
                                OrganizerPersonId = task.OrganizerPersonId,
                                OwnerName = "",// calmgr.GetOwnerNameById(task.OrganizerPersonId == 0 ? Int32.Parse(task.OrganizerPersonId.ToString()) : 0),
                                UserRoleColor = calmgr.GetTaskBackGroundColor(task.PersonId),
                                UserTextColor = calmgr.GetUserTextColor(task.PersonId),
                                Attendee = CacheManager.UserCollection.FirstOrDefault(f => f.PersonId == task.PersonId).Email,
                                IsPrivateAccess = calmgr.GetAuthorisedPrivateCalendar(task.id, task.IsPrivate, true),
                            }).AsQueryable();
                        }

                        if (calmgr == null) { calmgr = new CalendarManager(SessionManager.CurrentUser); }
                        var calEvents = events.ToList().Select(evnt => new TaskViewModel
                        {
                            TaskID = evnt.id,
                            Title = evnt.title,
                            Start = evnt.start,
                            End = evnt.end,
                            Description = evnt.Message,
                            IsAllDay = evnt.allDay,
                            RecurrenceRule = evnt.RRRule,
                            RecurrenceException = evnt.RecurrenceException,
                            RecurrenceID = evnt.RecurringEventId,
                            AppointmentTypeName = evnt.AppointmentTypeName,
                            CalName = evnt.CalName,
                            Location = evnt.Location,
                            eventType = 2,
                            EventType = evnt.EventType,
                            ReminderId = evnt.ReminderMinute,
                            IsPrivate = evnt.IsPrivate,
                            IsTask = false,
                            LeadId = evnt.LeadId,
                            AccountId = evnt.AccountId == null ? 0 : (int)evnt.AccountId,
                            AccountName = evnt.AccountName,
                            AccountPhone = evnt.AccountPhone,
                            PhoneNumber = evnt.PhoneNumber,
                            OpportunityId = evnt.OpportunityId == null ? 0 : (int)evnt.OpportunityId,
                            OpportunityName = evnt.OpportunityName,
                            OrderId = evnt.OrderId == null ? 0 : (int)evnt.OrderId,
                            OrganizerPersonId = evnt.OrganizerPersonId,
                            opportunity = null,// calmgr.GetEventOpportunities(evnt.id),
                            OwnerName = evnt.OrganizerName,// calmgr.GetOwnerNameById(Int32.Parse(evnt.OrganizerPersonId.ToString())),
                            Attendee = evnt.PersonId == 0 ? null : CacheManager.UserCollection.FirstOrDefault(f => f.PersonId == evnt.PersonId).Email,
                            AttendeeName = evnt.PersonId == 0 ? null : CacheManager.UserCollection.FirstOrDefault(f => f.PersonId == evnt.PersonId).Person.FullName,
                            AppointmentTypeColor = evnt.AppointmentTypeColor,// calmgr.GetTaskBackGroundColor(evnt.PersonId),
                            UserRoleColor = calmgr.GetTaskBackGroundColor(evnt.PersonId),
                            UserTextColor = calmgr.GetUserTextColor(evnt.PersonId),
                            IsPrivateAccess = calmgr.GetAuthorisedPrivateCalendar(evnt.id, evnt.IsPrivate, false),
                            TimeSlot = (int)TimeSlot(evnt.start.ToString(), evnt.end.ToString()),
                        }).AsQueryable();

                        if (caltasks != null)
                        {
                            var mergecalevent = caltasks.Union(calEvents);
                            return Json(mergecalevent);
                        }
                        else
                        {
                            return Json(calEvents);
                        }
                    }
                }
                return Json(tasks);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Save Calendar
        /// </summary>
        /// <param name="savecalendar">Model</param>
        /// <returns></returns>
        [HttpPost, Route("SaveCalendar")]
        public IHttpActionResult SaveCalendar(SaveCalendar savecalendar)
        {
            try
            {
                CalendarManager calmgr = new CalendarManager(SessionManager.CurrentUser);
                var status = calmgr.SaveCalendar(savecalendar);
                return Json(status);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Managed Calendar List by UserId
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetManagedCalendarListbyUserId")]
        public IHttpActionResult GetManagedCalendarListbyUserId()
        {
            try 
            {
                int id = SessionManager.CurrentUser.PersonId;
                CalendarManager calmgr = new CalendarManager(SessionManager.CurrentUser);
                var data = calmgr.GetManagedCalendarListbyUserId(id);
                return Json(data);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Update Saved Calendar
        /// </summary>
        /// <param name="saveorder">Model</param>
        /// <returns></returns>
        [HttpPut, Route("UpdateSavedCalendar")]
        public IHttpActionResult UpdateSavedCalendar(List<SavedCalendarOrder> saveorder)
        {
            try
            {
                CalendarManager calmgr = new CalendarManager(SessionManager.CurrentUser);
                var result = calmgr.UpdateSavedCalendar(saveorder);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        double TimeSlot(string start, string end)
        {
            try
            {
                var startspan = TimeSpan.Parse(DateTime.Parse(start).TimeOfDay.ToString());
                var endspan = TimeSpan.Parse(DateTime.Parse(end).TimeOfDay.ToString());

                TimeSpan result = endspan - startspan;
                var diffInMinutes = result.TotalMinutes;
                return diffInMinutes;
            }
            catch (Exception e)
            {
                return 0;
            }

        }
    }
}