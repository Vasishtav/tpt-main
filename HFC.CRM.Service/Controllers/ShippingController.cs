﻿using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    [RoutePrefix("api/Shipping")]
    public class ShippingController : BaseController
    {
        /// <summary>
        /// Get Shipping info
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult Get()
        {
            ShippingManager ShippingMgr = new ShippingManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var ships = ShippingMgr.Get();
            return Json(ships);
        }

        /// <summary>
        /// Get Vendor Value
        /// </summary>
        /// <param name="id"></param>
        /// <param name="VendorId"></param>
        /// <param name="Status"></param>
        /// <returns></returns>
        [HttpGet,Route("GetValue/{id}/{VendorId}/{Status}")]
        public IHttpActionResult GetValue(int id, int VendorId, string Status)
        {
            ShippingManager ShippingMgr = new ShippingManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var ships = ShippingMgr.GetVendor(id, VendorId, Status);
            return Json(ships);
        }

        /// <summary>
        /// Get Vendor Value
        /// </summary>
        /// <param name="id"></param>
        /// <param name="VendorId"></param>
        /// <param name="FranchiseID"></param>
        /// <param name="Status"></param>
        /// <returns></returns>
        [HttpGet, Route("GetValue/{id}/{VendorId}/{FranchiseID}/{Status}")]
        public IHttpActionResult GetValue(int id, int VendorId, int FranchiseID, string Status)
        {
            ShippingManager ShippingMgr = new ShippingManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var ships = ShippingMgr.GetVendor(id, VendorId, FranchiseID, Status);
            return Json(ships);
        }

        /// <summary>
        /// Save ShipToLocation
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost,Route("SaveShipToLocation")]
        public IHttpActionResult SaveShipToLocation([FromBody]ShipToLocations data)
        {
            ShippingManager ShippingMgr = new ShippingManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var updateStatus = "";
            if (data.Id > 0)
            {
                updateStatus = ShippingMgr.Update(data);
            }
            else
                updateStatus = ShippingMgr.Add(data);
            return ResponseResult(updateStatus);
        }

        /// <summary>
        /// Activate Deactivate Ship
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost,Route("ActivateDeactivateShip")]
        public IHttpActionResult ActivateDeactivateShip(ShipToLocations data)
        {
            ShippingManager ShippingMgr = new ShippingManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var updateStatus = ShippingMgr.ActivateDeactivateShipLocation(data);
            return ResponseResult(updateStatus);
        }

        /// <summary>
        /// Duplicate ShippingName
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpGet,Route("DuplicateShippingName/{id}/{value}")]
        public IHttpActionResult DuplicateShippingName(int id, string value)
        {
            ShippingManager ShippingMgr = new ShippingManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var status = ShippingMgr.DuplicateShippingName(id, value);
            return Ok(new { status = status });
        }

        /// <summary>
        /// Get BP Value
        /// </summary>
        /// <param name="id"></param>
        /// <param name="VendorId"></param>
        /// <returns></returns>
        [HttpGet, Route("GetBPValue/{id}/{VendorId}")]
        public IHttpActionResult GetBPValue(int id, int VendorId)
        {
            ShippingManager ShippingMgr = new ShippingManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var ships = ShippingMgr.GetBP(id, VendorId);
            return Json(ships);
        }
    }
}