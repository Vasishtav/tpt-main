﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    [RoutePrefix("api/ProductTP")]
    public class ProductTPController : BaseController
    {
        ProductManagerTP Prdt_Mngr = new ProductManagerTP(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        /// <summary>
        /// To save the product
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult Post([FromBody]FranchiseProducts model)
        {
            if (SessionManager.CurrentFranchise != null)
            {
                FranchiseDiscount Fran_Dis = new FranchiseDiscount();
                if (model.ProductType == 4 & model.ProductID != 0)
                {
                    Fran_Dis.DiscountId = model.ProductID;
                    Fran_Dis.Name = model.ProductName;
                    Fran_Dis.Description = model.Description;
                    Fran_Dis.Status = model.ProductStatus;
                    Fran_Dis.DiscountValue = model.Discount;
                    if (model.DiscountType == "%")
                        model.DiscountType = "1";
                    Fran_Dis.DiscountFactor = model.DiscountType;
                    var Status = Prdt_Mngr.SaveDiscount(Fran_Dis);
                    return ResponseResult("Success", Json(new { Status }));
                }
                else if (model.ProductID != 0)
                {
                    //model.ProductSurfaceSetupForCC.
                    if (model.VendorName == "")
                    {
                        model.VendorName = model.VendorID.ToString();
                    }
                    var Status = Prdt_Mngr.Save(model);
                    return ResponseResult("Success", Json(new { Status }));
                }
            }
            return ResponseResult("Failed");
        }

        /// <summary>
        /// To Generate the product id automatically
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetProductNoNew")]
        public Nullable<int> GetProductNoNew()
        {
            var data = Prdt_Mngr.GetProductNumberNew();
            return data;
        }

        /// <summary>
        /// list to kendo
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetSearchProducts")]
        public IHttpActionResult GetSearchProducts()
        {
            var result = Prdt_Mngr.GetListData();
            return Json(result);
        }

        /// <summary>
        /// For the view for single product
        /// </summary>
        /// <param name="Productkey"></param>
        /// <param name="FranchiseId"></param>
        /// <returns></returns>
        [HttpGet, Route("Get/{Productkey}/{FranchiseId}")]
        public IHttpActionResult Get(int Productkey, int FranchiseId)
        {
            var result = Prdt_Mngr.GetProduct(Productkey, FranchiseId);
            return Json(result);

        }

        /// <summary>
        /// For the edit page the data is been listed
        /// </summary>
        /// <param name="ProductId"></param>
        /// <param name="FranchiseId"></param>
        /// <returns></returns>
        [HttpGet, Route("GetParticular/{ProductId}/{FranchiseId}")]
        public IHttpActionResult GetParticular(int ProductId, int FranchiseId)
        {
            var result = Prdt_Mngr.GetDetails(ProductId, FranchiseId);
            return Json(result);
        }

        /// <summary>
        /// Dropdown selection for the product and list the details
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Route("ProductOption/{id}")]
        public IHttpActionResult ProductOption(int id)
        {
            var result = Prdt_Mngr.GetListDetails(id);
            return Json(result);
        }

        /// <summary>
        /// To get all product when checkbox is clicked
        /// </summary>
        /// <param name="id"></param>
        /// <param name="DropValue"></param>
        /// <returns></returns>
        [HttpGet, Route("GetallPrdtDetails/{id}/{DropValue}")]
        public IHttpActionResult GetallPrdtDetails(int id, int DropValue)
        {
            var result = Prdt_Mngr.GetListDataAll(id, DropValue);
            return Json(result);
        }

        /// <summary>
        /// Refresh Products From HFCList
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Route("RefreshProductsFromHFCList/{id}")]
        public IHttpActionResult RefreshProductsFromHFCList(int id)
        {
            var result = Prdt_Mngr.GetRefreshProductsFromHFCList(id);
            return Json(result);
        }

        /// <summary>
        /// To get core producttype
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetProductType")]
        public IHttpActionResult GetProductType()
        {
            var result = Prdt_Mngr.GetProductType();
            return Json(result);
        }

        /// <summary>
        /// Save Temp
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, Route("SaveTemp")]
        public IHttpActionResult SaveTemp([FromBody] FranchiseProducts model)
        {
            if (model.ProductID != 0)
            {
                var Id = Prdt_Mngr.SaveTempdata(model);
                return Json(Id);
            }
            return null;
        }

        /// <summary>
        /// Get TempProduct
        /// </summary>
        /// <param name="id"></param>
        /// <param name="Guid"></param>
        /// <returns></returns>
        [HttpGet, Route("GetTempProduct/{Guid}")]
        public IHttpActionResult GetTempProduct(int id, string Guid)
        {
            if (Guid != null || Guid != "")
            {
                var ProductData = Prdt_Mngr.GetTempProd(Guid);
                return Json(ProductData);
            }
            return null;
        }

        /// <summary>
        /// Get the Product History Price data by Product Key
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Route("GetHistoryProductPricingLog/{id}")]
        public IHttpActionResult GetHistoryProductPricingLog(int id)
        {
            try
            {
                var PricingLog = Prdt_Mngr.GetHistoryProductPricingLog(id);
                return Json(PricingLog);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get Material For TLorCC JobSurfacing
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetMaterialForTLorCCJobSurfacing")]
        public IHttpActionResult GetMaterialForTLorCCJobSurfacing()
        {
            try
            {
                List<JObSurfacing> temp = new List<JObSurfacing>();
                var res = Prdt_Mngr.GetMaterialForTLorCCJobSurfacing();
                foreach (var obj in res)
                {
                    JObSurfacing ms = new JObSurfacing();
                    ms.ProductKey = obj.ProductKey;
                    ms.ProductID = obj.ProductID;
                    ms.ProductName = obj.ProductID.ToString() + " - " + obj.ProductName;
                    ms.Description = obj.Description;
                    ms.MultipleSurfaceProductSet = JsonConvert.DeserializeObject<List<MultipleSurfaceProduct>>(obj.MultipleSurfaceProductSet);
                    temp.Add(ms);
                }
                return Json(temp);

            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get Labor For TLorCC JobSurfacing
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetLaborForTLorCCJobSurfacing")]
        public IHttpActionResult GetLaborForTLorCCJobSurfacing()
        {
            try
            {
                List<JObSurfacing> temp = new List<JObSurfacing>();
                var res = Prdt_Mngr.GetLaborForTLorCCJobSurfacing();
                foreach (var obj in res)
                {
                    JObSurfacing ms = new JObSurfacing();
                    ms.ProductKey = obj.ProductKey;
                    ms.ProductID = obj.ProductID;
                    ms.ProductName = obj.ProductName;
                    ms.Description = obj.Description;
                    if (obj.Cost != null) ms.Cost = obj.Cost; else ms.Cost = 0;
                    if (obj.SalePrice != null) ms.SalePrice = obj.SalePrice; else obj.SalePrice = 0;
                    // if(obj.MultipleSurfaceProductSet != null)
                    // ms.MultipleSurfaceProductSet = JsonConvert.DeserializeObject<List<MultipleSurfaceProduct>>(obj.MultipleSurfaceProductSet);

                    ProductSurfaceSetup pss = new ProductSurfaceSetup();
                    // pss = JsonConvert.DeserializeObject<ProductSurfaceSetup>(obj.SurfaceLaborSet);
                    //dynamic d = JObject.Parse(obj.SurfaceLaborSet);
                    //pss.MaximumJobSize = d.MaximumJobSize;
                    //pss.MinimumJobSize = d.MinimumJobSize;
                    //if (SessionManager.CurrentFranchise.BrandId == 3)
                    //{
                    //    if (d.ProductSurfaceSetupForCC != null)
                    //        pss.ProductSurfaceSetupForCC = JsonConvert.DeserializeObject<List<ProductSurfaceSetupForCC>>(d.ProductSurfaceSetupForCC);
                    //}
                    //else if (SessionManager.CurrentFranchise.BrandId == 2)
                    //{
                    //    if(d.ProductSurfaceSetupForTL != null)
                    //    pss.ProductSurfaceSetupForTL = JsonConvert.DeserializeObject<List<ProductSurfaceSetupForTL>>(d.ProductSurfaceSetupForTL);
                    //}

                    ms.ProductSurfaceSetup = JsonConvert.DeserializeObject<ProductSurfaceSetup>(obj.SurfaceLaborSet);
                    temp.Add(ms);
                }
                return Json(temp);

            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

    }
}


public class JObSurfacing
{
    public int ProductKey { get; set; }
    public int ProductID { get; set; }
    public string ProductName { get; set; }
    public string Description { get; set; }
    public decimal? Cost { get; set; }
    public decimal? SalePrice { get; set; }
    public List<MultipleSurfaceProduct> MultipleSurfaceProductSet { get; set; }
    public ProductSurfaceSetup ProductSurfaceSetup { get; set; }

}

//public class laborForJObSurfacing
//{
//    public int ProductKey { get; set; }
//    public int ProductID { get; set; }
//    public int Cost { get; set; }
//    public int UnitPrice { get; set; }
//    public string ProductName { get; set; }
//    public string Description { get; set; }
//    public List<MultipleSurfaceProduct> MultipleSurfaceProductSet { get; set; }
//}
