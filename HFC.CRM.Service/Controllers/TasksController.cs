﻿using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Newtonsoft.Json;
using HFC.CRM.Core.Logs;
using System.Net.Mail;
using HFC.CRM.Service.Models;

namespace HFC.CRM.Service.Controllers
{
    [RoutePrefix("api/Tasks")]
    public class TasksController : BaseController
    {
        //public class TaskFilter
        //{
        //    public List<int> jobIds { get; set; }
        //    public List<int> personIds { get; set; }
        //    public int? pageIndex { get; set; }
        //    public int? pageSize { get; set; }
        //    public DateTimeOffset? createdOnStart { get; set; }
        //    public DateTimeOffset? createdOnEnd { get; set; }
        //    public int? TaskId { get; set; }
        //}

        /// <summary>
        /// GET Task
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        //[HttpGet, Route("")]
        //public IHttpActionResult Get([FromUri]TaskFilter filter)
        //{
        //    try
        //    {
        //        TaskManager TaskMgr = new TaskManager(SessionManager.CurrentUser);
        //        int totalRecords = 0;
        //        List<TaskModel> result = null;
        //        if (filter != null && ((filter.personIds != null && filter.personIds.Count > 0)))
        //        {
        //            var tasks = TaskMgr.GetTasks(out totalRecords, filter.personIds, filter.pageIndex, filter.pageSize ?? 25, filter.createdOnStart != null ? Convert.ToDateTime((DateTimeOffset)filter.createdOnStart) : DateTime.Now, filter.createdOnEnd != null ? Convert.ToDateTime((DateTimeOffset)filter.createdOnEnd) : DateTime.Now);
        //            if (tasks != null)
        //            {
        //                result = tasks.Select(task => task.ToTaskModel()).ToList();
        //            }
        //        }
        //        List<Task> result1 = null;
        //        if (filter.TaskId > 0)
        //        {
        //            result1 = CRMDBContext.Tasks.ToList();
        //            result1 = result1.Where(t => t.TaskId == filter.TaskId).ToList();
        //            return Json(new
        //            {
        //                TotalRecords = totalRecords,
        //                TaskData = result1
        //            });
        //        }

        //        return Json(new
        //        {
        //            TotalRecords = totalRecords,
        //            TaskData = result
        //        });
        //    }
        //    catch (Exception ex)
        //    {
        //        return BadRequest(ex.Message);
        //    }
        //}

        /// <summary>
        /// create new tasks and edit the tasks
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, Route("")]
        public IHttpActionResult Post([FromBody]TaskModel model)
        {
            string warning = null;

            if (model != null)
            {
                try
                {
                    TaskManager TaskMgr = new TaskManager(SessionManager.CurrentUser);
                    int taskId = 0;
                    var task = model.ToTask(SessionManager.CurrentFranchise.FranchiseId);
                    if (task.AssignedPersonId == null)
                    {
                        task.AssignedPersonId = SessionManager.CurrentUser.PersonId;
                        task.AssignedName = SessionManager.CurrentUser.UserName;
                    }
                    //task.CreatedOnUtc = ConvertToFranchiseDateTime(DateTime.Parse(model.start.ToString()));
                    task.CreatedOnUtc = DateTime.UtcNow;
                    task.CreatedByPersonId = SessionManager.CurrentUser.PersonId;
                    task.LastUpdatedOnUtc = DateTime.UtcNow;
                    task.LastUpdatedPersonId = SessionManager.CurrentUser.PersonId;
                    task.DueDate = model.end;
                    if (TaskMgr == null) { TaskMgr = new TaskManager(SessionManager.CurrentUser); }

                    //   task.DueDate = ConvertToFranchiseDateTime(DateTime.Parse(task.DueDate.ToString()));
                    // task.DueDate = TimeZoneManager.ToUTC(DateTime.Parse(task.DueDate.Value.DateTime.ToString()));
                    var status = TaskMgr.Create(out taskId, task, out warning);
                    if (status == TaskManager.Success && taskId > 0)
                        return Json(new { Id = taskId, warning });
                    else
                        return BadRequest(status);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else
                return BadRequest();
        }

        //protected DateTime ConvertToFranchiseDateTime(DateTime date)
        //{
        //    string FrianchiseTimeZone = SessionManager.CurrentFranchise.TimezoneCode.ToString();
        //    decimal franchiseOffSetTime = CacheManager.TimeZoneCollection.Where(t => t.TimeZone.Contains(FrianchiseTimeZone)).SingleOrDefault().OffSetTime;
        //    DateTime val = date.AddSeconds(double.Parse(franchiseOffSetTime.ToString()) * (-1));
        //    return val;

        //}

        /// <summary>
        /// updating tasks
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        //[HttpPut, Route("")]
        //public IHttpActionResult Put(int id, [FromBody]TaskModel model)
        //{
        //    string warning = null;
        //    string status = "";
        //    TaskManager TaskMgr = new TaskManager(SessionManager.CurrentUser);
        //    if (model != null && id > 0)
        //    {
        //        try
        //        {
        //            var task = model.ToTask(SessionManager.CurrentFranchise.FranchiseId, id);
        //            task.LastUpdatedOnUtc = DateTime.Now;
        //            task.LastUpdatedPersonId = SessionManager.CurrentUser.PersonId;
        //            //EventToPerson eventperson = new EventToPerson();
        //            List<EventToPerson> eventperosn = new List<EventToPerson>(model.AttendeesTP.Count);
        //            if (model.Attendees == null)
        //            {
        //                //  model.Attendees = model.AttendeesTP;
        //                foreach (var attendee in model.AttendeesTP)
        //                {
        //                    var EventToPerson = new EventToPerson
        //                    {
        //                        PersonId = attendee.OwnerID,
        //                        PersonName = attendee.Name,
        //                        TaskId = task.TaskId
        //                    };
        //                    // model.Attendees.Take(1);
        //                    // model.Attendees.Add(EventToPerson);
        //                    eventperosn.Add(EventToPerson);
        //                }
        //                task.Attendees = eventperosn;
        //            }
        //            if (TaskMgr == null) { TaskMgr = new TaskManager(SessionManager.CurrentUser); }
        //            status = TaskMgr.Update(task, out warning);
        //        }
        //        catch (Exception ex)
        //        {
        //            return BadRequest(ex.Message);
        //        }
        //    }
        //    else
        //        status = "Invalid task data";

        //    return status == "Success"
        //        ? ResponseResult(status, Json(new { warning, task = TaskMgr.GetTask(id, SessionManager.CurrentFranchise.FranchiseId) }))
        //        : ResponseResult(status, Json(new { warning }));
        //}

        /// <summary>
        /// Delete Task
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete, Route("")]
        public IHttpActionResult Delete(int? id = null)
        {
            TaskManager TaskMgr = new TaskManager(SessionManager.CurrentUser);
            string result = "";
            if (id.HasValue)
            {
                result = TaskMgr.DeleteTasks(id.Value);
            }
            else //remove all completed if theres no id
            {
                result = TaskMgr.ClearCompleted();
            }


            return Json(result);
        }

        /// <summary>
        /// Mark as Complete
        /// </summary>
        /// <param name="id"></param>
        /// <param name="IsComplete"></param>
        /// <returns></returns>
        [HttpGet, Route("Mark/{id}")]
        public IHttpActionResult Mark(int id, [FromUri]bool IsComplete = false)
        {
            TaskManager TaskMgr = new TaskManager(SessionManager.CurrentUser);
            var status = TaskMgr.MarkComplete(id, IsComplete);

            return ResponseResult(status);
        }
        
    }
}
