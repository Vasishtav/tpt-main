﻿using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.DTO.PIC;
using HFC.CRM.Managers;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Runtime.Caching;
using System.Web;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    [RoutePrefix("api/PIC")]
    public class PICController : BaseController
    {
        PICManager picMgr = new PICManager();
        Stopwatch start = new Stopwatch();

        public static string PICToken()
        {
            string CacheKey = "PICToken";
            ObjectCache cache = MemoryCache.Default;

            if (cache.Contains(CacheKey))
                return cache.Get(CacheKey).ToString();
            else
            {
                var authMgr = new OAuthManager();
                var picToken = authMgr.GetAccessToken();
                CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
                cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddMinutes(55);
                cache.Add(CacheKey, picToken.token.access_token, cacheItemPolicy);

                return picToken.token.access_token;
            }
        }

        /// <summary>
        /// Get PIC Product Group
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetPICProductGroup")]
        public IHttpActionResult GetPICProductGroup()
        {
            var prdMgr = new ProductManagerTP();
            var res = prdMgr.GetAllPICBBProducts();
            var rep = JsonConvert.DeserializeObject<dynamic>(res);
            return Json(rep);
        }

        /// <summary>
        /// Get PIC Product Group For Configurator
        /// </summary>
        /// <param name="franchiseCode"></param>
        /// <param name="vendorConfig"></param>
        /// <returns></returns>
        [HttpGet, Route("GetPICProductGroupForConfigurator")]
        public IHttpActionResult GetPICProductGroupForConfigurator(string franchiseCode, bool vendorConfig = false)
        {
            var prdMgr = new ProductManagerTP();
            if (string.IsNullOrEmpty(franchiseCode))
                franchiseCode = "19001001";
            var res = prdMgr.GetPICProductGroupForConfigurator(franchiseCode, vendorConfig);
            return Json(res);
            //return Json(new { valid = true, PICItems = res });
        }

        /// <summary>
        /// Clear Model Cache
        /// </summary>
        /// <returns></returns>
        [System.Web.Http.AllowAnonymous]
        [HttpGet, Route("ClearModelCache")]
        public IHttpActionResult ClearModelCache()
        {
            List<string> cacheKeys = MemoryCache.Default.Select(x => x.Key).ToList().Where(x => x.Contains("GetPICProductGroup_")).ToList();
            foreach (string cacheKey in cacheKeys)
            {
                MemoryCache.Default.Remove(cacheKey);
            }
            return Json(true);
        }

        /// <summary>
        /// Create Model Cache
        /// </summary>
        /// <returns></returns>
        [System.Web.Http.AllowAnonymous]
        [HttpGet, Route("CreateModelCache")]
        public IHttpActionResult CreateModelCache()
        {
            var prdMgr = new ProductManagerTP();
            prdMgr.CreateModelCache();
            return Json(true);
        }

        /// <summary>
        /// Get Datasource URL
        /// </summary>
        /// <param name="productid"></param>
        /// <param name="modelId"></param>
        /// <returns></returns>
        [HttpGet, Route("GetDatasourceURL")]
        public IHttpActionResult GetDatasourceURL(string productid, string modelId)
        {
            var dataSource = picMgr.GetDatasourceURL(productid, modelId);
            return Json(dataSource);
        }

        /// <summary>
        /// Get DataSource
        /// </summary>
        /// <param name="productid"></param>
        /// <param name="modelId"></param>
        /// <returns></returns>
        [HttpGet, Route("GetDataSource")]
        public IHttpActionResult GetDataSource(string productid, string modelId)
        {
            var prdInfo = PICManager.GetProductInfo(productid);
            var dataSource = PICManager.GetDataSource(productid, modelId);
            return Json(new { productInfo = prdInfo, dataSource = dataSource });
        }

        /// <summary>
        /// Get DataSource And PromptAnswers
        /// </summary>
        /// <param name="productid"></param>
        /// <param name="modelId"></param>
        /// <returns></returns>
        [HttpGet, Route("GetDataSourceAndPromptAnswers")]
        public IHttpActionResult GetDataSourceAndPromptAnswers(string productid, string modelId)
        {
            //var prdInfo = PICManager.GetProductInfo(productid);
            var dsurl = picMgr.GetDatasourceURL(productid, modelId);
            var ppa = PICManager.PICProductPromptAnswers(productid, modelId, SessionManager.CurrentFranchise.Code);
            return Json(new { productInfo = "", dataSource = "", promptAnswers = ppa, dsurl = dsurl });
        }

        /// <summary>
        /// Get DataSource And PromptAnswers By FranchiseId
        /// </summary>
        /// <param name="productid"></param>
        /// <param name="modelId"></param>
        /// <param name="franchiseCode"></param>
        /// <returns></returns>
        [HttpGet, Route("GetDataSourceAndPromptAnswersByFranchiseId")]
        public IHttpActionResult GetDataSourceAndPromptAnswersByFranchiseId(string productid, string modelId, string franchiseCode)
        {
            var prdInfo = PICManager.GetProductInfo(productid);
            var dsurl = picMgr.GetDatasourceURL(productid, modelId);
            if (string.IsNullOrEmpty(franchiseCode))
            {
                franchiseCode = "19001001";
            }
            var ppa = PICManager.PICProductPromptAnswers(productid, modelId, franchiseCode);
            return Json(new { productInfo = prdInfo, dataSource = "", promptAnswers = ppa, dsurl = dsurl });
        }

        /// <summary>
        /// Get Models
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        [HttpGet, Route("GetModels")]
        public IHttpActionResult GetModels(string productId)
        {
            var PICModels = PICManager.GetModels(productId);
            if (PICModels.Count > 0)
            {
                return Json(new { error = "", Data = PICModels });
            }
            else
            {
                return Json(new { error = "An error occurred." });
            }
        }

        /// <summary>
        /// PIC Datasource By Product
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        [HttpGet, Route("PICDatasourceByProduct")]
        public IHttpActionResult PICDatasourceByProduct(string productId)
        {
            var pdbp = PICManager.PICDatasourceByProduct(productId);
            return Json(pdbp);
        }

        /// <summary>
        /// PIC Product PromptAnswers
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="modelId"></param>
        /// <returns></returns>
        //[System.Web.Http.AcceptVerbs("GET")]
        [HttpGet]
        [Route("PICProductPromptAnswers")]
        public IHttpActionResult PICProductPromptAnswers(string productId, string modelId)
        {
            var ppa = PICManager.PICProductPromptAnswers(productId, modelId, SessionManager.CurrentFranchise.Code);
            var dsurl = picMgr.GetDatasourceURL(productId, modelId);
            return Json(new { ppa = ppa, dsurl = dsurl });
        }

        /// <summary>
        /// PIC validate PromptAnswers
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="fCode"></param>
        /// <returns></returns>
        //[System.Web.Http.AcceptVerbs("POST")]
        [System.Web.Http.HttpPost]
        [Route("PICvalidatePromptAnswers")]
        public IHttpActionResult PICvalidatePromptAnswers([FromBody]CoreProductVM obj, string fCode)
        {
            string content = "";
            try
            {
                string url = AppConfigManager.PICUrl;
                /// Configurator / PromptAnswers / Validate /{ customerId}/{ productId}/{ modelId}/{ colorId}
                //TODO: for now hard code the franchisecode as these will be later changed as Franchisecode by PIC
                if (string.IsNullOrEmpty(fCode))
                {
                    fCode = "19001001";
                }
                //string FranchiseCode = SessionManager.CurrentFranchise.Code;
                var api = url + "/Configurator/PromptAnswers/Validate/" + fCode + "/" + obj.PicProduct + "/" + obj.ModelId + "?access_token=" + PICToken();
                var client = new RestClient(api);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");

                request.AddHeader("content-type", "application/x-www-form-urlencoded");
                request.AddParameter("application/x-www-form-urlencoded", "promptAnswers=" + obj.PromptAnswers, ParameterType.RequestBody);
                start.Start();
                IRestResponse response = client.Execute(request);
                start.Stop();
                EventLogger.LogRequest(request, response, start.ElapsedMilliseconds, "PIC", "Validate");
                content = response.Content;
                dynamic responseObj = JsonConvert.DeserializeObject<ExpandoObject>(response.Content);

                return Json(new { valid = true, data = responseObj });
            }
            catch (Exception ex)
            {
                string msg = "";
                if (ex.Message ==
                    "Unexpected character encountered while parsing value: <. Path '', line 0, position 0.")
                {
                    msg = content;
                }
                else
                {
                    msg = ex.Message;
                }
                return Json(new { valid = false, messages = msg });
            }
        }

        /// <summary>
        /// validate PromptAnswers
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        //[System.Web.Http.AcceptVerbs("POST")]
        [System.Web.Http.HttpPost]
        [Route("validatePromptAnswers")]
        public IHttpActionResult validatePromptAnswers([FromBody]CoreProductVM obj)
        {
            string content = "";
            try
            {
                QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                string url = AppConfigManager.PICUrl;
                /// Configurator / PromptAnswers / Validate /{ customerId}/{ productId}/{ modelId}/{ colorId}
                //TODO: for now hard code the franchisecode as these will be later changed as Franchisecode by PIC
                string FranchiseCode = SessionManager.CurrentFranchise.Code;
                var api = url + "/Configurator/PromptAnswers/Validate/" + FranchiseCode + "/" + obj.PicProduct + "/" + obj.ModelId + "?access_token=" + PICToken();
                var client = new RestClient(api);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");

                request.AddHeader("content-type", "application/x-www-form-urlencoded");
                request.AddParameter("application/x-www-form-urlencoded", "promptAnswers=" + obj.PromptAnswers, ParameterType.RequestBody);
                start.Start();
                IRestResponse response = client.Execute(request);
                start.Stop();
                EventLogger.LogRequest(request, response, start.ElapsedMilliseconds, "PIC", "Validate");
                content = response.Content;
                dynamic responseObj = JsonConvert.DeserializeObject<ExpandoObject>(response.Content);

                if (Convert.ToBoolean(responseObj.valid) == true && responseObj.messages.errors != null && responseObj.messages.errors.Count == 0)
                {
                    if (Convert.ToDecimal(responseObj.cost) == 0 || Convert.ToDecimal(responseObj.list) == 0)
                    {
                        return Json(new
                        {
                            valid = false,
                            message = "Price Validation has failed on this product configuration. Please contact your system administrator.",
                            data = responseObj,
                            QuoteKey = obj.QuoteKey,
                            OpportunityId = obj.OpportunityId
                        });
                    }
                    else
                    {
                        var res = QuotesMgr.UpdateCoreProductQuoteconfiguration(obj, responseObj);
                    }
                }

                return Json(new { valid = true, data = responseObj, QuoteKey = obj.QuoteKey, OpportunityId = obj.OpportunityId });
            }
            catch (Exception ex)
            {
                string msg = "";
                if (ex.Message ==
                    "Unexpected character encountered while parsing value: <. Path '', line 0, position 0.")
                {
                    msg = content;
                }
                else
                {
                    msg = ex.Message;
                }
                return Json(new { valid = false, messages = msg });
            }
        }

        /// <summary>
        /// Get Vendor and Product information into PIC service
        /// </summary>
        [System.Web.Http.AllowAnonymous]
        //[System.Web.Http.AcceptVerbs("GET")]
        [System.Web.Http.HttpGet]
        [Route("PICVendorProduct")]
        public IHttpActionResult PICVendorProduct()
        {
            picMgr.PICVendorProduct();
            return Json("Success");
        }
               
        /// <summary>
        /// Get Vendor and Product information into PIC service
        /// </summary>
        [System.Web.Http.AllowAnonymous]
        //[System.Web.Http.AcceptVerbs("GET")]
        [System.Web.Http.HttpGet]
        [Route("BatchVendorCustomer")]
        public IHttpActionResult BatchVendorCustomer()
        {
            picMgr.BatchVendorCustomer();
            return Json("Success");
        }

        /// <summary>
        /// Sync All VendorCustomer AccountNumbers
        /// </summary>
        /// <returns></returns>
        [System.Web.Http.AllowAnonymous]
        [System.Web.Http.HttpPost, Route("SyncAllVendorCustomerAccountNumbers")]
        public IHttpActionResult SyncAllVendorCustomerAccountNumbers()
        {

            var file = HttpContext.Current.Request.Files.Count > 0 ?
                HttpContext.Current.Request.Files[0] : null;

            var arr = new List<string>();
            DataTable dt = new DataTable();
            using (StreamReader sr = new StreamReader(file.InputStream))
            {
                string[] headers = sr.ReadLine().Split(',');
                foreach (string header in headers)
                {
                    dt.Columns.Add(header);
                }
                while (!sr.EndOfStream)
                {
                    string[] rows = sr.ReadLine().Split(',');
                    DataRow dr = dt.NewRow();
                    for (int i = 0; i < headers.Length; i++)
                    {
                        dr[i] = rows[i];
                    }
                    dt.Rows.Add(dr);
                }

            }

            foreach (DataRow row in dt.Rows)
            {
                string cust = row["Customer"].ToString();
                string Vendor = row["Vendor"].ToString();
                string vCust = row["VendorCustomer"].ToString();
                if (Vendor.ToUpper() == "SPR")
                {
                    if (vCust.Trim().Length == 5)
                    {
                        vCust = "0" + vCust;
                    }
                }
                var res = picMgr.PUTBatchVendorCustomer(Vendor, vCust, cust);

                dynamic picJson = JsonConvert.DeserializeObject<dynamic>(res);


                if (!picJson.valid.Value)
                {
                    dynamic err = JsonConvert.DeserializeObject<dynamic>(picJson.error);
                    //var test = picJson.error.Value;

                    arr.Add(picJson.error);
                }



            }
            if (arr.Count > 0)
            {
                return Json(arr);
            }
            return Json("Success");
        }
    }
}