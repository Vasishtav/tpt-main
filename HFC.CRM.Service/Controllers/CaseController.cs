﻿using System;
using System.Web.Http;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Extensions;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;

namespace HFC.CRM.Service.Controllers
{
    [RoutePrefix("api/Case")]
    public class CaseController : BaseController
    {
        /// <summary>
        /// Get Feedback List By Franchise
        /// </summary>
        /// <param name="IncludeClosed"></param>
        /// <returns></returns>
        [HttpGet, Route("GetFeedbackListByFranchise/{IncludeClosed}")]
        public IHttpActionResult GetFeedbackListByFranchise(bool IncludeClosed)
        {
            try
            {
                CaseManager Case_Mngr = new CaseManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = Case_Mngr.GetFeedbackListByFranchise(IncludeClosed);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get the Configurator Feedback By Id
        /// </summary>
        /// If the user is vendor, then IsVendor = true else Isvendor = False
        /// <param name="Id">FeedbackcaseId</param>
        /// <param name="IsVendor">IsVendor</param>
        /// <returns></returns>
        [HttpGet, Route("GetConfiguratorFeedbackById/{Id}/{IsVendor}")]
        public IHttpActionResult GetConfiguratorFeedbackById(int Id, bool IsVendor)
        {
            try
            {
                CaseManager Case_Mngr = new CaseManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = Case_Mngr.GetConfiguratorFeedbackById(Id, IsVendor);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get the Feedback Change Lookup
        /// </summary>
        /// The TableName are FeedbackStatus, ChangeRequestStatus, 
        /// ClosedReason, ErrorType, RequestType, DeclinedReason.
        /// Give the proper tablename and get the lookup values
        /// <param name="TableName"></param>
        /// <returns></returns>
        [HttpGet, Route("GetFeedbackChangeLookup/{TableName}")]
        public IHttpActionResult GetFeedbackChangeLookup(string TableName)
        {
            try
            {
                CaseManager Case_Mngr = new CaseManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = Case_Mngr.GetFeedbackChangeLookup(TableName);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get the History details
        /// </summary>
        /// <param name="id">FeedbackcaseId</param>
        /// <returns></returns>
        [HttpGet, Route("GetCaseHistoryForCase/{id}")]
        public IHttpActionResult GetCaseHistoryForCase(int id)
        {
            try
            {
                CaseManager Case_Mngr = new CaseManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var res = Case_Mngr.GetCaseHistoryForCase(id);
                foreach (HistoryTable ht in res)
                {
                    ht.Date = TimeZoneManager.ToLocal(ht.Date);
                    ht.LastUpdatedOn = TimeZoneManager.ToLocal(ht.LastUpdatedOn);
                }
                return Json(res);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }

        }

        /// <summary>
        /// Get the list of Change Request CaseNumber
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetChangeRequestCaseNumberByVendor")]
        public IHttpActionResult GetChangeRequestCaseNumberByVendor()
        {
            try
            {
                CaseManager Case_Mngr = new CaseManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = Case_Mngr.GetChangeRequestCaseNumberByVendor(0);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Save the Feedback Details
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult Post([FromBody] CaseFeedbackDetails model)
        {
            try
            {
                CaseManager Case_Mngr = new CaseManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = Case_Mngr.Save(model);
                return ResponseResult("Success", Json(new { result }));
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }

        }

        /// <summary>
        /// Get the Change Request By Id
        /// </summary>
        /// <param name="Id">ChangeRequest Case Id</param>
        /// <returns></returns>
        [HttpGet, Route("GetChangeRequestById/{Id}")]
        public IHttpActionResult GetChangeRequestById(int Id)
        {
            try
            {
                CaseManager Case_Mngr = new CaseManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = Case_Mngr.GetChangeRequestById(Id);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// get the Case Attachments List
        /// </summary>
        /// <param name="Id">ChangeRequest Case Id</param>
        /// <param name="flag"></param>
        /// <returns></returns>
        [HttpGet, Route("getCaseAttachmentsList/{Id}/{flag}")]
        public IHttpActionResult getCaseAttachmentsList(int Id, bool flag)
        {
            try
            {
                CaseManager Case_Mngr = new CaseManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var res = Case_Mngr.getCaseAttachmentsList(Id);

                if (flag == true)
                {
                    var tz = Case_Mngr.getFranchiseTimeZone(Id).TimezoneCode;
                    foreach (var r in res)
                    {
                        if (r.CreatedOn == null)
                        {
                            r.FileDetails = r.FileDetails.Insert(10, " " + tz.ToString() + " ");
                        }
                        else
                        {
                            DateTime lt = TimeZoneManager.ToLocal((DateTime)r.CreatedOn, tz);
                            r.FileDetails = r.FileDetails.Substring(10, r.FileDetails.Length - 10);
                            r.FileDetails = lt.Date.GlobalDateFormat() + " " + tz.ToString() + r.FileDetails;
                        }
                    }
                }
                else
                {
                    var tz = Case_Mngr.getFranchiseTimeZone(Id).TimezoneCode;
                    foreach (var r in res)
                    {
                        if (r.CreatedOn != null)
                        {
                            DateTime lt = TimeZoneManager.ToLocal((DateTime)r.CreatedOn, tz);
                            r.FileDetails = r.FileDetails.Substring(10, r.FileDetails.Length - 10);
                            r.FileDetails = lt.Date.GlobalDateFormat() + r.FileDetails;
                        }
                    }
                }
                return Json(res);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Save the ChangeRequest details
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, Route("SaveChangeRequest")]
        public IHttpActionResult SaveChangeRequest([FromBody] CaseChangeDetails model)
        {
            try
            {
                CaseManager Case_Mngr = new CaseManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = Case_Mngr.SaveChangeRequest(model);
                return ResponseResult("Success", Json(new { result }));
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Overview Of Case Feedback Change List By Vendor
        /// </summary>
        /// <param name="IncludeClosed"></param>
        /// <returns></returns>
        [HttpGet, Route("GetOverviewOfCaseFeedbackChangeListByVendor/{IncludeClosed}")]
        public IHttpActionResult GetOverviewOfCaseFeedbackChangeListByVendor(bool IncludeClosed)
        {
            try
            {
                CaseManager Case_Mngr = new CaseManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = Case_Mngr.GetOverviewOfCaseFeedbackChangeListByVendor(IncludeClosed);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Overview List By Franchise
        /// </summary>
        /// <param name="IncludeClosed"></param>
        /// <returns></returns>
        [HttpGet, Route("GetOverviewListByFranchise/{IncludeClosed}")]
        public IHttpActionResult GetOverviewListByFranchise(bool IncludeClosed)
        {
            try
            {
                CaseManager Case_Mngr = new CaseManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = Case_Mngr.GetOverviewListByFranchise(IncludeClosed);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Change Request List by PIC
        /// </summary>
        /// If the user is PIC, then IsPIC=true else IsPIC=false.
        /// <param name="IsPIC"></param>
        /// <param name="IncludeClosed"></param>
        /// <returns></returns>
        [HttpGet, Route("GetChangeRequestList/{IsPIC}/{IncludeClosed}")]
        public IHttpActionResult GetChangeRequestList(bool IsPIC, bool IncludeClosed)
        {
            try
            {
                CaseManager Case_Mngr = new CaseManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = Case_Mngr.GetChangeRequestList(IsPIC, IncludeClosed);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Remove Case Attachments Details
        /// </summary>
        /// <param name="Id">AttachmentId</param>
        /// <param name="CaseId">CaseId</param>
        /// <returns></returns>
        [HttpPost, Route("removeCaseAttachmentsDetails")]
        public IHttpActionResult removeCaseAttachmentsDetails(int Id, int CaseId)
        {
            try
            {
                CaseManager Case_Mngr = new CaseManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var res = Case_Mngr.removeCaseAttachmentsDetails(Id, CaseId);
                return Json(true);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }

        }
    }
}