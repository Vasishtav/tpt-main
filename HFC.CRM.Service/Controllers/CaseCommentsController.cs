﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;

namespace HFC.CRM.Service.Controllers
{
    [RoutePrefix("api/CaseComments")]
    public class CaseCommentsController : BaseController
    {
        /// <summary>
        /// get the Userid And Name
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("getUseridAndName")]
        public IHttpActionResult getUseridAndName()
        {
            int UserId = 0;
            string UserName = "";
            if (SessionManager.CurrentUser.Person != null)
            {
                UserId = SessionManager.CurrentUser.PersonId;
                UserName = SessionManager.CurrentUser.UserName;
            }
            else if (SessionManager.SecurityUser.Person != null)
            {
                UserId = SessionManager.SecurityUser.PersonId;
                UserName = SessionManager.SecurityUser.UserName;
            }

            return Json(new { id = UserId, name = UserName });
        }

        /// <summary>
        /// Get the Initial Comments
        /// </summary>
        /// <param name="id">Feedback Case Id</param>
        /// <returns></returns>
        [HttpGet, Route("loadInitialComments/{id}")]
        public IHttpActionResult loadInitialComments(int id)
        {
            try
            {
                CaseCommentsManager casecommentsmngr = new CaseCommentsManager();
                var result = casecommentsmngr.loadInitialComments(id);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get the user list
        /// </summary>
        /// <param name="id">Feedback Case Id</param>
        /// <returns></returns>
        [HttpGet, Route("loadUserlist/{id}")]
        public IHttpActionResult loadUserlist(int id)
        {
            try
            {
                CaseCommentsManager casecommentsmngr = new CaseCommentsManager();
                var result = casecommentsmngr.loadUserlist(id);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get the Franchise TimeZone
        /// </summary>
        /// <param name="id">Feedback Case Id</param>
        /// <returns></returns>
        [HttpGet, Route("FindFranchiseTimeZone/{id}")]
        public IHttpActionResult FindFranchiseTimeZone(int id)
        {
            try
            {
                CaseCommentsManager casecommentsmngr = new CaseCommentsManager();
                var result = casecommentsmngr.FindFranchiseTimeZone(id);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Save the Comment
        /// </summary>
        /// <param name="Comments"></param>
        /// <returns></returns>
        [HttpPost, Route("SaveParentComment")]
        public IHttpActionResult SaveParentComment(CaseComments Comments)
        {
            try
            {
                CaseCommentsManager casecommentsmngr = new CaseCommentsManager();
                var result = casecommentsmngr.SaveParentComment(Comments);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// update the Comment
        /// </summary>
        /// <param name="id">Comment Id</param>
        /// <param name="Comments"></param>
        /// <returns></returns>
        [HttpPost, Route("updateParentComment")]
        public IHttpActionResult updateParentComment(int id, CaseComments Comments)
        {
            try
            {
                CaseCommentsManager casecommentsmngr = new CaseCommentsManager();
                var result = casecommentsmngr.updateParentComment(id, Comments);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Delete the Comment
        /// </summary>
        /// If there is no parent comment, then give parentId=0
        /// <param name="id">Comment Id</param>
        /// <param name="parentId">Parent Comment Id</param>
        /// <returns></returns>
        [HttpPost, Route("deleteComment/{id}/{parentId}")]
        public IHttpActionResult deleteComment(int id, int parentId)
        {
            try
            {
                CaseCommentsManager casecommentsmngr = new CaseCommentsManager();
                var result = casecommentsmngr.deleteComment(id, parentId);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Save the Reply Comments
        /// </summary>
        /// <param name="Comments"></param>
        /// <returns></returns>
        [HttpPost, Route("SaveReplyComment")]
        public IHttpActionResult SaveReplyComment(CaseComments Comments)
        {
            try
            {
                CaseCommentsManager casecommentsmngr = new CaseCommentsManager();
                var result = casecommentsmngr.SaveReplyComment(Comments);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
    }
}