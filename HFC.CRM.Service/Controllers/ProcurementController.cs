﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    /// <summary>
    /// Procurement module
    /// </summary>
    public class ProcurementController : BaseController
    {
        /// <summary>
        /// Get Admin Vendor Name List
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("api/Procurement/AdminVendorNameList")]
        public IHttpActionResult AdminVendorNameList()
        {
            ProcurementDashboardManager mgr = new ProcurementDashboardManager(null, null);
            var result = mgr.GetAdminVendorname(0);
            return Json(result);
        }

        /// <summary>
        /// Get Franchise name list
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("api/Procurement/GetFranchiseList")]
        public IHttpActionResult GetFranchiseList()
        {
            ProcurementDashboardManager mgr = new ProcurementDashboardManager(null, null);
            var result = mgr.GetAdminFranchisename(0);
            return Json(result);

        }

        /// <summary>
        /// Get Drop down date filter option
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("api/Procurement/Type_DateTime")]
        public IHttpActionResult Type_DateTime()
        {
            // Todo web project need to update
            ProcurementDashboardManager mgr = new ProcurementDashboardManager(null, null);
            var result = mgr.Type_DateTime(4); // TableId - 4
            return Json(result);
        }

        /// <summary>
        /// Get Procurement data
        /// </summary>
        /// <param name="vendorid"></param>
        /// <param name="franchiseid"></param>
        /// <param name="dateFilter"></param>
        /// <returns></returns>
        [HttpGet, Route("api/Procurement/GetData/{vendorid}/{franchiseid}/{dateFilter}")]
        public IHttpActionResult GetData(int vendorid, int franchiseid, string dateFilter)
        {
            ProcurementDashboardManager mgr = new ProcurementDashboardManager(null, null);
            var result = mgr.GetData(franchiseid, vendorid, dateFilter);
            return Json(result);
        }

        /// <summary>
        /// Get the Vendor Name List
        /// </summary>
        /// <param name="id">Franchise id</param>
        /// <returns></returns>
        [HttpGet, Route("VendorNameList/{id}")]
        public IHttpActionResult VendorNameList(int id)
        {
            try
            {
                ProcurementDashboardManager mgr = new ProcurementDashboardManager(null, null);
                var result = mgr.GetVendorname(id);
                return Json(result);
            }
            catch(Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get the Procurement List
        /// </summary>
        /// <param name="id">vendorId</param>
        /// <param name="listStatus">listStatus</param>
        /// <param name="dateFilter">date</param>
        /// <returns></returns>
        [HttpGet, Route("Get/{id}/{listStatus}/{dateFilter}")]
        public IHttpActionResult Get(int id, string listStatus, string dateFilter)
        {
            try
            {
                ProcurementDashboardManager mgr = new ProcurementDashboardManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                VendorManager VendorMngr = new VendorManager();
                PermissionManager permissionManager = new PermissionManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                List<int> value = new List<int>();

                if (listStatus != null)
                {
                    var v1 = listStatus.Split(',').ToList();
                    value = v1.Select(int.Parse).ToList();
                }

                if (dateFilter == null || dateFilter == "null")
                    dateFilter = "";

                var result = mgr.Get(id, value, dateFilter);
                var GetPermission = permissionManager.GetUserRolesPermissions();
                var ModuleData = GetPermission.Find(x => x.ModuleCode == "ProcurementDashboard");
                var specialPermission = ModuleData.SpecialPermission.Find(x => x.PermissionCode == "ProcurementDashboardAllRecordAccess").CanAccess;
                if (result != null)
                {
                    if (specialPermission == false)
                    {
                        // Need to apply the filter for SalesAgent/Installation.
                        var filtered = result.Where(x => (x.SalesAgentId ==
                                            SessionManager.CurrentUser.PersonId ||
                                            x.InstallerId == SessionManager.CurrentUser.PersonId)
                                            ).ToList();

                        result = filtered;
                    }
                    return Json(result);
                }
                return OkTP();
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

    }
}
