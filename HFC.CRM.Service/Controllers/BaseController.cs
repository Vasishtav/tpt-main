﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HFC.CRM.Core.Membership;
using HFC.CRM.Managers;
using HFC.CRM.Data;
using HFC.CRM.Data.Constants;
using Newtonsoft.Json;
using System.Text;
using HFC.CRM.Core.Common;
using HFC.CRM.Service.Models;
using HFC.CRM.Data.Context;

namespace HFC.CRM.Service.Controllers
{
    /// <summary>
    /// Base Data
    /// </summary>
    [Authorize]
    public class BaseController : ApiController
    {
        /// <summary>
        /// throw error
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        protected IHttpActionResult ResponseResult(Exception ex)
        {
            var message = new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
                ReasonPhrase = ex.Message.Replace("\r\n", " "),
                Content = new StringContent(ex.Message.Replace("\r\n", " ").ToString())
            };
            throw new HttpResponseException(message);
        }

        protected IHttpActionResult ResponseResult(string status, IHttpActionResult successResult = null)
        {
            if (status == IMessageConstants.Success)
                return successResult ?? Ok();
            else return Ok();
        }

        protected override System.Web.Http.Results.JsonResult<T> Json<T>(T content, JsonSerializerSettings serializerSettings, Encoding encoding)
        {
            //this will set the default for ignoring reference
            serializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            //serializerSettings.NullValueHandling = NullValueHandling.Ignore;

            return base.Json<T>(content, serializerSettings, encoding);
        }

        protected IHttpActionResult OkTP()
        {
            var accept = Request.GetHeaderValue("accept") ?? string.Empty;

            if (accept.Contains("application/json") || accept.Contains("text/javascript") || accept.Contains("application/x-javascript"))
                return Json<string>(null);
            else
                return base.Ok();
        }

        /// <summary>
        /// Get User info
        /// </summary>
        /// <returns></returns>
        protected Data.User Getuser()
        {
            if (SessionManager.CurrentUser != null)
                return SessionManager.CurrentUser;

            var aUser = LocalMembership.GetUser(User.Identity.Name, "Person", "Roles", "OAuthUsers");
            return aUser;
        }

        /// <summary>
        /// Get Franchise info
        /// </summary>
        /// <returns></returns>
        protected Data.Franchise GetFranchise()
        {
            if (SessionManager.CurrentFranchise != null)
                return SessionManager.CurrentFranchise;

            var user = Getuser();
            if (user.FranchiseId == null || user.FranchiseId <= 0)
                return null;

            var frnManager = new FranchiseManager(user);
            var fedata = frnManager.GetFEinfo((int)user.FranchiseId);
            return fedata;
        }

        /// <summary>
        /// Get Franchise Detail
        /// </summary>
        /// <returns></returns>
        protected Data.Franchise GetFranchiseDetail()
        {
            if (SessionManager.CurrentFranchise != null)
                return SessionManager.CurrentFranchise;

            var user = Getuser();
            if (user.FranchiseId == null || user.FranchiseId <= 0)
                return null;

            var frnManager = new FranchiseManager(user);
            var fedata = frnManager.Get((int)user.FranchiseId);
            return fedata;
        }

        /// <summary>
        /// Find FE Timezone
        /// </summary>
        /// <returns></returns>
        protected TimeZoneEnum GetFranchiseTimezone()
        {
            var user = LocalMembership.GetUser(User.Identity.Name, "Person", "Roles", "OAuthUsers");
            var frnManager = new FranchiseManager(user);
            if (user.FranchiseId != null)
            {
                var fedata = frnManager.GetFEinfo((int)user.FranchiseId);
                TimeZoneEnum feTimeZone = fedata.TimezoneCode;
                return feTimeZone;
            }
            else
                return TimeZoneEnum.UTC;
        }

        /// <summary>
        /// DB db for this controller
        /// </summary>
        protected CRMContext CRMDBContext = ContextFactory.Current.Context;

        /// <summary>
        /// Can Access
        /// </summary>
        /// <param name="permissionName"></param>
        /// <returns></returns>
        protected bool CanAccess(string permissionName)
        {
            var permission = PermissionProviderTP.GetPermissionTP(SessionManager.CurrentUser, permissionName);
            return permission == null ? false : permission.CanAccess;
        }
    }
}
