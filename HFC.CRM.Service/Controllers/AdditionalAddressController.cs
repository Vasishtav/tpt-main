﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using HFC.CRM.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    /// <summary>
    /// Address info
    /// </summary>
    [RoutePrefix("api/AdditionalAddress")]
    public class AdditionalAddressController : BaseController
    {
        /// <summary>
        /// Get Country
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetCountry")]
        public IHttpActionResult GetCountry()
        {
            try
            {
                var result = CacheManager.StatesCollection.Select(s => new { s.Type_Country.ISOCode2Digits, s.Type_Country.Country, s.Type_Country.ZipCodeMask }).Distinct();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get State
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetState")]
        public IHttpActionResult GetState()
        {
            try
            {
                var result = CacheManager.StatesCollection.Select(s => new { s.StateAbv, s.StateName, s.CountryISO2 });
                return Json(result);
            }

            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get Lead Address
        /// </summary>
        /// <param name="id">LeadId</param>
        /// <param name="showDeleted"></param>
        /// <returns></returns>
        [HttpGet, Route("GetLeadAddress/{id}/{showDeleted}")]
        public IHttpActionResult GetLeadAddress(int id, bool showDeleted)
        {
            try
            {
                AdditionalAddressManager addressManager = new AdditionalAddressManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = addressManager.GetLeadAddress(id, showDeleted);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Additional Account Address
        /// </summary>
        /// <param name="id">AccountId</param>
        /// <param name="showDeleted"></param>
        /// <returns></returns>
        [HttpGet, Route("GetAccountAddress/{id}/{showDeleted}")]
        public IHttpActionResult GetAccountAddress(int id, bool showDeleted)
        {
            try
            {
                AdditionalAddressManager addressManager = new AdditionalAddressManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = addressManager.GetAccountAddress(id, showDeleted);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get All Account Contacts
        /// </summary>
        /// <param name="id">AccountId</param>
        /// <param name="showDeleted">showDeleted</param>
        /// <returns></returns>
        [HttpGet, Route("GetAllAccountContacts/{id}/{showDeleted}")]
        public IHttpActionResult GetAllAccountContacts(int id, bool showDeleted)
        {
            try
            {
                AdditionalAddressManager addressManager = new AdditionalAddressManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = addressManager.GetAccountContactsIncludingPrimary(id, showDeleted);
                return Json(result);

            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Add New Address
        /// </summary>
        /// <remarks>
        /// Notes
        /// 
        ///     POST 
        ///     {
        ///        "AssociatedSource": 1, //Lead = 1,Account = 2,Opportunity = 3
        ///     }
        ///     
        /// </remarks>
        /// <param name="viewmodel"></param>
        /// <returns></returns>
        [HttpPost, Route("AddNewAddress")]
        public IHttpActionResult AddNewAddress(AddressTPViewModel viewmodel)
        {
            try
            {
                AddressTP model = viewmodel != null ? AddressTPViewModel.MapFrom(viewmodel) : null;
                var status = "Save Failed. Please try again";
                var newaddressid = 0;
                if (model != null)
                {
                    AdditionalAddressManager addressManager = new AdditionalAddressManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                    model.LeadId = (model.LeadId > 0) ? model.LeadId : null;
                    model.AccountId = (model.AccountId > 0) ? model.AccountId : null;
                    model.OpportunityId = (model.OpportunityId > 0) ? model.OpportunityId : null;
                    model.CreatedOnUtc = DateTime.Now;
                    model.CreatedBy = SessionManager.CurrentUser.PersonId;
                    newaddressid = addressManager.AddAddress(model);
                }
                //return ResponseResult(status);
                return ResponseResult("Success", Json(new { newAddressId = newaddressid }));

            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Update Address
        /// </summary>
        /// <remarks>
        /// Notes
        /// 
        ///     POST 
        ///     {
        ///        "AssociatedSource": 1, //Lead = 1,Account = 2,Opportunity = 3
        ///     }
        ///     
        /// </remarks>
        /// <param name="viewmodel"></param>
        /// <returns></returns>
        [HttpPost, Route("UpdateAddress")]
        public IHttpActionResult UpdateAddress(AddressTPViewModel viewmodel)
        {

            try
            {
                AddressTP model = viewmodel != null ? AddressTPViewModel.MapFrom(viewmodel) : null;
                var status = "Update Failed. Please try again";
                if (model != null)
                {
                    AdditionalAddressManager addressManager = new AdditionalAddressManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                    model.CreatedOnUtc = DateTime.Now;
                    model.CreatedBy = SessionManager.CurrentUser.PersonId;
                    status = addressManager.Update(model);
                }
                return ResponseResult(status);

            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Can be Deleted
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Route("CanbeDeleted/{id}")]
        public IHttpActionResult CanbeDeleted(int id)
        {
            try
            {
                AdditionalAddressManager addressManager = new AdditionalAddressManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = addressManager.CanbeDeleted(id);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Delete Address
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                AdditionalAddressManager addressManager = new AdditionalAddressManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var status = "Update Failed. Please try again";
                status = addressManager.Delete(id);
                return ResponseResult(status);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
    }
}
