﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Managers;

namespace HFC.CRM.Service.Controllers
{
    [RoutePrefix("api/TaskEvents")]
    public class TaskEventsController : BaseController
    {
        /// <summary>
        /// Get Lead List
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetLeadsList")]
        public IHttpActionResult GetLeadsList()
        {
            try
            {
                TaskEventsManager manager = new TaskEventsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = manager.GetLeadsList();
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Account List
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetAccountList")]
        public IHttpActionResult GetAccountList()
        {
            try
            {
                TaskEventsManager manager = new TaskEventsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = manager.GetAccountList();
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Opportunity List
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetOpportunityList")]
        public IHttpActionResult GetOpportunityList()
        {
            try
            {
                TaskEventsManager manager = new TaskEventsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = manager.GetOpportunityList();
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Order List
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetOrderList")]
        public IHttpActionResult GetOrderList()
        {
            try
            {
                TaskEventsManager manager = new TaskEventsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = manager.GetOrderList();
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Case List
        /// </summary>
        /// <param name="Id">orderId/opportunityId/accountId</param>
        /// <param name="type">order/opportunity/account</param>
        /// <returns></returns>
        [HttpGet, Route("GetCaseList")]
        public IHttpActionResult GetCaseList(int Id, string type)
        {
            try
            {
                TaskEventsManager manager = new TaskEventsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = manager.GetCaseList(Id, type);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Vendor Case List
        /// </summary>
        /// <param name="Id">orderId/opportunityId/accountId/caseId</param>
        /// <param name="type">order/opportunity/account/case</param>
        /// <returns></returns>
        [HttpGet, Route("GetVendorCaseList")]
        public IHttpActionResult GetVendorCaseList(int Id, string type)
        {
            try
            {
                TaskEventsManager manager = new TaskEventsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = manager.GetVendorCaseList(Id, type);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Opportunity List By Account
        /// </summary>
        /// <param name="Id">Account Id</param>
        /// <returns></returns>
        [HttpGet, Route("GetOpportunityListByAccount/{Id}")]
        public IHttpActionResult GetOpportunityListByAccount(int Id = 0)
        {
            try
            {
                TaskEventsManager manager = new TaskEventsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = manager.GetOpportunityListByAccount(Id);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Order List By Opportunity
        /// </summary>
        /// <param name="Id">Opportunity Id</param>
        /// <returns></returns>
        [HttpGet, Route("GetOrderListByOpportunity/{Id}")]
        public IHttpActionResult GetOrderListByOpportunity(int Id = 0)
        {
            try
            {
                TaskEventsManager manager = new TaskEventsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = manager.GetOrderListByOpportunity(Id);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Order List By Account
        /// </summary>
        /// <param name="Id">AccountId</param>
        /// <returns></returns>
        [HttpGet, Route("GetOrderListByAccount/{Id}")]
        public IHttpActionResult GetOrderListByAccount(int Id = 0)
        {
            TaskEventsManager manager = new TaskEventsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var result = manager.GetOrderListByAccount(Id);
            return Json(result);
        }
    }
}