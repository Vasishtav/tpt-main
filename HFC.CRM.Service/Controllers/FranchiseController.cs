﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using HFC.CRM.Service.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    /// <summary>
    /// Admin related Franchise Data operation
    /// </summary>
    public class FranchiseController : BaseController
    {
        /// <summary>
        /// Get the list of Franchise for Admin
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("api/Franchise/GetFranchiseList")]
        public IHttpActionResult GetFranchiseList()
        {
            var user = Getuser();
            FranchiseManager _franMgr = new FranchiseManager(user);
            try
            {
                int totalRecords;
                var result = _franMgr.Get(out totalRecords);

                var fanchises = result.Select(f => new
                {
                    Name = f.Name,
                    IsSuspended = f.IsSuspended,
                    FranchiseOwners = string.Join(" & ", f.FranchiseOwners.Select(s => s.User.Person.FullName)),
                    Address = f.Address.ToString(false, true),
                    Code = f.Code,
                    FranchiseId = f.FranchiseId,
                    BrandId = f.BrandId,
                    Users = f.Users.Where(v => (f.FranchiseOwners.Any() && v.UserId != f.FranchiseOwners.First().UserId) || !f.FranchiseOwners.Any()).ToList().OrderBy(x => x.UserName),
                    //Users = f.Users.ToList(),
                    Owner = f.FranchiseOwners.FirstOrDefault(),
                    Brand = f.Brand,
                    ownername = f.owner,
                    CountryCode = f.Address.CountryCode2Digits,

                });
                return Json(fanchises.ToList());
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }


        }

        /// <summary>
        /// Get Franchise info based on franchiseid
        /// </summary>
        /// <param name="franchiseid"></param>
        /// <returns></returns>
        [HttpGet, Route("api/Franchise/GetFranchise/{franchiseid}")]
        public IHttpActionResult GetFranchise(int franchiseid)
        {
            try
            {
                var user = Getuser();
                FranchiseManager _franMgr = new FranchiseManager(user);
                if (franchiseid <= 0) throw new Exception("FranchiseId should not be less than or equal to zero");
                return Ok(new
                {
                    Franchise = _franMgr.Get(franchiseid)
                });
            }

            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewmodel"></param>
        /// <returns></returns>
        [HttpPost, Route("api/Franchise/SaveFranchise")]
        public IHttpActionResult SaveFranchise(FranchiseViewModel viewmodel)
        {
            try
            {
                var model = FranchiseViewModel.MapFrom(viewmodel);
                var user = Getuser();
                FranchiseManager _franMgr = new FranchiseManager(user);
                int franchiseId = 0;
                _franMgr.InitFranchise(model);
                if (model.lstOwnerName != null)
                {
                    if (model.lstOwnerName.Count != 0)
                        model.OwnerName = JsonConvert.SerializeObject(model.lstOwnerName);
                    else model.OwnerName = null;
                }

                if (model.FranchiseId > 0)
                {
                    var successResult = _franMgr.Update(model);
                    return Json(successResult);
                }
                else
                {
                    var successResult = _franMgr.Add(out franchiseId, model);
                    return Json(successResult);
                }

            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        /// <summary>
        /// GetTerritory By FranchiseId
        /// </summary>
        /// <param name="franchiseid"></param>
        /// <returns></returns>
        [HttpGet, Route("api/Franchise/GetTerritoryByFranchiseId/{franchiseid}")]
        public IHttpActionResult GetTerritoryByFranchiseId(int franchiseid)
        {
            var user = Getuser();
            FranchiseManager _franMgr = new FranchiseManager(user);
            var result = _franMgr.GetTerritoryByFranchiseId(franchiseid);
            return Json(result);
        }

        /// <summary>
        /// Save Territory
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, Route("api/Franchise/SaveTerritory")]
        public IHttpActionResult SaveTerritory(TerritoriesModel model)
        {
            var territoryId = 0;
            var user = Getuser();
            FranchiseManager _franMgr = new FranchiseManager(user);
            var fe = _franMgr.GetFEinfo(model.FranchiseId);
            model.BrandId = fe.BrandId;

            var status = _franMgr.AssignTerritory(TerritoriesModel.MapFrom(model), model.IsInUsed, out territoryId);

            return Json(new { Result = status, TerritoryId = territoryId });
        }

        /// <summary>
        /// Enable QB for Franchise
        /// </summary>
        /// <param name="franchiseid"></param>
        /// <returns></returns>
        [HttpPost, Route("api/Franchise/AddToQB")]
        public IHttpActionResult AddToQB(int franchiseid)
        {
            try
            {
                FranchiseManager _franMgr = new FranchiseManager(Getuser());
                var franchise = _franMgr.Get(franchiseid);

                if (franchise != null)
                {
                    _franMgr.AddQBApp(franchiseid);

                    return ResponseResult("Success");
                }

                return ResponseResult("Franchise id not valid");
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get Territory
        /// </summary>
        /// <param name="Zipcode"></param>
        /// <param name="Country"></param>
        /// <returns></returns>
        [HttpGet, Route("api/Franchise/GetTerritory")]
        public IHttpActionResult GetTerritory(string Zipcode, string Country)
        {
            FranchiseManager _franMgr = new FranchiseManager(Getuser());
            var TerritoryName = _franMgr.GetTerritoryName(Zipcode, Country);
            return Json(TerritoryName);
        }

        #region Fe Setting's MSR Reports Code's
        /// <summary>
        /// Get Fe Territories
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetFeTerritories")]
        public IHttpActionResult GetFeTerritories()
        {
            try
            {
                FranchiseManager _franMgr = new FranchiseManager(Getuser());
                int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                int brandId = SessionManager.CurrentFranchise.BrandId;
                var result = _franMgr.GetTerritorydatas(brandId, FranchiseId);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get Months Value
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetMonthsValue")]
        public IHttpActionResult GetMonthsValue()
        {
            try
            {
                FranchiseManager _franMgr = new FranchiseManager(Getuser());
                var result = _franMgr.GetMonthSets();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        /// <summary>
        /// Update Territory Date
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, Route("UpdateTerritoryDate")]
        public IHttpActionResult UpdateTerritoryDate([FromBody]TerritoriesModel model)
        {
            try
            {
                FranchiseManager _franMgr = new FranchiseManager(Getuser());
                var result = _franMgr.SaveorUpdateTerritory(TerritoriesModel.MapFrom(model));
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }
        #endregion
    }
}
