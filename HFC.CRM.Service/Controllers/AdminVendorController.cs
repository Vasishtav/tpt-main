﻿using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.DTO.Vendors;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    /// <summary>
    /// AdminVendorController
    /// </summary>
    public class AdminVendorController : BaseController
    {
        /// <summary>
        /// to get value at initial load(alliance vendor)
        /// if vendortype = 0 then return alliancevendor else Non  alliancevendor will return
        /// </summary>
        /// <param name="vendortype"></param>
        /// <returns></returns>
        [HttpGet, Route("api/AdminVendor/GetAllianceVendor/{vendortype}")]
        public IHttpActionResult GetAllianceVendor(int vendortype)
        {
            VendorManager VendorMngr = new VendorManager(null, null);
            var result = VendorMngr.GetAlliance(vendortype);
            return Json(result);
        }

        /// <summary>
        /// call for Save on add/edit data 
        /// </summary>
        /// <param name="vendortype"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, Route("api/AdminVendor/SaveAdminVendor/{vendortype}")]
        public IHttpActionResult SaveAdminVendor(int vendortype, [FromBody]AdminAllianceModel model)
        {
            var user = Getuser();
            VendorManager VendorMngr = new VendorManager(null, null);
            if (model != null && model.Name != null && model.Name != "")
            {
                if (model.VendorIdPk == 0 && model.CreatedBy == 0)
                {
                    model.CreatedBy = user.PersonId;
                }
                if (model.VendorIdPk == 0 && model.VendorTypeName == "Alliance Vendor")
                    vendortype = 0;
                else if (model.VendorIdPk == 0 && model.VendorTypeName != "Alliance Vendor")
                    vendortype = 1;
                if (model.VendorIdPk != 0 && model.VendorTypeName == "Alliance Vendor")
                    model.VendorType = 1;
                else if (model.VendorIdPk != 0 && model.VendorTypeName != "Alliance Vendor")
                    model.VendorType = 2;

                var status = VendorMngr.SaveAllianceVendor(vendortype, model);
                //return Json(status);
                return ResponseResult(status);
            }
            return ResponseResult("Failed");
        }

        /// <summary>
        /// Get Vendor Case List
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("api/AdminVendor/GetVendorCaseList")]
        public IHttpActionResult GetVendorCaseList()
        {
            VendorManager VendorMngr = new VendorManager(null, null);
            var result = VendorMngr.GetVendorCaseConfigList();
            return Json(result);
        }

        /// <summary>
        /// Update Vendor Case
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, Route("api/AdminVendor/UpdateVendorCase")]
        public IHttpActionResult UpdateVendorCase(VendorCaseConfig model)
        {
            try
            {
                VendorManager VendorMngrHFC = new VendorManager(Getuser(), null);
                if (VendorMngrHFC.CheckUserBelongsToFe(model.CaseLogin) != false)
                {
                    return ResponseResult("This is Franchise User. Please contact HFC Tech Support if the problem persists.");
                }
                else if (VendorMngrHFC.CheckUserBelongsToVendor(model.CaseLogin, model.VendorId) != false)
                {
                    return ResponseResult("This user already belongs to another vendor. Please contact HFC Tech Support if the problem persists.");
                }
                else
                {

                    var usrPrimaryEmail = VendorMngrHFC.GetADDetails(model.CaseLogin);
                    if (usrPrimaryEmail == "")
                        return ResponseResult("Invalid HFC Username.  Please check the spelling of the username issued by HFC Tech Support and contact them if the problem persists.");

                    if (!VendorMngrHFC.CheckUserExist(model.CaseLogin))
                    {
                        UserManager usrmgr = new UserManager(null, null);
                        usrmgr.CreateVendorUser(model.CaseLogin);
                    }


                    var result = VendorMngrHFC.UpdateVendorCaseConfig(model);
                    return ResponseResult(result);
                }


            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

    }
}
