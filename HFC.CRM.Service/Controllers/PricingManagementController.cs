﻿using HFC.CRM.Managers;
using HFC.CRM.Core.Common;
using System.Web.Http;
using HFC.CRM.Data;

namespace HFC.CRM.Service.Controllers
{
    [RoutePrefix("api/PricingManagement")]
    public class PricingManagementController : BaseController
    {
        /// <summary>
        /// Get FranchiseLevel Pricing setting
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetFranchiseLevel")]
        public IHttpActionResult GetFranchiseLevel()
        {
            PricingManager pricingManager = new PricingManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var response = pricingManager.Get();
            if (response != null)
            {
                return Json(response);
            }
            return OkTP();
        }

        /// <summary>
        /// Get Vendor Dropdown
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetVendorDropdown")]
        public IHttpActionResult GetVendorDropdown()
        {
            VendorManager vendorManager = new VendorManager();
            var response = vendorManager.GetVendorPricingDropDown();
            if (response != null)
            {
                return Json(response);
            }
            return OkTP();
        }

        /// <summary>
        /// Get VendorDropdown by ProductCategory
        /// </summary>
        /// <param name="ProductCategory"></param>
        /// <returns></returns>
        [HttpGet, Route("GetVendorDropdown/{ProductCategory}")]
        public IHttpActionResult GetVendorDropdown(int ProductCategory)
        {
            VendorManager vendorManager = new VendorManager();
            var response = vendorManager.FilterVendorPricingDropDown(ProductCategory);
            if (response != null)
            {
                return Json(response);
            }
            return OkTP();
        }

        /// <summary>
        /// Get ProductCategory Dropdown
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetProductCategoryDropdown")]
        public IHttpActionResult GetProductCategoryDropdown()
        {
            ProductsManager productManager = new ProductsManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
            var response = productManager.GetProductCategoryDropDown();
            if (response != null)
            {
                return Json(response);
            }
            return OkTP();
        }

        /// <summary>
        /// Get PricingStrategy Dropdown
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetPricingStrategyDropdown")]
        public IHttpActionResult GetPricingStrategyDropdown()
        {
            PricingManager pricingManager = new PricingManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var response = pricingManager.GetPriceStrategyTypeDropDown();
            if (response != null)
            {
                return Json(response);
            }
            return OkTP();
        }

        /// <summary>
        /// Get AdvancedPricing
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetAdvancedPricing")]
        public IHttpActionResult GetAdvancedPricing()
        {
            PricingManager pricingManager = new PricingManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var response = pricingManager.GetAdvancedPricing();
            if (response != null)
            {
                return Json(response);
            }
            return OkTP();
        }

        /// <summary>
        /// Save AdvancedPricing
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet, Route("SaveAdvancedPricing")]
        public IHttpActionResult SaveAdvancedPricing(PricingSettings model)
        {
            PricingManager pricingManager = new PricingManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var status = "Invalid data";
            if (model.MarkUp == null)
            {
                model.MarkUpFactor = "";
            }
            if (model != null)
            {
                model.IsActive = true;
                status = pricingManager.Save(model);
            }
            var response = pricingManager.GetAdvancedPricing();
            //if (status == ContantStrings.Success)
            //{
            return Json(new { error = status, data = response });
            //}
            //else
            //{
            //    return Json(new { error = "Unknown error occurred.", data = response });
            //}

        }

        /// <summary>
        /// Save FranchiseLevel
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet, Route("SaveFranchiseLevel")]
        public IHttpActionResult SaveFranchiseLevel(PricingSettings model)
        {
            PricingManager pricingManager = new PricingManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var status = "Invalid data";
            if (model != null)
            {
                model.IsActive = true;
                status = pricingManager.FranchiseLevelSave(model);
            }
            return Json(status);
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete]
        public IHttpActionResult Delete(int Id)
        {
            PricingManager pricingManager = new PricingManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var status = "Invalid data";
            if (Id > 0)
            {
                status = pricingManager.PricingDelete(Id);
            }
            return Json(status);
        }
    }
}
