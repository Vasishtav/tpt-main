﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;

namespace HFC.CRM.Service.Controllers
{
    [RoutePrefix("api/Notes")]
    public class NotesController : BaseController
    {
        /// <summary>
        /// Get the Global Notes
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>
        [HttpGet, Route("GetGlobalNotess/{id}")]
        public IHttpActionResult GetGlobalNotess(int id)
        {
            try
            {
                Note note = new Note { NoteId = id };
                NoteManager noteMgr = new NoteManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                IList<Note> notes = noteMgr.GetGlobalNotes(note);
                return Json(notes);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Gets a notes for that particular Lead
        /// </summary>
        /// <param name="id">Lead Id</param>
        /// <param name="showDeleted">showDeleted</param>
        /// <returns></returns>
        [HttpGet, Route("GetLeadNotes/{id}/{showDeleted}")]
        public IHttpActionResult GetLeadNotes(int id, bool showDeleted)
        {
            try
            {
                Note note = new Note { LeadId = id };
                NoteManager noteMgr = new NoteManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                IList<Note> notes = noteMgr.GetLeadNotes(note, showDeleted);
                return Json(notes);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Gets a notes for that particular Account
        /// </summary>
        /// <param name="id">Account Id</param>
        /// <param name="showDeleted">showDeleted</param>
        /// <returns></returns>
        [HttpGet, Route("GetAccountNotes/{id}/{showDeleted}")]
        public IHttpActionResult GetAccountNotes(int id, bool showDeleted)
        {
            try
            {
                Note note = new Note { AccountId = id };
                NoteManager noteMgr = new NoteManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                IList<Note> notes = noteMgr.GetAccountNotes(note, showDeleted);
                return Json(notes);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Gets a notes for that particular Opportunity
        /// </summary>
        /// <param name="id">Opportunity Id</param>
        /// <param name="showDeleted">showDeleted</param>
        /// <returns></returns>
        [HttpGet, Route("GetOpportunityNotes/{id}/{showDeleted}")]
        public IHttpActionResult GetOpportunityNotes(int id, bool showDeleted)
        {
            try
            {
                Note note = new Note { OpportunityId = id };
                NoteManager noteMgr = new NoteManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                IList<Note> notes = noteMgr.GetOpportunityNotes(note, showDeleted);
                return Json(notes);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Gets a notes for that particular Order
        /// </summary>
        /// <param name="id">Order Id</param>
        /// <param name="showDeleted">showDeleted</param>
        /// <returns></returns>
        [HttpGet, Route("GetOrderNotes/{id}/{showDeleted}")]
        public IHttpActionResult GetOrderNotes(int id, bool showDeleted)
        {
            try
            {
                Note note = new Note { OrderId = id };
                NoteManager noteMgr = new NoteManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                IList<Note> notes = noteMgr.GetOrderNotes(note, showDeleted);
                return Json(notes);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Gets a Documents for Current Franchise
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetFranchiseDocuments")]
        public IHttpActionResult GetFranchiseDocuments()
        {
            try
            {
                Note note = new Note { FranchiseId = SessionManager.CurrentFranchise.FranchiseId };
                NoteManager noteMgr = new NoteManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                IList<Note> notes = noteMgr.GetFranchiseDocuments(note);
                return Json(notes);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Post a Global Document
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, Route("AddGlobalDocument")]
        public IHttpActionResult AddGlobalDocument([FromBody] Note model)
        {
            try
            {
                var status = "Invalid data. Save Failed. Please try again";
                if (model != null)
                {
                    NoteManager noteMgr = new NoteManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                    status = noteMgr.Add(model);
                }
                return ResponseResult(status);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Post the note
        /// </summary>
        /// <param name="Notes">Note</param>
        /// <returns></returns>
        [HttpPost, Route("AddNewNote")]
        public IHttpActionResult AddNewNote([FromBody]Note Notes)
        {
            try
            {
                var status = "Invalid data. Save Failed. Please try again";
                if (Notes != null)
                {
                    NoteManager noteMgr = new NoteManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                    status = noteMgr.Add(Notes);
                }
                return ResponseResult(status);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Update the note
        /// </summary>
        /// <param name="Notes">Note</param>
        /// <returns></returns>
        [HttpPost, Route("UpdateNote")]
        public IHttpActionResult UpdateNote([FromBody]Note Notes)
        {
            try
            {
                var status = "Invalid data. Update Failed. Please try again";
                if (Notes != null)
                {
                    NoteManager noteMgr = new NoteManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                    status = noteMgr.Update(Notes);
                }
                return ResponseResult(status);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Delete the note
        /// </summary>
        /// <param name="id">Note ID</param>
        /// <returns></returns>
        [HttpDelete, Route("Delete/{id}")]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                var status = "Invalid data. Update Failed. Please try again";
                NoteManager noteMgr = new NoteManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                status = noteMgr.Delete(id);
                return ResponseResult(status);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Recover the note
        /// </summary>
        /// <param name="id">Note ID</param>
        /// <returns></returns>
        [HttpPost, Route("RecoverNote/{id}")]
        public IHttpActionResult RecoverNote(int id)
        {
            try
            {
                var status = "Invalid data. Update Failed. Please try again";
                NoteManager noteMgr = new NoteManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                status = noteMgr.Recover(id);
                return ResponseResult(status);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
    }
}