﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    [RoutePrefix("api/EmailSettings")]
    public class EmailSettingsController : BaseController
    {
        /// <summary>
        /// Get Territory Email Details
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetTerritoryEmailDetails")]
        public IHttpActionResult GetTerritoryEmailDetails()
        {
            try
            {
                EmailSettingsManager mailsetmang = new EmailSettingsManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                var result = mailsetmang.GetDetails();
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }
        /// <summary>
        /// Get Territory Location
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetTerritoryLocation")]
        public IHttpActionResult GetTerritoryLocation()
        {
            try
            {
                EmailSettingsManager mailsetmang = new EmailSettingsManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                var location = mailsetmang.GetLoaction();
                return Json(location);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get Address Location
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet, Route("GetAddressLocation/{Id}")]
        public IHttpActionResult GetAddressLocation(int Id)
        {
            try
            {
                EmailSettingsManager mailsetmang = new EmailSettingsManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                var location = mailsetmang.GetLocAddress(Id);
                return Json(location);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }

        /// <summary>
        /// Save Territory Details
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, Route("SaveTerritoryDetails")]
        public IHttpActionResult SaveTerritoryDetails(List<TerritoryEmailSettings> model)
        {
            try
            {
                EmailSettingsManager mailsetmang = new EmailSettingsManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                var result = mailsetmang.SaveData(model);
                return Json(result);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return this.ResponseResult(ex);
            }
        }
    }
}
