﻿using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using HFC.CRM.Data;

using HFC.CRM.Core.Logs;

namespace HFC.CRM.Service.Controllers
{
    [RoutePrefix("api/Tax")]
    public class TaxController : BaseController
    {
        /// <summary>
        /// Get TaxSettings
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult Get()
        {
            try
            {
                FranchiseManager FranMgr = new FranchiseManager(SessionManager.CurrentUser);
                int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                var result = FranMgr.GetTaxSettingsByFranchise(FranchiseId);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Save Tax Settings
        /// </summary>
        /// <param name="taxSettings"></param>
        /// <returns></returns>
        [HttpPut, Route("UpdateSaveTaxSettings")]
        public IHttpActionResult UpdateSaveTaxSettings(TaxSettings taxSettings)
        {
            FranchiseManager FranMgr = new FranchiseManager(SessionManager.CurrentUser);
            int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
            try
            {
                var sav = false;
                if (taxSettings != null)
                {
                    sav = FranMgr.AddorUpdateTaxSettings(taxSettings);
                }

                var result = FranMgr.GetTaxSettingsByFranchise(FranchiseId);
                return Json(new { valid = sav, data = result });
            }
            catch (Exception ex)
            {
                var result = FranMgr.GetTaxSettingsByFranchise(FranchiseId);
                return Json(new { valid = false, data = result });
            }
        }
    }
}