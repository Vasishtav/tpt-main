﻿using HFC.CRM.Data;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    /// <summary>
    /// Admin Currency
    /// </summary>
    [RoutePrefix("api/AdminCurrency")]
    public class AdminCurrencyController : BaseController
    {
        CurrencyManager Currency_manager = new CurrencyManager();

        /// <summary>
        /// get all currency value to grid code(View Grid)
        /// </summary>
        /// <returns></returns>
        [HttpGet,Route("Currencydetails")]
        public IHttpActionResult Currencydetails()
        {
            //var result = Lookup_manager.GetCurrency(value);
            var result = Currency_manager.GetCurrencydata();
            return Json(result);
        }

        /// <summary>
        /// get value for add grid code
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("CountryCurrency")]
        public IHttpActionResult CountryCurrency()
        {
            var result = Currency_manager.GetCurrencyCountry();
            return Json(result);
        }

        /// <summary>
        /// Save grid value
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, Route("SaveCurrency")]
        public IHttpActionResult SaveCurrency(List<CurrencyAdd> model)
        {
            if (model != null)
            {
                foreach(var item in model)
                {
                    if (item.FromType == "*") item.FromTypeValue = 1;
                    else item.FromTypeValue = 2;
                    if (item.ToType == "*") item.ToTypeValue = 1;
                    else item.ToTypeValue = 2;
                }
                var status = Currency_manager.Savedata(model);
                return ResponseResult(status);
            }
            return ResponseResult("Failed");
        }
       
    }
}
