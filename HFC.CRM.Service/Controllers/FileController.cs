﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    public class FileController : BaseController
    {
        /// <summary>
        /// File Upload
        /// </summary>
        /// <remarks>
        /// Notes
        /// 
        ///     Form Data //Admin system document 
        ///     {
        ///        Id1:0,
        ///        title: title,
        ///        sysWideIsEnabled: true,
        ///        sysWideCategory: 1,
        ///        sysWideConcept: 1,
        ///        sysWideTitle: Test,
        ///        Document: SYS-DOC,
        ///        IsFranchiseSettings: false,
        ///        IsEnabled: false,
        ///        files: (binary)
        ///     }
        ///     Form Data //Lead Attachment
        ///     {
        ///        title: title,
        ///        categories: 6 //Photo - 6,Plans - 7,Document - 8,Other - 9,Sales - 10,Product Guide - 11,Installation - 12
        ///        leadId:leadId 
        ///        IsFranchiseSettings: false
        ///        IsEnabled: false
        ///        files: (binary)
        ///     }
        ///     Form Data //Account Attachment
        ///     {
        ///        title: title,
        ///        categories: 6 //Photo - 6,Plans - 7,Document - 8,Other - 9,Sales - 10,Product Guide - 11,Installation - 12
        ///        accountId:accountId 
        ///        IsFranchiseSettings: false
        ///        IsEnabled: false
        ///        files: (binary)
        ///     }
        ///     Form Data //Opportunity Attachment
        ///     {
        ///        title: title,
        ///        categories: 6 //Photo - 6,Plans - 7,Document - 8,Other - 9,Sales - 10,Product Guide - 11,Installation - 12
        ///        opportunityId:opportunityId 
        ///        IsFranchiseSettings: false
        ///        IsEnabled: false
        ///        files: (binary)
        ///     }
        ///     Form Data //Order Attachment
        ///     {
        ///        title: title,
        ///        categories: 6 //Photo - 6,Plans - 7,Document - 8,Other - 9,Sales - 10,Product Guide - 11,Installation - 12
        ///        orderId:orderId 
        ///        IsFranchiseSettings: false
        ///        IsEnabled: false
        ///        files: (binary)
        ///     }
        ///     Form Data //Manage Document
        ///     {
        ///        title: title,
        ///        categories: 10,12,11 //Photo - 6,Plans - 7,Document - 8,Other - 9,Sales - 10,Product Guide - 11,Installation - 12
        ///        IsFranchiseSettings: true
        ///        IsEnabled: true
        ///        files: (binary)
        ///     }
        ///     
        /// </remarks>       
        /// <returns></returns>
        [HttpPost, Route("api/File/UploadFileTP")]
        public IHttpActionResult UploadFileTP()
        {
            //, int Id1, string Branch, string Document
            try
            {
                var user = Getuser();
                var franchise = GetFranchise();
                int? leadId = null; int? accountId = null; int? opportunityId = null; int? orderId = null;
                string title = ""; int? category = null;
                bool IsFranchiseSettings = false; bool? IsEnabled = null;
                int Id1 = 0;
                bool? sysWideIsEnabled = null; string sysWideTitle = ""; int? sysWideCategory = null;
                int? sysWideConcept = null; string Document = "";
                string categories = null;

                var httpRequest = HttpContext.Current.Request;
                var files = httpRequest.Files;

                if (httpRequest.Form["leadId"] != null)
                    leadId = Convert.ToInt32(httpRequest.Form["leadId"]);
                if (httpRequest.Form["accountId"] != null)
                    accountId = Convert.ToInt32(httpRequest.Form["accountId"]);
                if (httpRequest.Form["opportunityId"] != null)
                    opportunityId = Convert.ToInt32(httpRequest.Form["opportunityId"]);
                if (httpRequest.Form["orderId"] != null)
                    orderId = Convert.ToInt32(httpRequest.Form["orderId"]);

                if (httpRequest.Form["Id1"] != null)
                    Id1 = Convert.ToInt32(httpRequest.Form["Id1"]);

                if (httpRequest.Form["title"] != null)
                    title = Convert.ToString(httpRequest.Form["title"]);
                if (httpRequest.Form["category"] != null)
                    category = Convert.ToInt32(httpRequest.Form["category"]);

                if (httpRequest.Form["categories"] != null)
                    categories = Convert.ToString(httpRequest.Form["categories"]);

                if (httpRequest.Form["IsFranchiseSettings"] != null)
                    IsFranchiseSettings = Convert.ToBoolean(httpRequest.Form["IsFranchiseSettings"]);
                if (httpRequest.Form["IsEnabled"] != null)
                    IsEnabled = Convert.ToBoolean(httpRequest.Form["IsEnabled"]);

                if (httpRequest.Form["sysWideIsEnabled"] != null)
                    sysWideIsEnabled = Convert.ToBoolean(httpRequest.Form["sysWideIsEnabled"]);
                if (httpRequest.Form["sysWideTitle"] != null)
                    sysWideTitle = Convert.ToString(httpRequest.Form["sysWideTitle"]);
                if (httpRequest.Form["sysWideCategory"] != null)
                    sysWideCategory = Convert.ToInt32(httpRequest.Form["sysWideCategory"]);
                if (httpRequest.Form["sysWideConcept"] != null)
                    sysWideConcept = Convert.ToInt32(httpRequest.Form["sysWideConcept"]);
                if (httpRequest.Form["Document"] != null)
                    Document = Convert.ToString(httpRequest.Form["Document"]);

                var LeadMgr = new LeadsManager(user, franchise);
                var notemgr = new NoteManager(user, franchise);

                if (files != null && files.Count > 0)
                {
                    byte[] fileData = null;
                    using (var binaryReader = new BinaryReader(files[0].InputStream))
                    {
                        fileData = binaryReader.ReadBytes(files[0].ContentLength);
                    }
                    // We are allways getting one file.
                    // TODO: we need to find why we are not getting multiple files.
                    var streamId = LeadMgr.UploadFileTP(fileData, files[0].FileName);

                    if (Document == "SYS-DOC")
                    {
                        string filename = files[0].FileName;
                        notemgr.UpdateGlobalDocumentStreamId(Id1, sysWideTitle, sysWideIsEnabled, sysWideCategory, sysWideConcept, streamId, filename);
                        return Json(true);
                    }



                    // TODO: Update the Note table with this.
                    var newnote = new Note
                    {
                        Title = title,
                        TypeEnum = 0, // (NoteTypeEnum)category,
                        Categories = categories,
                        IsNote = false,
                        AttachmentStreamId = new Guid(streamId),
                        CreatedOn = DateTime.Now,
                        CreatedBy = user.PersonId,
                    };

                    newnote.LeadId = (leadId != null && leadId > 0) ? leadId : null;
                    newnote.AccountId = (accountId != null && accountId > 0) ? accountId : null;
                    newnote.OpportunityId = (opportunityId != null && opportunityId > 0) ? opportunityId : null;
                    newnote.OrderId = (orderId != null && orderId > 0) ? orderId : null;
                    newnote.IsEnabled = IsEnabled;

                    if (IsFranchiseSettings)
                    {
                        newnote.FranchiseId = user.FranchiseId;
                    }
                    //TODO: if all the above are null, then it is an invalid entry. 

                    notemgr.Add(newnote);

                    return Json(new
                    {
                        Message = "Success",
                        Filename = files[0].FileName,
                        FileId = streamId
                    });
                }

                string status = "Invalid file";

                return Json(status);
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return Json(ex.Message);
            }
            //string status = "Invalid file";
        }


        [HttpPost, Route("UpdateGlobalDocDetails")]
        public IHttpActionResult UpdateGlobalDocDetails()//            int Id1, string sysWideTitle, int sysWideCategory, int sysWideConcept, bool sysWideIsEnabled)
        {
            try
            {
                var Request = HttpContext.Current.Request;
                int Id = Convert.ToInt32(Request.Form["Id1"]);
                string tiltle = Request.Form["sysWideTitle"];
                int Category = Convert.ToInt32(Request.Form["sysWideCategory"]);
                int Concept = Convert.ToInt32(Request.Form["sysWideConcept"]);
                bool IsEnabled = bool.Parse(Request.Form["sysWideIsEnabled"]);
                var notemgr = new NoteManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                notemgr.UpdateGlobalDocumentStreamId(Id, tiltle, IsEnabled, Category, Concept, "", "");
                return Json("Success");
            }
            catch (Exception ex)
            {
                // Response.StatusCode = 400;
                //Response.StatusDescription = ex.Message;
                EventLogger.LogEvent(ex);
                return Json(ex.Message);
            }

        }


        /// <summary>
        /// This is used to post file
        /// </summary>
        /// <param name="CaseId"></param>
        /// <param name="Module"></param>
        /// <param name="flag"></param>
        /// <returns></returns>
        [HttpPost, Route("api/File/UploadAttachment/{CaseId}/{Module}/{flag}")]
        public IHttpActionResult UploadAttachment(int CaseId, string Module, bool flag) // , List<franchiseCaseAttachmentDetails> description = null // string fileIconType=null, string fileSize=null, string fileName=null, string color=null
        {
            LeadsManager LeadMgr = new LeadsManager(Getuser(), GetFranchise());
            CaseAddEditManager Case_Mangr = new CaseAddEditManager(GetFranchise(), Getuser());
            try
            {
                //  List<franchiseCaseAttachmentDetails> description = new List<franchiseCaseAttachmentDetails>();
                var httpRequest = HttpContext.Current.Request;
                var files = httpRequest.Files;
                // var ee = httpRequest.Form["description"];
                //  int CaseId = id;
                var description = JsonConvert.DeserializeObject<List<franchiseCaseAttachmentDetails>>(httpRequest.Form["description"]);
                List<franchiseCaseAttachmentDetails> fcad = new List<franchiseCaseAttachmentDetails>();

                if (files != null && files.Count > 0)
                {
                    for (int q = 0; q < files.Count; q++)
                    {
                        byte[] fileData = null;
                        using (var binaryReader = new BinaryReader(files[q].InputStream))
                        {
                            fileData = binaryReader.ReadBytes(files[q].ContentLength);
                        }
                        var streamId = LeadMgr.UploadFileTP(fileData, files[q].FileName);

                        List<string> ImageExtensions = new List<string> { ".JPG", ".JPEG", ".BMP", ".GIF", ".PNG" };
                        string extension = Path.GetExtension(files[q].FileName).ToUpper();
                        var streamId1 = "";
                        var VendorCaseId = CaseId;
                        if (ImageExtensions.Contains(extension))
                        {
                            WebImage img = new WebImage(fileData);
                            img.Resize(50, 30);
                            byte[] thump = img.GetBytes();
                            string thumpfileName = DateTime.UtcNow.ToString("MMddyyyy") + "_thump_" + DateTime.UtcNow.ToString("HHMMssfff") + "_" + files[q].FileName;
                            streamId1 = LeadMgr.UploadFileTP(thump, thumpfileName);
                        }

                        var res = Case_Mangr.saveFranchiseCaseAttachmentDetails(CaseId, VendorCaseId, Module, description[q].fileIconType, description[q].fileSize, description[q].fileName, description[q].color, streamId, streamId1, flag);
                        fcad.Add(res);
                    }
                    return Json(fcad);
                }
                return Json("File Not found");
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return Json("Error Occurred");
            }

        }

        /// <summary>
        /// Upload Attachment for VendorGovernance
        /// </summary>
        /// <param name="id">ChangeRequestCaseID</param>
        /// <param name="flag"></param>
        /// <returns></returns>
        [HttpPost, Route("api/File/UploadAttachmentVendorGovernance/{id}/{flag}")]
        public IHttpActionResult UploadAttachmentVendorGovernance(int id, bool flag)
        {
            try
            {
                var httpRequest = HttpContext.Current.Request;
                var files = httpRequest.Files;
                int CaseId = id;
                var description = JsonConvert.DeserializeObject<List<CaseAttachments>>(httpRequest.Form["description"]);
                List<CaseAttachments> fcad = new List<CaseAttachments>();
                LeadsManager LeadMgr = new LeadsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                CaseManager CaseVg_Mangr = new CaseManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                if (files != null && files.Count > 0)
                {
                    for (int q = 0; q < files.Count; q++)
                    {
                        byte[] fileData = null;
                        using (var binaryReader = new BinaryReader(files[q].InputStream))
                        {
                            fileData = binaryReader.ReadBytes(files[q].ContentLength);
                        }
                        var streamId = LeadMgr.UploadFileTP(fileData, files[q].FileName);

                        List<string> ImageExtensions = new List<string> { ".JPG", ".JPEG", ".BMP", ".GIF", ".PNG" };
                        string extension = Path.GetExtension(files[q].FileName).ToUpper();
                        var streamId1 = "";
                        if (ImageExtensions.Contains(extension))
                        {
                            WebImage img = new WebImage(fileData);
                            img.Resize(50, 30);
                            byte[] thump = img.GetBytes();
                            string thumpfileName = DateTime.UtcNow.ToString("MMddyyyy") + "_thump_" + DateTime.UtcNow.ToString("HHMMssfff") + "_" + files[q].FileName;
                            streamId1 = LeadMgr.UploadFileTP(thump, thumpfileName);
                        }

                        var res = CaseVg_Mangr.saveCaseAttachmentDetails(CaseId, description[q].FileIconType, description[q].FileDetails, description[q].FileName, streamId, streamId1, flag);
                        fcad.Add(res);
                    }
                    return Json(fcad);
                }
                return Json("File Not found");
            }
            catch (Exception ex)
            {
                var message = EventLogger.LogEvent(ex);
                return Json("Error Occurred");
            }
        }
    }
}
