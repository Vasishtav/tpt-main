﻿using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [RoutePrefix("api/Permission")]
    public class PermissionController : BaseController
    {
        /// <summary>
        /// Get PermissionSets List
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetPermissionSetsList")]
        public IHttpActionResult GetPermissionSetsList()
        {
            try
            {
                var user = Getuser();
                var franchise = GetFranchise();
                PermissionManager permissionManager = new PermissionManager(user, franchise);
                var response = permissionManager.GetPermissionSetList();
                if (response != null)
                {
                    return Json(response);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get PermissionSets by PermissionSetId
        /// </summary>
        /// <param name="PermissionSetId"></param>
        /// <returns></returns>
        [HttpGet, Route("GetPermission/{PermissionSetId}")]
        public IHttpActionResult GetPermission(int PermissionSetId)
        {
            try
            {
                var user = Getuser();
                var franchise = GetFranchise();
                PermissionManager permissionManager = new PermissionManager(user, franchise);
                // This will get the Franchise permission set
                var response = permissionManager.GetPermissionSet(PermissionSetId, 2);
                if (response != null)
                {
                    return Json(response);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Update PermissionSet using PermissionSetId
        /// </summary>
        /// <param name="permissionSets"></param>
        /// <returns></returns>
        [HttpPost, Route("UpdatePermissionSet")]
        public IHttpActionResult UpdatePermissionSet(Permissionset_VM permissionSets)
        {
            try
            {
                var user = Getuser();
                var franchise = GetFranchise();
                PermissionManager permissionManager = new PermissionManager(user, franchise);
                var status = permissionManager.UpdatePermissionSet(permissionSets);
                return Json(status);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Update Permission to Particular Role by RoleId
        /// </summary>
        /// <param name="permissionSets"></param>
        /// <returns></returns>
        [HttpPost, Route("UpdatePermissionSetRole")]
        public IHttpActionResult UpdatePermissionSetRole(Permissionset_VM permissionSets)
        {
            try
            {
                var user = Getuser();
                var franchise = GetFranchise();
                PermissionManager permissionManager = new PermissionManager(user, franchise);
                var status = permissionManager.UpdatePermissionSetRole(permissionSets);
                return Json(status);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get User Permission based on assinged roles
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetUserRolesPermission")]
        public IHttpActionResult GetUserRolesPermission()
        {
            try
            {
                var user = Getuser();
                var franchise = GetFranchise();
                PermissionManager permissionManager = new PermissionManager(user, franchise);
                var response = permissionManager.GetUserRolesPermissions();
                if (response != null)
                {
                    return Json(response);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Check Duplicate PermissionSet
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpPost, Route("CheckDuplicatePermissionSet")]
        public IHttpActionResult CheckDuplicatePermissionSet(int id, string name)
        {
            try
            {
                var user = Getuser();
                var franchise = GetFranchise();
                PermissionManager permissionManager = new PermissionManager(user, franchise);
                var response = permissionManager.CheckDuplicatePermissionSets(id, name);
                return Json(response);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get Roles List
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetRolesList")]
        public IHttpActionResult GetRolesList()
        {
            try
            {
                var user = Getuser();
                var franchise = GetFranchise();
                PermissionManager permissionManager = new PermissionManager(user, franchise);
                var response = permissionManager.GetRolesList();
                if (franchise != null)
                    response = response.Where(x => x.RoleType == 2).ToList();
                if (response != null)
                {
                    return Json(response);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get Permission By RolesId
        /// </summary>
        /// <param name="roleid"></param>
        /// <returns></returns>
        [HttpGet, Route("GetPermissionByRolesId/{roleid}")]
        public IHttpActionResult GetPermissionByRolesId(System.Guid roleid)
        {
            try
            {
                var user = Getuser();
                var franchise = GetFranchise();
                PermissionManager permissionManager = new PermissionManager(user, franchise);
                var response = permissionManager.GetPermissionSetbyRole(roleid);
                if (response != null)
                {
                    return Json(response);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get Assigned UserList
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Route("GetAssignedUserList/{id}")]
        public IHttpActionResult GetAssignedUserList(int id)
        {
            try
            {
                var user = Getuser();
                var franchise = GetFranchise();
                PermissionManager permissionManager = new PermissionManager(user, franchise);
                var response = permissionManager.GetAssignedUser(id);
                if (response != null)
                {
                    return Json(response);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Get RoleName
        /// </summary>
        /// <param name="roleid"></param>
        /// <returns></returns>
        [HttpGet, Route("GetRoleName/{roleid}")]
        public IHttpActionResult GetRoleName(System.Guid roleid)
        {
            try
            {
                var user = Getuser();
                var franchise = GetFranchise();
                PermissionManager permissionManager = new PermissionManager(user, franchise);
                var value = permissionManager.GetRoleName(roleid);
                return Json(value);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }

        /// <summary>
        /// Disable/Enable DataSet
        /// </summary>
        /// <param name="id"></param>
        /// <param name="Type"></param>
        /// <returns></returns>
        [HttpGet, Route("DisableEnableDataSet/{id}/{Type}")]
        public IHttpActionResult DisableEnableDataSet(int id, string Type)
        {
            try
            {
                var user = Getuser();
                var franchise = GetFranchise();
                PermissionManager permissionManager = new PermissionManager(user, franchise);
                var value = permissionManager.DisablePermissionSets(id, Type);
                return Json(value);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ResponseResult(ex);
            }
        }
    }
}
