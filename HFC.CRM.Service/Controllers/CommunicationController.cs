﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    [RoutePrefix("api/Communication")]
    public class CommunicationController : BaseController
    {
        #region Communication Tabs Lead/Account/Opportunity

        /// <summary>
        /// Get Lead Communication
        /// </summary>
        /// <param name="id">LeadId</param>
        /// <param name="filterOptions"></param>
        /// <returns></returns>
        [HttpGet, Route("GetLeadCommunication/{id}")]
        public IHttpActionResult GetLeadCommunication(int id, string filterOptions)
        {
            try
            {
                var manager = new CalendarManager(SessionManager.CurrentUser);
                var result = manager.GetCommunicationForLead(id, filterOptions);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Account Communication
        /// </summary>
        /// <param name="id">AccountId</param>
        /// <param name="filterOptions"></param>
        /// <returns></returns>
        [HttpGet, Route("GetAccountCommunication/{id}")]
        public IHttpActionResult GetAccountCommunication(int id, string filterOptions)
        {
            try
            {
                var manager = new CalendarManager(SessionManager.CurrentUser);
                var result = manager.GetCommunicationForAccount(id, filterOptions);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }

        /// <summary>
        /// Get Opportunity Communication
        /// </summary>
        /// <param name="id">OpportunityId</param>
        /// <param name="filterOptions"></param>
        /// <returns></returns>
        [HttpGet, Route("GetOpportunityCommunication/{id}")]
        public IHttpActionResult GetOpportunityCommunication(int id, string filterOptions)
        {
            try
            {
                var manager = new CalendarManager(SessionManager.CurrentUser);
                var result = manager.GetCommunicationForOpportunity(id, filterOptions);
                return Json(result);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
        #endregion


        #region ----- Tochpoint/SendEmail -----
        /// <summary>
        /// Resend Email
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet,Route("ResendEmail/{id}")]
        public IHttpActionResult ResendEmail(int id)
        {
            CommunicationManager Communication = new CommunicationManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var value = Communication.ResendEmailNew(id);
            if (value == "Success")
                return Json(true);
            else return Json(value);
        }
        #endregion ----- Tochpoint/SendEmail -----

        /// <summary>
        /// Get Email deatils
        /// </summary>
        /// <param name="id">SentEmailId</param>
        /// <returns></returns>
        [HttpGet,Route("GetEmaildeatils/{id}")]
        public IHttpActionResult GetEmaildeatils(int id)
        {
            CommunicationManager Communication = new CommunicationManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var value = Communication.GetHistory(id);
            return Json(value);
        }

        /// <summary>
        /// Get Resend Email Data
        /// </summary>
        /// <param name="id">SentEmailId</param>
        /// <returns></returns>
        [HttpGet, Route("GetResendEmailData/{id}")]
        public IHttpActionResult GetResendEmailData(int id)
        {
            CommunicationManager Communication = new CommunicationManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var emailvalue = Communication.GetEmaildetails(id);
            return Json(emailvalue);
        }

        /// <summary>
        /// Get Texting Details
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet,Route("GetTextingDetails/{id}")]
        public IHttpActionResult GetTextingDetails(int id)
        {
            CommunicationManager Communication = new CommunicationManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var value = Communication.GetTextingDetails(id);
            return Json(value);
        }

        /// <summary>
        /// Resend Texting
        /// </summary>
        /// <param name="id">TextingId</param>
        /// <returns></returns>
        [HttpPost,Route("ResendTexting/{id}")]
        public IHttpActionResult ResendTexting(int id)
        {
            try
            {
                CommunicationManager Communication = new CommunicationManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var value = Communication.ResendTexting(id);
                if (value == "Success")
                    return Json(true);
                else return Json(value);
            }
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }

        }

        /// <summary>
        /// Get Texting Status
        /// </summary>
        /// <param name="cellPhone"></param>
        /// <returns></returns>
        [HttpGet,Route("GetTextingStatus/{cellPhone}")]
        public IHttpActionResult GetTextingStatus(string cellPhone)
        {
            try
            {
                CommunicationManager Communication = new CommunicationManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var result = Communication.GetOptingInfo(cellPhone);

                if (result != null)
                    return Json(new { optOut = result.Optout, optIn = result.Optin, msgSent = result.IsOptinmessagesent });
                else
                    return Json(result);

            }            
            catch (Exception ex)
            {
                return ResponseResult(EventLogger.LogEvent(ex));
            }
        }
    }
}
