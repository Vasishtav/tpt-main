﻿using HFC.CRM.Core.Common;
using HFC.CRM.Managers.AdvancedEmailManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HFC.CRM.Service.Controllers
{
    [RoutePrefix("api/AppointmentTypeColor")]
    public class AppointmentTypeColorController : BaseController
    {
        /// <summary>
        /// Get Colors
        /// </summary>
        /// <returns></returns>
        [HttpGet,Route("GetColors")]
        public IHttpActionResult GetColors()
        {
            AppointmentTypeColorManager colorManager = new AppointmentTypeColorManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var colorlist = colorManager.GetColors();
            return Json(colorlist);
        }
    }
}
