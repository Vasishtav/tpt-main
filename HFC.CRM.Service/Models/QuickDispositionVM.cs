﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HFC.CRM.Service.Models
{
    public class QuickDispositionVM
    {
        public string name { get; set; }
        public string Value { get; set; }
    }

    public class AccountType
    {
        public string name { get; set; }
        public int Id { get; set; }
    }
}