﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HFC.CRM.Service.Models
{
    public class UserVM
    {
        public string FullName { get; set; }
        public string RoleName { get; set; }
        public int PersonId { get; set; }
    }
}