﻿
using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.DTO.Calandar;
using HFC.CRM.Managers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HFC.CRM.Service.Models
{

    public class EventBase
    {
        public int id { get; set; }
        public bool startEditable { get; set; }
        public bool durationEditable { get; set; }
        public string className { get; set; }

        public virtual ICollection<EventToPersonModel> Attendees { get; set; }
        public virtual ICollection<SalesAgents> AttendeesTP { get; set; }
        /// <summary>
        /// Equivalent to Subject
        /// </summary>        
        public string title { get; set; }
        public string AppointmentTypeColor { get; set; }
        public string UserRoleColor { get; set; }
        /// <summary>
        /// Equivalent to StartDateUtc for Calendar, DueDateUtc for tasks
        /// </summary>        


        public bool editable { get; set; }
        public bool allDay { get; set; }


        //Properties below are extended properties
        public string Location { get; set; }
        public string Message { get; set; }
        public Nullable<int> LeadId { get; set; }
        public Nullable<int> AccountId { get; set; }
        public string AccountName { get; set; }
        public string AccountPhone { get; set; }
        public Nullable<int> OpportunityId { get; set; }
        public string OpportunityName { get; set; }
        public Nullable<int> OrderId { get; set; }
        public Nullable<int> CaseId { get; set; }
        public Nullable<int> VendorCaseId { get; set; }

        public bool? IsPrivateAccess { get; set; }
        public Nullable<int> LeadNumber { get; set; }
        public bool IsNotifyemails { get; set; }

        public bool IsNotifyText { get; set; }
        public int? JobId { get; set; }
        public int? JobNumber { get; set; }
        public bool? IsPrivate { get; set; }
        private System.DateTime _CreatedOnUtc = DateTime.Now;
        public System.DateTime CreatedOnUtc
        {
            get { return _CreatedOnUtc; }
            set { _CreatedOnUtc = DateTime.SpecifyKind(value, DateTimeKind.Unspecified); }
        }
        private System.DateTime _LastUpdatedUtc = DateTime.Now;
        public System.DateTime LastUpdatedUtc
        {
            get { return _LastUpdatedUtc; }
            set { _LastUpdatedUtc = DateTime.SpecifyKind(value, DateTimeKind.Unspecified); }
        }
        public Nullable<int> LastUpdatedByPersonId { get; set; }
        public int RevisionSequence { get; set; }
        public int? OrganizerPersonId { get; set; }
        public string OrganizerEmail { get; set; }
        public string OrganizerName { get; set; }

        public int? AssignedPersonId { get; set; }
        public string AssignedName { get; set; }
        /// <summary>
        /// List of attendees email
        /// </summary>
        public ICollection<string> AdditionalPeople { get; set; }
        /// <summary>
        /// Get and Set Reminder minute.  Set to 0 to disable all reminders
        /// </summary>
        public short ReminderMinute { get; set; }
        public bool IsDeletable { get; set; }
        //public Nullable<bool> IsAccepted { get; set; }                          
        public bool? IsCancelled { get; set; }

        /// <summary>
        /// Get and Set Reminder Method bitwise flags.  Set to null to disable all methods.
        /// </summary>
        public RemindMethodEnum? ReminderMethods { get; set; }

        public string PhoneNumber { get; set; }

        [JsonIgnore]
        public bool IsDeleted { get; set; }

        public string emailType { get; set; }
        //Used for comparison purposes
        [JsonIgnore]
        public Guid GlobalId { get; set; }
        public int PersonId { get; set; }

        public override int GetHashCode()
        {
            return GlobalId.GetHashCode();
        }

        public string RRRule { get; set; }

        public bool AddRecurrence { get; set; }

        public string RecurrenceException { get; set; }
        public bool SendEmail { get; set; }
    }

    public class EventModel : EventBase
    {

        public DateTime start { get; set; }
        //  public DateTimeOffset start { get; set; }

        /// <summary>
        /// Equivalent to EndDateUtc
        /// </summary>     
        public DateTime end { get; set; }
        public override bool Equals(object obj)
        {
            if (obj is EventModel)
            {
                var compareB = obj as EventModel;
                return GlobalId.Equals(compareB.GlobalId) && start == compareB.start && end == compareB.end;
            }
            else
                return false;
        }


    }

    /// <summary>
    /// Used for combining calendar and task events to display on calendar
    /// </summary>    
    public class CalendarModel : EventModel
    {
        public EventTypeEnum EventType { get; set; }
        public int? RecurringEventId { get; set; }
        public AppointmentTypeEnumTP? AptTypeEnum { get; set; }
        public string AppointmentTypeName { get; set; }
        public string CalName { get; set; }
        public EventRecurringModel EventRecurring { get; set; }
        public System.DateTimeOffset? OriginalStartDate { get; set; }
        public bool EnableBorders { get; set; }
        public string QuickDispositionValue { get; set; }
    }

    public class TaskModel : EventBase
    {
        public DateTime? start { get; set; }
        //  public DateTimeOffset start { get; set; }

        /// <summary>
        /// Equivalent to EndDateUtc
        /// </summary>     
        public DateTime? end { get; set; }
        public DateTime? CompletedDateUtc { get; set; }
        public int? PriorityOrder { get; set; }
        public override bool Equals(object obj)
        {
            if (obj is EventModel)
            {
                var compareB = obj as EventModel;
                return GlobalId.Equals(compareB.GlobalId) && start == compareB.start && end == compareB.end;
            }
            else
                return false;
        }



    }

    public class EventToPersonModel
    {
        public int EventPersonId { get; set; }
        public Nullable<int> TaskId { get; set; }
        public Nullable<int> CalendarId { get; set; }
        public Nullable<int> PersonId { get; set; }
        public string PersonEmail { get; set; }
        public Nullable<RemindMethodEnum> RemindMethodEnum { get; set; }
        public string PersonName { get; set; }
        public Nullable<System.DateTimeOffset> RemindDate { get; set; }
        public Nullable<System.DateTimeOffset> DismissedDate { get; set; }
        public Nullable<System.DateTimeOffset> SnoozedDate { get; set; }
        public Nullable<System.DateTimeOffset> RecurringNextStartDate { get; set; }
        public Nullable<System.DateTimeOffset> RecurringNextEndDate { get; set; }

        //public static List<EventToPerson> MapFrom(List<EventToPersonModel> vm)
        //{
        //    List<EventToPerson> model = new List<EventToPerson>();
        //    if (vm != null)
        //    {
        //        foreach (var obj in vm)
        //        {
        //            model.Add(EventToPersonModel.MapFrom(obj));
        //        }
        //    }
        //    return model;
        //}

        //public static EventToPerson MapFrom(EventToPersonModel vm)
        //{
        //    EventToPerson model = new EventToPerson();
        //    model.EventPersonId = vm.EventPersonId;
        //    model.TaskId = vm.TaskId;
        //    model.CalendarId = vm.CalendarId;
        //    model.PersonId = vm.PersonId;
        //    model.PersonEmail = vm.PersonEmail;
        //    model.RemindMethodEnum = vm.RemindMethodEnum;
        //    model.PersonName = vm.PersonName;
        //    model.RemindDate = vm.RemindDate;
        //    model.DismissedDate = vm.DismissedDate;
        //    model.SnoozedDate = vm.SnoozedDate;
        //    model.RecurringNextStartDate = vm.RecurringNextStartDate;
        //    model.RecurringNextEndDate = vm.RecurringNextEndDate;
        //    return model;
        //}

        public static ICollection<EventToPersonModel> MapFrom(ICollection<EventToPerson> vm)
        {
            if (vm == null)
                return new List<EventToPersonModel>();

            List<EventToPersonModel> model = new List<EventToPersonModel>();
            if (vm != null)
            {
                foreach (var obj in vm)
                {
                    model.Add(MapFrom(obj));
                }
            }
            return model;
        }

        public static EventToPersonModel MapFrom(EventToPerson vm)
        {
            if (vm == null)
                return null;
            EventToPersonModel model = new EventToPersonModel();
            model.EventPersonId = vm.EventPersonId;
            model.TaskId = vm.TaskId;
            model.CalendarId = vm.CalendarId;
            model.PersonId = vm.PersonId;
            model.PersonEmail = vm.PersonEmail;
            model.RemindMethodEnum = vm.RemindMethodEnum;
            model.PersonName = vm.PersonName;
            model.RemindDate = vm.RemindDate;
            model.DismissedDate = vm.DismissedDate;
            model.SnoozedDate = vm.SnoozedDate;
            model.RecurringNextStartDate = vm.RecurringNextStartDate;
            model.RecurringNextEndDate = vm.RecurringNextEndDate;
            return model;
        }

    }

    public class EventRecurringModel
    {
        public int RecurringEventId { get; set; }
        public Nullable<int> EndsAfterXOccurrences { get; set; }
        public int RecursEvery { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTimeOffset> EndsOn { get; set; }
        public Nullable<DayOfWeekEnum> DayOfWeekEnum { get; set; }
        public Nullable<MonthEnum> MonthEnum { get; set; }
        public Nullable<short> DayOfMonth { get; set; }
        public RecurringPatternEnum PatternEnum { get; set; }
        public Nullable<DayOfWeekIndexEnum> DayOfWeekIndex { get; set; }
        public System.DateTimeOffset StartDate { get; set; }
        public System.DateTimeOffset EndDate { get; set; }

        public static EventRecurring MapFrom(EventRecurringModel vm)
        {
            if (vm == null)
                return null;

            EventRecurring model = new EventRecurring();
            model.RecurringEventId = vm.RecurringEventId;
            model.EndsAfterXOccurrences = vm.EndsAfterXOccurrences;
            model.RecursEvery = vm.RecursEvery;
            model.IsDeleted = vm.IsDeleted;
            model.EndsOn = vm.EndsOn;
            model.DayOfWeekEnum = vm.DayOfWeekEnum;
            model.MonthEnum = vm.MonthEnum;
            model.DayOfMonth = vm.DayOfMonth;
            model.PatternEnum = vm.PatternEnum;
            model.DayOfWeekIndex = vm.DayOfWeekIndex;
            model.StartDate = vm.StartDate;
            model.EndDate = vm.EndDate;
            return model;
        }

        public static EventRecurringModel MapFrom(EventRecurring vm)
        {
            if (vm == null)
                return null;
            EventRecurringModel model = new EventRecurringModel();
            model.RecurringEventId = vm.RecurringEventId;
            model.EndsAfterXOccurrences = vm.EndsAfterXOccurrences;
            model.RecursEvery = vm.RecursEvery;
            model.IsDeleted = vm.IsDeleted;
            model.EndsOn = vm.EndsOn;
            model.DayOfWeekEnum = vm.DayOfWeekEnum;
            model.MonthEnum = vm.MonthEnum;
            model.DayOfMonth = vm.DayOfMonth;
            model.PatternEnum = vm.PatternEnum;
            model.DayOfWeekIndex = vm.DayOfWeekIndex;
            model.StartDate = vm.StartDate;
            model.EndDate = vm.EndDate;
            return model;
        }
    }


    #region Extended helper methods

    public static class CalendarExtensions
    {
        public static TaskModel ToTaskModel(this Task task)
        {

            return new TaskModel
            {
                id = task.TaskId,
                GlobalId = task.TaskGuid,
                IsDeleted = task.IsDeleted,
                PersonId = task.PersonId,
                title = task.Subject,
                Message = task.Message,
                ReminderMinute = task.ReminderMinute,
                ReminderMethods = task.RemindMethodEnum,
                AdditionalPeople = task.Attendees != null && task.Attendees.Count() > 0 ? task.Attendees.Where(w => w.PersonId.HasValue).Select(x => x.PersonEmail).ToList() : null,
                OrganizerPersonId = task.OrganizerPersonId,
                AssignedPersonId = task.AssignedPersonId,
                Attendees = EventToPersonModel.MapFrom(task.Attendees),
                AssignedName = task.AssignedName,
                className = task.ColorClassName,
                CreatedOnUtc = TimeZoneManager.ToLocal(task.CreatedOnUtc),
                start = DateTime.SpecifyKind(Convert.ToDateTime(((DateTimeOffset)task.DueDate).Date), DateTimeKind.Utc),
                end = DateTime.SpecifyKind(Convert.ToDateTime(((DateTimeOffset)task.DueDate).Date), DateTimeKind.Utc),
                allDay = true,
                IsDeletable = task.IsDeletable,
                editable = task.IsEditable,
                LeadId = task.LeadId,
                AccountId = task.AccountId,
                CaseId = task.CaseId,
                VendorCaseId = task.VendorCaseId,
                OpportunityId = task.OpportunityId,
                OrderId = task.OrderId,
                LeadNumber = task.LeadNumber,
                PhoneNumber = task.PhoneNumber,
                JobId = task.JobId,
                JobNumber = task.JobNumber,
                PriorityOrder = task.PriorityOrder,
                CompletedDateUtc = TimeZoneManager.ToLocal(task.CompletedDate),
                IsPrivate = task.IsPrivate == null ? false : task.IsPrivate,
                RevisionSequence = task.RevisionSequence
            };
        }

        public static CalendarModel ToCalendarModel(this CalendarVM calendar)
        {
            var result = new CalendarModel
            {
                id = calendar.CalendarId,
                GlobalId = calendar.CalendarGuid,
                title = calendar.Subject,
                Message = calendar.Message,
                PersonId = calendar.PersonId,
                AccountId = calendar.AccountId,
                OpportunityId = calendar.OpportunityId,
                OrderId = calendar.OrderId,
                RRRule = calendar.RRRule,
                end = calendar.EndDate,
                start = calendar.StartDate,
                //end = DateTime.SpecifyKind(calendar.EndDate.DateTime, DateTimeKind.Utc), 
                //start = DateTime.SpecifyKind(calendar.StartDate.DateTime, DateTimeKind.Utc),
                ReminderMinute = calendar.ReminderMinute,
                ReminderMethods = calendar.RemindMethodEnum,
                AdditionalPeople = calendar.Attendees != null ? calendar.Attendees.Where(w => w.PersonId.HasValue).Select(x => x.PersonEmail).ToList() : null,
                Attendees = EventToPersonModel.MapFrom(calendar.Attendees),
                AptTypeEnum = calendar.AptTypeEnum,
                OrganizerPersonId = calendar.OrganizerPersonId,
                OrganizerEmail = calendar.OrganizerEmail,
                OrganizerName = calendar.OrganizerName,
                AssignedPersonId = calendar.AssignedPersonId,
                AssignedName = calendar.AssignedName,
                className = calendar.ColorClassName,
                CreatedOnUtc = DateTime.SpecifyKind((DateTime)calendar.CreatedOnUtc, DateTimeKind.Utc),
                Location = calendar.Location,
                allDay = calendar.IsAllDay,
                RevisionSequence = calendar.RevisionSequence,
                IsDeletable = calendar.IsDeletable,
                editable = calendar.IsEditable,
                startEditable = calendar.IsEditable,
                durationEditable = calendar.IsEditable,
                IsCancelled = calendar.IsCancelled,
                LeadId = calendar.LeadId,
                PhoneNumber = calendar.PhoneNumber,
                LeadNumber = calendar.LeadNumber,
                JobId = calendar.JobId,
                JobNumber = calendar.JobNumber,
                IsDeleted = calendar.IsDeleted,
                IsPrivate = calendar.IsPrivate == null ? false : calendar.IsPrivate,
                EventType = calendar.EventTypeEnum,
                RecurringEventId = calendar.RecurringEventId,
                EventRecurring = EventRecurringModel.MapFrom(calendar.EventRecurring),
                CaseId = calendar.CaseId,
                VendorCaseId = calendar.VendorCaseId,
                IsPrivateAccess = calendar.IsPrivateAccess,
                OpportunityName = calendar.OpportunityName,
                AccountName = calendar.AccountName,
                AccountPhone = calendar.AccountPhone,
                RecurrenceException = calendar.RecurrenceException
            };

            return result;
        }

        public static CalendarModel ToCalendarModel(this Calendar calendar)
        {
            var result = new CalendarModel
            {
                id = calendar.CalendarId,
                GlobalId = calendar.CalendarGuid,
                title = calendar.Subject,
                Message = calendar.Message,
                PersonId = calendar.PersonId,
                AccountId = calendar.AccountId,
                OpportunityId = calendar.OpportunityId,
                OrderId = calendar.OrderId,
                RRRule = calendar.RRRule,
                end = DateTime.SpecifyKind(TimeZoneManager.ToLocal(calendar.EndDate.DateTime), DateTimeKind.Utc),
                start = DateTime.SpecifyKind(TimeZoneManager.ToLocal(calendar.StartDate.DateTime), DateTimeKind.Utc),
                ReminderMinute = calendar.ReminderMinute,
                ReminderMethods = calendar.RemindMethodEnum,
                AdditionalPeople = calendar.Attendees != null ? calendar.Attendees.Where(w => w.PersonId.HasValue).Select(x => x.PersonEmail).ToList() : null,
                Attendees = EventToPersonModel.MapFrom(calendar.Attendees),
                AptTypeEnum = calendar.AptTypeEnum,
                OrganizerPersonId = calendar.OrganizerPersonId,
                OrganizerEmail = calendar.OrganizerEmail,
                OrganizerName = calendar.OrganizerName,
                AssignedPersonId = calendar.AssignedPersonId,
                AssignedName = calendar.AssignedName,
                className = calendar.ColorClassName,
                CreatedOnUtc = DateTime.SpecifyKind((DateTime)calendar.CreatedOnUtc, DateTimeKind.Utc),
                Location = calendar.Location,
                allDay = calendar.IsAllDay,
                RevisionSequence = calendar.RevisionSequence,
                IsDeletable = calendar.IsDeletable,
                editable = calendar.IsEditable,
                startEditable = calendar.IsEditable,
                durationEditable = calendar.IsEditable,
                IsCancelled = calendar.IsCancelled,
                LeadId = calendar.LeadId,
                PhoneNumber = calendar.PhoneNumber,
                LeadNumber = calendar.LeadNumber,
                JobId = calendar.JobId,
                JobNumber = calendar.JobNumber,
                IsDeleted = calendar.IsDeleted,
                IsPrivate = calendar.IsPrivate == null ? false : calendar.IsPrivate,
                EventType = calendar.EventTypeEnum,
                RecurringEventId = calendar.RecurringEventId,
                EventRecurring = EventRecurringModel.MapFrom(calendar.EventRecurring),
                CaseId = calendar.CaseId,
                VendorCaseId = calendar.VendorCaseId,
                IsPrivateAccess = calendar.IsPrivateAccess,
                OpportunityName = calendar.OpportunityName,
                AccountName = calendar.AccountName,
                AccountPhone = calendar.AccountPhone,
                RecurrenceException = calendar.RecurrenceException,
                AppointmentTypeName = calendar.AptTypeEnum.GetDisplayName(),
                CalName = calendar.CalName
            };

            return result;
        }

        /// <summary>
        /// Converts CalendarModelExtended to a Task object
        /// </summary>        
        /// <param name="taskId">If provided, will override the model Id</param>
        /// <param name="franchiseId">Required franchise Id to attach to task</param>        
        public static Task ToTask(this TaskModel model, int franchiseId, int? taskId = null)
        {
            if (model == null || franchiseId == 0)
                throw new ArgumentNullException("Model and franchise Id can not be empty or 0");

            var task = new Task
            {
                TaskId = taskId ?? model.id,
                Subject = model.title,
                Message = model.Message,
                OrganizerPersonId = model.OrganizerPersonId,
                AssignedPersonId = model.AssignedPersonId,
                DueDate = model.end,
                // Are we using this variable
                //CompletedDate = model.CompletedDateUtc != "{1/1/0001 12:00:00 AM}"? model.CompletedDateUtc: DateTime.MinValue,
                ReminderMinute = model.ReminderMinute,
                RemindMethodEnum = model.ReminderMethods,
                LeadId = model.LeadId.HasValue && model.LeadId.Value > 0 ? model.LeadId : null,
                LeadNumber = model.LeadNumber.HasValue && model.LeadNumber.Value > 0 ? model.LeadNumber : null,
                PhoneNumber = model.PhoneNumber,
                JobId = model.JobId.HasValue && model.JobId.Value > 0 ? model.JobId : null,
                JobNumber = model.JobNumber.HasValue && model.JobNumber.Value > 0 ? model.JobNumber : null,
                FranchiseId = franchiseId,
                RevisionSequence = model.RevisionSequence,
                PriorityOrder = model.PriorityOrder,
                AccountId = model.AccountId,
                OpportunityId = model.OpportunityId,
                OrderId = model.OrderId,
                CaseId = model.CaseId,
                VendorCaseId = model.VendorCaseId,
                IsPrivate = model.IsPrivate
            };

            if (model.AdditionalPeople == null)
                model.AdditionalPeople = new List<string>();

            if (model.AssignedPersonId.HasValue)
            {
                var person = CacheManager.UserCollection.FirstOrDefault(f => f.PersonId == model.AssignedPersonId.Value);
                if (person != null && !model.AdditionalPeople.Any(a => string.Equals(a, person.Email, StringComparison.InvariantCultureIgnoreCase)))
                    model.AdditionalPeople.Add(person.Email);
            }

            if (task.RemindMethodEnum.HasValue && task.ReminderMinute > 0 && task.DueDate.HasValue)
                task.FirstRemindDate = task.DueDate.Value.AddMinutes(-task.ReminderMinute);

            task.Attendees = new List<EventToPerson>();
            foreach (var email in model.AdditionalPeople)
            {
                var person = CacheManager.UserCollection.FirstOrDefault(f => string.Equals(f.Email, email, StringComparison.InvariantCultureIgnoreCase) && f.FranchiseId == franchiseId);
                task.Attendees.Add(new EventToPerson
                {
                    PersonId = person != null ? (int?)person.PersonId : null,
                    PersonEmail = email,
                    PersonName = person != null ? person.Person.FullName : null,
                    RemindMethodEnum = task.RemindMethodEnum,
                    RemindDate = task.FirstRemindDate
                });
            }
            return task;

        }

        /// <summary>
        /// Converst CalendarModelExtended to a calendar object
        /// </summary>        
        /// <param name="franchiseId">Required franchise Id to attach to calendar</param>
        /// <param name="calendarId">If provided will override model Id</param>        
        public static CalendarVM ToCalendar(this CalendarModel model, int franchiseId, int? calendarId = null)
        {


            var calendar = new CalendarVM
            {
                CalendarId = calendarId ?? model.id,
                Subject = model.title,
                Message = model.Message,
                AddRecurrence = model.AddRecurrence,
                OrganizerPersonId = model.OrganizerPersonId,
                AssignedPersonId = model.AssignedPersonId,
                RemindMethodEnum = model.ReminderMethods,
                ReminderMinute = model.ReminderMinute,
                StartDate = Convert.ToDateTime(model.start),
                EndDate = model.end,
                LeadId = model.LeadId.HasValue && model.LeadId.Value > 0 ? model.LeadId : null,
                AccountId = model.AccountId.HasValue && model.AccountId.Value > 0 ? model.AccountId : null,
                OpportunityId = model.OpportunityId.HasValue && model.OpportunityId.Value > 0 ? model.OpportunityId : null,
                OrderId = model.OrderId.HasValue && model.OrderId.Value > 0 ? model.OrderId : null,
                CaseId = model.CaseId.HasValue && model.CaseId.Value > 0 ? model.CaseId : null,
                VendorCaseId = model.VendorCaseId.HasValue && model.VendorCaseId.Value > 0 ? model.VendorCaseId : null,
                LeadNumber = model.LeadNumber.HasValue && model.LeadNumber.Value > 0 ? model.LeadNumber : null,
                JobId = model.JobId.HasValue && model.JobId.Value > 0 ? model.JobId : null,
                JobNumber = model.JobNumber.HasValue && model.JobNumber.Value > 0 ? model.JobNumber : null,
                PhoneNumber = model.PhoneNumber,
                Location = model.Location,
                FranchiseId = SessionManager.CurrentFranchise.FranchiseId,
                AptTypeEnum = model.AptTypeEnum.HasValue ? model.AptTypeEnum.Value : AppointmentTypeEnumTP.Appointment,
                IsAllDay = model.allDay,
                IsPrivate = model.IsPrivate,
                IsCancelled = model.IsCancelled,
                RecurringEventId = model.RecurringEventId,
                EventRecurring = EventRecurringModel.MapFrom(model.EventRecurring),
                RevisionSequence = model.RevisionSequence,
                EventTypeEnum = model.EventType
            };

            if (model.ReminderMinute > 0 && model.ReminderMethods.HasValue)
            {
                calendar.FirstRemindDate = calendar.StartDate.AddMinutes(-model.ReminderMinute);
            }

            if (model.AdditionalPeople == null)
                model.AdditionalPeople = new List<string>();

            if (model.AssignedPersonId.HasValue)
            {
                var person = CacheManager.UserCollection.FirstOrDefault(f => f.PersonId == model.AssignedPersonId.Value);
                if (person != null && !model.AdditionalPeople.Any(a => string.Equals(a, person.Email, StringComparison.InvariantCultureIgnoreCase)))
                    model.AdditionalPeople.Add(person.Email);
            }

            calendar.Attendees = new List<EventToPerson>();
            foreach (var email in model.AdditionalPeople)
            {
                var person = CacheManager.UserCollection.FirstOrDefault(f => string.Equals(f.Email, email, StringComparison.InvariantCultureIgnoreCase) && f.FranchiseId == franchiseId);
                calendar.Attendees.Add(new EventToPerson
                {
                    PersonId = person != null ? (int?)person.PersonId : null,
                    PersonEmail = email,
                    PersonName = person != null ? person.Person.FullName : null,
                    RemindMethodEnum = calendar.RemindMethodEnum,
                    RemindDate = calendar.FirstRemindDate
                });
            }

            return calendar;
        }

    }


    #endregion


}