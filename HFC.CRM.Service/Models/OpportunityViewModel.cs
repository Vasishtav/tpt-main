﻿using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HFC.CRM.Service.Models
{
    public class OpportunityViewModel
    {
        public int OpportunityId { get; set; }
        public string OpportunityName { get; set; }
        public string CampaignName { get; set; }
        public bool IsCommercial { get; set; }
        public string SideMark { get; set; }
        public Nullable<int> SalesAgentId { get; set; }
        public Nullable<int> InstallerId { get; set; }
        public Nullable<int> InstallationAddressId { get; set; }
        public Nullable<int> BillingAddressId { get; set; }
        public string Description { get; set; }
        public int LeadId { get; set; }
        public int AccountId { get; set; }
        public bool SkipDuplicateCheck { get; set; }
        public System.Guid OpportunityGuid { get; set; }
        public int OpportunityNumber { get; set; }
        public short OpportunityStatusId { get; set; }
        public Nullable<int> FranchiseId { get; set; }
        public Nullable<int> TerritoryId { get; set; }
        public string TerritoryType { get; set; }
        public Nullable<int> PersonId { get; set; }
        public Nullable<int> SecPersonId { get; set; }
        public string OpportunityStatusName { get; set; }
        public string InstallationAddress { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public string OpportunitySourceId { get; set; }
        public int SourcesTPId { get; set; }
        public int SourcesTPIdFromCampaign { get; set; }
        public int? CampaignId { get; set; }
        public Nullable<int> SecCustomerId { get; set; }
        public int QuickDisposition { get; set; }
        public Nullable<int> DispositionId { get; set; }
        public Nullable<int> TypeId { get; set; }
        public bool IsDeleted { get; set; }
        public bool CanUpdate { get; set; }
        public bool CanDelete { get; set; }
        public bool CanUpdateStatus { get; set; }
        public string ReferringCustomerId { get; set; }
        public Nullable<int> CreatedByPersonId { get; set; }
        public Nullable<int> LastUpdatedByPersonId { get; set; }
        public string Notes { get; set; }
        public string SelectedOpportunityStatus { get; set; }
        public string selectedSalesAgent { get; set; }
        public string selectedInstaller { get; set; }
        public string selectedInstallationAddress { get; set; }
        public string selectedAccount { get; set; }
        public bool IsTaxExempt { get; set; }
        public string TaxExemptID { get; set; }
        public bool IsPSTExempt { get; set; }
        public bool IsGSTExempt { get; set; }
        public bool IsHSTExempt { get; set; }
        public bool IsVATExempt { get; set; }
        public bool IsNewConstruction { get; set; }
        public int? RelatedAccountId { get; set; }
        public Nullable<int> ImpersonatorPersonId { get; set; }
        public int? TerritoryDisplayId { get; set; }
        public string DisplayTerritoryName { get; set; }
        public bool SendMsg { get; set; }
        public bool UncheckNotifyviaText { get; set; }
        public DateTime? ReceivedDate { get; set; }

        //public virtual ICollection<LeadAccount> LeadAccount { get; set; }
        //public virtual ICollection<User> SalesAgent { get; set; }
        //public virtual ICollection<User> Installer { get; set; }
        //public virtual ICollection<JobQuote> Quotes { get; set; }
        //public virtual ICollection<Order> Orders { get; set; }
        //public ICollection<Note> Note { get; set; }
        //public Disposition Disposition { get; set; }
        //public virtual ICollection<AttachmentInfo> Attachments { get; set; }
        //public virtual ICollection<PhysicalFile> PhysicalFiles { get; set; }
        //public virtual ICollection<Calendar> Calendars { get; set; }
        //public virtual Franchise Franchise { get; set; }
        //public virtual Person PrimPerson { get; set; }
        //public virtual ICollection<Person> SecPerson { get; set; }
        //public virtual ICollection<Task> Tasks { get; set; }
        public virtual AccountTPViewModel AccountTP { get; set; }
        public virtual CustomerTPViewModel PrimCustomer { get; set; }
        public virtual ICollection<CustomerTPViewModel> SecCustomer { get; set; }
        public virtual Type_OpportunityStatusViewModel OpportunityStatus { get; set; }
        public virtual ICollection<AddressTPViewModel> Addresses { get; set; }
        public virtual ICollection<SourceList> SourcesList { get; set; }
        public virtual ICollection<OpportunitySourceViewModel> OpportunitySources { get; set; }
        public virtual ICollection<NotesAttachmentslist> NotesAttachmentslist { get; set; }
        public List<Type_QuestionViewModel> OpportunityQuestionAns { get; set; }
        //public virtual ICollection<EditHistory> EditHistories { get; set; }
        //public virtual ICollection<Job> Jobs { get; set; }
        //public virtual ICollection<OpportunityNote> OpportunityNotes { get; set; }
        //public virtual ICollection<Note> OpportunityNotesTP { get; set; }
        //public virtual ICollection<OpportunityAdditionalInfo> OpportunityAdditionalInfo { get; set; }
        //public virtual Territory Territory { get; set; }
        //public virtual ICollection<SentEmail> SentEmails { get; set; }
        //public virtual AddressTP AccountBillingAddressTP { get; set; }
        //public virtual AddressTP InstallationAddressTP { get; set; }


        public static Opportunity MapFrom(OpportunityViewModel vm)
        {
            Opportunity model = new Opportunity();
            model.OpportunityId = vm.OpportunityId;
            model.OpportunityName = vm.OpportunityName;
            model.CampaignName = vm.CampaignName;
            model.IsCommercial = vm.IsCommercial;
            model.SideMark = vm.SideMark;
            model.SalesAgentId = vm.SalesAgentId;
            model.InstallerId = vm.InstallerId;
            model.InstallationAddressId = vm.InstallationAddressId;
            model.BillingAddressId = vm.BillingAddressId;
            model.Description = vm.Description;
            model.LeadId = vm.LeadId;
            model.AccountId = vm.AccountId;
            model.SkipDuplicateCheck = vm.SkipDuplicateCheck;
            model.OpportunityGuid = vm.OpportunityGuid;
            model.OpportunityNumber = vm.OpportunityNumber;
            model.OpportunityStatusId = vm.OpportunityStatusId;
            model.FranchiseId = vm.FranchiseId;
            model.TerritoryId = vm.TerritoryId;
            model.TerritoryType = vm.TerritoryType;
            model.PersonId = vm.PersonId;
            model.SecPersonId = vm.SecPersonId;
            model.OpportunityStatusName = vm.OpportunityStatusName;
            model.InstallationAddress = vm.InstallationAddress;
            model.CustomerId = vm.CustomerId;
            model.OpportunitySourceId = vm.OpportunitySourceId;
            model.SourcesTPId = vm.SourcesTPId;
            model.SourcesTPIdFromCampaign = vm.SourcesTPIdFromCampaign;
            model.CampaignId = vm.CampaignId;
            model.SecCustomerId = vm.SecCustomerId;
            model.QuickDisposition = vm.QuickDisposition;
            model.DispositionId = vm.DispositionId;
            model.TypeId = vm.TypeId;
            model.IsDeleted = vm.IsDeleted;
            model.CanUpdate = vm.CanUpdate;
            model.CanDelete = vm.CanDelete;
            model.CanUpdateStatus = vm.CanUpdateStatus;
            model.ReferringCustomerId = vm.ReferringCustomerId;
            model.CreatedByPersonId = vm.CreatedByPersonId;
            model.LastUpdatedByPersonId = vm.LastUpdatedByPersonId;
            model.Notes = vm.Notes;
            model.SelectedOpportunityStatus = vm.SelectedOpportunityStatus;
            model.selectedSalesAgent = vm.selectedSalesAgent;
            model.selectedInstaller = vm.selectedInstaller;
            model.selectedInstallationAddress = vm.selectedInstallationAddress;
            model.selectedAccount = vm.selectedAccount;
            model.IsTaxExempt = vm.IsTaxExempt;
            model.TaxExemptID = vm.TaxExemptID;
            model.IsPSTExempt = vm.IsPSTExempt;
            model.IsGSTExempt = vm.IsGSTExempt;
            model.IsHSTExempt = vm.IsHSTExempt;
            model.IsVATExempt = vm.IsVATExempt;
            model.IsNewConstruction = vm.IsNewConstruction;
            model.RelatedAccountId = vm.RelatedAccountId;
            model.ImpersonatorPersonId = vm.ImpersonatorPersonId;
            model.TerritoryDisplayId = vm.TerritoryDisplayId;
            model.DisplayTerritoryName = vm.DisplayTerritoryName;
            model.SendMsg = vm.SendMsg;
            model.UncheckNotifyviaText = vm.UncheckNotifyviaText;
            model.ReceivedDate = vm.ReceivedDate;

            if (vm.AccountTP != null)
                model.AccountTP = AccountTPViewModel.MapFrom(vm.AccountTP);
            if (vm.PrimCustomer != null)
                model.PrimCustomer = CustomerTPViewModel.MapFrom(vm.PrimCustomer);

            List<CustomerTP> lstseccus = new List<CustomerTP>();
            if (vm.SecCustomer != null)
            {
                foreach (var cus in vm.SecCustomer)
                {
                    lstseccus.Add(CustomerTPViewModel.MapFrom(cus));
                }
            }
            model.SecCustomer = lstseccus;

            List<AddressTP> lstadd = new List<AddressTP>();
            if (vm.Addresses != null)
            {
                foreach (var add in vm.Addresses)
                {
                    lstadd.Add(AddressTPViewModel.MapFrom(add));
                }
            }
            model.Addresses = lstadd;

            if (vm.OpportunityStatus != null)
                model.OpportunityStatus = Type_OpportunityStatusViewModel.MapFrom(vm.OpportunityStatus);

            List<OpportunitySource> lstOpportunitySource = new List<OpportunitySource>();
            if (vm.OpportunitySources != null && vm.OpportunitySources.Count > 0)
            {
                foreach (var sourcevm in vm.OpportunitySources)
                {
                    OpportunitySource Source = OpportunitySourceViewModel.MapFrom(sourcevm);
                    lstOpportunitySource.Add(Source);
                }
            }
            model.OpportunitySources = lstOpportunitySource;

            if (vm.OpportunityQuestionAns != null && vm.OpportunityQuestionAns.Count > 0)
                model.OpportunityQuestionAns = Type_QuestionViewModel.MapFrom(vm.OpportunityQuestionAns);

            model.SourcesList = vm.SourcesList;
            model.NotesAttachmentslist = vm.NotesAttachmentslist;


            return model;
        }
    }

    public class OpportunitySourceViewModel
    {
        public int OpportunitySourceId { get; set; }
        public int Value { get; set; }
        public int SourceId { get; set; }
        public int OpportunityId { get; set; }
        public bool IsManuallyAdded { get; set; }
        public bool IsPrimarySource { get; set; }
        public static OpportunitySource MapFrom(OpportunitySourceViewModel vm)
        {
            OpportunitySource Source = new OpportunitySource();
            Source.OpportunitySourceId = vm.OpportunitySourceId;
            Source.Value = vm.Value;
            Source.SourceId = vm.SourceId;
            Source.OpportunityId = vm.OpportunityId;
            Source.IsManuallyAdded = vm.IsManuallyAdded;
            Source.IsPrimarySource = vm.IsPrimarySource;
            return Source;
        }
    }

    public class Type_OpportunityStatusViewModel
    {
        public short OpportunityStatusId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<int> BrandId { get; set; }
        public Nullable<int> FranchiseId { get; set; }
        public Nullable<int> DisplayOrder { get; set; }
        public string ClassName { get; set; }
        public Nullable<bool> isDeleted { get; set; }
        public static Type_OpportunityStatus MapFrom(Type_OpportunityStatusViewModel vm)
        {
            Type_OpportunityStatus Source = new Type_OpportunityStatus();
            Source.OpportunityStatusId = vm.OpportunityStatusId;
            Source.Name = vm.Name;
            Source.Description = vm.Description;
            Source.BrandId = vm.BrandId;
            Source.FranchiseId = vm.FranchiseId;
            Source.DisplayOrder = vm.DisplayOrder;
            Source.ClassName = vm.ClassName;
            Source.isDeleted = vm.isDeleted;
            return Source;
        }
    }

}