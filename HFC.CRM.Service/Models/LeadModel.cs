﻿using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HFC.CRM.Service.Models
{

    public class LeadViewModel
    {
        public int LeadId { get; set; }
        public System.Guid LeadGuid { get; set; }
        public int LeadNumber { get; set; }
        public short LeadStatusId { get; set; }
        public Nullable<int> FranchiseId { get; set; }
        public Nullable<int> RelatedAccountId { get; set; }
        public string RelatedAccountName { get; set; }
        public string ReferralTypeName { get; set; }
        public Nullable<int> ReferralTypeId { get; set; }
        public string AccountName { get; set; }
        public string LeadType { get; set; }
        public string LeadTerritory { get; set; }
        public Nullable<int> TerritoryId { get; set; }
        public string TerritoryType { get; set; }
        public Nullable<int> SecPersonId { get; set; }
        public Nullable<int> PersonId { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public string leadSourceId { get; set; }
        public int SourcesTPId { get; set; }
        public int SourcesTPIdFromCampaign { get; set; }
        public Nullable<bool> AllowFranchisetoReassign { get; set; }
        public int? CampaignId { get; set; }
        public Nullable<int> SecCustomerId { get; set; }
        public int QuickDisposition { get; set; }
        public Nullable<int> DispositionId { get; set; }
        public bool IsDeleted { get; set; }
        public string ReferringCustomerId { get; set; }
        public string Notes { get; set; }
        public virtual CustomerTPViewModel PrimCustomer { get; set; }
        public virtual ICollection<CustomerTPViewModel> SecCustomer { get; set; }
        public virtual ICollection<AddressTPViewModel> Addresses { get; set; }
        //public virtual ICollection<SourceListViewModel> SourcesList { get; set; }
        public virtual ICollection<LeadSourceViewModel> LeadSources { get; set; }
        //public List<Type_QuestionViewModel> LeadQuestionAns { get; set; }
        public string KeyAcct { get; set; }
        public bool LegacyAccount { get; set; }
        public bool IsCommercial { get; set; }
        public int? CommercialTypeId { get; set; }
        public string CommercialValue { get; set; }
        public int? RelationshipTypeId { get; set; }
        public string RelationShipTypeValue { get; set; }
        public int? IndustryId { get; set; }
        public string IndustryValue { get; set; }
        public string OtherIndustry { get; set; }
        public bool IsTaxExempt { get; set; }
        public string TaxExemptID { get; set; }
        public string HFCUTMCampaignCode { get; set; }
        public int? LMDBSourceID { get; set; }
        public int? SourcesHFCId { get; set; }
        public Nullable<int> ImpersonatorPersonId { get; set; }
        public bool? EmailSent { get; set; }
        public int? BrandId { get; set; }
        public int? TerritoryDisplayId { get; set; }
        public string DisplayTerritoryName { get; set; }
        public bool IsNotifyemails { get; set; }
        public bool IsNotifyText { get; set; }
        public bool SendText { get; set; }

        public static Lead MapFrom(LeadViewModel vm)
        {
            Lead model = new Lead();
            model.LeadId = vm.LeadId;
            model.LeadGuid = vm.LeadGuid;
            model.LeadNumber = vm.LeadNumber;
            model.LeadStatusId = vm.LeadStatusId;
            model.FranchiseId = vm.FranchiseId;
            model.RelatedAccountId = vm.RelatedAccountId;
            model.RelatedAccountName = vm.RelatedAccountName;
            model.ReferralTypeName = vm.ReferralTypeName;
            model.ReferralTypeId = vm.ReferralTypeId;
            model.AccountName = vm.AccountName;
            model.LeadType = vm.LeadType;
            model.LeadTerritory = vm.LeadTerritory;
            model.TerritoryId = vm.TerritoryId;
            model.TerritoryType = vm.TerritoryType;
            model.SecPersonId = vm.SecPersonId;
            model.PersonId = vm.PersonId;
            model.CustomerId = vm.CustomerId;
            model.leadSourceId = vm.leadSourceId;
            model.SourcesTPId = vm.SourcesTPId;
            model.SourcesTPIdFromCampaign = vm.SourcesTPIdFromCampaign;
            model.AllowFranchisetoReassign = vm.AllowFranchisetoReassign;
            model.CampaignId = vm.CampaignId;
            model.SecCustomerId = vm.SecCustomerId;
            model.QuickDisposition = vm.QuickDisposition;
            model.DispositionId = vm.DispositionId;
            model.IsDeleted = vm.IsDeleted;
            model.ReferringCustomerId = vm.ReferringCustomerId;
            model.Notes = vm.Notes;
            model.KeyAcct = vm.KeyAcct;
            model.LegacyAccount = vm.LegacyAccount;
            model.IsCommercial = vm.IsCommercial;
            model.CommercialTypeId = vm.CommercialTypeId;
            model.CommercialValue = vm.CommercialValue;
            model.RelationshipTypeId = vm.RelationshipTypeId;
            model.RelationShipTypeValue = vm.RelationShipTypeValue;
            model.IndustryId = vm.IndustryId;
            model.IndustryValue = vm.IndustryValue;
            model.OtherIndustry = vm.OtherIndustry;
            model.IsTaxExempt = vm.IsTaxExempt;
            model.TaxExemptID = vm.TaxExemptID;
            model.HFCUTMCampaignCode = vm.HFCUTMCampaignCode;
            model.LMDBSourceID = vm.LMDBSourceID;
            model.SourcesHFCId = vm.SourcesHFCId;
            model.ImpersonatorPersonId = vm.ImpersonatorPersonId;
            model.EmailSent = vm.EmailSent;
            model.BrandId = vm.BrandId;
            model.TerritoryDisplayId = vm.TerritoryDisplayId;
            model.DisplayTerritoryName = vm.DisplayTerritoryName;
            model.IsNotifyemails = vm.IsNotifyemails;
            model.IsNotifyText = vm.IsNotifyText;
            model.SendText = vm.SendText;

            if (vm.PrimCustomer != null)
                model.PrimCustomer = CustomerTPViewModel.MapFrom(vm.PrimCustomer);

            List<CustomerTP> SecCustomer = new List<CustomerTP>();
            if (vm.SecCustomer != null && vm.SecCustomer.Count > 0)
            {
                foreach (var Cusvm in vm.SecCustomer)
                {
                    CustomerTP seccus = CustomerTPViewModel.MapFrom(Cusvm);
                    SecCustomer.Add(seccus);
                }
            }
            model.SecCustomer = SecCustomer;

            List<AddressTP> lstaddress = new List<AddressTP>();
            if (vm.Addresses != null && vm.Addresses.Count > 0)
            {
                foreach (var addvm in vm.Addresses)
                {
                    AddressTP address = AddressTPViewModel.MapFrom(addvm);
                    lstaddress.Add(address);
                }
            }
            model.Addresses = lstaddress;

            List<LeadSource> lstLeadSource = new List<LeadSource>();
            if (vm.LeadSources != null && vm.LeadSources.Count > 0)
            {
                foreach (var sourcevm in vm.LeadSources)
                {
                    LeadSource Source = LeadSourceViewModel.MapFrom(sourcevm);
                    lstLeadSource.Add(Source);
                }
            }
            model.LeadSources = lstLeadSource;

            return model;

        }
    }



    public class CustomerTPViewModel
    {
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MI { get; set; }
        public string HomePhone { get; set; }
        public string CellPhone { get; set; }
        public string CompanyName { get; set; }
        public string WorkTitle { get; set; }
        public string WorkPhone { get; set; }
        public string WorkPhoneExt { get; set; }
        public string FaxPhone { get; set; }
        public string PrimaryEmail { get; set; }
        public string SecondaryEmail { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string PreferredTFN { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public Nullable<bool> IsAccount { get; set; }
        public string Notes { get; set; }
        public bool IsReceiveEmails { get; set; }
        public Nullable<System.Guid> CustomerGuid { get; set; }
        public Nullable<int> FranchiseId { get; set; }
        public int? LeadId { get; set; }
        public int? AccountId { get; set; }
        public int? OpportunityId { get; set; }
        public bool IsPrimaryCustomer { get; set; }
        public string TextOptin { get; set; }
        public static CustomerTP MapFrom(CustomerTPViewModel vm)
        {
            CustomerTP Customer = new CustomerTP();
            Customer.CustomerId = vm.CustomerId;
            Customer.FirstName = vm.FirstName;
            Customer.LastName = vm.LastName;
            Customer.MI = vm.MI;
            Customer.HomePhone = vm.HomePhone;
            Customer.CellPhone = vm.CellPhone;
            Customer.CompanyName = vm.CompanyName;
            Customer.WorkTitle = vm.WorkTitle;
            Customer.WorkPhone = vm.WorkPhone;
            Customer.WorkPhoneExt = vm.WorkPhoneExt;
            Customer.FaxPhone = vm.FaxPhone;
            Customer.PrimaryEmail = vm.PrimaryEmail;
            Customer.SecondaryEmail = vm.SecondaryEmail;
            Customer.IsDeleted = vm.IsDeleted;
            Customer.CreatedOn = vm.CreatedOn;
            Customer.PreferredTFN = vm.PreferredTFN;
            Customer.CreatedBy = vm.CreatedBy;
            Customer.UpdatedBy = vm.UpdatedBy;
            Customer.UpdatedOn = vm.UpdatedOn;
            Customer.IsAccount = vm.IsAccount;
            Customer.Notes = vm.Notes;
            Customer.IsReceiveEmails = vm.IsReceiveEmails;
            Customer.CustomerGuid = vm.CustomerGuid;
            Customer.FranchiseId = vm.FranchiseId;
            Customer.LeadId = vm.LeadId;
            Customer.AccountId = vm.AccountId;
            Customer.OpportunityId = vm.OpportunityId;
            Customer.IsPrimaryCustomer = vm.IsPrimaryCustomer;
            Customer.TextOptin = vm.TextOptin;
            return Customer;
        }
    }

    public class AddressTPViewModel
    {
        public int AddressId { get; set; }
        public string AttentionText { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string CountryCode2Digits { get; set; }
        public int CreatedBy { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsResidential { get; set; }
        public string CrossStreet { get; set; }
        public bool IsValidated { get; set; }
        public bool IsBusiness { get; set; }
        public string Location { get; set; }
        public System.Guid AddressesGuid { get; set; }
        public Nullable<int> LeadId { get; set; }
        public Nullable<int> AccountId { get; set; }
        public Nullable<int> OpportunityId { get; set; }
        public string AddressType { get; set; }
        public int ContactId { get; set; }
        public string ContactName { get; set; }
        public string FullAddress { get; set; }
        public bool IsInstallationAddress { get; set; }
        public CustomerSource AssociatedSource { get; set; }
        public static AddressTP MapFrom(AddressTPViewModel vm)
        {
            AddressTP address = new AddressTP();
            address.AddressId = vm.AddressId;
            address.AttentionText = vm.AttentionText;
            address.Address1 = vm.Address1;
            address.Address2 = vm.Address2;
            address.City = vm.City;
            address.State = vm.State;
            address.ZipCode = vm.ZipCode;
            address.CountryCode2Digits = vm.CountryCode2Digits;
            address.CreatedBy = vm.CreatedBy;
            address.IsDeleted = vm.IsDeleted;
            address.IsResidential = vm.IsResidential;
            address.CrossStreet = vm.CrossStreet;
            address.IsValidated = vm.IsValidated;
            address.IsBusiness = vm.IsBusiness;
            address.Location = vm.Location;
            address.AddressesGuid = vm.AddressesGuid;
            address.LeadId = vm.LeadId;
            address.AccountId = vm.AccountId;
            address.OpportunityId = vm.OpportunityId;
            address.AddressType = vm.AddressType;
            address.ContactId = vm.ContactId;
            address.ContactName = vm.ContactName;
            address.FullAddress = vm.FullAddress;
            address.IsInstallationAddress = vm.IsInstallationAddress;
            address.AssociatedSource = vm.AssociatedSource;
            return address;
        }
    }

    public class LeadSourceViewModel
    {
        public int LeadSourceId { get; set; }
        public int Value { get; set; }
        public int SourceId { get; set; }
        public int LeadId { get; set; }
        public bool IsManuallyAdded { get; set; }
        public bool IsPrimarySource { get; set; }
        public static LeadSource MapFrom(LeadSourceViewModel vm)
        {
            LeadSource Source = new LeadSource();
            Source.LeadSourceId = vm.LeadSourceId;
            Source.Value = vm.Value;
            Source.SourceId = vm.SourceId;
            Source.LeadId = vm.LeadId;
            Source.IsManuallyAdded = vm.IsManuallyAdded;
            Source.IsPrimarySource = vm.IsPrimarySource;
            return Source;
        }
    }

    public class Type_QuestionViewModel
    {
        public int QuestionId { get; set; }
        public string Question { get; set; }
        public string SubQuestion { get; set; }
        public string SelectionType { get; set; }
        public int DisplayOrder { get; set; }
        public Nullable<int> FranchiseId { get; set; }
        public Nullable<int> BrandId { get; set; }
        public Nullable<int> AnswerId { get; set; }
        public string Answer { get; set; }
        public string Answers { get; set; }
        public string PastDate { get; set; }
        public bool IsPrimaryQuestion { get; set; }

        public static List<Type_Question> MapFrom(List<Type_QuestionViewModel> vm)
        {
            List<Type_Question> lstQuestion = new List<Type_Question>();
            if (vm != null && vm.Count > 0)
            {
                foreach (var qvm in vm)
                {
                    Type_Question ques = new Type_Question();
                    ques.QuestionId = qvm.QuestionId;
                    ques.Question = qvm.Question;
                    ques.SubQuestion = qvm.SubQuestion;
                    ques.SelectionType = qvm.SelectionType;
                    ques.DisplayOrder = qvm.DisplayOrder;
                    ques.FranchiseId = qvm.FranchiseId;
                    ques.BrandId = qvm.BrandId;
                    ques.AnswerId = qvm.AnswerId;
                    ques.Answer = qvm.Answer;
                    ques.Answers = qvm.Answers;
                    ques.PastDate = qvm.PastDate;
                    ques.IsPrimaryQuestion = qvm.IsPrimaryQuestion;
                    lstQuestion.Add(ques);
                }
            }
            return lstQuestion;
        }
    }

    public class ConvertLead
    {
        public int leadId { get; set; }
        public int selectedaccountid { get; set; }
        public int addnewaccount { get; set; }
        public int addopportunity { get; set; }
        public string opportunityName { get; set; }
        public int SalesAgentId { get; set; }
    }
}