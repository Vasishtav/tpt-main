﻿using HFC.CRM.Data;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HFC.CRM.Service.Models
{
    public class TaskViewModel
    {
        public int TaskID { get; set; }
        public DateTime? CompletedDateUtc { get; set; }
        public int? OrganizerPersonId { get; set; }
        public int PersonId { get; set; }
        public int AccountId { get; set; }
        public int OpportunityId { get; set; }
        public int OrderId { get; set; }
        public int? LeadId { get; set; }

        public string Title { get; set; }
        public string Attendee { get; set; }
        public string Description { get; set; }
        public int eventType { get; set; }
        public EventTypeEnum EventType { get; set; }
        public string AppointmentTypeName { get; set; }
        public string UserRoleColor { get; set; }
        public string UserTextColor { get; set; }
        public int TimeSlot { get; set; }
        public string AppointmentTypeColor { get; set; }
        public Nullable<bool> IsPrivate { get; set; }
        public Nullable<bool> IsTask { get; set; }
        public string OwnerName { get; set; }
        public string AttendeeName { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime Start { get; set; }

        public string StartTimezone { get; set; }
        public DateTime End { get; set; }
        public bool? IsPrivateAccess { get; set; }

        public string EndTimezone { get; set; }

        public string RecurrenceRule { get; set; }
        public int? RecurrenceID { get; set; }
        public string RecurrenceException { get; set; }
        public bool IsAllDay { get; set; }

        public virtual ICollection<EventToPerson> OwnerID1 { get; set; }
        public List<string> OwnerID2 { get; set; }
        public List<SalesAgents> OwnerID { get; set; }
        public int? ReminderId { get; set; }
        public string Location { get; set; }
        public Opportunity opportunity { get; set; }
        public string OpportunityName { get; set; }
        public string AccountName { get; set; }
        public string AccountPhone { get; set; }
        public string CalName { get; set; }
    }
}