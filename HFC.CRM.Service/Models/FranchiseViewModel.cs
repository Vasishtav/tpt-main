﻿using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HFC.CRM.Service.Models
{
    public class FranchiseViewModel
    {
        public int FranchiseId { get; set; }
        public Guid FranchiseGuid { get; set; }
        public int BrandId { get; set; }
        public string Code { get; set; }       
        public string Name { get; set; }
        public string DBAName { get; set; }
        public TimeZoneEnum TimezoneCode { get; set; }        
        public string DMA { get; set; }
        public string Region { get; set; }
        public string Website { get; set; }
        public string CoachName { get; set; }
        public DateTime? CreatedOnUtc { get; set; }
        public DateTime? TPTGoLiveDate { get; set; }        
        public int OwnerId { get; set; }
        public string OwnerEmail { get; set; }
        public string LocalPhoneNumber { get; set; }
        public string AdminName { get; set; }
        public string AdminEmail { get; set; }
        public string BusinessPhone { get; set; }
        public string Currency { get; set; }       
        public bool? IsSuspended { get; set; }
        public bool EnableTexting { get; set; }
        public bool EnableCase { get; set; }
        public bool EnableVendorManagement { get; set; }
        public int QuoteExpiryDays { get; set; }
        public int BuyerremorseDay { get; set; }
        public List<OwnerFVM> lstOwnerName { get; set; }
        public AddressVM Address { get; set; }

        public static Franchise MapFrom(FranchiseViewModel vm)
        {
            Franchise model = new Franchise();
            model.FranchiseId = vm.FranchiseId;
            model.FranchiseGuid = vm.FranchiseGuid;
            model.BrandId = vm.BrandId;
            model.Code = vm.Code;
            model.Name = vm.Name;
            model.DBAName = vm.DBAName;
            model.TimezoneCode = vm.TimezoneCode;
            model.DMA = vm.DMA;
            model.Region = vm.Region;
            model.Website = vm.Website;
            model.CoachName = vm.CoachName;
            if (vm.CreatedOnUtc != null)
                model.CreatedOnUtc = (DateTime)vm.CreatedOnUtc;
            model.TPTGoLiveDate = vm.TPTGoLiveDate;
            model.OwnerId = vm.OwnerId;
            model.OwnerEmail = vm.OwnerEmail;
            model.LocalPhoneNumber = vm.LocalPhoneNumber;
            model.AdminName = vm.AdminName;
            model.AdminEmail = vm.AdminEmail;
            model.BusinessPhone = vm.BusinessPhone;
            model.Currency = vm.Currency;
            model.IsSuspended = vm.IsSuspended;
            model.EnableTexting = vm.EnableTexting;
            model.EnableCase = vm.EnableCase;
            model.EnableVendorManagement = vm.EnableVendorManagement;
            model.QuoteExpiryDays = vm.QuoteExpiryDays;
            model.BuyerremorseDay = vm.BuyerremorseDay;

            if (vm.lstOwnerName != null && vm.lstOwnerName.Count > 0)
            {
                List<OwnerVM> lstovm = new List<OwnerVM>();
                foreach (var ownname in vm.lstOwnerName)
                {
                    OwnerVM ovm = new OwnerVM();
                    ovm.OwnerNameId = ownname.OwnerNameId;
                    ovm.FirstName = ownname.FirstName;
                    ovm.LastName = ownname.LastName;
                    lstovm.Add(ovm);
                }
                model.lstOwnerName = lstovm;
            }
            if (vm.Address != null)
            {
                Address add = new Address();
                add.AddressId = vm.Address.AddressId;
                add.Address1 = vm.Address.Address1;
                add.Address2 = vm.Address.Address2;
                add.City = vm.Address.City;
                add.State = vm.Address.State;
                add.CountryCode2Digits = vm.Address.CountryCode2Digits;
                add.ZipCode = vm.Address.ZipCode;
                model.Address = add;
                model.AddressId = vm.Address.AddressId;
            }
            return model;
        }
    }
    public class AddressVM
    {
        public int AddressId { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string CountryCode2Digits { get; set; }
    }

    public class OwnerFVM
    {
        public int OwnerNameId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}