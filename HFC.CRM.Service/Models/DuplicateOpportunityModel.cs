﻿namespace HFC.CRM.Service.Models
{
    using System.Collections.Generic;

    public class DuplicateOpportunityModel
    {
        public int OpportunityId { get; set; }
        public string Address { get; set; }
        public string HomePhone { get; set; }
        public string CellPhone { get; set; }
        public string FaxPhone { get; set; }
        public string WorkPhone { get; set; }
        public string PrimaryEmail { get; set; }
        public string SecondaryEmail { get; set; }
        public string FullName { get; set; }
        public int OpportunityNumber { get; set; }
    }
}