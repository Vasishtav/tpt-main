﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HFC.CRM.Service.Models
{
    public class globaldoc
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public int Category { get; set; }
        public int Concept { get; set; }
        public string streamId { get; set; }
        public bool IsEnabled { get; set; }
        public string CreatedOn { get; set; }
        public string UpdatedOn { get; set; }
        public string CategoryTitle { get; set; }
        public string ConceptTitle { get; set; }
    }
}