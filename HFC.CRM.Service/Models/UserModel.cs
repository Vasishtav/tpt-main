﻿using HFC.CRM.Data;
using System;
using System.Collections.Generic;

namespace HFC.CRM.Service.Models
{
    public class UserModel
    {
        public Guid UserId { get; set; }
        public int PersonId { get; set; }
        public int? AddressId { get; set; }
        public string UserName { get; set; }
        public DateTime CreationDateUtc { get; set; }
        public DateTime? LastLoginDateUtc { get; set; }
        public List<Guid> RoleIds { get; set; }
        public List<string> PermissionSetIds { get; set; }
        public bool IsApproved { get; set; }
        public bool IsLockedOut { get; set; }
        public bool IsDisabled { get; set; }
        public bool IsADUser { get; set; }
        public bool? CalSync { get; set; }
        public bool IsUserProfile { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public DateTime? SyncStartsOnUtc { get; set; }
        public int CalendarFirstHour { get; set; }
        public string Domain { get; set; }
        public string DomainPath { get; set; }
        public string Comment { get; set; }
        /// <summary>
        /// Background Color
        /// </summary>
        public string BGColor { get; set; }
        /// <summary>
        /// Foreground Color
        /// </summary>
        public string FGColor { get; set; }
        public PersonViewModel Person { get; set; }
        public Address Address { get; set; }
    }

    public class PersonViewModel
    {
        public int PersonId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MI { get; set; }
        public string HomePhone { get; set; }
        public string CellPhone { get; set; }
        public string CompanyName { get; set; }
        public string WorkTitle { get; set; }
        public string WorkPhone { get; set; }
        public string WorkPhoneExt { get; set; }
        public string PrimaryEmail { get; set; }
        public string SecondaryEmail { get; set; }
        public string Notes { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsReceiveEmails { get; set; }
        public HFC.CRM.Data.TimeZoneEnum TimezoneCode { get; set; }
        public string FaxPhone { get; set; }
        private Nullable<System.DateTime> _UnsubscribedOnUtc;
        public string PreferredTFN { get; set; }
        public System.Guid PersonGuid { get; set; }
        public Nullable<int> FranchiseID { get; set; }
        public static Person MapFrom(PersonViewModel vm)
        {
            Person p = new Person();
            p.PersonId = vm.PersonId;
            p.FirstName = vm.FirstName;
            p.LastName = vm.LastName;
            p.MI = vm.MI;
            p.HomePhone = vm.HomePhone;
            p.CellPhone = vm.CellPhone;
            p.CompanyName = vm.CompanyName;
            p.WorkTitle = vm.WorkTitle;
            p.WorkPhone = vm.WorkPhone;
            p.WorkPhoneExt = vm.WorkPhoneExt;
            p.PrimaryEmail = vm.PrimaryEmail;
            p.SecondaryEmail = vm.SecondaryEmail;
            p.Notes = vm.Notes;
            p.IsDeleted = vm.IsDeleted;
            p.IsReceiveEmails = vm.IsReceiveEmails;
            p.TimezoneCode = vm.TimezoneCode;
            p.FaxPhone = vm.FaxPhone;
            p.PreferredTFN = vm.PreferredTFN;
            p.PersonGuid = vm.PersonGuid;
            p.FranchiseID = vm.FranchiseID;
            return p;
        }
    }

    public class UserViewModel
    {
        public bool CalSync { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string HomePhone { get; set; }
        public string CellPhone { get; set; }
        public string PreferredTFN { get; set; }
        public string Zipcode { get; set; }
        public string CountryCode2Digits { get; set; }
        public int Calendar1sthr { get; set; }
        public string Comments { get; set; }
        public string BGColor { get; set; }
        public string FGColor { get; set; }
        public List<Guid> RoleIds { get; set; }
        public List<string> PermissionSetIds { get; set; }
        public HFC.CRM.Data.TimeZoneEnum TimezoneCode { get; set; }

    }

}