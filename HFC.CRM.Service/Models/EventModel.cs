﻿using HFC.CRM.Data;
using HFC.CRM.Data.Constants;
using HFC.CRM.DTO.Calandar;
using HFC.CRM.Managers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HFC.CRM.Service
{

    public class EventListTPConnect
    {
        public List<EventListDataTPConnect> EventList { get; set; }
    }

    /// <summary>
    /// Return List of event
    /// </summary>
    public class EventListDataTPConnect
    {
        public int CalendarId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public object Account { get; set; }
        public string Location { get; set; }
        public string Subject { get; set; }
        public object AppointmentType { get; set; }
        public bool IsAllDay { get; set; }
    }

    public class EventDetailTPConnect
    {
        public EventDetailsTPConnect EventDetails { get; set; }
    }

    /// <summary>
    /// View Event Details
    /// </summary>
    public class EventDetailsTPConnect
    {
        public int CalendarId { get; set; }
        public string Subject { get; set; }
        public string Location { get; set; }
        public string CloseType { get; set; }

        public object Lead { get; set; }
        public object Account { get; set; }
        public object Opportunity { get; set; }
        public object Order { get; set; }

        //public string Lead { get; set; }
        //public string Account { get; set; }
        //public string Opportunity { get; set; }
        //public string Order { get; set; }

        //public Nullable<int> LeadId { get; set; }
        //public Nullable<int> AccountId { get; set; }
        //public Nullable<int> OpportunityId { get; set; }
        //public Nullable<int> OrderId { get; set; }
        //public List<int> AttendeesId { get; set; }
        //public int AppointmentTypeId { get; set; }

        public object AppointmentType { get; set; }
        public object Attendees { get; set; }
        public string Message { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Reminder { get; set; }
        public Nullable<bool> IsPrivate { get; set; }
        public bool? IsRecurring { get; set; }
        public DateTime CreatedByDate { get; set; }
        public DateTime ModifiedByDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public RecurringTPConnect RecurringDetails { get; set; }
        public bool? IsAllDay { get; set; }

    }

    public class UpdateEventTPConnect
    {
        public EventTPConnect EditEventDetails { get; set; }
    }

    #region Add / Update Event
    public class EventTPConnect
    {
        public int CalendarId { get; set; }
        public string Subject { get; set; }
        public string Location { get; set; }
        public Nullable<int> LeadId { get; set; }
        public Nullable<int> AccountId { get; set; }
        public Nullable<int> OpportunityId { get; set; }
        public Nullable<int> OrderId { get; set; }
        public AppointmentTypeEnumTP AppointmentType { get; set; }
        public List<int> Attendees { get; set; }
        public string Message { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime EndDate { get; set; }
        public bool AllDay { get; set; }
        public int Reminder { get; set; }
        public Nullable<bool> IsRecurring { get; set; }
        public int? RecurringEventId { get; set; }
        public RecurringTPConnect Recurring { get; set; }
        public Nullable<bool> IsPrivate { get; set; }

    }
    public class RecurringTPConnect
    {
        public int RecurringEventId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime? EndsOn { get; set; }
        public int? EndsAfterXOccurrences { get; set; }
        public int RecursEvery { get; set; }
        public Nullable<DayOfWeekEnum> DayOfWeekEnum { get; set; }
        public Nullable<MonthEnum> MonthEnum { get; set; }
        public Nullable<int> DayOfMonth { get; set; }
        public RecurringPatternEnum PatternEnum { get; set; }
        public Nullable<DayOfWeekIndexEnum> DayOfWeekIndex { get; set; }
    }
    #endregion

}