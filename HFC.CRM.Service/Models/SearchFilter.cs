﻿using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HFC.CRM.Service.Models
{
    public class SearchFilter
    {
        public List<int> leadStatusIds { get; set; }
        public List<int> jobStatusIds { get; set; }
        public List<int> opportunityStatusIds { get; set; }
        
        public List<int> salesPersonIds { get; set; }
        public List<int> sourceIds { get; set; }
        public DateTime? createdOnUtcStart { get; set; }
        public DateTime? createdOnUtcEnd { get; set; }
        public SearchFilterEnum searchFilter { get; set; }
        public string searchTerm { get; set; }
        public string orderBy { get; set; }
        public OrderByEnum orderByDirection { get; set; }
        public int pageIndex { get; set; }
        public int pageSize { get; set; }
        public int leadId { get; set; }
        public byte iscommercial { get; set; }
        public List<int> leadIds { get; set; }
        public bool includeAddlLookup { get; set; }

        public List<int> invoiceStatuses { get; set; }
        public CommercialTypes commercialType { get; set; }
        public int isReportSearch { get; set; }
        public int AccountId { get; set; }
    }

    public class SalesFilter
    {
        public string ReportType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int additionalFilterId { get; set; }
    }
    public class OpportunityDuplicateFilter
    {
        public string SearchTerm { get; set; }
        public SearchDuplicateOpportunityEnum SearchType { get; set; }
    }

    public class LeadDuplicateFilter 
    {
        public string SearchTerm { get; set; }
        public SearchDuplicateLeadEnum SearchType { get; set; }
    }

    public class FranchiseSearchFilter
    {
        public int BrandId { get; set; }
        public AdminFilterEnum searchFilter { get; set; }
        public string searchTerm { get; set; }
        public bool ShowInActive { get; set; }
        public int searchBrand { get; set; }
        public int pageIndex { get; set; }
        public int pageSize { get; set; }
    }

}