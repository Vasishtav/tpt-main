﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HFC.CRM.Service.Models
{
    public class OpportunityStatusModel
    {
        public short StatusId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class QuoteCreateResponse
    {
        public int quoteid { get; set; }
    }
}