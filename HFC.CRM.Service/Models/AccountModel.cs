﻿using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HFC.CRM.Service.Models
{
    public class AccountTPViewModel
    {
        public int AccountId { get; set; }
        public string FullName { get; set; }
        public System.Guid AccountGuid { get; set; }
        public int AccountNumber { get; set; }
        public int AccountStatusId { get; set; }
        public int LeadId { get; set; }
        public Nullable<int> AccountTypeId { get; set; }
        public Nullable<int> FranchiseId { get; set; }
        public Nullable<int> TerritoryId { get; set; }
        public string TerritoryType { get; set; }
        public Nullable<int> PersonId { get; set; }
        public Nullable<int> SecPersonId { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreatedOnUtc { get; set; }
        public string ReferringCustomerId { get; set; }
        public Nullable<int> CreatedByPersonId { get; set; }
        public Nullable<System.DateTime> LastUpdatedOnUtc { get; set; }
        public Nullable<int> LastUpdatedByPersonId { get; set; }
        public string Notes { get; set; }
        public Nullable<int> DispositionId { get; set; }
        public Nullable<int> SalesStepId { get; set; }
        public Nullable<int> TempCustomerId { get; set; }
        public Nullable<int> CampaignId { get; set; }
        public Nullable<bool> IsPerpetual { get; set; }
        public Nullable<int> RelatedAccountId { get; set; }
        public Nullable<int> ReferralTypeId { get; set; }
        public string LeadType { get; set; }
        public string AccountName { get; set; }
        public string ReferralTypeName { get; set; }
        public string RelatedAccountName { get; set; }
        public Nullable<bool> AllowFranchisetoReassign { get; set; }
        //public virtual ICollection<AccountAddress> AccountAddresses { get; set; }
        //public virtual ICollection<AccountCampaign> AccountCampaigns { get; set; }
        //public virtual Type_AccountStatus Type_AccountStatus { get; set; }
        //public virtual ICollection<AccountSecondaryPerson> AccountSecondaryPersons { get; set; }
        public virtual ICollection<AccountSourceViewModel> AccountSources { get; set; }
        public int SourcesTPIdFromCampaign { get; set; }
        public int SourcesTPId { get; set; }
        public string CampaignName { get; set; }
        public virtual CustomerTPViewModel PrimCustomer { get; set; }
        public virtual ICollection<AddressTPViewModel> Addresses { get; set; }
        public string AccountSourceId { get; set; }
        //public ICollection<Note> AccountNotes { get; set; }
        public virtual ICollection<CustomerTPViewModel> SecCustomer { get; set; }
        //public virtual Territory Territory { get; set; }
        public Nullable<int> CustomerId { get; set; }
        //public virtual ICollection<NotesAttachmentslist> NotesAttachmentslist { get; set; }
        //public virtual ICollection<SourceList> SourcesList { get; set; }
        //public virtual ICollection<Opportunity> Opportunities { get; set; }
        //public Disposition Disposition { get; set; }
        //public virtual ICollection<EditHistory> EditHistories { get; set; }
        public bool SkipDuplicateCheck { get; set; }
        public int OpportunityId { get; set; }
        //public List<Type_Question> AccountQuestionAns { get; set; }
        public bool IsTaxExempt { get; set; }
        public string TaxExemptID { get; set; }
        public bool IsPSTExempt { get; set; }
        public bool IsGSTExempt { get; set; }
        public bool IsHSTExempt { get; set; }
        public bool IsVATExempt { get; set; }
        public bool AllOpportunityTaxExempt { get; set; }
        public string AccountType { get; set; }
        public string KeyAcct { get; set; }
        public bool LegacyAccount { get; set; }
        public bool IsCommercial { get; set; }
        public int? CommercialTypeId { get; set; }
        public string CommercialValue { get; set; }
        public int? RelationshipTypeId { get; set; }
        public string RelationShipTypeValue { get; set; }
        public int? IndustryId { get; set; }
        public string IndustryValue { get; set; }
        public string OtherIndustry { get; set; }
        public string HFCUTMCampaignCode { get; set; }
        public int? LMDBSourceID { get; set; }
        public int? SourcesHFCId { get; set; }
        public Nullable<int> ImpersonatorPersonId { get; set; }
        //public List<AccountChild> ChildAccountList { get; set; }
        public Nullable<int> MyCRMLeadId { get; set; }
        public string AccountTypeName { get; set; }
        public int? TerritoryDisplayId { get; set; }
        public string DisplayTerritoryName { get; set; }
        public bool IsNotifyemails { get; set; }
        public bool IsNotifyText { get; set; }
        public bool SendText { get; set; }
        public static AccountTP MapFrom(AccountTPViewModel vm)
        {
            AccountTP model = new AccountTP();
            model.AccountId = vm.AccountId;
            model.FullName = vm.FullName;
            model.AccountGuid = vm.AccountGuid;
            model.AccountNumber = vm.AccountNumber;
            model.AccountStatusId = vm.AccountStatusId;
            model.LeadId = vm.LeadId;
            model.AccountTypeId = vm.AccountTypeId;
            model.FranchiseId = vm.FranchiseId;
            model.TerritoryId = vm.TerritoryId;
            model.TerritoryType = vm.TerritoryType;
            model.PersonId = vm.PersonId;
            model.SecPersonId = vm.SecPersonId;
            model.IsDeleted = vm.IsDeleted;
            model.CreatedOnUtc = vm.CreatedOnUtc;
            model.ReferringCustomerId = vm.ReferringCustomerId;
            model.CreatedByPersonId = vm.CreatedByPersonId;
            model.LastUpdatedOnUtc = vm.LastUpdatedOnUtc;
            model.LastUpdatedByPersonId = vm.LastUpdatedByPersonId;
            model.Notes = vm.Notes;
            model.DispositionId = vm.DispositionId;
            model.SalesStepId = vm.SalesStepId;
            model.TempCustomerId = vm.TempCustomerId;
            model.CampaignId = vm.CampaignId;
            model.IsPerpetual = vm.IsPerpetual;
            model.RelatedAccountId = vm.RelatedAccountId;
            model.ReferralTypeId = vm.ReferralTypeId;
            model.LeadType = vm.LeadType;
            model.AccountName = vm.AccountName;
            model.ReferralTypeName = vm.ReferralTypeName;
            model.RelatedAccountName = vm.RelatedAccountName;
            model.AllowFranchisetoReassign = vm.AllowFranchisetoReassign;
            model.SourcesTPIdFromCampaign = vm.SourcesTPIdFromCampaign;
            model.SourcesTPId = vm.SourcesTPId;
            model.CampaignName = vm.CampaignName;
            model.AccountSourceId = vm.AccountSourceId;
            model.CustomerId = vm.CustomerId;
            model.SkipDuplicateCheck = vm.SkipDuplicateCheck;
            model.OpportunityId = vm.OpportunityId;
            model.IsTaxExempt = vm.IsTaxExempt;
            model.TaxExemptID = vm.TaxExemptID;
            model.IsPSTExempt = vm.IsPSTExempt;
            model.IsGSTExempt = vm.IsGSTExempt;
            model.IsHSTExempt = vm.IsHSTExempt;
            model.IsVATExempt = vm.IsVATExempt;
            model.AllOpportunityTaxExempt = vm.AllOpportunityTaxExempt;
            model.AccountType = vm.AccountType;
            model.KeyAcct = vm.KeyAcct;
            model.LegacyAccount = vm.LegacyAccount;
            model.IsCommercial = vm.IsCommercial;
            model.CommercialTypeId = vm.CommercialTypeId;
            model.CommercialValue = vm.CommercialValue;
            model.RelationshipTypeId = vm.RelationshipTypeId;
            model.RelationShipTypeValue = vm.RelationShipTypeValue;
            model.IndustryId = vm.IndustryId;
            model.IndustryValue = vm.IndustryValue;
            model.OtherIndustry = vm.OtherIndustry;
            model.HFCUTMCampaignCode = vm.HFCUTMCampaignCode;
            model.LMDBSourceID = vm.LMDBSourceID;
            model.SourcesHFCId = vm.SourcesHFCId;
            model.ImpersonatorPersonId = vm.ImpersonatorPersonId;
            model.MyCRMLeadId = vm.MyCRMLeadId;
            model.AccountTypeName = vm.AccountTypeName;
            model.TerritoryDisplayId = vm.TerritoryDisplayId;
            model.DisplayTerritoryName = vm.DisplayTerritoryName;
            model.IsNotifyemails = vm.IsNotifyemails;
            model.IsNotifyText = vm.IsNotifyText;
            model.SendText = vm.SendText;

            if (vm.PrimCustomer != null)
                model.PrimCustomer = CustomerTPViewModel.MapFrom(vm.PrimCustomer);

            List<CustomerTP> SecCustomer = new List<CustomerTP>();
            if (vm.SecCustomer != null && vm.SecCustomer.Count > 0)
            {
                foreach (var Cusvm in vm.SecCustomer)
                {
                    CustomerTP seccus = CustomerTPViewModel.MapFrom(Cusvm);
                    SecCustomer.Add(seccus);
                }
            }
            model.SecCustomer = SecCustomer;

            List<AddressTP> lstaddress = new List<AddressTP>();
            if (vm.Addresses != null && vm.Addresses.Count > 0)
            {
                foreach (var addvm in vm.Addresses)
                {
                    AddressTP address = AddressTPViewModel.MapFrom(addvm);
                    lstaddress.Add(address);
                }
            }
            model.Addresses = lstaddress;

            List<AccountSource> lstAccountSource = new List<AccountSource>();
            if (vm.AccountSources != null && vm.AccountSources.Count > 0)
            {
                foreach (var sourcevm in vm.AccountSources)
                {
                    AccountSource Source = AccountSourceViewModel.MapFrom(sourcevm);
                    lstAccountSource.Add(Source);
                }
            }
            model.AccountSources = lstAccountSource;

            return model;
        }
    }

    public partial class AccountSourceViewModel
    {
        public int AccountSourceId { get; set; }
        public int Value { get; set; }
        public int SourceId { get; set; }
        public int AccountId { get; set; }
        public bool IsManuallyAdded { get; set; }
        public bool IsPrimarySource { get; set; }
        public static AccountSource MapFrom(AccountSourceViewModel vm)
        {
            AccountSource Source = new AccountSource();
            Source.AccountSourceId = vm.AccountSourceId;
            Source.Value = vm.Value;
            Source.SourceId = vm.SourceId;
            Source.AccountId = vm.AccountId;
            Source.IsManuallyAdded = vm.IsManuallyAdded;
            Source.IsPrimarySource = vm.IsPrimarySource;
            return Source;
        }
    }

}