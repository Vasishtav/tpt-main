﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("HFC.CRM.Service")]
[assembly: AssemblyDescription("HFC CRM")]
[assembly: AssemblyConfiguration("HFC CRM")]
[assembly: AssemblyCompany("HFC CRM")]
[assembly: AssemblyProduct("HFC.CRM.Service")]
[assembly: AssemblyCopyright("HFC CRM")]
[assembly: AssemblyTrademark("HFC CRM")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("5e924571-1a8d-4bab-b4d2-2119d6cf8ec5")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.0.0.2")]
[assembly: AssemblyFileVersion("1.0.0.2")]
