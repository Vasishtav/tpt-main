﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(HFC.CRM.Service.Startup))]

namespace HFC.CRM.Service
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            SelectPdf.GlobalProperties.LicenseKey = "l7ymt6Wiprekp6akt6avuae3pKa5pqW5rq6urg==";
        }
    }
}
