﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HFC.CRM.Core.Membership;
using HFC.CRM.Data;
using System.Data.Entity;
using System.Runtime.Caching;

/// <summary>
/// The Common namespace.
/// </summary>
namespace HFC.CRM.Core.Common
{
    /// <summary>
    /// Summary description for CacheManager
    /// </summary>
    public class CacheManager
    {
        /// <summary>
        /// Gets or sets the _ permission collection.
        /// </summary>
        /// <value>The _ permission collection.</value>
        private static List<Permission> _PermissionCollection
        {
            get { return GetCache<List<Permission>>("CRM:PermissionCollection"); }
            set { SetCache("CRM:PermissionCollection", value); }
        }
        /// <summary>
        /// Gets or sets the permission collection.
        /// </summary>
        /// <value>The permission collection.</value>
        internal static List<Permission> PermissionCollection
        {
            get
            {
                if (_PermissionCollection == null)
                {
                    using (var db = new CRMContext())
                    {
                        _PermissionCollection = db.Permissions.ToList();
                    }
                }
                return _PermissionCollection;
            }
            set
            {
                _PermissionCollection = value;
            }
        }

        /// <summary>
        /// Gets or sets the _ permission type collection.
        /// </summary>
        /// <value>The _ permission type collection.</value>
        private static List<Type_Permission> _PermissionTypeCollection
        {
            get { return GetCache<List<Type_Permission>>("CRM:PermissionTypeCollection"); }
            set { SetCache("CRM:PermissionTypeCollection", value); }
        }
        /// <summary>
        /// Gets or sets the permission type collection.
        /// </summary>
        /// <value>The permission type collection.</value>
        public static List<Type_Permission> PermissionTypeCollection
        {
            get
            {
                if (_PermissionTypeCollection == null)
                {
                    using (var db = new CRMContext())
                    {
                        _PermissionTypeCollection = db.Type_Permission.ToList();
                    }
                }
                return _PermissionTypeCollection;
            }
            set
            {
                _PermissionTypeCollection = value;
            }
        }

        /// <summary>
        /// Gets or sets the _ role collection.
        /// </summary>
        /// <value>The _ role collection.</value>
        private static List<Role> _RoleCollection
        {
            get { return GetCache<List<Role>>("CRM:RoleCollection"); }
            set { SetCache("CRM:RoleCollection", value); }
        }
        /// <summary>
        /// Gets or sets the role collection.
        /// </summary>
        /// <value>The role collection.</value>
        public static List<Role> RoleCollection
        {
            get
            {
                if (_RoleCollection == null)
                {
                    using (var db = new CRMContext())
                    {
                        _RoleCollection = db.Roles.ToList();
                    }
                }
                return _RoleCollection;
            }
            set
            {
                _RoleCollection = value;
            }
        }

        /// <summary>
        /// Gets or sets the _ application configuration collection.
        /// </summary>
        /// <value>The _ application configuration collection.</value>
        private static List<AppConfig> _AppConfigCollection
        {
            get { return GetCache<List<Data.AppConfig>>("CRM:AppConfigCollection"); }
            set { SetCache("CRM:AppConfigCollection", value); }
        }
        /// <summary>
        /// Returns list of all app config
        /// </summary>
        /// <value>The application configuration collection.</value>
        internal static List<AppConfig> AppConfigCollection
        {
            get
            {
                if (_AppConfigCollection == null)
                {
                    using (var db = new CRMContext())
                    {
                        _AppConfigCollection = db.AppConfigs.ToList();
                    }
                }
                return _AppConfigCollection;
            }
            set
            {
                _AppConfigCollection = value;
            }
        }


        /// <summary>
        /// Gets or sets the _ job status type collection.
        /// </summary>
        /// <value>The _ job status type collection.</value>
        private static List<Type_JobStatus> _JobStatusTypeCollection
        {
            get { return GetCache<List<Type_JobStatus>>("CRM:JobStatusTypeCollection"); }
            set { SetCache("CRM:JobStatusTypeCollection", value); }
        }
        /// <summary>
        /// Gets or sets the job status type collection.
        /// </summary>
        /// <value>The job status type collection.</value>
        public static List<Type_JobStatus> JobStatusTypeCollection
        {
            get
            {
                if (_JobStatusTypeCollection == null)
                {
                    using (var db = new CRMContext())
                    {
                        _JobStatusTypeCollection = db.Type_JobStatus.OrderBy(o => o.ParentId).ThenBy(o => o.DisplayOrder).ToList();
                    }
                }
                return _JobStatusTypeCollection;
            }
            set
            {
                _JobStatusTypeCollection = value;
            }
        }

        /// <summary>
        /// Gets or sets the _ lead status type collection.
        /// </summary>
        /// <value>The _ lead status type collection.</value>
        private static List<Type_LeadStatus> _LeadStatusTypeCollection
        {
            get { return GetCache<List<Type_LeadStatus>>("CRM:LeadStatusTypeCollection"); }
            set { SetCache("CRM:LeadStatusTypeCollection", value); }
        }
        /// <summary>
        /// Gets or sets the lead status type collection.
        /// </summary>
        /// <value>The lead status type collection.</value>
        public static List<Type_LeadStatus> LeadStatusTypeCollection
        {
            get
            {
                if (_LeadStatusTypeCollection == null)
                {
                    using (var db = new CRMContext())
                    {
                        _LeadStatusTypeCollection = db.Type_LeadStatus.OrderBy(o => o.ParentId).ThenBy(o => o.DisplayOrder).ToList();
                    }
                }
                return _LeadStatusTypeCollection;
            }
            set
            {
                _LeadStatusTypeCollection = value;
            }
        }

        /// <summary>
        /// Gets or sets the _ product type collection.
        /// </summary>
        /// <value>The _ product type collection.</value>
        private static List<Type_Product> _ProductTypeCollection
        {
            get { return GetCache<List<Type_Product>>("CRM:ProductTypeCollection"); }
            set { SetCache("CRM:ProductTypeCollection", value); }
        }
        /// <summary>
        /// Gets the product type collection.
        /// </summary>
        /// <value>The product type collection.</value>
        public static List<Type_Product> ProductTypeCollection
        {
            get
            {
                if (_ProductTypeCollection == null)
                {
                    using (var db = new CRMContext())
                    {
                        _ProductTypeCollection = db.Type_Products.ToList();
                    }
                }
                return _ProductTypeCollection;
            }
        }

        private static List<Manufacturer> _MfgCollection
        {
            get { return GetCache<List<Manufacturer>>("CRM:ManufacturerCollection"); }
            set { SetCache("CRM:ManufacturerCollection", value); }
        }
        /// <summary>
        /// Gets a list of manufacturers
        /// </summary>
        public static List<Manufacturer> ManufacturerCollection
        {
            get
            {
                if (_MfgCollection == null)
                {
                    using (var db = new CRMContext())
                    {
                        _MfgCollection = db.Manufacturers.ToList();
                    }
                }
                return _MfgCollection;
            }
        }

        private static List<Type_Style> _ProductStyleCollection
        {
            get { return GetCache<List<Type_Style>>("CRM:ProductStyleCollection"); }
            set { SetCache("CRM:ProductStyleCollection", value); }
        }
        public static List<Type_Style> ProductStyleCollection
        {
            get
            {
                if (_ProductStyleCollection == null)
                {
                    using (var db = new CRMContext())
                    {
                        _ProductStyleCollection = db.Type_Styles.ToList();
                    }
                }
                return _ProductStyleCollection;
            }
        }

        /// <summary>
        /// Gets or sets the _ product category collection.
        /// </summary>
        /// <value>The _ product category collection.</value>
        private static List<Type_Category> _ProductCategoryCollection
        {
            get { return GetCache<List<Type_Category>>("CRM:ProductCategoryCollection"); }
            set { SetCache("CRM:ProductCategoryCollection", value); }
        }
        /// <summary>
        /// Gets the product category collection.
        /// </summary>
        /// <value>The product category collection.</value>
        public static List<Type_Category> ProductCategoryCollection
        {
            get {
                if (_ProductCategoryCollection == null)
                {
                    using (var db = new CRMContext())
                    {
                        _ProductCategoryCollection = db.Type_Category.ToList();
                    }
                }
                return _ProductCategoryCollection;
            }
        }
        /// <summary>
        /// Gets or sets the _ concept collection.
        /// </summary>
        /// <value>The _ concept collection.</value>
        private static List<Type_Concept> _ConceptCollection
        {
            get { return GetCache<List<Type_Concept>>("CRM:ConceptCollection"); }
            set { SetCache("CRM:ConceptCollection", value); }
        }
        /// <summary>
        /// Gets the concept collection.
        /// </summary>
        /// <value>The concept collection.</value>
        public static List<Type_Concept> ConceptCollection
        {
            get
            {
                if (_ConceptCollection == null)
                {
                    using (var db = new CRMContext())
                    {
                        _ConceptCollection = db.Type_Concepts.ToList();
                    }
                }
                return _ConceptCollection;
            }
        }               

        /// <summary>
        /// Gets or sets the _ source type collection.
        /// </summary>
        /// <value>The _ source type collection.</value>
        private static List<Type_Source> _SourceTypeCollection
        {
            get { return GetCache<List<Type_Source>>("CRM:SourceTypeCollection"); }
            set { SetCache("CRM:SourceTypeCollection", value); }
        }
        /// <summary>
        /// Gets or sets the source type collection.
        /// </summary>
        /// <value>The source type collection.</value>
        public static List<Type_Source> SourceTypeCollection
        {
            get
            {
                if (_SourceTypeCollection == null)
                {
                    using (var db = new CRMContext())
                    {
                        _SourceTypeCollection = db.Type_Sources.ToList();
                    }
                }
                return _SourceTypeCollection;
            }
            set
            {
                _SourceTypeCollection = null;
            }
        }


        /// <summary>
        /// Gets or sets the _ question type collection.
        /// </summary>
        /// <value>The _ question type collection.</value>
        private static List<Type_Question> _QuestionTypeCollection
        {
            get { return GetCache<List<Type_Question>>("CRM:QuestionTypeCollection"); }
            set { SetCache("CRM:QuestionTypeCollection", value); }
        }
        /// <summary>
        /// Gets the question type collection.
        /// </summary>
        /// <value>The question type collection.</value>
        public static List<Type_Question> QuestionTypeCollection
        {
            get
            {
                if (_QuestionTypeCollection == null)
                {
                    using (var db = new CRMContext())
                    {
                        _QuestionTypeCollection = db.Type_Questions.OrderBy(o => o.Question).ThenBy(o => o.DisplayOrder).ToList();
                    }
                }
                return _QuestionTypeCollection;
            }
        }

        /// <summary>
        /// Gets or sets the _ states collection.
        /// </summary>
        /// <value>The _ states collection.</value>
        private static List<Type_State> _StatesCollection
        {
            get { return GetCache<List<Type_State>>("CRM:StatesCollection"); }
            set { SetCache("CRM:StatesCollection", value); }
        }
        /// <summary>
        /// Gets the states collection.
        /// </summary>
        /// <value>The states collection.</value>
        public static List<Type_State> StatesCollection
        {
            get
            {
                if (_StatesCollection == null)
                {
                    using (var db = new CRMContext())
                    {
                        _StatesCollection = db.Type_States.Include("Type_Country").ToList();
                    }
                }
                return _StatesCollection;
            }
        }

        /// <summary>
        /// Gets or sets the _ color palettes.
        /// </summary>
        /// <value>The _ color palettes.</value>
        private static List<Type_Color> _ColorPalettes
        {
            get { return GetCache<List<Type_Color>>("CRM:ColorPalettes"); }
            set { SetCache("CRM:ColorPalettes", value); }
        }
        /// <summary>
        /// Gets the color palettes.
        /// </summary>
        /// <value>The color palettes.</value>
        public static List<Type_Color> ColorPalettes
        {
            get
            {
                if (_ColorPalettes == null)
                {
                    using (var db = new CRMContext())
                    {
                        _ColorPalettes = db.Type_Colors.ToList();
                    }
                }
                return _ColorPalettes;
            }
        }

        /// <summary>
        /// Gets or sets the _ user collection.
        /// </summary>
        /// <value>The _ user collection.</value>
        private static List<User> _UserCollection
        {
            get { return GetCache<List<User>>("CRM:UserCollection"); }
            //15 minute absolute timeout so this can be as fresh as possible without re-querying DB often
            set { SetCache("CRM:UserCollection", value, DateTime.Now.AddMinutes(15)); } 
        }
        /// <summary>
        /// All User collection for quick reference, has 15 minute expire time
        /// </summary>
        /// <value>The user collection.</value>
        public static List<User> UserCollection
        {
            get 
            {
                if (_UserCollection == null)
                {
                    _UserCollection = LocalMembership.GetUsers(null, "OAuthUsers", "Roles", "Person");
                }
                return _UserCollection;
            }
            internal set
            {
                _UserCollection = value;
            }
        }
        /// <summary>
        /// Only clears CRM related cache
        /// </summary>
        public static void ClearCache()
        {            
            //clear cache properties here
            _RoleCollection = null;
            _PermissionCollection = null;
            _PermissionTypeCollection = null;
            _AppConfigCollection = null;
            _QuestionTypeCollection = null;
            _LeadStatusTypeCollection = null;
            //_AppointmentTypeCollection = null;
            _SourceTypeCollection = null;
            _ConceptCollection = null;
            _StatesCollection = null;
            _UserCollection = null;
            _ColorPalettes = null;
        }

        /// <summary>
        /// Gets the cache.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name">The name.</param>
        /// <returns>T.</returns>
        public static T GetCache<T>(string name)
        {             
            var cache = MemoryCache.Default;
            if (cache[name] != null)
                return Util.CastObject<T>(cache[name]);
            else
                return default(T);
        }

        /// <summary>
        /// Sets session
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        /// <param name="absoluteExpire">The absolute expire.</param>
        /// <param name="slidingExpire">The sliding expire.</param>
        private static void _SetCache(string name, object value, DateTime? absoluteExpire = null, TimeSpan? slidingExpire = null)
        {
            var cache = MemoryCache.Default;
            if (value == null)
            {
                cache.Remove(name);
            }
            else
            {
                var policy = new CacheItemPolicy();
                policy.Priority = CacheItemPriority.Default;
                if (slidingExpire.HasValue)
                {
                    policy.SlidingExpiration = slidingExpire.Value;
                    policy.AbsoluteExpiration = ObjectCache.InfiniteAbsoluteExpiration;
                }
                else
                {
                    if (!absoluteExpire.HasValue)
                        absoluteExpire = DateTime.UtcNow.AddHours(10);
                    policy.AbsoluteExpiration = absoluteExpire.Value;
                    policy.SlidingExpiration = ObjectCache.NoSlidingExpiration;                    
                }
				if(cache[name] != null)
					cache.Set(name, value, policy);
				else
					cache.Add(name, value, policy);
            }
        }

        /// <summary>
        /// Sets cache with default of 10 hour absolute expire
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public static void SetCache(string name, object value)
        {
            _SetCache(name, value);
        }

        /// <summary>
        /// Removes the cache.
        /// </summary>
        /// <param name="name">The name.</param>
        public static void RemoveCache(string name)
        {
            _SetCache(name, null);
        }

        /// <summary>
        /// Sets the cache.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        /// <param name="absoluteExpire">The absolute expire.</param>
        public static void SetCache(string name, object value, DateTime absoluteExpire)
        {
            _SetCache(name, value, absoluteExpire);
        }

        /// <summary>
        /// Sets the cache.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        /// <param name="slidingExpire">The sliding expire.</param>
        public static void SetCache(string name, object value, TimeSpan slidingExpire)
        {
            _SetCache(name, value, null, slidingExpire);
        }


        /// <summary>
        /// Gets the lead status.
        /// </summary>
        /// <param name="statusId">The status identifier.</param>
        /// <returns>Type_LeadStatus.</returns>
        public static Type_LeadStatus GetLeadStatus(int statusId)
        {
            if (statusId > 0)
                return LeadStatusTypeCollection.SingleOrDefault(s => s.Id == statusId);
            else
                return null;
        }

        /// <summary>
        /// Gets the lead status string.
        /// </summary>
        /// <param name="statusId">The status identifier.</param>
        /// <returns>System.String.</returns>
        public static string GetLeadStatusString(int statusId)
        {
            var src = GetLeadStatus(statusId);
            if (src != null)
                return src.ToString();
            else
                return string.Empty;
        }

        /// <summary>
        /// Gets the job status.
        /// </summary>
        /// <param name="statusId">The status identifier.</param>
        /// <returns>Type_JobStatus.</returns>
        public static Type_JobStatus GetJobStatus(int statusId)
        {
            if (statusId > 0)
                return JobStatusTypeCollection.SingleOrDefault(s => s.Id == statusId);
            else
                return null;
        }

        /// <summary>
        /// Gets the job status string.
        /// </summary>
        /// <param name="statusId">The status identifier.</param>
        /// <returns>System.String.</returns>
        public static string GetJobStatusString(int statusId)
        {
            var src = GetJobStatus(statusId);
            if (src != null)
                return src.ToString();
            else
                return string.Empty;
        }

        /// <summary>
        /// Gets the source.
        /// </summary>
        /// <param name="sourceId">The source identifier.</param>
        /// <returns>Type_Source.</returns>
        public static Type_Source GetSource(int sourceId)
        {
            if (sourceId > 0)
                return SourceTypeCollection.SingleOrDefault(s => s.Id == sourceId);
            else
                return null;
        }

        /// <summary>
        /// Gets the source string.
        /// </summary>
        /// <param name="sourceId">The source identifier.</param>
        /// <returns>System.String.</returns>
        public static string GetSourceString(int sourceId)
        {
            var src = GetSource(sourceId);
            if (src != null)
                return src.ToString();
            else
                return string.Empty;
        }

        /// <summary>
        /// Gets the category.
        /// </summary>
        /// <param name="categoryId">The category identifier.</param>
        /// <returns>Type_Category.</returns>
        public static Type_Category GetCategory(int categoryId)
        {
            if (categoryId > 0)
                return ProductCategoryCollection.SingleOrDefault(s => s.Id == categoryId);
            else
                return null;
        }

        /// <summary>
        /// Gets the name of the category.
        /// </summary>
        /// <param name="categoryId">The category identifier.</param>
        /// <returns>System.String.</returns>
        public static string GetCategoryName(int categoryId)
        {
            var cat = GetCategory(categoryId);
            if (cat != null)
                return cat.Category;
            else
                return string.Empty;
        }
        /// <summary>
        /// Gets the type of the product.
        /// </summary>
        /// <param name="typeId">The type identifier.</param>
        /// <returns>Type_Product.</returns>
        public static Type_Product GetProductType(int typeId)
        {
            if (typeId > 0)
                return ProductTypeCollection.SingleOrDefault(s => s.ProductTypeId == typeId);
            else
                return null;
        }

        /// <summary>
        /// Gets the name of the product type.
        /// </summary>
        /// <param name="typeId">The type identifier.</param>
        /// <returns>System.String.</returns>
        public static string GetProductTypeName(int typeId)
        {
            var prodtype = GetProductType(typeId);
            if (prodtype != null)
                return prodtype.TypeName;
            else
                return string.Empty;
        }

    }

    
}