﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO
{
    public class FileTableTb
    {
        public string fileName { get; set; }
        public string physicalfilePath { get; set; }
        public string virtualfilePath { get; set; }
        public byte[] bytes { get; set; }

        public string FileType { get; set; }
    }
}
