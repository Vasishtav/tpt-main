﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO
{
    public class DavinciOpportunityInfo
    {
        public int OpportunityId { get; set; }
        public string OpportunityName { get; set; }
        public string InstallationAddress { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Status { get; set; }
    }
}
