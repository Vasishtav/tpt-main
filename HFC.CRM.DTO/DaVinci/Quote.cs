﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace HFC.CRM.DTO
{
    public class QuoteDavinci
    {
        public int OpportunityId { get; set; }
        public FileDataDavinci File { get; set; }
        public PartsDetailDavinci[] PartsDetail { get; set; }
        public QuoteLinesDavinci[] QuoteLines { get; set; }
        public decimal totalcost { get; set; }
        public decimal totalmargin { get; set; }
        public decimal totalprice { get; set; }
        public decimal totallabor { get; set; }
    }

    public class QuoteLinesDavinci
    {
        public string RoomName { get; set; }
        public int QTY { get; set; }
        public string ProductCategory { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public decimal UnitPrice { get; set; }
        public List<FileDataDavinci> image { get; set; }
        public PartsDetailDavinci[] PartsDetail { get; set; }
        public decimal totalcost { get; set; }
        public decimal totalmargin { get; set; }
        public decimal totalprice { get; set; }
        public decimal totallabor { get; set; }
    }

    public class PartsDetailDavinci
    {
        public string partid { get; set; }
        public string desc { get; set; }
        public int qty { get; set; }
        public string vendor { get; set; }
        public string productcategory { get; set; }
        public string subcategory { get; set; }
        public string finish { get; set; }
        public int cutlen { get; set; }
        public float cutht { get; set; }
        public int cutdepth { get; set; }
        public float cost { get; set; }
        public float material { get; set; }
        public int margin { get; set; }
        public float total { get; set; }
        public int weight { get; set; }
    }


    public class FileDataDavinci
    {
        public byte[] File_Base64String { get; set; }
        public string FileName { get; set; }
    }


    public class PICJsonDavinci
    {
        public string VendorName { get; set; }
        public string ProductName { get; set; }
        public string PCategory { get; set; }
        public string PicVendor { get; set; }
        public int PicProduct { get; set; }
        public int ProductId { get; set; }
        public string ModelId { get; set; }
        public int GGroup { get; set; }
        public int QuoteLineId { get; set; }
        public int Quantity { get; set; }
        public int VendorId { get; set; }
    }

}
