﻿using HFC.CRM.DTO.Person;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Lead
{
    public class LeadInfoDTO
    {
        public int LeadId { get; set; }
        public int LeadNumber { get; set; }
        public int CreatedOnUtc { get; set; }
        public int LeadStatusId { get; set; }
        public int AddressCount { get; set; }
        public int PrimePersonId { get; set; }
        public int POCount { get; set; }
        public int NotesCount { get; set; }
        public string LeadSourceIdsStr { get; set; }
        public List<int> LeadSourceIds
        {
            get
            {
                return LeadSourceIdsStr.Split(',').Select(int.Parse).ToList();
            }
        }

        public string PrimaryNotes { get; set; }
        public PersonDTO PrimaryPerson { get; set; }
    }
}
