﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.BulkPurchase
{
    public class QuoteLinesBulkPurchaseDTO
    {
        public int QuoteLineId { get; set; }
        public int QuoteKey { get; set; }
        public int Quantity { get; set; }
        public string RoomName { get; set; }
        public int VendorId { get; set; }
        public string VendorName { get; set; }
        public string ProductName { get; set; }
        public decimal Width { get; set; }
        public decimal Height { get; set; }
        public string MountType { get; set; }
        public string Color { get; set; }
        public string Fabric { get; set; }
        public decimal ExtendedPrice { get; set; }
        public decimal Cost { get; set; }
        public int QuoteId { get; set; }
        public string FranctionalValueHeight { get; set; }
        public string FranctionalValueWidth { get; set; }

        //public int OrderId { get; set; }
        //public string OrderName { get; set; }
        //public int OrderNumber { get; set; }
        //public DateTime CreatedOn { get; set; }
        public decimal Sales { get; set; }
        public int QuoteLineNumber { get; set; }




    }
}
