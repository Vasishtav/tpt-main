﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.BulkPurchase
{
    public class OrderBulkPurchaseDTO
    {
        public int OrderID { get; set; }
        public int OrderNumber { get; set; }
        public string OrderName { get; set; }
        public decimal TotalCost { get; set; }
        public decimal TotalSales { get; set; }
        public int TotalQuantity { get; set; }
        public int QuoteKey { get; set; }
        public DateTime CreatedOn { get; set; }
        public IList<QuoteLinesBulkPurchaseDTO> QuoteLines { get; set; }

    }
}
