﻿using System;
using System.Runtime.Serialization;

namespace HFC.CRM.DTO.Sync
{
    [KnownType(typeof(QBReceivePayment))]
    public sealed class QBReceivePayment
    {
        public string CustomerListID { get; set; }

        public int CustomerQbID { get; set; }
        public string ARAccountListID { get; set; }
        public string PaymentMethodListID { get; set; }
        public string InvoiceListID { get; set; }

        public int ID { get; set; }
        public string Customer { get; set; }
        public string PaymentMethod { get; set; }
        public string InvoiceNumber { get; set; }


        //public string ARAccount { get { return "Accounts Receivable BBI"; } }

        //public string ARAccount { get { return AccountHandler.AccountsReceivableAccount; } }

        public DateTime TxnDate { get; set; }
        public string TxnDateString { get { return TxnDate.ToString(@"yyyy-MM-dd"); } }
        //public string TxnDate { get; set; }
        public string RefNumber { get; set; }
        public string TotalAmount { get; set; }
        public string Memo { get; set; }
        public string PaymentAmount { get; set; }
    }
}
