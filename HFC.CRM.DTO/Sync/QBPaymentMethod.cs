﻿using System.Runtime.Serialization;

namespace HFC.CRM.DTO.Sync
{
    [KnownType(typeof(QBPaymentMethod))]
    public sealed class QBPaymentMethod
    {
        public static QBPaymentMethod Make(int id, string name)
        {
            return new QBPaymentMethod
            {
                ID = id,
                Name = name,
            };
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public string Type { get { return QBPaymentMethodHelper.TypeFor(this.Name); } }
    }

    public static class QBPaymentMethodHelper
    {
        public static string TypeFor(string typeName)
        {
            if (typeName == "Cash")
                return "Cash";
            else if (typeName.Contains("AmericanExpress"))
                return "AmericanExpress";
            else if (typeName.Contains("Visa"))
                return "Visa";
            else if (typeName.Contains("MasterCard"))
                return "MasterCard";
            else if (typeName.ToLower().Contains("check"))
                return "Check";
            else if (typeName.ToLower().Contains("discover"))
                return "Discover";
            return "Cash";
        }
    }
}
