﻿
namespace HFC.CRM.DTO.Sync
{
    public sealed class QBBillLineItem
    {
        public static QBBillLineItem Make(string id, string item, string desc, string quantity, string cost, string customer)
        {
            return new QBBillLineItem
            {
                ID = id,
                Item = item ?? string.Empty,
                Desc = desc ?? string.Empty,
                Quantity = quantity,
                Cost = cost,
                Customer = customer
            };
        }

        public string ItemListID { get; set; }
        public string Customer { get; set; }

        public string ID { get; set; }
        public string Item { get; set; }
        public string Desc { get; set; }
        public string Quantity { get; set; }
        public string Cost { get; set; }
        public string Amount { get { return (int.Parse(this.Quantity) * double.Parse(this.Cost)).ToString(); } }
    }
}
