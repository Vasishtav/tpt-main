﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HFC.CRM.Data;

namespace HFC.CRM.DTO.Sync
{
    public class PreSyncCheckModel
    {
        public PreSyncCheckModel()
        {
            Invoices = new Dictionary<string, List<InvoicesTP>>();
            Opportunities = new Dictionary<string, List<Data.Opportunity>>();
        }
        public Dictionary<string, List<InvoicesTP>> Invoices { get; set; }
        public Dictionary<string, List<Data.Opportunity>> Opportunities { get; set; }
        public List<List<CustomerTP>> DuplicateCustomerName { get; set; }

        public bool Correct
        {
            get { return Invoices.Count + Opportunities.Count + DuplicateCustomerName?.Count == 0; }
        }

        public void AddInvoice(InvoicesTP invoice, string error)
        {
            if (!Invoices.ContainsKey(error))
            {
                Invoices[error] = new List<InvoicesTP>();
            }
            if (!Invoices[error].Any(x => x.InvoiceId == invoice.InvoiceId))
            {
                Invoices[error].Add(invoice);
            }
        }

        public void AddOpportunity(Data.Opportunity Opportunity, string error)
        {
            if (!Opportunities.ContainsKey(error))
            {
                Opportunities[error] = new List<Data.Opportunity>();
            }
            if (!Opportunities[error].Any(x => x.AccountId == Opportunity.AccountId))
            {
                Opportunities[error].Add(Opportunity);
            }
        }

    }
}
