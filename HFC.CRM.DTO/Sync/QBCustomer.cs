﻿using System.Runtime.Serialization;

namespace HFC.CRM.DTO.Sync
{
    [KnownType(typeof(QBOCustomer))]
    public class QBOCustomer
    {
        public static QBOCustomer Make(int id, int qbId, string name, string companyName, string salutation, string firstName, string lastName,
                        string billAddr1, string billCity, string billState, string billPostalCode, string billCountry, string billNote,
                        string shipAddr1, string shipCity, string shipState, string shipPostalCode, string shipCountry, string shipNote,
                        string phone, string altPhone, string fax, string email, string contact, string altContact)
        {
            return new QBOCustomer
            {
                ID = id,
                QbId = qbId,
                Name = name ?? string.Empty,
                CompanyName = companyName ?? string.Empty,
                Salutation = salutation ?? string.Empty,
                FirstName = firstName ?? string.Empty,
                LastName = lastName ?? string.Empty,
                BillAddr1 = billAddr1 ?? string.Empty,
                BillCity = billCity ?? string.Empty,
                BillState = billState ?? string.Empty,
                BillPostalCode = billPostalCode ?? string.Empty,
                BillCountry = billCountry ?? string.Empty,
                BillNote = billNote ?? string.Empty,
                ShipAddr1 = shipAddr1 ?? string.Empty,
                ShipCity = shipCity ?? string.Empty,
                ShipState = shipState ?? string.Empty,
                ShipPostalCode = shipPostalCode ?? string.Empty,
                ShipCountry = shipCountry ?? string.Empty,
                ShipNote = shipNote ?? string.Empty,
                Phone = phone ?? string.Empty,
                AltPhone = altPhone ?? string.Empty,
                Fax = fax ?? string.Empty,
                Email = email ?? string.Empty,
                Contact = contact ?? string.Empty,
                AltContact = altContact ?? string.Empty,
            };
        }

        public int ID { get; set; }

        public int QbId { get; set; }
        public string Name { get; set; }
        public string CompanyName { get; set; }
        public string Salutation { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }

        public string BillAddr1 { get; set; }
        public string BillCity { get; set; }
        public string BillState { get; set; }
        public string BillPostalCode { get; set; }
        public string BillCountry { get; set; }
        public string BillNote { get; set; }

        public string ShipAddr1 { get; set; }
        public string ShipCity { get; set; }
        public string ShipState { get; set; }
        public string ShipPostalCode { get; set; }
        public string ShipCountry { get; set; }
        public string ShipNote { get; set; }

        public string Phone { get; set; }
        public string AltPhone { get; set; }

        public string CellPhone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Contact { get; set; }
        public string AltContact { get; set; }
        public bool IsCommercial { get; set; }
        public int AccountId { get; set; }
    }
}
