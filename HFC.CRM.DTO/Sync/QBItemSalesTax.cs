﻿using System.Runtime.Serialization;

namespace HFC.CRM.DTO.Sync
{
    [KnownType(typeof(QBItemSalesTax))]
    public sealed class QBItemSalesTax
    {
        public static QBItemSalesTax Make(int id, string name, string itemDesc, string taxRate)//, string zip)
        {
            return new QBItemSalesTax
            {
                ID = id,
                Name = name ?? string.Empty,
                ItemDesc = itemDesc ?? string.Empty,
                TaxRate = taxRate ?? string.Empty //, Zip = zip ?? string.Empty
            };
        }

        public string SalesTaxAgencyID { get; set; }
        public string SalesTaxAgency { get { return "General Tax Agency"; } }
        public string SalesTaxReturnLineID { get; set; }//For canada only

        public int ID { get; set; }
        public string Name { get; set; }
        public string ItemDesc { get; set; }
        public string TaxRate { get; set; }

        public int InvoviceId { get; set; }

        ////Murugan:
        //public string Zip { get; set; }
    }
}
