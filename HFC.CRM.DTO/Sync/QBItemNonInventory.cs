﻿using System.Runtime.Serialization;

namespace HFC.CRM.DTO.Sync
{
    [KnownType(typeof(QBItemNonInventory))]
    public sealed class QBItemNonInventory
    {
        public static QBItemNonInventory Make(int id, string name, string manufacturerPartNumber, string desc, string price)
        {
            return new QBItemNonInventory
            {
                ID = id,
                Name = name ?? string.Empty,
                ManufacturerPartNumber = manufacturerPartNumber ?? string.Empty,
                Desc = desc ?? string.Empty,
                Price = price ?? string.Empty
            };
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public string ManufacturerPartNumber { get; set; }
        public string Desc { get; set; }
        public string Price { get; set; }

        //public string AcctName { get { return "Sales BBI"; } }
        //public string AcctName { get { return AccountHandler.SalesAccount; } }
        public string AcctListID { get; set; }
    }
}
