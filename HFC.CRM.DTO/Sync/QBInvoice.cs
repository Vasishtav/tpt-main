﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using HFC.CRM.Data;

namespace HFC.CRM.DTO.Sync
{
    [KnownType(typeof(QBOInvoice))]
    public sealed class QBOInvoice
    {
        public string CustomerListID { get; set; }

        public int CustomerQbID { get; set; }

        public string ARAccountListID { get; set; }

        public int ID { get; set; }
        public string Customer { get; set; }
        public DateTime TxnDate { get; set; }
        public string TxnDateString { get { return TxnDate.ToString(@"yyyy-MM-dd"); } }
        public string RefNumber { get; set; }

        public string BillAddr1 { get; set; }
        public string BillCity { get; set; }
        public string BillState { get; set; }
        public string BillPostalCode { get; set; }
        public string BillCountry { get; set; }
        public string BillNote { get; set; }

        public string ShipAddr1 { get; set; }
        public string ShipCity { get; set; }
        public string ShipState { get; set; }
        public string ShipPostalCode { get; set; }
        public string ShipCountry { get; set; }
        public string ShipNote { get; set; }

        public string ItemSalesTaxID { get; set; }
        public string ItemSalesTaxName { get; set; }

        public string PONumber { get; set; }
        public string DueDate { get; set; }

        //public string ARAccount { get; set; }

        public List<QBInvoiceLineItem> InvoiceItems { get; set; }

    }

    public static partial class ext
    {
        public static IList<QBBill> ToBills(this QBOInvoice obj)
        {
            var bills = new List<QBBill>();

            var vendors = new List<string>();

            foreach (var item in obj.InvoiceItems)
            {
                if (!string.IsNullOrEmpty(item.Vendor) && !vendors.Contains(item.Vendor) && item.ItemType == "ItemNonInventory" && item.IsSaleAdjustment == false)
                    vendors.Add(item.Vendor);
            }
            var index = 1;
            foreach (var item in vendors)
            {
                if (!string.IsNullOrEmpty(item))
                {
                    var billLineItems = new List<QBBillLineItem>();

                    var vendorItems = obj.InvoiceItems.Where(x => x.Vendor == item).ToList();

                    vendorItems.ForEach(x => billLineItems.Add(QBBillLineItem.Make(obj.RefNumber + "_" + index, x.Name, x.Desc, x.Quantity, x.Cost, obj.Customer)));

                    bills.Add(QBBill.Make(obj.RefNumber + "_" + index, item, obj.Customer, obj.TxnDateString, obj.DueDate, obj.RefNumber, "", billLineItems));
                    index++;
                }
            }

            return bills;
        }
    }

}
