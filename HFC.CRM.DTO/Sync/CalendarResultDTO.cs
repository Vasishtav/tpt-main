﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Sync
{
    public class CalendarResultDTO
    {
        public System.Guid UserId { get; set; }
        public int CalendarId { get; set; }
    }
}
