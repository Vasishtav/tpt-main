﻿
using System;

namespace HFC.CRM.DTO.Sync
{
    public sealed class QBInvoiceLineItem
    {
        public static QBInvoiceLineItem Make(int id, string name, string desc, string quantity, string amount,
            string amountType, string itemType, string taxable, string cost, string vendor)
        {
            return new QBInvoiceLineItem
            {
                ID = id,
                Name = name ?? string.Empty,
                Desc = desc ?? string.Empty,
                Quantity = quantity,
                //AmountDec = amount,
                AmountType = amountType,
                ItemType = itemType,
                Taxable = taxable,
                Cost = cost,
                Vendor = vendor
            };
        }

        public string ListID { get; set; }

        public int ID { get; set; }
        public string Name { get; set; }
        public string Desc { get; set; }
        public string Quantity { get; set; }
        public string AmountType { get; set; } //Rate or RatePercent
        public string ItemType { get; set; } //ItemNonInventory or ItemSalesTax
        public bool IsSaleAdjustment { get; set; }
        public string Taxable { get; set; }
        public string Cost { get; set; }
        public string Vendor { get; set; }

        public string TaxableSalesTaxCode { get; set; }
        public string NonTaxableSalesTaxCode { get; set; }

        public Nullable<bool> SuppressSalesTax { get; set; }
        public decimal TaxRate { get; set; }

        public decimal AmountDec;

        public string Amount
        {
            get { return AmountDec.ToString("0.00"); }
        }

    }
}

