﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HFC.CRM.DTO.Sync
{
    public sealed class QBBill
    {
        public static QBBill Make(string id, string vendorName,string customerName, string txnDate, string dueDate, string refNumber, string memo, List<QBBillLineItem> billLineItems)
        {
            return new QBBill
            {
                ID = id,
                VendorName = vendorName,
                CustomerName=customerName,
                TxnDate = txnDate,
                DueDate = dueDate,
                RefNumber = refNumber,
                Memo = memo,
                BillLineItems = billLineItems
            };
        }

        public string ID { get; set; }
        public string VendorName { get; set; }
        public string CustomerName { get; set; }
        public string APAccount { get { return "Accounts Payable"; } }

        //public string ExpAccount { get { return "Cost of Goods Sold BBI"; } }

        //public string ExpAccount { get { return AccountHandler.CostOfGoodsSoldAccount; } }
        public string VendorListID { get; set; }
        public string TxnDate { get; set; }
        public string TxnDateString { get { return Convert.ToDateTime(TxnDate).ToString(@"yyyy-MM-dd"); } }
        public string DueDate { get; set; }
        public string DueDateString { get { return Convert.ToDateTime(DueDate).ToString(@"yyyy-MM-dd"); } }
        public string RefNumber { get; set; }
        public string Memo { get; set; }
        public List<QBBillLineItem> BillLineItems { get; set; }
        public decimal Amount { get { return this.BillLineItems.Sum(x => decimal.Parse(x.Amount)); } }
        public string AmountString { get { return this.Amount.ToString("0.00"); } }

        public string ItemMemo
        {
            get
            {
                var sb = new StringBuilder();
                foreach (var item in BillLineItems)
                {
                    sb.Append(item.Item);
                    sb.Append("(Qty:");
                    sb.Append(item.Quantity);
                    sb.Append(")");
                }
                return sb.ToString();
            }
        }

    }
}
