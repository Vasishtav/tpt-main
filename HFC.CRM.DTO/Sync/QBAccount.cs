﻿using System.Runtime.Serialization;

namespace HFC.CRM.DTO.Sync
{
    [KnownType(typeof(QBAccount))]
    public sealed class QBAccount
    {
        public static QBAccount Make(int id, string name, string accountType, string accountNumber)
        {
            return new QBAccount
            {
                ID=id,
                Name = name ?? string.Empty,
                AccountType = accountType ?? string.Empty,
                AccountNumber = accountNumber ?? string.Empty
            };
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public string AccountType { get; set; }
        public string AccountNumber { get; set; }
    }
}
