﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Sync
{
    public class QBSyncState
    {
        public List<string> Log { get; set; }
        public string duplicateTPTCustomer { get; set; }
        public bool IsLoggedIn { get; set; }
        public bool IsAuthorized { get; set; }

        public bool IsLocked { get; set; }

        //Murugan:
        public string CustomersLog { get; set; }
        public string QBCustomersLog { get; set; }

        // When "Yes", the process continues, when No, the user is displayed
        // with an interface to select lsit of quickbookCustomers.
        public string SyncCanContinue { get; set; }

    }
}
