﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.PIC
{
    public class ProductRoot
    {
        public bool valid { get; set; }
        public string[] error { get; set; }
        public ProductProperties properties { get; set; }
    }

    public class ProductProperties
    {
        public string Vendor { get; set; }
        public string Name { get; set; }
        public ProductGroup[] Groups { get; set; }
    }

    public class ProductGroup
    {
        public string GGroup { get; set; }
        public string Description { get; set; }
        public VendorProduct[] Products { get; set; }
    }

    public class VendorProduct
    {
        public string Product { get; set; }
        public string Description { get; set; }
    }

    public class sProductRoot
    {
        public bool valid { get; set; }
        public string[] error { get; set; }
        public sProductProperties properties { get; set; }
    }

    public class sProductProperties
    {
        public sProduct Product { get; set; }
    }

    public class sProduct
    {
        public string product { get; set; }
        public string description { get; set; }
        public string type { get; set; }
        public string phaseOutDate { get; set; }
        public string discontinuedDate { get; set; }
        public string TS_product { get; set; }
        public int widthPrompt { get; set; }
        public int heightPrompt { get; set; }
        public int roomPrompt { get; set; }
        public int mountPrompt { get; set; }
        public int fabricPrompt { get; set; }
        public int colorPrompt { get; set; }
    }

    public class ModelProperty
    {
        public string Model { get; set; }
        public string Description { get; set; }
        public string PhaseOutDate { get; set; }
        public string DiscontinuedDate { get; set; }
    }

    public class PICModelJson
    {
        public bool valid { get; set; }
        public List<ModelProperty> properties { get; set; }
    }
}