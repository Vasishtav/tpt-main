﻿using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.PIC
{
    public class VendorRoot
    {
        public bool valid { get; set; }
        public string[] error { get; set; }
        public VendorProperties properties { get; set; }
    }

    public class VendorProperties
    {
        public VendorInfo Vendor { get; set; }
    }

    public class VendorInfo
    {
        public string vendor { get; set; }
        public string name { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string country { get; set; }
        public string aVS { get; set; }
        public string contact { get; set; }
        public string phone { get; set; }
        public string fax { get; set; }
        public string gets1099s { get; set; }
        public string box1099 { get; set; }
        public string dueDays { get; set; }
        public string poDiscount { get; set; }
        public string gl { get; set; }
        public string discountGl { get; set; }
        public string terms { get; set; }
        public string days { get; set; }
        public string other { get; set; }
        public string currency { get; set; }
        public string discount { get; set; }
        public string taxID { get; set; }
        public string remitAddress1 { get; set; }
        public string remitAddress2 { get; set; }
        public string remitCity { get; set; }
        public string remitState { get; set; }
        public string remitZip { get; set; }
        public string remitCountry { get; set; }
        public string profile { get; set; }
        public string addressType { get; set; }
        public string email { get; set; }
        public string oSCEmail { get; set; }
        public string oscContractRequired { get; set; }
        public string oscUseStatus5 { get; set; }
        public string printEmail { get; set; }
        public string remitName { get; set; }
        public string accountNo { get; set; }
        public string vendorServiceProviderCanShipOrder { get; set; }
        public string gXml { get; set; }
        public string supplier { get; set; }
        public string suspended { get; set; }
        public string defaultShipVia { get; set; }
    }

    public class PICHFCVendorData
    {
        public HFCVendor h { get; set; }
        public VendorRoot v { get; set; }
    }


    public class AllVendorRoot
    {
        public bool valid { get; set; }
        public string[] error { get; set; }
        public VendorProperty[] properties { get; set; }
    }

    public class VendorProperty
    {
        public string vendor { get; set; }
        public string name { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string country { get; set; }
        public string contact { get; set; }
        public string phone { get; set; }
        public string fax { get; set; }
    }


}
