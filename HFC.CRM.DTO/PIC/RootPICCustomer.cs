﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.PIC
{
    public class RootPICCustomer
    {
        public PICCustomer Customer { get; set; }
        public bool valid { get; set; }
        public string message { get; set; }
    }

    public class PICCustomer
    {
        public string Customer { get; set; }
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string AccountType { get; set; }
        public string Contact { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Territory { get; set; }
        public string Site { get; set; }
        public string BillName { get; set; }
        public string InvAttn { get; set; }
        public string BillAddress1 { get; set; }
        public string BillAddress2 { get; set; }
        public string BillAddress3 { get; set; }
        public string BillAddress4 { get; set; }
        public string BillCity { get; set; }
        public string BillState { get; set; }
        public string BillZip { get; set; }
        public string BillCountry { get; set; }
        public string BillFax { get; set; }
        public string BillPhone { get; set; }
        public string CellPhone { get; set; }
        public string DateOpened { get; set; }
        public string Source { get; set; }
        public string AddressType { get; set; }
        public int Constant1 { get; set; }
        public int Constant2 { get; set; }
        public int Constant3 { get; set; }
        public int Constant4 { get; set; }
        public int Constant5 { get; set; }
        public int Constant6 { get; set; }
        public int Constant7 { get; set; }
        public int Constant8 { get; set; }
        public int Constant9 { get; set; }
        public int Constant10 { get; set; }
        public int Constant11 { get; set; }
        public int Constant12 { get; set; }
        public int Constant13 { get; set; }
        public int Constant14 { get; set; }
        public int Constant15 { get; set; }
        public int Constant16 { get; set; }
        public int Constant17 { get; set; }
        public int Constant18 { get; set; }
        public int Constant19 { get; set; }
        public int Constant20 { get; set; }
        public string UserField1 { get; set; }
        public string UserField2 { get; set; }
        public string UserField3 { get; set; }
        public string UserField4 { get; set; }
        public string UserField5 { get; set; }
        public string UserField6 { get; set; }
        public string UserField7 { get; set; }
        public string UserField8 { get; set; }
        public string UserField9 { get; set; }
        public string UserField10 { get; set; }
        public string UserField11 { get; set; }
        public string UserField12 { get; set; }
        public string UserField13 { get; set; }
        public string UserField14 { get; set; }
        public string UserField15 { get; set; }
        public string UserField16 { get; set; }
        public string UserField17 { get; set; }
        public string UserField18 { get; set; }
        public string UserField19 { get; set; }
        public string UserField20 { get; set; }
        public string CountryCode { get; set; }
        public string ShipEmail { get; set; }
        public string OrderEmail { get; set; }
        public string DeActivate { get; set; }
        public string InternalXMLID { get; set; }
        public int Salesperson1 { get; set; }
        public int Salesperson2 { get; set; }
        public int Salesperson3 { get; set; }
        public int Salesperson4 { get; set; }
        public int Salesperson5 { get; set; }
    }
}
