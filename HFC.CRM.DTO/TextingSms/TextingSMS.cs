﻿using Dapper;
using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Calandar
{
    public class TextingSMS
    {
        public int CalendarId { get; set; }
        public Nullable<int> LeadId { get; set; }
        public Nullable<int> AccountId { get; set; }
        public Nullable<int> OrderId { get; set; }
        public Nullable<int> OpportunityId { get; set; }
        public virtual ICollection<EventToPerson> Attendees { get; set; }
        public DateTime StartDate { get; set; }
        public int FranchiseId { get; set; }
        public string AppointmentType { get; set; }
        public bool IsAllDay { get; set; }
    }

    public class NotifyValue
    {
        public bool IsNotifyemails { get; set; }
        public bool IsNotifyText { get; set; }
    }
}
