﻿using Dapper;
using HFC.CRM.DTO.Person;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Opportunity
{
    public class OpportunityInfoDTO
    {
        public int OpportunityId { get; set; }
        public int OpportunityNumber { get; set; }
        public int CreatedOnUtc { get; set; }
        public int OpportunityStatusId { get; set; }
        public int AddressCount { get; set; }
        public int PrimePersonId { get; set; }
        public int POCount { get; set; }
        public int NotesCount { get; set; }
        public string OpportunitySourceIdsStr { get; set; }
        public List<int> OpportunitySourceIds
        {
            get
            {
                return OpportunitySourceIdsStr.Split(',').Select(int.Parse).ToList();
            }
        }

        public string PrimaryNotes { get; set; }
        public PersonDTO PrimaryPerson { get; set; }
    }

    public class MoveOpportunityDTO
    {
        public int OpportunityId { get; set; }
        public string OpportunityName { get; set; }
        public string SideMark { get; set; }
        public short OpportunityStatusId { get; set; }
        public Nullable<int> SalesAgentId { get; set; }
        public Nullable<int> InstallationAddressId { get; set; }
        public Nullable<int> BillingAddressId { get; set; }
        public string Description { get; set; }
        public int AccountId { get; set; }
        public int SourcesTPId { get; set; }
        public int QuoteKey { get; set; }
        [NotMapped]
        public bool SendMsg { get; set; }
        [NotMapped]
        public bool UncheckNotifyviaText { get; set; }
    }
}
