﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HFC.CRM.Data;

namespace HFC.CRM.DTO.ProcurementDashboard
{
    public class ProcurementDashboardDTO
    {
        public int PurchaseOrderId { get; set; }
        public int MasterPONumber { get; set; }
        public int PurchaseOrdersDetailId { get; set; }
        public int PICPO { get; set; }
        public string VendorName { get; set; }
        public int VendorId { get; set; }
        public string OpportunityName { get; set; }
        public int OpportunityId { get; set; }
        public string AccountName { get; set; }
        public int AccountId { get; set; }
        public string OrderNumber { get; set; }
        public int OrderID { get; set; }
        public int ProductTypeId { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime MPODate { get; set; }
        public DateTime VPODate { get; set; }
        public DateTime? PromiseByDate { get; set; }
        public DateTime? EstShipDate { get; set; }
        public DateTime? ShippedDate { get; set; }
        public string VPOStatus { get; set; }
        public int StatusId { get; set; }
        public List<HFC.CRM.Data.ShipNotice> ListShipment { get; set; }
        public List<HFC.CRM.Data.VendorInvoice> ListVendorInvoice { get; set; }
        public string FranchiseName { get; set; }
        public decimal CoreProductMargin { get; set; }
        public decimal CoreProductMarginPercent { get; set; }
        public string Errors { get; set; }
        public decimal MPOCost { get; set; }
        public decimal MPOValue { get; set; }
        public DateTime? ContractedDate { get; set; }
        public int Orderlines { get; set; }
        public DateTime? SubmittedDate { get; set; }
        public decimal? OrderSubtotal { get; set; }
        public decimal? Cost { get; set; }
        public string TotalQuantity { get; set; }
        public string Territory { get; set; }
        public int SalesAgentId { get; set; }
        public int InstallerId { get; set; }
        public bool IsXMpo { get; set; }
    }
}
