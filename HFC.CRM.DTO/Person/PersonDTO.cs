﻿using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Person
{
    public class PersonDTO
    {
        public int PersonId { get; set; }
        public string FullName { get; set; }
        public string CompanyName { get; set; }
        public string Title { get; set; }
        public string HomePhone { get; set; }
        public string CellPhone { get; set; }
        public string FaxPhone { get; set; }
        public string WorkPhone { get; set; }
        public string PrimaryEmail { get; set; }
        public string SecondaryEmail { get; set; }
        public string PreferredTFN { get; set; }
    }

    public class UserCollectionDTO
    {
        public Guid UserId { get; set; }
        public string FullName { get; set; }
        public int PersonId { get; set; }
        public int ColorId { get; set; }
        public string Email { get; set; }
        public string AvatarSrc { get; set; }
        public string EmailSignature { get; set; }
        public bool InSales { get; set; }
        public bool InInstall { get; set; }
        public bool? IsDisabled { get; set; }
        public string SelectColor { get; set; }
        public string color { get; set; }
        public string FirstName { get; set; }
        public string Domain { get; set; }
        public ICollection<Role> Roles { get; set; }
    }


    public class UserInfoDTO
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public bool IsDisabled { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PrimaryEmail { get; set; }
    }


}
