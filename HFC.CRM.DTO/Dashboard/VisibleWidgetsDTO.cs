﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Dashboard
{
   public class VisibleWidgetsDTO
    {
       public bool IsVisible { get; set; }
       public string Name { get; set; }
    }
}
