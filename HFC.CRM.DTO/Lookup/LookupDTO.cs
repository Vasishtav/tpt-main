﻿using System.Collections.Generic;

namespace HFC.CRM.DTO.Lookup
{
    public class LookupDTO<T>
    {
        public IEnumerable<T> Added { get; set; }
        public IEnumerable<T> Updated { get; set; }
        public long Version { get; set; }
        public long Took { get; set; }
    }

    public class LookupBaseDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class MyCRMMapData : LookupBaseDTO
    {
        public int? TPTSourceID { get; set; }
        public string TPTSourceName { get; set; }
    }

    public class QBOTaxData
    {
        public string Jurisdiction { get; set; }
        public string TaxName { get; set; }
        public decimal Amount { get; set; }
        public decimal Rate { get; set; }
    }

    public class QBOTaxRate : LookupBaseDTO
    {
        public string Value { get; set; }
    }

    public class QBOCOA : LookupBaseDTO
    {
        public string accountType { get; set; }
        public string accountSubType { get; set; }
    }

    public class MonthValues
    {
        public int Id { get; set; }
        public string MonthName { get; set; }
    }
}