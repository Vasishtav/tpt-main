﻿using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO
{
    public class RecurringDetailsTPConnect
    {
        public int RecurringEventId { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string EndsOn { get; set; }
        public int? EndsAfterXOccurrences { get; set; }
        public string RecursEvery { get; set; }
    }

    //public class EventLookUp
    //{
    //    public List<AccountPickListTPConnect> Account { get; set; }
    //    public List<OpportunityPickListTPConnect> Opportunity { get; set; }
    //    public List<OrderPickListTPConnect> Order { get; set; }
    //}

    //public class EventLeadLookUp
    //{
    //    public List<PickListTPConnect> Lead { get; set; }
    //}

    //public class EventAttendeesLookUp
    //{
    //    public List<PickListTPConnect> Attendees { get; set; }
    //}

    //public class EventAppointmentTypeLookUp
    //{
    //    public List<PickListTPConnect> AppointmentType { get; set; }
    //}

    //public class PickListTPConnect
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}

    //public class LeadPickListTPConnect
    //{
    //    public int LeadId { get; set; }
    //    public string Name { get; set; }
    //}

    //public class AccountPickListTPConnect
    //{
    //    public int AccountId { get; set; }
    //    public string Name { get; set; }
    //}

    //public class OpportunityPickListTPConnect
    //{
    //    public int AccountId { get; set; }
    //    public int OpportunityId { get; set; }
    //    public string Name { get; set; }
    //}

    //public class OrderPickListTPConnect
    //{
    //    public int AccountId { get; set; }
    //    public int OpportunityId { get; set; }
    //    public int OrderID { get; set; }
    //    public string Name { get; set; }
    //}

    public class LookUpFilter
    {
        public int AccountId { get; set; }
        public int OpportunityId { get; set; }
        public int OrderId { get; set; }
    }

    //public class EventLookUpAll
    //{
    //    public List<PickListTPConnect> leads { get; set; }
    //    public List<AccountPickListTPConnect> accounts { get; set; }
    //    public List<OpportunityPickListTPConnect> opportunities { get; set; }
    //    public List<OrderPickListTPConnect> orders { get; set; }
    //    public List<PickListTPConnect> types { get; set; }
    //    public List<PickListTPConnect> attendees { get; set; }
    //}
}
