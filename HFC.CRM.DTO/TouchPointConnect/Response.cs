﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO
{
    public class Response
    {
        public bool status;
        public string message;
        public object content;
        public Response(bool status, string message, object content)
        {
            this.status = status;
            this.message = message;
            this.content = content;
        }
        public Response(bool status, string message)
        {
            this.status = status;
            this.message = message;
            this.content = null;
        }
    }

    public class Response<T>
    {
        public bool status;
        public string message;
        public T content;
        public Response(bool status, string message, T content)
        {
            this.status = status;
            this.message = message;
            this.content = content;
        }
        public Response(bool status, string message)
        {
            this.status = status;
            this.message = message;
        }
    }
}

