﻿using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO
{
    // For List API
    public class EventListDataTPConnectModel
    {
        public int CalendarId { get; set; }
        public System.DateTimeOffset StartDate { get; set; }
        public System.DateTimeOffset EndDate { get; set; }
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        public string Location { get; set; }
        public string Subject { get; set; }
        public AppointmentTypeEnumTP AptTypeEnum { get; set; }
        public string Type { get { return AptTypeEnum.GetDisplayName(); } }
        public bool IsAllDay { get; set; }
    }

    // For Details API
    public class EventDetailsTPConnectModel
    {
        public int CalendarId { get; set; }
        public string Subject { get; set; }
        public string Location { get; set; }
        public string Lead { get; set; }
        public string Account { get; set; }
        public string Opportunity { get; set; }
        public string Order { get; set; }

        public Nullable<int> LeadId { get; set; }
        public Nullable<int> AccountId { get; set; }
        public Nullable<int> OpportunityId { get; set; }
        public Nullable<int> OrderId { get; set; }
        public string AttendeesId { get; set; }

        public AppointmentTypeEnumTP AptTypeEnum { get; set; }
        public string Type { get { return AptTypeEnum.GetDisplayName(); } }
        public string Attendees { get; set; }
        public string Message { get; set; }
        public System.DateTimeOffset StartDate { get; set; }
        public System.DateTimeOffset EndDate { get; set; }
        public int ReminderMinute { get; set; }
        public Nullable<bool> IsPrivate { get; set; }
        public Nullable<bool> Recurring { get; set; }
        public int? RecurringEventId { get; set; }
        public DateTime? CreatedOnUtc { get; set; }
        public DateTime? LastUpdatedUtc { get; set; }
        public string CreatedBy { get; set; }
        public string LastUpdatedBy { get; set; }

        public bool IsAllDay { get; set;}

    }

    // Get Update Event info
    public class UpdateEventTPConnectModel
    {
        public int CalendarId { get; set; }
        public string Subject { get; set; }
        public string Location { get; set; }
        public Nullable<int> LeadId { get; set; }
        public Nullable<int> AccountId { get; set; }
        public Nullable<int> OpportunityId { get; set; }
        public Nullable<int> OrderId { get; set; }
        public string Message { get; set; }
        public AppointmentTypeEnumTP AptTypeEnum { get; set; }
        public string Attendees { get; set; }
        public System.DateTimeOffset StartDate { get; set; }
        public System.DateTimeOffset EndDate { get; set; }
        public int ReminderMinute { get; set; }
        public Nullable<bool> IsPrivate { get; set; }
        public Nullable<int> RecurringEventId { get; set; }
        public Nullable<bool> Recurring { get; set; }
        public DateTime? CreatedOnUtc { get; set; }
        public DateTime? LastUpdatedUtc { get; set; }
    }
}
