﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HFC.CRM.DTO.Notes;

namespace HFC.CRM.DTO.Search
{
    public class OpportunitySeachResultModel
    {
        public string Territory { get; set; }
        public int OpportunityID { get; set; }
        public int AccountID { get; set; }
        public string OpportunityFullName { get; set; }
        public string AccountName { get; set; }
        public string OpportunityStatus { get; set; }
        public decimal OrderAmount { get; set; }
        public DateTime? ContractDate { get; set; }
        public string SalesAgentName { get; set; }

        // TP-1698: CLONE - Opportunities and related need record level permissions
        // Used to filter the records for specific user
        // based on sales agent id and installation user id
        public int OpportunityNumber { get; set; }

        public int SalesAgentId { get; set; }
        public int InstallerId { get; set; }
    }
}