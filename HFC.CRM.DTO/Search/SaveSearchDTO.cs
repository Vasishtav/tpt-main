﻿
namespace HFC.CRM.DTO.Search
{
    public class SaveSearchDTO
    {
        public int SavedSearchID { get; set; }
        public string Name { get; set; }
        public string SearchParams { get; set; }
    }
}
