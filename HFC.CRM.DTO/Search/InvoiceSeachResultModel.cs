﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HFC.CRM.DTO.Notes;

namespace HFC.CRM.DTO.Search
{
   public class InvoiceSeachResultModel
    {

		public int InvoiceId { get; set; }
        public int OpportunityID { get; set; }
        public int OrderID { get; set; }
		public int AccountId { get; set; }
		public string OpportunityName { get; set; }
        public string InvoiceType { get; set; }
		public decimal? TotalAmount { get; set; }
        public decimal? Balance { get; set; }
		public DateTime? InvoiceDate { get; set; }
      
        
    }
       
}
