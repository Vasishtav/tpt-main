﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HFC.CRM.DTO.Notes;

namespace HFC.CRM.DTO.Search
{
    public class OrderSeachResultModel
    {

        public string Territory { get; set; }
        public int OrderId { get; set; }
        public int QuoteID { get; set; }
        public int QuoteKey { get; set; }
        public string SideMark { get; set; }
        public int OrderNumber { get; set; }
        public string OrderName { get; set; }
        public decimal BalanceDue { get; set; }
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        public int OpportunityId { get; set; }
        public string OpportunityName { get; set; }
        public string Status { get; set; }
        public string SalesAgentName { get; set; }
        //public string TotalAmount { get; set; }
        public decimal TotalAmount { get; set; }
        public DateTime ContractDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        // TP-1698: CLONE - Opportunities and related need record level permissions
        // Used to filter the records for specific user
        // based on sales agent id and installation user id
        public int InstallerId { get; set; }
        
        public int SalesAgentId { get; set; }
        public decimal? ReversalAmount { get; set; }
        public int TotalLines { get; set; }
        public int TotalQTY { get; set; }



    }

}
