﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HFC.CRM.DTO.Notes;

namespace HFC.CRM.DTO.Search
{
    public class JobSearchExportDTO
    {

        public int LeadId { get; set; }
        public int JobNumber { get; set; }
        public int CustomerPersonId { get; set; }
        public string CustomerFullName { get; set; }
        public string PreferredTFN { get; set; }
        public string CellPhone { get; set; }
        public string WorkPhone { get; set; }
        public string HomePhone { get; set; }
        public int JobStatusId { get; set; }
        public string JobStatusName { get; set; }
        public int? SourceId { get; set; }
        public int InstallAddressId { get; set; }
        public int BillingAddressId { get; set; }
        public string SalesFullName { get; set; }
        public int? SalesPersonId { get; set; }
        public string PrimaryEmail { get; set; }
        public decimal Subtotal { get; set; }
        public decimal DiscountTotal { get; set; }
        public decimal SurchargeTotal { get; set; }
        public decimal Taxtotal { get; set; }
        public decimal NetProfit { get; set; }
        public decimal NetTotal { get; set; }
        public decimal Balance { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public DateTime? ContractedOnUtc { get; set; }
        public string Manufacturer { get; set; }
        public string ProductName { get; set; }
        public string CategoryName { get; set; }
        public string ProductType { get; set; }
        public short Quantity { get; set; }
        public decimal SalePrice { get; set; }
        public decimal UnitCost { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal JobItemSubtotal { get; set; }

    }
}
