﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Search
{
   public class JobStatusDTO
    {
       public int Id { get; set; }
       public string Name { get; set; }
       public bool IsChiled { get; set; }
    }
}
