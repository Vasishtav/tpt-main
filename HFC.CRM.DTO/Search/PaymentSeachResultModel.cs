﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HFC.CRM.DTO.Notes;

namespace HFC.CRM.DTO.Search
{
   public class PaymentSeachResultModel
    {

		public int PaymentID { get; set; }
        public int OpportunityID { get; set; }
        public int OrderID { get; set; }
		public int AccountId { get; set; }
		public string OpportunityName { get; set; }
        public string Sidemark { get; set; }
        public decimal? Total { get; set; }
        public decimal? BalanceDue { get; set; }
        public bool? PayBalance { get; set; }
        public int? PaymentMethod { get; set; }
        public string Method { get; set; }
        public string VerificationCheck { get; set; }
        public DateTime? PaymentDate { get; set; }
        public decimal? Amount { get; set; }
        public string Memo { get; set; }
        public bool Reversal { get; set; }
        public int OrderStatus { get; set; }


    }
       
}
