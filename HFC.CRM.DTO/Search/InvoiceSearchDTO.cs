﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Search
{
    public class InvoiceSearchDTO
    {
        public int InvoiceId { get; set; }
        public string InvoiceNumber { get; set; }
        public decimal? Balance { get; set; }
        public decimal NetTotal { get; set; }
        public int JobId { get; set; }
        public int LeadId { get; set; }
        public int JobNumber { get; set; }
        public string PrimaryEmail { get; set; }
        public string WorkPhone { get; set; }
        public string HomePhone { get; set; }
        public string CellPhone { get; set; }
        public string FaxPhone { get; set; }
        public string PreferredTFN { get; set; }
        public string WorkPhoneExt { get; set; }
        public DateTimeOffset CreatedOn { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public long? Row { get; set; }
        public int? TotalCount { get; set; }
        public int StatusEnum { get; set; }
        public string FullName {
            get { return string.Format("{0} {1}", FirstName, LastName); }
        }

        public string PreferredTFNHtml
        {
            get
            {
                if (PreferredTFN == "C" && !string.IsNullOrEmpty(CellPhone))
                {
                    return "<abbr title='Cell Phone'>C:</abbr><a href='tel:" + CellPhone + "'><span>" + string.Format("{0:(###) ###-####}", double.Parse(CellPhone)) + "</span></a>";
                }
                else if (PreferredTFN == "W" && !string.IsNullOrEmpty(WorkPhone))
                {
                    return "<abbr title='Work Phone'>W:</abbr><a href='tel:" + WorkPhone + "'><span>" + string.Format("{0:(###) ###-####}", double.Parse(WorkPhone)) + (!string.IsNullOrEmpty(WorkPhoneExt) ? (" x" + WorkPhoneExt) : "") + "</span></a>";
                }
                else if (PreferredTFN == "H" && !string.IsNullOrEmpty(HomePhone))
                {
                    return "<abbr title='Home Phone'>H:</abbr><a href='tel:" + HomePhone + "'><span>" + string.Format("{0:(###) ###-####}", double.Parse(HomePhone)) + "</span></a>";
                }
                else if (PreferredTFN == "F" && !string.IsNullOrEmpty(FaxPhone))
                {
                    return "<abbr title='Fax Phone'>F:</abbr><a href='tel:" + FaxPhone + "'><span>" + string.Format("{0:(###) ###-####}", double.Parse(FaxPhone)) + "</span></a>";
                }
                else
                {
                    return string.Empty;
                }
            }
        }
    }

}
