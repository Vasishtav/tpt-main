﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HFC.CRM.DTO.Notes;

namespace HFC.CRM.DTO.Search
{
  public  class LeadJobSearchInfoDTO
    {
        public int JobId { get; set; }
        public int LeadId { get; set; }
        public int JobNumber { get; set; }
        public int LeadNumber { get; set; }
        public int InstallAddressId { get; set; }
        public string InstallAddress { get; set; }
        public decimal NetTotal { get; set; }
     
        public int JobStatusId { get; set; }
        public int? SalesPersonId { get; set; }
        public List<JobNoteDTO> JobNotes { get; set; }
    }
}

