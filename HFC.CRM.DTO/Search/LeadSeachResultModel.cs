﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HFC.CRM.DTO.Notes;

namespace HFC.CRM.DTO.Search
{
   public class LeadSeachResultModel
    {
        public int LeadId { get; set; }
        public Guid LeadGuid { get; set; }
        public int? PersonId { get; set; }
        public int LeadNumber { get; set; }
        public DateTime? LastUpdatedOnUtc { get; set; }
        public string Notes { get; set; }
        public byte iscommercial { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public short LeadStatusId { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public Guid AddressesGuid { get; set; }
        public int AddressId { get; set; }
        public int? SecPersonId { get; set; }
        public string WorkPhone { get; set; }
        public string HomePhone { get; set; }
        public string FaxPhone { get; set; }
        public string Channel { get; set; }
        public string Status { get; set; }
        public string Campaign { get; set; }
        public string source { get; set; }
        public string CellPhone {get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PrimaryEmail { get; set; }
        public string PreferredTFN { get; set; }
        public string WorkPhoneExt { get; set; }
        public string CompanyName { get; set; }
        public long? Row { get; set; }
        public int? TotalCount { get; set; }
        public List<LeadNoteDTO> LeadNotes { get; set; }
        public bool CanUpdateStatus {
            get { return true; }
        }
        public string LeadFullName { get; set; }
        public string Territory { get; set; }
        public string DisplayPhone { get; set; }
        public string PreferredTFNHtml
        {
            get
            {
                if (PreferredTFN == "C" && !string.IsNullOrEmpty(CellPhone))
                {
                    return "<abbr title='Cell Phone'>C:</abbr><a href='tel:" + CellPhone + "'><span>" + string.Format("{0:(###) ###-####}", double.Parse(CellPhone)) + "</span></a>";
                }
                else if (PreferredTFN == "W" && !string.IsNullOrEmpty(WorkPhone))
                {
                    return "<abbr title='Work Phone'>W:</abbr><a href='tel:" + WorkPhone + "'><span>" + string.Format("{0:(###) ###-####}", double.Parse(WorkPhone)) + (!string.IsNullOrEmpty(WorkPhoneExt) ? (" x" + WorkPhoneExt) : "") + "</span></a>";
                }
                else if (PreferredTFN == "H" && !string.IsNullOrEmpty(HomePhone))
                {
                    return "<abbr title='Home Phone'>H:</abbr><a href='tel:" + HomePhone + "'><span>" + string.Format("{0:(###) ###-####}", double.Parse(HomePhone)) + "</span></a>";
                }
                else if (PreferredTFN == "F" && !string.IsNullOrEmpty(FaxPhone))
                {
                    return "<abbr title='Fax Phone'>F:</abbr><a href='tel:" + FaxPhone + "'><span>" + string.Format("{0:(###) ###-####}", double.Parse(FaxPhone)) + "</span></a>";
                }
                else
                {
                    return string.Empty;
                }
            }
        }
    }

    public class LeadSeachResultNewModel
    {
        public int LeadId { get; set; }
        public byte iscommercial { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Channel { get; set; }
        public string Status { get; set; }
        public string Campaign { get; set; }
        public string source { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PrimaryEmail { get; set; }
        public string CompanyName { get; set; }
        public string LeadFullName { get; set; }
        public string Territory { get; set; }
        public string DisplayPhone { get; set; }
    }
}
