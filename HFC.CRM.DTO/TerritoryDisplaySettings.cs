﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO
{
    public class TerritoryDisplay
    {
        public int? Id { get; set; }
        public bool UseFranchiseName { get; set; }
        public string Displayname { get; set; }
        public string WebSiteURL { get; set; }
        public string Phone { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string LicenseNumber { get; set; }
        public string PrimaryEmail { get; set; }

    }
}
