﻿using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTOs
{
    public class ChannelDTO
    {
        public int ChannelId { get; set; }
        public string Name { get; set; }
        public int TotalCampaign { get; set; }
        public decimal MonthlyCost { get; set; }
        public List<Campaign> Campaigns { get; set; }
    }

    public enum RecurringType
    {
        Invalid = 0,
        OneTime = 1,
        Monthly = 2,
        Quarterly = 3,
        Yearly = 4
    }
}
