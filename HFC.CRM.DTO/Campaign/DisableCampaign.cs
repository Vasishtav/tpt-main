﻿using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTOs
{
    public class DisableCampaign
    {
        public int CampaignId { get; set; }
        public DateTime EndDate { get; set; }
        public Nullable<int> FranchiseId { get; set; }
        public TimeZoneEnum TimezoneCode { get; set; }
        public bool DeactivateOnEndDate { get; set; }

    }
}
