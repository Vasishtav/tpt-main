﻿using Dapper;
using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Vendors
{
    [Table("Acct.HFCVendors")]
    public class AdminAllianceModel: BaseEntity
    {
        [Key]
        public int VendorIdPk { get; set; }
        public int VendorId { get; set; }
        public int VendorType { get; set; }
        public string Name { get; set; }
        public string AccountRep { get; set; }
        public string AccountRepPhone { get; set; }
        public string AccountRepEmail { get; set; }
        public string OrderingMethod { get; set; }
        public string ContactDetails { get; set; }
        public int? AddressId { get; set; }
        public bool? IsActive { get; set; }
        //public DateTime CreatedOn { get; set; }
        //public  int CreatedBy { get; set; }
        //public DateTime? LastUpdatedOn { get; set; }
        //public int? LastUpdatedBy { get; set; }
        public string PICVendorId { get; set; }
        public bool? IsAlliance { get; set; }
        public bool? BudgetBlind { get; set; }
        public bool? TailoredLiving { get; set; }
        public bool? ConcreteCraft { get; set; }
        public bool? Beta { get; set; }
        [NotMapped]
        public int Status { get; set; }
        [NotMapped]
        public string StatusValue { get; set; }
        [NotMapped]
        public string VendorTypeName { get; set; }
        public bool USCurrency { get; set; }
        public bool CANCurrency { get; set; }

    }
}
