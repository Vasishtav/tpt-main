﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Vendors
{
   public class VendorCurrencyModel
    {
        public int CountryCodeId { get; set; }
        public string ISOCode2Digits { get; set; }
        public string Country { get; set; }
        public string CurrencyCode { get; set; }
    }
}
