﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Vendors
{
    public class VendorListModel
    {
        public int VendorIdPk { get; set; }
        public int VendorId { get; set; }
        public string Name { get; set; }
        public int VendorTypeId { get; set; }
        public string AccountRep { get; set; }
        public string VendorType { get; set; }
        public string AccountRepPhone { get; set; }
        public string Location { get; set; }
        public string VendorStatus { get; set; }
        public string OrderingMethod { get; set; }
        public string OrderingMethodName { get; set; }
        public Boolean IsAlliance{get;set;}
        public bool? BudgetBlind { get; set; }
        public bool? TailoredLiving { get; set; }
        public bool? ConcreteCraft { get; set; }
        public bool? Beta { get; set; }


    }
}
