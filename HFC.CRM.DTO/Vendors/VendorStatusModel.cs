﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Vendors
{
    public class VendorStatusModel
    {
        public int Id { get; set; }
        public string VendorStatus { get; set; }
        public bool IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTimeOffset CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTimeOffset? LastUpdatedOn { get; set; }
        public int? LastUpdatedBy { get; set; }
    }
}
