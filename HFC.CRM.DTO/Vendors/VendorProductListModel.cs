﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Vendors
{
    public class VendorProductListModel
    {
        [Key]
        public int ProductKey { get; set; }
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public string ProductCategory { get; set; }
        public string Description { get; set; }
        public int FranchiseId { get; set; }
        public int VendorId { get; set; }
        public string VendorName { get; set; }
        public string ProductSubCategory { get; set; }
        public string ProductStatus { get; set; }
    }

    public class VendorPricingDropDown
    {
        public int VendorId { get; set; }
        public string VendorName { get; set; }
        public string RetailBasis { get; set; }
    }
}
