﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Vendors
{
    public class AdminAllianceStatusModel
    {

        public int Id { get; set; }

        public string VendorStatus { get; set; }

    }



    public class VendorCustomer
    {
        public bool valid { get; set; }
        public object[] error { get; set; }
        public List<VendorCustomerProps> properties { get; set; }
    }

    public class VendorCustomerProps
    {
        public string Vendor { get; set; }
        public string[] VendorCustomer { get; set; }
    }


}
