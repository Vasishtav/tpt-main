﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Calandar
{
   public class CalendarToSynch
    {
       public System.Guid UserID { get; set; }
       public int CalendarId { get; set; }
       public int EventToPeople_HistoryID { get; set; }
    }
   //public class AssignedChangedUsers
   //{
   //    public System.Guid UserID { get; set; }
   //    public int CalendarId { get; set; }
   //}
}
