﻿using Dapper;
using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Calandar
{
   
    public class DashboardVM
    {
        public string Subject { get; set; }
        public string LeadName { get; set; }
        public string AccountName { get; set; }
        public Nullable<int> LeadId { get; set; }
        public Nullable<int> AccountId { get; set; }
        public Nullable<int> OrderId { get; set; }
        public Nullable<int> OpportunityId { get; set; }
        public bool IsDeleted { get; set; }
        public bool? IsPrivate { get; set; }
        public bool? IsPrivateAccess { get; set; }
        public string AttendeesNames { get; set; }
        public string Message { get; set; }
        public int LeadStatusId { get; set; }
        public int InstallationAddressId { get; set; }
        public string OppurtunityName { get; set; }
        public string OrderName { get; set; }
        public string Related { get; set; }

    }

    public class AppointmentVM: DashboardVM
    {
        public int CalendarId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string AppointmentType { get; set; }
        public DateTimeOffset StartDateUTC { get; set; }
        public DateTimeOffset EndDateUTC { get; set; }
        public bool IsAllDay { get; set; }
    }

    public class TaskVM : DashboardVM
    {
        public int TaskId { get; set; }
        public DateTime DueDate { get; set; }
        public DateTimeOffset? completed { get; set; }

        //public DateTimeOffset DueDateUTC { get; set; }
    }
}
