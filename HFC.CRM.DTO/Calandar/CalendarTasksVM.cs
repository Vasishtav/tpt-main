﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Calandar
{
    public class CalendarTasksVM
    {
       
        public int TaskId { get; set; }
        public Guid TaskGuid { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public DateTime DueDate { get; set; }
        public DateTime CompletedDate { get; set; }
        public int? LeadId { get; set; }
        public int? OrganizerPersonId { get; set; }
        public string OrganizerEmail { get; set; }
        public int CreatedByPersonId { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? LastUpdatedOnUtc { get; set; }
        public int? LastUpdatedPersonId { get; set; }
        public int? PriorityOrder { get; set; }
        public int? LeadNumber { get; set; }
        public int FranchiseId { get; set; }
        public bool? IsPrivate { get; set; }
        public short ReminderMinute { get; set; }
        public byte? RemindMethodEnum { get; set; }
        public DateTime FirstRemindDate { get; set; }
        public int RevisionSequence { get; set; }
        public int? JobId { get; set; }
        public int? JobNumber { get; set; }
        public int? AssignedPersonId { get; set; }
        public string AssignedName { get; set; }
        public string PhoneNumber { get; set; }
        public int? AccountId { get; set; }
        public int? OpportunityId { get; set; }
        public int? OrderId { get; set; }
        [NotMapped]
        public string AttendeesName { get; set; }

        [NotMapped]
        public bool? IsPrivateAccess { get; set; }
    }
}
