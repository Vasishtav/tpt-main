﻿using Dapper;
using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Calandar
{
    public class CalendarVM
    {
        public CalendarVM()
        {
            this.Attendees = new HashSet<EventToPerson>();
            this.CalendarSyncs = new HashSet<CalendarSync>();
            this.ModifiedOccurrences = new HashSet<CalendarSingleOccurrence>();
            this.EventToPeople_History = new HashSet<EventToPeople_History>();
        }

        
        public int CalendarId { get; set; }
        [NotMapped]
        public int EventPersonId { get; set; }
        [NotMapped]
        public int PersonId { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        [NotMapped]
        public bool AddRecurrence { get; set; }

        [NotMapped]
        public bool? IsPrivateAccess { get; set; }
        
        public string AccountName { get; set; }
        public string AccountPhone { get; set; }
        public string OpportunityName { get; set; }
        public Nullable<int> LeadId { get; set; }
        public Nullable<int> AccountId { get; set; }
        public Nullable<int> OrderId { get; set; }
        public Nullable<int> OpportunityId { get; set; }
        public Nullable<int> CaseId { get; set; }
        public Nullable<int> VendorCaseId { get; set; }
        
        public Nullable<int> CreatedByPersonId { get; set; }
        private System.DateTime _CreatedOnUtc = DateTime.UtcNow;
        public System.DateTime CreatedOnUtc
        {
            get { return _CreatedOnUtc; }
            set { _CreatedOnUtc = DateTime.SpecifyKind(value, DateTimeKind.Unspecified); }
        }
        public bool IsAllDay { get; set; }
        private System.DateTime _LastUpdatedUtc = DateTime.UtcNow;
        public System.DateTime LastUpdatedUtc
        {
            get { return _LastUpdatedUtc; }
            set { _LastUpdatedUtc = DateTime.SpecifyKind(value, DateTimeKind.Unspecified); }
        }
        public bool IsDeleted { get; set; }
        public Nullable<int> LastUpdatedByPersonId { get; set; }
        public Nullable<int> LeadNumber { get; set; }
        public System.Guid CalendarGuid { get; set; }
        public int FranchiseId { get; set; }
        public Nullable<int> RecurringEventId { get; set; }
        public Nullable<bool> IsPrivate { get; set; }

      
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public AppointmentTypeEnumTP AptTypeEnum { get; set; }
        public string AppointmentType { get; set; }
        public string Location { get; set; }
        public Nullable<int> OrganizerPersonId { get; set; }
        public string OrganizerEmail { get; set; }
        public Nullable<bool> IsCancelled { get; set; }
        public short ReminderMinute { get; set; }
        public Nullable<RemindMethodEnum> RemindMethodEnum { get; set; }
        public string OrganizerName { get; set; }
        public Nullable<System.DateTimeOffset> FirstRemindDate { get; set; }
        public EventTypeEnum EventTypeEnum { get; set; }
        public int RevisionSequence { get; set; }
        public Nullable<int> JobId { get; set; }
        public Nullable<int> JobNumber { get; set; }
        public Nullable<int> AssignedPersonId { get; set; }
        public string AssignedName { get; set; }
        public string PhoneNumber { get; set; }

        [NotMapped]
        public virtual HFC.CRM.Data.Person CreatedByPerson { get; set; }
        [NotMapped]
        public virtual Franchise Franchise { get; set; }
        [NotMapped]
        public virtual ICollection<EventToPerson> Attendees { get; set; }
        [NotMapped]
        public virtual EventRecurring EventRecurring { get; set; }
        [NotMapped]
        public virtual HFC.CRM.Data.Person Organizer { get; set; }
        [NotMapped]
        public virtual ICollection<CalendarSync> CalendarSyncs { get; set; }
        [NotMapped]
        public virtual ICollection<CalendarSingleOccurrence> ModifiedOccurrences { get; set; }
        [NotMapped]
        public virtual  HFC.CRM.Data.Job Job { get; set; }
        [NotMapped]
        public virtual HFC.CRM.Data.Lead Lead { get; set; }
        [NotMapped]
        public virtual HFC.CRM.Data.Person Assigned { get; set; }
        [NotMapped]
        public virtual ICollection<EventToPeople_History> EventToPeople_History { get; set; }
        [NotMapped]
        public string RRRule { get; set; }
        [NotMapped]
        public string AttendeesName { get; set; }

        [NotMapped]
        public bool IsEditable { get; set; }
       
        [NotMapped]
        public bool IsDeletable { get; set; }
       
        [NotMapped]
        public string ColorClassName { get; set; }

        [NotMapped]
        public string RecurrenceException { get; set; }

        public override bool Equals(object obj)
        {
            if (obj is Calendar)
                return CalendarGuid.Equals(((Calendar)obj).CalendarGuid);
            else
                return false;
        }

       
        public override int GetHashCode()
        {
            return CalendarGuid.GetHashCode();
        }
        
    }
}
