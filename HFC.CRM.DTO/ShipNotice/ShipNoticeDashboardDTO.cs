﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HFC.CRM.Data;


namespace HFC.CRM.DTO.ShipNotice
{
    public class ShipNoticeDTO
    {
        public int PurchaseOrderId { get; set; }
        public int OrderID { get; set; }
        public int VendorId { get; set; }
        public string FullVendorName { get; set; }
        public string SideMark { get; set; }
        public int TotalQuantityShipped { get; set; }
        public string VPO { get; set; }
        public int MasterPONumber { get; set; }
        public int OrderNumber { get; set; }
        public string ShipmentId { get; set; }
        public int ShipNoticeId { get; set; }
        public DateTime ShippedDate { get; set; }
        public int POQuantity { get; set; }
        public bool ReceivedComplete { get; set; }
        public DateTime? ReceivedCompleteDate { get; set; }
        public string Status { get; set; }
        public int POLines { get; set; }
        public int StatusId { get; set; }
        public int QuantityReceived { get; set; }
        public int InstallerId { get; set; }
        public int SalesAgentId { get; set; }
    }
}
