﻿using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.PurchaseOrder
{

    public class XMpoDTO
    {
        public DateTime OrderDate { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? SubmittedDate { get; set; }
        public int VPONumber { get; set; }
        public string Status { get; set; }
        public int VendorPromoId { get; set; }
        public string VendorPromoText { get; set; }
        public string ShipToAccount { get; set; }
        public string ShipVia { get; set; }
        public string  ShipToAddress { get; set; }
        public string ShipToAddressId { get; set; }
        public int TotalQuantity { get; set; }
        public decimal TotalCost { get; set; }
        public int CancelReasonId { get; set; }
        public string CancelReasonText { get; set; }
        public string VendorName { get; set; }
        public int VendorId { get; set; }
        public int? PurchaseOrderNumber { get; set; }
        public int? VendorOrderNumber { get; set; }
        public int? InvoiceNumber { get; set; }
        public int? ShipmentNumber { get; set; }
        public IList<XMpoLineDetailDTO> LineItems { get; set; }
        public bool SubmittedTOPIC { get; set; }
        public int? ShipAddressId { get; set; }
        public bool IsValidated { get; set; }
        public string Address { get; set; }

        public int POStatusId { get; set; }

        public AddressTP DropShipAddress { get; set; }
        public List<ShipToLocations> ShipToLocationList { get; set; }
        public bool DropShip { get; set; }
        public int PurchaseOrderId { get; set; }
        public int MasterPONumber { get; set; }
    }

    public class XMpoLineDetailDTO
    {
        public int LineNumber { get; set; }
        public int QuoteLineId { get; set; }
        public string ProductName { get; set; }
        public string ProductNumber { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
        public decimal Cost { get; set; }
        public DateTime? PromiseByDate { get; set; }
        public DateTime? EstimatedShipDate { get; set; }
        public string Status { get; set; }
        public string NotesVendor { get; set; }
        public int PurchaseOrdersDetailId { get; set; }
        public string Errors { get; set; }
        public int PurchaseOrderId { get; set; }
        public int QuoteKey { get; set; }
        public int OpportunityId { get; set; }
        public int OrderId { get; set; }
        public int OrderNumber { get; set; }
        public string WindowName { get; set; }
    }
}
