﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Files
{
    public class FileDTO
    {
        public int PhysicalFileId { get; set; }
        public System.Guid PhysicalFileGuid { get; set; }
        public string FileName { get; set; }
        public string MimeType { get; set; }
        public string FileDescription { get; set; }
        public string ThumbUrl { get; set; }
        public string UrlPath { get; set; }
        public bool IsRelativeUrlPath { get; set; }
        private System.DateTime _CreatedOnUtc = DateTime.Now;
        public System.DateTime CreatedOnUtc
        {
            get { return _CreatedOnUtc; }
            set { _CreatedOnUtc = DateTime.SpecifyKind(value, DateTimeKind.Unspecified); }
        }
        public Nullable<int> AddedByPersonId { get; set; }
        public Nullable<int> FileSize { get; set; }
        private Nullable<System.DateTime> _LastUpdatedUtc;
        public Nullable<System.DateTime> LastUpdatedUtc
        {
            get { return _LastUpdatedUtc; }
            set
            {
                if (value == null)
                    _LastUpdatedUtc = null;
                else
                    _LastUpdatedUtc = DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified);
            }
        }
        public Nullable<int> LastUpdatedByPersonId { get; set; }
        public Nullable<short> Width { get; set; }
        public Nullable<short> Height { get; set; }
        public string FileCategory { get; set; }
    }
}
