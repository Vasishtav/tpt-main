﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Permission
{
    public class RoleListDTO
    {
        public string RoleId { get; set; }
        public string RoleName { get; set; }
    }
}
