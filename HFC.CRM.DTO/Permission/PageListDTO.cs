﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Permission
{
    public class PageListDTO
    {
        public int PermissionTypeId { get; set; }
        public string Name { get; set; }
    }
}
