﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HFC.CRM.Cache.Cache;

namespace HFC.CRM.DTO.Source
{
    public class SourceDTO : ICacheable
    {
        public int SourceId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? ParentId { get; set; }
        public int? FranchiseId { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset? EndDate { get; set; }
        public decimal? Amount { get; set; }
        public DateTimeOffset CreatedOn { get; set; }
        public bool IsActive { get; set; }
        public string Memo { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsCampaign { get; set; }

        public List<SourceDTO> Children { get; set; }
        public int LeadSourcesCount { get; set; }

        public string DateRange
        {
            get
            {
                //Wed, Oct 19 to Sat, Oct 22
                return StartDate.Date.ToString("ddd, MMM dd") + " to " +
                       (EndDate.HasValue ? EndDate.Value.Date.ToString("ddd, MMM dd") : "Infinity");
            }

        }
    }

    public class MyCRMSourceMap
    {
        public int MyCRMSourceID { get; set; }
        public int TPTSourceID { get; set; }
    }
}
