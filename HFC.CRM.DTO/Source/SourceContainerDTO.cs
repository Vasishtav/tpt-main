﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Source
{
    public class SourceContainerDTO
    {
        public SourceDTO SourceParent { get; set; }
        public string Text {
            get { return SourceParent.Name; }
        }
        public int Id
        {
            get { return SourceParent.SourceId; }
        }

        public int? ParentId
        {
            get { return SourceParent.ParentId; }
        }

        public List<SourceDTO> Items { get; set; }
    }
}
