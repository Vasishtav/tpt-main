﻿using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Notes
{
    public class LeadNoteDTO
    {
        public int NoteId { get; set; }
        public int LeadId { get; set; }
        public string Message { get; set; }
        public Nullable<int> CreatedByPersonId { get; set; }
        public NoteTypeEnum TypeEnum { get; set; }
        public System.DateTimeOffset CreatedOn { get; set; }
    }
}
