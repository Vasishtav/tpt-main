﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Pricing
{
    public class AdvancedPricingList
    {
        public int Id { get; set; }
        public int FranchiseId { get; set; }
        public int PricingStrategyTypeId { get; set; }
        public int? VendorId { get; set; }
        public int? ProductCategoryId { get; set; }
        public string RetailBasis { get; set; }
        public decimal? MarkUp { get; set; }
        public string MarkUpFactor { get; set; }
        public decimal? Discount { get; set; }
        public string DiscountFactor { get; set; }
        public string PriceRoundingOpt { get; set; }
        public string PricingStrategyType { get; set; }
        public string ProductCategory { get; set; }
        public string Vendor { get; set; }
    }
}
