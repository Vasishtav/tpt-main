﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO
{
    public class SalesAgentDTO
    {
        public int PersonId { get; set; }
        public string SalesAgent { get; set; }
        public int opportunities { get; set; }
        public int Quote { get; set; }
        public int OpenQuote { get; set; }
        public int Lostsales { get; set; }
        public int Orders { get; set; }
        public double ClosingRate { get; set; }
        public int QuotetoClose { get; set; }
        public double TotalOrders { get; set; }
        public double AvgOrder { get; set; }
    }
}
