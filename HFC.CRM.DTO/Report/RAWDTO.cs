﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO
{
    public class RAWDTO
    {
        public string TerrName { get; set; }
        public string CustomerName { get; set; }
        public int ZipCode { get; set; }
        public int OrderNumber { get; set; }
        public DateTime ContractedDate { get; set; }
        public int Quantity { get; set; }
        public string ItemType { get; set; }
        public string ItemName { get; set; }
        public string ItemDetail { get; set; }
        public string IsTaxable { get; set; }
        public double UnitPrice { get; set; }
        public double ItemSubtotal { get; set; }
        public double UnitCost { get; set; }
        public double ItemCost { get; set; }
        public double ItemTaxPercent { get; set; }
        public double ItemTaxAmount { get; set; }
        public double TaxRateTotal { get; set; }
    }
}
