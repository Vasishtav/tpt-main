﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO
{
    public class MonthlyStatisticsDTO
    {
        public DateTime Month { get; set; }
        public int Leads { get; set; }
        public int Opportunities { get; set; }
        public int LostOppotunities { get; set; }
        public int LostQuote { get; set; }
        public double TotalLostQuote { get; set; }
        public int Orders { get; set; }
        public double Totalorders { get; set; }
        public int ConversionRate { get; set; }
        public int ClosingRate { get; set; }
        public int AvgOrder { get; set; }
        public int RevenuePerLead { get; set; }
        public int Quote { get; set; }
        public double TotalQuotes { get; set; }
        public int AvgQuote { get; set; }
        public int QuotetoClose { get; set; }
    }
}
