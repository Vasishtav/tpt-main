﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO
{
    public class ReportFilter
    {
        public int? VendorID { get; set; }
        public int? IndustryId { get; set; }
        public int? CommercialTypeId { get; set; }
        public int? PersonID { get; set; }
        public string CustomerName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? SourceId { get; set; }
        public string SourceList { get; set; }
        public int? campaignId { get; set; }
        public string Zip { get; set; }
        public int? TerritoryId { get; set; }
        public string ProductCategory { get; set; }
        public string ReportType { get; set; }
        public string Value { get; set; }
        public int? Year { get; set; }
    }

    public class SalesSummaryByMonth
    {
        public int Year { get; set; }
        public string ThisYear { get; set; }
        public int rowno { get; set; }
        public decimal Jan { get; set; }
        public decimal Feb { get; set; }
        public decimal Mar { get; set; }
        public decimal Apr { get; set; }
        public decimal May { get; set; }
        public decimal Jun { get; set; }
        public decimal Jul { get; set; }
        public decimal Aug { get; set; }
        public decimal Sep { get; set; }
        public decimal Oct { get; set; }
        public decimal Nov { get; set; }
        public decimal Dec { get; set; }
        public decimal tot { get; set; }
    }

    public class SalesSummaryByMonth_VM
    {
        public int Year { get; set; }
        public string ThisYear { get; set; }
        public int rowno { get; set; }
        public string Jan { get; set; }
        public string Feb { get; set; }
        public string Mar { get; set; }
        public string Apr { get; set; }
        public string May { get; set; }
        public string Jun { get; set; }
        public string Jul { get; set; }
        public string Aug { get; set; }
        public string Sep { get; set; }
        public string Oct { get; set; }
        public string Nov { get; set; }
        public string Dec { get; set; }
        public string tot { get; set; }
    }

    public class VendorSalesperformance_DM
    {
        public string VendorName { get; set; }
        public string TotSale { get; set; }
        public decimal COGS_Jan { get; set; }
        public decimal TotSale_Jan { get; set; }
        public decimal GPValue_Jan { get; set; }
        public decimal GPPerc_Jan { get; set; }
        public decimal COGS_Feb { get; set; }
        public decimal TotSale_Feb { get; set; }
        public decimal GPValue_Feb { get; set; }
        public decimal GPPerc_Feb { get; set; }
        public decimal COGS_Mar { get; set; }
        public decimal TotSale_Mar { get; set; }
        public decimal GPValue_Mar { get; set; }
        public decimal GPPerc_Mar { get; set; }
        public decimal COGS_Apr { get; set; }
        public decimal TotSale_Apr { get; set; }
        public decimal GPValue_Apr { get; set; }
        public decimal GPPerc_Apr { get; set; }
        public decimal COGS_May { get; set; }
        public decimal TotSale_May { get; set; }
        public decimal GPValue_May { get; set; }
        public decimal GPPerc_May { get; set; }
        public decimal COGS_Jun { get; set; }
        public decimal TotSale_Jun { get; set; }
        public decimal GPValue_Jun { get; set; }
        public decimal GPPerc_Jun { get; set; }
        public decimal COGS_Jul { get; set; }
        public decimal TotSale_Jul { get; set; }
        public decimal GPValue_Jul { get; set; }
        public decimal GPPerc_Jul { get; set; }
        public decimal COGS_Aug { get; set; }
        public decimal TotSale_Aug { get; set; }
        public decimal GPValue_Aug { get; set; }
        public decimal GPPerc_Aug { get; set; }
        public decimal COGS_Sep { get; set; }
        public decimal TotSale_Sep { get; set; }
        public decimal GPValue_Sep { get; set; }
        public decimal GPPerc_Sep { get; set; }
        public decimal COGS_Oct { get; set; }
        public decimal TotSale_Oct { get; set; }
        public decimal GPValue_Oct { get; set; }
        public decimal GPPerc_Oct { get; set; }
        public decimal COGS_Nov { get; set; }
        public decimal TotSale_Nov { get; set; }
        public decimal GPValue_Nov { get; set; }
        public decimal GPPerc_Nov { get; set; }
        public decimal COGS_Dec { get; set; }
        public decimal TotSale_Dec { get; set; }
        public decimal GPValue_Dec { get; set; }
        public decimal GPPerc_Dec { get; set; }
    }

    public class VendorSalesperformance_vm
    {
        public string VendorName { get; set; }
        public string TotSale { get; set; }
        public string COGS_Jan { get; set; }
        public string TotSale_Jan { get; set; }
        public string GPValue_Jan { get; set; }
        public string GPPerc_Jan { get; set; }
        public string COGS_Feb { get; set; }
        public string TotSale_Feb { get; set; }
        public string GPValue_Feb { get; set; }
        public string GPPerc_Feb { get; set; }
        public string COGS_Mar { get; set; }
        public string TotSale_Mar { get; set; }
        public string GPValue_Mar { get; set; }
        public string GPPerc_Mar { get; set; }
        public string COGS_Apr { get; set; }
        public string TotSale_Apr { get; set; }
        public string GPValue_Apr { get; set; }
        public string GPPerc_Apr { get; set; }
        public string COGS_May { get; set; }
        public string TotSale_May { get; set; }
        public string GPValue_May { get; set; }
        public string GPPerc_May { get; set; }
        public string COGS_Jun { get; set; }
        public string TotSale_Jun { get; set; }
        public string GPValue_Jun { get; set; }
        public string GPPerc_Jun { get; set; }
        public string COGS_Jul { get; set; }
        public string TotSale_Jul { get; set; }
        public string GPValue_Jul { get; set; }
        public string GPPerc_Jul { get; set; }
        public string COGS_Aug { get; set; }
        public string TotSale_Aug { get; set; }
        public string GPValue_Aug { get; set; }
        public string GPPerc_Aug { get; set; }
        public string COGS_Sep { get; set; }
        public string TotSale_Sep { get; set; }
        public string GPValue_Sep { get; set; }
        public string GPPerc_Sep { get; set; }
        public string COGS_Oct { get; set; }
        public string TotSale_Oct { get; set; }
        public string GPValue_Oct { get; set; }
        public string GPPerc_Oct { get; set; }
        public string COGS_Nov { get; set; }
        public string TotSale_Nov { get; set; }
        public string GPValue_Nov { get; set; }
        public string GPPerc_Nov { get; set; }
        public string COGS_Dec { get; set; }
        public string TotSale_Dec { get; set; }
        public string GPValue_Dec { get; set; }
        public string GPPerc_Dec { get; set; }
    }

    public class RawData_DM
    {
        public string FranchiseName { get; set; }
        public int? FranchiseCode { get; set; }
        public int? LeadNumber { get; set; }
        public string LeadTerritory { get; set; }
        public string LeadTerritoryCode { get; set; }
        public string LeadDate { get; set; }
        public string LeadFirstName { get; set; }
        public string LeadLastName { get; set; }
        public string LeadCompanyName { get; set; }
        public string LeadEMail { get; set; }
        public string LeadAddress1 { get; set; }
        public string LeadAddress2 { get; set; }
        public string LeadCity { get; set; }
        public string LeadState { get; set; }
        public string LeadZipCode { get; set; }
        public string LeadAttentionText { get; set; }
        public string LeadSource { get; set; }
        public string LeadCampaign { get; set; }
        public string LeadApptSubject { get; set; }
        public string LeadApptMessage { get; set; }
        public string LeadAppt { get; set; }
        public string LeadStatus { get; set; }
        public int? AccountNumber { get; set; }
        public string AccountTerritory { get; set; }
        public string AccountTerritoryCode { get; set; }
        public string AccountDate { get; set; }
        public string AccountFirstName { get; set; }
        public string AccountLastName { get; set; }
        public string AccountCompanyName { get; set; }
        public string AccountEMail { get; set; }
        public string AccountAddress1 { get; set; }
        public string AccountAddress2 { get; set; }
        public string AccountCity { get; set; }
        public string AccountState { get; set; }
        public string AccountZipCode { get; set; }
        public string AccountAttentionText { get; set; }
        public string AccountSource { get; set; }
        public string AccountCampaign { get; set; }
        public string AccountApptSubject { get; set; }
        public string AccountApptMessage { get; set; }
        public string AccountAppt { get; set; }
        public string AccountStatus { get; set; }
        public int? OpportunityNumber { get; set; }
        public string OpportunityName { get; set; }
        public string OpportunityTerritory { get; set; }
        public string OpportunityTerritoryCode { get; set; }
        public string OpportunityDate { get; set; }
        public string OpportunityFirstName { get; set; }
        public string OpportunityLastName { get; set; }
        public string OpportunityCompanyName { get; set; }
        public string OpportunityEMail { get; set; }
        public string OpportunityAddress1 { get; set; }
        public string OpportunityAddress2 { get; set; }
        public string OpportunityCity { get; set; }
        public string OpportunityState { get; set; }
        public string OpportunityZipCode { get; set; }
        public string OpportunityAttentionText { get; set; }
        public string OpportunitySource { get; set; }
        public string OpportunityCampaign { get; set; }
        public string OpportunityApptSubject { get; set; }
        public string OpportunityApptMessage { get; set; }
        public string OpportunityAppt { get; set; }
        public string QuoteTerritory { get; set; }
        public string QuoteTerritoryCode { get; set; }
        public int? QuoteNumber { get; set; }
        public int? QuoteLineId { get; set; }
        public int? LineNumber { get; set; }
        public string OrderName { get; set; }
        public string OrderDate { get; set; }
        public string VendorName { get; set; }
        public string ProductCategory { get; set; }
        public string ProductName { get; set; }
        public string ProductModel { get; set; }
        public string Color { get; set; }
        public string Fabric { get; set; }
        public string Description { get; set; }
        public string ItemCost { get; set; }
        public string Qty { get; set; }
        public string LineCost { get; set; }
        public string LineValue { get; set; }
        public string SuggestedResale { get; set; }
        public string DiscountAmount { get; set; }
        public string MarginPercentage { get; set; }
        public string QuoteDate { get; set; }
        public string PrimaryQuote { get; set; }
        public string QuoteStatus { get; set; }
        public string OrderStatus { get; set; }
        public string ProductType { get; set; }
        public string IsAlliance { get; set; }
        public string MPODate { get; set; }
        public string VPODate { get; set; }
        public string PromiseByDate { get; set; }
        public string EstShipDate { get; set; }
        public string ShipDate { get; set; }
        public string InvoiceDate { get; set; }
        public string ReceivedDate { get; set; }
        public string VPOStatus { get; set; }
        public int? RevenueCounted { get; set; }
    }

    public class MSR_DM
    {
        public string Territory { get; set; }
        public DateTime Mth { get; set; }
        public int DisplaySort { get; set; }
        public string DisplayGroup { get; set; }
        public string Category { get; set; }
        public decimal Cost { get; set; }
        public decimal Retail { get; set; }
    }
}
