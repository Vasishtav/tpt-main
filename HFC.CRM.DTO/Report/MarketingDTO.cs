﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO
{
    public class MarketingDTO
    {
        public string Source { get; set; }
        public string Campaign { get; set; }
        public string ParentCampaign { get; set; }
        public int Leads { get; set; }
        public int Opportunities { get; set; }
        public int Orders { get; set; }
        public double TotalOrders { get; set; }
        public int ConversionRate { get; set; }
        public int ClosingRate { get; set; }
        public int AvgOrder { get; set; }
        public int RevenuePerLead { get; set; }
        public int Quote { get; set; }
        public double TotalQuotes { get; set; }
        public int AvgQuote { get; set; }
        public int QuotetoClose { get; set; }
        public double CampaignCost { get; set; }
        public double CostPerOpportunity { get; set; }
        public double CostPerOrder { get; set; }
        public double ROI { get; set; }
    }
}
