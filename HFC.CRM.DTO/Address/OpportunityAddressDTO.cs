﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Address
{
    public class OpportunityAddressDTO
    {
        public int AddressId { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        private System.DateTime _CreatedOnUtc = DateTime.Now;
        public System.DateTime CreatedOnUtc
        {
            get { return _CreatedOnUtc; }
            set { _CreatedOnUtc = DateTime.SpecifyKind(value, DateTimeKind.Unspecified); }
        }
        public bool IsDeleted { get; set; }
        public bool IsResidential { get; set; }
        public string CrossStreet { get; set; }
        public string CountryCode2Digits { get; set; }
        public string AttentionText { get; set; }
        public string Location { get; set; }
        public System.Guid AddressesGuid { get; set; }
    }
}
