﻿using Dapper;
using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Product
{
   public class ProductListViewModel
    {
       
        public int ProductKey { get; set; }
        public int ProductID { get; set; }
        public int VendorId { get; set; }
        public string ProductName { get; set; }
        public string VendorName { get; set; }
        public string VendorProductSKU { get; set; }
        public string ProductCategory { get; set; }
        public string ProductSubCategory { get; set; }
        public string Description { get; set; }
        public decimal? SalePrice { get; set; }
        public int FranchiseId { get; set; }
        public string ProductStatus { get; set; }
        public int MasterSurfacingProductID { get; set; }
    }
    public class MultipartProductListViewModel
    {

        public int ProductKey { get; set; }
        public int ProductID { get; set; }
        public int VendorId { get; set; }
        public string ProductName { get; set; }
        public string VendorName { get; set; }
        public string VendorProductSKU { get; set; }
        public string ProductCategory { get; set; }
        public string ProductSubCategory { get; set; }
        public string Description { get; set; }
        public decimal? SalePrice { get; set; }
        public int FranchiseId { get; set; }
        public string ProductStatus { get; set; }
        public int MasterSurfacingProductID { get; set; }
        public string MultipleSurfaceProductSet { get; set; }
        public List<MultipleSurfaceProduct> MultipleSurfaceProduct { get; set; }
    }
}
