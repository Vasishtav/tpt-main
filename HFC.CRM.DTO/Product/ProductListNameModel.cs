﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Product
{
   public class ProductListNameModel
    {
      
        public int VendorIdPk { get; set; }
        public int VendorID { get; set; }
        public string Name { get; set; }
        public int VendorType { get; set; }
    }
}
