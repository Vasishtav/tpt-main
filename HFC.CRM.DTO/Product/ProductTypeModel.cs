﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Product
{
    public class ProductTypeModel
    {
        public int Id { get; set; }
        public string ProductType { get; set; } 
        public bool IsActive { get; set; }
    }
}
