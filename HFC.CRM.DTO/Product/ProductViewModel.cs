﻿using Dapper;
using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Product
{
    public class ProductViewModel
    {
        [Key]
        public int ProductKey { get; set; }
        public int ProductID { get; set; }
        public string ProductType { get; set; }
        public string ProductName { get; set; }
        public int? PICProductID { get; set; }
        public int? VendorID { get; set; }
        public string VendorName { get; set; }
        public string VendorProductSKU { get; set; }
        public string ProductCategory { get; set; }
        public string ProductSubCategory { get; set; }
        public string Description { get; set; }
        public decimal? Cost { get; set; }
        public decimal? SalePrice { get; set; }
        public int FranchiseId { get; set; }
        public string ProductStatus { get; set; }
        public string MarkUp { get; set; }
        public string MarkupType { get; set; }
        public decimal? Discount { get; set; }
        public string DiscountType { get; set; }
        public decimal? CalculatedSalesPrice { get; set; }
        public string ProductCollection { get; set; }
        public string ProductModelID { get; set; }
        public DateTime? DiscontinuedDate { get; set; }
        public DateTime? PhasedoutDate { get; set; }
        public string ProductGroupDesc { get; set; }
        public int VendorType { get; set; }
        public string Model { get; set; }
        public string Color { get; set; }
        public string Collection { get; set; }
        public int ProductCategoryId { get; set; }
        public bool Taxable { get; set; }
        public bool MultipartSurfacing { get; set; }
        public string SurfaceLaborSet { get; set; }
        public ProductSurfaceSetup ProductSurfaceSetup { get; set; }
        public string MultipleSurfaceProductSet { get; set; }
        public List<MultipleSurfaceProduct> MultipleSurfaceProduct { get; set; }
        public string MultipartAttributeSet { get; set; }
        public MultipartAttributes Attributes { get; set; }
        public int BrandID { get; set; }
    }

    public class HistoryProductPricingLog
    {
        public string TempData { get; set; }
        public DateTime CreatedOn { get; set; }
        public decimal? Cost { get; set; }
        public decimal? UnitPrice { get; set; }
        public string UserName { get; set; }
    }
}
