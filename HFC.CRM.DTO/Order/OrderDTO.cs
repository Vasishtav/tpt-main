﻿using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Order
{
    public class OrderDTO
    {
        public OrderDTO()
        {
            this.BillAddress = new AddressTP();
            this.InstallAddress = new AddressTP();
        }

        public int OrderID { get; set; }
        public int AccountId { get; set; }
        public int OpportunityId { get; set; }
        public int QuoteKey { get; set; }
        public string OrderName { get; set; }
        public int? OrderStatus { get; set; }
        public DateTimeOffset? ContractedDate { get; set; }
        public decimal? ProductSubtotal { get; set; }
        public decimal? AdditionalCharges { get; set; }
        public decimal? OrderDiscount { get; set; }
        public decimal? OrderSubtotal { get; set; }
        public decimal? Tax { get; set; }
        public decimal? OrderTotal { get; set; }
        public decimal? BalanceDue { get; set; }
        public int OrderNumber { get; set; }

        //Additional Info
        public string SalesAgent { get; set; }

        public string Opportunity { get; set; }
        public string AccountName { get; set; }
        public string AccountPhone { get; set; }
        public string InstallationAddress { get; set; }
        public AddressTP InstallAddress { get; set; }
        public int InstallationAddressId { get; set; }
        public string BillingAddress { get; set; }
        public AddressTP BillAddress { get; set; }
        public int BillingAddressId { get; set; }
        public string OrderStatusText { get; set; }
        public DateTimeOffset? CreatedOn { get; set; }
        public DateTimeOffset? LastUpdate { get; set; }
        public List<OrderLines> OrderLines { get; set; }
        public List<QuoteLines> QuoteLines { get; set; }
        public List<Payments> OrderPayments { get; set; }
        public List<InvoiceHistory> OrderInvoices { get; set; }
        public List<QuoteLinesVM> QuoteLinesVM { get; set; }
        public List<TaxDetails> TaxDetails { get; set; }
        public List<Tax> Taxinfo { get; set; }
        public List<DiscountsAndPromo> DiscountsAndPromo { get; set; }
        public Quote Quote { get; set; }
        public List<QuoteLineDetail> QuoteLineDetail { get; set; }
        public bool MpoExists { get; set; }
        public bool TaxCalculated { get; set; }
        public string SideMark { get; set; }

        // TP-2230: Mark Opportunity as tax exempt - Canadian Sales Tax
        public bool IsTaxExempt { get; set; }

        public string TaxExemptID { get; set; }
        public bool IsPSTExempt { get; set; }
        public bool IsGSTExempt { get; set; }
        public bool IsHSTExempt { get; set; }
        public bool IsVATExempt { get; set; }
        public bool IsNewConstruction { get; set; }

        public string CancelReason { get; set; }
        public string OrderInvoiceLevelNotes { get; set; }
        public string InstallerInvoiceNotes { get; set; }
        public string DiscountDescription { get; set; }
        public string PrimaryEmail { get; set; }
        public bool SendReviewEmail { get; set; }
        public bool IsTaxDisabled { get; set; }

        public DateTimeOffset? ContractedDateUpdatedOn { get; set; }
        public int ContractedDateUpdatedBy { get; set; }
        public DateTime? PreviousContractedDate { get; set; }
        public String ContractedDateUpdatedBy_Name { get; set; }
        public DateTimeOffset? OpportunityCreatedDate { get; set; }
        public decimal? OverPaymentAmount { get; set; }
        public bool IsNotifyemails { get; set; }
        public bool IsNotifyText { get; set; }
        // TP-2959 : CLONE - Suggestion: link to navigate from Order to MPO
        public int MasterPONumber { get; set; }
        public int PurchaseOrderId { get; set; }
        public DateTimeOffset? OpportunityReceivedDate { get; set; }

        // Represents whether a sales order can be cancelled/voide.
        public bool CanVoidCancelOrder { get; set; }
        public bool CanConvertToMpo { get; set; }

        public List<OrderMPODetails> MPODetails { get; set; }
        public decimal? PaymnetsApplied { get; set; }
        public bool UseTax { get; set; }
    }

    public class OrderStatusOpen
    {
        public int OrderID { get; set; }
        public DateTime CreatedOn { get; set; }
        public int BuyerremorseDay { get; set; }
    }

    public class OrderMPODetails
    {
        public bool IsXMpo { get; set; }
        public string MasterPONumber { get; set; }
        public int PurchaseOrderId { get; set; }
    }
}