﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Order
{
    public class PaymentDTO
    {
        public int PaymentID { get; set; }
        public int OpportunityID { get; set; }
        public int OrderID { get; set; }
        public string Sidemark { get; set; }
        public decimal? Total { get; set; }
        public decimal? BalanceDue { get; set; }
        public bool? PayBalance { get; set; }
        public int? PaymentMethod { get; set; }
        public string VerificationCheck { get; set; }
        public DateTime? PaymentDate { get; set; }
        public decimal? Amount { get; set; }
        public string Memo { get; set; }
        //public DateTime? CreatedOn { get; set; }
        //public int? CreatedBy { get; set; }
       // public DateTime? LastUpdatedOn { get; set; }
        //public int? LastUpdatedBy { get; set; }

        //Additional Info
        public OrderDTO order { get; set; }
        public bool IncludeDiscount { get; set; }
        public bool Sendemail { get; set; }

        public bool? Reversal { get; set; }
        public decimal? ReversalAmount { get; set; }

        public int? ReasonCode { get; set; }
        public decimal? OverPaymentAmount { get; set; }
        public bool IsNotifyemails { get; set; }
    }
}
