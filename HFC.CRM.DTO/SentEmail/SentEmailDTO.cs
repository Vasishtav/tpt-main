﻿using System;

namespace HFC.CRM.DTO.SentEmail
{
    public class SentEmailDTO
    {
        public long SentEmailId { get; set; }
        public int? LeadId { get; set; }
        public int? JobId { get; set; }
        public string Recipients { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public System.DateTime? SentAt { get; set; }

        public string TemplateName { get; set; }
        public string SentByPerson { get; set; }
        public string LeadFullName { get; set; }
    }

    public class SyncfailedEmail
    {
        public int CalendarId { get; set; }
        public int SyncRecId { get; set; }
        public string FranchiseName { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public DateTime AppointmentDate { get; set; }
        public string OrganizerName { get; set; }
        public string OrganizerEmail { get; set; }
        public string Attendees { get; set; }
        public string AttendeesEmail { get; set; }
    }

    public class EmailData
    {
        public string toAddresses { get; set; }
        public string bccAddresses { get; set; }
        public string ccAddresses { get; set; }
        public string subject { get; set; }
        public string body { get; set; }
        public string[] streamid { get; set; }
        public string printOptions { get; set; }
        public string salesPacketOption { get; set; }
        public string installPacketOptions { get; set; }
        public string packetType { get; set; }
        public string Associatedwith { get; set; }
        public int id { get; set; }
        public bool Emailsignature { get; set; }

    }

    public class EmailData_MSR
    {
        public string toAddresses { get; set; }
        public string bccAddresses { get; set; }
        public string ccAddresses { get; set; }
        public string subject { get; set; }
        public string body { get; set; }
        public int TerritoryID { get; set; }
        public DateTime Mth { get; set; }
    }
}