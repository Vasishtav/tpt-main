﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Job
{
    public class JobQuoteDTO
    {
        public int QuoteId { get; set; }
        public int JobId { get; set; }
        public decimal Balance { get; set; }
        public decimal NetTotal { get; set; }
        public decimal DiscountTotal { get; set; }
        public decimal SurchargeTotal { get; set; }
        public decimal SubTotal { get; set; }
        public decimal TaxTotal { get; set; }
        public decimal TotalSaving { get; set; }
        public decimal PaymentsApplied { get; set; }
        public decimal Margin { get; set; }
        public decimal NetProfit { get; set; }
        public List<JobItemDTO> JobItems { get; set; }
        public List<JobSaleAdjustmentDTO> SaleAdjustments { get; set; }
        public bool CanDistribute { get; set; }
        public bool CanUpdateStatus { get; set; }
        public HFC.CRM.Data.Permission CostPermission { get; set; }
        public string InstallAddress { get; set; }
        public string BillingAddress { get; set; }
        
        public bool IsPrimary { get; set; }

        public bool DisplayCost { get; set; }
        public decimal TaxPercent { get; set; }

        public decimal CostSubtotal { get; set; }
    }
}
