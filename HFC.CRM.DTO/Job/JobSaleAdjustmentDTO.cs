﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Job
{
    public class JobSaleAdjustmentDTO
    {
        public int SaleAdjustmentId { get; set; }
        public int QuoteId { get; set; }
        public byte TypeEnum { get; set; }
        public string Category { get; set; }
        public decimal Amount { get; set; }
        public decimal Percent { get; set; }
        public string Memo { get; set; }
        public bool IsDebit { get; set; }
        public bool IsTaxable { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public DateTimeOffset LastUpdated { get; set; }
        public bool? ApplyDiscountBeforeTax { get; set; }
    }
}
