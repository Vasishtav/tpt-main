﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Job
{
    public class JobItemDTO
    {
        public int JobItemId { get; set; }
        public int QuoteId { get; set; }
        public short Quantity { get; set; }
        public int? StyleId { get; set; }
        public string Style { get; set; }
        public int? CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int? ProductTypeId { get; set; }
        public string ProductType { get; set; }
        public string ProductName { get; set; }
        public Guid? ProductGuid { get; set; }
        public string Description { get; set; }
        public string Color { get; set; }
        public int? ManufacturerId { get; set; }
        public string Manufacturer { get; set; }
        public bool IsTaxable { get; set; }
        public decimal SalePrice { get; set; }
        public decimal ListPrice { get; set; }
        public decimal MSRP { get; set; }
        public decimal JobberPrice { get; set; }
        public decimal JobberDiscountPercent { get; set; }
        public decimal UnitCost { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal DiscountPercent { get; set; }
        public string DiscountRemark { get; set; }
        public decimal? MarginPercent { get; set; }
        public decimal CostSubtotal { get; set; }
        public decimal Subtotal { get; set; }
        public int? CreatedByPersonId { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public DateTimeOffset LastUpdated { get; set; }
        public int? LastUpdatedPersonId { get; set; }
        public bool? isCustom { get; set; }
        public int? SortOrder { get; set; }
        public string DiscountType { get; set; }
        public bool? JobberPriceOverriden { get; set; }
        public int? SessionId { get; set; }
        public bool? IsSavedWithErrors { get; set; }
    }

}
