﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Job
{
    public class JobQuestionAnswersDTO
    {
        public int AnswerId { get; set; }
        public int JobId { get; set; }
        public int QuestionId { get; set; }
        public string Answer { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public int? LoggedByPersonId { get; set; }
        public DateTime? LastUpdatedOnUtc { get; set; }
    }
}
