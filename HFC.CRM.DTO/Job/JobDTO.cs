﻿using HFC.CRM.DTO.Calandar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HFC.CRM.Data;
using HFC.CRM.DTO.Files;

namespace HFC.CRM.DTO.Job
{
   public class JobDTO
    {
       public int JobId { get; set; }
       public int JobNumber { get; set; }
       public string Description { get; set; }
       public int? SourceId { get; set; }
       public int QuoteId { get; set; }
       public int? CustomerPersonId { get; set; }
       public int? SalesPersonId { get; set; }
       public int? InstallerPersonId { get; set; }
       public int? TerritoryId { get; set; }
       public int? BillingAddressId { get; set; }
       public int? InstallAddressId { get; set; }
       public string BillingAddress { get; set; }
       public string InstallAddress { get; set; }
       public int JobConceptId { get; set; }
       public DateTime? ContractedOnUtc { get; set; }
       public DateTime? QuoteDateUTC { get; set; }
       public string Hint { get; set; }
       public int JobStatusId { get; set; }
       public decimal Balance { get; set; }
       public decimal NetTotal { get; set; }
       public string SideMark { get; set; }
        public string QuotesIdsStr { get; set; }
       public bool SaleMade { get; set; }

       public List<int> QuotesIds {
           get {
               return QuotesIdsStr.Split(',').Select(int.Parse).ToList();
           }
       }

       public Data.Person Customer { get; set; }
       public Data.Person InstallerPerson { get; set; }
       public Data.Person SalesPerson { get; set; }
       public int LeadId { get; set; }
       public int FilesCount { get; set; }
       public int NotesCount { get; set; }
       public List<JobQuestionAnswersDTO> JobQuestionAnswers { get; set; }
       public List<FileDTO> PhysicalFiles { get; set; }
       public bool CanDistribute { get; set; }
       public bool CanUpdateStatus { get; set; }
       public HFC.CRM.Data.Permission Permission { get; set; }
       public HFC.CRM.Data.Permission CostPermission { get; set; }

       public JobQuote PrimeQuote { get; set; }
       public ICollection<JobQuote> JobQuotes { get; set; }
       public ICollection<Invoice> Invoices { get; set; }
    }
}
