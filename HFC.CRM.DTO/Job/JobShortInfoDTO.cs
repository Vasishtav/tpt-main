﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.Job
{
    public class JobShortInfoDTO
    {
        public int JobId { get; set; }
        public int JobNumber { get; set; }
        public int InstallAddressId { get; set; }
        public string InstallAddress { get; set; }
        public decimal NetTotal { get; set; }
        public bool SaleMade { get; set; }
        public decimal Balance { get; set; }
        public string Hint { get; set; }
        public int JobStatusId { get; set; }
    }
}
