﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO.VendorInvoice
{
    public class VendorInvoiceDTO
    {
        public int VendorInvoiceId { get; set; }
        public int PurchaseOrderId { get; set; }
        public string Vendor { get; set; }
        public string InvoiceNo { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public string Currency { get; set; }
        public string SoldTo { get; set; }
        public string BillTo { get; set; }
        public string CustomerID { get; set; }
        public string Terms { get; set; }
        public DateTime? RemitBy { get; set; }
        public string VendorComments { get; set; }
        public string VendorRef { get; set; }
        public decimal? Subtotal { get; set; }
        public decimal? Discount { get; set; }
        public decimal? Freight { get; set; }
        public decimal? ProcessingFee { get; set; }
        public decimal? MiscCharge { get; set; }
        public decimal? Tax { get; set; }
        public decimal? TotalAmount { get; set; }
        public string PurchaseOrder { get; set; }
        public int QTYOrdered { get; set; }
        public int QTYInvoiced { get; set; }
        public DateTime? ShippedDate { get; set; }
        public string ShippedVia { get; set; }
        public decimal? AdjSubtotal { get; set; }
        public decimal? AdjDiscount { get; set; }
        public decimal? AdjFreight { get; set; }
        public decimal? AdjProcessingFee { get; set; }
        public decimal? AdjMiscCharge { get; set; }
        public decimal? AdjTax { get; set; }
        public decimal? AdjTotal { get; set; }
        public string AdjComments { get; set; }
        public bool IsInvoiceRevised { get; set; }
        public List<VendorInvoiceDetailDTO> VendorInvoiceDetail { get; set; }
        public List<VendorInvoiceChangeHistoryDTO> VendorInvoiceChangeHistory { get; set; }
        public List<string> AuditTrail { get; set; }
    }

    public class VendorInvoiceDetailDTO
    {
        public int VendorInvoiceId { get; set; }
        public int VendorInvoiceDetailId { get; set; }
        public int? POLineNum { get; set; }
        public string ProductNumber { get; set; }
        public string Item { get; set; }
        public string ItemDescription { get; set; }
        public int? QTY { get; set; }
        public decimal? Cost { get; set; }
        public decimal? Discount { get; set; }
        public decimal? Amount { get; set; }
        public string Comment { get; set; }
        public decimal? AdjCost { get; set; }
        public decimal? AdjDiscount { get; set; }
        public decimal? AdjAmount { get; set; }
        public string AdjComments { get; set; }
    }

    public class VendorInvoiceChangeHistoryDTO
    {
        public DateTime CreatedOn { get; set; }
        public string PersonName { get; set; }
        public int? POLineNumber { get; set; }
        public string Change { get; set; }
        public string OrgValue { get; set; }
        public string AdjValue { get; set; }
    }

    public class VendorInvoiceDashboardDTO
    {
        public DateTime? Date { get; set; }
        public string InvoiceNumber { get; set; }
        public string VendorName { get; set; }
        public int PICVpo { get; set; }
        public int PurchaseOrderId { get; set; }
        public int OrderID { get; set; }
        public int OrderNumber { get; set; }
        public string SideMark { get; set; }
        public int Quantity { get; set; }
        public decimal? QuotedProductCost { get; set; }
        public decimal? InvoicedProductCost { get; set; }
        public decimal? OtherCharges { get; set; }
        public decimal? InvoiceTotal { get; set; }
        public decimal? VarianceAmount { get; set; }
        public bool PPV { get; set; }
    }

    public enum PPVfilterenum
    {
        All = 0,
        PPVTrue = 1,
        PPVFalse = 2
    }
}
