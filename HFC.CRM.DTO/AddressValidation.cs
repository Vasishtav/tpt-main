﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.DTO
{
    public class SmartyStreetResponse
    {
        public Suggestion[] suggestions { get; set; }
    }

    public class Suggestion
    {
        public string text { get; set; }
        public string street_line { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string city_line { get; set; }
    }


}
