﻿
-- =============================================
-- Author:		Rajkumar
-- Create date: 05/14/2019
-- Description:	Get calendar records based on Calendar Id (or) PersonId, Startdate and EndDate
-- =============================================
CREATE PROCEDURE CRM.spGETCalendarData
@FranchiseId int,
@CalendarId int,
@PersonIds varchar(4000)=null,
@StartDate datetime=null,
@EndDate datetime=null
AS
BEGIN
	
	SET NOCOUNT ON;

	if(@CalendarId>0)
	begin

		select  c.*,
         case when c.AccountId is not null
		then
		(select
		case when PreferredTFN='C' then CellPhone
		else case when PreferredTFN='H' then HomePhone
		else case when PreferredTFN='W' then WorkPhone
		else case when CellPhone is not null then CellPhone
		else case when HomePhone is not null then HomePhone
		else case when WorkPhone is not null then WorkPhone
		else ''
		end end end end end end
		from CRM.Customer cus
		join CRM.AccountCustomers acus on cus.CustomerId=acus.CustomerId and IsPrimaryCustomer=1
		where acus.AccountId=c.AccountId) end as AccountPhone,
		 case when c.LeadId is not null
		 then
		(select
		case when PreferredTFN='C' then CellPhone
		else case when PreferredTFN='H' then HomePhone
		else case when PreferredTFN='W' then WorkPhone
		else case when CellPhone is not null then CellPhone
		else case when HomePhone is not null then HomePhone
		else case when WorkPhone is not null then WorkPhone
		else ''
		end end end end end end
		from CRM.Customer cus
		join CRM.LeadCustomers lcus on cus.CustomerId=lcus.CustomerId and IsPrimaryCustomer=1
		where lcus.LeadId=c.LeadId)
		end as [PhoneNumber],
        (select [CRM].[fnGetAccountNameByAccountId](c.AccountId)) as AccountName,
		(select	oppo.OpportunityName from CRM.[Opportunities] oppo
		where oppo.OpportunityId=c.OpportunityId) as [OpportunityName],
        ep.[EventPersonId] AS [EventPersonId],
        ep.[PersonId] AS [PersonId],
        ep.[PersonName] AS [PersonName],
        case when c.OrderId is not null and c.OrderId>0 then (select OrderName from CRM.Orders where OrderID=c.OrderId)
		else case when c.OpportunityId is not null and c.OpportunityId>0 then (select OpportunityName from CRM.Opportunities where OpportunityId=c.OpportunityId)
		else case when c.AccountId is not null and c.AccountId>0 then (select [CRM].[fnGetAccountNameByAccountId](c.AccountId))
		else case when c.LeadId is not null and c.LeadId>0 then (select [CRM].[fnGetAccountNameByLeadId](c.LeadId))
		else ''
		end end end end as CalName       		
		from CRM.Calendar c
        join CRM.EventToPeople ep on c.CalendarId=ep.CalendarId
        left join CRM.EventRecurring er on c.RecurringEventId=er.RecurringEventId
        where c.IsDeleted=0 and ISNULL(c.IsCancelled,0)=0 and ISNULL(er.IsDeleted,0)=0 
        and  c.FranchiseId=@FranchiseId and c.CalendarId=@CalendarId

	end

	else
	Begin

		select  c.*,
         case when c.AccountId is not null
		then
		(select
		case when PreferredTFN='C' then CellPhone
		else case when PreferredTFN='H' then HomePhone
		else case when PreferredTFN='W' then WorkPhone
		else case when CellPhone is not null then CellPhone
		else case when HomePhone is not null then HomePhone
		else case when WorkPhone is not null then WorkPhone
		else ''
		end end end end end end
		from CRM.Customer cus
		join CRM.AccountCustomers acus on cus.CustomerId=acus.CustomerId and IsPrimaryCustomer=1
		where acus.AccountId=c.AccountId) end as AccountPhone,
		 case when c.LeadId is not null
		 then
		(select
		case when PreferredTFN='C' then CellPhone
		else case when PreferredTFN='H' then HomePhone
		else case when PreferredTFN='W' then WorkPhone
		else case when CellPhone is not null then CellPhone
		else case when HomePhone is not null then HomePhone
		else case when WorkPhone is not null then WorkPhone
		else ''
		end end end end end end
		from CRM.Customer cus
		join CRM.LeadCustomers lcus on cus.CustomerId=lcus.CustomerId and IsPrimaryCustomer=1
		where lcus.LeadId=c.LeadId)
		end as [PhoneNumber],
        (select [CRM].[fnGetAccountNameByAccountId](c.AccountId)) as AccountName,
		(select	oppo.OpportunityName from CRM.[Opportunities] oppo
		where oppo.OpportunityId=c.OpportunityId) as [OpportunityName],
        ep.[EventPersonId] AS [EventPersonId],
        ep.[PersonId] AS [PersonId],
        ep.[PersonName] AS [PersonName],
        case when c.OrderId is not null and c.OrderId>0 then (select OrderName from CRM.Orders where OrderID=c.OrderId)
		else case when c.OpportunityId is not null and c.OpportunityId>0 then (select OpportunityName from CRM.Opportunities where OpportunityId=c.OpportunityId)
		else case when c.AccountId is not null and c.AccountId>0 then (select [CRM].[fnGetAccountNameByAccountId](c.AccountId))
		else case when c.LeadId is not null and c.LeadId>0 then (select [CRM].[fnGetAccountNameByLeadId](c.LeadId))
		else ''
		end end end end as CalName           		
		from CRM.Calendar c
        join CRM.EventToPeople ep on c.CalendarId=ep.CalendarId
        left join CRM.EventRecurring er on c.RecurringEventId=er.RecurringEventId
        where c.IsDeleted=0 and ISNULL(c.IsCancelled,0)=0 and ISNULL(er.IsDeleted,0)=0 
		and c.EventTypeEnum in (0,1)
        and  c.FranchiseId=@FranchiseId and ep.PersonId in (select * from CRM.CSVToTable(@PersonIds)) 
		and (c.StartDate between @StartDate and @EndDate 
		or c.EndDate between @StartDate and @EndDate
		or (c.StartDate<=@StartDate and c.EndDate>=@EndDate))
		union
		select  c.*,
         case when c.AccountId is not null
		then
		(select
		case when PreferredTFN='C' then CellPhone
		else case when PreferredTFN='H' then HomePhone
		else case when PreferredTFN='W' then WorkPhone
		else case when CellPhone is not null then CellPhone
		else case when HomePhone is not null then HomePhone
		else case when WorkPhone is not null then WorkPhone
		else ''
		end end end end end end
		from CRM.Customer cus
		join CRM.AccountCustomers acus on cus.CustomerId=acus.CustomerId and IsPrimaryCustomer=1
		where acus.AccountId=c.AccountId) end as AccountPhone,
		 case when c.LeadId is not null
		 then
		(select
		case when PreferredTFN='C' then CellPhone
		else case when PreferredTFN='H' then HomePhone
		else case when PreferredTFN='W' then WorkPhone
		else case when CellPhone is not null then CellPhone
		else case when HomePhone is not null then HomePhone
		else case when WorkPhone is not null then WorkPhone
		else ''
		end end end end end end
		from CRM.Customer cus
		join CRM.LeadCustomers lcus on cus.CustomerId=lcus.CustomerId and IsPrimaryCustomer=1
		where lcus.LeadId=c.LeadId)
		end as [PhoneNumber],
        (select [CRM].[fnGetAccountNameByAccountId](c.AccountId)) as AccountName,
		(select	oppo.OpportunityName from CRM.[Opportunities] oppo
		where oppo.OpportunityId=c.OpportunityId) as [OpportunityName],
        ep.[EventPersonId] AS [EventPersonId],
        ep.[PersonId] AS [PersonId],
        ep.[PersonName] AS [PersonName],
        case when c.OrderId is not null and c.OrderId>0 then (select OrderName from CRM.Orders where OrderID=c.OrderId)
		else case when c.OpportunityId is not null and c.OpportunityId>0 then (select OpportunityName from CRM.Opportunities where OpportunityId=c.OpportunityId)
		else case when c.AccountId is not null and c.AccountId>0 then (select [CRM].[fnGetAccountNameByAccountId](c.AccountId))
		else case when c.LeadId is not null and c.LeadId>0 then (select [CRM].[fnGetAccountNameByLeadId](c.LeadId))
		else ''
		end end end end as CalName           		
		from CRM.Calendar c
        join CRM.EventToPeople ep on c.CalendarId=ep.CalendarId
        left join CRM.EventRecurring er on c.RecurringEventId=er.RecurringEventId
        where c.IsDeleted=0 and ISNULL(c.IsCancelled,0)=0 and ISNULL(er.IsDeleted,0)=0 
		and c.EventTypeEnum in (2)
        and  c.FranchiseId=@FranchiseId and ep.PersonId in (select * from CRM.CSVToTable(@PersonIds))
        order by c.CalendarId

	end

  
END
GO
