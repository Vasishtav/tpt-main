

CREATE PROCEDURE CRM.SPGetMPO
(
@PurchaseOrderId int,
@FranchiseId int
)
AS
BEGIN
--PO Details
SELECT pod.PurchaseOrdersDetailId, pod.PurchaseOrderId, pod.QuoteLineId, pod.PartNumber, pod.ShipDate, pod.Status, pod.PromiseByDate, pod.VendorNotes, pod.QuantityPurchased, pod.PickedBy, pod.PONumber, pod.CreatedOn, pod.CreatedBy, pod.UpdatedOn, pod.UpdatedBy, pod.NotesVendor, pod.PICPO, pod.StatusId, pod.Errors, pod.vendorReference, pod.Information, pod.Warning
 FROM crm.PurchaseOrderDetails pod
 INNER JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = pod.PurchaseOrderId
 left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
 INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
 inner join crm.QuoteLineDetail qld on ql.QuoteLineId = qld.QuoteLineId
 INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
 WHERE pod.PurchaseOrderId = @PurchaseOrderId AND o.FranchiseId = @FranchiseId;

 --quotelines
 SELECT ql.QuoteLineId, ql.QuoteKey, ql.MeasurementsetId, ql.VendorId, ql.ProductId, ql.SuggestedResale, ql.Discount, ql.CreatedOn, ql.CreatedBy,
 ql.LastUpdatedOn, ql.LastUpdatedBy, ql.IsActive, ql.Width, ql.Height, ql.UnitPrice, ql.Quantity, ql.Description, ql.MountType, ql.ExtendedPrice,
 ql.Memo, ql.ProductTypeId, ql.Quntity, ql.MarkupfactorType, ql.DiscountType,       ql.ProductName, ql.VendorName, ql.CurrencyExchangeId, ql.ConversionRate,
 ql.VendorCurrency, ql.ConversionCalculationType, ql.QuoteLineNumber, ql.FranctionalValueWidth, ql.FranctionalValueHeight, ql.RoomName, ql.WindowLocation,
 ql.ProductCategoryId, ql.ProductSubCategoryId, ql.BillofMaterial
 FROM crm.PurchaseOrderDetails pod
 INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
 inner join crm.QuoteLineDetail qld on ql.QuoteLineId = qld.QuoteLineId
 INNER JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = pod.PurchaseOrderId
 left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
 INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
 WHERE pod.PurchaseOrderId = @PurchaseOrderId AND o.FranchiseId = @FranchiseId;

 --quoteline detail
 SELECT qld.* FROM crm.PurchaseOrderDetails pod
 INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
 INNER JOIN	crm.QuoteLineDetail qld ON qld.QuoteLineId = pod.QuoteLineId
 INNER JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = pod.PurchaseOrderId
 left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
 INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
 WHERE pod.PurchaseOrderId = @PurchaseOrderId AND o.FranchiseId = @FranchiseId;

 --MPO
SELECT mpox.QuoteId,ord.OrderNumber, mpox.AccountId, mpox.OpportunityId, mpox.OrderId,
  mpo.*,o.OpportunityName,q.SideMark,[CRM].[fnGetAccountName_forOpportunitySearch](mpox.OpportunityId) AS AccountName, tp.POStatusName AS Status,
  (
  SELECT SUM(qld1.BaseCost*ql1.Quantity) FROM crm.Quote q1
  INNER JOIN crm.QuoteLines ql1 ON ql1.QuoteKey = q1.QuoteKey
  inner JOIN crm.QuoteLineDetail qld1 ON qld1.QuoteLineId = ql1.QuoteLineId
   INNER JOIN crm.MasterPurchaseOrderExtn mpo1x ON mpo1x.QuoteId = q1.QuoteKey

  INNER JOIN crm.MasterPurchaseOrder mpo1 ON mpo1.PurchaseOrderId = mpo1x.PurchaseOrderId
  WHERE ql1.ProductTypeId IN (1,2,5) and mpo1.PurchaseOrderId = mpo.PurchaseOrderId ) AS Total,
 STUFF(
 COALESCE(', ' + NULLIF(Address1, ''), '')  +
 COALESCE(', ' + NULLIF(Address2, ''), '')  +
 COALESCE(', ' + NULLIF(City, ''), '') +
 COALESCE(', ' + NULLIF([State], ''), '') +
 COALESCE(' ' + NULLIF(ZipCode, ''), '') +
 COALESCE(', ' + NULLIF(CountryCode2Digits , ''), ''),
 1, 1, '') AS Address,a.IsValidated,o.SalesAgentId,p.FirstName +' '+ p.LastName AS SalesAgent
 FROM crm.MasterPurchaseOrder mpo
 left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
 INNER JOIN crm.Type_POStatus tp ON tp.POStatusId = mpo.POStatusId
 INNER JOIN crm.orders ord ON ord.OrderId = mpox.OrderID
 INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
 INNER JOIN crm.Person p ON p.PersonId = o.SalesAgentId
 LEFT JOIN crm.Addresses a ON a.AddressId = mpo.ShipAddressId
 left join CRM.Quote q on o.OpportunityId =q.OpportunityId
 WHERE mpo.PurchaseOrderId = @PurchaseOrderId AND o.FranchiseId = @FranchiseId
	and q.PrimaryQuote = 1  ;

	--MPO Header
 SELECT *,ROW_NUMBER() OVER(ORDER BY VendorName ASC) AS LineNumber FROM (
 SELECT hven.Name as VendorName,ql.vendorId, SUM(ql.Quantity) AS TotalQuantity, count(*) TotalLines,pod.Status AS Status,
 pod.ShipDate,pod.PICPO FROM crm.PurchaseOrderDetails pod
 INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
 inner join crm.QuoteLineDetail qld on ql.QuoteLineId = qld.QuoteLineId
 INNER JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = pod.PurchaseOrderId
 left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
 INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
 LEFT JOIN Acct.FranchiseVendors AS ven ON ven.VendorId = ql.VendorId AND ven.FranchiseId = o.FranchiseId
 LEFT JOIN Acct.HFCVendors AS hven ON ven.VendorId = hven.VendorId
 WHERE pod.PurchaseOrderId = @PurchaseOrderId AND o.FranchiseId = @FranchiseId   AND ql.ProductTypeId =1
 GROUP BY pod.PICPO,ql.VendorId, hven.Name,pod.Status,pod.ShipDate
 union all
 SELECT hven.Name as VendorName,ql.vendorId, SUM(ql.Quantity) AS TotalQuantity, count(*) TotalLines,pod.Status AS Status,
 pod.ShipDate,pod.PONumber AS PICPO FROM crm.PurchaseOrderDetails pod
 INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
 inner join crm.QuoteLineDetail qld on ql.QuoteLineId = qld.QuoteLineId
 INNER JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = pod.PurchaseOrderId
 left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
 INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
 LEFT JOIN Acct.FranchiseVendors AS ven ON ven.VendorId = ql.VendorId AND ven.FranchiseId = o.FranchiseId
 LEFT JOIN Acct.HFCVendors AS hven ON ven.VendorId = hven.VendorId
 WHERE pod.PurchaseOrderId = @PurchaseOrderId AND o.FranchiseId = @FranchiseId    AND ql.ProductTypeId <>1
 GROUP BY pod.PONumber,ql.VendorId, hven.Name,pod.Status,pod.ShipDate) AS mpoheader;

 --VPO Details -- Core
 SELECT ISNULL(mpox.OrderId, 0) as OrderId,ql.QuoteLineNumber as LineNumber,pod.PurchaseOrdersDetailId,pod.PurchaseOrderId,pod.QuoteLineId,pod.PartNumber,ql.ProductName,ql.ProductTypeId,pod.QuantityPurchased AS purchased,
 ql.VendorName,ql.Description + ' Room: <b>' + ql.RoomName +'</b>' as Description,ql.Quantity,pod.PartNumber,pod.ShipDate,pod.PromiseByDate,pod.Status,pod.NotesVendor,pod.PICPO,pod.POResponse,pod.StatusId,
 pod.warning,pod.information,pod.Errors,(ql.quantity * qld.BaseCost) AS Cost,
 pod.PONumber,  pod.PickedBy,pod.vendorReference,ql.ExtendedPrice,ql.Width, ql.Height, qld.CounterId,qld.ValidConfiguration,qld.LastValidatedon,qld.ProductErrors,
ql.FranctionalValueWidth, ql.FranctionalValueHeight,ql.RoomName as WindowName  FROM crm.PurchaseOrderDetails pod
 INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
 inner join crm.QuoteLineDetail qld on ql.QuoteLineId = qld.QuoteLineId
 INNER JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = pod.PurchaseOrderId
 left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
 INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
 WHERE ql.ProductTypeId = 1 AND  pod.PurchaseOrderId = @PurchaseOrderId AND o.FranchiseId = @FranchiseId
 ORDER BY ql.QuoteLineNumber ASC;

 --VPO Details My product,One time
 SELECT qld.BaseCost * ql.Quantity AS Cost, ql.QuoteLineNumber as LineNumber,pod.PurchaseOrdersDetailId,pod.PurchaseOrderId,pod.QuoteLineId,pod.PartNumber,ql.ProductName,ql.ProductTypeId,pod.QuantityPurchased AS purchased,
 CASE WHEN ISNULL(ql.vendorid,0)<>0 AND hven.name=null THEN ven.Name WHEN ISNULL(ql.vendorid,0)<>0 AND ISNULL(hven.name,'')<>NULL THEN hven.name ELSE ql.vendorname END AS VendorName,ql.Description,ql.Quantity,pod.PartNumber,pod.ShipDate,pod.PromiseByDate,pod.Status,pod.NotesVendor,pod.PICPO,pod.StatusId,
 pod.PONumber,  pod.PickedBy ,ql.ExtendedPrice,ql.Width, ql.Height,
ql.FranctionalValueWidth, ql.FranctionalValueHeight,ql.RoomName as WindowName  FROM crm.PurchaseOrderDetails pod
 INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
 inner join crm.QuoteLineDetail qld on ql.QuoteLineId = qld.QuoteLineId
 INNER JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = pod.PurchaseOrderId
 left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
 INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
LEFT JOIN Acct.FranchiseVendors AS ven ON ven.VendorId = ql.VendorId AND ven.FranchiseId = o.FranchiseId
 LEFT JOIN Acct.HFCVendors AS hven ON ven.VendorId = hven.VendorId
 WHERE ql.ProductTypeId IN (2,5) AND  pod.PurchaseOrderId = @PurchaseOrderId AND o.FranchiseId = @FranchiseId
 ORDER BY ql.QuoteLineNumber ASC;

 --Vendor Account Numbers based on Territories
 select vta.*,stl.ShipToName from [CRM].[VendorTerritoryAccount] vta
 INNER JOIN crm.ShipToLocations stl ON stl.ShipToLocation = vta.ShipToLocation
 left JOIN crm.Opportunities o ON o.TerritoryId = vta.TerritoryId
 left join crm.MasterPurchaseOrderExtn mpox on mpox.OpportunityId = o.OpportunityId  --mpox.PurchaseOrderId = mpo.PurchaseOrderId
 INNER JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = mpox.PurchaseOrderId
 where o.FranchiseId=@FranchiseId AND mpo.PurchaseOrderId = @PurchaseOrderId;

-- Ship to location
SELECT stlm.Id, stl.ShipToLocation, t.Name + ' - '+stl.ShipToName AS ShipToName , stl.AddressId, stl.Active, stl.Deleted, stl.CreatedOn, stl.CreatedBy,
stl.LastUpdatedOn, stl.LastUpdatedBy, stl.FranchiseId, stlm.TerritoryId, STUFF(
                                     COALESCE(', ' + NULLIF(Address1, ''), '')  +
 		                            COALESCE(', ' + NULLIF(Address2, ''), '')  +
                                     COALESCE(', ' + NULLIF(City, ''), '') +
 		                            COALESCE(', ' + NULLIF([State], ''), '') +
                                     COALESCE(', ' + NULLIF(ZipCode, ''), '') +
                                     COALESCE(', ' + NULLIF(CountryCode2Digits , ''), ''),
                                     1, 1, '') AS Address,a.IsValidated FROM crm.ShipToLocations stl
 INNER JOIN crm.ShipToLocationsMap stlm ON stl.Id = stlm.ShipToLocation
 INNER JOIN crm.Addresses a ON a.AddressId = stl.AddressId
INNER JOIN crm.Territories t ON t.TerritoryId = stlm.TerritoryId
 WHERE ISNULL(stl.Active,0)=1 AND ISNULL(stl.Deleted,0)=0  AND   stlm.FranchiseId = @FranchiseId;

--Get address related to MPO
SELECT a.* FROM crm.MasterPurchaseOrder mpo
left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
INNER JOIN crm.Addresses a ON a.AddressId = mpo.ShipAddressId
WHERE mpo.PurchaseOrderId = @PurchaseOrderId  AND o.FranchiseId =  @FranchiseId;

--Shipnotice
SELECT distinct sn.* FROM crm.ShipNotice sn
--INNER JOIN crm.ShipmentDetail sd ON sd.ShipmentId = sn.ShipmentId
INNER JOIN crm.ShipmentDetail sd ON sd.ShipNoticeId = sn.ShipNoticeId
INNER JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = sn.PurchaseOrderId
left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
WHERE mpo.PurchaseOrderId = @PurchaseOrderId AND o.FranchiseId = @FranchiseId;

--Shipment Detail
SELECT distinct sd.* FROM crm.ShipNotice sn
INNER JOIN crm.ShipmentDetail sd ON sd.ShipmentId = sn.ShipmentId
INNER JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = sn.PurchaseOrderId
left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
WHERE mpo.PurchaseOrderId = @PurchaseOrderId AND o.FranchiseId = @FranchiseId

--Vendor Invoice
SELECT distinct vi.* FROM crm.VendorInvoice vi
INNER JOIN crm.VendorInvoiceDetail vid ON vid.VendorInvoiceId = vi.VendorInvoiceId
INNER JOIN crm.PurchaseOrderDetails pod ON pod.PICPO = vi.PICVpo
INNER JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = pod.PurchaseOrderId
left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
WHERE mpo.PurchaseOrderId = @PurchaseOrderId AND o.FranchiseId = @FranchiseId;

--Vendor Invoice Details
SELECT distinct vid.* FROM crm.VendorInvoice vi
INNER JOIN crm.VendorInvoiceDetail vid ON vid.VendorInvoiceId = vi.VendorInvoiceId
INNER JOIN crm.PurchaseOrderDetails pod ON pod.PICPO = vi.PICVpo
INNER JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = pod.PurchaseOrderId
left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
WHERE mpo.PurchaseOrderId = @PurchaseOrderId AND o.FranchiseId = @FranchiseId;

END
GO

