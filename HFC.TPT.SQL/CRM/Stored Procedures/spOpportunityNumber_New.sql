﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [CRM].[spOpportunityNumber_New]  
	@FranchiseId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF EXISTS(SELECT * FROM CRM.OpportunityNJobNumbers WHERE FranchiseId = @FranchiseId)
	BEGIN
		UPDATE CRM.OpportunityNJobNumbers SET OpportunityNumber = OpportunityNumber + 1
		OUTPUT INSERTED.OpportunityNumber 
		WHERE FranchiseId = @FranchiseId
	END
	ELSE
	BEGIN
		INSERT INTO CRM.OpportunityNJobNumbers (FranchiseId) 
		OUTPUT INSERTED.OpportunityNumber
		VALUES(@FranchiseId)
	END
END

