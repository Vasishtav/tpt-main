﻿
create PROC [CRM].[GetSourcesByFranchise]
(
@FranchiseId int
 )
as
/*
	EXEC [CRM].[GetSourcesByFranchise] @FranchiseId=2268
*/


SELECT DISTINCT [SourceId],[Name],[Description],[ParentId],[FranchiseId],[StartDate],[EndDate],[Amount],[CreatedOn],[IsActive],[Memo],[isDeleted],[isCampaign] 
FROM 
(
	SELECT [SourceId],[Name],[Description],[ParentId],[FranchiseId],[StartDate],[EndDate],[Amount],[CreatedOn],[IsActive],[Memo],[isDeleted],[isCampaign] 
	FROM  [CRM].[Sources] 
	WHERE FranchiseId IS NULL and isnull(isDeleted, 0) <> 1 and ParentId IS NULL
	UNION ALL 
	SELECT [SourceId],[Name],[Description],[ParentId],[FranchiseId],[StartDate],[EndDate],[Amount],[CreatedOn],[IsActive],[Memo],[isDeleted],[isCampaign] 
	FROM [CRM].[Sources] 
	WHERE parentId in (SELECT DISTINCT SourceId from [CRM].[Sources] WHERE FranchiseId IS NULL and isnull(isDeleted, 0) <> 1 and ParentId IS NULL) AND 
	(FranchiseId = @FranchiseId or FranchiseId is null) and 
	isnull(isDeleted, 0) <> 1
) as retVal 
ORDER BY [ParentId], [CreatedOn],[SourceId] 


