﻿

CREATE PROCEDURE [CRM].[spVendor_New]  	
AS
BEGIN
	
	SET NOCOUNT ON;

	IF EXISTS(SELECT VendorNumber FROM CRM.GetNewNumber)
	BEGIN
		UPDATE CRM.GetNewNumber SET VendorNumber = ISNULL(VendorNumber,0) + 1
		OUTPUT INSERTED.VendorNumber 
	END
	ELSE
	BEGIN
		INSERT INTO CRM.GetNewNumber (VendorNumber) 
		OUTPUT INSERTED.VendorNumber
		VALUES(1001)
	END
END


