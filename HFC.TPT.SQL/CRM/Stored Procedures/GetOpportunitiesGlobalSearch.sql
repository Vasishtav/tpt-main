﻿CREATE PROC [CRM].[GetOpportunitiesGlobalSearch]
( @FranchiseId int
	,@SearchType varchar(50)
	, @Value varchar(500))

AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
set @Value = '%'+ @Value + '%';

IF (@SearchType = 'name')
	BEGIN
		select o.OpportunityId, o.OpportunityNumber,  o.AccountId
        , Case when o.TerritoryId is not null then ter.Name
	        else o.TerritoryType
	        End Territory 
        , o.OpportunityName ,o.InstallationAddressId
        , [CRM].[fnGetAccountNameByAccountId](o.AccountId) AccountName
        , os.Name Status, Concat(p.FirstName, ' ', p.LastName) SalesAgent, o.SideMark,
        (select top 1 OrderTotal from [crm].[Orders] 
            where OpportunityId in(o.OpportunityId) 
                and OrderStatus !=9 and OrderStatus !=6) as OrderAmount,
        (select top 1 ContractedDate from crm.orders 
            where opportunityId in(o.OpportunityId) 
                and OrderStatus !=9 and OrderStatus !=6) as ContractDate
        , o.SalesAgentId, o.InstallerId
        from crm.Opportunities o 
        join crm.Type_OpportunityStatus os on os.OpportunityStatusId = o.OpportunityStatusId
        join crm.Person p on p.PersonId = o.SalesAgentId
        left join crm.Territories ter on ter.TerritoryId = o.TerritoryId 

		join crm.Accounts a2 on a2.AccountId = o.AccountId
		join crm.AccountCustomers ac2 on ac2.AccountId = a2.AccountId and ac2.IsPrimaryCustomer = 1
		join crm.Customer c2 on c2.CustomerId = ac2.CustomerId
		where o.FranchiseId = @FranchiseId
		and (o.OpportunityName like @Value 
			or o.SideMark like @Value 
			or c2.FirstName like @Value 
			or c2.LastName like @Value 
			or CONCAT(C2.firstname, ' ', C2.LastName) like @Value) 
		Order by AccountName, ContractDate desc
	END
ELSE IF (@SearchType = 'address')
	BEGIN
		select o.OpportunityId, o.OpportunityNumber,  o.AccountId
        , Case when o.TerritoryId is not null then ter.Name
	        else o.TerritoryType
	        End Territory 
        , o.OpportunityName ,o.InstallationAddressId
        , [CRM].[fnGetAccountNameByAccountId](o.AccountId) AccountName
        , os.Name Status, Concat(p.FirstName, ' ', p.LastName) SalesAgent, o.SideMark,
        (select top 1 OrderTotal from [crm].[Orders] 
            where OpportunityId in(o.OpportunityId) 
                and OrderStatus !=9 and OrderStatus !=6) as OrderAmount,
        (select top 1 ContractedDate from crm.orders 
            where opportunityId in(o.OpportunityId) 
                and OrderStatus !=9 and OrderStatus !=6) as ContractDate
        , o.SalesAgentId, o.InstallerId
        from crm.Opportunities o 
        join crm.Type_OpportunityStatus os on os.OpportunityStatusId = o.OpportunityStatusId
        join crm.Person p on p.PersonId = o.SalesAgentId
        left join crm.Territories ter on ter.TerritoryId = o.TerritoryId 

		left join crm.Addresses ia on ia.AddressId = o.InstallationAddressId
		left join crm.Addresses ba on ba.AddressId = o.BillingAddressId
        where o.FranchiseId = @FranchiseId
			and (ia.Address1 like @Value 
				or ia.Address2 like @Value 
				or ba.Address1 like @Value 
				or ba.Address2 like @Value) 
        Order by AccountName, ContractDate desc
	END
ELSE IF(@SearchType = 'city')
	BEGIN
		select o.OpportunityId, o.OpportunityNumber,  o.AccountId
        , Case when o.TerritoryId is not null then ter.Name
	        else o.TerritoryType
	        End Territory 
        , o.OpportunityName ,o.InstallationAddressId
        , [CRM].[fnGetAccountNameByAccountId](o.AccountId) AccountName
        , os.Name Status, Concat(p.FirstName, ' ', p.LastName) SalesAgent, o.SideMark,
        (select top 1 OrderTotal from [crm].[Orders] 
            where OpportunityId in(o.OpportunityId) 
                and OrderStatus !=9 and OrderStatus !=6) as OrderAmount,
        (select top 1 ContractedDate from crm.orders 
            where opportunityId in(o.OpportunityId) 
                and OrderStatus !=9 and OrderStatus !=6) as ContractDate
        , o.SalesAgentId, o.InstallerId
        from crm.Opportunities o 
        join crm.Type_OpportunityStatus os on os.OpportunityStatusId = o.OpportunityStatusId
        join crm.Person p on p.PersonId = o.SalesAgentId
        left join crm.Territories ter on ter.TerritoryId = o.TerritoryId 
		
		left join crm.Addresses ia on ia.AddressId = o.InstallationAddressId
		left join crm.Addresses ba on ba.AddressId = o.BillingAddressId
        where o.FranchiseId = @FranchiseId
			and (ia.City like @Value
				or ba.City like @Value) 
        Order by AccountName, ContractDate desc
	END
ELSE IF (@SearchType = 'id')
	BEGIN
		select o.OpportunityId, o.OpportunityNumber,  o.AccountId
        , Case when o.TerritoryId is not null then ter.Name
	        else o.TerritoryType
	        End Territory 
        , o.OpportunityName ,o.InstallationAddressId
        , [CRM].[fnGetAccountNameByAccountId](o.AccountId) AccountName
        , os.Name Status, Concat(p.FirstName, ' ', p.LastName) SalesAgent, o.SideMark,
        (select top 1 OrderTotal from [crm].[Orders] 
            where OpportunityId in(o.OpportunityId) 
                and OrderStatus !=9 and OrderStatus !=6) as OrderAmount,
        (select top 1 ContractedDate from crm.orders 
            where opportunityId in(o.OpportunityId) 
                and OrderStatus !=9 and OrderStatus !=6) as ContractDate
        , o.SalesAgentId, o.InstallerId
        from crm.Opportunities o 
        join crm.Type_OpportunityStatus os on os.OpportunityStatusId = o.OpportunityStatusId
        join crm.Person p on p.PersonId = o.SalesAgentId
        left join crm.Territories ter on ter.TerritoryId = o.TerritoryId 

		where o.FranchiseId = @FranchiseId
			and (o.OpportunityNumber like @Value) 
		Order by AccountName, ContractDate desc
	END
ELSE IF (@SearchType = 'all')
	BEGIN
		select o.OpportunityId, o.OpportunityNumber,  o.AccountId
        , Case when o.TerritoryId is not null then ter.Name
	        else o.TerritoryType
	        End Territory 
        , o.OpportunityName ,o.InstallationAddressId
        , [CRM].[fnGetAccountNameByAccountId](o.AccountId) AccountName
        , os.Name Status, Concat(p.FirstName, ' ', p.LastName) SalesAgent, o.SideMark,
        (select top 1 OrderTotal from [crm].[Orders] 
            where OpportunityId in(o.OpportunityId) 
                and OrderStatus !=9 and OrderStatus !=6) as OrderAmount,
        (select top 1 ContractedDate from crm.orders 
            where opportunityId in(o.OpportunityId) 
                and OrderStatus !=9 and OrderStatus !=6) as ContractDate
        , o.SalesAgentId, o.InstallerId
        from crm.Opportunities o 
        join crm.Type_OpportunityStatus os on os.OpportunityStatusId = o.OpportunityStatusId
        join crm.Person p on p.PersonId = o.SalesAgentId
        left join crm.Territories ter on ter.TerritoryId = o.TerritoryId

		join crm.Accounts a2 on a2.AccountId = o.AccountId
        join crm.AccountCustomers ac2 on ac2.AccountId = a2.AccountId and ac2.IsPrimaryCustomer = 1
        join crm.Customer c2 on c2.CustomerId = ac2.CustomerId
	    left join crm.Addresses ia on ia.AddressId = o.InstallationAddressId
	    left join crm.Addresses ba on ba.AddressId = o.BillingAddressId
        where o.FranchiseId = @FranchiseId
        and (o.OpportunityName like @Value 
	        or o.SideMark like @Value 
	        or c2.FirstName like @Value 
	        or c2.LastName like @Value 
            or CONCAT(C2.firstname, ' ', C2.LastName) like @Value
		    or (o.OpportunityNumber like @Value)
		    or ia.Address1 like @Value 
			or ia.Address2 like @Value 
			or ia.City like @Value
		    or ba.Address1 like @Value 
			or ba.Address2 like @Value
			or ba.City like @Value) 
        Order by AccountName, ContractDate desc
	END
ELSE 
	BEGIN
		declare @messsage varchar(500) = 'Invaid search type :' + @SearchType;
		THROW 60000, @messsage, 1;
	END;
	
	