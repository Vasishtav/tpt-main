﻿
/*
Validate all Quote lines in a nightly job that were created or updated in last 30 days
created By: Vasishta Veeramasuneni
Created on: 12/11/2019
*/

CREATE PROCEDURE [CRM].[GetQuotesForBatchJob]
AS
    BEGIN
        SELECT DISTINCT
               (ql.QuoteKey) AS quoteKey,
               f.FranchiseId,
               f.BrandId
        FROM crm.QuoteLines ql
             INNER JOIN crm.QuoteLineDetail qld ON qld.QuoteLineId = ql.QuoteLineId
                                                   AND ISNULL(qld.CounterId, 0) <> 0
             INNER JOIN crm.Quote q ON q.QuoteKey = ql.QuoteKey
                                       AND q.QuoteStatusId NOT IN(4, 5)
                                       AND ql.ProductTypeId = 1
             INNER JOIN crm.Opportunities o2 ON o2.OpportunityId = q.OpportunityId
             INNER JOIN crm.Franchise f ON f.FranchiseId = o2.FranchiseId
                                           AND f.BrandId = 1
             LEFT JOIN crm.Orders o ON o.QuoteKey = q.QuoteKey
                                       AND o.OrderStatus NOT IN(6, 9)
             LEFT JOIN crm.MasterPurchaseOrderExtn mpoe ON mpoe.QuoteId = q.QuoteKey
             LEFT JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = mpoe.PurchaseOrderId
                                                      AND ISNULL(mpo.SubmittedTOPIC, 0) <> 0
                                                      AND mpo.POStatusId IN(1)
						       AND q.LastUpdatedOn < DATEADD(day, 30, GETUTCDATE());
    END;