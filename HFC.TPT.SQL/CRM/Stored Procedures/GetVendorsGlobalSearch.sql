﻿CREATE PROC [CRM].[GetVendorsGlobalSearch]
( @FranchiseId int
	,@SearchType varchar(50)
	, @Value varchar(500))

AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @phoneValue varchar(20) =  '%' + [CRM].[fnRmoveFormatPhone](@value) + '%';
set @Value = '%'+ @Value + '%';

IF (@SearchType = 'email')
	BEGIN
		select v.VendorId Id
        , case v.VendorType
	        WHEN 3 THEN v.Name
	        ELSE hv.Name
	        END Name 
        , vt.VendorType
        , v.AccountRepPhone RepPhone, vs.VendorStatus Status
        , v.AccountRep
        , CASE 
			WHEN ISNULL(hv.isalliance, 0) = 1 then 'PIC'
			ELSE
				CASE v.OrderingMethod
				WHEN 1 THEN 'Email'
				WHEN 2 THEN 'Phone'
				WHEN 3 THEN 'Fax'
				WHEN 4 THEN 'Vendor Portal'
				WHEN 5 THEN 'Will Call'
				ELSE '' 
				END 
			END 'OrderingMethod'
        , a.Address1 + ' ' + a.Address2 +' '+ a.City + ' ' + a.State + ' ' + a.ZipCode Location
        , a.Address1, a.Address2, a.City, a.State, a.ZipCode
        from acct.FranchiseVendors v 
        join crm.Addresses a on a.AddressId = v.AddressId 
        join crm.Type_VendorType vt on vt.Id = v.VendorType
        left join Acct.HFCVendors hv on hv.VendorId = v.VendorId
        join crm.Type_VendorStatus vs on vs.Id = v.VendorStatus 

		where v.FranchiseId = @FranchiseId
            and v.VendorStatus = 1
            and v.AccountRepEmail like @Value 
       Order by Name
	END;
ELSE IF (@SearchType = 'phone')
	BEGIN
		select v.VendorId Id
        , case v.VendorType
	        WHEN 3 THEN v.Name
	        ELSE hv.Name
	        END Name 
        , vt.VendorType
        , v.AccountRepPhone RepPhone, vs.VendorStatus Status
        , v.AccountRep
        , CASE 
			WHEN ISNULL(hv.isalliance, 0) = 1 then 'PIC'
			ELSE
				CASE v.OrderingMethod
				WHEN 1 THEN 'Email'
				WHEN 2 THEN 'Phone'
				WHEN 3 THEN 'Fax'
				WHEN 4 THEN 'Vendor Portal'
				WHEN 5 THEN 'Will Call'
				ELSE '' 
				END 
			END 'OrderingMethod'
        , a.Address1 + ' ' + a.Address2 +' '+ a.City + ' ' + a.State + ' ' + a.ZipCode Location
        , a.Address1, a.Address2, a.City, a.State, a.ZipCode
        from acct.FranchiseVendors v 
        join crm.Addresses a on a.AddressId = v.AddressId 
        join crm.Type_VendorType vt on vt.Id = v.VendorType
        left join Acct.HFCVendors hv on hv.VendorId = v.VendorId
        join crm.Type_VendorStatus vs on vs.Id = v.VendorStatus 

		where v.FranchiseId = @FranchiseId
			and v.VendorStatus = 1
			and v.AccountRepPhone like @phoneValue
        Order by Name
		
	END;
ELSE IF (@SearchType = 'name')
	BEGIN
		select v.VendorId Id
        , case v.VendorType
	        WHEN 3 THEN v.Name
	        ELSE hv.Name
	        END Name 
        , vt.VendorType
        , v.AccountRepPhone RepPhone, vs.VendorStatus Status
        , v.AccountRep
        , CASE 
			WHEN ISNULL(hv.isalliance, 0) = 1 then 'PIC'
			ELSE
				CASE v.OrderingMethod
				WHEN 1 THEN 'Email'
				WHEN 2 THEN 'Phone'
				WHEN 3 THEN 'Fax'
				WHEN 4 THEN 'Vendor Portal'
				WHEN 5 THEN 'Will Call'
				ELSE '' 
				END 
			END 'OrderingMethod'
        , a.Address1 + ' ' + a.Address2 +' '+ a.City + ' ' + a.State + ' ' + a.ZipCode Location
        , a.Address1, a.Address2, a.City, a.State, a.ZipCode
        from acct.FranchiseVendors v 
        join crm.Addresses a on a.AddressId = v.AddressId 
        join crm.Type_VendorType vt on vt.Id = v.VendorType
        left join Acct.HFCVendors hv on hv.VendorId = v.VendorId
        join crm.Type_VendorStatus vs on vs.Id = v.VendorStatus

		where v.FranchiseId = @FranchiseId
			and v.VendorStatus = 1
			and (v.Name like @Value 
				or hv.Name like @Value)
        Order by Name

	END
ELSE IF (@SearchType = 'address')
	BEGIN
		select v.VendorId Id
        , case v.VendorType
	        WHEN 3 THEN v.Name
	        ELSE hv.Name
	        END Name 
        , vt.VendorType
        , v.AccountRepPhone RepPhone, vs.VendorStatus Status
        , v.AccountRep
        , CASE 
			WHEN ISNULL(hv.isalliance, 0) = 1 then 'PIC'
			ELSE
				CASE v.OrderingMethod
				WHEN 1 THEN 'Email'
				WHEN 2 THEN 'Phone'
				WHEN 3 THEN 'Fax'
				WHEN 4 THEN 'Vendor Portal'
				WHEN 5 THEN 'Will Call'
				ELSE '' 
				END 
			END 'OrderingMethod'
        , a.Address1 + ' ' + a.Address2 +' '+ a.City + ' ' + a.State + ' ' + a.ZipCode Location
        , a.Address1, a.Address2, a.City, a.State, a.ZipCode
        from acct.FranchiseVendors v 
        join crm.Addresses a on a.AddressId = v.AddressId 
        join crm.Type_VendorType vt on vt.Id = v.VendorType
        left join Acct.HFCVendors hv on hv.VendorId = v.VendorId
        join crm.Type_VendorStatus vs on vs.Id = v.VendorStatus 

		left join crm.Addresses ad on ad.AddressId = v.AddressId
        where v.FranchiseId = @FranchiseId 
			and v.VendorStatus = 1
			and (ad.Address1 like @Value 
				or ad.Address2 like @Value) 
        Order by Name
	END
ELSE IF(@SearchType = 'city')
	BEGIN
		select v.VendorId Id
        , case v.VendorType
	        WHEN 3 THEN v.Name
	        ELSE hv.Name
	        END Name 
        , vt.VendorType
        , v.AccountRepPhone RepPhone, vs.VendorStatus Status
        , v.AccountRep
        , CASE 
			WHEN ISNULL(hv.isalliance, 0) = 1 then 'PIC'
			ELSE
				CASE v.OrderingMethod
				WHEN 1 THEN 'Email'
				WHEN 2 THEN 'Phone'
				WHEN 3 THEN 'Fax'
				WHEN 4 THEN 'Vendor Portal'
				WHEN 5 THEN 'Will Call'
				ELSE '' 
				END 
			END 'OrderingMethod'
        , a.Address1 + ' ' + a.Address2 +' '+ a.City + ' ' + a.State + ' ' + a.ZipCode Location
        , a.Address1, a.Address2, a.City, a.State, a.ZipCode
        from acct.FranchiseVendors v 
        join crm.Addresses a on a.AddressId = v.AddressId 
        join crm.Type_VendorType vt on vt.Id = v.VendorType
        left join Acct.HFCVendors hv on hv.VendorId = v.VendorId
        join crm.Type_VendorStatus vs on vs.Id = v.VendorStatus 

		left join crm.Addresses ad on ad.AddressId = v.AddressId
		where v.FranchiseId = @FranchiseId 
			and v.VendorStatus = 1
			and (ad.City like @Value) 
		Order by Name
	END
ELSE IF(@SearchType = 'id')
	BEGIN
		select v.VendorId Id
        , case v.VendorType
	        WHEN 3 THEN v.Name
	        ELSE hv.Name
	        END Name 
        , vt.VendorType
        , v.AccountRepPhone RepPhone, vs.VendorStatus Status
        , v.AccountRep
        , CASE 
			WHEN ISNULL(hv.isalliance, 0) = 1 then 'PIC'
			ELSE
				CASE v.OrderingMethod
				WHEN 1 THEN 'Email'
				WHEN 2 THEN 'Phone'
				WHEN 3 THEN 'Fax'
				WHEN 4 THEN 'Vendor Portal'
				WHEN 5 THEN 'Will Call'
				ELSE '' 
				END 
			END 'OrderingMethod'
        , a.Address1 + ' ' + a.Address2 +' '+ a.City + ' ' + a.State + ' ' + a.ZipCode Location
        , a.Address1, a.Address2, a.City, a.State, a.ZipCode
        from acct.FranchiseVendors v 
        join crm.Addresses a on a.AddressId = v.AddressId 
        join crm.Type_VendorType vt on vt.Id = v.VendorType
        left join Acct.HFCVendors hv on hv.VendorId = v.VendorId
        join crm.Type_VendorStatus vs on vs.Id = v.VendorStatus

		where v.FranchiseId = @FranchiseId 
			and v.VendorStatus = 1
			and (v.VendorId like @Value) 
        Order by Name
	END
ELSE IF (@SearchType = 'all')
	BEGIN
		select v.VendorId Id
        , case v.VendorType
	        WHEN 3 THEN v.Name
	        ELSE hv.Name
	        END Name 
        , vt.VendorType
        , v.AccountRepPhone RepPhone, vs.VendorStatus Status
        , v.AccountRep
        , CASE 
		    WHEN ISNULL(hv.isalliance, 0) = 1 then 'PIC'
		    ELSE
			    CASE v.OrderingMethod
			    WHEN 1 THEN 'Email'
			    WHEN 2 THEN 'Phone'
			    WHEN 3 THEN 'Fax'
			    WHEN 4 THEN 'Vendor Portal'
			    WHEN 5 THEN 'Will Call'
			    ELSE '' 
			    END 
		    END 'OrderingMethod'
        , a.Address1 + ' ' + a.Address2 +' '+ a.City + ' ' + a.State + ' ' + a.ZipCode Location
        , a.Address1, a.Address2, a.City, a.State, a.ZipCode
        from acct.FranchiseVendors v 
        join crm.Addresses a on a.AddressId = v.AddressId 
        join crm.Type_VendorType vt on vt.Id = v.VendorType
        left join Acct.HFCVendors hv on hv.VendorId = v.VendorId
        join crm.Type_VendorStatus vs on vs.Id = v.VendorStatus 

	    left join crm.Addresses ad on ad.AddressId = v.AddressId

        where v.FranchiseId = @FranchiseId
        and v.VendorStatus = 1
        and (v.AccountRepEmail like @Value 
		    or v.Name like @Value 
		    or hv.Name like @Value

		    or v.AccountRepPhone like @phoneValue 

		    or ad.Address1 like @Value  
		    or ad.Address2 like @Value 

		    or ad.City like  @Value

		    or v.VendorId like @Value
	    ) Order by Name
	END
ELSE 
	BEGIN
		declare @messsage varchar(500) = 'Invaid search type :' + @SearchType;
		THROW 60000, @messsage, 1;
	END;
	
	