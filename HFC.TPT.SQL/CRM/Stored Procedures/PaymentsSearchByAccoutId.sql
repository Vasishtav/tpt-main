﻿

CREATE PROC [CRM].[PaymentsSearchByAccoutId]
(
@AccountId int
)
as
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
select p.PaymentID , p.Reversal, o.OrderID, o.OrderStatus,op.OpportunityId as OpportunityID,op.AccountId,
op.OpportunityName,
(select pm.PaymentMethod from  [CRM].[Type_PaymentMethod] pm where pm.Id=p.PaymentMethod) as Method 
,p.VerificationCheck,
CONVERT(date, p.PaymentDate) as PaymentDate,
p.Amount,p.Memo
from [CRM].[Opportunities] op  
join [crm].[Orders] o on op.OpportunityId=o.OpportunityId
join [crm].[Payments] p on o.OrderID=p.OrderID
where op.AccountId=@AccountId
