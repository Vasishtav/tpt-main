Create Procedure [CRM].[Report_OpenBalance] (
  @FranchiseId int,
  @PersonId varchar(max)=null,
  @IndustryId int=null,
  @CommercialTypeId int=null,
  @CustomerName varchar(200)=null,
  @StartDate datetime=null,
  @EndDate datetime=null )

as

begin
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
DECLARE @sqlCommand nvarchar(max)

set @sqlCommand='SELECT 
  case when opp.TerritoryID is not null then tr.Name else opp.TerritoryType end as TerritoryName
  ,sa.FirstName + '' '' +sa.LastName as Salesperson
  ,p.FirstName
  ,p.LastName
  ,case when a.IsCommercial=1 
  then case when p.CompanyName is not null and p.CompanyName!='''' then p.CompanyName+'' / '' else '''' end + p.FirstName + '' '' + p.LastName 
  else 
  p.FirstName + '' '' + p.LastName + case when p.CompanyName is not null and p.CompanyName!='''' then '' / ''+p.CompanyName else '''' end
  end as CustName 
  ,a.AccountNumber
  ,o.OrderNumber as CustOrder
  ,o.OrderID
  ,cast(o.ContractedDate as Date) ContractDate
  ,cast(max(pay.PaymentDate) as Date) LastPaymentDate
  ,o.OrderTotal Amount
  ,sum(isnull(case when isnull(pay.Reversal, 0) = 0 then pay.Amount else pay.Amount * -1 end, 0)) PaymentsReceived
  ,o.BalanceDue
  ,	os.OrderStatus
  ,	datediff(day, jc.Completed, getdate()) DaysOpen
  FROM CRM.Orders o
  LEFT JOIN
		CRM.Payments pay
			on o.OrderID = pay.OrderID
  INNER JOIN CRM.Opportunities opp ON o.OpportunityId = opp.OpportunityId
  INNER JOIN CRM.Accounts a ON opp.AccountId = a.AccountId
  INNER JOIN CRM.AccountCustomers acc on a.AccountId =acc.AccountId and acc.IsPrimaryCustomer=1
  INNER JOIN CRM.Customer p ON p.CustomerId = acc.CustomerId 
  INNER JOIN CRM.Person sa ON sa.PersonId = opp.SalesAgentId
  LEFT JOIN CRM.Type_OrderStatus os on o.OrderStatus = os.OrderStatusId
  LEFT JOIN (	select OrderID, CreatedOn Completed
				from (	select OrderID, CreatedOn, OrderStatusID, row_number() over (partition by OrderID order by CreatedOn desc) Idx
						from crm.OrderStatusTracker) x where OrderStatusID = 5 and Idx = 1 )
							jc on o.OrderID = jc.OrderID 
  LEFT JOIN CRM.Territories tr on tr.TerritoryId=opp.TerritoryID
  WHERE a.FranchiseId = @FranchiseID and o.OrderStatus not in (6,9)
  and o.BalanceDue != 0 '

     if(@PersonId is not null and @PersonId !='' )
		set @sqlCommand=@sqlCommand + ' and opp.SalesAgentId in ('+@PersonId+') '

    if(@CommercialTypeId is not null )
		set @sqlCommand=@sqlCommand + ' and a.CommercialTypeId=@CommercialTypeId '

    if(@IndustryId is not null )
		set @sqlCommand=@sqlCommand + ' and a.IndustryId=@IndustryId'
  
   if(@StartDate is not null and @EndDate is not null and @StartDate<=@EndDate)
		set @sqlCommand=@sqlCommand + ' and CAST(o.ContractedDate as DATE) between @StartDate and @EndDate '

     if(@CustomerName is not null)
		begin
		set @CustomerName='%'+@CustomerName+'%'

		--set @sqlCommand= 'select * from ( '+@sqlCommand+' ) as p where CustName like @CustomerName '
		set @sqlCommand=@sqlCommand + ' and case when a.IsCommercial=1 
  then case when p.CompanyName is not null and p.CompanyName!='''' then p.CompanyName+'' / '' else '''' end + p.FirstName + '' '' + p.LastName 
  else 
  p.FirstName + '' '' + p.LastName + case when p.CompanyName is not null and p.CompanyName!='''' then '' / ''+p.CompanyName else '''' end
  end like @CustomerName '

		end

	set @sqlCommand = @sqlCommand + ' 
	group by
  case when opp.TerritoryID is not null then tr.Name else opp.TerritoryType end
  ,sa.FirstName + '' '' +sa.LastName
  ,p.FirstName
  ,p.LastName
  ,case when a.IsCommercial=1 
  then case when p.CompanyName is not null and p.CompanyName!='''' then p.CompanyName+'' / '' else '''' end + p.FirstName + '' '' + p.LastName 
  else 
  p.FirstName + '' '' + p.LastName + case when p.CompanyName is not null and p.CompanyName!='''' then '' / ''+p.CompanyName else '''' end
  end
  ,a.AccountNumber
  ,o.OrderNumber
  ,o.OrderId
  ,cast(o.ContractedDate as Date)
  ,o.OrderTotal
  ,o.BalanceDue
  ,	os.OrderStatus
  ,	datediff(day, jc.Completed, getdate())
  order by o.OrderNumber asc'
	
	print @sqlCommand

   EXECUTE sp_executesql @sqlCommand,N'@FranchiseId int,@CommercialTypeId int,@IndustryId int,@CustomerName varchar(200),@StartDate datetime,@EndDate datetime'
  , @FranchiseId=@FranchiseId,@CommercialTypeId=@CommercialTypeId,@IndustryId=@IndustryId,@CustomerName=@CustomerName,@StartDate=@StartDate,@EndDate=@EndDate
   

end
