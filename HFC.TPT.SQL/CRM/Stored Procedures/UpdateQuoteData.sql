﻿CREATE PROCEDURE crm.UpdateQuoteData @quote  INT,
                                    @userId INT = 0
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        DECLARE @qSubtotal DECIMAL(18, 2), @coreCost DECIMAL(20, 10), @coreTotal DECIMAL(20, 10), @myProductTotal DECIMAL(20, 10), @myProductCost DECIMAL(20, 10), @serviceCost DECIMAL(20, 10), @additionalCharges DECIMAL(18, 2), @nvr DECIMAL(18, 10);
        DECLARE @ProductSubTotal DECIMAL(18, 2), @NetCharges DECIMAL(18, 2), @CoreProductMarginPercent DECIMAL(18, 2), @CoreProductMargin DECIMAL(18, 2), @MyProductMargin DECIMAL(18, 2), @MyProductMargin_Percent DECIMAL(18, 2), @ServiceMargin DECIMAL(18, 2);
        DECLARE @ServiceMarginPercent DECIMAL(18, 2), @TotalMargin DECIMAL(18, 2), @TotalMarginPercent DECIMAL(18, 2);
        DECLARE @TotalDiscounts DECIMAL(18, 2);
        SET @qSubtotal = 0;
        SET @coreCost = 0;
        SET @coreTotal = 0;
        SET @myProductTotal = 0;
        SET @myProductCost = 0;
        SET @serviceCost = 0;
        SET @netCharges = 0;
        SET @additionalCharges = 0;
        SET @nvr = 0;
        ---Calculate and get NVR
        SELECT @nvr = ISNULL(CRM.CalculateNVR(@quote), 1);

        --Calculate Sum of core Products
        SELECT @coreTotal = ISNULL(SUM(ISNULL(qld.ExtendedPrice, 0)), 0),
               @coreCost = ISNULL(SUM(ISNULL(qld.BaseCost, 0) * ISNULL(ql.Quantity, 0)), 0)
        FROM crm.QuoteLines ql
             INNER JOIN crm.QuoteLineDetail qld ON ql.QuoteLineId = qld.QuoteLineId
        WHERE ql.ProductTypeId = 1
              AND ql.QuoteKey = @quote;

        --Calculate sum of my Products
        SELECT @myProductTotal = ISNULL(SUM(ISNULL(qld.ExtendedPrice, 0)), 0),
               @myProductCost = ISNULL(SUM(ISNULL(qld.BaseCost, 0) * ISNULL(ql.Quantity, 0)), 0)
        FROM crm.QuoteLines ql
             INNER JOIN crm.QuoteLineDetail qld ON ql.QuoteLineId = qld.QuoteLineId
        WHERE ql.ProductTypeId IN(2, 5)
             AND ql.QuoteKey = @quote;

        --Calculate Sum of Services
        SELECT @additionalCharges = ISNULL(SUM(ISNULL(qld.ExtendedPrice, 0)), 0),
               @serviceCost = ISNULL(SUM(ISNULL(qld.BaseCost, 0) * ISNULL(ql.Quantity, 0)), 0)
        FROM crm.QuoteLines ql
             INNER JOIN crm.QuoteLineDetail qld ON ql.QuoteLineId = qld.QuoteLineId
        WHERE ql.ProductTypeId IN(3)
             AND ql.QuoteKey = @quote;

        --Core Product
        SET @CoreProductMargin = ROUND(((@coreTotal * @nvr) - @coreCost), 2);
        IF ISNULL(@coreTotal, 0) <> 0
            BEGIN
                SET @CoreProductMarginPercent = ROUND(((@CoreProductMargin / (@coreTotal * @nvr)) * 100), 2);
        END;

        -- My Product
        SET @MyProductMargin = ROUND(((@myProductTotal * @nvr) - @myProductCost), 2);
        IF ISNULL(@myProductTotal, 0) <> 0
            BEGIN
                SET @MyProductMargin_Percent = ROUND(((@MyProductMargin / (@myProductTotal * @nvr)) * 100), 2);
        END;

        -- Services
        SET @ServiceMargin = ROUND((@additionalCharges - @serviceCost), 2);
        IF ISNULL(@additionalCharges, 0) <> 0
            BEGIN
                SET @ServiceMarginPercent = ROUND(((@ServiceMargin / @additionalCharges) * 100), 2);
        END;

        --Product Sub total
        SET @ProductSubTotal = ROUND((@coreTotal + @myProductTotal), 2);

        --Net Charge
        SET @netCharges = ROUND(((@ProductSubTotal * @nvr) + @additionalCharges), 2);
        --Total Discounts
        SET @TotalDiscounts = ROUND(((@coreTotal + @myProductTotal) - (@coreTotal + @myProductTotal) * @nvr), 2);

        --Quote Subtotal
        SET @qSubtotal = ROUND(((@ProductSubTotal + @additionalCharges) - @TotalDiscounts), 2);

        -- Margin
        --@TotalMargin decimal(20,10),@TotalMarginPercent decimal(20,10)
        SET @TotalMargin = ROUND((@CoreProductMargin + @MyProductMargin + @ServiceMargin), 2);
        IF ISNULL(@netCharges, 0) <> 0
            BEGIN
                SET @TotalMarginPercent = ROUND(((@TotalMargin / @netCharges) * 100), 2);
        END;

        --SELECT @nvr AS nvr,@coreTotal AS Core_Total,@coreCost AS Core_Cost,@myProductTotal AS MyPrd_Total,@myProductCost as MyPrd_Cost,@additionalCharges AS Additional_Charges,@serviceCost AS Service_Cost,CAST(ROUND(@CoreProductMargin,2) AS decimal(18,2)) AS core_Margin,
        --ROUND(@CoreProductMarginPercent,2) AS Core_Margin_Percent,@MyProductMargin AS MyProductMargin, @MyProductMargin_Percent AS MyProductMargin_Percent
        --,@ServiceMargin AS ServiceMargin, @ServiceMarginPercent AS ServiceMargin_Percent
        --,@TotalMargin AS TotalMargin, @TotalMarginPercent AS TotalMargin_Percent,@TotalDiscounts AS Total_Discounts,@netCharges AS Net_Charges, @ProductSubTotal AS Product_Subtotal, @qSubtotal AS Sub_total
        --SELECT q.nvr,q.CoreProductMargin,q.CoreProductMarginPercent,q.MyProductMargin,q.MyProductMarginPercent,q.AdditionalCharges,q.ServiceMargin,q.ServiceMarginPercent,q.TotalMargin,q.TotalMarginPercent
        --,q.TotalDiscounts,q.NetCharges,q.ProductSubTotal,q.QuoteSubTotal FROM  crm.Quote q WHERE q.QuoteKey=@quote

        UPDATE crm.Quote
          SET
              TotalDiscounts = @TotalDiscounts,
              NetCharges = @netCharges,
              LastUpdatedOn = GETUTCDATE(),
              LastUpdatedBy = @userId,
              AdditionalCharges = @additionalCharges,
              TotalMargin = @TotalMargin,
              ProductSubTotal = @ProductSubTotal,
              QuoteSubTotal = @qSubtotal,
              CoreProductMargin = @CoreProductMargin,
              CoreProductMarginPercent = @CoreProductMarginPercent,
              MyProductMargin = @MyProductMargin,
              MyProductMarginPercent = @MyProductMargin_Percent,
              ServiceMargin = @ServiceMargin,
              ServiceMarginPercent = @ServiceMarginPercent,
              TotalMarginPercent = @TotalMarginPercent,
              NVR = @nvr
        WHERE QuoteKey = @quote;

        --SELECT @TotalDiscounts AS TotalDiscounts,
        --       @netCharges AS Netcharges,
        --       @additionalCharges AS AdditionalCharges,
        --       @TotalMargin AS TotalMargin,
        --       @ProductSubTotal AS ProductSubtotal,
        --       @qSubtotal AS QuoteSubtotal,
        --       @CoreProductMargin AS CoreProductMargin,
        --       @CoreProductMarginPercent AS CoreProductMarginPercent,
        --       @MyProductMargin AS MyproductMargin,
        --       @MyProductMargin_Percent AS MyproductmarginPercent,
        --       @ServiceMargin AS ServiceMargin,
        --       @ServiceMarginPercent AS ServiceMarginPercent,
        --       @TotalMargin AS TotalMargin,
        --       @nvr AS NVR;
    END;
GO