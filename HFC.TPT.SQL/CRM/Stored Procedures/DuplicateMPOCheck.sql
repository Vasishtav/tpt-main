﻿CREATE PROCEDURE [CRM].[DuplicateMPOCheck] @orderId INT = 0
AS
    BEGIN
        IF EXISTS
        (
            SELECT pod.*
            FROM crm.QuoteLines ql
                 INNER JOIN crm.Orders o ON o.QuoteKey = ql.QuoteKey
                 INNER JOIN crm.MasterPurchaseOrderExtn mpoe ON mpoe.OrderID = o.OrderId
                 INNER JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = mpoe.PurchaseOrderId
                 INNER JOIN crm.PurchaseOrderDetails pod ON ql.QuoteLineId = pod.QuoteLineId
            WHERE mpo.POStatusId <> 8
                  AND pod.StatusId <> 8
                  AND o.OrderID = @orderId
        ) AND NOT EXISTS  (SELECT 1 FROM crm.Orders o
INNER JOIN crm.OrderLines ol ON o.OrderID = ol.OrderID AND ol.OrderLineStatus  IN (7,8)
INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = ol.QuoteLineId AND ql.ProductTypeId=1
WHERE o.OrderID=@orderId )
            BEGIN
                SELECT 1;
        END;
            ELSE
            BEGIN
                SELECT 0;
        END;
    END;