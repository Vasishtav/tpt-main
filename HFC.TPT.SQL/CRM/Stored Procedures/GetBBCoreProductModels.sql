﻿CREATE PROCEDURE CRM.[GetBBCoreProductModels] @franchiseid INT
AS
    BEGIN
        SELECT CAST(
        (
            SELECT prdGrp.*,
                   PICProductList.ProductName,
                   PICProductList.PICProductID 'PICProductId',
                   PICProductList.ProductID,
                   PICProductList.PhasedoutDate,
                   PICProductList.DiscontinuedDate,
                   PICProductList.HeightPrompt,
                   PICProductList.WidthPrompt,
                   PICProductList.MountPrompt,
                   PICProductList.ColorPrompt,
                   PICProductList.FabricPrompt,
                   PICProductList.RoomPrompt,
                   PICProductList.ProductCategoryId,
                   CASE
                       WHEN PICProductList.DiscontinuedDate IS NOT NULL
                            AND DATEDIFF(day, CAST(PICProductList.DiscontinuedDate AS DATETIME), GETUTCDATE()) < 0
                       THEN 'Product will be Discontinued from ' + CONVERT(VARCHAR, PICProductList.DiscontinuedDate, 111)
                       WHEN PICProductList.DiscontinuedDate IS NOT NULL
                            AND DATEDIFF(day, CAST(PICProductList.DiscontinuedDate AS DATETIME), GETUTCDATE()) > 0
                       THEN 'Product is Discontinued on ' + CONVERT(VARCHAR, PICProductList.DiscontinuedDate, 111)
                       WHEN PICProductList.PhasedoutDate IS NOT NULL
                       THEN 'Product is Phasing out from ' + CONVERT(VARCHAR, PICProductList.PhasedoutDate, 111)
                       ELSE ''
                   END AS WarningPhasingOut,
                   models.Model,
                   models.Description,
                   models.WarningMessage,
                   models.DiscontinuedDate,
                   models.PhasedoutDate
            FROM
            (
                SELECT hven.PICVendorId,
                       hven.VendorId,
                       hven.Name AS VendorName,
                       hprd.ProductGroup AS PICProductGroupId,
                       hprd.ProductGroupDesc AS PICProductGroupName
                FROM CRM.HFCProducts hprd
                     INNER JOIN Acct.HFCVendors hven ON hven.VendorId = hprd.VendorID
                                                        AND ISNULL(hven.IsActive, 0) <> 0
            ) AS prdGrp
            INNER JOIN CRM.HFCProducts PICProductList ON PICProductList.VendorID = prdGrp.VendorId
                                                         AND PICProductList.ProductGroup = prdGrp.PICProductGroupId
            INNER JOIN crm.hfcmodels models ON models.PICProductId = PICProductList.PICProductID
            INNER JOIN acct.FranchiseVendors fv ON fv.vendorid = PICProductList.vendorid
                                                   AND fv.vendortype = 1
                                                   AND fv.vendorstatus = 1
                                                   AND PICProductList.Active = 1
            INNER JOIN crm.Franchise f ON f.FranchiseId = fv.FranchiseId
            WHERE fv.franchiseid = @franchiseid
                  AND ISNULL(PICProductList.Active, 0) <> 0
                  AND ISNULL(models.Active, 0) <> 0
                  AND 1 = CASE
                              WHEN f.Currency = 'US'
                                   AND models.USActive = 1
                              THEN 1
                              WHEN f.Currency = 'CA'
                                   AND models.CAActive = 1
                              THEN 1
                              ELSE 0
                          END
            GROUP BY prdGrp.VendorName,
                     prdGrp.PICVendorId,
                     PICProductList.ProductGroupDesc,
                     prdGrp.VendorId,
                     PICProductList.ProductName,
                     models.Description,
                     prdGrp.PICProductGroupId,
                     prdGrp.PICProductGroupName,
                     PICProductList.PICProductID,
                     PICProductList.ProductID,
                     PICProductList.PhasedoutDate,
                     PICProductList.DiscontinuedDate,
                     PICProductList.HeightPrompt,
                     PICProductList.WidthPrompt,
                     PICProductList.MountPrompt,
                     PICProductList.ColorPrompt,
                     PICProductList.FabricPrompt,
                     PICProductList.RoomPrompt,
                     PICProductList.ProductCategoryId,
                     models.ModelKey,
                     models.Model,
                     models.Description,
                     models.ProductId,
                     models.PICProductId,
                     models.Active,
                     models.DiscontinuedDate,
                     models.PhasedoutDate,
                     models.USActive,
                     models.CAActive,
                     models.VendorId,
                     models.WarningMessage,
                     models.IsManualFLag,
                     models.CreatedOn,
                     models.CreatedBy,
                     models.LastUpdatedOn,
                     models.LastUpdatedBy
            ORDER BY VendorName,
                     ProductGroupDesc,
                     ProductName,
                     models.description ASC FOR JSON AUTO
        ) AS VARCHAR(MAX));
    END;