﻿
/*
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
EXEC [CRM].[spOrderNumber_New] 55 
-- =============================================
*/
CREATE PROCEDURE [CRM].[spOrderNumber_New]  
	@FranchiseId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF EXISTS(SELECT * FROM CRM.LeadNJobNumbers WHERE FranchiseId = @FranchiseId)
	BEGIN
		UPDATE CRM.LeadNJobNumbers 
		SET OrderNumber = ISNULL(OrderNumber,0) + 1
		OUTPUT INSERTED.OrderNumber 
		WHERE FranchiseId = @FranchiseId
	END
	ELSE
	BEGIN
		INSERT INTO CRM.LeadNJobNumbers (FranchiseId) 
		OUTPUT INSERTED.OrderNumber
		VALUES(@FranchiseId)
	END
END