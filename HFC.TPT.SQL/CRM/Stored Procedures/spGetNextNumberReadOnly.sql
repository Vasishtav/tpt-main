﻿

-- =============================================
-- Author:		Murugan G.
-- Create date: 07/16/2018
-- Description:	Get the next number for Account, order, Invoice etc.. however don't update to the table,
-- =============================================

CREATE PROCEDURE [CRM].[spGetNextNumberReadOnly] 
				 @FranchiseId int, @numberType int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF EXISTS
	(
		SELECT *
		FROM [CRM].[TPTNumbers]
		WHERE FranchiseId = @FranchiseId
	)
	BEGIN
		IF(@numberType = 1)
		BEGIN
			SELECT AccountNumber + 1 as nextNumber
			from [CRM].[TPTNumbers]
			WHERE FranchiseId = @FranchiseId;
		END;
		IF(@numberType = 2)
		BEGIN
			SELECT QuoteNumber + 1 as nextNumber
			from [CRM].[TPTNumbers]
			WHERE FranchiseId = @FranchiseId;
		END;
		IF(@numberType = 3)
		BEGIN
			SELECT OrderNumber + 1 as nextNumber
			from [CRM].[TPTNumbers]
			WHERE FranchiseId = @FranchiseId;
		END;
		IF(@numberType = 4)
		BEGIN
			SELECT InvoiceNumber + 1 as nextNumber
			from [CRM].[TPTNumbers]
			WHERE FranchiseId = @FranchiseId;
		END;
		IF(@numberType = 5)
		BEGIN
			SELECT PONumber + 1 as nextNumber
			from [CRM].[TPTNumbers]
			WHERE FranchiseId = @FranchiseId;
		END;
	END;
	ELSE
	BEGIN
		SELECT 1000 as nextNumber
	END;
END;

