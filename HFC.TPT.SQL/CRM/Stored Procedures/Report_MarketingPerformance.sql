CREATE PROCEDURE [CRM].[Report_MarketingPerformance]  (  
@FranchiseId int,  
@SourceId int=null,  
@campaignId int=null,  
@CommercialTypeId int=null,  
@IndustryId int=null,  
@Zip varchar(100)=null,  
@PersonId varchar(max)=null,  
@startdate datetime= null,  
@enddate datetime= null )  
as  
BEGIN  
  
DECLARE @Sql NVARCHAR(MAX)  
  
DECLARE @Sqltemp VARCHAR(MAX), @condition1 VARCHAR(MAX),@condition2 VARCHAR(MAX),@condition3 VARCHAR(MAX);  
  
 set @condition1=''  
 set @condition2=''  
 set @condition3=''  
  
   if(@SourceId is not null)  
   set @condition1=' and s.SourceId=@SourceId '  
  
   if(@campaignId is not null)  
   set @condition1=@condition1 + ' and cam.CampaignId=@campaignId '      
    
   if(@CommercialTypeId is not null )  
   set @condition2=@condition2 + ' and CommercialTypeId=@CommercialTypeId'  
  
    if(@IndustryId is not null )  
   set @condition2=@condition2 + ' and IndustryId=@IndustryId'  
  
    if(@Zip is not null and @Zip!='')  
   set @condition3=@condition3 + ' and ZipCode=@Zip'  
    
set @Sqltemp=' select   
  --[Owner], [Channel], [Source]  
  [Owner]+''/''+[Channel]+''/''+[Source] Source  
  ,Campaign,ParentCampaign  
  ,Leads,Accounts,Opportunities   
  ,AdSpend  
  ,isnull((CAST(AdSpend as float)/NULLIF(CAST(Leads as float),0)),0.0) as CostperRawLead   
  ,Appointments  
  ,isnull((cast(Appointments as float) / NULLIF(Leads,0.0))*100,0.0) as AppointmentRatio  
  ,AppointmentsCancelled  
  ,isnull((cast(AppointmentsCancelled as float) / NULLIF(Appointments,0.0))*100,0.0) as AppointmentsCancelledRatio  
  ,Orders  
  ,isnull((cast(Orders as float) / NULLIF(Appointments,0.0))*100,0.0) as ClosingRatio  
  ,isnull(TotalSales,0.0) as TotalSales  
  ,isnull((CAST(TotalSales as float)/NULLIF(CAST(AdSpend as float),0)),0.0) as ForROAI  
  ,TotalSales-AdSpend as TotalROAI  
  ,isnull((CAST(TotalSales as float)/NULLIF(CAST(Orders as float),0)),0.0) as AvgSale  
 from (  
 select   
   ms.[Owner],ms.[Channel],ms.[Source]  
  ,ms.Campaign  
  ,'''' as ParentCampaign  
  ,ms.CampaignCost as AdSpend  
  ,(select count(1) from CRM.Leads l     
  join (select * from CRM.LeadSources where IsPrimarySource=1) ls on ls.LeadId=l.LeadId  
--  join CRM.SourcesTP stp on ls.SourceId=stp.SourcesTPId  
--  join CRM.Sources s on stp.SourceId=s.SourceId  
  CROSS APPLY (SELECT top 1 ladd.AddressId,ladd.LeadId FROM CRM.LeadAddresses ladd  
  join CRM.Addresses ad on ladd.AddressId=ad.AddressId  
  where LeadId=l.LeadId '+@condition3+') lad  
  where l.FranchiseId=@FranchiseId '+@condition2+' and ls.SourceId=ms.SourcesTPId and isnull(l.CampaignId,0)=ms.CampaignId and CAST(l.CreatedOnUtc as date) between @startdate and @enddate ) as Leads  
   
  ,(select count(1) from CRM.Accounts acc  
  join (select * from CRM.AccountSources where IsPrimarySource=1) accs on accs.AccountId=acc.AccountId  
--  join CRM.SourcesTP stp on accs.SourceId=stp.SourcesTPId  
--  join CRM.Sources s on stp.SourceId=s.SourceId  
  CROSS APPLY (SELECT top 1 aadd.AddressId,aadd.AccountId FROM CRM.AccountAddresses aadd  
  join CRM.Addresses ad on aadd.AddressId=ad.AddressId  
  where AccountId=acc.AccountId '+@condition3+') aad  
  where acc.FranchiseId=@FranchiseId '+@condition2+' and accs.SourceId=ms.SourcesTPId and isnull(acc.CampaignId,0)=ms.CampaignId and CAST(acc.CreatedOnUtc as date) between @startdate and @enddate) as Accounts  
   
  ,(select count(1) from CRM.Opportunities op  
  join CRM.Accounts acc on op.AccountId=acc.AccountId  
  join (select * from CRM.OpportunitySources where IsPrimarySource=1) ops on ops.OpportunityId=op.OpportunityID  
--  join CRM.SourcesTP stp on ops.SourceId=stp.SourcesTPId  
--  join CRM.Sources s on stp.SourceId=s.SourceId  
  CROSS APPLY (SELECT top 1 aadd.AddressId,aadd.AccountId FROM CRM.AccountAddresses aadd  
  join CRM.Addresses ad on aadd.AddressId=ad.AddressId  
  where AccountId=acc.AccountId '+@condition3+') aad  
  where op.FranchiseId=@FranchiseId '+@condition2+' and ops.SourceId=ms.SourcesTPId and isnull(op.CampaignId,0)=ms.CampaignId and CAST(op.CreatedOnUtc as date) between @startdate and @enddate and (@PersonId is null or op.SalesAgentId in (select id from [CRM].[CSVToTable] (@PersonId)))) as Opportunities  
   
  ,(select count(1) from CRM.Orders o  
  join CRM.Opportunities op on o.OpportunityId=op.OpportunityId  
  join CRM.Accounts acc on op.AccountId=acc.AccountId  
  join (select * from CRM.AccountSources where IsPrimarySource=1) accs on accs.AccountId=acc.AccountId  
--  join CRM.SourcesTP stp on accs.SourceId=stp.SourcesTPId  
--  join CRM.Sources s on stp.SourceId=s.SourceId  
  CROSS APPLY (SELECT top 1 aadd.AddressId,aadd.AccountId FROM CRM.AccountAddresses aadd  
  join CRM.Addresses ad on aadd.AddressId=ad.AddressId  
  where AccountId=acc.AccountId '+@condition3+') aad  
  where op.FranchiseId=@FranchiseId and o.OrderStatus not in (6,9) '+@condition2+' and accs.SourceId=ms.SourcesTPId and isnull(acc.CampaignId,0)=ms.CampaignId and CAST(o.ContractedDate as date) between @startdate and @enddate and (@PersonId is null or op.
SalesAgentId in (select id from [CRM].[CSVToTable] (@PersonId)))) as Orders  
  
  ,(select isnull(sum(qld.Netcharge),0.0) from CRM.Orders o  
  join CRM.Opportunities op on o.OpportunityId=op.OpportunityId  
  join CRM.Quote q on o.QuoteKey=q.QuoteKey AND q.PrimaryQuote=1  
  JOIN CRM.QuoteLines ql ON ql.QuoteKey = q.QuoteKey AND ql.ProductTypeId not in (4)  
  JOIN CRM.QuoteLineDetail qld on ql.QuoteLineId=qld.QuoteLineId  
  join CRM.Accounts acc on op.AccountId=acc.AccountId  
  join (select * from CRM.AccountSources where IsPrimarySource=1) accs on accs.AccountId=acc.AccountId  
--  join CRM.SourcesTP stp on accs.SourceId=stp.SourcesTPId  
--  join CRM.Sources s on stp.SourceId=s.SourceId  
  CROSS APPLY (SELECT top 1 aadd.AddressId,aadd.AccountId FROM CRM.AccountAddresses aadd  
  join CRM.Addresses ad on aadd.AddressId=ad.AddressId  
  where AccountId=acc.AccountId '+@condition3+') aad  
  where op.FranchiseId=@FranchiseId and o.OrderStatus not in (6,9) '+@condition2+' and accs.SourceId=ms.SourcesTPId and isnull(acc.CampaignId,0)=ms.CampaignId and CAST(o.ContractedDate as date) between @startdate and @enddate and (@PersonId is null or op.
SalesAgentId in (select id from [CRM].[CSVToTable] (@PersonId)))  
  ) as TotalSales   
  
  ,(select count(1) from (  
  SELECT c.CalendarId FROM CRM.Leads l  
  join (select * from CRM.LeadSources where IsPrimarySource=1) ls on ls.LeadId=l.LeadId  
--  join CRM.SourcesTP stp on ls.SourceId=stp.SourcesTPId  
--  join CRM.Sources s on stp.SourceId=s.SourceId  
  CROSS APPLY (SELECT top 1 CalendarId FROM CRM.Calendar where LeadId=l.LeadId) c  
  CROSS APPLY (SELECT top 1 ladd.AddressId,ladd.LeadId FROM CRM.LeadAddresses ladd  
  join CRM.Addresses ad on ladd.AddressId=ad.AddressId  
  where LeadId=l.LeadId '+@condition3+') lad  
  where l.FranchiseId=@FranchiseId '+@condition2+' and ls.SourceId=ms.SourcesTPId and isnull(l.CampaignId,0)=ms.CampaignId and CAST(l.CreatedOnUtc as date) between @startdate and @enddate  
  union  
  select c.CalendarId from CRM.Accounts acc  
  join (select * from CRM.AccountSources where IsPrimarySource=1) accs on accs.AccountId=acc.AccountId  
--  join CRM.SourcesTP stp on accs.SourceId=stp.SourcesTPId  
--  join CRM.Sources s on stp.SourceId=s.SourceId  
  CROSS APPLY (SELECT top 1 CalendarId FROM CRM.Calendar where AccountId=acc.AccountId) c  
  CROSS APPLY (SELECT top 1 aadd.AddressId,aadd.AccountId FROM CRM.AccountAddresses aadd  
  join CRM.Addresses ad on aadd.AddressId=ad.AddressId  
  where AccountId=acc.AccountId '+@condition3+') aad  
  where acc.FranchiseId=@FranchiseId '+@condition2+' and accs.SourceId=ms.SourcesTPId and isnull(acc.CampaignId,0)=ms.CampaignId and CAST(acc.CreatedOnUtc as date) between @startdate and @enddate  
  )as app) as Appointments  
  
  ,(select count(1) from (  
  SELECT c.CalendarId FROM CRM.Leads l  
  join (select * from CRM.LeadSources where IsPrimarySource=1) ls on ls.LeadId=l.LeadId  
--  join CRM.SourcesTP stp on ls.SourceId=stp.SourcesTPId  
--  join CRM.Sources s on stp.SourceId=s.SourceId  
  CROSS APPLY (SELECT top 1 CalendarId FROM CRM.Calendar where LeadId=l.LeadId and isDeleted=1) c  
  CROSS APPLY (SELECT top 1 ladd.AddressId,ladd.LeadId FROM CRM.LeadAddresses ladd  
  join CRM.Addresses ad on ladd.AddressId=ad.AddressId  
  where LeadId=l.LeadId '+@condition3+') lad  
  where l.FranchiseId=@FranchiseId '+@condition2+' and ls.SourceId=ms.SourcesTPId and isnull(l.CampaignId,0)=ms.CampaignId and CAST(l.CreatedOnUtc as date) between @startdate and @enddate   
  union  
  select c.CalendarId from CRM.Accounts acc  
  join (select * from CRM.AccountSources where IsPrimarySource=1) accs on accs.AccountId=acc.AccountId  
--  join CRM.SourcesTP stp on accs.SourceId=stp.SourcesTPId  
--  join CRM.Sources s on stp.SourceId=s.SourceId  
  CROSS APPLY (SELECT top 1 CalendarId FROM CRM.Calendar where AccountId=acc.AccountId and isDeleted=1) c  
  CROSS APPLY (SELECT top 1 aadd.AddressId,aadd.AccountId FROM CRM.AccountAddresses aadd  
  join CRM.Addresses ad on aadd.AddressId=ad.AddressId  
  where AccountId=acc.AccountId '+@condition3+') aad  
  where acc.FranchiseId=@FranchiseId '+@condition2+' and accs.SourceId=ms.SourcesTPId and isnull(acc.CampaignId,0)=ms.CampaignId and CAST(acc.CreatedOnUtc as date) between @startdate and @enddate   
  )as app) as AppointmentsCancelled  
  
 from (  
select distinct  
     stp.SourcesTPId  
    ,o.[Name] [Owner], c.[Name] [Channel], s.[Name] [Source]  
    ,isnull(cam.CampaignId,0) as CampaignId  
    ,isnull(cam.Name,'''') as Campaign  
    ,isnull(cam.Amount,0) as CampaignCost  
  from ( select distinct SourceId from CRM.LeadSources ls  
    join CRM.Leads l on l.LeadId=ls.LeadId  
    where l.FranchiseId=@FranchiseId) as lsource  
  join CRM.SourcesTP stp on lsource.SourceId=stp.SourcesTPId  
  join CRM.Sources s on stp.SourceId=s.SourceId  
  join CRM.Type_Owner o on stp.OwnerId = o.OwnerID  
  join crm.Type_Channel c on c.ChannelID=stp.ChannelID  
  left join CRM.Campaign cam on s.SourceId=cam.SourcesTPId and cam.FranchiseId=@FranchiseId  
  where 1=1 '+@condition1+'  
  union  
  select  
    stp.SourcesTPId  
   ,o.[Name] [Owner], c.[Name] [Channel], s.[Name] [Source]  
   ,isnull(cam.CampaignId,0) as CampaignId  
   ,isnull(cam.Name,'''') as Campaign  
   ,isnull(cam.Amount,0) as CampaignCost  
  from ( select distinct SourceId from CRM.AccountSources accs  
     join CRM.Accounts acc on accs.AccountId=acc.AccountId  
     where acc.FranchiseId=@FranchiseId) as Accsource  
  join CRM.SourcesTP stp on Accsource.SourceId=stp.SourcesTPId  
  join CRM.Sources s on stp.SourceId=s.SourceId  
  join CRM.Type_Owner o on stp.OwnerId = o.OwnerID  
  join crm.Type_Channel c on c.ChannelID=stp.ChannelID  
  left join CRM.Campaign cam on s.SourceId=cam.SourcesTPId and cam.FranchiseId=@FranchiseId  
  where 1=1 '+@condition1+'  
  union  
  select  
    stp.SourcesTPId  
   ,o.[Name] [Owner], c.[Name] [Channel], s.[Name] [Source]  
   ,isnull(cam.CampaignId,0) as CampaignId,isnull(cam.Name,'''') as Campaign  
   ,isnull(cam.Amount,0) as CampaignCost  
  from( select distinct SourceId, campaignID from CRM.LeadSources ls  
    join CRM.Leads l on l.LeadId=ls.LeadId  
    where l.FranchiseId=@FranchiseId) as lsource  
  join CRM.SourcesTP stp on lsource.SourceId=stp.SourcesTPId  
  join CRM.Sources s on stp.SourceId=s.SourceId  
  join CRM.Type_Owner o on stp.OwnerId = o.OwnerID  
  join crm.Type_Channel c on c.ChannelID=stp.ChannelID  
  left join CRM.Campaign cam on lsource.SourceId=cam.CampaignID and cam.FranchiseId=@FranchiseId  
  where 1=1 '+@condition1+'  
  union  
  select  
   stp.SourcesTPId  
   ,o.[Name] [Owner], c.[Name] [Channel], s.[Name] [Source]  
   ,isnull(cam.CampaignId,0) as CampaignId  
   ,isnull(cam.Name,'''') as Campaign  
   ,isnull(cam.Amount,0) as CampaignCost  
  from ( select distinct SourceId, campaignID from CRM.AccountSources accs  
     join CRM.Accounts acc on accs.AccountId=acc.AccountId  
     where acc.FranchiseId=@FranchiseId) as Accsource  
  join CRM.SourcesTP stp on Accsource.SourceId=stp.SourcesTPId  
  join CRM.Sources s on stp.SourceId=s.SourceId  
  join CRM.Type_Owner o on stp.OwnerId = o.OwnerID  
  join crm.Type_Channel c on c.ChannelID=stp.ChannelID  
  left join CRM.Campaign cam on AccSource.CampaignID=cam.CampaignID and cam.FranchiseId=@FranchiseId  
  where 1=1 '+@condition1+'  
  ) as ms  
 ) as leadsource order by 1 asc'  
  
 print @Sqltemp  
 set @Sql=Cast(@Sqltemp as nvarchar(max))  
  
  
 EXECUTE sp_executesql @Sql,N'@FranchiseId int,  
@SourceId int,  
@campaignId int,  
@CommercialTypeId int,  
@IndustryId int null,  
@Zip varchar(100),  
@PersonId varchar(max),  
@startdate datetime,  
@enddate datetime',  
@FranchiseId =@FranchiseId,  
@SourceId =@SourceId,  
@campaignId =@campaignId,  
@CommercialTypeId =@CommercialTypeId,  
@IndustryId =@IndustryId,  
@Zip =@Zip,  
@PersonId =@PersonId,  
@startdate =@startdate,  
@enddate =@enddate  
   
     
END  
  