﻿CREATE PROCEDURE [CRM].[LeadSearch]
(
@FranchiseId int,
@LostLead bit
)
as
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

--If  LostLead=0 means avoid lost lead in select query
if (@LostLead = 0)
Begin

 SELECT 
   case when lead.TerritoryId is not null then tr.Name else lead.TerritoryType end as Territory
     ,lead.[LeadId]   
	,lead.IsCommercial    
	,lead.[CreatedOnUtc]
    ,[Address1]
    ,[Address2]
    ,[City]
    ,[State]
    ,[ZipCode]
    ,p.[FirstName]
    ,p.[LastName]
    ,p.[PrimaryEmail]
    ,p.CompanyName
    ,tls.Name AS Status
    ,c.Name AS Campaign
    ,sourcechannel.Name + ' - '+ sources.Name AS Source
    ,sourcechannel.Name AS Channel
	, Case When p.PreferredTFN  = 'C' then p.CellPhone
			When p.PreferredTFN  = 'W' then p.WorkPhone
			When p.PreferredTFN = 'H' then p.HomePhone
			ELSE '' 
		End as DisplayPhone
 FROM crm.Leads lead 
 INNER JOIN CRM.Type_LeadStatus tls ON tls.id = lead.leadstatusid
 --Address
 INNER JOIN CRM.LeadAddresses lad on lad.LeadId=lead.LeadId and lad.IsPrimaryAddress=1
 INNER JOIN [CRM].[Addresses] a on a.[AddressId]=lad.AddressId
 --Customer
 INNER JOIN CRM.LeadCustomers lc on lc.LeadId = lead.LeadId and lc.IsPrimaryCustomer = 1
 INNER JOIN [CRM].[Customer] p on p.CustomerId=lc.CustomerId
 LEFT JOIN [CRM].[Customer] secondPerson on secondPerson.CustomerId = lead.SecPersonId
 -- Based on Campaign
 left join crm.campaign c on c.CampaignId = lead.campaignid
 --Based on source
 left join [CRM].[LeadSources] lsource on lead.LeadId=lsource.LeadId and lsource.IsPrimarySource=1
 left join [CRM].[SourcesTP] stpSource on lsource.SourceId=stpSource.SourcesTPId
 left join [CRM].[Sources] sources on  stpSource.SourceId=sources.SourceId
 left join crm.type_channel sourcechannel on sourcechannel.ChannelId = stpSource.ChannelId
 -- territory
 LEFT JOIN CRM.Territories tr on tr.TerritoryId=lead.TerritoryId
  WHERE lead.LeadStatusId<>31422 AND lead.leadstatusid <> 2 AND lead.leadstatusid <> 13 AND lead.FranchiseId= @FranchiseId AND ISNULL(lead.IsDeleted,0)=0
 order by lead.[CreatedOnUtc] desc, [LastUpdatedOnUtc] desc
 End
 else
 Begin
 SELECT 
 case when lead.TerritoryId is not null then tr.Name else lead.TerritoryType end as Territory
     ,lead.[LeadId] 
	 ,lead.IsCommercial   
	,lead.[CreatedOnUtc]
    ,[Address1]
    ,[Address2]
    ,[City]
    ,[State]
    ,[ZipCode]
    ,p.[FirstName]
    ,p.[LastName]
    ,p.[PrimaryEmail]
    ,p.CompanyName
    ,tls.Name AS Status
    ,c.Name AS Campaign
    ,sourcechannel.Name + ' - '+ sources.Name AS Source
    ,sourcechannel.Name AS Channel
	, Case When p.PreferredTFN  = 'C' then p.CellPhone
			When p.PreferredTFN  = 'W' then p.WorkPhone
			When p.PreferredTFN = 'H' then p.HomePhone
			ELSE '' 
		End as DisplayPhone
 FROM crm.Leads lead 
 INNER JOIN CRM.Type_LeadStatus tls ON tls.id = lead.leadstatusid
  --Address
 INNER JOIN CRM.LeadAddresses lad on lad.LeadId=lead.LeadId and lad.IsPrimaryAddress=1
 INNER JOIN [CRM].[Addresses] a on a.[AddressId]=lad.AddressId
 --Customer
 INNER JOIN CRM.LeadCustomers lc on lc.LeadId = lead.LeadId and lc.IsPrimaryCustomer = 1
 INNER JOIN [CRM].[Customer] p on p.CustomerId=lc.CustomerId
 LEFT JOIN [CRM].[Customer] secondPerson on secondPerson.CustomerId = lead.SecPersonId
 -- Based on Campaign
 left join crm.campaign c on c.CampaignId = lead.campaignid
 --Based on source
 left join [CRM].[LeadSources] lsource on lead.LeadId=lsource.LeadId and lsource.IsPrimarySource=1
 left join [CRM].[SourcesTP] stpSource on lsource.SourceId=stpSource.SourcesTPId
 left join [CRM].[Sources] sources on  stpSource.SourceId=sources.SourceId
 left join crm.type_channel sourcechannel on sourcechannel.ChannelId = stpSource.ChannelId
 LEFT JOIN CRM.Territories tr on tr.TerritoryId=lead.TerritoryId
  WHERE lead.LeadStatusId<>31422 AND lead.FranchiseId= @FranchiseId AND ISNULL(lead.IsDeleted,0)=0
 order by lead.[CreatedOnUtc] desc, [LastUpdatedOnUtc] desc

 End
GO

