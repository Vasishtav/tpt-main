﻿
CREATE procedure [CRM].[Report_VendorSalesPerformance] 
@FranchiseId int,
@PersonId varchar(max)=null,
@VendorID int=null,
@startdate datetime=null,
@enddate datetime=null,
@ProductCategory varchar(64) = 'All'

as

Begin;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
With Sales as (
	select  coalesce(v.Name, ql.VendorName, '*One Time Products') VendorName
		,	YEAR(cast (o.ContractedDate as Date)) [Year]
		,	Datepart(MONTH,cast(o.ContractedDate as Date)) [Mth]
		,	sum(qld.Quantity * qld.BaseCost) COGS
		,	sum(qld.NetCharge) TotalSales
	from CRM.Opportunities opp
--	join CRM.Accounts a ON opp.AccountId = a.AccountId
--	join CRM.AccountCustomers acc on a.AccountId =acc.AccountId and acc.IsPrimaryCustomer=1
--	join CRM.Customer p ON p.CustomerId = acc.CustomerId 
	join CRM.Person sa ON sa.PersonId = opp.SalesAgentId 
	join CRM.Quote q on opp.opportunityid = q.OpportunityId and q.PrimaryQuote=1
	join CRM.QuoteLines ql ON ql.QuoteKey = q.QuoteKey AND ql.ProductTypeId not in (4)
	join CRM.QuoteLineDetail qld on ql.QuoteLineId=qld.QuoteLineId
	join CRM.Orders o on q.QuoteKey=o.QuoteKey
 --		left JOIN CRM.MasterPurchaseOrder mpo ON o.OrderId = mpo.OrderID
 --		left JOIN CRM.PurchaseOrderDetails pod ON pod.PurchaseOrderId = mpo.PurchaseOrderId and pod.QuoteLineId = ql.QuoteLineId
 -- 	left join CRM.ShipNotice sn ON sn.PurchaseOrdersDetailId = pod.PurchaseOrdersDetailId
 -- 	left join CRM.Territories tr on tr.TerritoryId=opp.TerritoryId
 -- 	left join CRM.HFCProducts Hprd ON Hprd.ProductID=ql.ProductId
  	left join CRM.Type_ProductCategory pc	ON ql.ProductCategoryId = pc.ProductCategoryId
-- 	left join CRM.Type_ProductCategory psc	ON ql.ProductSubCategoryId = psc.ProductCategoryId
 -- 	left join CRM.FranchiseProducts fp ON fp.ProductKey = ql.ProductID and fp.FranchiseId = a.FranchiseId
 -- 	left join CRM.Type_ProductCategory fpc ON	 fp.ProductCategory = fpc.ProductCategoryId
	left join Acct.HFCVendors v ON ql.VendorId = v.VendorID
	where	opp.FranchiseId = @FranchiseID
	and		isnull(ql.VendorID, 0) = coalesce(@VendorID, ql.VendorID, 0)
	and		isnull(o.OrderStatus, 0) not in (6,9)
	and		isnull(ql.ProductTypeID, 0) not in (4)
	and		(@PersonId  is null or opp.SalesAgentId in (select id from [CRM].[CSVToTable] (@PersonId)))
	and		(isnull(@ProductCategory, 'All') = 'All' or @ProductCategory = pc.ProductCategory)
	and		cast(o.ContractedDate as date) between @startdate and @enddate
	group by coalesce(v.Name, ql.VendorName, '*One Time Products')
		,	YEAR(cast (o.ContractedDate as Date))
		,	Datepart(MONTH,cast(o.ContractedDate as Date)))
select dbo.ProperCase(VendorName) VendorName
	,isnull(@ProductCategory, 'All') ProductCategory
	,sum(TotalSales) [YTD_Sales]
	,sum(case when [Mth] = 1 then Cogs else 0 end) [COGS_Jan]
	,sum(case when [Mth] = 1 then TotalSales else 0 end) [TotSale_Jan]
	,sum(case when [Mth] = 1 then TotalSales else 0 end) - sum(case when [Mth] = 1 then Cogs else 0 end) as [GPValue_Jan]
	,isnull((sum(case when [Mth] = 1 then TotalSales else 0 end) - sum(case when [Mth] = 1 then Cogs else 0 end))/NULLIF(sum(case when [Mth] = 1 then TotalSales else 0 end),0),0)*100 as [GPPerc_Jan]

	,sum(case when [Mth] = 2 then Cogs else 0 end) [COGS_Feb]
	,sum(case when [Mth] = 2 then TotalSales else 0 end) [TotSale_Feb]
	,sum(case when [Mth] = 2 then TotalSales else 0 end) - sum(case when [Mth] = 2 then Cogs else 0 end) as [GPValue_Feb]
	,isnull((sum(case when [Mth] = 2 then TotalSales else 0 end) - sum(case when [Mth] = 2 then Cogs else 0 end))/NULLIF(sum(case when [Mth] = 2 then TotalSales else 0 end),0),0)*100 as [GPPerc_Feb]

	,sum(case when [Mth] = 3 then Cogs else 0 end) [COGS_Mar]
	,sum(case when [Mth] = 3 then TotalSales else 0 end) [TotSale_Mar]
	,sum(case when [Mth] = 3 then TotalSales else 0 end) - sum(case when [Mth] = 3 then Cogs else 0 end) as [GPValue_Mar]
	,isnull((sum(case when [Mth] = 3 then TotalSales else 0 end) - sum(case when [Mth] = 3 then Cogs else 0 end))/NULLIF(sum(case when [Mth] = 3 then TotalSales else 0 end),0),0)*100 as [GPPerc_Mar]

	,sum(case when [Mth] = 4 then Cogs else 0 end) [COGS_Apr]
	,sum(case when [Mth] = 4 then TotalSales else 0 end) [TotSale_Apr]
	,sum(case when [Mth] = 4 then TotalSales else 0 end) - sum(case when [Mth] = 4 then Cogs else 0 end) as [GPValue_Apr]
	,isnull((sum(case when [Mth] = 4 then TotalSales else 0 end) - sum(case when [Mth] = 4 then Cogs else 0 end))/NULLIF(sum(case when [Mth] = 4 then TotalSales else 0 end),0),0)*100 as [GPPerc_Apr]

	,sum(case when [Mth] = 5 then Cogs else 0 end) [COGS_May]
	,sum(case when [Mth] = 5 then TotalSales else 0 end) [TotSale_May]
	,sum(case when [Mth] = 5 then TotalSales else 0 end) - sum(case when [Mth] = 5 then Cogs else 0 end) as [GPValue_May]
	,isnull((sum(case when [Mth] = 5 then TotalSales else 0 end) - sum(case when [Mth] = 5 then Cogs else 0 end))/NULLIF(sum(case when [Mth] = 5 then TotalSales else 0 end),0),0)*100 as [GPPerc_May]

	,sum(case when [Mth] = 6 then Cogs else 0 end) [COGS_Jun]
	,sum(case when [Mth] = 6 then TotalSales else 0 end) [TotSale_Jun]
	,sum(case when [Mth] = 6 then TotalSales else 0 end) - sum(case when [Mth] = 6 then Cogs else 0 end) as [GPValue_Jun]
	,isnull((sum(case when [Mth] = 6 then TotalSales else 0 end) - sum(case when [Mth] = 6 then Cogs else 0 end))/NULLIF(sum(case when [Mth] = 6 then TotalSales else 0 end),0),0)*100 as [GPPerc_Jun]

	,sum(case when [Mth] = 7 then Cogs else 0 end) [COGS_Jul]
	,sum(case when [Mth] = 7 then TotalSales else 0 end) [TotSale_Jul]
	,sum(case when [Mth] = 7 then TotalSales else 0 end) - sum(case when [Mth] = 7 then Cogs else 0 end) as [GPValue_Jul]
	,isnull((sum(case when [Mth] = 7 then TotalSales else 0 end) - sum(case when [Mth] = 7 then Cogs else 0 end))/NULLIF(sum(case when [Mth] = 7 then TotalSales else 0 end),0),0)*100 as [GPPerc_Jul]

	,sum(case when [Mth] = 8 then Cogs else 0 end) [COGS_Aug]
	,sum(case when [Mth] = 8 then TotalSales else 0 end) [TotSale_Aug]
	,sum(case when [Mth] = 8 then TotalSales else 0 end) - sum(case when [Mth] = 8 then Cogs else 0 end) as [GPValue_Aug]
	,isnull((sum(case when [Mth] = 8 then TotalSales else 0 end) - sum(case when [Mth] = 8 then Cogs else 0 end))/NULLIF(sum(case when [Mth] = 8 then TotalSales else 0 end),0),0)*100 as [GPPerc_Aug]

	,sum(case when [Mth] = 9 then Cogs else 0 end) [COGS_Sep]
	,sum(case when [Mth] = 9 then TotalSales else 0 end) [TotSale_Sep]
	,sum(case when [Mth] = 9 then TotalSales else 0 end) - sum(case when [Mth] = 9 then Cogs else 0 end) as [GPValue_Sep]
	,isnull((sum(case when [Mth] = 9 then TotalSales else 0 end) - sum(case when [Mth] = 9 then Cogs else 0 end))/NULLIF(sum(case when [Mth] = 9 then TotalSales else 0 end),0),0)*100 as [GPPerc_Sep]

	,sum(case when [Mth] = 10 then Cogs else 0 end) [COGS_Oct]
	,sum(case when [Mth] = 10 then TotalSales else 0 end) [TotSale_Oct]
	,sum(case when [Mth] = 10 then TotalSales else 0 end) - sum(case when [Mth] = 10 then Cogs else 0 end) as [GPValue_Oct]
	,isnull((sum(case when [Mth] = 10 then TotalSales else 0 end) - sum(case when [Mth] = 10 then Cogs else 0 end))/NULLIF(sum(case when [Mth] = 10 then TotalSales else 0 end),0),0)*100 as [GPPerc_Oct]

	,sum(case when [Mth] = 11 then Cogs else 0 end) [COGS_Nov]
	,sum(case when [Mth] = 11 then TotalSales else 0 end) [TotSale_Nov]
	,sum(case when [Mth] = 11 then TotalSales else 0 end) - sum(case when [Mth] = 11 then Cogs else 0 end) as [GPValue_Nov]
	,isnull((sum(case when [Mth] = 11 then TotalSales else 0 end) - sum(case when [Mth] = 11 then Cogs else 0 end))/NULLIF(sum(case when [Mth] = 11 then TotalSales else 0 end),0),0)*100 as [GPPerc_Nov]

	,sum(case when [Mth] = 12 then Cogs else 0 end) [COGS_Dec]
	,sum(case when [Mth] = 12 then TotalSales else 0 end) [TotSale_Dec]
	,sum(case when [Mth] = 12 then TotalSales else 0 end) - sum(case when [Mth] = 12 then Cogs else 0 end) as [GPValue_Dec]
	,isnull((sum(case when [Mth] = 12 then TotalSales else 0 end) - sum(case when [Mth] = 12 then Cogs else 0 end))/NULLIF(sum(case when [Mth] = 12 then TotalSales else 0 end),0),0)*100 as [GPPerc_Dec]

	,sum(Cogs) [COGS]
	,sum(TotalSales) [TotSale]
	,sum(TotalSales) - sum(Cogs) as [GPValue]
	,isnull((sum(TotalSales) - sum(Cogs))/NULLIF(sum(TotalSales),0),0)*100 as [GPPerc]


from Sales
group by dbo.ProperCase(VendorName)
having sum(TotalSales) > 0
order by sum(TotalSales) desc;

end