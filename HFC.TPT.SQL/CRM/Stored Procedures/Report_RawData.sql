
create procedure [CRM].[Report_RawData]
	(	@FranchiseID int = 3
	,	@StartDate datetime = '2019-04-01'
	,	@EndDate datetime = '2019-04-30' )
as
begin;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;


--declare @FranchiseID int = 3
--	,	@StartDate datetime = '2018-01-01'
--	,	@EndDate datetime = '2020-01-30';

with
	 LeadCalendar as
		(	select 	 LeadID
					,row_number() over (partition by LeadID order by StartDate) LeadIdx
					,Subject
					,Message
					,StartDate
				from CRM.Calendar
				where LeadID is not null )
	,LeadAddress as
		(	select	 ladd.AddressId
					,ladd.LeadId
					,row_number() over (partition by ladd.LeadID order by ladd.AddressID) LeadIdx
					,ad.Address1
					,ad.Address2
					,ad.City
					,ad.State
					,ad.ZipCode
					,ad.AttentionText
				from CRM.LeadAddresses ladd
				join CRM.Addresses ad
					on ladd.AddressId=ad.AddressId )
select		l.FranchiseId
	,	l.LeadID
	,	format(l.LeadNumber, '#') LeadNumber
	,	isnull(l.TerritoryType, '') LeadTerritory
	,	isnull(t.Code, '') LeadTerritoryCode
	,	isnull(format(cast(l.CreatedOnUtc as Date), 'MM/dd/yyyy'), '') LeadDate
	,	isnull(cu.FirstName, '') LeadFirstName
	,	isnull(cu.LastName, '') LeadLastName
	,	isnull(cu.CompanyName, '') LeadCompanyName
	,	isnull(cu.PrimaryEmail, '') LeadEMail
	,	isnull(lad.Address1, '') LeadAddress1
	,	isnull(lad.Address2, '') LeadAddress2
	,	isnull(lad.City, '') LeadCity
	,	isnull(lad.State, '') LeadState
	,	isnull(lad.ZipCode, '') LeadZipCode
	,	isnull(lad.AttentionText, '') LeadAttentionText
	,	isnull(s.Name,'') LeadSource
	,	isnull(ca.Name, '') LeadCampaign
	,	isnull(c.Subject, '') LeadApptSubject
	,	isnull(c.Message, '') LeadApptMessage
	,	isnull(format(c.StartDate, 'MM/dd/yyyy HH:mm'), '') LeadAppt
	,	isnull(lst.Name, '') LeadStatus
into #Leads
from CRM.Leads l
join (select * from CRM.LeadSources where IsPrimarySource=1) ls on ls.LeadId=l.LeadId
join CRM.SourcesTP stp
	on ls.SourceId=stp.SourcesTPId
join CRM.Sources s
	on stp.SourceId=s.SourceId
left join CRM.Campaign ca
	on ca.CampaignId = l.CampaignId
left join CRM.Type_LeadStatus lst
	on	lst.Id = l.LeadStatusId
left join LeadCalendar c
				on c.LeadId=l.LeadId
				and c.LeadIdx = 1
left join  LeadAddress lad
				on lad.LeadId = l.LeadID
				and lad.LeadIdx = 1
left join CRM.LeadCustomers lc
	on	lc.LeadID = l.LeadID
	and lc.IsPrimaryCustomer = 1
left join CRM.Customer cu
	on	cu.CustomerId = lc.CustomerId
left join CRM.Territories t
	on	t.TerritoryId = l.TerritoryId
where l.FranchiseID = @FranchiseID;

with
	 AccountCalendar as
		(	select 	 AccountId
					,row_number() over (partition by AccountID order by StartDate) AccountIdx
					,Subject
					,Message
					,StartDate
				from CRM.Calendar
				where AccountId is not null )
	,AccountAddress as
		(	select	 aadd.AddressId
					,aadd.AccountId
					,row_number() over (partition by aadd.AccountID order by aadd.AddressID) AccountIdx
					,ad.Address1
					,ad.Address2
					,ad.City
					,ad.State
					,ad.ZipCode
					,ad.AttentionText
				from CRM.AccountAddresses aadd
				join CRM.Addresses ad
					on aadd.AddressId=ad.AddressId )
select
		ac.FranchiseId
	,	ac.AccountID
	,	format(ac.AccountNumber, '#') AccountNumber
	,	ac.LeadID
	,	isnull(ac.TerritoryType, '') AccountTerritory
	,	isnull(t.Code, '') AccountTerritoryCode
	,	isnull(format(cast(ac.CreatedOnUtc as Date), 'MM/dd/yyyy'), '') AccountDate
	,	isnull(cu.FirstName, '') AccountFirstName
	,	isnull(cu.LastName, '') AccountLastName
	,	isnull(cu.CompanyName, '') AccountCompanyName
	,	isnull(cu.PrimaryEmail, '') AccountEMail
	,	isnull(ad.Address1, '') AccountAddress1
	,	isnull(ad.Address2, '') AccountAddress2
	,	isnull(ad.City, '') AccountCity
	,	isnull(ad.State, '') AccountState
	,	isnull(ad.ZipCode, '') AccountZipCode
	,	isnull(ad.AttentionText, '') AccountAttentionText
	,	isnull(s.Name, '') AccountSource
	,	isnull(ca.Name, '') AccountCampaign
	,	isnull(c.Subject, '') AccountApptSubject
	,	isnull(c.Message, '') AccountApptMessage
	,	isnull(format(c.StartDate, 'MM/dd/yyyy HH:mm'), '') AccountAppt
	,	isnull(acs.Name, '') AccountStatus
into #Accounts
from CRM.Accounts ac
join (select * from CRM.AccountSources where IsPrimarySource=1) aas
	on aas.AccountId=ac.AccountId
join CRM.SourcesTP stp
	on aas.SourceId=stp.SourcesTPId
join CRM.Sources s
	on stp.SourceId=s.SourceId
left join CRM.Campaign ca
	on ca.CampaignId = ac.CampaignId
left join AccountCalendar c
	on c.AccountId= ac.AccountId
	and c.AccountIdx = 1
left join AccountAddress ad
	on	ad.AccountId = ac.AccountId
	and ad.AccountIdx = 1
join CRM.AccountCustomers aac
	on	 aac.AccountId = ac.AccountId
	and	aac.IsPrimaryCustomer = 1
left join CRM.Customer cu
	on	cu.CustomerId = aac.CustomerID
left join CRM.Territories t
	on	t.TerritoryId = ac.TerritoryId
left join CRM.Type_AccountStatus acs
	on	ac.AccountStatusId = acs.AccountStatusId
where ac.FranchiseID = @FranchiseID;


with
	 OpportunityCalendar as
		(	select 	 OpportunityId
					,row_number() over (partition by OpportunityID order by StartDate) OpportunityIdx
					,Subject
					,Message
					,StartDate
				from CRM.Calendar
				where OpportunityID is not null )
	,OpportunityAddress as
		(	select	 oadd.AddressId
					,oadd.OpportunityId
					,row_number() over (partition by oadd.OpportunityID order by oadd.AddressID) OpportunityIdx
					,ad.Address1
					,ad.Address2
					,ad.City
					,ad.State
					,ad.ZipCode
					,ad.AttentionText
				from CRM.OpportunityAddresses oadd
				join CRM.Addresses ad
					on oadd.AddressId=ad.AddressId )
select 
		op.FranchiseId
	,	op.OpportunityID
	,	op.AccountID
	,	sa.PersonID SalesPersonID
	,	sa.FirstName+' '+sa.LastName as SalesPerson
	,	format(op.OpportunityNumber, '#') OpportunityNumber
	,	isnull(op.OpportunityName, '') OpportunityName
	,	isnull(op.TerritoryType, '') OpportunityTerritory
	,	isnull(t.Code, '') OpportunityTerritoryCode
	,	format(cast(op.CreatedOnUtc as Date), 'MM/dd/yyyy') OpportunityDate
	,	isnull(cu.FirstName, '') OpportunityFirstName
	,	isnull(cu.LastName, '') OpportunityLastName
	,	isnull(cu.CompanyName, '') OpportunityCompanyName
	,	isnull(cu.PrimaryEmail, '') OpportunityEMail
	,	isnull(oad.Address1, '') OpportunityAddress1
	,	isnull(oad.Address2, '') OpportunityAddress2
	,	isnull(oad.City, '') OpportunityCity
	,	isnull(oad.State, '') OpportunityState
	,	isnull(oad.ZipCode, '') OpportunityZipCode
	,	isnull(oad.AttentionText, '') OpportunityAttentionText
	,	isnull(s.Name, '') OpportunitySource
	,	isnull(ca.Name, '') OpportunityCampaign
	,	isnull(c.Subject, '') OpportunityApptSubject
	,	isnull(c.Message, '') OpportunityApptMessage
	,	isnull(format(c.StartDate, 'MM/dd/yyyy HH:mm'), '') OpportunityAppt
into #Opportunities
from CRM.Opportunities op
join CRM.Person sa ON sa.PersonId = op.SalesAgentId 
join (select * from CRM.OpportunitySources where IsPrimarySource=1) os
	on os.OpportunityId = op.OpportunityId
join CRM.SourcesTP stp
	on os.SourceId=stp.SourcesTPId
join CRM.Sources s
	on stp.SourceId=s.SourceId
left join CRM.Campaign ca
	on ca.CampaignId = op.CampaignId
left join OpportunityCalendar c
	on	op.OpportunityId = c.OpportunityId
	and	c.OpportunityIdx = 1
left join OpportunityAddress oad
	on	op.OpportunityId = oad.OpportunityId
	and oad.OpportunityIdx = 1
left join CRM.Customer cu
	on	cu.CustomerId = op.PersonId
left join CRM.Territories t
	on op.TerritoryId = t.TerritoryId
where op.FranchiseID = @FranchiseID;

select
			opp.FranchiseId
		,	isnull(t.Name, 'Gray Area') QuoteTerritory
		,	isnull(t.Code, '') QuoteTerritoryCode
		,	q.QuoteKey QuoteID
		,	q.OpportunityId
		,	format(q.QuoteID, '#') QuoteNumber
		,	ql.QuoteLineId
		,	format(ql.QuoteLineNumber, '#') as LineNumber
		,	isnull(o.OrderName, '') OrderName
		,	isnull(format(o.OrderNumber, '#'), '') OrderNumber
		,	cast(o.ContractedDate as Date) OrderDate
		,	coalesce(vn.Name, ql.VendorName) VendorName
		,	dbo.ProperCase (	case when ql.ProductTypeId= 1 then Hprd.ProductGroupDesc
				when ql.ProductTypeID = 5 then pc.ProductCategory + '-' + psc.ProductCategory
				else fpc.ProductCategory end) ProductCategory
		,	ql.ProductName
		,	ql.ModelDescription ProductModel
		,	coalesce(nullif(col.ColorValueDescription, ''), col.ValueDescription) Color
		,	fab.ValueDescription Fabric
		,	ql.Description
		,	isnull(format(qld.BaseCost, '#,##0.00'), '') ItemCost
		,	isnull(format(qld.Quantity, '#,##0.00'), '') Qty
		,	isnull(format(qld.BaseCost * qld.Quantity, '#,##0.00'), '') as LineCost
		,	isnull(format(qld.Netcharge, '#,##0.00'), '') as LineValue
		,	isnull(format(qld.SuggestedResale, '#,##0.00'), '') SuggestedResale
		,	isnull(format(qld.DiscountAmount, '#,##0.00'), '') DiscountAmount
		,	isnull(format(qld.MarginPercentage , '#.##'), '') MarginPercentage
		,	format(cast(q.CreatedOn as date), 'MM/dd/yyyy') QuoteDate
		,	case q.PrimaryQuote when 1 then 'Y' else 'N' end PrimaryQuote
		,	isnull(qs.QuoteStatus, '') QuoteStatus
		,	isnull(os.OrderStatus, '') OrderStatus
		,	isnull(pt.ProductType, '') ProductType
		,	case vn.isAlliance when 1 then 'Y' else 'N' end isAlliance
		,	isnull(format(mpo.CreateDate, 'MM/dd/yyyy'), '') MPODate
		,	isnull(format(pod.CreatedOn, 'MM/dd/yyyy'), '') VPODate
		,	isnull(format(pod.PromiseByDate, 'MM/dd/yyyy'), '') PromiseByDate
		,	isnull(format(pod.ShipDate, 'MM/dd/yyyy'), '') EstShipDate
		,	isnull(format(sn.ShippedDate, 'MM/dd/yyyy'), '') ShipDate
		,	isnull(sd.TrackingId, '') TrackingNumber
		,	isnull(format(vi.[Date], 'MM/dd/yyyy'), '') InvoiceDate
		,	isnull(format(sn.ReceivedCompleteDate, 'MM/dd/yyyy'), '') ReceivedDate
		,	isnull(dbo.propercase(pod.Status), '') VPOStatus
		,	case when o.OrderID is not null and ql.ProductTypeId not in (4) and o.OrderStatus not in (6,9) then 1 else 0 end RevenueCounted
into #Quotes
from CRM.Opportunities opp
join CRM.Quote q
	on q.OpportunityId = opp.OpportunityId
join CRM.Type_QuoteStatus qs
	on	qs.QuoteStatusId = q.QuoteStatusId
join CRM.QuoteLines ql
	on ql.QuoteKey = q.QuoteKey
join CRM.QuoteLineDetail qld
	on qld.QuoteLineId = ql.QuoteLineID
join CRM.Type_ProductType pt
	on ql.ProductTypeID = pt.Id
join CRM.Accounts a
	on opp.AccountId = a.AccountId
join CRM.Person sa ON sa.PersonId = opp.SalesAgentId 
left join Acct.HFCVendors vn
	on vn.VendorID = ql.VendorID
left join CRM.HFCProducts Hprd
	on Hprd.ProductID=ql.ProductId
left join CRM.Type_ProductCategory pc
	on ql.ProductCategoryId = pc.ProductCategoryId
left join CRM.Type_ProductCategory psc
	on ql.ProductSubCategoryId = psc.ProductCategoryId
left join CRM.FranchiseProducts fp
	on fp.ProductKey = ql.ProductID and fp.FranchiseId = a.FranchiseId
left join CRM.Type_ProductCategory fpc
	on fp.ProductCategory = fpc.ProductCategoryId
left join CRM.QuoteCoreProductDetails col
	on ql.QuoteLineId = col.QuotelineID
	and col.Description = 'Color'
left join CRM.QuoteCoreProductDetails fab
	on ql.QuoteLineId = fab.QuotelineID
	and fab.Description = 'Fabric'
left join CRM.Orders o
	on q.QuoteKey = o.QuoteKey
left join CRM.Type_OrderStatus os
	on	o.OrderStatus = os.OrderStatusId
left join crm.MasterPurchaseOrderExtn mpox
	on mpox.OrderId = o.OrderID  --mpox.PurchaseOrderId = mpo.PurchaseOrderId
left join CRM.MasterPurchaseOrder mpo 
	on mpo.PurchaseOrderId = mpox.PurchaseOrderId 	--mpo.OrderId = o.OrderID
left join CRM.PurchaseOrderDetails pod
	on	pod.PurchaseOrderId = mpo.PurchaseOrderId
	and pod.QuoteLineId = ql.QuoteLineID
left join crm.ShipmentDetail sd
	on ql.quoteLineID = sd.QuoteLineID
left join crm.ShipNotice sn
	on sn.ShipNoticeID = sd.ShipNoticeID
left join crm.VendorInvoice vi
	on vi.PurchaseOrdersDetailId = pod.PurchaseOrdersDetailId
left join crm.Territories t
	on opp.TerritoryId = t.TerritoryId
where opp.FranchiseID = @FranchiseID
union all
select
			opp.FranchiseId
		,	isnull(t.Name, 'Gray Area') QuoteTerritory
		,	isnull(t.Code, '') QuoteTerritoryCode
		,	q.QuoteKey QuoteID
		,	q.OpportunityId
		,	format(q.QuoteID, '#') QuoteNumber
		,	0 QuoteLineId
		,	format(0, '#') as LineNumber
		,	isnull(o.OrderName, '') OrderName
		,	isnull(format(o.OrderNumber, '#'), '') OrderNumber
		,	cast(o.ContractedDate as Date) OrderDate
		,	'' VendorName
		,	'TAX' ProductCategory
		,	'' ProductName
		,	'' ProductModel
		,	'' Color
		,	'' Fabric
		,	'' Description
		,	isnull(format(o.Tax, '#,##0.00'), '') ItemCost
		,	isnull(format(1, '#,##0.00'), '') Qty
		,	isnull(format(o.Tax, '#,##0.00'), '') as LineCost
		,	isnull(format(o.Tax, '#,##0.00'), '') as LineValue
		,	'' SuggestedResale
		,	'' DiscountAmount
		,	'' MarginPercentage
		,	format(cast(q.CreatedOn as date), 'MM/dd/yyyy') QuoteDate
		,	case q.PrimaryQuote when 1 then 'Y' else 'N' end PrimaryQuote
		,	isnull(qs.QuoteStatus, '') QuoteStatus
		,	isnull(os.OrderStatus, '') OrderStatus
		,	'' ProductType
		,	'N' isAlliance
		,	'' MPODate
		,	'' VPODate
		,	'' PromiseByDate
		,	'' EstShipDate
		,	'' ShipDate
		,	'' TrackingNumber
		,	'' InvoiceDate
		,	'' ReceivedDate
		,	'' VPOStatus
		,	0 RevenueCounted
from CRM.Opportunities opp
join CRM.Quote q
	on q.OpportunityId = opp.OpportunityId
join CRM.Type_QuoteStatus qs
	on	qs.QuoteStatusId = q.QuoteStatusId
join CRM.Accounts a
	on opp.AccountId = a.AccountId
join CRM.Person sa ON sa.PersonId = opp.SalesAgentId 
join CRM.Orders o
	on q.QuoteKey = o.QuoteKey
join CRM.Type_OrderStatus os
	on	o.OrderStatus = os.OrderStatusId
left join crm.Territories t
	on opp.TerritoryId = t.TerritoryId
where opp.FranchiseID = @FranchiseID;

select
	 fr.Name FranchiseName
	,fr.Code FranchiseCode
	,al.LeadNumber
	,al.LeadTerritory
	,al.LeadTerritoryCode
	,al.LeadDate
	,al.LeadFirstName
	,al.LeadLastName
	,al.LeadCompanyName
	,al.LeadEMail
	,al.LeadAddress1
	,al.LeadAddress2
	,al.LeadCity
	,al.LeadState
	,al.LeadZipCode
	,al.LeadAttentionText
	,al.LeadSource
	,al.LeadCampaign
	,al.LeadApptSubject
	,al.LeadApptMessage
	,al.LeadAppt
	,al.LeadStatus
	,al.AccountNumber
	,al.AccountTerritory
	,al.AccountTerritoryCode
	,al.AccountDate
	,al.AccountFirstName
	,al.AccountLastName
	,al.AccountCompanyName
	,al.AccountEMail
	,al.AccountAddress1
	,al.AccountAddress2
	,al.AccountCity
	,al.AccountState
	,al.AccountZipCode
	,al.AccountAttentionText
	,al.AccountSource
	,al.AccountCampaign
	,al.AccountApptSubject
	,al.AccountApptMessage
	,al.AccountAppt
	,al.AccountStatus
	,op.SalesPersonID
	,op.SalesPerson
	,op.OpportunityNumber
	,op.OpportunityName
	,op.OpportunityTerritory
	,op.OpportunityTerritoryCode
	,op.OpportunityDate
	,op.OpportunityFirstName
	,op.OpportunityLastName
	,op.OpportunityCompanyName
	,op.OpportunityEMail
	,op.OpportunityAddress1
	,op.OpportunityAddress2
	,op.OpportunityCity
	,op.OpportunityState
	,op.OpportunityZipCode
	,op.OpportunityAttentionText
	,op.OpportunitySource
	,op.OpportunityCampaign
	,op.OpportunityApptSubject
	,op.OpportunityApptMessage
	,op.OpportunityAppt
	,q.QuoteTerritory
	,q.QuoteTerritoryCode
	,q.QuoteNumber
	,q.QuoteLineId
	,q.LineNumber
	,q.OrderName
	,q.OrderNumber
	,q.OrderDate
	,q.VendorName
	,q.ProductCategory
	,q.ProductName
	,q.ProductModel
	,q.Color
	,q.Fabric
	,q.Description
	,q.ItemCost
	,q.Qty
	,q.LineCost
	,q.LineValue
	,q.SuggestedResale
	,q.DiscountAmount
	,q.MarginPercentage
	,q.QuoteDate
	,q.PrimaryQuote
	,q.QuoteStatus
	,q.OrderStatus
	,q.ProductType
	,q.IsAlliance
	,q.MPODate
	,q.VPODate
	,q.PromiseByDate
	,q.EstShipDate
	,q.ShipDate
	,q.TrackingNumber
	,q.InvoiceDate
	,q.ReceivedDate
	,q.VPOStatus
	,q.RevenueCounted
from (
	select coalesce(a.FranchiseId, l.FranchiseID) FranchiseID
				,	l.LeadID
				,	l.LeadNumber
				,	l.LeadTerritory
				,	l.LeadTerritoryCode
				,	l.LeadDate
				,	l.LeadFirstName
				,	l.LeadLastName
				,	l.LeadCompanyName
				,	l.LeadEMail
				,	l.LeadAddress1
				,	l.LeadAddress2
				,	l.LeadCity
				,	l.LeadState
				,	l.LeadZipCode
				,	l.LeadAttentionText
				,	l.LeadSource
				,	l.LeadCampaign
				,	l.LeadApptSubject
				,	l.LeadApptMessage
				,	l.LeadAppt
				,	l.LeadStatus
				,	a.AccountID
				,	a.AccountNumber
				,	a.AccountTerritory
				,	a.AccountTerritoryCode
				,	a.AccountDate
				,	a.AccountFirstName
				,	a.AccountLastName
				,	a.AccountCompanyName
				,	a.AccountEMail
				,	a.AccountAddress1
				,	a.AccountAddress2
				,	a.AccountCity
				,	a.AccountState
				,	a.AccountZipCode
				,	a.AccountAttentionText
				,	a.AccountSource
				,	a.AccountCampaign
				,	a.AccountApptSubject
				,	a.AccountApptMessage
				,	a.AccountAppt
				,	a.AccountStatus
	from #Accounts a
	full outer join #Leads l 
		on l.LeadID = a.LeadID ) al
join	CRM.Franchise fr
	on	al.FranchiseID = fr.FranchiseId
left join
	#Opportunities op
	on al.AccountID = op.AccountID
left join
	#Quotes q
	on	op.OpportunityId = q.OpportunityId
where (		isnull(cast(nullif(al.LeadDate, '') as Date), '1900-01-01') between @StartDate and @EndDate
		or	isnull(cast(nullif(al.AccountDate, '') as Date), '1900-01-01') between @StartDate and @EndDate
		or	isnull(cast(nullif(op.OpportunityDate, '') as Date), '1900-01-01') between @StartDate and @EndDate
		or	isnull(cast(nullif(q.OrderDate, '') as Date), '1900-01-01') between @StartDate and @EndDate
		or	isnull(cast(nullif(q.MPODate, '') as Date), '1900-01-01') between @StartDate and @EndDate
		or	isnull(cast(nullif(q.VPODate, '') as Date), '1900-01-01') between @StartDate and @EndDate );


drop table #Leads;
drop table #Accounts;
drop table #Opportunities;
drop table #Quotes;

end;