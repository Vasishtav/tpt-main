﻿
CREATE PROCEDURE CRM.SyncFailedCalendarAppointments 

AS
BEGIN
	SET NOCOUNT ON;
	SELECT c.CalendarId,cs.SyncRecId,  f.name AS FranchiseName, c.Subject,c.Message, CONVERT(date, c.StartDate) AS AppointmentDate,c.OrganizerName,c.OrganizerEmail,(SELECT Stuff(
                          (SELECT N', ' + etp.PersonName FROM crm.EventToPeople etp WHERE etp.CalendarId = cs.CalendarId  FOR XML PATH(''),TYPE)
                          .value('text()[1]','nvarchar(max)'),1,2,N'')) AS Attendees,(SELECT Stuff(
                          (SELECT N', ' + etp1.PersonEmail FROM crm.EventToPeople etp1 WHERE etp1.CalendarId = cs.CalendarId  FOR XML PATH(''),TYPE)
                          .value('text()[1]','nvarchar(max)'),1,2,N'')) AS AttendeesEmail
                        FROM api.CalendarSync cs 
                        INNER JOIN crm.Calendar c ON c.CalendarId = cs.CalendarId
                        INNER JOIN crm.Franchise f ON f.FranchiseId = c.FranchiseId
                        WHERE cs.ProviderUniqueId IS NULL AND f.FranchiseId not IN (2,7,13,43,78) AND  ISNULL(c.IsDeleted,0)=0 and cs.ErrorCount>=4 and ISNULL(cs.EmailSent, 0)<>1
   
END
