﻿Create procedure [CRM].[RefreshSurfacingMasterList] 
(
	@FranchiseId int,
	@PersonId int,
	@strProductID nvarchar(MAX),
	@BrandID int
)
as
BEGIN
DECLARE @ProductId as int;
Declare @ProductNumber as int;

	Select * into #SurfacingProducts from CRM.SurfacingProducts Where VendorID in
	(select hv.VendorId from Acct.HFCVendors hv
                   join Acct.FranchiseVendors fv on hv.VendorId=fv.VendorId
                   where FranchiseId=@FranchiseId and fv.VendorStatus=1 and (hv.IsAlliance=0 or hv.IsAlliance is null)
                   union
                   select VendorId from Acct.FranchiseVendors where FranchiseId=@FranchiseId and 
                   VendorType=3 and VendorStatus=1
	) and BrandID=@BrandID
	If(@strProductID IS NOT NULL) 
	Begin
	SET IDENTITY_INSERT #SurfacingProducts ON

	insert into #SurfacingProducts([ProductKey],[ProductID],[ProductName],[PICProductID],[VendorID],[VendorName],[VendorProductSKU],[Description],[ProductCategory],[Cost],[SalePrice]
		,[ProductStatus],[ProductSubCategory],[MarkUp],[MarkupType],[Discount],[DiscountType],[CalculatedSalesPrice],[CreatedOn],[CreatedBy],[LastUpdatedOn],[LastUpdatedBy],[FranchiseId],[ProductType],[Model],[Collection],[Color],[Taxable],[SurfaceLaborSet],[MultipartSurfacing]
		,[MultipleSurfaceProductSet],[MultipartAttributeSet],[BrandID]) 
	select [ProductKey],[ProductID],[ProductName],[PICProductID],[VendorID],[VendorName],[VendorProductSKU],[Description]
		,[ProductCategory],[Cost],[SalePrice],[ProductStatus],[ProductSubCategory],[MarkUp],[MarkupType],[Discount],[DiscountType],[CalculatedSalesPrice],[CreatedOn],[CreatedBy]
		,[LastUpdatedOn],[LastUpdatedBy],[FranchiseId],[ProductType],[Model],[Collection],[Color],[Taxable],[SurfaceLaborSet],[MultipartSurfacing],[MultipleSurfaceProductSet],[MultipartAttributeSet],[BrandID] from CRM.SurfacingProducts Where ProductID in (select convert(int, value)as ID FROM string_split(@strProductID, ','))
	
	SET IDENTITY_INSERT #SurfacingProducts OFF
	End
	
	if Exists(select ProductID from #SurfacingProducts)
	Begin

		Update CRM.FranchiseProducts set ProductStatus=2 where FranchiseId=@FranchiseId and ProductType=2
	
		DECLARE SurfacingProductsCursor CURSOR FOR select ProductID from #SurfacingProducts

		OPEN SurfacingProductsCursor

		FETCH NEXT FROM SurfacingProductsCursor   
		INTO @ProductID
  
		WHILE @@FETCH_STATUS = 0  
		BEGIN  

			IF EXISTS(SELECT ProductNumber FROM CRM.GetNewNumber)
			BEGIN
				UPDATE CRM.GetNewNumber SET ProductNumber = ISNULL(ProductNumber,0) + 1;
				SET @ProductNumber=(SELECT ProductNumber FROM CRM.GetNewNumber) ;
			END
			ELSE
			BEGIN
				INSERT INTO CRM.GetNewNumber (ProductNumber) VALUES(1)
				SET @ProductNumber	= 1;
			END


			Insert into CRM.FranchiseProducts(ProductID,ProductName,PICProductID,VendorID,VendorName,VendorProductSKU,Description,ProductCategory,Cost,SalePrice,ProductStatus,ProductSubCategory,MarkUp,MarkupType,Discount,DiscountType,CalculatedSalesPrice,CreatedOn,CreatedBy,LastUpdatedOn,LastUpdatedBy,FranchiseId,ProductType,Model,Collection,Color,Taxable,SurfaceLaborSet,MultipartSurfacing,MultipleSurfaceProductSet,MultipartAttributeSet,MasterSurfacingProductID)
			Select @ProductNumber,ProductName,PICProductID,VendorID,VendorName,VendorProductSKU,Description,ProductCategory,Cost,SalePrice,ProductStatus,ProductSubCategory,MarkUp,MarkupType,Discount,DiscountType,CalculatedSalesPrice,GETUTCDATE(),@PersonId,GETUTCDATE(),@PersonId,@FranchiseId,ProductType,Model,Collection,Color,Taxable,SurfaceLaborSet,MultipartSurfacing,MultipleSurfaceProductSet,MultipartAttributeSet,@ProductId
				from #SurfacingProducts where ProductId=@ProductId;

			UPdate crm.FranchiseProducts set MultipleSurfaceProductSet=replace(MultipleSurfaceProductSet,'"ProductID":"'+  cast(@ProductId as varchar) ,'"ProductID":"'+ cast(ProductId as varchar))
				where productkey=(select SCOPE_IDENTITY())

			FETCH NEXT FROM SurfacingProductsCursor   
			INTO @ProductID  
		END
		CLOSE SurfacingProductsCursor;  
		DEALLOCATE SurfacingProductsCursor;  

		--select ProductKey,ProductID,ProductName,fp.VendorId,case when exists
		--(select VendorId from [Acct].[HFCVendors] where VendorId=fp.VendorID)
		--then (select Name from [Acct].[HFCVendors] where VendorId=fp.VendorID)
		--else case when exists(select VendorId from [Acct].[FranchiseVendors])
		--then (select Name from [Acct].[FranchiseVendors] where VendorId=fp.VendorID) end end as VendorName,
		--VendorProductSKU ,pc.ProductCategory,psc.ProductCategory as ProductSubCategory,Description,
		--SalePrice,fp.FranchiseId,ps.ProductStatus,fp.MultipleSurfaceProductSet,fp.MasterSurfacingProductID from [CRM].[FranchiseProducts] fp
		--join Acct.FranchiseVendors fv on fp.VendorID=fv.VendorId
		--left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
		--left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
		--left join [CRM].[Type_ProductCategory] psc on fp.ProductSubCategory=psc.ProductCategoryId
		--where fv.FranchiseId = @franchiseid and fv.VendorStatus=1 and fp.FranchiseId= @franchiseid and ProductStatusId=1
		--and fp.ProductType!=3 
		--union
		--select ProductKey,ProductID,ProductName,fp.VendorId,'Multiple' as VendorName,
		--VendorProductSKU ,pc.ProductCategory,psc.ProductCategory as ProductSubCategory,Description,
		--SalePrice,fp.FranchiseId,ps.ProductStatus,fp.MultipleSurfaceProductSet,fp.MasterSurfacingProductID from [CRM].[FranchiseProducts] fp
		--left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
		--left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
		--left join [CRM].[Type_ProductCategory] psc on fp.ProductSubCategory=psc.ProductCategoryId
		--where fp.FranchiseId= @franchiseid and ProductStatusId=1 and fp.VendorID is null and fp.MultipartSurfacing=1
		--union
		--select ProductKey,ProductID,ProductName,fp.VendorId,case when exists
		--(select VendorId from [Acct].[HFCVendors] where VendorId=fp.VendorID)
		--then (select Name from [Acct].[HFCVendors] where VendorId=fp.VendorID)
		--else case when exists(select VendorId from [Acct].[FranchiseVendors])
		--then (select Name from [Acct].[FranchiseVendors] where VendorId=fp.VendorID) end end as VendorName,
		--VendorProductSKU, pc.ProductCategory,psc.ProductCategory as ProductSubCategory,fp.Description,fp.SalePrice,fp.FranchiseId, ps.ProductStatus,fp.MultipleSurfaceProductSet,fp.MasterSurfacingProductID from [CRM].[FranchiseProducts] fp
		--left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
		--left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
		--left join [CRM].[Type_ProductCategory] psc on fp.ProductSubCategory=psc.ProductCategoryId
		--where  fp.FranchiseId= @franchiseid and fp.ProductStatus=1 and fp.ProductType=3
	End
END