﻿
CREATE PROC [CRM].[CustomerNameJobSearchExport]
(
@FranchiseId int,
@SearchTerm varchar(300)=null,
@PageSize int=20,
@PageNumber int=0,
@SortOrder varchar(50) = 'CreatedOnUtc',
@SortDirection varchar(4) ='desc',
@leadStatusIds varchar(500) = null,
@installerPersonIds varchar(500) = null,
@salesPersonIds varchar(500) = null,
@jobStatusIds varchar(500) = null,
@InvoiceStatuses varchar(500) = null,
@SourceIds varchar(500)  = null,
@createdOnUtcStart datetime=null,
@createdOnUtcEnd datetime=null,
@CommercialType int = 0
)

as
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

declare @Ids TABLE 
(  
    id int
) 
insert into @Ids
  select * from  [CRM].[fnGetIds_forSearch] 
    (
	@leadStatusIds,
	@installerPersonIds,
	@salesPersonIds,
	@jobStatusIds,
	@InvoiceStatuses,
	@SourceIds,
	'job',
	@FranchiseId);


	
if	@SortDirection = 'Asc'
begin

	SELECT 
	lead.LeadId,
	job.[JobNumber],
	job.CustomerPersonId, 
	customer.FirstName + ' ' + customer.LastName as CustomerFullName,
	customer.PreferredTFN,
	Customer.CellPhone,
	Customer.WorkPhone,
	Customer.HomePhone,
	Customer.FaxPhone,
	job.JobStatusId,
	jobStatus.Name as JobStatusName,
	job.SourceId,
	job.InstallAddressId,
	job.BillingAddressId,
	salesPerson.FirstName + ' ' + salesPerson.LastName as salesFullName,
	job.SalesPersonId,
	p.PrimaryEmail,

	isnull( primaryQuote.Subtotal,0) as Subtotal,
    isnull(        primaryQuote.DiscountTotal,0) as DiscountTotal,
            isnull(primaryQuote.SurchargeTotal,0) as SurchargeTotal,
            isnull(primaryQuote.Taxtotal,0) as Taxtotal,
            isnull(primaryQuote.NetProfit,0) as NetProfit,
            isnull(primaryQuote.NetTotal,0) as NetTotal,
            isnull(job.Balance,0) as Balance,
			job.CreatedOnUtc,
			job.ContractedOnUtc,

		jobItem.Manufacturer,
		jobItem.ProductType,
		jobItem.ProductName,
		jobItem.CategoryName,
			isnull( jobItem.Quantity,0) as Quantity,
        isnull(    jobItem.SalePrice,0) as SalePrice,
            isnull(jobItem.UnitCost,0) as UnitCost,
            isnull(jobItem.DiscountAmount,0) as DiscountAmount,
            isnull(jobItem.Subtotal ,0) as jobItemSubtotal

	FROM crm.Leads lead 
	inner join crm.Jobs job on job.LeadId = lead.LeadId
	--inner join crm.LeadAddresses la on la.LeadId = lead.LeadId
	left join crm.Addresses as adres on adres.AddressId = [InstallAddressId]
   left JOIN [CRM].[Person] p	on p.[PersonId]=lead.[PersonId]
	left join crm.JobQuotes as primaryQuote on primaryQuote.JobId  = job.JobId and primaryQuote.IsDeleted <> 1 and primaryQuote.IsPrimary = 1
	left join crm.JobItems as jobItem on jobItem.QuoteId = primaryQuote.QuoteId
	left join [CRM].[Person] as salesPerson on job.SalesPersonId = salesPerson.PersonId
	inner join [CRM].Type_JobStatus as jobStatus on jobStatus.id = job.JobStatusId
	inner join [CRM].[Person] as customer on job.CustomerPersonId = customer.PersonId

	left join [CRM].[Person] secondPerson  on secondPerson.PersonId = lead.SecPersonId
	Inner JOIN [dbo].[vw_CommercialLeads] as com on com.leadId = lead.LeadId and (com.CommercialType = @CommercialType or @CommercialType = 0)	

	WHERE

(job.[JobId] in (select Id  from @Ids) or (@leadStatusIds is null and
	@installerPersonIds  is null and
	@salesPersonIds  is null and
	@jobStatusIds  is null and
	@InvoiceStatuses  is null  and
	@SourceIds is null) ) and
	
(job.[CreatedOnUtc] >= @createdOnUtcStart or @createdOnUtcStart is null ) and
(job.[CreatedOnUtc] <= @createdOnUtcEnd or @createdOnUtcEnd is null ) and
 lead.IsDeleted = 0 and job.IsDeleted = 0 and
 lead.FranchiseId=@FranchiseId AND (
 secondPerson.FirstName like '%' + @SearchTerm +'%' or
 secondPerson.LastName like '%' + @SearchTerm +'%' or 
 p.FirstName like '%' + @SearchTerm +'%' or
 p.LastName like '%' + @SearchTerm +'%'  or
 secondPerson.FirstName + ' ' + secondPerson.LastName like '%' + @SearchTerm +'%' or
 p.FirstName + ' ' + p.LastName like '%' + @SearchTerm +'%' or
 p.companyname like '%' + @SearchTerm +'%'  or
 @SearchTerm is null
									   )

									    ORDER BY case when @SortOrder = 'CreatedOnUtc' then job.CreatedOnUtc else null end ,
		  case when @SortOrder = 'customer' then p.[FirstName]  else '' end,
		  case when @SortOrder ='jobnumber' then [JobNumber]  else 0 end,
		  case when @SortOrder ='jobstatus' then [JobStatusId]  else 0 end,
		  case when @SortOrder ='nettotal' then [NetTotal]  else 0 end,
		  case when @SortOrder ='salespersonid' then[SalesPersonId]  else 0 end,
		  case when @SortOrder ='addresses' then [City]+[ZipCode]+[Address1]  else '' end,
		  case when @SortOrder ='balance' then ([NetTotal] - [CRM].[fnsGetSummJobPayments](job.JobId))  else 0 end
		  
end
else
begin

	SELECT 
	lead.LeadId,
	job.[JobNumber],
	job.CustomerPersonId, 
	customer.FirstName + ' ' + customer.LastName as CustomerFullName,
	customer.PreferredTFN,
	Customer.CellPhone,
	Customer.WorkPhone,
	Customer.HomePhone,
	Customer.FaxPhone,
	job.JobStatusId,
	jobStatus.Name as JobStatusName,
	job.SourceId,
	job.InstallAddressId,
	job.BillingAddressId,
	salesPerson.FirstName + ' ' + salesPerson.LastName as salesFullName,
	p.PrimaryEmail,

	
	isnull( primaryQuote.Subtotal,0) as Subtotal,
    isnull(        primaryQuote.DiscountTotal,0) as DiscountTotal,
            isnull(primaryQuote.SurchargeTotal,0) as SurchargeTotal,
            isnull(primaryQuote.Taxtotal,0) as Taxtotal,
            isnull(primaryQuote.NetProfit,0) as NetProfit,
            isnull(primaryQuote.NetTotal,0) as NetTotal,
            isnull(job.Balance,0) as Balance,
			job.CreatedOnUtc,
			job.ContractedOnUtc,
			--isnull(installAddress.Address1, '') + ' ' + isnull(installAddress.Address2, '') + ', ' + isnull(installAddress.City, '') + ', ' + isnull(installAddress.State, '') + ', ' + isnull(installAddress.ZipCode, '') as InstallAddress,
		 --   isnull(billingAddress.Address1, '') + ' ' + isnull(billingAddress.Address2, '') + ', ' + isnull(billingAddress.City, '') + ', ' + isnull(billingAddress.State, '') + ', ' + isnull(billingAddress.ZipCode, '') as BillingAddress
		
		jobItem.Manufacturer,
		jobItem.ProductType,
		jobItem.ProductName,
		jobItem.CategoryName,
		isnull( jobItem.Quantity,0) as Quantity,
        isnull(    jobItem.SalePrice,0) as SalePrice,
            isnull(jobItem.UnitCost,0) as UnitCost,
            isnull(jobItem.DiscountAmount,0) as DiscountAmount,
            isnull(jobItem.Subtotal ,0) as jobItemSubtotal

	FROM crm.Leads lead 
	inner join crm.Jobs job on job.LeadId = lead.LeadId
	--inner join crm.LeadAddresses la on la.LeadId = lead.LeadId
	left join crm.Addresses as adres on adres.AddressId = [InstallAddressId]
   left JOIN [CRM].[Person] p	on p.[PersonId]=lead.[PersonId]
	left join crm.JobQuotes as primaryQuote on primaryQuote.JobId  = job.JobId and primaryQuote.IsDeleted <> 1 and primaryQuote.IsPrimary = 1
	left join crm.JobItems as jobItem on jobItem.QuoteId = primaryQuote.QuoteId
	--left join crm.Addresses as installAddress on installAddress.AddressId = [InstallAddressId]
	--left join crm.Addresses as billingAddress on installAddress.AddressId = BillingAddressId
	left join [CRM].[Person] as salesPerson on job.SalesPersonId = salesPerson.PersonId
	inner join [CRM].Type_JobStatus as jobStatus on jobStatus.id = job.JobStatusId
	inner join [CRM].[Person] as customer on job.CustomerPersonId = customer.PersonId

	left join [CRM].[Person] secondPerson  on secondPerson.PersonId = lead.SecPersonId
	Inner JOIN [dbo].[vw_CommercialLeads] as com on com.leadId = lead.LeadId and (com.CommercialType = @CommercialType or @CommercialType = 0)	

	WHERE

(job.[JobId] in (select Id  from @Ids) or (@leadStatusIds is null and
	@installerPersonIds  is null and
	@salesPersonIds  is null and
	@jobStatusIds  is null and
	@InvoiceStatuses  is null  and
	@SourceIds is null) ) and
	
(job.[CreatedOnUtc] >= @createdOnUtcStart or @createdOnUtcStart is null ) and
(job.[CreatedOnUtc] <= @createdOnUtcEnd or @createdOnUtcEnd is null ) and
 lead.IsDeleted = 0 and job.IsDeleted = 0 and
 lead.FranchiseId=@FranchiseId AND (
 secondPerson.FirstName like '%' + @SearchTerm +'%' or
 secondPerson.LastName like '%' + @SearchTerm +'%' or 
 p.FirstName like '%' + @SearchTerm +'%' or
 p.LastName like '%' + @SearchTerm +'%'  or
 secondPerson.FirstName + ' ' + secondPerson.LastName like '%' + @SearchTerm +'%' or
 p.FirstName + ' ' + p.LastName like '%' + @SearchTerm +'%' or
 p.companyname like '%' + @SearchTerm +'%'  or
 @SearchTerm is null
									   )

									    ORDER BY case when @SortOrder = 'CreatedOnUtc' then job.CreatedOnUtc else null end desc,
		  case when @SortOrder = 'customer' then p.[FirstName]  else '' end desc,
		  case when @SortOrder ='jobnumber' then [JobNumber]  else 0 end desc,
		  case when @SortOrder ='jobstatus' then [JobStatusId]  else 0 end desc,
		  case when @SortOrder ='nettotal' then [NetTotal]  else 0 end desc,
		  case when @SortOrder ='salespersonid' then[SalesPersonId]  else 0 end desc,
		  case when @SortOrder ='addresses' then [City]+[ZipCode]+[Address1]  else '' end  desc,
		  case when @SortOrder ='balance' then ([NetTotal] - [CRM].[fnsGetSummJobPayments](job.JobId))  else 0 end desc
		  
									   


end		  
		 
			



