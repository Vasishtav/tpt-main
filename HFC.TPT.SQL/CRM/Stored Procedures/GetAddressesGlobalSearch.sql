﻿CREATE PROC [CRM].[GetAddressesGlobalSearch]
( @FranchiseId int
	,@SearchType varchar(50)
	, @Value varchar(500))

AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
set @Value = '%'+ @Value + '%';

IF (@SearchType = 'address' )
	BEGIN
		select ad.AddressId
        , isnull(ad.AttentionText, '') AttentionText
        , concat(c.FirstName, ' ', c.LastName) Contact 
        , c.CustomerId ContactId
        , ad.Address1, ad.Address2, ad.City, ad.State, ad.ZipCode, ad.CrossStreet
        , case when l.LeadStatusId <> 31422 then l.LeadId 
		        else null end LeadId
	        , a.AccountId,
			CASE
			WHEN isnull(a.AccountId,'') <> '' THEN aa.IsPrimaryAddress
			ELSE la.IsPrimaryAddress
			END IsPrimaryAddress
        from crm.Addresses ad
        left join crm.Customer c on c.CustomerId = ad.ContactId
        left join crm.LeadAddresses la on la.AddressId = ad.AddressId
        left join crm.Leads l on l.LeadId = la.LeadId
        left join crm.AccountAddresses aa on aa.AddressId = ad.AddressId
        left join crm.Accounts a on a.AccountId = aa.AccountId
        where isnull(ad.IsDeleted, 0) = 0
			and (l.FranchiseId = @FranchiseId 
				or a.FranchiseId = @FranchiseId)

			and (ad.Address1 like @Value 
				or ad.Address2 like @Value)
		Order by AttentionText asc, Contact asc
	END
ELSE IF (@SearchType = 'city')
	BEGIN
		select ad.AddressId
        , isnull(ad.AttentionText, '') AttentionText
        , concat(c.FirstName, ' ', c.LastName) Contact 
        , c.CustomerId ContactId
        , ad.Address1, ad.Address2, ad.City, ad.State, ad.ZipCode, ad.CrossStreet
        , case when l.LeadStatusId <> 31422 then l.LeadId 
		        else null end LeadId
	        , a.AccountId,
			CASE
			WHEN isnull(a.AccountId,'') <> '' THEN aa.IsPrimaryAddress
			ELSE la.IsPrimaryAddress
			END IsPrimaryAddress
        from crm.Addresses ad
        left join crm.Customer c on c.CustomerId = ad.ContactId
        left join crm.LeadAddresses la on la.AddressId = ad.AddressId
        left join crm.Leads l on l.LeadId = la.LeadId
        left join crm.AccountAddresses aa on aa.AddressId = ad.AddressId
        left join crm.Accounts a on a.AccountId = aa.AccountId
        where isnull(ad.IsDeleted, 0) = 0
			and (l.FranchiseId = @FranchiseId 
				or a.FranchiseId = @FranchiseId)

			and (ad.City like @Value) 
		Order by AttentionText asc, Contact asc
	END
ELSE IF (@SearchType = 'all')
	BEGIN
		select ad.AddressId
        , isnull(ad.AttentionText, '') AttentionText
        , concat(c.FirstName, ' ', c.LastName) Contact 
        , c.CustomerId ContactId
        , ad.Address1, ad.Address2, ad.City, ad.State, ad.ZipCode, ad.CrossStreet
        , case when l.LeadStatusId <> 31422 then l.LeadId 
		        else null end LeadId
	        , a.AccountId,
			CASE
			WHEN isnull(a.AccountId,'') <> '' THEN aa.IsPrimaryAddress
			ELSE la.IsPrimaryAddress
			END IsPrimaryAddress
        from crm.Addresses ad
        left join crm.Customer c on c.CustomerId = ad.ContactId
        left join crm.LeadAddresses la on la.AddressId = ad.AddressId
        left join crm.Leads l on l.LeadId = la.LeadId
        left join crm.AccountAddresses aa on aa.AddressId = ad.AddressId
        left join crm.Accounts a on a.AccountId = aa.AccountId
        where isnull(ad.IsDeleted, 0) = 0
			and (l.FranchiseId = @FranchiseId 
				or a.FranchiseId = @FranchiseId)

			and (ad.Address1 like @Value 
				or ad.Address2 like @Value
				or ad.City like @Value
				or c.FirstName like  @Value 
				or c.LastName like @Value
				or CONCAT(C.FirstName, ' ', C.LastName) like @Value)
		Order by AttentionText asc, Contact asc
	END
ELSE IF (@SearchType = 'name')
	BEGIN
		select ad.AddressId
        , isnull(ad.AttentionText, '') AttentionText
        , concat(c.FirstName, ' ', c.LastName) Contact 
        , c.CustomerId ContactId
        , ad.Address1, ad.Address2, ad.City, ad.State, ad.ZipCode, ad.CrossStreet
        , case when l.LeadStatusId <> 31422 then l.LeadId 
		        else null end LeadId
	        , a.AccountId,
			CASE
			WHEN isnull(a.AccountId,'') <> '' THEN aa.IsPrimaryAddress
			ELSE la.IsPrimaryAddress
			END IsPrimaryAddress
        from crm.Addresses ad
        left join crm.Customer c on c.CustomerId = ad.ContactId
        left join crm.LeadAddresses la on la.AddressId = ad.AddressId
        left join crm.Leads l on l.LeadId = la.LeadId
        left join crm.AccountAddresses aa on aa.AddressId = ad.AddressId
        left join crm.Accounts a on a.AccountId = aa.AccountId
        where isnull(ad.IsDeleted, 0) = 0
			and (l.FranchiseId = @FranchiseId 
				or a.FranchiseId = @FranchiseId)

			and (c.FirstName like  @Value 
				or c.LastName like @Value
				or CONCAT(C.FirstName, ' ', C.LastName) like @Value)
		Order by AttentionText asc, Contact asc
	END
ELSE 
	BEGIN
		declare @messsage varchar(500) = 'Invaid search type :' + @SearchType;
		THROW 60000, @messsage, 1;
	END;
	
	