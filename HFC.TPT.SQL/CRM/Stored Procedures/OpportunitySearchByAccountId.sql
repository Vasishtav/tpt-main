﻿CREATE PROCEDURE [CRM].[OpportunitySearchByAccountId]
(
@AccountId int
)
as
Begin
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SELECT 
		case when op.TerritoryId is not null then tr.Name else op.TerritoryType end as Territory
		,op.OpportunityId
		,op.OpportunityName as OpportunityFullName
		,opps.Name OpportunityStatus
		,OrderTotal OrderAmount
		,op.OpportunityStatusId
		,op.SalesAgentId
		,op.InstallerId
		,op.OpportunityNumber
		,AccountId
		,sa.FirstName+' '+sa.LastName SalesAgentName
		,[CRM].[fnGetAccountName_forOpportunitySearch](op.OpportunityId) as AccountName
		,CONVERT(date, ContractedDate) ContractDate
		from [CRM].[Opportunities]  op
		LEFT JOIN CRM.Territories tr on tr.TerritoryId=op.TerritoryId
		LEFT JOIN CRM.Type_OpportunityStatus opps on opps.OpportunityStatusId=op.OpportunityStatusId
		LEFT JOIN CRM.Orders o on o.OpportunityId=op.OpportunityId and o.OrderStatus !=9 and o.OrderStatus !=6
		LEFT JOIN [CRM].[Person] sa on sa.PersonId=op.SalesAgentId
		WHERE 
		op.AccountId=@AccountId
		order by op.OpportunityId desc
		End
Go

