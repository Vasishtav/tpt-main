﻿CREATE PROC [CRM].[GetContactsGlobalSearch]
( @FranchiseId int
	,@SearchType varchar(50)
	, @Value varchar(500))

AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @phoneValue varchar(20) =  '%' + [CRM].[fnRmoveFormatPhone](@value) + '%';
set @Value = '%'+ @Value + '%';

IF (@SearchType = 'email')
	BEGIN
		select l.LeadId Id, Concat(c.FirstName, ' ', c.LastName) Name
        , [CRM].[fnGetLeadNameByLeadId](l.LeadId) LeadAccountName
        , lc.LeadId Id,lc.IsPrimaryCustomer,lc.CustomerId 
        , CASE WHEN isnull(c.PrimaryEmail,'') <> '' THEN c.PrimaryEmail ELSE c.SecondaryEmail END Email
		, CASE c.PreferredTFN 
		        WHEN 'C' THEN c.CellPhone
		        WHEN 'H' THEN c.HomePhone
		        WHEN 'W' THEN 
					CASE WHEN isnull(c.WorkPhoneExt, '') <> '' THEN CONCAT(c.WorkPhone, ' - ', c.WorkPhoneExt) 
					ELSE c.WorkPhone
					END
				ELSE ''
		        END Phone 
		        , 1 IsLead
        from crm.Customer c 
        Join crm.LeadCustomers lc on lc.CustomerId = c.CustomerId
        join crm.Leads l on l.LeadId = lc.LeadId
        where l.FranchiseId = @FranchiseId 
            and l.LeadStatusId <> 31422
            and isnull(c.IsDeleted, 0) = 0 
			and ( c.PrimaryEmail like  @Value 
				or c.SecondaryEmail like @Value )
		Union 

		select a.AccountId Id, Concat(c.FirstName, ' ', c.LastName) Name
		, [CRM].[fnGetAccountNameByAccountId](a.AccountId) LeadAccountName
		, ac.AccountId Id, ac.IsPrimaryCustomer, ac.CustomerId
		, CASE WHEN isnull(c.PrimaryEmail,'') <> '' THEN c.PrimaryEmail ELSE c.SecondaryEmail END Email
		, CASE c.PreferredTFN 
				WHEN 'C' THEN c.CellPhone
				WHEN 'H' THEN c.HomePhone
				WHEN 'W' THEN 
					CASE WHEN isnull(c.WorkPhoneExt, '') <> '' THEN CONCAT(c.WorkPhone, ' - ', c.WorkPhoneExt) 
					ELSE c.WorkPhone
					END
				ELSE ''
				END Phone 
		, 0 IsLead
		from crm.Customer c 
		Join crm.AccountCustomers ac on ac.CustomerId = c.CustomerId
		join crm.Accounts a on a.AccountId = ac.AccountId
		where a.FranchiseId = @FranchiseId
			and isnull(c.IsDeleted, 0) = 0
			and ( c.PrimaryEmail like  @Value 
				or c.SecondaryEmail like @Value ) 
		Order by LeadAccountName

	END;
ELSE IF (@SearchType = 'phone')
	BEGIN
		select l.LeadId Id, Concat(c.FirstName, ' ', c.LastName) Name
        , [CRM].[fnGetLeadNameByLeadId](l.LeadId) LeadAccountName
        , lc.LeadId Id,lc.IsPrimaryCustomer,lc.CustomerId 
        , c.PrimaryEmail Email, CASE WHEN c.CellPhone like @phoneValue THEN c.CellPhone
			WHEN c.HomePhone like @phoneValue THEN c.HomePhone
			WHEN c.WorkPhone like @phoneValue THEN 
				CASE WHEN isnull(c.WorkPhoneExt, '') <> '' THEN CONCAT(c.WorkPhone, ' - ', c.WorkPhoneExt) 
				ELSE c.WorkPhone
				END
			ELSE CASE c.PreferredTFN 
				WHEN 'C' THEN c.CellPhone
				WHEN 'H' THEN c.HomePhone
				WHEN 'W' THEN 
					CASE WHEN isnull(c.WorkPhoneExt, '') <> '' THEN CONCAT(c.WorkPhone, ' - ', c.WorkPhoneExt) 
					ELSE c.WorkPhone
					END
				ELSE ''
				END 
			END Phone 
		        , 1 IsLead
        from crm.Customer c 
        Join crm.LeadCustomers lc on lc.CustomerId = c.CustomerId
        join crm.Leads l on l.LeadId = lc.LeadId
        where l.FranchiseId = @FranchiseId 
            and l.LeadStatusId <> 31422
            and isnull(c.IsDeleted, 0) = 0 
			and ( c.HomePhone like  @phoneValue 
				or c.WorkPhone like @phoneValue
				or c.CellPhone like @phoneValue ) 
		Union 

		select a.AccountId Id, Concat(c.FirstName, ' ', c.LastName) Name
        , [CRM].[fnGetAccountNameByAccountId](a.AccountId) LeadAccountName
        , ac.AccountId Id, ac.IsPrimaryCustomer, ac.CustomerId
        , c.PrimaryEmail Email, CASE WHEN c.CellPhone like @phoneValue THEN c.CellPhone
			WHEN c.HomePhone like @phoneValue THEN c.HomePhone
			WHEN c.WorkPhone like @phoneValue THEN 
				CASE WHEN isnull(c.WorkPhoneExt, '') <> '' THEN CONCAT(c.WorkPhone, ' - ', c.WorkPhoneExt) 
				ELSE c.WorkPhone
				END 
			ELSE CASE c.PreferredTFN 
				WHEN 'C' THEN c.CellPhone
				WHEN 'H' THEN c.HomePhone
				WHEN 'W' THEN 
					CASE WHEN isnull(c.WorkPhoneExt, '') <> '' THEN CONCAT(c.WorkPhone, ' - ', c.WorkPhoneExt) 
					ELSE c.WorkPhone
					END
				ELSE ''
				END 
			END Phone 
        , 0 IsLead
        from crm.Customer c 
        Join crm.AccountCustomers ac on ac.CustomerId = c.CustomerId
        join crm.Accounts a on a.AccountId = ac.AccountId
        where a.FranchiseId = @FranchiseId
			and isnull(c.IsDeleted, 0) = 0 
			and ( c.HomePhone like  @phoneValue 
				or c.WorkPhone like @phoneValue
				or c.CellPhone like @phoneValue ) 
        Order by LeadAccountName
		
	END;
ELSE IF (@SearchType = 'all')
	BEGIN
		select l.LeadId Id, Concat(c.FirstName, ' ', c.LastName) Name
        , [CRM].[fnGetLeadNameByLeadId](l.LeadId) LeadAccountName
        , lc.LeadId Id,lc.IsPrimaryCustomer,lc.CustomerId 
        , CASE WHEN isnull(c.PrimaryEmail,'') <> '' THEN c.PrimaryEmail ELSE c.SecondaryEmail END Email
		, CASE WHEN c.CellPhone like @phoneValue THEN c.CellPhone
			WHEN c.HomePhone like @phoneValue THEN c.HomePhone
			WHEN c.WorkPhone like @phoneValue THEN 
				CASE WHEN isnull(c.WorkPhoneExt, '') <> '' THEN CONCAT(c.WorkPhone, ' - ', c.WorkPhoneExt) 
				ELSE c.WorkPhone
				END 
			ELSE CASE c.PreferredTFN 
				WHEN 'C' THEN c.CellPhone
				WHEN 'H' THEN c.HomePhone
				WHEN 'W' THEN 
					CASE WHEN isnull(c.WorkPhoneExt, '') <> '' THEN CONCAT(c.WorkPhone, ' - ', c.WorkPhoneExt) 
					ELSE c.WorkPhone
					END
				ELSE ''
				END 
			END Phone 
		        , 1 IsLead
        from crm.Customer c 
        Join crm.LeadCustomers lc on lc.CustomerId = c.CustomerId
        join crm.Leads l on l.LeadId = lc.LeadId
        where l.FranchiseId = @FranchiseId 
            and l.LeadStatusId <> 31422
            and isnull(c.IsDeleted, 0) = 0 

			and ( c.HomePhone like  @phoneValue 
	            or c.WorkPhone like @phoneValue
	            or c.CellPhone like @phoneValue 
                or c.PrimaryEmail like  @Value 
	            or c.SecondaryEmail like @Value
				or c.FirstName like  @Value 
				or c.LastName like @Value
				or CONCAT(C.firstname, ' ', C.LastName) like @Value
				or c.CompanyName like @Value) 
        Union

		select a.AccountId Id, Concat(c.FirstName, ' ', c.LastName) Name
        , [CRM].[fnGetAccountNameByAccountId](a.AccountId) LeadAccountName
        , ac.AccountId Id, ac.IsPrimaryCustomer, ac.CustomerId
        ,CASE WHEN isnull(c.PrimaryEmail,'') <> '' THEN c.PrimaryEmail ELSE c.SecondaryEmail END Email
		, CASE WHEN c.CellPhone like @phoneValue THEN c.CellPhone
			WHEN c.HomePhone like @phoneValue THEN c.HomePhone
			WHEN c.WorkPhone like @phoneValue THEN 
				CASE WHEN isnull(c.WorkPhoneExt, '') <> '' THEN CONCAT(c.WorkPhone, ' - ', c.WorkPhoneExt) 
				ELSE c.WorkPhone
				END 
			ELSE CASE c.PreferredTFN 
				WHEN 'C' THEN c.CellPhone
				WHEN 'H' THEN c.HomePhone
				WHEN 'W' THEN 
					CASE WHEN isnull(c.WorkPhoneExt, '') <> '' THEN CONCAT(c.WorkPhone, ' - ', c.WorkPhoneExt) 
					ELSE c.WorkPhone
					END
				ELSE ''
				END 
			END Phone 
        , 0 IsLead
        from crm.Customer c 
        Join crm.AccountCustomers ac on ac.CustomerId = c.CustomerId
        join crm.Accounts a on a.AccountId = ac.AccountId
        where a.FranchiseId = @FranchiseId
			and isnull(c.IsDeleted, 0) = 0

			and ( c.HomePhone like  @phoneValue 
				or c.WorkPhone like @phoneValue
				or c.CellPhone like @phoneValue 
				or c.PrimaryEmail like  @Value 
				or c.SecondaryEmail like @Value
				or c.FirstName like  @Value 
				or c.LastName like @Value
				or CONCAT(C.firstname, ' ', C.LastName) like @Value
				or c.CompanyName like @Value) 
		Order by LeadAccountName
	END
ELSE IF (@SearchType = 'name')
	BEGIN
		select l.LeadId Id, Concat(c.FirstName, ' ', c.LastName) Name
        , [CRM].[fnGetLeadNameByLeadId](l.LeadId) LeadAccountName
        , lc.LeadId Id,lc.IsPrimaryCustomer,lc.CustomerId 
        , c.PrimaryEmail Email, CASE c.PreferredTFN 
		        WHEN 'C' THEN c.CellPhone
		        WHEN 'H' THEN c.HomePhone
		        WHEN 'W' THEN 
					CASE WHEN isnull(c.WorkPhoneExt, '') <> '' THEN CONCAT(c.WorkPhone, ' - ', c.WorkPhoneExt) 
					ELSE c.WorkPhone
					END
				ELSE ''
		        END Phone 
		        , 1 IsLead
        from crm.Customer c 
        Join crm.LeadCustomers lc on lc.CustomerId = c.CustomerId
        join crm.Leads l on l.LeadId = lc.LeadId
        where l.FranchiseId = @FranchiseId 
            and l.LeadStatusId <> 31422
            and isnull(c.IsDeleted, 0) = 0 

			and ( c.FirstName like  @Value 
				or c.LastName like @Value
				or CONCAT(C.FirstName, ' ', C.LastName) like @Value
				or c.CompanyName like @Value) 
        Union

		select a.AccountId Id, Concat(c.FirstName, ' ', c.LastName) Name
        , [CRM].[fnGetAccountNameByAccountId](a.AccountId) LeadAccountName
        , ac.AccountId Id, ac.IsPrimaryCustomer, ac.CustomerId
        , c.PrimaryEmail Email, CASE c.PreferredTFN 
		        WHEN 'C' THEN c.CellPhone
		        WHEN 'H' THEN c.HomePhone
		        WHEN 'W' THEN 
					CASE WHEN isnull(c.WorkPhoneExt, '') <> '' THEN CONCAT(c.WorkPhone, ' - ', c.WorkPhoneExt) 
					ELSE c.WorkPhone
					END
				ELSE ''
		        END Phone 
        , 0 IsLead
        from crm.Customer c 
        Join crm.AccountCustomers ac on ac.CustomerId = c.CustomerId
        join crm.Accounts a on a.AccountId = ac.AccountId
        where a.FranchiseId = @FranchiseId
			and isnull(c.IsDeleted, 0) = 0

			and ( c.FirstName like  @Value 
				or c.LastName like @Value
				or CONCAT(C.firstname, ' ', C.LastName) like @Value
				or c.CompanyName like @Value) 
		Order by LeadAccountName
	END
ELSE 
	BEGIN
		declare @messsage varchar(500) = 'Invaid search type :' + @SearchType;
		THROW 60000, @messsage, 1;
	END;
	
	