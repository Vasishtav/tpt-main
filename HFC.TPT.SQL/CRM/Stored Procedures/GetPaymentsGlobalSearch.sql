﻿CREATE PROC [CRM].[GetPaymentsGlobalSearch]
( @FranchiseId int
	,@SearchType varchar(50)
	, @Value varchar(500))

AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
set @Value = '%'+ @Value + '%';

IF (@SearchType = 'name')
	BEGIN
		select p.PaymentID, p.OrderId, ord.OrderNumber, pm.PaymentMethod, p.PaymentDate
        , p.Amount PaymentAmount, p.Memo, rrc.Reason ReverseReason, p.VerificationCheck Authorisation
        ,  Concat(cust.FirstName, ' ', cust.LastName )Account
        , a.AccountId
        from crm.Payments p
        left join crm.Opportunities opp on opp.OpportunityId = p.OpportunityID
        left join crm.Accounts a on a.AccountId = opp.AccountId
        left join crm.AccountCustomers ac on ac.AccountId = a.AccountId and ac.IsPrimaryCustomer = 1
        left join crm.Customer cust on cust.CustomerId = ac.CustomerId
        left join crm.Type_PaymentMethod pm on pm.Id = p.PaymentMethod
        left join crm.Type_ReverseReasonCode rrc on rrc.ReasonCodeId = p.ReasonCode
        left join crm.Orders ord on ord.OrderId = p.OrderId
        where opp.FranchiseId = @FranchiseId 

			and (p.Memo like @Value 
				or rrc.Reason like @Value 
				or cust.FirstName like @Value 
				or cust.LastName like @Value 
				or CONCAT(cust.firstname, ' ', cust.LastName) like @Value ) 
		Order by Account
	END
ELSE IF (@SearchType = 'id')
	BEGIN
		select p.PaymentID, p.OrderId, ord.OrderNumber, pm.PaymentMethod, p.PaymentDate
        , p.Amount PaymentAmount, p.Memo, rrc.Reason ReverseReason, p.VerificationCheck Authorisation
        ,  Concat(cust.FirstName, ' ', cust.LastName )Account
        , a.AccountId
        from crm.Payments p
        left join crm.Opportunities opp on opp.OpportunityId = p.OpportunityID
        left join crm.Accounts a on a.AccountId = opp.AccountId
        left join crm.AccountCustomers ac on ac.AccountId = a.AccountId and ac.IsPrimaryCustomer = 1
        left join crm.Customer cust on cust.CustomerId = ac.CustomerId
        left join crm.Type_PaymentMethod pm on pm.Id = p.PaymentMethod
        left join crm.Type_ReverseReasonCode rrc on rrc.ReasonCodeId = p.ReasonCode
        left join crm.Orders ord on ord.OrderId = p.OrderId
        where opp.FranchiseId = @FranchiseId 

			and (p.PaymentID like @Value  
				or ord.OrderNumber like @Value)
		Order by Account
	END
ELSE IF (@SearchType = 'all')
	BEGIN
		select p.PaymentID, p.OrderId, ord.OrderNumber, pm.PaymentMethod, p.PaymentDate
        , p.Amount PaymentAmount, p.Memo, rrc.Reason ReverseReason, p.VerificationCheck Authorisation
        ,  Concat(cust.FirstName, ' ', cust.LastName )Account
        , a.AccountId
        from crm.Payments p
        left join crm.Opportunities opp on opp.OpportunityId = p.OpportunityID
        left join crm.Accounts a on a.AccountId = opp.AccountId
        left join crm.AccountCustomers ac on ac.AccountId = a.AccountId and ac.IsPrimaryCustomer = 1
        left join crm.Customer cust on cust.CustomerId = ac.CustomerId
        left join crm.Type_PaymentMethod pm on pm.Id = p.PaymentMethod
        left join crm.Type_ReverseReasonCode rrc on rrc.ReasonCodeId = p.ReasonCode
        left join crm.Orders ord on ord.OrderId = p.OrderId
        where opp.FranchiseId = @FranchiseId

		and (p.Memo like @Value 
		    or rrc.Reason like @Value 
	        or cust.FirstName like @Value 
	        or cust.LastName like @Value 
            or CONCAT(cust.firstname, ' ', cust.LastName) like @Value 
		    or p.PaymentID like @Value  
		    or ord.OrderNumber like @Value) 
        Order by Account
	END
ELSE 
	BEGIN
		declare @messsage varchar(500) = 'Invaid search type :' + @SearchType;
		THROW 60000, @messsage, 1;
	END;
	
	