﻿

/*
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
EXEC [CRM].[spInvoiceNumber_New] 2092 
-- =============================================
*/
create PROCEDURE [CRM].[spInvoiceNumber_New]  
	@FranchiseId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF EXISTS(SELECT * FROM CRM.LeadNJobNumbers WHERE FranchiseId = @FranchiseId)
	BEGIN
		UPDATE CRM.LeadNJobNumbers 
		SET [InvoiceNumber] = ISNULL([InvoiceNumber],0) + 1
		OUTPUT INSERTED.InvoiceNumber 
		WHERE FranchiseId = @FranchiseId
	END
	ELSE
	BEGIN
		INSERT INTO CRM.LeadNJobNumbers (FranchiseId) 
		OUTPUT INSERTED.InvoiceNumber
		VALUES(@FranchiseId)
	END
END

