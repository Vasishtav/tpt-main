﻿CREATE PROC [CRM].[GetAppointmentsGlobalSearch]
( @FranchiseId int
	,@SearchType varchar(50)
	, @Value varchar(500))

AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
set @Value = '%'+ @Value + '%';

IF (@SearchType = 'name' OR @SearchType = 'all')
	BEGIN
		select c.CalendarId, c.Subject
        , c.LeadId, c.AccountId, c.OpportunityId, c.OrderId
        , (SELECT Stuff((SELECT ', ' + PersonName FROM CRM.EventToPeople
                                    where CalendarId=c.CalendarId FOR XML PATH('')), 1, 2, '')) AS AttendeesName

        , (SELECT Stuff((SELECT ', ' + convert(varchar, PersonId) FROM CRM.EventToPeople
                                    where CalendarId=c.CalendarId FOR XML PATH('')), 1, 2, '')) AS AttendeesId

        , c.StartDate StartDateUTC, c. EndDate EndDateUTC
        , (case  when isnull(c.OrderId, 0) <> 0 then concat('Ord-', o.OrderName)
	        when isnull(c.OpportunityId, 0) <> 0 then concat('Opp-', opp.OpportunityName)
	        when isnull(c.AccountId, 0) <> 0 then concat('Acct-', ac.firstname, ' ', ac.LastName)
	        when  isnull(c.LeadId, 0) <> 0 then   concat('Lead-', lc.firstname, ' ', lc.LastName)
	        else '' end)as Related 
        , at.Name AppointmentType, c.IsCancelled
        from crm.Calendar c
        Left join crm.Orders o on o.OrderID = c.OrderId
        Left join crm.Opportunities opp on opp.OpportunityId = c.OpportunityId
        Left join crm.AccountCustomers acj on acj.AccountId = c.AccountId and acj.IsPrimaryCustomer = 1
        Left join crm.Customer ac on ac.CustomerId = acj.CustomerId
        Left join crm.LeadCustomers lcj on lcj.LeadId = c.LeadId and lcj.IsPrimaryCustomer = 1
        Left join crm.Customer lc on lc.CustomerId = lcj.CustomerId
        Left join crm.Type_Appointments at on at.AppointmentTypeId = c.AptTypeEnum
        where c.FranchiseId = @FranchiseId
			and ( c.Subject like @Value
				or lc.FirstName like @Value 
				or lc.LastName like @Value 
				or CONCAT(lc.firstname, ' ', lc.LastName) like @Value
				or ac.FirstName like @Value 
				or ac.LastName like @Value 
				or CONCAT(ac.firstname, ' ', ac.LastName) like @Value
				or opp.OpportunityName like @Value 
				or c.Message like @Value 
				or c.CalendarId in (select CalendarId from crm.EventToPeople 
					where PersonName like @Value) 
				or c.location like @Value) 
			and	isnull(c.IsDeleted, 0) = 0
        Order by c.StartDate Desc
	END
ELSE 
	BEGIN
		declare @messsage varchar(500) = 'Invaid search type :' + @SearchType;
		THROW 60000, @messsage, 1;
	END;
	
	