﻿


CREATE PROC [CRM].[SeachLeadAccountById]
(
@AccountId int
)
as
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

--select * from CRM.Accounts where AccountId=39
--http://localhost:6166/#!/leads/2441146
--Declare @LeadId int;
--set @LeadId=2441146;



SELECT 
     Account.[AccountId]
	 ,Account.FranchiseId
    ,Account.[AccountGuid]
    ,Account.[PersonId]
    ,Account.[AccountNumber]
    ,[LastUpdatedOnUtc]
    ,Account.[Notes]
    ,Account.[CreatedOnUtc]
	,[CRM].[fnGetAccountTypeName_forAccountSearch](Account.AccountTypeId) as AccountType
     ,Account.AccountTypeId 
    ,Account.[AccountStatusID]
    ,[Address1]
    ,[Address2]
    ,[City]
    ,[State]
    ,[ZipCode]
    ,[AddressesGuid]
    ,a.[AddressId]
    ,p.[WorkPhone]
    ,p.[HomePhone]
    ,p.[CellPhone]
    ,p.[FirstName]
    ,p.[LastName]
    ,p.[PrimaryEmail]
    ,p.PreferredTFN
    ,p.WorkPhoneExt
    ,p.CompanyName
    ,p.[FaxPhone]
    ,Account.SecPersonId
    ,tls.Name AS [Status]
 FROM [crm].[Accounts] Account 
 INNER JOIN CRM.Type_AccountStatus tls ON tls.Accountstatusid = Account.Accountstatusid
 INNER JOIN [CRM].[Addresses] a
  on a.[AddressId]=(select top 1 la.AddressId from crm.AccountAddresses la where la.AccountId = Account.AccountId)
 INNER JOIN [CRM].[Customer] p 
  on p.CustomerId=Account.[PersonId]
 LEFT JOIN [CRM].[Customer] secondPerson  
  on secondPerson.CustomerId = Account.SecPersonId
WHERE 
--(Account.FranchiseId= @FranchiseId) and
Account.AccountId=@AccountId
 AND (ISNULL(Account.IsDeleted,0)=0)


