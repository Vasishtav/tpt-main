﻿CREATE procedure [CRM].[VendorInvoiceDashboard] 
  @FranchiseId int,
  @vendorId int,
  @datefilter varchar(200),
  @PersonId varchar(max) = null
as
Begin
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
DECLARE @StartDate datetime,@EndDate datetime

	set @EndDate = CAST(getdate() as date)

	if(@datefilter='4000') --Today
		set @StartDate = CAST(getdate() as date)
		
	else if(@datefilter = '4001') -- LastDay
		set @StartDate = CAST(getdate()-1 as date)
		
	else if(@datefilter = '4002') -- Week to today date From Sunday
		SELECT @StartDate = CAST(DATEADD(DAY, 1-DATEPART(WEEKDAY, GETDATE()), GETDATE()) as Date)
		
	else if(@datefilter = '4003') -- Month to today date
		SELECT @StartDate = DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0)
		
	else if(@datefilter = '4004')
		SELECT @StartDate = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0)
		
	else if(@datefilter = '4005')  -- Last 7 days
		select @StartDate = Cast(GETDATE()-7 as Date)

	else if(@datefilter='4006')  -- Last 14 days
		select @StartDate=Cast(GETDATE()-14 as Date)

	else if(@datefilter='4007')  -- Last 30 days
		select @StartDate=Cast(GETDATE()-30 as Date)

	else if(@datefilter='4008')  -- Last 90 days
		select @StartDate=Cast(GETDATE()-90 as Date)

	else if(@datefilter='4009')  -- Last 180 days
		select @StartDate=Cast(GETDATE()-180 as Date)
	else
		select @StartDate= cast(-53680 as datetime)

	if(@vendorId>0)
	Begin
	select *,
		case when VarianceAmount!=0 then 1 else 0 end PPV
		 from (
		select distinct vi.Date,vi.InvoiceNumber,vi.VendorName
		,vi.PICVpo,vi.PurchaseOrderId
		,o.OrderID,o.OrderNumber
		,case when q.SideMark is not null then q.SideMark else opp.SideMark end SideMark
		,vi.Quantity
		,sum(ISNULL(qld.BaseCost,0)*ql.Quantity) QuotedProductCost
		,sum(ISNULL(vid.Cost,0)*vid.Quantity) InvoicedProductCost
		,ISNULL(vi.Freight,0)+ISNULL(vi.Tax,0)+ISNULL(vi.ProcessingFee,0)+ISNULL(vi.MiscCharge,0) OtherCharges
		,ISNULL(vi.Total,0) InvoiceTotal
		,ISNULL(vi.Total,0) - sum(ISNULL(qld.BaseCost,0)*ql.Quantity) VarianceAmount 
		from CRM.VendorInvoice vi
		join CRM.VendorInvoiceDetail vid on vi.VendorInvoiceId=vid.VendorInvoiceId
		join CRM.MasterPurchaseOrder mpo on mpo.PurchaseOrderId=vi.PurchaseOrderId
		join CRM.MasterPurchaseOrderExtn mpox on vi.PurchaseOrderId=mpox.PurchaseOrderId and mpo.PurchaseOrderId=mpox.PurchaseOrderId
		join CRM.Orders o on mpox.OrderId=o.OrderID
		join CRM.Opportunities opp on o.OpportunityId=opp.OpportunityId
		join CRM.Quote q on q.QuoteKey=o.QuoteKey
		join CRM.QuoteLines ql on q.QuoteKey=ql.QuoteKey and vid.QuoteLineId=ql.QuoteLineId	
		join CRM.QuoteLineDetail qld on ql.QuoteLineId=qld.QuoteLineId and qld.QuoteLineId=vid.QuoteLineId
		where opp.FranchiseId=@FranchiseId and PICVpo is not null
		and (@PersonId is null or opp.SalesAgentId in (select id from [CRM].[CSVToTable] (@PersonId)))
		and ql.VendorId=@vendorId --- Only vendor input passed
		and CAST(vi.CreatedOn as date) between @StartDate and @EndDate
		group by vi.Date,vi.InvoiceNumber,vi.VendorName,vi.PICVpo,o.OrderID,o.OrderNumber,q.SideMark,vi.Quantity
		,vi.Freight,vi.Tax,vi.ProcessingFee,vi.MiscCharge,vi.Total,vi.Subtotal,vi.Discount,opp.SideMark,vi.PurchaseOrderId ) as p
	end
	else
	begin
	select *,
		case when VarianceAmount!=0 then 1 else 0 end PPV
		 from (
		select distinct vi.Date,vi.InvoiceNumber,vi.VendorName
		,vi.PICVpo,vi.PurchaseOrderId
		,o.OrderID,o.OrderNumber
		,case when q.SideMark is not null then q.SideMark else opp.SideMark end SideMark
		,vi.Quantity
		,sum(ISNULL(qld.BaseCost,0)*ql.Quantity) QuotedProductCost
		,sum(ISNULL(vid.Cost,0)*vid.Quantity) InvoicedProductCost
		,ISNULL(vi.Freight,0)+ISNULL(vi.Tax,0)+ISNULL(vi.ProcessingFee,0)+ISNULL(vi.MiscCharge,0) OtherCharges
		,ISNULL(vi.Total,0) InvoiceTotal
		,ISNULL(vi.Total,0) - sum(ISNULL(qld.BaseCost,0)*ql.Quantity) VarianceAmount 
		from CRM.VendorInvoice vi
		join CRM.VendorInvoiceDetail vid on vi.VendorInvoiceId=vid.VendorInvoiceId
		join CRM.MasterPurchaseOrder mpo on mpo.PurchaseOrderId=vi.PurchaseOrderId
		join CRM.MasterPurchaseOrderExtn mpox on vi.PurchaseOrderId=mpox.PurchaseOrderId and mpo.PurchaseOrderId=mpox.PurchaseOrderId
		join CRM.Orders o on mpox.OrderId=o.OrderID
		join CRM.Opportunities opp on o.OpportunityId=opp.OpportunityId
		join CRM.Quote q on q.QuoteKey=o.QuoteKey
		join CRM.QuoteLines ql on q.QuoteKey=ql.QuoteKey and vid.QuoteLineId=ql.QuoteLineId	
		join CRM.QuoteLineDetail qld on ql.QuoteLineId=qld.QuoteLineId and qld.QuoteLineId=vid.QuoteLineId
		where opp.FranchiseId=@FranchiseId and PICVpo is not null
		and (@PersonId is null or opp.SalesAgentId in (select id from [CRM].[CSVToTable] (@PersonId)))
		and CAST(vi.CreatedOn as date) between @StartDate and @EndDate
		group by vi.Date,vi.InvoiceNumber,vi.VendorName,vi.PICVpo,o.OrderID,o.OrderNumber,q.SideMark,vi.Quantity
		,vi.Freight,vi.Tax,vi.ProcessingFee,vi.MiscCharge,vi.Total,vi.Subtotal,vi.Discount,opp.SideMark,vi.PurchaseOrderId ) as p
	end	

End

