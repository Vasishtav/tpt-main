Create procedure [CRM].[Report_FranchiseMSR] (
	  @TerritoryID int
	, @Mth date )

as
begin

set nocount on

declare @FranchiseID int

select @FranchiseID = FranchiseID from CRM.Territories where TerritoryId = @TerritoryID

select
	 op.FranchiseId
	,op.TerritoryId
	,case when pt.ProductType != 'Service' then 1 else 2 end DisplaySort
	,case when pt.ProductType != 'Service' then 'Product' else 'Adjustment' end DisplayGroup
	,pt.ProductType
	,case when pt.ProductType = 'Service' then 'Surcharges' else ql.ProductName end Category
	,convert(datetime, format(o.ContractedDate, 'yyyy-MM-01')) Mth
	,isnull(Sum(qld.BaseCost*qld.Quantity),0) Cost
	,isnull(Sum(qld.ExtendedPrice), 0) ExtendedPrice
	,isnull(sum(qld.NetCharge), 0) Retail
into #RevenueBase
from CRM.Orders o
join CRM.Quote q on o.QuoteKey=q.QuoteKey AND q.PrimaryQuote=1
join CRM.Opportunities op on q.OpportunityId=op.OpportunityId
join CRM.QuoteLines ql on q.QuoteKey=ql.QuoteKey
join CRM.QuoteLineDetail qld on ql.QuoteLineId=qld.QuoteLineId
join CRM.Type_ProductType pt on ql.ProductTypeId = pt.Id
WHERE convert(datetime, format(o.ContractedDate, 'yyyy-MM-01'))  = @Mth
AND op.FranchiseID = @FranchiseId
AND o.OrderStatus NOT IN (6,9)
group by
		op.FranchiseID
	,op.TerritoryID
	,convert(datetime, format(o.ContractedDate, 'yyyy-MM-01'))
	,case when pt.ProductType = 'Service' then 'Surcharges' else ql.ProductName end
	,pt.ProductType
Union
select
	 op.FranchiseId
	,op.TerritoryId
	,3 DisplaySort
	,'Adjustment' DisplayGroup
	,'Discount' ProductType
	,'Discount' Category
	,convert(datetime, format(o.ContractedDate, 'yyyy-MM-01')) Mth
	,0 Cost
	,-sum(o.OrderDiscount) ExtendedPrice
	,0 Retail
from CRM.Orders o
join CRM.Quote q on o.QuoteKey=q.QuoteKey AND q.PrimaryQuote=1
join CRM.Opportunities op on q.OpportunityId=op.OpportunityId
WHERE convert(datetime, format(o.ContractedDate, 'yyyy-MM-01'))  = @Mth
AND op.FranchiseID = @FranchiseId
AND o.OrderStatus NOT IN (6,9)
group by
		op.FranchiseID
	,op.TerritoryID
	,convert(datetime, format(o.ContractedDate, 'yyyy-MM-01'));

with
	  TerrTotals as (select TerritoryID, sum(Retail) TotalRetail from #RevenueBase where TerritoryID is not null group by TerritoryID)
	, TerrAlloc as (Select TerritoryId, TotalRetail / NULLIF((select sum(Retail) TotalRetail from #RevenueBase where TerritoryID is not null), 0) GrayAlloc from TerrTotals)
	, Prods as (Select distinct  rb.FranchiseID, t.TerritoryID, rb.Mth, rb.DisplaySort, rb.DisplayGroup, rb.ProductType, rb.Category from #RevenueBase rb join CRM.Territories t on t.FranchiseID = rb.FranchiseID)
Select
	  p.FranchiseId
	, p.TerritoryId
	, p.DisplaySort
	, p.DisplayGroup
	, p.ProductType
	, p.Category
	, p.Mth
	, isnull(rbt.Cost, 0) Cost
	, isnull(rbt.ExtendedPrice, 0) ExtendedPrice
	, isnull(rbt.Retail, 0) Retail
	, isnull(rbg.Cost * ta.GrayAlloc, 0) GrayAllocCost
	, isnull(rbg.ExtendedPrice * ta.GrayAlloc, 0) GrayAllocExtendedPrice
	, isnull(rbg.Retail * ta.GrayAlloc, 0) GrayAllocRetail
	, isnull(rbt.Cost + (rbg.Cost * ta.GrayAlloc), 0) TotalWithGrayAllocCost
	, isnull(rbt.ExtendedPrice + (rbg.ExtendedPrice * ta.GrayAlloc), 0) TotalWithGrayAllocExtendedPrice
	, isnull(rbt.Retail + (rbg.Retail * ta.GrayAlloc), 0) TotalWithGrayAllocRetail
into #Output
from Prods p
left join TerrAlloc ta
	on p.TerritoryId = ta.TerritoryId
left join #RevenueBase rbt
	on p.DisplayGroup = rbt.DisplayGroup
	and p.ProductType = rbt.ProductType
	and p.Category = rbt.Category
	and p.Mth = rbt.Mth
	and p.TerritoryID = rbt.TerritoryId
left join #RevenueBase rbg
	on  p.DisplayGroup = rbg.DisplayGroup
	and p.ProductType = rbg.ProductType
	and p.Category = rbg.Category
	and p.Mth = rbg.Mth
	and rbg.TerritoryId is null

drop table #RevenueBase

Select
	  t.Name + isnull(' - ' + t.Code, '') Territory
	, o.Mth
	, o.DisplaySort
	, o.DisplayGroup
	, dbo.ProperCase(o.Category) Category
	, o.Cost
	, o.Retail
from #Output o
join CRM.Territories t
	on o.TerritoryId = t.TerritoryId
where o.TerritoryID = @TerritoryID
union all
Select
	  t.Name + isnull(' - ' + t.Code, '') Territory
	,  o.Mth
	, 4 DisplaySort
	, 'Gray Allocation' DisplayGroup
	, 'Gray Allocation' Category
	, 0 Cost
	, sum(o.GrayAllocRetail) Retail
from #Output o
join CRM.Territories t
	on o.TerritoryId = t.TerritoryId
where o.TerritoryID = @TerritoryID
group by t.Name + isnull(' - ' + t.Code, ''), o.Mth
order by DisplaySort, Category

drop table #Output

end