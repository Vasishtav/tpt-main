﻿CREATE PROCEDURE [CRM].[OrderSearchByAccoutId]
(
@FranchiseId int,
@AccountId int
)
as
begin
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET NOCOUNT ON;
select 
		case when op.TerritoryId is not null then tr.Name else op.TerritoryType end as Territory
		,o.OrderId
		,OrderNumber
		,CONVERT(date, ContractedDate) ContractDate
		,q.QuoteKey
		,q.QuoteID
		,op.AccountId
		,(select [CRM].[fnGetAccountNameByAccountId](op.AccountId)) AccountName
		,op.OpportunityId
		,op.OpportunityName
		,q.SideMark
		,p.FirstName+' '+p.LastName SalesAgentName
		,COUNT(1) TotalLines
		,SUM(isnull(ql.Quantity,0)) TotalQTY
		,isnull(OrderTotal,0) TotalAmount
		,isnull(o.BalanceDue,0) BalanceDue
		,os.OrderStatus as [Status]
		,o.CreatedOn
		,op.InstallerId
		,op.SalesAgentId
		from [CRM].[Orders] o
		 join [CRM].[Opportunities] op on o.OpportunityId=op.OpportunityId
		 join [CRM].[Quote] q on o.QuoteKey = q.QuoteKey
		 join CRM.QuoteLines ql on q.QuoteKey=ql.QuoteKey
		 left join [CRM].[Person] p on op.SalesAgentId=p.PersonId
		 left join [CRM].[Type_OrderStatus] os on o.OrderStatus=os.OrderStatusId
		 LEFT JOIN CRM.Territories tr on tr.TerritoryId=op.TerritoryId
		 where 
		 op.FranchiseId=@FranchiseId and op.AccountId=@AccountId 		
		 Group by
		 op.TerritoryId,tr.Name,op.TerritoryType
		,o.OrderId
		,OrderNumber
		,ContractedDate
		,q.QuoteKey
		,q.QuoteID
		,op.AccountId
		,op.OpportunityId
		,op.OpportunityName
		,q.SideMark
		,p.FirstName,p.LastName
		,OrderTotal
		,o.BalanceDue
		,os.OrderStatus
		,o.CreatedOn
		,op.InstallerId
		,op.SalesAgentId
		 order by o.CreatedOn desc 
end
GO

