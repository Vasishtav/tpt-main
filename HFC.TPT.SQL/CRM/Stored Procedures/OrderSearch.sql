﻿CREATE PROCEDURE [CRM].[OrderSearch] 
(
@FranchiseId int,
@DateRangeId varchar(100)='',
@selectedOrderStatus varchar(500)=''
)
as
begin
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @CreatedOn datetime

	if(@DateRangeId = '4005')  -- Last 7 days
		select @CreatedOn = Cast(GETDATE()-7 as Date)

	else if(@DateRangeId='4006')  -- Last 14 days
		select @CreatedOn=Cast(GETDATE()-14 as Date)

	else if(@DateRangeId='4007')  -- Last 30 days
		select @CreatedOn=Cast(GETDATE()-30 as Date)

	else if(@DateRangeId='4008')  -- Last 90 days
		select @CreatedOn=Cast(GETDATE()-90 as Date)

	else if(@DateRangeId='4009')  -- Last 180 days
		select @CreatedOn=Cast(GETDATE()-180 as Date)
	else
		select @CreatedOn= cast(-53680 as datetime)

	if(@selectedOrderStatus='')
	set @selectedOrderStatus=(select stuff((select ','+ Cast(OrderStatusId as varchar(100)) from CRM.Type_OrderStatus 
  where OrderStatusId not in (5,6,9) for xml path('')), 1, 1, '' ))

select 
		case when op.TerritoryId is not null then tr.Name else op.TerritoryType end as Territory
		,o.OrderId
		,OrderNumber
		,CONVERT(date, ContractedDate) ContractDate
		,q.QuoteKey
		,q.QuoteID
		,op.AccountId
		,(select [CRM].[fnGetAccountNameByAccountId](op.AccountId)) AccountName
		,op.OpportunityId
		,op.OpportunityName
		,q.SideMark
		,p.FirstName+' '+p.LastName SalesAgentName
		,COUNT(1) TotalLines
		,SUM(isnull(ql.Quantity,0)) TotalQTY
		,isnull(OrderTotal,0) TotalAmount
		,isnull(o.BalanceDue,0) BalanceDue
		,os.OrderStatus as [Status]
		,o.CreatedOn
		,op.InstallerId
		,op.SalesAgentId
		from [CRM].[Orders] o
		 join [CRM].[Opportunities] op on o.OpportunityId=op.OpportunityId
		 join [CRM].[Quote] q on o.QuoteKey = q.QuoteKey
		 join CRM.QuoteLines ql on q.QuoteKey=ql.QuoteKey
		 left join [CRM].[Person] p on op.SalesAgentId=p.PersonId
		 left join [CRM].[Type_OrderStatus] os on o.OrderStatus=os.OrderStatusId
		 LEFT JOIN CRM.Territories tr on tr.TerritoryId=op.TerritoryId
		 where 
		 op.FranchiseId=@FranchiseId 
		 and cast(o.CreatedOn as date) > @CreatedOn 
		 and o.OrderStatus IN (select id from CRM.CSVToTable(@selectedOrderStatus))
		 Group by
		 op.TerritoryId,tr.Name,op.TerritoryType
		,o.OrderId
		,OrderNumber
		,ContractedDate
		,q.QuoteKey
		,q.QuoteID
		,op.AccountId
		,op.OpportunityId
		,op.OpportunityName
		,q.SideMark
		,p.FirstName,p.LastName
		,OrderTotal
		,o.BalanceDue
		,os.OrderStatus
		,o.CreatedOn
		,op.InstallerId
		,op.SalesAgentId
		 order by o.CreatedOn desc 
  end
GO

