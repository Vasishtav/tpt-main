﻿CREATE PROCEDURE [CRM].[UpdateVendorAllianceCurrency]
	@VendorId int
	
AS
BEGIN
UPDATE fv 
SET fv.Currency=
CASE WHEN f.Currency='US' AND h.USCurrency=1 then 223
WHEN f.Currency='CA' AND h.CANCurrency = 1 THEN 39
WHEN h.USCurrency=1 THEN 223
else 39 end 
FROM Acct.FranchiseVendors fv
INNER JOIN Acct.HFCVendors h ON h.VendorId = fv.VendorId
INNER JOIN crm.Franchise f ON f.FranchiseId = fv.FranchiseId
INNER JOIN crm.Addresses a ON a.AddressId = f.AddressId
WHERE fv.VendorType=1 AND h.IsAlliance=1 AND h.IsActive=1 AND fv.VendorStatus=1 and h.VendorId=@VendorId

END	