﻿CREATE procedure [CRM].[Report_SalesSummary] (
		@FranchiseID int
	,	@StartDate datetime
	,	@EndDate datetime
	,	@PersonId varchar(max) = null
	,	@IndustryId int=null
	,	@CommercialTypeId int=null
	,	@CustomerName varchar(200)=null )
as
begin
--SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
select	isnull(p.FirstName, '') FirstName
	,	isnull(p.LastName, '') LastName
	,	isnull(p.CompanyName, '') Company
	,	dbo.ProperCase(sa.FirstName + ' ' + sa.LastName) SalesPerson
	,	s.Name [Source]
	,	isnull(c.Name, '') Campaign
	,	o.OrderNumber
	,	po.MasterPONumber
	,	dbo.ProperCase(ps.POStatusName) MPOStatus
	,	cast(o.ContractedDate as Date) OrderDate
	,	os.OrderStatus
	,	sum (qld.NetCharge) TotalSales
	,	sum (qld.BaseCost * qld.Quantity) COGS
	,	sum (case when ql.ProductTypeId not in (3,4) then qld.BaseCost * qld.Quantity end) COGSProd
	,	sum (qld.Quantity) QTY
	,	sum (case when ql.ProductTypeId not in (4) then qld.NetCharge else 0 end) - sum (case when ql.ProductTypeId not in (4) then qld.BaseCost * qld.Quantity else 0 end) GPValue
	,	case when isnull(  (sum (case when ql.ProductTypeId not in (4) then qld.NetCharge else 0 end) - sum (case when ql.ProductTypeId not in (4) then qld.BaseCost * qld.Quantity else 0 end) ) / nullif(sum (case when ql.ProductTypeId not in (4) then qld.NetCharge else 0 end) , 0), 0)  < 0
				then 0
				else isnull(  (sum (case when ql.ProductTypeId not in (4) then qld.NetCharge else 0 end) - sum (case when ql.ProductTypeId not in (4) then qld.BaseCost * qld.Quantity else 0 end) ) / nullif(sum (case when ql.ProductTypeId not in (4) then qld.NetCharge else 0 end) , 0), 0) end * 100.0 GPPercenatge
	,	sum (case when ql.ProductTypeId not in (3,4) then qld.NetCharge else 0 end) TotalSalesProd
	,	sum (case when ql.ProductTypeId not in (3,4) then qld.NetCharge else 0 end) - sum (case when ql.ProductTypeId not in (3,4) then qld.BaseCost * qld.Quantity else 0 end) GPValueProd
	,	case when isnull(  (sum (case when ql.ProductTypeId not in (3,4) then qld.NetCharge else 0 end) - sum (case when ql.ProductTypeId not in (3,4) then qld.BaseCost * qld.Quantity else 0 end) ) / nullif(sum (case when ql.ProductTypeId not in (3,4) then qld.NetCharge else 0 end) , 0), 0)  < 0
				then 0
				else isnull(  (sum (case when ql.ProductTypeId not in (3,4) then qld.NetCharge else 0 end) - sum (case when ql.ProductTypeId not in (3,4) then qld.BaseCost * qld.Quantity else 0 end) ) / nullif(sum (case when ql.ProductTypeId not in (3,4) then qld.NetCharge else 0 end) , 0), 0) end * 100.0 GPPercenatgeProd
	,	o.Tax
	--,	sum(case when qld.Netcharge>0 then Cast(isnull((qld.Netcharge-(qld.BaseCost * qld.Quantity)),0.00) as decimal(18,2)) else 0.0 end) as GPValue
    --,	case when sum(ql.ExtendedPrice) >0
	--		then
	--			(sum(case when qld.Netcharge>0 then Cast(isnull((qld.Netcharge-(qld.BaseCost * qld.Quantity)),0.00) as decimal(18,2)) else 0.0 end) / sum(ql.ExtendedPrice)) *100
	--		else 0.0 end as GPPercenatge
    FROM CRM.Orders o
	  Join CRM.Quote q on o.QuoteKey=q.QuoteKey and q.PrimaryQuote=1
	  JOIN CRM.QuoteLines ql ON ql.QuoteKey = q.QuoteKey AND ql.ProductTypeId not in (4)
	  JOIN CRM.QuoteLineDetail qld on ql.QuoteLineId=qld.QuoteLineId
	  JOIN CRM.Type_OrderStatus os on o.OrderStatus = os.OrderStatusId
	  Left  Join (
			select mpox.OrderId, mpo.MasterPONumber, mpo.PurchaseOrderID, mpo.POStatusId
			from crm.MasterPurchaseOrderExtn mpox
			JOIN CRM.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = mpox.PurchaseOrderId
			where mpo.PicPO != 0 ) po on po.OrderId = o.OrderID --o.OrderId = mpo.OrderID
	  left join CRM.Type_POStatus ps on ps.POStatusId = po.POStatusId
	  --left join CRM.PurchaseOrderDetails pod ON pod.PurchaseOrderId = mpo.PurchaseOrderId and pod.QuoteLineId = ql.QuoteLineId
	  join CRM.Opportunities opp ON o.OpportunityId = opp.OpportunityId
	  join CRM.Accounts a ON opp.AccountId = a.AccountId
	  join CRM.AccountCustomers acc on a.AccountId =acc.AccountId and acc.IsPrimaryCustomer=1
	  join CRM.Customer p ON p.CustomerId = acc.CustomerId 
	  join CRM.Person sa ON sa.PersonId = opp.SalesAgentId 
	  --LEFT JOIN CRM.ShipNotice sn ON sn.PurchaseOrdersDetailId = pod.PurchaseOrdersDetailId
	  left join CRM.Territories tr on tr.TerritoryId=opp.TerritoryId
	  --LEFT JOIN CRM.HFCProducts Hprd ON Hprd.ProductID=ql.ProductId
	  --LEFT JOIN CRM.Type_ProductCategory pc	ON ql.ProductCategoryId = pc.ProductCategoryId
	  --LEFT JOIN CRM.Type_ProductCategory psc	ON ql.ProductSubCategoryId = psc.ProductCategoryId
	  --LEFT JOIN CRM.FranchiseProducts fp ON fp.ProductKey = ql.ProductID and fp.FranchiseId = a.FranchiseId
	  --LEFT JOIN CRM.Type_ProductCategory fpc ON	 fp.ProductCategory = fpc.ProductCategoryId
	  left join (select * from CRM.OpportunitySources where IsPrimarySource=1) opps on opp.opportunityID = opps.OpportunityId
	  left join crm.SourcesTP stp on stp.SourcesTPId = opps.SourceID
	  left join crm.Sources s on s.SourceID = stp.SourceId
	  left join crm.Campaign c on c.CampaignID = isnull(a.CampaignId, 0)
	where o.OrderStatus not in (6,9)
	and cast(o.ContractedDate as Date) between @StartDate and @EndDate
	and (@PersonID is null or opp.SalesAgentId in (select id from [CRM].[CSVToTable] (@PersonId)))
	and	opp.FranchiseId = isnull(nullif(@FranchiseID, 0), opp.FranchiseId) 
	and isnull(a.CommercialTypeId, 0) = coalesce(@CommercialTypeID, a.CommercialTypeId, 0)
	and isnull(a.IndustryId, 0) = coalesce(@IndustryID, a.IndustryID, 0)
	and case
			when a.IsCommercial=1 
				then case when p.CompanyName is not null and p.CompanyName!='' then p.CompanyName+' / ' else '' end + p.FirstName + ' ' + p.LastName 
				else p.FirstName + ' ' + p.LastName + case when p.CompanyName is not null and p.CompanyName!='' then ' / '+p.CompanyName else '' end
					end like '%' + isnull(@CustomerName, '') + '%'
group by
		isnull(p.FirstName, '')
	,	isnull(p.LastName, '')
	,	isnull(p.CompanyName, '')
	,	dbo.ProperCase(sa.FirstName + ' ' + sa.LastName)
	,	s.Name
	,	isnull(c.Name, '')
	,	o.OrderNumber
	,	po.MasterPONumber
	,	dbo.ProperCase(ps.POStatusName)
	,	cast(o.ContractedDate as Date)
	,	os.OrderStatus
	,	o.Tax

end
