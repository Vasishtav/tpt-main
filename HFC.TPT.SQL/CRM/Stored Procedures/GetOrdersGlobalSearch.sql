﻿CREATE PROC [CRM].[GetOrdersGlobalSearch]
( @FranchiseId int
	,@SearchType varchar(50)
	, @Value varchar(500))

AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
set @Value = '%'+ @Value + '%';

IF (@SearchType = 'name')
	BEGIN
		select o.OrderId,o.OrderNumber, q.QuoteId, a.AccountId, q.OpportunityId
        , o.OrderName, o.ContractedDate
        , o.QuoteKey, Concat(c.FirstName, ' ', c.LastName) Account
        ,  opp.OpportunityName Opportunity
        , Case when opp.TerritoryId is not null then ter.Name
	        else opp.TerritoryType
	        End Territory
        , q.Sidemark
        , os.OrderStatus Status
        , o.OrderTotal TotalAmount
        , opp.SalesAgentId, opp.InstallerId
        from crm.Orders o
        left join crm.Quote q on q.QuoteKey = o.QuoteKey
        left join crm.Opportunities opp on opp.OpportunityId = q.OpportunityId
        left join crm.Accounts a on a.AccountId = opp.AccountId
        left join crm.AccountCustomers ac on ac.AccountId = a.AccountId and ac.IsPrimaryCustomer = 1
        left join crm.Customer c on c.CustomerId = ac.CustomerId
        left join crm.Territories ter on ter.TerritoryId = opp.TerritoryId
        left join crm.Type_OrderStatus os on os.OrderStatusId = o.OrderStatus
        where opp.FranchiseId = @FranchiseId 
			and o.OrderStatus not in (6, 9)

			and (o.OrderName like @Value 
				or q.sidemark like @Value 
				or concat(c.firstname, ' ', c.lastname) like @Value) 
		Order by o.OrderNumber desc
	END
ELSE IF (@SearchType = 'id')
	BEGIN
		select o.OrderId,o.OrderNumber, q.QuoteId, a.AccountId, q.OpportunityId
        , o.OrderName, o.ContractedDate
        , o.QuoteKey, Concat(c.FirstName, ' ', c.LastName) Account
        ,  opp.OpportunityName Opportunity
        , Case when opp.TerritoryId is not null then ter.Name
	        else opp.TerritoryType
	        End Territory
        , q.Sidemark
        , os.OrderStatus Status
        , o.OrderTotal TotalAmount
        , opp.SalesAgentId, opp.InstallerId
        from crm.Orders o
        left join crm.Quote q on q.QuoteKey = o.QuoteKey
        left join crm.Opportunities opp on opp.OpportunityId = q.OpportunityId
        left join crm.Accounts a on a.AccountId = opp.AccountId
        left join crm.AccountCustomers ac on ac.AccountId = a.AccountId and ac.IsPrimaryCustomer = 1
        left join crm.Customer c on c.CustomerId = ac.CustomerId
        left join crm.Territories ter on ter.TerritoryId = opp.TerritoryId
        left join crm.Type_OrderStatus os on os.OrderStatusId = o.OrderStatus
        where opp.FranchiseId = @FranchiseId 
			and o.OrderStatus not in (6, 9)

			and (o.OrderNumber like @Value 
				or q.QuoteID like @Value) 
		Order by o.OrderNumber desc
	END
ELSE IF (@SearchType = 'all')
	BEGIN
		select o.OrderId,o.OrderNumber, q.QuoteId, a.AccountId, q.OpportunityId
        , o.OrderName, o.ContractedDate
        , o.QuoteKey, Concat(c.FirstName, ' ', c.LastName) Account
        ,  opp.OpportunityName Opportunity
        , Case when opp.TerritoryId is not null then ter.Name
	        else opp.TerritoryType
	        End Territory
        , q.Sidemark
        , os.OrderStatus Status
        , o.OrderTotal TotalAmount
        , opp.SalesAgentId, opp.InstallerId
        from crm.Orders o
        left join crm.Quote q on q.QuoteKey = o.QuoteKey
        left join crm.Opportunities opp on opp.OpportunityId = q.OpportunityId
        left join crm.Accounts a on a.AccountId = opp.AccountId
        left join crm.AccountCustomers ac on ac.AccountId = a.AccountId and ac.IsPrimaryCustomer = 1
        left join crm.Customer c on c.CustomerId = ac.CustomerId
        left join crm.Territories ter on ter.TerritoryId = opp.TerritoryId
        left join crm.Type_OrderStatus os on os.OrderStatusId = o.OrderStatus
        where opp.FranchiseId = @FranchiseId 
			and o.OrderStatus not in (6, 9)

			and (o.OrderName like @value 
			    or q.sidemark like @value 
			    or o.OrderNumber like @value 
                or q.QuoteID like @value
				or concat(c.firstname, ' ', c.lastname) like @value)
		Order by o.OrderNumber desc 
	END
ELSE 
	BEGIN
		declare @messsage varchar(500) = 'Invaid search type :' + @SearchType;
		THROW 60000, @messsage, 1;
	END;
	
	