﻿
CREATE PROCEDURE [CRM].[GetOrderDetails]
(@FranchiseId INT,
 @orderId     INT
)
AS
     SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    BEGIN
        SELECT o.OrderNumber,
               q.SideMark,
               q.OrderInvoiceLevelNotes,
               q.InstallerInvoiceNotes,
               op.InstallationAddressId,
               op.BillingAddressId,
               o.OrderID,
               o.OpportunityId,
               o.QuoteKey,
               OrderName,
               o.OrderStatus,
               o.CancelReason,
               ContractedDate,
               op.AccountId,
               CASE
                   WHEN o.TaxCalculated IS NULL
                   THEN 0
                   ELSE 1
               END AS TaxCalculated,
               o.ProductSubtotal,
               o.AdditionalCharges,
               OrderDiscount,
               OrderSubtotal,
               Tax,
               OrderTotal,
               BalanceDue,
               p.FirstName + ' ' + p.LastName AS SalesAgent,
               op.OpportunityName AS Opportunity,
        (
            SELECT [CRM].[fnGetAccountNameByAccountId](ac.AccountId)
        ) AS AccountName,
               CASE
                   WHEN EXISTS
        (
            SELECT 1
            FROM crm.MasterPurchaseOrderExtn mpox
            WHERE mpox.OrderId = o.OrderID
        )
                   THEN 1
                   ELSE 0
               END AS MpoExists,
               CASE
                   WHEN EXISTS
        (
            SELECT 1
            FROM crm.MasterPurchaseOrderExtn mpox
                 LEFT JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = mpox.PurchaseOrderId
            -- 8: Cancelled..cancelled mpos order only can be cancelled or void otherwise not.
            WHERE mpox.OrderId = o.OrderID
                  AND mpo.POStatusId <> 8
        )
                   THEN 0
                   ELSE 1
               END AS CanVoidCancelOrder,
               CASE
                   WHEN EXISTS
        (
            SELECT 1
            FROM crm.orderlines ol
                 INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = ol.QuoteLineId
                                                 AND ql.ProductTypeId IN(1, 2, 5)
            WHERE OrderID = @orderId
                  AND (OrderLineStatus = 7
                       OR OrderLineStatus = 8)
        )
                   THEN 1
                   ELSE 0
               END CanConvertToMpo,
               CASE
                   WHEN op.AccountId IS NOT NULL
                   THEN
        (
            SELECT CASE
                       WHEN PreferredTFN = 'C'
                       THEN CellPhone
                       ELSE CASE
                                WHEN PreferredTFN = 'H'
                                THEN HomePhone
                                ELSE CASE
                                         WHEN PreferredTFN = 'W'
                                         THEN WorkPhone
                                         ELSE CASE
                                                  WHEN CellPhone IS NOT NULL
                                                  THEN CellPhone
                                                  ELSE CASE
                                                           WHEN HomePhone IS NOT NULL
                                                           THEN HomePhone
                                                           ELSE CASE
                                                                    WHEN WorkPhone IS NOT NULL
                                                                    THEN WorkPhone
                                                                    ELSE ''
                                                                END
                                                       END
                                              END
                                     END
                            END
                   END
            FROM CRM.Customer cus
                 JOIN CRM.AccountCustomers acus ON cus.CustomerId = acus.CustomerId
                                                   AND IsPrimaryCustomer = 1
            WHERE acus.AccountId = op.AccountId
        )
               END AS AccountPhone,
               cu.PrimaryEmail,
               cu.PrimaryEmail,
               os.OrderStatus AS OrderStatusText,
               o.CreatedOn AS CreatedOn,
               o.LastUpdatedOn AS LastUpdate,
               ISNULL(
        (
            SELECT CASE
                       WHEN(ISNULL(ts.UseCustomerAddress, 0) = 0
                            AND ISNULL(ts.ShipToLocationId, 0) = 0
                            AND ts.TaxType = 2)
                           OR f.EnableTax = 0
                           OR (ts.TaxType IN(1))
                       THEN 1
                       ELSE 0
                   END
        ), 0) AS IsTaxDisabled,
               CASE
                   WHEN ts.TaxType = 3
                   THEN 1
                   ELSE 0
               END AS UseTax,
               q.IsTaxExempt,
               q.TaxExemptID,
               q.IsVATExempt,
               q.IsPSTExempt,
               q.IsGSTExempt,
               q.IsVATExempt,
               q.IsHSTExempt,
               q.IsNewConstruction,
               pe.FirstName + ' ' + pe.LastName AS ContractedDateUpdatedBy_Name,
               o.ContractedDateUpdatedOn,
               o.ContractedDateUpdatedBy,
               o.PreviousContractedDate,
               op.CreatedOnUtc AS OpportunityCreatedDate,
               op.ReceivedDate AS OpportunityReceivedDate,
               ac.IsNotifyemails,
               ac.IsNotifyText,
               mpo.MasterPONumber,
               mpo.PurchaseOrderId
        FROM [CRM].[Orders] o
             JOIN [CRM].[Opportunities] op ON o.OpportunityId = op.OpportunityId
             JOIN [CRM].[Person] p ON op.SalesAgentId = p.PersonId
             LEFT JOIN [CRM].[Person] pe ON o.ContractedDateUpdatedBy = pe.PersonId
             JOIN [CRM].[Accounts] ac ON op.AccountId = ac.AccountId
             JOIN CRM.AccountCustomers accus ON ac.AccountId = accus.AccountId
                                                AND accus.IsPrimaryCustomer = 1
             JOIN [CRM].[Customer] cu ON accus.CustomerId = cu.CustomerId
             LEFT JOIN [CRM].[Type_OrderStatus] os ON o.OrderStatus = os.OrderStatusId
             JOIN [CRM].[Quote] q ON o.QuoteKey = q.QuoteKey
             LEFT JOIN crm.MasterPurchaseOrderExtn mpox ON mpox.OrderId = o.OrderID
             LEFT JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = mpox.PurchaseOrderId
             INNER JOIN crm.TaxSettings ts ON ts.FranchiseId = ac.FranchiseId
                                              AND ISNULL(ts.TerritoryId, 0) = ISNULL(op.TerritoryId, 0)
             INNER JOIN crm.Franchise f ON f.FranchiseId = ac.FranchiseId
        WHERE o.OrderID = @orderId
              AND op.FranchiseId = @FranchiseId;
    END;