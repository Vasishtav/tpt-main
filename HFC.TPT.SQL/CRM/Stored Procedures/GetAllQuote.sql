﻿CREATE PROCEDURE [CRM].[GetAllQuote]  
@FranchiseId int,
@datefilter varchar(200)='',
@QuoteStatusId varchar(100)='',
@PrimaryQuote bit=0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @CreatedOn datetime,@strPrimaryQuote varchar(100)

	if(@datefilter = '4005')  -- Last 7 days
		select @CreatedOn = Cast(GETDATE()-7 as Date)

	else if(@datefilter='4006')  -- Last 14 days
		select @CreatedOn=Cast(GETDATE()-14 as Date)

	else if(@datefilter='4007')  -- Last 30 days
		select @CreatedOn=Cast(GETDATE()-30 as Date)

	else if(@datefilter='4008')  -- Last 90 days
		select @CreatedOn=Cast(GETDATE()-90 as Date)

	else if(@datefilter='4009')  -- Last 180 days
		select @CreatedOn=Cast(GETDATE()-180 as Date)
	else
		select @CreatedOn= cast(-53680 as datetime)

	if(@QuoteStatusId='')
	set @QuoteStatusId=(select stuff((select ','+ Cast(QuoteStatusId as varchar(100)) from CRM.Type_QuoteStatus 
  where QuoteStatusId not in (5) for xml path('')), 1, 1, '' ) )

	if(@PrimaryQuote=1)
		set @strPrimaryQuote='1'
	else
		set @strPrimaryQuote='0,1'

  select
		QuoteID
		,q.QuoteKey
		,[QuoteName]
		,PrimaryQuote
		,q.CreatedOn
		,Case when t.QuoteStatusId=4 then null when t.QuoteStatusId=5 then null else q.LastUpdatedOn end as LastUpdatedOn
		,o.AccountId
		,(select [CRM].[fnGetAccountNameByAccountId](o.AccountId)) as AccountName
		,q.OpportunityId
		,o.OpportunityName
		,case when o.TerritoryId is not null then tr.Name else o.TerritoryType end as Territory
		,q.SideMark
		,p.FirstName+' '+p.LastName as SalesAgent
		,sum(CASE WHEN ql.producttypeid in (1,2,3,5) THEN 1 ELSE 0 END) NumberOfQuotelines
		,SUM(CASE WHEN ql.producttypeid in (1,2,5) THEN ql.Quantity ELSE 0 END) Quantity
		,q.Quotesubtotal + Isnull(SUM(tax.Amount),0) Quotesubtotal
		,t.QuoteStatus
		,o.SalesAgentId
		,o.InstallerId
		 from [CRM].[Quote] q
		 join [CRM].[Opportunities] o on o.OpportunityId=q.OpportunityId
		 left join CRM.QuoteLines ql on q.QuoteKey=ql.QuoteKey
		 left join CRM.Tax tax on tax.QuoteLineId=ql.QuoteLineId
		 join [CRM].Accounts acc on o.AccountId = acc.AccountId
		 join [CRM].Customer cus on acc.PersonId = cus.CustomerId
		 left join [CRM].[Type_QuoteStatus] t on q.QuoteStatusId=t.QuoteStatusId
		 left join [CRM].[Person] p on o.SalesAgentId=p.PersonId
		 left join [CRM].[Person] ins on o.InstallerId=ins.PersonId
		 LEFT JOIN CRM.Territories tr on tr.TerritoryId=o.TerritoryId
		 where o.FranchiseId =@FranchiseId 
		 and cast(q.CreatedOn as date) > @CreatedOn 
		 and q.QuoteStatusId IN (select id from CRM.CSVToTable(@QuoteStatusId))
		 and q.PrimaryQuote in (select id from CRM.CSVToTable(@strPrimaryQuote))
		 group by QuoteID
		,q.QuoteKey
		,[QuoteName]
		,PrimaryQuote
		,q.CreatedOn
		,o.AccountId
		,q.OpportunityId
		,o.OpportunityName
		,o.TerritoryId,tr.Name,o.TerritoryType
		,q.SideMark
		,p.FirstName,p.LastName
		,q.Quotesubtotal
		,NetCharges
		,t.QuoteStatus
		,o.SalesAgentId
		,o.InstallerId
		,q.LastUpdatedOn
		,t.QuoteStatusId
		order by q.CreatedOn desc
END
GO

