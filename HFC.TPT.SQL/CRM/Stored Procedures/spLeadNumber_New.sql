﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [CRM].[spLeadNumber_New]  
	@FranchiseId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF EXISTS(SELECT * FROM CRM.LeadNJobNumbers WHERE FranchiseId = @FranchiseId)
	BEGIN
		UPDATE CRM.LeadNJobNumbers SET LeadNumber = LeadNumber + 1
		OUTPUT INSERTED.LeadNumber 
		WHERE FranchiseId = @FranchiseId
	END
	ELSE
	BEGIN
		INSERT INTO CRM.LeadNJobNumbers (FranchiseId) 
		OUTPUT INSERTED.LeadNumber
		VALUES(@FranchiseId)
	END
END

