create procedure CRM.Report_IncomeSummary (
	@FranchiseID int, @Year int)
as
begin;

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
set nocount on



select
	 case
		when ProductName = 'Installation' then 'Installation'
		when ProductName = 'Shipping' then 'Shipping'
		else pt.ProductType end +
				case when (isnull(q.IsTaxExempt, 0) = 0 and ql.TaxExempt = 0 ) then ' (taxable)' else ' (non-taxable)' end IncomeType
	,sum(case when datepart(month, o.ContractedDate) =  1 then qld.NetCharge else 0 end) [January]
	,sum(case when datepart(month, o.ContractedDate) =  2 then qld.NetCharge else 0 end) [February]
	,sum(case when datepart(month, o.ContractedDate) =  3 then qld.NetCharge else 0 end) [March]
	,sum(case when datepart(month, o.ContractedDate) =  4 then qld.NetCharge else 0 end) [April]
	,sum(case when datepart(month, o.ContractedDate) =  5 then qld.NetCharge else 0 end) [May]
	,sum(case when datepart(month, o.ContractedDate) =  6 then qld.NetCharge else 0 end) [June]
	,sum(case when datepart(month, o.ContractedDate) =  7 then qld.NetCharge else 0 end) [July]
	,sum(case when datepart(month, o.ContractedDate) =  8 then qld.NetCharge else 0 end) [August]
	,sum(case when datepart(month, o.ContractedDate) =  9 then qld.NetCharge else 0 end) [September]
	,sum(case when datepart(month, o.ContractedDate) =  10 then qld.NetCharge else 0 end) [October]
	,sum(case when datepart(month, o.ContractedDate) =  11 then qld.NetCharge else 0 end) [November]
	,sum(case when datepart(month, o.ContractedDate) =  12 then qld.NetCharge else 0 end) [December]
	,sum(qld.NetCharge) [Total]
	from CRM.Opportunities opp
	join CRM.Quote q on opp.opportunityid = q.OpportunityId and q.PrimaryQuote=1
	join CRM.QuoteLines ql ON ql.QuoteKey = q.QuoteKey AND ql.ProductTypeId not in (4)
	join CRM.QuoteLineDetail qld on ql.QuoteLineId=qld.QuoteLineId
	join CRM.Orders o on q.QuoteKey=o.QuoteKey
	join CRM.Type_ProductType pt on ql.ProductTypeId = pt.ID
	where opp.FranchiseId = @FranchiseID
	and year(o.ContractedDate) = @Year 
	and isnull(o.OrderStatus, 0) not in (6,9)
	group by
	 case
		when ProductName = 'Installation' then 'Installation'
		when ProductName = 'Shipping' then 'Shipping'
		else pt.ProductType end +
				case when (isnull(q.IsTaxExempt, 0) = 0 and ql.TaxExempt = 0 ) then ' (taxable)' else ' (non-taxable)' end
	order by
	 case
		when ProductName = 'Installation' then 'Installation'
		when ProductName = 'Shipping' then 'Shipping'
		else pt.ProductType end +
				case when (isnull(q.IsTaxExempt, 0) = 0 and ql.TaxExempt = 0 ) then ' (taxable)' else ' (non-taxable)' end


end

