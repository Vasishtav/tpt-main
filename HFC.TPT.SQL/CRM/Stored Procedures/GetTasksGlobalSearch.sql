﻿CREATE PROC [CRM].[GetTasksGlobalSearch]
( @FranchiseId int
	,@SearchType varchar(50)
	, @Value varchar(500))

AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
set @Value = '%'+ @Value + '%';

IF (@SearchType = 'name' OR @SearchType = 'all')
	BEGIN
		select t.TaskId, t.Subject
        , t.LeadId, t.AccountId, t.OpportunityId, t.OrderId
        , (SELECT Stuff((SELECT ', ' + PersonName FROM CRM.EventToPeople
                                    where TaskId=t.TaskId FOR XML PATH('')), 1, 2, '')) AS AttendeesName
        , t.DueDate 
        , (case  when isnull(t.OrderId, 0) <> 0 then   concat('Ord-', o.OrderName)
	        when isnull(t.OpportunityId, 0) <> 0 then   concat('Opp-', opp.OpportunityName)
	        when isnull(t.AccountId, 0) <> 0 then   Concat('Acct-', ac.firstname, ' ', ac.LastName)
	        when isnull(t.LeadId, 0) <> 0  then  Concat('Lead-', lc.firstname, ' ', lc.LastName)
	        else '' end )as Related 
        , t.Message Note, t.CompletedDate as completed
        from crm.Tasks t
        Left join crm.Orders o on o.OrderID = t.OrderId
        Left join crm.Opportunities opp on opp.OpportunityId = t.OpportunityId
        Left join crm.AccountCustomers acj on acj.AccountId = t.AccountId and acj.IsPrimaryCustomer = 1
        Left join crm.Customer ac on ac.CustomerId = acj.CustomerId
        Left join crm.LeadCustomers lcj on lcj.LeadId = t.LeadId and lcj.IsPrimaryCustomer = 1
        Left join crm.Customer lc on lc.CustomerId = lcj.CustomerId
        where t.FranchiseId = @FranchiseId
			and ( t.Subject like @Value
				or lc.FirstName like @Value 
				or lc.LastName like @Value 
				or CONCAT(lc.firstname, ' ', lc.LastName) like @Value
				or ac.FirstName like @Value 
				or ac.LastName like @Value 
				or CONCAT(ac.firstname, ' ', ac.LastName) like @Value
				or opp.OpportunityName like @Value 
				or t.Message like @Value 
				or t.TaskId in  (select TaskId from crm.EventToPeople 
					where PersonName like @Value)
				)
			and	isnull(t.IsDeleted, 0) = 0 
        Order by t.DueDate Desc
	END
ELSE 
	BEGIN
		declare @messsage varchar(500) = 'Invaid search type :' + @SearchType;
		THROW 60000, @messsage, 1;
	END;
	
	