-- =================================================================
-- Author:		Vasishta Veeramasuneni
-- Create date: 10/07/2019
-- Updated: 08/18/2019 BY Dan
-- Description:	Validate Vendors and Products for quotelines
-- =================================================================

Create PROCEDURE [CRM].[SPValidatetQuoteLines] @quoteId     INT,
                                               @franchiseId INT
AS
    BEGIN
        SELECT distinct ql.quotelinenumber,ql.QuoteLineId,
               ISNULL(CASE
                          WHEN QL.ProductTypeId IN(2, 3)
                          THEN fprd.ProductName
                          WHEN QL.ProductTypeID = 4
                          THEN
        (
            SELECT fd.name
            FROM crm.FranchiseDiscount fd
            WHERE fd.DiscountId = QL.[ProductId]
        )
                          ELSE QL.ProductName
                      END, '') AS ProductName,
               ISNULL(CASE
                          WHEN QL.ProductTypeId = 1
                          THEN hven.Name
                          ELSE QL.VendorName
                      END, '') AS VendorName,
               CASE
                   WHEN QL.ProductTypeId = 1
                   THEN CASE
                            WHEN hven.IsActive <> 1
                                 OR fven.VendorStatus <> 1
                                 OR hprd.Active <> 1
                                 OR fprd.ProductStatus <> 1
                                 OR hmod.Active <> 1
                                 OR (hmod.USActive <> 1
                                     AND f.Currency = 'US')
                                 OR (hmod.CAActive <> 1
                                     AND f.Currency = 'CA')
                            THEN CASE
                                     WHEN hven.IsActive <> 1
                                          OR fven.VendorStatus <> 1
                                     THEN QL.VendorName + ' is not Active. '
                                     ELSE ''
                                 END + CASE
                                           WHEN hprd.Active <> 1
                                                OR ISNULL(fprd.ProductStatus, 1) <> 1
                                           THEN+' ' + QL.ProductName + ' is not Active.'
                                           ELSE ''
                                       END
                            ELSE ''
                        END + CASE
                                  WHEN hmod.Active <> 1
                                       OR (hmod.USActive <> 1
                                           AND f.Currency = 'US')
                                       OR (hmod.CAActive <> 1
                                           AND f.Currency = 'CA')
                                  THEN+' ' + QL.ModelDescription + ' is not Active.'
                                  ELSE ''
                              END
                   WHEN QL.ProductTypeId IN(2, 3)
                   THEN CASE
                            WHEN fven.VendorStatus <> 1
                                 OR fprd.ProductStatus <> 1
                            THEN CASE
                                     WHEN fven.VendorStatus <> 1
                                     THEN QL.VendorName + ' is not Active. '
                                     ELSE ''
                                 END + CASE
                                           WHEN fprd.ProductStatus <> 1
                                           THEN+' ' + fprd.ProductName + ' is not Active.'
                                           ELSE ''
                                       END
                            ELSE ''
                        END
                   ELSE ''
               END AS vend,
               qld.ValidConfiguration,
               qld.LastValidatedon
        FROM [CRM].[QuoteLines] AS QL
             INNER JOIN crm.QuoteLineDetail qld ON qld.QuoteLineId = QL.QuoteLineId
             INNER JOIN CRM.Quote AS Q ON Q.QuoteKey = QL.[QuoteKey]
             INNER JOIN crm.Opportunities o ON o.OpportunityId = Q.OpportunityId
             INNER JOIN crm.Franchise f ON f.FranchiseId = o.FranchiseId
             LEFT JOIN [CRM].[HFCProducts] AS hprd ON hprd.PICProductID = JSON_VALUE(ql.PICJson, '$.PicProduct')
                                                      AND QL.ProductTypeId = 1 AND hprd.Active=1
             LEFT JOIN CRM.HFCModels hmod ON hmod.model = JSON_VALUE(ql.PICJson, '$.ModelId') AND hmod.Active = 1 AND hmod.PICProductId = JSON_VALUE(ql.PICJson, '$.PicProduct')
             LEFT JOIN crm.PurchaseOrderDetails pod ON pod.QuoteLineId = QL.QuoteLineId
                                                       AND QL.ProductTypeId = 1
             LEFT JOIN Acct.FranchiseVendors AS fven ON fven.VendorId = ql.VendorId
                                                        AND fven.FranchiseId = o.FranchiseId
                                                        AND QL.ProductTypeId IN(1, 2, 3)
             LEFT JOIN ACCT.HFCVendors AS hven ON(hven.VendorId = ql.VendorID
                                                  AND QL.ProductTypeId = 1)
                                                 OR (hven.VendorId = fven.VendorId
                                                     AND QL.ProductTypeId IN(2, 3))
             LEFT JOIN [CRM].[FranchiseProducts] AS fprd ON fprd.ProductKey = ql.ProductId
                                                            AND fprd.FranchiseId = o.FranchiseId
                                                            AND QL.ProductTypeId IN(2, 3)
             LEFT JOIN CRM.Type_ProductCategory tpc ON(tpc.ProductCategoryId = fprd.ProductCategory
                                                       AND QL.ProductTypeId IN(2, 3))
                                                      OR (tpc.ProductCategoryId = QL.ProductSubCategoryId
                                                          AND QL.ProductTypeId NOT IN(1, 2, 3, 4))
        WHERE ql.IsActive = 1
              AND o.FranchiseId = @franchiseId
              AND QL.[QuoteKey] = @quoteId
        ORDER BY ql.QuoteLineNumber;
    END;
GO


