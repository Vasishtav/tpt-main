﻿
-- =============================================
-- Author:		Vasishta Veeramasuneni
-- Create date: 12/15/2017
-- Description:	Get the next number for Account, order, Invoice etc..,
-- =============================================

CREATE PROCEDURE [CRM].[spGetNextNumber] 
				 @FranchiseId int, @numberType int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF EXISTS
	(
		SELECT *
		FROM [CRM].[TPTNumbers]
		WHERE FranchiseId = @FranchiseId
	)
	BEGIN

		IF(@numberType = 1)
		BEGIN
			UPDATE [CRM].[TPTNumbers]
			  SET AccountNumber = AccountNumber + 1
			OUTPUT INSERTED.AccountNumber AS nextNumber
			WHERE FranchiseId = @FranchiseId;
		END;
		IF(@numberType = 2)
		BEGIN
			UPDATE [CRM].[TPTNumbers]
			  SET [QuoteNumber] = [QuoteNumber] + 1
			OUTPUT INSERTED.[QuoteNumber] AS nextNumber
			WHERE FranchiseId = @FranchiseId;
		END;
		IF(@numberType = 3)
		BEGIN
			UPDATE [CRM].[TPTNumbers]
			  SET [OrderNumber] = [OrderNumber] + 1
			OUTPUT INSERTED.[OrderNumber] AS nextNumber
			WHERE FranchiseId = @FranchiseId;
		END;
		IF(@numberType = 4)
		BEGIN
			UPDATE [CRM].[TPTNumbers]
			  SET [InvoiceNumber] = [InvoiceNumber] + 1
			OUTPUT INSERTED.[InvoiceNumber] AS nextNumber
			WHERE FranchiseId = @FranchiseId;
		END;
		IF(@numberType = 5)
		BEGIN
			UPDATE [CRM].[TPTNumbers]
			  SET [PONumber] = [PONumber] + 1
			OUTPUT INSERTED.[PONumber] AS nextNumber
			WHERE FranchiseId = @FranchiseId;
		END;

	END;
	ELSE
	BEGIN

		IF(@numberType = 1)
		BEGIN
			INSERT INTO CRM.[TPTNumbers]( FranchiseId )
			OUTPUT INSERTED.AccountNumber AS nextNumber
			VALUES( @FranchiseId );
		END;
		IF(@numberType = 2)
		BEGIN

			INSERT INTO CRM.[TPTNumbers]( FranchiseId )
			OUTPUT INSERTED.[QuoteNumber] AS nextNumber
			VALUES( @FranchiseId );
		END;
		IF(@numberType = 3)
		BEGIN
			INSERT INTO CRM.[TPTNumbers]( FranchiseId )
			OUTPUT INSERTED.[OrderNumber] AS nextNumber
			VALUES( @FranchiseId );
		END;
		IF(@numberType = 4)
		BEGIN
			INSERT INTO CRM.[TPTNumbers]( FranchiseId )
			OUTPUT INSERTED.[InvoiceNumber] AS nextNumber
			VALUES( @FranchiseId );
		END;
		IF(@numberType = 5)
		BEGIN
			INSERT INTO CRM.[TPTNumbers]( FranchiseId )
			OUTPUT INSERTED.[PONumber] AS nextNumber
			VALUES( @FranchiseId );
		END;
	END;
END;


