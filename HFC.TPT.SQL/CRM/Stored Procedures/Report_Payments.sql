create procedure CRM.Report_Payments (
  @FranchiseId int,
  @StartDate datetime,
  @EndDate datetime )
as
begin
--20200408
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
set nocount on

select
	 isnull(nullif(cu.CompanyName, ''), cu.FirstName + ' ' + cu.LastName)  AccountName
	,cast(p.PaymentDate as Date) PaymentDate
	,pm.PaymentMethod TransactionType
	,p.Memo MemoDescription
	,o.OrderNumber OrderNo
	, p.Amount * case when p.Reversal = 1 then -1 else 1 end Amount
from crm.payments p
join crm.Orders o on p.OrderID = o.OrderID
join crm.Opportunities opp on p.OpportunityID = opp.OpportunityID
join crm.AccountCustomers acu on opp.AccountId = acu.AccountId and acu.IsPrimaryCustomer = 1
join crm.Customer cu on acu.CustomerID = cu.CustomerId
join crm.Type_PaymentMethod pm on p.PaymentMethod = pm.Id
where opp.FranchiseID = @FranchiseID
and	cast(p.PaymentDate as Date) between @StartDate and @EndDate
order by cast(p.PaymentDate as Date), o.OrderNumber

end