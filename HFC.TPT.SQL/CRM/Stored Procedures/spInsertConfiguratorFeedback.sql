﻿CREATE PROCEDURE [CRM].[spInsertConfiguratorFeedback]
@CaseId int,
@CaseNumber	nvarchar(15),
@CaseType	nvarchar(256),
@FranchiseId	int,
@VendorId	int,
@Details	nvarchar(max),
@StatusId	int,
@CaseFeedbackId int,
@RecordTypeId	nvarchar(256),
@ProductGroupId	int,
@TouchpointURL	varchar(128),
@TouchpointVersion	varchar(128),
@BrowserDetails	varchar(128),
@PICJson	nvarchar(max),
@FieldErrors nvarchar(max),
@ChangeCaseId int,
@CloseReason	int,
@ClosedOn datetime,
@CreatedOn	datetime2,
@CreatedBy	int,
@LastUpdatedOn	datetime2,
@LastUpdatedBy	int

--CaseTypeId

AS
BEGIN
IF (@CaseId<>0)
BEGIN
		UPDATE [CRM].[Case] SET 
		StatusId=@StatusId,		
		LastUpdatedOn=@LastUpdatedOn,
		LastUpdatedBy=@LastUpdatedBy WHERE CaseId=@CaseId

		UPDATE [CRM].[CaseFeedbackDetails] SET 
		ChangeCaseId=@ChangeCaseId,		
		CloseReason=@CloseReason,
		ClosedOn=@ClosedOn,
		LastUpdatedOn=@LastUpdatedOn,
		LastUpdatedBy=@LastUpdatedBy WHERE CaseFeedbackId=@CaseFeedbackId

		SET @CaseNumber=(SELECT CaseNumber FROM [CRM].[Case] WHERE CaseId=@CaseId)
END
ELSE
BEGIN
 	DECLARE @CaseTypeId bigint 
	SET @CaseTypeId=(SELECT max(CaseTypeId) FROM [CRM].[Case] WHERE VendorId=@VendorId and CaseType=@CaseType)
	SET @CaseTypeId=ISNULL(@CaseTypeId,0)+1;	
	SET @CaseNumber=@CaseNumber+'-'+CAST(@CaseTypeId AS VARCHAR(10));

	INSERT INTO [CRM].[Case] (CaseNumber,
		CaseTypeId,
		CaseType,
		FranchiseId,
		VendorId,
		Details,
		StatusId,
		CreatedOn,
		CreatedBy,
		LastUpdatedOn,
		LastUpdatedBy) VALUES 
		(@CaseNumber,
		@CaseTypeId,
		@CaseType,
		@FranchiseId,		
		@VendorId,		
		@Details,
		@StatusId,				 
		@CreatedOn,
		@CreatedBy,
		@LastUpdatedOn,
		@LastUpdatedBy)

		SET @CaseId =(SELECT IDENT_CURRENT('CRM.Case'))

		INSERT INTO [CRM].[CaseFeedbackDetails] (
			CaseId,
			RecordTypeId,
			ProductGroupId,
			TouchpointURL,
			TouchpointVersion,
			BrowserDetails,
			PICJson,
			FieldErrors,
			ChangeCaseId,
			CloseReason,
			ClosedOn,
			CreatedOn,
			CreatedBy,
			LastUpdatedOn,
			LastUpdatedBy)
			VALUES (@CaseId,@RecordTypeId,@ProductGroupId,
			@TouchpointURL,
			@TouchpointVersion,
			@BrowserDetails,
			@PICJson,@FieldErrors,@ChangeCaseId,@CloseReason,@ClosedOn,
			@CreatedOn,
			@CreatedBy,
			@LastUpdatedOn,
			@LastUpdatedBy)
END
SELECT @CaseId as CaseId,@CaseNumber as CaseNumber
END
--RETURN 0
