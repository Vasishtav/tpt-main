Create procedure CRM.Report_PPCMatchBack (
  @FranchiseId int,
  @SourceList varchar(max)=null,
  @PersonId varchar(max)=null,
  @IndustryId int=null,
  @CommercialTypeId int=null,
  @CustomerName varchar(200)=null,
  @StartDate datetime,
  @EndDate datetime )
as
begin

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
set nocount on

select 
		isnull(tr.Name, 'Gray Area') Territory
	,	[CRM].[fnGetAccountNameByAccountId](opp.AccountId) CustomerName
	,	ad.ZipCode
	,	p.PrimaryEmail EmailAddress
	,	s.Name LeadSource
	,	cast(o.ContractedDate as Date) OrderDate
	,	sum (qld.NetCharge) TotalSales
	,	case p.PreferredTFN
			when 'c' then p.CellPhone
			when 'h' then p.HomePhone
			when 'w' then p.WorkPhone
			else coalesce (p.WorkPhone, p.HomePhone, p.CellPhone)
		end Phone
from CRM.Opportunities opp
	join (select * from CRM.LeadSources where IsPrimarySource=1) ls on ls.LeadId=opp.LeadId
	join CRM.SourcesTP stp on ls.SourceId=stp.SourcesTPId
	join CRM.Sources s on stp.SourceId=s.SourceId
	join CRM.Accounts a ON opp.AccountId = a.AccountId
	join CRM.AccountCustomers acc on a.AccountId =acc.AccountId and acc.IsPrimaryCustomer=1
	join CRM.Customer p ON p.CustomerId = acc.CustomerId 
	join CRM.AccountAddresses aa on a.AccountId = aa.AccountId and aa.IsPrimaryAddress = 1
	join CRM.Addresses ad on aa.AddressId = ad.AddressId
	join CRM.Person sa ON sa.PersonId = opp.SalesAgentId 
	left join CRM.Quote q on opp.opportunityid = q.OpportunityId and q.PrimaryQuote=1
	left join CRM.QuoteLines ql ON ql.QuoteKey = q.QuoteKey AND ql.ProductTypeId not in (4)
	left join CRM.QuoteLineDetail qld on ql.QuoteLineId=qld.QuoteLineId
	left join CRM.Orders o on q.QuoteKey=o.QuoteKey
	join CRM.Franchise f on opp.FranchiseId = f.FranchiseID
	left join CRM.Territories tr on opp.TerritoryId = tr.TerritoryId
	where opp.FranchiseId = @FranchiseID
	and (@SourceList is null or s.SourceID in (select id from [CRM].[CSVToTable] (@SourceList)))
	and (@PersonID is null or opp.SalesAgentId in (select id from [CRM].[CSVToTable] (@PersonId)))
	and (@IndustryID is null or a.IndustryId = @IndustryId)
	and (@CommercialTypeID is null or a.CommercialTypeId = @CommercialTypeId)
	and (@CustomerName is null or isnull(nullif(p.CompanyName, ''), p.FirstName + ' ' + p.LastName) like '%' + @CustomerName + '%')
	and cast(o.ContractedDate as Date)  between @StartDate and @EndDate
group by
		isnull(tr.Name, 'Gray Area')
	,	[CRM].[fnGetAccountNameByAccountId](opp.AccountId)
	,	ad.ZipCode
	,	p.PrimaryEmail
	,	s.Name
	,	cast(o.ContractedDate as Date)
	,	case p.PreferredTFN
			when 'c' then p.CellPhone
			when 'h' then p.HomePhone
			when 'w' then p.WorkPhone
			else coalesce (p.WorkPhone, p.HomePhone, p.CellPhone)
		end

end