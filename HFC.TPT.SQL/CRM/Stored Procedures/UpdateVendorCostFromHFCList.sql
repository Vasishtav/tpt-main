﻿CREATE PROCEDURE [CRM].[UpdateVendorCostFromHFCList]
(
	@FranchiseId int,
	@PersonId int,
	@strProductID nvarchar(MAX),
	@BrandID int
)
AS
Begin
	
	update crm.FranchiseProducts set [crm].[FranchiseProducts].Cost=[crm].[SurfacingProducts].Cost,	[crm].[FranchiseProducts].SalePrice=[crm].[SurfacingProducts].SalePrice,  [crm].[FranchiseProducts].LastUpdatedBy=@PersonId,
	[crm].[FranchiseProducts].LastUpdatedOn=GETUTCDATE()
	from [crm].[FranchiseProducts] join [crm].[SurfacingProducts] on [crm].[SurfacingProducts].ProductID=crm.FranchiseProducts.MasterSurfacingProductID
	where [crm].[FranchiseProducts].VendorID in (select hv.VendorId from Acct.HFCVendors hv	join Acct.FranchiseVendors fv on hv.VendorId=fv.VendorId
	where FranchiseId=@FranchiseId and fv.VendorStatus=1 and (hv.IsAlliance=0 or hv.IsAlliance is null)
	union select VendorId from Acct.FranchiseVendors where FranchiseId=@FranchiseId and VendorType=3 and VendorStatus=1) and [crm].[FranchiseProducts].ProductStatus=1

	--select ProductKey,ProductID,ProductName,fp.VendorId,case when exists
 --   (select VendorId from [Acct].[HFCVendors] where VendorId=fp.VendorID)
 --   then (select Name from [Acct].[HFCVendors] where VendorId=fp.VendorID)
 --   else case when exists(select VendorId from [Acct].[FranchiseVendors])
 --   then (select Name from [Acct].[FranchiseVendors] where VendorId=fp.VendorID) end end as VendorName,
 --   VendorProductSKU ,pc.ProductCategory,psc.ProductCategory as ProductSubCategory,Description,
 --   SalePrice,fp.FranchiseId,ps.ProductStatus,fp.MultipleSurfaceProductSet,fp.MasterSurfacingProductID from [CRM].[FranchiseProducts] fp
 --   join Acct.FranchiseVendors fv on fp.VendorID=fv.VendorId
 --   left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
 --   left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
 --   left join [CRM].[Type_ProductCategory] psc on fp.ProductSubCategory=psc.ProductCategoryId
 --   where fv.FranchiseId = @franchiseid and fv.VendorStatus=1 and fp.FranchiseId= @franchiseid and ProductStatusId=1
	--and fp.ProductType!=3 
 --   union
 --   select ProductKey,ProductID,ProductName,fp.VendorId,'Multiple' as VendorName,
 --   VendorProductSKU ,pc.ProductCategory,psc.ProductCategory as ProductSubCategory,Description,
 --   SalePrice,fp.FranchiseId,ps.ProductStatus,fp.MultipleSurfaceProductSet,fp.MasterSurfacingProductID from [CRM].[FranchiseProducts] fp
 --   left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
 --   left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
 --   left join [CRM].[Type_ProductCategory] psc on fp.ProductSubCategory=psc.ProductCategoryId
 --   where fp.FranchiseId= @franchiseid and ProductStatusId=1 and fp.VendorID is null and fp.MultipartSurfacing=1
 --   union
 --   select ProductKey,ProductID,ProductName,fp.VendorId,case when exists
 --   (select VendorId from [Acct].[HFCVendors] where VendorId=fp.VendorID)
 --   then (select Name from [Acct].[HFCVendors] where VendorId=fp.VendorID)
 --   else case when exists(select VendorId from [Acct].[FranchiseVendors])
 --   then (select Name from [Acct].[FranchiseVendors] where VendorId=fp.VendorID) end end as VendorName,
 --   VendorProductSKU, pc.ProductCategory,psc.ProductCategory as ProductSubCategory,fp.Description,fp.SalePrice,fp.FranchiseId, ps.ProductStatus,fp.MultipleSurfaceProductSet,fp.MasterSurfacingProductID from [CRM].[FranchiseProducts] fp
 --   left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
 --   left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
 --   left join [CRM].[Type_ProductCategory] psc on fp.ProductSubCategory=psc.ProductCategoryId
 --   where  fp.FranchiseId= @franchiseid and fp.ProductStatus=1 and fp.ProductType=3
End