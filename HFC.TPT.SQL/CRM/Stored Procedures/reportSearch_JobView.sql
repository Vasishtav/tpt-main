﻿


CREATE PROC [CRM].[reportSearch_JobView]
(
@FranchiseId int,
@SearchTerm varchar(300)=null,
@PageSize int=20,
@PageNumber int=0,
@SortOrder varchar(50) = 'CreatedOnUtc',
@SortDirection varchar(4) ='desc',
@leadStatusIds varchar(500) = null,
@installerPersonIds varchar(500) = null,
@salesPersonIds varchar(500) = null,
@jobStatusIds varchar(500) = null,
@InvoiceStatuses varchar(500) = null,
@SourceIds varchar(500)  = null,
@createdOnUtcStart datetime=null,
@createdOnUtcEnd datetime=null,
@CommercialType int = 0
)

as
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @Ids TABLE (   id int) ;
DECLARE @PreFilter table (id int);


DECLARE @Ids_JobStatus TABLE (JobId int) ;
DECLARE @Ids_Source TABLE (JobId int) ;

DECLARE @BySource table (id int);



IF @jobStatusIds IS NOT NULL
	BEGIN 
		INSERT INTO @PreFilter(id) 
		SELECT cast(Value as int) as id   FROM dbo.SplitCommaToTable(@jobStatusIds);
		-------
		INSERT INTO @PreFilter(id)
		SELECT [Id] FROM [CRM].[Type_JobStatus] 
		WHERE 
		(FranchiseId= @franchiseId or FranchiseId IS NULL) 
		and [ParentId] IN (SELECT Id from @PreFilter) AND [ParentId] IS NOT NULL;
	END	
ELSE
	BEGIN 
		INSERT INTO @PreFilter(id)
		SELECT DISTINCT ID  FROM [CRM].[Type_JobStatus] WHERE id IN (29,30,32) 
		INSERT INTO @PreFilter(id)
		SELECT DISTINCT [Id] FROM [CRM].[Type_JobStatus] ts WHERE 
		(ts.FranchiseId= @franchiseId or ts.FranchiseId is null) 
		and [ParentId] IN  (29,30,32) and ts.isDeleted != 1 
	END

IF @SourceIds IS NOT NULL
	BEGIN 
	PRINT ( '3 @JobsWithSale #Leads By Source, (MUST be not null called from Marketing Preformance report)');	
		INSERT INTO @BySource(id)
		SELECT cast(Value as int) as id   FROM dbo.SplitCommaToTable(@SourceIds)
		/**/ UNION ALL ---
		SELECT SourceId as [Id] FROM [CRM].[Sources]  s
		WHERE 
		(FranchiseId= @franchiseId or FranchiseId IS NULL) 
		and [ParentId] IN (SELECT Id from @BySource) AND [ParentId] IS NOT NULL;

		INSERT INTO @Ids_Source (JobId)
		SELECT DISTINCT j.JobId  as id
		FROM CRM.Leads l 
		INNER JOIN CRM.Jobs j 
			on j.LeadId = l.LeadId
		INNER JOIN crm.LeadSources ls
			on ls.leadid=l.leadid
		INNER JOIN @BySource s
			on ls.[SourceId] = s.id
		INNER JOIN [dbo].[vw_CommercialLeads] as com 
			on com.leadId = l.LeadId
		WHERE l.FranchiseId =@franchiseId AND
		cast(j.ContractedOnUtc as date) BETWEEN @createdOnUtcStart AND @createdOnUtcEnd
		AND isnull(l.isDeleted,0)=0
		AND isnull(j.isDeleted,0)=0
		AND (com.CommercialType = @CommercialType or @CommercialType = 0)

	END	

INSERT INTO @Ids_JobStatus (JobId)
SELECT DISTINCT j.JobId  as JobId
FROM CRM.Leads l 
LEFT OUTER JOIN [dbo].[vw_CommercialLeads] as com 
	on com.leadId = l.LeadId
INNER JOIN CRM.Jobs j 
	on j.LeadId = l.LeadId
	and j.salesPersonId=ISNULL(CAST(@salesPersonIds AS INT),j.salesPersonId)
INNER JOIN @PreFilter s
	on j.[JobStatusId]=s.id
WHERE l.FranchiseId =@franchiseId and
cast(j.ContractedOnUtc as date)  between @createdOnUtcStart and @createdOnUtcEnd and
(CommercialType = @CommercialType or @CommercialType = 0) and 
ISNULL(l.IsDeleted, 0) <> 1 and 
ISNULL(j.IsDeleted, 0) <> 1 

/* MARKETING PERFOMANCE REPORT < */
	IF @jobStatusIds IS NOT NULL AND @SourceIds IS NOT NULL --- Valid lead per Source
	BEGIN 
	--print ('1 Valid lead per Source');
			INSERT INTO @Ids (id)
			SELECT DISTINCT l.JobId 
			FROM @Ids_JobStatus l
			INNER JOIN @Ids_Source s
				on s.JobId=l.JobId
	END
	ELSE 
	BEGIN
			INSERT INTO @Ids (id)
			SELECT DISTINCT l.JobId 
			FROM @Ids_JobStatus l
	END

	
if	@SortDirection = 'Asc'
begin
;WITH Paged AS
(
	SELECT 
		   lead.[LeadId]
		  ,lead.[LeadGuid]
		  ,lead.[PersonId]
		  ,[LastUpdatedOnUtc]
		  ,lead.[Notes]
		  ,job.[JobId]
		  ,[JobGuid]
		  ,[JobNumber]
		  ,isnull(primaryQuote.NetTotal,0) as NetTotal
		  ,isnull(primaryQuote.NetTotal,0) - [CRM].[fnsGetSummJobPayments](job.JobId) as Balance
		  ,job.[CreatedOnUtc]
		  ,[BillingAddressId]
		  ,[InstallAddressId]
		  ,[CustomerPersonId]
		  ,[JobStatusId]
		  ,[SalesPersonId]
		  ,[InstallerPersonId]
		  ,[JobConceptId]
		  ,[ContractedOnUtc]
		  ,[CompletedOnUtc]
		 -- ,[Balance]
		  ,job.[LastUpdated]
		  ,[Hint]
		  ,[Description]
		  ,[SideMark]
		  ,[IsCompleted]
		  ,[CompletionDT]
		  ,[QuoteDateUTC]
		  ,[LeadStatusID]
		  ,[SourceId]
		  ,[Address1]
		  ,[Address2]
		  ,[City]
		  ,[State]
		  ,[ZipCode]
		  ,[AddressesGuid]
		  ,adres.[AddressId]
		  ,p.[WorkPhone]
		  ,p.[HomePhone]
		  ,p.[CellPhone]
		  ,p.[FaxPhone]
		  ,p.[FirstName]
		  ,p.[LastName]
		  ,p.[PrimaryEmail]
		  ,p.PreferredTFN
		  ,p.WorkPhoneExt
		  ,p.CompanyName
		  ,ROW_NUMBER() OVER ( ORDER BY case when @SortOrder = 'CreatedOnUtc' then job.CreatedOnUtc else null end ,
		  case when @SortOrder = 'customer' then p.[FirstName]  else '' end,
		  case when @SortOrder ='jobnumber' then [JobNumber]  else 0 end,
		  case when @SortOrder ='jobstatus' then [JobStatusId]  else 0 end,
		  case when @SortOrder ='nettotal' then [NetTotal]  else 0 end,
		  case when @SortOrder ='salespersonid' then[SalesPersonId]  else 0 end,
		  case when @SortOrder ='balance' then[balance]  else 0 end,
		  case when @SortOrder ='addresses' then [City]+[ZipCode]+[Address1]  else '' end,
		  case when @SortOrder ='addresses' then [City]+[ZipCode]+[Address1]  else '' end
		    ) AS [Row]
	FROM crm.Leads lead 
	INNER JOIN crm.Jobs job 
		on job.LeadId = lead.LeadId
		and lead.FranchiseId=@FranchiseId 
		and lead.isDeleted=0
	INNER JOIN @Ids S
		on s.id=job.[JobId]
	LEFT OUTER JOIN crm.Addresses as adres 
		on adres.AddressId = [InstallAddressId]
	LEFT OUTER JOIN [CRM].[Person] p	
		on p.[PersonId]=lead.[PersonId]
	LEFT OUTER JOIN crm.JobQuotes as primaryQuote 
		on primaryQuote.JobId  = job.JobId 
		and primaryQuote.IsDeleted <> 1 
		and primaryQuote.IsPrimary = 1
	LEFT OUTER JOIN [CRM].[Person] secondPerson  
		on secondPerson.PersonId = lead.SecPersonId
  ),
 TotalCount as (
 SELECT COUNT(*) as TotalCount from Paged
 )
 
 SELECT 
		   [leadid]
		  ,[leadGuid]
		  ,[PersonId]

		  ,[LastUpdatedOnUtc]
		  ,[Notes]
		  ,[JobId]
		  ,[JobGuid]
		  ,[JobNumber]
		  ,[CreatedOnUtc]
		  ,[BillingAddressId]
		  ,[InstallAddressId]
		  ,[CustomerPersonId]
		  ,[JobStatusId]
		  ,[SalesPersonId]
		  ,[InstallerPersonId]
		  ,[JobConceptId]
		  ,[ContractedOnUtc]
		  ,[CompletedOnUtc]
		  ,[Balance]
		  ,[NetTotal]
		  ,[LastUpdated]
		  ,[Hint]
		  ,[Description]
		  ,[SideMark]
		  ,[IsCompleted]
		  ,[CompletionDT]
		  ,[QuoteDateUTC]
		  ,[LeadStatusID]
		  ,[SourceId]
		  ,[Address1]
		  ,[Address2]
		  ,[City]
		  ,[State]
		  ,[ZipCode]
		  ,[AddressesGuid]
		  ,[AddressId]
		  ,[WorkPhone]
		  ,[HomePhone]
		  ,[CellPhone]
		  ,[FaxPhone]
		  ,[FirstName]
		  ,[LastName]
		  ,[PrimaryEmail]
		  ,PreferredTFN
		  ,WorkPhoneExt
		  ,CompanyName
		  ,[Row]
		  ,t.TotalCount
		  FROM Paged c
		  ,TotalCount t
		  WHERE c.[Row] BETWEEN (@PageSize*@PageNumber)+1 AND (@PageSize*@PageNumber)+@PageSize
end
else
begin
;WITH Paged AS
(
	SELECT 
		   lead.[LeadId]
		  ,lead.[LeadGuid]
		  ,lead.[PersonId]
		  ,[LastUpdatedOnUtc]
		  ,lead.[Notes]
		  ,job.[JobId]
		  ,[JobGuid]
		  ,[JobNumber]
		  ,isnull(primaryQuote.NetTotal,0) as NetTotal
		  ,isnull(primaryQuote.NetTotal,0) - [CRM].[fnsGetSummJobPayments](job.JobId) as Balance
		  ,job.[CreatedOnUtc]
		  ,[BillingAddressId]
		  ,[InstallAddressId]
		  ,[CustomerPersonId]
		  ,[JobStatusId]
		  ,[SalesPersonId]
		  ,[InstallerPersonId]
		  ,[JobConceptId]
		  ,[ContractedOnUtc]
		  ,[CompletedOnUtc]
		 -- ,[Balance]
		  ,job.[LastUpdated]
		  ,[Hint]
		  ,[Description]
		  ,[SideMark]
		  ,[IsCompleted]
		  ,[CompletionDT]
		  ,[QuoteDateUTC]
		  ,[LeadStatusID]
		  ,[SourceId]
		  ,[Address1]
		  ,[Address2]
		  ,[City]
		  ,[State]
		  ,[ZipCode]
		  ,[AddressesGuid]
		  ,adres.[AddressId]
		  ,p.[WorkPhone]
		  ,p.[HomePhone]
		  ,p.[CellPhone]
		  ,p.[FaxPhone]
		  ,p.[FirstName]
		  ,p.[LastName]
		  ,p.[PrimaryEmail]
		  ,p.PreferredTFN
		  ,p.WorkPhoneExt
		  ,p.CompanyName
		  --,job.Balance

		  ,ROW_NUMBER() OVER ( ORDER BY case when @SortOrder = 'CreatedOnUtc' then job.CreatedOnUtc else null end  desc,
		  case when @SortOrder = 'customer' then p.[FirstName]  else '' end desc,
		  case when @SortOrder ='jobnumber' then [JobNumber]  else 0 end desc,
		  case when @SortOrder ='jobstatus' then [JobStatusId]  else 0 end desc,
		  case when @SortOrder ='nettotal' then [NetTotal]  else 0 end desc,
		  case when @SortOrder ='salespersonid' then[SalesPersonId]  else 0 end desc,
		  case when @SortOrder ='addresses' then [City]+[ZipCode]+[Address1]  else '' end  desc,
		  case when @SortOrder ='balance' then [balance]  else 0 end desc,
		  case when @SortOrder ='addresses' then [City]+[ZipCode]+[Address1]  else '' end  desc
		  -- case when @SortOrder = 'CreatedOnUtc' then job.CreatedOnUtc else 0 end desc
		    ) AS [Row]
FROM crm.Leads lead 
	INNER JOIN crm.Jobs job 
		on job.LeadId = lead.LeadId
		and lead.FranchiseId=@FranchiseId 
	INNER JOIN @Ids S
		on s.id=job.[JobId]
	LEFT OUTER JOIN crm.Addresses as adres 
		on adres.AddressId = [InstallAddressId]
	LEFT OUTER JOIN [CRM].[Person] p	
		on p.[PersonId]=lead.[PersonId]
	LEFT OUTER JOIN crm.JobQuotes as primaryQuote 
		on primaryQuote.JobId  = job.JobId 
		and primaryQuote.IsDeleted <> 1 
		and primaryQuote.IsPrimary = 1
	LEFT OUTER JOIN [CRM].[Person] secondPerson  
		on secondPerson.PersonId = lead.SecPersonId
 )
 ,
 TotalCount as (
 SELECT COUNT(*) as TotalCount from Paged
 )

 SELECT 
		   [leadid]
		  ,[leadGuid]
		  ,[PersonId]

		  ,[LastUpdatedOnUtc]
		  ,[Notes]
		  ,[JobId]
		  ,[JobGuid]
		  ,[JobNumber]
		  ,[CreatedOnUtc]
		  ,[BillingAddressId]
		  ,[InstallAddressId]
		  ,[CustomerPersonId]
		  ,[JobStatusId]
		  ,[SalesPersonId]
		  ,[InstallerPersonId]
		  ,[JobConceptId]
		  ,[ContractedOnUtc]
		  ,[CompletedOnUtc]
		  ,[Balance]
		  ,[NetTotal]
		  ,[LastUpdated]
		  ,[Hint]
		  ,[Description]
		  ,[SideMark]
		  ,[IsCompleted]
		  ,[CompletionDT]
		  ,[QuoteDateUTC]
		  ,[LeadStatusID]
		  ,[SourceId]
		  ,[Address1]
		  ,[Address2]
		  ,[City]
		  ,[State]
		  ,[ZipCode]
		  ,[AddressesGuid]
		  ,[AddressId]
		  ,[WorkPhone]
		  ,[HomePhone]
		  ,[CellPhone]
		  ,[FaxPhone]
		  ,[FirstName]
		  ,[LastName]
		  ,[PrimaryEmail]
		  ,PreferredTFN
		  ,WorkPhoneExt
		  ,CompanyName
		  ,[Row]
		  ,t.TotalCount
		  
		  FROM Paged c
		  ,TotalCount t
		  WHERE c.[Row] BETWEEN (@PageSize*@PageNumber)+1 AND (@PageSize*@PageNumber)+@PageSize

		 
end		  
		 
			









