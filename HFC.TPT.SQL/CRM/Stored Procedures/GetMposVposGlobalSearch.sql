﻿CREATE PROC [CRM].[GetMposVposGlobalSearch]
( @FranchiseId int
	,@SearchType varchar(50)
	, @Value varchar(500))

AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
set @Value = '%'+ @Value + '%';

IF (@SearchType = 'id' OR @SearchType = 'all')
	BEGIN
		select mpo.MasterPONumber mpo, pod.PICPO vpo, ql.VendorName 
        , (select crm.fnGetAccountNameByAccountId(a.AccountId)) as AccountName
        , o.OrderNumber , mpo.CreateDate MpoDate, pod.CreatedOn as VpoDate
        , pod.PromiseByDate PromiseDate, pod.Status VpoStatus, pod.PICPO
        , case when opp.TerritoryId is not null then  ter.Name
	        else opp.TerritoryType end Territory
        , ql.VendorId, mpo.PurchaseOrderId, pod.StatusId
        , mpox.AccountId
        , opp.OpportunityId, opp.OpportunityName, mpox.QuoteId, mpox.OrderId
        , opp.SalesAgentId, opp.InstallerId
		from crm.MasterPurchaseOrder mpo
		left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
		join crm.Accounts a on a.AccountId = mpox.AccountId
		join crm.Opportunities opp on opp.OpportunityId = mpox.OpportunityId
		join crm.Orders o on o.OrderID = mpox.OrderId
		join crm.PurchaseOrderDetails pod on pod.PurchaseOrderId = mpo.PurchaseOrderId
		join crm.QuoteLines ql on ql.QuoteLineId = pod.QuoteLineId and ql.ProductTypeId not in (3,4)
		left join crm.Territories ter on ter.TerritoryId = opp.TerritoryId
		where a.FranchiseId = @FranchiseId 
			and pod.StatusId <> 8
			and (mpo.MasterPONumber like @Value 
					or pod.PICPO like @Value 
					or o.OrderNumber like @Value)
	END
ELSE 
	BEGIN
		declare @messsage varchar(500) = 'Invaid search type :' + @SearchType;
		THROW 60000, @messsage, 1;
	END;
	
	