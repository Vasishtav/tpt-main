﻿CREATE PROC [CRM].[GetShipmentsGlobalSearch]
( @FranchiseId int
	,@SearchType varchar(50)
	, @Value varchar(500))

AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
set @Value = '%'+ @Value + '%';

IF (@SearchType = 'id' OR @SearchType = 'all')
	BEGIN
		select distinct sn.ShipNoticeId, sn.ShipmentId, sn.ShippedDate
        , ql.VendorName, ql.VendorId, ord.OrderID
        , case when isnull(q.SideMark, '') <> '' then q.SideMark
	            when ISNULL(opp.sidemark, '') <> '' then opp.SideMark
	            else q.QuoteName end SideMark
        , mpo.MasterPONumber Mpo, pod.PICPO Vpo
        , mpo.PurchaseOrderId, ord.OrderNumber
        , (select count(*) from crm.PurchaseOrderDetails pod2
	        where pod2.PICPO = sn.PICVpo) POLines
        , (select sum(pod3.QuantityPurchased) from crm.PurchaseOrderDetails pod3 
	        where pod3.PICPO = sn.PICVpo) POQty
        , (select sum(sd.QuantityShipped) from crm.ShipmentDetail sd 
	        where sd.ShipNoticeId = sn.ShipNoticeId) QtyShipped
        , (select sum(sd2.QuantityReceived) from crm.ShipmentDetail sd2
	        where sd2.ShipNoticeId = sn.ShipNoticeId) QtyReceived
        from crm.ShipNotice sn
        join crm.PurchaseOrderDetails pod on pod.PICPO = sn.PICVpo
        join crm.QuoteLines ql on ql.QuoteLineId = pod.QuoteLineId
        join crm.MasterPurchaseOrder mpo on mpo.PurchaseOrderId = sn.PurchaseOrderId
		join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
        join crm.orders ord on ord.OrderID = mpox.OrderId 
        join crm.Quote q on q.QuoteKey = mpox.QuoteId
        join crm.Opportunities opp on opp.OpportunityId = mpox.OpportunityId
        where opp.FranchiseId = @FranchiseId
	        and ( sn.ShipmentId like @Value)
	END
ELSE 
	BEGIN
		declare @messsage varchar(500) = 'Invaid search type :' + @SearchType;
		THROW 60000, @messsage, 1;
	END;
	
	