﻿



CREATE PROC [CRM].[ConvertLeadAccountSearch]
(
@LeadId int
)
as
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET NOCOUNT ON;

Declare @FranchiseId int;
set @FranchiseId=(select FranchiseId from crm.Leads where leadId=@LeadId)

Declare @LeadPersonId int;
set @LeadPersonId=(select PersonId from crm.Leads where leadId=@LeadId)

Declare @LeadEmailAddress varchar(200);
set @LeadEmailAddress=(select PrimaryEmail from crm.Customer where CustomerId=@LeadPersonId)

Declare @LeadFirstName varchar(200);
set @LeadFirstName=(select FirstName from crm.Customer where CustomerId=@LeadPersonId)

Declare @LeadLastName varchar(200);
set @LeadLastName=(select LastName from crm.Customer where CustomerId=@LeadPersonId)

Declare @LeadAdress1 varchar(200);
Declare @LeadAddressId int;
set @LeadAddressId=(select Top 1 AddressId  from CRM.LeadAddresses where leadId=@LeadId)
set @LeadAdress1 = (select a.Address1 from crm.Addresses a where AddressId=@LeadAddressId)

Declare @LeadCity  varchar(200);
set @LeadCity = (select a.City  from crm.Addresses a where AddressId=@LeadAddressId)

Declare @LeadState  varchar(200);
set @LeadState = (select a.State  from crm.Addresses a where AddressId=@LeadAddressId)
Declare @LeadZip  varchar(200);
set @LeadZip = (select a.ZipCode  from crm.Addresses a where AddressId=@LeadAddressId)

if((@LeadEmailAddress is not null) and (@LeadEmailAddress<>''))
begin
SELECT 
     Account.[AccountId]
	 ,Account.FranchiseId
    ,Account.[AccountGuid]
    ,Account.[PersonId]
    ,Account.[AccountNumber]
    ,[LastUpdatedOnUtc]
    ,Account.[Notes]
    ,Account.[CreatedOnUtc]
	,lv.Name as AccountType
     ,Account.AccountTypeId 
    ,Account.[AccountStatusID]
    ,[Address1]
    ,[Address2]
    ,[City]
    ,[State]
    ,[ZipCode]
    ,[AddressesGuid]
    ,a.[AddressId]
    ,p.[WorkPhone]
    ,p.[HomePhone]
    ,p.[CellPhone]
    ,p.[FirstName]
    ,p.[LastName]
    ,p.[PrimaryEmail]
    ,p.PreferredTFN
    ,p.WorkPhoneExt
    ,p.CompanyName
    ,p.[FaxPhone]
    ,Account.SecPersonId
    ,tls.Name AS [Status]
 FROM [crm].[Accounts] Account 
 INNER JOIN CRM.Type_AccountStatus tls ON tls.Accountstatusid = Account.Accountstatusid
 INNER JOIN [CRM].[Addresses] a
  on a.[AddressId]=(select top 1 la.AddressId from crm.AccountAddresses la where la.AccountId = Account.AccountId)
 INNER JOIN [CRM].[Customer] p 
  on p.CustomerId=Account.[PersonId]
 LEFT JOIN [CRM].[Customer] secondPerson  
  on secondPerson.CustomerId = Account.SecPersonId
  LEFT JOIN CRM.Type_LookUpValues lv on Account.AccountTypeId=lv.Id
WHERE 
(Account.FranchiseId= @FranchiseId) and
 (
(p.PrimaryEmail = @LeadEmailAddress) or 
 
 (p.FirstName= @LeadFirstName and p.LastName=@LeadLastName)
 or(a.Address1= @LeadAdress1 and a.State=@LeadState and a.City=@LeadCity and a.ZipCode=@LeadZip)
 )
 AND (ISNULL(Account.IsDeleted,0)=0)

end;
else
begin
SELECT 
     Account.[AccountId]
	 ,Account.FranchiseId
    ,Account.[AccountGuid]
    ,Account.[PersonId]
    ,Account.[AccountNumber]
    ,[LastUpdatedOnUtc]
    ,Account.[Notes]
    ,Account.[CreatedOnUtc]
	,lv.Name as AccountType
     ,Account.AccountTypeId 
    ,Account.[AccountStatusID]
    ,[Address1]
    ,[Address2]
    ,[City]
    ,[State]
    ,[ZipCode]
    ,[AddressesGuid]
    ,a.[AddressId]
    ,p.[WorkPhone]
    ,p.[HomePhone]
    ,p.[CellPhone]
    ,p.[FirstName]
    ,p.[LastName]
    ,p.[PrimaryEmail]
    ,p.PreferredTFN
    ,p.WorkPhoneExt
    ,p.CompanyName
    ,p.[FaxPhone]
    ,Account.SecPersonId
    ,tls.Name AS [Status]
 FROM [crm].[Accounts] Account 
 INNER JOIN CRM.Type_AccountStatus tls ON tls.Accountstatusid = Account.Accountstatusid
 INNER JOIN [CRM].[Addresses] a
  on a.[AddressId]=(select top 1 la.AddressId from crm.AccountAddresses la where la.AccountId = Account.AccountId)
 INNER JOIN [CRM].[Customer] p 
  on p.CustomerId=Account.[PersonId]
 LEFT JOIN [CRM].[Customer] secondPerson  
  on secondPerson.CustomerId = Account.SecPersonId
  LEFT JOIN CRM.Type_LookUpValues lv on Account.AccountTypeId=lv.Id
WHERE 
(Account.FranchiseId= @FranchiseId) and
 (
 (p.FirstName= @LeadFirstName and p.LastName=@LeadLastName)
 or(a.Address1= @LeadAdress1 and a.State=@LeadState and a.City=@LeadCity and a.ZipCode=@LeadZip)
 )
 AND (ISNULL(Account.IsDeleted,0)=0)

end;

 

