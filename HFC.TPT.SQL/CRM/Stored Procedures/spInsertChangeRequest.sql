﻿CREATE PROCEDURE [CRM].[spInsertChangeRequest]
@CaseId int,
@CaseNumber	nvarchar(15),
@CaseType	nvarchar(256),
@FranchiseId int,
@VendorId	int,
@Details	nvarchar(max),
@StatusId	int,
@CaseChangeId int,
@RecordTypeId	nvarchar(256),
@ProductGroupId	nvarchar(max),
@PublishMethod	varchar(128),
@AttachmentURL nvarchar(max),
@EffectiveDate datetime2,
@CreatedOn	datetime2,
@CreatedBy	int,
@LastUpdatedOn	datetime2,
@LastUpdatedBy	int,
@DeclinedReasonId int,
@Comments varchar(max),
@IsAllSelected bit

--CaseTypeId

AS
BEGIN
IF (@CaseId<>0)
BEGIN
		UPDATE [CRM].[Case] SET 
		StatusId=@StatusId,		
		Details=@Details,		
		LastUpdatedOn=@LastUpdatedOn,
		LastUpdatedBy=@LastUpdatedBy WHERE CaseId=@CaseId

		UPDATE [CRM].[CaseChangeDetails] SET 
		PublishMethod=@PublishMethod,	
		RecordTypeId=@RecordTypeId,
		ProductGroupId=@ProductGroupId,			
		EffectiveDate=@EffectiveDate,
		AttachmentURL=@AttachmentURL,
		DeclinedReasonId=@DeclinedReasonId,
		Comments=@Comments,			
		IsAllSelected=@IsAllSelected,
		LastUpdatedOn=@LastUpdatedOn,		
		LastUpdatedBy=@LastUpdatedBy WHERE CaseChangeId=@CaseChangeId
		
		SET @CaseNumber=(SELECT CaseNumber FROM [CRM].[Case] WHERE CaseId=@CaseId)
END
ELSE
BEGIN
 	DECLARE @CaseTypeId bigint 
	SET @CaseTypeId=(SELECT max(CaseTypeId) FROM [CRM].[Case] WHERE VendorId=@VendorId and CaseType=@CaseType)
	SET @CaseTypeId=ISNULL(@CaseTypeId,0)+1;	
	SET @CaseNumber=@CaseNumber+'-'+CAST(@CaseTypeId AS VARCHAR(10));

	INSERT INTO [CRM].[Case] (CaseNumber,
		CaseTypeId,
		CaseType,		
		VendorId,FranchiseId,
		Details,
		StatusId,
		CreatedOn,
		CreatedBy,
		LastUpdatedOn,
		LastUpdatedBy) VALUES 
		(@CaseNumber,
		@CaseTypeId,
		@CaseType,				
		@VendorId,	@FranchiseId,	
		@Details,
		@StatusId,				 
		@CreatedOn,
		@CreatedBy,
		@LastUpdatedOn,
		@LastUpdatedBy)

		SET @CaseId =(SELECT IDENT_CURRENT('CRM.Case'))

		INSERT INTO [CRM].[CaseChangeDetails] (
			CaseId,
			RecordTypeId,
			ProductGroupId,
			PublishMethod,
			EffectiveDate,
			AttachmentURL,
			Comments,
			DeclinedReasonId,
			CreatedOn,
			CreatedBy,
			IsAllSelected,
			LastUpdatedOn,
			LastUpdatedBy)
			VALUES (@CaseId,@RecordTypeId,@ProductGroupId,@PublishMethod,@EffectiveDate,@AttachmentURL,	@Comments,@DeclinedReasonId,		
			@CreatedOn,
			@CreatedBy,
			@IsAllSelected,
			@LastUpdatedOn,
			@LastUpdatedBy)
END
SELECT @CaseId as CaseId,@CaseNumber as CaseNumber
END
--RETURN 0
