﻿CREATE procedure [CRM].[Report_FranchisePerformance_ProductCategory]
@FranchiseId int,
@startdate datetime null,
@enddate datetime null
as
begin
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
set nocount on

-- Notes core product union my product

if(@startdate is null or @startdate='')
	set @startdate=GETDATE()-365

if(@enddate is null or @enddate='')
	set @enddate=GETDATE()

select ProductCategory,opportunities,Quote,OpenQuote,OpenQuoteAmount,Orders,Unit,
-- ClosingRate
Cast(isnull(CAST(Orders as float)/NULLIF(CAST(Opportunities as float),0),0.00) * 100 as decimal(18,2)) as ClosingRate,
-- QuotetoClose
Cast(isnull(CAST(Orders as float)/NULLIF(CAST(Quote as float),0),0.00) * 100 as decimal(18,2)) as QuotetoClose,
COGS,
COGSProd,
TotalSales,

--GP $
TotalSales-COGS as GPValue,

--GP %
Cast(isnull((TotalSales-COGS)/NULLIF(TotalSales,0),0.00) * 100 as decimal(18,2)) as GPPercentage,

TotalSalesProd,

--GPProd $
TotalSalesProd-COGSProd as GPValueProd,

--GPProd %
Cast(isnull((TotalSalesProd-COGSProd)/NULLIF(TotalSalesProd,0),0.00) * 100 as decimal(18,2)) as GPPercentageProd,
-- AvgOrder
Cast(isnull((CAST(TotalSales as float)/NULLIF(CAST(Orders as float),0)),0.00)  as decimal(18,2)) as AvgOrder,
Cast(isnull((CAST(TotalSalesProd as float)/NULLIF(CAST(Orders as float),0)),0.00)  as decimal(18,2)) as AvgOrderProd

		,	NonConvertedOpportunitiesInPeriod
		,	AllNonConvertedOpportunities
		,	AllOpenQuote
		,	AllOpenQuoteAmount

 from (

 -- 1st sub query

	select
			isnull(case	when ql.ProductTypeId = 1 then pc.ProductCategory -- Hprd.ProductGroupDesc
					when ql.ProductTypeID = 5 then isnull(pc.ProductCategory + '-' + psc.ProductCategory, '*One Time Product')
					else fpc.ProductCategory end ,'*Missing Category') as ProductCategory
		,	count(distinct case when  cast(opp.CreatedOnUtc as date) between @startdate and @enddate then opp.OpportunityID end) Opportunities
		,	count(distinct case when cast(q.CreatedOn as date) between @startdate and @enddate then q.QuoteKey end) Quote
		,	count(distinct case when cast(q.CreatedOn as date) between @startdate and @enddate  and not (o.OrderID is not null and isnull(o.OrderStatus, 0) not in (6,9)) and q.QuoteStatusId not in (4) and opp.OpportunityStatusId not in (6) then q.QuoteID end) OpenQuote
		,	sum(case when cast(q.CreatedOn as date) between @startdate and @enddate and not (o.OrderID is not null and isnull(o.OrderStatus, 0) not in (6,9)) and q.QuoteStatusId not in (4) and opp.OpportunityStatusId not in (6) then ql.ExtendedPrice else 0 end) OpenQuoteAmount
		,	count(distinct case when isnull(o.OrderStatus, 0) not in (6,9) and cast(o.ContractedDate as date) between @startdate and @enddate then o.OrderID end) Orders
		,	sum(case when o.OrderID is not null and isnull(o.OrderStatus, 0) not in (6,9) and isnull(ql.ProductTypeID, 0) not in (4) and cast(o.ContractedDate as date) between @startdate and @enddate then qld.Quantity else 0 end) Unit
		,	sum(case when o.OrderID is not null and isnull(o.OrderStatus, 0) not in (6,9) and isnull(ql.ProductTypeID, 0) not in (4) and cast(o.ContractedDate as date) between @startdate and @enddate then qld.Quantity * qld.BaseCost else 0 end) COGS
		,	sum(case when o.OrderID is not null and isnull(o.OrderStatus, 0) not in (6,9) and isnull(ql.ProductTypeID, 0) not in (4) and cast(o.ContractedDate as date) between @startdate and @enddate then qld.NetCharge else 0 end) TotalSales
		,	sum(case when o.OrderID is not null and isnull(o.OrderStatus, 0) not in (6,9) and isnull(ql.ProductTypeID, 0) not in (3,4) and cast(o.ContractedDate as date) between @startdate and @enddate then qld.Quantity * qld.BaseCost else 0 end) COGSProd
		,	sum(case when o.OrderID is not null and isnull(o.OrderStatus, 0) not in (6,9) and isnull(ql.ProductTypeID, 0) not in (3,4) and cast(o.ContractedDate as date) between @startdate and @enddate then qld.NetCharge else 0 end) TotalSalesProd

		,	count(distinct case when cast(opp.CreatedOnUtc as date) between @startdate and @enddate and q.QuoteKey is null and opp.OpportunityStatusId != 6 then opp.OpportunityID end) NonConvertedOpportunitiesInPeriod
		,	count(distinct case when q.QuoteKey is null and opp.OpportunityStatusId != 6 then opp.OpportunityID end) AllNonConvertedOpportunities
		,	count(distinct case when not (o.OrderID is not null and isnull(o.OrderStatus, 0) not in (6,9)) and q.QuoteStatusId not in (4) and opp.OpportunityStatusId not in (6) then q.QuoteID end) AllOpenQuote
		,	sum(case when not (o.OrderID is not null and isnull(o.OrderStatus, 0) not in (6,9)) and q.QuoteStatusId not in (4) and opp.OpportunityStatusId not in (6) then ql.ExtendedPrice else 0 end) AllOpenQuoteAmount

	from CRM.Opportunities opp
--	join CRM.Accounts a ON opp.AccountId = a.AccountId
--	join CRM.AccountCustomers acc on a.AccountId =acc.AccountId and acc.IsPrimaryCustomer=1
--	join CRM.Customer p ON p.CustomerId = acc.CustomerId
--	join CRM.Person sa ON sa.PersonId = opp.SalesAgentId
	left join CRM.Quote q on opp.opportunityid = q.OpportunityId and q.PrimaryQuote=1
	left join CRM.QuoteLines ql ON ql.QuoteKey = q.QuoteKey AND ql.ProductTypeId not in (4)
	left join CRM.QuoteLineDetail qld on ql.QuoteLineId=qld.QuoteLineId
	left join CRM.Orders o on q.QuoteKey=o.QuoteKey
--	left JOIN CRM.MasterPurchaseOrder mpo ON o.OrderId = mpo.OrderID
--	left JOIN CRM.PurchaseOrderDetails pod ON pod.PurchaseOrderId = mpo.PurchaseOrderId and pod.QuoteLineId = ql.QuoteLineId
-- 	left join CRM.ShipNotice sn ON sn.PurchaseOrdersDetailId = pod.PurchaseOrdersDetailId
--	left join CRM.Territories tr on tr.TerritoryId=opp.TerritoryId
-- 	left join CRM.HFCProducts Hprd ON Hprd.ProductID=ql.ProductId
  	left join CRM.Type_ProductCategory pc	ON ql.ProductCategoryId = pc.ProductCategoryId
  	left join CRM.Type_ProductCategory psc	ON ql.ProductSubCategoryId = psc.ProductCategoryId
  	left join CRM.FranchiseProducts fp ON fp.ProductKey = ql.ProductID and fp.FranchiseId = opp.FranchiseId
  	left join CRM.Type_ProductCategory fpc ON	 fp.ProductCategory = fpc.ProductCategoryId
	left join Acct.HFCVendors v ON ql.VendorId = v.VendorID
	where opp.FranchiseId = @FranchiseID
	group by isnull(case	when ql.ProductTypeId = 1 then pc.ProductCategory --Hprd.ProductGroupDesc
					when ql.ProductTypeID = 5 then isnull(pc.ProductCategory + '-' + psc.ProductCategory, '*One Time Product')
					else fpc.ProductCategory end ,'*Missing Category')
	having count(distinct case when  cast(opp.CreatedOnUtc as date) between @startdate and @enddate then opp.OpportunityID end) > 0
		or count(distinct case when cast(q.CreatedOn as date)  between @startdate and @enddate then opp.OpportunityID end) > 0
		or count(distinct case when isnull(o.OrderStatus, 0) not in (6,9) and cast(o.ContractedDate as date) between @startdate and @enddate  then o.OrderID end) > 0
		or count(distinct case when cast(opp.CreatedOnUtc as date) between @startdate and @enddate and q.QuoteKey is null and opp.OpportunityStatusId != 6 then opp.OpportunityID end) > 0
		or count(distinct case when q.QuoteKey is null and opp.OpportunityStatusId != 6 then opp.OpportunityID end) > 0
		or count(distinct case when not (o.OrderID is not null and isnull(o.OrderStatus, 0) not in (6,9)) and q.QuoteStatusId not in (4) and opp.OpportunityStatusId not in (6) then q.QuoteID end) > 0
		or sum(case when not (o.OrderID is not null and isnull(o.OrderStatus, 0) not in (6,9)) and q.QuoteStatusId not in (4) and opp.OpportunityStatusId not in (6) then ql.ExtendedPrice else 0 end) > 0	 ) as Sales
order by ProductCategory asc

end
GO


