﻿CREATE PROCEDURE [CRM].[spSurfacingProductNumber_New]
	AS
BEGIN
	
	SET NOCOUNT ON;

	IF EXISTS(SELECT SurfacingProductNumber FROM CRM.GetNewNumber)
	BEGIN
		UPDATE CRM.GetNewNumber SET SurfacingProductNumber = ISNULL(SurfacingProductNumber,999) + 1
		OUTPUT INSERTED.SurfacingProductNumber 
	END
END
