Create procedure [CRM].[Report_SalesTaxSummary] (
	 @FranchiseId int
	,@PersonId varchar(max)=null
	,@StartDate datetime
	,@EndDate datetime
	,@ReportType varchar(25) = 'Zip' )
as
Begin

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SET NOCOUNT ON


--declare @FranchiseID int = 3, @StartDate datetime = '2019-01-01', @EndDate datetime = '2019-01-31 23:59:59', @PersonID nvarchar(512) = null, @ReportType nvarchar(64) = 'Code'

select distinct QuoteLineID
into #QuoteLines
from CRM.QuoteLines ql
join CRM.Quote q on ql.QuoteKey=q.QuoteKey
join CRM.Orders o on q.QuoteKey = o.QuoteKey
join CRM.Opportunities op on q.OpportunityId=op.OpportunityId
and op.FranchiseId=@FranchiseId
and (op.SalesAgentId in (select id from [CRM].[CSVToTable] (@PersonId)) or @PersonID is null)
and o.OrderStatus not in (6, 9)
order by QuoteLineID


select
	 t.ZipCode
	,td.JurisType
	,td.Jurisdiction
	,t.Code
	,sum(td.TaxableAmount) TaxableAmount
	,td.Rate
	,sum(td.Amount) Amount
into #Output
from CRM.TaxDetails td
join CRM.Tax t on td.TaxId=t.TaxId
join #QuoteLines ql on t.QuoteLineId=ql.QuoteLineId
where t.FranchiseId=@FranchiseId
and cast(t.CreatedOn as Date) between @StartDate and @EndDate
group by
	 t.ZipCode
	,td.JurisType
	,td.Jurisdiction
	,t.Code
	,td.Rate

drop table #QuoteLines

select
	 @ReportType ReportType
	,convert(varchar(128), ZipCode) ReportColValue
	,sum(TaxableAmount) TaxableAmount
	,Rate
	,sum(Amount) Amount
from #Output
where @ReportType = 'Zip'
group by convert(varchar(128), ZipCode), Rate

union all

select
	 @ReportType ReportType
	,convert(varchar(128), Jurisdiction) ReportColValue
	,sum(TaxableAmount) TaxableAmount
	,Rate
	,sum(Amount) Amount
from #Output
where @ReportType = 'City'
and JurisType = 'City'
group by convert(varchar(128), Jurisdiction), Rate

union all

select
	 @ReportType ReportType
	,convert(varchar(128), Jurisdiction) ReportColValue
	,sum(TaxableAmount) TaxableAmount
	,Rate
	,sum(Amount) Amount
from #Output
where @ReportType = 'County'
and JurisType = 'County'
group by convert(varchar(128), Jurisdiction), Rate

union all

select
	 @ReportType ReportType
	,convert(varchar(128), Jurisdiction) ReportColValue
	,sum(TaxableAmount) TaxableAmount
	,Rate
	,sum(Amount) Amount
from #Output
where @ReportType = 'State'
and JurisType = 'State'
group by convert(varchar(128), Jurisdiction), Rate

union all

select
	 @ReportType ReportType
	,convert(varchar(128), Jurisdiction) ReportColValue
	,sum(TaxableAmount) TaxableAmount
	,Rate
	,sum(Amount) Amount
from #Output
where @ReportType = 'Special'
and JurisType = 'Special'
group by convert(varchar(128), Jurisdiction), Rate

union all

select
	 @ReportType ReportType
	,convert(varchar(128), Code) ReportColValue
	,sum(TaxableAmount) TaxableAmount
	,Rate
	,sum(Amount) Amount
from #Output
where @ReportType = 'Code'
group by convert(varchar(128), Code), Rate

drop table #Output

end
