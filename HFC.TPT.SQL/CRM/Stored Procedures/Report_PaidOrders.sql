﻿CREATE procedure CRM.Report_PaidOrders (
	@FranchiseId int,
	@PersonID varchar(max) = null,
	@Year int = 2020,
	@Mth int = 3)
as
begin;
set nocount on;

with PayOff as (
		select OrderID, PaymentDate
		from (
			select
				  OrderID
				, PaymentDate
				, case when Reversal = 1 then -Amount else Amount end Amount
				, Reversal
				, BalanceDue
				, row_number() over (partition by OrderID order by PaymentDate) Idx
			from CRM.Payments where BalanceDue <= 0) d
		where Idx = 1
		and year(PaymentDate) = @Year
		and datepart(month, PaymentDate) = @Mth )
select
		 isnull(tr.Name, 'Gray Area') Territory
		,o.ContractedDate ContractedDate
		,po.PaymentDate PaidDate
		,jc.JobCompleted CompletedDate
		,sa.FirstName+' '+sa.LastName SalesPerson
		,o.OrderNumber OrderNo
		--,ProductName ProductType
		,sum(isnull(qld.BaseCost * qld.Quantity,0.00)) COGS
		,sum(isnull(qld.NetCharge, 0.00)) Sale
		,sum(case when ql.ProductTypeID not in (3,4) then isnull(qld.BaseCost * qld.Quantity,0.00) else 0 end) COGSProduct
		,sum(case when ql.ProductTypeID not in (3,4) then isnull(qld.NetCharge, 0.00) else 0 end) SaleProduct
		,sum(isnull(t.Amount, 0.00)) Tax
		,case when a.IsCommercial=1
			then
				case when p.CompanyName is not null and p.CompanyName!='' then p.CompanyName +' / ' else '' end + p.FirstName + ' ' + p.LastName 
			else 
				p.FirstName + ' ' + p.LastName + case when p.CompanyName is not null and p.CompanyName!='' then ' / '+p.CompanyName else '' end
		  end as Customer
		,case
			when nullif(a.KeyAcct, '') is not null then 'Key Account'
			when a.IsCommercial = 1 then 'Commercial'
			else 'Customer' end AccountType
		,a.AccountNumber AccountNo
		,'TPT-' + convert(varchar(12), o.OrderNumber) QBInvoiceNo
	from CRM.Opportunities opp
	join CRM.Person sa ON sa.PersonId = opp.SalesAgentId 
	join CRM.Quote q on opp.opportunityid = q.OpportunityId and q.PrimaryQuote=1
	join CRM.QuoteLines ql ON ql.QuoteKey = q.QuoteKey AND ql.ProductTypeId not in (4)
	join CRM.QuoteLineDetail qld on ql.QuoteLineId=qld.QuoteLineId
	join CRM.Orders o on q.QuoteKey=o.QuoteKey
	join PayOff po on o.OrderID = po.OrderID
	join CRM.Accounts a ON opp.AccountId = a.AccountId
    join CRM.AccountCustomers acc on a.AccountId =acc.AccountId and acc.IsPrimaryCustomer=1
    join CRM.Customer p ON p.CustomerId = acc.CustomerId
	left join CRM.Tax t on ql.QuoteLineId = t.QuoteLineId
 	left join CRM.Territories tr on tr.TerritoryId=opp.TerritoryId
	left join ( SELECT [OrderID], max(CreatedOn) JobCompleted
				FROM [CRM].[OrderStatusTracker]
				where OrderStatusId = 5
				group by orderid ) jc
		on o.OrderID = jc.OrderID
	where opp.FranchiseId = @FranchiseID
	and (@PersonID is null or sa.PersonID in (select id from [CRM].[CSVToTable] (@PersonId)))
	and isnull(o.OrderStatus, 0) not in (6,9)
	and isnull(ql.ProductTypeID, 0) not in (4)
	and o.BalanceDue <= 0
	group by
		 isnull(tr.Name, 'Gray Area')
		,o.ContractedDate
		,po.PaymentDate
		,jc.JobCompleted
		,sa.FirstName+' '+sa.LastName
		,o.OrderNumber
		--,ProductName
		,case when a.IsCommercial=1
			then
				case when p.CompanyName is not null and p.CompanyName!='' then p.CompanyName +' / ' else '' end + p.FirstName + ' ' + p.LastName 
			else 
				p.FirstName + ' ' + p.LastName + case when p.CompanyName is not null and p.CompanyName!='' then ' / '+p.CompanyName else '' end
		  end
		,case
			when nullif(a.KeyAcct, '') is not null then 'Key Account'
			when a.IsCommercial = 1 then 'Commercial'
			else 'Customer' end
		,a.AccountNumber
		,'TPT-' + convert(varchar(12), o.OrderNumber);

end;