﻿CREATE procedure [CRM].[Report_SalesSummaryByMonth] (
@FranchiseId int,
@TerritoryId int =null,
@PersonId int=null,
@Zipcode varchar(50)=null,
@CurrentYear datetime = null )
as
begin
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET NOCOUNT ON;

set @Zipcode = nullif(@Zipcode, '');
set @CurrentYear = isnull(@CurrentYear, getdate());

declare	@StartDate datetime = format(dateadd(year, -4, @CurrentYear), 'yyyy-01-01')
	,	@EndDate datetime = format(@CurrentYear, 'yyyy-12-31')
	,	@MSREndDate datetime
	,	@Sql nvarchar(max);

Declare @Data table (
	 OrderID int
	,TerritoryID int
	,Mth Date
	,MSRSales numeric(18,2)
	,TotalSales numeric(18,2)
	,COGSAmount numeric(18,2)
	,GP numeric(18,2)
	,CoreProductSales numeric(18,2)
	,MyProductSales numeric(18,2)
	,Services numeric(18,2)
	,MSR bit );


insert into @Data
select
	 o.[OrderID]
	,t.TerritoryId
	,cast(format(ContractedDate, 'yyyy-MM-01') as datetime)[Mth]
	,0 [MSRSales]
	,sum(qld.NetCharge) [TotalSales]
	,isnull(Sum(qld.BaseCost*qld.Quantity),0) [COGSAmount]
	,sum(qld.NetCharge) - isnull(Sum(qld.BaseCost*qld.Quantity),0) GP
	,sum(case when ql.ProductTypeId in (1) then qld.NetCharge else 0 end) [CoreProductSales]
	,sum(case when ql.ProductTypeId in (2,5) then qld.NetCharge else 0 end) [MyProductSales]
	,q.AdditionalCharges [Services]
	,0 MSR
from CRM.Opportunities op
join CRM.Quote q on op.opportunityid = q.OpportunityId and q.PrimaryQuote=1
join CRM.QuoteLines ql ON ql.QuoteKey = q.QuoteKey AND ql.ProductTypeId not in (4)
join CRM.QuoteLineDetail qld on ql.QuoteLineId=qld.QuoteLineId
join CRM.Orders o on q.QuoteKey=o.QuoteKey
join CRM.Addresses ad on op.InstallationAddressId=ad.AddressId
left join CRM.Territories t on op.TerritoryId = t.TerritoryId
where op.FranchiseId=@FranchiseId
and o.OrderStatus not in (6,9)
and CAST(ContractedDate as date) between @startdate and @enddate
and isnull(@TerritoryID, isnull(op.TerritoryId, -1)) = isnull(op.TerritoryID, -1)
and isnull(@PersonID, op.SalesAgentID) = op.SalesAgentId
and isnull(@Zipcode, ad.ZipCode) = ad.ZipCode
--and

--(	 o.ContractedDate >= isnull(case when t.TerritoryID is not null then t.MsrReportDate
--		else (select min(MSRReportDate) from CRM.Territories where FranchiseID = @FranchiseID)
--		end, getdate())
--	or	@PersonID is not null --Remove MSRReportDate Requirement if filtered
--	or	@ZipCode is not null )
GROUP BY o.[OrderID], t.TerritoryID, cast(format(o.ContractedDate, 'yyyy-MM-01') as datetime), q.AdditionalCharges;


select @MSREndDate = isnull(dateadd(month, -1, min(Mth)), @EndDate) from @Data;

if exists (select * from sys.databases where name = 'BFast2006' and state_desc = 'online' and @MSREndDate is not null)
begin

select @Sql =
'SELECT	null [OrderID]
	,	t.TerritoryID
	,	cast(format(m.PeriodFile, ''yyyy-MM-01'') as datetime) [Mth]
	,	sum(isnull(nullif(r.gsBlinds, 0) , m.gsBlinds ) + isnull(nullif(r.gsShutters, 0), m.gsShutters) ) [MSRSales]
	,	0 [TotalSales]
	,	0 [COGSAmount]
	,	0 [GP]
	,	0 [CoreProductSales]
	,	0 [MyProductSales]
	,	0 [Services]
	,	1 MSR
from	[BFast2006].[dbo].[eomMsr_BFastMSR] m
join	CRM.Territories t
	on rtrim(ltrim(m.TerrCode)) = t.Code
left join [BFast2006].[dbo].[eomMsr_Receivables] r
	on	m.PeriodFile = r.PeriodFile
	and m.TerrID = r.TerrID
where  t.FranchiseID = @FranchiseID
and m.PeriodFile >= @StartDate
--and m.PeriodFile < isnull(t.MsrReportDate, getdate())
and isnull(@TerritoryID, t.TerritoryId) = t.TerritoryID
and @PersonID is null
and @Zipcode is null
group by t.TerritoryID, cast(format(m.PeriodFile, ''yyyy-MM-01'') as datetime)'

insert into @Data
EXECUTE sp_executesql @Sql,N'@FranchiseId int,@TerritoryId int,@PersonId int,@Zipcode varchar(50),@startdate datetime,@enddate datetime,@MSREndDate datetime'
  , @FranchiseId=@FranchiseId,@TerritoryId=@TerritoryId,@PersonId=@PersonId,@Zipcode=@Zipcode,@startdate=@startdate,@enddate=@enddate, @MSREndDate=@MSREndDate;

end;

if exists (select * from sys.databases where name = 'TailorTools' and state_desc = 'online')
begin

select @SQl =
'SELECT	null [OrderID]
	,	t.TerritoryID
	,	cast(format(m.PeriodFile, ''yyyy-MM-01'') as datetime) [Mth]
	,   cast(sum(m.GrossSales) as Numeric(18,2)) [MSRSales]
	,	0 [TotalSales]
	,	0 [COGSAmount]
	,	0 [GP]
	,	0 [CoreProductSales]
	,	0 [MyProductSales]
	,	0 [Services]
	,	1 MSR
from	[TailorTools].[dbo].eomMsr_MyCRM_tl m
join	CRM.Territories t
	on	rtrim(ltrim(m.TerritoryCode)) = t.Code
where  t.FranchiseID = @FranchiseID
and m.PeriodFile >= @StartDate
--and m.PeriodFile < isnull(t.MsrReportDate, getdate())
and isnull(@TerritoryID, t.TerritoryId) = t.TerritoryID
and @PersonID is null
and @Zipcode is null
group by t.TerritoryID, cast(format(m.PeriodFile, ''yyyy-MM-01'') as datetime)'

insert into @Data
EXECUTE sp_executesql @Sql,N'@FranchiseId int,@TerritoryId int,@PersonId int,@Zipcode varchar(50),@startdate datetime,@enddate datetime,@MSREndDate datetime'
  , @FranchiseId=@FranchiseId,@TerritoryId=@TerritoryId,@PersonId=@PersonId,@Zipcode=@Zipcode,@startdate=@startdate,@enddate=@enddate,@MSREndDate=@MSREndDate;

end;

with Yrs as
( select cast(format(@CurrentYear, 'yyyy-MM-01') as datetime)  [Mth]
  union all
  select dateadd(Year, -1, Mth)  [Mth]
  from Yrs
  where Mth > dateadd(year, -4, @CurrentYear))
insert into @Data -- Always return rows if no data
select	null [OrderID]
	,	null TerritoryID
	,	[Mth]
	,	0 [MSRSales]
	,	0 [TotalSales]
	,	0 [COGSAmount]
	,	0 [GP]
	,	0 [CoreProductSales]
	,	0 [MyProductSales]
	,	0 [Services]
	,	0 [MSR]
from Yrs;

declare @Temp table
(
    [Year] Varchar(50),
    ThisYear Varchar(50),
    rowno int,
    [Jan] numeric(18,2),[Feb] numeric(18,2),[Mar] numeric(18,2),[Apr] numeric(18,2),[May] numeric(18,2),
    [Jun] numeric(18,2),[Jul] numeric(18,2),[Aug] numeric(18,2),[Sep] numeric(18,2),[Oct] numeric(18,2),
	[Nov] numeric(18,2),[Dec] numeric(18,2),[tot] numeric(18,2)
);


insert into @Temp
select  cast([Year] as varchar(50)) [Year]
	,	cast(case DataType
			when 'MSRSales' then 'MSR Sales'
			when 'TotalSales' then 'Total Sales'
			when 'CogsAmount' then 'COGS'
			when 'GP' then 'GP $'
			when 'GPPct' then 'GP %'
			when 'OrderCt' then 'Order count'
			when 'OrderAvg' then 'Order Avg'
			when 'CoreProductSales' then 'Core Product Sales'
			when 'MyProductSales' then 'My Product Sales'
			when 'Services' then 'Services' end as varchar(50)) [ThisYear]
	,	cast(case DataType
			when 'MSRSales' then 1
			when 'TotalSales' then 2
			when 'CogsAmount' then 3
			when 'GP' then 4
			when 'GPPct' then 5
			when 'OrderCt' then 6
			when 'OrderAvg' then 7
			when 'CoreProductSales' then 8
			when 'MyProductSales' then 9
			when 'Services' then 10 end as int) [rowno]
	,	isnull([Jan], 0) [Jan]
	,	isnull([Feb], 0) [Feb]
	,	isnull([Mar], 0) [Mar]
	,	isnull([Apr], 0) [Apr]
	,	isnull([May], 0) [May]
	,	isnull([Jun], 0) [Jun]
	,	isnull([Jul], 0) [Jul]
	,	isnull([Aug], 0) [Aug]
	,	isnull([Sep], 0) [Sep]
	,	isnull([Oct], 0) [Oct]
	,	isnull([Nov], 0) [Nov]
	,	isnull([Dec], 0) [Dec]
	,	isnull([tot], 0) [tot]
from
(
select [Year], Mth, DataType, Val
  from (	select	 year(Mth) [Year]
					,format(Mth, 'MMM') Mth
					, cast(sum([MSRSales]) as Numeric(18,2)) [MSRSales]
					, cast(sum([TotalSales]) as Numeric(18,2)) [TotalSales]
					, cast(sum([CogsAmount]) as Numeric(18,2)) [CogsAmount]
					, cast(sum([GP]) as Numeric(18,2)) [GP]
					, cast(isnull(sum([GP])/NULLIF(sum(case when [MSR] = 0 then [TotalSales] else 0 end),0),0)*100 as numeric(18,2)) GPPct
					, cast(count(distinct OrderID) as Numeric(18,2)) [OrderCt]
					, cast(isnull(sum(case when [MSR] = 0 then [TotalSales] else 0 end) / nullif(count(distinct OrderID), 0), 0) as Numeric(18,2)) [OrderAvg]
					, cast(sum([CoreProductSales]) as Numeric(18,2)) [CoreProductSales]
					, cast(sum([MyProductSales]) as Numeric(18,2)) [MyProductSales]
					, cast(sum([Services]) as Numeric(18,2)) [Services]
			from	@Data
			group by year(Mth), format(Mth, 'MMM')
			union all
			select		year(Mth) [Year]
					,  'tot' Mth
					, cast(sum([MSRSales]) as Numeric(18,2)) [MSRSales]
					, cast(sum([TotalSales]) as Numeric(18,2)) [TotalSales]
					, cast(sum([CogsAmount]) as Numeric(18,2)) [CogsAmount]
					, cast(sum([GP]) as Numeric(18,2)) [GP]
					, cast(isnull(sum([GP])/NULLIF(sum(case when [MSR] = 0 then [TotalSales] else 0 end),0),0)*100 as numeric(18,2)) GPPct
					, cast(count(distinct OrderID) as Numeric(18,2)) [OrderCt]
					, cast(isnull(sum(case when [MSR] = 0 then [TotalSales] else 0 end) / nullif(count(distinct OrderID), 0), 0) as Numeric(18,2)) [OrderAvg]
					, cast(sum([CoreProductSales]) as Numeric(18,2)) [CoreProductSales]
					, cast(sum([MyProductSales]) as Numeric(18,2)) [MyProductSales]
					, cast(sum([Services]) as Numeric(18,2)) [Services]
			from	@Data
			group by year(Mth) ) d
  unpivot (
    Val for DataType in ([MSRSales], [TotalSales], [CogsAmount], [GP], [GPPct], [OrderCt], [OrderAvg], [MyProductSales],[CoreProductSales], [Services])
  ) unpiv ) src
pivot (
	sum(val) for Mth in ([Jan], [Feb], [Mar], [Apr], [May], [Jun], [Jul], [Aug], [Sep], [Oct], [Nov], [Dec], [tot]) ) piv
order by rowno;

select * from @Temp where [Year]=YEAR(@CurrentYear) order by [Year], rowno;
select * from @Temp where [Year]=YEAR(@CurrentYear)-1 order by [Year], rowno;
select * from @Temp where [Year]=YEAR(@CurrentYear)-2 order by [Year], rowno;
select * from @Temp where [Year]=YEAR(@CurrentYear)-3 order by [Year], rowno;
select * from @Temp where [Year]=YEAR(@CurrentYear)-4 order by [Year], rowno;

end;

go