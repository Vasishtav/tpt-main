﻿CREATE PROCEDURE [CRM].[GetCoreProductModels]
AS
    BEGIN
        SELECT hv.Name AS Vendor, 
               hv.PICVendorId, 
               hp.ProductName, 
               hp.ProductGroup, 
               hp.ProductGroupDesc, 
               hp.BrandId, 
               CONCAT(hp.WidthPrompt, ', ', hp.HeightPrompt, ', ', hp.MountPrompt, ', ', hp.ColorPrompt, ', ', hp.FabricPrompt) AS Prompts, 
               h.*, 
        (
            SELECT SUBSTRING(
            (
                SELECT Concat(hm.FieldName, ' flag changed from ',
                                            CASE
                                                WHEN hm.oldvalue = 1
                                                THEN ' Enabled '
                                                ELSE ' Disabled '
                                            END, ' to ',
                                                 CASE
                                                     WHEN hm.newvalue = 1
                                                     THEN ' Enabled '
                                                     ELSE ' Disabled '
                                                 END, ' by ', p.FirstName, ' ', p.LastName, ' on ', CONVERT(VARCHAR(100), CONVERT(DATETIME, SWITCHOFFSET(hm.ChangedOn, DATEPART(TZOFFSET, hm.ChangedOn AT TIME ZONE 'Pacific Standard Time'))))) + ', '
                FROM History.HFCModelFlags hm
                     INNER JOIN crm.Person p ON p.PersonId = hm.Changedby
                WHERE hm.ModelKey = h.ModelKey
                ORDER BY hm.ChangedOn DESC FOR XML PATH('')
            ), 0, 9999)
        ) AS Audittrail
        FROM crm.HFCModels h
             INNER JOIN crm.HFCProducts hp ON hp.ProductKey = h.ProductId
             INNER JOIN Acct.HFCVendors hv ON hv.VendorId = h.VendorId
        ORDER BY hv.name, 
                 hp.ProductName, 
                 h.Model ASC;
    END;