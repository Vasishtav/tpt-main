﻿


CREATE PROCEDURE [CRM].[SyncFailedCalendarAppointmentsByTime]
--@startdate datetime=null
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;
SELECT  c.*
FROM api.CalendarSync cs 
INNER JOIN crm.Calendar c ON c.CalendarId = cs.CalendarId
INNER JOIN crm.Franchise f ON f.FranchiseId = c.FranchiseId
WHERE cs.ProviderUniqueId IS NULL AND f.FranchiseId not IN (2,7,13,43,78) AND ISNULL(c.IsDeleted,0)=0 and  cs.ErrorCount >0 AND cs.ErrorCount<5
--AND c.CreatedOnUtc>dateadd(day,-1, getutcdate())
END;
