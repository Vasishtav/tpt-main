﻿CREATE PROC [CRM].[GetAccountsGlobalSearch]
( @FranchiseId int
	,@SearchType varchar(50)
	, @Value varchar(500))

AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @phoneValue varchar(20) =  '%' + [CRM].[fnRmoveFormatPhone](@value) + '%';
set @Value = '%'+ @Value + '%';

IF (@SearchType = 'email')
	BEGIN
		select a.AccountId Id
        , Case when a.TerritoryId is not null then ter.Name
	            else a.TerritoryType
	            End Territory 
        , a.TerritoryType Territory
        , [CRM].[fnGetAccountNameByAccountId](a.AccountId) Name
	    ,CASE WHEN isnull(c.PrimaryEmail,'') <> '' THEN c.PrimaryEmail ELSE c.SecondaryEmail END Email
		, CASE c.PreferredTFN 
		    WHEN 'C' THEN c.CellPhone
		    WHEN 'H' THEN c.HomePhone
		    WHEN 'W' THEN 
					CASE WHEN isnull(c.WorkPhoneExt, '') <> '' THEN CONCAT(c.WorkPhone, ' - ', c.WorkPhoneExt) 
					ELSE c.WorkPhone
					END
			ELSE ''
		    END Phone 
	    from crm.Accounts a 
        join crm.AccountCustomers ac on ac.AccountId = a.AccountId and ac.IsPrimaryCustomer = 1
        join crm.Customer c on c.CustomerId = ac.CustomerId 
        left join crm.Territories ter on ter.TerritoryId = a.TerritoryId

		where a.FranchiseId = @Franchiseid
            and a.AccountStatusId = 1
            and ( c.PrimaryEmail like  @Value 
	            or c.SecondaryEmail like @Value ) 
		Order by Name
	END;
ELSE IF (@SearchType = 'phone')
	BEGIN
		select a.AccountId Id
        , Case when a.TerritoryId is not null then ter.Name
	            else a.TerritoryType
	            End Territory 
        , a.TerritoryType Territory
        , [CRM].[fnGetAccountNameByAccountId](a.AccountId) Name
	    , c.PrimaryEmail Email, CASE WHEN c.CellPhone like @phoneValue THEN c.CellPhone
			WHEN c.HomePhone like @phoneValue THEN c.HomePhone
			WHEN c.WorkPhone like @phoneValue THEN 
				CASE WHEN isnull(c.WorkPhoneExt, '') <> '' THEN CONCAT(c.WorkPhone, ' - ', c.WorkPhoneExt) 
				ELSE c.WorkPhone
				END
			ELSE CASE c.PreferredTFN 
				WHEN 'C' THEN c.CellPhone
				WHEN 'H' THEN c.HomePhone
				WHEN 'W' THEN 
					CASE WHEN isnull(c.WorkPhoneExt, '') <> '' THEN CONCAT(c.WorkPhone, ' - ', c.WorkPhoneExt) 
					ELSE c.WorkPhone
					END
				ELSE ''
				END 
			END Phone 
	    from crm.Accounts a 
        join crm.AccountCustomers ac on ac.AccountId = a.AccountId and ac.IsPrimaryCustomer = 1
        join crm.Customer c on c.CustomerId = ac.CustomerId 
        left join crm.Territories ter on ter.TerritoryId = a.TerritoryId

		where a.FranchiseId = @FranchiseId 
            and a.AccountStatusId = 1
            and ( c.HomePhone like  @phoneValue 
                or c.WorkPhone like @phoneValue
                or c.CellPhone like @phoneValue ) 
		Order by Name
	END;
ELSE IF (@SearchType = 'name')
	BEGIN
		select a.AccountId Id
        , Case when a.TerritoryId is not null then ter.Name
	            else a.TerritoryType
	            End Territory 
        , a.TerritoryType Territory
        , [CRM].[fnGetAccountNameByAccountId](a.AccountId) Name
	    , c.PrimaryEmail Email, CASE c.PreferredTFN 
		    WHEN 'C' THEN c.CellPhone
		    WHEN 'H' THEN c.HomePhone
		    WHEN 'W' THEN 
					CASE WHEN isnull(c.WorkPhoneExt, '') <> '' THEN CONCAT(c.WorkPhone, ' - ', c.WorkPhoneExt) 
					ELSE c.WorkPhone
					END
			ELSE ''
		    END Phone 
	    from crm.Accounts a 
        join crm.AccountCustomers ac on ac.AccountId = a.AccountId and ac.IsPrimaryCustomer = 1
        join crm.Customer c on c.CustomerId = ac.CustomerId 
        left join crm.Territories ter on ter.TerritoryId = a.TerritoryId

		left join crm.Accounts a2 on a2.AccountId = a.RelatedAccountId  
		left join crm.AccountCustomers ac2 on ac2.AccountId = a2.AccountId and ac2.IsPrimaryCustomer = 1
		left join crm.Customer c2 on c2.CustomerId = ac2.CustomerId 
		where a.FranchiseId = @FranchiseId 
			and a.AccountStatusId = 1
			and ( c.FirstName like @Value
				or c.LastName like @Value
				or CONCAT(C.firstname, ' ', C.LastName) like @Value
				or c.CompanyName like @Value
				or c2.FirstName like @Value
				or c2.LastName like @Value
				or CONCAT(C2.firstname, ' ', C2.LastName) like @Value
				or c2.CompanyName like @Value )
		order by Name
	END
ELSE IF (@SearchType = 'address')
	BEGIN
		select a.AccountId Id
        , Case when a.TerritoryId is not null then ter.Name
	            else a.TerritoryType
	            End Territory 
        , a.TerritoryType Territory
        , [CRM].[fnGetAccountNameByAccountId](a.AccountId) Name
	    , c.PrimaryEmail Email, CASE c.PreferredTFN 
		    WHEN 'C' THEN c.CellPhone
		    WHEN 'H' THEN c.HomePhone
		    WHEN 'W' THEN 
					CASE WHEN isnull(c.WorkPhoneExt, '') <> '' THEN CONCAT(c.WorkPhone, ' - ', c.WorkPhoneExt) 
					ELSE c.WorkPhone
					END
			ELSE ''
		    END Phone 
	    from crm.Accounts a 
        join crm.AccountCustomers ac on ac.AccountId = a.AccountId and ac.IsPrimaryCustomer = 1
        join crm.Customer c on c.CustomerId = ac.CustomerId 
        left join crm.Territories ter on ter.TerritoryId = a.TerritoryId

		where a.AccountId in (
			select aa.AccountId from crm.AccountAddresses aa 
			Left join crm.Addresses addr on addr.AddressId = aa.AddressId
			left join crm.Accounts a on a.AccountId = aa.AccountId 
			where a.FranchiseId = @FranchiseId 
			and a.AccountStatusId = 1
			and ( addr.Address1 like @Value 
				or addr.Address2 like @Value )) 
		Order by Name
	END
ELSE IF(@SearchType = 'city')
	BEGIN
		select a.AccountId Id
        , Case when a.TerritoryId is not null then ter.Name
	            else a.TerritoryType
	            End Territory 
        , a.TerritoryType Territory
        , [CRM].[fnGetAccountNameByAccountId](a.AccountId) Name
	    , c.PrimaryEmail Email, CASE c.PreferredTFN 
		    WHEN 'C' THEN c.CellPhone
		    WHEN 'H' THEN c.HomePhone
		    WHEN 'W' THEN 
					CASE WHEN isnull(c.WorkPhoneExt, '') <> '' THEN CONCAT(c.WorkPhone, ' - ', c.WorkPhoneExt) 
					ELSE c.WorkPhone
					END
			ELSE ''
		    END Phone 
	    from crm.Accounts a 
        join crm.AccountCustomers ac on ac.AccountId = a.AccountId and ac.IsPrimaryCustomer = 1
        join crm.Customer c on c.CustomerId = ac.CustomerId 
        left join crm.Territories ter on ter.TerritoryId = a.TerritoryId

		where a.AccountId in (
            select aa.AccountId from crm.AccountAddresses aa 
            Left join crm.Addresses addr on addr.AddressId = aa.AddressId
            left join crm.Accounts a on a.AccountId = aa.AccountId 
            where a.FranchiseId = @FranchiseId 
            and a.AccountStatusId = 1
            and ( addr.City like @Value )
		) Order by Name
	END
ELSE IF (@SearchType = 'all')
	BEGIN
		select a.AccountId Id
        , Case when a.TerritoryId is not null then ter.Name
	            else a.TerritoryType
	            End Territory 
        , a.TerritoryType Territory
        , [CRM].[fnGetAccountNameByAccountId](a.AccountId) Name
	    , CASE WHEN isnull(c.PrimaryEmail,'') <> '' THEN c.PrimaryEmail ELSE c.SecondaryEmail END Email
		, CASE WHEN c.CellPhone like @phoneValue THEN c.CellPhone
			WHEN c.HomePhone like @phoneValue THEN c.HomePhone
			WHEN c.WorkPhone like @phoneValue THEN 
				CASE WHEN isnull(c.WorkPhoneExt, '') <> '' THEN CONCAT(c.WorkPhone, ' - ', c.WorkPhoneExt) 
				ELSE c.WorkPhone
				END
			ELSE CASE c.PreferredTFN 
				WHEN 'C' THEN c.CellPhone
				WHEN 'H' THEN c.HomePhone
				WHEN 'W' THEN 
					CASE WHEN isnull(c.WorkPhoneExt, '') <> '' THEN CONCAT(c.WorkPhone, ' - ', c.WorkPhoneExt) 
					ELSE c.WorkPhone
					END
				ELSE ''
				END 
			END Phone 
	    from crm.Accounts a 
        join crm.AccountCustomers ac on ac.AccountId = a.AccountId and ac.IsPrimaryCustomer = 1
        join crm.Customer c on c.CustomerId = ac.CustomerId 
        left join crm.Territories ter on ter.TerritoryId = a.TerritoryId

	    left join crm.Accounts a2 on a2.AccountId = a.RelatedAccountId  
	    left join crm.AccountCustomers ac2 on ac2.AccountId = a2.AccountId and ac2.IsPrimaryCustomer = 1
	    left join crm.Customer c2 on c2.CustomerId = ac2.CustomerId 

	    where a.FranchiseId = @FranchiseId
            and a.AccountStatusId = 1
            and ( c.PrimaryEmail like  @Value 
	            or c.SecondaryEmail like @Value 
			
			    or c.HomePhone like  @phoneValue 
                or c.WorkPhone like @phoneValue
                or c.CellPhone like @phoneValue 
			
			    or c.FirstName like @Value
	            or c.LastName like @Value
                or CONCAT(C.firstname, ' ', C.LastName) like @Value
	            or c.CompanyName like @Value
	            or c2.FirstName like @Value
	            or c2.LastName like @Value
                or CONCAT(C2.firstname, ' ', C2.LastName) like @Value
	            or c2.CompanyName like @Value 
			
			    or a.AccountId in (
				    select aa.AccountId from crm.AccountAddresses aa 
				    Left join crm.Addresses addr on addr.AddressId = aa.AddressId
				    left join crm.Accounts a on a.AccountId = aa.AccountId 
				    where a.FranchiseId = @FranchiseId 
				    and a.AccountStatusId = 1
				    and 
				    ( addr.Address1 like @Value 
					    or addr.Address2 like @Value 
					    or addr.City like @Value)
				) 
			) Order by Name
	END
ELSE 
	BEGIN
		declare @messsage varchar(500) = 'Invaid search type :' + @SearchType;
		THROW 60000, @messsage, 1;
	END;
	
	