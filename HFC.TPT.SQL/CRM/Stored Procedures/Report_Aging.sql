create Procedure CRM.Report_Aging (
	@FranchiseID int )
as
begin

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET NOCOUNT ON;

SELECT 
   case when a.IsCommercial=1 
		then case when p.CompanyName is not null and p.CompanyName!='' then p.CompanyName+' / ' else '' end + p.FirstName + ' ' + p.LastName 
		else   p.FirstName + ' ' + p.LastName + case when p.CompanyName is not null and p.CompanyName!='' then ' / '+p.CompanyName else '' end
	end as CustName
  ,sum(isnull(pay.PaymentsReceived, 0)) CurrentPaid
  ,sum(case when datediff(day, getdate(), o.ContractedDate) < 31 then o.BalanceDue else 0 end) Day1
  ,sum(case when datediff(day, getdate(), o.ContractedDate) between 31 and 60 then o.BalanceDue else 0 end) Day31
  ,sum(case when datediff(day, getdate(), o.ContractedDate) between 61 and 90 then o.BalanceDue else 0 end) Day61
  ,sum(case when datediff(day, getdate(), o.ContractedDate) > 90 then o.BalanceDue else 0 end) Day91
  ,sum(o.BalanceDue) OpenAgainst
  ,sum(o.OrderTotal) TotalSale
  FROM CRM.Orders o
  LEFT JOIN
		(select orderid, sum(isnull(case when isnull(Reversal, 0) = 0 then Amount else Amount * -1 end, 0)) PaymentsReceived from CRM.Payments group by OrderID) pay
			on o.OrderID = pay.OrderID
  INNER JOIN CRM.Opportunities opp ON o.OpportunityId = opp.OpportunityId
  INNER JOIN CRM.Accounts a ON opp.AccountId = a.AccountId
  INNER JOIN CRM.AccountCustomers acc on a.AccountId =acc.AccountId and acc.IsPrimaryCustomer=1
  INNER JOIN CRM.Customer p ON p.CustomerId = acc.CustomerId 
  INNER JOIN CRM.Person sa ON sa.PersonId = opp.SalesAgentId 
  LEFT JOIN CRM.Territories tr on tr.TerritoryId=a.TerritoryId
  WHERE o.OrderStatus not in (6,9)
  AND opp.FranchiseId = @FranchiseID
  AND o.BalanceDue != 0
group by
   case when a.IsCommercial=1 
		then case when p.CompanyName is not null and p.CompanyName!='' then p.CompanyName+' / ' else '' end + p.FirstName + ' ' + p.LastName 
		else   p.FirstName + ' ' + p.LastName + case when p.CompanyName is not null and p.CompanyName!='' then ' / '+p.CompanyName else '' end
	end

end