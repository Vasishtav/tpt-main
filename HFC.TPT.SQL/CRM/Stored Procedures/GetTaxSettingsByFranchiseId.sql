﻿CREATE PROCEDURE [CRM].[GetTaxSettingsByFranchiseId] @FranchiseId INT,
                                                    @PersonId    INT
AS
    BEGIN
        SET NOCOUNT ON;
        IF NOT EXISTS
        (
            SELECT 1
            FROM CRM.TaxSettings ts
            WHERE ts.FranchiseId = @FranchiseId
                  AND ts.TerritoryId IS NULL
        )
            BEGIN
                INSERT INTO CRM.TaxSettings
                (FranchiseId,
                 TerritoryId,
                 ShipToLocationId,
                 UseCustomerAddress,
                 CreatedOn,
                 CreatedBy,
                 UpdatedBy,
                 UpdatedOn
                )
                VALUES
                (@FranchiseId,
                 NULL,
                 NULL,
                 1,
                 GETUTCDATE(),
                 @PersonId,
                 @PersonId,
                 GETUTCDATE()
                );
        END;
        WITH tax_Settings
             AS (SELECT ts.Id,
                        ts.FranchiseId,
                        ts.TerritoryId,
                        ts.ShipToLocationId,
                        CASE
                            WHEN ISNULL(ts.UseCustomerAddress, 1) = 1
                            THEN 1
                            ELSE ts.UseCustomerAddress
                        END AS UseCustomerAddress,
                        stl.AddressId,
                        ts.CreatedOn,
                        ts.CreatedBy,
                        ts.UpdatedBy,
                        ts.UpdatedOn,
                        t.Name AS TerritoryName,
                        stl.ShipToName,
                        STUFF(COALESCE(', ' + NULLIF(Address1, ''), '') + COALESCE(', ' + NULLIF(Address2, ''), '') + COALESCE(', ' + NULLIF(City, ''), '') + COALESCE(', ' + NULLIF([State], ''), '') + COALESCE(', ' + NULLIF(ZipCode, ''), '') + COALESCE(', ' + NULLIF(CountryCode2Digits, ''), ''), 1, 1, '') AS Address,
                        ts.TaxType,
                        ts.UseTaxPercentage,
                 (
                     SELECT lv.Name
                     FROM crm.Type_LookUpValues lv
                          INNER JOIN CRM.Type_LookUpTable tlut ON lv.TableId = tlut.Id
                     WHERE tlut.TableName = 'TaxType'
                           AND lv.SortOrder = ts.TaxType
                 ) AS TaxTypeName
                 FROM crm.Territories t
                      RIGHT JOIN CRM.TaxSettings ts ON t.TerritoryId = ts.TerritoryId
                      LEFT JOIN crm.ShipToLocations stl ON stl.id = ts.ShipToLocationId
                      LEFT JOIN crm.Addresses a ON a.AddressId = stl.AddressId
                 WHERE ts.FranchiseId = @FranchiseId
                       AND ts.TerritoryId IS NULL
                 UNION
                 SELECT ts.Id,
                        t.FranchiseId,
                        t.TerritoryId,
                        ts.ShipToLocationId,
                        CASE
                            WHEN ISNULL(ts.UseCustomerAddress, 1) = 1
                            THEN 1
                            ELSE ts.UseCustomerAddress
                        END AS UseCustomerAddress,
                        stl.AddressId,
                        ts.CreatedOn,
                        ts.CreatedBy,
                        ts.UpdatedBy,
                        ts.UpdatedOn,
                        t.Name AS TerritoryName,
                        stl.ShipToName,
                        STUFF(COALESCE(', ' + NULLIF(Address1, ''), '') + COALESCE(', ' + NULLIF(Address2, ''), '') + COALESCE(', ' + NULLIF(City, ''), '') + COALESCE(', ' + NULLIF([State], ''), '') + COALESCE(', ' + NULLIF(ZipCode, ''), '') + COALESCE(', ' + NULLIF(CountryCode2Digits, ''), ''), 1, 1, '') AS Address,
                        ts.TaxType,
                        ts.UseTaxPercentage,
                 (
                     SELECT lv.Name
                     FROM crm.Type_LookUpValues lv
                          INNER JOIN CRM.Type_LookUpTable tlut ON lv.TableId = tlut.Id
                     WHERE tlut.TableName = 'TaxType'
                           AND lv.SortOrder = ts.TaxType
                 ) AS TaxTypeName
                 FROM crm.Territories t
                      LEFT JOIN CRM.TaxSettings ts ON t.TerritoryId = ts.TerritoryId
                      LEFT JOIN crm.ShipToLocations stl ON stl.id = ts.ShipToLocationId
                      LEFT JOIN crm.Addresses a ON a.AddressId = stl.AddressId
                 WHERE t.FranchiseId = @FranchiseId)
             SELECT *,
                    ROW_NUMBER() OVER(
                    ORDER BY TerritoryName ASC) AS TempId
             FROM tax_Settings
             ORDER BY TerritoryName ASC;
    END;
GO