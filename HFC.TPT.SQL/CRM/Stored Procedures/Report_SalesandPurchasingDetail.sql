﻿CREATE   Procedure [CRM].[Report_SalesandPurchasingDetail] (
  @FranchiseId int,
  @PersonId varchar(max)=null,
  @VendorID int=null,
  @IndustryId int=null,
  @CommercialTypeId int=null,
  @CustomerName varchar(200)=null,
  @StartDate datetime=null,
  @EndDate datetime=null )

as
  begin
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
  DECLARE @sqlCommand nvarchar(max)

    set @sqlCommand='SELECT
  case when opp.TerritoryID is not null then tr.Name else opp.TerritoryType end as TerritoryName
  ,sa.FirstName + '' '' +sa.LastName as Salesperson
  ,case when a.IsCommercial=1
  then case when p.CompanyName is not null and p.CompanyName!='''' then p.CompanyName+'' / '' else '''' end + p.FirstName + '' '' + p.LastName
  else
  p.FirstName + '' '' + p.LastName + case when p.CompanyName is not null and p.CompanyName!='''' then '' / ''+p.CompanyName else '''' end
  end as CustName
  ,o.OrderNumber as CustOrder
  ,po.MasterPONumber as MPO
  ,po.PICPO as VPO
  , ql.QuoteLineNumber as LineNumber
  , ql.VendorName
  ,case when ql.ProductTypeId= 1 then pc.ProductCategory -- Hprd.ProductGroupDesc
		when ql.ProductTypeID = 5 then pc.ProductCategory + ''-'' + psc.ProductCategory
		else fpc.ProductCategory end as ProductCategory
  ,ql.ProductName
  ,'''' ProductModel
  ,ql.WindowLocation
  ,ql.Description
  ,isnull(qld.BaseCost * qld.Quantity,0.00) as LineCost
  ,qld.Netcharge as LineValue
  ,	 (qld.NetCharge) - (qld.BaseCost * qld.Quantity) GPValue
  ,	case when isnull(  ( (qld.NetCharge) - (qld.BaseCost * qld.Quantity) ) / nullif( (qld.NetCharge) , 0), 0)  < 0
				then 0 else isnull(  ( (qld.NetCharge) -  (qld.BaseCost * qld.Quantity) ) / nullif( (qld.NetCharge) , 0), 0) end * 100.0 GPPercenatge
	,	case when ql.ProductTypeId not in (3,4) then qld.NetCharge else 0 end LineCostProd
	,	case when ql.ProductTypeID not in (3,4) then (qld.NetCharge) - (qld.BaseCost * qld.Quantity) else 0 end GPValueProd
  ,	case when ql.ProductTypeID not in (3,4) and isnull(  ( (qld.NetCharge) - (qld.BaseCost * qld.Quantity) ) / nullif( (qld.NetCharge) , 0), 0)  < 0
				then 0 else case when ql.ProductTypeID not in (3,4) then isnull(  ( (qld.NetCharge) -  (qld.BaseCost * qld.Quantity) ) / nullif( (qld.NetCharge) , 0), 0) else 0 end end * 100.0 GPPercenatgeProd
 --,case when qld.Netcharge>0 then Cast(isnull((qld.Netcharge-(qld.BaseCost * qld.Quantity)),0.00) as decimal(18,2)) else 0.0 end as GPValue
 -- ,case when qld.Netcharge>0 then Cast(isnull(((qld.Netcharge-(qld.BaseCost * qld.Quantity))/ql.ExtendedPrice)*100 ,0.00) as decimal(18,2)) else 0.0 end as GPPercenatge
  ,CONVERT(date, o.CreatedOn) AS CustOrderDate
  ,CONVERT(date, po.CreateDate) AS MPODate
  ,CONVERT(date, po.CreatedOn) as VPODate
  ,CONVERT(date, po.PromiseByDate) PromiseByDate
  ,CONVERT(date, po.ShipDate) as EstShipDate
  ,CONVERT(date, ship.ShippedDate) as actualshipdate
  ,CONVERT(date, vi.[Date]) as InvoiceDate
  ,po.[Status] as VPOStatus
  ,cast(po.Errors as XML).value(''.'', ''nvarchar(max)'') as Errors
  FROM CRM.Orders o
  Join CRM.Quote q on o.QuoteKey=q.QuoteKey and q.PrimaryQuote=1
  JOIN CRM.QuoteLines ql ON ql.QuoteKey = q.QuoteKey AND ql.ProductTypeId not in (4)
  JOIN CRM.QuoteLineDetail qld on ql.QuoteLineId=qld.QuoteLineId
  left Join (
	SELECT poe.OrderID
		, poe.QuoteID
		, pod.QuoteLineID
		, mpo.MasterPONumber
		, mpo.PICPO
		, mpo.CreateDate
		, pod.CreatedOn
		, pod.ShipDate
		, pod.PromiseByDate
		, pod.[Status]
		, pod.Errors
		, pod.PurchaseOrdersDetailId
    FROM CRM.MasterPurchaseOrderExtn poe
	JOIN CRM.PurchaseOrderDetails pod ON poe.PurchaseOrderID = pod.PurchaseOrderID
	JOIN CRM.MasterPurchaseOrder mpo ON pod.PurchaseOrderId = mpo.PurchaseOrderId
	where pod.Status != ''Canceled'' and pod.PICPO != 0 ) po
		on	o.OrderID = po.OrderID
		and	q.QuoteKey = po.QUoteID
		and ql.QuoteLineId = po.QuoteLineId
  INNER JOIN CRM.Opportunities opp ON o.OpportunityId = opp.OpportunityId
  INNER JOIN CRM.Accounts a ON opp.AccountId = a.AccountId
  INNER JOIN CRM.AccountCustomers acc on a.AccountId =acc.AccountId and acc.IsPrimaryCustomer=1
  INNER JOIN CRM.Customer p ON p.CustomerId = acc.CustomerId
  INNER JOIN CRM.Person sa ON sa.PersonId = opp.SalesAgentId
  left join (
	select sd.QuoteLineId, max(sn.ShippedDate) ShippedDate
	from crm.ShipmentDetail sd
	join crm.ShipNotice sn on sn.ShipNoticeID = sd.ShipNoticeID
	group by sd.QuoteLineId ) ship
		on ql.QuoteLineId = ship.QuoteLineId
  LEFT JOIN CRM.Territories tr on tr.TerritoryId=opp.TerritoryID
 -- LEFT JOIN CRM.HFCProducts Hprd ON Hprd.ProductID=ql.ProductId
  LEFT JOIN CRM.Type_ProductCategory pc	ON ql.ProductCategoryId = pc.ProductCategoryId
  LEFT JOIN CRM.Type_ProductCategory psc	ON ql.ProductSubCategoryId = psc.ProductCategoryId
  LEFT JOIN CRM.FranchiseProducts fp ON fp.ProductKey = ql.ProductID and fp.FranchiseId = a.FranchiseId
  LEFT JOIN CRM.Type_ProductCategory fpc ON	 fp.ProductCategory = fpc.ProductCategoryId
  left join crm.VendorInvoice vi on vi.PurchaseOrdersDetailId = po.PurchaseOrdersDetailId
  WHERE a.FranchiseId = @FranchiseID and o.OrderStatus not in (6,9)'
  --and isnull(mpo.PICPO, -1) != 0 '

   if(@PersonId is not null and @PersonId !='' )
		set @sqlCommand=@sqlCommand + ' and opp.SalesAgentId in ('+@PersonId+')'

    if(@VendorID is not null )
		set @sqlCommand=@sqlCommand + ' and ql.VendorId=@VendorID'

    if(@CommercialTypeId is not null )
		set @sqlCommand=@sqlCommand + ' and a.CommercialTypeId=@CommercialTypeId'

    if(@IndustryId is not null )
		set @sqlCommand=@sqlCommand + ' and a.IndustryId=@IndustryId'

   if(@StartDate is not null and @EndDate is not null and @StartDate<=@EndDate)
		set @sqlCommand=@sqlCommand + ' and CAST(o.ContractedDate as DATE) between @StartDate and @EndDate'

     if(@CustomerName is not null)
		begin
		set @CustomerName='%'+@CustomerName+'%'
		set @sqlCommand= 'select * from ( '+@sqlCommand+' ) as p where CustName like @CustomerName order by CustOrder,LineNumber asc'
		end
	 else
		set @sqlCommand=@sqlCommand + ' order by o.OrderNumber,ql.QuoteLineNumber asc'

	print @sqlCommand

   EXECUTE sp_executesql @sqlCommand,N'@FranchiseId int,@VendorID int,@CommercialTypeId int,@IndustryId int,@CustomerName varchar(200),@StartDate datetime,@EndDate datetime'
  , @FranchiseId=@FranchiseId,@VendorID=@VendorID,@CommercialTypeId=@CommercialTypeId,@IndustryId=@IndustryId,@CustomerName=@CustomerName,@StartDate=@StartDate,@EndDate=@EndDate

  end
GO


