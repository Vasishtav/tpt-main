﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [CRM].[spBatch_Counter_and_Reminder]
	-- Add the parameters for the stored procedure here
	@PersonId INT
	, @FranchiseId INT
	, @RemindMethodEnum TINYINT = 1
	, @TaskCount INT OUTPUT
	, @NewLeadCount INT OUTPUT 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT @TaskCount = COUNT(*)
	FROM CRM.Tasks t with (nolock)
		JOIN CRM.EventToPeople p with (nolock) on t.TaskId = p.TaskId
	WHERE t.IsDeleted = 0 AND t.FranchiseId = @FranchiseId AND t.CompletedDate is null
		AND p.PersonId = @PersonId

	SELECT @NewLeadCount = COUNT(*)
	FROM CRM.Leads l with (nolock)
	WHERE l.LeadStatusId = 1 AND l.IsDeleted = 0
		AND l.FranchiseId = @FranchiseId AND l.CreatedOnUtc >= DATEADD(day, -30, GETUTCDATE())

	SELECT p.EventPersonId,
		c.AptTypeEnum,
		COALESCE(c.[Subject], t.[Subject]) [Subject],
		COALESCE(c.[Message], t.[Message]) [Message],
		COALESCE(p.RecurringNextStartDate, c.[StartDate], t.[DueDate]) StartDate, --use recurring next start date if any, then start date, then due date
		COALESCE(p.RecurringNextEndDate, c.[EndDate]) EndDate,
		c.IsAllDay,
		COALESCE(c.LeadId, t.LeadId) LeadId,
		COALESCE(c.LeadNumber, t.LeadNumber) LeadNumber
	FROM CRM.EventToPeople p with (nolock)
		LEFT JOIN CRM.Tasks t with (nolock) on p.TaskId = t.TaskId
		LEFT JOIN CRM.Calendar c with (nolock) on p.CalendarId = c.CalendarId		
	WHERE p.PersonId = @PersonId 
		AND ((p.RemindMethodEnum & @RemindMethodEnum) != 0)
		AND p.DismissedDate IS NULL
		AND p.RemindDate is not null AND CONVERT(datetime2, p.RemindDate, 1) <= GETUTCDATE() 
		-- Convert reminddate to utc date
		-- Now we add in calendar or task logic so we dont retrieve deleted and not completed for task
		AND ((p.CalendarId is not null AND c.IsDeleted = 0)
			 OR (p.TaskId is not null AND t.IsDeleted = 0 AND t.CompletedDate is null)) 

END

