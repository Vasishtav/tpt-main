﻿CREATE procedure [CRM].[ProcurementDashboard]
  @FranchiseId int,
  @vendorId int,
  @MPOStatusId varchar(200),
  @VPOStatusId varchar(200),
  @datefilter varchar(200)
as
Begin
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
DECLARE @sqlCommand nvarchar(max)
DECLARE @StartDate datetime,@EndDate datetime

set @EndDate = CAST(getutcdate() as date)

	if(@datefilter='4000') --Today
		set @StartDate = CAST(getdate() as date)

	else if(@datefilter = '4001') -- LastDay
		set @StartDate = CAST(getdate()-1 as date)

	else if(@datefilter = '4002') -- Week to today date From Sunday
		SELECT @StartDate = CAST(DATEADD(DAY, 1-DATEPART(WEEKDAY, GETDATE()), GETDATE()) as Date)

	else if(@datefilter = '4003') -- Month to today date
		SELECT @StartDate = DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0)

	else if(@datefilter = '4004')
		SELECT @StartDate = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0)

	else if(@datefilter = '4005')  -- Last 7 days
		select @StartDate = Cast(GETDATE()-7 as Date)

	else if(@datefilter='4006')  -- Last 14 days
		select @StartDate=Cast(GETDATE()-14 as Date)

	else if(@datefilter='4007')  -- Last 30 days
		select @StartDate=Cast(GETDATE()-30 as Date)

	else if(@datefilter='4008')  -- Last 90 days
		select @StartDate=Cast(GETDATE()-90 as Date)

	else if(@datefilter='4009')  -- Last 180 days
		select @StartDate=Cast(GETDATE()-180 as Date)
	else
		select @StartDate= cast(-53680 as datetime)

set @sqlCommand='SELECT distinct
		case when o2.TerritoryId is not null then tr.Name else o2.TerritoryType end as Territory
		,sn.ShippedDate ShippedDate
		,ql.ProductTypeId
		,mpo.MasterPONumber
		,mpo.PurchaseOrderId
		,pod.PICPO
		,o.OrderID
		,o.OrderNumber
		,o.CreatedOn OrderDate
		,mpo.CreateDate MPODate
		,o2.OpportunityId
		,o2.OpportunityName
		,a.AccountId
		,o2.SalesAgentId
		,o2.InstallerId
		,mpo.IsXMpo
		,(select [CRM].[fnGetAccountNameByAccountId](a.AccountId)) as AccountName
		,CAST(pod.CreatedOn AS DATE) as VPODate
		,pod.PromiseByDate PromiseByDate
		,pod.ShipDate EstShipDate
		,pod.StatusId, pod.Status as VPOStatus, ql.VendorId, ql.VendorName,ql.VendorId
		,MPO.SubmittedDate SubmittedDate
		,(select sum(NetCharge) from CRM.PurchaseOrderDetails spod INNER JOIN CRM.QuoteLines subql ON subql.QuoteLineId = spod.QuoteLineId AND subql.ProductTypeId not in (3,4 )
		INNER JOIN [CRM].[QuoteLineDetail] sqld on subql.QuoteLineId = sqld.QuoteLineId
		where spod.PurchaseOrderId=mpo.PurchaseOrderId) OrderSubtotal
		,(select sum(subql.Quantity) from CRM.PurchaseOrderDetails spod
		INNER JOIN CRM.QuoteLines subql ON subql.QuoteLineId = spod.QuoteLineId AND subql.ProductTypeId not in (3,4 )
		where spod.PurchaseOrderId=mpo.PurchaseOrderId AND spod.PICPO = pod.PICPO AND subql.VendorId=ql.VendorId) TotalQuantity
		,(select sum(ISNULL(sqld1.BaseCost,0)*sqld1.Quantity) from CRM.PurchaseOrderDetails spod1
		 INNER JOIN CRM.QuoteLines subql1 ON subql1.QuoteLineId = spod1.QuoteLineId AND subql1.ProductTypeId not in (3,4 )
		INNER JOIN [CRM].[QuoteLineDetail] sqld1 on sqld1.QuoteLineId = spod1.QuoteLineId
		where  ISNULL(spod1.PICPO,0) = ISNULL(pod.PICPO,0) AND subql1.VendorId=ql.VendorId AND spod1.PurchaseOrderId=pod.PurchaseOrderId) Cost
		FROM crm.MasterPurchaseOrder mpo
		Left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
		INNER JOIN crm.PurchaseOrderDetails pod ON pod.PurchaseOrderId = mpo.PurchaseOrderId
		INNER JOIN crm.Orders o ON o.OrderId = mpox.OrderID
		INNER JOIN crm.Opportunities o2 ON o.OpportunityId = o2.OpportunityId
		INNER JOIN crm.Accounts a ON mpox.AccountId = a.AccountId
		INNER JOIN crm.Customer p ON p.CustomerId = a.PersonId
		INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId AND ql.ProductTypeId not in (3,4 )
		LEFT JOIN crm.ShipNotice sn ON sn.PICVpo = pod.PICPO
		LEFT JOIN CRM.Territories tr on tr.TerritoryId=o2.TerritoryId
		WHERE a.FranchiseId = @FranchiseId
		AND CAST(mpo.CreateDate as date) between '''+CONVERT(char(10), @StartDate,126)+''' and '''+CONVERT(char(10), @EndDate,126)+'''
		';

if(@vendorId>0)
	set @sqlCommand = @sqlCommand + ' AND ql.VendorId = @vendorId';

if(@MPOStatusId is not null and @MPOStatusId!='' and @VPOStatusId is not null and @VPOStatusId!='')
	begin
		set @sqlCommand = @sqlCommand + ' AND (mpo.POStatusId =1 or pod.StatusId IN (select id from CRM.CSVToTable(@VPOStatusId)))'
	end
else if(@MPOStatusId is not null and @MPOStatusId!='')
	begin
		set @sqlCommand = @sqlCommand +' AND mpo.POStatusId =1'
	end
else if(@VPOStatusId is not null and @VPOStatusId!='')
	begin
		set @sqlCommand = @sqlCommand +' AND pod.StatusId IN (select id from CRM.CSVToTable(@VPOStatusId))'
	end

	set @sqlCommand = @sqlCommand +' order by VPODate desc'

 EXECUTE sp_executesql @sqlCommand,N'
  @FranchiseId int,
  @vendorId int,
  @MPOStatusId varchar(200),
  @VPOStatusId varchar(200),
  @datefilter varchar(200)',
  @FranchiseId=@FranchiseId,
  @vendorId=@vendorId,
  @MPOStatusId=@MPOStatusId,
  @VPOStatusId=@VPOStatusId,
  @datefilter=@datefilter
End
GO

