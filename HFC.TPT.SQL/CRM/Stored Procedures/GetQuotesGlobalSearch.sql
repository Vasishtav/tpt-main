﻿CREATE PROC [CRM].[GetQuotesGlobalSearch]
( @FranchiseId int
	,@SearchType varchar(50)
	, @Value varchar(500))

AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
set @Value = '%'+ @Value + '%';

IF (@SearchType = 'name')
	BEGIN
		select q.ExpiryDate, q.QuoteKey, q.QuoteID QuoteId, a.AccountId, o.OpportunityId
        , q.QuoteName, o.OpportunityName Opportunity, q.SaleDate
        , Concat(c.FirstName, ' ', c.LastName) Account
        , Case when o.TerritoryId is not null then ter.Name
	        else o.TerritoryType
            End Territory
        , case when exists(select 1 from crm.orders 
                where orderstatus not in(6, 9) and opportunityid = q.OpportunityId) then 1
            else 0
        end OrderExists  
        , q.SideMark, qs.QuoteStatus
        , q.QuoteSubTotal + ISNULL( (select sum(td.Amount) from crm.Tax t
        Left join crm.TaxDetails td on td.TaxId = t.TaxId
        Left join crm.QuoteLines ql on ql.QuoteLineId = t.QuoteLineId
        where ql.QuoteKey = q.QuoteKey
        group by ql.QuoteKey ), 0) TotalAmount
        , o.InstallerId, o.SalesAgentId, q.QuoteStatusId
        from crm.Quote q
        left join crm.Opportunities o on o.OpportunityId = q.OpportunityId
        left join crm.Accounts a on a.AccountId = o.AccountId
        left join crm.AccountCustomers ac on ac.AccountId = a.AccountId and ac.IsPrimaryCustomer = 1
        left join crm.Customer c on c.CustomerId = ac.CustomerId
        left Join crm.Type_QuoteStatus qs on qs.QuoteStatusId = q.QuoteStatusId
        left join crm.Territories ter on ter.TerritoryId = o.TerritoryId
        where o.FranchiseId = @FranchiseId 
            and q.QuoteStatusId <> 5
			and ( q.QuoteName like @Value 
				or q.SideMark like @Value )
	END
ELSE IF (@SearchType = 'id')
	BEGIN
		select q.ExpiryDate, q.QuoteKey, q.QuoteID QuoteId, a.AccountId, o.OpportunityId
        , q.QuoteName, o.OpportunityName Opportunity, q.SaleDate
        , Concat(c.FirstName, ' ', c.LastName) Account
        , Case when o.TerritoryId is not null then ter.Name
	        else o.TerritoryType
            End Territory
        , case when exists(select 1 from crm.orders 
                where orderstatus not in(6, 9) and opportunityid = q.OpportunityId) then 1
            else 0
        end OrderExists  
        , q.SideMark, qs.QuoteStatus
        , q.QuoteSubTotal + ISNULL( (select sum(td.Amount) from crm.Tax t
        Left join crm.TaxDetails td on td.TaxId = t.TaxId
        Left join crm.QuoteLines ql on ql.QuoteLineId = t.QuoteLineId
        where ql.QuoteKey = q.QuoteKey
        group by ql.QuoteKey ), 0) TotalAmount
        , o.InstallerId, o.SalesAgentId, q.QuoteStatusId
        from crm.Quote q
        left join crm.Opportunities o on o.OpportunityId = q.OpportunityId
        left join crm.Accounts a on a.AccountId = o.AccountId
        left join crm.AccountCustomers ac on ac.AccountId = a.AccountId and ac.IsPrimaryCustomer = 1
        left join crm.Customer c on c.CustomerId = ac.CustomerId
        left Join crm.Type_QuoteStatus qs on qs.QuoteStatusId = q.QuoteStatusId
        left join crm.Territories ter on ter.TerritoryId = o.TerritoryId
        where o.FranchiseId = @FranchiseId 
            and q.QuoteStatusId <> 5
			and ( q.QuoteId like @Value 
				 or o.OpportunityNumber like @Value)
	END
ELSE IF (@SearchType = 'all')
	BEGIN
		select q.ExpiryDate, q.QuoteKey, q.QuoteID QuoteId, a.AccountId, o.OpportunityId
        , q.QuoteName, o.OpportunityName Opportunity, q.SaleDate
        , Concat(c.FirstName, ' ', c.LastName) Account
        , Case when o.TerritoryId is not null then ter.Name
	        else o.TerritoryType
            End Territory
        , case when exists(select 1 from crm.orders 
                where orderstatus not in(6, 9) and opportunityid = q.OpportunityId) then 1
            else 0
        end OrderExists  
        , q.SideMark, qs.QuoteStatus
        , q.QuoteSubTotal + ISNULL( (select sum(td.Amount) from crm.Tax t
        Left join crm.TaxDetails td on td.TaxId = t.TaxId
        Left join crm.QuoteLines ql on ql.QuoteLineId = t.QuoteLineId
        where ql.QuoteKey = q.QuoteKey
        group by ql.QuoteKey ), 0) TotalAmount
        , o.InstallerId, o.SalesAgentId, q.QuoteStatusId
        from crm.Quote q
        left join crm.Opportunities o on o.OpportunityId = q.OpportunityId
        left join crm.Accounts a on a.AccountId = o.AccountId
        left join crm.AccountCustomers ac on ac.AccountId = a.AccountId and ac.IsPrimaryCustomer = 1
        left join crm.Customer c on c.CustomerId = ac.CustomerId
        left Join crm.Type_QuoteStatus qs on qs.QuoteStatusId = q.QuoteStatusId
        left join crm.Territories ter on ter.TerritoryId = o.TerritoryId
        where o.FranchiseId = @FranchiseId 
            and q.QuoteStatusId <> 5

			and ( q.QuoteName like @Value 
                or q.SideMark like @Value 
                or q.QuoteId like @Value 
                or o.OpportunityNumber like @Value)
	END
ELSE 
	BEGIN
		declare @messsage varchar(500) = 'Invaid search type :' + @SearchType;
		THROW 60000, @messsage, 1;
	END;
	
	