﻿

CREATE PROCEDURE [CRM].[spProductNumber_New]  	
AS
BEGIN
	
	SET NOCOUNT ON;

	IF EXISTS(SELECT ProductNumber FROM CRM.GetNewNumber)
	BEGIN
		UPDATE CRM.GetNewNumber SET ProductNumber = ISNULL(ProductNumber,0) + 1
		OUTPUT INSERTED.ProductNumber 
	END
	ELSE
	BEGIN
		INSERT INTO CRM.GetNewNumber (ProductNumber) 
		OUTPUT INSERTED.ProductNumber
		VALUES(1)
	END
END


