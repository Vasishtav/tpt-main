﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [CRM].[spAccountNumber_New]  
	@FranchiseId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF EXISTS(SELECT * FROM CRM.AccountNJobNumbers WHERE FranchiseId = @FranchiseId)
	BEGIN
		UPDATE CRM.AccountNJobNumbers SET AccountNumber = AccountNumber + 1
		OUTPUT INSERTED.AccountNumber 
		WHERE FranchiseId = @FranchiseId
	END
	ELSE
	BEGIN
		INSERT INTO CRM.AccountNJobNumbers (FranchiseId) 
		OUTPUT INSERTED.AccountNumber
		VALUES(@FranchiseId)
	END
END




