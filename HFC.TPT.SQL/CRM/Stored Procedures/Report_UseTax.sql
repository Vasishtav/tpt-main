﻿Create procedure CRM.Report_UseTax (
	 @FranchiseID int
	,@Year int
	,@Month int
	,@TerritoryID int
	,@Zip varchar(10)	)
as
begin

select		o.OrderNumber
		,	isnull(case	when ql.ProductTypeId = 1 then pc.ProductCategory -- Hprd.ProductGroupDesc
					when ql.ProductTypeID = 5 then isnull(pc.ProductCategory + '-' + psc.ProductCategory, '*One Time Product')
					else fpc.ProductCategory end ,'*Missing Category') as Product
		,	ad.ZipCode as ZipCode
 		,	cast(o.ContractedDate as Date) Dt
		,	case when ql.ProductTypeID not in (3) then ts.UseTaxPercentage else 0 end UseTaxPercentage
		,	sum(qld.Quantity * qld.BaseCost) COGS
		,	case when ql.ProductTypeID not in (3) then ts.UseTaxPercentage/100.0 else 0 end * sum(qld.Quantity * qld.BaseCost) UseTax
	from CRM.Opportunities opp
	join CRM.TaxSettings ts
		on	opp.FranchiseId = ts.FranchiseId
		and isnull(opp.TerritoryID, -1) = isnull(ts.TerritoryID, -1)
--	join CRM.Accounts a ON opp.AccountId = a.AccountId
--	join CRM.AccountCustomers acc on a.AccountId =acc.AccountId and acc.IsPrimaryCustomer=1
--	join CRM.Customer p ON p.CustomerId = acc.CustomerId 
--	join CRM.Person sa ON sa.PersonId = opp.SalesAgentId 
	join CRM.Quote q on opp.opportunityid = q.OpportunityId and q.PrimaryQuote=1
	join CRM.QuoteLines ql ON ql.QuoteKey = q.QuoteKey AND ql.ProductTypeId not in (4)
	join CRM.QuoteLineDetail qld on ql.QuoteLineId=qld.QuoteLineId
	join CRM.Orders o on q.QuoteKey=o.QuoteKey
--	left JOIN CRM.MasterPurchaseOrder mpo ON o.OrderId = mpo.OrderID
--	left JOIN CRM.PurchaseOrderDetails pod ON pod.PurchaseOrderId = mpo.PurchaseOrderId and pod.QuoteLineId = ql.QuoteLineId
-- 	left join CRM.ShipNotice sn ON sn.PurchaseOrdersDetailId = pod.PurchaseOrdersDetailId
-- 	left join CRM.Territories tr on tr.TerritoryId=opp.TerritoryId
-- 	left join CRM.HFCProducts Hprd ON Hprd.ProductID=ql.ProductId
 	left join CRM.Type_ProductCategory pc	ON ql.ProductCategoryId = pc.ProductCategoryId
 	left join CRM.Type_ProductCategory psc	ON ql.ProductSubCategoryId = psc.ProductCategoryId
 	left join CRM.FranchiseProducts fp ON fp.ProductKey = ql.ProductID and fp.FranchiseId = opp.FranchiseId
 	left join CRM.Type_ProductCategory fpc ON	 fp.ProductCategory = fpc.ProductCategoryId
	--left join Acct.HFCVendors v ON ql.VendorId = v.VendorID
	join CRM.Addresses ad on opp.InstallationAddressId=ad.AddressId
	where opp.FranchiseId = @FranchiseID
	and datepart(year, o.ContractedDate) = @Year
	and datepart(month, o.ContractedDate) = @Month
	and coalesce(@TerritoryID, opp.TerritoryID, -1) = coalesce(opp.TerritoryID, -1)
	and coalesce(@Zip, ad.ZipCode) = ad.ZipCode
	and ts.TaxType = 3
	and isnull(o.OrderStatus, 0) not in (6,9)
	and isnull(ql.ProductTypeID, 0) not in (4)
	group by o.OrderNumber
	,	isnull(case	when ql.ProductTypeId = 1 then pc.ProductCategory -- Hprd.ProductGroupDesc
					when ql.ProductTypeID = 5 then isnull(pc.ProductCategory + '-' + psc.ProductCategory, '*One Time Product')
					else fpc.ProductCategory end ,'*Missing Category')
	,	ad.ZipCode
	,	cast(o.ContractedDate as Date)
	,	ts.UseTaxPercentage
	,	ql.ProductTypeID

end

