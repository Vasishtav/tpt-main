﻿CREATE PROCEDURE [CRM].[OpportunitySearch]
(
@FranchiseId int
)
as
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
select 
case when op.TerritoryId is not null then tr.Name else op.TerritoryType end as Territory
,op.OpportunityId
,op.OpportunityName as OpportunityFullName
,opps.Name as  OpportunityStatus
,OrderTotal as OrderAmount
,op.OpportunityStatusId
,op.SalesAgentId
,op.InstallerId
,op.OpportunityNumber
,AccountId
,sa.FirstName+' '+sa.LastName SalesAgentName
,[CRM].[fnGetAccountName_forOpportunitySearch](op.OpportunityId) as AccountName
,CONVERT(date, ContractedDate) ContractDate
from [CRM].[Opportunities]  op
LEFT JOIN CRM.Territories tr on tr.TerritoryId=op.TerritoryId
LEFT JOIN CRM.Type_OpportunityStatus opps on opps.OpportunityStatusId=op.OpportunityStatusId
LEFT JOIN CRM.Orders o on o.OpportunityId=op.OpportunityId and o.OrderStatus !=9 and o.OrderStatus !=6
LEFT JOIN [CRM].[Person] sa on sa.PersonId=op.SalesAgentId
WHERE op.FranchiseId= @FranchiseId
order by op.OpportunityId desc
GO


