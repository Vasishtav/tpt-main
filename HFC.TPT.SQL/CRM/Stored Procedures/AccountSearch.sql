﻿CREATE PROCEDURE [CRM].[AccountSearch] 
(
@FranchiseId int,
@IncludeInactiveMerged bit
)
as
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

IF (@IncludeInactiveMerged = 0)
Begin

SELECT 
  case when Account.TerritoryId is not null then tr.Name else Account.TerritoryType end as Territory
     ,Account.[AccountId]
    ,Account.[PersonId]
    ,Account.[AccountNumber]
	,Account.[CreatedOnUtc]
	,lv.Name as AccountType
     ,Account.AccountTypeId 
    ,Account.[AccountStatusID]
    ,[Address1]
    ,[Address2]
    ,[City]
    ,[State]
    ,[ZipCode]
    ,p.[FirstName]
    ,p.[LastName]
    ,p.[PrimaryEmail]
    ,p.CompanyName
	,Account.IsCommercial
    ,tls.Name AS [Status]
	, Case When p.PreferredTFN  = 'C' then p.CellPhone
		When p.PreferredTFN  = 'W' then p.WorkPhone
		When p.PreferredTFN = 'H' then p.HomePhone
		ELSE '' 
	End as DisplayPhone
 FROM [crm].[Accounts] Account 
 INNER JOIN CRM.Type_AccountStatus tls ON tls.Accountstatusid = Account.Accountstatusid
 INNER JOIN CRM.AccountAddresses aad on aad.AccountId=Account.AccountId and aad.IsPrimaryAddress=1
 INNER JOIN [CRM].[Addresses] a  on a.[AddressId]=aad.AddressId
 INNER JOIN CRM.AccountCustomers acccus on Account.AccountId=acccus.AccountId  and IsPrimaryCustomer=1
 INNER JOIN [CRM].[Customer] p on p.CustomerId=acccus.CustomerId
 LEFT JOIN CRM.Territories tr on tr.TerritoryId=Account.TerritoryId
 LEFT JOIN CRM.Type_LookUpValues lv on Account.AccountTypeId=lv.Id
WHERE Account.FranchiseId= @FranchiseId 
	AND ISNULL(Account.IsDeleted,0)=0
	AND Account.AccountStatusId = 1 -- Include only active records. 
	order by isnull(Account.LastUpdatedOnUtc,Account.CreatedOnUtc) desc

End
Else
Begin

SELECT 
  case when Account.TerritoryId is not null then tr.Name else Account.TerritoryType end as Territory
     ,Account.[AccountId]
    ,Account.[PersonId]
    ,Account.[AccountNumber]
	,Account.[CreatedOnUtc]
	,lv.Name as AccountType
     ,Account.AccountTypeId 
    ,Account.[AccountStatusID]
    ,[Address1]
    ,[Address2]
    ,[City]
    ,[State]
    ,[ZipCode]
    ,p.[FirstName]
    ,p.[LastName]
    ,p.[PrimaryEmail]
    ,p.CompanyName
	,Account.IsCommercial
    ,tls.Name AS [Status]
	, Case When p.PreferredTFN  = 'C' then p.CellPhone
			When p.PreferredTFN  = 'W' then p.WorkPhone
			When p.PreferredTFN = 'H' then p.HomePhone
			ELSE '' 
		End as DisplayPhone
 FROM [crm].[Accounts] Account 
 INNER JOIN CRM.Type_AccountStatus tls ON tls.Accountstatusid = Account.Accountstatusid
 INNER JOIN CRM.AccountAddresses aad on aad.AccountId=Account.AccountId and aad.IsPrimaryAddress=1
 INNER JOIN [CRM].[Addresses] a  on a.[AddressId]=aad.AddressId
 INNER JOIN CRM.AccountCustomers acccus on Account.AccountId=acccus.AccountId  and IsPrimaryCustomer=1
 INNER JOIN [CRM].[Customer] p on p.CustomerId=acccus.CustomerId
 LEFT JOIN CRM.Territories tr on tr.TerritoryId=Account.TerritoryId
 LEFT JOIN CRM.Type_LookUpValues lv on Account.AccountTypeId=lv.Id
WHERE Account.FranchiseId= @FranchiseId AND ISNULL(Account.IsDeleted,0)=0 order by isnull(Account.LastUpdatedOnUtc,Account.CreatedOnUtc) desc

End
GO

