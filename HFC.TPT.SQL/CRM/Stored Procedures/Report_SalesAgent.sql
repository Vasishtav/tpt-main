﻿
--exec CRM.Report_SalesAgent 10390,'',''
Create procedure [CRM].[Report_SalesAgent] 
@FranchiseId int,
@startdate datetime null,
@enddate datetime null
as
begin
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
set nocount on

if(@startdate is null or @startdate='')
	set @startdate=GETDATE()-365

if(@enddate is null or @enddate='')
	set @enddate=GETDATE()

select PersonId,SalesAgent,opportunities,Quote,OpenQuote,Lostsales,Orders,
-- ClosingRate
isnull(CAST(Orders as float)/NULLIF(CAST(Opportunities as float),0),0.00) as ClosingRate,
-- QuotetoClose
isnull(CAST(Orders as float)/NULLIF(CAST(Quote as float),0),0.00) as QuotetoClose,
isnull(TotalOrders,0.00) as TotalOrders,
-- AvgOrder
isnull((CAST(TotalOrders as float)/NULLIF(CAST(Orders as float),0)),0.00) as AvgOrder
 from (

select au.PersonId, p.FirstName+' '+p.LastName as SalesAgent, 
-- opportunities
(select count(*) from CRM.Opportunities where SalesAgentId=au.PersonId and CreatedOnUtc between @startdate and @enddate) as opportunities,

-- Quote
(select count(*) from CRM.Quote q
join CRM.Opportunities op on q.OpportunityId=op.OpportunityId
where SalesAgentId=au.PersonId and q.PrimaryQuote=1 and CreatedOn between @startdate and @enddate) as Quote,
--Open Quote
(select count(*) from CRM.Quote q
join CRM.Opportunities op on q.OpportunityId=op.OpportunityId
where SalesAgentId=au.PersonId and q.PrimaryQuote=1 and q.QuoteStatusId not in (3,4) and op.OpportunityStatusId not in (6) and CreatedOn between @startdate and @enddate) as OpenQuote,
--Lost sales
(select count(*) from CRM.Quote q
join CRM.Opportunities op on q.OpportunityId=op.OpportunityId
where SalesAgentId=au.PersonId and q.PrimaryQuote=1 and op.OpportunityStatusId in (6) and CreatedOn between @startdate and @enddate) as Lostsales,
--Order
(select count(*) from CRM.Orders o
join CRM.Opportunities op on o.OpportunityId=op.OpportunityId
where SalesAgentId=au.PersonId and ContractedDate between @startdate and @enddate) as Orders,

(select sum(QuoteSubTotal)+SUM(Amount) from CRM.Orders o
join CRM.Opportunities op on o.OpportunityId=op.OpportunityId
join CRM.Quote q on o.QuoteKey=q.QuoteKey
join CRM.QuoteLines ql on q.QuoteKey=ql.QuoteKey
left join CRM.Tax T on ql.QuoteLineId=t.QuoteLineId
where SalesAgentId=au.PersonId  and ContractedDate between @startdate and @enddate
) as TotalOrders 
from [Auth].[Users] au 
join [Auth].[UsersInRoles] aur on au.UserId=aur.UserId
join CRM.Person p on au.PersonId=p.PersonId
where au.FranchiseId=@FranchiseId and RoleId='9CFE899F-A036-4D69-A58B-A1BFBDD8DCA0') as sales

end
