﻿
-- =============================================
-- Author:		Vasishta Veeramasuneni
-- Create date: 12/18/2017
-- Updated: 08/18/2019 BY Dan 
-- Description:	Get all quote lines based on QuoteId for both My Products and Vendor alliance Products
-- =============================================

CREATE PROCEDURE [CRM].[SPGetQuoteLines] @quoteId     INT, 
                                         @franchiseId INT
AS
    BEGIN

        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        DECLARE @brandid INT;
        SELECT @brandid = f.BrandId
        FROM crm.Franchise f
        WHERE f.FranchiseId = @franchiseId;
        SELECT ISNULL(CASE
                          WHEN QL.ProductTypeId NOT IN(1, 4)
                          THEN tpc.ProductCategory
                      END, '') AS PrintProductCategory,
               CASE
                   WHEN QL.ProductTypeId = 1
                        AND ISNULL(QL.ModelDescription, '') <> ''
                        AND ISNULL(QL.ModelDescription, '') = 'All'
                   THEN 'Model - ' + QL.ModelDescription + ', ' + QL.Description
                   ELSE ISNULL(QL.Description, '')
               END AS Description, 
               ISNULL(CASE
                          WHEN ql.ProductTYpeID IN(2, 3)
                          THEN fprd.ProductName
                          ELSE QL.ModelDescription
                      END, '') AS ModelDescription, 
               ISNULL(ql.FranctionalValueWidth, '0') AS FranctionalValueWidth, 
               ISNULL(ql.FranctionalValueHeight, '0') AS FranctionalValueHeight, 
               ql.quotelinenumber, 
               ISNULL(CASE
                          WHEN QL.ProductTypeId IN(2, 3)
                          THEN fprd.ProductName
                          WHEN QL.ProductTypeID = 4
                          THEN
        (
            SELECT fd.name
            FROM crm.FranchiseDiscount fd
            WHERE fd.DiscountId = QL.[ProductId]
        )
                          ELSE QL.ProductName
                      END, '') AS ProductName, 
               ISNULL(CASE
                          WHEN QL.ProductTypeId = 1
                          THEN hven.Name
                          ELSE QL.VendorName
                      END, '') AS VendorName, 
               Q.MeasurementId, 
               QL.[QuoteLineId], 
               QL.[QuoteKey], 
               QL.[MeasurementsetId], 
               QL.[VendorId], 
               QL.[ProductId], 
               QL.[SuggestedResale], 
               QL.[Discount], 
               QL.DiscountType, 
               QL.[CreatedOn], 
               QL.[CreatedBy], 
               QL.[LastUpdatedOn], 
               QL.[LastUpdatedBy], 
               QL.[IsActive], 
               QL.[Width], 
               QL.[Height], 
               QL.[UnitPrice], 
               QL.[Quantity], 
               QL.[Description], 
               QL.[MountType], 
               QL.[ExtendedPrice], 
               QL.[PICJson], 
               QL.[ProductTypeId], 
               QL.[Memo], 
               QL.RoomName, 
               QL.WindowLocation,
			   QL.ProductCategoryId, 
               QL.ProductSubCategoryId, 
               qld.BaseCost AS Cost, 
               QL.InternalNotes, 
               QL.VendorNotes, 
               QL.CustomerNotes, 
               qld.BaseResalePriceWOPromos, 
               QL.CancelReason,
               CASE
                   WHEN QL.ProductTypeID = 1
                        AND @brandid = 1
                        AND ISNULL(hven.BBDisplayName, '') <> ''
                   THEN hven.BBDisplayName
                   WHEN QL.ProductTypeID = 1
                        AND @brandid = 2
                        AND ISNULL(hven.TLDIsplayName, '') <> ''
                   THEN hven.TLDIsplayName
                   WHEN QL.ProductTypeID = 1
                        AND @brandid = 3
                        AND ISNULL(hven.CCDisplayName, '') <> ''
                   THEN hven.CCDisplayName
                   ELSE ''
               END AS VendorDisplayname,
               CASE
                   WHEN QL.ProductTypeID = 1
                   THEN hprd.TS_product
                   ELSE NULL
               END AS ProductUpdatedOn, 
               QL.ValidatedOn, 
               --ISNULL(CASE
               --           WHEN QL.ProductTypeID = 1
               --           THEN pod.Errors
               --           ELSE NULL
               --       END, '') AS ConfigErrors,
               CASE
                   WHEN QL.ProductTypeID = 1
                   THEN ql.Color
                   ELSE ''
               END AS Color,
               CASE
                   WHEN QL.ProductTypeID = 1
                   THEN ql.Fabric
                   ELSE ''
               END AS Fabric,
			   qld.CounterId,
			   qld.ValidConfiguration,
			   qld.ProductErrors,
			   qld.LastValidatedon,
			   QL.IsGroupDiscountApplied,
               		   QL.TaxExempt
			   --,
      --         CASE
      --             WHEN QL.ProductTypeID = 1
      --             THEN pod.PICPO
      --             ELSE ''
      --         END AS PICPO
        FROM [CRM].[QuoteLines] AS QL
             INNER JOIN crm.QuoteLineDetail qld ON qld.QuoteLineId = QL.QuoteLineId
             INNER JOIN CRM.Quote AS Q ON Q.QuoteKey = QL.[QuoteKey]
             INNER JOIN crm.Opportunities o ON o.OpportunityId = Q.OpportunityId
             LEFT JOIN [CRM].[HFCProducts] AS hprd ON hprd.ProductId = ql.ProductId
                                                      AND QL.ProductTypeId = 1
             --LEFT JOIN crm.PurchaseOrderDetails pod ON pod.QuoteLineId = QL.QuoteLineId
             --                                          AND QL.ProductTypeId = 1
             LEFT JOIN Acct.FranchiseVendors AS fven ON fven.VendorId = ql.VendorId
                                                        AND fven.FranchiseId = o.FranchiseId
                                                        AND QL.ProductTypeId IN(2, 3)
             LEFT JOIN ACCT.HFCVendors AS hven ON(hven.VendorId = ql.VendorID
                                                  AND QL.ProductTypeId = 1)
                                                 OR (hven.VendorId = fven.VendorId
                                                     AND QL.ProductTypeId IN(2, 3))
             LEFT JOIN [CRM].[FranchiseProducts] AS fprd ON fprd.ProductKey = ql.ProductId
                                                            AND fprd.FranchiseId = o.FranchiseId
                                                            AND QL.ProductTypeId IN(2, 3)
             LEFT JOIN CRM.Type_ProductCategory tpc ON(tpc.ProductCategoryId = fprd.ProductCategory
                                                       AND QL.ProductTypeId IN(2, 3))
                                                      OR (tpc.ProductCategoryId = QL.ProductSubCategoryId
                                                          AND QL.ProductTypeId NOT IN(1, 2, 3, 4))
        WHERE ql.IsActive = 1
              AND QL.[QuoteKey] = @quoteId
              AND o.FranchiseId = @franchiseId
        ORDER BY ql.QuoteLineNumber;
    END;