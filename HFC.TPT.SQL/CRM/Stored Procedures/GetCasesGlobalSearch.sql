﻿CREATE PROC [CRM].[GetCasesGlobalSearch]
( @FranchiseId int
	,@SearchType varchar(50)
	, @Value varchar(500))

AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
set @Value = '%'+ @Value + '%';

IF (@SearchType = 'id' OR @SearchType = 'all')
	BEGIN
		select fc.CaseId, fc.CaseNumber, ql.QuoteLineNumber CaseLineNumber
        , mpo.MasterPONumber mpo, mpo.PICPO vpo, ql.VendorId, ql.VendorName
        , a.AccountId, CONCAT(cus.firstname, ' ', cus.lastname) AccountName
        , fci.IssueDescription Description --, fci.Status
        , lvstatus.Name Status
        , fc.CreatedOn DateOpened, pod.PICPO, mpo.PurchaseOrderId
        , fci.VendorCaseNumber
        from crm.FranchiseCase fc
        join crm.Orders o on o.OrderID = fc.SalesOrderId
        join crm.franchisecaseaddinfo  fci on fci.CaseId = fc.caseId 
        join crm.PurchaseOrderDetails pod on pod.QuoteLineId = fci.QuoteLineId
        join crm.QuoteLines ql on ql.QuoteLineId = pod.QuoteLineId
        join crm.MasterPurchaseOrder mpo on mpo.PurchaseOrderId = fc.MPO_MasterPONum_POId
		left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
        join crm.Accounts a on a.AccountId = mpox.AccountId --mpo.AccountId
        join crm.AccountCustomers ac on ac.AccountId = a.AccountId and ac.IsPrimaryCustomer = 1
        join crm.Customer cus on cus.CustomerId = ac.CustomerId 
        left join crm.Type_LookUpValues lvstatus on lvstatus.Id = fci.Status
        where a.FranchiseId = @FranchiseId 
            and fc.Status <> 7006
	        and (fc.CaseNumber like @Value
                or mpo.MasterPONumber like @Value 
				or pod.PICPO like @Value)
	END
ELSE 
	BEGIN
		declare @messsage varchar(500) = 'Invaid search type :' + @SearchType;
		THROW 60000, @messsage, 1;
	END;
	
	