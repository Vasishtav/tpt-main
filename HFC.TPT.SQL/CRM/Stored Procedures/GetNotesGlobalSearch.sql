﻿CREATE PROC [CRM].[GetNotesGlobalSearch]
( @FranchiseId int
	,@SearchType varchar(50)
	, @Value varchar(500))

AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
set @Value = '%'+ @Value + '%';

IF (@SearchType = 'name' OR @SearchType = 'all')
	BEGIN
		select stuff((select ',' + cast(nc.typeenum as varchar)
        from crm.notescategories nc
        where nc.noteid = n.noteid
        for xml path('')), 1, 1, '' ) Categories
                ,n.noteid NoteId,  case when n.FranchiseId is not null then n.FranchiseId
			    when n.LeadId is not null then l.FranchiseId
			    when n.AccountId is not null then a.FranchiseId
			    when n.OpportunityId is not null then opp.FranchiseId
			    when n.OrderId is not null then opp2.FranchiseId
		    end FranchiseIdrefer, n.FranchiseId
		, case when n.IsNote = 1 then 'Note'
        else 'Attachment' 
        end Type, ft.name FileName, n.LeadId, n.AccountId, n.OpportunityId, n.QuoteId, n.OrderId
        , n.Title , Concat(cus.FirstName, ' ', cus.LastName) Account
        , opp.OpportunityName Opportunity, ord.OrderName [Order], n.CreatedOn ,n.AttachmentStreamId  
        from crm.Note n
		Left join FileTableTb ft on ft.stream_id = n.AttachmentStreamId
		Left join crm.Leads l on l.LeadId = n.LeadId 
		left join crm.Accounts a on a.AccountId = n.AccountId
		left join crm.AccountCustomers ac on ac.AccountId = n.AccountId and ac.IsPrimaryCustomer = 1
		left join crm.Customer cus on cus.CustomerId = ac.CustomerId 
		left join crm.Opportunities opp on opp.OpportunityId = n.OpportunityId 
		left join crm.Orders ord on ord.OrderID = n.OrderId 
		left join crm.Quote q on q.QuoteKey = ord.QuoteKey
		left join crm.Opportunities opp2 on opp2.OpportunityId = q.OpportunityId
		where isnull(n.IsDeleted, 0) = 0
			and (n.FranchiseId = @FranchiseId 
				or l.FranchiseId = @FranchiseId 
				or a.FranchiseId = @FranchiseId 
				or opp.FranchiseId = @FranchiseId 
				or opp2.FranchiseId = @FranchiseId)
			and (n.Title like @Value 
				or ft.name like @Value)
		order by n.CreatedOn Desc
	END
ELSE 
	BEGIN
		declare @messsage varchar(500) = 'Invaid search type :' + @SearchType;
		THROW 60000, @messsage, 1;
	END;
	
	