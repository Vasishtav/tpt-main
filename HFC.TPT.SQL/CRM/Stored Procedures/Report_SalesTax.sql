﻿
CREATE procedure [CRM].[Report_SalesTax]
@FranchiseId int,
@PersonId varchar(max)=null,
@StartDate datetime=null,
@EndDate datetime=null
as
Begin

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

select
	 ad.ZipCode
	,o.OrderNumber
	,o.OrderID
	,td.JurisType
	,td.Jurisdiction
	,t.Code
	,sum(td.TaxableAmount) TaxableAmount 
	,td.Rate
	,sum(td.Amount) Amount
from CRM.TaxDetails td
join CRM.Tax t on td.TaxId=t.TaxId
join CRM.QuoteLines ql on t.QuoteLineId=ql.QuoteLineId
join CRM.Quote q on ql.QuoteKey=q.QuoteKey
join CRM.Orders o on q.QuoteKey = o.QuoteKey
join CRM.Opportunities op on q.OpportunityId=op.OpportunityId
join CRM.Addresses ad on op.InstallationAddressId=AddressId
where op.FranchiseId=@FranchiseId
and (@PersonID is null or op.SalesAgentId in (select id from [CRM].[CSVToTable] (@PersonId)))
and cast(t.CreatedOn as Date) between @StartDate and @EndDate
and o.OrderStatus not in (6,9)
group by
	 ad.ZipCode
	,o.OrderNumber
	,o.OrderID
	,td.JurisType
	,td.Jurisdiction
	,t.Code
	,td.Rate
order by
	 ad.ZipCode
	,o.OrderNumber
	,td.JurisType
	,td.Jurisdiction
	,t.Code
	,td.Rate


end

