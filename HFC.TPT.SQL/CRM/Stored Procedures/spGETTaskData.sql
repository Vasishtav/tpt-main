﻿ -- =============================================
-- Author:		Rajkumar
-- Create date: 05/14/2019
-- Description:	Get Task records based on PersonId, Startdate and EndDate
-- =============================================
 CREATE PROCEDURE [CRM].[spGETTaskData]
@FranchiseId int,
@TaskId int,
@PersonIds varchar(4000)=null,
@StartDate datetime=null,
@EndDate datetime=null
AS
BEGIN

SET NOCOUNT ON;

if(@TaskId>0)
begin
	SELECT top 1 etp.[PersonId] AS [PersonId], T.*
	 FROM  [CRM].[Tasks] AS T
	 INNER JOIN [CRM].[EventToPeople] AS etp ON T.[TaskId] = etp.[TaskId]
	 WHERE T.FranchiseId=@FranchiseId and T.[IsDeleted]=0  and t.TaskId=@TaskId
end
else
begin
	SELECT etp.[PersonId] AS [PersonId], T.*
	 FROM  [CRM].[Tasks] AS T
	 INNER JOIN [CRM].[EventToPeople] AS etp ON T.[TaskId] = etp.[TaskId]
	 WHERE T.FranchiseId=@FranchiseId and T.[IsDeleted]=0  and (etp.[PersonId] IN (select * from CRM.CSVToTable(@PersonIds))) 
	 and t.DueDate between @StartDate and @EndDate
end

 
 
 END