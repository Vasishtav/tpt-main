﻿
create PROCEDURE [CRM].[spMLSReportForExcel]
(
 @fromDT datetime,  
 @toDT datetime,
 @terrCodeList	varchar(50)
)
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

            --declare @fromDT datetime 
            --declare     @toDT datetime
            --declare @terrCodeList nvarchar(max)
            --SET @fromDT = CONVERT(datetime, '2015-01-09' , 102)
            --SET @toDT = CONVERT(datetime, '2015-01-15'+ ' 23:59:59', 102)
            --SET @terrCodeList = '3683'


		select	 q.TerrCode,q.TerrName,q.LeadId, q.JobId, q.ContractedDate,	q.JobNumber,q.QuoteId,	q.CustomerName,  q.ZipCode, q.Taxtotal,
				'Product' as [ItemType], ji.ProductType as [ItemName], ji.[ProductName] as [ItemDetail], ji.[Quantity],
				ji.[IsTaxable],ji.[SalePrice],ji.[UnitCost],ji.[CostSubtotal] as [ItemCostSubtotal],ji.[Subtotal] as [ItemSubtotal],
				(CASE ji.IsTaxable WHEN 0 THEN 0 WHEN 1 THEN q.TaxPercent END) as [ItemTaxPercent],
				(CASE ji.IsTaxable WHEN 0 THEN 0 WHEN 1 THEN ji.[Subtotal]*q.TaxPercent/100 END) as [ItemTaxAmount] ,NULL as [TaxRateTotal]
		from	[CRM].[fntReports_JobQuotesFor](@fromDT,@toDT,@terrCodeList) q
		join	[CRM].[JobItems] AS ji on ji.QuoteId=q.QuoteId

		union all

		select	 q.TerrCode,q.TerrName,q.LeadId, q.JobId, q.ContractedDate,	q.JobNumber,q.QuoteId,	q.CustomerName,  q.ZipCode, q.Taxtotal,
				jsa.[Category] as [ItemType],jsa.Category as [ItemName],jsa.[Memo] as [ItemDetail], null as [Quantity],
				jsa.[IsTaxable],
				case 
				when jsa.Category in ('Discount', 'Coupon', 'Credit', 'Gift Certificate') then jsa.Amount * -1 				
				else jsa.Amount end as [SalePrice],
				null as [UnitCost] ,
				null as [ItemCostSubtotal],
				case 
				 when jsa.Category in ('Discount', 'Coupon', 'Credit', 'Gift Certificate') then jsa.Amount * -1 else jsa.Amount end as [ItemSubtotal],
				(CASE jsa.IsTaxable WHEN 0 THEN 0 WHEN 1 THEN q.TaxPercent END)as [ItemTaxPercent],
				(CASE jsa.IsTaxable WHEN 0 THEN 0 WHEN 1 THEN (jsa.Amount*q.TaxPercent)/100 END) as [ItemTaxAmount] ,NULL as [TaxRateTotal]
		from	[CRM].[fntReports_JobQuotesFor](@fromDT,@toDT,@terrCodeList) q
		join	[CRM].[JobSaleAdjustments] AS jsa on jsa.QuoteId=q.QuoteId 
		where	jsa.TypeEnum!=2 and jsa.Amount > 0
		
		union all

		
		select	 q.TerrCode,q.TerrName,q.LeadId, q.JobId, q.ContractedDate,	q.JobNumber,q.QuoteId,	q.CustomerName,  q.ZipCode, q.Taxtotal,
				jsa.[Category] as [ItemType],jsa.Category as [ItemName],jsa.[Memo] as [ItemDetail], null as [Quantity],
				jsa.[IsTaxable],
				case  
				when jsa.Category in ('Discount', 'Coupon', 'Credit', 'Gift Certificate') then((jsa.[Percent]* q.Subtotal)/100) * -1 else (jsa.[Percent]* q.Subtotal)/100 end as [SalePrice], 
				null as [UnitCost] ,null as [ItemCostSubtotal],
				case 
				when jsa.Category in ('Discount', 'Coupon', 'Credit', 'Gift Certificate') then((jsa.[Percent]* q.Subtotal)/100) * -1 else (jsa.[Percent]* q.Subtotal)/100 end as [ItemSubtotal],
				(CASE jsa.IsTaxable WHEN 0 THEN 0 WHEN 1 THEN q.TaxPercent END)as [ItemTaxPercent],
				(CASE jsa.IsTaxable WHEN 0 THEN 0 WHEN 1 THEN (jsa.Amount*q.TaxPercent)/100 END) as [ItemTaxAmount] ,NULL as [TaxRateTotal]
		from	[CRM].[fntReports_JobQuotesFor](@fromDT,@toDT,@terrCodeList) q
		join	[CRM].[JobSaleAdjustments] AS jsa on jsa.QuoteId=q.QuoteId 
		where	jsa.TypeEnum!=2 and jsa.[Percent] > 0 and q.Subtotal > 0
		
		union all

		select	 q.TerrCode,q.TerrName,q.LeadId, q.JobId, q.ContractedDate,	q.JobNumber,q.QuoteId,	q.CustomerName,  q.ZipCode, q.Taxtotal,
				jsa.[Category] as [ItemType],jsa.Category as [ItemName],jsa.[Memo] as [ItemDetail], null as [Quantity],
				jsa.[IsTaxable],null as [SalePrice],null as [UnitCost] ,null as [ItemCostSubtotal],null  as [ItemSubtotal],
				jsa.[Percent] as [ItemTaxPercent], null as [ItemTaxAmount],
			    (CASE q.Taxtotal WHEN 0 THEN 0 ELSE q.Taxtotal* (jsa.[Percent]/case when q.TaxPercent = 0 then 1 else q.TaxPercent end) END) as [TaxRateTotal]

		from	[CRM].[fntReports_JobQuotesFor](@fromDT,@toDT,@terrCodeList) q
		join	[CRM].[JobSaleAdjustments] AS jsa on jsa.QuoteId=q.QuoteId 
		where	jsa.TypeEnum=2 
		order	by q.quoteid, [itemType]

END
