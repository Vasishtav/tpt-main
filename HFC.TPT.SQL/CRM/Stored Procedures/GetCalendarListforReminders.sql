﻿
-- =============================================
-- Author:		Vasishta Veeramasuneni
-- Create date: 11/16/2017
-- Description:	Get all Appointments for email eminders

-- =============================================
CREATE PROCEDURE  [CRM].[GetCalendarListforReminders]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
SELECT * FROM CRM.Calendar c
WHERE  IsNULL(IsDeleted,0)  <> 1 AND IsNULL(IsCancelled,0) <> 1 AND AptTypeEnum IN (1) AND 
Startdate between dateadd(MINUTE, 1439, GETUTCDATE()) and dateadd(MINUTE, 1499, GETUTCDATE())

SELECT etp.* FROM crm.EventToPeople etp
INNER JOIN crm.Calendar c ON c.calendarid=etp.CalendarId 
WHERE IsNULL(IsDeleted,0)  <> 1 AND IsNULL(IsCancelled,0) <> 1 AND AptTypeEnum IN (1)
AND Startdate between dateadd(MINUTE, 1439, GETUTCDATE()) and dateadd(MINUTE, 1499, GETUTCDATE())


END
