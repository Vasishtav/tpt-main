﻿-- =============================================
-- Author:		Vasishta Veeramasuneni
-- Create date: 12/18/2017
-- Description:	Get Currency Conversion rates
-- =============================================

CREATE PROCEDURE CRM.SPGetCurrencyConversion 
				 @FranchiseId int, @VendorId int
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @fromCurrency varchar(2), @toCurrency varchar(2);

	SELECT @toCurrency = frn.Currency
	FROM CRM.Franchise AS frn where frn.FranchiseId = @FranchiseId;

	SELECT @fromCurrency = tcc.ISOCode2Digits
	FROM acct.FranchiseVendors AS fv
		 INNER JOIN
		 [CRM].[Type_CountryCodes] AS tcc
		 ON fv.Currency = tcc.CountryCodeId 
		 where fv.VendorId = @VendorId and FV.FranchiseId=@FranchiseId;

	SELECT *
	FROM [CRM].[CurrencyExchange]
	WHERE CountryFrom = @fromCurrency AND 
		  CountryTo = @toCurrency;

END;
