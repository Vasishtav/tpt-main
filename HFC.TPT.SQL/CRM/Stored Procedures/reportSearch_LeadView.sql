﻿


create  PROC [CRM].[reportSearch_LeadView]
(
@FranchiseId int,
@SearchTerm varchar(300)=null,
@PageSize int=20,
@PageNumber int=0,
@SortOrder varchar(50) = 'CreatedOnUtc',
@SortDirection varchar(4) ='desc',
@leadStatusIds varchar(500) = null,
@installerPersonIds varchar(500) = null,
@salesPersonIds varchar(500) = null,
@jobStatusIds varchar(500) = null,
@InvoiceStatuses varchar(500) = null,
@SourceIds varchar(500)  = null,
@createdOnUtcStart datetime = null,
@createdOnUtcEnd datetime  = null,
@CommercialType int = 0
 )
as
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @Ids_LeadStatus TABLE (leadid int) ;
DECLARE @Ids_Source TABLE (leadid int) ;
DECLARE @Ids_Sales TABLE (leadid int) ;

	DECLARE @Ids TABLE (leadid int) ;

DECLARE @ValidLead table (id int);
DECLARE @BySource table (id int);
DECLARE @JobsWithSale table (id int);

IF @jobStatusIds IS NOT NULL --- Sales by Source
	BEGIN 
		INSERT INTO @JobsWithSale(id)
		SELECT DISTINCT ID  FROM [CRM].[Type_JobStatus] WHERE id IN (29,30,32) 
		INSERT INTO @JobsWithSale(id)
		SELECT DISTINCT [Id] FROM [CRM].[Type_JobStatus] ts WHERE 
		(ts.FranchiseId= @franchiseId or ts.FranchiseId is null) 
		and [ParentId] IN  (29,30,32) and ts.isDeleted != 1 

		INSERT INTO @Ids_Sales (leadid)
		SELECT DISTINCT l.leadid as id
		FROM CRM.Leads l 
		INNER JOIN jobs j
			on j.LeadId=l.LeadId
		INNER JOIN @JobsWithSale s
			on j.[JobStatusId]=s.id	
		INNER JOIN [dbo].[vw_CommercialLeads] as com 
			on com.leadId = l.LeadId
		WHERE l.FranchiseId =@franchiseId and
		cast(j.ContractedOnUtc as date) between @createdOnUtcStart and @createdOnUtcEnd
		and l.isDeleted=0
		and (com.CommercialType = @CommercialType or @CommercialType = 0)
	END

IF @leadStatusIds IS NOT NULL -- Valid Leads by Source
	BEGIN 
	PRINT ( 'Valid Leads by Source');	
		INSERT INTO @ValidLead(id)
		SELECT DISTINCT  ID  FROM [CRM].Type_LeadStatus WHERE id IN (3) 
		INSERT INTO @ValidLead(id)
		SELECT DISTINCT [Id] FROM [CRM].Type_LeadStatus ts WHERE 
		(ts.FranchiseId= @franchiseId or ts.FranchiseId is null) 
		and [ParentId] IN  (3)  and ts.isDeleted!=1 

		INSERT INTO @Ids_LeadStatus (leadid)
		SELECT DISTINCT l.leadid
		FROM CRM.Leads l 
		INNER JOIN jobs j
			on j.LeadId=l.LeadId
			and j.salesPersonId=ISNULL(CAST(@salesPersonIds AS INT),j.salesPersonId)
		LEFT OUTER JOIN [dbo].[vw_CommercialLeads] as com 
			on com.leadId = l.LeadId
		INNER JOIN @ValidLead s
			on l.leadStatusId=s.id
		WHERE
		 l.isDeleted=0 and
		 FranchiseId =@FranchiseId and
		cast(l.CreatedOnUtc as date) between @createdOnUtcStart and @createdOnUtcEnd
		and (com.CommercialType = @CommercialType or @CommercialType = 0)
	END	
IF @SourceIds IS NOT NULL ---- #Leads By Source, (MUST be not null called from Marketing Preformance report)
	BEGIN 
	PRINT ( '3 @JobsWithSale #Leads By Source, (MUST be not null called from Marketing Preformance report)');	
		INSERT INTO @BySource(id)
		SELECT cast(Value as int) as id   FROM dbo.SplitCommaToTable(@SourceIds)
		/**/ UNION ALL ---
		SELECT SourceId as [Id] FROM [CRM].[Sources]  s
		WHERE 
		(FranchiseId= @franchiseId or FranchiseId IS NULL) 
		and [ParentId] IN (SELECT Id from @BySource) AND [ParentId] IS NOT NULL;

		INSERT INTO @Ids_Source (leadid)
		SELECT DISTINCT l.leadid as id
		FROM CRM.Leads l 
		INNER JOIN crm.LeadSources ls
			on ls.leadid=l.leadid
		INNER JOIN @BySource s
			on ls.[SourceId] = s.id
		INNER JOIN [dbo].[vw_CommercialLeads] as com 
			on com.leadId = l.LeadId
		WHERE l.FranchiseId =@franchiseId AND
		cast(l.CreatedOnUtc as date) BETWEEN @createdOnUtcStart AND @createdOnUtcEnd
		AND isnull(l.isDeleted,0)=0
		AND (com.CommercialType = @CommercialType or @CommercialType = 0)

	END	
---------------------------------------
/* MARKETING PERFOMANCE REPORT < */
	IF @leadStatusIds IS NOT NULL AND @SourceIds IS NOT NULL --- Valid lead per Source
	BEGIN 
	print ('1 Valid lead per Source');


			INSERT INTO @Ids (leadid)
			SELECT DISTINCT l.leadid 
			FROM @Ids_LeadStatus l
			INNER JOIN @Ids_Source s
				on s.leadid=l.leadid
	END
	IF @jobStatusIds IS NOT NULL AND @SourceIds IS NOT NULL --- Sales by Source
	BEGIN
	print ('2 Sales by Source');
			INSERT INTO @Ids (leadid)
			SELECT DISTINCT l.leadid 
			from @Ids_Sales l
			INNER JOIN @Ids_Source s
				on s.leadid=l.leadid
	END

	IF @jobStatusIds IS NULL and @leadStatusIds IS NULL AND @SourceIds IS NOT NULL --- # Of Leads with no other filter
	BEGIN 
	print ('3 # OF LEADS WITH NO OTHER FILTER');
			INSERT INTO @Ids (leadid)
			SELECT DISTINCT l.leadid 
			FROM @Ids_Source l
END
/* MARKETING PERFOMANCE REPORT > */
---------------------------------------
-------ALL ELSE:
---------------------------------------
IF @jobStatusIds IS NULL AND @SourceIds IS NULL AND @leadStatusIds IS NOT NULL
	BEGIN 
	PRINT ('ALL ELSE: (marketing stats)');
		INSERT INTO @Ids (leadid)
		SELECT DISTINCT l.leadid 
		from @Ids_LeadStatus l
	END
--------------
if	@SortDirection = 'Asc'
BEGIN
;WITH Paged AS
(
	SELECT 
		   lead.[LeadId]
		  ,lead.[LeadGuid]
		  ,lead.[PersonId]
		  ,lead.[LeadNumber]
		  ,[LastUpdatedOnUtc]
		  ,lead.[Notes]
		  ,lead.[CreatedOnUtc]
		  ,[LeadStatusID]
		  ,[Address1]
		  ,[Address2]
		  ,[City]
		  ,[State]
		  ,[ZipCode]
		  ,[AddressesGuid]
		  ,a.[AddressId]
		  ,p.[WorkPhone]
		  ,p.[HomePhone]
		  ,p.[CellPhone]
		  ,p.[FirstName]
		  ,p.[LastName]
		  ,p.[PrimaryEmail]
		  ,p.PreferredTFN
		  ,p.WorkPhoneExt
		  ,p.CompanyName
		  ,p.[FaxPhone]
		  ,lead.SecPersonId
		  ,ROW_NUMBER() OVER (ORDER BY case when @SortOrder = 'CreatedOnUtc' then lead.CreatedOnUtc else null end,
		  case when @SortOrder = 'primperson' then p.[FirstName]  else '' end ,
		  case when @SortOrder ='addresses' then [Address1]  else '' end ,
		  case when @SortOrder ='leadStatusid' then [LeadStatusID]  else 0 end 
		    ) AS [Row]
	FROM crm.Leads lead 
	INNER JOIN @Ids s
		on s.leadid=lead.LeadId
		and lead.IsDeleted=0
	INNER JOIN [CRM].[Addresses] a
		on a.[AddressId]=(select top 1 la.AddressId from crm.LeadAddresses la where la.LeadId = lead.LeadId)
	INNER JOIN [CRM].[Person] p	
		on p.[PersonId]=lead.[PersonId]
	LEFT JOIN [CRM].[Person] secondPerson  
		on secondPerson.PersonId = lead.SecPersonId
	INNER JOIN [dbo].[vw_CommercialLeads] as com 
		on com.leadId = lead.LeadId and (com.CommercialType = @CommercialType or @CommercialType = 0)	
 )
 ,
 TotalCount as (
 SELECT COUNT(*) as TotalCount from Paged
 )

 SELECT 
		   [leadid]
		  ,[leadGuid]
		  ,[PersonId]
		  ,[LastUpdatedOnUtc]
		  ,[Notes]
		  ,[LeadNumber]
		  ,[CreatedOnUtc]
		  ,[LeadStatusID]
		  ,[Address1]
		  ,[Address2]
		  ,[City]
		  ,[State]
		  ,[ZipCode]
		  ,[AddressesGuid]
		  ,[AddressId]
		  ,[WorkPhone]
		  ,[HomePhone]
		  ,[CellPhone]
		  ,[FirstName]
		  ,[LastName]
		  ,[PrimaryEmail]
		  ,PreferredTFN
		  ,WorkPhoneExt
		  ,CompanyName
		  ,[FaxPhone]
		  ,SecPersonId
		  ,[Row]
		  ,t.TotalCount
		  FROM Paged c
		  ,TotalCount t
		  WHERE c.[Row] BETWEEN (@PageSize*@PageNumber)+1 AND (@PageSize*@PageNumber)+@PageSize
END
ELSE
BEGIN
;WITH Paged AS
(
	SELECT 
		   lead.[LeadId]
		  ,lead.[LeadGuid]
		  ,lead.[PersonId]
		  ,[LastUpdatedOnUtc]
		  ,lead.[Notes]
		  ,[LeadNumber]
		  ,lead.[CreatedOnUtc]
		  ,[LeadStatusID]
		  ,[Address1]
		  ,[Address2]
		  ,[City]
		  ,[State]
		  ,[ZipCode]
		  ,[AddressesGuid]
		  ,a.[AddressId]
		  ,p.[WorkPhone]
		  ,p.[HomePhone]
		  ,p.[CellPhone]
		  ,p.[FirstName]
		  ,p.[LastName]
		  ,p.[PrimaryEmail]
		  ,p.PreferredTFN
		  ,p.WorkPhoneExt
		  ,p.CompanyName
		  ,p.[FaxPhone]
		  ,lead.SecPersonId
		  ,ROW_NUMBER() OVER (ORDER BY case when @SortOrder = 'CreatedOnUtc' then lead.CreatedOnUtc else null end desc,
		  case when @SortOrder = 'primperson' then p.[FirstName]  else '' end desc,
		  case when @SortOrder ='addresses' then [Address1]  else '' end desc,
		  case when @SortOrder ='leadStatusid' then [LeadStatusID]  else 0 end desc
		    ) AS [Row]
	FROM crm.Leads lead 
	INNER JOIN @Ids s
		on s.leadid=lead.LeadId
	INNER JOIN [CRM].[Addresses] a
		on a.[AddressId]=(select top 1 la.AddressId from crm.LeadAddresses la where la.LeadId = lead.LeadId)
	INNER JOIN [CRM].[Person] p	
		on p.[PersonId]=lead.[PersonId]
	LEFT JOIN [CRM].[Person] secondPerson  
		on secondPerson.PersonId = lead.SecPersonId
	INNER JOIN [dbo].[vw_CommercialLeads] as com 
		on com.leadId = lead.LeadId and (com.CommercialType = @CommercialType or @CommercialType = 0)	
)
 ,
 TotalCount as (
 SELECT COUNT(*) as TotalCount from Paged
 )

 SELECT 
		   [leadid]
		  ,[leadGuid]
		  ,[PersonId]

		  ,[LastUpdatedOnUtc]
		  ,[Notes]
		  ,[LeadNumber]
		  ,[CreatedOnUtc]
		  ,[LeadStatusID]
		  ,[Address1]
		  ,[Address2]
		  ,[City]
		  ,[State]
		  ,[ZipCode]
		  ,[AddressesGuid]
		  ,[AddressId]
		  ,[WorkPhone]
		  ,[HomePhone]
		  ,[CellPhone]
		  ,[FirstName]
		  ,[LastName]
		  ,[PrimaryEmail]
		  ,PreferredTFN
		  ,WorkPhoneExt
		  ,CompanyName
	      ,[FaxPhone]
		  ,SecPersonId
		  ,[Row]
		   
		  ,t.TotalCount
		  
		  FROM Paged c
		  ,TotalCount t
		  WHERE c.[Row] BETWEEN (@PageSize*@PageNumber)+1 AND (@PageSize*@PageNumber)+@PageSize
end		  
		 
			






