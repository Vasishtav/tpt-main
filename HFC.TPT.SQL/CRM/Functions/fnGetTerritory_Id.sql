﻿CREATE FUNCTION CRM.fnGetTerritory_Id
( 
				@BrandId int, @zip varchar(25), @country varchar(25), @FranchiseId int
)
RETURNS varchar(500)
AS
BEGIN
	DECLARE @ret varchar(500), @terrnum varchar(25), @contrac varchar(25);

	IF @country = 'CA'

	BEGIN
		SET @zip = replace(@zip, ' ', '');
		IF EXISTS
(
	SELECT *
	FROM hfcgis.dbo.vwTPT_HFCGIS AS vw
	WHERE vw.ZIP = @zip AND 
		  vw.country = @country
)
		BEGIN
			SET @zip = @zip;
		END;
		ELSE
		BEGIN
			IF EXISTS
(
	SELECT *
	FROM hfcgis.dbo.vwTPT_HFCGIS AS vw
	WHERE vw.ZIP = SUBSTRING(@zip, 1, 3) AND 
		  vw.country = @country
)
			BEGIN
				SET @zip = SUBSTRING(@zip, 1, 3);
			END;
			ELSE
			BEGIN
				SET @zip = SUBSTRING(@zip, 1, 3);
			END;
		END;
	END;
	ELSE
	BEGIN
		SET @zip = SUBSTRING(@zip, 1, 5);
	END;




	IF NOT EXISTS
(
	SELECT *
	FROM hfcgis.dbo.vwTPT_HFCGIS AS vw
	WHERE vw.ZIP = @zip
)
	BEGIN
		SET @ret = NULL;
	END;
	ELSE
	BEGIN
		IF @BrandId = 1
		BEGIN
			SELECT @terrnum = vw.BBTERRNUM, @contrac = vw.BBICONTRAC
			FROM hfcgis.dbo.vwTPT_HFCGIS vw
			WHERE vw.ZIP = @zip;
		END;
		IF @BrandId = 2
		BEGIN
			SELECT @terrnum = vw.TLTERRNUM, @contrac = vw.TLCONTRAC
			FROM hfcgis.dbo.vwTPT_HFCGIS vw
			WHERE vw.ZIP = @zip;
		END;
		IF @BrandId = 3
		BEGIN
			SELECT @terrnum = vw.CCTERRNUM, @contrac = vw.CCCONTRAC
			FROM hfcgis.dbo.vwTPT_HFCGIS vw
			WHERE vw.ZIP = @zip;
		END;
	END;
--END


	IF @contrac = 'c'
	BEGIN
		SELECT @ret = t.TerritoryId
		FROM crm.Territories AS t
		WHERE t.BrandId = @BrandId AND 
			  t.Minion = @terrnum AND 
			  t.FranchiseId = @FranchiseId;
	END;
	ELSE
	BEGIN
		SET @ret = NULL;

	END;


	IF @ret IS NULL
	BEGIN
		SET @ret = NULL;
	END;
	RETURN @ret;
END; 