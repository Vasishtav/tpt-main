﻿-- =============================================
-- Author:		Quan Tran
-- Create date: 3/12/2014
-- Description:	query for summary report by category
-- =============================================
CREATE FUNCTION [CRM].[fntReports_ProductSummary] 
(	
	@startDate		date,
	@endDate		date,	
	@franchiseId	INT = null
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT ji.ProductGuid GuidId, MONTH(j.ContractedOnUtc) [Month], YEAR(j.ContractedOnUtc) [Year], DAY(j.ContractedOnUtc) [Day], ISNULL(ji.ProductName, 'Unknown') [Key], SUM(ji.Subtotal) [Sum]
	FROM CRM.Jobs j  with (nolock)
		JOIN CRM.JobQuotes jq with(nolock) on jq.JobId = j.JobId and jq.IsPrimary = 1			
		JOIN CRM.JobItems ji with(nolock) on jq.QuoteId = ji.QuoteId
		JOIN CRM.Leads l with(nolock) on j.LeadId = l.LeadId
	WHERE l.IsDeleted = 0 AND j.IsDeleted = 0 AND 
		(l.FranchiseId = @franchiseId OR @franchiseId IS NULL) AND
		CAST(j.ContractedOnUtc as date) BETWEEN @startDate AND @endDate
	GROUP BY MONTH(j.ContractedOnUtc), YEAR(j.ContractedOnUtc), DAY(j.ContractedOnUtc), ji.ProductGuid, ji.ProductName
)



