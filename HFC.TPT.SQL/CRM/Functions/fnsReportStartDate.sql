﻿
CREATE FUNCTION [CRM].[fnsReportStartDate]
(
	@FranchiseId INT
)
RETURNS DATETIME
AS
BEGIN
	DECLARE @startDate DateTime
	SELECT @startDate = MIN(CreatedOnUtc) from CRM.Leads with (nolock)
	where FranchiseId = @FranchiseId AND IsDeleted = 0
	RETURN ISNULL(@startDate, GETDATE())
END


