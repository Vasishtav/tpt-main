﻿
CREATE FUNCTION [CRM].[fntReports_JobStatusSales] 
(	
	@startDate		date,
	@endDate		date,	
	@franchiseId	INT = null,
	@statusId		INT = null
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT 
		j.LeadId, 
		j.JobId, 
		j.JobNumber, 
		j.JobStatusId,
		CASE p.Name WHEN 'service' THEN p.Name + ' - ' +  tj.Name ELSE isnull(p.Name, tj.Name) END as JobStatus,   	
		j.JobStatusId KeyId,
		ISNULL(ISNULL(parent.Name + ' - ', '') + src.Name, 'Unassigned') [KeyName], 
		s.FirstName, 
		s.LastName, 
		jq.Subtotal, 
		jq.SurchargeTotal, 
		ISNULL(jq.DiscountTotal-[CRM].[fnsDiscountAmountForMSR](jq.QuoteId),0) DiscountTotal, 
		j.contractedonutc, 
		jq.createdonutc, 
		apt.FirstAppointment		
	FROM CRM.Jobs j with(nolock)
		JOIN CRM.JobQuotes jq with(nolock) on jq.JobId = j.JobId and jq.IsPrimary = 1			
		JOIN CRM.Person s with(nolock) on j.CustomerPersonId = s.PersonId	
		JOIN CRM.Leads l with(nolock) on j.LeadId = l.LeadId
		JOIN CRM.Type_JobStatus src with(nolock) on src.Id = j.JobStatusId
		LEFT JOIN CRM.Type_JobStatus parent with(nolock) on parent.Id = src.ParentId
		join CRM.Type_JobStatus tj with(nolock) ON tj.Id =  j.JobStatusId		
		left join CRM.Type_JobStatus p with(nolock) on tj.ParentId = p.Id	
		OUTER APPLY (
			select TOP 1 StartDate FirstAppointment
			from CRM.Calendar WITH(NOLOCK)
			where AptTypeEnum = 1 AND JobId = j.JobId
			Order by StartDate
		) apt
	WHERE l.IsDeleted = 0 AND j.IsDeleted = 0 AND 		
		(l.FranchiseId = @franchiseId OR @franchiseId IS NULL) AND		
		(j.JobStatusId = @statusId OR @statusId IS NULL) AND
		(
			CAST(j.ContractedOnUtc as date) BETWEEN @startDate AND @endDate
			OR
			(j.ContractedOnUtc IS NULL AND CAST(jq.CreatedOnUtc as date) BETWEEN @startDate and @endDate)
		)
)