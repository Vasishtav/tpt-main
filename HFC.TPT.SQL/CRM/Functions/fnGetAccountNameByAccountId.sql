﻿
CREATE FUNCTION CRM.fnGetAccountNameByAccountId
(
  @AccountId int)
RETURNS varchar(500)
AS
-- Returns the stock level for the product.  
BEGIN
DECLARE
  @ret varchar(500);
SELECT @ret=CASE
              WHEN ac.IsCommercial=1
              THEN CASE
                     WHEN c.CompanyName IS NOT NULL
                          AND c.CompanyName!=''
                     THEN ISNULL(c.CompanyName,'')+' / '
                     ELSE ''
                   END+ISNULL(c.FirstName,'')+' '+ISNULL(c.LastName,'')
              ELSE ISNULL(c.FirstName,'')+' '+ISNULL(c.LastName,'')+CASE
                                                WHEN c.CompanyName IS NOT NULL
                                                     AND c.CompanyName!=''
                                                THEN ' / '+ISNULL(c.CompanyName,'')
                                                ELSE ''
                                              END
            END
FROM CRM.Accounts ac
JOIN CRM.AccountCustomers acccus on ac.AccountId=acccus.AccountId and acccus.IsPrimaryCustomer=1
JOIN CRM.Customer c ON acccus.CustomerId=c.CustomerId
WHERE ac.accountid=@AccountId;
if @ret is null 
BEGIN
set @ret=''
END
RETURN @ret;
END; 




