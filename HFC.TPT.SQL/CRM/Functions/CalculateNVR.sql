﻿CREATE FUNCTION CRM.CalculateNVR
(@quoteKey INT
)
RETURNS DECIMAL(18, 10)
AS
     BEGIN
         DECLARE @lineDiscountSum DECIMAL(10, 2), @linediscountPercent DECIMAL(10, 8), @productTotal DECIMAL(10, 2), @qpercent DECIMAL(10, 8), @nvr DECIMAL(18, 10);
         SET @lineDiscountSum = 0;
         SET @linediscountPercent = 0;
         SET @productTotal = 0;
         SET @qpercent = 1;
         --SET @nvr = 1;
         --Calculate the discount totals of quotelines
         SELECT @lineDiscountSum = SUM(discount)
         FROM crm.QuoteLines ql
         WHERE ql.quotekey = @quoteKey
               AND ql.ProductTypeId = 4
               AND ql.discounttype = '$'
         GROUP BY ProductTypeId;
         SELECT @lineDiscountSum = @lineDiscountSum + q.Discount
         FROM crm.Quote q
         WHERE q.QuoteKey = @quoteKey
               AND q.DiscountType = '$';

         --Calculating all Product Totals using core/my/one-time Products
         SELECT @productTotal = SUM(ql.ExtendedPrice)
         FROM crm.QuoteLines ql
         WHERE ql.quotekey = @quoteKey
               AND ql.ProductTypeId IN(1, 2, 5);
         --GROUP BY ProductTypeId;
         IF(@productTotal <> 0)
             BEGIN
                 SET @qpercent = (1 - (@lineDiscountSum / @productTotal));
         END;

         --Discount Percentages at line level
         IF NOT EXISTS
         (
             SELECT 1
             FROM crm.QuoteLines ql
             WHERE ql.QuoteKey = @quoteKey
         )
             BEGIN
                 SELECT @nvr = CAST(1.000 AS DECIMAL(18, 10));
         END;
             ELSE
             BEGIN
                 SELECT @nvr = CASE
                                   WHEN ROUND(EXP(SUM(LOG(DiscPercentage))), 10) IS NULL
                                   THEN CAST(1.000 AS DECIMAL(18, 10))
                                   ELSE ROUND(EXP(SUM(LOG(DiscPercentage))), 10)
                               END
                 FROM
                 (
                     SELECT *
                     FROM
                     (
                         SELECT ABS(1 - (ql.Discount / 100)) AS DiscPercentage
                         FROM crm.QuoteLines ql
                         WHERE ql.quotekey = @quoteKey
                               AND ql.ProductTypeId = 4
                               AND ql.discounttype = '%'
                         UNION ALL
                         SELECT ABS(100 - q.Discount) / 100 AS DiscPercentage
                         FROM crm.Quote q
                         WHERE q.QuoteKey = @quoteKey
                               AND q.DiscountType = '%'
                         UNION ALL
                         SELECT ABS(@qpercent)
                     ) AS x
                     WHERE ISNULL(x.DiscPercentage, 0) <> 0
                           AND LOG(DiscPercentage) <> 0
                 ) AS y;
         END;
         IF EXISTS
         (
             SELECT 1
             FROM
             (
                 SELECT *
                 FROM
                 (
                     SELECT(1 - (ql.Discount / 100)) AS DiscPercentage
                     FROM crm.QuoteLines ql
                     WHERE ql.quotekey = @quoteKey
                           AND ql.ProductTypeId = 4
                           AND ql.discounttype = '%'
                     UNION ALL
                     SELECT((100 - q.Discount) / 100) AS DiscPercentage
                     FROM crm.Quote q
                     WHERE q.QuoteKey = @quoteKey
                           AND q.DiscountType = '%'
                     UNION ALL
                     SELECT(@qpercent)
                 ) AS x
                 WHERE ISNULL(x.DiscPercentage, 0) <> 0
                       AND DiscPercentage < 0
             ) AS y
         )
             BEGIN
                 SET @nvr = (@nvr * -1);
         END;
         RETURN @nvr;
     END;