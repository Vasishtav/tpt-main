﻿
CREATE FUNCTION CRM.fnGetTerritoryDisplayName
( 
				@BrandId int, @territoryId int
)
RETURNS varchar(500)
AS
BEGIN
	DECLARE @ret varchar(500), @minion varchar(25), @contrac varchar(25);

	SELECT @minion = t.Minion FROM crm.Territories t WHERE t.TerritoryId=@territoryId

	IF LTRIM(RTRIM(@minion))<>''
	BEGIN
			IF @BrandId = 1
		BEGIN
			SELECT @ret = vw.BB_TerrName, @contrac = vw.BBICONTRAC
			FROM hfcgis.dbo.vwTPT_HFCGIS vw
			WHERE vw.BB_MinionCode = @minion;
		END;
		IF @BrandId = 2
		BEGIN
			SELECT @ret = vw.TL_TerrName, @contrac = vw.TLCONTRAC
			FROM hfcgis.dbo.vwTPT_HFCGIS vw
			WHERE vw.TL_MinionCode = @minion;
		END;
		IF @BrandId = 3
		BEGIN
			SELECT @ret = vw.CC_TerrName, @contrac = vw.CCCONTRAC
			FROM hfcgis.dbo.vwTPT_HFCGIS vw
			WHERE vw.CC_MinionCode = @minion;
		END;
END


	IF @ret IS NULL
	BEGIN
		SET @ret = '';
	END;
	RETURN @ret;
END; 



