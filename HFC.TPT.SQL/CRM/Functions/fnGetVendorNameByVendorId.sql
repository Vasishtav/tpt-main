﻿create FUNCTION [CRM].[fnGetVendorNameByVendorId]
(
  @VendorId int,
  @FranchiseId int)
RETURNS varchar(500)
AS
-- Returns the stock level for the product.  
BEGIN
DECLARE
  @ret varchar(500);

SELECT @ret = CASE WHEN fv.VendorType = 3 THEN fv.Name
	Else hv.Name
	End  
from acct.FranchiseVendors fv
left join acct.HFCVendors hv on hv.VendorId = fv.VendorId
where fv.VendorId = @VendorId and fv.FranchiseId = FranchiseId

if @ret is null 
BEGIN
	set @ret=''
END

RETURN @ret;
END;