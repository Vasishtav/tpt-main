﻿-- =============================================
-- Author:		Quan Tran
-- Create date: 3/12/2014
-- Description:	query for summary report by sales agent
-- =============================================
CREATE FUNCTION [CRM].[fntReports_SalesAgentSummary] 
(	
	@startDate		date,
	@endDate		date,	
	@franchiseId	INT = null
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT p.PersonId Id, MONTH(j.ContractedOnUtc) [Month], YEAR(j.ContractedOnUtc) [Year], Day(j.ContractedOnUtc) [Day], ISNULL(p.FirstName + ' ' + p.LastName, 'Unassigned') [Key], 
		SUM(jq.Subtotal) [Sum]
	FROM CRM.Jobs j  with (nolock)
		JOIN CRM.JobQuotes jq with(nolock) on jq.JobId = j.JobId and jq.IsPrimary = 1
		JOIN CRM.Leads l with(nolock) on j.LeadId = l.LeadId
		LEFT JOIN CRM.Person p with(nolock) on j.SalesPersonId = p.PersonId
	WHERE l.IsDeleted = 0 AND j.IsDeleted = 0 AND 
		(l.FranchiseId = @franchiseId OR @franchiseId IS NULL) AND
		CAST(j.ContractedOnUtc as date) BETWEEN @startDate AND @endDate
	GROUP BY MONTH(j.ContractedOnUtc), YEAR(j.ContractedOnUtc), Day(j.ContractedOnUtc), p.PersonId, p.FirstName, p.LastName	
)
