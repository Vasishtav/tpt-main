﻿
create FUNCTION [CRM].[fnGetMeasurement_forAccountSearch]  (@AccountId int)
RETURNS VARCHAR(250)
AS BEGIN
    DECLARE @Opportunity varchar(250)
	DECLARE @Measurments varchar(250)
	set @Opportunity=(select top 1 op.OpportunityId from crm.Opportunities op where op.AccountId= @AccountId)

	set @Opportunity=(SELECT IIF(@Opportunity IS NULL, 0,@Opportunity))

	set @Measurments=(select top 1 op.OpportunityId from crm.MeasurementHeader op where op.OpportunityId= @Opportunity)
	
	set @Measurments=(SELECT IIF(@Measurments IS NULL, 0,@Measurments))

RETURN @Measurments
End
