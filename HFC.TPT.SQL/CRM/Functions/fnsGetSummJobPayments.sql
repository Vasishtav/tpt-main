﻿
create FUNCTION [CRM].[fnsGetSummJobPayments]
(
	@JobId INT
)
RETURNS decimal (18,2)
AS
BEGIN
	DECLARE @Payments decimal(18,2)

	select @Payments = sum(Payments.Amount)
	from Acct.Invoices
	join Acct.Payments on Invoices.InvoiceId = Payments.InvoiceId
	where JobId = @jobId and IsDeleted <> 1

	RETURN ISNULL(@Payments, 0)
END
