﻿
CREATE FUNCTION [CRM].[fntChart_SalesAgent] 
(	
	@startDate		date,
	@endDate		date,	
	@franchiseId	INT = null,
	@personId		INT = null
)
RETURNS TABLE 
AS
RETURN 
(
   select j.SalesPersonId SalesPersonid, u.ColorId,
		ISNULL(RTRIM(usr.FirstName + ' ' + usr.LastName), 'Unassigned') [SalesPerson],
		isnull(COUNT(j.contractedonutc), 0) as Count ,
		isnull(sum(jq.Subtotal + jq.SurchargeTotal - (ISNULL(jq.DiscountTotal-[CRM].[fnsDiscountAmountForMSR](jq.QuoteId), 0))), 0) as Total
	FROM CRM.Jobs j  with (nolock)
		JOIN CRM.JobQuotes jq with (nolock) on jq.JobId = j.JobId and jq.IsPrimary = 1
		left JOIN CRM.Person usr with (nolock) on usr.PersonId = j.SalesPersonId	
		JOIN CRM.Person s with (nolock) on j.CustomerPersonId = s.PersonId	
		JOIN CRM.Leads l with (nolock) on j.LeadId = l.LeadId
		left join Auth.Users u with (nolock) on u.personid =  j.SalesPersonId	
		join CRM.Type_JobStatus tj with (nolock) ON tj.Id =  j.JobStatusId
		join 
		(
					SELECT DISTINCT
					ts1.[Id]
					FROM [CRM].[Type_JobStatus] ts with (nolock)
					left outer join [CRM].[Type_JobStatus] ts1 with (nolock)
						   on ts1.[ParentId]=ts.[Id]
					where
					ts1.[Id] is not null and
					( ts.[Name] like '%sold%'
					or ts.[Name] like '%sale made%'
					or ts.[Name] like '%Complete%')
					
					union
					select ts.[id]
					FROM [CRM].[Type_JobStatus] ts with (nolock)
					where ParentId is null and ( ts.[Name] like '%sold%'
					or ts.[Name] like '%sale made%'
					or ts.[Name] like '%Complete%')	
					
					union
					select ts.[id] FROM [CRM].[Type_JobStatus] ts
					where Name like '%closed%'
					and ParentId in (
					select id FROM [CRM].[Type_JobStatus] ts1 with (nolock)
					where Name like '%service%')		
						
		) sq	on sq.[Id]=j.JobStatusId
	WHERE l.IsDeleted = 0 AND j.IsDeleted = 0 AND 		
		(l.FranchiseId = @franchiseId OR @franchiseId IS NULL) AND
		(j.SalesPersonId = @personId OR @personId IS NULL) AND
		(
			CAST(j.ContractedOnUtc as date) BETWEEN @startDate AND @endDate
			OR
			(j.ContractedOnUtc IS NULL AND CAST(jq.CreatedOnUtc as date) BETWEEN @startDate and @endDate)
		)
	group by j.SalesPersonId, usr.FirstName, usr.LastName, u.ColorId

)