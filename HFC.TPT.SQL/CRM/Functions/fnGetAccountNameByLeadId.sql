﻿

CREATE FUNCTION CRM.fnGetAccountNameByLeadId
(
  @LeadId int)
RETURNS varchar(500)
AS
-- Returns the stock level for the product.  
BEGIN
DECLARE
  @ret varchar(500);
SELECT @ret=CASE
              WHEN l.IsCommercial=1
              THEN CASE
                     WHEN c.CompanyName IS NOT NULL
                          AND c.CompanyName!=''
                     THEN ISNULL(c.CompanyName,'')+' / '
                     ELSE ''
                   END+ISNULL(c.FirstName,'')+' '+ISNULL(c.LastName,'')
              ELSE ISNULL(c.FirstName,'')+' '+ISNULL(c.LastName,'')+CASE
                                                WHEN c.CompanyName IS NOT NULL
                                                     AND c.CompanyName!=''
                                                THEN ' / '+ISNULL(c.CompanyName,'')
                                                ELSE ''
                                              END
            END
FROM CRM.Leads  l
JOIN CRM.LeadCustomers lcus on l.LeadId=lcus.LeadId and lcus.IsPrimaryCustomer=1
JOIN CRM.Customer c ON lcus.CustomerId=c.CustomerId
WHERE l.LeadId=@LeadId;
IF @ret IS NULL
BEGIN
SET @ret=''
END
RETURN @ret;
END; 


