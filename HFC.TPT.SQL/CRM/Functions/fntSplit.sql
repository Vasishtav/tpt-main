﻿
CREATE FUNCTION [CRM].[fntSplit]
(	
	@string VARCHAR(MAX),
	@delimiter VARCHAR(255)
)
-- =============================================
-- Author:		Jack, Chris
-- Create date: 4/20/2015
-- Description:	Split
-- =============================================
RETURNS @output TABLE(splitdata NVARCHAR(MAX) 
) 
BEGIN 
    DECLARE @start INT, @end INT 
    SELECT @start = 1, @end = CHARINDEX(@delimiter, @string) 
    WHILE @start < LEN(@string) + 1 BEGIN 
        IF @end = 0  
            SET @end = LEN(@string) + 1
       
        INSERT INTO @output (splitdata)  
        VALUES(SUBSTRING(@string, @start, @end - @start)) 
        SET @start = @end + 1 
        SET @end = CHARINDEX(@delimiter, @string, @start)
        
    END 
    RETURN 
END
