﻿CREATE FUNCTION [CRM].[fntReports_CategorySales] 
(	
	@startDate		date,
	@endDate		date,	
	@franchiseId	INT = null,
	@categoryId		INT = null
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT j.LeadId, j.JobId, j.JobNumber, j.JobStatusId,  isnull(p.Name, tj.Name) as JobStatus,
		ji.ProductTypeId KeyId,
		ISNULL(ISNULL(ji.CategoryName + ' - ', '') + ji.ProductType, 'Unknown') [KeyName], 
		s.FirstName, s.LastName, 
		ji.Subtotal, jq.SurchargeTotal, jq.DiscountTotal, 
		j.contractedonutc, jq.createdonutc, apt.FirstAppointment		
	FROM CRM.Jobs j with (nolock)
		JOIN CRM.JobQuotes jq with (nolock) on jq.JobId = j.JobId and jq.IsPrimary = 1			
		JOIN CRM.JobItems ji with (nolock) on jq.QuoteId = ji.QuoteId
		JOIN CRM.Person s with (nolock) on j.CustomerPersonId = s.PersonId		
		JOIN CRM.Leads l with (nolock) on j.LeadId = l.LeadId
	    join CRM.Type_JobStatus tj with (nolock) ON tj.Id =  j.JobStatusId		
		left join CRM.Type_JobStatus p with (nolock) on tj.ParentId = p.Id	
		OUTER APPLY (
			select TOP 1 StartDate FirstAppointment
			from CRM.Calendar with (nolock)
			where AptTypeEnum = 1 AND JobId = j.JobId
			Order by StartDate
		) apt
	WHERE l.IsDeleted = 0 AND j.IsDeleted = 0 AND 		
		(l.FranchiseId = @franchiseId OR @franchiseId IS NULL) AND
		(ji.CategoryId = @categoryId OR @categoryId IS NULL) AND
		(
			CAST(j.ContractedOnUtc as date) BETWEEN @startDate AND @endDate
			OR
			(j.ContractedOnUtc IS NULL AND CAST(jq.CreatedOnUtc as date) BETWEEN @startDate and @endDate)
		)
)
