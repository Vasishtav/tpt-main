﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [CRM].[fntReports_MSR]
(	
	@startDate		date,
	@endDate		date,	
	@territoryId	INT = null
)
RETURNS @Results TABLE(
	TerritoryId int,
	Category_Type_Id int,
	Category_Type varchar(50),
	Cost decimal(18,4),
	Sales decimal(18,4),
	[Sum] decimal(18,4)
) 
AS BEGIN

IF db_name() like '%BB%' 
	 BEGIN       
		  INSERT @Results(TerritoryId, Category_Type_Id, Category_Type, Cost, Sales, [Sum]) 
		  (
				SELECT j.TerritoryId, 
					ji.ProductTypeId, 
					ISNULL(ji.ProductType, 'Unknown') [ProductType], 
					SUM(ji.CostSubtotal) [Cost], 
					SUM(ji.Subtotal) [Sales], 
					NULL
				FROM CRM.Jobs j with(nolock)
					JOIN CRM.JobQuotes jq with(nolock) on jq.JobId = j.JobId and jq.IsPrimary = 1
					JOIN CRM.JobItems ji with(nolock) on jq.QuoteId = ji.QuoteId
					JOIN CRM.Leads l with(nolock) on j.LeadId = l.LeadId
					join CRM.Type_JobStatus tj with(nolock) ON tj.Id =  j.JobStatusId
					join 
				(
						SELECT DISTINCT
						ts1.[Id]
						FROM [CRM].[Type_JobStatus] ts with(nolock)
						left outer join [CRM].[Type_JobStatus] ts1 with(nolock) on ts1.[ParentId]=ts.[Id]
						where ts1.[Id] is not null and ( ts.[Name] like '%sold%' or ts.[Name] like '%sale made%' or ts.[Name] like '%Complete%')
					union
						select ts.[id]
						FROM [CRM].[Type_JobStatus] ts with(nolock)
						where ParentId is null and ( ts.[Name] like '%sold%' or ts.[Name] like '%sale made%' or ts.[Name] like '%Complete%')	
					union
						select ts.[id] FROM [CRM].[Type_JobStatus] ts with(nolock)
						where Name like '%service%' Or ParentId in (select id FROM [CRM].[Type_JobStatus] ts1 with(nolock) where Name like '%service%')									
				) sq on sq.[Id]=j.JobStatusId
					
				WHERE l.IsDeleted = 0 AND j.IsDeleted = 0 AND 
					(j.TerritoryId = @territoryId OR ISNULL(@territoryId, 0) = 0) AND
					CAST(j.ContractedOnUtc as date) BETWEEN @startDate AND @endDate
				GROUP BY j.TerritoryId, ji.ProductTypeId, ji.ProductType
				UNION ALL
				SELECT j.TerritoryId, null,
					'DiscountTotal' [ProductType], 
					0 [Cost], 0 [Sales], ISNULL(SUM(jq.DiscountTotal-[CRM].[fnsDiscountAmountForMSR](jq.QuoteId)), 0) [Sum]
				FROM CRM.Jobs j with (nolock)
					JOIN CRM.JobQuotes jq with(nolock) on jq.JobId = j.JobId and jq.IsPrimary = 1		
					JOIN CRM.Leads l with(nolock) on j.LeadId = l.LeadId					
					join CRM.Type_JobStatus tj with(nolock) ON tj.Id =  j.JobStatusId
					join 
				(
						SELECT DISTINCT
						ts1.[Id]
						FROM [CRM].[Type_JobStatus] ts with (nolock)
						left outer join [CRM].[Type_JobStatus] ts1 with(nolock) on ts1.[ParentId]=ts.[Id]
						where ts1.[Id] is not null and ( ts.[Name] like '%sold%' or ts.[Name] like '%sale made%' or ts.[Name] like '%Complete%')
					union
						select ts.[id]
						FROM [CRM].[Type_JobStatus] ts with (nolock)
						where Name like '%service%' Or ParentId is null and ( ts.[Name] like '%sold%' or ts.[Name] like '%sale made%' or ts.[Name] like '%Complete%')	
					union
						select ts.[id] FROM [CRM].[Type_JobStatus] ts with (nolock)
						where ParentId in ( select id FROM [CRM].[Type_JobStatus] ts1 with (nolock) where Name like '%service%')
				) sq on sq.[Id]=j.JobStatusId

				WHERE l.IsDeleted = 0 AND j.IsDeleted = 0 AND 
					(j.TerritoryId = @territoryId OR ISNULL(@territoryId, 0) = 0) AND
					CAST(j.ContractedOnUtc as date) BETWEEN @startDate AND @endDate
				GROUP BY j.TerritoryId
				UNION ALL
				
				SELECT j.TerritoryId, null,
					'SurchargeTotal' [Key],
					0 [Cost], 0 [Sales], ISNULL(SUM(jq.SurchargeTotal), 0) [Sum]
				FROM CRM.Jobs j with (nolock)
					JOIN CRM.JobQuotes jq with(nolock) on jq.JobId = j.JobId and jq.IsPrimary = 1
					JOIN CRM.Leads l with(nolock) on j.LeadId = l.LeadId
					join CRM.Type_JobStatus tj with(nolock) ON tj.Id =  j.JobStatusId
					join 
					(
						SELECT DISTINCT ts1.[Id]
						FROM [CRM].[Type_JobStatus] ts with (nolock)
						left outer join [CRM].[Type_JobStatus] ts1 with(nolock) on ts1.[ParentId]=ts.[Id]
						where ts1.[Id] is not null 
						and (ts.[Name] like '%sold%' or ts.[Name] like '%sale made%' or ts.[Name] like '%Complete%')
					
					union
						select ts.[id]
						FROM [CRM].[Type_JobStatus] ts with (nolock)
						where ParentId is null 
						and (ts.[Name] like '%sold%' or ts.[Name] like '%sale made%' or ts.[Name] like '%Complete%')	
					
					union
						select ts.[id] FROM [CRM].[Type_JobStatus] ts with (nolock)
						where Name like '%service%' Or ParentId in (select id FROM [CRM].[Type_JobStatus] ts1 with (nolock) where Name like '%service%')	
					) sq on sq.[Id]=j.JobStatusId

				WHERE l.IsDeleted = 0 AND j.IsDeleted = 0 AND 
					(j.TerritoryId = @territoryId OR ISNULL(@territoryId, 0) = 0) AND
					CAST(j.ContractedOnUtc as date) BETWEEN @startDate AND @endDate
				GROUP BY j.TerritoryId
		  )
	 END   
 ELSE
	 BEGIN
		  INSERT @Results(TerritoryId, Category_Type_Id, Category_Type, Cost, Sales, [Sum]) 
		  (
			SELECT j.TerritoryId, 
				ji.CategoryId, 
				ISNULL(ji.CategoryName, 'Unknown') [CategoryName], 
				SUM(ji.CostSubtotal) [Cost], 
				SUM(ji.Subtotal) [Sales], 
				null
			FROM CRM.Jobs j  with (nolock)
				JOIN CRM.JobQuotes jq with(nolock) on jq.JobId = j.JobId and jq.IsPrimary = 1
				JOIN CRM.JobItems ji with(nolock) on jq.QuoteId = ji.QuoteId
				JOIN CRM.Leads l with(nolock) on j.LeadId = l.LeadId
				join CRM.Type_JobStatus tj with(nolock) ON tj.Id =  j.JobStatusId
				join 
				(
					SELECT DISTINCT
					ts1.[Id]
					FROM [CRM].[Type_JobStatus] ts with (nolock)
					left outer join [CRM].[Type_JobStatus] ts1 with(nolock) on ts1.[ParentId]=ts.[Id]
					where
					ts1.[Id] is not null and ( ts.[Name] like '%sold%' or ts.[Name] like '%sale made%' or ts.[Name] like '%Complete%')
				union
					select ts.[id]
					FROM [CRM].[Type_JobStatus] ts with (nolock)
					where ParentId is null and ( ts.[Name] like '%sold%' or ts.[Name] like '%sale made%' or ts.[Name] like '%Complete%')
				union
						select ts.[id] FROM [CRM].[Type_JobStatus] ts with (nolock)
						where Name like '%service%' Or ParentId in (select id FROM [CRM].[Type_JobStatus] ts1 with (nolock) where Name like '%service%')			
			) sq on sq.[Id]=j.JobStatusId
				WHERE l.IsDeleted = 0 AND j.IsDeleted = 0 AND 
					(j.TerritoryId = @territoryId OR ISNULL(@territoryId, 0) = 0) AND
					CAST(j.ContractedOnUtc as date) BETWEEN @startDate AND @endDate
				GROUP BY j.TerritoryId, ji.CategoryId, ji.CategoryName
				UNION ALL
				SELECT j.TerritoryId, null,
					'DiscountTotal' [CategoryName], 
					0 [Cost], 0 [Sales], ISNULL(SUM(jq.DiscountTotal-[CRM].[fnsDiscountAmountForMSR](jq.QuoteId)), 0) [Sum]
				FROM CRM.Jobs j with (nolock)
					JOIN CRM.JobQuotes jq with(nolock) on jq.JobId = j.JobId and jq.IsPrimary = 1		
					JOIN CRM.Leads l with(nolock) on j.LeadId = l.LeadId
					join CRM.Type_JobStatus tj with(nolock) ON tj.Id =  j.JobStatusId
					join 
		(
					SELECT DISTINCT
					ts1.[Id]
					FROM [CRM].[Type_JobStatus] ts with (nolock)
					left outer join [CRM].[Type_JobStatus] ts1 on ts1.[ParentId]=ts.[Id]
					where
					ts1.[Id] is not null and
					( ts.[Name] like '%sold%'
					or ts.[Name] like '%sale made%'
					or ts.[Name] like '%Complete%')
					
					union
					select ts.[id]
					FROM [CRM].[Type_JobStatus] ts with (nolock)
					where ParentId is null and ( ts.[Name] like '%sold%'
					or ts.[Name] like '%sale made%'
					or ts.[Name] like '%Complete%')		

					union
					select ts.[id]
				    FROM [CRM].[Type_JobStatus] ts with (nolock)
					where Name like '%service%' Or ParentId in (select id FROM [CRM].[Type_JobStatus] ts1 with (nolock) where Name like '%service%')	
		) sq	on sq.[Id]=j.JobStatusId
				WHERE l.IsDeleted = 0 AND j.IsDeleted = 0 AND 
					(j.TerritoryId = @territoryId OR ISNULL(@territoryId, 0) = 0) AND
					CAST(j.ContractedOnUtc as date) BETWEEN @startDate AND @endDate
				GROUP BY j.TerritoryId
				UNION ALL
				SELECT j.TerritoryId, null,
					'SurchargeTotal' [Key],
					0 [Cost], 0 [Sales], ISNULL(SUM(jq.SurchargeTotal), 0) [Sum]
				FROM CRM.Jobs j with (nolock)
					JOIN CRM.JobQuotes jq with(nolock) on jq.JobId = j.JobId and jq.IsPrimary = 1
					JOIN CRM.Leads l with(nolock) on j.LeadId = l.LeadId
					join CRM.Type_JobStatus tj with(nolock) ON tj.Id =  j.JobStatusId
					join 
		(
					SELECT DISTINCT
					ts1.[Id]
					FROM [CRM].[Type_JobStatus] ts with (nolock)
					left outer join [CRM].[Type_JobStatus] ts1 with(nolock) on ts1.[ParentId]=ts.[Id]
					where
					ts1.[Id] is not null and
					( ts.[Name] like '%sold%' or ts.[Name] like '%sale made%' or ts.[Name] like '%Complete%')					
					union
					select ts.[id]
					FROM [CRM].[Type_JobStatus] ts with (nolock)
					where ParentId is null and ( ts.[Name] like '%sold%' or ts.[Name] like '%sale made%' or ts.[Name] like '%Complete%')	
					union
						select ts.[id] FROM [CRM].[Type_JobStatus] ts with (nolock)
						where Name like '%service%' Or ParentId in (select id FROM [CRM].[Type_JobStatus] ts1 with (nolock) where Name like '%service%')		
		) sq	on sq.[Id]=j.JobStatusId
				WHERE l.IsDeleted = 0 AND j.IsDeleted = 0 AND 
					(j.TerritoryId = @territoryId OR ISNULL(@territoryId, 0) = 0) AND
					CAST(j.ContractedOnUtc as date) BETWEEN @startDate AND @endDate
				GROUP BY j.TerritoryId
		 )
	 END
RETURN
END

