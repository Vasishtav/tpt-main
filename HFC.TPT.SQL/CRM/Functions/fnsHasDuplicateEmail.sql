﻿
-- Create Function fnsHasDuplicateEmail
CREATE FUNCTION [CRM].[fnsHasDuplicateEmail]
(
	@FranchiseId INT
	, @PrimEmail varchar(256)
	, @SecEmail varchar(256)
	, @CustomerId INT = NULL --Person To Exclude
)
RETURNS BIT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Exists BIT
	SET @Exists = 0

	-- Add the T-SQL statements to compute the return value here
 declare @customId int;
 select @customId=lc.CustomerId from CRM.Leads lead join crm.LeadCustomers lc on lead.LeadId = lc.LeadId;
	IF EXISTS(
		select * from Customer cus join crm.Franchise f on cus.CustomerId = @customId
		WHERE f.FranchiseId = @FranchiseId AND 
			(ISNULL(@CustomerId, 0) = 0 ) AND			
			((ISNULL(@PrimEmail, '') <> '' AND (cus.PrimaryEmail = @PrimEmail or cus.SecondaryEmail = @PrimEmail)) or 
			 (ISNULL(@SecEmail, '') <> '' AND (cus.PrimaryEmail = @SecEmail OR cus.SecondaryEmail = @SecEmail)))
	) 
		SET @Exists = 1

	-- Return the result of the function
	RETURN @Exists

END

