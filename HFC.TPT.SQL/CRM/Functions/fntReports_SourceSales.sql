﻿
CREATE FUNCTION [CRM].[fntReports_SourceSales] 
(	
	@startDate		date,
	@endDate		date,	
	@franchiseId	INT = null,
	@sourceId		INT = null
)
RETURNS @Results TABLE(   
	LeadId int,
	JobId int,
	JobNumber int,
	JobStatusId int,
	JobStatus varchar(100),
	KeyId int,
	KeyName varchar(100),
	FirstName varchar(50),
	LastName varchar(50),
	JobCount decimal(3,2),
	Subtotal decimal(18,4),
	SurchargeTotal decimal(18,4),
	DiscountTotal decimal(18,4),
	CreatedOnUtc datetime2 null,
	ContractedOnUtc datetime2 null,
	FirstAppointment datetimeoffset null
) 
AS BEGIN
		--- insert data for Allocate to all Sources
		INSERT @Results(LeadId, JobId, JobNumber, JobStatusId, JobStatus, KeyId, KeyName, FirstName, LastName, JobCount, Subtotal, SurchargeTotal, DiscountTotal, CreatedOnUtc, ContractedOnUtc, FirstAppointment) 
		(
			SELECT 
			
			j.LeadId, 
				   j.JobId, 
				   j.JobNumber, 
				   j.JobStatusId,
                  --isnull(p.Name, tj.Name) as JobStatus,
			CASE  p.Name 
      WHEN 'service' THEN p.Name + ' - ' +  tj.Name
      ELSE isnull(p.Name, tj.Name)
    END as JobStatus,
				CAST(lsrc.SourceId as int) KeyId,
				
				ISNULL(ISNULL(parent.Name + ' - ', '') + src.Name, 'Unknown') [KeyName], 
				s.FirstName, s.LastName,
				isnull(CAST(1 as decimal(3,2)) / leadCount.CountValue, 0), 
				isnull((jq.Subtotal / leadCount.CountValue),0) as Subtotal, 
				isnull((jq.SurchargeTotal / leadCount.CountValue), 0) as SurchargeTotal, 
				isnull(isnull(jq.DiscountTotal-[CRM].[fnsDiscountAmountForMSR](jq.QuoteId), 0)/ leadCount.CountValue, 0) as DiscountTotal, 
				jq.createdonutc,
				j.contractedonutc,  
				apt.FirstAppointment
			FROM CRM.Jobs j with (nolock)
				JOIN CRM.JobQuotes jq with(nolock) on jq.JobId = j.JobId and jq.IsPrimary = 1				
				JOIN CRM.Person s with(nolock) on j.CustomerPersonId = s.PersonId	
				JOIN CRM.Leads l with(nolock) on j.LeadId = l.LeadId
				LEFT JOIN CRM.LeadSources lsrc with(nolock) on l.LeadId = lsrc.LeadId
				LEFT JOIN CRM.Sources src with(nolock) on src.SourceId = lsrc.SourceId
				LEFT JOIN CRM.Sources parent with(nolock) on parent.SourceId = src.ParentId
				left JOIN (select LeadId, count(*) as CountValue from CRM.LeadSources with (nolock) group by LeadId) leadCount on l.LeadId = leadCount.LeadId
				join CRM.Type_JobStatus tj with(nolock) ON tj.Id =  j.JobStatusId		
				left join CRM.Type_JobStatus p with(nolock) on tj.ParentId = p.Id	
				OUTER APPLY (
					select TOP 1 StartDate FirstAppointment
					from CRM.Calendar with (nolock)
					where AptTypeEnum = 1 AND JobId = j.JobId
					Order by StartDate
				) apt
				
			WHERE l.IsDeleted = 0 AND j.IsDeleted = 0 AND 		
				(l.FranchiseId = @franchiseId OR @franchiseId IS NULL) AND		
				(j.SourceId = 9999) AND
				(
					CAST(j.ContractedOnUtc as date) BETWEEN @startDate AND @endDate
					OR
					(j.ContractedOnUtc IS NULL AND CAST(jq.CreatedOnUtc as date) BETWEEN @startDate and @endDate)
				)
		)	
		
		--- Insert data for all other Allocate to all Sources
		INSERT @Results( LeadId, JobId, JobNumber, JobStatusId,JobStatus, KeyId, KeyName, FirstName, LastName, JobCount, Subtotal, SurchargeTotal, DiscountTotal, CreatedOnUtc, ContractedOnUtc, FirstAppointment) 
		(
			SELECT 
	
			j.LeadId, j.JobId, j.JobNumber, j.JobStatusId,
			    --isnull(p.Name, tj.Name) as JobStatus,
			    
			    CASE  p.Name 
      WHEN 'service' THEN p.Name + ' - ' +  tj.Name
      ELSE isnull(p.Name, tj.Name)
    END as JobStatus,
			    
				CAST(j.SourceId as int) KeyId,
				ISNULL(ISNULL(parent.Name + ' - ', '') + src.Name, 'Unknown') [KeyName], 
				s.FirstName, s.LastName, 
				1.0,
				isnull(jq.Subtotal,0), isnull(jq.SurchargeTotal,0), ISNULL(jq.DiscountTotal-[CRM].[fnsDiscountAmountForMSR](jq.QuoteId), 0) as DiscountTotal, 
				jq.createdonutc,
				j.contractedonutc,
				apt.FirstAppointment		
			FROM CRM.Jobs j with (nolock)
				JOIN CRM.JobQuotes jq with(nolock) on jq.JobId = j.JobId and jq.IsPrimary = 1				
				JOIN CRM.Person s with(nolock) on j.CustomerPersonId = s.PersonId	
				JOIN CRM.Leads l with(nolock) on j.LeadId = l.LeadId
				LEFT JOIN CRM.Sources src with(nolock) on src.SourceId = j.SourceId
				LEFT JOIN CRM.Sources parent with(nolock) on parent.SourceId = src.ParentId
				join CRM.Type_JobStatus tj with(nolock) ON tj.Id = j.JobStatusId		
				left join CRM.Type_JobStatus p with(nolock) on tj.ParentId = p.Id	
				OUTER APPLY (
					select TOP 1 StartDate FirstAppointment
					from CRM.Calendar with (nolock)
					where AptTypeEnum = 1 AND JobId = j.JobId
					Order by StartDate
				) apt
			WHERE l.IsDeleted = 0 AND j.IsDeleted = 0 AND 		
				(l.FranchiseId = @franchiseId OR @franchiseId IS NULL) AND		
				(j.SourceId = @sourceId OR @sourceId IS NULL) AND
				(j.SourceId != 9999 or j.SourceId is null) AND
				(
					CAST(j.ContractedOnUtc as date) BETWEEN @startDate AND @endDate
					OR
					(j.ContractedOnUtc IS NULL AND CAST(jq.CreatedOnUtc as date) BETWEEN @startDate and @endDate)
				)	
				
		)
	
RETURN
END

