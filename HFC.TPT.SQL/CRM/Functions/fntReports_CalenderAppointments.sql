﻿
CREATE FUNCTION [CRM].[fntReports_CalenderAppointments]
(
	@fromDT			datetime,
	@toDT			datetime,	
	@territoryId	INT = null
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ApptCount int
	

select		@ApptCount =	Count(l.leadid)
					
        from			[CRM].Calendar c with (nolock)
        join			[CRM].Leads l on c.LeadId = l.LeadId
		
		where			c.IsCancelled = 0 and c.IsDeleted = 0
						and c.StartDate between CONVERT(datetime, @fromDT , 102) AND CONVERT(datetime, @toDT + ' 23:59:59', 102)
						and	(l.TerritoryId = @territoryId OR ISNULL(@territoryId, 0) = 0)

	-- Return the result of the function
	RETURN ISNULL(@ApptCount, 0)

END

