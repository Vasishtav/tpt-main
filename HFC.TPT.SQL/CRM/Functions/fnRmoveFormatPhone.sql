﻿
CREATE FUNCTION [CRM].[fnRmoveFormatPhone] 
( @Source varchar(20) )
RETURNS varchar(20)

AS
BEGIN
	declare @temp varchar(20);
	
	IF (ISNULL(@Source, '') = '') RETURN @Source
	
	set @temp = REPLACE(@Source, ' ','')
	set @temp = REPLACE(@temp, '-','')
	set @temp = REPLACE(@temp, '(','')
	set @temp = REPLACE(@temp, ')','')

	RETURN @temp

END
