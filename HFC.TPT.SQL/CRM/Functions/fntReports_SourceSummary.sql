﻿
-- =============================================
-- Author:		Quan Tran
-- Create date: 3/12/2014
-- Description:	query for summary report by job status
-- =============================================
CREATE FUNCTION [CRM].[fntReports_SourceSummary] 
(	
	@startDate		date,
	@endDate		date,	
	@franchiseId	INT = null
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT [Id], [Month], [Year], [Day], [Key], [Sum] from 
	(
		(
			SELECT cast(t.SourceId as int) as [Id], MONTH(j.ContractedOnUtc) [Month], YEAR(j.ContractedOnUtc) [Year], DAY(j.ContractedOnUtc) [Day], ISNULL(ISNULL(parent.Name + ' - ', '') + t.Name, 'Unknown') [Key], SUM(jq.Subtotal/leadCount.CountValue) [Sum]
			FROM CRM.Jobs j with (nolock)
				JOIN CRM.JobQuotes jq with(nolock) on jq.JobId = j.JobId and jq.IsPrimary = 1
				JOIN CRM.Leads l with(nolock) on j.LeadId = l.LeadId
				LEFT JOIN CRM.LeadSources lsrc with(nolock) on l.LeadId = lsrc.LeadId
				LEFT JOIN CRM.Sources t with(nolock) on t.SourceId = lsrc.SourceId
				LEFT JOIN CRM.Sources parent with(nolock) on parent.SourceId = t.ParentId
				INNER JOIN (select LeadId, count(*) as CountValue from CRM.LeadSources with (nolock) group by LeadId) leadCount on l.LeadId = leadCount.LeadId
			WHERE l.IsDeleted = 0 AND j.IsDeleted = 0 AND 
				(l.FranchiseId = @franchiseId OR @franchiseId IS NULL) AND
				(j.SourceId = 9999) AND
				CAST(j.ContractedOnUtc as date) BETWEEN @startDate AND @startDate
	  		GROUP BY MONTH(j.ContractedOnUtc), YEAR(j.ContractedOnUtc), DAY(j.ContractedOnUtc), t.SourceId, ISNULL(parent.Name + ' - ', '') + t.Name
		)
		UNION ALL
		(
			SELECT cast(t.SourceId as int) as [Id], MONTH(j.ContractedOnUtc) [Month], YEAR(j.ContractedOnUtc) [Year], DAY(j.ContractedOnUtc) [Day], ISNULL(ISNULL(parent.Name + ' - ', '') + t.Name, 'Unknown') [Key], SUM(jq.Subtotal) [Sum]
			FROM CRM.Jobs j with (nolock)
				JOIN CRM.JobQuotes jq with(nolock) on jq.JobId = j.JobId and jq.IsPrimary = 1
				JOIN CRM.Leads l with(nolock) on j.LeadId = l.LeadId
				LEFT JOIN CRM.Sources t with(nolock) on j.SourceId = t.SourceId		
				LEFT JOIN CRM.Sources parent with(nolock) on parent.SourceId = t.ParentId
			WHERE l.IsDeleted = 0 AND j.IsDeleted = 0 AND 
				(l.FranchiseId = @franchiseId OR @franchiseId IS NULL) AND
				(j.SourceId != 9999) AND
				CAST(j.ContractedOnUtc as date) BETWEEN @startDate AND @endDate
			GROUP BY MONTH(j.ContractedOnUtc), YEAR(j.ContractedOnUtc), DAY(j.ContractedOnUtc), t.SourceId, ISNULL(parent.Name + ' - ', '') + t.Name
 		) 
	) SS 
	GROUP BY [Month], [Year], [Day], [Id], [Key], [Sum]
)