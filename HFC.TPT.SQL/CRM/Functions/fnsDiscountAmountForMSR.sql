﻿
CREATE FUNCTION [CRM].[fnsDiscountAmountForMSR]
(
@jobQuoteid INT
)
RETURNS  decimal(18,4)
AS
BEGIN
	DECLARE @qidiscountamount decimal(18,4)
	set @qidiscountamount = 0
    SELECT @qidiscountamount= SUM(ji.DiscountAmount)
    FROM CRM.JobItems ji with (nolock)
    where ji.QuoteId =@jobQuoteid
	RETURN @qidiscountamount
END

