﻿
Create FUNCTION [CRM].[fntget_AccountName] 
(@AccountId int)
RETURNS VARCHAR(250)
AS BEGIN
    DECLARE @AccountName VARCHAR(250)
	DECLARE @CustomerId int
	set @CustomerId =(select CustomerId from [CRM].[LeadCustomers] lc where lc.IsPrimaryCustomer=1 and lc.LeadId=@AccountId)
	
   set @AccountName = (select FirstName + ' ' + LastName from [CRM].Customer c where c.CustomerId=@CustomerId)
   if (@AccountName is null)
  begin
    set @AccountName='';
  end

    RETURN @AccountName 
END

