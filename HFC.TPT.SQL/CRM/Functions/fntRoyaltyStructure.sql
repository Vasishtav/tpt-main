﻿CREATE FUNCTION [CRM].[fntRoyaltyStructure] 
(	
@startDate		date,
@RoyaltyTemplateId int
)
RETURNS @Royalty TABLE(
	[RoyaltyId] [int]  NULL,
	[Royalty] [varchar](500) NULL,
	[Month] [int]  NULL,
	[Order] [int]  NULL,
	[Amount] [decimal](9, 4)  NULL,
	[StartDate] [datetime]  NULL,
	[EndDate] [datetime]  NULL
	)  
AS BEGIN 
	DECLARE @InputStartDate DATETIME, @enddate datetime,@RoyaltyId int,@Month int;
	select @InputStartDate=@startDate;

	INSERT INTO @Royalty ([RoyaltyId],[Royalty],[Month],[Order],[Amount])
	SELECT DISTINCT
	r.[RoyaltyId],r.[Royalty],r.[Month],r.[Order],r.[Amount]
	FROM [CRM].[vRoyaltyStructures] r  with (nolock)
	WHERE r.[RoyaltyTemplateId]=@RoyaltyTemplateId and r.[isDependant]=1

	DECLARE curRoyal CURSOR READ_ONLY FOR 
		select [RoyaltyId],[Month] FROM @Royalty
	OPEN curRoyal

	FETCH NEXT FROM curRoyal INTO @RoyaltyId,@Month
	WHILE @@fetch_status =0
	BEGIN

		SELECT @startDate=@startDate, @enddate=DATEADD(MM, @Month, @startDate);

		UPDATE t SET startDate=@startDate,enddate=@enddate 
		FROM @Royalty t
		WHERE RoyaltyId=@RoyaltyId
		SELECT @startDate=@enddate;
		FETCH NEXT FROM curRoyal INTO @RoyaltyId,@Month
	END
	CLOSE curRoyal
	DEALLOCATE curRoyal;

	INSERT INTO @Royalty ([RoyaltyId],[Royalty],[Month],[Order],[Amount],startDate,enddate)
	SELECT DISTINCT
	r.[RoyaltyId],r.[Royalty],r.[Month],r.[Order],r.[Amount],@InputStartDate,DATEADD(MM, r.Month, @InputStartDate)
	FROM [CRM].[vRoyaltyStructures] r with (nolock)
	WHERE r.[RoyaltyTemplateId]=@RoyaltyTemplateId and r.[isDependant]<>1

RETURN
END
