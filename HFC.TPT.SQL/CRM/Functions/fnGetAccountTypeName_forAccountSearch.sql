﻿CREATE FUNCTION [CRM].[fnGetAccountTypeName_forAccountSearch]  (@AccountTypeId int)
RETURNS VARCHAR(250)
AS BEGIN
    DECLARE @AccountTypeName VARCHAR(250)
IF @AccountTypeId = 1  
   set @AccountTypeName='Prospect';
ELSE   
   BEGIN  
      IF @AccountTypeId =2   
     set @AccountTypeName='Key/Commercial';
   ELSE  
     set @AccountTypeName='Customer';

END
 RETURN @AccountTypeName
End
