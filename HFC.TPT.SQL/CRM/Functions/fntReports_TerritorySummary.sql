﻿-- =============================================
-- Author:		Quan Tran
-- Create date: 3/12/2014
-- Description:	query for summary report by category
-- =============================================
CREATE FUNCTION [CRM].[fntReports_TerritorySummary] 
(	
	@startDate		date,
	@endDate		date,	
	@franchiseId	INT = null
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT t.TerritoryId Id, MONTH(j.ContractedOnUtc) [Month], YEAR(j.ContractedOnUtc) [Year], DAY(j.ContractedOnUtc) [Day], ISNULL(t.Name + ' - ' + t.Code, 'N/A') [Key], SUM(jq.Subtotal) [Sum]
	FROM CRM.Jobs j with (nolock)
		JOIN CRM.JobQuotes jq with(nolock) on jq.JobId = j.JobId AND jq.IsPrimary = 1
		JOIN CRM.Leads l with(nolock) on j.LeadId = l.LeadId
		LEFT JOIN CRM.Territories t with(nolock) on j.TerritoryId = t.TerritoryId		
	WHERE l.IsDeleted = 0 AND j.IsDeleted = 0 AND 
		(l.FranchiseId = @franchiseId OR @franchiseId IS NULL) AND
		CAST(j.ContractedOnUtc as date) BETWEEN @startDate AND @endDate
	GROUP BY MONTH(j.ContractedOnUtc), YEAR(j.ContractedOnUtc), DAY(j.ContractedOnUtc), t.TerritoryId, t.Name + ' - ' + t.Code
)
