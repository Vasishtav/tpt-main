﻿
CREATE FUNCTION [CRM].[fntGetSalesBySourceSummary]
(	
	@FranchiseId int
	, @ContractedOnStart datetime
	, @ContractedOnEnd datetime
)
RETURNS TABLE 
AS
RETURN 
(	
	SELECT CAST(j.SourceId as int) SourceId, SUM(jq.NetTotal) SalesTotal 
	FROM CRM.Jobs j with (nolock)
		JOIN CRM.Leads l with (nolock) on j.LeadId = l.LeadId
		JOIN CRM.JobQuotes jq with (nolock) on j.JobId = jq.JobId and jq.IsPrimary = 1					
	WHERE l.IsDeleted = 0 AND j.IsDeleted = 0 AND l.FranchiseId = @FranchiseId
		AND CAST(j.ContractedOnUtc as date) BETWEEN @ContractedOnStart AND @ContractedOnEnd
	GROUP BY j.SourceId
	HAVING SUM(jq.NetTotal) > 0
	
)

