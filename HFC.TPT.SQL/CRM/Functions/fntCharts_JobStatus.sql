﻿
CREATE FUNCTION [CRM].[fntCharts_JobStatus] 
(	
	@startDate		date,
	@endDate		date,	
	@franchiseId	INT = null,
	@statusId		INT = null
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT j.LeadId, j.JobId, j.JobNumber, j.JobStatusId, isnull(p.Name, tj.Name) as JobStatus,
		j.JobStatusId KeyId,
		ISNULL(ISNULL(parent.Name + ' - ', '') + src.Name, 'Unassigned') [KeyName] 	
	FROM CRM.Jobs j  with (nolock)		
		JOIN CRM.JobQuotes jq with (nolock) on jq.JobId = j.JobId and jq.IsPrimary = 1			
		JOIN CRM.Leads l with (nolock) on j.LeadId = l.LeadId
		JOIN CRM.Type_JobStatus src with (nolock) on src.Id = j.JobStatusId
		LEFT JOIN CRM.Type_JobStatus parent with (nolock) on parent.Id = src.ParentId
		join CRM.Type_JobStatus tj with (nolock) ON tj.Id =  j.JobStatusId		
		left join CRM.Type_JobStatus p with (nolock) on tj.ParentId = p.Id	
	WHERE l.IsDeleted = 0 AND j.IsDeleted = 0 AND 		
		(l.FranchiseId = @franchiseId OR @franchiseId IS NULL) AND		
		(j.JobStatusId = @statusId OR @statusId IS NULL) AND
		(
			CAST(j.ContractedOnUtc as date) BETWEEN @startDate AND @endDate
			OR
			(j.ContractedOnUtc IS NULL AND CAST(jq.CreatedOnUtc as date) BETWEEN @startDate and @endDate)
		)
)