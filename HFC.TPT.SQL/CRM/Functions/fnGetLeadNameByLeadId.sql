﻿
Create FUNCTION [CRM].[fnGetLeadNameByLeadId]
(@LeadId int)
RETURNS varchar(500)
AS
-- Returns the stock level for the product.  
BEGIN
DECLARE @ret varchar(500);

SELECT @ret= case when l.IsCommercial = 1 then 
	case when c.CompanyName is not null and ISNULL(c.companyname, '') <> '' then 
		CONCAT(c.companyname, ' / ')
		else ''
	end + CONCAT( c.firstname, ' ', c.lastname)
else 
	CONCAT( c.firstname, ' ', c.lastname) 
		+	case when c.CompanyName is not null and ISNULL(c.companyname, '') <> '' then 
				CONCAT(' / ', c.companyname)
			else ''
			end
end
from crm.leads l 
left join crm.LeadCustomers lc on lc.LeadId = l.LeadId and lc.IsPrimaryCustomer = 1
left join crm.Customer c on c.CustomerId = lc.CustomerId
WHERE l.LeadId=@LeadId;

if @ret is null 
	BEGIN
		set @ret=''
	END

RETURN @ret;
END;
