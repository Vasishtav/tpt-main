﻿
CREATE FUNCTION [CRM].[fnsGetTotalLineDiscounts]
(
	@QuoteId INT
)
RETURNS decimal (18,2)
AS
BEGIN
	DECLARE @result decimal
	select  @result = sum(JobItems.DiscountAmount) from crm.JobItems with (nolock) where QuoteId = @QuoteId

	RETURN ISNULL(@result, 0)
END


