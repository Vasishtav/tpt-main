﻿
CREATE FUNCTION [CRM].[fnGetAccountName_forOpportunitySearch]  (@OpportunityId int)
RETURNS VARCHAR(250)
AS BEGIN
    DECLARE @AccountName VARCHAR(250)

	set @AccountName=(select case when acc.IsCommercial=1 
		then case when CompanyName is not null and CompanyName!='' then CompanyName+' / ' else '' end + FirstName + ' ' + LastName 
		else 
		FirstName + ' ' + LastName + case when CompanyName is not null and CompanyName!='' then ' / '+CompanyName else '' end
		end
		as Name  
		from CRM.Opportunities op 
		Inner join CRM.Accounts acc on op.AccountId=acc.AccountId
		Inner join CRM.AccountCustomers acccus on acc.AccountId=acccus.AccountId and acccus.IsPrimaryCustomer=1
		Inner join [CRM].[Customer] cus on cus.CustomerId=acccus.CustomerId 
		where op.OpportunityId=@OpportunityId)
		if(@AccountName is null)
		begin
		set @AccountName = '';
		end
    RETURN @AccountName
END
