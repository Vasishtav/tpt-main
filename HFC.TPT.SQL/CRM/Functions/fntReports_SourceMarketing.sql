﻿CREATE FUNCTION [CRM].[fntReports_SourceMarketing] 
(	
	@startDate		date,
	@endDate		date,	
	@franchiseId	INT = null,
	@sourceId		INT = null
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT 
		CAST(j.SourceId as int) SourceId,
		ISNULL(ISNULL(parent.Name + ' - ', '') + src.Name, 'Unknown') [SourceName], 		
		SUM(CASE WHEN j.ContractedOnUtc IS NOT NULL THEN jq.Subtotal + jq.SurchargeTotal + jq.DiscountTotal ELSE NULL END) TotalSales, 		
		COUNT(j.ContractedOnUtc) TotalSalesCount,
		COUNT(*) TotalCount,
		cp.TotalExpense
	FROM CRM.Jobs j with (nolock)	
		JOIN CRM.JobQuotes jq with(nolock) on jq.JobId = j.JobId and jq.IsPrimary = 1				
		JOIN CRM.Leads l with(nolock) on j.LeadId = l.LeadId
		LEFT JOIN CRM.Sources src with(nolock) on src.SourceId = j.SourceId
		LEFT JOIN CRM.Sources parent with(nolock) on parent.SourceId = src.ParentId		
		OUTER APPLY 
		(
			SELECT SUM(Amount) TotalExpense
			FROM CRM.MarketingCampaigns with(nolock)
			WHERE IsActive = 1 AND SourceId = j.SourceId AND FranchiseId = l.FranchiseId AND
				CAST(j.CreatedOnUtc as date) >= CAST(StartDate as date) AND 
				(CAST(j.CreatedOnUtc as date) <= CAST(EndDate as date) OR EndDate IS NULL)
		) cp		
	WHERE l.IsDeleted = 0 AND j.IsDeleted = 0 AND 		
		(l.FranchiseId = @franchiseId OR @franchiseId IS NULL) AND		
		(j.SourceId = @sourceId OR @sourceId IS NULL) AND
		(
			CAST(j.ContractedOnUtc as date) BETWEEN @startDate AND @endDate
			OR
			(j.ContractedOnUtc IS NULL AND CAST(jq.CreatedOnUtc as date) BETWEEN @startDate and @endDate)
		)
	GROUP BY j.SourceId, parent.Name, src.Name, cp.TotalExpense
)
