﻿CREATE FUNCTION [CRM].[fntGetSalesRepSummary]
(	
	@FranchiseId int
	, @ContractedOnStart datetime
	, @ContractedOnEnd datetime
)
RETURNS TABLE 
AS
RETURN 
(
	-- Add the SELECT statement with parameter references here
	SELECT j.SalesPersonId, SUM(jq.NetTotal) SalesTotal
	FROM CRM.Jobs j  with (nolock)
		JOIN CRM.JobQuotes jq with (nolock) on j.JobId = jq.JobId AND jq.IsPrimary = 1
		JOIN CRM.Leads l with (nolock) on j.LeadId = l.LeadId		
	WHERE j.IsDeleted = 0 AND l.IsDeleted = 0 
		AND j.SalesPersonId IS NOT NULL
		AND (l.FranchiseId = @FranchiseId OR @FranchiseId IS NULL)
		AND CAST(j.ContractedOnUtc as DATE) BETWEEN @ContractedOnStart AND @ContractedOnEnd
	GROUP BY j.SalesPersonId
	HAVING SUM(jq.NetTotal) > 0
)

