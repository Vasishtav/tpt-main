﻿CREATE FUNCTION [CRM].[fntReports_VendorSales] 
(	
	@startDate		date,
	@endDate		date,	
	@franchiseId	INT = null,
	@mfgId		INT = null
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT j.LeadId, j.JobId, j.JobNumber, 
	j.JobStatusId,
--isnull(p.Name, tj.Name) as JobStatus,

CASE  p.Name 
      WHEN 'service' THEN p.Name + ' - ' +  tj.Name
      ELSE isnull(p.Name, tj.Name)
    END as JobStatus,
    
		ji.ManufacturerId KeyId,
		ISNULL(ji.Manufacturer, 'Unknown') [KeyName], 
		s.FirstName, s.LastName, 
		ji.Subtotal, jq.SurchargeTotal, jq.DiscountTotal, 
		j.contractedonutc, jq.createdonutc, apt.FirstAppointment		
	FROM CRM.Jobs j with (nolock)
		JOIN CRM.JobQuotes jq on jq.JobId = j.JobId and jq.IsPrimary = 1			
		JOIN CRM.JobItems ji on jq.QuoteId = ji.QuoteId
		JOIN CRM.Person s on j.CustomerPersonId = s.PersonId		
		JOIN CRM.Leads l on j.LeadId = l.LeadId
		  join CRM.Type_JobStatus tj ON tj.Id =  j.JobStatusId		
		left join CRM.Type_JobStatus p on tj.ParentId = p.Id	
		OUTER APPLY (
			select TOP 1 StartDate FirstAppointment
			from CRM.Calendar
			where AptTypeEnum = 1 AND JobId = j.JobId
			Order by StartDate
		) apt
	WHERE l.IsDeleted = 0 AND j.IsDeleted = 0 AND 		
		(l.FranchiseId = @franchiseId OR @franchiseId IS NULL) AND
		(ji.ManufacturerId = @mfgId OR @mfgId IS NULL) AND
		(
			CAST(j.ContractedOnUtc as date) BETWEEN @startDate AND @endDate
			OR
			(j.ContractedOnUtc IS NULL AND CAST(jq.CreatedOnUtc as date) BETWEEN @startDate and @endDate)
		)
		
		union all

		
SELECT j.LeadId,
 j.JobId, 
 j.JobNumber, 
 j.JobStatusId,isnull(p.Name, tj.Name) as JobStatus,
	   null GuidKeyId,
	   case when jsa.TypeEnum = 0 then 'Surcharge' when jsa.TypeEnum = 1 then 'Discount' end as  [KeyName], 
		s.FirstName,
		 s.LastName,
		 case when jsa.TypeEnum = 0 then jsa.Amount when jsa.TypeEnum = 1 then jsa.Amount * -1 end as	 Subtotal,  
		
		 0 SurchargeTotal, 
		 0 DiscountTotal, 
		j.contractedonutc,
		jq.createdonutc,
		apt.FirstAppointment	
	FROM CRM.Jobs j  with (nolock)
		JOIN CRM.JobQuotes jq on jq.JobId = j.JobId and jq.IsPrimary = 1			
		JOIN CRM.Person s on j.CustomerPersonId = s.PersonId		
		JOIN CRM.Leads l on j.LeadId = l.LeadId
		join [CRM].[JobSaleAdjustments] AS jsa on jsa.QuoteId=jq.QuoteId 
		  join CRM.Type_JobStatus tj ON tj.Id =  j.JobStatusId		
		left join CRM.Type_JobStatus p on tj.ParentId = p.Id	
		OUTER APPLY (
			select TOP 1 StartDate FirstAppointment
			from CRM.Calendar
			where AptTypeEnum = 1 AND JobId = j.JobId
			Order by StartDate
		) apt	
			WHERE l.IsDeleted = 0 AND j.IsDeleted = 0 AND 		
		(l.FranchiseId = @franchiseId OR @franchiseId IS NULL)
		 AND
		(
			CAST(j.ContractedOnUtc as date) BETWEEN @startDate AND @endDate
			OR
			(j.ContractedOnUtc IS NULL AND CAST(jq.CreatedOnUtc as date) BETWEEN @startDate and @endDate)
		)
		and 
		jsa.TypeEnum!=2 and jsa.Amount > 0
		
		union all				
		
		SELECT j.LeadId, j.JobId, j.JobNumber, j.JobStatusId,isnull(p.Name, tj.Name) as JobStatus,
		null GuidKeyId,
		case when jsa.TypeEnum = 0 then 'Surcharge'
		when jsa.TypeEnum = 1 then 'Discount'
		end as  [KeyName], 
		s.FirstName, s.LastName,
	    case when jsa.TypeEnum = 0 then (jsa.[Percent]* jq.Subtotal)/100 when jsa.TypeEnum = 1 then (jsa.[Percent]* jq.Subtotal)/100 * -1 end as Subtotal,  
	
		0 SurchargeTotal, 0 DiscountTotal, 
		j.contractedonutc, jq.createdonutc,
		apt.FirstAppointment	
	FROM CRM.Jobs j  with (nolock)
		JOIN CRM.JobQuotes jq on jq.JobId = j.JobId and jq.IsPrimary = 1			

		JOIN CRM.Person s on j.CustomerPersonId = s.PersonId		
		JOIN CRM.Leads l on j.LeadId = l.LeadId
		join [CRM].[JobSaleAdjustments] AS jsa on jsa.QuoteId=jq.QuoteId
		join CRM.Type_JobStatus tj ON tj.Id =  j.JobStatusId		
		left join CRM.Type_JobStatus p on tj.ParentId = p.Id	 
		OUTER APPLY (
			select TOP 1 StartDate FirstAppointment
			from CRM.Calendar
			where AptTypeEnum = 1 AND JobId = j.JobId
			Order by StartDate
		) apt	
			WHERE l.IsDeleted = 0 AND j.IsDeleted = 0 AND 		
		(l.FranchiseId = @franchiseId OR @franchiseId IS NULL) AND

		(
			CAST(j.ContractedOnUtc as date) BETWEEN @startDate AND @endDate
			OR
			(j.ContractedOnUtc IS NULL AND CAST(jq.CreatedOnUtc as date) BETWEEN @startDate and @endDate)
		)
		and 
		jsa.TypeEnum!=2 and jsa.[Percent] > 0 and jq.Subtotal > 0	
		
		
		
)

