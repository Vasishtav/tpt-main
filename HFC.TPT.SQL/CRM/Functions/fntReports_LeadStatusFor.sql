﻿create FUNCTION [CRM].[fntReports_LeadStatusFor]
(	
	@startDate			datetime,
	@endDate			datetime,	
	@franchiseId	int,
	@CommercialType int
)
-- =============================================
-- Author:		Chris Huang
-- Create date: 1/21/2016
-- Description:	Sales Beta Reports Lead Report
-- =============================================
RETURNS TABLE 
AS
RETURN 
(
	--declare     @startDate		datetime
	--declare 	@endDate		datetime	
	--declare 	@franchiseId	INT 

	 
	-- set @startDate = '10/01/2015 00:00:00.00'
	-- set @endDate = '12/31/2015 23:59:59.00'
	-- set @franchiseId = 2154;
 
  select tl.name as LeadStatus, tl.Id as ParentStatusId, tl.Name as SubStatus, tl.Id as SubStatusId, l.* from crm.Leads l
	join crm.Type_LeadStatus tl on tl.Id = l.LeadStatusId
	 Inner JOIN [dbo].[vw_CommercialLeads] as com on com.leadId = l.LeadId and (com.CommercialType = @CommercialType or @CommercialType = 0)
	  WHERE l.IsDeleted = 0 AND tl.Parentid is null and 
			(l.FranchiseId = @franchiseId OR @franchiseId IS NULL) AND						 
			 	 CAST(l.CreatedOnUtc as date) BETWEEN @startDate and @endDate
  union all		 	 
		 
  select p.Name as LeadStatus,tl.ParentId as ParentStatusId, tl.name as SubStatus, tl.Id as SubStatusId, l.* from crm.Leads l
	join crm.Type_LeadStatus tl on tl.Id = l.LeadStatusId
	left join crm.Type_LeadStatus p on tl.ParentId = p.Id
	Inner JOIN [dbo].[vw_CommercialLeads] as com on com.leadId = l.LeadId and (com.CommercialType = @CommercialType or @CommercialType = 0)
	
	  WHERE l.IsDeleted = 0 AND tl.Parentid = p.Id and 
			(l.FranchiseId = @franchiseId OR @franchiseId IS NULL) AND						 
			 	 CAST(l.CreatedOnUtc as date) BETWEEN @startDate and @endDate
)
