﻿
CREATE FUNCTION [CRM].[fnGetOpportunity_forAccountSearch]  (@AccountId int)
RETURNS VARCHAR(250)
AS BEGIN
    DECLARE @Opportunity varchar(250)

	set @Opportunity=(select top 1 op.OpportunityId from crm.Opportunities op where op.AccountId= @AccountId)

	set @Opportunity=(SELECT IIF(@Opportunity IS NULL, 0,@Opportunity))


RETURN @Opportunity
End
