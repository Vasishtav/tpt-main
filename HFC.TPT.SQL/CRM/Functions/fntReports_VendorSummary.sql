﻿-- =============================================
-- Author:		Quan Tran
-- Create date: 3/12/2014
-- Description:	query for summary report by category
-- =============================================
CREATE FUNCTION [CRM].[fntReports_VendorSummary] 
(	
	@startDate		date,
	@endDate		date,	
	@franchiseId	INT = null
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT ji.ManufacturerId Id, MONTH(j.ContractedOnUtc) [Month], YEAR(j.ContractedOnUtc) [Year], DAY(j.ContractedOnUtc) [Day], ISNULL(ji.Manufacturer, 'Unknown') [Key], SUM(ji.Subtotal) [Sum]
	FROM CRM.Jobs j  with (nolock)
		JOIN CRM.JobQuotes jq on jq.JobId = j.JobId and jq.IsPrimary = 1			
		JOIN CRM.JobItems ji on jq.QuoteId = ji.QuoteId
		JOIN CRM.Leads l on j.LeadId = l.LeadId
	WHERE l.IsDeleted = 0 AND j.IsDeleted = 0 AND 
		(l.FranchiseId = @franchiseId OR @franchiseId IS NULL) AND
		CAST(j.ContractedOnUtc as date) BETWEEN @startDate AND @endDate
	GROUP BY MONTH(j.ContractedOnUtc), YEAR(j.ContractedOnUtc), DAY(j.ContractedOnUtc), ji.ManufacturerId, ji.Manufacturer
)


