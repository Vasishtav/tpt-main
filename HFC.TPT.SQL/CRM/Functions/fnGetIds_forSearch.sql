﻿CREATE FUNCTION [CRM].[fnGetIds_forSearch] 
(	
	@leadStatusIds varchar(500),
	@installerPersonIds varchar(500),
	@salesPersonIds varchar(500),
	@jobStatusIds varchar(500),
	@InvoiceStatuses varchar(500),
    @SourceIds varchar(500),
	@Type varchar(10),
	@FranchiseId int
)

RETURNS @Ids TABLE(id int) 
AS
BEGIN

if	@leadStatusIds is null and 
	@installerPersonIds is null and  
	@salesPersonIds is null and 
	@jobStatusIds is null and 
	@InvoiceStatuses is null and 
    @SourceIds  is null
	return 

	DECLARE @InvoiceStatusList table (id int);
	DECLARE @JobStatusList table (id int);
	DECLARE @LeadStatusList table (id int);
	DECLARE @SalesPersonsList table (id int);
	DECLARE @InstallerPersonsList table (id int);
	DECLARE @SourceList table (id int);

	IF @InvoiceStatuses is not null
	BEGIN 
	INSERT INTO @InvoiceStatusList(id)
		SELECT cast(Value as int)  FROM dbo.SplitCommaToTable(@InvoiceStatuses);
	END	
	IF @jobStatusIds is not null
	BEGIN 
		INSERT INTO @JobStatusList(id)
		SELECT cast(Value as int)  FROM dbo.SplitCommaToTable(@jobStatusIds);
		INSERT INTO @JobStatusList(id)
		SELECT [Id] FROM [CRM].[Type_JobStatus] WHERE  isDeleted != 1 and [ParentId] IN (SELECT Id from @JobStatusList) AND [ParentId] is not null;
	END	
	IF @LeadStatusIds is not null
	BEGIN 
		INSERT INTO @LeadStatusList(id)
		SELECT cast(Value as int)  FROM dbo.SplitCommaToTable(@LeadStatusIds);
		-------
		INSERT INTO @LeadStatusList(id)
		SELECT [Id] FROM [CRM].[Type_LeadStatus] WHERE isDeleted != 1 and [ParentId] IN (SELECT Id from @LeadStatusList) AND [ParentId] is not null;

	END	
	IF @salesPersonIds is not null
	BEGIN 
	INSERT INTO @SalesPersonsList(id)
		SELECT cast(Value as int)  FROM dbo.SplitCommaToTable(@salesPersonIds);
	END
	
	IF @installerPersonIds is not null
	BEGIN 
	INSERT INTO @InstallerPersonsList(id)
		SELECT cast(Value as int)  FROM dbo.SplitCommaToTable(@installerPersonIds);
	END		

	IF @SourceIds is not null
	BEGIN
		INSERT INTO @SourceList(id)
				select SourceId from [CRM].[Sources] where ParentId in (
  					select SourceId From [CRM].[Sources] as s
					inner join dbo.SplitCommaToTable(@SourceIds) as sc on s.[SourceId] = sc.Value
					where isDeleted != 1 and s.ParentId is null)
				and (FranchiseId = @FranchiseId or FranchiseId is null) and isnull(isCampaign, 0) = 0
			union 
				SELECT cast(Value as int) as SourceId  FROM dbo.SplitCommaToTable(@SourceIds);
		
	END

	--return
--------------------------------------

	IF @type = 'lead'
 	BEGIN

			INSERT INTO @Ids
			SELECT DISTINCT lead.LeadId
			FROM crm.Leads lead
			LEFT OUTER JOIN crm.Jobs job  
				ON lead.LeadId = job.LeadId
				AND isnull(job.IsDeleted, 0) <> 1 
			LEFT OUTER JOIN Acct.Invoices inv 
				ON  job.JobId = inv.JobId AND
				isnull(inv.IsDeleted, 0) <> 1
			LEFT OUTER JOIN CRM.LeadSources as ls 
				on ls.LeadId = lead.LeadId
			WHERE 
				lead.LeadId is not null and
				lead.FranchiseId = @FranchiseId AND
				isnull(lead.IsDeleted, 0) <> 1 AND
				(inv.StatusEnum IN ( select id from @InvoiceStatusList) or @InvoiceStatuses IS NULL ) AND
				(job.JobStatusId IN (select  id from @JobStatusList ) or @jobStatusIds IS NULL ) AND
				(job.InstallerPersonId IN (select  id from @InstallerPersonsList) or @installerPersonIds IS NULL ) AND
				(job.SalesPersonId IN (select  id from @SalesPersonsList) or @salesPersonIds IS NULL ) AND
				(lead.[LeadStatusID] IN (select  id from  @LeadStatusList   ) or @LeadStatusIDs IS NULL ) AND	
				(ls.sourceId IN (select  id from  @SourceList   ) or @SourceIds IS NULL )
				;
		END
--------
	IF @type = 'job'
		BEGIN
			INSERT INTO @Ids
			SELECT DISTINCT job.JobId
			FROM crm.Leads lead
			LEFT OUTER JOIN crm.Jobs job 
				ON lead.LeadId = job.LeadId
			LEFT OUTER JOIN Acct.Invoices inv 
				ON  job.JobId = inv.JobId
			LEFT OUTER JOIN CRM.LeadSources as ls 
				on ls.LeadId = lead.LeadId
			WHERE 
				job.JobId is not null and
				lead.FranchiseId = @FranchiseId AND
				isnull(inv.IsDeleted, 0) <> 1 AND
				isnull(job.IsDeleted, 0) <> 1 AND
				isnull(lead.IsDeleted, 0) <> 1 AND
				(inv.StatusEnum IN ( select id from @InvoiceStatusList) or @InvoiceStatuses IS NULL ) AND
				(job.JobStatusId IN (select  id from @JobStatusList ) or @jobStatusIds IS NULL ) AND
				(job.InstallerPersonId IN (select  id from @InstallerPersonsList) or @installerPersonIds IS NULL ) AND
				(job.SalesPersonId IN (select  id from @SalesPersonsList) or @salesPersonIds IS NULL ) AND
				(lead.[LeadStatusID] IN (select  id from  @LeadStatusList   ) or @LeadStatusIDs IS NULL ) AND	
				(ls.sourceId IN (select  id from  @SourceList   ) or @SourceIds IS NULL )
				;
		END
----------
	IF @type = 'invoice'
		BEGIN
			INSERT INTO @Ids
			SELECT DISTINCT inv.InvoiceId
			FROM crm.Leads lead
			INNER JOIN crm.Jobs job 
				ON lead.LeadId = job.LeadId
				and isnull(job.IsDeleted, 0) <> 1
			INNER JOIN Acct.Invoices inv 
				ON job.JobId = inv.JobId AND
				isnull(inv.IsDeleted, 0) <> 1
			LEFT OUTER JOIN CRM.LeadSources as ls 
				on ls.LeadId = lead.LeadId
			WHERE 
				inv.InvoiceId IS NOT NULL AND
				lead.FranchiseId = @FranchiseId AND
				isnull(lead.IsDeleted, 0) <> 1 AND
				(inv.StatusEnum IN ( select id from @InvoiceStatusList) or @InvoiceStatuses IS NULL ) AND
				(job.JobStatusId IN (select  id from @JobStatusList ) or @jobStatusIds IS NULL ) AND
				(job.InstallerPersonId IN (select  id from @InstallerPersonsList) or @installerPersonIds IS NULL ) AND
				(job.SalesPersonId IN (select  id from @SalesPersonsList) or @salesPersonIds IS NULL ) AND
				(lead.[LeadStatusID] IN (select  id from  @LeadStatusList   ) or @LeadStatusIDs IS NULL ) AND	
				(ls.sourceId IN (select  id from  @SourceList   ) or @SourceIds IS NULL )
				;
		END
	RETURN 
END

