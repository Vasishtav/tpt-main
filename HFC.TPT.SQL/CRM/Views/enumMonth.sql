﻿
  CREATE VIEW [CRM].[enumMonth] AS
  SELECT 1 AS [Value], 'January' as Enum
  union 
  SELECT 2, 'February'
  union
  SELECT 3, 'March'
   union
   select 4, 'April'
   union
   select 5, 'May'
   union
   select 6, 'June'
   union
   select 7, 'July'
   union
   select 8, 'August'
   union
   select 9, 'September'
   union
   select 10, 'October'
   union
   select 11, 'November'
   union
   select 12, 'December'
