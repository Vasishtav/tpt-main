﻿CREATE VIEW [CRM].[enumRecurringPattern] AS
  SELECT 1 AS [Value], 'Daily' as Enum
  union 
  SELECT 2, 'Weekly'
  union
  SELECT 3, 'Monthly'
   union
   select 4, 'Yearly'
