Create view vwMinionFranchiseLookup
as
SELECT
 'Budget Blinds' Brand
,'' [StartDate]
,o.CompName [FranchiseName]
,'' [FranchiseDBAName]
,'' [TimeZone]
,'' [WebSite]
,o.OwnerID [OwnerID]
,o.Email [Email]
,o.Phone1 [PrimaryPhone]
,o.FName [OwnerFName]
,o.LName [OwnerLName]
,'' [AdminName]
,o.Email [AdminEmail]
,s.Country
,o.ZipCode [ZipPostalCode]
,o.[Address] [Address1]
,o.[Address2] [Address2]
,o.[City] [City]
,o.[State] [State]
FROM BBdotComSM.[dbo].[Owner] o
JOIN BBdotComSM.[dbo].[State] s
	on o.[State] = s.Abbr
UNION ALL
SELECT
 'Concrete Craft' Brand
,'' [StartDate]
,o.CompName [FranchiseName]
,'' [FranchiseDBAName]
,'' [TimeZone]
,'' [WebSite]
,o.OwnerID [OwnerID]
,o.Email [Email]
,o.Phone1 [PrimaryPhone]
,o.FName [OwnerFName]
,o.LName [OwnerLName]
,'' [AdminName]
,o.Email [AdminEmail]
,s.Country
,o.ZipCode [ZipPostalCode]
,o.[Address] [Address1]
,o.[Address2] [Address2]
,o.[City] [City]
,o.[State] [State]
FROM CCdotCom.[dbo].[Owner] o
JOIN CCdotCOM.[dbo].[State] s
	on o.[State] = s.Abbr
UNION ALL
SELECT
 'Tailored Living' Brand
,'' [StartDate]
,o.CompName [FranchiseName]
,'' [FranchiseDBAName]
,'' [TimeZone]
,'' [WebSite]
,o.OwnerID [OwnerID]
,o.Email [Email]
,o.Phone1 [PrimaryPhone]
,o.FName [OwnerFName]
,o.LName [OwnerLName]
,'' [AdminName]
,o.Email [AdminEmail]
,s.Country
,o.ZipCode [ZipPostalCode]
,o.[Address] [Address1]
,o.[Address2] [Address2]
,o.[City] [City]
,o.[State] [State]
FROM CTdotCom.[dbo].[Owner] o
JOIN CTdotCOM.[dbo].[State] s
	on o.[State] = s.Abbr