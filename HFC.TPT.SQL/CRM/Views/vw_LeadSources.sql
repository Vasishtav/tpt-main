﻿

create view [CRM].[vw_LeadSources]
as

with cte as (
    select 
    
       t.[SourceId]
      ,t.[Name]
      ,t.[Description]
      ,t.[ParentId]
      ,t.[FranchiseId]
      ,t.[StartDate]
      ,t.[EndDate]
      ,t.[Amount]
      ,t.[CreatedOn]
      ,t.[IsActive]
      ,t.[Memo]
      ,t.[isDeleted]
      ,t.[isCampaign],  
      CAST(ISNULL(Name + '' ,'')  + '' AS VARCHAR(4000)) AS Path
        from [CRM].[Sources] t
        where t.ParentID is null
    union all
    select 
       t.[SourceId]
      ,t.[Name]
      ,t.[Description]
      ,t.[ParentId]
      ,t.[FranchiseId]
      ,t.[StartDate]
      ,t.[EndDate]
      ,t.[Amount]
      ,t.[CreatedOn]
      ,t.[IsActive]
      ,t.[Memo]
      ,t.[isDeleted]
      ,t.[isCampaign]
    ,list= CAST(ISNULL(t.Name  + '->' ,'')  + c.Path AS VARCHAR(4000))
        from [CRM].[Sources] t
            inner join cte c
                on t.ParentId = c.SourceId
)


select c.*, (select count(*) from CRM.LeadSources ls where ls.SourceId = c.SourceId) as LeadSourcesCount from cte c




