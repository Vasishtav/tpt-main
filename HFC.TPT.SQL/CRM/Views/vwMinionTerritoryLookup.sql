Create view vwMinionTerritoryLookup
as
SELECT
 'Budget Blinds' Brand
,o.[OwnerID]
,t.[TErrCode] [TerritoryCode]
,t.[TerrName] [TerritoryName]
,t.[MinionCode] [MinionID]
,'https://www.budgetblinds.com/'+t.[DirName]+'/' [WebsiteURL]
FROM BBdotComSM.[dbo].[Territory] t
JOIN BBdotComSM.[dbo].[Owner] o
	ON t.OwnerID = o.ID
UNION ALL
SELECT
 'Concrete Craft' Brand
,o.[OwnerID]
,t.[TErrCode] [TerritoryCode]
,t.[TerrName] [TerritoryName]
,t.[MinionCode] [MinionID]
,'https://www.concretecraft.com/'+t.[DirName]+'/' [WebsiteURL]
FROM CCdotCom.[dbo].[Territory] t
JOIN CCdotCom.[dbo].[Owner] o
	ON t.OwnerID = o.ID
UNION ALL
SELECT
 'Tailored Living' Brand
,o.[OwnerID]
,t.[TErrCode] [TerritoryCode]
,t.[TerrName] [TerritoryName]
,t.[MinionCode] [MinionID]
,'https://www.tailoredliving.com/'+t.[DirName]+'/' [WebsiteURL]
FROM CTdotCom.[dbo].[Territory] t
JOIN CTdotCom.[dbo].[Owner] o
	ON t.OwnerID = o.ID