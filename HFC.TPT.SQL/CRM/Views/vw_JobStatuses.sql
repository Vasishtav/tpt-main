﻿

create view [CRM].[vw_JobStatuses]
as
  
  with cte as (
    select 
    
       t.[Id]
      ,t.[Name]
      ,t.[ParentId]
      ,t.[FranchiseId]
      ,t.[DisplayOrder]
      ,t.[ClassName]
      ,t.[isDeleted]
      ,CAST(ISNULL(Name + '' ,'')  + '' AS VARCHAR(4000)) AS Path
        from [CRM].[Type_JobStatus] t
        where t.ParentID is null
    union all
    select 
        t.[Id]
      ,t.[Name]
      ,t.[ParentId]
      ,t.[FranchiseId]
      ,t.[DisplayOrder]
      ,t.[ClassName]
      ,t.[isDeleted]
    ,list= CAST(ISNULL(t.Name  + '->' ,'')  + c.Path AS VARCHAR(4000))
        from [CRM].[Type_JobStatus] t
            inner join cte c
                on t.ParentId = c.Id
   )


select c.*, (select count(*) from CRM.Jobs ls where ls.JobStatusId = c.Id) as JobsCount from cte c




