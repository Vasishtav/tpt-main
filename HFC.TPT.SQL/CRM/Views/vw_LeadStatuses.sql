﻿
  create view [CRM].[vw_LeadStatuses]
as
   with cte as (
    select 
    
       t.[Id]
      ,t.[Name]
      ,t.[ParentId]
      ,t.[FranchiseId]
      ,t.[ClassName]
      ,t.[DisplayOrder]
      ,t.[isDeleted]
      ,CAST(ISNULL(Name + '' ,'')  + '' AS VARCHAR(4000)) AS Path
        from [CRM].[Type_LeadStatus] t
        where t.ParentId is null
    union all
    select 
         t.[Id]
      ,t.[Name]
      ,t.[ParentId]
      ,t.[FranchiseId]
      ,t.[ClassName]
      ,t.[DisplayOrder]
      ,t.[isDeleted]
    ,list= CAST(ISNULL(t.Name  + '->' ,'')  + c.Path AS VARCHAR(4000))
        from [CRM].[Type_LeadStatus] t
            inner join cte c
                on t.ParentId = c.Id
   )


select c.*, (select count(*) from CRM.Leads ls where ls.LeadStatusId = c.Id) as LeadsCount from cte c

