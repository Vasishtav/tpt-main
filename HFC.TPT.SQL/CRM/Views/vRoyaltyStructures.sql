﻿

CREATE VIEW [CRM].[vRoyaltyStructures]
AS
SELECT TOP 100 PERCENT
rp.Id as RoyaltyTemplateId,
rp.Name as RoyaltyTemplate,
tr.Id as RoyaltyId,
tr.Name as Royalty,
rt.NumberOfMonth as [Month],
rt.[Amount],
tr.[isDependant],
tr.[Order]
FROM [CRM].[RoyaltyTemplate] rt
INNER JOIN [CRM].[Type_Royalty] tr
	ON rt.Type_RoyaltyId=tr.id
INNER JOIN [CRM].[Type_RoyaltyProfile] rp
	on rp.Id=rt.RoyaltyProfileId
ORDER BY rp.Id,tr.[Order]

