﻿CREATE TABLE [CRM].[OpportunityNotes] (
    [NoteId]            INT                IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [OpportunityId]     INT                NOT NULL,
    [Message]           VARCHAR (MAX)      NULL,
    [CreatedOn]         DATETIMEOFFSET (2) CONSTRAINT [DF_OpportunityNotes_CreatedOnUtc] DEFAULT (getdate()) NOT NULL,
    [CreatedByPersonId] INT                NULL,
    [TypeEnum]          TINYINT            CONSTRAINT [DF_OpportunityNotes_TypeEnum] DEFAULT ((0)) NOT NULL,
    [Title]             VARCHAR (50)       NULL,
    CONSTRAINT [PK_OpportunityNotes] PRIMARY KEY CLUSTERED ([NoteId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_OpportunityNotes_Opportunitys] FOREIGN KEY ([OpportunityId]) REFERENCES [CRM].[Opportunities] ([OpportunityId]),
    CONSTRAINT [FK_OpportunityNotes_Person] FOREIGN KEY ([CreatedByPersonId]) REFERENCES [CRM].[Person] ([PersonId])
);

