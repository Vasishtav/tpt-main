﻿CREATE TABLE [CRM].[FranchiseSocial] (
    [SocialId]       INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Name]           VARCHAR (50)  NULL,
    [SocialUrl]      VARCHAR (512) NULL,
    [FranchiseId]    INT           NOT NULL,
    [IncludeInEmail] BIT           CONSTRAINT [DF_FranchiseSocial_IncludeInEmail] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_FranchiseSocial] PRIMARY KEY CLUSTERED ([SocialId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_FranchiseSocial_Franchise] FOREIGN KEY ([FranchiseId]) REFERENCES [CRM].[Franchise] ([FranchiseId])
);

