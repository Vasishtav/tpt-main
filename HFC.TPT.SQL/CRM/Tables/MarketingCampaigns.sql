﻿CREATE TABLE [CRM].[MarketingCampaigns] (
    [MktCampaignId] INT                IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [SourceId]      INT                NOT NULL,
    [FranchiseId]   INT                NOT NULL,
    [StartDate]     DATETIMEOFFSET (2) NOT NULL,
    [EndDate]       DATETIMEOFFSET (2) NULL,
    [Amount]        DECIMAL (9, 4)     CONSTRAINT [DF_MarketingCampaigns_Amount] DEFAULT ((0)) NOT NULL,
    [Label]         VARCHAR (256)      NULL,
    [CreatedOn]     DATETIMEOFFSET (2) CONSTRAINT [DF_MarketingCampaigns_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [IsActive]      BIT                CONSTRAINT [DF_MarketingCampaigns_IsActive] DEFAULT ((1)) NOT NULL,
    [Memo]          VARCHAR (1000)     NULL,
    CONSTRAINT [PK_MarketingCampaigns] PRIMARY KEY CLUSTERED ([MktCampaignId] ASC) WITH (FILLFACTOR = 90)
);

