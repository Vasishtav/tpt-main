﻿CREATE TABLE [CRM].[APITraceLog] (
    [APITraceLogId] UNIQUEIDENTIFIER NOT NULL,
    [API]           VARCHAR (50)     NULL,
    [Group]         VARCHAR (50)     NULL,
    [Type]          VARCHAR (50)     NULL,
    [URL]           VARCHAR (1000)   NULL,
    [input]         VARCHAR (MAX)    NULL,
    [Response]      VARCHAR (MAX)    NULL,
    [CreatedOn]     DATETIME         NULL,
    [ElapsedTime]   NUMERIC (18, 7)  NULL,
    CONSTRAINT [PK_APITraceLog] PRIMARY KEY CLUSTERED ([APITraceLogId] ASC) WITH (DATA_COMPRESSION = PAGE)
);


GO
CREATE NONCLUSTERED INDEX [idxAPITraceLog]
    ON [CRM].[APITraceLog]([CreatedOn] ASC, [APITraceLogId] ASC);

