﻿CREATE TABLE [CRM].[Log_Error] (
    [ErrorLogId]   BIGINT        IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [FranchiseId]  INT           NULL,
    [Type]         VARCHAR (100) NOT NULL,
    [Source]       VARCHAR (100) NOT NULL,
    [Message]      VARCHAR (MAX) NOT NULL,
    [Trace]        VARCHAR (MAX) NULL,
    [UserName]     VARCHAR (50)  NULL,
    [Severity]     VARCHAR (50)  NOT NULL,
    [CreatedOnUtc] DATETIME      CONSTRAINT [DF_Table_1_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [Obj]          VARCHAR (MAX) NULL,
    [OwnerId]      VARCHAR (100) NULL,
    CONSTRAINT [PK_Log_Error] PRIMARY KEY CLUSTERED ([ErrorLogId] ASC) WITH (FILLFACTOR = 90)
);


GO

CREATE NONCLUSTERED INDEX [_dta_index_Log_Error_38_1717581157__K9_5] ON [CRM].[Log_Error]
(
	[CreatedOnUtc] ASC
)
INCLUDE([Message]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

