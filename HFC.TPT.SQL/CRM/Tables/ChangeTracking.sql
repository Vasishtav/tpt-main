﻿CREATE TABLE [CRM].[ChangeTracking] (
    [ChangeTrackingId] INT           NOT NULL,
    [LeadId]           INT           NULL,
    [FranchiseId]      INT           NULL,
    [JobId]            INT           NULL,
    [CustomerId]       INT           NULL,
    [Status]           VARCHAR (50)  NULL,
    [SubStatus]        VARCHAR (50)  NULL,
    [AdminLogInId]     INT           NULL,
    [CreatedOn]        DATETIME      NOT NULL,
    [CreatedBy]        INT           NULL,
    [Disposition]      VARCHAR (100) NULL,
    CONSTRAINT [PK_ChangeTracking_1] PRIMARY KEY CLUSTERED ([ChangeTrackingId] ASC),
    CONSTRAINT [FK_ChangeTracking_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [CRM].[Customer] ([CustomerId]),
    CONSTRAINT [FK_ChangeTracking_Franchise] FOREIGN KEY ([FranchiseId]) REFERENCES [CRM].[Franchise] ([FranchiseId]),
    CONSTRAINT [FK_ChangeTracking_Job] FOREIGN KEY ([JobId]) REFERENCES [CRM].[Jobs] ([JobId]),
    CONSTRAINT [FK_ChangeTracking_Lead] FOREIGN KEY ([LeadId]) REFERENCES [CRM].[Leads] ([LeadId])
);

