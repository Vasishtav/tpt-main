﻿Create table CRM.smsOptinginfo (
[Id] INT NOT NULL PRIMARY KEY identity(1,1)
,Phonenumber varchar(50) NOT NULL
,Optin bit null
,Optout bit null
,BrandId int
,IsOptinmessagesent bit NULL
,Errorinfo varchar(500)
,CreatedOn datetime2
,LastUpdatedOn datetime2
)