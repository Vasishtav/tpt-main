﻿CREATE TABLE [CRM].[LeadSources] (
    [LeadSourceId]    INT      IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [SourceId]        INT      NOT NULL,
    [LeadId]          INT      NOT NULL,
    [CreatedOnUtc]    DATETIME CONSTRAINT [DF_Key_LeadSource_CreatedOnUtc] DEFAULT (getutcdate()) NOT NULL,
    [IsManuallyAdded] BIT      CONSTRAINT [DF_Key_LeadSource_IsManuallyAdded] DEFAULT ((0)) NOT NULL,
    [CreatedBy]       INT      NULL,
    [UpdatedOn]       DATETIME NULL,
    [UpdatedBy]       INT      NULL,
    [IsPrimarySource] BIT      DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Key_LeadSource_1] PRIMARY KEY CLUSTERED ([LeadSourceId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Key_LeadSource_Leads] FOREIGN KEY ([LeadId]) REFERENCES [CRM].[Leads] ([LeadId]),
    CONSTRAINT [FK_LeadSources_SourcesTP] FOREIGN KEY ([SourceId]) REFERENCES [CRM].[SourcesTP] ([SourcesTPId]),
    CONSTRAINT [UX_LeadSource_LeadIDSourceID] UNIQUE NONCLUSTERED ([SourceId] ASC, [LeadId] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [LeadId]
    ON [CRM].[LeadSources]([LeadId] ASC)
    INCLUDE([SourceId]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [ix_SourceId]
    ON [CRM].[LeadSources]([SourceId] ASC) WITH (FILLFACTOR = 90);

