﻿CREATE TABLE [CRM].[QuoteCoreProductDetails] (
    [QuoteCoreProductDetailsId] INT           IDENTITY (1, 1) NOT NULL,
    [QuotelineId]               INT           NOT NULL,
    [QuoteKey]                  INT           NOT NULL,
    [Value]                     VARCHAR (500) NULL,
    [ColorValue]                VARCHAR (250) NULL,
    [Description]               VARCHAR (500) NULL,
    [ValueDescription]          VARCHAR (500) NULL,
    [ColorValueDescription]     VARCHAR (500) NULL,
    CONSTRAINT [PK_QuoteCoreProductDetails] PRIMARY KEY CLUSTERED ([QuoteCoreProductDetailsId] ASC),
    CONSTRAINT [FK_QuoteCoreProductDetails_Quote] FOREIGN KEY ([QuoteKey]) REFERENCES [CRM].[Quote] ([QuoteKey]),
    CONSTRAINT [FK_QuoteCoreProductDetails_QuoteLines] FOREIGN KEY ([QuotelineId]) REFERENCES [CRM].[QuoteLines] ([QuoteLineId])
);

