﻿CREATE TABLE [CRM].[FranchiseProducts] (
    [ProductKey]           INT             IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ProductID]            INT             NOT NULL,
    [ProductName]          VARCHAR (100)   NULL,
    [PICProductID]         INT             NULL,
    [VendorID]             INT             NULL,
    [VendorName]           VARCHAR (150)   NULL,
    [VendorProductSKU]     VARCHAR (100)   NULL,
    [Description]          VARCHAR (500)   NULL,
    [ProductCategory]      INT             NULL,
    [Cost]                 NUMERIC (18, 2) NULL,
    [SalePrice]            NUMERIC (18, 2) NULL,
    [ProductStatus]        INT             NULL,
    [ProductSubCategory]   INT             NULL,
    [MarkUp]               NUMERIC (18, 2) NULL,
    [MarkupType]           VARCHAR (10)    NULL,
    [Discount]             NUMERIC (18, 2) NULL,
    [DiscountType]         VARCHAR (30)    NULL,
    [CalculatedSalesPrice] NUMERIC (18, 2) NULL,
    [CreatedOn]            DATETIME2 (7)   NOT NULL,
    [CreatedBy]            INT             NOT NULL,
    [LastUpdatedOn]        DATETIME2 (7)   NULL,
    [LastUpdatedBy]        INT             NULL,
    [FranchiseId]          INT             CONSTRAINT [DF__Franchise__Franc__125BED31] DEFAULT ((0)) NOT NULL,
    [ProductType]          INT             DEFAULT ((0)) NOT NULL,
    [Model]                VARCHAR (MAX)   NULL,
    [Collection]           VARCHAR (MAX)   NULL,
    [Color]                VARCHAR (MAX)   NULL,
    [Taxable]              BIT             NULL,
    [SurfaceLaborSet] VARCHAR(MAX) NULL, 
    [MultipartSurfacing] BIT NULL, 
    [MultipleSurfaceProductSet] VARCHAR(MAX) NULL, 
    [MultipartAttributeSet] VARCHAR(MAX) NULL, 
    [MasterSurfacingProductID] INT NULL, 
    CONSTRAINT [PK__Franchis__B40CC6ED75928A1A] PRIMARY KEY CLUSTERED ([ProductKey] ASC)
);

GO
CREATE NONCLUSTERED INDEX [_dta_index_FranchiseProducts_33_1396018261__K23_K9_K1_3] ON [CRM].[FranchiseProducts]
(
	[FranchiseId] ASC,
	[ProductCategory] ASC,
	[ProductKey] ASC
)
INCLUDE([ProductName]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
