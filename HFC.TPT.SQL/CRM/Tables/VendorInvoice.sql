﻿CREATE TABLE [CRM].[VendorInvoice] (
    [VendorInvoiceId]        INT             IDENTITY (1, 1) NOT NULL,
    [PurchaseOrderId]        INT             NOT NULL,
    [PurchaseOrdersDetailId] INT             NULL,
    [InvoiceNumber]          VARCHAR (25)    NULL,
    [Date]                   DATETIME        NULL,
    [Terms]                  VARCHAR (500)   NULL,
    [RemitByDate]            DATETIME        NULL,
    [Quantity]               INT             NOT NULL,
    [Subtotal]               DECIMAL (18, 4) NULL,
    [Tax]                    DECIMAL (18, 4) NULL,
    [Freight]                DECIMAL (18, 4) NULL,
    [Discount]               DECIMAL (18, 4) NULL,
    [DiscountDate]           DATETIME        NULL,
    [Total]                  DECIMAL (18, 4) NULL,
    [PICInvoiceResponse]     VARCHAR (MAX)   NULL,
    [CreatedOn]              DATETIME        NOT NULL,
    [CreatedBy]              INT             NOT NULL,
    [UpdatedOn]              DATETIME        NULL,
    [UpdatedBy]              INT             NULL,
    [VendorName]             VARCHAR (100)   NULL,
    [PICAP]                  INT             NULL,
    [PICVpo]                 VARCHAR (25)    NULL,
    [Currency]				VARCHAR(100) NULL, 
    [ProcessingFee]			DECIMAL(18, 4) NULL, 
    [MiscCharge]			DECIMAL(18, 4) NULL, 
    [Comments]				VARCHAR(MAX) NULL, 
	[AdjSubtotal]           DECIMAL (18, 4) NULL,
	[AdjDiscount]           DECIMAL (18, 4) NULL,    
    [AdjFreight]            DECIMAL (18, 4) NULL,    
	[AdjProcessingFee]	    DECIMAL(18, 4) NULL, 
    [AdjMiscCharge]			DECIMAL(18, 4) NULL, 
	[AdjTax]                DECIMAL (18, 4) NULL,
	[AdjTotal]              DECIMAL (18, 4) NULL,
    [AdjComments] VARCHAR(MAX) NULL, 
    [IsInvoiceRevised] BIT NULL, 
    CONSTRAINT [PK_VendorInvoice] PRIMARY KEY CLUSTERED ([VendorInvoiceId] ASC)
);

GO

CREATE NONCLUSTERED INDEX [IX_PurchaseOrderId] ON [CRM].[VendorInvoice]
(
	[PurchaseOrderId] ASC
) WITH  (FILLFACTOR = 90);
GO