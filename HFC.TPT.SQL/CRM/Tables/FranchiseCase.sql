﻿CREATE TABLE [CRM].[FranchiseCase] (
    [CaseId]               INT           IDENTITY (1, 1) NOT NULL,
    [CaseNumber]           INT           NOT NULL,
    [Owner_PersonId]       INT           NOT NULL,
    [IncidentDate]         DATETIME2 (7) NULL,
    [Status]               INT           NULL,
    [DateTimeOpened]       DATETIME2 (7) NULL,
    [DateTimeClosed]       DATETIME2 (7) NULL,
    [Description]          VARCHAR (1000) NULL,
    [VPO_PICPO_PODId]      INT           NULL,
    [MPO_MasterPONum_POId] VARCHAR (50)  NULL,
    [SalesOrderId]         NVARCHAR(MAX)           NULL,
    [CreatedOn]            DATETIME2 (7) NOT NULL,
    [CreatedBy]            INT           NOT NULL,
    [LastUpdatedOn]        DATETIME2 (7) NULL,
    [LastUpdatedBy]        INT           NULL,
    CONSTRAINT [PK_FranchiseCase] PRIMARY KEY CLUSTERED ([CaseId] ASC)
);

