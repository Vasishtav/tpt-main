﻿CREATE TABLE [CRM].[QuoteLines] (
    [QuoteLineId]               INT             IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [QuoteKey]                  INT             NOT NULL,
    [MeasurementsetId]          INT             NULL,
    [VendorId]                  INT             NULL,
    [ProductId]                 INT             NULL,
    [SuggestedResale]           NUMERIC (18, 2) NULL,
    [Discount]                  NUMERIC (18, 2) NULL,
    [CreatedOn]                 DATETIME2 (7)   NOT NULL,
    [CreatedBy]                 INT             NOT NULL,
    [LastUpdatedOn]             DATETIME2 (7)   NULL,
    [LastUpdatedBy]             INT             NULL,
    [IsActive]                  BIT             NULL,
    [Width]                     NUMERIC (18, 3) NULL,
    [Height]                    NUMERIC (18, 3) NULL,
    [UnitPrice]                 NUMERIC (18, 2) NULL,
    [Quantity]                  INT             NULL,
    [Description]               VARCHAR (MAX)   NULL,
    [MountType]                 VARCHAR (20)    NULL,
    [ExtendedPrice]             NUMERIC (18, 2) NULL,
    [Memo]                      VARCHAR (500)   NULL,
    [PICJson]                   VARCHAR (MAX)   NULL,
    [ProductTypeId]             INT             DEFAULT ((1)) NULL,
    [Quntity]                   INT             DEFAULT ((1)) NOT NULL,
    [MarkupfactorType]          VARCHAR (5)     NULL,
    [DiscountType]              VARCHAR (5)     NULL,
    [PICPriceResponse]          VARCHAR (MAX)   NULL,
    [ProductName]               VARCHAR (100)   NULL,
    [VendorName]                VARCHAR (100)   NULL,
    [CurrencyExchangeId]        INT             NULL,
    [ConversionRate]            NUMERIC (18, 2) NULL,
    [VendorCurrency]            VARCHAR (10)    NULL,
    [ConversionCalculationType] SMALLINT        NULL,
    [QuoteLineNumber]           INT             NULL,
    [FranctionalValueWidth]     VARCHAR (5)     NULL,
    [FranctionalValueHeight]    VARCHAR (5)     NULL,
    [RoomName]                  VARCHAR (50)    NULL,
    [WindowLocation]            VARCHAR (50)    NULL,
    [ProductCategoryId]         INT             NULL,
    [ProductSubCategoryId]      INT             NULL,
    [BillofMaterial]            VARCHAR (MAX)   NULL,
    [CancelReason]              VARCHAR (100)   NULL,
    [InternalNotes]             VARCHAR (MAX)   NULL,
    [CustomerNotes]             VARCHAR (MAX)   NULL,
    [VendorNotes]               VARCHAR (MAX)   NULL,
    [ModelDescription] VARCHAR(250) NULL,
    [ValidatedOn] DATETIME2 NULL,
    [Color] VARCHAR(250) NULL,
    [Fabric] VARCHAR(250) NULL,
    [IsGroupDiscountApplied] BIT NULL,
    [TaxExempt] BIT NOT NULL DEFAULT 0,
    CONSTRAINT [PK_QuoteLines] PRIMARY KEY CLUSTERED ([QuoteLineId] ASC),
    CONSTRAINT [FK_QuoteLines_Person_Createdby] FOREIGN KEY ([CreatedBy]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_QuoteLines_Person_LastUpdatedBy] FOREIGN KEY ([LastUpdatedBy]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_QuoteLines_Quote_QuoteKey] FOREIGN KEY ([QuoteKey]) REFERENCES [CRM].[Quote] ([QuoteKey])
);

GO
CREATE NONCLUSTERED INDEX [_dta_index_QuoteLines_33_1072541111__K1_K22_K4_K28_16] ON [CRM].[QuoteLines]
(
	[QuoteLineId] ASC,
	[ProductTypeId] ASC,
	[VendorId] ASC,
	[VendorName] ASC
)
INCLUDE([Quantity]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_QuoteLines_33_1072541111__K2_K22_K12_K1] ON [CRM].[QuoteLines]
(
	[QuoteKey] ASC,
	[ProductTypeId] ASC,
	[IsActive] ASC,
	[QuoteLineId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [_dta_index_QuoteLines_33_1072541111__K22_K12_K1_K39_K5_K4_K2_K33_K3_K6_K7_K25_K8_K9_K10_K11_13_14_15_16_17_18_19_20_21_27_28_] ON [CRM].[QuoteLines]
(
	[ProductTypeId] ASC,
	[IsActive] ASC,
	[QuoteLineId] ASC,
	[ProductSubCategoryId] ASC,
	[ProductId] ASC,
	[VendorId] ASC,
	[QuoteKey] ASC,
	[QuoteLineNumber] ASC,
	[MeasurementsetId] ASC,
	[SuggestedResale] ASC,
	[Discount] ASC,
	[DiscountType] ASC,
	[CreatedOn] ASC,
	[CreatedBy] ASC,
	[LastUpdatedOn] ASC,
	[LastUpdatedBy] ASC
)
INCLUDE([Width],[Height],[UnitPrice],[Quantity],[Description],[MountType],[ExtendedPrice],[Memo],[PICJson],[ProductName],[VendorName],[FranctionalValueWidth],[FranctionalValueHeight],[RoomName],[WindowLocation],[ProductCategoryId],[CancelReason],[InternalNotes],[CustomerNotes],[VendorNotes],[ModelDescription],[ValidatedOn],[Color],[Fabric]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO



CREATE NONCLUSTERED INDEX [_dta_index_QuoteLines_6_1072541111__K4_K1_K22_16] ON [CRM].[QuoteLines]
(
	[VendorId] ASC,
	[QuoteLineId] ASC,
	[ProductTypeId] ASC
)
INCLUDE([Quantity]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go