﻿CREATE TABLE [CRM].[Type_OrderStatus_old] (
    [Id]           INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Name]         VARCHAR (50) NOT NULL,
    [ParentId]     INT          NULL,
    [FranchiseId]  INT          NULL,
    [DisplayOrder] INT          CONSTRAINT [DF_Type_OrderStatus_DisplayOrder] DEFAULT ((99999)) NOT NULL,
    [ClassName]    VARCHAR (50) NULL,
    [isDeleted]    BIT          NULL,
    [CreatedAt]    DATETIME     NULL,
    [UpdatedAt]    DATETIME     NULL,
    CONSTRAINT [PK_Type_OrderStatus] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Type_OrderStatus_Franchise] FOREIGN KEY ([FranchiseId]) REFERENCES [CRM].[Franchise] ([FranchiseId]),
    CONSTRAINT [FK_Type_OrderStatus_Type_OrderStatus] FOREIGN KEY ([ParentId]) REFERENCES [CRM].[Type_OrderStatus_old] ([Id])
);

