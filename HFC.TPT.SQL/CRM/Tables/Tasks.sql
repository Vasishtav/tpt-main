﻿CREATE TABLE [CRM].[Tasks] (
    [TaskId]              INT                IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [TaskGuid]            UNIQUEIDENTIFIER   CONSTRAINT [DF_Tasks_TaskGuid] DEFAULT (newid()) NOT NULL,
    [Subject]             VARCHAR (256)      NOT NULL,
    [Message]             VARCHAR (MAX)      NULL,
    [CreatedOnUtc]        DATETIME2 (2)      CONSTRAINT [DF_Tasks_CreatedOnUtc] DEFAULT (getutcdate()) NOT NULL,
    [DueDate]             DATETIMEOFFSET (2) NULL,
    [CompletedDate]       DATETIMEOFFSET (2) NULL,
    [LeadId]              INT                NULL,
    [OrganizerPersonId]   INT                NULL,
    [OrganizerEmail]      VARCHAR (256)      NULL,
    [CreatedByPersonId]   INT                NOT NULL,
    [IsDeleted]           BIT                CONSTRAINT [DF_Tasks_IsDeleted] DEFAULT ((0)) NOT NULL,
    [LastUpdatedOnUtc]    DATETIME2 (2)      NULL,
    [LastUpdatedPersonId] INT                NULL,
    [PriorityOrder]       INT                NULL,
    [LeadNumber]          INT                NULL,
    [FranchiseId]         INT                NOT NULL,
    [IsPrivate]           BIT                NULL,
    [ReminderMinute]      SMALLINT           CONSTRAINT [DF_Tasks_ReminderMinute] DEFAULT ((0)) NOT NULL,
    [RemindMethodEnum]    TINYINT            NULL,
    [FirstRemindDate]     DATETIMEOFFSET (2) NULL,
    [RevisionSequence]    INT                CONSTRAINT [DF_Tasks_RevisionSequence] DEFAULT ((0)) NOT NULL,
    [JobId]               INT                NULL,
    [JobNumber]           INT                NULL,
    [AssignedPersonId]    INT                NULL,
    [AssignedName]        VARCHAR (128)      NULL,
    [PhoneNumber]         NVARCHAR (255)     NULL,
    [AccountId]           INT                NULL,
    [OpportunityId]       INT                NULL,
    [OrderId]             INT                NULL,
    [CaseId]              INT                NULL,
    [VendorCaseId]        INT                NULL,
    CONSTRAINT [PK_Tasks] PRIMARY KEY CLUSTERED ([TaskId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Tasks_Franchise] FOREIGN KEY ([FranchiseId]) REFERENCES [CRM].[Franchise] ([FranchiseId]),
    CONSTRAINT [FK_Tasks_Jobs] FOREIGN KEY ([JobId]) REFERENCES [CRM].[Jobs] ([JobId]),
    CONSTRAINT [FK_Tasks_Leads] FOREIGN KEY ([LeadId]) REFERENCES [CRM].[Leads] ([LeadId]),
    CONSTRAINT [FK_Tasks_Person] FOREIGN KEY ([OrganizerPersonId]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_Tasks_Person1] FOREIGN KEY ([CreatedByPersonId]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_Tasks_PersonAssigned] FOREIGN KEY ([AssignedPersonId]) REFERENCES [CRM].[Person] ([PersonId])
);


GO
CREATE NONCLUSTERED INDEX [NotCompletedDueIdx]
    ON [CRM].[Tasks]([DueDate] ASC, [CompletedDate] ASC, [IsDeleted] ASC)
    INCLUDE([Subject], [Message], [LeadId], [LeadNumber], [TaskId]) WITH (FILLFACTOR = 90);

