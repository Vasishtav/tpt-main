﻿CREATE TABLE [CRM].[TaxSettings]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
	[FranchiseId] INT NULL,
    [TerritoryId] INT NULL,
    [ShipToLocationId] INT NULL,
    [UseCustomerAddress] BIT NOT NULL DEFAULT 1,
    [TaxType] INT NOT NULL DEFAULT 1,
    [UseTaxPercentage] NUMERIC(18,2) NOT NULL DEFAULT 0,
    [CreatedOn] DATETIME2 NULL,
    [CreatedBy] INT NULL,
    [UpdatedBy] INT NOT NULL,
    [UpdatedOn] DATETIME2 NOT NULL,
    CONSTRAINT [FK_TaxSettings_Territory] FOREIGN KEY (TerritoryId) REFERENCES CRM.Territories(TerritoryId),
    CONSTRAINT [FK_TaxSettings_ShipToLocation] FOREIGN KEY (ShipToLocationId) REFERENCES CRM.ShipToLocations(Id),
    CONSTRAINT [FK_TaxSettings_Franchise] FOREIGN KEY (FranchiseId) REFERENCES crm.Franchise(FranchiseId)
)
