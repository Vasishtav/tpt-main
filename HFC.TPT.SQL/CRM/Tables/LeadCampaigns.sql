﻿CREATE TABLE [CRM].[LeadCampaigns] (
    [LeadCampaignId]  INT      IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [MktCampaignId]   INT      NOT NULL,
    [LeadId]          INT      NOT NULL,
    [CreatedOnUtc]    DATETIME CONSTRAINT [DF_Key_LeadCampaigns_CreatedOnUtc] DEFAULT (getutcdate()) NOT NULL,
    [IsManuallyAdded] BIT      CONSTRAINT [DF_Key_LeadCampaigns_IsManuallyAdded] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_LeadCampaigns_1] PRIMARY KEY CLUSTERED ([LeadCampaignId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Key_LeadCampaigns_Leads] FOREIGN KEY ([LeadId]) REFERENCES [CRM].[Leads] ([LeadId]),
    CONSTRAINT [FK_Key_LeadCampaigns_MarketingCampaigns] FOREIGN KEY ([MktCampaignId]) REFERENCES [CRM].[MarketingCampaigns] ([MktCampaignId])
);

