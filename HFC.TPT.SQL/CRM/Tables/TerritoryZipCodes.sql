﻿CREATE TABLE [CRM].[TerritoryZipCodes] (
    [Id]                INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [TerritoryId]       INT           NOT NULL,
    [ZipCode]           VARCHAR (15)  NULL,
    [City]              VARCHAR (50)  NULL,
    [State]             VARCHAR (2)   NULL,
    [ContractStatus]    VARCHAR (5)   NULL,
    [CreatedOnUtc]      DATETIME2 (2) CONSTRAINT [DF_TerritoryZipCodes_CreatedOnUtc] DEFAULT (getutcdate()) NOT NULL,
    [AcquiredOnUtc]     DATETIME2 (2) NULL,
    [EffectiveStartUtc] DATETIME2 (2) NULL,
    [EffectiveEndUtc]   DATETIME2 (2) NULL,
    CONSTRAINT [PK_TerritoryZipCodes] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_TerritoryZipCodes_Territories] FOREIGN KEY ([TerritoryId]) REFERENCES [CRM].[Territories] ([TerritoryId])
);

