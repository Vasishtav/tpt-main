﻿CREATE TABLE [CRM].[SavedCalendarOrder] (
    [Id]             INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [LoginUserId]    INT            NULL,
    [SaveCalendarId] INT            NULL,
    [SequenceOrder]  INT            NULL,
    [ViewTitle]      NVARCHAR (255) NULL,
    [IsDefault]      INT            NULL,
    [IsDeleted]      INT            DEFAULT ((0)) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

