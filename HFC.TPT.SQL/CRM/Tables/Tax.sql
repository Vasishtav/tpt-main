﻿CREATE TABLE [CRM].[Tax] (
    [TaxId]         INT             IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [QuoteLineId]   INT             NOT NULL,
    [Code]          VARCHAR (100)   NULL,
    [Rate]          DECIMAL (18, 3) NULL,
    [Amount]        NUMERIC (18, 2) NULL,
    [QuoteKey] INT NULL,
    [FranchiseId] INT NULL,
    [Zipcode] VARCHAR(15) NULL,
    [CreatedOn]     DATETIME2 (7)   NOT NULL,
    [CreatedBy]     INT             NOT NULL,
    [LastUpdatedOn] DATETIME2 (7)   NULL,
    [LastUpdatedBy] INT             NULL,

    CONSTRAINT [PK_Tax] PRIMARY KEY CLUSTERED ([TaxId] ASC),
    CONSTRAINT [FK_Tax_QuoteLine] FOREIGN KEY ([QuoteLineId]) REFERENCES [CRM].[QuoteLines] ([QuoteLineId])
);

