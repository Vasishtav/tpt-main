﻿CREATE TABLE [CRM].[CalendarAppointments] (
    [CalendarAppointmentId] INT IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [CalendarId]            INT NOT NULL,
    [LeadId]                INT NULL,
    [AccountId]             INT NULL,
    [OpportunityId]         INT NULL,
    [OrderId]               INT NULL
);

