﻿CREATE TABLE [CRM].[CaseComments]
(
	[Id] BIGINT NOT NULL PRIMARY KEY IDENTITY, 
    [CaseId] INT NULL, 
    [PersonId] INT NULL, 
    [ParentId] INT NULL, 
    [HasChildId] BIT NULL, 
    [HTMLComment] NVARCHAR(MAX) NULL, 
    [isReply] BIT NULL, 
    [isEdit] BIT NULL, 
    [DeletedOn] DATETIME2 NULL, 
    [BelongsTo] INT NULL, 
    [CreatedOn] DATETIME2 NULL, 
    [LastUpdatedOn] DATETIME2 NULL,
	CONSTRAINT [FK_CaseComments_Case] FOREIGN KEY ([CaseId]) REFERENCES [CRM].[Case] ([CaseId])
)
