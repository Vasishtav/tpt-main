﻿CREATE TABLE [CRM].[Territories] (
    [TerritoryId]  INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [FranchiseId]  INT           NULL,
    [Name]         VARCHAR (100) NULL,
    [Code]         VARCHAR (20)  NULL,
    [CreatedOnUtc] DATETIME      CONSTRAINT [DF_Territories_CreatedOnUtc] DEFAULT (getutcdate()) NOT NULL,
    [isArrears]    BIT           NULL,
    [Minion]       VARCHAR (20)  CONSTRAINT [DF_Territories_Minion] DEFAULT ('MID0001') NOT NULL,
    [BrandId]      SMALLINT      CONSTRAINT [DF_Territories_BrandId] DEFAULT ((1)) NOT NULL,
    [WebSiteURL]   VARCHAR (100) NULL,
    [MsrReportDate] DATETIME2 NULL, 
    CONSTRAINT [PK_Territories] PRIMARY KEY CLUSTERED ([TerritoryId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Territories_Franchise] FOREIGN KEY ([FranchiseId]) REFERENCES [CRM].[Franchise] ([FranchiseId])
);

