﻿CREATE TABLE [CRM].[Disposition] (
    [DispositionId] INT           NOT NULL,
    [FranchiseId]   INT           NULL,
    [BrandId]       TINYINT       NULL,
    [LeadStatusId]  SMALLINT      NULL,
    [Name]          VARCHAR (50)  NULL,
    [Description]   VARCHAR (250) NULL,
    [CreatedOn]     DATETIME      NULL,
    [Createdby]     INT           NULL,
    [UpdatedBy]     INT           NULL,
    [UpdatedON]     DATETIME      NULL,
    CONSTRAINT [PK_Disposition] PRIMARY KEY CLUSTERED ([DispositionId] ASC),
    CONSTRAINT [FK_Disposition_BrandId] FOREIGN KEY ([BrandId]) REFERENCES [CRM].[Type_Brand] ([BrandID]),
    CONSTRAINT [FK_Disposition_Franchise] FOREIGN KEY ([FranchiseId]) REFERENCES [CRM].[Franchise] ([FranchiseId]),
    CONSTRAINT [FK_Disposition_LeadStatus] FOREIGN KEY ([LeadStatusId]) REFERENCES [CRM].[Type_LeadStatus] ([Id])
);

