﻿CREATE TABLE [CRM].[Leads] (
    [LeadId]                   INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [LeadGuid]                 UNIQUEIDENTIFIER CONSTRAINT [DF_Leads_LeadGuid] DEFAULT (newid()) NOT NULL,
    [LeadNumber]               INT              CONSTRAINT [DF_Leads_LeadNumber] DEFAULT ((0)) NOT NULL,
    [LeadStatusId]             SMALLINT         CONSTRAINT [DF_Leads_LeadStatusId] DEFAULT ((1)) NOT NULL,
    [FranchiseId]              INT              CONSTRAINT [DF_Leads_TerritoryId] DEFAULT ((1)) NULL,
    [TerritoryId]              INT              NULL,
    [PersonId]                 INT              NULL,
    [SecPersonId]              INT              NULL,
    [IsDeleted]                BIT              CONSTRAINT [DF_Leads_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOnUtc]             DATETIME2 (2)    CONSTRAINT [DF_Leads_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ReferringCustomerId]      VARCHAR (50)     NULL,
    [CreatedByPersonId]        INT              NULL,
    [LastUpdatedOnUtc]         DATETIME2 (2)    CONSTRAINT [DF_Leads_LastUpdatedOnUtc] DEFAULT (getdate()) NULL,
    [LastUpdatedByPersonId]    INT              NULL,
    [Notes]                    NVARCHAR (MAX)   NULL,
    [DispositionId]            INT              NULL,
    [SalesStepId]              INT              CONSTRAINT [DF__Leads__SalesStep__026F9E2B] DEFAULT ((1)) NULL,
    [TempCustomerId]           INT              NULL,
    [CampaignId]               INT              NULL,
    [RelatedAccountId]         INT              NULL,
    [ReferralTypeId]           INT              NULL,
    [AccountName]              VARCHAR (500)    NULL,
    [LeadTerritory]            NVARCHAR (500)   NULL,
    [AllowFranchisetoReassign] BIT              DEFAULT ((1)) NULL,
    [LeadType]                 VARCHAR (100)    NULL,
    [TerritoryType]            VARCHAR (100)    NULL,
    [KeyAcct]                  NVARCHAR (100)   NULL,
    [LegacyAccount]            BIT              NULL,
    [LegacyMemo]               NVARCHAR (256)   NULL,
    [IsCommercial]             BIT              NULL,
    [CommercialTypeId]         INT              NULL,
    [RelationshipTypeId]       INT              NULL,
    [IndustryId]               INT              NULL,
    [OtherIndustry]            NVARCHAR (500)   NULL,
    [IsTaxExempt]              BIT              NULL,
    [TaxExemptID]              NVARCHAR (25)    NULL,
    [HFCUTMCampaignCode]       VARCHAR (200)    NULL,
    [LMDBSourceID]             INT              NULL,
    [SourcesHFCId]             INT              NULL,
    [ImpersonatorPersonId]     INT              NULL,
    [EmailSent]                BIT              NULL,
    [BrandId] INT NULL, 
    [TerritoryDisplayId] INT NULL, 
    [IsNotifyemails] BIT NULL , 
    [IsNotifyText] BIT NULL, 
    CONSTRAINT [PK_Leads] PRIMARY KEY CLUSTERED ([LeadId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_CRM_Leads_IndustryId] FOREIGN KEY ([IndustryId]) REFERENCES [CRM].[Type_LookUpValues] ([Id]),
    CONSTRAINT [FK_Leads_CreatorPerson] FOREIGN KEY ([CreatedByPersonId]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_Leads_Customers] FOREIGN KEY ([PersonId]) REFERENCES [CRM].[Customer] ([CustomerId]),
    CONSTRAINT [FK_Leads_Franchise] FOREIGN KEY ([FranchiseId]) REFERENCES [CRM].[Franchise] ([FranchiseId]),
    CONSTRAINT [FK_Leads_LastUpdatedPerson] FOREIGN KEY ([LastUpdatedByPersonId]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_Leads_SecPerson] FOREIGN KEY ([SecPersonId]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_Leads_Territories] FOREIGN KEY ([TerritoryId]) REFERENCES [CRM].[Territories] ([TerritoryId]),
    CONSTRAINT [FK_Leads_Type_LeadStatus] FOREIGN KEY ([LeadStatusId]) REFERENCES [CRM].[Type_LeadStatus] ([Id]),
    CONSTRAINT [Lead_ReferralTypeId_Type_Referral_Id] FOREIGN KEY ([ReferralTypeId]) REFERENCES [CRM].[Type_Referral] ([Id]),
    CONSTRAINT [Lead_RelatedAccountId_Accounts_AccountId] FOREIGN KEY ([RelatedAccountId]) REFERENCES [CRM].[Accounts] ([AccountId])
);


GO
CREATE NONCLUSTERED INDEX [FranchiseId_PersonId]
    ON [CRM].[Leads]([FranchiseId] ASC, [IsDeleted] ASC)
    INCLUDE([LeadId], [LeadNumber]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [ix_LeadStatusId]
    ON [CRM].[Leads]([LeadStatusId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [ix_leadGUID]
    ON [CRM].[Leads]([LeadGuid] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [Leads_Index_FranchiseId_LeadId_PersonId]
    ON [CRM].[Leads]([FranchiseId] ASC)
    INCLUDE([LeadId], [PersonId]) WITH (FILLFACTOR = 90);


GO

CREATE TRIGGER [CRM].[LeadStatusTrackerTrigger] ON [CRM].[Leads]
FOR INSERT, UPDATE
AS
     BEGIN
       IF UPDATE(LeadStatusId)  AND NOT EXISTS(SELECT 1 FROM DELETED d INNER JOIN INSERTED i on i.LeadStatusId = d.LeadStatusId)
             BEGIN
                 INSERT INTO CRM.LeadStatusTracker
                 ([LeadId],
                  [LeadStatusId],
                  [CreatedOn],
                  [CreatedBy]
                 )
                        SELECT Leadid,
                               LeadStatusId,
                               GETUTCDATE(),
                               CreatedByPersonId
                        FROM inserted;
         END;
            
     END;
