﻿CREATE TABLE [CRM].[OpportunityStatusTracker] (
    [OpportunityTrackerId] INT      IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [OpportunityId]        INT      NOT NULL,
    [OpportunityStatusId]  TINYINT  NOT NULL,
    [CreatedOn]            DATETIME NOT NULL,
    [CreatedBy]            INT      NULL,
    CONSTRAINT [FK_Opportunities_OpportunityTracker] FOREIGN KEY ([OpportunityId]) REFERENCES [CRM].[Opportunities] ([OpportunityId]),
    CONSTRAINT [FK_OpportunityTracker_Type_OpportunityStatus] FOREIGN KEY ([OpportunityStatusId]) REFERENCES [CRM].[Type_OpportunityStatus] ([OpportunityStatusId])
);

