﻿CREATE TABLE [CRM].[AppConfig] (
    [ConfigId]              INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Name]                  VARCHAR (50)   NOT NULL,
    [Value]                 VARCHAR (256)  NULL,
    [GroupName]             VARCHAR (50)   NOT NULL,
    [SubGroupName]          VARCHAR (50)   NULL,
    [AvailableValueOptions] VARCHAR (1000) NULL,
    [CreatedOnUtc]          DATETIME       CONSTRAINT [DF_AppConfig_CreatedOnUtc] DEFAULT (getutcdate()) NOT NULL,
    [Description]           VARCHAR (1000) NULL,
    [FranchiseId]           INT            NULL,
    [Inheritable]           BIT            CONSTRAINT [DF_AppConfig_Inheritable] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_AppConfig] PRIMARY KEY CLUSTERED ([ConfigId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_AppConfig_Franchise] FOREIGN KEY ([FranchiseId]) REFERENCES [CRM].[Franchise] ([FranchiseId])
);

