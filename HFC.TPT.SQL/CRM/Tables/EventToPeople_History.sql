﻿CREATE TABLE [CRM].[EventToPeople_History] (
    [EventToPeople_HistoryID] INT IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [TaskId]                  INT NULL,
    [CalendarId]              INT NULL,
    [PersonId]                INT NULL,
    [IsGoogleSynced]          BIT NULL,
    [IsExchangeSynced]        BIT NULL,
    CONSTRAINT [PK_EventToPeople_History_1] PRIMARY KEY CLUSTERED ([EventToPeople_HistoryID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_EventToPeople_History_Calendar] FOREIGN KEY ([CalendarId]) REFERENCES [CRM].[Calendar] ([CalendarId]) ON DELETE CASCADE,
    CONSTRAINT [FK_EventToPeople_History_Person] FOREIGN KEY ([PersonId]) REFERENCES [CRM].[Person] ([PersonId])
);

