﻿CREATE TABLE [CRM].[OpportunitySources] (
    [OpportunitySourceId] INT      IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [SourceId]            INT      NOT NULL,
    [OpportunityId]       INT      NOT NULL,
    [CreatedOnUtc]        DATETIME CONSTRAINT [DF_Key_OpportunitySource_CreatedOnUtc] DEFAULT (getutcdate()) NOT NULL,
    [IsManuallyAdded]     BIT      CONSTRAINT [DF_Key_OpportunitySource_IsManuallyAdded] DEFAULT ((0)) NOT NULL,
    [CreatedBy]           INT      NULL,
    [UpdatedOn]           DATETIME NULL,
    [UpdatedBy]           INT      NULL,
    [IsPrimarySource]     BIT      DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Key_OpportunitySource_1] PRIMARY KEY CLUSTERED ([OpportunitySourceId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Key_OpportunitySource_Opportunitys] FOREIGN KEY ([OpportunityId]) REFERENCES [CRM].[Opportunities] ([OpportunityId]),
    CONSTRAINT [FK_OpportunitySources_SourcesTP] FOREIGN KEY ([SourceId]) REFERENCES [CRM].[SourcesTP] ([SourcesTPId]),
    CONSTRAINT [UX_OpportunitySource_OpportunityIDSourceID] UNIQUE NONCLUSTERED ([SourceId] ASC, [OpportunityId] ASC) WITH (FILLFACTOR = 90)
);

