﻿CREATE TABLE [CRM].[Type_LookUpValues] (
    [Id]            INT           NOT NULL,
    [TableId]       INT           NOT NULL,
    [Name]          VARCHAR (200) NOT NULL,
    [IsActive]      BIT           NOT NULL,
    [IsDeleted]     BIT           NULL,
    [CreatedOn]     DATETIME2 (7) NOT NULL,
    [CreatedBy]     INT           NOT NULL,
    [LastUpdatedOn] DATETIME2 (7) NULL,
    [LastUpdatedBy] INT           NULL,
    [SortOrder] INT NULL, 
    CONSTRAINT [PK_Type_LookUpValues] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Type_LookUpValues_Createdby] FOREIGN KEY ([CreatedBy]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_Type_LookUpValues_Modifiedby] FOREIGN KEY ([LastUpdatedBy]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_Type_LookUpValues_TableId] FOREIGN KEY ([TableId]) REFERENCES [CRM].[Type_LookUpTable] ([Id])
);

