﻿CREATE TABLE [CRM].[Campaign] (
    [CampaignId]    INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Name]          VARCHAR (80)   NULL,
    [Description]   VARCHAR (500)  NULL,
    [StartDate]     DATETIME       NULL,
    [EndDate]       DATETIME       NULL,
    [IsDeleted]     BIT            NULL,
    [IsActive]      BIT            NULL,
    [BrandId]       TINYINT        NULL,
    [FranchiseId]   INT            NULL,
    [CreatedOn]     DATETIME       NULL,
    [CreatedBy]     INT            NULL,
    [LastUpdatedOn] DATETIME       NULL,
    [LastUpdatedBy] INT            NULL,
    [SourcesTPId]   INT            NULL,
    [Amount]        DECIMAL (18, 2) NULL,
    [Memo]          VARCHAR (500)  NULL,
    [Duration]      INT             NOT NULL DEFAULT 0,
    [RecurringType]     INT             NOT NULL DEFAULT 0,
    [DeactivateOnEndDate]   bit     NOT NULL DEFAULT 0,
    CONSTRAINT [PK_Campaign] PRIMARY KEY CLUSTERED ([CampaignId] ASC)
);

