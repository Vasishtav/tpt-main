﻿CREATE TABLE [CRM].[JobQuestionAnswers] (
    [AnswerId]         INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [JobId]            INT           NOT NULL,
    [QuestionId]       INT           NOT NULL,
    [Answer]           VARCHAR (256) NULL,
    [CreatedOnUtc]     DATETIME      CONSTRAINT [DF_QAAnswers_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [LoggedByPersonId] INT           NULL,
    [LastUpdatedOnUtc] DATETIME      NULL,
    [LeadId]           INT           NULL,
    CONSTRAINT [PK_QAAnswers] PRIMARY KEY CLUSTERED ([AnswerId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_QAAnswers_Jobs] FOREIGN KEY ([JobId]) REFERENCES [CRM].[Jobs] ([JobId]),
    CONSTRAINT [FK_QAAnswers_Type_QAQuestions] FOREIGN KEY ([QuestionId]) REFERENCES [CRM].[Type_Questions] ([QuestionId])
);

