﻿CREATE TABLE [CRM].[WebOEJobItems] (
    [JobItemId]    INT            NOT NULL,
    [WebOEItemId]  INT            NOT NULL,
    [LineNumber]   SMALLINT       NOT NULL,
    [Description2] VARCHAR (1024) NULL,
    CONSTRAINT [PK_WebOEJobItems] PRIMARY KEY CLUSTERED ([JobItemId] ASC, [WebOEItemId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_JobItems_WebOEJobItems] FOREIGN KEY ([JobItemId]) REFERENCES [CRM].[JobItems] ([JobItemId])
);

