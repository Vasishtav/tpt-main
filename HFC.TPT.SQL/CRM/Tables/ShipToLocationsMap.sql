﻿CREATE TABLE [CRM].[ShipToLocationsMap] (
    [Id]             INT IDENTITY (1, 1) NOT NULL,
    [FranchiseId]    INT NOT NULL,
    [TerritoryId]    INT NULL,
    [ShipToLocation] INT NULL,
    CONSTRAINT [Pk_ShipToLocationsMap] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [Fk_Franchise_ShipToLocationsMap_FranchiseId] FOREIGN KEY ([FranchiseId]) REFERENCES [CRM].[Franchise] ([FranchiseId]),
    CONSTRAINT [Fk_Territories_ShipToLocations_ShipToLocation] FOREIGN KEY ([ShipToLocation]) REFERENCES [CRM].[ShipToLocations] ([Id]),
    CONSTRAINT [Fk_Territories_ShipToLocationsMap_TerritoryId] FOREIGN KEY ([TerritoryId]) REFERENCES [CRM].[Territories] ([TerritoryId])
);

