﻿CREATE TABLE [CRM].[Type_LookUpTable] (
    [Id]            INT           IDENTITY (1, 1) NOT NULL,
    [TableName]     VARCHAR (200) NOT NULL,
    [IsActive]      BIT           NOT NULL,
    [IsDeleted]     BIT           NULL,
    [CreatedOn]     DATETIME2 (7) NOT NULL,
    [CreatedBy]     INT           NOT NULL,
    [LastUpdatedOn] DATETIME2 (7) NULL,
    [LastUpdatedBy] INT           NULL,
    CONSTRAINT [PK_Type_LookUpTable] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Type_LookUpTable_Createdby] FOREIGN KEY ([CreatedBy]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_Type_LookUpTable_Modifiedby] FOREIGN KEY ([LastUpdatedBy]) REFERENCES [CRM].[Person] ([PersonId])
);

