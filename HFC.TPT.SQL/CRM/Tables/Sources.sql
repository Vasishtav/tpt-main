﻿CREATE TABLE [CRM].[Sources] (
    [SourceId]    INT                IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Name]        VARCHAR (50)       NOT NULL,
    [Description] VARCHAR (250)      NULL,
    [ParentId]    INT                NULL,
    [FranchiseId] INT                NULL,
    [StartDate]   DATETIMEOFFSET (2) NOT NULL,
    [EndDate]     DATETIMEOFFSET (2) NULL,
    [Amount]      DECIMAL (9, 4)     CONSTRAINT [DF_SourceCampaigns_Amount] DEFAULT ((0)) NULL,
    [CreatedOn]   DATETIMEOFFSET (2) CONSTRAINT [DF_Source_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [IsActive]    BIT                CONSTRAINT [DF_SourceCampaigns_IsActive] DEFAULT ((1)) NOT NULL,
    [Memo]        VARCHAR (1000)     NULL,
    [isDeleted]   BIT                NULL,
    [isCampaign]  BIT                NULL,
    [CreatedBy]   INT                NULL,
    [UpdatedOn]   DATETIME           NULL,
    [UpdatedBy]   INT                NULL,
    CONSTRAINT [PK_Sources] PRIMARY KEY CLUSTERED ([SourceId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Source_Franchise] FOREIGN KEY ([FranchiseId]) REFERENCES [CRM].[Franchise] ([FranchiseId]),
    CONSTRAINT [FK_Source_Source] FOREIGN KEY ([ParentId]) REFERENCES [CRM].[Sources] ([SourceId])
);

