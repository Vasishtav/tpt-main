﻿CREATE TABLE [CRM].[FranchiseRoyalties] (
    [RoyaltyId]      INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Name]           VARCHAR (50)   NULL,
    [Amount]         DECIMAL (9, 4) NULL,
    [Percent]        DECIMAL (9, 4) NULL,
    [StartOnUtc]     DATETIME2 (2)  NULL,
    [EndOnUtc]       DATETIME2 (2)  NULL,
    [TerritoryId]    INT            NOT NULL,
    [Type_RoyaltyId] INT            NULL,
    CONSTRAINT [PK_Royalties] PRIMARY KEY CLUSTERED ([RoyaltyId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_FranchiseRoyalties_Territories] FOREIGN KEY ([TerritoryId]) REFERENCES [CRM].[Territories] ([TerritoryId]),
    CONSTRAINT [FK_FranchiseRoyalties_Type_Royalty] FOREIGN KEY ([Type_RoyaltyId]) REFERENCES [CRM].[Type_Royalty] ([Id])
);

