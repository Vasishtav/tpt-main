﻿CREATE TABLE [CRM].[Type_RoyaltyProfile] (
    [Id]      INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Name]    VARCHAR (500) NULL,
    [BrandId] SMALLINT      NULL,
    CONSTRAINT [PK_Type_RoyaltyProfile] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Type_RoyaltyProfile_Type_RoyaltyProfile1] FOREIGN KEY ([Id]) REFERENCES [CRM].[Type_RoyaltyProfile] ([Id])
);

