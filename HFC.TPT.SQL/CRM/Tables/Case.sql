﻿CREATE TABLE [CRM].[Case]
(
	[CaseId] INT NOT NULL PRIMARY KEY IDENTITY, 
    [CaseNumber] NVARCHAR(15) NULL, 
    [CaseTypeId] INT NULL, 
    [CaseType] NVARCHAR(256) NULL, 
    [FranchiseId] INT NULL, 
    [VendorId] INT NULL, 
    [Details] NVARCHAR(MAX) NULL, 
    [StatusId] INT NULL, 
    [CreatedOn] DATETIME2 NULL, 
    [CreatedBy] INT NULL, 
    [LastUpdatedOn] DATETIME2 NULL, 
    [LastUpdatedBy] INT NULL
)
