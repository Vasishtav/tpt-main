﻿CREATE TABLE [CRM].[JobItems] (
    [JobItemId]             INT                IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [QuoteId]               INT                NOT NULL,
    [Quantity]              SMALLINT           CONSTRAINT [DF_JobItems_Quantity] DEFAULT ((0)) NOT NULL,
    [StyleId]               INT                NULL,
    [Style]                 VARCHAR (50)       NULL,
    [CategoryId]            INT                NULL,
    [CategoryName]          VARCHAR (50)       NULL,
    [ProductTypeId]         INT                NULL,
    [ProductType]           VARCHAR (50)       NULL,
    [ProductName]           VARCHAR (100)      NULL,
    [ProductGuid]           UNIQUEIDENTIFIER   NULL,
    [Description]           VARCHAR (1024)     NULL,
    [Color]                 VARCHAR (100)      NULL,
    [ManufacturerId]        INT                NULL,
    [Manufacturer]          VARCHAR (50)       NULL,
    [IsTaxable]             BIT                CONSTRAINT [DF_JobItems_IsTaxable] DEFAULT ((0)) NOT NULL,
    [SalePrice]             DECIMAL (18, 4)    CONSTRAINT [DF_JobItems_SalePrice] DEFAULT ((0)) NOT NULL,
    [ListPrice]             DECIMAL (18, 4)    CONSTRAINT [DF_JobItems_ItemPrice] DEFAULT ((0)) NOT NULL,
    [MSRP]                  DECIMAL (18, 4)    CONSTRAINT [DF_JobItems_ProductMSRP] DEFAULT ((0)) NOT NULL,
    [JobberPrice]           DECIMAL (18, 4)    CONSTRAINT [DF_JobItems_UnitBaseCost] DEFAULT ((0)) NOT NULL,
    [JobberDiscountPercent] DECIMAL (9, 4)     CONSTRAINT [DF_JobItems_CostDiscountPercent] DEFAULT ((0)) NOT NULL,
    [UnitCost]              DECIMAL (18, 4)    CONSTRAINT [DF_JobItems_ItemCost] DEFAULT ((0)) NOT NULL,
    [DiscountAmount]        DECIMAL (18, 4)    CONSTRAINT [DF_JobItems_DiscountAmount] DEFAULT ((0)) NOT NULL,
    [DiscountPercent]       DECIMAL (9, 4)     CONSTRAINT [DF_JobItems_DiscountPercent] DEFAULT ((0)) NOT NULL,
    [DiscountRemark]        VARCHAR (256)      NULL,
    [MarginPercent]         DECIMAL (9, 4)     NULL,
    [CostSubtotal]          DECIMAL (18, 4)    CONSTRAINT [DF_JobItems_CostSubtotal] DEFAULT ((0)) NOT NULL,
    [Subtotal]              DECIMAL (18, 4)    CONSTRAINT [DF_JobItems_Subtotal] DEFAULT ((0)) NOT NULL,
    [CreatedByPersonId]     INT                NULL,
    [CreatedOnUtc]          DATETIME2 (2)      CONSTRAINT [DF_JobItems_CreatedOnUtc] DEFAULT (getutcdate()) NOT NULL,
    [LastUpdated]           DATETIMEOFFSET (2) CONSTRAINT [DF_JobItems_LastUpdatedUtc] DEFAULT (getutcdate()) NOT NULL,
    [LastUpdatedPersonId]   INT                NULL,
    [isCustom]              BIT                NULL,
    [SortOrder]             INT                NULL,
    [DiscountType]          VARCHAR (50)       NULL,
    [JobberPriceOverriden]  BIT                NULL,
    [SessionId]             INT                NULL,
    [IsSavedWithErrors]     BIT                NULL,
    [Notes]                 VARCHAR (1024)     NULL,
    CONSTRAINT [PK_JobItems] PRIMARY KEY CLUSTERED ([JobItemId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_JobItems_JobQuotes] FOREIGN KEY ([QuoteId]) REFERENCES [CRM].[JobQuotes] ([QuoteId]),
    CONSTRAINT [FK_JobItems_Person] FOREIGN KEY ([LastUpdatedPersonId]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_JobItems_Person1] FOREIGN KEY ([CreatedByPersonId]) REFERENCES [CRM].[Person] ([PersonId])
);


GO
CREATE NONCLUSTERED INDEX [QuoteId]
    ON [CRM].[JobItems]([QuoteId] ASC)
    INCLUDE([JobItemId], [Quantity], [StyleId], [Style], [CategoryId], [CategoryName], [ProductTypeId], [ProductType], [ProductName], [ProductGuid], [Description], [Color], [ManufacturerId], [Manufacturer], [IsTaxable], [SalePrice], [ListPrice], [MSRP], [JobberPrice], [JobberDiscountPercent], [UnitCost], [DiscountAmount], [DiscountPercent], [DiscountRemark], [MarginPercent], [CostSubtotal], [Subtotal], [CreatedByPersonId], [CreatedOnUtc], [LastUpdated], [LastUpdatedPersonId]) WITH (FILLFACTOR = 90);

