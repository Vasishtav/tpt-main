﻿CREATE TABLE [CRM].[ProductStyles] (
    [Id]                    INT                IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ProductId]             INT                NOT NULL,
    [StyleId]               SMALLINT           NOT NULL,
    [FranchiseId]           INT                NULL,
    [JobberPrice]           DECIMAL (18, 4)    NULL,
    [JobberDiscountPercent] DECIMAL (9, 4)     NULL,
    [MarginPercent]         DECIMAL (9, 4)     NULL,
    [IsActive]              BIT                CONSTRAINT [DF_ProductStyles_IsActive] DEFAULT ((1)) NOT NULL,
    [IsTaxable]             BIT                CONSTRAINT [DF_ProductStyles_IsTaxable] DEFAULT ((1)) NOT NULL,
    [CreatedOn]             DATETIMEOFFSET (2) CONSTRAINT [DF_ProductStyles_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [isCustom]              BIT                NULL,
    CONSTRAINT [PK_ProductStyles] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ProductStyles_Franchise] FOREIGN KEY ([FranchiseId]) REFERENCES [CRM].[Franchise] ([FranchiseId]),
    CONSTRAINT [FK_ProductStyles_Products] FOREIGN KEY ([ProductId]) REFERENCES [CRM].[Products] ([ProductId]),
    CONSTRAINT [FK_ProductStyles_Type_Styles] FOREIGN KEY ([StyleId]) REFERENCES [CRM].[Type_Styles] ([Id])
);

