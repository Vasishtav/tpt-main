﻿CREATE TABLE [CRM].[AccountCampaigns] (
    [AccountCampaignId] INT      IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [MktCampaignId]     INT      NOT NULL,
    [AccountId]         INT      NOT NULL,
    [CreatedOnUtc]      DATETIME CONSTRAINT [DF_Key_AccountCampaigns_CreatedOnUtc] DEFAULT (getutcdate()) NOT NULL,
    [IsManuallyAdded]   BIT      CONSTRAINT [DF_Key_AccountCampaigns_IsManuallyAdded] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_AccountCampaigns_1] PRIMARY KEY CLUSTERED ([AccountCampaignId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Key_AccountCampaigns_Accounts] FOREIGN KEY ([AccountId]) REFERENCES [CRM].[Accounts] ([AccountId]),
    CONSTRAINT [FK_Key_AccountCampaigns_MarketingCampaigns] FOREIGN KEY ([MktCampaignId]) REFERENCES [CRM].[MarketingCampaigns] ([MktCampaignId])
);

