﻿CREATE TABLE [CRM].[Calendar] (
    [CalendarId]            INT                IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [CalendarGuid]          UNIQUEIDENTIFIER   CONSTRAINT [DF_Calendar_CalendarGuid] DEFAULT (newid()) NOT NULL,
    [Subject]               VARCHAR (256)      NULL,
    [Message]               VARCHAR (MAX)      NULL,
    [LeadId]                INT                NULL,
    [LeadNumber]            INT                NULL,
    [StartDate]             DATETIMEOFFSET (2) NOT NULL,
    [EndDate]               DATETIMEOFFSET (2) NOT NULL,
    [EventTypeEnum]         TINYINT            CONSTRAINT [DF_Calendar_EventType] DEFAULT ((0)) NOT NULL,
    [AptTypeEnum]           TINYINT            CONSTRAINT [DF_Calendar_AptTypeEnum] DEFAULT ((1)) NOT NULL,
    [IsCancelled]           BIT                NULL,
    [CreatedByPersonId]     INT                NULL,
    [CreatedOnUtc]          DATETIME2 (2)      CONSTRAINT [DF_Calendar_CreatedOnUtc] DEFAULT (getutcdate()) NOT NULL,
    [IsAllDay]              BIT                CONSTRAINT [DF_Calendar_IsAllDay] DEFAULT ((0)) NOT NULL,
    [LastUpdatedUtc]        DATETIME2 (2)      CONSTRAINT [DF_Calendar_LastUpdatedUtc] DEFAULT (getutcdate()) NOT NULL,
    [IsDeleted]             BIT                CONSTRAINT [DF_Calendar_IsDeleted] DEFAULT ((0)) NOT NULL,
    [LastUpdatedByPersonId] INT                NULL,
    [OrganizerPersonId]     INT                NULL,
    [OrganizerEmail]        VARCHAR (256)      NULL,
    [OrganizerName]         VARCHAR (128)      NULL,
    [FranchiseId]           INT                NOT NULL,
    [RecurringEventId]      INT                NULL,
    [IsPrivate]             BIT                NULL,
    [Location]              VARCHAR (1024)     NULL,
    [ReminderMinute]        SMALLINT           CONSTRAINT [DF_Calendar_ReminderMinute] DEFAULT ((0)) NOT NULL,
    [RemindMethodEnum]      TINYINT            NULL,
    [FirstRemindDate]       DATETIMEOFFSET (2) NULL,
    [RevisionSequence]      INT                CONSTRAINT [DF_Calendar_RevisionSequence] DEFAULT ((0)) NOT NULL,
    [JobId]                 INT                NULL,
    [JobNumber]             INT                NULL,
    [AssignedPersonId]      INT                NULL,
    [AssignedName]          VARCHAR (128)      NULL,
    [PhoneNumber]           NVARCHAR (255)     NULL,
    [AccountId]             INT                NULL,
    [OpportunityId]         INT                NULL,
    [OrderId]               INT                NULL,
    [CaseId]                INT                NULL,
    [VendorCaseId]          INT                NULL,
    CONSTRAINT [PK_Calendar] PRIMARY KEY CLUSTERED ([CalendarId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Calendar_Calendar_Recurring] FOREIGN KEY ([RecurringEventId]) REFERENCES [CRM].[EventRecurring] ([RecurringEventId]),
    CONSTRAINT [FK_Calendar_Creator] FOREIGN KEY ([CreatedByPersonId]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_Calendar_Franchise] FOREIGN KEY ([FranchiseId]) REFERENCES [CRM].[Franchise] ([FranchiseId]),
    CONSTRAINT [FK_Calendar_Jobs] FOREIGN KEY ([JobId]) REFERENCES [CRM].[Jobs] ([JobId]),
    CONSTRAINT [FK_Calendar_Leads] FOREIGN KEY ([LeadId]) REFERENCES [CRM].[Leads] ([LeadId]),
    CONSTRAINT [FK_Calendar_Person] FOREIGN KEY ([OrganizerPersonId]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_Calendar_PersonAssigned] FOREIGN KEY ([AssignedPersonId]) REFERENCES [CRM].[Person] ([PersonId])
);


GO
CREATE NONCLUSTERED INDEX [AssignedPersonId]
    ON [CRM].[Calendar]([OrganizerPersonId] ASC, [EventTypeEnum] ASC)
    INCLUDE([CalendarId], [LastUpdatedUtc], [IsDeleted], [RecurringEventId], [CreatedOnUtc]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_IsDeleted_FranchiseId_JobId_EventTypeEnum]
    ON [CRM].[Calendar]([IsDeleted] ASC, [FranchiseId] ASC, [JobId] ASC, [EventTypeEnum] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_EventTypeEnum_FranchiseId_JobId_IsDeleted]
    ON [CRM].[Calendar]([EventTypeEnum] ASC, [FranchiseId] ASC, [JobId] ASC, [IsDeleted] ASC) WITH (FILLFACTOR = 90);

