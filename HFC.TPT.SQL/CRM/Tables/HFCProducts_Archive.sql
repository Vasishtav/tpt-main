﻿CREATE TABLE [CRM].[HFCProducts_Archive](
	[Id] [int] IDENTITY(1,1),
	[ProductKey] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[ProductName] [varchar](500) NULL,
	[PICProductID] [int] NULL,
	[VendorID] [int] NULL,
	[VendorName] [varchar](150) NULL,
	[VendorProductSKU] [varchar](100) NULL,
	[Description] [varchar](500) NULL,
	[ProductCategory] [int] NULL,
	[ProductCollection] [varchar](100) NULL,
	[ProductModelID] [varchar](100) NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastUpdatedOn] [datetime2](7) NULL,
	[LastUpdatedBy] [int] NULL,
	[ProductGroup] [varchar](50) NULL,
	[ProductGroupDesc] [varchar](max) NULL,
	[Active] [bit] NULL,
	[DiscontinuedDate] [datetime2](7) NULL,
	[PhasedoutDate] [datetime2](7) NULL,
	[TS_product] [datetime2](7) NULL,
	[ArchiveInsertedOn] [datetime2](7) default(Getutcdate()) NULL,
 CONSTRAINT [PK_HFCProducts_Archive] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)
)
