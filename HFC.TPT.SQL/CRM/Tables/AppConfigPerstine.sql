﻿CREATE TABLE [CRM].[AppConfigPerstine] (
    [Name]                  VARCHAR (50)   NOT NULL,
    [Value]                 VARCHAR (256)  NULL,
    [GroupName]             VARCHAR (50)   NOT NULL,
    [SubGroupName]          VARCHAR (50)   NULL,
    [AvailableValueOptions] VARCHAR (1000) NULL,
    [Description]           VARCHAR (1000) NULL,
    [FranchiseId]           INT            NULL,
    [Inheritable]           BIT            NOT NULL
);

