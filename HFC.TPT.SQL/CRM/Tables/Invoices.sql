﻿CREATE TABLE [CRM].[Invoices] (
    [InvoiceId]     INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [OrderID]       INT              NOT NULL,
    [InvoiceType]   VARCHAR (100)    NOT NULL,
    [InvoiceDate]   DATETIME2 (7)    NULL,
    [TotalAmount]   NUMERIC (18, 2)  NULL,
    [Balance]       NUMERIC (18, 2)  NULL,
    [Streamid]      UNIQUEIDENTIFIER NULL,
    [CreatedOn]     DATETIME2 (7)    NOT NULL,
    [CreatedBy]     INT              NOT NULL,
    [LastUpdatedOn] DATETIME2 (7)    NULL,
    [LastUpdatedBy] INT              NULL,
    CONSTRAINT [PK_Invoices] PRIMARY KEY CLUSTERED ([InvoiceId] ASC),
    CONSTRAINT [FK_Invoices_Createdby] FOREIGN KEY ([CreatedBy]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_Invoices_Modifiedby] FOREIGN KEY ([LastUpdatedBy]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_Invoices_Orders] FOREIGN KEY ([OrderID]) REFERENCES [CRM].[Orders] ([OrderID])
);

