﻿CREATE TABLE [CRM].[Type_Brand] (
    [BrandID]     TINYINT       NOT NULL,
    [Name]        VARCHAR (50)  NOT NULL,
    [Code]        VARCHAR (5)   NOT NULL,
    [Description] VARCHAR (250) NULL,
    [CreatedBy]   INT           NOT NULL,
    [CreatedOn]   DATETIME      NOT NULL,
    [UpdatedBy]   INT           NULL,
    [UpdatedOn]   DATETIME      NULL,
    CONSTRAINT [PK_Brand_1] PRIMARY KEY CLUSTERED ([BrandID] ASC)
);

