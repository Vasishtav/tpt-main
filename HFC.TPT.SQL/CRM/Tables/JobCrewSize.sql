﻿CREATE TABLE [CRM].[JobCrewSize]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [BrandId] INT NOT NULL, 
    [HTML] NVARCHAR(MAX) NOT NULL, 
    [CreatedOn] DATETIME2 NULL, 
    [CreatedBy] INT NULL, 
    [LastUpdatedOn] DATETIME2 NULL, 
    [LastUpdatedBy] INT NULL
)
