﻿CREATE TABLE [CRM].[JobSaleAdjustments] (
    [SaleAdjustmentId]       INT                IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [QuoteId]                INT                NOT NULL,
    [TypeEnum]               TINYINT            CONSTRAINT [DF_JobSaleAdjustments_TypeEnum] DEFAULT ((0)) NOT NULL,
    [Category]               VARCHAR (50)       NULL,
    [Amount]                 DECIMAL (18, 4)    CONSTRAINT [DF_SaleAdjustments_Amount] DEFAULT ((0)) NOT NULL,
    [Percent]                DECIMAL (9, 4)     CONSTRAINT [DF_JobSaleAdjustments_Percent] DEFAULT ((0)) NOT NULL,
    [Memo]                   VARCHAR (256)      NULL,
    [IsDebit]                BIT                CONSTRAINT [DF_SaleAdjustments_Type] DEFAULT ((1)) NOT NULL,
    [IsTaxable]              BIT                CONSTRAINT [DF_SaleAdjustments_IsTaxable] DEFAULT ((0)) NOT NULL,
    [CreatedOnUtc]           DATETIME2 (2)      CONSTRAINT [DF_SaleAdjustments_CreatedOnUtc] DEFAULT (getutcdate()) NOT NULL,
    [LastUpdated]            DATETIMEOFFSET (2) CONSTRAINT [DF_JobSaleAdjustments_LastUpdatedOnUtc] DEFAULT (getutcdate()) NOT NULL,
    [ApplyDiscountBeforeTax] BIT                NULL,
    CONSTRAINT [PK_SaleAdjustments] PRIMARY KEY CLUSTERED ([SaleAdjustmentId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_JobSaleAdjustments_JobQuotes] FOREIGN KEY ([QuoteId]) REFERENCES [CRM].[JobQuotes] ([QuoteId])
);


GO
CREATE NONCLUSTERED INDEX [QuoteId]
    ON [CRM].[JobSaleAdjustments]([QuoteId] ASC)
    INCLUDE([SaleAdjustmentId], [TypeEnum], [Category], [Amount], [Percent], [Memo], [IsDebit], [IsTaxable], [CreatedOnUtc], [LastUpdated]) WITH (FILLFACTOR = 90);

