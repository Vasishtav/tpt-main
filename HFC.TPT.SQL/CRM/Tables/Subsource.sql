﻿CREATE TABLE [CRM].[Subsource] (
    [SubsourceId] INT           NOT NULL,
    [Name]        VARCHAR (50)  NULL,
    [Description] VARCHAR (250) NULL,
    [SourceId]    INT           NULL,
    [FranchiseId] INT           NULL,
    [BrandId]     TINYINT       NULL,
    [IsActive]    BIT           NULL,
    [CreatedBy]   INT           NULL,
    [CreatedOn]   DATETIME      NULL,
    [UpdatedBy]   INT           NULL,
    [UpdateOn]    DATETIME      NULL,
    CONSTRAINT [PK_Subsource] PRIMARY KEY CLUSTERED ([SubsourceId] ASC),
    CONSTRAINT [FK_Subsource_Source] FOREIGN KEY ([SourceId]) REFERENCES [CRM].[Sources] ([SourceId])
);

