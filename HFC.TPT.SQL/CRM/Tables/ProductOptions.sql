﻿CREATE TABLE [CRM].[ProductOptions] (
    [Id]                    INT                IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ProductStyleId]        INT                NOT NULL,
    [OptionName]            VARCHAR (50)       NOT NULL,
    [OptionValue]           VARCHAR (50)       NOT NULL,
    [JobberPrice]           DECIMAL (18, 4)    NULL,
    [JobberDiscountPercent] DECIMAL (9, 4)     NULL,
    [MarginPercent]         DECIMAL (9, 4)     NULL,
    [CreatedOn]             DATETIMEOFFSET (2) CONSTRAINT [DF_ProductOptions_CreatedOn] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ProductOptions] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ProductOptions_ProductStyles] FOREIGN KEY ([ProductStyleId]) REFERENCES [CRM].[ProductStyles] ([Id])
);

