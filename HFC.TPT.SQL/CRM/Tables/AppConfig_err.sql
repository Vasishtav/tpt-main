﻿CREATE TABLE [CRM].[AppConfig_err] (
    [Err_id]                INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [CreatedOn]             DATETIME       CONSTRAINT [DF_AppConfig_err_CreatedOn] DEFAULT (getdate()) NULL,
    [ConfigId]              INT            NULL,
    [Name]                  VARCHAR (50)   NOT NULL,
    [Value]                 VARCHAR (256)  NULL,
    [GroupName]             VARCHAR (50)   NOT NULL,
    [SubGroupName]          VARCHAR (50)   NULL,
    [AvailableValueOptions] VARCHAR (1000) NULL,
    [CreatedOnUtc]          DATETIME       NOT NULL,
    [Description]           VARCHAR (1000) NULL,
    [FranchiseId]           INT            NULL,
    [Inheritable]           BIT            NOT NULL
);

