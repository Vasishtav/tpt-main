﻿CREATE TABLE [CRM].[Type_Category] (
    [Id]          INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Category]    VARCHAR (50) NULL,
    [FranchiseId] INT          NULL,
    [BrandId]     SMALLINT     CONSTRAINT [DF_Type_Category_BrandId] DEFAULT ((1)) NOT NULL,
    [OriginalId]  SMALLINT     NULL,
    CONSTRAINT [PK_Type_Category] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Type_Category_Franchise] FOREIGN KEY ([FranchiseId]) REFERENCES [CRM].[Franchise] ([FranchiseId]),
    CONSTRAINT [ucCategoryBrandId] UNIQUE NONCLUSTERED ([Category] ASC, [BrandId] ASC)
);

