﻿CREATE TABLE [CRM].[VendorCaseDetail] (
    [VendorCaseDetailId] INT           IDENTITY (1, 1) NOT NULL,
    [CaseLineId]         INT           NOT NULL,
    [VendorCaseId]       INT           NOT NULL,
    [TripChargeApproved] INT           NULL,
    [Resolution]         INT           NULL,
    [Status]             INT           NULL,
    [CreatedOn]          DATETIME2 (7) NOT NULL,
    [CreatedBy]          INT           NOT NULL,
    [LastUpdatedOn]      DATETIME2 (7) NULL,
    [LastUpdatedBy]      INT           NULL,
    [Amount] NUMERIC(18, 2) NULL, 
    CONSTRAINT [PK_VendorCaseDetail] PRIMARY KEY CLUSTERED ([VendorCaseDetailId] ASC),
    CONSTRAINT [FK_VendorCase_CaseLineId] FOREIGN KEY ([CaseLineId]) REFERENCES [CRM].[FranchiseCaseAddInfo] ([Id]),
    CONSTRAINT [FK_VendorCase_VendorCaseId] FOREIGN KEY ([VendorCaseId]) REFERENCES [CRM].[VendorCase] ([VendorCaseId])
);

