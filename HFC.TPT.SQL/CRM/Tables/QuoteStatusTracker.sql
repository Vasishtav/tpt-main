﻿CREATE TABLE [CRM].[QuoteStatusTracker] (
    [QuoteStatusTrackerId] INT      IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [QuoteKey]             INT      NOT NULL,
    [QuoteStatusId]        INT      NOT NULL,
    [CreatedOn]            DATETIME NOT NULL,
    [CreatedBy]            INT      NULL,
    CONSTRAINT [FK_Quote_QuoteStatusTracker] FOREIGN KEY ([QuoteKey]) REFERENCES [CRM].[Quote] ([QuoteKey]),
    CONSTRAINT [FK_QuoteStatusTracker_Type_QuoteStatus] FOREIGN KEY ([QuoteStatusId]) REFERENCES [CRM].[Type_QuoteStatus] ([QuoteStatusId])
);

