﻿CREATE TABLE [CRM].[LeadNJobNumbers] (
    [FranchiseId]   INT NOT NULL,
    [JobNumber]     INT CONSTRAINT [DF_JobNumbers_JobNumber] DEFAULT ((0)) NOT NULL,
    [LeadNumber]    INT CONSTRAINT [DF_JobNumbers_LeadNum] DEFAULT ((1)) NOT NULL,
    [OrderNumber]   INT NULL,
    [InvoiceNumber] INT NULL,
    [VendorNumber]  INT NULL,
    [ProductNumber] INT DEFAULT ((0)) NULL,
    [QuoteNumber]   INT DEFAULT ((0)) NULL,
    CONSTRAINT [PK_JobNumbers] PRIMARY KEY CLUSTERED ([FranchiseId] ASC) WITH (FILLFACTOR = 90)
);

