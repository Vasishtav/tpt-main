﻿CREATE TABLE [CRM].[AccountSources] (
    [AccountSourceId] INT      IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [SourceId]        INT      NOT NULL,
    [AccountId]       INT      NOT NULL,
    [CreatedOnUtc]    DATETIME CONSTRAINT [DF_Key_AccountSource_CreatedOnUtc] DEFAULT (getutcdate()) NOT NULL,
    [IsManuallyAdded] BIT      CONSTRAINT [DF_Key_AccountSource_IsManuallyAdded] DEFAULT ((0)) NOT NULL,
    [CreatedBy]       INT      NULL,
    [UpdatedOn]       DATETIME NULL,
    [UpdatedBy]       INT      NULL,
    [IsPrimarySource] BIT      DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Key_AccountSource_1] PRIMARY KEY CLUSTERED ([AccountSourceId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_AccountSources_SourcesTP] FOREIGN KEY ([SourceId]) REFERENCES [CRM].[SourcesTP] ([SourcesTPId]),
    CONSTRAINT [FK_Key_AccountSource_Accounts] FOREIGN KEY ([AccountId]) REFERENCES [CRM].[Accounts] ([AccountId]),
    CONSTRAINT [UX_AccountSource_AccountIDSourceID] UNIQUE NONCLUSTERED ([SourceId] ASC, [AccountId] ASC) WITH (FILLFACTOR = 90)
);

