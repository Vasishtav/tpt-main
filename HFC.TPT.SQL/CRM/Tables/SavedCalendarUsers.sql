﻿CREATE TABLE [CRM].[SavedCalendarUsers] (
    [Id]             INT IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [SaveCalendarid] INT NULL,
    [SelectedUserid] INT NULL,
    [SelectedRoleid] INT NULL,
    [LoginUserid]    INT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

