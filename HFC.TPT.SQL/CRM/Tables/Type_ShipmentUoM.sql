﻿CREATE TABLE [CRM].[Type_ShipmentUoM] (
    [Id]            INT         IDENTITY (1, 1) NOT NULL,
    [UomCode]            varchar (200) NOT NULL,
    [UomDefinition]      varchar (200) NOT NULL,
    [IsActive]      BIT           NOT NULL,
    [IsDeleted]     BIT           NULL,
    [CreatedOn]     DATETIME2 (7) NOT NULL,
    [CreatedBy]     INT           NOT NULL,
    [LastUpdatedOn] DATETIME2 (7) NULL,
    [LastUpdatedBy] INT           NULL,
    CONSTRAINT [PK_Type_ShipmentUoM] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [UQ_Type_ShipmentUomCode] UNIQUE([UomCode]),
    CONSTRAINT [FK_Type_ShipmentUoM_Createdby] FOREIGN KEY ([CreatedBy]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_Type_ShipmentUoM_Modifiedby] FOREIGN KEY ([LastUpdatedBy]) REFERENCES [CRM].[Person] ([PersonId])
);

