﻿CREATE TABLE [CRM].[CaseFollow] (
    [id]       INT IDENTITY (1, 1) NOT NULL,
    [CaseId]   INT NULL,
    [PersonId] INT NULL,
    [IsFollow] BIT NULL,
    [ModuleId] INT NULL, 
    [VendorId] INT NULL, 
    PRIMARY KEY CLUSTERED ([id] ASC)
);

