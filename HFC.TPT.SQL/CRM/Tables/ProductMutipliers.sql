﻿CREATE TABLE [CRM].[ProductMutipliers] (
    [ProductMutiplierId] INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ProductGuid]        UNIQUEIDENTIFIER NOT NULL,
    [FranchiseId]        INT              NOT NULL,
    [Multiplier]         FLOAT (53)       NULL,
    CONSTRAINT [PK_ProductMultipliers] PRIMARY KEY CLUSTERED ([ProductMutiplierId] ASC) WITH (FILLFACTOR = 90)
);

