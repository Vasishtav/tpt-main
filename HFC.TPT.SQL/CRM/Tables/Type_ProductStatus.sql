﻿CREATE TABLE [CRM].[Type_ProductStatus] (
    [ProductStatusId] INT                IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ProductStatus]   VARCHAR (100)      NOT NULL,
    [IsActive]        BIT                NOT NULL,
    [IsDeleted]       BIT                NULL,
    [CreatedOn]       DATETIMEOFFSET (2) NOT NULL,
    [CreatedBy]       INT                NOT NULL,
    [LastUpdatedOn]   DATETIMEOFFSET (2) NULL,
    [LastUpdatedBy]   INT                NULL,
    CONSTRAINT [PK_Type_ProductStatus] PRIMARY KEY CLUSTERED ([ProductStatusId] ASC),
    CONSTRAINT [FK_Type_ProductStatus_Createdby] FOREIGN KEY ([CreatedBy]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_Type_ProductStatus_Modifiedby] FOREIGN KEY ([LastUpdatedBy]) REFERENCES [CRM].[Person] ([PersonId])
);

