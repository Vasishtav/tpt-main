﻿CREATE TABLE [CRM].[ManufacturerContacts] (
    [ManufacturerContactId] INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ManufacturerId]        INT            NOT NULL,
    [FranchiseId]           INT            NOT NULL,
    [Contact]               NVARCHAR (200) NULL,
    [ContactNumber]         NVARCHAR (200) NULL,
    [ContactEmail]          NVARCHAR (100) NULL,
    [AccountNumber]         NVARCHAR (50)  NULL,
    [IsActive]              BIT            NULL,
    CONSTRAINT [PK_ManufacturerContacts] PRIMARY KEY CLUSTERED ([ManufacturerContactId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ManufacturerContacts_Franchise] FOREIGN KEY ([FranchiseId]) REFERENCES [CRM].[Franchise] ([FranchiseId]),
    CONSTRAINT [FK_ManufacturerContacts_Manufacturers] FOREIGN KEY ([ManufacturerId]) REFERENCES [CRM].[Manufacturers] ([ManufacturerId])
);

