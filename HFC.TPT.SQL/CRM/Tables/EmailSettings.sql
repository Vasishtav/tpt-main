﻿CREATE TABLE [CRM].[EmailSettings] (
    [Id]                    INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [FranchiseId]           INT              NOT NULL,
    [EmailType]             INT              NOT NULL,
    [FromEmailId]           UNIQUEIDENTIFIER NULL,
    [CopyAssignedSales]     BIT              NULL,
    [CopyAssignedInstaller] BIT              NULL,
    [CreatedOn]             DATETIME2 (7)    NOT NULL,
    [CreatedBy]             INT              NOT NULL,
    [LastUpdatedOn]         DATETIME2 (7)    NULL,
    [LastUpdatedBy]         INT              NULL,
    CONSTRAINT [PK_EmailSettings] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_EmailSettings_Createdby] FOREIGN KEY ([CreatedBy]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_EmailSettings_Modifiedby] FOREIGN KEY ([LastUpdatedBy]) REFERENCES [CRM].[Person] ([PersonId])
);

