﻿CREATE TABLE [CRM].[MPOStatusTracker] (
    [MPOStatusTrackerId] INT      IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PurchaseOrderId]    INT      NOT NULL,
    [MPOStatusId]        INT      NOT NULL,
    [CreatedOn]          DATETIME NOT NULL,
    [CreatedBy]          INT      NULL
);

