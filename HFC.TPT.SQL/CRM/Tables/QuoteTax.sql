﻿CREATE TABLE [CRM].[QuoteTax]
(
	[Id]  INT             IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [QuoteKey] INT NOT NULL,
    [TaxReponseObject] VARCHAR(MAX) NOT NULL,
    [CreatedOn] DATETIME NOT NULL,
    [CreatedBy] INT NOT NULL,
    [LastUpdatedOn] DATETIME NOT NULL,
    [LastUpdatedBy] INT NOT NULL,
    CONSTRAINT [FK_QuoteTax_Quote] FOREIGN KEY (QuoteKey) REFERENCES [CRM].[Quote](QuoteKey)
)
