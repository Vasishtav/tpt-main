﻿CREATE TABLE [CRM].[FranchiseOneTimeProduct] (
    [FranchiseOnetimeProductId] INT             IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [FranchiseId]               INT             NOT NULL,
    [VendorName]                VARCHAR (100)   NULL,
    [ProductName]               VARCHAR (100)   NULL,
    [Description]               VARCHAR (250)   NULL,
    [Cost]                      DECIMAL (18, 2) NULL,
    [UnitPrice]                 DECIMAL (18, 2) NULL,
    [CreatedOn]                 DATETIME        NOT NULL,
    [CreatedBy]                 INT             NOT NULL,
    [UpdatedOn]                 DATETIME        NULL,
    [UpdatedBy]                 INT             NULL,
    CONSTRAINT [PK_FranchiseOneTimeProduct] PRIMARY KEY CLUSTERED ([FranchiseOnetimeProductId] ASC)
);

