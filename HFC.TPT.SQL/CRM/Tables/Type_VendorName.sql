﻿CREATE TABLE [CRM].[Type_VendorName] (
    [VendorNameId]  INT                IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [VendorName]    VARCHAR (100)      NOT NULL,
    [IsActive]      BIT                NOT NULL,
    [IsDeleted]     BIT                NULL,
    [CreatedOn]     DATETIMEOFFSET (2) NOT NULL,
    [CreatedBy]     INT                NOT NULL,
    [LastUpdatedOn] DATETIMEOFFSET (2) NULL,
    [LastUpdatedBy] INT                NULL,
    CONSTRAINT [PK_Type_VendorName] PRIMARY KEY CLUSTERED ([VendorNameId] ASC),
    CONSTRAINT [FK_Type_VendorName_Createdby] FOREIGN KEY ([CreatedBy]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_Type_VendorName_Modifiedby] FOREIGN KEY ([LastUpdatedBy]) REFERENCES [CRM].[Person] ([PersonId])
);

