﻿CREATE TABLE [CRM].[CaseChangeDetails]
(	 
    CaseChangeId	INT NOT NULL PRIMARY KEY IDENTITY, 
	CaseId	int	,
	RecordTypeId	int	,
	ProductGroupId	NVARCHAR(MAX)	,
	PublishMethod	varchar(128)	,
	EffectiveDate	datetime2	,
	CreatedOn	datetime2	,
	CreatedBy	int	,
	LastUpdatedOn	datetime2	,
	LastUpdatedBy	int	,
	[AttachmentURL] NVARCHAR(MAX) NULL, 
    [DeclinedReasonId] INT NULL, 
    [Comments] NVARCHAR(MAX) NULL, 
    [IsAllSelected] BIT NULL, 
    constraint [FK_CaseChangeDetails_Case] FOREIGN KEY ([CaseId]) REFERENCES [CRM].[Case] ([CaseId])
	
)
