﻿CREATE TABLE [CRM].[CommonChangeHistory](
	[ChangeHistoryId] [int] IDENTITY(1,1) NOT NULL,
	[TableCode] [varchar](500) NOT NULL,
	[PrimaryTablePrimaryKey] [int] NOT NULL,
	[SecondaryTablePrimaryKey] [int] NULL,
	[ChangedFieldName] [varchar](500) NOT NULL,
	[ChangedValue] [varchar](max) NOT NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	CONSTRAINT [PK_CommonChangeHistory_Id] PRIMARY KEY CLUSTERED ([ChangeHistoryId] ASC),
	CONSTRAINT [FK_CommonChangeHistory_CreatedBy] FOREIGN KEY ([CreatedBy]) REFERENCES [CRM].[Person] ([PersonId])

)