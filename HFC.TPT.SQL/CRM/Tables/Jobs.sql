﻿CREATE TABLE [CRM].[Jobs] (
    [JobId]                 INT                IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [JobGuid]               UNIQUEIDENTIFIER   CONSTRAINT [DF_Jobs_JobGuid] DEFAULT (newid()) NOT NULL,
    [JobNumber]             INT                CONSTRAINT [DF_Jobs_JobNumber] DEFAULT ((1)) NOT NULL,
    [LeadId]                INT                NOT NULL,
    [CreatedOnUtc]          DATETIME2 (2)      CONSTRAINT [DF_Jobs_CreatedOnUtc] DEFAULT (getutcdate()) NOT NULL,
    [BillingAddressId]      INT                NOT NULL,
    [InstallAddressId]      INT                NOT NULL,
    [CustomerPersonId]      INT                NOT NULL,
    [JobStatusId]           INT                CONSTRAINT [DF_Jobs_JobStatusId] DEFAULT ((1)) NOT NULL,
    [IsDeleted]             BIT                CONSTRAINT [DF_Jobs_IsDeleted] DEFAULT ((0)) NOT NULL,
    [SalesPersonId]         INT                NULL,
    [InstallerPersonId]     INT                NULL,
    [LastUpdated]           DATETIMEOFFSET (2) CONSTRAINT [DF_Jobs_LastUpdatedOnUtc] DEFAULT (getutcdate()) NOT NULL,
    [LastUpdatedByPersonId] INT                NULL,
    [JobConceptId]          SMALLINT           CONSTRAINT [DF_Jobs_ConceptId] DEFAULT ((1)) NOT NULL,
    [CreatedByPersonId]     INT                NULL,
    [ContractedOnUtc]       DATETIME2 (2)      NULL,
    [CompletedOnUtc]        DATETIME2 (2)      NULL,
    [Balance]               DECIMAL (18, 2)    CONSTRAINT [DF_Jobs_Balance] DEFAULT ((0)) NOT NULL,
    [TerritoryId]           INT                NULL,
    [SourceId]              INT                NULL,
    [SideMark]              NVARCHAR (150)     NULL,
    [Hint]                  NVARCHAR (500)     NULL,
    [Description]           NVARCHAR (MAX)     NULL,
    [IsCompleted]           BIT                NULL,
    [CompletionDT]          DATETIMEOFFSET (2) NULL,
    [QuoteDateUTC]          DATETIME2 (2)      NULL,
    [OpportunityId]         INT                DEFAULT ((0)) NULL,
    CONSTRAINT [PK_Jobs] PRIMARY KEY CLUSTERED ([JobId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Jobs_BillingAddress] FOREIGN KEY ([BillingAddressId]) REFERENCES [CRM].[Addresses] ([AddressId]),
    CONSTRAINT [FK_Jobs_CreatorPerson] FOREIGN KEY ([CreatedByPersonId]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_Jobs_CustomerPerson] FOREIGN KEY ([CustomerPersonId]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_Jobs_InstallAddress] FOREIGN KEY ([InstallAddressId]) REFERENCES [CRM].[Addresses] ([AddressId]),
    CONSTRAINT [FK_Jobs_InstallPerson] FOREIGN KEY ([InstallerPersonId]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_Jobs_LastUpdaterPerson] FOREIGN KEY ([LastUpdatedByPersonId]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_Jobs_Leads] FOREIGN KEY ([LeadId]) REFERENCES [CRM].[Leads] ([LeadId]),
    CONSTRAINT [FK_Jobs_SalesPerson] FOREIGN KEY ([SalesPersonId]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_Jobs_Source] FOREIGN KEY ([SourceId]) REFERENCES [CRM].[Sources] ([SourceId]),
    CONSTRAINT [FK_Jobs_Territories] FOREIGN KEY ([TerritoryId]) REFERENCES [CRM].[Territories] ([TerritoryId]),
    CONSTRAINT [FK_Jobs_Type_JobStatus] FOREIGN KEY ([JobStatusId]) REFERENCES [CRM].[Type_JobStatus] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [NotDeleted]
    ON [CRM].[Jobs]([JobStatusId] ASC, [IsDeleted] ASC, [CreatedOnUtc] ASC, [SalesPersonId] ASC, [LeadId] ASC)
    INCLUDE([JobId], [JobNumber], [CustomerPersonId], [Balance]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_LeadId_IsDeleted]
    ON [CRM].[Jobs]([LeadId] ASC, [IsDeleted] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [ix_SourceId]
    ON [CRM].[Jobs]([SourceId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [ix_JobStatusId]
    ON [CRM].[Jobs]([JobStatusId] ASC) WITH (FILLFACTOR = 90);

