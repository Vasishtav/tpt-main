﻿CREATE TABLE [CRM].[OrderLines] (
    [OrderLineID]   INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [QuoteKey]      INT           NULL,
    [QuoteLineId]   INT           NULL,
    [CreatedOn]     DATETIME2 (7) NULL,
    [CreatedBy]     INT           NULL,
    [LastUpdatedOn] DATETIME2 (7) NULL,
    [LastUpdatedBy] INT           NULL,
    [OrderID]       INT           NULL,
	[OrderLineStatus] INT			NULL,
    CONSTRAINT [PK_OrderLines] PRIMARY KEY CLUSTERED ([OrderLineID] ASC)
);

