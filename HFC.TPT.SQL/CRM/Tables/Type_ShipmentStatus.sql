﻿CREATE TABLE [CRM].[Type_ShipmentStatus] (
    [ShipmentStatusId] INT           IDENTITY (1, 1) NOT NULL,
    [Name]             VARCHAR (100) NOT NULL,
    [IsActive]         BIT           NOT NULL,
    [CreatedOn]        DATETIME      NOT NULL,
    [CreatedBy]        INT           NOT NULL,
    [LastUpdatedOn]    DATETIME      NULL,
    [LastUpdatedBy]    INT           NULL,
    CONSTRAINT [PK_Type_ShipmentStatus] PRIMARY KEY CLUSTERED ([ShipmentStatusId] ASC)
);

