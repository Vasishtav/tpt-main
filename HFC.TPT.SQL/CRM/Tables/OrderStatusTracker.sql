﻿CREATE TABLE [CRM].[OrderStatusTracker] (
    [OrderStatusTrackerId] INT      IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [OrderID]              INT      NOT NULL,
    [OrderStatusId]        INT      NOT NULL,
    [CreatedOn]            DATETIME NOT NULL,
    [CreatedBy]            INT      NULL,
    CONSTRAINT [FK_Orders_OrderStatusTracker] FOREIGN KEY ([OrderID]) REFERENCES [CRM].[Orders] ([OrderID]),
    CONSTRAINT [FK_Type_OrderStatus_OrderStatusTracker] FOREIGN KEY ([OrderStatusId]) REFERENCES [CRM].[Type_OrderStatus] ([OrderStatusId])
);

