﻿CREATE TABLE [CRM].[Orders_old] (
    [OrderId]                 INT                IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [QuoteId]                 INT                NOT NULL,
    [OrderStatusId]           INT                NULL,
    [Note]                    VARCHAR (1024)     NULL,
    [OrderTotal]              DECIMAL (18, 4)    CONSTRAINT [DF_Orders_OrderTotal] DEFAULT ((0)) NOT NULL,
    [IsDeleted]               BIT                NULL,
    [CreatedByPersonId]       INT                NULL,
    [ReferenceNumber]         VARCHAR (24)       NULL,
    [CreatedOnUtc]            DATETIME2 (2)      CONSTRAINT [DF_Orders_CreatedOnUtc] DEFAULT (getutcdate()) NOT NULL,
    [LastUpdated]             DATETIMEOFFSET (2) CONSTRAINT [DF_Orders_LastUpdatedUtc] DEFAULT (getutcdate()) NOT NULL,
    [LastUpdatedPersonId]     INT                NULL,
    [JobId]                   INT                NOT NULL,
    [OrderNumber]             INT                NULL,
    [OrderGuid]               UNIQUEIDENTIFIER   NULL,
    [VendorId]                INT                NULL,
    [PONumber]                VARCHAR (24)       NULL,
    [SessionId]               INT                NULL,
    [HasItemsSavedWithErrors] BIT                NULL,
    CONSTRAINT [PK_Orders] PRIMARY KEY CLUSTERED ([OrderId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Order_JobQuotes] FOREIGN KEY ([QuoteId]) REFERENCES [CRM].[JobQuotes] ([QuoteId]),
    CONSTRAINT [FK_Orders_Jobs] FOREIGN KEY ([JobId]) REFERENCES [CRM].[Jobs] ([JobId]),
    CONSTRAINT [FK_Orders_Person_CreatedByPersonId] FOREIGN KEY ([CreatedByPersonId]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_Orders_Person_LastUpdatedPersonId] FOREIGN KEY ([LastUpdatedPersonId]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_Orders_Vendors] FOREIGN KEY ([VendorId]) REFERENCES [Acct].[Vendors] ([VendorId])
);


GO
CREATE NONCLUSTERED INDEX [IX_JobId_IsDeleted]
    ON [CRM].[Orders_old]([QuoteId] ASC, [IsDeleted] ASC) WITH (FILLFACTOR = 90);

