﻿CREATE TABLE [CRM].[VendorTerritoryAccount] (
    [Id]             INT           IDENTITY (1, 1) NOT NULL,
    [FranchiseId]    INT           NOT NULL,
    [VendorId]       INT           NOT NULL,
    [TerritoryId]    INT           NOT NULL,
    [AccountNumber]  VARCHAR (100) NULL,
    [ShipToLocation] INT           NULL,
    CONSTRAINT [Pk_VendorTerritoryAccount] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [Fk_Franchise_VendorTerritoryAccount_FranchiseId] FOREIGN KEY ([FranchiseId]) REFERENCES [CRM].[Franchise] ([FranchiseId]),
    CONSTRAINT [Fk_ShipToLocations_VendorTerritoryAccount_ShipToLocationId] FOREIGN KEY ([ShipToLocation]) REFERENCES [CRM].[ShipToLocations] ([Id]),
    CONSTRAINT [Fk_Territories_VendorTerritoryAccount_TerritoryId] FOREIGN KEY ([TerritoryId]) REFERENCES [CRM].[Territories] ([TerritoryId])
);

