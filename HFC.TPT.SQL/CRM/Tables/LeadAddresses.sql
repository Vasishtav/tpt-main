﻿CREATE TABLE [CRM].[LeadAddresses] (
    [LeadId]    INT NOT NULL,
    [AddressId] INT NOT NULL,
    [IsPrimaryAddress] BIT NOT NULL DEFAULT (0), 
    CONSTRAINT [PK_LeadAddresses] PRIMARY KEY CLUSTERED ([LeadId] ASC, [AddressId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_LeadAddresses_Addresses] FOREIGN KEY ([AddressId]) REFERENCES [CRM].[Addresses] ([AddressId]) ON DELETE CASCADE,
    CONSTRAINT [FK_LeadAddresses_Leads] FOREIGN KEY ([LeadId]) REFERENCES [CRM].[Leads] ([LeadId])
);

