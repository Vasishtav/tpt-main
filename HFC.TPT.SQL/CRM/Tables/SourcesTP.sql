﻿CREATE TABLE [CRM].[SourcesTP] (
    [SourcesTPId] INT IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [OwnerId]     INT NOT NULL,
    [ChannelId]   INT NOT NULL,
    [SourceId]    INT NOT NULL,
    [IsActive]    BIT DEFAULT ((1)) NOT NULL,
    [IsDeleted]   BIT DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_SourcesTP] PRIMARY KEY CLUSTERED ([SourcesTPId] ASC),
    CONSTRAINT [AK_UniqueSourcesTP] UNIQUE NONCLUSTERED ([OwnerId] ASC, [ChannelId] ASC, [SourceId] ASC)
);

