﻿CREATE TABLE [CRM].[Type_LeadStatus] (
    [Id]           SMALLINT     IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Name]         VARCHAR (50) NOT NULL,
    [ParentId]     SMALLINT     CONSTRAINT [DF_Type_LeadStatus_LinkedToStatusId] DEFAULT ((0)) NULL,
    [FranchiseId]  INT          NULL,
    [ClassName]    VARCHAR (50) NULL,
    [DisplayOrder] INT          CONSTRAINT [DF_Type_LeadStatus_DisplayOrder] DEFAULT ((99999)) NULL,
    [isDeleted]    BIT          NULL,
    [CreatedAt]    DATETIME     NULL,
    [UpdatedAt]    DATETIME     NULL,
    [CreatedBy]    INT          NULL,
    [UpdatedBy]    INT          NULL,
    CONSTRAINT [PK_Type_LeadStatus] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Type_LeadStatus_Franchise] FOREIGN KEY ([FranchiseId]) REFERENCES [CRM].[Franchise] ([FranchiseId]),
    CONSTRAINT [FK_Type_LeadStatus_Type_LeadStatus] FOREIGN KEY ([ParentId]) REFERENCES [CRM].[Type_LeadStatus] ([Id])
);

