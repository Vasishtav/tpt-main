﻿CREATE TABLE [CRM].[JobQuotes] (
    [QuoteId]            INT                IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [JobId]              INT                NOT NULL,
    [QuoteNumber]        INT                CONSTRAINT [DF_JobQuotes_QuoteNumber] DEFAULT ((1)) NOT NULL,
    [CreatedOnUtc]       DATETIME2 (2)      CONSTRAINT [DF_JobQuotes_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [CreatedByPersonId]  INT                NULL,
    [Subtotal]           DECIMAL (18, 4)    CONSTRAINT [DF_JobQuotes_Subtotal] DEFAULT ((0)) NOT NULL,
    [Taxtotal]           DECIMAL (18, 4)    CONSTRAINT [DF_JobQuotes_Taxtotal] DEFAULT ((0)) NOT NULL,
    [DiscountTotal]      DECIMAL (18, 4)    CONSTRAINT [DF_JobQuotes_DiscountTotal] DEFAULT ((0)) NOT NULL,
    [SurchargeTotal]     DECIMAL (18, 4)    CONSTRAINT [DF_JobQuotes_SurchargeTotal] DEFAULT ((0)) NOT NULL,
    [CostSubtotal]       DECIMAL (18, 4)    CONSTRAINT [DF_JobQuotes_CostSubtotal] DEFAULT ((0)) NOT NULL,
    [NetTotal]           DECIMAL (18, 2)    CONSTRAINT [DF_JobQuotes_NetTotal] DEFAULT ((0)) NOT NULL,
    [NetProfit]          DECIMAL (18, 2)    CONSTRAINT [DF_JobQuotes_NetProfit] DEFAULT ((0)) NOT NULL,
    [TaxPercent]         DECIMAL (9, 4)     CONSTRAINT [DF_JobQuotes_TaxPercent] DEFAULT ((0)) NOT NULL,
    [Margin]             DECIMAL (9, 4)     CONSTRAINT [DF_JobQuotes_Margin] DEFAULT ((0)) NOT NULL,
    [LastUpdated]        DATETIMEOFFSET (2) CONSTRAINT [DF_JobQuotes_LastUpdatedOn] DEFAULT (getutcdate()) NOT NULL,
    [IsDeleted]          BIT                CONSTRAINT [DF_JobQuotes_IsDeleted] DEFAULT ((0)) NOT NULL,
    [IsPrimary]          BIT                CONSTRAINT [DF_JobQuotes_IsPrimary] DEFAULT ((1)) NOT NULL,
    [EnvelopeId]         NVARCHAR (100)     NULL,
    [EnvelopeStatus]     NVARCHAR (100)     NULL,
    [EnvelopeSentAt]     DATETIMEOFFSET (2) NULL,
    [EnvelopeReceivedAt] DATETIMEOFFSET (2) NULL,
    CONSTRAINT [PK_JobQuotes] PRIMARY KEY CLUSTERED ([QuoteId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_JobQuotes_Jobs] FOREIGN KEY ([JobId]) REFERENCES [CRM].[Jobs] ([JobId])
);


GO
CREATE NONCLUSTERED INDEX [PrimaryQuote]
    ON [CRM].[JobQuotes]([IsPrimary] ASC, [JobId] ASC)
    INCLUDE([NetTotal]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_JobId_IsDeleted]
    ON [CRM].[JobQuotes]([JobId] ASC, [IsDeleted] ASC) WITH (FILLFACTOR = 90);

