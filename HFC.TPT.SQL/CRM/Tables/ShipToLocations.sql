﻿CREATE TABLE [CRM].[ShipToLocations] (
    [Id]             INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ShipToLocation] INT           NOT NULL,
    [ShipToName]     VARCHAR (200) NULL,
    [AddressId]      INT           NOT NULL,
    [Active]         BIT           NULL,
    [Deleted]        BIT           NULL,
    [CreatedOn]      DATETIME2 (7) CONSTRAINT [DF__ShipToLoc__Creat__45FB8CC8] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]      INT           NOT NULL,
    [LastUpdatedOn]  DATETIME2 (7) NULL,
    [LastUpdatedBy]  INT           NULL,
    [FranchiseId]    INT           NULL,
    [PhoneNumber] NVARCHAR(225) NULL, 
    CONSTRAINT [Pk_ShipToLocations] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ShipToLocations_Createdby] FOREIGN KEY ([CreatedBy]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_ShipToLocations_Modifiedby] FOREIGN KEY ([LastUpdatedBy]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [Fk_Type_Addresses_AddressId] FOREIGN KEY ([AddressId]) REFERENCES [CRM].[Addresses] ([AddressId]),
    CONSTRAINT [Fk_Type_ShipToLocations_ShipToLocation] FOREIGN KEY ([ShipToLocation]) REFERENCES [CRM].[Type_ShipToLocations] ([Id])
);

