﻿CREATE TABLE [CRM].[Type_Source] (
    [Id]          INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Name]        VARCHAR (50)  NOT NULL,
    [Description] VARCHAR (250) NULL,
    [ParentId]    INT           NULL,
    [FranchiseId] INT           NULL,
    [isDeleted]   BIT           NULL,
    [CreatedAt]   DATETIME      NULL,
    [UpdatedAt]   DATETIME      NULL,
    CONSTRAINT [PK_Type_Source] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 90)
);

