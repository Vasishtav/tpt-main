﻿CREATE TABLE [CRM].[Type_QuoteStatus] (
    [QuoteStatusId] INT                IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [QuoteStatus]   VARCHAR (100)      NOT NULL,
    [IsActive]      BIT                NOT NULL,
    [IsDeleted]     BIT                NULL,
    [CreatedOn]     DATETIMEOFFSET (2) NOT NULL,
    [CreatedBy]     INT                NOT NULL,
    [LastUpdatedOn] DATETIMEOFFSET (2) NULL,
    [LastUpdatedBy] INT                NULL,
    CONSTRAINT [PK_Type_QuoteStatus] PRIMARY KEY CLUSTERED ([QuoteStatusId] ASC),
    CONSTRAINT [FK_CRM]].[Type_QuoteStatus_Createdby] FOREIGN KEY ([CreatedBy]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_CRM]].[Type_QuoteStatus_Modifiedby] FOREIGN KEY ([LastUpdatedBy]) REFERENCES [CRM].[Person] ([PersonId])
);

