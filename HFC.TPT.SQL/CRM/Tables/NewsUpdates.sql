﻿CREATE TABLE [CRM].[NewsUpdates] (
    [NewsUpdatesId] INT            IDENTITY (1, 1) NOT NULL,
    [Headline]      NVARCHAR (255) NOT NULL,
    [IsActive]      BIT            DEFAULT ((0)) NOT NULL,
    [StartDate]     DATETIME       NULL,
    [EndDate]       DATETIME       NULL,
    [Message]       NVARCHAR (MAX) NULL,
    [SortOrder]     INT            NULL,
    [CreatedOn]     DATETIME       NULL,
    [CreatedBy]     INT            NULL,
    [LastUpdatedOn] DATETIME       NULL,
    [LastUpdatedBy] INT            NULL,
    [ShortMessage]  NVARCHAR (250) NULL,
    CONSTRAINT [PK_NewsUpdates] PRIMARY KEY CLUSTERED ([NewsUpdatesId] ASC)
);

