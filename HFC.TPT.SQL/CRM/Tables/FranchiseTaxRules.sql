﻿CREATE TABLE [CRM].[FranchiseTaxRules] (
    [TaxRuleId]   INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [FranchiseId] INT            NOT NULL,
    [County]      VARCHAR (50)   NULL,
    [City]        VARCHAR (50)   NULL,
    [State]       VARCHAR (50)   NULL,
    [ZipCode]     VARCHAR (15)   NULL,
    [Percent]     DECIMAL (9, 4) CONSTRAINT [DF_FranchiseTaxRules_Percent] DEFAULT ((0)) NOT NULL,
    [Default]     BIT            NULL,
    [RuleName]    NVARCHAR (250) NULL,
    CONSTRAINT [PK_FranchiseTaxRules] PRIMARY KEY CLUSTERED ([TaxRuleId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_FranchiseTaxRules_Franchise] FOREIGN KEY ([FranchiseId]) REFERENCES [CRM].[Franchise] ([FranchiseId])
);

