﻿CREATE TABLE [CRM].[Type_Questions] (
    [QuestionId]          INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Question]            VARCHAR (50) NOT NULL,
    [SubQuestion]         VARCHAR (50) NULL,
    [CreatedOnUtc]        DATETIME     CONSTRAINT [DF_Type_QAQuestions_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [SelectionType]       VARCHAR (50) NULL,
    [DisplayOrder]        INT          CONSTRAINT [DF_Type_QAQuestions_DisplayOrder] DEFAULT ((0)) NULL,
    [FranchiseId]         INT          NULL,
    [BrandId]             TINYINT      NULL,
    [LeadQuestion]        BIT          NULL,
    [CreatedOn]           DATETIME     NULL,
    [CreatedBy]           INT          NULL,
    [UpdatedBy]           INT          NULL,
    [UpdatedOn]           DATETIME     NULL,
    [OpportunityQuestion] INT          NULL,
    [AccountQuestion]     BIT          NULL,
    [IsPrimaryQuestion]   BIT          NULL,
    CONSTRAINT [PK_Type_QAQuestions] PRIMARY KEY CLUSTERED ([QuestionId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Type_Questions_Franchise] FOREIGN KEY ([FranchiseId]) REFERENCES [CRM].[Franchise] ([FranchiseId])
);

