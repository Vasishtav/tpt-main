﻿CREATE TABLE [CRM].[Type_POStatus] (
    [POStatusId]    INT           IDENTITY (1, 1) NOT NULL,
    [POStatusName]  VARCHAR (100) NOT NULL,
    [IsActive]      BIT           NOT NULL,
    [IsDeleted]     BIT           NOT NULL,
    [CreatedOn]     DATETIME2 (7) NOT NULL,
    [CreatedBy]     INT           NOT NULL,
    [LastUpdatedOn] DATETIME2 (7) NULL,
    [LastUpdatedBy] INT           NULL,
    CONSTRAINT [PK_Type_POStatus] PRIMARY KEY CLUSTERED ([POStatusId] ASC)
);

