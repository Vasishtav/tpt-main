﻿CREATE TABLE [CRM].[HFCModels] (
    [ModelKey]        INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Model]       VARCHAR (500) NULL,
    [Description]       VARCHAR (500) NULL,
	[ProductId] int NOT null,
	[PICProductId] int NOT null,
	[Active]            BIT           NULL,
    [DiscontinuedDate]  DATETIME2 (7) NULL,
    [PhasedoutDate]     DATETIME2 (7) NULL,
    [USActive]            BIT           NOT NULL,
	[CAActive]            BIT           NOT NULL,    
    [VendorId] INT NOT NULL, 
    [WarningMessage] VARCHAR(500) NULL, 
	[IsManualFLag]    bit			not null DEFAULT 0,
	[CreatedOn]         DATETIME2 (7) NOT NULL,
    [CreatedBy]         INT           NOT NULL,
    [LastUpdatedOn]     DATETIME2 (7) NULL,
    [LastUpdatedBy]     INT           NULL,	
    CONSTRAINT [PK_HFCModels] PRIMARY KEY CLUSTERED ([ModelKey] ASC), 
    CONSTRAINT [FK_HFCModels_HFCProducts] FOREIGN KEY ([ProductId]) REFERENCES CRM.HFCProducts([ProductKey])
);

