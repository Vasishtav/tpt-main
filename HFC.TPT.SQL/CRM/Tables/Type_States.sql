﻿CREATE TABLE [CRM].[Type_States] (
    [StateId]       INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [StateName]     VARCHAR (50) NOT NULL,
    [StateAbv]      VARCHAR (2)  NOT NULL,
    [CountryCodeId] INT          CONSTRAINT [DF_Type_States_CountryCodeId] DEFAULT ((223)) NOT NULL,
    [IsEnabled]     BIT          CONSTRAINT [DF_States_IsEnabled] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_States] PRIMARY KEY CLUSTERED ([StateId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Type_States_Type_CountryCodes] FOREIGN KEY ([CountryCodeId]) REFERENCES [CRM].[Type_CountryCodes] ([CountryCodeId])
);

