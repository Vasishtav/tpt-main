﻿CREATE TABLE [CRM].[PurchaseOrderDetailsStatusTracker] (
    [PurchaseOrderDetailsStatusTrackerId] INT      IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PurchaseOrdersDetailId]              INT      NOT NULL,
    [PurchaseOrderId]                     INT      NOT NULL,
    [POStatusId]                          INT      NOT NULL,
    [CreatedOn]                           DATETIME NOT NULL,
    [CreatedBy]                           INT      NULL
);

