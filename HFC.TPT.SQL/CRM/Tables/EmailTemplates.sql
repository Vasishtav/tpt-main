﻿CREATE TABLE [CRM].[EmailTemplates] (
    [EmailTemplateId]     SMALLINT       IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [EmailTemplateTypeId] SMALLINT       NOT NULL,
    [Description]         VARCHAR (250)  NULL,
    [TemplateSubject]     NVARCHAR (250) NULL,
    [TemplateBody]        NVARCHAR (MAX) NULL,
    [FranchiseId]         INT            NULL,
    [isDeleted]           BIT            NULL,
    [TemplateLayout]      NVARCHAR (MAX) NULL,
    [IsVisible]           BIT            DEFAULT ((0)) NULL,
    [IsVisibleAfterDate]  DATE           NULL,
    [TemplateBrand]       INT            NULL,
    CONSTRAINT [PK_EmailTemplate] PRIMARY KEY CLUSTERED ([EmailTemplateId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_EmailTemplate_Franchise] FOREIGN KEY ([FranchiseId]) REFERENCES [CRM].[Franchise] ([FranchiseId])
);

