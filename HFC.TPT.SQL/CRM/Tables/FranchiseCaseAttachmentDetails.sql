﻿CREATE TABLE [CRM].[FranchiseCaseAttachmentDetails] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [Module]       VARCHAR (50)   NULL,
    [ModuleId]     INT            NULL,
    [LineId]       INT            NULL,
    [fileIconType] NVARCHAR (100) NULL,
    [fileSize]     NVARCHAR (100) NULL,
    [fileName]     NVARCHAR (500) NULL,
    [color]        NVARCHAR (100) NULL,
    [streamId]     VARCHAR (100)  NULL,
    [streamId1]    VARCHAR (100)  NULL,
    [CreatedOn] DATETIME2 NULL, 
    [CreatedBy] INT NULL, 
    [LastUpdatedOn] DATETIME2 NULL, 
    [LastUpdatedBy] INT NULL, 
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

