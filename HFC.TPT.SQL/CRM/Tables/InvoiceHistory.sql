﻿CREATE TABLE [CRM].[InvoiceHistory] (
    [Id]            INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [OrderID]       INT              NOT NULL,
    [InvoiceId]     INT              NOT NULL,
    [InvoiceType]   VARCHAR (100)    NULL,
    [InvoiceDate]   DATETIME2 (7)    NULL,
    [TotalAmount]   NUMERIC (18, 2)  NULL,
    [Balance]       NUMERIC (18, 2)  NULL,
    [Streamid]      UNIQUEIDENTIFIER NULL,
    [CreatedOn]     DATETIME2 (7)    NOT NULL,
    [CreatedBy]     INT              NOT NULL,
    [LastUpdatedOn] DATETIME2 (7)    NULL,
    [LastUpdatedBy] INT              NULL,
    CONSTRAINT [PK_InvoiceHistory] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_InvoiceHistory_Invoices] FOREIGN KEY ([InvoiceId]) REFERENCES [CRM].[Invoices] ([InvoiceId]),
    CONSTRAINT [FK_InvoiceHistory_Orders] FOREIGN KEY ([OrderID]) REFERENCES [CRM].[Orders] ([OrderID])
);

