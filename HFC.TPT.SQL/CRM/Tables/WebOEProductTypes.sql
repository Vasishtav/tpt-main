﻿CREATE TABLE [CRM].[WebOEProductTypes] (
    [ProductTypeId] INT NOT NULL,
    [WebOETypeId]   INT NOT NULL,
    CONSTRAINT [PK_WebOEProductTypes] PRIMARY KEY CLUSTERED ([ProductTypeId] ASC, [WebOETypeId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_WebOEProductTypes_Type_Products] FOREIGN KEY ([ProductTypeId]) REFERENCES [CRM].[Type_Products] ([ProductTypeId])
);

