﻿CREATE TABLE [CRM].[OrderCustomerView] 
  ( 
     [OrderCustomerViewId]  INT IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL, 
     [OrderId]              INT NOT NULL, 
     [TermsAccepted]        BIT NULL, 
     [TermsVersion]         VARCHAR (20) NULL, 
     [TermsAcceptedDate]    DATETIME2 (7) NULL, 
     [CustomerSignStreamId] VARCHAR(50) NULL, 
     [CustomerSignJson]     NVARCHAR(MAX) NULL, 
     [CustomerSignedDate]   DATETIME2 (7) NULL, 
     [SalesRepSignStreamId] VARCHAR(50) NULL, 
     [SalesRepSignJson]     NVARCHAR(MAX) NULL, 
     [SalesRepSignedDate]   DATETIME2 (7) NULL, 
     [CreatedOn]            DATETIME2 (7) CONSTRAINT [DF__OrdCustView__CreatedOn] DEFAULT (Getutcdate()) NOT NULL, 
     [CreatedBy]            INT NOT NULL, 
     [UpdatedOn]            DATETIME2 (7) NULL, 
     [UpdatedBy]            INT NULL, 
     CONSTRAINT [Pk_OrderCustomerView] PRIMARY KEY CLUSTERED ( [OrderCustomerViewId] ASC), 
     CONSTRAINT [FK_OrderCustomerView_Createdby] FOREIGN KEY ([CreatedBy]) REFERENCES [CRM].[Person] ([PersonId]), 
     CONSTRAINT [FK_OrderCustomerView_Modifiedby] FOREIGN KEY ([UpdatedBy]) REFERENCES [CRM].[Person] ([PersonId]), 
     CONSTRAINT [Fk_OrderCustomerView_Orders_OrderId] FOREIGN KEY ([OrderId]) REFERENCES [CRM].[Orders] ([OrderId]) 
  ); 