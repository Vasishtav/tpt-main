﻿CREATE TABLE [CRM].[SourcesHFC] (
    [SourcesHFCId]             INT           IDENTITY (1, 1) NOT NULL,
    [OwnerId]                  INT           NOT NULL,
    [ChannelId]                INT           NOT NULL,
    [SourceId]                 INT           NOT NULL,
    [SourcesTPId]              INT           NOT NULL,
    [AllowFranchisetoReassign] BIT           NULL,
    [IsActive]                 BIT           CONSTRAINT [SourcesHFC_IsActive] DEFAULT ((1)) NOT NULL,
    [IsDeleted]                BIT           CONSTRAINT [SourcesHFC_IsDeleted] DEFAULT ((0)) NOT NULL,
    [LMDBSourceID]             INT           NULL,
    [LeadType]                 VARCHAR (200) NULL,
    CONSTRAINT [PK_SourcesHFC] PRIMARY KEY CLUSTERED ([SourcesHFCId] ASC),
    CONSTRAINT [FK_SourcesHFC_SourcesTP] FOREIGN KEY ([SourcesTPId]) REFERENCES [CRM].[SourcesTP] ([SourcesTPId]),
    CONSTRAINT [AK_UniqueSourcesHFC] UNIQUE NONCLUSTERED ([OwnerId] ASC, [ChannelId] ASC, [SourceId] ASC, [LeadType] ASC)
);

