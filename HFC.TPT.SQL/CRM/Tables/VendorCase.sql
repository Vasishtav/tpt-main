﻿CREATE TABLE [CRM].[VendorCase] (
    [VendorCaseId]     INT           IDENTITY (1, 1) NOT NULL,
    [CaseId]           INT           NOT NULL,
    [VendorCaseNumber] NVARCHAR(10)           NULL,
    [Status]           INT           NULL,
    [VendorId]         INT           NULL,
    [CreatedOn]        DATETIME2 (7) NOT NULL,
    [CreatedBy]        INT           NOT NULL,
    [LastUpdatedOn]    DATETIME2 (7) NULL,
    [LastUpdatedBy]    INT           NULL,
    CONSTRAINT [PK_VendorCase] PRIMARY KEY CLUSTERED ([VendorCaseId] ASC),
    CONSTRAINT [FK_FranchiseCase_Id] FOREIGN KEY ([CaseId]) REFERENCES [CRM].[FranchiseCase] ([CaseId])
);

