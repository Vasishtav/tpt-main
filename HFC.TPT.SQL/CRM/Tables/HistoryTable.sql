﻿CREATE TABLE [CRM].[HistoryTable] (
    [Id]            INT            IDENTITY (1, 1) NOT NULL,
    [Table]         NVARCHAR (200) NULL,
    [TableId]       INT            NULL,
    [Date]          DATETIME2 (7)  NULL,
    [Field]         NVARCHAR (200) NULL,
    [UserId]        INT            NULL,
    [OriginalValue] NVARCHAR (MAX) NULL,
    [NewValue]      NVARCHAR (MAX) NULL,
    [CreatedOn]     DATETIME2 (7)  NULL,
    [CreatedBy]     INT            NULL,
    [LastUpdatedOn] DATETIME2 (7)  NULL,
    [LastUpdatedBy] INT            NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

