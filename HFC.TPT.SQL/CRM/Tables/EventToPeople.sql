﻿CREATE TABLE [CRM].[EventToPeople] (
    [EventPersonId]          INT                IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [TaskId]                 INT                NULL,
    [CalendarId]             INT                NULL,
    [PersonId]               INT                NULL,
    [PersonEmail]            VARCHAR (256)      NULL,
    [PersonName]             VARCHAR (128)      NULL,
    [RemindDate]             DATETIMEOFFSET (2) NULL,
    [CreatedOnUtc]           DATETIME2 (2)      CONSTRAINT [DF_EventToPeople_CreatedOnUtc] DEFAULT (getutcdate()) NOT NULL,
    [DismissedDate]          DATETIMEOFFSET (2) NULL,
    [SnoozedDate]            DATETIMEOFFSET (2) NULL,
    [RemindMethodEnum]       TINYINT            NULL,
    [RecurringNextStartDate] DATETIMEOFFSET (2) NULL,
    [RecurringNextEndDate]   DATETIMEOFFSET (2) NULL,
    CONSTRAINT [PK_EventToPeople] PRIMARY KEY CLUSTERED ([EventPersonId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_EventToPeople_Calendar] FOREIGN KEY ([CalendarId]) REFERENCES [CRM].[Calendar] ([CalendarId]) ON DELETE CASCADE,
    CONSTRAINT [FK_EventToPeople_Person] FOREIGN KEY ([PersonId]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_EventToPeople_Tasks] FOREIGN KEY ([TaskId]) REFERENCES [CRM].[Tasks] ([TaskId]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [CalendarOrTaskIdx]
    ON [CRM].[EventToPeople]([CalendarId] ASC, [TaskId] ASC, [PersonId] ASC)
    INCLUDE([EventPersonId], [PersonEmail], [PersonName], [RemindDate], [CreatedOnUtc], [DismissedDate], [SnoozedDate], [RemindMethodEnum]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [CalendarTaskPersonIdx]
    ON [CRM].[EventToPeople]([PersonId] ASC, [RemindDate] ASC, [DismissedDate] ASC, [RemindMethodEnum] ASC)
    INCLUDE([EventPersonId], [TaskId], [CalendarId]) WITH (FILLFACTOR = 90);

