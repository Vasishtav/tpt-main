﻿CREATE TABLE [CRM].[CaseFeedbackDetails]
(
	CaseFeedbackId	int NOT NULL IDENTITY,
CaseId	int,
RecordTypeId	int,
ProductGroupId	int,
TouchpointURL	nvarchar(256),
TouchpointVersion nvarchar(256),
BrowserDetails	nvarchar(256),
PICJson	nvarchar(max),
ChangeCaseId	int,
CloseReason	INT,
ClosedOn	datetime2,
CreatedOn	datetime2,
CreatedBy	int,
LastUpdatedOn	datetime2,
LastUpdatedBy	int, 
    [FieldErrors] NVARCHAR(MAX) NULL, 
    CONSTRAINT [PK_CaseFeedbackDetails] PRIMARY KEY ([CaseFeedbackId]),
	CONSTRAINT [FK_CaseFeedbackDetails_Case] FOREIGN KEY ([CaseId]) REFERENCES [CRM].[Case] ([CaseId]))

