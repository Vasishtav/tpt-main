﻿CREATE TABLE [CRM].[TaxSettingsAudit]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
    [TaxSettingsId] INT NOT NULL,
    [OldUseTaxValue] NUMERIC(18, 2) NOT NULL DEFAULT 0,
    [NewUseTaxValue] NUMERIC(18, 2) NOT NULL DEFAULT 0,
    [OldTaxSettingsJson] VARCHAR(1000) NULL,
    [NewTaxSettingsJson] VARCHAR(1000) NULL,
    [UseTaxChanged] BIT NOT NULL DEFAULT 0,
    [UpdatedBy] INT NULL,
    [UpdatedOn] DATETIME2 NULL,
    CONSTRAINT [FK_TaxSettingsAudit_TaxSettings] FOREIGN KEY ([TaxSettingsId]) REFERENCES [CRM].[TaxSettings](Id)
)
