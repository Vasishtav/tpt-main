﻿CREATE TABLE [CRM].[LeadStatusTracker] (
    [LeadStatusTrackerId] INT      IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [LeadId]              INT      NOT NULL,
    [LeadStatusId]        SMALLINT NOT NULL,
    [CreatedOn]           DATETIME NOT NULL,
    [CreatedBy]           INT      NULL,
    CONSTRAINT [FK_Leads_LeadStatusTracker] FOREIGN KEY ([LeadId]) REFERENCES [CRM].[Leads] ([LeadId]),
    CONSTRAINT [FK_LeadStatus_LeadStatusTracker] FOREIGN KEY ([LeadStatusId]) REFERENCES [CRM].[Type_LeadStatus] ([Id])
);

