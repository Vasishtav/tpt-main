﻿CREATE TABLE [CRM].[ShipmentStatus] (
    [ShipmentDetailId]     INT      NOT NULL,
    [ShipmentStatusId]             INT      NOT NULL,
    CONSTRAINT [UQ_ShipmentStatus_ShipmentDetailID_StatusId] UNIQUE(ShipmentDetailId, ShipmentStatusId), 
    CONSTRAINT [FK_ShipmentStatus_ShipmentDetailID] FOREIGN KEY ([ShipmentDetailID]) REFERENCES [CRM].[ShipmentDetail] ([ShipmentDetailId]),
    CONSTRAINT [FK_ShipmentStatus_StatusId] FOREIGN KEY ([ShipmentStatusId]) REFERENCES [CRM].[Type_ShipmentStatus] ([ShipmentStatusId])
);

