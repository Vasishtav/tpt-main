﻿CREATE TABLE [CRM].[AccountNJobNumbers] (
    [FranchiseId]   INT NOT NULL,
    [JobNumber]     INT CONSTRAINT [DF_JobAccountNumbers_JobNumber] DEFAULT ((0)) NOT NULL,
    [AccountNumber] INT CONSTRAINT [DF_JobAccountNumbers_AccountNum] DEFAULT ((1)) NOT NULL,
    [OrderNumber]   INT NULL,
    [InvoiceNumber] INT NULL,
    CONSTRAINT [PK_AccountJobNumbers] PRIMARY KEY CLUSTERED ([FranchiseId] ASC) WITH (FILLFACTOR = 90)
);

