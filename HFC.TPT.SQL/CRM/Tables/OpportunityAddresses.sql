﻿CREATE TABLE [CRM].[OpportunityAddresses] (
    [OpportunityId] INT NOT NULL,
    [AddressId]     INT NOT NULL,
    CONSTRAINT [PK_OpportunityAddresses] PRIMARY KEY CLUSTERED ([OpportunityId] ASC, [AddressId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_OpportunityAddresses_Addresses] FOREIGN KEY ([AddressId]) REFERENCES [CRM].[Addresses] ([AddressId]) ON DELETE CASCADE,
    CONSTRAINT [FK_OpportunityAddresses_Opportunitys] FOREIGN KEY ([OpportunityId]) REFERENCES [CRM].[Opportunities] ([OpportunityId])
);

