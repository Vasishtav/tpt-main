﻿CREATE TABLE [CRM].[Type_ProductCategory] (
    [ProductCategoryId] INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ProductCategory]   VARCHAR (100) NOT NULL,
    [Parant]            INT           NOT NULL,
    [IsActive]          BIT           NOT NULL,
    [IsDeleted]         BIT           NULL,
    [CreatedOn]         DATETIME2 (7) NOT NULL,
    [CreatedBy]         INT           NOT NULL,
    [LastUpdatedOn]     DATETIME2 (7) NULL,
    [LastUpdatedBy]     INT           NULL,
    [PICGroupId]        INT           NULL,
    [PICProductId]      INT           NULL,
    [BrandId]           INT           DEFAULT ((1)) NOT NULL,
    [SalesTaxCode]      VARCHAR (100) NULL,
    [ProductTypeId]     INT           DEFAULT ((2)) NOT NULL,
    CONSTRAINT [PK_Type_ProductCategory] PRIMARY KEY CLUSTERED ([ProductCategoryId] ASC),
    CONSTRAINT [FK_ProductTypeId_Type_ProductType] FOREIGN KEY ([ProductTypeId]) REFERENCES [CRM].[Type_ProductType] ([Id]),
    CONSTRAINT [FK_Type_ProductCategory_Createdby] FOREIGN KEY ([CreatedBy]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_Type_ProductCategory_Modifiedby] FOREIGN KEY ([LastUpdatedBy]) REFERENCES [CRM].[Person] ([PersonId])
);

