﻿CREATE TABLE [CRM].[Type_MasterStatus] (
    [Id]          INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Description] NVARCHAR (50) NULL,
    CONSTRAINT [PK_Type_MasterStatus] PRIMARY KEY CLUSTERED ([Id] ASC)
);

