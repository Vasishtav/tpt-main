﻿CREATE TABLE [CRM].[FranchiseCaseAddInfo] (
    [Id]                 INT             IDENTITY (1, 1) NOT NULL,
    [VendorCaseNumber]   NVARCHAR(10)             NULL,
    [CaseId]             INT             NOT NULL,
    [QuoteLineId]        INT             NOT NULL,
    [Type]               INT             NULL,
    [ReasonCode]         INT             NULL,
    [IssueDescription]   VARCHAR (7500)  NULL,
    [ExpediteReq]        BIT             NULL,
    [ExpediteApproved]   BIT             NULL,
    [ReqTripCharge]      BIT             NULL,
    [TripChargeApproved] INT NULL,
    [Status]             INT             NULL,
    [Resolution]         INT             NULL,
    [CreatedOn]          DATETIME2 (7)   NOT NULL,
    [CreatedBy]          INT             NOT NULL,
    [LastUpdatedOn]      DATETIME2 (7)   NULL,
    [LastUpdatedBy]      INT             NULL,
    [ConvertToVendor]    BIT             NULL,
    [Amount] NUMERIC(18, 2) NULL, 
    CONSTRAINT [PK_FranchiseCaseAddInfo] PRIMARY KEY CLUSTERED ([Id] ASC)
);

