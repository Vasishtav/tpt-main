﻿CREATE TABLE [CRM].[UserWidgets] (
    [UserWidgetId] INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [FranchiseId]  INT              NULL,
    [UserId]       UNIQUEIDENTIFIER NULL,
    [RoleId]       UNIQUEIDENTIFIER NULL,
    [Show]         BIT              NULL,
    [WidgetId]     INT              NOT NULL,
    CONSTRAINT [PK_User_Widgets] PRIMARY KEY CLUSTERED ([UserWidgetId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Key_UserWidgets_Roles] FOREIGN KEY ([RoleId]) REFERENCES [Auth].[Roles] ([RoleId]),
    CONSTRAINT [FK_Key_UserWidgets_Users] FOREIGN KEY ([UserId]) REFERENCES [Auth].[Users] ([UserId]),
    CONSTRAINT [FK_UserWidgets_Franchise] FOREIGN KEY ([FranchiseId]) REFERENCES [CRM].[Franchise] ([FranchiseId]),
    CONSTRAINT [FK_UserWidgets_Type_Widgets] FOREIGN KEY ([WidgetId]) REFERENCES [CRM].[Type_Widgets] ([WidgetId])
);

