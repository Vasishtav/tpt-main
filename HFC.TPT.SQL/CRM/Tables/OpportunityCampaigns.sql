﻿CREATE TABLE [CRM].[OpportunityCampaigns] (
    [OpportunityCampaignId] INT      IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [MktCampaignId]         INT      NOT NULL,
    [OpportunityId]         INT      NOT NULL,
    [CreatedOnUtc]          DATETIME CONSTRAINT [DF_Key_OpportunityCampaigns_CreatedOnUtc] DEFAULT (getutcdate()) NOT NULL,
    [IsManuallyAdded]       BIT      CONSTRAINT [DF_Key_OpportunityCampaigns_IsManuallyAdded] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_OpportunityCampaigns_1] PRIMARY KEY CLUSTERED ([OpportunityCampaignId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Key_OpportunityCampaigns_MarketingCampaigns] FOREIGN KEY ([MktCampaignId]) REFERENCES [CRM].[MarketingCampaigns] ([MktCampaignId]),
    CONSTRAINT [FK_Key_OpportunityCampaigns_Opportunitys] FOREIGN KEY ([OpportunityId]) REFERENCES [CRM].[Opportunities] ([OpportunityId])
);

