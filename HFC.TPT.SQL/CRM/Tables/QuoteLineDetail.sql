﻿CREATE TABLE [CRM].[QuoteLineDetail] (
    [QuoteLineDetailId]       INT             IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [QuoteLineId]             INT             NOT NULL,
    [Quantity]                INT             NOT NULL,
    [BaseCost]                NUMERIC (18, 2) NULL,
    [BaseResalePrice]         NUMERIC (18, 2) NULL,
    [BaseResalePriceWOPromos] NUMERIC (18, 2) NULL,
    [PricingCode]             VARCHAR (5)     NULL,
    [PricingRule]             VARCHAR (50)    NULL,
    [MarkupFactor]            NUMERIC (18, 2) NULL,
    [Rounding]                VARCHAR (100)   NULL,
    [SuggestedResale]         NUMERIC (18, 2) NULL,
    [Discount]                NUMERIC (18, 2) NULL,
    [unitPrice]               NUMERIC (18, 2) NULL,
    [DiscountAmount]          NUMERIC (18, 2) NULL,
    [ExtendedPrice]           NUMERIC (18, 2) NULL,
    [MarginPercentage]        NUMERIC (18, 2) NULL,
    [PricingId]               INT             NULL,
    [ProductOrOption]         VARCHAR (100)   NULL,
    [SelectedPrice]           VARCHAR (100)   NULL,
    [CreatedOn]               DATETIME2 (7)   NOT NULL,
    [CreatedBy]               INT             NOT NULL,
    [LastUpdatedOn]           DATETIME2 (7)   NULL,
    [LastUpdatedBy]           INT             NULL,
    [Cost]                    NUMERIC (18, 2) NULL,
    [NetCharge]               NUMERIC (18, 2) NULL,
    [ManualUnitPrice]         BIT             DEFAULT ((0)) NULL,
    [CounterId] INT NULL, 
    [ValidConfiguration] BIT NULL, 
    [ProductErrors] VARCHAR(MAX) NULL, 
    [LastValidatedon] DATETIME2 NULL, 
    CONSTRAINT [PK_QuoteLineDetail] PRIMARY KEY CLUSTERED ([QuoteLineDetailId] ASC),
    CONSTRAINT [FK_QuoteLineDetail_Person_CreatedBy] FOREIGN KEY ([CreatedBy]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_QuoteLineDetail_Person_LastUpdatedBy] FOREIGN KEY ([LastUpdatedBy]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_QuoteLineDetail_PricingSettings_Id] FOREIGN KEY ([PricingId]) REFERENCES [CRM].[PricingSettings] ([Id]),
    CONSTRAINT [FK_QuoteLineDetail_QuoteLines_QuoteLineId] FOREIGN KEY ([QuoteLineId]) REFERENCES [CRM].[QuoteLines] ([QuoteLineId])
);
go
CREATE NONCLUSTERED INDEX [_dta_index_QuoteLineDetail_6_1152541396__K2_3_4_25] ON [CRM].[QuoteLineDetail]
(
	[QuoteLineId] ASC
)
INCLUDE([Quantity],[BaseCost],[NetCharge]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

