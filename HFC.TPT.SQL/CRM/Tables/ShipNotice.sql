﻿CREATE TABLE [CRM].[ShipNotice] (
    [ShipNoticeId]           INT             IDENTITY (1, 1) NOT NULL,
    [ShipmentId]             VARCHAR (50)    NULL,
    [ShippedDate]            DATETIME        NULL,
    [EstDeliveryDate]        DATETIME        NULL,
    [ShippedVia]             VARCHAR (100)   NULL,
    [ShippedViaText]         VARCHAR (100)   NULL,
    [BOL]                    VARCHAR (100)   NULL,
    [Weight]                 NUMERIC (18, 3) NULL,
    [TotalBoxes]             INT             NULL,
    [PurchaseOrderId]        INT             NOT NULL,
    [PurchaseOrdersDetailId] INT             NULL,
    [VendorName]             VARCHAR (100)   NULL,
    [ReceivedComplete]       BIT             NULL,
    [ReceivedCompleteDate]   DATETIME        NULL,
    [ShipNoticeJson]         VARCHAR (MAX)   NULL,
    [CreatedBy]              INT             NULL,
    [CreatedOn]              DATETIME        NULL,
    [UpdatedBy]              INT             NULL,
    [UpdatedOn]              DATETIME        NULL,
    [PICVpo]                 VARCHAR (25)    NULL,
    [TrackingURL]            VARCHAR (500)   NULL,
    [TrackingNumber]         VARCHAR (100)   NULL,
    [CheckedinDate]          DATETIME        NULL, 
    [RemakeReference]        NVARCHAR(100)   NULL, 
    CONSTRAINT [PK_ShipNotice] PRIMARY KEY CLUSTERED ([ShipNoticeId] ASC)
);

go

CREATE NONCLUSTERED INDEX [_dta_index_ShipNotice_6_211350074__K19_K3_K1] ON [CRM].[ShipNotice]
(
	[PICVpo] ASC,
	[ShippedDate] ASC,
	[ShipNoticeId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
