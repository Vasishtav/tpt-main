﻿CREATE TABLE [CRM].[FranchiseDiscount] (
    [DiscountIdPk]   INT             IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [DiscountId]     INT             NOT NULL,
    [Name]           VARCHAR (50)    NOT NULL,
    [FranchiseId]    INT             NOT NULL,
    [Description]    VARCHAR (250)   NULL,
    [Status]         INT             NULL,
    [DiscountValue]  DECIMAL (18, 2) NULL,
    [DiscountFactor] VARCHAR (1)     NULL,
    [StartDate]      DATETIME        NULL,
    [EndDate]        DATETIME        NULL,
    [CreatedBy]      INT             NOT NULL,
    [CreatedOn]      DATETIME2 (7)   NOT NULL,
    [LastUpdatedBy]  INT             NULL,
    [LastUpdatedOn]  DATETIME2 (7)   NULL,
    CONSTRAINT [PK_FranchiseDiscount_1] PRIMARY KEY CLUSTERED ([DiscountIdPk] ASC)
);

