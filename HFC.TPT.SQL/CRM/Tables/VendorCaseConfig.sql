﻿CREATE TABLE [CRM].[VendorCaseConfig] (
    [Id]                 INT            IDENTITY (1, 1) NOT NULL,
    [VendorId]           INT            NOT NULL,
    [CaseEnabled]        BIT            NOT NULL,
    [CaseLogin]          NVARCHAR (100) NULL,
    [VendorSupportEmail] NVARCHAR (100) NULL,
    [VGEmailNotify]      BIT            NULL,
    [CreatedOn]          DATETIME2 (7)  NOT NULL,
    [CreatedBy]          INT            NOT NULL,
    [LastUpdatedOn]      DATETIME2 (7)  NULL,
    [LastUpdatedBy]      INT            NULL,
    [PersonId] INT NOT NULL DEFAULT 0, 
    CONSTRAINT [PK_VendorCaseConfig] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_VendorCaseConfig_VendorId] FOREIGN KEY ([VendorId]) REFERENCES [Acct].[HFCVendors] ([VendorIdPk])
);

