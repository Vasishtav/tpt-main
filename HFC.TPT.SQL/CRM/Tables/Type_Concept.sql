﻿CREATE TABLE [CRM].[Type_Concept] (
    [Id]           SMALLINT     IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Concept]      VARCHAR (50) NULL,
    [Abbreviation] VARCHAR (5)  NULL,
    [BrandId]      SMALLINT     CONSTRAINT [DF_Type_Concept_BrandId] DEFAULT ((1)) NOT NULL,
    [OriginalId]   SMALLINT     NULL,
    CONSTRAINT [ucConceptBrandId] UNIQUE NONCLUSTERED ([Concept] ASC, [BrandId] ASC)
);

