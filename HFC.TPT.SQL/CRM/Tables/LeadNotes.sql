﻿CREATE TABLE [CRM].[LeadNotes] (
    [NoteId]            INT                IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [LeadId]            INT                NOT NULL,
    [Message]           VARCHAR (MAX)      NULL,
    [CreatedOn]         DATETIMEOFFSET (2) CONSTRAINT [DF_LeadNotes_CreatedOnUtc] DEFAULT (getdate()) NOT NULL,
    [CreatedByPersonId] INT                NULL,
    [TypeEnum]          TINYINT            CONSTRAINT [DF_LeadNotes_TypeEnum] DEFAULT ((0)) NOT NULL,
    [Title]             VARCHAR (50)       NULL,
    CONSTRAINT [PK_LeadNotes] PRIMARY KEY CLUSTERED ([NoteId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_LeadNotes_Leads] FOREIGN KEY ([LeadId]) REFERENCES [CRM].[Leads] ([LeadId]),
    CONSTRAINT [FK_LeadNotes_Person] FOREIGN KEY ([CreatedByPersonId]) REFERENCES [CRM].[Person] ([PersonId])
);

