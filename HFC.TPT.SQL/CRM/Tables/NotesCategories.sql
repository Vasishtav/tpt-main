﻿CREATE TABLE [CRM].[NotesCategories]
(
	[NoteId] INT NOT NULL ,
	[TypeEnum] TINYINT NOT NULL, 
    CONSTRAINT [FK_NotesCategories_NoteId] FOREIGN KEY ([NoteId]) REFERENCES [CRM].[Note] ([NoteId]),
	CONSTRAINT [UQ_NoteId_TypeEnum2] UNIQUE NONCLUSTERED( [NoteId], [TypeEnum])

)
