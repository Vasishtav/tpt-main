﻿CREATE TABLE [CRM].[OpportunityCustomers] (
    [OpportunityId]     INT NOT NULL,
    [CustomerId]        INT NOT NULL,
    [IsPrimaryCustomer] BIT DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_OpportunityCustomers] PRIMARY KEY CLUSTERED ([OpportunityId] ASC, [CustomerId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_OpportunityCustomers_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [CRM].[Customer] ([CustomerId]) ON DELETE CASCADE
);

