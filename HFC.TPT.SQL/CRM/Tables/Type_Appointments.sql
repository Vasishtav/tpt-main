﻿CREATE TABLE [CRM].[Type_Appointments] (
    [AppointmentTypeId] INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Name]              VARCHAR (50) NULL,
    [FranchiseId]       INT          NULL,
    CONSTRAINT [PK_Type_Appointments] PRIMARY KEY CLUSTERED ([AppointmentTypeId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Type_Appointments_Franchise] FOREIGN KEY ([FranchiseId]) REFERENCES [CRM].[Franchise] ([FranchiseId])
);

