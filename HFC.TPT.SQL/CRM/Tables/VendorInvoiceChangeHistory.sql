﻿CREATE TABLE [CRM].[VendorInvoiceChangeHistory](
	[VendorInvoiceChangeHistoryId] [int] IDENTITY(1,1) NOT NULL,
	[VendorInvoiceId] [int] NOT NULL,
	[VendorInvoiceDetailId] [int] NULL,
	[PersonId] [int] NOT NULL,
	[Change] [varchar](500) NULL,
	[AdjValue] [varchar](max) NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_VendorInvoiceChangeHistoryId] PRIMARY KEY CLUSTERED ([VendorInvoiceChangeHistoryId] ASC)
)