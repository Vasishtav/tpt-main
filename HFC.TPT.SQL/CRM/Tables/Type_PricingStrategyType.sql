﻿CREATE TABLE [CRM].[Type_PricingStrategyType] (
    [Id]                  INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PricingStrategyType] VARCHAR (100) NOT NULL,
    [IsActive]            BIT           NOT NULL,
    [IsDeleted]           BIT           NULL,
    [CreatedOn]           DATETIME2 (7) NOT NULL,
    [CreatedBy]           INT           NOT NULL,
    [LastUpdatedOn]       DATETIME2 (7) NULL,
    [LastUpdatedBy]       INT           NULL,
    CONSTRAINT [PK_Type_PricingStrategyType] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Type_PricingStrategyType_Createdby] FOREIGN KEY ([CreatedBy]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_Type_PricingStrategyType_Modifiedby] FOREIGN KEY ([LastUpdatedBy]) REFERENCES [CRM].[Person] ([PersonId])
);

