﻿CREATE TABLE [CRM].[WebOEJobs] (
    [JobId]            INT      NOT NULL,
    [WebOEOrderId]     INT      NOT NULL,
    [WebOECustomerId]  INT      NULL,
    [LastUpdatedOnUtc] DATETIME NULL,
    CONSTRAINT [PK_WebOEJobs] PRIMARY KEY CLUSTERED ([JobId] ASC, [WebOEOrderId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Jobs_WebOEJobs] FOREIGN KEY ([JobId]) REFERENCES [CRM].[Jobs] ([JobId])
);

