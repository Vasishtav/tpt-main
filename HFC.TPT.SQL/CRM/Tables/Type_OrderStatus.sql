﻿CREATE TABLE [CRM].[Type_OrderStatus] (
    [OrderStatusId] INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [OrderStatus]   VARCHAR (100) NOT NULL,
    [IsActive]      BIT           NOT NULL,
    [IsDeleted]     BIT           NULL,
    [CreatedOn]     DATETIME2 (7) NOT NULL,
    [CreatedBy]     INT           NOT NULL,
    [LastUpdatedOn] DATETIME2 (7) NULL,
    [LastUpdatedBy] INT           NULL,
    CONSTRAINT [PK_TypeOrderStatus] PRIMARY KEY CLUSTERED ([OrderStatusId] ASC),
    CONSTRAINT [FK_Type_OrderStatus_Createdby] FOREIGN KEY ([CreatedBy]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_Type_OrderStatus_Modifiedby] FOREIGN KEY ([LastUpdatedBy]) REFERENCES [CRM].[Person] ([PersonId])
);

