﻿CREATE TABLE [CRM].[Type_StatusChanges] (
    [Id]                  INT IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [MasterStatusTypeId]  INT NULL,
    [SourceStatusId]      INT NULL,
    [DestinationStatusId] INT NULL,
    CONSTRAINT [PK_Type_StatusChanges] PRIMARY KEY CLUSTERED ([Id] ASC)
);

