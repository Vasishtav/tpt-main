﻿CREATE TABLE [CRM].[Category_Color] (
    [Category_ColorId] INT      IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [CategoryId]       INT      NOT NULL,
    [NamedColorId]     INT      NOT NULL,
    [FranchiseId]      INT      NULL,
    [CreatedOn]        DATETIME NULL,
    CONSTRAINT [PK_Category_Color] PRIMARY KEY CLUSTERED ([Category_ColorId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_CategoryId] FOREIGN KEY ([CategoryId]) REFERENCES [CRM].[Type_Category] ([Id]),
    CONSTRAINT [FK_NamedColorId] FOREIGN KEY ([NamedColorId]) REFERENCES [CRM].[Type_NamedColor] ([NamedColorId])
);

