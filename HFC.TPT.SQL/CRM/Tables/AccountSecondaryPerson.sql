﻿CREATE TABLE [CRM].[AccountSecondaryPerson] (
    [AccountId] INT NOT NULL,
    [PersonId]  INT NOT NULL,
    CONSTRAINT [PK_AccountSecondaryPerson] PRIMARY KEY CLUSTERED ([AccountId] ASC, [PersonId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_AccountSecondaryPerson_Accounts] FOREIGN KEY ([AccountId]) REFERENCES [CRM].[Accounts] ([AccountId]),
    CONSTRAINT [FK_AccountSecondaryPerson_Person] FOREIGN KEY ([PersonId]) REFERENCES [CRM].[Person] ([PersonId]) ON DELETE CASCADE
);

