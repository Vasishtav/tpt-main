﻿CREATE TABLE [CRM].[Attachments] (
    [AttachmentId]   INT        NOT NULL,
    [FranchiseId]    INT        NULL,
    [LeadId]         INT        NULL,
    [JobId]          INT        NULL,
    [QuoteId]        INT        NULL,
    [PhysicalFileId] INT        NULL,
    [CustomerId]     INT        NULL,
    [createdOn]      INT        NULL,
    [CreatedBy]      NCHAR (10) NULL,
    [AccountId]      INT        DEFAULT ((0)) NULL,
    [OpportunityId]  INT        DEFAULT ((0)) NULL,
    CONSTRAINT [PK_Attachments] PRIMARY KEY CLUSTERED ([AttachmentId] ASC),
    CONSTRAINT [FK_Attachment_Franchise] FOREIGN KEY ([FranchiseId]) REFERENCES [CRM].[Franchise] ([FranchiseId]),
    CONSTRAINT [FK_Attachments_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [CRM].[Customer] ([CustomerId]),
    CONSTRAINT [FK_Attachments_Job] FOREIGN KEY ([JobId]) REFERENCES [CRM].[Jobs] ([JobId]),
    CONSTRAINT [FK_Attachments_Lead] FOREIGN KEY ([LeadId]) REFERENCES [CRM].[Leads] ([LeadId]),
    CONSTRAINT [FK_Attachments_PhysicalFiles] FOREIGN KEY ([PhysicalFileId]) REFERENCES [CRM].[PhysicalFiles] ([PhysicalFileId]),
    CONSTRAINT [FK_Attachments_Quote] FOREIGN KEY ([QuoteId]) REFERENCES [CRM].[JobQuotes] ([QuoteId])
);

