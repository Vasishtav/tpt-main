﻿CREATE TABLE [CRM].[WebOEMfgs] (
    [ManufacturerId] INT NOT NULL,
    [WebOEMfgId]     INT NOT NULL,
    CONSTRAINT [PK_WebOEMfgs] PRIMARY KEY CLUSTERED ([ManufacturerId] ASC, [WebOEMfgId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Manufacturers_Manufacturers1] FOREIGN KEY ([ManufacturerId]) REFERENCES [CRM].[Manufacturers] ([ManufacturerId])
);

