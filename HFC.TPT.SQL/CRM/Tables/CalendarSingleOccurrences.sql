﻿CREATE TABLE [CRM].[CalendarSingleOccurrences] (
    [SingleOccurrenceId] INT                IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [RecurringEventId]   INT                NOT NULL,
    [OriginalStartDate]  DATETIMEOFFSET (2) NOT NULL,
    [DeletedOnUtc]       DATETIME2 (2)      NULL,
    [CalendarId]         INT                NULL,
    [LastUpdatedUtc]     DATETIME2 (2)      CONSTRAINT [DF_CalendarSingleOccurrences_LastUpdatedUtc] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_EventRecurringSingleOccurrences] PRIMARY KEY CLUSTERED ([SingleOccurrenceId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_CalendarSingleOccurrences_Calendar] FOREIGN KEY ([CalendarId]) REFERENCES [CRM].[Calendar] ([CalendarId]),
    CONSTRAINT [FK_EventRecurringSingleOccurrences_EventRecurring] FOREIGN KEY ([RecurringEventId]) REFERENCES [CRM].[EventRecurring] ([RecurringEventId])
);

