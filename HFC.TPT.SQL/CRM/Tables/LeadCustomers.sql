﻿CREATE TABLE [CRM].[LeadCustomers] (
    [LeadId]            INT NOT NULL,
    [CustomerId]        INT NOT NULL,
    [IsPrimaryCustomer] BIT DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_LeadCustomers] PRIMARY KEY CLUSTERED ([LeadId] ASC, [CustomerId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_LeadCustomers_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [CRM].[Customer] ([CustomerId]) ON DELETE CASCADE
);

