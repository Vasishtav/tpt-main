﻿CREATE TABLE [CRM].[MPOErrorEditConfigAudit]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [PurchaseOrderId] INT NOT NULL, 
    [QuoteLineId] INT NOT NULL, 
    [PromptAnswerOLD] VARCHAR(MAX) NULL, 
    [PromptAnswers] VARCHAR(MAX) NULL, 
    [PICResponseOLD] VARCHAR(MAX) NULL, 
	[old_quoteline]  VARCHAR(MAX) NULL, 
    [PICResponse] VARCHAR(MAX) NULL, 
    [UpdatedBy] INT NOT NULL, 
    [UpdatedOn] DATETIME2 NOT NULL, 
    [Impersonatedby] INT NULL
)
