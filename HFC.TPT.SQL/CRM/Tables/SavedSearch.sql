﻿CREATE TABLE [CRM].[SavedSearch] (
    [SavedSearchID] INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [UserId]        UNIQUEIDENTIFIER NOT NULL,
    [Name]          VARCHAR (200)    NOT NULL,
    [SearchParams]  VARCHAR (MAX)    NULL,
    CONSTRAINT [PK_SavedSearch] PRIMARY KEY NONCLUSTERED ([SavedSearchID] ASC) WITH (FILLFACTOR = 90)
);

