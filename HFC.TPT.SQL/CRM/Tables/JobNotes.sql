﻿CREATE TABLE [CRM].[JobNotes] (
    [NoteId]            INT                IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [JobId]             INT                NOT NULL,
    [Message]           VARCHAR (MAX)      NULL,
    [CreatedOn]         DATETIMEOFFSET (2) CONSTRAINT [DF_JobNotes_CreatedOnUtc] DEFAULT (getdate()) NOT NULL,
    [CreatedByPersonId] INT                NULL,
    [TypeEnum]          TINYINT            CONSTRAINT [DF_JobNotes_TypeEnum] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_JobNotes] PRIMARY KEY CLUSTERED ([NoteId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_JobNotes_Jobs] FOREIGN KEY ([JobId]) REFERENCES [CRM].[Jobs] ([JobId]),
    CONSTRAINT [FK_JobNotes_Person] FOREIGN KEY ([CreatedByPersonId]) REFERENCES [CRM].[Person] ([PersonId])
);

