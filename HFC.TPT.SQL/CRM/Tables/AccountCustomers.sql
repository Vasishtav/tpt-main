﻿CREATE TABLE [CRM].[AccountCustomers] (
    [AccountId]         INT NOT NULL,
    [CustomerId]        INT NOT NULL,
    [IsPrimaryCustomer] BIT DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_AccountCustomers] PRIMARY KEY CLUSTERED ([AccountId] ASC, [CustomerId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_AccountCustomers_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [CRM].[Customer] ([CustomerId]) ON DELETE CASCADE
);

