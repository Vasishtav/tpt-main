﻿CREATE TABLE [CRM].[SavedCalendar] (
    [SaveCalendarId] INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [LoginUserid]    INT            NULL,
    [ViewTitle]      NVARCHAR (255) NULL,
    [Viewmode]       INT            NULL,
    [EmailId]        NVARCHAR (255) NULL,
    [FranchiseId]    INT            NULL,
    [IsBussinessHour] BIT NULL, 
    PRIMARY KEY CLUSTERED ([SaveCalendarId] ASC)
);

