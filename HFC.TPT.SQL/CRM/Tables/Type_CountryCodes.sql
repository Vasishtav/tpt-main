﻿CREATE TABLE [CRM].[Type_CountryCodes] (
    [CountryCodeId]  INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ISOCode2Digits] VARCHAR (2)  NULL,
    [ISOCode3Digits] VARCHAR (3)  NULL,
    [Country]        VARCHAR (50) NOT NULL,
    [ZipCodeMask]    VARCHAR (50) NULL,
    [PhoneMask]      VARCHAR (50) NULL,
    [CurrencyCode]   VARCHAR (10) NULL,
    CONSTRAINT [PK_Type_CountryCodes] PRIMARY KEY CLUSTERED ([CountryCodeId] ASC) WITH (FILLFACTOR = 90)
);

