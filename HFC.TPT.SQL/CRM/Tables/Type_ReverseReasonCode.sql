﻿CREATE TABLE [CRM].[Type_ReverseReasonCode] (
    [ReasonCodeId]  INT                IDENTITY (1, 1) NOT NULL,
    [Reason]        VARCHAR (50)       NOT NULL,
    [IsActive]      BIT                NOT NULL,
    [IsDeleted]     BIT                NOT NULL,
    [CreatedOn]     DATETIMEOFFSET (2) NOT NULL,
    [CreatedBy]     INT                NOT NULL,
    [LastUpdatedOn] DATETIMEOFFSET (2) NOT NULL,
    [LastUpdatedBy] INT                NOT NULL,
    CONSTRAINT [PK_Type_ReverseReasonCode] PRIMARY KEY CLUSTERED ([ReasonCodeId] ASC)
);

