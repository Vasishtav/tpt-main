﻿CREATE TABLE [CRM].[Type_OpportunityStatus] (
    [OpportunityStatusId] TINYINT       IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Name]                VARCHAR (100) NOT NULL,
    [Description]         VARCHAR (250) NULL,
    [BrandId]             TINYINT       NULL,
    [CreatedOn]           DATETIME      NOT NULL,
    [CreatedBy]           INT           NOT NULL,
    [UpdatedOn]           DATETIME      NULL,
    [UpdatedBy]           INT           NULL,
    CONSTRAINT [PK_Type_OpportunityStatus] PRIMARY KEY CLUSTERED ([OpportunityStatusId] ASC)
);

