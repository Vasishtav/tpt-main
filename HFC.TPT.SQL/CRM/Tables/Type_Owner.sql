﻿CREATE TABLE [CRM].[Type_Owner] (
    [OwnerId]     TINYINT       NOT NULL,
    [Name]        VARCHAR (50)  NOT NULL,
    [Description] VARCHAR (100) NULL,
    [BrandId]     TINYINT       NULL,
    [CreatedOn]   DATETIME      CONSTRAINT [DF_Type_Owner_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]   INT           NOT NULL,
    [UpdatedOn]   DATETIME      NULL,
    [UpdatedBy]   INT           NULL,
    [IsActive]    BIT           DEFAULT ((1)) NOT NULL,
    [IsDeleted]   BIT           DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Type_Owner] PRIMARY KEY CLUSTERED ([OwnerId] ASC)
);

