﻿CREATE TABLE [CRM].[TerritoryEmailSettings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TerritoryId] [int] NOT NULL,
	[UseFranchiseName] [bit] NULL,
	[LicenseNumber] [varchar](25) NULL,
	[FromEmailPersonId] int NULL,
	[ShipToLocationId] [int] NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastUpdatedOn] [datetime2](7) NULL,
	[LastUpdatedBy] [int] NULL,
 CONSTRAINT [Pk_TerritoryEmailSettings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [CRM].[TerritoryEmailSettings]  WITH CHECK ADD  CONSTRAINT [Fk_ShipToLocations_TerritoryEmailSettings_Id] FOREIGN KEY([ShipToLocationId])
REFERENCES [CRM].[ShipToLocations] ([Id])
GO

ALTER TABLE [CRM].[TerritoryEmailSettings] CHECK CONSTRAINT [Fk_ShipToLocations_TerritoryEmailSettings_Id]
GO

ALTER TABLE [CRM].[TerritoryEmailSettings]  WITH CHECK ADD  CONSTRAINT [Fk_Territories_TerritoryEmailSettings_TerritoryId] FOREIGN KEY([TerritoryId])
REFERENCES [CRM].[Territories] ([TerritoryId])
GO

ALTER TABLE [CRM].[TerritoryEmailSettings] CHECK CONSTRAINT [Fk_Territories_TerritoryEmailSettings_TerritoryId]
GO

ALTER TABLE [CRM].[TerritoryEmailSettings]  WITH CHECK ADD  CONSTRAINT [FK_TerritoryEmailSettings_Createdby] FOREIGN KEY([CreatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO

ALTER TABLE [CRM].[TerritoryEmailSettings] CHECK CONSTRAINT [FK_TerritoryEmailSettings_Createdby]
GO

ALTER TABLE [CRM].[TerritoryEmailSettings]  WITH CHECK ADD  CONSTRAINT [FK_TerritoryEmailSettings_Modifiedby] FOREIGN KEY([LastUpdatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO

ALTER TABLE [CRM].[TerritoryEmailSettings] CHECK CONSTRAINT [FK_TerritoryEmailSettings_Modifiedby]
GO

ALTER TABLE [CRM].[TerritoryEmailSettings]  WITH CHECK ADD  CONSTRAINT [Fk_Users_TerritoryEmailSettings_PersonId] FOREIGN KEY([FromEmailPersonId])
REFERENCES [CRM].[Person] ([PersonId])
GO

ALTER TABLE [CRM].[TerritoryEmailSettings] CHECK CONSTRAINT [Fk_Users_TerritoryEmailSettings_PersonId]
GO