﻿CREATE TABLE [CRM].[AccountSourceTP] (
    [SourcesTPId] INT IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [OwnerId]     INT NOT NULL,
    [ChannelId]   INT NOT NULL,
    [SourceId]    INT NOT NULL,
    CONSTRAINT [PK_AccountSourcesTP] PRIMARY KEY CLUSTERED ([SourcesTPId] ASC),
    CONSTRAINT [AK_UniqueAccountSourcesTP] UNIQUE NONCLUSTERED ([OwnerId] ASC, [ChannelId] ASC, [SourceId] ASC)
);

