﻿CREATE TABLE [CRM].[ChannelSourceSubsourceCampaignMapping] (
    [CSCMappingId] INT IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [CampaignId]   INT NOT NULL,
    [SubSourceId]  INT NOT NULL,
    [SourceId]     INT NOT NULL,
    [ChannelId]    INT NOT NULL,
    CONSTRAINT [PK_ChannelSourceSubsourceCampaignMapping] PRIMARY KEY CLUSTERED ([CSCMappingId] ASC)
);

