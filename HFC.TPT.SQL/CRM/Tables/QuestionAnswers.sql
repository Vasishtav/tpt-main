﻿CREATE TABLE [CRM].[QuestionAnswers] (
    [AnswerId]         INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [QuestionId]       INT           NOT NULL,
    [Answer]           VARCHAR (MAX) NULL,
    [LeadId]           INT           NULL,
    [JobId]            INT           NULL,
    [CreatedOnUtc]     DATETIME      CONSTRAINT [DF_QuestionAnswers_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [LoggedByPersonId] INT           NULL,
    [LastUpdatedOnUtc] DATETIME      NULL,
    [OpportunityId]    INT           DEFAULT ((0)) NULL,
    [AccountId]        INT           NULL,
    CONSTRAINT [PK_QuestionAnswers] PRIMARY KEY CLUSTERED ([AnswerId] ASC),
    CONSTRAINT [FK_QuestionAnswers_Type_QAQuestions] FOREIGN KEY ([QuestionId]) REFERENCES [CRM].[Type_Questions] ([QuestionId])
);

