﻿CREATE TABLE [CRM].[Manufacturers] (
    [ManufacturerId]   INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ManufacturerGuid] UNIQUEIDENTIFIER CONSTRAINT [DF_Manufacturers_ManufacturerGuid] DEFAULT (newid()) NOT NULL,
    [Name]             VARCHAR (256)    NULL,
    [isCustom]         BIT              NULL,
    CONSTRAINT [PK_Manufacturers] PRIMARY KEY CLUSTERED ([ManufacturerId] ASC) WITH (FILLFACTOR = 90)
);

