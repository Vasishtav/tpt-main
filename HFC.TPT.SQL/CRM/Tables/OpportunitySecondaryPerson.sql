﻿CREATE TABLE [CRM].[OpportunitySecondaryPerson] (
    [OpportunityId] INT NOT NULL,
    [PersonId]      INT NOT NULL,
    CONSTRAINT [PK_OpportunitySecondaryPerson] PRIMARY KEY CLUSTERED ([OpportunityId] ASC, [PersonId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_OpportunitySecondaryPerson_Opportunitys] FOREIGN KEY ([OpportunityId]) REFERENCES [CRM].[Opportunities] ([OpportunityId]),
    CONSTRAINT [FK_OpportunitySecondaryPerson_Person] FOREIGN KEY ([PersonId]) REFERENCES [CRM].[Person] ([PersonId]) ON DELETE CASCADE
);

