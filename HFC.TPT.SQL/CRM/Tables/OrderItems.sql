﻿CREATE TABLE [CRM].[OrderItems] (
    [OrderItemId]       INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [OrderId]           INT              NULL,
    [ProductStyleId]    INT              NULL,
    [Quantity]          SMALLINT         CONSTRAINT [DF_OrderItems_Quantity] DEFAULT ((0)) NOT NULL,
    [ManufacturerId]    INT              NULL,
    [Manufacturer]      VARCHAR (50)     NULL,
    [ProductTypeId]     INT              NULL,
    [ProductType]       VARCHAR (50)     NULL,
    [ProductId]         INT              NULL,
    [ProductName]       VARCHAR (100)    NULL,
    [ProductGuid]       UNIQUEIDENTIFIER NULL,
    [StyleId]           INT              NULL,
    [Style]             VARCHAR (50)     NULL,
    [CategoryId]        INT              NULL,
    [CategoryName]      VARCHAR (50)     NULL,
    [Description]       VARCHAR (1024)   NULL,
    [Color]             VARCHAR (100)    NULL,
    [ReferenceNumber]   VARCHAR (24)     NULL,
    [CreatedByPersonId] INT              NULL,
    [CreatedOnUtc]      DATETIME2 (2)    CONSTRAINT [DF_OrderItems_CreatedOnUtc] DEFAULT (getutcdate()) NOT NULL,
    [Cost]              DECIMAL (18, 4)  NULL,
    [JobItemId]         INT              NOT NULL,
    CONSTRAINT [PK_OrderItems] PRIMARY KEY CLUSTERED ([OrderItemId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_OrderItems_JobItems] FOREIGN KEY ([JobItemId]) REFERENCES [CRM].[JobItems] ([JobItemId]),
    CONSTRAINT [FK_OrderItems_Orders] FOREIGN KEY ([OrderId]) REFERENCES [CRM].[Orders_old] ([OrderId]),
    CONSTRAINT [FK_OrderItems_Person1] FOREIGN KEY ([CreatedByPersonId]) REFERENCES [CRM].[Person] ([PersonId])
);


GO
CREATE NONCLUSTERED INDEX [IX_QuoteId_JobItemId]
    ON [CRM].[OrderItems]([OrderId] ASC, [JobItemId] ASC) WITH (FILLFACTOR = 90);

