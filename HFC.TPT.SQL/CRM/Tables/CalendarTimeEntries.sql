﻿CREATE TABLE [CRM].[CalendarTimeEntries] (
    [Id]            INT           IDENTITY (1, 1) NOT NULL,
    [CalendarId]    INT           NOT NULL,
    [EntryType]     INT           NOT NULL,
    [StartDate]     DATETIME2 (7) NULL,
    [EndDate]       DATETIME2 (7) NULL,
    [Notes]         VARCHAR (500) NULL,
    [Adjusted]      BIT           NULL,
    [CreatedOn]     DATETIME2 (7) NOT NULL,
    [CreatedBy]     INT           NOT NULL,
    [LastUpdatedOn] DATETIME2 (7) NULL,
    [LastUpdatedBy] INT           NULL,
    CONSTRAINT [PK_CalendarTimeEntries] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Calendar_CalendarId] FOREIGN KEY ([CalendarId]) REFERENCES [CRM].[Calendar] ([CalendarId])
);

