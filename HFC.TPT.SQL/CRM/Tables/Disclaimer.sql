﻿CREATE TABLE [CRM].[Disclaimer] (
    [Id]              INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [FranchiseId]     INT           NOT NULL,
    [ShortDisclaimer] VARCHAR (MAX) NULL,
    [LongDisclaimer]  VARCHAR (MAX) NULL,
    [CreatedOn]       DATETIME2 (7) NOT NULL,
    [CreatedBy]       INT           NOT NULL,
    [LastUpdatedOn]   DATETIME2 (7) NULL,
    [LastUpdatedBy]   INT           NULL,
    CONSTRAINT [PK_Disclaimer] PRIMARY KEY CLUSTERED ([Id] ASC)
);

