﻿CREATE TABLE [CRM].[PricingSettings] (
    [Id]                    INT             IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [FranchiseId]           INT             NOT NULL,
    [PricingStrategyTypeId] INT             NOT NULL,
    [VendorId]              INT             NULL,
    [ProductCategoryId]     INT             NULL,
    [RetailBasis]           VARCHAR (100)   NULL,
    [MarkUp]                NUMERIC (18, 2) NULL,
    [MarkUpFactor]          VARCHAR (10)    NULL,
    [Discount]              NUMERIC (18, 2) NULL,
    [DiscountFactor]        VARCHAR (10)    NULL,
    [PriceRoundingOpt]      VARCHAR (10)    NULL,
    [CreatedOn]             DATETIME2 (7)   NOT NULL,
    [CreatedBy]             INT             NOT NULL,
    [LastUpdatedOn]         DATETIME2 (7)   NOT NULL,
    [LastUpdatedBy]         INT             NOT NULL,
    [IsActive]              BIT             CONSTRAINT [DF_PricingSettings_IsActive] DEFAULT ((1)) NOT NULL,
    [ImpersonatorPersonId]  INT             NULL,
    CONSTRAINT [PK__PricingSettings] PRIMARY KEY CLUSTERED ([Id] ASC)
);

