﻿CREATE TABLE [CRM].[CurrencyExchange] (
    [CurrencyExchangeId] INT             IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Year]               INT             NOT NULL,
    [Period]             SMALLINT        NOT NULL,
    [StartDate]          DATE            NOT NULL,
    [EndDate]            DATE            NULL,
    [CountryFrom]        VARCHAR (2)     NOT NULL,
    [CountryTo]          VARCHAR (2)     NOT NULL,
    [CalculationType]    SMALLINT        NOT NULL,
    [Rate]               DECIMAL (18, 2) NOT NULL,
    CONSTRAINT [PK_CurrencyExchange] PRIMARY KEY CLUSTERED ([CurrencyExchangeId] ASC)
);

