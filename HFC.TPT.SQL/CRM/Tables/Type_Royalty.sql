﻿CREATE TABLE [CRM].[Type_Royalty] (
    [Id]          INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Name]        VARCHAR (500) NULL,
    [Order]       INT           NOT NULL,
    [isDependant] BIT           NOT NULL,
    CONSTRAINT [PK_Type_Royalty] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 90)
);

