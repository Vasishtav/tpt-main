﻿CREATE TABLE [CRM].[HFCModelsArchive](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ModelKey] [int] NULL,
	[OldModelJson] [varchar](5000) NULL,
	[NewModelJson] [varchar](5000) NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[CreatedBy] [int] NOT NULL,
 CONSTRAINT [PK_HFCModels_Archive] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
))