﻿CREATE TABLE [CRM].[MeasurementHeader] (
    [Id]                    INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [OpportunityId]         INT            NULL,
    [MeasurementSet]        NVARCHAR (MAX) NULL,
    [IsDeleted]             BIT            CONSTRAINT [DF_MeasurementHeader_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOnUtc]          DATETIME2 (2)  CONSTRAINT [DF_MeasurementHeader_CreatedOnUtc] DEFAULT (getdate()) NOT NULL,
    [CreatedByPersonId]     INT            NULL,
    [LastUpdatedOnUtc]      DATETIME2 (2)  CONSTRAINT [DF_MeasurementHeader_LastUpdatedOnUtc] DEFAULT (getdate()) NOT NULL,
    [LastUpdatedByPersonId] INT            NULL,
    [InstallationAddressId] INT            NULL,
    CONSTRAINT [PK_Table_1] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_MeasurementHeader_Address] FOREIGN KEY ([InstallationAddressId]) REFERENCES [CRM].[Addresses] ([AddressId])
);

