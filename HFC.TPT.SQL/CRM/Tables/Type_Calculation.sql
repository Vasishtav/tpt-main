﻿CREATE TABLE [CRM].[Type_Calculation] (
    [CalculationTypeId] INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Name]              VARCHAR (50) NULL,
    CONSTRAINT [PK_Type_Calculation] PRIMARY KEY CLUSTERED ([CalculationTypeId] ASC)
);

