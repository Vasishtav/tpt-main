﻿CREATE TABLE [CRM].[Type_DeactivationReason] (
    [DeactivationReasonId] SMALLINT       IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Reason]               NVARCHAR (500) NOT NULL,
    CONSTRAINT [PK_Crm.Type_DeactivarionReason] PRIMARY KEY CLUSTERED ([DeactivationReasonId] ASC)
);

