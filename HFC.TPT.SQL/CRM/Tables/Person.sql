﻿CREATE TABLE [CRM].[Person] (
    [PersonId]          INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [FirstName]         VARCHAR (50)     NULL,
    [LastName]          VARCHAR (50)     NULL,
    [MI]                VARCHAR (4)      NULL,
    [HomePhone]         NVARCHAR (255)   NULL,
    [CellPhone]         NVARCHAR (255)   NULL,
    [CompanyName]       VARCHAR (256)    NULL,
    [WorkTitle]         VARCHAR (50)     NULL,
    [WorkPhone]         NVARCHAR (255)   NULL,
    [WorkPhoneExt]      NVARCHAR (255)   NULL,
    [FaxPhone]          NVARCHAR (255)   NULL,
    [PrimaryEmail]      VARCHAR (256)    NULL,
    [SecondaryEmail]    VARCHAR (256)    NULL,
    [Notes]             VARCHAR (1000)   NULL,
    [IsDeleted]         BIT              CONSTRAINT [DF_Customer_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOnUtc]      DATETIME         CONSTRAINT [DF_Customers_CreatedOnUtc] DEFAULT (getutcdate()) NOT NULL,
    [UnsubscribedOnUtc] DATETIME         NULL,
    [PreferredTFN]      CHAR (1)         CONSTRAINT [DF_Person_PrimaryTFN] DEFAULT (N'H') NOT NULL,
    [FranchiseID]       INT              NULL,
    [PersonGuid]        UNIQUEIDENTIFIER CONSTRAINT [DF_Person_LeadGuid] DEFAULT (newid()) NOT NULL,
    [IsReceiveEmails]   BIT              NULL,
    [TimezoneCode] INT NULL, 
    [EnableEmailSignature] BIT NULL, 
    [AddEmailSignAllEmails] BIT NULL, 
    CONSTRAINT [PK_Person] PRIMARY KEY CLUSTERED ([PersonId] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [FullName]
    ON [CRM].[Person]([PersonId] ASC)
    INCLUDE([FirstName], [LastName]) WITH (FILLFACTOR = 90);

