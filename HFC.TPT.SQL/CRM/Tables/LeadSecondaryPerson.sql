﻿CREATE TABLE [CRM].[LeadSecondaryPerson] (
    [LeadId]   INT NOT NULL,
    [PersonId] INT NOT NULL,
    CONSTRAINT [PK_LeadSecondaryPerson] PRIMARY KEY CLUSTERED ([LeadId] ASC, [PersonId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_LeadSecondaryPerson_Leads] FOREIGN KEY ([LeadId]) REFERENCES [CRM].[Leads] ([LeadId]),
    CONSTRAINT [FK_LeadSecondaryPerson_Person] FOREIGN KEY ([PersonId]) REFERENCES [CRM].[Person] ([PersonId]) ON DELETE CASCADE
);

