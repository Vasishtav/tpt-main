﻿CREATE TABLE [CRM].[CaseAttachments]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [CaseId] INT NULL, 
    [FileIconType] NVARCHAR(200) NULL, 
    [FileDetails] NVARCHAR(500) NULL, 
    [FileName] NVARCHAR(1000) NULL, 
    [StreamId] VARCHAR(100) NULL, 
    [CreatedOn] DATETIME2 NULL, 
    [CreatedBy] INT NULL, 
    [LastUpdatedOn] DATETIME2 NULL, 
    [LastUpdatedBy] INT NULL,
	CONSTRAINT [FK_CaseAttachments_Case] FOREIGN KEY ([CaseId]) REFERENCES [CRM].[Case] ([CaseId])
)
