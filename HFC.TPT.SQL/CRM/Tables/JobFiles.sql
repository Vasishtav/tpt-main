﻿CREATE TABLE [CRM].[JobFiles] (
    [JobId]          INT NOT NULL,
    [PhysicalFileId] INT NOT NULL,
    CONSTRAINT [PK_LeadFiles] PRIMARY KEY CLUSTERED ([JobId] ASC, [PhysicalFileId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_JobFiles_Jobs] FOREIGN KEY ([JobId]) REFERENCES [CRM].[Jobs] ([JobId]),
    CONSTRAINT [FK_LeadFiles_PhysicalFiles] FOREIGN KEY ([PhysicalFileId]) REFERENCES [CRM].[PhysicalFiles] ([PhysicalFileId]) ON DELETE CASCADE
);

