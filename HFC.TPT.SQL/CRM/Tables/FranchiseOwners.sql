﻿CREATE TABLE [CRM].[FranchiseOwners] (
    [OwnerId]          INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [UserId]           UNIQUEIDENTIFIER NULL,
    [FranchiseId]      INT              NOT NULL,
    [OwnershipPercent] DECIMAL (5, 2)   CONSTRAINT [DF_FranchiseOwners_OwnershipPercent] DEFAULT ((1)) NOT NULL,
    [CreatedOnUtc]     DATETIME         CONSTRAINT [DF_FranchiseOwners_CreatedOnUtc] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_FranchiseOwners] PRIMARY KEY CLUSTERED ([OwnerId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_FranchiseOwners_Franchise] FOREIGN KEY ([FranchiseId]) REFERENCES [CRM].[Franchise] ([FranchiseId]),
    CONSTRAINT [FK_FranchiseOwners_Users] FOREIGN KEY ([UserId]) REFERENCES [Auth].[Users] ([UserId])
);

