﻿CREATE TABLE [CRM].[VendorInvoiceDetail] (
    [VendorInvoiceDetailId] INT             IDENTITY (1, 1) NOT NULL,
    [VendorInvoiceId]       INT             NOT NULL,
    [POLineNumber]          INT             NULL,
    [Quantity]              INT             NOT NULL,
    [Cost]                  DECIMAL (18, 4) NULL,
    [Name]                  VARCHAR (500)   NULL,
    [ProductNumber]         VARCHAR (25)    NULL,
    [Comment]               VARCHAR (500)   NULL,
    [CreatedOn]             DATETIME        NOT NULL,
    [CreatedBy]             INT             NOT NULL,
    [UpdatedOn]             DATETIME        NULL,
    [UpdatedBy]             INT             NULL,
    [QuoteLineId]           INT             NULL,
    [ItemDiscount]			DECIMAL(18, 4) NULL, 
    [ItemAmount]			DECIMAL(18, 4) NULL, 
    [ShipDate]				DATETIME NULL, 
    [FOB]					VARCHAR(500) NULL, 
	[AdjCost]               DECIMAL (18, 4) NULL,
	[AdjDiscount]			DECIMAL(18, 4) NULL, 
    [AdjAmount]				DECIMAL(18, 4) NULL, 
    [AdjComments] VARCHAR(MAX) NULL, 
    CONSTRAINT [PK_VendorInvoiceDetail] PRIMARY KEY CLUSTERED ([VendorInvoiceDetailId] ASC)
);

GO

CREATE NONCLUSTERED INDEX [IX_VendorInvoiceId] ON [CRM].[VendorInvoiceDetail]
(
	[VendorInvoiceId] ASC
)WITH (FILLFACTOR = 90);
GO
