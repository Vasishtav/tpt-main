﻿CREATE TABLE [CRM].[Type_NamedColor] (
    [NamedColorId] INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [NamedColor]   VARCHAR (300) NULL,
    [FranchiseId]  INT           NULL,
    [CreatedOn]    DATETIME      NULL,
    CONSTRAINT [PK_Type_NamedColor] PRIMARY KEY CLUSTERED ([NamedColorId] ASC) WITH (FILLFACTOR = 90)
);

