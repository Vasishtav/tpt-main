﻿CREATE TABLE [CRM].[OpportunityNJobNumbers] (
    [FranchiseId]       INT NOT NULL,
    [JobNumber]         INT CONSTRAINT [DF_JobNumbers_JobNumber1] DEFAULT ((0)) NOT NULL,
    [OpportunityNumber] INT CONSTRAINT [DF_JobNumbers_OpportunityNum] DEFAULT ((1)) NOT NULL,
    [OrderNumber]       INT NULL,
    [InvoiceNumber]     INT NULL,
    CONSTRAINT [PK_JobNumbers1] PRIMARY KEY CLUSTERED ([FranchiseId] ASC) WITH (FILLFACTOR = 90)
);

