﻿CREATE TABLE [CRM].[Type_SalesStep] (
    [SalesStepId]  INT      IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [FranchiseId]  INT      NULL,
    [BrandId]      TINYINT  NOT NULL,
    [ForLead]      BIT      CONSTRAINT [DF_Type_SalesStep_ForLead] DEFAULT ((0)) NOT NULL,
    [DisplayOrder] INT      NULL,
    [IsDeleted]    BIT      CONSTRAINT [DF_Type_SalesStep_IsDeleted] DEFAULT ((0)) NOT NULL,
    [IsActive]     BIT      CONSTRAINT [DF_Type_SalesStep_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn]    DATETIME NULL,
    [CreatedBy]    INT      NULL,
    [UpdatedOn]    DATETIME NULL,
    [UpdatedBy]    INT      NULL,
    CONSTRAINT [PK_Type_SalesStep] PRIMARY KEY CLUSTERED ([SalesStepId] ASC),
    CONSTRAINT [FK_Type_SalesStep_Brand] FOREIGN KEY ([BrandId]) REFERENCES [CRM].[Type_Brand] ([BrandID]),
    CONSTRAINT [FK_Type_SalesStep_Franchise] FOREIGN KEY ([FranchiseId]) REFERENCES [CRM].[Franchise] ([FranchiseId])
);

