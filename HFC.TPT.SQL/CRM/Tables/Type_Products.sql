﻿CREATE TABLE [CRM].[Type_Products] (
    [ProductTypeId] INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [TypeName]      VARCHAR (50) NULL,
    [FranchiseId]   INT          NULL,
    [ParentId]      INT          NULL,
    [isCustom]      BIT          NULL,
    CONSTRAINT [PK_Type_Products] PRIMARY KEY CLUSTERED ([ProductTypeId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Type_Products_Franchise] FOREIGN KEY ([FranchiseId]) REFERENCES [CRM].[Franchise] ([FranchiseId])
);

