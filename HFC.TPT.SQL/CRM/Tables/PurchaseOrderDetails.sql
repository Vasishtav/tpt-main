﻿CREATE TABLE [CRM].[PurchaseOrderDetails] (
    [PurchaseOrdersDetailId] INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PurchaseOrderId]        INT            NOT NULL,
    [QuoteLineId]            INT            NOT NULL,
    [PartNumber]             VARCHAR (25)   NULL,
    [ShipDate]               DATETIME       NULL,
    [Status]                 VARCHAR (50)   NULL,
    [PromiseByDate]          DATETIME       NULL,
    [VendorNotes]            VARCHAR (500)  NULL,
    [QuantityPurchased]      INT            NULL,
    [PickedBy]               VARCHAR (25)   NULL,
    [PONumber]               INT            NULL,
    [POResponse]             VARCHAR (MAX)  NULL,
    [CreatedOn]              DATETIME       NOT NULL,
    [CreatedBy]              INT            NOT NULL,
    [UpdatedOn]              DATETIME       NULL,
    [UpdatedBy]              INT            NULL,
    [NotesVendor]            VARCHAR (500)  NULL,
    [PICPO]                  INT            NULL,
    [StatusId]               INT            NULL,
    [Errors]                 VARCHAR (MAX)  NULL,
    [vendorReference]        VARCHAR (25)   NULL,
    [Information]            VARCHAR (1000) NULL,
    [Warning]                VARCHAR (1000) NULL,
    [CancelReason]           VARCHAR (100)  NULL,
    [ImpersonatorPersonId]   INT            NULL,
    [estShipDate]            DATETIME       NULL,
    [QTY]                    INT            NULL,
    [AckCost]                NUMERIC (18, 2)NULL,
    [Discount]               NUMERIC (18, 2)NULL,
    [Item]                   INT            NULL,
    [woItem]                 INT            NULL,
    [UOM]                    VARCHAR (100)  NULL,
    CONSTRAINT [PK_PurchaseOrderDetails] PRIMARY KEY CLUSTERED ([PurchaseOrdersDetailId] ASC),
    CONSTRAINT [FK_PurchaseOrderDetails_MasterPurchaseOrder] FOREIGN KEY ([PurchaseOrderId]) REFERENCES [CRM].[MasterPurchaseOrder] ([PurchaseOrderId])
);
GO

CREATE NONCLUSTERED INDEX [_dta_index_PurchaseOrderDetails_33_678827737__K3_K2_K18_K1_5_6_7_13_19] ON [CRM].[PurchaseOrderDetails]
(
	[QuoteLineId] ASC,
	[PurchaseOrderId] ASC,
	[PICPO] ASC,
	[PurchaseOrdersDetailId] ASC
)
INCLUDE([ShipDate],[Status],[PromiseByDate],[CreatedOn],[StatusId]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]


GO


CREATE TRIGGER [CRM].[PurchaseOrderDetailsStatusTrackerTrigger] ON [CRM].[PurchaseOrderDetails]

FOR INSERT, UPDATE
AS
     BEGIN
          IF UPDATE(StatusId)  AND NOT EXISTS(SELECT 1 FROM DELETED d INNER JOIN INSERTED i on i.StatusId = d.StatusId)
             BEGIN			 
                INSERT INTO [CRM].[PurchaseOrderDetailsStatusTracker]
            ([PurchaseOrdersDetailId]
           ,[PurchaseOrderId]
           ,[POStatusId]
           ,[CreatedOn]
           ,[CreatedBy])
     
                       SELECT PurchaseOrdersDetailId, [PurchaseOrderId],
                              StatusId,
                               GETUTCDATE(),
                               CreatedBy
                        FROM inserted;
         END;
             
     END;

	 GO
	 CREATE NONCLUSTERED INDEX [_dta_index_PurchaseOrderDetails_33_678827737__K3_K1_18_20] ON [CRM].[PurchaseOrderDetails]
(
	[QuoteLineId] ASC,
	[PurchaseOrdersDetailId] ASC
)
INCLUDE([PICPO],[Errors]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

go
CREATE NONCLUSTERED INDEX [_dta_index_PurchaseOrderDetails_6_678827737__K2_K18_K3] ON [CRM].[PurchaseOrderDetails]
(
	[PurchaseOrderId] ASC,
	[PICPO] ASC,
	[QuoteLineId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go


CREATE NONCLUSTERED INDEX [_dta_index_PurchaseOrderDetails_6_678827737__K2_K3_K18_K7_K5_K19_K6_K1_13] ON [CRM].[PurchaseOrderDetails]
(
	[PurchaseOrderId] ASC,
	[QuoteLineId] ASC,
	[PICPO] ASC,
	[PromiseByDate] ASC,
	[ShipDate] ASC,
	[StatusId] ASC,
	[Status] ASC,
	[PurchaseOrdersDetailId] ASC
)
INCLUDE([CreatedOn]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

go
