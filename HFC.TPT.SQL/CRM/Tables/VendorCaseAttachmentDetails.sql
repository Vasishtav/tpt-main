﻿CREATE TABLE [CRM].[VendorCaseAttachmentDetails] (
    [VendorCaseAttachmentId] INT            IDENTITY (1, 1) NOT NULL,
    [VendorCaseId]           INT            NOT NULL,
    [VendorCaseDetailId]     INT            NULL,
    [fileIconType]           NVARCHAR (100) NULL,
    [fileSize]               NVARCHAR (100) NULL,
    [fileName]               NVARCHAR (100) NULL,
    [color]                  NVARCHAR (100) NULL,
    [streamId]               VARCHAR (100)  NULL,
    [streamId1]              VARCHAR (100)  NULL,
    CONSTRAINT [PK_VendorCaseAttachmentDetails] PRIMARY KEY CLUSTERED ([VendorCaseAttachmentId] ASC),
    CONSTRAINT [FK_VendorCaseAttachmentDetails_VendorCaseDetailId] FOREIGN KEY ([VendorCaseDetailId]) REFERENCES [CRM].[VendorCaseDetail] ([VendorCaseDetailId]),
    CONSTRAINT [FK_VendorCaseAttachmentDetails_VendorCaseId] FOREIGN KEY ([VendorCaseId]) REFERENCES [CRM].[VendorCase] ([VendorCaseId])
);

