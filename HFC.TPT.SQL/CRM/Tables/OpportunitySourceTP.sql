﻿CREATE TABLE [CRM].[OpportunitySourceTP] (
    [SourcesTPId] INT IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [OwnerId]     INT NOT NULL,
    [ChannelId]   INT NOT NULL,
    [SourceId]    INT NOT NULL,
    CONSTRAINT [PK_SourcesTP1] PRIMARY KEY CLUSTERED ([SourcesTPId] ASC),
    CONSTRAINT [AK_UniqueSourcesTP1] UNIQUE NONCLUSTERED ([OwnerId] ASC, [ChannelId] ASC, [SourceId] ASC)
);

