﻿CREATE TABLE [CRM].[MasterPurchaseOrder] (
    [PurchaseOrderId]      INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [MasterPONumber]       VARCHAR (50)   NOT NULL,
    --[AccountId]            INT            NOT NULL,
    --[OpportunityId]        INT            NOT NULL,
    [CreateDate]           DATETIME       NOT NULL,
    [SubmittedDate]        DATETIME       NULL,
    --[OrderId]              INT            NOT NULL,
    --[QuoteId]              INT            NOT NULL,
    [Status]               VARCHAR (50)   NULL,
    [CreatedBy]            INT            NOT NULL,
    [CreatedOn]            DATETIME       NOT NULL,
    [UpdatedBy]            INT            NULL,
    [UpdateOn]             DATETIME       NULL,
    [POStatusId]           INT            NULL,
    [PICResponse]          VARCHAR (MAX)  NULL,
    [SubmittedTOPIC]       BIT            NULL,
    [PICPO]                INT            NULL,
    [ConfirmAddress]       BIT            DEFAULT ((0)) NULL,
    [DropShip]             BIT            DEFAULT ((0)) NULL,
    [ShipToLocation]       INT            NULL,
    [TerritoryId]          INT            NULL,
    [ShipAddressId]        INT            NULL,
    [CounterId]            INT            NULL,
    [SubmittedBy]          INT            NULL,
    [Message]              VARCHAR (1000) NULL,
    [CancelReason]         NVARCHAR (100) NULL,
    [ImpersonatorPersonId] INT            NULL,
    [IsXMpo] BIT						DEFAULT((0)) NULL,
	[VendorId] int						NULL,
    CONSTRAINT [PK_MasterPurchaseOrder] PRIMARY KEY CLUSTERED ([PurchaseOrderId] ASC),
    -- CONSTRAINT [FK_MasterPurchaseOrder_VendorId] FOREIGN KEY ([VendorId]) REFERENCES [ACCT].[FranchiseVendors] ([VendorId]),
    --CONSTRAINT [FK_MasterPurchaseOrder_Opportunities] FOREIGN KEY ([OpportunityId]) REFERENCES [CRM].[Opportunities] ([OpportunityId]),
    --CONSTRAINT [FK_MasterPurchaseOrder_Orders] FOREIGN KEY ([OrderId]) REFERENCES [CRM].[Orders] ([OrderID]),
    --CONSTRAINT [FK_MasterPurchaseOrder_Quote] FOREIGN KEY ([QuoteId]) REFERENCES [CRM].[Quote] ([QuoteKey])
);


GO

CREATE TRIGGER [CRM].MPOCounterIDChangeTrigger ON [CRM].[MasterPurchaseOrder]
FOR UPDATE
AS
     BEGIN
         IF UPDATE(CounterId)
            AND NOT EXISTS
         (
             SELECT 1
             FROM DELETED d
                  INNER JOIN INSERTED i ON i.CounterId = d.CounterId
         )
             BEGIN
                 INSERT INTO [CRM].[MPOCounterIdHistoryTracker]
                 ([PurchaseOrderId],
                  [OrderId],
                  [CounterId],
                  [SubmittedBy],
                  [CreatedOn],
                  [CreatedBy]
                 )
                        SELECT i.PurchaseOrderId,
                               mpoe.OrderId,
                               i.CounterId,
                               i.SubmittedBy,
                               GETUTCDATE(),
                               i.SubmittedBy
                        FROM inserted i
                             INNER JOIN crm.MasterPurchaseOrderExtn mpoe ON mpoe.PurchaseOrderId = i.PurchaseOrderId;
         END;
     END;
GO
CREATE TRIGGER [CRM].MPOStatusTrackerTrigger ON [CRM].[MasterPurchaseOrder]
FOR UPDATE
AS
     BEGIN
           IF UPDATE(POStatusId)  AND NOT EXISTS(SELECT 1 FROM DELETED d INNER JOIN INSERTED i on i.POStatusId = d.POStatusId)
             BEGIN
			 INSERT INTO CRM.MPOStatusTracker
			 (
			     --MPOStatusTrackerId - column value is auto-generated
			     PurchaseOrderId,
			     MPOStatusId,
			     CreatedOn,
			     CreatedBy
			 )
			 select

			     i.PurchaseOrderId, -- PurchaseOrderId - int
			     i.POStatusId, -- MPOStatusId - int
			      GETUTCDATE(), -- CreatedOn - datetime
			     i.CreatedBy -- CreatedBy - int
			 FROM inserted i
         END;

     END;
GO
 CREATE NONCLUSTERED INDEX [_dta_index_MasterPurchaseOrder_6_921588578__K5_K1_2_6_28] ON [CRM].[MasterPurchaseOrder]
(
	[CreateDate] ASC,
	[PurchaseOrderId] ASC
)
INCLUDE([MasterPONumber],[SubmittedDate],[IsXMpo]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go
CREATE NONCLUSTERED INDEX [_dta_index_MasterPurchaseOrderExtn_6_53445521__K2_K3_K6_K1] ON [CRM].[MasterPurchaseOrderExtn]
(
	[PurchaseOrderId] ASC,
	[AccountId] ASC,
	[OrderId] ASC,
	[MpoExtnId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go


