﻿CREATE TABLE [CRM].[NewsUpdatesBrands]
(
	[NewsUpdatesId] INT NOT NULL, 
    [BrandId] SMALLINT NOT NULL, 
    CONSTRAINT [FK_NewsUpdatesBrands_NewsUpdates] FOREIGN KEY ([NewsUpdatesId]) REFERENCES CRM.NewsUpdates([NewsUpdatesId]), 
    CONSTRAINT [FK_NewsUpdatesBrands_Type_Brands] FOREIGN KEY ([BrandId]) REFERENCES CRM.Type_Brands(id), 
    CONSTRAINT Unique_NewsIdBrandId UNIQUE(NewsUpdatesId, BrandId) 
)
