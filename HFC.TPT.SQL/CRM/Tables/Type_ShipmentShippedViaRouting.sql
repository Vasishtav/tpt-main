﻿CREATE TABLE [CRM].[Type_ShipmentShippedViaRouting] (
    [Id]            INT         IDENTITY (1, 1) NOT NULL,
    [JsonShippedVia]            varchar (200) NOT NULL,
    [TouchpointShippedVia]      varchar (200) NOT NULL,
    [TouchpointRouting]         varchar (200) NULL,
    [IsActive]      BIT           NOT NULL,
    [IsDeleted]     BIT           NULL,
    [CreatedOn]     DATETIME2 (7) NOT NULL,
    [CreatedBy]     INT           NOT NULL,
    [LastUpdatedOn] DATETIME2 (7) NULL,
    [LastUpdatedBy] INT           NULL,
    CONSTRAINT [PK_Type_ShipmentShippedViaRouting] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [UQ_Type_ShipmentShippedViaRouting_JsonShipppedVia] UNIQUE([JsonShippedVia]),
    CONSTRAINT [FK_Type_ShipmentShippedViaRouting_Createdby] FOREIGN KEY ([CreatedBy]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_Type_ShipmentShippedViaRouting_Modifiedby] FOREIGN KEY ([LastUpdatedBy]) REFERENCES [CRM].[Person] ([PersonId])
);

