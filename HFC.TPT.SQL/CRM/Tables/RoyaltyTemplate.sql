﻿CREATE TABLE [CRM].[RoyaltyTemplate] (
    [RoyaltyTemplateId] INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [RoyaltyProfileId]  INT            NOT NULL,
    [Type_RoyaltyId]    INT            NULL,
    [NumberOfMonth]     INT            NOT NULL,
    [Amount]            DECIMAL (9, 4) NOT NULL,
    [BrandId]           SMALLINT       NULL,
    CONSTRAINT [PK_RoyaltyTemplate] PRIMARY KEY CLUSTERED ([RoyaltyTemplateId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_RoyaltyTemplate_RoyaltyTemplate] FOREIGN KEY ([BrandId]) REFERENCES [CRM].[Type_Brands] ([id]),
    CONSTRAINT [FK_Type_RoyaltyProfile_Type_RoyaltyProfile] FOREIGN KEY ([RoyaltyProfileId]) REFERENCES [CRM].[Type_RoyaltyProfile] ([Id]),
    CONSTRAINT [FK_Type_RoyaltyProfile_Type_Type_Royalty] FOREIGN KEY ([Type_RoyaltyId]) REFERENCES [CRM].[Type_Royalty] ([Id])
);

