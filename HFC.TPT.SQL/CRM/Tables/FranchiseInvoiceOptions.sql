﻿CREATE TABLE [CRM].[FranchiseInvoiceOptions] (
    [FranchiseInvoiceOptionId] INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ShortLegal]               VARCHAR (255)  NULL,
    [LongLegal]                NVARCHAR (MAX) NULL,
    [FranchiseId]              INT            NULL,
    [isDeleted]                BIT            NULL,
    [IsVisible]                BIT            CONSTRAINT [DF__Franchise__IsVis__02333863] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_FranchiseInvoiceOption] PRIMARY KEY CLUSTERED ([FranchiseInvoiceOptionId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_FranchiseInvoiceOptions_Franchise] FOREIGN KEY ([FranchiseId]) REFERENCES [CRM].[Franchise] ([FranchiseId])
);

