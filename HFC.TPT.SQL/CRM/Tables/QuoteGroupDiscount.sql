﻿CREATE TABLE [CRM].[QuoteGroupDiscount] (
    [QuoteGroupDiscountId]    INT             IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [QuoteKey]				  INT             NOT NULL,
	[ProductTypeId]			  INT			  NOT NULL,
	[ProductGroupId]		  INT			  NOT NULL,
	[ProductGroupDesc]		  NVARCHAR(255)	  NOT NULL,
    [Discount]                NUMERIC (18, 2) NULL,
    [CreatedOn]               DATETIME2 (7)   NOT NULL,
    [CreatedBy]               INT             NOT NULL,
    [LastUpdatedOn]           DATETIME2 (7)   NULL,
    [LastUpdatedBy]           INT             NULL,
    CONSTRAINT [PK_QuoteGroupDiscount] PRIMARY KEY ([QuoteGroupDiscountId]),
	CONSTRAINT [FK_QuoteGroupDiscount_QuoteKey] FOREIGN KEY ([QuoteKey]) REFERENCES [CRM].[Quote] ([QuoteKey]),
	CONSTRAINT [FK_QuoteGroupDiscount_UniqueKey] UNIQUE ([QuoteKey],[ProductTypeId],[ProductGroupId]),
	CONSTRAINT [FK_QuoteGroupDiscount_Person_CreatedBy] FOREIGN KEY ([CreatedBy]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_QuoteGroupDiscount_Person_LastUpdatedBy] FOREIGN KEY ([LastUpdatedBy]) REFERENCES [CRM].[Person] ([PersonId])
);

