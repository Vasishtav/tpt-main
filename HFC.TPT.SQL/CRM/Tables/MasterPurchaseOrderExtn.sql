﻿CREATE TABLE [CRM].[MasterPurchaseOrderExtn](
	[MpoExtnId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[PurchaseOrderId] [int],
	[AccountId] [int] NOT NULL,
	[OpportunityId] [int] NOT NULL,
	[QuoteId] [int] NOT NULL,
	[OrderId] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdateOn] [datetime] NULL,
	CONSTRAINT [PK_MasterPurchaseOrderExtn] PRIMARY KEY CLUSTERED ([MpoExtnId] ASC),
	CONSTRAINT [FK_MasterPurchaseOrderExtn_MpoExtnId] FOREIGN KEY ([PurchaseOrderId]) REFERENCES [CRM].[MasterPurchaseOrder] ([PurchaseOrderId]),
    CONSTRAINT [FK_MasterPurchaseOrderExtn_Accounts] FOREIGN KEY ([AccountId]) REFERENCES [CRM].[Accounts] ([AccountId]),
    CONSTRAINT [FK_MasterPurchaseOrderExtn_Opportunities] FOREIGN KEY ([OpportunityId]) REFERENCES [CRM].[Opportunities] ([OpportunityId]),
    CONSTRAINT [FK_MasterPurchaseOrderExtn_Quote] FOREIGN KEY ([QuoteId]) REFERENCES [CRM].[Quote] ([QuoteKey]),
	CONSTRAINT [FK_MasterPurchaseOrderExtn_Orders] FOREIGN KEY ([OrderId]) REFERENCES [CRM].[Orders] ([OrderID])
)
GO

CREATE NONCLUSTERED INDEX [IX_PurchaseOrderId] ON [CRM].[MasterPurchaseOrderExtn]
(
	[PurchaseOrderId] ASC
)WITH (FILLFACTOR = 90) ON [PRIMARY]
GO