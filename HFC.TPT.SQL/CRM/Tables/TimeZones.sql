﻿CREATE TABLE [CRM].[TimeZones] (
    [TimeZoneId]  INT             IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [TimeZone]    NCHAR (10)      NULL,
    [OffSetTime]  DECIMAL (18, 2) NULL,
    [Description] NCHAR (100)     NULL,
    [CountryCode] NCHAR (10)      NULL,
    CONSTRAINT [PK_TimeZones] PRIMARY KEY CLUSTERED ([TimeZoneId] ASC)
);

