﻿CREATE TABLE [CRM].[UTMCampaign] (
    [Id]            INT             IDENTITY (1, 1) NOT NULL,
    [UTMCode]       VARCHAR (100)   NOT NULL,
    [SourcesHFCId]  INT             NOT NULL,
    [StartDate]     DATETIME2 (7)   NULL,
    [EndDate]       DATETIME2 (7)   NULL,
    [Amount]        NUMERIC (18, 2) NULL,
    [Memo]          VARCHAR (500)   NULL,
    [IsActive]      BIT             NULL,
    [IsDeleted]     BIT             NULL,
    [CreatedOn]     DATETIME2 (7)   NOT NULL,
    [CreatedBy]     INT             NOT NULL,
    [LastUpdatedOn] DATETIME2 (7)   NULL,
    [LastUpdatedBy] INT             NULL,
    CONSTRAINT [PK_UTMCampaign] PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([SourcesHFCId]) REFERENCES [CRM].[SourcesHFC] ([SourcesHFCId])
);

