﻿CREATE TABLE [CRM].[MPOCounterIdHistoryTracker] (
    [MPOCounterHistoryTrackerId] INT      IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PurchaseOrderId]            INT      NOT NULL,
    [OrderId]                    INT      NOT NULL,
    [CounterId]                  INT      NULL,
    [SubmittedBy]                INT      NULL,
    [CreatedOn]                  DATETIME NOT NULL,
    [CreatedBy]                  INT      NULL
);

