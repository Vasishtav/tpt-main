﻿CREATE TABLE [CRM].[Type_Channel] (
    [ChannelId]   INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Name]        VARCHAR (50)  NULL,
    [Description] VARCHAR (100) NULL,
    [BrandId]     INT           NULL,
    [CreatedOn]   DATETIME      CONSTRAINT [DF_Type_Channel_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]   INT           NULL,
    [UpdatedOn]   DATETIME      NULL,
    [UpdatedBy]   INT           NULL,
    [IsActive]    BIT           DEFAULT ((1)) NOT NULL,
    [IsDeleted]   BIT           DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Type_Channel] PRIMARY KEY CLUSTERED ([ChannelId] ASC)
);

