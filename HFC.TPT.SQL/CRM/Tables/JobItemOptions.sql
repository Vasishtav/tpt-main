﻿CREATE TABLE [CRM].[JobItemOptions] (
    [OptionId]        INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [JobItemId]       INT          NOT NULL,
    [Name]            VARCHAR (50) NULL,
    [Value]           VARCHAR (50) NULL,
    [IsVisible]       BIT          CONSTRAINT [DF_JobItemOptions_IsVisible] DEFAULT ((1)) NOT NULL,
    [ProcessIfHidden] BIT          NULL,
    CONSTRAINT [PK_JobItemOptions] PRIMARY KEY CLUSTERED ([OptionId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_JobItemOptions_JobItems] FOREIGN KEY ([JobItemId]) REFERENCES [CRM].[JobItems] ([JobItemId]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_JobItemId]
    ON [CRM].[JobItemOptions]([JobItemId] ASC)
    INCLUDE([OptionId], [Name], [Value], [IsVisible]) WITH (FILLFACTOR = 90);

