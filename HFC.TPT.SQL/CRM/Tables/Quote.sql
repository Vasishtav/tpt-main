﻿CREATE TABLE [CRM].[Quote] (
    [QuoteKey]                 INT             IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [QuoteID]                  INT             NOT NULL,
    [OpportunityId]            INT             NOT NULL,
    [QuoteName]                VARCHAR (100)   NOT NULL,
    [PrimaryQuote]             BIT             NULL,
    [QuoteStatusId]            INT             NOT NULL,
    [Discount]                 NUMERIC (18, 2) NULL,
    [DiscountType]             VARCHAR (10)    NULL,
    [TotalDiscounts]           NUMERIC (18, 2) NULL,
    [NetCharges]               NUMERIC (18, 2) NULL,
    [CreatedOn]                DATETIME2 (7)   NOT NULL,
    [CreatedBy]                INT             NOT NULL,
    [LastUpdatedOn]            DATETIME2 (7)   NULL,
    [LastUpdatedBy]            INT             NULL,
    [ExpiryDate]               DATETIME2 (7)   NULL,
    [AdditionalCharges]        NUMERIC (18, 2) NULL,
    [QuoteNumber]              INT             NULL,
    [TotalMargin]              NUMERIC (18, 2) NULL,
    [ProductSubTotal]          NUMERIC (18, 2) NULL,
    [QuoteSubTotal]            NUMERIC (18, 2) NULL,
    [MeasurementId]            INT             NULL,
    [CoreProductMargin]        NUMERIC (18, 2) NULL,
    [CoreProductMarginPercent] NUMERIC (18, 2) NULL,
    [MyProductMargin]          NUMERIC (18, 2) NULL,
    [MyProductMarginPercent]   NUMERIC (18, 2) NULL,
    [ServiceMargin]            NUMERIC (18, 2) NULL,
    [ServiceMarginPercent]     NUMERIC (18, 2) NULL,
    [TotalMarginPercent]       NUMERIC (18, 2) NULL,
    [DiscountId]               INT             NULL,
    [NVR]                      NUMERIC (18, 2) NULL,
    [IsTaxExempt]              BIT             NULL,
    [TaxExemptID]              NVARCHAR (50)   NULL,
    [BillofMaterial]           VARCHAR (MAX)   NULL,
    [SideMark]                 VARCHAR (50)    NULL,
    [QuoteLevelNotes]          VARCHAR (MAX)   NULL,
    [OrderInvoiceLevelNotes]   VARCHAR (MAX)   NULL,
    [InstallerInvoiceNotes]    VARCHAR (MAX)   NULL,
    [IsPSTExempt] BIT NOT NULL DEFAULT 0, 
    [IsGSTExempt] BIT NOT NULL DEFAULT 0, 
    [IsHSTExempt] BIT NOT NULL DEFAULT 0, 
    [IsVATExempt] BIT NOT NULL DEFAULT 0, 
    [IsNewConstruction] BIT NOT NULL DEFAULT 0, 
    [SaleDate] DATETIME2 NULL, 
    [SaleDateCreatedBy] INT NULL, 
    [SaleDateCreatedOn] DATETIME2 NULL, 
    [PreviousSaleDate] DATETIME2 NULL, 
    CONSTRAINT [PK_CRM.Quote] PRIMARY KEY CLUSTERED ([QuoteKey] ASC),
    CONSTRAINT [FK_CRM.Quote_Createdby] FOREIGN KEY ([CreatedBy]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_CRM.Quote_Modifiedby] FOREIGN KEY ([LastUpdatedBy]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_CRM.Quote_Opportunities] FOREIGN KEY ([OpportunityId]) REFERENCES [CRM].[Opportunities] ([OpportunityId]),
    CONSTRAINT [FK_Quote_MeasurementHeader_MeasurementId] FOREIGN KEY ([MeasurementId]) REFERENCES [CRM].[MeasurementHeader] ([Id])
);


GO

CREATE TRIGGER [CRM].[QuoteStatusTrackerTrigger] ON [CRM].[Quote]
FOR INSERT, UPDATE
AS
     BEGIN
           IF UPDATE(QuoteStatusId)  AND NOT EXISTS(SELECT 1 FROM DELETED d INNER JOIN INSERTED i on i.QuoteStatusId = d.QuoteStatusId)
             BEGIN
                 INSERT INTO CRM.QuoteStatusTracker
                 ([QuoteKey],
                  [QuoteStatusId],
                  [CreatedOn],
                  [CreatedBy]
                 )
                       SELECT [QuoteKey],
                              [QuoteStatusId],
                               GETUTCDATE(),
                               CreatedBy
                        FROM inserted;
         END;
            
     END;

