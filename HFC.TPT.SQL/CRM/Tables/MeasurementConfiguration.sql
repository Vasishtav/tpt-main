﻿CREATE TABLE [CRM].[MeasurementConfiguration] (
    [Id]          INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [System]      NVARCHAR (50) NULL,
    [StorageType] NVARCHAR (50) NULL,
    [Width]       BIT           NULL,
    [Height]      BIT           NULL,
    [Depth]       BIT           NULL,
    [Laptop]      BIT           NULL,
    [Desktop]     BIT           NULL,
    [Quantity]    BIT           NULL,
    [UseTP]       BIT           NULL,
    [Store]       BIT           NULL,
    CONSTRAINT [PK_MeasurementConfiguration] PRIMARY KEY CLUSTERED ([Id] ASC)
);

