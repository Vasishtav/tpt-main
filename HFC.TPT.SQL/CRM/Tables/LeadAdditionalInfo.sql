﻿CREATE TABLE [CRM].[LeadAdditionalInfo] (
    [Id]           INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [LeadId]       INT           NOT NULL,
    [Name]         VARCHAR (50)  NULL,
    [Value]        VARCHAR (256) NULL,
    [CreatedOnUtc] DATETIME2 (2) CONSTRAINT [DF_LeadAdditionalInfo_CreatedOnUtc] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]    INT           NULL,
    [UpdatedOn]    DATETIME      NULL,
    [UpdatedBy]    INT           NULL,
    CONSTRAINT [PK_LeadAdditionalInfo] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_LeadAdditionalInfo_Leads] FOREIGN KEY ([LeadId]) REFERENCES [CRM].[Leads] ([LeadId])
);

