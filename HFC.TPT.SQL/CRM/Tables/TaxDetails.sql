﻿CREATE TABLE [CRM].[TaxDetails] (
    [TaxDetailsId]  INT             IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [TaxId]         INT             NOT NULL,
    [Jurisdiction]  VARCHAR (100)   NULL,
    [JurisType]     VARCHAR (100)   NULL,
    [Rate]          DECIMAL (18, 3) NULL,
    [TaxableAmount] NUMERIC (18, 2) NULL,
    [Amount]        NUMERIC (18, 2) NULL,
    [CreatedOn]     DATETIME2 (7)   NOT NULL,
    [CreatedBy]     INT             NOT NULL,
    [LastUpdatedOn] DATETIME2 (7)   NULL,
    [LastUpdatedBy] INT             NULL,
    [TaxName]       VARCHAR (250)   NULL,
    CONSTRAINT [PK_TaxDetails] PRIMARY KEY CLUSTERED ([TaxDetailsId] ASC),
    CONSTRAINT [FK_TaxDetails_Tax] FOREIGN KEY ([TaxId]) REFERENCES [CRM].[Tax] ([TaxId])
);

