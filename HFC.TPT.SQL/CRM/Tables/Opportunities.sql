﻿CREATE TABLE [CRM].[Opportunities] (
    [OpportunityId]         INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [OpportunityName]       VARCHAR (50)     NOT NULL,
    [SideMark]              VARCHAR (50)     NULL,
    [OpportunityStatusId]   TINYINT          NULL,
    [SalesAgentId]          INT              NOT NULL,
    [InstallerId]           INT              NULL,
    [InstallationAddressId] INT              NULL,
    [InstallationAddress]   VARCHAR (200)    NOT NULL,
    [Description]           VARCHAR (550)    NULL,
    [LeadId]                INT              NOT NULL,
    [AccountId]             INT              NOT NULL,
    [OpportunityGuid]       UNIQUEIDENTIFIER CONSTRAINT [DF_Opportunities_OpportunityGuid] DEFAULT (newid()) NOT NULL,
    [OpportunityNumber]     INT              CONSTRAINT [DF_Opportunities_OpportunityNumber] DEFAULT ((0)) NOT NULL,
    [OpportunitiestatusId]  SMALLINT         CONSTRAINT [DF_Opportunities_OpportunitiestatusId] DEFAULT ((1)) NOT NULL,
    [FranchiseId]           INT              CONSTRAINT [DF_Opportunities_TerritoryId] DEFAULT ((1)) NULL,
    [TerritoryId]           INT              NULL,
    [PersonId]              INT              NULL,
    [SecPersonId]           INT              NULL,
    [IsDeleted]             BIT              CONSTRAINT [DF_Opportunities_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOnUtc]          DATETIME2 (2)    CONSTRAINT [DF_Opportunities_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ReferringCustomerId]   VARCHAR (50)     NULL,
    [CreatedByPersonId]     INT              NULL,
    [LastUpdatedOnUtc]      DATETIME2 (2)    CONSTRAINT [DF_Opportunities_LastUpdatedOnUtc] DEFAULT (getdate()) NULL,
    [LastUpdatedByPersonId] INT              NULL,
    [Notes]                 NVARCHAR (MAX)   NULL,
    [DispositionId]         INT              NULL,
    [SalesStepId]           INT              CONSTRAINT [DF__Opportunities__SalesStep__026F9E2B] DEFAULT ((1)) NULL,
    [TempCustomerId]        INT              NULL,
    [CampaignId]            INT              NULL,
    [TypeId]                INT              CONSTRAINT [DF__Opportuni__TypeI__00724720] DEFAULT ((0)) NULL,
    [BillingAddressId]      INT              NULL,
    [TerritoryType]         VARCHAR (100)    NULL,
    [ImpersonatorPersonId]  INT              NULL,
    [IsTaxExempt] BIT NULL, 
    [TaxExemptID] NVARCHAR(50) NULL, 
    [IsPSTExempt] BIT NULL DEFAULT 0, 
    [IsGSTExempt] BIT NULL DEFAULT 0, 
    [IsHSTExempt] BIT NULL DEFAULT 0, 
    [IsVATExempt] BIT NULL DEFAULT 0, 
    [IsNewConstruction] BIT NULL DEFAULT 0, 
    [TerritoryDisplayId] INT NULL, 
    [ReceivedDate] DATETIME2(2) NULL, 
    CONSTRAINT [PK_Opportunities] PRIMARY KEY CLUSTERED ([OpportunityId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Opportunities_CreatorPerson] FOREIGN KEY ([CreatedByPersonId]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_Opportunities_Customers] FOREIGN KEY ([PersonId]) REFERENCES [CRM].[Customer] ([CustomerId]),
    CONSTRAINT [FK_Opportunities_Franchise] FOREIGN KEY ([FranchiseId]) REFERENCES [CRM].[Franchise] ([FranchiseId]),
    CONSTRAINT [FK_Opportunities_LastUpdatedPerson] FOREIGN KEY ([LastUpdatedByPersonId]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_Opportunities_SecPerson] FOREIGN KEY ([SecPersonId]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_Opportunities_Territories] FOREIGN KEY ([TerritoryId]) REFERENCES [CRM].[Territories] ([TerritoryId]),
    CONSTRAINT [FK_Opportunities_Type_Opportunitystatus] FOREIGN KEY ([OpportunityStatusId]) REFERENCES [CRM].[Type_OpportunityStatus] ([OpportunityStatusId])
);


GO
CREATE TRIGGER crm.OpportunityStatusTrackerTrigger ON [CRM].[Opportunities]
FOR INSERT, UPDATE
AS
     BEGIN
         IF(EXISTS
           (
               SELECT *
               FROM INSERTED
           )
            AND EXISTS
           (
               SELECT *
               FROM DELETED
           ))
             BEGIN
                 INSERT INTO CRM.OpportunityStatusTracker
                 ([OpportunityId],
                  [OpportunityStatusId],
                  [CreatedOn],
                  [CreatedBy]
                 )
                       SELECT [OpportunityId],
                              [OpportunityStatusId],
                               GETUTCDATE(),
                               CreatedByPersonId
                        FROM inserted;
         END;
             ELSE
             BEGIN
                 INSERT INTO CRM.OpportunityStatusTracker
                  ([OpportunityId],
                  [OpportunityStatusId],
                  [Createdon],
                  [CreatedBy]
                 )
                        SELECT [OpportunityId],
                               [OpportunityStatusId],
                               GETUTCDATE(),
                               CreatedByPersonId
                        FROM inserted;
         END;
     END;

 


