﻿CREATE TABLE [CRM].[Type_Colors] (
    [ColorId]  INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Red]      TINYINT        CONSTRAINT [DF_Type_Colors_Red] DEFAULT ((0)) NOT NULL,
    [Green]    TINYINT        CONSTRAINT [DF_Type_Colors_Green] DEFAULT ((0)) NOT NULL,
    [Blue]     TINYINT        NOT NULL,
    [Alpha]    NUMERIC (3, 2) CONSTRAINT [DF_Type_Colors_Opacity] DEFAULT ((1)) NOT NULL,
    [IsCustom] BIT            CONSTRAINT [DF_Type_Colors_IsCustom] DEFAULT ((0)) NOT NULL,
    [FGRed]    TINYINT        CONSTRAINT [DF_Type_Colors_FGRed] DEFAULT ((0)) NOT NULL,
    [FGGreen]  TINYINT        CONSTRAINT [DF_Type_Colors_FGGreen] DEFAULT ((0)) NOT NULL,
    [FGBlue]   TINYINT        CONSTRAINT [DF_Type_Colors_FGBlue] DEFAULT ((0)) NOT NULL,
    [FGAlpha]  NUMERIC (3, 2) CONSTRAINT [DF_Type_Colors_FGOpacity] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_Type_Colors] PRIMARY KEY CLUSTERED ([ColorId] ASC) WITH (FILLFACTOR = 90)
);

