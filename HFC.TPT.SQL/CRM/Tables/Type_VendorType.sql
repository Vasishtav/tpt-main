﻿CREATE TABLE [CRM].[Type_VendorType] (
    [Id]            INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [VendorType]    VARCHAR (100) NOT NULL,
    [IsActive]      BIT           NOT NULL,
    [IsDeleted]     BIT           NULL,
    [CreatedOn]     DATETIME2 (7) NOT NULL,
    [CreatedBy]     INT           NOT NULL,
    [LastUpdatedOn] DATETIME2 (7) NULL,
    [LastUpdatedBy] INT           NULL,
    CONSTRAINT [PK_Type_VendorType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

