﻿CREATE TABLE [CRM].[Dimensions] (
    [DimensionId] INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Width]       DECIMAL (9, 4) NULL,
    [WidthUnit]   VARCHAR (15)   NULL,
    [Height]      DECIMAL (9, 4) NULL,
    [HeightUnit]  VARCHAR (15)   NULL,
    [Length]      DECIMAL (9, 4) NULL,
    [LengthUnit]  VARCHAR (15)   NULL,
    [Weight]      DECIMAL (9, 4) NULL,
    [WeightUnit]  VARCHAR (15)   NULL,
    CONSTRAINT [PK_Dimensions] PRIMARY KEY CLUSTERED ([DimensionId] ASC) WITH (FILLFACTOR = 90)
);

