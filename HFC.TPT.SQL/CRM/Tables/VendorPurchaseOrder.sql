﻿CREATE TABLE [CRM].[VendorPurchaseOrder] (
    [VendorPurchaseOrderId]  INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PurchaseOrderId]        INT            NOT NULL,
    [PICPO]                  INT            NOT NULL,
    [VendorReference]        VARCHAR (25)   NOT NULL,
    [Status]                 INT            NULL,
    [StatusDescription]      VARCHAR (50)   NULL,
    [EstShipDate]            DATETIME       NULL,
    [DateAcknowledged]       DATETIME       NULL,
    [Currency]               VARCHAR (10)   NULL,
    [Terms]                  VARCHAR (100)  NULL,
    [Comment]                VARCHAR (500)  NULL,
    [VendorMsg]              VARCHAR (500)  NULL,
    [ShipName]               VARCHAR (100)  NULL,
    [ShipAddress1]           VARCHAR (150)  NULL,
    [ShipAddress2]           VARCHAR (150)  NULL,
    [ShipCity]               VARCHAR (50)  NULL,
    [ShipState]              VARCHAR (25)  NULL,
    [ShipZip]                VARCHAR (15)  NULL,
    [ShipCountry]            VARCHAR (10)  NULL,
    [ShipPhone]              VARCHAR (15)  NULL,
    [ShipFax]                VARCHAR (15)  NULL,
    [POResponse]             VARCHAR (MAX)  NULL,
    [CreatedBy]              INT             NULL,
    [CreatedOn]              DATETIME        NULL,
    [UpdatedBy]              INT             NULL,
    [UpdatedOn]              DATETIME        NULL,
    CONSTRAINT [PK_VendorPurchaseOrder] PRIMARY KEY CLUSTERED ([VendorPurchaseOrderId] ASC),
    CONSTRAINT [FK_VendorPurchaseOrder_MasterPurchaseOrder] FOREIGN KEY ([PurchaseOrderId]) REFERENCES [CRM].[MasterPurchaseOrder] ([PurchaseOrderId])
);
GO

CREATE NONCLUSTERED INDEX [_dta_index_VendorPurchaseOrder] ON [CRM].[VendorPurchaseOrder]
(
	[PurchaseOrderId] ASC,
	[PICPO] ASC,
    [vendorReference] ASC
)
