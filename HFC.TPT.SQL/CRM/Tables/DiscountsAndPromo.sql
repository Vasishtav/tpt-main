﻿CREATE TABLE [CRM].[DiscountsAndPromo] (
    [Id]                    INT             IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [QuoteLineId]           INT             NOT NULL,
    [QuoteKey]              INT             NOT NULL,
    [QuoteLineDetailId]     INT             NULL,
    [Description]           VARCHAR (500)   NULL,
    [Discount]              NUMERIC (18, 2) NULL,
    [PromoStartDate]        DATETIME        NULL,
    [PromoEndtDate]         DATETIME        NULL,
    [DiscountAmountPassed]  NUMERIC (18, 2) NULL,
    [DiscountPercentPassed] NUMERIC (18, 2) NULL,
    [CreatedOn]             DATETIME2 (7)   NOT NULL,
    [CreatedBy]             INT             NOT NULL,
    [LastUpdatedOn]         DATETIME2 (7)   NULL,
    [LastUpdatedBy]         INT             NULL,
    [Cost] NUMERIC(18, 2) NULL, 
    CONSTRAINT [PK_DiscountsAndPromo] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_DiscountsAndPromo_Person_Createdby] FOREIGN KEY ([CreatedBy]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_DiscountsAndPromo_Person_LastUpdatedBy] FOREIGN KEY ([LastUpdatedBy]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_DiscountsAndPromo_QuoteLineDetail_QuoteLineDetailId] FOREIGN KEY ([QuoteLineDetailId]) REFERENCES [CRM].[QuoteLineDetail] ([QuoteLineDetailId])
);

