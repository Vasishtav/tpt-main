﻿CREATE TABLE [CRM].[Orders] (
    [OrderID]              INT             IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [OpportunityId]        INT             NOT NULL,
    [QuoteKey]             INT             NOT NULL,
    [OrderName]            VARCHAR (100)   NOT NULL,
    [OrderStatus]          INT             NULL,
    [ContractedDate]       DATETIME2 (7)   NULL,
    [ProductSubtotal]      NUMERIC (18, 2) NULL,
    [AdditionalCharges]    NUMERIC (18, 2) NULL,
    [OrderDiscount]        NUMERIC (18, 2) NULL,
    [OrderSubtotal]        NUMERIC (18, 2) NULL,
    [Tax]                  NUMERIC (18, 2) NULL,
    [OrderTotal]           NUMERIC (18, 2) NULL,
    [BalanceDue]           NUMERIC (18, 2) NULL,
    [CreatedOn]            DATETIME2 (7)   NULL,
    [CreatedBy]            INT             NULL,
    [LastUpdatedOn]        DATETIME2 (7)   NULL,
    [LastUpdatedBy]        INT             NULL,
    [OrderNumber]          INT             NULL,
    [TaxCalculated]        BIT             DEFAULT ((1)) NULL,
    [IsTaxExempt]          BIT             NULL,
    [TaxExemptID]          NVARCHAR (25)   NULL,
    [CancelReason]         NVARCHAR (100)  NULL,
    [ImpersonatorPersonId] INT             NULL,
    [SendReviewEmail] BIT NOT NULL DEFAULT 0, 
    [ContractedDateUpdatedOn] DATETIME2 NULL, 
    [ContractedDateUpdatedBy] INT NULL, 
    [PreviousContractedDate] DATETIME2 NULL, 
    CONSTRAINT [PK__Orders__C3905BAFC38F435F] PRIMARY KEY CLUSTERED ([OrderID] ASC),
    CONSTRAINT [FK_Orders_Opportunities] FOREIGN KEY ([OpportunityId]) REFERENCES [CRM].[Opportunities] ([OpportunityId]),
    CONSTRAINT [FK_Orders_Person] FOREIGN KEY ([CreatedBy]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_Orders_Person1] FOREIGN KEY ([LastUpdatedBy]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_Orders_Quote] FOREIGN KEY ([QuoteKey]) REFERENCES [CRM].[Quote] ([QuoteKey])
);


GO

CREATE TRIGGER [CRM].[OrderStatusTrackerTrigger] ON [CRM].[Orders]
FOR INSERT, UPDATE
AS
     BEGIN
          IF UPDATE(OrderStatus)  AND NOT EXISTS(SELECT 1 FROM DELETED d INNER JOIN INSERTED i on i.OrderStatus = d.OrderStatus)
             BEGIN
                 INSERT INTO CRM.OrderStatusTracker
                 ([OrderID],
                  [OrderStatusId],
                  [CreatedOn],
                  [CreatedBy]
                 )
                       SELECT [OrderID],
                              [OrderStatus],
                               GETUTCDATE(),
                               CreatedBy
                        FROM inserted;
         END;            
     END;
     GO
     CREATE NONCLUSTERED INDEX [_dta_index_Orders_6_1812019743__K1_2_14_18] ON [CRM].[Orders]
(
	[OrderID] ASC
)
INCLUDE([OpportunityId],[CreatedOn],[OrderNumber]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
