﻿CREATE TABLE [CRM].[TPTNumbers] (
    [FranchiseId]   INT NOT NULL,
    [AccountNumber] INT DEFAULT ((1000)) NOT NULL,
    [QuoteNumber]   INT DEFAULT ((1000)) NOT NULL,
    [OrderNumber]   INT DEFAULT ((1000)) NOT NULL,
    [InvoiceNumber] INT DEFAULT ((1000)) NOT NULL,
    [PONumber]      INT DEFAULT ((1000)) NOT NULL,
    CONSTRAINT [PK_TPTNumbers] PRIMARY KEY CLUSTERED ([FranchiseId] ASC) WITH (FILLFACTOR = 90)
);

