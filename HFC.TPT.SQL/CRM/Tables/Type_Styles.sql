﻿CREATE TABLE [CRM].[Type_Styles] (
    [Id]       SMALLINT     IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Name]     VARCHAR (50) NULL,
    [isCustom] BIT          NULL,
    CONSTRAINT [PK_Type_Styles] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 90)
);

