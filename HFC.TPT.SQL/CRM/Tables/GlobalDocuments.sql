﻿CREATE TABLE [CRM].[GlobalDocuments] (
    [Id]        INT            IDENTITY (1, 1) NOT NULL,
    [Title]     NVARCHAR (500) NULL,
    [Name]      NVARCHAR (500) NULL,
    [Category]  INT            NULL,
    [Concept]   INT            NULL,
    [streamId]  NVARCHAR (100) NULL,
    [IsEnabled] BIT            NULL,
    [CreatedOn] DATETIME2 (7)  NULL,
    [UpdatedOn] DATETIME2 (7)  NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

