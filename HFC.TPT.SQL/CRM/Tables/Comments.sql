﻿CREATE TABLE [CRM].[Comments] (
    [id]                 BIGINT          IDENTITY (1, 1) NOT NULL,
    [module]             INT  NULL,
    [moduleId]           INT             NULL,
    [userId]             INT             NULL,
    [parentId]           INT             NULL,
    [haschildId]         BIT             NULL,
    [userImageUrl]       NVARCHAR (1000) NULL,
    [userName]           NVARCHAR (100)  NULL,
    [htmlComment]        NVARCHAR (MAX)  NULL,
    [htmlCurrentComment] NVARCHAR (MAX)  NULL,
    [isReply]            BIT             NULL,
    [isEdit]             BIT             NULL,
    [createdAt]          DATETIME2 (7)   NULL,
    [updatedAt]          DATETIME2 (7)   NULL,
    [deletedAt]          DATETIME2 (7)   NULL,
    [isDeleted]          BIT             NULL,
    [VendorId] INT NULL, 
    [belongsTo] INT NOT NULL DEFAULT 0, 
    PRIMARY KEY CLUSTERED ([id] ASC)
);

