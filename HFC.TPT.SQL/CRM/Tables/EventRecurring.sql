﻿CREATE TABLE [CRM].[EventRecurring] (
    [RecurringEventId]      INT                IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [StartDate]             DATETIMEOFFSET (2) NOT NULL,
    [EndDate]               DATETIMEOFFSET (2) NOT NULL,
    [EndsOn]                DATETIMEOFFSET (2) NULL,
    [EndsAfterXOccurrences] INT                NULL,
    [RecursEvery]           INT                CONSTRAINT [DF_Table_1_RepeatEvery] DEFAULT ((1)) NOT NULL,
    [DayOfWeekEnum]         TINYINT            NULL,
    [CreatedOnUtc]          DATETIME2 (2)      CONSTRAINT [DF_Calendar_Recurring_CreatedOnUtc] DEFAULT (getutcdate()) NOT NULL,
    [CreatedByPersonId]     INT                NOT NULL,
    [LastUpdatedUtc]        DATETIME2 (2)      NULL,
    [LastUpdatedByPersonId] INT                NULL,
    [IsDeleted]             BIT                CONSTRAINT [DF_EventRecurring_IsDeleted] DEFAULT ((0)) NOT NULL,
    [MonthEnum]             TINYINT            NULL,
    [DayOfMonth]            SMALLINT           NULL,
    [PatternEnum]           TINYINT            CONSTRAINT [DF_EventRecurring_PatternEnum] DEFAULT ((1)) NOT NULL,
    [DayOfWeekIndex]        TINYINT            NULL,
    CONSTRAINT [PK_Calendar_Recurring] PRIMARY KEY CLUSTERED ([RecurringEventId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Calendar_Recurring_Person] FOREIGN KEY ([CreatedByPersonId]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_Calendar_Recurring_Person1] FOREIGN KEY ([LastUpdatedByPersonId]) REFERENCES [CRM].[Person] ([PersonId])
);

