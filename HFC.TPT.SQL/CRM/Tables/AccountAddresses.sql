﻿CREATE TABLE [CRM].[AccountAddresses] (
    [AccountId] INT NOT NULL,
    [AddressId] INT NOT NULL,
	[IsPrimaryAddress] BIT NOT NULL DEFAULT (0),
    CONSTRAINT [PK_AccountAddresses] PRIMARY KEY CLUSTERED ([AccountId] ASC, [AddressId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_AccountAddresses_Accounts] FOREIGN KEY ([AccountId]) REFERENCES [CRM].[Accounts] ([AccountId]),
    CONSTRAINT [FK_AccountAddresses_Addresses] FOREIGN KEY ([AddressId]) REFERENCES [CRM].[Addresses] ([AddressId]) ON DELETE CASCADE
);

