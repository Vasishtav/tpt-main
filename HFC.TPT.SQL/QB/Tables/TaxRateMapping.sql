﻿CREATE TABLE [QB].[TaxRateMapping](
	[FranchiseId] [int] NULL,
	[TPTTaxName] [varchar](100) NULL,
	[QBOTaxName] [varchar](100) NULL
);