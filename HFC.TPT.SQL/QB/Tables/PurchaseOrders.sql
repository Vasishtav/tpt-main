﻿CREATE TABLE [QB].[PurchaseOrders] (
    [QBPOGuid]        VARCHAR (50)       NOT NULL,
    [PurchaseOrderId] INT                NOT NULL,
    [PONumber]        VARCHAR (15)       NOT NULL,
    [CreatedOn]       DATETIMEOFFSET (2) CONSTRAINT [DF_PurchaseOrders_CreatedOnUtc] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_PurchaseOrders_1] PRIMARY KEY CLUSTERED ([QBPOGuid] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_PurchaseOrders_CRMPurchaseOrders] FOREIGN KEY ([PurchaseOrderId]) REFERENCES [Acct].[PurchaseOrders] ([PurchaseOrderId])
);

