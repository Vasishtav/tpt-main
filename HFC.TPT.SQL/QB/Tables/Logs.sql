﻿CREATE TABLE [QB].[Logs] (
    [LogId]        INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [IPAddress]    VARCHAR (50)  NULL,
    [SessionId]    VARCHAR (50)  NULL,
    [Message]      VARCHAR (MAX) NULL,
    [Trace]        VARCHAR (MAX) NULL,
    [Severity]     VARCHAR (50)  NULL,
    [CreatedOnUtc] DATETIME2 (2) CONSTRAINT [DF_Logs_CreatedOnUtc] DEFAULT (getutcdate()) NOT NULL,
    [Parameters]   VARCHAR (MAX) NULL,
    CONSTRAINT [PK_Logs] PRIMARY KEY CLUSTERED ([LogId] ASC) WITH (FILLFACTOR = 90)
);

