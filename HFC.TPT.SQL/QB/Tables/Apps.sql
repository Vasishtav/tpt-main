﻿CREATE TABLE [QB].[Apps] (
    [AppId]            INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [AppGuid]          UNIQUEIDENTIFIER CONSTRAINT [DF_Auth_AuthGuid] DEFAULT (newid()) NOT NULL,
    [FranchiseId]      INT              NOT NULL,
    [OwnerID]          VARCHAR (250)    NULL,
    [UserName]         VARCHAR (50)     NULL,
    [Password]         VARCHAR (250)    NULL,
    [IsActive]         BIT              CONSTRAINT [DF_Auth_IsActive] DEFAULT ((1)) NOT NULL,
    [SynchInvoices]    BIT              CONSTRAINT [DF_SynchInvoices] DEFAULT ((1)) NOT NULL,
    [SynchCost]        BIT              CONSTRAINT [DF_SynchCost] DEFAULT ((1)) NOT NULL,
    [SynchPayments]    BIT              CONSTRAINT [DF_SynchPayments] DEFAULT ((1)) NOT NULL,
    [TaxAgency]        VARCHAR (550)    NULL,
    [QWC]              VARCHAR (MAX)    NULL,
    [IsLastNameFirst]  BIT              DEFAULT ((1)) NOT NULL,
    [AppToken]         VARCHAR (4000)    CONSTRAINT [DF_Apps_AppToken] DEFAULT (' ') NULL,
    [ConsumerKey]      VARCHAR (100)    CONSTRAINT [DF_Apps_ConsumerKey] DEFAULT (' ') NULL,
    [ConsumerSecret]   VARCHAR (100)    CONSTRAINT [DF_Apps_ConsumerSecret] DEFAULT (' ') NULL,
    [IsSyncLocked]     BIT              NULL,
    [SyncLockedTime]   DATETIME         NULL,
    [SuppressSalesTax] BIT              NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [CreatedBy]        INT              NULL,
    [LastUpdatedOn]    DATETIME2 (7)    NULL,
    [LastUpdatedBy]    INT              NULL,
    [TaxDetailjurisdictions] BIT CONSTRAINT [DF_Apps_TaxDetailjurisdictions] DEFAULT (0) NULL, 
    [TaxCode] VARCHAR(200) NULL, 
    [AR] VARCHAR(200) NULL, 
    [Sales] VARCHAR(200) NULL, 
    [COG] VARCHAR(200) NULL, 
    CONSTRAINT [PK_Auth] PRIMARY KEY CLUSTERED ([AppId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_AuthApp_Franchise] FOREIGN KEY ([FranchiseId]) REFERENCES [CRM].[Franchise] ([FranchiseId])
);


GO
CREATE NONCLUSTERED INDEX [ix_o]
    ON [QB].[Apps]([OwnerID] ASC) WITH (FILLFACTOR = 90);

