﻿CREATE TABLE [QB].[Addresses] (
    [QBAddressGuid] VARCHAR (50)       NOT NULL,
    [AddressId]     INT                NOT NULL,
    [CreatedOn]     DATETIMEOFFSET (2) CONSTRAINT [DF_Addresses_CreatedOnUtc] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Addresses_2] PRIMARY KEY CLUSTERED ([QBAddressGuid] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Addresses_Addresses] FOREIGN KEY ([AddressId]) REFERENCES [CRM].[Addresses] ([AddressId])
);

