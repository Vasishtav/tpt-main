﻿CREATE TABLE [QB].[Customers] (
    [QBCustomerGuid] VARCHAR (50)       NOT NULL,
    [PersonId]       INT                NOT NULL,
    [CreatedOn]      DATETIMEOFFSET (2) CONSTRAINT [DF_Customers_CreatedOnUtc] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Customers_1] PRIMARY KEY CLUSTERED ([QBCustomerGuid] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Customers_Person] FOREIGN KEY ([PersonId]) REFERENCES [CRM].[Person] ([PersonId])
);

