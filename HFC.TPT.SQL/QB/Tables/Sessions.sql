﻿CREATE TABLE [QB].[Sessions] (
    [SessionId]       UNIQUEIDENTIFIER CONSTRAINT [DF_Sessions_SessionId] DEFAULT (newid()) NOT NULL,
    [AppId]           INT              NOT NULL,
    [CreatedOnUtc]    DATETIME2 (2)    CONSTRAINT [DF_Sessions_CreatedOnUtc] DEFAULT (getutcdate()) NOT NULL,
    [LastErrorMsg]    VARCHAR (MAX)    NULL,
    [IPAddress]       VARCHAR (50)     NULL,
    [ClosedOnUtc]     DATETIME2 (2)    NULL,
    [MajorVersion]    INT              NULL,
    [MinorVersion]    INT              NULL,
    [CompanyFileName] VARCHAR (256)    NULL,
    [Country]         VARCHAR (50)     NULL,
    CONSTRAINT [PK_Sessions] PRIMARY KEY CLUSTERED ([SessionId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Sessions_AuthApp] FOREIGN KEY ([AppId]) REFERENCES [QB].[Apps] ([AppId])
);

