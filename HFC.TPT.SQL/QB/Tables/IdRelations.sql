﻿CREATE TABLE [QB].[IdRelations] (
    [RelationId]  INT IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ObjectId]    INT NOT NULL,
    [QbId]        INT NOT NULL,
    [ObjectType]  INT NOT NULL,
    [FranchiseId] INT NOT NULL,
    CONSTRAINT [PK_IdRelations] PRIMARY KEY CLUSTERED ([RelationId] ASC) WITH (FILLFACTOR = 90)
);

