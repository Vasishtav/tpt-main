﻿CREATE TABLE [QB].[SynchLog] (
    [SynchLogID]  INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [FranchiseId] INT           NOT NULL,
    [ObjectType]  INT           NOT NULL,
    [ObjectID]    INT           NOT NULL,
    [ObjectRef]   VARCHAR (500)  NULL,
    [SynchOn]     DATETIME      NOT NULL,
    [Message]     VARCHAR (MAX) NULL,
    [Status]      INT           NULL,
    [OwnerId]     VARCHAR (100) NULL,
    [QbId]        INT           NULL,
    CONSTRAINT [PK_SynchLog] PRIMARY KEY CLUSTERED ([SynchLogID] ASC) WITH (FILLFACTOR = 90)
);

