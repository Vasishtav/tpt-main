﻿CREATE TABLE [QB].[Payments] (
    [QBPaymentGuid] VARCHAR (50)       NOT NULL,
    [PaymentId]     INT                NOT NULL,
    [CreatedOn]     DATETIMEOFFSET (2) CONSTRAINT [DF_Payments_CreatedOnUtc] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Payments_1] PRIMARY KEY CLUSTERED ([QBPaymentGuid] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Payments_CRMPayments] FOREIGN KEY ([PaymentId]) REFERENCES [Acct].[Payments] ([PaymentId])
);

