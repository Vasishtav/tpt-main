﻿CREATE TABLE [History].[Edits] (
    [EditHistoryId]    BIGINT        IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [IPAddress]        VARCHAR (40)  NULL,
    [CreatedOnUtc]     DATETIME      CONSTRAINT [DF_Edits_CreatedOnUtc] DEFAULT (getutcdate()) NOT NULL,
    [HistoryValue]     VARCHAR (MAX) NULL,
    [LoggedByPersonId] INT           NULL,
    CONSTRAINT [PK_Edits] PRIMARY KEY CLUSTERED ([EditHistoryId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Edits_Person] FOREIGN KEY ([LoggedByPersonId]) REFERENCES [CRM].[Person] ([PersonId])
);

