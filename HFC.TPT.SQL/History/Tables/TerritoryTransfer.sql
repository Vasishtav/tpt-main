﻿CREATE TABLE [History].[TerritoryTransfer] (
    [Id]                 INT IDENTITY (1, 1) NOT NULL,
    [OrginalAccountId]   INT NULL,
    [NewAccountId]       INT NULL,
    [OrginalFranchiseId] INT NULL,
    [NewFranchiseid]     INT NULL
);

