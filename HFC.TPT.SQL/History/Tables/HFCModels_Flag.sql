﻿CREATE TABLE [History].[HFCModelFlags]
([Id]        INT IDENTITY(1, 1) NOT FOR REPLICATION NOT NULL, 
 [ModelKey]  INT, 
 [FieldName] VARCHAR(100), 
 [Oldvalue]  BIT, 
 [NewValue]  BIT, 
 [ChangedOn] DATETIME2(7) NOT NULL, 
 [ChangedBy] INT NOT NULL
                 CONSTRAINT [PK_HFCModelFlags] PRIMARY KEY CLUSTERED ([Id] ASC)
);


