﻿CREATE TABLE [History].[Leads] (
    [EditHistoryId] BIGINT NOT NULL,
    [LeadId]        INT    NOT NULL,
    CONSTRAINT [PK_Leads_1] PRIMARY KEY CLUSTERED ([EditHistoryId] ASC, [LeadId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Leads_Edits] FOREIGN KEY ([EditHistoryId]) REFERENCES [History].[Edits] ([EditHistoryId]),
    CONSTRAINT [FK_Leads_Leads] FOREIGN KEY ([LeadId]) REFERENCES [CRM].[Leads] ([LeadId])
);


GO
CREATE NONCLUSTERED INDEX [LeadId]
    ON [History].[Leads]([LeadId] ASC) WITH (FILLFACTOR = 90);

