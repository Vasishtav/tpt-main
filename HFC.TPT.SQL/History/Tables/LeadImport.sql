﻿CREATE TABLE [History].[LeadImport] (
    [LeadId]          INT              NULL,
    [LeadGuid]        UNIQUEIDENTIFIER NULL,
    [LeadImportValue] VARCHAR (MAX)    NULL,
    [Message]         VARCHAR (MAX)    NULL,
    [CreatedOn]       DATETIME2 (7)    CONSTRAINT [LeadImport_CreatedOn] DEFAULT (getutcdate()) NOT NULL
);

