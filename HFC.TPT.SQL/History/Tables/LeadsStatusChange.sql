﻿CREATE TABLE [History].[LeadsStatusChange] (
    [LeadsStatusChangeId] INT      IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [LeadId]              INT      NOT NULL,
    [LeadStatus]          INT      NOT NULL,
    [CreatedOnUtc]        DATETIME NOT NULL,
    [CreatedByPersonId]   INT      NULL,
    CONSTRAINT [PK_LeadsStatusChange] PRIMARY KEY CLUSTERED ([LeadsStatusChangeId] ASC)
);

