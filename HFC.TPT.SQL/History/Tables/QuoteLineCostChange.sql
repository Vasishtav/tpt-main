﻿CREATE TABLE [History].[QuoteLineCostChange]
(
	[Id] INT NOT NULL PRIMARY KEY   IDENTITY (1, 1), 
    [QuoteLineId] INT NOT NULL, 
    [QuoteKey] INT NOT NULL, 
    [OldCost] NUMERIC(18, 2) NULL, 
    [Cost] NUMERIC(18, 2) NULL, 
    [QuoteLineNumber] INT NULL, 
    [EditedBy] INT NULL, 
    [EditedOn] DATETIME2 NULL
)
