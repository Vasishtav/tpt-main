﻿CREATE TABLE [History].[AuditTrail] (
    [LeadId]                 INT           NULL,
    [AccountId]              INT           NULL,
    [OpportunityId]          INT           NULL,
    [OrderId]                INT           NULL,
    [TempData]               VARCHAR (MAX) NULL,
    [CreatedOn]              DATETIME2 (7) CONSTRAINT [DK_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [PricingId]              INT           NULL,
    [PurchaseOrderId]        INT           NULL,
    [PurchaseOrdersDetailId] INT           NULL, 
    [ProductId] INT NULL
);

