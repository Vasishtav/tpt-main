﻿CREATE TABLE [History].[MSRSubmission] (
    [Id]          INT                IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [TerritoryId] INT                NOT NULL,
    [ReportDate]  DATE               NOT NULL,
    [CreatedOn]   DATETIMEOFFSET (2) CONSTRAINT [DF_MSRSubmissionLogs_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [FromAddress] VARCHAR (256)      NOT NULL,
    CONSTRAINT [PK_MSRSubmissionLogs] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 90)
);

