﻿CREATE TABLE [History].[Jobs] (
    [EditHistoryId] BIGINT NOT NULL,
    [JobId]         INT    NOT NULL,
    CONSTRAINT [PK_Jobs_1] PRIMARY KEY CLUSTERED ([EditHistoryId] ASC, [JobId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Jobs_Edits] FOREIGN KEY ([EditHistoryId]) REFERENCES [History].[Edits] ([EditHistoryId]) ON DELETE CASCADE,
    CONSTRAINT [FK_Jobs_Jobs] FOREIGN KEY ([JobId]) REFERENCES [CRM].[Jobs] ([JobId])
);


GO
CREATE NONCLUSTERED INDEX [IX_JobId]
    ON [History].[Jobs]([JobId] ASC) WITH (FILLFACTOR = 90);

