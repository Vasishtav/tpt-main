﻿CREATE TABLE [API].[EventLog] (
    [LogId]        INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Type]         VARCHAR (100)  NULL,
    [Source]       VARCHAR (100)  NULL,
    [Message]      VARCHAR (1500) NULL,
    [Trace]        VARCHAR (MAX)  NULL,
    [Severity]     VARCHAR (50)   NULL,
    [UserName]     VARCHAR (50)   NULL,
    [CreatedOnUtc] DATETIME       CONSTRAINT [DF_EventLog_CreatedOnUtc] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_EventLog] PRIMARY KEY CLUSTERED ([LogId] ASC) WITH (FILLFACTOR = 90)
);

