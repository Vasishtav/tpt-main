﻿CREATE TABLE [API].[CalendarSync] (
    [SyncRecId]               INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [CalendarId]              INT            NULL,
    [ProviderUniqueId]        VARCHAR (256)  NULL,
    [ProviderName]            VARCHAR (50)   NOT NULL,
    [ProviderLastModifiedUtc] DATETIME2 (2)  NULL,
    [LastSyncDateUtc]         DATETIME2 (2)  CONSTRAINT [DF_CalendarSync_LastSyncDateUtc] DEFAULT (getutcdate()) NOT NULL,
    [ErrorCount]              SMALLINT       CONSTRAINT [DF_CalendarSync_ErrorCount] DEFAULT ((0)) NOT NULL,
    [LastErrorDateUtc]        DATETIME2 (2)  CONSTRAINT [DF_CalendarSync_LastErrorDateUtc] DEFAULT (getutcdate()) NULL,
    [LastErrorMessage]        VARCHAR (2000) NULL,
    [ProviderSequence]        INT            CONSTRAINT [DF_CalendarSync_ProviderSequence] DEFAULT ((0)) NOT NULL,
    [EventPersonId]           INT            NULL,
    [EmailSent]               BIT            NULL,
    [SyncURL]                 VARCHAR (250)  NULL,
    CONSTRAINT [PK_CalendarSync_1] PRIMARY KEY CLUSTERED ([SyncRecId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_CalendarSync_Calendar] FOREIGN KEY ([CalendarId]) REFERENCES [CRM].[Calendar] ([CalendarId])
);


GO
CREATE NONCLUSTERED INDEX [CalendarIdx]
    ON [API].[CalendarSync]([CalendarId] ASC, [ProviderUniqueId] ASC, [ProviderName] ASC)
    INCLUDE([SyncRecId], [ProviderLastModifiedUtc], [ErrorCount]) WITH (FILLFACTOR = 90);

