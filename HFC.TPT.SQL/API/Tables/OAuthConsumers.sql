﻿CREATE TABLE [API].[OAuthConsumers] (
    [OAConsumerId]     INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Key]              UNIQUEIDENTIFIER CONSTRAINT [DF_OAuthConsumers_ConsumerGuid] DEFAULT (newid()) NOT NULL,
    [Secret]           UNIQUEIDENTIFIER CONSTRAINT [DF_OAuthConsumers_ClientSecret] DEFAULT (newid()) NOT NULL,
    [CallbackUrl]      VARCHAR (2000)   NOT NULL,
    [AllowIPAddresses] VARCHAR (MAX)    NULL,
    [CreatedOnUtc]     DATETIME2 (2)    CONSTRAINT [DF_OAuthConsumers_CreatedOnUtc] DEFAULT (getutcdate()) NOT NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_OAuthConsumers_IsDeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_OAuthConsumers] PRIMARY KEY CLUSTERED ([OAConsumerId] ASC) WITH (FILLFACTOR = 90)
);

