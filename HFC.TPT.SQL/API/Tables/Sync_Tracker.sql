﻿CREATE TABLE [API].[Sync_Tracker] (
    [SyncRecId]              INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [UserId]                 UNIQUEIDENTIFIER NOT NULL,
    [ProviderName]           VARCHAR (50)     NULL,
    [LastSyncDateUtc]        DATETIME2 (2)    CONSTRAINT [DF_ExchangeSync_Tracker_LastSyncDateUtc] DEFAULT (getutcdate()) NOT NULL,
    [LastErrorMessage]       VARCHAR (2000)   NULL,
    [LastErrorDateUtc]       DATETIME2 (2)    NULL,
    [ErrorCount]             INT              CONSTRAINT [DF_Sync_Tracker_ErrorCount] DEFAULT ((0)) NOT NULL,
    [MessageNumber]          INT              NULL,
    [ChannelId]              VARCHAR (256)    NULL,
    [ResourceId]             VARCHAR (256)    NULL,
    [ResourceExtraInfo]      VARCHAR (2048)   NULL,
    [ExpiresOnUtc]           DATETIME2 (2)    NULL,
    [IsConnected]            BIT              CONSTRAINT [DF_Sync_Tracker_IsConnected] DEFAULT ((0)) NOT NULL,
    [ProviderLastUpdatedUtc] DATETIME2 (2)    NULL,
    [LastSyncState]          VARCHAR (MAX)    NULL,
    CONSTRAINT [PK_ExchangeSync_Tracker] PRIMARY KEY CLUSTERED ([SyncRecId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Sync_Tracker_Users] FOREIGN KEY ([UserId]) REFERENCES [Auth].[Users] ([UserId])
);

