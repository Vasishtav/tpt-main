﻿CREATE FUNCTION [DW].[fnt_SourceSales_Monthly] 
(	
	@startDate		date,
	@endDate		date,	
	@franchiseId	INT = null,
	@sourceId		INT = null
)
/*
select [Key],[Value],[Color] from [DW].[fnt_SourceSales_Monthly] ('01/01/2016', '05/31/2016', 2268, null)
select [Key],[Value],[Color] from [DW].[fnt_SourceSales_Monthly] ('01/01/2016', '12/31/2016', 2268, null)
*/
RETURNS @Results TABLE(   
	[Key] [varchar](1000) NULL,
	[Value] [decimal](38, 4) NULL,
	[Color] [int] NULL
) 
AS BEGIN
		INSERT @Results([Key],[Value],[Color]) 
		SELECT TOP 5 [Key], sum([Value]) as [Value],null [Color]
		FROM [DW].[SourceSales_Monthly] (NOLOCK)
		WHERE franchiseId=@franchiseId and startDate >=@startDate and endDate <=@endDate
		group by [Key]
		ORDER BY [Value] desc;
RETURN
END

