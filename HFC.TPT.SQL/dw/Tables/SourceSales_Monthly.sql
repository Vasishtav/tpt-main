﻿CREATE TABLE [dw].[SourceSales_Monthly] (
    [FranchiseId] INT             NULL,
    [startDate]   DATETIME        NULL,
    [endDate]     DATETIME        NULL,
    [Key]         VARCHAR (1000)  NULL,
    [Value]       DECIMAL (38, 4) NULL,
    [Color]       INT             NULL
);

