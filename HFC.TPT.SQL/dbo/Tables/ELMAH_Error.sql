﻿CREATE TABLE [dbo].[ELMAH_Error] (
    [Sequence]    INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ErrorId]     UNIQUEIDENTIFIER CONSTRAINT [DF_ELMAH_Error_ErrorId] DEFAULT (newid()) NULL,
    [Application] VARCHAR (120)    NULL,
    [Host]        VARCHAR (120)    NULL,
    [Type]        VARCHAR (200)    NULL,
    [Source]      VARCHAR (200)    NULL,
    [Message]     NVARCHAR (3000)  NULL,
    [User]        NVARCHAR (500)   NULL,
    [StatusCode]  INT              NULL,
    [TimeUtc]     DATETIME         NULL,
    [AllXml]      NTEXT            NULL,
    CONSTRAINT [PK_ELMAH_Error] PRIMARY KEY NONCLUSTERED ([Sequence] ASC) WITH (FILLFACTOR = 90)
);

