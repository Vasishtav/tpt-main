﻿
CREATE view [dbo].[vw_CommercialLeads]
as
SELECT DISTINCT l.LeadId,
		case 
			when (sl.JobConceptId = 2 or a.IsResidential = 0) then 1
			when  sl.JobConceptId = 3 then 2
			when  a.IsResidential = 1 then 3 
			when  a.IsResidential = 2 then 3
			else -1
		end as CommercialType
FROM  CRM.Leads l  with (nolock)	
LEFT OUTER JOIN 
(
	SELECT  la.LeadId,COUNT(DISTINCT a.IsResidential) as IsResidential /*IF LEAD HAS BOTH ITS COMMERCIAL*/
	FROM  crm.LeadAddresses as la  WITH (NOLOCK) 
	INNER JOIN crm.Addresses  as a  WITH (NOLOCK)
		on a.AddressId = la.AddressId
	GROUP BY la.LeadId 
) a
	on l.LeadId=a.LeadId
LEFT OUTER JOIN (
	SELECT DISTINCT l.LeadId, j.JobConceptId as JobConceptId 
	FROM crm.Leads l WITH (NOLOCK) 
	LEFT JOIN [CRM].[Jobs] j WITH (NOLOCK) 
		on j.LeadId = l.LeadId
	WHERE j.JobConceptId = 2
)  sl 
	on sl.LeadId = l.LeadId

--GO



