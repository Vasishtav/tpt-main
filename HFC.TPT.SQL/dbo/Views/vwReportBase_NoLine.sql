﻿
CREATE VIEW [dbo].[vwReportBase_NoLine]
as
SELECT	
	l.FranchiseId,			
	DATEPART(MONTH,cast(j.ContractedOnUtc as date)) as YMonth,
	DATEPART(YEAR,cast(j.ContractedOnUtc as date)) as YYear,
	cast(j.ContractedOnUtc as date) as  ContractedOn,
	cast(l.CreatedOnUtc as date) as CreatedOn,
	com.CommercialType,
	l.leadid,
	isnull(a.ZipCode,'N/A') as ZipCode ,
	isnull(a.city,'N/A') as City,
	j.JobId  as JobId,
	jq.QuoteId,
	j.JobNumber as JobNumber,
	j.JobStatusId,
	l.leadStatusId,
	t.Code as TerritoryCode,
	j.TerritoryId, 
	ji.CostSubtotal [Cost], 
	ji.Subtotal Subtotal, 
	ji.Subtotal +isnull(ac.SurchargeTotal,0)-isnull(d.DiscountTotal,0) as Sales,
	isnull(ac.SurchargeTotal,0) as SurchargeTotal,
	isnull(d.DiscountTotal,0) as DiscountTotal
FROM CRM.Leads l 
LEFT OUTER JOIN [dbo].[vw_CommercialLeads] as com 
	on com.leadId = l.LeadId
LEFT OUTER JOIN CRM.Jobs j 
	on j.LeadId = l.LeadId
LEFT OUTER JOIN CRM.JobQuotes jq 
	on jq.JobId = j.JobId 
	and jq.IsPrimary = 1
LEFT OUTER JOIN CRM.Addresses a 
	on a.AddressId = j.[InstallAddressId]
LEFT OUTER JOIN 
	(
		SELECT 
		t.QuoteId,isnull(sum(t.amount),0)  as SurchargeTotal
		FROM [CRM].[JobSaleAdjustments] t
		WHERE t.[TypeEnum]=0
		GROUP BY t.QuoteId
	) AC
	on ac.QuoteId=jq.QuoteId
LEFT OUTER JOIN 
	(
		SELECT t.QuoteId,  (ISNULL(SUM(t.amount),0) + SUM(ji.Subtotal *(t.[percent]*.01))) as DiscountTotal
		FROM [CRM].[JobSaleAdjustments] t
		LEFT OUTER JOIN 
				(SELECT i.QuoteId, sum(i.Subtotal) as Subtotal
				FROM CRM.JobItems i 
				GROUP BY i.QuoteId ) ji 
			ON t.QuoteId = ji.QuoteId
		WHERE t.[TypeEnum]=1
		GROUP BY t.QuoteId
	) D 
	on d.QuoteId=jq.QuoteId
LEFT OUTER JOIN 
(
	SELECT i.QuoteId,
		sum(i.CostSubtotal) CostSubtotal, 
		sum(i.Subtotal) Subtotal 
	FROM CRM.JobItems i 
	GROUP BY i.QuoteId
) ji 
	on jq.QuoteId = ji.QuoteId
LEFT OUTER JOIN [CRM].[Territories] t
	on t.TerritoryId=j.TerritoryId
WHERE
ISNULL(l.IsDeleted, 0) <> 1
