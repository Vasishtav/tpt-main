﻿CREATE TABLE [Acct].[Invoices] (
    [InvoiceId]           INT                IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [InvoiceNumber]       VARCHAR (50)       NULL,
    [InvoiceGuid]         UNIQUEIDENTIFIER   CONSTRAINT [DF_Invoices_InvoiceGuid] DEFAULT (newid()) NOT NULL,
    [JobId]               INT                NOT NULL,
    [LastUpdated]         DATETIMEOFFSET (2) CONSTRAINT [DF_Invoices_LastUpdated] DEFAULT (getdate()) NOT NULL,
    [CreatedOn]           DATETIMEOFFSET (2) CONSTRAINT [DF_Invoices_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatorPersonId]     INT                NULL,
    [LastUpdatedPersonId] INT                NULL,
    [IsDeleted]           BIT                CONSTRAINT [DF_Invoices_IsDeleted] DEFAULT ((0)) NOT NULL,
    [QuoteId]             INT                NOT NULL,
    [StatusEnum]          TINYINT            CONSTRAINT [DF_Invoices_StatusEnum] DEFAULT ((0)) NOT NULL,
    [PaidInFullOn]        DATETIMEOFFSET (2) NULL,
    [Amount]              DECIMAL (18, 4)    CONSTRAINT [DF_Invoices_Amount] DEFAULT ((0)) NOT NULL,
    [InvoiceDate]         DATETIME           NULL,
    [UpdatedDate]         DATETIME           CONSTRAINT [DF_Invoices_UpdatedDate] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_Invoices] PRIMARY KEY CLUSTERED ([InvoiceId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Invoices_CreatorPerson] FOREIGN KEY ([CreatorPersonId]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_Invoices_JobQuotes] FOREIGN KEY ([QuoteId]) REFERENCES [CRM].[JobQuotes] ([QuoteId]),
    CONSTRAINT [FK_Invoices_Jobs] FOREIGN KEY ([JobId]) REFERENCES [CRM].[Jobs] ([JobId]),
    CONSTRAINT [FK_Invoices_LastUpdatedPerson] FOREIGN KEY ([LastUpdatedPersonId]) REFERENCES [CRM].[Person] ([PersonId])
);


GO
CREATE NONCLUSTERED INDEX [IX_QuoteId]
    ON [Acct].[Invoices]([QuoteId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [ix_JobId]
    ON [Acct].[Invoices]([JobId] ASC)
    INCLUDE([InvoiceNumber]) WITH (FILLFACTOR = 90);

