﻿CREATE TABLE [Acct].[Vendors] (
    [VendorId]      INT                IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [VendorGuid]    UNIQUEIDENTIFIER   CONSTRAINT [DF_Vendors_VendorGuid] DEFAULT (newid()) NOT NULL,
    [Name]          VARCHAR (256)      NOT NULL,
    [Description]   VARCHAR (MAX)      NULL,
    [CreatedOn]     DATETIMEOFFSET (2) CONSTRAINT [DF_Vendors_CreatedOnUtc] DEFAULT (getdate()) NOT NULL,
    [Phone]         VARCHAR (15)       NULL,
    [AltPhone]      VARCHAR (15)       NULL,
    [Fax]           VARCHAR (15)       NULL,
    [Email]         VARCHAR (256)      NULL,
    [Website]       VARCHAR (256)      NULL,
    [BillAddressId] INT                NULL,
    [ShipAddressId] INT                NULL,
    [SplitPOs]      BIT                CONSTRAINT [DF_Vendors_SplitPOs] DEFAULT ((0)) NOT NULL,
    [LastUpdated]   DATETIMEOFFSET (2) CONSTRAINT [DF_Vendors_LastUpdated] DEFAULT (getdate()) NOT NULL,
    [ReferenceId]   INT                NULL,
    [ReferenceNum]  VARCHAR (50)       NULL,
    [IsActive]      BIT                NULL,
    [IsManual]      BIT                DEFAULT ((0)) NULL,
    CONSTRAINT [PK_Vendors] PRIMARY KEY CLUSTERED ([VendorId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Vendors_BillAddresses] FOREIGN KEY ([BillAddressId]) REFERENCES [CRM].[Addresses] ([AddressId]),
    CONSTRAINT [FK_Vendors_ShipAddresses] FOREIGN KEY ([ShipAddressId]) REFERENCES [CRM].[Addresses] ([AddressId])
);

