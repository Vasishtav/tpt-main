﻿CREATE TABLE [Acct].[Payments] (
    [PaymentId]     INT                IDENTITY (10000, 1) NOT FOR REPLICATION NOT NULL,
    [PaymentGuid]   UNIQUEIDENTIFIER   CONSTRAINT [DF_Payments_PaymentGuid] DEFAULT (newid()) NOT NULL,
    [Amount]        DECIMAL (18, 4)    CONSTRAINT [DF_Payments_Amount] DEFAULT ((0)) NOT NULL,
    [InvoiceId]     INT                NOT NULL,
    [CreatedOn]     DATETIMEOFFSET (2) CONSTRAINT [DF_Payments_CreatedOnUtc] DEFAULT (getdate()) NOT NULL,
    [PaymentMethod] VARCHAR (50)       NULL,
    [PaymentDate]   DATETIMEOFFSET (2) NOT NULL,
    [ReferenceNum]  VARCHAR (50)       NULL,
    [Memo]          VARCHAR (256)      NULL,
    [UpdatedDate]   DATETIME           CONSTRAINT [DF_Payments_UpdatedDate] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_Payments] PRIMARY KEY CLUSTERED ([PaymentId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Payments_Invoices] FOREIGN KEY ([InvoiceId]) REFERENCES [Acct].[Invoices] ([InvoiceId])
);


GO
CREATE NONCLUSTERED INDEX [IX_InvoiceId]
    ON [Acct].[Payments]([InvoiceId] ASC)
    INCLUDE([PaymentId], [PaymentGuid], [Amount], [CreatedOn], [PaymentMethod], [PaymentDate], [ReferenceNum], [Memo], [UpdatedDate]) WITH (FILLFACTOR = 90);

