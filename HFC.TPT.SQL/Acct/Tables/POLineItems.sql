﻿CREATE TABLE [Acct].[POLineItems] (
    [POLineItemId]       INT                IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PurchaseOrderId]    INT                NOT NULL,
    [JobItemId]          INT                NULL,
    [ProductGuid]        UNIQUEIDENTIFIER   NULL,
    [ProductCode]        VARCHAR (50)       NULL,
    [ProductName]        VARCHAR (200)      NULL,
    [ProductDescription] VARCHAR (MAX)      NULL,
    [Quantity]           INT                CONSTRAINT [DF_POLineItems_Quantity] DEFAULT ((0)) NOT NULL,
    [Price]              DECIMAL (18, 4)    CONSTRAINT [DF_POLineItems_Price] DEFAULT ((0)) NOT NULL,
    [Subtotal]           DECIMAL (18, 4)    CONSTRAINT [DF_POLineItems_Subtotal] DEFAULT ((0)) NOT NULL,
    [CreatedOn]          DATETIMEOFFSET (2) CONSTRAINT [DF_POLineItems_CreatedOnUtc] DEFAULT (getdate()) NOT NULL,
    [CreatedByPersonId]  INT                NULL,
    [UpdatedOn]          DATETIMEOFFSET (2) CONSTRAINT [DF_POLineItems_UpdatedOn] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_POLineItems] PRIMARY KEY CLUSTERED ([POLineItemId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_POLineItems_CreatorPerson] FOREIGN KEY ([CreatedByPersonId]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_POLineItems_JobItems] FOREIGN KEY ([JobItemId]) REFERENCES [CRM].[JobItems] ([JobItemId]),
    CONSTRAINT [FK_POLineItems_PurchaseOrders] FOREIGN KEY ([PurchaseOrderId]) REFERENCES [Acct].[PurchaseOrders] ([PurchaseOrderId])
);

