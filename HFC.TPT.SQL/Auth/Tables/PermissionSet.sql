﻿CREATE TABLE [Auth].[PermissionSet] (
    [Id]                INT           IDENTITY (1, 1) NOT NULL,
    [PermissionSetName] VARCHAR (200) NOT NULL,
    [Description]       VARCHAR (500) NULL,
    [FranchiseId]       INT           NULL,
    [CreatedOn]         DATETIME2 (7) NOT NULL,
    [CreatedBy]         INT           NOT NULL,
    [LastUpdatedOn]     DATETIME2 (7) NULL,
    [LastUpdatedBy]     INT           NULL,
    [IsActive] BIT NULL, 
    CONSTRAINT [PK_PermissionSet] PRIMARY KEY CLUSTERED ([Id] ASC)
);

