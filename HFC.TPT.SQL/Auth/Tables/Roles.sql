﻿CREATE TABLE [Auth].[Roles] (
    [RoleId]        UNIQUEIDENTIFIER CONSTRAINT [DF_Roles_RoleId] DEFAULT (newid()) NOT NULL,
    [RoleName]      NVARCHAR (256)   NOT NULL,
    [Description]   NVARCHAR (MAX)   NULL,
    [CreatedOnUtc]  DATETIME         CONSTRAINT [DF_Roles_CreatedOnUtc] DEFAULT (getutcdate()) NOT NULL,
    [IsDeletable]   BIT              CONSTRAINT [DF_Roles_IsDeleteable] DEFAULT ((1)) NOT NULL,
    [IsInheritable] BIT              CONSTRAINT [DF_Roles_IsInheritable] DEFAULT ((0)) NOT NULL,
    [FranchiseId]   INT              NULL,
    [IsActive]      BIT              NULL,
    [DisplayName]   VARCHAR (50)     NULL,
    [SortOrder] TINYINT NULL, 
    [RoleType] INT NULL, 
    CONSTRAINT [PK__Roles] PRIMARY KEY CLUSTERED ([RoleId] ASC) WITH (FILLFACTOR = 90)
);

