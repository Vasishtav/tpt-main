﻿CREATE TABLE [Auth].[Type_Module] (
    [Id]            SMALLINT      IDENTITY (1, 1) NOT NULL,
    [ModuleName]    VARCHAR (500) NOT NULL,
    [ModuleCode]    VARCHAR (500) NOT NULL,
    [ModuleGroupId] INT           NULL,
    [ParentGroupId] INT NULL, 
    [RoleType] INT NULL, 
    CONSTRAINT [PK_Type_Module] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Type_Module_ModuleGroupId] FOREIGN KEY ([ModuleGroupId]) REFERENCES [CRM].[Type_LookUpValues] ([Id]),
	CONSTRAINT [FK_Type_Module_ParentGroupId] FOREIGN KEY ([ParentGroupId]) REFERENCES [CRM].[Type_LookUpValues] ([Id])
);

