﻿CREATE TABLE [Auth].[Users] (
    [UserId]                            UNIQUEIDENTIFIER CONSTRAINT [DF_Users_UserId] DEFAULT (newid()) NOT NULL,
    [UserName]                          NVARCHAR (150)   NOT NULL,
    [PersonId]                          INT              CONSTRAINT [DF_Users_PersonId] DEFAULT ((0)) NOT NULL,
    [AddressId]                         INT              NULL,
    [CreationDateUtc]                   DATETIME         CONSTRAINT [DF_Users_CreationDate] DEFAULT (getutcdate()) NOT NULL,
    [Password]                          NVARCHAR (128)   NOT NULL,
    [Domain]                            NVARCHAR (256)   NULL,
    [DomainPath]                        NVARCHAR (512)   NULL,
    [LastActivityDateUtc]               DATETIME         NULL,
    [IsDeleted]                         BIT              CONSTRAINT [DF_Users_IsDeleted] DEFAULT ((0)) NOT NULL,
    [IsApproved]                        BIT              CONSTRAINT [DF_Users_IsApproved] DEFAULT ((0)) NOT NULL,
    [IsLockedOut]                       BIT              CONSTRAINT [DF_Users_IsLockedOut] DEFAULT ((0)) NOT NULL,
    [LastLoginDateUtc]                  DATETIME         NULL,
    [LastPasswordChangedDateUtc]        DATETIME         NULL,
    [LastLockoutDateUtc]                DATETIME         NULL,
    [FailedPasswordAttemptCount]        INT              CONSTRAINT [DF_Users_FailedPasswordAttemptCount] DEFAULT ((0)) NOT NULL,
    [FailedPasswordAttemptStartDateUtc] DATETIME         NULL,
    [Comment]                           NVARCHAR (256)   NULL,
    [LastKnownIPAddress]                VARCHAR (40)     NULL,
    [FranchiseId]                       INT              NULL,
    [Email]                             VARCHAR (256)    NULL,
    [ColorId]                           INT              CONSTRAINT [DF_Users_ColorId] DEFAULT ((1)) NOT NULL,
    [SyncStartsOnUtc]                   DATETIME         CONSTRAINT [DF_Users_IsSyncEnabled] DEFAULT (getutcdate()) NULL,
    [CalendarFirstHour]                 INT              CONSTRAINT [DF_Users_CalendarFirstHour] DEFAULT ((8)) NOT NULL,
    [AvatarSrc]                         VARCHAR (512)    NULL,
    [EmailSignature]                    VARCHAR (MAX)    NULL,
    [CalSync]                           BIT              DEFAULT ((0)) NULL,
    [IsDisabled]                        BIT              NULL,
    CONSTRAINT [PK__Users] PRIMARY KEY CLUSTERED ([UserId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Users_Addresses] FOREIGN KEY ([AddressId]) REFERENCES [CRM].[Addresses] ([AddressId]) ON DELETE CASCADE,
    CONSTRAINT [FK_Users_Franchise] FOREIGN KEY ([FranchiseId]) REFERENCES [CRM].[Franchise] ([FranchiseId]),
    CONSTRAINT [FK_Users_Person] FOREIGN KEY ([PersonId]) REFERENCES [CRM].[Person] ([PersonId]) ON DELETE CASCADE,
    CONSTRAINT [FK_Users_Type_Colors] FOREIGN KEY ([ColorId]) REFERENCES [CRM].[Type_Colors] ([ColorId])
);


GO
CREATE NONCLUSTERED INDEX [PersonIdIndex]
    ON [Auth].[Users]([PersonId] ASC)
    INCLUDE([UserId], [Domain], [IsDeleted], [SyncStartsOnUtc]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [ix_FranchiseId]
    ON [Auth].[Users]([FranchiseId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE UNIQUE NONCLUSTERED INDEX [iU_UserPerFranchise]
    ON [Auth].[Users]([UserId] ASC, [FranchiseId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [ix_userDeleted]
    ON [Auth].[Users]([UserName] ASC, [IsDeleted] ASC) WITH (FILLFACTOR = 90);

