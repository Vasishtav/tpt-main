﻿CREATE TABLE [Auth].[Permissions] (
    [PermissionId]     INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [CanCreate]        BIT              CONSTRAINT [DF_Auth.Permissions_CanCreate] DEFAULT ((0)) NOT NULL,
    [CanRead]          BIT              CONSTRAINT [DF_Auth.Permissions_CanRead] DEFAULT ((0)) NOT NULL,
    [CanUpdate]        BIT              CONSTRAINT [DF_Table_1_CanWrite] DEFAULT ((0)) NOT NULL,
    [CanDelete]        BIT              CONSTRAINT [DF_Auth.Permissions_CanDelete] DEFAULT ((0)) NOT NULL,
    [PermissionTypeId] INT              CONSTRAINT [DF_Auth.Permissions_PermissionTypeId] DEFAULT ((0)) NOT NULL,
    [FranchiseId]      INT              NULL,
    [RoleId]           UNIQUEIDENTIFIER NULL,
    [CreatedOnUtc]     DATETIME         CONSTRAINT [DF_Permissions_UTCCreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [PersonId]         INT              NULL,
    [CanAccess]        BIT              NULL,
    CONSTRAINT [PK_Auth.Permissions] PRIMARY KEY CLUSTERED ([PermissionId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Permissions_Person] FOREIGN KEY ([PersonId]) REFERENCES [CRM].[Person] ([PersonId]),
    CONSTRAINT [FK_Permissions_Roles] FOREIGN KEY ([RoleId]) REFERENCES [Auth].[Roles] ([RoleId]),
    CONSTRAINT [FK_Permissions_Type_Permission] FOREIGN KEY ([PermissionTypeId]) REFERENCES [Auth].[Type_Permission] ([PermissionTypeId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Null FranchiseId will apply to all franchise (base defaults)', @level0type = N'SCHEMA', @level0name = N'Auth', @level1type = N'TABLE', @level1name = N'Permissions', @level2type = N'COLUMN', @level2name = N'FranchiseId';

