﻿CREATE TABLE [Auth].[Type_Permission] (
    [PermissionTypeId]    INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Name]                VARCHAR (50)  NULL,
    [Subsection]          VARCHAR (50)  NULL,
    [Description]         VARCHAR (256) NULL,
    [IsSpecialPermission] BIT           CONSTRAINT [DF_Type_Permission_IsSpecialPermission] DEFAULT ((0)) NOT NULL,
    [GroupName]           VARCHAR (100) NULL,
    CONSTRAINT [PK_Type_Permission] PRIMARY KEY CLUSTERED ([PermissionTypeId] ASC) WITH (FILLFACTOR = 90)
);

