﻿CREATE TABLE [Auth].[Permission_Role] (
    [Id]               SMALLINT         IDENTITY (1, 1) NOT NULL,
    [PermissionTypeId] INT              NULL,
    [RoleId]           UNIQUEIDENTIFIER NOT NULL,
    [TypeModuleId]     SMALLINT         NULL,
    [IsAccessible]     BIT              NULL,
    CONSTRAINT [PK_Permission_Role] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Permission_Role_PermissionTypeId] FOREIGN KEY ([PermissionTypeId]) REFERENCES [Auth].[Type_PermissionTP] ([Id]),
    CONSTRAINT [FK_Permission_Role_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [Auth].[Roles] ([RoleId]),
    CONSTRAINT [FK_Permission_Role_TypeModuleId] FOREIGN KEY ([TypeModuleId]) REFERENCES [Auth].[Type_Module] ([Id])
);

