﻿CREATE TABLE [Auth].[OAuthUsers] (
    [ProviderLinkId]        INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Provider]              VARCHAR (30)     NOT NULL,
    [ProviderUserId]        VARCHAR (100)    NOT NULL,
    [UserId]                UNIQUEIDENTIFIER NOT NULL,
    [CreatedOnUtc]          DATETIME         CONSTRAINT [DF_OAuthUsers_CreatedOnUtc] DEFAULT (getutcdate()) NULL,
    [IsActive]              BIT              CONSTRAINT [DF_OAuthUsers_IsActive] DEFAULT ((1)) NOT NULL,
    [AccessToken]           NVARCHAR (MAX)   NULL,
    [TokenLastUpdatedOnUtc] DATETIME         NULL,
    [TokenExpiresIn]        SMALLINT         NULL,
    [RefreshToken]          NVARCHAR (MAX)   NULL,
    [SyncStartsOnUtc]       DATETIME         NULL,
    CONSTRAINT [PK_OAuthUsers] PRIMARY KEY CLUSTERED ([Provider] ASC, [ProviderUserId] ASC, [UserId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_OAuthUsers_Users] FOREIGN KEY ([UserId]) REFERENCES [Auth].[Users] ([UserId])
);

