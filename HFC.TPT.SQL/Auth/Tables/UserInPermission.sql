﻿CREATE TABLE [Auth].[UserInPermission] (
    [UserId]          UNIQUEIDENTIFIER NOT NULL,
    [PermissionSetId] INT              NULL,
    [CreatedOn]       DATETIME2 (7)    NOT NULL,
    [CreatedBy]       INT              NOT NULL,
    [LastUpdatedOn]   DATETIME2 (7)    NULL,
    [LastUpdatedBy]   INT              NULL,
    CONSTRAINT [FK_UserInPermission_PermissionSet] FOREIGN KEY ([PermissionSetId]) REFERENCES [Auth].[PermissionSet] ([Id]),
    CONSTRAINT [FK_UserInPermission_UserId] FOREIGN KEY ([UserId]) REFERENCES [Auth].[Users] ([UserId])
);

