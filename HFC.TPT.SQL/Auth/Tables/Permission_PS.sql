﻿CREATE TABLE [Auth].[Permission_PS] (
    [Id]               INT      IDENTITY (1, 1) NOT NULL,
    [PermissionTypeId] INT      NULL,
    [TypeModuleId]     SMALLINT NULL,
    [PermissionSetId]  INT      NULL,
    [IsAccessible]     BIT      NULL,
    CONSTRAINT [PK_Permission_PS] PRIMARY KEY CLUSTERED ([Id] ASC)
);

