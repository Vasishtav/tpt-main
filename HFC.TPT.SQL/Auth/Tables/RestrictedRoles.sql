﻿CREATE TABLE [Auth].[RestrictedRoles] (
    [RestrictedRolesID] INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [RoleId]            UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_RestrictedRoles] PRIMARY KEY CLUSTERED ([RestrictedRolesID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [fk_RestrictedRoles] FOREIGN KEY ([RoleId]) REFERENCES [Auth].[Roles] ([RoleId])
);

