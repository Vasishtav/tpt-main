﻿CREATE TABLE [Auth].[Type_PermissionTP] (
    [Id]                  INT           IDENTITY (1, 1) NOT NULL,
    [Name]                VARCHAR (50)  NOT NULL,
    [PermissionCode]      VARCHAR (100) NULL,
    [Description]         VARCHAR (500) NOT NULL,
    [IsSpecialPermission] BIT           NULL,
    [TypeModuleId]        INT           NULL,
    CONSTRAINT [PK_Type_PermissionTP] PRIMARY KEY CLUSTERED ([Id] ASC)
);

