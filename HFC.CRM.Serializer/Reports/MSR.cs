﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Validation;
using DocumentFormat.OpenXml.Wordprocessing;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using A = DocumentFormat.OpenXml.Drawing;
using DW = DocumentFormat.OpenXml.Drawing.Wordprocessing;
using PIC = DocumentFormat.OpenXml.Drawing.Pictures;
using HFC.CRM.Managers.DTO;

namespace HFC.CRM.Serializer.Reports
{
    using HFC.CRM.Core;

    /// <summary>
    /// Monthly Sales Report Serializer
    /// </summary>
    public class MSR
    {
        #region
        ////OBSOLETE METHOD - THIS USES Interop.Word to launch Word to process documents but the WINWORD process doesn't close properly
        ///// <summary>
        ///// Serializes the specified franchise.
        ///// </summary>
        ///// <param name="franchise">The franchise.</param>
        ///// <param name="reportStartDate">The report start date.</param>
        ///// <param name="bodyHtml">The body HTML.</param>
        ///// <param name="destinationPath">The destination path.</param>
        ///// <param name="exportType">Type of the export.</param>
        //public static void Serialize(Franchise franchise, DateTime reportStartDate, string bodyHtml, ref string destinationPath, ExportTypeEnum exportType = ExportTypeEnum.PDF)
        //{
        //    var bodyPath = HostingEnvironment.MapPath("/Templates/Base/HtmlTemplate.mht");

        //    string tempPath = HostingEnvironment.MapPath("/Templates/Temp/");
        //    if (!Directory.Exists(tempPath))
        //    {
        //        Directory.CreateDirectory(tempPath);
        //    }
        //    WdSaveFormat saveFormat = WdSaveFormat.wdFormatPDF;
        //    string extension = ".pdf";
        //    switch(exportType)
        //    {
        //        case ExportTypeEnum.Word: extension = ".docx";
        //            saveFormat = WdSaveFormat.wdFormatDocumentDefault;
        //            break;
        //        default: break;
        //    }

        //    if (string.IsNullOrEmpty(destinationPath))
        //        destinationPath = Path.Combine(tempPath, string.Format("{0}_{1}_MSR{2}", franchise.Code, reportStartDate.ToString("MMMyyyy"), extension));


        //    var template_bodyhtml = System.IO.File.ReadAllText(bodyPath);

        //    string base64Image = Convert.ToBase64String(System.IO.File.ReadAllBytes(HostingEnvironment.MapPath("/Images/conceptlogo.png")), Base64FormattingOptions.InsertLineBreaks);

        //    template_bodyhtml = template_bodyhtml.Replace("@Model.ImageBase64", base64Image);

        //    template_bodyhtml = template_bodyhtml.Replace("@Model.FranchiseName", franchise.Name);

        //    List<string> footerInfo = new List<string>();
        //    if (franchise.Address != null && !string.IsNullOrEmpty(franchise.Address.ToString()))
        //        footerInfo.Add(franchise.Address.ToString());
        //    if (!string.IsNullOrEmpty(franchise.TollFreeNumber))
        //        footerInfo.Add(string.Format("P: {0}", RegexUtil.FormatUSPhone(franchise.TollFreeNumber)));
        //    if (!string.IsNullOrEmpty(franchise.FaxNumber))
        //        footerInfo.Add(string.Format("F: {0}", RegexUtil.FormatUSPhone(franchise.FaxNumber)));

        //    template_bodyhtml = template_bodyhtml.Replace("@Model.Footer", footerInfo.Count > 0 ? Util.EncodeQuotedPrintable(string.Join(" - ", footerInfo)) : "");

        //    if (!string.IsNullOrEmpty(franchise.Website))
        //    {
        //        string anchor = string.Format("<a href='http://{0}'>{0}</a>", franchise.Website);
        //        template_bodyhtml = template_bodyhtml.Replace("@Model.Website", Util.EncodeQuotedPrintable(anchor));
        //    }
        //    else
        //        template_bodyhtml = template_bodyhtml.Replace("@Model.Website", "");

        //    template_bodyhtml = template_bodyhtml.Replace("@Model.Body", Util.EncodeQuotedPrintable(bodyHtml));

        //    var msrTemplatePath = Path.Combine(tempPath, string.Format("{0}.mht", Guid.NewGuid()));
        //    System.IO.File.WriteAllText(msrTemplatePath, template_bodyhtml);

        //    Microsoft.Office.Interop.Word.Application winword = new Microsoft.Office.Interop.Word.Application();
        //    winword.Visible = true;

        //    Microsoft.Office.Interop.Word.Documents docs = winword.Documents;
        //    Microsoft.Office.Interop.Word.Document doc = null;
        //    object m = Missing.Value;
        //    try
        //    {
        //        FileInfo destFile = new FileInfo(destinationPath);
        //        if (!destFile.Directory.Exists)
        //            Directory.CreateDirectory(destFile.DirectoryName);

        //        object readOnly = true;
        //        object isVisible = false;

        //        //This is a temporary solution, there is no easy method to handle all this in memory so we will save to directory and read it back.
        //        doc = docs.Open(msrTemplatePath, ref m, ref readOnly, ref m,
        //            ref m, ref m, ref m, ref m, ref m, ref m, ref m, ref isVisible,
        //            ref m, ref m, ref m, ref m);

        //        doc.SaveAs2(destinationPath, saveFormat, ref m, ref m, ref m, ref m, ref m,
        //            ref m, ref m, ref m, ref m, ref m, ref m, ref m, ref m, ref m, ref m);                

        //        doc.Close(ref m, ref m, ref m);
        //    }
        //    catch (Exception ex) { EventLogger.LogEvent(ex); }
        //    finally
        //    {
        //        // Release all Interop objects.
        //        if (doc != null)
        //            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(doc);
        //        if(docs != null)
        //            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(docs);

        //        winword.Quit(ref m, ref m, ref m);
        //        if (winword != null)
        //            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(winword);

        //        doc = null;
        //        docs = null;
        //        winword = null;
        //        GC.Collect();
        //    }

        //    if(File.Exists(msrTemplatePath))
        //        File.Delete(msrTemplatePath);

        //}

        ///// <summary>
        ///// Serializes the specified franchise.
        ///// </summary>
        ///// <param name="franchise">The franchise.</param>
        ///// <param name="reportStartDate">The report start date.</param>
        ///// <param name="bodyHtml">The body HTML.</param>
        ///// <param name="exportType">Type of the export.</param>
        ///// <returns>System.Byte[].</returns>
        //public static byte[] Serialize(Franchise franchise, DateTime reportStartDate, string bodyHtml, ExportTypeEnum exportType = ExportTypeEnum.PDF)
        //{
        //    string destinationPath = null;
        //    Serialize(franchise, reportStartDate, bodyHtml, ref destinationPath, exportType);
        //    if (File.Exists(destinationPath))
        //    {
        //        var data = System.IO.File.ReadAllBytes(destinationPath);

        //        File.Delete(destinationPath);
        //        return data;
        //    }
        //    else
        //        return null;
        //}
        #endregion
        /// <summary>
        /// Creates an MSR word or PDF document that has Key Acccount Section
        /// </summary>
        /// <param name="franchise">Franchise to run report for</param>
        /// <param name="territory">Territory for this MSR</param>
        /// <param name="reportStartDate">Report Date</param>        
        /// <param name="grossSalesData">Sales data</param>
        /// <param name="royalties">Royalty data</param>
        /// <param name="type">Export format</param>        
        public static MemoryStream Serialize(Franchise franchise, Territory territory, DateTime reportStartDate, DateTime gStartDT, List<MSRData> grossSalesData, List<FranchiseRoyalty> royalties, 
            List<KeyAccountDisp> keyaccount, ExportTypeEnum type = ExportTypeEnum.PDF)
        {
            string srcFilePath = HostingEnvironment.MapPath(string.Format("/Templates/Base{0}/MSRTemplate.docx", AppConfigManager.BrandedPath.Replace(@"\", @"/")));
            MemoryStream destination = new MemoryStream();

            using (FileStream fs = File.OpenRead(srcFilePath))
            {
                fs.CopyTo(destination);
            }

            int _month = gStartDT.Month + 1;
            int _year = gStartDT.Year;

            using (WordprocessingDocument document = WordprocessingDocument.Open(destination, true))
            {
                document.ChangeDocumentType(WordprocessingDocumentType.Document);

                MainDocumentPart mainPart = document.MainDocumentPart;

                mainPart.FillHeader(franchise);
                mainPart.FillFooter(franchise);

                List<SdtBlock> sdtBlockList = mainPart.Document.Descendants<SdtBlock>().ToList();
                List<SdtRun> sdtRunList = mainPart.Document.Descendants<SdtRun>().ToList();
                for (int i = 0; i < sdtBlockList.Count; i++)
                {
                    var run = sdtBlockList[i];
                    var prop = run.SdtProperties.GetFirstChild<SdtAlias>().Val.Value;
                    if (prop == "H1Title")
                        OpenXmlUtil.FillContentControl(run, string.Format("{0} - {1}", territory.Name, territory.Code));
                    else if (prop == "GrossSalesTitle")
                        OpenXmlUtil.FillContentControl(run, string.Format("Gross Sales for {0:MMMM yyyy}", gStartDT));
                    else if (prop == "RoyaltyTitle")
                        OpenXmlUtil.FillContentControl(run, string.Format("Royalties Due for {0:MMMM yyyy}", reportStartDate));
                    else if (prop == "KeyAccountTitle")
                        OpenXmlUtil.FillContentControl(run, string.Format("Key Accounts: {0:MMMM} Sales Due By : {1}/15/{2}", gStartDT, _month, _year));
                    else if (prop == "EmptyTitle")
                        OpenXmlUtil.FillContentControl(run, "");

                }

                var cellMargin = new TableCellMargin
                {
                    BottomMargin = new BottomMargin { Width = "144", Type = TableWidthUnitValues.Dxa },
                    TopMargin = new TopMargin { Width = "144", Type = TableWidthUnitValues.Dxa },
                    LeftMargin = new LeftMargin { Width = "144", Type = TableWidthUnitValues.Dxa },
                    RightMargin = new RightMargin { Width = "144", Type = TableWidthUnitValues.Dxa }
                };

                Table grossSalesTable = null;
                Table keyaccountTable = null;
                Table royaltyTable = null;

                foreach (var table in mainPart.Document.Descendants<Table>())
                {
                    var prop = table.Descendants<TableProperties>().FirstOrDefault();
                    if (prop != null)
                    {
                        var cap = prop.Descendants<TableCaption>().FirstOrDefault();

                        if (cap != null && cap.Val == "tblGross")
                        {
                            grossSalesTable = table;
                        }

                        if (cap != null && cap.Val == "tblRoyalty")
                        {
                            royaltyTable = table;
                        }

                        if (cap != null && cap.Val == "tblKeyAcc")
                        {
                            keyaccountTable = table;
                        }
                    }
                }

                #region Gross Sales Table
                // var grossSalesTable = mainPart.Document.Descendants<Table>().FirstOrDefault();
                if (grossSalesTable != null)
                {
                    if (grossSalesData != null && grossSalesData.Count > 0)
                    {
                        var SalesData = grossSalesData.Where(w => w.Category_Type != "DiscountTotal" && w.Category_Type != "SurchargeTotal").ToList();

                        decimal surchargeTotal = 0, discountTotal = 0;
                        var dis = grossSalesData.FirstOrDefault(w => w.Category_Type == "DiscountTotal");
                        var sur = grossSalesData.FirstOrDefault(w => w.Category_Type == "SurchargeTotal");
                        if (dis != null)
                            discountTotal = dis.Sum ?? 0;
                        if (sur != null)
                            surchargeTotal = sur.Sum ?? 0;

                        foreach (var data in SalesData)
                        {
                            grossSalesTable.Append(new TableRow(
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Left }
                                        ),
                                        new Run(
                                            new Text(data.Category_Type)
                                        )
                                    )
                                ),
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text(string.Format("{0:C}", data.Cost))
                                        )
                                    )
                                ),
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text(string.Format("{0:C}", data.Sales))
                                        )
                                    )
                                )
                            ));
                        }
                        grossSalesTable.Append(new TableRow(
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text("Total:")
                                        )
                                    )
                                ),
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text(string.Format("{0:C}", SalesData.Sum(s => s.Cost)))
                                        )
                                    )
                                ),
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text(string.Format("{0:C}", SalesData.Sum(s => s.Sales)))
                                        )
                                    )
                                )
                            )
                        );
                        grossSalesTable.Append(new TableRow(
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Left }
                                        ),
                                        new Run(
                                            new Text("Surcharges")
                                        )
                                    )
                                ),
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text("")
                                        )
                                    )
                                ),
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text(string.Format("{0:C}", surchargeTotal))
                                        )
                                    )
                                )
                            )
                        );
                        grossSalesTable.Append(new TableRow(
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Left }
                                        ),
                                        new Run(
                                            new Text("Discounts")
                                        )
                                    )
                                ),
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text("")
                                        )
                                    )
                                ),
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text(string.Format("{0:C}", discountTotal))
                                        )
                                    )
                                )
                            )
                        );
                        grossSalesTable.Append(new TableRow(
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text("Adjustment total:")
                                        )
                                    )
                                ),
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text("")
                                        )
                                    )
                                ),
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text(string.Format("{0:C}", surchargeTotal - discountTotal))
                                        )
                                    )
                                )
                            )
                        );
                        grossSalesTable.Append(new TableRow(
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text("Total Gross Sales:")
                                        )
                                    )
                                ),
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text("")
                                        )
                                    )
                                ),
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text(string.Format("{0:C}", SalesData.Sum(s => s.Sales) + surchargeTotal - discountTotal))
                                        )
                                    )
                                )
                            )
                        );
                    }
                    else
                    {
                        grossSalesTable.Append(
                            new TableRow(
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellWidth = new TableCellWidth { Width = "0", Type = TableWidthUnitValues.Auto },
                                        GridSpan = new GridSpan { Val = 3 },
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Center }
                                        ),
                                        new Run(
                                            new Text("No Data")
                                        )
                                    )
                                )
                            )
                        );
                    }
                }
                #endregion

                #region Royalties Table
                //var royaltyTable = mainPart.Document.Descendants<Table>().LastOrDefault();
                if (royaltyTable != null)
                {
                    if (royalties != null && royalties.Count > 0)
                    {
                        foreach (var data in royalties)
                        {
                            royaltyTable.Append(new TableRow(
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Left }
                                        ),
                                        new Run(
                                            new Text("")
                                        )
                                    )
                                ),
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text(data.Name)
                                        )
                                    )
                                ),
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text(string.Format("{0:C}", data.Amount))
                                        )
                                    )
                                )
                            ));
                        }
                    }
                    else
                    {
                        royaltyTable.Append(
                            new TableRow(
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellWidth = new TableCellWidth { Width = "0", Type = TableWidthUnitValues.Auto },
                                        GridSpan = new GridSpan { Val = 3 },
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId { Val = "NoSpacing" },
                                            new Justification { Val = JustificationValues.Center }
                                        ),
                                        new Run(
                                            new Text("No Data")
                                        )
                                    )
                                )
                            )
                        );
                    }
                }
                #endregion

                #region KeyAccount Table
                //  var keyaccountTable = mainPart.Document.Descendants<Table>().LastOrDefault();
                if (keyaccountTable != null)
                {
                    if (keyaccount != null && keyaccount.Count > 0)
                    {
                        foreach (var data in keyaccount)
                        {
                            keyaccountTable.Append(

                                new TableRow(
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Left }
                                        ),
                                        new Run(
                                            new Text(data.KeyAccountName)
                                        )
                                    )
                                ),
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text(string.Format("{0:C}", data.GrandTotal)
                                        )
                                    )
                                )),
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text(string.Format("{0:C}", data.ApptTotal))
                                        )
                                    )
                                )
                                ,
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text(string.Format("{0:C}", data.SalesTotal))
                                        )
                                    )
                                )

                            ));
                        }
                    }
                    else
                    {
                        keyaccountTable.Append(
                            new TableRow(
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellWidth = new TableCellWidth { Width = "0", Type = TableWidthUnitValues.Auto },
                                        GridSpan = new GridSpan { Val = 4 },
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId { Val = "NoSpacing" },
                                            new Justification { Val = JustificationValues.Center }
                                        ),
                                        new Run(
                                            new Text("No Data")
                                        )
                                    )
                                )
                            )
                        );
                    }
                }
                #endregion

                #region KACheckTable Table

                if (keyaccountTable != null)
                {
                   

                    var counter=0;

                    if (keyaccount != null && keyaccount.Count > 0)
                    {
                        foreach (var data in keyaccount)
                        {
                            counter++;

                            Table KACheckTable = new Table();

                            TableProperties tblProp = new TableProperties(
                                new TableBorders(
                                    new TopBorder() { Val = new EnumValue<BorderValues>(BorderValues.BasicBlackDashes), Size = 4 },
                                    new LeftBorder() { Val = new EnumValue<BorderValues>(BorderValues.BasicBlackDashes), Size = 4 },
                                    new RightBorder() { Val = new EnumValue<BorderValues>(BorderValues.BasicBlackDashes), Size = 4 },
                                    new BottomBorder() { Val = new EnumValue<BorderValues>(BorderValues.BasicBlackDashes), Size = 4 }
                                )
                            );

                            KACheckTable.AppendChild<TableProperties>(tblProp);

                            KACheckTable.Append(new TableRow(
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellWidth = new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "3200" }

                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Left }
                                        ),
                                        new Run(
                                            new Text(data.KeyAccountName)
                                        )
                                    )
                                ),
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellWidth = new TableCellWidth { Width = "5000", Type = TableWidthUnitValues.Dxa },
                                        GridSpan = new GridSpan { Val = 2 },
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Left }
                                        ),
                                        new Run(
                                            new Text(string.Format("Amount due by " + _month.ToString() + "/15/" + _year.ToString() + "  ") +
                                                    string.Format("{0:C}", data.TotalAmount)
                                        )
                                    )
                                )
                                )
                            )
                            );
                            KACheckTable.Append(new TableRow(
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Left }
                                        ),
                                        new Run(
                                            new Text("")
                                        )
                                    )
                                ),
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellWidth = new TableCellWidth { Width = "5000", Type = TableWidthUnitValues.Dxa },
                                        GridSpan = new GridSpan { Val = 2 },
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Left }
                                        ),
                                        new Run(
                                            new Text(string.Format("Amount enclosed:              ---------------")
                                        )
                                    )
                                )
                                )
                            ));
                            KACheckTable.Append(new TableRow(
                                new TableCell(
                                    new TableCellProperties
                                    {
                                         TableCellWidth = new TableCellWidth { Width = "0", Type = TableWidthUnitValues.Auto },
                                         GridSpan = new GridSpan { Val = 3 },
                                         TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                        
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Left }
                                        ),
                                        new Run(
                                            new Text("Territory Number:" + data.TerrCode)
                                        )
                                    )
                                )
                            ));
                            KACheckTable.Append(new TableRow(
                               new TableCell(
                                   new TableCellProperties
                                   {
                                       TableCellWidth = new TableCellWidth { Width = "0", Type = TableWidthUnitValues.Auto },
                                       GridSpan = new GridSpan { Val = 3 },
                                       TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                   },
                                   new Paragraph(
                                       new ParagraphProperties(
                                           new ParagraphStyleId() { Val = "NoSpacing" },
                                           new Justification() { Val = JustificationValues.Left }
                                       ),
                                       new Run(
                                           new Text("Budget Blinds of " + data.TerrName)
                                       )
                                   )
                               )
                           ));
                            KACheckTable.Append(new TableRow(
                              new TableCell(
                                  new TableCellProperties
                                  {
                                       TableCellWidth = new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "3200" },
                                       TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                  },
                                  new Paragraph(
                                      new ParagraphProperties(
                                          new ParagraphStyleId() { Val = "NoSpacing" },
                                          new Justification() { Val = JustificationValues.Left }
                                      ),
                                      new Run(
                                          new Text("")
                                      )
                                  )
                              ),
                              new TableCell(
                                  new TableCellProperties
                                  {
                                      TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                  },
                                  new Paragraph(
                                      new ParagraphProperties(
                                          new ParagraphStyleId() { Val = "NoSpacing" },
                                          new Justification() { Val = JustificationValues.Right }
                                      ),
                                      new Run(
                                          new Text("")
                                      )
                                  )
                              ),
                              new TableCell(
                                  new TableCellProperties
                                  {
                                      TableCellWidth = new TableCellWidth { Width = "0", Type = TableWidthUnitValues.Auto },
                                      TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                  },
                                  new Paragraph(
                                      new ParagraphProperties(
                                          new ParagraphStyleId() { Val = "NoSpacing" },
                                          new Justification() { Val = JustificationValues.Left }
                                      ),
                                      new Run(
                                          new Text("To:" + data.PaymentName)
                                      )
                                  )
                              )
                          ));
                            KACheckTable.Append(new TableRow(
                             new TableCell(
                                 new TableCellProperties
                                 {
                                     TableCellWidth = new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "3200" },
                                     TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                 },
                                 new Paragraph(
                                     new ParagraphProperties(
                                         new ParagraphStyleId() { Val = "NoSpacing" },
                                         new Justification() { Val = JustificationValues.Left }
                                     ),
                                     new Run(
                                         new Text("")
                                     )
                                 )
                             ),
                             new TableCell(
                                 new TableCellProperties
                                 {
                                     TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                 },
                                 new Paragraph(
                                     new ParagraphProperties(
                                         new ParagraphStyleId() { Val = "NoSpacing" },
                                         new Justification() { Val = JustificationValues.Right }
                                     ),
                                     new Run(
                                         new Text("")
                                     )
                                 )
                             ),
                             new TableCell(
                                 new TableCellProperties
                                 {
                                     TableCellWidth = new TableCellWidth { Width = "0", Type = TableWidthUnitValues.Auto },
                                     TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                 },
                                 new Paragraph(
                                     new ParagraphProperties(
                                         new ParagraphStyleId() { Val = "NoSpacing" },
                                         new Justification() { Val = JustificationValues.Left }
                                     ),
                                     new Run(
                                         new Text("  " + data.PaymentAddr)
                                     )
                                 )
                             )
                         ));
                            KACheckTable.Append(new TableRow(
                             new TableCell(
                                 new TableCellProperties
                                 {
                                     TableCellWidth = new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "3200" },
                                     TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                 },
                                 new Paragraph(
                                     new ParagraphProperties(
                                         new ParagraphStyleId() { Val = "NoSpacing" },
                                         new Justification() { Val = JustificationValues.Left }
                                     ),
                                     new Run(
                                         new Text("")
                                     )
                                 )
                             ),
                             new TableCell(
                                 new TableCellProperties
                                 {
                                     TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                 },
                                 new Paragraph(
                                     new ParagraphProperties(
                                         new ParagraphStyleId() { Val = "NoSpacing" },
                                         new Justification() { Val = JustificationValues.Right }
                                     ),
                                     new Run(
                                         new Text("")
                                     )
                                 )
                             ),
                             new TableCell(
                                 new TableCellProperties
                                 {
                                     TableCellWidth = new TableCellWidth { Width = "0", Type = TableWidthUnitValues.Auto },
                                     TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                 },
                                 new Paragraph(
                                     new ParagraphProperties(
                                         new ParagraphStyleId() { Val = "NoSpacing" },
                                         new Justification() { Val = JustificationValues.Left }
                                     ),
                                     new Run(
                                         new Text("   " + data.PaymentZip)
                                     )
                                 )
                             )
                         ));
                            KACheckTable.Append(new TableRow(
                             new TableCell(
                                 new TableCellProperties
                                 {
                                     TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                 },
                                 new Paragraph(
                                     new ParagraphProperties(
                                         new ParagraphStyleId() { Val = "NoSpacing" },
                                         new Justification() { Val = JustificationValues.Left }
                                     ),
                                     new Run(
                                         new Text(data.CustName)
                                     )
                                 )
                             ),
                             new TableCell(
                                 new TableCellProperties
                                 {
                                     TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                 },
                                 new Paragraph(
                                     new ParagraphProperties(
                                         new ParagraphStyleId() { Val = "NoSpacing" },
                                         new Justification() { Val = JustificationValues.Left }
                                     ),
                                     new Run(
                                         new Text(string.Format("{0:C}", data.TotalAmount))
                                     )
                                 )
                             ),
                             new TableCell(
                                 new TableCellProperties
                                 {
                                     TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                 },
                                 new Paragraph(
                                     new ParagraphProperties(
                                         new ParagraphStyleId() { Val = "NoSpacing" },
                                         new Justification() { Val = JustificationValues.Right }
                                     ),
                                     new Run(
                                         new Text("")
                                     )
                                 )
                             )
                         ));

                            mainPart.Document.Body.Append(KACheckTable);

                            Paragraph para = mainPart.Document.Body.AppendChild(new Paragraph());
                            Run run = para.AppendChild(new Run());
                            run.AppendChild(new Text(" "));

                            if (IsEven(counter) && counter != keyaccount.Count)
                            {
                                Paragraph paragraph232 = new Paragraph();
                                ParagraphProperties paragraphProperties220 = new ParagraphProperties();
                                SectionProperties sectionProperties1 = new SectionProperties();
                                SectionType sectionType1 = new SectionType() { Val = SectionMarkValues.NextPage };
                                sectionProperties1.Append(sectionType1);
                                paragraphProperties220.Append(sectionProperties1);
                                paragraph232.Append(paragraphProperties220);

                                mainPart.Document.Body.AppendChild(paragraph232);
                            }
                        }
                    }
                }

                #endregion

                mainPart.Document.Save();

#if DEBUG
                var validator = new OpenXmlValidator();
                var errors = validator.Validate(document);
#endif

                document.Close();
            }

            var outputStream = new MemoryStream();
            if (type == ExportTypeEnum.PDF)
            {
                var license = new Aspose.Words.License();
                license.SetLicense("Aspose.Words.lic");

                var doc = new Aspose.Words.Document(destination);
                doc.Save(outputStream, Aspose.Words.SaveFormat.Pdf);
            }
            else
                outputStream = destination;

            return outputStream;
        }
        /// <summary>
        ///  Creates an MSR word or PDF document that does not have Key Acccount Section
        /// </summary>
        /// <param name="rowNum"></param>
        /// <returns></returns>
        public static MemoryStream Serialize(Franchise franchise, Territory territory, DateTime reportStartDate, DateTime gStartDT, List<MSRData> grossSalesData, List<FranchiseRoyalty> royalties, ExportTypeEnum type = ExportTypeEnum.PDF)
        {
            string srcFilePath = HostingEnvironment.MapPath(string.Format("/Templates/Base{0}/MSRTemplate.docx", AppConfigManager.BrandedPath.Replace(@"\", @"/")));
            MemoryStream destination = new MemoryStream();

            using (FileStream fs = File.OpenRead(srcFilePath))
            {
                fs.CopyTo(destination);
            }

            using (WordprocessingDocument document = WordprocessingDocument.Open(destination, true))
            {
                document.ChangeDocumentType(WordprocessingDocumentType.Document);

                MainDocumentPart mainPart = document.MainDocumentPart;

                mainPart.FillHeader(franchise);
                mainPart.FillFooter(franchise);

                List<SdtBlock> sdtRunList = mainPart.Document.Descendants<SdtBlock>().ToList();
                for (int i = 0; i < sdtRunList.Count; i++)
                {
                    var run = sdtRunList[i];
                    var prop = run.SdtProperties.GetFirstChild<SdtAlias>().Val.Value;
                    if (prop == "H1Title")
                        OpenXmlUtil.FillContentControl(run, string.Format("{0} - {1}", territory.Name, territory.Code));
                    else if (prop == "GrossSalesTitle")
                        OpenXmlUtil.FillContentControl(run, string.Format("Gross Sales for {0:MMMM yyyy}", gStartDT));
                    else if (prop == "RoyaltyTitle")
                        OpenXmlUtil.FillContentControl(run, string.Format("Royalties Due for {0:MMMM yyyy}", reportStartDate));
                }

                var cellMargin = new TableCellMargin
                {
                    BottomMargin = new BottomMargin { Width = "144", Type = TableWidthUnitValues.Dxa },
                    TopMargin = new TopMargin { Width = "144", Type = TableWidthUnitValues.Dxa },
                    LeftMargin = new LeftMargin { Width = "144", Type = TableWidthUnitValues.Dxa },
                    RightMargin = new RightMargin { Width = "144", Type = TableWidthUnitValues.Dxa }
                };

                #region Gross Sales Table
                var grossSalesTable = mainPart.Document.Descendants<Table>().FirstOrDefault();
                if (grossSalesTable != null)
                {
                    if (grossSalesData != null && grossSalesData.Count > 0)
                    {
                        var SalesData = grossSalesData.Where(w => w.Category_Type != "DiscountTotal" && w.Category_Type != "SurchargeTotal").ToList();

                        decimal surchargeTotal = 0, discountTotal = 0;
                        var dis = grossSalesData.FirstOrDefault(w => w.Category_Type == "DiscountTotal");
                        var sur = grossSalesData.FirstOrDefault(w => w.Category_Type == "SurchargeTotal");
                        if (dis != null)
                            discountTotal = dis.Sum ?? 0;
                        if (sur != null)
                            surchargeTotal = sur.Sum ?? 0;

                        foreach (var data in SalesData)
                        {
                            grossSalesTable.Append(new TableRow(
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Left }
                                        ),
                                        new Run(
                                            new Text(data.Category_Type)
                                        )
                                    )
                                ),
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text(string.Format("{0:C}", data.Cost))
                                        )
                                    )
                                ),
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text(string.Format("{0:C}", data.Sales))
                                        )
                                    )
                                )
                            ));
                        }
                        grossSalesTable.Append(new TableRow(
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text("Total:")
                                        )
                                    )
                                ),
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text(string.Format("{0:C}", SalesData.Sum(s => s.Cost)))
                                        )
                                    )
                                ),
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text(string.Format("{0:C}", SalesData.Sum(s => s.Sales)))
                                        )
                                    )
                                )
                            )
                        );
                        grossSalesTable.Append(new TableRow(
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Left }
                                        ),
                                        new Run(
                                            new Text("Surcharges")
                                        )
                                    )
                                ),
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text("")
                                        )
                                    )
                                ),
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text(string.Format("{0:C}", surchargeTotal))
                                        )
                                    )
                                )
                            )
                        );
                        grossSalesTable.Append(new TableRow(
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Left }
                                        ),
                                        new Run(
                                            new Text("Discounts")
                                        )
                                    )
                                ),
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text("")
                                        )
                                    )
                                ),
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text(string.Format("{0:C}", discountTotal))
                                        )
                                    )
                                )
                            )
                        );
                        grossSalesTable.Append(new TableRow(
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text("Adjustment total:")
                                        )
                                    )
                                ),
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text("")
                                        )
                                    )
                                ),
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text(string.Format("{0:C}", surchargeTotal - discountTotal))
                                        )
                                    )
                                )
                            )
                        );
                        grossSalesTable.Append(new TableRow(
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text("Total Gross Sales:")
                                        )
                                    )
                                ),
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text("")
                                        )
                                    )
                                ),
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text(string.Format("{0:C}", SalesData.Sum(s => s.Sales) + surchargeTotal - discountTotal))
                                        )
                                    )
                                )
                            )
                        );
                    }
                    else
                    {
                        grossSalesTable.Append(
                            new TableRow(
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellWidth = new TableCellWidth { Width = "0", Type = TableWidthUnitValues.Auto },
                                        GridSpan = new GridSpan { Val = 3 },
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Center }
                                        ),
                                        new Run(
                                            new Text("No Data")
                                        )
                                    )
                                )
                            )
                        );
                    }
                }
                #endregion

                #region Royalties Table
                var royaltyTable = mainPart.Document.Descendants<Table>().LastOrDefault();
                if (royaltyTable != null)
                {
                    if (royalties != null && royalties.Count > 0)
                    {
                        foreach (var data in royalties)
                        {
                            royaltyTable.Append(new TableRow(
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Left }
                                        ),
                                        new Run(
                                            new Text("")
                                        )
                                    )
                                ),
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text(data.Name)
                                        )
                                    )
                                ),
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId() { Val = "NoSpacing" },
                                            new Justification() { Val = JustificationValues.Right }
                                        ),
                                        new Run(
                                            new Text(string.Format("{0:C}", data.Amount))
                                        )
                                    )
                                )
                            ));
                        }
                    }
                    else
                    {
                        royaltyTable.Append(
                            new TableRow(
                                new TableCell(
                                    new TableCellProperties
                                    {
                                        TableCellWidth = new TableCellWidth { Width = "0", Type = TableWidthUnitValues.Auto },
                                        GridSpan = new GridSpan { Val = 3 },
                                        TableCellMargin = (TableCellMargin)cellMargin.Clone()
                                    },
                                    new Paragraph(
                                        new ParagraphProperties(
                                            new ParagraphStyleId { Val = "NoSpacing" },
                                            new Justification { Val = JustificationValues.Center }
                                        ),
                                        new Run(
                                            new Text("No Data")
                                        )
                                    )
                                )
                            )
                        );
                    }
                }
                #endregion

                mainPart.Document.Save();

#if DEBUG
                var validator = new OpenXmlValidator();
                var errors = validator.Validate(document);
#endif

                document.Close();
            }

            var outputStream = new MemoryStream();
            if (type == ExportTypeEnum.PDF)
            {
                var license = new Aspose.Words.License();
                license.SetLicense("Aspose.Words.lic");

                var doc = new Aspose.Words.Document(destination);
                doc.Save(outputStream, Aspose.Words.SaveFormat.Pdf);
            }
            else
                outputStream = destination;

            return outputStream;
        }



        public static bool IsEven(int rowNum)
        {
            if (rowNum % 2 == 0)
                return true;
            return false;
        }
    }
}
