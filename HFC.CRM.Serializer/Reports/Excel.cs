﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HFC.CRM.Managers;
using System.IO;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Validation;
using HFC.CRM.Core.Logs;
using HFC.CRM.Core;
using HFC.CRM.Managers.DTO;


/// <summary>
/// The Reports namespace.
/// </summary>
namespace HFC.CRM.Serializer.Reports
{
    using Core.Extensions;
    using DocumentFormat.OpenXml;
    using DTO;
    using HFC.CRM.Managers.DTO;
    using System.Data;

    /// <summary>
    /// Class Excel.
    /// </summary>
    public class Excel
    {
        /// <summary>
        /// Serializes the specified data.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="ReportType">Type of the report.</param>
        /// <returns>MemoryStream.</returns>
        public static MemoryStream Serialize(List<ReportData> data, string ReportType)
        {

            MemoryStream outputStream = new MemoryStream();
            try
            {
                //create spreadsheet  -- create a spreadsheet decument from an IO stream
                SpreadsheetDocument spreadsheet = OpenXmlUtil.CreateWorkbook(outputStream);
                Worksheet worksheet = spreadsheet.AddWorksheet(ReportType + " Summary");
                spreadsheet.AddBasicStyles();


                List<OpenXmlColumnData> cols = new List<OpenXmlColumnData>();
                cols.Add(new OpenXmlColumnData("Date"));
                var keys = data.Select(x => x.Key).Distinct().OrderBy(o => o).ToList();

                foreach (var key in keys)
                    cols.Add(new OpenXmlColumnData(key));

                cols.Add(new OpenXmlColumnData("Totals"));

                //add header row
                var headRow = worksheet.AddHeaderRow(cols.ToArray());

                uint rowCounter = 2;
                var dates = data.Select(x => new { x.Month, x.Year }).Distinct();
                int totalDates = dates.Count();
                uint totalCols = (uint)cols.Count;
                foreach (var date in dates)
                {
                    var row = worksheet.AddRow();
                    DateTime recDate = new DateTime(date.Year, date.Month, 1);
                    row.AddCell(recDate.GlobalDateFormat() + " totals");

                    foreach (var col in cols)
                    {
                        if ((col.Name != "Date") && (col.Name != "Totals"))
                        {
                            var u = data.Where(v => v.Key == col.Name && v.Month == date.Month && v.Year == date.Year).Sum(w => w.Sum);
                            row.AddCell(u);
                        }
                    }
                    row.AddCellWithFormula(string.Format("SUM({0}{1}:{2}{1})",
                       OpenXmlUtil.ColumnNameFromIndex(2),
                       rowCounter.ToString(),
                       OpenXmlUtil.ColumnNameFromIndex(totalCols - 1)
                        ));
                    rowCounter++;
                }

                var totalsRow = worksheet.AddRow();
                totalsRow.AddCell("Totals");


                uint colCounter = 2;
                foreach (var col in cols)
                {
                    if (colCounter <= totalCols)
                    {
                        if (col.Name != "Date")
                        {

                            string colName = OpenXmlUtil.ColumnNameFromIndex(colCounter);
                            totalsRow.AddCellWithFormula(string.Format("SUM({0}{1}:{0}{2})",
                            colName, "2", (totalDates + 1).ToString()));
                            colCounter++;
                        }
                    }
                }
                worksheet.Save();
#if DEBUG
                OpenXmlValidator validator = new OpenXmlValidator();
                var errors = validator.Validate(spreadsheet);
#endif
                spreadsheet.Close();
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
            }

            return outputStream;
        }

        public static MemoryStream SalesInfo(string type, List<SalesData> salesCollection)
        {
            MemoryStream outputStream = new MemoryStream();

            try
            {
                //create spreadsheet  -- create a spreadsheet decument from an IO stream
                SpreadsheetDocument spreadsheet = OpenXmlUtil.CreateWorkbook(outputStream);
                Worksheet worksheet = spreadsheet.AddWorksheet("Sales");
                spreadsheet.AddBasicStyles();

                Row headRow;
                //add header row
                // if (AppConfigManager.ApplicationTitle.ToLower().Contains("tailored living") && type.ToLower().Contains("category") )
                if ((AppConfigManager.ApplicationTitle.ToLower().Contains("tailored living") && type.ToLower().Contains("category")) ||
                      type.ToLower().Contains("type") || type.ToLower().Contains("product") || type.ToLower().Contains("vendor"))
                {
                    headRow = worksheet.AddHeaderRow(new OpenXmlColumnData(type),
                                               new OpenXmlColumnData("Job Number"),
                                               new OpenXmlColumnData("First Appointment"),
                                               new OpenXmlColumnData("ContractedOnUtc"),
                                               new OpenXmlColumnData("FirstName"),
                                               new OpenXmlColumnData("LastName"),
                                               new OpenXmlColumnData("Total"));

                }
                else
                {
                    headRow = worksheet.AddHeaderRow(new OpenXmlColumnData(type),
                                                new OpenXmlColumnData("Job Number"),
                                                new OpenXmlColumnData("First Appointment"),
                                                new OpenXmlColumnData("ContractedOnUtc"),
                                                new OpenXmlColumnData("FirstName"),
                                                new OpenXmlColumnData("LastName"),
                                                new OpenXmlColumnData("Subtotal"),
                                                new OpenXmlColumnData("SurchargeTotal"),
                                                new OpenXmlColumnData("DiscountTotal"),
                                                new OpenXmlColumnData("Total"));
                }


                //Add data
                foreach (var sales in salesCollection)
                {
                    var row = worksheet.AddRow();
                    AddData(row, sales, type);
                }

                worksheet.Save();
#if DEBUG
                OpenXmlValidator validator = new OpenXmlValidator();
                var errors = validator.Validate(spreadsheet);
#endif

                spreadsheet.Close();
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
            }

            return outputStream;

        }

        public static MemoryStream MarketingInfo(List<MarketingData> collection)
        {

            MemoryStream outputStream = new MemoryStream();

            try
            {
                //create spreadsheet  -- create a spreadsheet decument from an IO stream
                SpreadsheetDocument spreadsheet = OpenXmlUtil.CreateWorkbook(outputStream);
                Worksheet worksheet = spreadsheet.AddWorksheet("Marketing");
                spreadsheet.AddBasicStyles();

                Row headRow;
                //add header row

                headRow = worksheet.AddHeaderRow(new OpenXmlColumnData("Source"),
                                            new OpenXmlColumnData("Ranking"),
                                            new OpenXmlColumnData("Total Jobs"),
                                            new OpenXmlColumnData("Number Of Sales"),
                                            new OpenXmlColumnData("Conversion Rate(%)"),
                                            new OpenXmlColumnData("Total Sales"),
                                            new OpenXmlColumnData("% of Total Sales"),
                                            new OpenXmlColumnData("Avg Sales"),
                                            new OpenXmlColumnData("Sales per Job"),
                                            new OpenXmlColumnData("Campaign Expenses"),
                                            new OpenXmlColumnData("Cost per Job"));

                var subtotal = collection.Sum(s => s.TotalSales ?? 0);

                //Add data
                foreach (var sales in collection)
                {
                    var row = worksheet.AddRow();
                    AddData(row, sales, subtotal);
                }

                worksheet.Save();
#if DEBUG
                OpenXmlValidator validator = new OpenXmlValidator();
                var errors = validator.Validate(spreadsheet);
#endif

                spreadsheet.Close();
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
            }

            return outputStream;
        }

        public static MemoryStream MLSInfo(List<MlsData> collection)
        {

            MemoryStream outputStream = new MemoryStream();

            try
            {
                //create spreadsheet  -- create a spreadsheet decument from an IO stream
                SpreadsheetDocument spreadsheet = OpenXmlUtil.CreateWorkbook(outputStream);
                Worksheet worksheet = spreadsheet.AddWorksheet("MLS");
                spreadsheet.AddBasicStyles();


                Row headRow;
                //add header row

                headRow = worksheet.AddHeaderRow(new OpenXmlColumnData("Terr Name"),
                                            new OpenXmlColumnData("Customer Name"),
                                            new OpenXmlColumnData("Job Number"),
                                            new OpenXmlColumnData("Zip Code"),
                                            new OpenXmlColumnData("Contracted Date"),
                                            new OpenXmlColumnData("Quantity"),

                                            new OpenXmlColumnData("Item Type"),
                                            new OpenXmlColumnData("Item Name"),
                                            new OpenXmlColumnData("Item Detail"),

                                            new OpenXmlColumnData("Is Taxable"),
                                            new OpenXmlColumnData("Unit Price"),

                                            new OpenXmlColumnData("Item Subtotal"),
                                            new OpenXmlColumnData("Unit Cost"),
                                            new OpenXmlColumnData("Item Cost"),
                                            new OpenXmlColumnData("Item Tax Percent"),
                                            new OpenXmlColumnData("Item Tax Amount"),
                                            new OpenXmlColumnData("Tax Rate Total")

                                            );

                //Add data
                foreach (var item in collection)
                {
                    var row = worksheet.AddRow();
                    AddData(row, item);
                }

                worksheet.Save();
#if DEBUG
                OpenXmlValidator validator = new OpenXmlValidator();
                var errors = validator.Validate(spreadsheet);
#endif

                spreadsheet.Close();
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
            }

            return outputStream;
        }

        private static void AddData(Row row, MlsData item)
        {

            if (item.TerrName != null)  //0
                row.AddCell(item.TerrName);
            else
                row.AddCell("");


            if (item.CustomerName != null)  //1
                row.AddCell(item.CustomerName);
            else
                row.AddCell("");


            if (item.JobNumber != null) //2
                row.AddCell(item.JobNumber);
            else
                row.AddCell("");

            if (item.ZipCode != null) //3
                row.AddCell(Convert.ToInt32(item.ZipCode));
            else
                row.AddCell("");

            if (item.ContractedDate != null)
                row.AddCell(item.ContractedDate);
            else
                row.AddCell("");

            if (item.Quantity != null)
                row.AddCell(Convert.ToInt32(item.Quantity.Value));
            else
                row.AddCell("");

            if (item.ItemType != null)
                row.AddCell(item.ItemType);
            else
                row.AddCell("");

            if (item.ItemName != null)
                row.AddCell(item.ItemName);
            else
                row.AddCell("");

            if (item.ItemDetail != null)
                row.AddCell(item.ItemDetail);
            else
                row.AddCell("");

            if (item.IsTaxable != null)
                row.AddCell(item.IsTaxable.ToString());
            else
                row.AddCell("");

            if (item.SalePrice != null)
                row.AddCell(item.SalePrice.Value);
            else
                row.AddCell("");

            if (item.ItemSubtotal != null)
                row.AddCell(item.ItemSubtotal.Value);
            else
                row.AddCell("");

            if (item.UnitCost != null)
                row.AddCell(item.UnitCost.Value);
            else
                row.AddCell("");

            if (item.ItemCostSubtotal != null)
                row.AddCell(item.ItemCostSubtotal.Value);
            else
                row.AddCell("");

            if (item.ItemTaxPercent != null)
                row.AddCell((float)(item.ItemTaxPercent.Value));
            else
                row.AddCell("");

            if (item.ItemTaxAmount != null)
                row.AddCell(item.ItemTaxAmount.Value);
            else
                row.AddCell("");

            if (item.TaxRateTotal != null)
                row.AddCell(item.TaxRateTotal.Value);
            else
                row.AddCell("");

        }


        private static void AddData(Row row, string item1, decimal item2)
        {


            if (item1 != null)
                row.AddTextInBoldCell(item1);
            else
                row.AddCell("");

            if (item2 != null)
                row.AddCurrencyInBoldCell(item2);
            else
                row.AddCell("");
        }

        private static void AddData(Row row, MarketingData item, decimal subtotal)
        {

            if (item.SourceName != null)  //0
                row.AddCell(item.SourceName);
            else
                row.AddCell("");


            if (item.Ranking != null)  //1
                row.AddCell(item.Ranking);
            else
                row.AddCell("");


            if (item.TotalCount != null) //2
                row.AddCell(item.TotalCount);
            else
                row.AddCell("");

            if (item.TotalSalesCount != null) //3
                row.AddCell(item.TotalSalesCount);
            else
                row.AddCell("");


            if (item.TotalCount > 0) //4
            {

                var conversion = Math.Round((float)(item.TotalSalesCount / (decimal)item.TotalCount) * 100, 2, MidpointRounding.AwayFromZero);
                row.AddCell((float)conversion);
            }
            else
                row.AddCell(0);


            if (item.TotalSales > 0) //5
                row.AddCell(item.TotalSales ?? 0);
            else
                row.AddCell("");


            if (item.TotalSales > 0) //6
            {

                decimal? perOfTotalSales = null;
                perOfTotalSales = (decimal)Math.Round((float)((item.TotalSales ?? 0) / subtotal) * 100, 2, MidpointRounding.AwayFromZero);
                row.AddCell((float)perOfTotalSales);
            }
            else
                row.AddCell(0);


            if (item.TotalSalesCount > 0) //7
            {
                var avg = (item.TotalSales ?? 0) / (decimal)item.TotalSalesCount;
                row.AddCell(avg);
            }
            else
                row.AddCell("");

            decimal salesPerJob = 0;
            if (item.TotalCount > 0) //8
            {
                salesPerJob = (item.TotalSales ?? 0) / (decimal)item.TotalCount;
                row.AddCell(salesPerJob);
            }
            else
                row.AddCell("");


            if (item.TotalExpense > 0) //9
            {
                row.AddCell((decimal)item.TotalExpense);
            }
            else
                row.AddCell("");

            if (item.TotalExpense > 0 && item.TotalExpense > 0) //10
            {
                decimal? cpj = null;
                if (item.TotalCount > 0 && item.TotalExpense.HasValue)
                    cpj = (item.TotalExpense ?? 0) / (decimal)item.TotalCount;
                else if (item.TotalExpense.HasValue)
                    cpj = item.TotalExpense.Value;

                row.AddCell((decimal)cpj);
            }
            else
                row.AddCell("");
        }

        private static void AddData(Row row, SalesData item, string type)
        {
            if (type.ToLower() == "city")
            {
                row.AddCell(item.City);
            }
            else if (type.ToLower() == "zipcode")
            {
                row.AddCell(item.ZipCode);
            }
            else
            {

                if (item.KeyName != null)
                    row.AddCell(item.KeyName);
                else
                    row.AddCell("");
            }


            if (item.JobNumber != null)
                row.AddCell(item.JobNumber);
            else
                row.AddCell("");
            if (item.FirstAppointment != null)
            {
                DateTimeOffset date = (DateTimeOffset)item.FirstAppointment;
                if (date != null)
                    row.AddCell(date.DateTime);
            }
            else
                row.AddCell("");
            if (item.ContractedOnUtc != null)
            {
                DateTimeOffset date = (DateTimeOffset)item.ContractedOnUtc;
                if (date != null)
                    row.AddCell(date.DateTime);
            }
            else
                row.AddCell("");

            if (item.FirstName != null)
                row.AddCell(item.FirstName);
            else
                row.AddCell("");
            if (item.LastName != null)
                row.AddCell(item.LastName);
            else
                row.AddCell("");

            row.AddCell(item.Subtotal);

            if ((AppConfigManager.ApplicationTitle.ToLower().Contains("tailored living") && type.ToLower().Contains("category")) ||
                type.ToLower().Contains("type") || type.ToLower().Contains("product") || type.ToLower().Contains("vendor"))
            {
                return;
            }

            row.AddCell(item.SurchargeTotal);
            row.AddCell(item.DiscountTotal);
            row.AddCell(item.Subtotal + item.SurchargeTotal - item.DiscountTotal);

        }


        private static void AddDataForSurchargeOrTax(Row row, MLSSummaryItem item)
        {


            if (item.ItemName != null)
                row.AddCell(item.ItemName);
            else
                row.AddCell("");

            if (item.Retail != null)
                row.AddCell(item.Retail);
            else
                row.AddCell("");

        }

        private static void AddData(Row row, MLSSummaryItem item)
        {

            if (item.ItemName != null)
                row.AddCell(item.ItemName);
            else
                row.AddCell("");

            if (item.Cost != null)
                row.AddCell(item.Cost);
            else
                row.AddCell("");
            if (item.Retail != null)
                row.AddCell(item.Retail);
            else
                row.AddCell("");

            if (item.GP != null)
                row.AddCell(item.GP + "%");
            else
                row.AddCell("");

        }

        // TODO: is this required in TP???
        //        public static MemoryStream MLSSummary(ChartsManager.MLSSummary_Data summaryData)
        //        {
        //           MemoryStream outputStream = new MemoryStream();

        //           try
        //           {
        //               //create spreadsheet  -- create a spreadsheet decument from an IO stream
        //               SpreadsheetDocument spreadsheet = OpenXmlUtil.CreateWorkbook(outputStream);
        //               Worksheet worksheet = spreadsheet.AddWorksheet("MLSSummary");
        //               spreadsheet.AddBasicStyles();

        //               Row headRow;
        //               string totalByHeader = AppConfigManager.GetConfig<bool>("IsBudgetBlends", "System Settings") ? "Totals By Vendor" : "Totals By Category";
        //               headRow = worksheet.AddHeaderInBoldRow(new OpenXmlColumnData(totalByHeader),
        //                                           new OpenXmlColumnData("Cost"),
        //                                           new OpenXmlColumnData("Sale Price"),
        //                                           new OpenXmlColumnData("GP%")
        //                                           );
        //               decimal totalCost = 0m;
        //               decimal totalSaleprice = 0m;
        //               decimal gp = 0m;



        //               //Add data
        //               foreach (var item in summaryData.MLSSummary.MLSVendorSummaryItems)
        //               {
        //                   var row = worksheet.AddRow();
        //                   AddData(row, item);

        //                   totalCost += item.Cost;
        //                   totalSaleprice += item.Retail;
        //               }
        //                  if (totalSaleprice != 0)
        //                   gp = Math.Round((totalSaleprice - totalCost) / totalSaleprice * 100, 2);


        //               AddDataWithTextBoldBorderTop(worksheet.AddRow(), new MLSSummaryItem
        //               {
        //                   ItemName = "Total",
        //                   Cost = Math.Round(totalCost, 2, MidpointRounding.ToEven),
        //                   Retail = Math.Round(totalSaleprice, 2, MidpointRounding.ToEven),
        //                   GP = gp
        //               });



        //               worksheet.AddRow();
        //               worksheet.AddRow();

        //               worksheet.AddHeaderInBoldRow(new OpenXmlColumnData("Totals By Product Type"),
        //                                          new OpenXmlColumnData("Cost"),
        //                                          new OpenXmlColumnData("Sale Price"),
        //                                          new OpenXmlColumnData("GP%")
        //                                          );


        //               totalCost = 0m;
        //             totalSaleprice = 0m;
        //               gp = 0m;


        //               //Add data
        //               foreach (var item in summaryData.MLSSummary.MLSTypeSummaryItems)
        //               {
        //                   var row = worksheet.AddRow();
        //                   AddData(row, item);
        //                   totalCost += item.Cost;
        //                   totalSaleprice += item.Retail;
        //               }

        //                gp = Math.Round((totalSaleprice - totalCost) / totalSaleprice * 100, 2);

        //                AddDataWithTextBoldBorderTop(worksheet.AddRow(), new MLSSummaryItem
        //                {
        //                    ItemName = "Total",
        //                    Cost = Math.Round(totalCost, 2, MidpointRounding.ToEven),
        //                    Retail = Math.Round(totalSaleprice, 2, MidpointRounding.ToEven),
        //                    GP = gp
        //                });


        //                worksheet.AddRow();
        //                worksheet.AddRow();


        //               totalSaleprice = 0m;
        //                gp = 0m;


        //                worksheet.AddHeaderInBoldRow(new OpenXmlColumnData("Additional Charges"),
        //                                       new OpenXmlColumnData("Sale Price"));

        //               //Add data
        //               foreach (var item in summaryData.MLSSummary.MLSSurchargeSummaryItems)
        //               {
        //                   var row = worksheet.AddRow();
        //                   AddDataForSurchargeOrTax(row, item);
        //                   totalSaleprice += item.Retail;
        //               }

        //               AddData(worksheet.AddRow(), "Total", (decimal)Math.Round(totalSaleprice, 2, MidpointRounding.ToEven));

        //               worksheet.AddRow();
        //               worksheet.AddRow();


        //               totalSaleprice = 0m;
        //               gp = 0m;
        //               worksheet.AddHeaderInBoldRow(new OpenXmlColumnData("Tax Name/Rate"),
        //                                         new OpenXmlColumnData("Tax Total")
        //                                         );
        //               //Add data
        //               foreach (var item in summaryData.MLSSummary.MLSTaxSummaryItems)
        //               {
        //                   var row = worksheet.AddRow();
        //                   AddDataForSurchargeOrTax(row, item);
        //                   totalSaleprice += item.Retail;
        //               }

        //               AddData(worksheet.AddRow(), "Total", (decimal) Math.Round(totalSaleprice, 2, MidpointRounding.ToEven));



        //               worksheet.Save();
        //#if DEBUG
        //               OpenXmlValidator validator = new OpenXmlValidator();
        //               var errors = validator.Validate(spreadsheet);
        //#endif

        //               spreadsheet.Close();
        //           }
        //           catch (Exception ex)
        //           {
        //               EventLogger.LogEvent(ex);
        //           }

        //            return outputStream;
        //        }

        private static void AddDataWithTextBoldBorderTop(Row row, MLSSummaryItem item)
        {
            if (item.ItemName != null)
                row.AddTextInBoldCell(item.ItemName);
            else
                row.AddCell("");

            if (item.Cost != null)
                row.AddCurrencyInBoldCell(item.Cost);
            else
                row.AddCell("");
            if (item.Retail != null)
                row.AddCurrencyInBoldCell(item.Retail);
            else
                row.AddCell("");

            if (item.GP != null)
                row.AddTextInBoldCell(item.GP + "%");
            else
                row.AddCell("");
        }


        public static MemoryStream RawData(List<RawData_DM> collection)
        {

            MemoryStream outputStream = new MemoryStream();

            try
            {
                //create spreadsheet  -- create a spreadsheet decument from an IO stream
                SpreadsheetDocument spreadsheet = OpenXmlUtil.CreateWorkbook(outputStream);
                Worksheet worksheet = spreadsheet.AddWorksheet("RawData");
                spreadsheet.AddBasicStyles();

                Row headRow;
                //add header row

                headRow = worksheet.AddHeaderRow(new OpenXmlColumnData("Terr code"),
                                            new OpenXmlColumnData("Zip"),
                                            new OpenXmlColumnData("Account #"),
                                            new OpenXmlColumnData("Opportunity #"),
                                            new OpenXmlColumnData("SaleDate"),
                                            new OpenXmlColumnData("Fname"),
                                            new OpenXmlColumnData("Lname"),
                                            //new OpenXmlColumnData("Tax$"),
                                            //new OpenXmlColumnData("Tax%"),
                                            //new OpenXmlColumnData("Cost"),
                                            //new OpenXmlColumnData("Sale$$"),
                                            //new OpenXmlColumnData("GP$"),
                                            //new OpenXmlColumnData("GP%"),
                                            new OpenXmlColumnData("Vendor"),
                                            new OpenXmlColumnData("ProductType"),
                                            new OpenXmlColumnData("Model"),
                                            new OpenXmlColumnData("Source"),
                                            new OpenXmlColumnData("InstallAddress"),
                                            new OpenXmlColumnData("BillAddress")
                                            );

                //Add data
                foreach (var sales in collection)
                {
                    var row = worksheet.AddRow();
                    AddRawData(row, sales);
                }

                worksheet.Save();
#if DEBUG
                OpenXmlValidator validator = new OpenXmlValidator();
                var errors = validator.Validate(spreadsheet);
#endif

                spreadsheet.Close();
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
            }

            return outputStream;
        }

        private static void AddRawData(Row row, RawData_DM item)
        {

            if (item.LeadTerritoryCode != null)
                row.AddCell(item.LeadTerritoryCode);
            else
                row.AddCell("");

            if (item.AccountZipCode != null)
                row.AddCell(item.AccountZipCode.ToString());
            else
                row.AddCell("");

            if (item.AccountNumber != null)
                row.AddCell(item.AccountNumber.ToString());
            else
                row.AddCell("");

            if (item.OpportunityNumber != null)
                row.AddCell(item.OpportunityNumber.ToString());
            else
                row.AddCell("");

            if (item.OrderDate != null)
                row.AddCell(item.OrderDate.ToString());
            else
                row.AddCell("");

            if (item.AccountFirstName != null)
                row.AddCell(item.AccountFirstName);
            else
                row.AddCell("");

            if (item.AccountLastName != null)
                row.AddCell(item.AccountLastName);
            else
                row.AddCell("");

            if (item.VendorName != null)
                row.AddCell(item.VendorName);
            else
                row.AddCell("");

            if (item.ProductCategory != null)
                row.AddCell(item.ProductCategory);
            else
                row.AddCell("");

            if (item.ProductModel != null)
                row.AddCell(item.ProductModel);
            else
                row.AddCell("");

            if (item.OpportunitySource != null)
                row.AddCell(item.OpportunitySource);
            else
                row.AddCell("");

            if (item.OpportunityZipCode != null)
                row.AddCell(item.OpportunityAddress1 + item.OpportunityAddress2 + item.OpportunityCity + item.OpportunityState + item.OpportunityZipCode);
            else
                row.AddCell("");

            if (item.OpportunityZipCode != null)
                row.AddCell(item.OpportunityAddress1 + item.OpportunityAddress2 + item.OpportunityCity + item.OpportunityState + item.OpportunityZipCode);
            else
                row.AddCell("");

        }

        public static MemoryStream ExportDataTable(DataTable table)
        {
            MemoryStream outputStream = new MemoryStream();
            using (var workbook = SpreadsheetDocument.Create(outputStream, SpreadsheetDocumentType.Workbook))
            {
                var workbookPart = workbook.AddWorkbookPart();

                workbook.WorkbookPart.Workbook = new DocumentFormat.OpenXml.Spreadsheet.Workbook();

                workbook.WorkbookPart.Workbook.Sheets = new DocumentFormat.OpenXml.Spreadsheet.Sheets();

                var sheetPart = workbook.WorkbookPart.AddNewPart<WorksheetPart>();
                var sheetData = new DocumentFormat.OpenXml.Spreadsheet.SheetData();
                sheetPart.Worksheet = new DocumentFormat.OpenXml.Spreadsheet.Worksheet(sheetData);

                DocumentFormat.OpenXml.Spreadsheet.Sheets sheets = workbook.WorkbookPart.Workbook.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.Sheets>();
                string relationshipId = workbook.WorkbookPart.GetIdOfPart(sheetPart);

                uint sheetId = 1;
                if (sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Count() > 0)
                {
                    sheetId =
                        sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                }

                DocumentFormat.OpenXml.Spreadsheet.Sheet sheet = new DocumentFormat.OpenXml.Spreadsheet.Sheet() { Id = relationshipId, SheetId = sheetId, Name = table.TableName };
                sheets.Append(sheet);

                DocumentFormat.OpenXml.Spreadsheet.Row headerRow = new DocumentFormat.OpenXml.Spreadsheet.Row();

                List<String> columns = new List<string>();
                foreach (System.Data.DataColumn column in table.Columns)
                {
                    columns.Add(column.ColumnName);

                    DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(column.ColumnName);
                    headerRow.AppendChild(cell);
                }

                sheetData.AppendChild(headerRow);

                foreach (System.Data.DataRow dsrow in table.Rows)
                {
                    DocumentFormat.OpenXml.Spreadsheet.Row newRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                    foreach (String col in columns)
                    {
                        DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                        cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dsrow[col].ToString()); //
                        newRow.AppendChild(cell);
                    }
                    sheetData.AppendChild(newRow);
                }
            }
            return outputStream;
        }

        private void ExportDataSet(DataSet ds, string destination)
        {
            using (var workbook = SpreadsheetDocument.Create(destination, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook))
            {
                var workbookPart = workbook.AddWorkbookPart();

                workbook.WorkbookPart.Workbook = new DocumentFormat.OpenXml.Spreadsheet.Workbook();

                workbook.WorkbookPart.Workbook.Sheets = new DocumentFormat.OpenXml.Spreadsheet.Sheets();

                foreach (System.Data.DataTable table in ds.Tables)
                {

                    var sheetPart = workbook.WorkbookPart.AddNewPart<WorksheetPart>();
                    var sheetData = new DocumentFormat.OpenXml.Spreadsheet.SheetData();
                    sheetPart.Worksheet = new DocumentFormat.OpenXml.Spreadsheet.Worksheet(sheetData);

                    DocumentFormat.OpenXml.Spreadsheet.Sheets sheets = workbook.WorkbookPart.Workbook.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.Sheets>();
                    string relationshipId = workbook.WorkbookPart.GetIdOfPart(sheetPart);

                    uint sheetId = 1;
                    if (sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Count() > 0)
                    {
                        sheetId =
                            sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                    }

                    DocumentFormat.OpenXml.Spreadsheet.Sheet sheet = new DocumentFormat.OpenXml.Spreadsheet.Sheet() { Id = relationshipId, SheetId = sheetId, Name = table.TableName };
                    sheets.Append(sheet);

                    DocumentFormat.OpenXml.Spreadsheet.Row headerRow = new DocumentFormat.OpenXml.Spreadsheet.Row();

                    List<String> columns = new List<string>();
                    foreach (System.Data.DataColumn column in table.Columns)
                    {
                        columns.Add(column.ColumnName);

                        DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                        cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(column.ColumnName);
                        headerRow.AppendChild(cell);
                    }


                    sheetData.AppendChild(headerRow);

                    foreach (System.Data.DataRow dsrow in table.Rows)
                    {
                        DocumentFormat.OpenXml.Spreadsheet.Row newRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                        foreach (String col in columns)
                        {
                            DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dsrow[col].ToString()); //
                            newRow.AppendChild(cell);
                        }

                        sheetData.AppendChild(newRow);
                    }

                }
            }
        }


    }
}
