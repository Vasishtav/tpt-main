﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Validation;
using DocumentFormat.OpenXml.Wordprocessing;
using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using DocumentFormat.OpenXml.Office2010.Word;
using HFC.CRM.Data.Context;
using System.Data.SqlClient;
using System.Data;
using Dapper;
using SelectPdf;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using HFC.CRM.Core.Extensions;

namespace HFC.CRM.Serializer
{
    public class QuoteSheetReport : DataManager
    {
        private OrderManager ordermgr = new OrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private QuotesManager quotemgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private MeasurementManager MeasurementMgr = new MeasurementManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private ProductManagerTP product_mgr = new ProductManagerTP();
        public string phoneFormat = "(###) ###-####";

        public string QuoteSheet(int QuoteKey, PrintVersion printVersion = PrintVersion.IncludeDiscount)
        {
            string data = string.Empty;
            if (SessionManager.BrandId == 1)
                data = QuoteSheet_BudgetBlinds(QuoteKey, printVersion);
            else if (SessionManager.BrandId == 2)
                data = QuoteSheet_TailorLiving(QuoteKey, printVersion);
            else
                data = QuoteSheet_ConcreteCraft(QuoteKey, printVersion);
            return data;
        }

        public string QuoteSheetHeader(int QuoteKey)
        {
            string data = string.Empty;
            if (SessionManager.BrandId == 1)
                data = QuoteSheetHeader_BudgetBlinds(QuoteKey);
            else if (SessionManager.BrandId == 2)
                data = QuoteSheetHeader_TailorLiving(QuoteKey);
            else
                data = QuoteSheetHeader_ConcreteCraft(QuoteKey);
            return data;
        }

        public string QuoteSheetFooter(int QuoteKey)
        {
            string data = string.Empty;
            if (SessionManager.BrandId == 1)
                data = QuoteSheetFooter_BudgetBlinds(QuoteKey);
            else if (SessionManager.BrandId == 2)
                data = QuoteSheetFooter_TailorLiving(QuoteKey);
            else
                data = QuoteSheetFooter_ConcreteCraft(QuoteKey);
            return data;
        }

        public string QuoteSheet_BudgetBlinds(int QuoteKey, PrintVersion printVersion)
        {
            string Path = string.Empty;
            Path = string.Format("/Templates/Base/Brand/BB/BB_Quote_Template-bodycontent.html");
            string srcFilePath = HostingEnvironment.MapPath(Path);
            string data = System.IO.File.ReadAllText(srcFilePath);
            try
            {
                int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;

                var Quote = quotemgr.GetQuoteInfo(QuoteKey);

                var tax = quotemgr.GetTaxinfo(QuoteKey);
                //var taxAmt = tax.Sum(x => x.Amount);
                if (Quote == null)
                    return "";

                var Opportunity = quotemgr.GetOpportunityByQuoteKey(QuoteKey);
                var customer = quotemgr.GetCustomerByQuoteKey(QuoteKey);
                var BillingAddress = quotemgr.GetBillingAddressByQuoteKey(QuoteKey);
                var InstallationAddress = quotemgr.GetInstallationAddressByQuoteKey(QuoteKey);
                if (BillingAddress == null) BillingAddress = InstallationAddress;
                var disclaimer = ordermgr.GetFranchiseDisclaimer();
                var FEdiscounts = product_mgr.GetListDetails(3);

                if (disclaimer != null && !string.IsNullOrEmpty(disclaimer.ShortDisclaimer) && disclaimer.ShortDisclaimer != "")
                    disclaimer.ShortDisclaimer = disclaimer.ShortDisclaimer.Replace(Environment.NewLine, "<br />").Replace("\r", "<br />").Replace("\n", "<br />");

                if (disclaimer != null && !string.IsNullOrEmpty(disclaimer.LongDisclaimer) && disclaimer.LongDisclaimer != "")
                    disclaimer.LongDisclaimer = disclaimer.LongDisclaimer.Replace(Environment.NewLine, "<br />").Replace("\r", "<br />").Replace("\n", "<br />");

                if (Quote != null && !string.IsNullOrEmpty(Quote.QuoteLevelNotes) && Quote.QuoteLevelNotes != "")
                    Quote.QuoteLevelNotes = Quote.QuoteLevelNotes.Replace(Environment.NewLine, "<br />").Replace("\r", "<br />").Replace("\n", "<br />");

                var Measurementres = MeasurementMgr.Get(InstallationAddress.AddressId);
                List<MeasurementLineItemBB> MeasurementData = new List<MeasurementLineItemBB>();
                if (Measurementres != null)
                    MeasurementData = Measurementres.MeasurementBB;

                if (customer.HomePhone != null && customer.HomePhone.Length > 0)
                {
                    customer.HomePhone = Convert.ToInt64(customer.HomePhone).ToString(phoneFormat);
                    if (customer.PreferredTFN != null && customer.PreferredTFN == "H")
                        customer.HomePhone = "*" + customer.HomePhone;
                }
                else
                    customer.HomePhone = "";

                if (customer.CellPhone != null && customer.CellPhone.Length > 0)
                {
                    customer.CellPhone = Convert.ToInt64(customer.CellPhone).ToString(phoneFormat);
                    if (customer.PreferredTFN != null && customer.PreferredTFN == "C")
                        customer.CellPhone = "*" + customer.CellPhone;
                }
                else
                    customer.CellPhone = "";

                if (customer.WorkPhone != null && customer.WorkPhone.Length > 0)
                {
                    customer.WorkPhone = Convert.ToInt64(customer.WorkPhone).ToString(phoneFormat);
                    if (customer.PreferredTFN != null && customer.PreferredTFN == "W")
                        customer.WorkPhone = "*" + customer.WorkPhone;
                }
                else
                    customer.WorkPhone = "";

                if (customer.CellPhone != "")
                    data = data.Replace("{displayAccountCellPhone}", "");
                else
                    data = data.Replace("{displayAccountCellPhone}", "none");

                if (customer.HomePhone != "")
                    data = data.Replace("{displayAccountHomePhone}", "");
                else
                    data = data.Replace("{displayAccountHomePhone}", "none");

                if (customer.WorkPhone != "")
                    data = data.Replace("{displayAccountWorkPhone}", "");
                else
                    data = data.Replace("{displayAccountWorkPhone}", "none");

                //Read lead html and append the data
                data = data.Replace("{AccountCustomerName}", Quote.AccountName);
                data = data.Replace("{AccountHomePhone}", customer.HomePhone);
                data = data.Replace("{AccountCellPhone}", customer.CellPhone);
                data = data.Replace("{AccountWorkPhone}", customer.WorkPhone);
                data = data.Replace("{AccountEmail}", customer.PrimaryEmail);
                data = data.Replace("{OppSideMark}", Quote != null && Quote.SideMark != null ? Quote.SideMark : "");

                data = data.Replace("{BillingAddress1}", BillingAddress.Address1);
                data = data.Replace("{BillingAddress2}", BillingAddress.Address2);
                data = data.Replace("{BillingCityStateZip}", BillingAddress.City + ", " + BillingAddress.State + " " + BillingAddress.ZipCode);

                data = data.Replace("{InstallationAddress1}", InstallationAddress.Address1);
                data = data.Replace("{InstallationAddress2}", InstallationAddress.Address2);
                data = data.Replace("{InstallationCityStateZip}", InstallationAddress.City + ", " + InstallationAddress.State + " " + InstallationAddress.ZipCode);

                data = data.Replace("{HeaderLevelComments}", Quote != null && !string.IsNullOrEmpty(Quote.QuoteLevelNotes) && Quote.QuoteLevelNotes != "" ? "<br /><b>Comments : </b>" + Quote.QuoteLevelNotes : "");

                data = data.Replace("{shortlegaldisclaimer}", disclaimer != null ? disclaimer.ShortDisclaimer : "");
                data = data.Replace("{longlegaldisclaimer}", disclaimer != null ? disclaimer.LongDisclaimer : "");

                if (Quote.QuoteLinesVM != null && Quote.QuoteLinesVM.Count > 0)
                {
                    var products = (from ql in Quote.QuoteLinesVM where ql.ProductTypeId == 1 || ql.ProductTypeId == 2 || ql.ProductTypeId == 5 select ql).ToList();

                    if (products != null && products.Count > 0)
                    {
                        StringBuilder sbProduct = new StringBuilder();
                        sbProduct.Append("<table class='table table-quoteprint' >");
                        sbProduct.Append("<thead style='display: table-header-group;'>");
                        sbProduct.Append("<tr>");
                        if (printVersion != PrintVersion.CondensedVersion)
                            sbProduct.Append("<th>Window Name</th>");
                        sbProduct.Append("<th>Product</th>");
                        if (printVersion == PrintVersion.IncludeDiscount)
                        {
                            sbProduct.Append("<th>Sugg Resale</th>");
                            sbProduct.Append("<th>Discount</th>");
                        }
                        if (printVersion != PrintVersion.CondensedVersion)
                            sbProduct.Append("<th>Unit Price</th>");
                        sbProduct.Append("<th>Qty</th>");
                        sbProduct.Append("<th>Total</th>");
                        sbProduct.Append("</tr>");
                        sbProduct.Append("</thead>");
                        sbProduct.Append("<tbody>");

                        var CondensedGrouped = products.ToList().GroupBy(p => new { p.ProductName, p.VendorName }).Select(m=> new CondensedProductVendor() { ProductName= m.Key.ProductName,VendorName=m.Key.VendorName,chk=false}).ToList();

                        foreach (var p in products)
                        {
                            if (string.IsNullOrEmpty(p.CancelReason))
                            {
                                if (printVersion == PrintVersion.CondensedVersion)
                                {
                                    if(CondensedGrouped!=null)
                                    {
                                        var CondensedRecord = CondensedGrouped.Where(x => x.ProductName == p.ProductName && x.VendorName == p.VendorName && x.chk == false).FirstOrDefault();
                                        if (CondensedRecord == null)
                                            continue;
                                        else
                                            CondensedRecord.chk = true;
                                    }
                                }

                                sbProduct.Append("<tr style='border-bottom:1px solid #ddd;page-break-inside:avoid;'>");
                                var md = (from m in MeasurementData where m.id == p.MeasurementsetId select m).FirstOrDefault();

                                // Get vendor name from picjson
                                string vendorname = "";
                                if (!string.IsNullOrEmpty(p.VendorDisplayname))
                                {
                                    vendorname = p.VendorDisplayname;
                                }
                                else if (p.ProductTypeId != 1 && !string.IsNullOrEmpty(p.PICJson) && p.PICJson != "")
                                {
                                    var picjson = JsonConvert.DeserializeObject<dynamic>(p.PICJson);
                                    if (picjson != null && picjson.VendorName != null)
                                        vendorname = picjson.VendorName;
                                }
                                else
                                    vendorname = p.VendorName;

                                // Remove Hight and width in core product description

                                string prodctDesc = p.Description;

                                if (p.ProductTypeId == 1 && !string.IsNullOrEmpty(p.Description) && p.Description != "")
                                {
                                    List<string> lstdesc = p.Description.Split(';').ToList();

                                    var filterWidth = lstdesc.Where(x => x.TrimStart().StartsWith("Width:")).ToList();

                                    if (filterWidth != null && filterWidth.Count > 0)
                                    {
                                        foreach (var item in filterWidth)
                                            lstdesc.Remove(item);
                                    }

                                    var filterHeight = lstdesc.Where(x => x.TrimStart().StartsWith("Height:")).ToList();
                                    if (filterHeight != null && filterHeight.Count > 0)
                                    {
                                        foreach (var item in filterHeight)
                                            lstdesc.Remove(item);
                                    }

                                    prodctDesc = string.Join(",", lstdesc.ToArray());
                                }

                                if (string.IsNullOrEmpty(vendorname) && p.VendorId.HasValue && p.VendorId.Value != 0)
                                {
                                    var vendormgr = new VendorManager();
                                    var vendor = vendormgr.GetLocalvendor(p.VendorId.Value);
                                    if (vendor != null && (vendor.VendorType == 2 ||
                                        vendor.VendorType == 3))
                                    {
                                        vendorname = "";
                                    }
                                }

                                if (vendorname != "")
                                    if (printVersion == PrintVersion.CondensedVersion)
                                        prodctDesc = "<b>" + vendorname + "</b>";
                                    else
                                        prodctDesc = "<b>" + vendorname + "</b>; " + prodctDesc;

                                //if (!string.IsNullOrEmpty(p.RoomName) && !string.IsNullOrEmpty(p.WindowLocation))
                                //    sbProduct.Append("<td>" + p.RoomName + " / " + p.WindowLocation + "</td>");
                                //else if (!string.IsNullOrEmpty(p.RoomName) || !string.IsNullOrEmpty(p.WindowLocation))
                                //    sbProduct.Append("<td>" + p.RoomName + "" + p.WindowLocation + "</td>");

                                if (printVersion != PrintVersion.CondensedVersion)
                                {
                                    if (!string.IsNullOrEmpty(p.RoomName))
                                        sbProduct.Append("<td>" + p.RoomName + "</td>");
                                    else
                                        sbProduct.Append("<td></td>");
                                }

                                if (printVersion == PrintVersion.CondensedVersion)
                                    sbProduct.Append("<td><b>" + p.ProductName + "</b> - " + prodctDesc + "<br />");
                                else
                                    sbProduct.Append("<td><b>" + p.ProductName + "</b><br />" + prodctDesc + "<br />");

                                if ((p.Memo != null && !string.IsNullOrEmpty(p.Memo)) || (!string.IsNullOrEmpty(p.CustomerNotes) && p.CustomerNotes != ""))
                                {
                                    sbProduct.Append("<p>Comments:</p>");
                                    sbProduct.Append("<p class='comment_desc'>");
                                    if (p.Memo != null && !string.IsNullOrEmpty(p.Memo))
                                    {
                                        sbProduct.Append(p.Memo);
                                        sbProduct.Append("<br />");
                                    }
                                    //if (md != null && !string.IsNullOrEmpty(md.Comments))
                                    //{
                                    //    sbProduct.Append(md.Comments);
                                    //    sbProduct.Append("<br />");
                                    //}
                                    if (!string.IsNullOrEmpty(p.CustomerNotes) && p.CustomerNotes != "")
                                    {
                                        sbProduct.Append(p.CustomerNotes);
                                    }
                                    sbProduct.Append("</p>");
                                }
                                if (printVersion == PrintVersion.IncludeDiscount && Quote.DiscountsAndPromo != null && Quote.DiscountsAndPromo.Count > 0)
                                {
                                    var dpromo = (from dp in Quote.DiscountsAndPromo where dp.QuoteLineId == p.QuoteLineId select dp).FirstOrDefault();
                                    if (dpromo != null && dpromo.DiscountAmountPassed != null && dpromo.DiscountAmountPassed > 0)
                                    {
                                        sbProduct.Append("<p>Promo Applied:</p> Vendor promotion");
                                    }
                                }
                                sbProduct.Append("</td>");
                                if (printVersion == PrintVersion.IncludeDiscount)
                                {
                                    sbProduct.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", p.SuggestedResale) + "</td>");
                                    sbProduct.Append("<td class='quoteprint_ralign'>" + Convert.ToDecimal(p.Discount).ToString("F") + "%" + "</td>");
                                }
                                if (printVersion != PrintVersion.CondensedVersion)
                                    sbProduct.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", p.UnitPrice) + "</td>");

                                var Quantity = p.Quantity;
                                var ExtendedPrice = p.ExtendedPrice;

                                if (printVersion == PrintVersion.CondensedVersion)
                                {
                                    Quantity = products.Where(x => x.ProductName == p.ProductName && x.VendorName == p.VendorName && string.IsNullOrEmpty(x.CancelReason)).Sum(x => x.Quantity);
                                    ExtendedPrice = products.Where(x => x.ProductName == p.ProductName && x.VendorName == p.VendorName && string.IsNullOrEmpty(x.CancelReason)).Sum(x => x.ExtendedPrice);
                                }

                                sbProduct.Append("<td class='quoteprint_ralign'>" + Quantity + "</td>");
                                sbProduct.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", ExtendedPrice) + "</td>");
                                sbProduct.Append("</tr>");
                            }
                        }

                        sbProduct.Append("</tbody>");
                        sbProduct.Append("</table>");

                        data = data.Replace("{ProductSection}", sbProduct.ToString());
                    }
                    else
                        data = data.Replace("{ProductSection}", "");
                }
                else
                    data = data.Replace("{ProductSection}", "");

                if (Quote.QuoteLinesVM != null && Quote.QuoteLinesVM.Count > 0)
                {
                    var Service = (from ql in Quote.QuoteLinesVM where ql.ProductTypeId == 3 select ql).ToList();

                    if (Service != null && Service.Count > 0)
                    {
                        StringBuilder sbService = new StringBuilder();
                        sbService.Append("<table class='table table-quoteprint' style='border-bottom:1px solid #ddd;page-break-inside:avoid;'>");
                        sbService.Append("<thead style='display: table-header-group;'>");
                        sbService.Append("<tr>");
                        sbService.Append("<th>Additional Items</th>");
                        sbService.Append("<th>Memo</th>");
                        if (printVersion == PrintVersion.IncludeDiscount)
                        {
                            sbService.Append("<th>Sugg Resale</th>");
                            sbService.Append("<th>Discount</th>");
                        }
                        sbService.Append("<th>Unit Price</th>");
                        sbService.Append("<th>Qty</th>");
                        sbService.Append("<th>Total</th>");
                        sbService.Append("</tr>");
                        sbService.Append("</thead>");
                        sbService.Append("<tbody>");
                        foreach (var s in Service)
                        {
                            sbService.Append("<tr class='border_quoteprintmemo' style='page-break-inside:avoid'>");
                            if (!string.IsNullOrEmpty(s.ModelDescription) && s.ProductName != s.ModelDescription)
                            {
                                sbService.Append("<td>" + s.ProductName + " - " + s.ModelDescription + "</td>");
                            }
                            else
                            {
                                sbService.Append("<td>" + s.ProductName + "</td>");
                            }
                            sbService.Append("<td>" + s.Description + "</td>");
                            if (printVersion == PrintVersion.IncludeDiscount)
                            {
                                sbService.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", s.SuggestedResale) + "</td>");
                                sbService.Append("<td class='quoteprint_ralign'>" + Convert.ToDecimal(s.Discount).ToString("F") + "%" + "</td>");
                            }
                            sbService.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", s.UnitPrice) + "</td>");
                            sbService.Append("<td class='quoteprint_ralign'>" + s.Quantity + "</td>");
                            sbService.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", s.ExtendedPrice) + "</td>");
                            sbService.Append("</tr>");
                        }
                        sbService.Append("</tbody>");
                        sbService.Append("</table>");
                        data = data.Replace("{ServicesSection}", sbService.ToString());
                    }
                    else
                        data = data.Replace("{ServicesSection}", "");
                }
                else
                    data = data.Replace("{ServicesSection}", "");

                if (Quote.QuoteLinesVM != null && Quote.QuoteLinesVM.Count > 0)
                {
                    var discountd = (from ql in Quote.QuoteLinesVM where ql.ProductTypeId == 4 select ql).ToList();

                    if ((discountd != null && discountd.Count > 0) || (Quote != null && Quote.Discount != null && Quote.Discount > 0))
                    {
                        StringBuilder sbDiscount = new StringBuilder();
                        sbDiscount.Append("<table class='table table-quoteprint' style='border-bottom:1px solid #ddd;'>");
                        sbDiscount.Append("<thead style='display: table-header-group;'>");
                        sbDiscount.Append("<tr>");
                        sbDiscount.Append("<th style = 'width:30%'> Discount Summary</th>");
                        sbDiscount.Append("<th width:70%>Memo</th>");
                        sbDiscount.Append("</tr>");
                        sbDiscount.Append("</thead>");
                        sbDiscount.Append("<tbody>");
                        if (Quote != null && Quote.Discount != null && Quote.Discount > 0)
                        {
                            string DiscountName = "", DiscountDesc = "Discount";
                            if (Quote.DiscountId > 0 && FEdiscounts != null && FEdiscounts.Count > 0)
                            {
                                var dis = (from d in FEdiscounts where d.ProductID == Quote.DiscountId select d).FirstOrDefault();
                                if (dis != null)
                                {
                                    DiscountName = dis.ProductName;
                                    DiscountDesc = dis.Description;
                                }
                            }
                            string discount = "0%";
                            if (Quote.Discount != null && Quote.DiscountType == "$")
                            {
                                discount = string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", Quote.Discount);
                                if (DiscountName != "")
                                    discount += " - " + DiscountName;
                            }
                            if (Quote.Discount != null && Quote.DiscountType == "%")
                            {
                                discount = Convert.ToDecimal(Quote.Discount).ToString("F") + "%";
                                if (DiscountName != "")
                                    discount += " - " + DiscountName;
                            }
                            sbDiscount.Append("<tr class='border_quoteprintmemo'>");
                            sbDiscount.Append("<td>" + discount + "</td>");
                            sbDiscount.Append("<td>" + DiscountDesc + "</td>");
                            sbDiscount.Append("</tr>");
                        }
                        if (discountd != null && discountd.Count > 0)
                        {
                            foreach (var d in discountd)
                            {
                                string DiscountName = "", DiscountDesc = "";
                                if (d.ProductId > 0 && FEdiscounts != null && FEdiscounts.Count > 0)
                                {
                                    var dis = (from di in FEdiscounts where di.ProductID == d.ProductId select di).FirstOrDefault();
                                    if (dis != null)
                                    {
                                        DiscountName = dis.ProductName;
                                        DiscountDesc = d.Description;
                                    }
                                }
                                if (d.Discount != null && d.Discount > 0)
                                {
                                    string discoutdata = "";
                                    if (d.Discount != null)
                                    {
                                        if (d.DiscountType == "$")
                                        {
                                            discoutdata += "$" + String.Format("{0:0.##}", d.Discount);
                                        }
                                        else
                                        {
                                            discoutdata += String.Format("{0:0.##}", d.Discount) + "%";
                                        }

                                        if (DiscountName != "")
                                            discoutdata += " - " + DiscountName;
                                    }

                                    sbDiscount.Append("<tr class='border_quoteprintmemo' style='page-break-inside:avoid'>");
                                    sbDiscount.Append("<td>" + discoutdata + "</td>");
                                    sbDiscount.Append("<td>" + d.Description + "</td>");
                                    sbDiscount.Append("</tr>");
                                }
                            }
                        }

                        sbDiscount.Append("</tbody>");
                        sbDiscount.Append("</table>");
                        data = data.Replace("{DiscountsSection}", sbDiscount.ToString());
                    }
                    else
                        data = data.Replace("{DiscountsSection}", "");
                }
                else
                    data = data.Replace("{DiscountsSection}", "");

                data = data.Replace("{ProductSubtotal}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", Quote.ProductSubTotal));
                data = data.Replace("{AdditionalCharges}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", Quote.AdditionalCharges));
                data = data.Replace("{SpecialDiscounts}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", Quote.TotalDiscounts));
                data = data.Replace("{TotalAmount}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", Quote.QuoteSubTotal));

                if (Quote.TotalDiscounts > 0)
                    data = data.Replace("{displaySpecialDiscounts}", "");
                else
                    data = data.Replace("{displaySpecialDiscounts}", "none");

                var countryCode = SessionManager.CurrentFranchise.Address.CountryCode2Digits;

                if (tax != null && tax.Count() > 0)
                {
                    var taxAmt = tax.Sum(x => x.Amount);
                    data = data.Replace("{TaxTotal}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", taxAmt));
                    if (taxAmt.HasValue)
                    {
                        if (Quote.QuoteSubTotal.HasValue)
                        {
                            Quote.QuoteSubTotal = Quote.QuoteSubTotal + taxAmt;
                        }
                    }
                    var taxgrp = tax.GroupBy(x => new { x.JurisType, x.Jurisdiction, x.TaxName, x.Rate })
                        .Select(z => new { txtype = z.Key, total = z.Sum(c => c.Amount) });

                    StringBuilder sbtax = new StringBuilder();
                    sbtax.Append("<table class='table table-quoteprint' style='border-bottom:1px solid #ddd;page-break-inside:avoid'>");
                    sbtax.Append("<thead >");
                    sbtax.Append("<tr>");
                    sbtax.Append("<th style = 'width:70%'>Tax Name</th>");
                    sbtax.Append("<th width:30%>Amount</th>");
                    sbtax.Append("</tr>");
                    sbtax.Append("</thead>");
                    sbtax.Append("<tbody>");
                    foreach (var item in taxgrp)
                    {
                        sbtax.Append("<tr class='border_quoteprintmemo' style='page-break-inside:avoid'>");
                        if (!string.IsNullOrEmpty(item.txtype.TaxName))
                        {
                            sbtax.Append("<td>" + item.txtype.TaxName + "</td>");
                        }
                        else
                        {
                            sbtax.Append("<td>" + item.txtype.Jurisdiction + "</td>");
                        }
                        sbtax.Append("<td>" + item.total + "</td>");
                        sbtax.Append("</tr>");
                    }
                    sbtax.Append("</tbody>");
                    sbtax.Append("</table>");
                    if (countryCode != "US")
                        data = data.Replace(" {taxGrid}", sbtax.ToString());
                    else
                        data = data.Replace(" {taxGrid}", string.Empty);
                }
                else
                {
                    data = data.Replace(" {taxGrid}", string.Empty);
                    data = data.Replace("{TaxTotal}", string.Empty);
                }
                data = data.Replace("{GrandTotal}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", Quote.QuoteSubTotal));
                data = data.Replace("{BalanceDue}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", Quote.QuoteSubTotal));

                if (Quote.QuoteStatusId != 2)
                    data = data.Replace("{Show}", "none");
                if (Quote.ExpiryDate != null && Quote.QuoteStatusId == 2)
                {
                    data = data.Replace("{ExpiryDate}", ((DateTime)Quote.ExpiryDate).GlobalDateFormat());
                    data = data.Replace("{Show}", "");
                }
                else if (Quote.ExpiryDate == null && Quote.QuoteStatusId == 2)
                {
                    data = data.Replace("{ExpiryDate}", "");
                    data = data.Replace("{Show}", "");
                }
            }
            catch (Exception ex)
            {
                return "Internal Server Error";
            }
            return data;
        }

        public string QuoteSheetHeader_BudgetBlinds(int QuoteKey)
        {
            string Path = string.Format("/Templates/Base/Brand/BB/BB_Quote_Template-header.html");
            string srcFilePath = HostingEnvironment.MapPath(Path);
            string data = System.IO.File.ReadAllText(srcFilePath);

            try
            {
                int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;

                var salesAgentInfo = quotemgr.GetSalesAgentByQuoteKey(QuoteKey);

                //Read lead html and append the data
                var Quote = quotemgr.GetQuoteData(QuoteKey);

                var TerritoryDisplayData = quotemgr.GetTerritoryDisplayData(QuoteKey);

                data = data.Replace("{QuoteNumber}", Quote.QuoteID.ToString());
                data = data.Replace("{Quotedate}", TimeZoneManager.ToLocal(DateTime.UtcNow).GlobalDateFormat());
                data = data.Replace("{SalesPersonName}", salesAgentInfo != null ? salesAgentInfo.FirstName + " " + salesAgentInfo.LastName : "");

                var feAddress = SessionManager.CurrentFranchise.Address;
                string FranchisePhone = SessionManager.CurrentFranchise.BusinessPhone;

                if (FranchisePhone != null && FranchisePhone.Length > 0)
                    FranchisePhone = Convert.ToInt64(FranchisePhone).ToString(phoneFormat);
                else
                    FranchisePhone = "";

                if (TerritoryDisplayData != null && TerritoryDisplayData.UseFranchiseName == false)
                {
                    if (TerritoryDisplayData.Phone != null && TerritoryDisplayData.Phone.Length > 0)
                        TerritoryDisplayData.Phone = Convert.ToInt64(TerritoryDisplayData.Phone).ToString(phoneFormat);
                    else
                        TerritoryDisplayData.Phone = "";

                    TerritoryDisplayData.Displayname = TerritoryDisplayData.Displayname + (TerritoryDisplayData.LicenseNumber != null && TerritoryDisplayData.LicenseNumber != "" ? " - " + TerritoryDisplayData.LicenseNumber : "");

                    data = data.Replace("{FEName}", TerritoryDisplayData.Displayname);
                    data = data.Replace("{FEAddress1}", TerritoryDisplayData.Address1 != null ? TerritoryDisplayData.Address1 : "");
                    data = data.Replace("{FEAddress2}", TerritoryDisplayData.Address2 != null ? TerritoryDisplayData.Address2 : "");
                    data = data.Replace("{FECitySateZip}", TerritoryDisplayData.City + "," + TerritoryDisplayData.State + " " + TerritoryDisplayData.ZipCode);
                    data = data.Replace("{FEPhone}", TerritoryDisplayData.Phone);
                    data = data.Replace("{FEEmail}", salesAgentInfo != null ? salesAgentInfo.PrimaryEmail : SessionManager.CurrentFranchise.AdminEmail);
                    data = data.Replace("{FEWebsite}", TerritoryDisplayData.WebSiteURL != null && TerritoryDisplayData.WebSiteURL != "" ? TerritoryDisplayData.WebSiteURL : "www.budgetblinds.com");
                }
                else
                {
                    //var TerritoryDisplay = quotemgr.GetTerritoryDisplay(QuoteKey);

                    //if (TerritoryDisplay != null)
                    //{
                    //    data = data.Replace("{FEName}", TerritoryDisplay.Name);
                    //    data = data.Replace("{FEAddress1}", feAddress.Address1 != null ? feAddress.Address1 : "");
                    //    data = data.Replace("{FEAddress2}", feAddress.Address2 != null ? feAddress.Address2 : "");
                    //    data = data.Replace("{FECitySateZip}", feAddress.City + "," + feAddress.State + " " + feAddress.ZipCode);
                    //    data = data.Replace("{FEPhone}", FranchisePhone);
                    //    data = data.Replace("{FEEmail}", salesAgentInfo != null ? salesAgentInfo.PrimaryEmail : SessionManager.CurrentFranchise.AdminEmail);
                    //    data = data.Replace("{FEWebsite}", "www.budgetblinds.com");

                    //}
                    //else
                    //{
                    if (TerritoryDisplayData != null)
                        data = data.Replace("{FEName}", SessionManager.CurrentFranchise.Name + (TerritoryDisplayData.LicenseNumber != null && TerritoryDisplayData.LicenseNumber != "" ? " - " + TerritoryDisplayData.LicenseNumber : ""));
                    else
                        data = data.Replace("{FEName}", SessionManager.CurrentFranchise.Name);
                    data = data.Replace("{FEAddress1}", feAddress.Address1 != null ? feAddress.Address1 : "");
                    data = data.Replace("{FEAddress2}", feAddress.Address2 != null ? feAddress.Address2 : "");
                    data = data.Replace("{FECitySateZip}", feAddress.City + "," + feAddress.State + " " + feAddress.ZipCode);
                    data = data.Replace("{FEPhone}", FranchisePhone);
                    data = data.Replace("{FEEmail}", salesAgentInfo != null ? salesAgentInfo.PrimaryEmail : SessionManager.CurrentFranchise.AdminEmail);
                    data = data.Replace("{FEWebsite}", "www.budgetblinds.com");
                    //}
                }
            }
            catch (Exception ex)
            {
                return "";
            }
            return data;
        }

        public string QuoteSheetFooter_BudgetBlinds(int QuoteKey)
        {
            string Path = string.Format("/Templates/Base/Brand/BB/BB_Quote_Template-footer.html");
            string srcFilePath = HostingEnvironment.MapPath(Path);
            string data = System.IO.File.ReadAllText(srcFilePath);
            data = data.Replace("{printeddate}", TimeZoneManager.ToLocal(DateTime.UtcNow).GlobalDateFormat());
            return data;
        }

        public string QuoteSheet_TailorLiving(int QuoteKey, PrintVersion printVersion)
        {
            string Path = string.Empty;
            Path = string.Format("/Templates/Base/Brand/TL/TL_Quote_Template-bodycontent.html");
            string srcFilePath = HostingEnvironment.MapPath(Path);
            string data = System.IO.File.ReadAllText(srcFilePath);
            try
            {
                int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;

                var Quote = quotemgr.GetQuoteInfo(QuoteKey);
                var tax = quotemgr.GetTaxinfo(QuoteKey);
                //var taxAmt = tax.Sum(x => x.Amount);

                var Opportunity = quotemgr.GetOpportunityByQuoteKey(QuoteKey);
                var customer = quotemgr.GetCustomerByQuoteKey(QuoteKey);
                var BillingAddress = quotemgr.GetBillingAddressByQuoteKey(QuoteKey);
                var InstallationAddress = quotemgr.GetInstallationAddressByQuoteKey(QuoteKey);
                if (BillingAddress == null) BillingAddress = InstallationAddress;
                var disclaimer = ordermgr.GetFranchiseDisclaimer();
                var FEdiscounts = product_mgr.GetListDetails(3);

                if (disclaimer != null && !string.IsNullOrEmpty(disclaimer.ShortDisclaimer) && disclaimer.ShortDisclaimer != "")
                    disclaimer.ShortDisclaimer = disclaimer.ShortDisclaimer.Replace(Environment.NewLine, "<br />").Replace("\r", "<br />").Replace("\n", "<br />");

                if (disclaimer != null && !string.IsNullOrEmpty(disclaimer.LongDisclaimer) && disclaimer.LongDisclaimer != "")
                    disclaimer.LongDisclaimer = disclaimer.LongDisclaimer.Replace(Environment.NewLine, "<br />").Replace("\r", "<br />").Replace("\n", "<br />");

                if (Quote != null && !string.IsNullOrEmpty(Quote.QuoteLevelNotes) && Quote.QuoteLevelNotes != "")
                    Quote.QuoteLevelNotes = Quote.QuoteLevelNotes.Replace(Environment.NewLine, "<br />").Replace("\r", "<br />").Replace("\n", "<br />");

                var Measurementres = MeasurementMgr.Get(InstallationAddress.AddressId);
                List<MeasurementLineItemTL> MeasurementData = new List<MeasurementLineItemTL>();
                if (Measurementres != null)
                    MeasurementData = Measurementres.MeasurementTL;

                if (customer.HomePhone != null && customer.HomePhone.Length > 0)
                {
                    customer.HomePhone = Convert.ToInt64(customer.HomePhone).ToString(phoneFormat);
                    if (customer.PreferredTFN != null && customer.PreferredTFN == "H")
                        customer.HomePhone = "*" + customer.HomePhone;
                }
                else
                    customer.HomePhone = "";

                if (customer.CellPhone != null && customer.CellPhone.Length > 0)
                {
                    customer.CellPhone = Convert.ToInt64(customer.CellPhone).ToString(phoneFormat);
                    if (customer.PreferredTFN != null && customer.PreferredTFN == "C")
                        customer.CellPhone = "*" + customer.CellPhone;
                }
                else
                    customer.CellPhone = "";

                if (customer.WorkPhone != null && customer.WorkPhone.Length > 0)
                {
                    customer.WorkPhone = Convert.ToInt64(customer.WorkPhone).ToString(phoneFormat);
                    if (customer.PreferredTFN != null && customer.PreferredTFN == "W")
                        customer.WorkPhone = "*" + customer.WorkPhone;
                }
                else
                    customer.WorkPhone = "";

                if (customer.CellPhone != "")
                    data = data.Replace("{displayAccountCellPhone}", "");
                else
                    data = data.Replace("{displayAccountCellPhone}", "none");

                if (customer.HomePhone != "")
                    data = data.Replace("{displayAccountHomePhone}", "");
                else
                    data = data.Replace("{displayAccountHomePhone}", "none");

                if (customer.WorkPhone != "")
                    data = data.Replace("{displayAccountWorkPhone}", "");
                else
                    data = data.Replace("{displayAccountWorkPhone}", "none");

                //Read lead html and append the data
                data = data.Replace("{AccountCustomerName}", Quote.AccountName);
                data = data.Replace("{AccountHomePhone}", customer.HomePhone);
                data = data.Replace("{AccountCellPhone}", customer.CellPhone);
                data = data.Replace("{AccountWorkPhone}", customer.WorkPhone);
                data = data.Replace("{AccountEmail}", customer.PrimaryEmail);
                data = data.Replace("{OppSideMark}", Quote != null && Quote.SideMark != null ? Quote.SideMark : "");

                data = data.Replace("{BillingAddress1}", BillingAddress.Address1);
                data = data.Replace("{BillingAddress2}", BillingAddress.Address2);
                data = data.Replace("{BillingCityStateZip}", BillingAddress.City + ", " + BillingAddress.State + " " + BillingAddress.ZipCode);

                data = data.Replace("{InstallationAddress1}", InstallationAddress.Address1);
                data = data.Replace("{InstallationAddress2}", InstallationAddress.Address2);
                data = data.Replace("{InstallationCityStateZip}", InstallationAddress.City + ", " + InstallationAddress.State + " " + InstallationAddress.ZipCode);

                data = data.Replace("{HeaderLevelComments}", Quote != null && !string.IsNullOrEmpty(Quote.QuoteLevelNotes) && Quote.QuoteLevelNotes != "" ? "<br /><b>Comments : </b>" + Quote.QuoteLevelNotes : "");

                data = data.Replace("{shortlegaldisclaimer}", disclaimer != null ? disclaimer.ShortDisclaimer : "");
                data = data.Replace("{longlegaldisclaimer}", disclaimer != null ? disclaimer.LongDisclaimer : "");

                if (Quote.QuoteLinesVM != null && Quote.QuoteLinesVM.Count > 0)
                {
                    var products = (from ql in Quote.QuoteLinesVM where ql.ProductTypeId == 1 || ql.ProductTypeId == 2 || ql.ProductTypeId == 5 select ql).ToList();

                    if (products != null && products.Count > 0)
                    {
                        StringBuilder sbProduct = new StringBuilder();
                        sbProduct.Append("<table class='table table-quoteprint'>");
                        sbProduct.Append("<thead style='display: table-header-group;'>");
                        sbProduct.Append("<tr>");
                        //sbProduct.Append("<th>Window Name</th>");
                        sbProduct.Append("<th>Product</th>");
                        if (printVersion == PrintVersion.IncludeDiscount)
                        {
                            sbProduct.Append("<th>Sugg Resale</th>");
                            sbProduct.Append("<th>Discount</th>");
                        }
                        if (printVersion != PrintVersion.CondensedVersion)
                            sbProduct.Append("<th>Unit Price</th>");
                        sbProduct.Append("<th>Qty</th>");
                        sbProduct.Append("<th>Total</th>");
                        sbProduct.Append("</tr>");
                        sbProduct.Append("</thead>");
                        sbProduct.Append("<tbody>");

                        var CondensedGrouped = products.ToList().GroupBy(p => new { p.ProductName, p.VendorName }).Select(m => new CondensedProductVendor() { ProductName = m.Key.ProductName, VendorName = m.Key.VendorName, chk = false }).ToList();

                        foreach (var p in products)
                        {
                            if (string.IsNullOrEmpty(p.CancelReason))
                            {
                                if (printVersion == PrintVersion.CondensedVersion)
                                {
                                    if (CondensedGrouped != null)
                                    {
                                        var CondensedRecord = CondensedGrouped.Where(x => x.ProductName == p.ProductName && x.VendorName == p.VendorName && x.chk == false).FirstOrDefault();
                                        if (CondensedRecord == null)
                                            continue;
                                        else
                                            CondensedRecord.chk = true;
                                    }
                                }

                                sbProduct.Append("<tr style='border-bottom:1px solid #ddd;page-break-inside:avoid;'>");
                                var md = (from m in MeasurementData where m.id == p.MeasurementsetId select m).FirstOrDefault();

                                // Get vendor name from picjson
                                string vendorname = "";
                                if (!string.IsNullOrEmpty(p.VendorDisplayname))
                                {
                                    vendorname = p.VendorDisplayname;
                                }
                                else if (p.ProductTypeId != 1 && !string.IsNullOrEmpty(p.PICJson) && p.PICJson != "")
                                {
                                    var picjson = JsonConvert.DeserializeObject<dynamic>(p.PICJson);
                                    if (picjson != null && picjson.VendorName != null)
                                        vendorname = picjson.VendorName;
                                }
                                else
                                    vendorname = p.VendorName;

                                string prodctDesc = p.Description;

                                if (string.IsNullOrEmpty(vendorname) && p.VendorId.HasValue && p.VendorId.Value != 0)
                                {
                                    var vendormgr = new VendorManager();
                                    var vendor = vendormgr.GetLocalvendor(p.VendorId.Value);
                                    if (vendor.VendorType == 2 ||
                                        vendor.VendorType == 3)
                                    {
                                        vendorname = "";
                                    }
                                }

                                if (vendorname != "")
                                    if (printVersion == PrintVersion.CondensedVersion)
                                        prodctDesc = "<b>" + vendorname + "</b>";
                                    else
                                        prodctDesc = "<b>" + vendorname + "</b>; " + prodctDesc;

                                //if (!string.IsNullOrEmpty(p.RoomName))
                                //    sbProduct.Append("<td>" + p.RoomName + "</td>");
                                //else
                                //    sbProduct.Append("<td></td>");

                                dynamic picData = JObject.Parse(p.PICJson);
                                string picCategory = "";
                                if (picData != null)
                                {
                                    if (picData.PCategory != null) picCategory = picData.PCategory;
                                }

                                if ((p.PrintProductCategory != null && p.PrintProductCategory != "") || (picCategory != ""))
                                {
                                    picCategory = p.PrintProductCategory != null && p.PrintProductCategory != "" ? p.PrintProductCategory : picCategory;

                                    if (printVersion == PrintVersion.CondensedVersion)
                                    {
                                        if (p.ProductTypeId == 5 && p.ProductCategory == null)
                                        {
                                            sbProduct.Append("<td><b>" + picData.PCategory + "</b> - " + picCategory + "<br />");
                                        }
                                        else
                                            sbProduct.Append("<td><b>" + picCategory + "</b> - " + prodctDesc + "<br />");
                                        //if (p.ProductCategory != null && p.ProductCategory != "")
                                        //    sbProduct.Append("<td><b>" + p.ProductCategory + "</b> - " + picCategory + "<br />");
                                        //else
                                        //    sbProduct.Append("<td><b>" + picData.PCategory + "</b> - " + picCategory + "<br />");
                                    }
                                    else
                                    {
                                        if (p.ProductTypeId == 5 && p.ProductCategory == null) {
                                            sbProduct.Append("<td><b>" + picData.PCategory + "</b>-" + picCategory + "</b></br><b>" + p.ProductName + "</b><br />" + prodctDesc + "<br />");
                                        }
                                        else
                                        sbProduct.Append("<td><b>" + picCategory + "</b></br><b>" + p.ProductName + "</b><br />" + prodctDesc + "<br />");
                                        //if (p.ProductCategory != null && p.ProductCategory != "")
                                        //   sbProduct.Append("<td><b>" + p.ProductCategory + "</b>-" + picCategory + "</b></br><b>" + p.ProductName + "</b><br />" + prodctDesc + "<br />");
                                        //else
                                        //    sbProduct.Append("<td><b>" + picData.PCategory + "</b>-" + picCategory + "</b></br><b>" + p.ProductName + "</b><br />" + prodctDesc + "<br />");

                                    }
                                }
                                else
                                {
                                    if (printVersion == PrintVersion.CondensedVersion)
                                        sbProduct.Append("<td><b>" + p.ProductName + "</b> - " + prodctDesc + "<br />");
                                    else
                                        sbProduct.Append("<td><b>" + p.ProductName + "</b><br />" + prodctDesc + "<br />");
                                }

                                if ((p.Memo != null && !string.IsNullOrEmpty(p.Memo)) || (md != null && !string.IsNullOrEmpty(md.Comments)) || (!string.IsNullOrEmpty(p.CustomerNotes) && p.CustomerNotes != ""))
                                {
                                    sbProduct.Append("<p>Comments:</p>");
                                    sbProduct.Append("<p class='comment_desc'>");
                                    if (p.Memo != null && !string.IsNullOrEmpty(p.Memo))
                                    {
                                        sbProduct.Append(p.Memo);
                                        sbProduct.Append("<br />");
                                    }
                                    if (md != null && !string.IsNullOrEmpty(md.Comments))
                                    {
                                        sbProduct.Append(md.Comments);
                                        sbProduct.Append("<br />");
                                    }
                                    if (!string.IsNullOrEmpty(p.CustomerNotes) && p.CustomerNotes != "")
                                    {
                                        sbProduct.Append(p.CustomerNotes);
                                    }
                                    sbProduct.Append("</p>");
                                }
                                if (printVersion == PrintVersion.IncludeDiscount && Quote.DiscountsAndPromo != null && Quote.DiscountsAndPromo.Count > 0)
                                {
                                    var dpromo = (from dp in Quote.DiscountsAndPromo where dp.QuoteLineId == p.QuoteLineId select dp).FirstOrDefault();
                                    if (dpromo != null && dpromo.DiscountAmountPassed != null && dpromo.DiscountAmountPassed > 0)
                                    {
                                        sbProduct.Append("<p>Promo Applied:</p> Vendor promotion");
                                    }
                                }
                                sbProduct.Append("</td>");
                                if (printVersion == PrintVersion.IncludeDiscount)
                                {
                                    sbProduct.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", p.SuggestedResale) + "</td>");
                                    sbProduct.Append("<td class='quoteprint_ralign'>" + Convert.ToDecimal(p.Discount).ToString("F") + "%" + "</td>");
                                }
                                if (printVersion != PrintVersion.CondensedVersion)
                                    sbProduct.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", p.UnitPrice) + "</td>");

                                var Quantity = p.Quantity;
                                var ExtendedPrice = p.ExtendedPrice;

                                if (printVersion == PrintVersion.CondensedVersion)
                                {
                                    Quantity = products.Where(x => x.ProductName == p.ProductName && x.VendorName == p.VendorName && string.IsNullOrEmpty(x.CancelReason)).Sum(x => x.Quantity);
                                    ExtendedPrice = products.Where(x => x.ProductName == p.ProductName && x.VendorName == p.VendorName && string.IsNullOrEmpty(x.CancelReason)).Sum(x => x.ExtendedPrice);
                                }

                                sbProduct.Append("<td class='quoteprint_ralign'>" + Quantity + "</td>");
                                sbProduct.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", ExtendedPrice) + "</td>");
                                sbProduct.Append("</tr>");
                            }
                        }

                        sbProduct.Append("</tbody>");
                        sbProduct.Append("</table>");

                        data = data.Replace("{ProductSection}", sbProduct.ToString());
                    }
                    else
                        data = data.Replace("{ProductSection}", "");
                }
                else
                    data = data.Replace("{ProductSection}", "");

                if (Quote.QuoteLinesVM != null && Quote.QuoteLinesVM.Count > 0)
                {
                    var Service = (from ql in Quote.QuoteLinesVM where ql.ProductTypeId == 3 select ql).ToList();

                    if (Service != null && Service.Count > 0)
                    {
                        StringBuilder sbService = new StringBuilder();
                        sbService.Append("<table class='table table-quoteprint' style='border-bottom:1px solid #ddd;'>");
                        sbService.Append("<thead style='display: table-header-group;'>");
                        sbService.Append("<tr>");
                        sbService.Append("<th>Additional Items</th>");
                        sbService.Append("<th>Memo</th>");
                        if (printVersion == PrintVersion.IncludeDiscount)
                        {
                            sbService.Append("<th>Sugg Resale</th>");
                            sbService.Append("<th>Discount</th>");
                        }
                        sbService.Append("<th>Unit Price</th>");
                        sbService.Append("<th>Qty</th>");
                        sbService.Append("<th>Total</th>");
                        sbService.Append("</tr>");
                        sbService.Append("</thead>");
                        sbService.Append("<tbody>");
                        foreach (var s in Service)
                        {
                            sbService.Append("<tr class='border_quoteprintmemo' style='page-break-inside:avoid;'>");
                            if (!string.IsNullOrEmpty(s.ModelDescription) && s.ProductName != s.ModelDescription)
                            {
                                sbService.Append("<td>" + s.ProductName + " - " + s.ModelDescription + "</td>");
                                //sbService.Append("<td>" + s.ProductCategory + " - " + s.PrintProductCategory + "</td>");
                            }
                            else
                            {
                                sbService.Append("<td>" + s.ProductName + "</td>");
                                //sbService.Append("<td>" + s.ProductCategory + " - " + s.ProductName + "</td>");
                            }
                            sbService.Append("<td>" + s.Description + "</td>");
                            if (printVersion == PrintVersion.IncludeDiscount)
                            {
                                sbService.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", s.SuggestedResale) + "</td>");
                                sbService.Append("<td class='quoteprint_ralign'>" + Convert.ToDecimal(s.Discount).ToString("F") + "%" + "</td>");
                            }
                            sbService.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", s.UnitPrice) + "</td>");
                            sbService.Append("<td class='quoteprint_ralign'>" + s.Quantity + "</td>");
                            sbService.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", s.ExtendedPrice) + "</td>");
                            sbService.Append("</tr>");
                        }
                        sbService.Append("</tbody>");
                        sbService.Append("</table>");
                        data = data.Replace("{ServicesSection}", sbService.ToString());
                    }
                    else
                        data = data.Replace("{ServicesSection}", "");
                }
                else
                    data = data.Replace("{ServicesSection}", "");

                if (Quote.QuoteLinesVM != null && Quote.QuoteLinesVM.Count > 0)
                {
                    var discountd = (from ql in Quote.QuoteLinesVM where ql.ProductTypeId == 4 select ql).ToList();

                    if ((discountd != null && discountd.Count > 0) || (Quote != null && Quote.Discount != null && Quote.Discount > 0))
                    {
                        StringBuilder sbDiscount = new StringBuilder();
                        sbDiscount.Append("<table class='table table-quoteprint' style='border-bottom:1px solid #ddd;'style='page-break-inside:avoid;'>");
                        sbDiscount.Append("<thead style='display: table-header-group;'>");
                        sbDiscount.Append("<tr>");
                        sbDiscount.Append("<th style = 'width:30%'> Discount Summary</th>");
                        sbDiscount.Append("<th width:70%>Memo</th>");
                        sbDiscount.Append("</tr>");
                        sbDiscount.Append("</thead>");
                        sbDiscount.Append("<tbody>");
                        if (Quote != null && Quote.Discount != null && Quote.Discount > 0)
                        {
                            string DiscountName = "", DiscountDesc = "Discount";
                            if (Quote.DiscountId > 0 && FEdiscounts != null && FEdiscounts.Count > 0)
                            {
                                var dis = (from d in FEdiscounts where d.ProductID == Quote.DiscountId select d).FirstOrDefault();
                                if (dis != null)
                                {
                                    DiscountName = dis.ProductName;
                                    DiscountDesc = dis.Description;
                                }
                            }
                            string discount = "0%";
                            if (Quote.Discount != null && Quote.DiscountType == "$")
                            {
                                discount = string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", Quote.Discount);
                                if (DiscountName != "")
                                    discount += " - " + DiscountName;
                            }
                            if (Quote.Discount != null && Quote.DiscountType == "%")
                            {
                                discount = String.Format("{0:0.##}", Quote.Discount) + "%";
                                if (DiscountName != "")
                                    discount += " - " + DiscountName;
                            }
                            sbDiscount.Append("<tr class='border_quoteprintmemo'>");
                            sbDiscount.Append("<td>" + discount + "</td>");
                            sbDiscount.Append("<td>" + DiscountDesc + "</td>");
                            sbDiscount.Append("</tr>");
                        }
                        if (discountd != null && discountd.Count > 0)
                        {
                            foreach (var d in discountd)
                            {
                                string DiscountName = "", DiscountDesc = "";
                                if (d.ProductId > 0 && FEdiscounts != null && FEdiscounts.Count > 0)
                                {
                                    var dis = (from di in FEdiscounts where di.ProductID == d.ProductId select di).FirstOrDefault();
                                    if (dis != null)
                                    {
                                        DiscountName = dis.ProductName;
                                        DiscountDesc = d.Description;
                                    }
                                }
                                if (d.Discount != null && d.Discount > 0)
                                {
                                    string discoutdata = "";
                                    if (d.Discount != null)
                                    {
                                        if (d.DiscountType == "$")
                                        {
                                            discoutdata += "$" + String.Format("{0:0.##}", d.Discount);
                                        }
                                        else
                                        {
                                            discoutdata += String.Format("{0:0.##}", d.Discount) + "%";
                                        }
                                        if (DiscountName != "")
                                            discoutdata += " - " + DiscountName;
                                    }

                                    sbDiscount.Append("<tr class='border_quoteprintmemo'>");
                                    sbDiscount.Append("<td>" + discoutdata + "</td>");
                                    sbDiscount.Append("<td>" + d.Description + "</td>");
                                    sbDiscount.Append("</tr>");
                                }
                            }
                        }

                        sbDiscount.Append("</tbody>");
                        sbDiscount.Append("</table>");
                        data = data.Replace("{DiscountsSection}", sbDiscount.ToString());
                    }
                    else
                        data = data.Replace("{DiscountsSection}", "");
                }
                else
                    data = data.Replace("{DiscountsSection}", "");

                data = data.Replace("{ProductSubtotal}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", Quote.ProductSubTotal));
                data = data.Replace("{AdditionalCharges}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", Quote.AdditionalCharges));
                data = data.Replace("{SpecialDiscounts}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", Quote.TotalDiscounts));
                data = data.Replace("{TotalAmount}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", Quote.QuoteSubTotal));

                if (Quote.TotalDiscounts > 0)
                    data = data.Replace("{displaySpecialDiscounts}", "");
                else
                    data = data.Replace("{displaySpecialDiscounts}", "none");

                var countryCode = SessionManager.CurrentFranchise.Address.CountryCode2Digits;

                if (tax != null && tax.Count() > 0)
                {
                    var taxAmt = tax.Sum(x => x.Amount);
                    data = data.Replace("{TaxTotal}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", taxAmt));
                    if (taxAmt.HasValue)
                    {
                        if (Quote.QuoteSubTotal.HasValue)
                        {
                            Quote.QuoteSubTotal = Quote.QuoteSubTotal + taxAmt;
                        }
                    }
                    var taxgrp = tax.GroupBy(x => new { x.JurisType, x.Jurisdiction, x.TaxName, x.Rate })
                        .Select(z => new { txtype = z.Key, total = z.Sum(c => c.Amount) });

                    StringBuilder sbtax = new StringBuilder();
                    sbtax.Append("<table class='table table-quoteprint' style='border-bottom:1px solid #ddd;page-break-inside:avoid;'>");
                    sbtax.Append("<thead>");
                    sbtax.Append("<tr>");
                    sbtax.Append("<th style = 'width:70%'>Tax Name</th>");
                    sbtax.Append("<th width:30%>Amount</th>");
                    sbtax.Append("</tr>");
                    sbtax.Append("</thead>");
                    sbtax.Append("<tbody>");
                    foreach (var item in taxgrp)
                    {
                        sbtax.Append("<tr class='border_quoteprintmemo'>");
                        if (!string.IsNullOrEmpty(item.txtype.TaxName))
                        {
                            sbtax.Append("<td>" + item.txtype.TaxName + "</td>");
                        }
                        else
                        {
                            sbtax.Append("<td>" + item.txtype.Jurisdiction + "</td>");
                        }
                        sbtax.Append("<td>" + item.total + "</td>");
                        sbtax.Append("</tr>");
                    }
                    sbtax.Append("</tbody>");
                    sbtax.Append("</table>");

                    if (countryCode != "US")
                        data = data.Replace(" {taxGrid}", sbtax.ToString());
                    else
                        data = data.Replace(" {taxGrid}", string.Empty);
                }
                else
                {
                    data = data.Replace("{TaxTotal}", string.Empty);
                    data = data.Replace(" {taxGrid}", string.Empty);
                }
                data = data.Replace("{GrandTotal}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", Quote.QuoteSubTotal));
                data = data.Replace("{BalanceDue}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", Quote.QuoteSubTotal));

                if (Quote.QuoteStatusId != 2)
                    data = data.Replace("{Show}", "none");
                if (Quote.ExpiryDate != null && Quote.QuoteStatusId == 2)
                {
                    data = data.Replace("{ExpiryDate}", ((DateTime)Quote.ExpiryDate).GlobalDateFormat());
                    data = data.Replace("{Show}", "");
                }
                else if (Quote.ExpiryDate == null && Quote.QuoteStatusId == 2)
                {
                    data = data.Replace("{ExpiryDate}", "");
                    data = data.Replace("{Show}", "");
                }
            }
            catch (Exception ex)
            {
                return "Internal Server Error";
            }
            return data;
        }

        public string QuoteSheetHeader_TailorLiving(int QuoteKey)
        {
            string Path = string.Format("/Templates/Base/Brand/TL/TL_Quote_Template-header.html");
            string srcFilePath = HostingEnvironment.MapPath(Path);
            string data = System.IO.File.ReadAllText(srcFilePath);

            try
            {
                int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;

                var salesAgentInfo = quotemgr.GetSalesAgentByQuoteKey(QuoteKey);

                var TerritoryDisplayData = quotemgr.GetTerritoryDisplayData(QuoteKey);

                //Read lead html and append the data
                var Quote = quotemgr.GetQuoteData(QuoteKey);

                data = data.Replace("{QuoteNumber}", Quote.QuoteID.ToString());
                data = data.Replace("{Quotedate}", TimeZoneManager.ToLocal(DateTime.UtcNow).GlobalDateFormat());
                data = data.Replace("{SalesPersonName}", salesAgentInfo != null ? salesAgentInfo.FirstName + " " + salesAgentInfo.LastName : "");

                var feAddress = SessionManager.CurrentFranchise.Address;
                string FranchisePhone = SessionManager.CurrentFranchise.BusinessPhone;

                if (FranchisePhone != null && FranchisePhone.Length > 0)
                    FranchisePhone = Convert.ToInt64(FranchisePhone).ToString(phoneFormat);
                else
                    FranchisePhone = "";

                if (TerritoryDisplayData != null && TerritoryDisplayData.UseFranchiseName == false)
                {
                    if (TerritoryDisplayData.Phone != null && TerritoryDisplayData.Phone.Length > 0)
                        TerritoryDisplayData.Phone = Convert.ToInt64(TerritoryDisplayData.Phone).ToString(phoneFormat);
                    else
                        TerritoryDisplayData.Phone = "";

                    TerritoryDisplayData.Displayname = TerritoryDisplayData.Displayname + (TerritoryDisplayData.LicenseNumber != null && TerritoryDisplayData.LicenseNumber != "" ? " - " + TerritoryDisplayData.LicenseNumber : "");

                    data = data.Replace("{FEName}", TerritoryDisplayData.Displayname);
                    data = data.Replace("{FEAddress1}", TerritoryDisplayData.Address1 != null ? TerritoryDisplayData.Address1 : "");
                    data = data.Replace("{FEAddress2}", TerritoryDisplayData.Address2 != null ? TerritoryDisplayData.Address2 : "");
                    data = data.Replace("{FECitySateZip}", TerritoryDisplayData.City + "," + TerritoryDisplayData.State + " " + TerritoryDisplayData.ZipCode);
                    data = data.Replace("{FEPhone}", TerritoryDisplayData.Phone);
                    data = data.Replace("{FEEmail}", salesAgentInfo != null ? salesAgentInfo.PrimaryEmail : SessionManager.CurrentFranchise.AdminEmail);
                    data = data.Replace("{FEWebsite}", TerritoryDisplayData.WebSiteURL != "" ? TerritoryDisplayData.WebSiteURL : "www.tailoredliving.com");
                }
                else
                {
                    //var TerritoryDisplay = quotemgr.GetTerritoryDisplay(QuoteKey);

                    //if (TerritoryDisplay != null)
                    //{
                    //    data = data.Replace("{FEName}", TerritoryDisplay.Name);
                    //    data = data.Replace("{FEAddress1}", feAddress.Address1 != null ? feAddress.Address1 : "");
                    //    data = data.Replace("{FEAddress2}", feAddress.Address2 != null ? feAddress.Address2 : "");
                    //    data = data.Replace("{FECitySateZip}", feAddress.City + "," + feAddress.State + " " + feAddress.ZipCode);
                    //    data = data.Replace("{FEPhone}", FranchisePhone);
                    //    data = data.Replace("{FEEmail}", SessionManager.CurrentFranchise.AdminEmail);
                    //    data = data.Replace("{FEWebsite}", "www.tailoredliving.com");
                    //}
                    //else
                    //{
                    if (TerritoryDisplayData != null)
                        data = data.Replace("{FEName}", SessionManager.CurrentFranchise.Name + (TerritoryDisplayData.LicenseNumber != null && TerritoryDisplayData.LicenseNumber != "" ? " - " + TerritoryDisplayData.LicenseNumber : ""));
                    else
                        data = data.Replace("{FEName}", SessionManager.CurrentFranchise.Name);
                    data = data.Replace("{FEAddress1}", feAddress.Address1 != null ? feAddress.Address1 : "");
                    data = data.Replace("{FEAddress2}", feAddress.Address2 != null ? feAddress.Address2 : "");
                    data = data.Replace("{FECitySateZip}", feAddress.City + "," + feAddress.State + " " + feAddress.ZipCode);
                    data = data.Replace("{FEPhone}", FranchisePhone);
                    data = data.Replace("{FEEmail}", SessionManager.CurrentFranchise.AdminEmail);
                    data = data.Replace("{FEWebsite}", "www.tailoredliving.com");

                    //}
                }
            }
            catch (Exception ex)
            {
                return "";
            }
            return data;
        }

        public string QuoteSheetFooter_TailorLiving(int QuoteKey)
        {
            string Path = string.Format("/Templates/Base/Brand/TL/TL_Quote_Template-footer.html");
            string srcFilePath = HostingEnvironment.MapPath(Path);
            string data = System.IO.File.ReadAllText(srcFilePath);
            data = data.Replace("{printeddate}", TimeZoneManager.ToLocal(DateTime.UtcNow).GlobalDateFormat());
            return data;
        }

        public string QuoteSheet_ConcreteCraft(int QuoteKey, PrintVersion printVersion)
        {
            string Path = string.Empty;
            Path = string.Format("/Templates/Base/Brand/CC/CC_Quote_Template-bodycontent.html");
            string srcFilePath = HostingEnvironment.MapPath(Path);
            string data = System.IO.File.ReadAllText(srcFilePath);
            try
            {
                int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;

                var Quote = quotemgr.GetQuoteInfo(QuoteKey);
                var tax = quotemgr.GetTaxinfo(QuoteKey);
                //var taxAmt = tax.Sum(x => x.Amount);

                var Opportunity = quotemgr.GetOpportunityByQuoteKey(QuoteKey);
                var customer = quotemgr.GetCustomerByQuoteKey(QuoteKey);
                var BillingAddress = quotemgr.GetBillingAddressByQuoteKey(QuoteKey);
                var InstallationAddress = quotemgr.GetInstallationAddressByQuoteKey(QuoteKey);
                if (BillingAddress == null) BillingAddress = InstallationAddress;
                var disclaimer = ordermgr.GetFranchiseDisclaimer();
                var FEdiscounts = product_mgr.GetListDetails(3);

                if (disclaimer != null && !string.IsNullOrEmpty(disclaimer.ShortDisclaimer) && disclaimer.ShortDisclaimer != "")
                    disclaimer.ShortDisclaimer = disclaimer.ShortDisclaimer.Replace(Environment.NewLine, "<br />").Replace("\r", "<br />").Replace("\n", "<br />");

                if (disclaimer != null && !string.IsNullOrEmpty(disclaimer.LongDisclaimer) && disclaimer.LongDisclaimer != "")
                    disclaimer.LongDisclaimer = disclaimer.LongDisclaimer.Replace(Environment.NewLine, "<br />").Replace("\r", "<br />").Replace("\n", "<br />");

                if (Quote != null && !string.IsNullOrEmpty(Quote.QuoteLevelNotes) && Quote.QuoteLevelNotes != "")
                    Quote.QuoteLevelNotes = Quote.QuoteLevelNotes.Replace(Environment.NewLine, "<br />").Replace("\r", "<br />").Replace("\n", "<br />");

                if (customer.HomePhone != null && customer.HomePhone.Length > 0)
                {
                    customer.HomePhone = Convert.ToInt64(customer.HomePhone).ToString(phoneFormat);
                    if (customer.PreferredTFN != null && customer.PreferredTFN == "H")
                        customer.HomePhone = "*" + customer.HomePhone;
                }
                else
                    customer.HomePhone = "";

                if (customer.CellPhone != null && customer.CellPhone.Length > 0)
                {
                    customer.CellPhone = Convert.ToInt64(customer.CellPhone).ToString(phoneFormat);
                    if (customer.PreferredTFN != null && customer.PreferredTFN == "C")
                        customer.CellPhone = "*" + customer.CellPhone;
                }
                else
                    customer.CellPhone = "";

                if (customer.WorkPhone != null && customer.WorkPhone.Length > 0)
                {
                    customer.WorkPhone = Convert.ToInt64(customer.WorkPhone).ToString(phoneFormat);
                    if (customer.PreferredTFN != null && customer.PreferredTFN == "W")
                        customer.WorkPhone = "*" + customer.WorkPhone;
                }
                else
                    customer.WorkPhone = "";

                if (customer.CellPhone != "")
                    data = data.Replace("{displayAccountCellPhone}", "");
                else
                    data = data.Replace("{displayAccountCellPhone}", "none");

                if (customer.HomePhone != "")
                    data = data.Replace("{displayAccountHomePhone}", "");
                else
                    data = data.Replace("{displayAccountHomePhone}", "none");

                if (customer.WorkPhone != "")
                    data = data.Replace("{displayAccountWorkPhone}", "");
                else
                    data = data.Replace("{displayAccountWorkPhone}", "none");

                //Read lead html and append the data
                data = data.Replace("{AccountCustomerName}", Quote.AccountName);
                data = data.Replace("{AccountHomePhone}", customer.HomePhone);
                data = data.Replace("{AccountCellPhone}", customer.CellPhone);
                data = data.Replace("{AccountWorkPhone}", customer.WorkPhone);
                data = data.Replace("{AccountEmail}", customer.PrimaryEmail);
                data = data.Replace("{OppSideMark}", Quote != null && Quote.SideMark != null ? Quote.SideMark : "");

                data = data.Replace("{BillingAddress1}", BillingAddress.Address1);
                data = data.Replace("{BillingAddress2}", BillingAddress.Address2);
                data = data.Replace("{BillingCityStateZip}", BillingAddress.City + ", " + BillingAddress.State + " " + BillingAddress.ZipCode);

                data = data.Replace("{InstallationAddress1}", InstallationAddress.Address1);
                data = data.Replace("{InstallationAddress2}", InstallationAddress.Address2);
                data = data.Replace("{InstallationCityStateZip}", InstallationAddress.City + ", " + InstallationAddress.State + " " + InstallationAddress.ZipCode);

                data = data.Replace("{HeaderLevelComments}", Quote != null && !string.IsNullOrEmpty(Quote.QuoteLevelNotes) && Quote.QuoteLevelNotes != "" ? "<br /><b>Comments : </b>" + Quote.QuoteLevelNotes : "");

                data = data.Replace("{shortlegaldisclaimer}", disclaimer != null ? disclaimer.ShortDisclaimer : "");
                data = data.Replace("{longlegaldisclaimer}", disclaimer != null ? disclaimer.LongDisclaimer : "");

                if (Quote.QuoteLinesVM != null && Quote.QuoteLinesVM.Count > 0)
                {
                    var products = (from ql in Quote.QuoteLinesVM where ql.ProductTypeId == 1 || ql.ProductTypeId == 2 || ql.ProductTypeId == 5 select ql).ToList();

                    if (products != null && products.Count > 0)
                    {
                        StringBuilder sbProduct = new StringBuilder();
                        sbProduct.Append("<table class='table table-quoteprint' >");
                        sbProduct.Append("<thead style='display: table-header-group;'>");
                        sbProduct.Append("<tr>");
                        //sbProduct.Append("<th>Window Name</th>");
                        sbProduct.Append("<th>Product</th>");
                        if (printVersion == PrintVersion.IncludeDiscount)
                        {
                            sbProduct.Append("<th>Sugg Resale</th>");
                            sbProduct.Append("<th>Discount</th>");
                        }
                        if (printVersion != PrintVersion.CondensedVersion)
                            sbProduct.Append("<th>Unit Price</th>");
                        sbProduct.Append("<th>Qty</th>");
                        sbProduct.Append("<th>Total</th>");
                        sbProduct.Append("</tr>");
                        sbProduct.Append("</thead>");
                        sbProduct.Append("<tbody>");

                        var CondensedGrouped = products.ToList().GroupBy(p => new { p.ProductName, p.VendorName }).Select(m => new CondensedProductVendor() { ProductName = m.Key.ProductName, VendorName = m.Key.VendorName, chk = false }).ToList();

                        foreach (var p in products)
                        {
                            if (string.IsNullOrEmpty(p.CancelReason))
                            {
                                if (printVersion == PrintVersion.CondensedVersion)
                                {
                                    if (CondensedGrouped != null)
                                    {
                                        var CondensedRecord = CondensedGrouped.Where(x => x.ProductName == p.ProductName && x.VendorName == p.VendorName && x.chk == false).FirstOrDefault();
                                        if (CondensedRecord == null)
                                            continue;
                                        else
                                            CondensedRecord.chk = true;
                                    }
                                }

                                // Get vendor name from picjson
                                string vendorname = "";
                                if (!string.IsNullOrEmpty(p.VendorDisplayname))
                                {
                                    vendorname = p.VendorDisplayname;
                                }
                                else if (p.ProductTypeId != 1 && !string.IsNullOrEmpty(p.PICJson) && p.PICJson != "")
                                {
                                    var picjson = JsonConvert.DeserializeObject<dynamic>(p.PICJson);
                                    if (picjson != null && picjson.VendorName != null)
                                        vendorname = picjson.VendorName;
                                }
                                else
                                    vendorname = p.VendorName;

                                string prodctDesc = p.Description;

                                if (string.IsNullOrEmpty(vendorname) && p.VendorId.HasValue && p.VendorId.Value != 0)
                                {
                                    var vendormgr = new VendorManager();
                                    var vendor = vendormgr.GetLocalvendor(p.VendorId.Value);
                                    if (vendor.VendorType == 2 ||
                                        vendor.VendorType == 3)
                                    {
                                        vendorname = "";
                                    }
                                }

                                if (vendorname != "")
                                    if (printVersion == PrintVersion.CondensedVersion)
                                        prodctDesc = "<b>" + vendorname + "</b>";
                                    else
                                        prodctDesc = "<b>" + vendorname + "</b>; " + prodctDesc;

                                sbProduct.Append("<tr style='border-bottom:1px solid #ddd;' style='page-break-inside:avoid;'>");

                                dynamic picData = JObject.Parse(p.PICJson);
                                string picCategory = "";
                                if (picData != null)
                                    if (picData.PCategory != null) picCategory = picData.PCategory;

                                if ((p.PrintProductCategory != null && p.PrintProductCategory != "") || (picCategory != ""))
                                {
                                    picCategory = p.PrintProductCategory != null && p.PrintProductCategory != "" ? p.PrintProductCategory : picCategory;

                                    if (printVersion == PrintVersion.CondensedVersion)
                                    {
                                        if (p.ProductTypeId == 5 && p.ProductCategory == null)
                                        {
                                            sbProduct.Append("<td><b>" + picData.PCategory + "</b> - " + picCategory + "<br />");
                                        }
                                        else
                                            sbProduct.Append("<td><b>" + picCategory + "</b> - " + prodctDesc + "<br />");

                                    }
                                    else
                                    {
                                        if (p.ProductTypeId == 5 && p.ProductCategory == null)
                                        {
                                            sbProduct.Append("<td><b>" + picData.PCategory + "</b>-" + picCategory + "</b></br><b>" + p.ProductName + "</b><br />" + prodctDesc + "<br />");
                                        }
                                        else
                                            sbProduct.Append("<td><b>" + picCategory + "</b></br><b>" + p.ProductName + "</b><br />" + prodctDesc + "<br />");

                                    }
                                }
                                else
                                {
                                    if (printVersion == PrintVersion.CondensedVersion)
                                        sbProduct.Append("<td><b>" + p.ProductName + "</b> - " + prodctDesc + "<br />");
                                    else
                                        sbProduct.Append("<td><b>" + p.ProductName + "</b><br />" + prodctDesc + "<br />");
                                }


                                if ((p.Memo != null && !string.IsNullOrEmpty(p.Memo)) || (!string.IsNullOrEmpty(p.CustomerNotes) && p.CustomerNotes != ""))
                                {
                                    sbProduct.Append("<p>Comments:</p>");
                                    sbProduct.Append("<p class='comment_desc'>");
                                    if ((p.Memo != null && !string.IsNullOrEmpty(p.Memo)))
                                    {
                                        sbProduct.Append(p.Memo);
                                        sbProduct.Append("<br />");
                                    }
                                    if (!string.IsNullOrEmpty(p.CustomerNotes) && p.CustomerNotes != "")
                                    {
                                        sbProduct.Append(p.CustomerNotes);
                                    }
                                    sbProduct.Append("</p>");
                                }
                                if (printVersion == PrintVersion.IncludeDiscount && Quote.DiscountsAndPromo != null && Quote.DiscountsAndPromo.Count > 0)
                                {
                                    var dpromo = (from dp in Quote.DiscountsAndPromo where dp.QuoteLineId == p.QuoteLineId select dp).FirstOrDefault();
                                    if (dpromo != null && dpromo.DiscountAmountPassed != null && dpromo.DiscountAmountPassed > 0)
                                    {
                                        sbProduct.Append("<p>Promo Applied:</p> Vendor promotion");
                                    }
                                }
                                sbProduct.Append("</td>");
                                if (printVersion == PrintVersion.IncludeDiscount)
                                {
                                    sbProduct.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", p.SuggestedResale) + "</td>");
                                    sbProduct.Append("<td class='quoteprint_ralign'>" + Convert.ToDecimal(p.Discount).ToString("F") + "%" + "</td>");
                                }
                                if (printVersion != PrintVersion.CondensedVersion)
                                    sbProduct.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", p.UnitPrice) + "</td>");

                                var Quantity = p.Quantity;
                                var ExtendedPrice = p.ExtendedPrice;

                                if (printVersion == PrintVersion.CondensedVersion)
                                {
                                    Quantity = products.Where(x => x.ProductName == p.ProductName && x.VendorName == p.VendorName && string.IsNullOrEmpty(x.CancelReason)).Sum(x => x.Quantity);
                                    ExtendedPrice = products.Where(x => x.ProductName == p.ProductName && x.VendorName == p.VendorName && string.IsNullOrEmpty(x.CancelReason)).Sum(x => x.ExtendedPrice);
                                }

                                sbProduct.Append("<td class='quoteprint_ralign'>" + Quantity + "</td>");
                                sbProduct.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", ExtendedPrice) + "</td>");
                                sbProduct.Append("</tr>");
                            }
                        }

                        sbProduct.Append("</tbody>");
                        sbProduct.Append("</table>");

                        data = data.Replace("{ProductSection}", sbProduct.ToString());
                    }
                    else
                        data = data.Replace("{ProductSection}", "");
                }
                else
                    data = data.Replace("{ProductSection}", "");

                if (Quote.QuoteLinesVM != null && Quote.QuoteLinesVM.Count > 0)
                {
                    var Service = (from ql in Quote.QuoteLinesVM where ql.ProductTypeId == 3 select ql).ToList();

                    if (Service != null && Service.Count > 0)
                    {
                        StringBuilder sbService = new StringBuilder();
                        sbService.Append("<table class='table table-quoteprint' style='border-bottom:1px solid #ddd;'>");
                        sbService.Append("<thead style='display: table-header-group;'>");
                        sbService.Append("<tr>");
                        sbService.Append("<th>Additional Items</th>");
                        sbService.Append("<th>Memo</th>");
                        if (printVersion == PrintVersion.IncludeDiscount)
                        {
                            sbService.Append("<th>Sugg Resale</th>");
                            sbService.Append("<th>Discount</th>");
                        }
                        sbService.Append("<th>Unit Price</th>");
                        sbService.Append("<th>Qty</th>");
                        sbService.Append("<th>Total</th>");
                        sbService.Append("</tr>");
                        sbService.Append("</thead>");
                        sbService.Append("<tbody>");
                        foreach (var s in Service)
                        {
                            sbService.Append("<tr class='border_quoteprintmemo'style='page-break-inside:avoid;'>");
                            if (!string.IsNullOrEmpty(s.ModelDescription) && s.ProductName != s.ModelDescription)
                            {
                                sbService.Append("<td>" + s.ProductName + " - " + s.ModelDescription + "</td>");

                            }
                            else
                            {
                                sbService.Append("<td>" + s.ProductName + "</td>");

                            }
                            sbService.Append("<td>" + s.Description + "</td>");
                            if (printVersion == PrintVersion.IncludeDiscount)
                            {
                                sbService.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", s.SuggestedResale) + "</td>");
                                sbService.Append("<td class='quoteprint_ralign'>" + Convert.ToDecimal(s.Discount).ToString("F") + "%" + "</td>");
                            }
                            sbService.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", s.UnitPrice) + "</td>");
                            sbService.Append("<td class='quoteprint_ralign'>" + s.Quantity + "</td>");
                            sbService.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", s.ExtendedPrice) + "</td>");
                            sbService.Append("</tr>");
                        }
                        sbService.Append("</tbody>");
                        sbService.Append("</table>");
                        data = data.Replace("{ServicesSection}", sbService.ToString());
                    }
                    else
                        data = data.Replace("{ServicesSection}", "");
                }
                else
                    data = data.Replace("{ServicesSection}", "");

                if (Quote.QuoteLinesVM != null && Quote.QuoteLinesVM.Count > 0)
                {
                    var discountd = (from ql in Quote.QuoteLinesVM where ql.ProductTypeId == 4 select ql).ToList();

                    if ((discountd != null && discountd.Count > 0) || (Quote != null && Quote.Discount != null && Quote.Discount > 0))
                    {
                        StringBuilder sbDiscount = new StringBuilder();
                        sbDiscount.Append("<table class='table table-quoteprint' style='border-bottom:1px solid #ddd;'style='page-break-inside:avoid;'>");
                        sbDiscount.Append("<thead style='display: table-header-group;'>");
                        sbDiscount.Append("<tr>");
                        sbDiscount.Append("<th style = 'width:30%'> Discount Summary</th>");
                        sbDiscount.Append("<th width:70%>Memo</th>");
                        sbDiscount.Append("</tr>");
                        sbDiscount.Append("</thead>");
                        sbDiscount.Append("<tbody>");
                        if (Quote != null && Quote.Discount != null && Quote.Discount > 0)
                        {
                            string DiscountName = "", DiscountDesc = "Discount";
                            if (Quote.DiscountId > 0 && FEdiscounts != null && FEdiscounts.Count > 0)
                            {
                                var dis = (from d in FEdiscounts where d.ProductID == Quote.DiscountId select d).FirstOrDefault();
                                if (dis != null)
                                {
                                    DiscountName = dis.ProductName;
                                    DiscountDesc = dis.Description;
                                }
                            }
                            string discount = "0%";
                            if (Quote.Discount != null && Quote.DiscountType == "$")
                            {
                                discount = string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", Quote.Discount);
                                if (DiscountName != "")
                                    discount += " - " + DiscountName;
                            }
                            if (Quote.Discount != null && Quote.DiscountType == "%")
                            {
                                discount = String.Format("{0:0.##}", Quote.Discount) + "%";
                                if (DiscountName != "")
                                    discount += " - " + DiscountName;
                            }
                            sbDiscount.Append("<tr class='border_quoteprintmemo'>");
                            sbDiscount.Append("<td>" + discount + "</td>");
                            sbDiscount.Append("<td>" + DiscountDesc + "</td>");
                            sbDiscount.Append("</tr>");
                        }
                        if (discountd != null && discountd.Count > 0)
                        {
                            foreach (var d in discountd)
                            {
                                string DiscountName = "", DiscountDesc = "";
                                if (d.ProductId > 0 && FEdiscounts != null && FEdiscounts.Count > 0)
                                {
                                    var dis = (from di in FEdiscounts where di.ProductID == d.ProductId select di).FirstOrDefault();
                                    if (dis != null)
                                    {
                                        DiscountName = dis.ProductName;
                                        DiscountDesc = d.Description;
                                    }
                                }
                                if (d.Discount != null && d.Discount > 0)
                                {
                                    string discoutdata = "";
                                    if (d.Discount != null)
                                    {
                                        if (d.DiscountType == "$")
                                        {
                                            discoutdata += "$" + String.Format("{0:0.##}", d.Discount);
                                        }
                                        else
                                        {
                                            discoutdata += String.Format("{0:0.##}", d.Discount) + "%";
                                        }
                                        if (DiscountName != "")
                                            discoutdata += " - " + DiscountName;
                                    }

                                    sbDiscount.Append("<tr class='border_quoteprintmemo'>");
                                    sbDiscount.Append("<td>" + discoutdata + "</td>");
                                    sbDiscount.Append("<td>" + d.Description + "</td>");
                                    sbDiscount.Append("</tr>");
                                }
                            }
                        }

                        sbDiscount.Append("</tbody>");
                        sbDiscount.Append("</table>");
                        data = data.Replace("{DiscountsSection}", sbDiscount.ToString());
                    }
                    else
                        data = data.Replace("{DiscountsSection}", "");
                }
                else
                    data = data.Replace("{DiscountsSection}", "");

                data = data.Replace("{ProductSubtotal}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", Quote.ProductSubTotal));
                data = data.Replace("{AdditionalCharges}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", Quote.AdditionalCharges));
                data = data.Replace("{SpecialDiscounts}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", Quote.TotalDiscounts));
                data = data.Replace("{TotalAmount}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", Quote.QuoteSubTotal));

                if (Quote.TotalDiscounts > 0)
                    data = data.Replace("{displaySpecialDiscounts}", "");
                else
                    data = data.Replace("{displaySpecialDiscounts}", "none");

                var countryCode = SessionManager.CurrentFranchise.Address.CountryCode2Digits;

                if (tax != null && tax.Count() > 0)
                {
                    var taxAmt = tax.Sum(x => x.Amount);
                    data = data.Replace("{TaxTotal}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", taxAmt));
                    if (taxAmt.HasValue)
                    {
                        if (Quote.QuoteSubTotal.HasValue)
                        {
                            Quote.QuoteSubTotal = Quote.QuoteSubTotal + taxAmt;
                        }
                    }
                    var taxgrp = tax.GroupBy(x => new { x.JurisType, x.Jurisdiction, x.TaxName, x.Rate })
                        .Select(z => new { txtype = z.Key, total = z.Sum(c => c.Amount) });

                    StringBuilder sbtax = new StringBuilder();
                    sbtax.Append("<table class='table table-quoteprint' style='border-bottom:1px solid #ddd;page-break-inside:avoid;'>");
                    sbtax.Append("<thead>");
                    sbtax.Append("<tr>");
                    sbtax.Append("<th style = 'width:70%'>Tax Name</th>");
                    sbtax.Append("<th width:30%>Amount</th>");
                    sbtax.Append("</tr>");
                    sbtax.Append("</thead>");
                    sbtax.Append("<tbody>");
                    foreach (var item in taxgrp)
                    {
                        sbtax.Append("<tr class='border_quoteprintmemo'>");
                        if (!string.IsNullOrEmpty(item.txtype.TaxName))
                        {
                            sbtax.Append("<td>" + item.txtype.TaxName + "</td>");
                        }
                        else
                        {
                            sbtax.Append("<td>" + item.txtype.Jurisdiction + "</td>");
                        }
                        sbtax.Append("<td>" + item.total + "</td>");
                        sbtax.Append("</tr>");
                    }
                    sbtax.Append("</tbody>");
                    sbtax.Append("</table>");

                    if (countryCode != "US")
                        data = data.Replace(" {taxGrid}", sbtax.ToString());
                    else
                        data = data.Replace(" {taxGrid}", string.Empty);
                }
                else
                {
                    data = data.Replace("{TaxTotal}", string.Empty);
                    data = data.Replace(" {taxGrid}", string.Empty);
                }
                data = data.Replace("{GrandTotal}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", Quote.QuoteSubTotal));
                data = data.Replace("{BalanceDue}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", Quote.QuoteSubTotal));

                if (Quote.QuoteStatusId != 2)
                    data = data.Replace("{Show}", "none");
                if (Quote.ExpiryDate != null && Quote.QuoteStatusId == 2)
                {
                    data = data.Replace("{ExpiryDate}", ((DateTime)Quote.ExpiryDate).GlobalDateFormat());
                    data = data.Replace("{Show}", "");
                }
                else if (Quote.ExpiryDate == null && Quote.QuoteStatusId == 2)
                {
                    data = data.Replace("{ExpiryDate}", "");
                    data = data.Replace("{Show}", "");
                }
            }
            catch (Exception ex)
            {
                return "Internal Server Error";
            }
            return data;
        }

        public string QuoteSheetHeader_ConcreteCraft(int QuoteKey)
        {
            string Path = string.Format("/Templates/Base/Brand/CC/CC_Quote_Template-header.html");
            string srcFilePath = HostingEnvironment.MapPath(Path);
            string data = System.IO.File.ReadAllText(srcFilePath);

            try
            {
                int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;

                var salesAgentInfo = quotemgr.GetSalesAgentByQuoteKey(QuoteKey);
                var TerritoryDisplayData = quotemgr.GetTerritoryDisplayData(QuoteKey);

                //Read lead html and append the data
                var Quote = quotemgr.GetQuoteData(QuoteKey);

                data = data.Replace("{QuoteNumber}", Quote.QuoteID.ToString());
                data = data.Replace("{Quotedate}", TimeZoneManager.ToLocal(DateTime.UtcNow).GlobalDateFormat());
                data = data.Replace("{SalesPersonName}", salesAgentInfo != null ? salesAgentInfo.FirstName + " " + salesAgentInfo.LastName : "");

                var feAddress = SessionManager.CurrentFranchise.Address;
                string FranchisePhone = SessionManager.CurrentFranchise.BusinessPhone;

                if (FranchisePhone != null && FranchisePhone.Length > 0)
                {
                    FranchisePhone = Convert.ToInt64(FranchisePhone).ToString(phoneFormat);
                }
                else
                    FranchisePhone = "";

                if (TerritoryDisplayData != null && TerritoryDisplayData.UseFranchiseName == false)
                {
                    if (TerritoryDisplayData.Phone != null && TerritoryDisplayData.Phone.Length > 0)
                        TerritoryDisplayData.Phone = Convert.ToInt64(TerritoryDisplayData.Phone).ToString(phoneFormat);
                    else
                        TerritoryDisplayData.Phone = "";

                    TerritoryDisplayData.Displayname = TerritoryDisplayData.Displayname + (TerritoryDisplayData.LicenseNumber != null && TerritoryDisplayData.LicenseNumber != "" ? " - " + TerritoryDisplayData.LicenseNumber : "");

                    data = data.Replace("{FEName}", TerritoryDisplayData.Displayname);
                    data = data.Replace("{FEAddress1}", TerritoryDisplayData.Address1 != null ? TerritoryDisplayData.Address1 : "");
                    data = data.Replace("{FEAddress2}", TerritoryDisplayData.Address2 != null ? TerritoryDisplayData.Address2 : "");
                    data = data.Replace("{FECitySateZip}", TerritoryDisplayData.City + "," + TerritoryDisplayData.State + " " + TerritoryDisplayData.ZipCode);
                    data = data.Replace("{FEPhone}", TerritoryDisplayData.Phone);
                    data = data.Replace("{FEEmail}", salesAgentInfo != null ? salesAgentInfo.PrimaryEmail : SessionManager.CurrentFranchise.AdminEmail);
                    data = data.Replace("{FEWebsite}", TerritoryDisplayData.WebSiteURL != "" ? TerritoryDisplayData.WebSiteURL : "www.concretecraft.com");
                }
                else
                {
                    //var TerritoryDisplay = quotemgr.GetTerritoryDisplay(QuoteKey);

                    //if (TerritoryDisplay != null)
                    //{
                    //    data = data.Replace("{FEName}", TerritoryDisplay.Name);
                    //    data = data.Replace("{FEAddress1}", feAddress.Address1 != null ? feAddress.Address1 : "");
                    //    data = data.Replace("{FEAddress2}", feAddress.Address2 != null ? feAddress.Address2 : "");
                    //    data = data.Replace("{FECitySateZip}", feAddress.City + "," + feAddress.State + " " + feAddress.ZipCode);
                    //    data = data.Replace("{FEPhone}", FranchisePhone);
                    //    data = data.Replace("{FEEmail}", SessionManager.CurrentFranchise.AdminEmail);
                    //    data = data.Replace("{FEWebsite}", "www.concretecraft.com");
                    //}
                    //else
                    //{
                    if (TerritoryDisplayData != null)
                        data = data.Replace("{FEName}", SessionManager.CurrentFranchise.Name + (TerritoryDisplayData.LicenseNumber != null && TerritoryDisplayData.LicenseNumber != "" ? " - " + TerritoryDisplayData.LicenseNumber : ""));
                    else
                        data = data.Replace("{FEName}", SessionManager.CurrentFranchise.Name);
                    data = data.Replace("{FEAddress1}", feAddress.Address1 != null ? feAddress.Address1 : "");
                    data = data.Replace("{FEAddress2}", feAddress.Address2 != null ? feAddress.Address2 : "");
                    data = data.Replace("{FECitySateZip}", feAddress.City + "," + feAddress.State + " " + feAddress.ZipCode);
                    data = data.Replace("{FEPhone}", FranchisePhone);
                    data = data.Replace("{FEEmail}", SessionManager.CurrentFranchise.AdminEmail);
                    data = data.Replace("{FEWebsite}", "www.concretecraft.com");
                    //  }
                }
            }
            catch (Exception)
            {
                return "";
            }
            return data;
        }

        public string QuoteSheetFooter_ConcreteCraft(int QuoteKey)
        {
            string Path = string.Empty;

            Path = string.Format("/Templates/Base/Brand/CC/CC_Quote_Template-footer.html");

            string srcFilePath = HostingEnvironment.MapPath(Path);

            string data = System.IO.File.ReadAllText(srcFilePath);
            data = data.Replace("{printeddate}", TimeZoneManager.ToLocal(DateTime.UtcNow).GlobalDateFormat());

            return data;
        }

        public Guid createQuotePDF(int QuoteKey, bool lineDiscount = true)
        {
            string fileDownloadName = "";
            fileDownloadName = string.Format("QuoteSheet_{0}.pdf", QuoteKey);

            PrintVersion printVersion = PrintVersion.CondensedVersion;
            if (lineDiscount)
                printVersion = PrintVersion.IncludeDiscount;
            else
                printVersion = PrintVersion.ExcludeDiscount;

            string data = QuoteSheet(QuoteKey, printVersion);
            string headerdata = QuoteSheetHeader(QuoteKey);
            string footerdata = QuoteSheetFooter(QuoteKey);

            string baseUrl = Serializer.Leads.LeadSheetReport.TemplateImageFilePath();

            // instantiate a html to pdf converter object
            HtmlToPdf converter = new HtmlToPdf();

            converter.Options.MarginTop = 20;
            converter.Options.MarginBottom = 30;
            converter.Options.MarginLeft = 10;
            converter.Options.MarginRight = 10;

            converter.Options.DisplayHeader = true;
            converter.Header.DisplayOnFirstPage = true;
            converter.Header.DisplayOnOddPages = true;
            converter.Header.DisplayOnEvenPages = true;
            converter.Header.Height = 100;

            PdfHtmlSection headerHtml = new PdfHtmlSection(headerdata, baseUrl);
            headerHtml.AutoFitHeight = HtmlToPdfPageFitMode.AutoFit;
            converter.Header.Add(headerHtml);

            converter.Options.DisplayFooter = true;
            converter.Footer.DisplayOnFirstPage = true;
            converter.Footer.DisplayOnEvenPages = true;
            converter.Footer.DisplayOnOddPages = true;
            converter.Footer.Height = 50;

            PdfHtmlSection footerHtml = new PdfHtmlSection(footerdata, baseUrl);
            footerHtml.AutoFitHeight = HtmlToPdfPageFitMode.AutoFit;
            converter.Footer.Add(footerHtml);

            // page numbers can be added using a PdfTextSection object
            PdfTextSection text = new PdfTextSection(300, 25, "Page {page_number} of {total_pages}  ", new System.Drawing.Font("Arial", 8));
            text.HorizontalAlign = PdfTextHorizontalAlign.Center;
            converter.Footer.Add(text);

            PdfDocument doc = converter.ConvertHtmlString(data, baseUrl);

            // save pdf document
            byte[] pdf = doc.Save();

            // close pdf document
            doc.Close();

            Guid fileId = new Guid();
            fileId = ordermgr.UploadFile(10, pdf, fileDownloadName);
            return fileId;
        }

        public PdfDocument createQuotePDFDoc(int QuoteKey, PrintVersion printVersion)
        {
            string fileDownloadName = "";
            fileDownloadName = string.Format("QuoteSheet_{0}.pdf", QuoteKey);

            string data = QuoteSheet(QuoteKey, printVersion);
            string headerdata = QuoteSheetHeader(QuoteKey);
            string footerdata = QuoteSheetFooter(QuoteKey);

            string baseUrl = Serializer.Leads.LeadSheetReport.TemplateImageFilePath();

            // instantiate a html to pdf converter object
            HtmlToPdf converter = new HtmlToPdf();

            converter.Options.PdfPageSize = PdfPageSize.Letter;
            converter.Options.PdfPageOrientation = PdfPageOrientation.Portrait;

            converter.Options.MarginTop = 20;
            converter.Options.MarginBottom = 30;
            converter.Options.MarginLeft = 10;
            converter.Options.MarginRight = 10;

            converter.Options.DisplayHeader = true;
            converter.Header.DisplayOnFirstPage = true;
            converter.Header.DisplayOnOddPages = true;
            converter.Header.DisplayOnEvenPages = true;
            converter.Header.Height = 100;

            PdfHtmlSection headerHtml = new PdfHtmlSection(headerdata, baseUrl);
            headerHtml.AutoFitHeight = HtmlToPdfPageFitMode.AutoFit;
            converter.Header.Add(headerHtml);

            converter.Options.DisplayFooter = true;
            converter.Footer.DisplayOnFirstPage = true;
            converter.Footer.DisplayOnEvenPages = true;
            converter.Footer.DisplayOnOddPages = true;
            converter.Footer.Height = 50;

            PdfHtmlSection footerHtml = new PdfHtmlSection(footerdata, baseUrl);
            footerHtml.AutoFitHeight = HtmlToPdfPageFitMode.AutoFit;
            converter.Footer.Add(footerHtml);

            // page numbers can be added using a PdfTextSection object
            PdfTextSection text = new PdfTextSection(300, 25, "Page {page_number} of {total_pages}  ", new System.Drawing.Font("Arial", 8));
            text.HorizontalAlign = PdfTextHorizontalAlign.Center;
            converter.Footer.Add(text);

            PdfDocument doc = converter.ConvertHtmlString(data, baseUrl);

            //MemoryStream strm = new MemoryStream();
            //doc.Save(strm);
            //strm.Position = 0;

            return doc;
        }
    }

    class CondensedProductVendor
    {
        public string ProductName { get; set; }
        public string VendorName { get; set; }
        public bool chk { get; set; }
    }
}