﻿using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// The Serializer namespace.
/// </summary>
namespace HFC.CRM.Serializer
{
    /// <summary>
    /// Class OpenXmlColumnData.
    /// </summary>
   public class OpenXmlColumnData
    {

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        /// <value>The width.</value>
        public double Width {get; set;}

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenXmlColumnData"/> class.
        /// </summary>
        public OpenXmlColumnData() 
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenXmlColumnData"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public OpenXmlColumnData(string name)
            : this()
        {
            Name = name;
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="OpenXmlColumnData"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="width">The width.</param>
        public OpenXmlColumnData(string name,double width)
            : this()
        {
            Name = name;
            Width = width;
        }
    }
}
