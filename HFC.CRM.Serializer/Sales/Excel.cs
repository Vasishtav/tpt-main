﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HFC.CRM.Managers;
using System.IO;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using HFC.CRM.Core;
using DocumentFormat.OpenXml.Validation;
using HFC.CRM.Core.Logs;

namespace HFC.CRM.Serializer.Sales
{
    public class Excel
    {
        public static MemoryStream BasicSalesInfo(string type, List<ChartsManager.SalesData> salesCollection)
        {
            MemoryStream outputStream = new MemoryStream();

            try
            {              
                //create spreadsheet  -- create a spreadsheet decument from an IO stream
                SpreadsheetDocument spreadsheet = OpenXmlUtil.CreateWorkbook(outputStream);
                Worksheet worksheet = spreadsheet.AddWorksheet("Sales");
                spreadsheet.AddBasicStyles();

                Row headRow;
                //add header row
                if (AppConfigManager.ApplicationTitle.ToLower().Contains("tailored living") && type.ToLower().Contains("category"))
                {
                    headRow = worksheet.AddHeaderRow(new OpenXmlColumnData(type),
                                               new OpenXmlColumnData("Job Number"),
                                               new OpenXmlColumnData("First Appointment"),
                                               new OpenXmlColumnData("ContractedOnUtc"),
                                               new OpenXmlColumnData("FirstName "),
                                               new OpenXmlColumnData("LastName "),
                                               new OpenXmlColumnData("Subtotal "));

                }
                else
                {
                    headRow = worksheet.AddHeaderRow(new OpenXmlColumnData(type),
                                                new OpenXmlColumnData("Job Number"),
                                                new OpenXmlColumnData("First Appointment"),
                                                new OpenXmlColumnData("ContractedOnUtc"),
                                                new OpenXmlColumnData("FirstName "),
                                                new OpenXmlColumnData("LastName "),
                                                new OpenXmlColumnData("Subtotal "),
                                                new OpenXmlColumnData("SurchargeTotal "),
                                                new OpenXmlColumnData("DiscountTotal "),
                                                new OpenXmlColumnData("Total "));
                }


                //Add data
                foreach (var sales in salesCollection)
                {
                    var row = worksheet.AddRow();
                    AddData(row, sales, type);
                }

                worksheet.Save();
#if DEBUG
                OpenXmlValidator validator = new OpenXmlValidator();
                var errors = validator.Validate(spreadsheet);
#endif

                spreadsheet.Close();
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
            }

            return outputStream;

        }


        private static void AddData(Row row, ChartsManager.SalesData item, string type)
        {
            if  (type.ToLower() == "city")
            {
                 row.AddCell(item.City);
            } else if (type.ToLower() == "zipcode")
            {
                row.AddCell(item.ZipCode);
            }
            else
            {

                if (item.KeyName != null)
                    row.AddCell(item.KeyName);
                else
                    row.AddCell("");
            }
            
           
            if (item.JobNumber != null)
                row.AddCell(item.JobNumber);
            else
                row.AddCell("");
            if (item.FirstAppointment != null)
            {
                DateTimeOffset date = (DateTimeOffset)item.FirstAppointment;
                if (date != null)
                    row.AddCell(date.DateTime);
            }
            else
                row.AddCell("");
            if (item.ContractedOnUtc != null)
            {
                DateTimeOffset date = (DateTimeOffset)item.ContractedOnUtc;
                if (date != null)
                    row.AddCell(date.DateTime);
            }
            else
                row.AddCell("");

            if (item.FirstName != null)
                row.AddCell(item.FirstName);
            else
                row.AddCell("");
            if (item.LastName != null)
                row.AddCell(item.LastName);
            else
                row.AddCell("");
            row.AddCell(item.Subtotal);

            if (AppConfigManager.ApplicationTitle.ToLower().Contains("tailored living") && type.ToLower().Contains("category"))
            {
                return;
            }
            
            row.AddCell(item.SurchargeTotal);
            row.AddCell(item.DiscountTotal);
            row.AddCell(item.Subtotal + item.SurchargeTotal - item.DiscountTotal); 
                 
        }

    }
}
