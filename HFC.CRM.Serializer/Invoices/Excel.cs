﻿using HFC.CRM.DTO.Search;

namespace HFC.CRM.Serializer.Invoices
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Spreadsheet;
    using DocumentFormat.OpenXml.Validation;

    using HFC.CRM.Core.Logs;
    using HFC.CRM.Data;

    /// <summary>
    /// Dependent on Session to convert dates to correct franchise local date
    /// </summary>
    public class Excel
    {
        /// <summary>
        ///  Exports the leads data into an Excel file
        /// </summary>
        /// <param name="outputStream"></param>
        /// <param name="invoiceCollection"></param>
        /// <returns></returns>
        public static MemoryStream BasicInvoiceInfo(List<InvoiceSearchDTO> invoiceCollection)
        {
            MemoryStream outputStream = new MemoryStream();

            try
            {
                //create spreadsheet  -- create a spreadsheet decument from an IO stream
                SpreadsheetDocument spreadsheet = OpenXmlUtil.CreateWorkbook(outputStream);
                Worksheet worksheet = spreadsheet.AddWorksheet("Invoices");
                spreadsheet.AddBasicStyles();


                //add header row
                var headRow = worksheet.AddHeaderRow(
                    new OpenXmlColumnData("Invoice Number"),
                    new OpenXmlColumnData("Status"),
                    new OpenXmlColumnData("Balance"),
                    new OpenXmlColumnData("Total"),
                    new OpenXmlColumnData("Job Number"),
                    new OpenXmlColumnData("Customer"),
                    new OpenXmlColumnData("Contact Info"),
                    new OpenXmlColumnData("Created On"));
                worksheet.Save();

                //Add data
                foreach (var invoice in invoiceCollection)
                {
                    var row = worksheet.AddRow();

                    row.AddCell(invoice.InvoiceNumber);
                    row.AddCell(((InvoiceStatusEnum)invoice.StatusEnum).ToString());
                    row.AddCell(invoice.Balance.HasValue ? invoice.Balance.Value : 0);
                    row.AddCell(invoice.NetTotal);
                    row.AddCell(invoice.JobNumber);
                    row.AddCell(invoice.FullName);

                    var contactInfo = invoice.PrimaryEmail;

                    if (invoice.PreferredTFN == "C") contactInfo = contactInfo + " " + RegexUtil.FormatUSPhone(invoice.CellPhone);
                    else if (invoice.PreferredTFN == "W")
                        contactInfo = contactInfo +  " " + RegexUtil.FormatUSPhone(invoice.WorkPhone);
                    else
                        contactInfo = contactInfo + " " + RegexUtil.FormatUSPhone(invoice.HomePhone);
                    
                    row.AddCell(contactInfo);
                    
                    row.AddCell(invoice.CreatedOn.Date.ToString());

                }

                worksheet.Save();
#if DEBUG
                OpenXmlValidator validator = new OpenXmlValidator();
                var errors = validator.Validate(spreadsheet);
#endif
                spreadsheet.Close();
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
            }            

            return outputStream;
        }
        
    }

}
