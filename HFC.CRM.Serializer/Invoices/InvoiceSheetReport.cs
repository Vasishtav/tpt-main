﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Validation;
using DocumentFormat.OpenXml.Wordprocessing;
using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using DocumentFormat.OpenXml.Office2010.Word;
using HFC.CRM.Data.Context;
using System.Data.SqlClient;
using System.Data;
using Dapper;
using SelectPdf;
using System.Globalization;
using Newtonsoft.Json;
using System.Web;
using Newtonsoft.Json.Linq;
using HFC.CRM.Core.Extensions;

namespace HFC.CRM.Serializer
{
    public class InvoiceSheetReport : DataManager
    {
        private OrderManager ordermgr = new OrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private MeasurementManager MeasurementMgr = new MeasurementManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private ProductManagerTP product_mgr = new ProductManagerTP();
        public string phoneFormat = "(###) ###-####";

        public string InvoiceSheet(int orderId, PrintVersion printVersion, bool printsignature)
        {
            string data = string.Empty;
            if (SessionManager.BrandId == 1)
                data = InvoiceSheet_BudgetBlinds(orderId, printVersion, printsignature);
            else if (SessionManager.BrandId == 2)
                data = InvoiceSheet_TailorLiving(orderId, printVersion, printsignature);
            else
                data = InvoiceSheet_ConcreteCraft(orderId, printVersion, printsignature);
            return data;
        }

        public string InvoiceSheetHeader(int orderId)
        {
            string data = string.Empty;
            if (SessionManager.BrandId == 1)
                data = InvoiceSheetHeader_BudgetBlinds(orderId);
            else if (SessionManager.BrandId == 2)
                data = InvoiceSheetHeader_TailorLiving(orderId);
            else
                data = InvoiceSheetHeader_ConcreteCraft(orderId);
            return data;
        }

        public string InvoiceSheetFooter(int id)
        {
            string data = string.Empty;
            if (SessionManager.BrandId == 1)
                data = InvoiceSheetFooter_BudgetBlinds(id);
            else if (SessionManager.BrandId == 2)
                data = InvoiceSheetFooter_TailorLiving(id);
            else
                data = InvoiceSheetFooter_ConcreteCraft(id);
            return data;
        }

        public string InvoiceSheet_BudgetBlinds(int orderId, PrintVersion printVersion, bool printsignature)
        {
            string Path = string.Format("/Templates/Base/Brand/BB/BB_Invoice_Template-bodycontent.html");
            string srcFilePath = HostingEnvironment.MapPath(Path);
            string data = System.IO.File.ReadAllText(srcFilePath);
            try
            {
                int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;

                var order = ordermgr.GetOrder(orderId, FranchiseId);

                var Opportunity = ordermgr.GetOpportunityByOrderId(orderId);
                var customer = ordermgr.GetCustomerByOrderId(orderId);
                var BillingAddress = ordermgr.GetBillingAddressByOrderId(orderId);
                var InstallationAddress = ordermgr.GetInstallationAddressByOrderId(orderId);
                if (BillingAddress == null) BillingAddress = InstallationAddress;
                var disclaimer = ordermgr.GetFranchiseDisclaimer();
                var FEdiscounts = product_mgr.GetListDetails(3);

                if (disclaimer != null && !string.IsNullOrEmpty(disclaimer.ShortDisclaimer) && disclaimer.ShortDisclaimer != "")
                    disclaimer.ShortDisclaimer = disclaimer.ShortDisclaimer.Replace(Environment.NewLine, "<br />").Replace("\r", "<br />").Replace("\n", "<br />");

                if (disclaimer != null && !string.IsNullOrEmpty(disclaimer.LongDisclaimer) && disclaimer.LongDisclaimer != "")
                    disclaimer.LongDisclaimer = disclaimer.LongDisclaimer.Replace(Environment.NewLine, "<br />").Replace("\r", "<br />").Replace("\n", "<br />");

                if (order != null && !string.IsNullOrEmpty(order.OrderInvoiceLevelNotes) && order.OrderInvoiceLevelNotes != "")
                    order.OrderInvoiceLevelNotes = order.OrderInvoiceLevelNotes.Replace(Environment.NewLine, "<br />").Replace("\r", "<br />").Replace("\n", "<br />");

                var Measurementres = MeasurementMgr.Get(InstallationAddress.AddressId);
                List<MeasurementLineItemBB> MeasurementData = new List<MeasurementLineItemBB>();
                if (Measurementres != null)
                    MeasurementData = Measurementres.MeasurementBB;

                if (customer.HomePhone != null && customer.HomePhone.Length > 0)
                {
                    customer.HomePhone = Convert.ToInt64(customer.HomePhone).ToString(phoneFormat);
                    if (customer.PreferredTFN != null && customer.PreferredTFN == "H")
                        customer.HomePhone = "*" + customer.HomePhone;
                }
                else
                    customer.HomePhone = "";

                if (customer.CellPhone != null && customer.CellPhone.Length > 0)
                {
                    customer.CellPhone = Convert.ToInt64(customer.CellPhone).ToString(phoneFormat);
                    if (customer.PreferredTFN != null && customer.PreferredTFN == "C")
                        customer.CellPhone = "*" + customer.CellPhone;
                }
                else
                    customer.CellPhone = "";

                if (customer.WorkPhone != null && customer.WorkPhone.Length > 0)
                {
                    customer.WorkPhone = Convert.ToInt64(customer.WorkPhone).ToString(phoneFormat);
                    if (customer.PreferredTFN != null && customer.PreferredTFN == "W")
                        customer.WorkPhone = "*" + customer.WorkPhone;
                }
                else
                    customer.WorkPhone = "";

                if (customer.CellPhone != "")
                    data = data.Replace("{displayAccountCellPhone}", "");
                else
                    data = data.Replace("{displayAccountCellPhone}", "none");

                if (customer.HomePhone != "")
                    data = data.Replace("{displayAccountHomePhone}", "");
                else
                    data = data.Replace("{displayAccountHomePhone}", "none");

                if (customer.WorkPhone != "")
                    data = data.Replace("{displayAccountWorkPhone}", "");
                else
                    data = data.Replace("{displayAccountWorkPhone}", "none");

                //Read lead html and append the data
                data = data.Replace("{AccountCustomerName}", order.AccountName);
                data = data.Replace("{AccountHomePhone}", customer.HomePhone);
                data = data.Replace("{AccountCellPhone}", customer.CellPhone);
                data = data.Replace("{AccountWorkPhone}", customer.WorkPhone);
                data = data.Replace("{AccountEmail}", customer.PrimaryEmail);
                data = data.Replace("{OppSideMark}", order != null && order.SideMark != null ? order.SideMark : "");

                data = data.Replace("{BillingAddress1}", BillingAddress.Address1);
                data = data.Replace("{BillingAddress2}", BillingAddress.Address2);
                data = data.Replace("{BillingCityStateZip}", BillingAddress.City + ", " + BillingAddress.State + " " + BillingAddress.ZipCode);

                data = data.Replace("{InstallationAddress1}", InstallationAddress.Address1);
                data = data.Replace("{InstallationAddress2}", InstallationAddress.Address2);
                data = data.Replace("{InstallationCityStateZip}", InstallationAddress.City + ", " + InstallationAddress.State + " " + InstallationAddress.ZipCode);

                //decimal OrderSubtotal = order.OrderSubtotal != null ? Convert.ToDecimal(order.OrderSubtotal) : 0m;
                //decimal Tax = order.Tax != null ? Convert.ToDecimal(order.Tax) : 0m;
                //decimal Taxperc = Tax / OrderSubtotal * 100;

                decimal Totalorderamount = order.OrderTotal != null ? Convert.ToDecimal(order.OrderTotal) : 0m;
                decimal BalanceDue = order.BalanceDue != null ? Convert.ToDecimal(order.BalanceDue) : 0m;
                decimal TotalPayments = Totalorderamount - BalanceDue;

                data = data.Replace("{ProductSubtotal}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", order.ProductSubtotal));
                data = data.Replace("{AdditionalCharges}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", order.AdditionalCharges));
                data = data.Replace("{SpecialDiscounts}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", order.OrderDiscount));
                data = data.Replace("{TotalAmount}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", order.OrderSubtotal));
                data = data.Replace("{TaxTotal}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", order.Tax));
                //data = data.Replace("{TaxPercentage}", Taxperc.ToString("F") + "%");
                data = data.Replace("{GrandTotal}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", order.OrderTotal));
                data = data.Replace("{TotalPayments}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", TotalPayments));
                data = data.Replace("{BalanceDue}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", order.BalanceDue));

                if (order.OrderDiscount > 0)
                    data = data.Replace("{displaySpecialDiscounts}", "");
                else
                    data = data.Replace("{displaySpecialDiscounts}", "none");

                data = data.Replace("{HeaderLevelComments}", order != null && !string.IsNullOrEmpty(order.OrderInvoiceLevelNotes) && order.OrderInvoiceLevelNotes != "" ? "<br /><b>Comments : </b>" + order.OrderInvoiceLevelNotes : "");

                data = data.Replace("{shortlegaldisclaimer}", disclaimer != null ? disclaimer.ShortDisclaimer : "");
                data = data.Replace("{longlegaldisclaimer}", disclaimer != null ? disclaimer.LongDisclaimer : "");

                var taxInfo = ordermgr.GetTaxDetails(orderId);

                var countryCode = SessionManager.CurrentFranchise.Address.CountryCode2Digits;

                if (countryCode != "US" && taxInfo != null && taxInfo.Count() > 0)
                {
                    var taxgrp = taxInfo.GroupBy(x => new { x.JurisType, x.Jurisdiction, x.TaxName, x.Rate })
                        .Select(z => new { txtype = z.Key, total = z.Sum(c => c.Amount) });

                    StringBuilder sbtax = new StringBuilder();
                    sbtax.Append("<table class='table table-quoteprint' style='border-bottom:1px solid #ddd;'>");
                    sbtax.Append("<thead style='display: table-header-group;'>");
                    sbtax.Append("<tr>");
                    sbtax.Append("<th style = 'width:70%'>Tax Name</th>");
                    sbtax.Append("<th width:30%>Amount</th>");
                    sbtax.Append("</tr>");
                    sbtax.Append("</thead>");
                    sbtax.Append("<tbody>");
                    foreach (var item in taxgrp)
                    {
                        sbtax.Append("<tr class='border_quoteprintmemo'>");
                        if (!string.IsNullOrEmpty(item.txtype.TaxName))
                        {
                            sbtax.Append("<td>" + item.txtype.TaxName + "</td>");
                        }
                        else
                        {
                            sbtax.Append("<td>" + item.txtype.Jurisdiction + "</td>");
                        }
                        sbtax.Append("<td>" + item.total + "</td>");
                        sbtax.Append("</tr>");
                    }
                    sbtax.Append("</tbody>");
                    sbtax.Append("</table>");
                    data = data.Replace(" {taxGrid}", sbtax.ToString());
                }
                else
                {
                    data = data.Replace(" {taxGrid}", string.Empty);
                }

                if (printsignature == true)
                    data = data.Replace("{printsignature}", "block");
                else
                    data = data.Replace("{printsignature}", "none");

                if (order.QuoteLinesVM != null && order.QuoteLinesVM.Count > 0)
                {
                    var products = (from ql in order.QuoteLinesVM where ql.ProductTypeId == 1 || ql.ProductTypeId == 2 || ql.ProductTypeId == 5 select ql).ToList();

                    if (products != null && products.Count > 0)
                    {
                        StringBuilder sbProduct = new StringBuilder();
                        sbProduct.Append("<table class='table table-quoteprint' >");
                        sbProduct.Append("<thead style='display: table-header-group;'>");
                        sbProduct.Append("<tr>");
                        if (printVersion != PrintVersion.CondensedVersion)
                            sbProduct.Append("<th>Window Name</th>");
                        sbProduct.Append("<th>Product</th>");
                        if (printVersion != PrintVersion.CondensedVersion)
                            sbProduct.Append("<th>Taxable</th>");
                        if (printVersion == PrintVersion.IncludeDiscount)
                        {
                            sbProduct.Append("<th>Sugg Resale</th>");
                            sbProduct.Append("<th>Discount</th>");
                        }
                        if (printVersion != PrintVersion.CondensedVersion)
                            sbProduct.Append("<th>Unit Price</th>");
                        sbProduct.Append("<th>Qty</th>");
                        sbProduct.Append("<th>Total</th>");
                        sbProduct.Append("</tr>");
                        sbProduct.Append("</thead>");
                        sbProduct.Append("<tbody>");

                        var CondensedGrouped = products.ToList().GroupBy(p => new { p.ProductName, p.VendorName }).Select(m => new CondensedProductVendor() { ProductName = m.Key.ProductName, VendorName = m.Key.VendorName, chk = false }).ToList();

                        foreach (var p in products)
                        {
                            if (string.IsNullOrEmpty(p.CancelReason))
                            {
                                if (printVersion == PrintVersion.CondensedVersion)
                                {
                                    if (CondensedGrouped != null)
                                    {
                                        var CondensedRecord = CondensedGrouped.Where(x => x.ProductName == p.ProductName && x.VendorName == p.VendorName && x.chk == false).FirstOrDefault();
                                        if (CondensedRecord == null)
                                            continue;
                                        else
                                            CondensedRecord.chk = true;
                                    }
                                }

                                sbProduct.Append("<tr style='border-bottom:1px solid #ddd;'>");
                                var tax = (from t in order.Taxinfo where t.QuoteLineId == p.QuoteLineId select t).FirstOrDefault();
                                string isTaxable = "";
                                if (tax != null && tax.Amount > 0)
                                    isTaxable = "Y";
                                else isTaxable = "N";

                                // Get vendor name from picjson
                                string vendorname = "";
                                if (!string.IsNullOrEmpty(p.VendorDisplayname))
                                {
                                    vendorname = p.VendorDisplayname;
                                }
                                else if (p.ProductTypeId != 1 && !string.IsNullOrEmpty(p.PICJson) && p.PICJson != "")
                                {
                                    var picjson = JsonConvert.DeserializeObject<dynamic>(p.PICJson);
                                    if (picjson != null && picjson.VendorName != null)
                                        vendorname = picjson.VendorName;
                                }
                                else
                                    vendorname = p.VendorName;

                                // Remove Hight and width in core product description
                                string prodctDesc = p.Description;

                                if (p.ProductTypeId == 1 && !string.IsNullOrEmpty(p.Description) && p.Description != "")
                                {
                                    List<string> lstdesc = p.Description.Split(';').ToList();
                                    var filterWidth = lstdesc.Where(x => x.TrimStart().StartsWith("Width: ")).ToList();

                                    if (filterWidth != null && filterWidth.Count > 0)
                                    {
                                        foreach (var item in filterWidth)
                                            lstdesc.Remove(item);
                                    }

                                    var filterHeight = lstdesc.Where(x => x.TrimStart().StartsWith("Height: ")).ToList();
                                    if (filterHeight != null && filterHeight.Count > 0)
                                    {
                                        foreach (var item in filterHeight)
                                            lstdesc.Remove(item);
                                    }

                                    prodctDesc = string.Join(",", lstdesc.ToArray());
                                }

                                if (string.IsNullOrEmpty(vendorname) && p.VendorId.HasValue && p.VendorId.Value != 0)
                                {
                                    var vendormgr = new VendorManager();
                                    var vendor = vendormgr.GetLocalvendor(p.VendorId.Value);
                                    if (vendor.VendorType == 2 ||
                                        vendor.VendorType == 3)
                                    {
                                        vendorname = "";
                                    }
                                }

                                if (vendorname != "")
                                {
                                    if (printVersion == PrintVersion.CondensedVersion)
                                        prodctDesc = "<b>" + vendorname + "</b>";
                                    else
                                        prodctDesc = "<b>" + vendorname + "</b>; " + p.Description;
                                }


                                var md = (from m in MeasurementData where m.id == p.MeasurementsetId select m).FirstOrDefault();

                                if (printVersion != PrintVersion.CondensedVersion)
                                    if (!string.IsNullOrEmpty(p.RoomName))
                                        sbProduct.Append("<td>" + p.RoomName + "</td>");
                                    else
                                        sbProduct.Append("<td></td>");

                                if (printVersion == PrintVersion.CondensedVersion)
                                    sbProduct.Append("<td><b>" + p.ProductName + "</b> - " + prodctDesc + "<br />");
                                else
                                    sbProduct.Append("<td><b>" + p.ProductName + "</b><br />" + prodctDesc + "<br />");

                                if ((p.Memo != null && !string.IsNullOrEmpty(p.Memo)) || (md != null && !string.IsNullOrEmpty(md.Comments)) || (!string.IsNullOrEmpty(p.CustomerNotes) && p.CustomerNotes != ""))
                                {
                                    sbProduct.Append("<p>Comments:</p>");
                                    sbProduct.Append("<p class='comment_desc'>");
                                    if (p.Memo != null && !string.IsNullOrEmpty(p.Memo))
                                    {
                                        sbProduct.Append(p.Memo);
                                        sbProduct.Append("<br />");
                                    }
                                    //if (md != null && !string.IsNullOrEmpty(md.Comments))
                                    //{
                                    //    sbProduct.Append(md.Comments);
                                    //    sbProduct.Append("<br />");
                                    //}
                                    if (!string.IsNullOrEmpty(p.CustomerNotes) && p.CustomerNotes != "")
                                    {
                                        sbProduct.Append(p.CustomerNotes);
                                    }
                                    sbProduct.Append("</p>");
                                }
                                if (printVersion == PrintVersion.IncludeDiscount && order.DiscountsAndPromo != null && order.DiscountsAndPromo.Count > 0)
                                {
                                    var dpromo = (from dp in order.DiscountsAndPromo where dp.QuoteLineId == p.QuoteLineId select dp).FirstOrDefault();
                                    if (dpromo != null && dpromo.DiscountAmountPassed != null && dpromo.DiscountAmountPassed > 0)
                                    {
                                        sbProduct.Append("<p>Promo Applied:</p> Vendor promotion");
                                    }
                                }
                                sbProduct.Append("</td>");
                                if (printVersion != PrintVersion.CondensedVersion)
                                    sbProduct.Append("<td class='text-center'>" + isTaxable + "</td>");
                                if (printVersion == PrintVersion.IncludeDiscount)
                                {
                                    sbProduct.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", p.SuggestedResale) + "</td>");
                                    sbProduct.Append("<td class='quoteprint_ralign'>" + Convert.ToDecimal(p.Discount).ToString("F") + "%" + "</td>");
                                }
                                if (printVersion != PrintVersion.CondensedVersion)
                                    sbProduct.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", p.UnitPrice) + "</td>");

                                var Quantity = p.Quantity;
                                var ExtendedPrice = p.ExtendedPrice;

                                if (printVersion == PrintVersion.CondensedVersion)
                                {
                                    Quantity = products.Where(x => x.ProductName == p.ProductName && x.VendorName == p.VendorName && string.IsNullOrEmpty(x.CancelReason)).Sum(x => x.Quantity);
                                    ExtendedPrice = products.Where(x => x.ProductName == p.ProductName && x.VendorName == p.VendorName && string.IsNullOrEmpty(x.CancelReason)).Sum(x => x.ExtendedPrice);
                                }

                                sbProduct.Append("<td class='quoteprint_ralign'>" + Quantity + "</td>");
                                sbProduct.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", ExtendedPrice) + "</td>");
                                sbProduct.Append("</tr>");
                            }
                        }

                        sbProduct.Append("</tbody>");
                        sbProduct.Append("</table>");

                        data = data.Replace("{ProductSection}", sbProduct.ToString());
                    }
                    else
                        data = data.Replace("{ProductSection}", "");
                }
                else
                    data = data.Replace("{ProductSection}", "");

                if (order.QuoteLinesVM != null && order.QuoteLinesVM.Count > 0)
                {
                    var Service = (from ql in order.QuoteLinesVM where ql.ProductTypeId == 3 select ql).ToList();

                    if (Service != null && Service.Count > 0)
                    {
                        StringBuilder sbService = new StringBuilder();
                        sbService.Append("<table class='table table-quoteprint' style='border-bottom:1px solid #ddd;'>");
                        sbService.Append("<thead style='display: table-header-group;'>");
                        sbService.Append("<tr>");
                        sbService.Append("<th>Additional Items</th>");
                        sbService.Append("<th>Memo</th>");
                        sbService.Append("<th>Taxable</th>");
                        if (printVersion == PrintVersion.IncludeDiscount)
                        {
                            sbService.Append("<th>Sugg Resale</th>");
                            sbService.Append("<th>Discount</th>");
                        }
                        sbService.Append("<th>Unit Price</th>");
                        sbService.Append("<th>Qty</th>");
                        sbService.Append("<th>Total</th>");
                        sbService.Append("</tr>");
                        sbService.Append("</thead>");
                        sbService.Append("<tbody>");
                        foreach (var s in Service)
                        {
                            var tax = (from t in order.Taxinfo where t.QuoteLineId == s.QuoteLineId select t).FirstOrDefault();
                            string isTaxable = "";
                            if (tax != null && tax.Amount > 0)
                                isTaxable = "Y";
                            else isTaxable = "N";

                            sbService.Append("<tr class='border_quoteprintmemo'>");

                            if (!string.IsNullOrEmpty(s.ModelDescription) && s.ProductName != s.ModelDescription)
                            {
                                sbService.Append("<td>" + s.ProductName + " - " + s.ModelDescription + "</td>");
                            }
                            else
                            {
                                sbService.Append("<td>" + s.ProductName + "</td>");
                            }
                            sbService.Append("<td>" + s.Description + "</td>");
                            sbService.Append("<td class='text-center'>" + isTaxable + "</td>");
                            if (printVersion == PrintVersion.IncludeDiscount)
                            {
                                sbService.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", s.SuggestedResale) + "</td>");
                                sbService.Append("<td class='quoteprint_ralign'>" + Convert.ToDecimal(s.Discount).ToString("F") + "%" + "</td>");
                            }
                            sbService.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", s.UnitPrice) + "</td>");
                            sbService.Append("<td class='quoteprint_ralign'>" + s.Quantity + "</td>");
                            sbService.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", s.ExtendedPrice) + "</td>");
                            sbService.Append("</tr>");
                        }
                        sbService.Append("</tbody>");
                        sbService.Append("</table>");
                        data = data.Replace("{ServicesSection}", sbService.ToString());
                    }
                    else
                        data = data.Replace("{ServicesSection}", "");
                }
                else
                    data = data.Replace("{ServicesSection}", "");

                if (order.QuoteLinesVM != null && order.QuoteLinesVM.Count > 0)
                {
                    var discountd = (from ql in order.QuoteLinesVM where ql.ProductTypeId == 4 select ql).ToList();

                    if ((discountd != null && discountd.Count > 0) || (order.Quote != null && order.Quote.Discount != null && order.Quote.Discount > 0))
                    {
                        StringBuilder sbDiscount = new StringBuilder();
                        sbDiscount.Append("<table class='table table-quoteprint' style='border-bottom:1px solid #ddd;'>");
                        sbDiscount.Append("<thead style='display: table-header-group;'>");
                        sbDiscount.Append("<tr>");
                        sbDiscount.Append("<th style = 'width:30%'> Discount Summary</th>");
                        sbDiscount.Append("<th width:70%>Memo</th>");
                        sbDiscount.Append("</tr>");
                        sbDiscount.Append("</thead>");
                        sbDiscount.Append("<tbody>");
                        if (order.Quote != null && order.Quote.Discount != null && order.Quote.Discount > 0)
                        {
                            string DiscountName = "", DiscountDesc = "Discount";
                            if (order.Quote.DiscountId > 0 && FEdiscounts != null && FEdiscounts.Count > 0)
                            {
                                var dis = (from d in FEdiscounts where d.ProductID == order.Quote.DiscountId select d).FirstOrDefault();
                                if (dis != null)
                                {
                                    DiscountName = dis.ProductName;
                                    DiscountDesc = dis.Description;
                                }
                            }
                            string discount = "0%";
                            if (order.Quote.Discount != null && order.Quote.DiscountType == "$")
                            {
                                discount = string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", order.Quote.Discount);
                                if (DiscountName != "")
                                    discount += " - " + DiscountName;
                            }
                            if (order.Quote.Discount != null && order.Quote.DiscountType == "%")
                            {
                                discount = String.Format("{0:0.##}", order.Quote.Discount) + "%";
                                if (DiscountName != "")
                                    discount += " - " + DiscountName;
                            }
                            sbDiscount.Append("<tr class='border_quoteprintmemo'>");
                            sbDiscount.Append("<td>" + discount + "</td>");
                            sbDiscount.Append("<td>" + DiscountDesc + "</td>");
                            sbDiscount.Append("</tr>");
                        }
                        if (discountd != null && discountd.Count > 0)
                        {
                            foreach (var d in discountd)
                            {
                                string DiscountName = "", DiscountDesc = "";
                                if (d.ProductId > 0 && FEdiscounts != null && FEdiscounts.Count > 0)
                                {
                                    var dis = (from di in FEdiscounts where di.ProductID == d.ProductId select di).FirstOrDefault();
                                    if (dis != null)
                                    {
                                        DiscountName = dis.ProductName;
                                        DiscountDesc = d.Description;
                                    }
                                }
                                if (d.Discount != null && d.Discount > 0)
                                {
                                    string discoutdata = "";
                                    if (d.Discount != null)
                                    {
                                        if (d.DiscountType == "$")
                                        {
                                            discoutdata += "$" + String.Format("{0:0.##}", d.Discount);
                                        }
                                        else
                                        {
                                            discoutdata += String.Format("{0:0.##}", d.Discount) + "%";
                                        }
                                        if (DiscountName != "")
                                            discoutdata += " - " + DiscountName;
                                    }

                                    sbDiscount.Append("<tr class='border_quoteprintmemo'>");
                                    sbDiscount.Append("<td>" + discoutdata + "</td>");
                                    sbDiscount.Append("<td>" + d.Description + "</td>");
                                    sbDiscount.Append("</tr>");
                                }
                            }
                        }

                        sbDiscount.Append("</tbody>");
                        sbDiscount.Append("</table>");
                        data = data.Replace("{DiscountsSection}", sbDiscount.ToString());
                    }
                    else
                        data = data.Replace("{DiscountsSection}", "");
                }
                else
                    data = data.Replace("{DiscountsSection}", "");

                // Payment
                if (order.OrderPayments != null && order.OrderPayments.Count > 0)
                {
                    StringBuilder sbPay = new StringBuilder();

                    sbPay.Append("");
                    sbPay.Append("<table class='table table-quoteprint' style='border-bottom:1px solid #ddd;'>");
                    sbPay.Append("<thead style='display: table-header-group;'>");
                    sbPay.Append("<tr>");
                    sbPay.Append("<th>Payment Date</th>");
                    sbPay.Append("<th>Payment Method</th>");
                    sbPay.Append("<th>Memo</th>");
                    sbPay.Append("<th>Amount</th>");
                    sbPay.Append("</tr>");
                    sbPay.Append("</thead>");
                    sbPay.Append("<tbody>");
                    foreach (var pay in order.OrderPayments)
                    {
                        sbPay.Append("<tr class='border_quoteprintmemo'>");
                        sbPay.Append("<td>" + Convert.ToDateTime(pay.PaymentDate).GlobalDateFormat() + "</td>");
                        sbPay.Append("<td>" + pay.Method + "</td>");
                        sbPay.Append("<td>" + pay.Memo + "</td>");
                        sbPay.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", pay.Amount) + "</td>");
                        sbPay.Append("</tr>");
                    }
                    sbPay.Append("</tbody>");
                    sbPay.Append("</table>");

                    data = data.Replace("{PaymentSection}", sbPay.ToString());
                }
                else
                    data = data.Replace("{PaymentSection}", "");

                var customerViewData = ordermgr.getOrderCustomerViewTable(orderId);
                string customerSignStreamId = "";
                string salesRepSignStreamId = "";
                string customerSignedDate = "";
                string salesRepSignedDate = "";

                if (customerViewData!=null)
                {
                    customerSignStreamId = customerViewData.CustomerSignStreamId;
                    salesRepSignStreamId = customerViewData.SalesRepSignStreamId;
                    customerSignedDate = Convert.ToDateTime(customerViewData.CustomerSignedDate).GlobalDateFormat();
                    salesRepSignedDate = Convert.ToDateTime(customerViewData.SalesRepSignedDate).GlobalDateFormat();
                }

                if(!string.IsNullOrEmpty(customerSignStreamId))
                {
                    var binary = Convert.ToBase64String(FileTable.GetFileData(customerSignStreamId).bytes);
                    data = data.Replace("{CustomerSignature}", "data:image/png;base64," +binary);
                    data = data.Replace("{CustomerSignedDate}", customerSignedDate);
                }
                else
                {
                    data = data.Replace("{displayCustomerSignature}", "none");
                    data = data.Replace("{displayCustomerSignedDate}", "none");
                }


                if (!string.IsNullOrEmpty(salesRepSignStreamId))
                {
                    var binary = Convert.ToBase64String(FileTable.GetFileData(salesRepSignStreamId).bytes);
                    data = data.Replace("{SalesRepSignature}", "data:image/png;base64," + binary);
                    data = data.Replace("{SalesRepSignedDate}", salesRepSignedDate);
                }
                else
                {
                    data = data.Replace("{displaySalesRepSignature}", "none");
                    data = data.Replace("{displaySalesRepSignedDate}", "none");
                }

            }
            catch (Exception ex) { return "Internal Server Error"; }
            return data;
        }

        public string InvoiceSheetHeader_BudgetBlinds(int orderId)
        {
            string Path = string.Format("/Templates/Base/Brand/BB/BB_Invoice_Template-header.html");
            string srcFilePath = HostingEnvironment.MapPath(Path);
            string data = System.IO.File.ReadAllText(srcFilePath);

            try
            {
                int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;

                var salesAgentInfo = ordermgr.GetSalesAgentByOrderId(orderId);
                var order = ordermgr.GetOrderByOrderId(orderId);

                var TerritoryDisplayData = ordermgr.GetTerritoryDisplayData(orderId);
                string ContractedDate = order.ContractedDate?.GlobalDateFormat();
                //Read lead html and append the data

                data = data.Replace("{InvoiceNumber}", order != null ? order.OrderNumber.ToString() : "0");
                data = data.Replace("{Invoicedate}", order?.ContractedDate?.GlobalDateFormat());
                data = data.Replace("{SalesPersonName}", salesAgentInfo != null ? salesAgentInfo.FirstName + " " + salesAgentInfo.LastName : "");

                var feAddress = SessionManager.CurrentFranchise.Address;
                string FranchisePhone = SessionManager.CurrentFranchise.BusinessPhone;

                if (FranchisePhone != null && FranchisePhone.Length > 0)
                    FranchisePhone = Convert.ToInt64(FranchisePhone).ToString(phoneFormat);
                else
                    FranchisePhone = "";

                if (TerritoryDisplayData != null && TerritoryDisplayData.UseFranchiseName == false)
                {
                    if (TerritoryDisplayData.Phone != null && TerritoryDisplayData.Phone.Length > 0)
                        TerritoryDisplayData.Phone = Convert.ToInt64(TerritoryDisplayData.Phone).ToString(phoneFormat);
                    else
                        TerritoryDisplayData.Phone = "";

                    TerritoryDisplayData.Displayname = TerritoryDisplayData.Displayname + (TerritoryDisplayData.LicenseNumber != null && TerritoryDisplayData.LicenseNumber != "" ? " - " + TerritoryDisplayData.LicenseNumber : "");

                    data = data.Replace("{FEName}", TerritoryDisplayData.Displayname);
                    data = data.Replace("{FEAddress1}", TerritoryDisplayData.Address1 != null ? TerritoryDisplayData.Address1 : "");
                    data = data.Replace("{FEAddress2}", TerritoryDisplayData.Address2 != null ? TerritoryDisplayData.Address2 : "");
                    data = data.Replace("{FECitySateZip}", TerritoryDisplayData.City + "," + TerritoryDisplayData.State + " " + TerritoryDisplayData.ZipCode);
                    data = data.Replace("{FEPhone}", TerritoryDisplayData.Phone);
                    data = data.Replace("{FEEmail}", salesAgentInfo != null ? salesAgentInfo.PrimaryEmail : SessionManager.CurrentFranchise.AdminEmail);
                    data = data.Replace("{FEWebsite}", TerritoryDisplayData.WebSiteURL != null && TerritoryDisplayData.WebSiteURL != "" ? TerritoryDisplayData.WebSiteURL : "www.budgetblinds.com");
                }
                else
                {
                    //var TerritoryDisplay = ordermgr.GetTerritoryDisplay(orderId);

                    //if(TerritoryDisplay != null)
                    //{
                    //    data = data.Replace("{FEName}", TerritoryDisplay.Name);
                    //    data = data.Replace("{FEAddress1}", feAddress.Address1 != null ? feAddress.Address1 : "");
                    //    data = data.Replace("{FEAddress2}", feAddress.Address2 != null ? feAddress.Address2 : "");
                    //    data = data.Replace("{FECitySateZip}", feAddress.City + "," + feAddress.State + " " + feAddress.ZipCode);
                    //    data = data.Replace("{FEPhone}", FranchisePhone);
                    //    data = data.Replace("{FEEmail}", salesAgentInfo != null ? salesAgentInfo.PrimaryEmail : SessionManager.CurrentFranchise.AdminEmail);
                    //    data = data.Replace("{FEWebsite}", "www.budgetblinds.com");
                    //}
                    //else
                    //{
                    if (TerritoryDisplayData != null)
                        data = data.Replace("{FEName}", SessionManager.CurrentFranchise.Name + (TerritoryDisplayData.LicenseNumber != null && TerritoryDisplayData.LicenseNumber != "" ? " - " + TerritoryDisplayData.LicenseNumber : ""));
                    else
                        data = data.Replace("{FEName}", SessionManager.CurrentFranchise.Name);
                    data = data.Replace("{FEAddress1}", feAddress.Address1 != null ? feAddress.Address1 : "");
                    data = data.Replace("{FEAddress2}", feAddress.Address2 != null ? feAddress.Address2 : "");
                    data = data.Replace("{FECitySateZip}", feAddress.City + "," + feAddress.State + " " + feAddress.ZipCode);
                    data = data.Replace("{FEPhone}", FranchisePhone);
                    data = data.Replace("{FEEmail}", salesAgentInfo != null ? salesAgentInfo.PrimaryEmail : SessionManager.CurrentFranchise.AdminEmail);
                    data = data.Replace("{FEWebsite}", "www.budgetblinds.com");
                    //   }
                }
            }
            catch (Exception)
            {
                return "";
            }

            return data;
        }

        public string InvoiceSheetFooter_BudgetBlinds(int id)
        {
            string Path = string.Format("/Templates/Base/Brand/BB/BB_Invoice_Template-footer.html");
            string srcFilePath = HostingEnvironment.MapPath(Path);
            string data = System.IO.File.ReadAllText(srcFilePath);
            data = data.Replace("{printeddate}", TimeZoneManager.ToLocal(DateTime.UtcNow).GlobalDateFormat());
            return data;
        }

        public string InvoiceSheet_TailorLiving(int orderId, PrintVersion printVersion, bool printsignature)
        {
            string Path = string.Format("/Templates/Base/Brand/TL/TL_Invoice_Template-bodycontent.html");
            string srcFilePath = HostingEnvironment.MapPath(Path);
            string data = System.IO.File.ReadAllText(srcFilePath);
            try
            {
                int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;

                var order = ordermgr.GetOrder(orderId, FranchiseId);

                var Opportunity = ordermgr.GetOpportunityByOrderId(orderId);
                var customer = ordermgr.GetCustomerByOrderId(orderId);
                var BillingAddress = ordermgr.GetBillingAddressByOrderId(orderId);
                var InstallationAddress = ordermgr.GetInstallationAddressByOrderId(orderId);
                if (BillingAddress == null) BillingAddress = InstallationAddress;
                var disclaimer = ordermgr.GetFranchiseDisclaimer();
                var FEdiscounts = product_mgr.GetListDetails(3);

                if (disclaimer != null && !string.IsNullOrEmpty(disclaimer.ShortDisclaimer) && disclaimer.ShortDisclaimer != "")
                    disclaimer.ShortDisclaimer = disclaimer.ShortDisclaimer.Replace(Environment.NewLine, "<br />").Replace("\r", "<br />").Replace("\n", "<br />");

                if (disclaimer != null && !string.IsNullOrEmpty(disclaimer.LongDisclaimer) && disclaimer.LongDisclaimer != "")
                    disclaimer.LongDisclaimer = disclaimer.LongDisclaimer.Replace(Environment.NewLine, "<br />").Replace("\r", "<br />").Replace("\n", "<br />");

                if (order != null && !string.IsNullOrEmpty(order.OrderInvoiceLevelNotes) && order.OrderInvoiceLevelNotes != "")
                    order.OrderInvoiceLevelNotes = order.OrderInvoiceLevelNotes.Replace(Environment.NewLine, "<br />").Replace("\r", "<br />").Replace("\n", "<br />");

                var Measurementres = MeasurementMgr.Get(InstallationAddress.AddressId);
                List<MeasurementLineItemTL> MeasurementData = new List<MeasurementLineItemTL>();
                if (Measurementres != null)
                    MeasurementData = Measurementres.MeasurementTL;

                if (customer.HomePhone != null && customer.HomePhone.Length > 0)
                {
                    customer.HomePhone = Convert.ToInt64(customer.HomePhone).ToString(phoneFormat);
                    if (customer.PreferredTFN != null && customer.PreferredTFN == "H")
                        customer.HomePhone = "*" + customer.HomePhone;
                }
                else
                    customer.HomePhone = "";

                if (customer.CellPhone != null && customer.CellPhone.Length > 0)
                {
                    customer.CellPhone = Convert.ToInt64(customer.CellPhone).ToString(phoneFormat);
                    if (customer.PreferredTFN != null && customer.PreferredTFN == "C")
                        customer.CellPhone = "*" + customer.CellPhone;
                }
                else
                    customer.CellPhone = "";

                if (customer.WorkPhone != null && customer.WorkPhone.Length > 0)
                {
                    customer.WorkPhone = Convert.ToInt64(customer.WorkPhone).ToString(phoneFormat);
                    if (customer.PreferredTFN != null && customer.PreferredTFN == "W")
                        customer.WorkPhone = "*" + customer.WorkPhone;
                }
                else
                    customer.WorkPhone = "";

                if (customer.CellPhone != "")
                    data = data.Replace("{displayAccountCellPhone}", "");
                else
                    data = data.Replace("{displayAccountCellPhone}", "none");

                if (customer.HomePhone != "")
                    data = data.Replace("{displayAccountHomePhone}", "");
                else
                    data = data.Replace("{displayAccountHomePhone}", "none");

                if (customer.WorkPhone != "")
                    data = data.Replace("{displayAccountWorkPhone}", "");
                else
                    data = data.Replace("{displayAccountWorkPhone}", "none");

                //Read lead html and append the data
                data = data.Replace("{AccountCustomerName}", order.AccountName);
                data = data.Replace("{AccountHomePhone}", customer.HomePhone);
                data = data.Replace("{AccountCellPhone}", customer.CellPhone);
                data = data.Replace("{AccountWorkPhone}", customer.WorkPhone);
                data = data.Replace("{AccountEmail}", customer.PrimaryEmail);
                data = data.Replace("{OppSideMark}", order != null && order.SideMark != null ? order.SideMark : "");

                data = data.Replace("{BillingAddress1}", BillingAddress.Address1);
                data = data.Replace("{BillingAddress2}", BillingAddress.Address2);
                data = data.Replace("{BillingCityStateZip}", BillingAddress.City + ", " + BillingAddress.State + " " + BillingAddress.ZipCode);

                data = data.Replace("{InstallationAddress1}", InstallationAddress.Address1);
                data = data.Replace("{InstallationAddress2}", InstallationAddress.Address2);
                data = data.Replace("{InstallationCityStateZip}", InstallationAddress.City + ", " + InstallationAddress.State + " " + InstallationAddress.ZipCode);

                //decimal OrderSubtotal = order.OrderSubtotal != null ? Convert.ToDecimal(order.OrderSubtotal) : 0m;
                //decimal Tax = order.Tax != null ? Convert.ToDecimal(order.Tax) : 0m;
                //decimal Taxperc = Tax / OrderSubtotal * 100;

                decimal Totalorderamount = order.OrderTotal != null ? Convert.ToDecimal(order.OrderTotal) : 0m;
                decimal BalanceDue = order.BalanceDue != null ? Convert.ToDecimal(order.BalanceDue) : 0m;
                decimal TotalPayments = Totalorderamount - BalanceDue;

                data = data.Replace("{ProductSubtotal}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", order.ProductSubtotal));
                data = data.Replace("{AdditionalCharges}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", order.AdditionalCharges));
                data = data.Replace("{SpecialDiscounts}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", order.OrderDiscount));
                data = data.Replace("{TotalAmount}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", order.OrderSubtotal));
                data = data.Replace("{TaxTotal}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", order.Tax));
                //data = data.Replace("{TaxPercentage}", Taxperc.ToString("F") + "%");
                data = data.Replace("{GrandTotal}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", order.OrderTotal));
                data = data.Replace("{TotalPayments}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", TotalPayments));
                data = data.Replace("{BalanceDue}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", order.BalanceDue));

                if (order.OrderDiscount > 0)
                    data = data.Replace("{displaySpecialDiscounts}", "");
                else
                    data = data.Replace("{displaySpecialDiscounts}", "none");

                data = data.Replace("{HeaderLevelComments}", order != null && !string.IsNullOrEmpty(order.OrderInvoiceLevelNotes) && order.OrderInvoiceLevelNotes != "" ? "<br /><b>Comments : </b>" + order.OrderInvoiceLevelNotes : "");

                data = data.Replace("{shortlegaldisclaimer}", disclaimer != null ? disclaimer.ShortDisclaimer : "");
                data = data.Replace("{longlegaldisclaimer}", disclaimer != null ? disclaimer.LongDisclaimer : "");

                var taxInfo = ordermgr.GetTaxDetails(orderId);

                var countryCode = SessionManager.CurrentFranchise.Address.CountryCode2Digits;

                if (countryCode != "US" && taxInfo != null && taxInfo.Count() > 0)
                {
                    var taxgrp = taxInfo.GroupBy(x => new { x.JurisType, x.Jurisdiction, x.TaxName, x.Rate })
                        .Select(z => new { txtype = z.Key, total = z.Sum(c => c.Amount) });

                    StringBuilder sbtax = new StringBuilder();
                    sbtax.Append("<table class='table table-quoteprint' style='border-bottom:1px solid #ddd;page-break-inside:avoid;'>");
                    sbtax.Append("<thead>");
                    sbtax.Append("<tr>");
                    sbtax.Append("<th style = 'width:70%'>Tax Name</th>");
                    sbtax.Append("<th width:30%>Amount</th>");
                    sbtax.Append("</tr>");
                    sbtax.Append("</thead>");
                    sbtax.Append("<tbody>");
                    foreach (var item in taxgrp)
                    {
                        sbtax.Append("<tr class='border_quoteprintmemo'>");
                        if (!string.IsNullOrEmpty(item.txtype.TaxName))
                        {
                            sbtax.Append("<td>" + item.txtype.TaxName + "</td>");
                        }
                        else
                        {
                            sbtax.Append("<td>" + item.txtype.Jurisdiction + "</td>");
                        }
                        sbtax.Append("<td>" + item.total + "</td>");
                        sbtax.Append("</tr>");
                    }
                    sbtax.Append("</tbody>");
                    sbtax.Append("</table>");
                    data = data.Replace(" {taxGrid}", sbtax.ToString());
                }
                else
                {
                    data = data.Replace(" {taxGrid}", string.Empty);
                }

                if (printsignature == true)
                    data = data.Replace("{printsignature}", "block");
                else
                    data = data.Replace("{printsignature}", "none");

                if (order.QuoteLinesVM != null && order.QuoteLinesVM.Count > 0)
                {
                    var products = (from ql in order.QuoteLinesVM where ql.ProductTypeId == 1 || ql.ProductTypeId == 2 || ql.ProductTypeId == 5 select ql).ToList();

                    if (products != null && products.Count > 0)
                    {
                        StringBuilder sbProduct = new StringBuilder();
                        sbProduct.Append("<table class='table table-quoteprint' >");
                        sbProduct.Append("<thead style='display: table-header-group;'>");
                        sbProduct.Append("<tr>");
                        //sbProduct.Append("<th>Window Name</th>");
                        sbProduct.Append("<th>Product</th>");
                        if (printVersion != PrintVersion.CondensedVersion)

                            sbProduct.Append("<th>Taxable</th>");
                        if (printVersion == PrintVersion.IncludeDiscount)
                        {
                            sbProduct.Append("<th>Sugg Resale</th>");
                            sbProduct.Append("<th>Discount</th>");
                        }
                        if (printVersion != PrintVersion.CondensedVersion)

                            sbProduct.Append("<th>Unit Price</th>");
                        sbProduct.Append("<th>Qty</th>");
                        sbProduct.Append("<th>Total</th>");
                        sbProduct.Append("</tr>");
                        sbProduct.Append("</thead>");
                        sbProduct.Append("<tbody>");

                        var CondensedGrouped = products.ToList().GroupBy(p => new { p.ProductName, p.VendorName }).Select(m => new CondensedProductVendor() { ProductName = m.Key.ProductName, VendorName = m.Key.VendorName, chk = false }).ToList();

                        foreach (var p in products)
                        {
                            if (string.IsNullOrEmpty(p.CancelReason))
                            {
                                if (printVersion == PrintVersion.CondensedVersion)
                                {
                                    if (CondensedGrouped != null)
                                    {
                                        var CondensedRecord = CondensedGrouped.Where(x => x.ProductName == p.ProductName && x.VendorName == p.VendorName && x.chk == false).FirstOrDefault();
                                        if (CondensedRecord == null)
                                            continue;
                                        else
                                            CondensedRecord.chk = true;
                                    }
                                }

                                sbProduct.Append("<tr style='border-bottom:1px solid #ddd;'>");
                                var tax = (from t in order.Taxinfo where t.QuoteLineId == p.QuoteLineId select t).FirstOrDefault();
                                string isTaxable = "";
                                if (tax != null && tax.Amount > 0)
                                    isTaxable = "Y";
                                else isTaxable = "N";

                                var md = (from m in MeasurementData where m.id == p.MeasurementsetId select m).FirstOrDefault();

                                // Get vendor name from picjson
                                string vendorname = "";
                                if (!string.IsNullOrEmpty(p.VendorDisplayname))
                                {
                                    vendorname = p.VendorDisplayname;
                                }
                                else if (p.ProductTypeId != 1 && !string.IsNullOrEmpty(p.PICJson) && p.PICJson != "")
                                {
                                    var picjson = JsonConvert.DeserializeObject<dynamic>(p.PICJson);
                                    if (picjson != null && picjson.VendorName != null)
                                        vendorname = picjson.VendorName;
                                }
                                else
                                    vendorname = p.VendorName;

                                string prodctDesc = p.Description;

                                if (string.IsNullOrEmpty(vendorname) && p.VendorId.HasValue && p.VendorId.Value != 0)
                                {
                                    var vendormgr = new VendorManager();
                                    var vendor = vendormgr.GetLocalvendor(p.VendorId.Value);
                                    if (vendor.VendorType == 2 ||
                                        vendor.VendorType == 3)
                                    {
                                        vendorname = "";
                                    }
                                }

                                if (vendorname != "")
                                {
                                    if (printVersion == PrintVersion.CondensedVersion)
                                        prodctDesc = "<b>" + vendorname + "</b>; " + p.Description;
                                    else
                                        prodctDesc = "<b>" + vendorname + "</b>; " + p.Description;
                                }


                                dynamic picData = JObject.Parse(p.PICJson);
                                string picCategory = "";
                                if (picData != null)
                                {
                                    if (picData.PCategory != null) picCategory = picData.PCategory;
                                }

                                if ((p.PrintProductCategory != null && p.PrintProductCategory != "") || (picCategory != ""))
                                {
                                    picCategory = p.PrintProductCategory != null && p.PrintProductCategory != "" ? p.PrintProductCategory : picCategory;
                                    if (printVersion == PrintVersion.CondensedVersion)
                                    {
                                        if (p.ProductTypeId == 5 && p.ProductCategory == null)
                                        {
                                            sbProduct.Append("<td><b>" + picData.PCategory + "</b> - " + picCategory + "<br />");
                                        }
                                        else
                                            sbProduct.Append("<td><b>" + picCategory + "</b> - " + prodctDesc + "<br />");
                                        //if (p.ProductCategory != null && p.ProductCategory != "")
                                        //    sbProduct.Append("<td><b>" + p.ProductCategory + "</b> - " + picCategory + "<br />");
                                        //else
                                        //    sbProduct.Append("<td><b>" + picData.PCategory + "</b> - " + picCategory + "<br />");
                                    }
                                    else
                                    {
                                        if (p.ProductTypeId == 5 && p.ProductCategory == null)
                                        {
                                            sbProduct.Append("<td><b>" + picData.PCategory + "</b>-" + picCategory + "</b></br><b>" + p.ProductName + "</b><br />" + prodctDesc + "<br />");
                                        }
                                        else
                                            sbProduct.Append("<td><b>" + picCategory + "</b></br><b>" + p.ProductName + "</b><br />" + prodctDesc + "<br />");
                                        //if (p.ProductCategory != null && p.ProductCategory != "")
                                        //    sbProduct.Append("<td><b>" + p.ProductCategory + "</b>-" + picCategory + "</b></br><b>" + p.ProductName + "</b><br />" + prodctDesc + "<br />");
                                        //else
                                        //    sbProduct.Append("<td><b>" + picData.PCategory + "</b>-" + picCategory + "</b></br><b>" + p.ProductName + "</b><br />" + prodctDesc + "<br />");
                                    }
                                }
                                else
                                {
                                    if (printVersion == PrintVersion.CondensedVersion)
                                        sbProduct.Append("<td><b>" + p.ProductName + "</b> - " + prodctDesc + "<br />");
                                    else
                                        sbProduct.Append("<td><b>" + p.ProductName + "</b><br />" + prodctDesc + "<br />");
                                }


                                //  sbProduct.Append("<td><b>" + p.ProductName + "</b><br />" + prodctDesc + "<br />");
                                if ((p.Memo != null && !string.IsNullOrEmpty(p.Memo)) || (md != null && !string.IsNullOrEmpty(md.Comments)) || (!string.IsNullOrEmpty(p.CustomerNotes) && p.CustomerNotes != ""))
                                {
                                    sbProduct.Append("<p>Comments:</p>");
                                    sbProduct.Append("<p class='comment_desc'>");
                                    if (p.Memo != null && !string.IsNullOrEmpty(p.Memo))
                                    {
                                        sbProduct.Append(p.Memo);
                                        sbProduct.Append("<br />");
                                    }
                                    if (md != null && !string.IsNullOrEmpty(md.Comments))
                                    {
                                        sbProduct.Append(md.Comments);
                                        sbProduct.Append("<br />");
                                    }
                                    if (!string.IsNullOrEmpty(p.CustomerNotes) && p.CustomerNotes != "")
                                    {
                                        sbProduct.Append(p.CustomerNotes);
                                    }
                                    sbProduct.Append("</p");
                                }
                                if (printVersion == PrintVersion.IncludeDiscount && order.DiscountsAndPromo != null && order.DiscountsAndPromo.Count > 0)
                                {
                                    var dpromo = (from dp in order.DiscountsAndPromo where dp.QuoteLineId == p.QuoteLineId select dp).FirstOrDefault();
                                    if (dpromo != null && dpromo.DiscountAmountPassed != null && dpromo.DiscountAmountPassed > 0)
                                    {
                                        sbProduct.Append("<p>Promo Applied:</p> Vendor promotion");
                                    }
                                }
                                sbProduct.Append("</td>");
                                if (printVersion != PrintVersion.CondensedVersion)
                                    sbProduct.Append("<td class='text-center'>" + isTaxable + "</td>");
                                if (printVersion == PrintVersion.IncludeDiscount)
                                {
                                    sbProduct.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", p.SuggestedResale) + "</td>");
                                    sbProduct.Append("<td class='quoteprint_ralign'>" + Convert.ToDecimal(p.Discount).ToString("F") + "%" + "</td>");
                                }
                                if (printVersion != PrintVersion.CondensedVersion)
                                    sbProduct.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", p.UnitPrice) + "</td>");

                                var Quantity = p.Quantity;
                                var ExtendedPrice = p.ExtendedPrice;

                                if (printVersion == PrintVersion.CondensedVersion)
                                {
                                    Quantity = products.Where(x => x.ProductName == p.ProductName && x.VendorName == p.VendorName && string.IsNullOrEmpty(x.CancelReason)).Sum(x => x.Quantity);
                                    ExtendedPrice = products.Where(x => x.ProductName == p.ProductName && x.VendorName == p.VendorName && string.IsNullOrEmpty(x.CancelReason)).Sum(x => x.ExtendedPrice);
                                }

                                sbProduct.Append("<td class='quoteprint_ralign'>" + Quantity + "</td>");
                                sbProduct.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", ExtendedPrice) + "</td>");
                                sbProduct.Append("</tr>");
                            }
                        }

                        sbProduct.Append("</tbody>");
                        sbProduct.Append("</table>");

                        data = data.Replace("{ProductSection}", sbProduct.ToString());
                    }
                    else
                        data = data.Replace("{ProductSection}", "");
                }
                else
                    data = data.Replace("{ProductSection}", "");

                if (order.QuoteLinesVM != null && order.QuoteLinesVM.Count > 0)
                {
                    var Service = (from ql in order.QuoteLinesVM where ql.ProductTypeId == 3 select ql).ToList();

                    if (Service != null && Service.Count > 0)
                    {
                        StringBuilder sbService = new StringBuilder();
                        sbService.Append("<table class='table table-quoteprint' style='border-bottom:1px solid #ddd;'>");
                        sbService.Append("<thead style='display: table-header-group;'>");
                        sbService.Append("<tr>");
                        sbService.Append("<th>Additional Items</th>");
                        sbService.Append("<th>Memo</th>");
                        sbService.Append("<th>Taxable</th>");
                        if (printVersion == PrintVersion.IncludeDiscount)
                        {
                            sbService.Append("<th>Sugg Resale</th>");
                            sbService.Append("<th>Discount</th>");
                        }
                        sbService.Append("<th>Unit Price</th>");
                        sbService.Append("<th>Qty</th>");
                        sbService.Append("<th>Total</th>");
                        sbService.Append("</tr>");
                        sbService.Append("</thead>");
                        sbService.Append("<tbody>");
                        foreach (var s in Service)
                        {
                            var tax = (from t in order.Taxinfo where t.QuoteLineId == s.QuoteLineId select t).FirstOrDefault();
                            string isTaxable = "";
                            if (tax != null && tax.Amount > 0)
                                isTaxable = "Y";
                            else isTaxable = "N";

                            sbService.Append("<tr class='border_quoteprintmemo'>");
                            if (!string.IsNullOrEmpty(s.ModelDescription) && s.ProductName != s.ModelDescription)
                            {
                                sbService.Append("<td>" + s.ProductName + " - " + s.ModelDescription + "</td>");
                                //sbService.Append("<td>" + s.ProductCategory + " - " + s.PrintProductCategory + "</td>");
                            }
                            else
                            {
                                sbService.Append("<td>" + s.ProductName + "</td>");
                                //sbService.Append("<td>" + s.ProductCategory + " - " + s.ProductName + "</td>");
                            }
                            sbService.Append("<td>" + s.Description + "</td>");
                            sbService.Append("<td class='text-center'>" + isTaxable + "</td>");
                            if (printVersion == PrintVersion.IncludeDiscount)
                            {
                                sbService.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", s.SuggestedResale) + "</td>");
                                sbService.Append("<td class='quoteprint_ralign'>" + Convert.ToDecimal(s.Discount).ToString("F") + "%" + "</td>");
                            }
                            sbService.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", s.UnitPrice) + "</td>");
                            sbService.Append("<td class='quoteprint_ralign'>" + s.Quantity + "</td>");
                            sbService.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", s.ExtendedPrice) + "</td>");
                            sbService.Append("</tr'>");
                        }
                        sbService.Append("</tbody>");
                        sbService.Append("</table>");
                        data = data.Replace("{ServicesSection}", sbService.ToString());
                    }
                    else
                        data = data.Replace("{ServicesSection}", "");
                }
                else
                    data = data.Replace("{ServicesSection}", "");

                if (order.QuoteLinesVM != null && order.QuoteLinesVM.Count > 0)
                {
                    var discountd = (from ql in order.QuoteLinesVM where ql.ProductTypeId == 4 select ql).ToList();

                    if ((discountd != null && discountd.Count > 0) || (order.Quote != null && order.Quote.Discount != null && order.Quote.Discount > 0))
                    {
                        StringBuilder sbDiscount = new StringBuilder();
                        sbDiscount.Append("<table class='table table-quoteprint' style='border-bottom:1px solid #ddd;'>");
                        sbDiscount.Append("<thead style='display: table-header-group;'>");
                        sbDiscount.Append("<tr>");
                        sbDiscount.Append("<th style = 'width:30%'> Discount Summary</th>");
                        sbDiscount.Append("<th width:70%>Memo</th>");
                        sbDiscount.Append("</tr>");
                        sbDiscount.Append("</thead>");
                        sbDiscount.Append("<tbody>");
                        if (order.Quote != null && order.Quote.Discount != null && order.Quote.Discount > 0)
                        {
                            string DiscountName = "", DiscountDesc = "Discount";
                            if (order.Quote.DiscountId > 0 && FEdiscounts != null && FEdiscounts.Count > 0)
                            {
                                var dis = (from d in FEdiscounts where d.ProductID == order.Quote.DiscountId select d).FirstOrDefault();
                                if (dis != null)
                                {
                                    DiscountName = dis.ProductName;
                                    DiscountDesc = dis.Description;
                                }
                            }
                            string discount = "0%";
                            if (order.Quote.Discount != null && order.Quote.DiscountType == "$")
                            {
                                discount = string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", order.Quote.Discount);
                                if (DiscountName != "")
                                    discount += " - " + DiscountName;
                            }
                            if (order.Quote.Discount != null && order.Quote.DiscountType == "%")
                            {
                                discount = String.Format("{0:0.##}", order.Quote.Discount) + "%";
                                if (DiscountName != "")
                                    discount += " - " + DiscountName;
                            }
                            sbDiscount.Append("<tr class='border_quoteprintmemo'>");
                            sbDiscount.Append("<td>" + discount + "</td>");
                            sbDiscount.Append("<td>" + DiscountDesc + "</td>");
                            sbDiscount.Append("</tr>");
                        }
                        if (discountd != null && discountd.Count > 0)
                        {
                            foreach (var d in discountd)
                            {
                                string DiscountName = "", DiscountDesc = "";
                                if (d.ProductId > 0 && FEdiscounts != null && FEdiscounts.Count > 0)
                                {
                                    var dis = (from di in FEdiscounts where di.ProductID == d.ProductId select di).FirstOrDefault();
                                    if (dis != null)
                                    {
                                        DiscountName = dis.ProductName;
                                        DiscountDesc = d.Description;
                                    }
                                }
                                if (d.Discount != null && d.Discount > 0)
                                {
                                    string discoutdata = "";
                                    if (d.Discount != null)
                                    {
                                        if (d.DiscountType == "$")
                                        {
                                            discoutdata += "$" + String.Format("{0:0.##}", d.Discount);
                                        }
                                        else
                                        {
                                            discoutdata += String.Format("{0:0.##}", d.Discount) + "%";
                                        }
                                        if (DiscountName != "")
                                            discoutdata += " - " + DiscountName;
                                    }

                                    sbDiscount.Append("<tr class='border_quoteprintmemo'>");
                                    sbDiscount.Append("<td>" + discoutdata + "</td>");
                                    sbDiscount.Append("<td>" + d.Description + "</td>");
                                    sbDiscount.Append("</tr>");
                                }
                            }
                        }

                        sbDiscount.Append("</tbody>");
                        sbDiscount.Append("</table>");
                        data = data.Replace("{DiscountsSection}", sbDiscount.ToString());
                    }
                    else
                        data = data.Replace("{DiscountsSection}", "");
                }
                else
                    data = data.Replace("{DiscountsSection}", "");

                // Payment
                if (order.OrderPayments != null && order.OrderPayments.Count > 0)
                {
                    StringBuilder sbPay = new StringBuilder();

                    sbPay.Append("");
                    sbPay.Append("<table class='table table-quoteprint' style='border-bottom:1px solid #ddd;'>");
                    sbPay.Append("<thead style='display: table-header-group;'>");
                    sbPay.Append("<tr>");
                    sbPay.Append("<th>Payment Date</th>");
                    sbPay.Append("<th>Payment Method</th>");
                    sbPay.Append("<th>Memo</th>");
                    sbPay.Append("<th>Amount</th>");
                    sbPay.Append("</tr>");
                    sbPay.Append("</thead>");
                    sbPay.Append("<tbody>");
                    foreach (var pay in order.OrderPayments)
                    {
                        sbPay.Append("<tr class='border_quoteprintmemo'>");
                        sbPay.Append("<td>" + Convert.ToDateTime(pay.PaymentDate).GlobalDateFormat() + "</td>");
                        sbPay.Append("<td>" + pay.Method + "</td>");
                        sbPay.Append("<td>" + pay.Memo + "</td>");
                        sbPay.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", pay.Amount) + "</td>");
                        sbPay.Append("</tr>");
                    }
                    sbPay.Append("</tbody>");
                    sbPay.Append("</table>");

                    data = data.Replace("{PaymentSection}", sbPay.ToString());
                }
                else
                    data = data.Replace("{PaymentSection}", "");

                var customerViewData = ordermgr.getOrderCustomerViewTable(orderId);
                string customerSignStreamId = "";
                string salesRepSignStreamId = "";
                string customerSignedDate = "";
                string salesRepSignedDate = "";

                if (customerViewData != null)
                {
                    customerSignStreamId = customerViewData.CustomerSignStreamId;
                    salesRepSignStreamId = customerViewData.SalesRepSignStreamId;
                    customerSignedDate = Convert.ToDateTime(customerViewData.CustomerSignedDate).GlobalDateFormat();
                    salesRepSignedDate = Convert.ToDateTime(customerViewData.SalesRepSignedDate).GlobalDateFormat();
                }

                if (!string.IsNullOrEmpty(customerSignStreamId))
                {
                    var binary = Convert.ToBase64String(FileTable.GetFileData(customerSignStreamId).bytes);
                    data = data.Replace("{CustomerSignature}", "data:image/png;base64," + binary);
                    data = data.Replace("{CustomerSignedDate}", customerSignedDate);
                }
                else
                {
                    data = data.Replace("{displayCustomerSignature}", "none");
                    data = data.Replace("{displayCustomerSignedDate}", "none");
                }


                if (!string.IsNullOrEmpty(salesRepSignStreamId))
                {
                    var binary = Convert.ToBase64String(FileTable.GetFileData(salesRepSignStreamId).bytes);
                    data = data.Replace("{SalesRepSignature}", "data:image/png;base64," + binary);
                    data = data.Replace("{SalesRepSignedDate}", salesRepSignedDate);
                }
                else
                {
                    data = data.Replace("{displaySalesRepSignature}", "none");
                    data = data.Replace("{displaySalesRepSignedDate}", "none");
                }
            }
            catch (Exception ex) { return "Internal Server Error"; }
            return data;
        }

        public string InvoiceSheetHeader_TailorLiving(int orderId)
        {
            string Path = string.Format("/Templates/Base/Brand/TL/TL_Invoice_Template-header.html");
            string srcFilePath = HostingEnvironment.MapPath(Path);
            string data = System.IO.File.ReadAllText(srcFilePath);

            try
            {
                int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;

                var salesAgentInfo = ordermgr.GetSalesAgentByOrderId(orderId);
                var order = ordermgr.GetOrderByOrderId(orderId);

                var TerritoryDisplayData = ordermgr.GetTerritoryDisplayData(orderId);

                data = data.Replace("{InvoiceNumber}", order != null ? order.OrderNumber.ToString() : "0");
                data = data.Replace("{Invoicedate}", order?.ContractedDate?.GlobalDateFormat());
                data = data.Replace("{SalesPersonName}", salesAgentInfo != null ? salesAgentInfo.FirstName + " " + salesAgentInfo.LastName : "");

                var feAddress = SessionManager.CurrentFranchise.Address;
                string FranchisePhone = SessionManager.CurrentFranchise.BusinessPhone;

                if (FranchisePhone != null && FranchisePhone.Length > 0)
                    FranchisePhone = Convert.ToInt64(FranchisePhone).ToString(phoneFormat);
                else
                    FranchisePhone = "";

                if (TerritoryDisplayData != null && TerritoryDisplayData.UseFranchiseName == false)
                {
                    if (TerritoryDisplayData.Phone != null && TerritoryDisplayData.Phone.Length > 0)
                        TerritoryDisplayData.Phone = Convert.ToInt64(TerritoryDisplayData.Phone).ToString(phoneFormat);
                    else
                        TerritoryDisplayData.Phone = "";

                    TerritoryDisplayData.Displayname = TerritoryDisplayData.Displayname + (TerritoryDisplayData.LicenseNumber != null && TerritoryDisplayData.LicenseNumber != "" ? " - " + TerritoryDisplayData.LicenseNumber : "");

                    data = data.Replace("{FEName}", TerritoryDisplayData.Displayname);
                    data = data.Replace("{FEAddress1}", TerritoryDisplayData.Address1 != null ? TerritoryDisplayData.Address1 : "");
                    data = data.Replace("{FEAddress2}", TerritoryDisplayData.Address2 != null ? TerritoryDisplayData.Address2 : "");
                    data = data.Replace("{FECitySateZip}", TerritoryDisplayData.City + "," + TerritoryDisplayData.State + " " + TerritoryDisplayData.ZipCode);
                    data = data.Replace("{FEPhone}", TerritoryDisplayData.Phone);
                    data = data.Replace("{FEEmail}", salesAgentInfo != null ? salesAgentInfo.PrimaryEmail : SessionManager.CurrentFranchise.AdminEmail);
                    data = data.Replace("{FEWebsite}", TerritoryDisplayData.WebSiteURL != "" ? TerritoryDisplayData.WebSiteURL : "www.tailoredliving.com");
                }
                else
                {
                    //var TerritoryDisplay = ordermgr.GetTerritoryDisplay(orderId);

                    //if (TerritoryDisplay != null)
                    //{
                    //    data = data.Replace("{FEName}", TerritoryDisplay.Name);
                    //    data = data.Replace("{FEAddress1}", feAddress.Address1 != null ? feAddress.Address1 : "");
                    //    data = data.Replace("{FEAddress2}", feAddress.Address2 != null ? feAddress.Address2 : "");
                    //    data = data.Replace("{FECitySateZip}", feAddress.City + "," + feAddress.State + " " + feAddress.ZipCode);
                    //    data = data.Replace("{FEPhone}", FranchisePhone);
                    //    data = data.Replace("{FEEmail}", SessionManager.CurrentFranchise.AdminEmail);
                    //    data = data.Replace("{FEWebsite}", "www.tailoredliving.com");
                    //}
                    //else
                    //{
                    if (TerritoryDisplayData != null)
                        data = data.Replace("{FEName}", SessionManager.CurrentFranchise.Name + (TerritoryDisplayData.LicenseNumber != null && TerritoryDisplayData.LicenseNumber != "" ? " - " + TerritoryDisplayData.LicenseNumber : ""));
                    else
                        data = data.Replace("{FEName}", SessionManager.CurrentFranchise.Name);
                    data = data.Replace("{FEAddress1}", feAddress.Address1 != null ? feAddress.Address1 : "");
                    data = data.Replace("{FEAddress2}", feAddress.Address2 != null ? feAddress.Address2 : "");
                    data = data.Replace("{FECitySateZip}", feAddress.City + "," + feAddress.State + " " + feAddress.ZipCode);
                    data = data.Replace("{FEPhone}", FranchisePhone);
                    data = data.Replace("{FEEmail}", SessionManager.CurrentFranchise.AdminEmail);
                    data = data.Replace("{FEWebsite}", "www.tailoredliving.com");

                    // }
                }
            }
            catch (Exception)
            {
                return "";
            }
            return data;
        }

        public string InvoiceSheetFooter_TailorLiving(int id)
        {
            string Path = string.Format("/Templates/Base/Brand/TL/TL_Invoice_Template-footer.html");
            string srcFilePath = HostingEnvironment.MapPath(Path);
            string data = System.IO.File.ReadAllText(srcFilePath);
            data = data.Replace("{printeddate}", TimeZoneManager.ToLocal(DateTime.UtcNow).GlobalDateFormat());
            return data;
        }

        public string InvoiceSheet_ConcreteCraft(int orderId, PrintVersion printVersion, bool printsignature)
        {
            string Path = string.Format("/Templates/Base/Brand/CC/CC_Invoice_Template-bodycontent.html");
            string srcFilePath = HostingEnvironment.MapPath(Path);
            string data = System.IO.File.ReadAllText(srcFilePath);
            try
            {
                int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;

                var order = ordermgr.GetOrder(orderId, FranchiseId);

                var Opportunity = ordermgr.GetOpportunityByOrderId(orderId);
                var customer = ordermgr.GetCustomerByOrderId(orderId);
                var BillingAddress = ordermgr.GetBillingAddressByOrderId(orderId);
                var InstallationAddress = ordermgr.GetInstallationAddressByOrderId(orderId);
                if (BillingAddress == null) BillingAddress = InstallationAddress;
                var disclaimer = ordermgr.GetFranchiseDisclaimer();
                var FEdiscounts = product_mgr.GetListDetails(3);

                if (disclaimer != null && !string.IsNullOrEmpty(disclaimer.ShortDisclaimer) && disclaimer.ShortDisclaimer != "")
                    disclaimer.ShortDisclaimer = disclaimer.ShortDisclaimer.Replace(Environment.NewLine, "<br />").Replace("\r", "<br />").Replace("\n", "<br />");

                if (disclaimer != null && !string.IsNullOrEmpty(disclaimer.LongDisclaimer) && disclaimer.LongDisclaimer != "")
                    disclaimer.LongDisclaimer = disclaimer.LongDisclaimer.Replace(Environment.NewLine, "<br />").Replace("\r", "<br />").Replace("\n", "<br />");

                if (order != null && !string.IsNullOrEmpty(order.OrderInvoiceLevelNotes) && order.OrderInvoiceLevelNotes != "")
                    order.OrderInvoiceLevelNotes = order.OrderInvoiceLevelNotes.Replace(Environment.NewLine, "<br />").Replace("\r", "<br />").Replace("\n", "<br />");

                if (customer.HomePhone != null && customer.HomePhone.Length > 0)
                {
                    customer.HomePhone = Convert.ToInt64(customer.HomePhone).ToString(phoneFormat);
                    if (customer.PreferredTFN != null && customer.PreferredTFN == "H")
                        customer.HomePhone = "*" + customer.HomePhone;
                }
                else
                    customer.HomePhone = "";

                if (customer.CellPhone != null && customer.CellPhone.Length > 0)
                {
                    customer.CellPhone = Convert.ToInt64(customer.CellPhone).ToString(phoneFormat);
                    if (customer.PreferredTFN != null && customer.PreferredTFN == "C")
                        customer.CellPhone = "*" + customer.CellPhone;
                }
                else
                    customer.CellPhone = "";

                if (customer.WorkPhone != null && customer.WorkPhone.Length > 0)
                {
                    customer.WorkPhone = Convert.ToInt64(customer.WorkPhone).ToString(phoneFormat);
                    if (customer.PreferredTFN != null && customer.PreferredTFN == "W")
                        customer.WorkPhone = "*" + customer.WorkPhone;
                }
                else
                    customer.WorkPhone = "";

                if (customer.CellPhone != "")
                    data = data.Replace("{displayAccountCellPhone}", "");
                else
                    data = data.Replace("{displayAccountCellPhone}", "none");

                if (customer.HomePhone != "")
                    data = data.Replace("{displayAccountHomePhone}", "");
                else
                    data = data.Replace("{displayAccountHomePhone}", "none");

                if (customer.WorkPhone != "")
                    data = data.Replace("{displayAccountWorkPhone}", "");
                else
                    data = data.Replace("{displayAccountWorkPhone}", "none");

                //Read lead html and append the data
                data = data.Replace("{AccountCustomerName}", order.AccountName);
                data = data.Replace("{AccountHomePhone}", customer.HomePhone);
                data = data.Replace("{AccountCellPhone}", customer.CellPhone);
                data = data.Replace("{AccountWorkPhone}", customer.WorkPhone);
                data = data.Replace("{AccountEmail}", customer.PrimaryEmail);
                data = data.Replace("{OppSideMark}", order != null && order.SideMark != null ? order.SideMark : "");

                data = data.Replace("{BillingAddress1}", BillingAddress.Address1);
                data = data.Replace("{BillingAddress2}", BillingAddress.Address2);
                data = data.Replace("{BillingCityStateZip}", BillingAddress.City + ", " + BillingAddress.State + " " + BillingAddress.ZipCode);

                data = data.Replace("{InstallationAddress1}", InstallationAddress.Address1);
                data = data.Replace("{InstallationAddress2}", InstallationAddress.Address2);
                data = data.Replace("{InstallationCityStateZip}", InstallationAddress.City + ", " + InstallationAddress.State + " " + InstallationAddress.ZipCode);

                //decimal OrderSubtotal = order.OrderSubtotal != null ? Convert.ToDecimal(order.OrderSubtotal) : 0m;
                //decimal Tax = order.Tax != null ? Convert.ToDecimal(order.Tax) : 0m;
                //decimal Taxperc = Tax / OrderSubtotal * 100;

                decimal Totalorderamount = order.OrderTotal != null ? Convert.ToDecimal(order.OrderTotal) : 0m;
                decimal BalanceDue = order.BalanceDue != null ? Convert.ToDecimal(order.BalanceDue) : 0m;
                decimal TotalPayments = Totalorderamount - BalanceDue;

                data = data.Replace("{ProductSubtotal}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", order.ProductSubtotal));
                data = data.Replace("{AdditionalCharges}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", order.AdditionalCharges));
                data = data.Replace("{SpecialDiscounts}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", order.OrderDiscount));
                data = data.Replace("{TotalAmount}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", order.OrderSubtotal));
                data = data.Replace("{TaxTotal}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", order.Tax));
                //data = data.Replace("{TaxPercentage}", Taxperc.ToString("F") + "%");
                data = data.Replace("{GrandTotal}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", order.OrderTotal));
                data = data.Replace("{TotalPayments}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", TotalPayments));
                data = data.Replace("{BalanceDue}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", order.BalanceDue));

                if (order.OrderDiscount > 0)
                    data = data.Replace("{displaySpecialDiscounts}", "");
                else
                    data = data.Replace("{displaySpecialDiscounts}", "none");

                data = data.Replace("{HeaderLevelComments}", order != null && !string.IsNullOrEmpty(order.OrderInvoiceLevelNotes) && order.OrderInvoiceLevelNotes != "" ? "<br /><b>Comments : </b>" + order.OrderInvoiceLevelNotes : "");

                data = data.Replace("{shortlegaldisclaimer}", disclaimer != null ? disclaimer.ShortDisclaimer : "");
                data = data.Replace("{longlegaldisclaimer}", disclaimer != null ? disclaimer.LongDisclaimer : "");

                var taxInfo = ordermgr.GetTaxDetails(orderId);

                var countryCode = SessionManager.CurrentFranchise.Address.CountryCode2Digits;

                if (countryCode != "US" && taxInfo != null && taxInfo.Count() > 0)
                {
                    var taxgrp = taxInfo.GroupBy(x => new { x.JurisType, x.Jurisdiction, x.TaxName, x.Rate })
                        .Select(z => new { txtype = z.Key, total = z.Sum(c => c.Amount) });

                    StringBuilder sbtax = new StringBuilder();
                    sbtax.Append("<table class='table table-quoteprint' style='border-bottom:1px solid #ddd;page-break-inside:avoid;'>");
                    sbtax.Append("<thead>");
                    sbtax.Append("<tr>");
                    sbtax.Append("<th style = 'width:70%'>Tax Name</th>");
                    sbtax.Append("<th width:30%>Amount</th>");
                    sbtax.Append("</tr>");
                    sbtax.Append("</thead>");
                    sbtax.Append("<tbody>");
                    foreach (var item in taxgrp)
                    {
                        sbtax.Append("<tr class='border_quoteprintmemo'>");
                        if (!string.IsNullOrEmpty(item.txtype.TaxName))
                        {
                            sbtax.Append("<td>" + item.txtype.TaxName + "</td>");
                        }
                        else
                        {
                            sbtax.Append("<td>" + item.txtype.Jurisdiction + "</td>");
                        }
                        sbtax.Append("<td>" + item.total + "</td>");
                        sbtax.Append("</tr>");
                    }
                    sbtax.Append("</tbody>");
                    sbtax.Append("</table>");
                    data = data.Replace(" {taxGrid}", sbtax.ToString());
                }
                else
                {
                    data = data.Replace(" {taxGrid}", string.Empty);
                }

                if (printsignature == true)
                    data = data.Replace("{printsignature}", "block");
                else
                    data = data.Replace("{printsignature}", "none");

                if (order.QuoteLinesVM != null && order.QuoteLinesVM.Count > 0)
                {
                    var products = (from ql in order.QuoteLinesVM where ql.ProductTypeId == 1 || ql.ProductTypeId == 2 || ql.ProductTypeId == 5 select ql).ToList();

                    if (products != null && products.Count > 0)
                    {
                        StringBuilder sbProduct = new StringBuilder();
                        sbProduct.Append("<table class='table table-quoteprint' >");
                        sbProduct.Append("<thead style='display: table-header-group;'>");
                        sbProduct.Append("<tr>");
                        //sbProduct.Append("<th>Window Name</th>");
                        sbProduct.Append("<th>Product</th>");
                        if (printVersion != PrintVersion.CondensedVersion)
                            sbProduct.Append("<th>Taxable</th>");
                        if (printVersion == PrintVersion.IncludeDiscount)
                        {
                            sbProduct.Append("<th>Sugg Resale</th>");
                            sbProduct.Append("<th>Discount</th>");
                        }
                        if (printVersion != PrintVersion.CondensedVersion)
                            sbProduct.Append("<th>Unit Price</th>");
                        sbProduct.Append("<th>Qty</th>");
                        sbProduct.Append("<th>Total</th>");
                        sbProduct.Append("</tr>");
                        sbProduct.Append("</thead>");
                        sbProduct.Append("<tbody>");

                        var CondensedGrouped = products.ToList().GroupBy(p => new { p.ProductName, p.VendorName }).Select(m => new CondensedProductVendor() { ProductName = m.Key.ProductName, VendorName = m.Key.VendorName, chk = false }).ToList();

                        foreach (var p in products)
                        {
                            if (string.IsNullOrEmpty(p.CancelReason))
                            {
                                if (printVersion == PrintVersion.CondensedVersion)
                                {
                                    if (CondensedGrouped != null)
                                    {
                                        var CondensedRecord = CondensedGrouped.Where(x => x.ProductName == p.ProductName && x.VendorName == p.VendorName && x.chk == false).FirstOrDefault();
                                        if (CondensedRecord == null)
                                            continue;
                                        else
                                            CondensedRecord.chk = true;
                                    }
                                }

                                sbProduct.Append("<tr style='border-bottom:1px solid #ddd;'>");
                                var tax = (from t in order.Taxinfo where t.QuoteLineId == p.QuoteLineId select t).FirstOrDefault();
                                string isTaxable = "";
                                if (tax != null && tax.Amount > 0)
                                    isTaxable = "Y";
                                else isTaxable = "N";

                                // Get vendor name from picjson
                                string vendorname = "";
                                if (!string.IsNullOrEmpty(p.VendorDisplayname))
                                {
                                    vendorname = p.VendorDisplayname;
                                }
                                else if (p.ProductTypeId != 1 && !string.IsNullOrEmpty(p.PICJson) && p.PICJson != "")
                                {
                                    var picjson = JsonConvert.DeserializeObject<dynamic>(p.PICJson);
                                    if (picjson != null && picjson.VendorName != null)
                                        vendorname = picjson.VendorName;
                                }
                                else
                                    vendorname = p.VendorName;

                                string prodctDesc = p.Description;

                                if (string.IsNullOrEmpty(vendorname) && p.VendorId.HasValue && p.VendorId.Value != 0)
                                {
                                    var vendormgr = new VendorManager();
                                    var vendor = vendormgr.GetLocalvendor(p.VendorId.Value);
                                    if (vendor.VendorType == 2 ||
                                        vendor.VendorType == 3)
                                    {
                                        vendorname = "";
                                    }
                                }

                                if (vendorname != "")
                                {
                                    if (printVersion == PrintVersion.CondensedVersion)
                                        prodctDesc = "<b>" + vendorname + "</b>; " + p.Description;
                                    else
                                        prodctDesc = "<b>" + vendorname + "</b>; " + p.Description;
                                }

                                dynamic picData = JObject.Parse(p.PICJson);
                                string picCategory = "";
                                if (picData != null)
                                {
                                    if (picData.PCategory != null) picCategory = picData.PCategory;
                                }

                                if ((p.PrintProductCategory != null && p.PrintProductCategory != "") || (picCategory != ""))
                                {
                                    picCategory = p.PrintProductCategory != null && p.PrintProductCategory != "" ? p.PrintProductCategory : picCategory;
                                    if (printVersion == PrintVersion.CondensedVersion)
                                    {
                                        if (p.ProductTypeId == 5 && p.ProductCategory == null)
                                        {
                                            sbProduct.Append("<td><b>" + picData.PCategory + "</b> - " + picCategory + "<br />");
                                        }
                                        else
                                            sbProduct.Append("<td><b>" + picCategory + "</b> - " + prodctDesc + "<br />");

                                    }
                                    else
                                    {
                                        if (p.ProductTypeId == 5 && p.ProductCategory == null)
                                        {
                                            sbProduct.Append("<td><b>" + picData.PCategory + "</b>-" + picCategory + "</b></br><b>" + p.ProductName + "</b><br />" + prodctDesc + "<br />");
                                        }
                                        else
                                            sbProduct.Append("<td><b>" + picCategory + "</b></br><b>" + p.ProductName + "</b><br />" + prodctDesc + "<br />");

                                    }
                                }
                                else
                                {
                                    if (printVersion == PrintVersion.CondensedVersion)
                                        sbProduct.Append("<td><b>" + p.ProductName + "</b> - " + prodctDesc + "<br />");
                                    else
                                        sbProduct.Append("<td><b>" + p.ProductName + "</b><br />" + prodctDesc + "<br />");
                                }

                                //  sbProduct.Append("<td><b>" + p.ProductName + "</b><br />" + prodctDesc + "<br />");
                                if ((p.Memo != null && !string.IsNullOrEmpty(p.Memo)) || (!string.IsNullOrEmpty(p.CustomerNotes) && p.CustomerNotes != ""))
                                {
                                    sbProduct.Append("<p>Comments:</p>");
                                    sbProduct.Append("<p class='comment_desc'>");
                                    if ((p.Memo != null && !string.IsNullOrEmpty(p.Memo)))
                                    {
                                        sbProduct.Append(p.Memo);
                                        sbProduct.Append("<br />");
                                    }
                                    if (!string.IsNullOrEmpty(p.CustomerNotes) && p.CustomerNotes != "")
                                    {
                                        sbProduct.Append(p.CustomerNotes);
                                    }
                                    sbProduct.Append("</p>");
                                }
                                if (printVersion == PrintVersion.IncludeDiscount && order.DiscountsAndPromo != null && order.DiscountsAndPromo.Count > 0)
                                {
                                    var dpromo = (from dp in order.DiscountsAndPromo where dp.QuoteLineId == p.QuoteLineId select dp).FirstOrDefault();
                                    if (dpromo != null && dpromo.DiscountAmountPassed != null && dpromo.DiscountAmountPassed > 0)
                                    {
                                        sbProduct.Append("<p>Promo Applied:</p> Vendor promotion");
                                    }
                                }
                                sbProduct.Append("</td>");
                                if (printVersion != PrintVersion.CondensedVersion)
                                    sbProduct.Append("<td class='text-center'>" + isTaxable + "</td>");
                                if (printVersion == PrintVersion.IncludeDiscount)
                                {
                                    sbProduct.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", p.SuggestedResale) + "</td>");
                                    sbProduct.Append("<td class='quoteprint_ralign'>" + Convert.ToDecimal(p.Discount).ToString("F") + "%" + "</td>");
                                }
                                if (printVersion != PrintVersion.CondensedVersion)
                                    sbProduct.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", p.UnitPrice) + "</td>");

                                var Quantity = p.Quantity;
                                var ExtendedPrice = p.ExtendedPrice;

                                if (printVersion == PrintVersion.CondensedVersion)
                                {
                                    Quantity = products.Where(x => x.ProductName == p.ProductName && x.VendorName == p.VendorName && string.IsNullOrEmpty(x.CancelReason)).Sum(x => x.Quantity);
                                    ExtendedPrice = products.Where(x => x.ProductName == p.ProductName && x.VendorName == p.VendorName && string.IsNullOrEmpty(x.CancelReason)).Sum(x => x.ExtendedPrice);
                                }

                                sbProduct.Append("<td class='quoteprint_ralign'>" + Quantity + "</td>");
                                sbProduct.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", ExtendedPrice) + "</td>");
                                sbProduct.Append("</tr>");



                            }
                        }

                        sbProduct.Append("</tbody>");
                        sbProduct.Append("</table>");

                        data = data.Replace("{ProductSection}", sbProduct.ToString());
                    }
                    else
                        data = data.Replace("{ProductSection}", "");
                }
                else
                    data = data.Replace("{ProductSection}", "");

                if (order.QuoteLinesVM != null && order.QuoteLinesVM.Count > 0)
                {
                    var Service = (from ql in order.QuoteLinesVM where ql.ProductTypeId == 3 select ql).ToList();

                    if (Service != null && Service.Count > 0)
                    {
                        StringBuilder sbService = new StringBuilder();
                        sbService.Append("<table class='table table-quoteprint' style='border-bottom:1px solid #ddd;'>");
                        sbService.Append("<thead style='display: table-header-group;'>");
                        sbService.Append("<tr>");
                        sbService.Append("<th>Additional Items</th>");
                        sbService.Append("<th>Memo</th>");
                        sbService.Append("<th>Taxable</th>");
                        if (printVersion == PrintVersion.IncludeDiscount)
                        {
                            sbService.Append("<th>Sugg Resale</th>");
                            sbService.Append("<th>Discount</th>");
                        }
                        sbService.Append("<th>Unit Price</th>");
                        sbService.Append("<th>Qty</th>");
                        sbService.Append("<th>Total</th>");
                        sbService.Append("</tr>");
                        sbService.Append("</thead>");
                        sbService.Append("<tbody>");
                        foreach (var s in Service)
                        {
                            var tax = (from t in order.Taxinfo where t.QuoteLineId == s.QuoteLineId select t).FirstOrDefault();
                            string isTaxable = "";
                            if (tax != null && tax.Amount > 0)
                                isTaxable = "Y";
                            else isTaxable = "N";

                            sbService.Append("<tr class='border_quoteprintmemo'>");
                            if (!string.IsNullOrEmpty(s.ModelDescription) && s.ProductName != s.ModelDescription)
                            {
                                sbService.Append("<td>" + s.ProductName + " - " + s.ModelDescription + "</td>");

                            }
                            else
                            {
                                sbService.Append("<td>" + s.ProductName + "</td>");

                            }
                            sbService.Append("<td>" + s.Description + "</td>");
                            sbService.Append("<td class='text-center'>" + isTaxable + "</td>");
                            if (printVersion == PrintVersion.IncludeDiscount)
                            {
                                sbService.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", s.SuggestedResale) + "</td>");
                                sbService.Append("<td class='quoteprint_ralign'>" + Convert.ToDecimal(s.Discount).ToString("F") + "%" + "</td>");
                            }
                            sbService.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", s.UnitPrice) + "</td>");
                            sbService.Append("<td class='quoteprint_ralign'>" + s.Quantity + "</td>");
                            sbService.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", s.ExtendedPrice) + "</td>");
                            sbService.Append("</tr>");
                        }
                        sbService.Append("</tbody>");
                        sbService.Append("</table>");
                        data = data.Replace("{ServicesSection}", sbService.ToString());
                    }
                    else
                        data = data.Replace("{ServicesSection}", "");
                }
                else
                    data = data.Replace("{ServicesSection}", "");

                if (order.QuoteLinesVM != null && order.QuoteLinesVM.Count > 0)
                {
                    var discountd = (from ql in order.QuoteLinesVM where ql.ProductTypeId == 4 select ql).ToList();

                    if ((discountd != null && discountd.Count > 0) || (order.Quote != null && order.Quote.Discount != null && order.Quote.Discount > 0))
                    {
                        StringBuilder sbDiscount = new StringBuilder();
                        sbDiscount.Append("<table class='table table-quoteprint' style='border-bottom:1px solid #ddd;'>");
                        sbDiscount.Append("<thead style='display: table-header-group;'>");
                        sbDiscount.Append("<tr>");
                        sbDiscount.Append("<th style = 'width:30%'> Discount Summary</th>");
                        sbDiscount.Append("<th width:70%>Memo</th>");
                        sbDiscount.Append("</tr>");
                        sbDiscount.Append("</thead>");
                        sbDiscount.Append("<tbody>");
                        if (order.Quote != null && order.Quote.Discount != null && order.Quote.Discount > 0)
                        {
                            string DiscountName = "", DiscountDesc = "Discount";
                            if (order.Quote.DiscountId > 0 && FEdiscounts != null && FEdiscounts.Count > 0)
                            {
                                var dis = (from d in FEdiscounts where d.ProductID == order.Quote.DiscountId select d).FirstOrDefault();
                                if (dis != null)
                                {
                                    DiscountName = dis.ProductName;
                                    DiscountDesc = dis.Description;
                                }
                            }
                            string discount = "0%";
                            if (order.Quote.Discount != null && order.Quote.DiscountType == "$")
                            {
                                discount = string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", order.Quote.Discount);
                                if (DiscountName != "")
                                    discount += " - " + DiscountName;
                            }
                            if (order.Quote.Discount != null && order.Quote.DiscountType == "%")
                            {
                                discount = String.Format("{0:0.##}", order.Quote.Discount) + "%";
                                if (DiscountName != "")
                                    discount += " - " + DiscountName;
                            }
                            sbDiscount.Append("<tr class='border_quoteprintmemo'>");
                            sbDiscount.Append("<td>" + discount + "</td>");
                            sbDiscount.Append("<td>" + DiscountDesc + "</td>");
                            sbDiscount.Append("</tr>");
                        }
                        if (discountd != null && discountd.Count > 0)
                        {
                            foreach (var d in discountd)
                            {
                                string DiscountName = "", DiscountDesc = "";
                                if (d.ProductId > 0 && FEdiscounts != null && FEdiscounts.Count > 0)
                                {
                                    var dis = (from di in FEdiscounts where di.ProductID == d.ProductId select di).FirstOrDefault();
                                    if (dis != null)
                                    {
                                        DiscountName = dis.ProductName;
                                        DiscountDesc = d.Description;
                                    }
                                }
                                if (d.Discount != null && d.Discount > 0)
                                {
                                    string discoutdata = "";
                                    if (d.Discount != null)
                                    {
                                        if (d.DiscountType == "$")
                                        {
                                            discoutdata += "$" + String.Format("{0:0.##}", d.Discount);
                                        }
                                        else
                                        {
                                            discoutdata += String.Format("{0:0.##}", d.Discount) + "%";
                                        }
                                        if (DiscountName != "")
                                            discoutdata += " - " + DiscountName;
                                    }

                                    sbDiscount.Append("<tr class='border_quoteprintmemo'>");
                                    sbDiscount.Append("<td>" + discoutdata + "</td>");
                                    sbDiscount.Append("<td>" + d.Description + "</td>");
                                    sbDiscount.Append("</tr>");
                                }
                            }
                        }

                        sbDiscount.Append("</tbody>");
                        sbDiscount.Append("</table>");
                        data = data.Replace("{DiscountsSection}", sbDiscount.ToString());
                    }
                    else
                        data = data.Replace("{DiscountsSection}", "");
                }
                else
                    data = data.Replace("{DiscountsSection}", "");

                // Payment
                if (order.OrderPayments != null && order.OrderPayments.Count > 0)
                {
                    StringBuilder sbPay = new StringBuilder();

                    sbPay.Append("");
                    sbPay.Append("<table class='table table-quoteprint' style='border-bottom:1px solid #ddd;'>");
                    sbPay.Append("<thead style='display: table-header-group;'>");
                    sbPay.Append("<tr>");
                    sbPay.Append("<th>Payment Date</th>");
                    sbPay.Append("<th>Payment Method</th>");
                    sbPay.Append("<th>Memo</th>");
                    sbPay.Append("<th>Amount</th>");
                    sbPay.Append("</tr>");
                    sbPay.Append("</thead>");
                    sbPay.Append("<tbody>");
                    foreach (var pay in order.OrderPayments)
                    {
                        sbPay.Append("<tr class='border_quoteprintmemo'>");
                        sbPay.Append("<td>" + Convert.ToDateTime(pay.PaymentDate).GlobalDateFormat() + "</td>");
                        sbPay.Append("<td>" + pay.Method + "</td>");
                        sbPay.Append("<td>" + pay.Memo + "</td>");
                        sbPay.Append("<td class='quoteprint_ralign'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", pay.Amount) + "</td>");
                        sbPay.Append("</tr>");
                    }
                    sbPay.Append("</tbody>");
                    sbPay.Append("</table>");

                    data = data.Replace("{PaymentSection}", sbPay.ToString());
                }
                else
                    data = data.Replace("{PaymentSection}", "");

                var customerViewData = ordermgr.getOrderCustomerViewTable(orderId);
                string customerSignStreamId = "";
                string salesRepSignStreamId = "";
                string customerSignedDate = "";
                string salesRepSignedDate = "";

                if (customerViewData != null)
                {
                    customerSignStreamId = customerViewData.CustomerSignStreamId;
                    salesRepSignStreamId = customerViewData.SalesRepSignStreamId;
                    customerSignedDate = Convert.ToDateTime(customerViewData.CustomerSignedDate).GlobalDateFormat();
                    salesRepSignedDate = Convert.ToDateTime(customerViewData.SalesRepSignedDate).GlobalDateFormat();
                }

                if (!string.IsNullOrEmpty(customerSignStreamId))
                {
                    var binary = Convert.ToBase64String(FileTable.GetFileData(customerSignStreamId).bytes);
                    data = data.Replace("{CustomerSignature}", "data:image/png;base64," + binary);
                    data = data.Replace("{CustomerSignedDate}", customerSignedDate);
                }
                else
                {
                    data = data.Replace("{displayCustomerSignature}", "none");
                    data = data.Replace("{displayCustomerSignedDate}", "none");
                }


                if (!string.IsNullOrEmpty(salesRepSignStreamId))
                {
                    var binary = Convert.ToBase64String(FileTable.GetFileData(salesRepSignStreamId).bytes);
                    data = data.Replace("{SalesRepSignature}", "data:image/png;base64," + binary);
                    data = data.Replace("{SalesRepSignedDate}", salesRepSignedDate);
                }
                else
                {
                    data = data.Replace("{displaySalesRepSignature}", "none");
                    data = data.Replace("{displaySalesRepSignedDate}", "none");
                }
            }
            catch (Exception ex) { return "Internal Server Error"; }
            return data;
        }

        public string InvoiceSheetHeader_ConcreteCraft(int orderId)
        {
            string Path = string.Format("/Templates/Base/Brand/CC/CC_Invoice_Template-header.html");
            string srcFilePath = HostingEnvironment.MapPath(Path);
            string data = System.IO.File.ReadAllText(srcFilePath);

            try
            {
                int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;

                var salesAgentInfo = ordermgr.GetSalesAgentByOrderId(orderId);
                var order = ordermgr.GetOrderByOrderId(orderId);

                var TerritoryDisplayData = ordermgr.GetTerritoryDisplayData(orderId);

                data = data.Replace("{InvoiceNumber}", order != null ? order.OrderNumber.ToString() : "0");
                data = data.Replace("{Invoicedate}", order?.ContractedDate?.GlobalDateFormat());
                data = data.Replace("{SalesPersonName}", salesAgentInfo != null ? salesAgentInfo.FirstName + " " + salesAgentInfo.LastName : "");

                var feAddress = SessionManager.CurrentFranchise.Address;
                string FranchisePhone = SessionManager.CurrentFranchise.BusinessPhone;

                if (FranchisePhone != null && FranchisePhone.Length > 0)
                    FranchisePhone = Convert.ToInt64(FranchisePhone).ToString(phoneFormat);
                else
                    FranchisePhone = "";

                if (TerritoryDisplayData != null && TerritoryDisplayData.UseFranchiseName == false)
                {
                    if (TerritoryDisplayData.Phone != null && TerritoryDisplayData.Phone.Length > 0)
                        TerritoryDisplayData.Phone = Convert.ToInt64(TerritoryDisplayData.Phone).ToString(phoneFormat);
                    else
                        TerritoryDisplayData.Phone = "";

                    TerritoryDisplayData.Displayname = TerritoryDisplayData.Displayname + (TerritoryDisplayData.LicenseNumber != null && TerritoryDisplayData.LicenseNumber != "" ? " - " + TerritoryDisplayData.LicenseNumber : "");

                    data = data.Replace("{FEName}", TerritoryDisplayData.Displayname);
                    data = data.Replace("{FEAddress1}", TerritoryDisplayData.Address1 != null ? TerritoryDisplayData.Address1 : "");
                    data = data.Replace("{FEAddress2}", TerritoryDisplayData.Address2 != null ? TerritoryDisplayData.Address2 : "");
                    data = data.Replace("{FECitySateZip}", TerritoryDisplayData.City + "," + TerritoryDisplayData.State + " " + TerritoryDisplayData.ZipCode);
                    data = data.Replace("{FEPhone}", TerritoryDisplayData.Phone);
                    data = data.Replace("{FEEmail}", salesAgentInfo != null ? salesAgentInfo.PrimaryEmail : SessionManager.CurrentFranchise.AdminEmail);
                    data = data.Replace("{FEWebsite}", TerritoryDisplayData.WebSiteURL != "" ? TerritoryDisplayData.WebSiteURL : "www.concretecraft.com");
                }
                else
                {
                    //var TerritoryDisplay = ordermgr.GetTerritoryDisplay(orderId);

                    //if (TerritoryDisplay != null)
                    //{
                    //    data = data.Replace("{FEName}", TerritoryDisplay.Name);
                    //    data = data.Replace("{FEAddress1}", feAddress.Address1 != null ? feAddress.Address1 : "");
                    //    data = data.Replace("{FEAddress2}", feAddress.Address2 != null ? feAddress.Address2 : "");
                    //    data = data.Replace("{FECitySateZip}", feAddress.City + "," + feAddress.State + " " + feAddress.ZipCode);
                    //    data = data.Replace("{FEPhone}", FranchisePhone);
                    //    data = data.Replace("{FEEmail}", SessionManager.CurrentFranchise.AdminEmail);
                    //    data = data.Replace("{FEWebsite}", "www.concretecraft.com");
                    //}
                    //else
                    //{
                    if (TerritoryDisplayData != null)
                        data = data.Replace("{FEName}", SessionManager.CurrentFranchise.Name + (TerritoryDisplayData.LicenseNumber != null && TerritoryDisplayData.LicenseNumber != "" ? " - " + TerritoryDisplayData.LicenseNumber : ""));
                    else
                        data = data.Replace("{FEName}", SessionManager.CurrentFranchise.Name);
                    data = data.Replace("{FEAddress1}", feAddress.Address1 != null ? feAddress.Address1 : "");
                    data = data.Replace("{FEAddress2}", feAddress.Address2 != null ? feAddress.Address2 : "");
                    data = data.Replace("{FECitySateZip}", feAddress.City + "," + feAddress.State + " " + feAddress.ZipCode);
                    data = data.Replace("{FEPhone}", FranchisePhone);
                    data = data.Replace("{FEEmail}", SessionManager.CurrentFranchise.AdminEmail);
                    data = data.Replace("{FEWebsite}", "www.concretecraft.com");
                    // }
                }
            }
            catch (Exception)
            {
                return "";
            }
            return data;
        }

        public string InvoiceSheetFooter_ConcreteCraft(int id)
        {
            string Path = string.Format("/Templates/Base/Brand/CC/CC_Invoice_Template-footer.html");
            string srcFilePath = HostingEnvironment.MapPath(Path);
            string data = System.IO.File.ReadAllText(srcFilePath);
            data = data.Replace("{printeddate}", TimeZoneManager.ToLocal(DateTime.UtcNow).GlobalDateFormat());
            return data;
        }

        public Guid createInvoicePDF(int id, PrintVersion printVersion = PrintVersion.IncludeDiscount, bool printsignature = true)
        {
            //Fetch order details
            int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
            //OrderId - Add code after table create
            int OrderId = 0;
            var query = @"Select * from [CRM].[Invoices] where InvoiceId=@Id";
            var Orderresult = ExecuteIEnumerableObject<InvoicesTP>(ContextFactory.CrmConnectionString, query, new
            {
                Id = id
            }).FirstOrDefault();
            if (Orderresult != null)
            {
                OrderId = Orderresult.OrderID;
            }

            string data = InvoiceSheet(OrderId, printVersion, printsignature);
            string headerdata = InvoiceSheetHeader(OrderId);
            string footerdata = InvoiceSheetFooter(OrderId);

            string fileDownloadName = string.Format("InvoiceSheet {0}-{1}.pdf", id, OrderId);

            string baseUrl = Serializer.Leads.LeadSheetReport.TemplateImageFilePath();

            // instantiate a html to pdf converter object
            HtmlToPdf converter = new HtmlToPdf();

            converter.Options.PdfPageSize = PdfPageSize.Letter;
            converter.Options.PdfPageOrientation = PdfPageOrientation.Portrait;

            converter.Options.MarginTop = 20;
            converter.Options.MarginBottom = 30;
            converter.Options.MarginLeft = 10;
            converter.Options.MarginRight = 10;

            converter.Options.DisplayHeader = true;
            converter.Header.DisplayOnFirstPage = true;
            converter.Header.DisplayOnOddPages = true;
            converter.Header.DisplayOnEvenPages = true;
            converter.Header.Height = 100;

            PdfHtmlSection headerHtml = new PdfHtmlSection(headerdata, baseUrl);
            headerHtml.AutoFitHeight = HtmlToPdfPageFitMode.AutoFit;
            converter.Header.Add(headerHtml);

            converter.Options.DisplayFooter = true;
            converter.Footer.DisplayOnFirstPage = true;
            converter.Footer.DisplayOnEvenPages = true;
            converter.Footer.DisplayOnOddPages = true;
            converter.Footer.Height = 50;

            PdfHtmlSection footerHtml = new PdfHtmlSection(footerdata, baseUrl);
            footerHtml.AutoFitHeight = HtmlToPdfPageFitMode.AutoFit;
            converter.Footer.Add(footerHtml);

            // page numbers can be added using a PdfTextSection object
            PdfTextSection text = new PdfTextSection(300, 25, "Page {page_number} of {total_pages}  ", new System.Drawing.Font("Arial", 8));
            text.HorizontalAlign = PdfTextHorizontalAlign.Center;
            converter.Footer.Add(text);

            // create a new pdf document converting an url

            PdfDocument doc = converter.ConvertHtmlString(data, baseUrl);

            // save pdf document
            byte[] pdf = doc.Save();

            // close pdf document
            doc.Close();

            Guid fileId = new Guid();
            fileId = ordermgr.UploadFile(id, pdf, fileDownloadName);
            return fileId;
        }

        public PdfDocument createInvoicePDFDoc(int orderId, PrintVersion printVersion = PrintVersion.CondensedVersion, bool printsignature = true)
        {
            string data = InvoiceSheet(orderId, printVersion, printsignature);
            string headerdata = InvoiceSheetHeader(orderId);
            string footerdata = InvoiceSheetFooter(orderId);

            string fileDownloadName = string.Format("InvoiceSheet_{0}.pdf", orderId);

            string baseUrl = Serializer.Leads.LeadSheetReport.TemplateImageFilePath();

            // instantiate a html to pdf converter object
            HtmlToPdf converter = new HtmlToPdf();

            converter.Options.PdfPageSize = PdfPageSize.Letter;
            converter.Options.PdfPageOrientation = PdfPageOrientation.Portrait;

            converter.Options.MarginTop = 20;
            converter.Options.MarginBottom = 30;
            converter.Options.MarginLeft = 10;
            converter.Options.MarginRight = 10;

            converter.Options.DisplayHeader = true;
            converter.Header.DisplayOnFirstPage = true;
            converter.Header.DisplayOnOddPages = true;
            converter.Header.DisplayOnEvenPages = true;
            converter.Header.Height = 100;

            PdfHtmlSection headerHtml = new PdfHtmlSection(headerdata, baseUrl);
            headerHtml.AutoFitHeight = HtmlToPdfPageFitMode.AutoFit;
            converter.Header.Add(headerHtml);

            converter.Options.DisplayFooter = true;
            converter.Footer.DisplayOnFirstPage = true;
            converter.Footer.DisplayOnEvenPages = true;
            converter.Footer.DisplayOnOddPages = true;
            converter.Footer.Height = 50;

            PdfHtmlSection footerHtml = new PdfHtmlSection(footerdata, baseUrl);
            footerHtml.AutoFitHeight = HtmlToPdfPageFitMode.AutoFit;
            converter.Footer.Add(footerHtml);

            // page numbers can be added using a PdfTextSection object
            PdfTextSection text = new PdfTextSection(300, 25, "Page {page_number} of {total_pages}  ", new System.Drawing.Font("Arial", 8));
            text.HorizontalAlign = PdfTextHorizontalAlign.Center;
            converter.Footer.Add(text);
            // create a new pdf document converting an url

            PdfDocument doc = converter.ConvertHtmlString(data, baseUrl);

            return doc;
        }
    }
}