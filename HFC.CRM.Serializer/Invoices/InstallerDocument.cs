﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Validation;
using DocumentFormat.OpenXml.Wordprocessing;
using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using DocumentFormat.OpenXml.Office2010.Word;
using HFC.CRM.Data.Context;
using System.Data.SqlClient;
using System.Data;
using Dapper;
using SelectPdf;
using System.Globalization;
using HFC.CRM.Core.Extensions;

namespace HFC.CRM.Serializer
{
    public class InstallerDocument : DataManager
    {
        OrderManager ordermgr = new OrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        MeasurementManager MeasurementMgr = new MeasurementManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        ProductManagerTP product_mgr = new ProductManagerTP();
        public string phoneFormat = "(###) ###-####";

        public string InstallerSheet(int orderId)
        {
            string data = string.Empty;
            if (SessionManager.BrandId == 1)
                data = InstallerSheet_BudgetBlinds(orderId);
            else if (SessionManager.BrandId == 2)
                data = InstallerSheet_TailorLiving(orderId);
            else if (SessionManager.BrandId == 3)
                data = InstallerSheet_ConcreteCraft(orderId);

            return data;
        }

        public string InstallerSheetHeader(int orderId)
        {
            string data = string.Empty;
            if (SessionManager.BrandId == 1)
                data = InstallerSheetHeader_BudgetBlinds(orderId);
            else if (SessionManager.BrandId == 2)
                data = InstallerSheetHeader_TailorLiving(orderId);
            else if (SessionManager.BrandId == 3)
                data = InstallerSheetHeader_ConcreteCraft(orderId);

            return data;
        }

        public string InstallerSheetFooter(int id)
        {
            string data = string.Empty;
            if (SessionManager.BrandId == 1)
                data = InstallerSheetFooter_BudgetBlinds(id);
            else if (SessionManager.BrandId == 2)
                data = InstallerSheetFooter_TailorLiving(id);
            else if (SessionManager.BrandId == 3)
                data = InstallerSheetFooter_ConcreteCraft(id);

            return data;
        }

        public string InstallerSheet_BudgetBlinds(int orderId)
        {
            string Path = string.Format("/Templates/Base/Brand/BB/BB_Installer_Template-bodycontent.html");
            string srcFilePath = HostingEnvironment.MapPath(Path);
            string data = System.IO.File.ReadAllText(srcFilePath);
            try
            {
                int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;

                var order = ordermgr.GetOrder(orderId, FranchiseId);

                if (order != null && !string.IsNullOrEmpty(order.InstallerInvoiceNotes) && order.InstallerInvoiceNotes != "")
                    order.InstallerInvoiceNotes = order.InstallerInvoiceNotes.Replace(Environment.NewLine, "<br />").Replace("\r", "<br />").Replace("\n", "<br />");

                var Opportunity = ordermgr.GetOpportunityByOrderId(orderId);
                var customer = ordermgr.GetCustomerByOrderId(orderId);
                //var BillingAddress = ordermgr.GetBillingAddressByOrderId(orderId);
                var InstallationAddress = ordermgr.GetInstallationAddressByOrderId(orderId);
                var disclaimer = ordermgr.GetFranchiseDisclaimer();

                var Measurementres = MeasurementMgr.Get(InstallationAddress.AddressId);
                List<MeasurementLineItemBB> MeasurementData = new List<MeasurementLineItemBB>();
                if (Measurementres != null)
                    MeasurementData = Measurementres.MeasurementBB;

                if (customer.HomePhone != null && customer.HomePhone.Length > 0)
                {
                    customer.HomePhone = Convert.ToInt64(customer.HomePhone).ToString(phoneFormat);
                    if (customer.PreferredTFN != null && customer.PreferredTFN == "H")
                        customer.HomePhone = "*" + customer.HomePhone;
                }
                else
                    customer.HomePhone = "";

                if (customer.CellPhone != null && customer.CellPhone.Length > 0)
                {
                    customer.CellPhone = Convert.ToInt64(customer.CellPhone).ToString(phoneFormat);
                    if (customer.PreferredTFN != null && customer.PreferredTFN == "C")
                        customer.CellPhone = "*" + customer.CellPhone;
                }
                else
                    customer.CellPhone = "";

                if (customer.WorkPhone != null && customer.WorkPhone.Length > 0)
                {
                    customer.WorkPhone = Convert.ToInt64(customer.WorkPhone).ToString(phoneFormat);
                    if (customer.PreferredTFN != null && customer.PreferredTFN == "W")
                        customer.WorkPhone = "*" + customer.WorkPhone;
                }
                else
                    customer.WorkPhone = "";

                if (customer.CellPhone != "")
                    data = data.Replace("{displayAccountCellPhone}", "");
                else
                    data = data.Replace("{displayAccountCellPhone}", "none");

                if (customer.HomePhone != "")
                    data = data.Replace("{displayAccountHomePhone}", "");
                else
                    data = data.Replace("{displayAccountHomePhone}", "none");

                if (customer.WorkPhone != "")
                    data = data.Replace("{displayAccountWorkPhone}", "");
                else
                    data = data.Replace("{displayAccountWorkPhone}", "none");

                //Read lead html and append the data
                data = data.Replace("{AccountCustomerName}", order.AccountName);
                data = data.Replace("{AccountHomePhone}", customer.HomePhone);
                data = data.Replace("{AccountCellPhone}", customer.CellPhone);
                data = data.Replace("{AccountWorkPhone}", customer.WorkPhone);
                data = data.Replace("{AccountEmail}", customer.PrimaryEmail);
                data = data.Replace("{OppSideMark}", order != null && order.SideMark != null ? order.SideMark : "");

                data = data.Replace("{InstallationAddress1}", InstallationAddress.Address1);
                data = data.Replace("{InstallationAddress2}", InstallationAddress.Address2);
                data = data.Replace("{InstallationCityStateZip}", InstallationAddress.City + ", " + InstallationAddress.State + " " + InstallationAddress.ZipCode);

                data = data.Replace("{BalanceDue}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", order.BalanceDue));

                data = data.Replace("{HeaderLevelComments}", order != null && !string.IsNullOrEmpty(order.InstallerInvoiceNotes) && order.InstallerInvoiceNotes != "" ? "<br /><b>Comments : </b>" + order.InstallerInvoiceNotes : "");
                data = data.Replace("{locationinfo}", "");
                data = data.Replace("{crossstreets}", "");

                if (order.QuoteLinesVM != null && order.QuoteLinesVM.Count > 0)
                {
                    var products = (from ql in order.QuoteLinesVM where ql.ProductTypeId == 1 || ql.ProductTypeId == 2 || ql.ProductTypeId == 5 select ql).ToList();

                    if (products != null && products.Count > 0)
                    {
                        StringBuilder sbProduct = new StringBuilder();
                        sbProduct.Append("<table class='table table-invoiceprint table-installer_grid'>");
                        sbProduct.Append("<thead style='display: table-header-group;'>");
                        sbProduct.Append("<tr>");
                        sbProduct.Append("<th>Qty</th>");
                        sbProduct.Append("<th style='width: 150px;'>Window Name</th>");
                        sbProduct.Append("<th style='width: 125px;'>Room</th>");
                        sbProduct.Append("<th style='width: 125px;'>Location</th>");
                        sbProduct.Append("<th>Mount</th>");
                        sbProduct.Append("<th style='width: 125px;'>Size (W x H)</th>");
                        //sbProduct.Append("<th style='width: 150px;'>Product Name</th>");
                        //  sbProduct.Append("<th style='width: 360px;'>Product Description</th>");
                        //sbProduct.Append("<th style = 'width: 360px;' ></th >");
                        sbProduct.Append("<th style = 'width: 510px;' >Vendor Name - Product Name</th >");
                        sbProduct.Append("<th>Instld</th>");
                        sbProduct.Append("<th style = 'width:80px;'>Case Mgt</th>");
                        sbProduct.Append("</tr>");
                        sbProduct.Append("</thead>");
                        sbProduct.Append("<tbody>");

                        foreach (var p in products)
                        {
                            if (string.IsNullOrEmpty(p.CancelReason))
                            {
                                var md = (from m in MeasurementData where m.id == p.MeasurementsetId select m).FirstOrDefault();

                                sbProduct.Append("<tr style='page-break-inside: avoid'>");
                                sbProduct.Append("<td>" + p.Quantity + "</td>");

                                if (!string.IsNullOrEmpty(p.RoomName))
                                    sbProduct.Append("<td>" + p.RoomName + "</td>");
                                else
                                    sbProduct.Append("<td></td>");
                                if (md != null && !string.IsNullOrEmpty(md.RoomLocation))
                                    sbProduct.Append("<td>" + md.RoomLocation + "</td>");
                                else
                                    sbProduct.Append("<td></td>");
                                if (md != null && !string.IsNullOrEmpty(md.WindowLocation))
                                    sbProduct.Append("<td>" + md.WindowLocation + "</td>");
                                else
                                    sbProduct.Append("<td></td>");
                                sbProduct.Append("<td>" + p.MountType + "</td>");

                                sbProduct.Append("<td>" + p.Width + " " + p.FranctionalValueWidth + " x " + p.Height + " " + p.FranctionalValueHeight + "</td>");
                                sbProduct.Append("<td>" + p.VendorName + " - " + p.ProductName + "</td>");
                                sbProduct.Append("<col> <td class='inbound' colspan='7'><div class='instaler_desc'> <div class='installer-column'> <strong>Description</strong> </div>");
                                sbProduct.Append("<div class='installer-column_1'>");
                                sbProduct.Append(p.Description + "</div> <div class='clearfix'></div> </div>");
                                if ((p.Memo != null && !string.IsNullOrEmpty(p.Memo)) || (md != null && !string.IsNullOrEmpty(md.Comments)) || (!string.IsNullOrEmpty(p.InternalNotes) && p.InternalNotes != ""))
                                {
                                    sbProduct.Append("<div class='instaler_desc'><div class='installer-column'><strong><i>Comments</strong> </div> <div class='installer-column_1'>");
                                    if (p.Memo != null && !string.IsNullOrEmpty(p.Memo))
                                    {
                                        sbProduct.Append(p.Memo);
                                    }
                                    //if (md != null && !string.IsNullOrEmpty(md.Comments))
                                    //{
                                    //    sbProduct.Append(md.Comments);
                                    //    sbProduct.Append("<br />");
                                    //}
                                    if (!string.IsNullOrEmpty(p.InternalNotes) && p.InternalNotes != "")
                                    {
                                        sbProduct.Append(p.InternalNotes);
                                    }
                                    sbProduct.Append(" </div></i><div class='clearfix'></div> </div ></td></col> ");
                                }

                                sbProduct.Append("<td style='border-bottom: 1px solid #ddd; border-left: 1px solid #ddd;'><div class='invoice_txtbox printinstaler_row'><input type='checkbox' class='option-input checkbox'></div></td>");
                                sbProduct.Append("<td style = 'border-bottom:1px solid #ddd;' ><div class='case_mgt'></div></td>");
                                //sbProduct.Append("<td>FUTURE</td>");
                                sbProduct.Append("</tr>");
                            }
                        }

                        sbProduct.Append("</tbody>");
                        sbProduct.Append("</table>");

                        data = data.Replace("{ProductSection}", sbProduct.ToString());
                    }
                    else
                        data = data.Replace("{ProductSection}", "");
                }
                else
                    data = data.Replace("{ProductSection}", "");
            }
            catch (Exception ex) { return "Internal Server Error"; }
            return data;
        }

        public string InstallerSheetHeader_BudgetBlinds(int orderId)
        {
            string Path = string.Format("/Templates/Base/Brand/BB/BB_Installer_Template-header.html");
            string srcFilePath = HostingEnvironment.MapPath(Path);
            string data = System.IO.File.ReadAllText(srcFilePath);

            try
            {
                int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;

                var salesAgentInfo = ordermgr.GetSalesAgentByOrderId(orderId);
                var order = ordermgr.GetOrderByOrderId(orderId);

                var TerritoryDisplayData = ordermgr.GetTerritoryDisplayData(orderId);

                data = data.Replace("{InvoiceNumber}", order != null ? order.OrderNumber.ToString() : "0");
                data = data.Replace("{Invoicedate}", order?.ContractedDate?.GlobalDateFormat());
                data = data.Replace("{SalesPersonName}", salesAgentInfo != null ? salesAgentInfo.FirstName + " " + salesAgentInfo.LastName : "");

                var feAddress = SessionManager.CurrentFranchise.Address;
                string FranchisePhone = SessionManager.CurrentFranchise.BusinessPhone;

                string phoneFormat = "(###) ###-####";
                if (FranchisePhone != null && FranchisePhone.Length > 0)
                    FranchisePhone = Convert.ToInt64(FranchisePhone).ToString(phoneFormat);
                else
                    FranchisePhone = "";

                if (TerritoryDisplayData != null && TerritoryDisplayData.UseFranchiseName == false)
                {
                    if (TerritoryDisplayData.Phone != null && TerritoryDisplayData.Phone.Length > 0)
                        TerritoryDisplayData.Phone = Convert.ToInt64(TerritoryDisplayData.Phone).ToString(phoneFormat);
                    else
                        TerritoryDisplayData.Phone = "";

                    TerritoryDisplayData.Displayname = TerritoryDisplayData.Displayname + (TerritoryDisplayData.LicenseNumber != null && TerritoryDisplayData.LicenseNumber != "" ? " - " + TerritoryDisplayData.LicenseNumber : "");

                    data = data.Replace("{FEName}", TerritoryDisplayData.Displayname);
                    data = data.Replace("{FEAddress1}", TerritoryDisplayData.Address1 != null ? TerritoryDisplayData.Address1 : "");
                    data = data.Replace("{FEAddress2}", TerritoryDisplayData.Address2 != null ? TerritoryDisplayData.Address2 : "");
                    data = data.Replace("{FECitySateZip}", TerritoryDisplayData.City + "," + TerritoryDisplayData.State + " " + TerritoryDisplayData.ZipCode);
                    data = data.Replace("{FEPhone}", TerritoryDisplayData.Phone);
                    data = data.Replace("{FEEmail}", salesAgentInfo != null ? salesAgentInfo.PrimaryEmail : SessionManager.CurrentFranchise.AdminEmail);
                    data = data.Replace("{FEWebsite}", TerritoryDisplayData.WebSiteURL != null && TerritoryDisplayData.WebSiteURL != "" ? TerritoryDisplayData.WebSiteURL : "www.budgetblinds.com");
                }
                else
                {
                    //var TerritoryDisplay = ordermgr.GetTerritoryDisplay(orderId);
                    //if (TerritoryDisplay != null)
                    //{
                    //    data = data.Replace("{FEName}", TerritoryDisplay.Name);
                    //    data = data.Replace("{FEAddress1}", feAddress.Address1 != null ? feAddress.Address1 : "");
                    //    data = data.Replace("{FEAddress2}", feAddress.Address2 != null ? feAddress.Address2 : "");
                    //    data = data.Replace("{FECitySateZip}", feAddress.City + "," + feAddress.State + " " + feAddress.ZipCode);
                    //    data = data.Replace("{FEPhone}", FranchisePhone);
                    //    data = data.Replace("{FEEmail}", salesAgentInfo != null ? salesAgentInfo.PrimaryEmail : SessionManager.CurrentFranchise.AdminEmail);
                    //    data = data.Replace("{FEWebsite}", "www.budgetblinds.com");
                    //}
                    //else
                    //{
                    if (TerritoryDisplayData != null)
                        data = data.Replace("{FEName}", SessionManager.CurrentFranchise.Name + (TerritoryDisplayData.LicenseNumber != null && TerritoryDisplayData.LicenseNumber != "" ? " - " + TerritoryDisplayData.LicenseNumber : ""));
                    else
                        data = data.Replace("{FEName}", SessionManager.CurrentFranchise.Name);
                    data = data.Replace("{FEAddress1}", feAddress.Address1 != null ? feAddress.Address1 : "");
                    data = data.Replace("{FEAddress2}", feAddress.Address2 != null ? feAddress.Address2 : "");
                    data = data.Replace("{FECitySateZip}", feAddress.City + "," + feAddress.State + " " + feAddress.ZipCode);
                    data = data.Replace("{FEPhone}", FranchisePhone);
                    data = data.Replace("{FEEmail}", salesAgentInfo != null ? salesAgentInfo.PrimaryEmail : SessionManager.CurrentFranchise.AdminEmail);
                    data = data.Replace("{FEWebsite}", "www.budgetblinds.com");
                    // }
                }
            }
            catch (Exception)
            {
                return "";
            }
            return data;
        }

        public string InstallerSheetFooter_BudgetBlinds(int id)
        {
            string Path = string.Format("/Templates/Base/Brand/BB/BB_Installer_Template-footer.html");
            string srcFilePath = HostingEnvironment.MapPath(Path);
            string data = System.IO.File.ReadAllText(srcFilePath);
            data = data.Replace("{printeddate}", TimeZoneManager.ToLocal(DateTime.UtcNow).GlobalDateFormat());
            return data;
        }

        public string InstallerSheet_TailorLiving(int orderId)
        {
            string Path = string.Format("/Templates/Base/Brand/TL/TL_Installer_Template-bodycontent.html");
            string srcFilePath = HostingEnvironment.MapPath(Path);
            string data = System.IO.File.ReadAllText(srcFilePath);
            try
            {
                int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;

                var order = ordermgr.GetOrder(orderId, FranchiseId);

                if (order != null && !string.IsNullOrEmpty(order.InstallerInvoiceNotes) && order.InstallerInvoiceNotes != "")
                    order.InstallerInvoiceNotes = order.InstallerInvoiceNotes.Replace(Environment.NewLine, "<br />").Replace("\r", "<br />").Replace("\n", "<br />");

                var Opportunity = ordermgr.GetOpportunityByOrderId(orderId);
                var customer = ordermgr.GetCustomerByOrderId(orderId);
                //var BillingAddress = ordermgr.GetBillingAddressByOrderId(orderId);
                var InstallationAddress = ordermgr.GetInstallationAddressByOrderId(orderId);
                var disclaimer = ordermgr.GetFranchiseDisclaimer();

                var Measurementres = MeasurementMgr.Get(InstallationAddress.AddressId);

                if (customer.HomePhone != null && customer.HomePhone.Length > 0)
                {
                    customer.HomePhone = Convert.ToInt64(customer.HomePhone).ToString(phoneFormat);
                    if (customer.PreferredTFN != null && customer.PreferredTFN == "H")
                        customer.HomePhone = "*" + customer.HomePhone;
                }
                else
                    customer.HomePhone = "";

                if (customer.CellPhone != null && customer.CellPhone.Length > 0)
                {
                    customer.CellPhone = Convert.ToInt64(customer.CellPhone).ToString(phoneFormat);
                    if (customer.PreferredTFN != null && customer.PreferredTFN == "C")
                        customer.CellPhone = "*" + customer.CellPhone;
                }
                else
                    customer.CellPhone = "";

                if (customer.WorkPhone != null && customer.WorkPhone.Length > 0)
                {
                    customer.WorkPhone = Convert.ToInt64(customer.WorkPhone).ToString(phoneFormat);
                    if (customer.PreferredTFN != null && customer.PreferredTFN == "W")
                        customer.WorkPhone = "*" + customer.WorkPhone;
                }
                else
                    customer.WorkPhone = "";

                if (customer.CellPhone != "")
                    data = data.Replace("{displayAccountCellPhone}", "");
                else
                    data = data.Replace("{displayAccountCellPhone}", "none");

                if (customer.HomePhone != "")
                    data = data.Replace("{displayAccountHomePhone}", "");
                else
                    data = data.Replace("{displayAccountHomePhone}", "none");

                if (customer.WorkPhone != "")
                    data = data.Replace("{displayAccountWorkPhone}", "");
                else
                    data = data.Replace("{displayAccountWorkPhone}", "none");

                //Read lead html and append the data
                data = data.Replace("{AccountCustomerName}", order.AccountName);
                data = data.Replace("{AccountHomePhone}", customer.HomePhone);
                data = data.Replace("{AccountCellPhone}", customer.CellPhone);
                data = data.Replace("{AccountWorkPhone}", customer.WorkPhone);
                data = data.Replace("{AccountEmail}", customer.PrimaryEmail);
                data = data.Replace("{OppSideMark}", order != null && order.SideMark != null ? order.SideMark : "");

                data = data.Replace("{InstallationAddress1}", InstallationAddress.Address1);
                data = data.Replace("{InstallationAddress2}", InstallationAddress.Address2);
                data = data.Replace("{InstallationCityStateZip}", InstallationAddress.City + ", " + InstallationAddress.State + " " + InstallationAddress.ZipCode);

                data = data.Replace("{BalanceDue}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", order.BalanceDue));

                data = data.Replace("{HeaderLevelComments}", order != null && !string.IsNullOrEmpty(order.InstallerInvoiceNotes) && order.InstallerInvoiceNotes != "" ? "<br /><b>Comments</b>" + order.InstallerInvoiceNotes : "");

                if (order.QuoteLinesVM != null && order.QuoteLinesVM.Count > 0)
                {
                    var products = (from ql in order.QuoteLinesVM where ql.ProductTypeId == 1 || ql.ProductTypeId == 2 || ql.ProductTypeId == 5 select ql).ToList();

                    if (products != null && products.Count > 0)
                    {
                        StringBuilder sbProduct = new StringBuilder();
                        sbProduct.Append("<table class='table table-invoiceprint table-installer_grid' >");
                        sbProduct.Append("<thead style='display: table-header-group;'>");
                        sbProduct.Append("<tr>");
                        sbProduct.Append("<th>Qty</th>");
                        sbProduct.Append("<th>Window Name</th>");
                        //sbProduct.Append("<th>Mount</th>");
                        sbProduct.Append("<th style='width: 125px; '>Size (W x H x D)</th>");
                        sbProduct.Append("<th style = 'width: 510px;' >Vendor Name - Product Name</th >");
                        //sbProduct.Append("<th style='width: 150px;'>Product Name</th>");
                        //sbProduct.Append("<th style='width: 360px;' ></th>");
                        //   sbProduct.Append("<th style='width: 360px; '>Product Description</th>");
                        sbProduct.Append("<th>Instld</th>");
                        sbProduct.Append("<th style='width:80px;'>Case Mgt</th>");
                        sbProduct.Append("</tr>");
                        sbProduct.Append("</thead>");
                        sbProduct.Append("<tbody>");
                        foreach (var p in products)
                        {
                            if (string.IsNullOrEmpty(p.CancelReason))
                            {
                                var md = new MeasurementLineItemTL();
                                if (Measurementres != null)
                                {
                                    var MeasurementData = Measurementres.MeasurementTL;
                                    md = (from m in MeasurementData where m.id == p.MeasurementId select m).FirstOrDefault();
                                }

                                sbProduct.Append("<tr style='page -break-after: always'>");
                                sbProduct.Append("<td>" + p.Quantity + "</td>");
                                if (!string.IsNullOrEmpty(p.RoomName))
                                    sbProduct.Append("<td>" + p.RoomName + "</td>");
                                else
                                    sbProduct.Append("<td></td>");
                                //sbProduct.Append("<td>" + p.MountType + "</td>");

                                sbProduct.Append("<td>" + p.Width + " " + p.FranctionalValueWidth + " x " + p.Height + " " + p.FranctionalValueHeight + "</td>");
                                //sbProduct.Append("<td>" + p.ProductName + "</td>");
                                sbProduct.Append("<td>" + p.VendorName + " - " + p.ProductName + "</td>");
                                sbProduct.Append("<col> <td class='inbound' colspan='4'> <div class='instaler_desc'> <div class='installer-column'><strong>Description</strong></div><div class='installer-column_1'>" + p.Description + "</div><div class='clearfix'></div></div>");
                                if ((p.Memo != null && !string.IsNullOrEmpty(p.Memo)) || (md != null && !string.IsNullOrEmpty(md.Comments)) || (!string.IsNullOrEmpty(p.InternalNotes) && p.InternalNotes != ""))
                                {
                                    sbProduct.Append("<div class='instaler_desc'><div class='installer-column'><i><strong>Comments:</strong></div><div class='installer-column_1'>");
                                    if (p.Memo != null && !string.IsNullOrEmpty(p.Memo))
                                    {
                                        sbProduct.Append(p.Memo);
                                    }
                                    if (md != null && !string.IsNullOrEmpty(md.Comments))
                                    {
                                        sbProduct.Append(md.Comments);
                                    }
                                    if (!string.IsNullOrEmpty(p.InternalNotes) && p.InternalNotes != "")
                                    {
                                        sbProduct.Append(p.InternalNotes);
                                    }
                                    sbProduct.Append("</div><div class='clearfix'></div></i></div></td></col>");
                                }

                                sbProduct.Append("<td style='border-bottom: 1px solid #ddd; border-left: 1px solid #ddd;'><div class='invoice_txtbox printinstaler_row'><input type='checkbox' class='option-input checkbox'></div></td>");
                                //sbProduct.Append("<td>FUTURE</td>");
                                sbProduct.Append("<td style='border-bottom:1px solid #ddd;'><div class='case_mgt'></div></td>");
                                sbProduct.Append("</tr>");
                            }
                        }

                        sbProduct.Append("</tbody>");
                        sbProduct.Append("</table>");

                        data = data.Replace("{ProductSection}", sbProduct.ToString());
                    }
                    else
                        data = data.Replace("{ProductSection}", "");
                }
                else
                    data = data.Replace("{ProductSection}", "");
            }
            catch (Exception ex) { return "Internal Server Error"; }
            return data;
        }

        public string InstallerSheetHeader_TailorLiving(int orderId)
        {
            string Path = string.Format("/Templates/Base/Brand/TL/TL_Installer_Template-header.html");
            string srcFilePath = HostingEnvironment.MapPath(Path);
            string data = System.IO.File.ReadAllText(srcFilePath);

            try
            {
                int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;

                var salesAgentInfo = ordermgr.GetSalesAgentByOrderId(orderId);
                var order = ordermgr.GetOrderByOrderId(orderId);

                var TerritoryDisplayData = ordermgr.GetTerritoryDisplayData(orderId);

                data = data.Replace("{InvoiceNumber}", order != null ? order.OrderNumber.ToString() : "0");
                data = data.Replace("{Invoicedate}", order?.ContractedDate?.GlobalDateFormat());
                data = data.Replace("{SalesPersonName}", salesAgentInfo != null ? salesAgentInfo.FirstName + " " + salesAgentInfo.LastName : "");

                var feAddress = SessionManager.CurrentFranchise.Address;
                string FranchisePhone = SessionManager.CurrentFranchise.BusinessPhone;

                string phoneFormat = "(###) ###-####";
                if (FranchisePhone != null && FranchisePhone.Length > 0)
                    FranchisePhone = Convert.ToInt64(FranchisePhone).ToString(phoneFormat);
                else
                    FranchisePhone = "";

                if (TerritoryDisplayData != null && TerritoryDisplayData.UseFranchiseName == false)
                {
                    if (TerritoryDisplayData.Phone != null && TerritoryDisplayData.Phone.Length > 0)
                        TerritoryDisplayData.Phone = Convert.ToInt64(TerritoryDisplayData.Phone).ToString(phoneFormat);
                    else
                        TerritoryDisplayData.Phone = "";

                    TerritoryDisplayData.Displayname = TerritoryDisplayData.Displayname + (TerritoryDisplayData.LicenseNumber != null && TerritoryDisplayData.LicenseNumber != "" ? " - " + TerritoryDisplayData.LicenseNumber : "");

                    data = data.Replace("{FEName}", TerritoryDisplayData.Displayname);
                    data = data.Replace("{FEAddress1}", TerritoryDisplayData.Address1 != null ? TerritoryDisplayData.Address1 : "");
                    data = data.Replace("{FEAddress2}", TerritoryDisplayData.Address2 != null ? TerritoryDisplayData.Address2 : "");
                    data = data.Replace("{FECitySateZip}", TerritoryDisplayData.City + "," + TerritoryDisplayData.State + " " + TerritoryDisplayData.ZipCode);
                    data = data.Replace("{FEPhone}", TerritoryDisplayData.Phone);
                    data = data.Replace("{FEEmail}", salesAgentInfo != null ? salesAgentInfo.PrimaryEmail : SessionManager.CurrentFranchise.AdminEmail);
                    data = data.Replace("{FEWebsite}", TerritoryDisplayData.WebSiteURL != "" ? TerritoryDisplayData.WebSiteURL : "www.tailoredliving.com");
                }
                else
                {
                    //var TerritoryDisplay = ordermgr.GetTerritoryDisplay(orderId);
                    //if (TerritoryDisplay != null)
                    //{
                    //    data = data.Replace("{FEName}", TerritoryDisplay.Name);
                    //    data = data.Replace("{FEAddress1}", feAddress.Address1 != null ? feAddress.Address1 : "");
                    //    data = data.Replace("{FEAddress2}", feAddress.Address2 != null ? feAddress.Address2 : "");
                    //    data = data.Replace("{FECitySateZip}", feAddress.City + "," + feAddress.State + " " + feAddress.ZipCode);
                    //    data = data.Replace("{FEPhone}", FranchisePhone);
                    //    data = data.Replace("{FEEmail}", SessionManager.CurrentFranchise.AdminEmail);
                    //    data = data.Replace("{FEWebsite}", "www.tailoredliving.com");
                    //}
                    //else
                    //{
                    if (TerritoryDisplayData != null)
                        data = data.Replace("{FEName}", SessionManager.CurrentFranchise.Name + (TerritoryDisplayData.LicenseNumber != null && TerritoryDisplayData.LicenseNumber != "" ? " - " + TerritoryDisplayData.LicenseNumber : ""));
                    else
                        data = data.Replace("{FEName}", SessionManager.CurrentFranchise.Name);
                    data = data.Replace("{FEAddress1}", feAddress.Address1 != null ? feAddress.Address1 : "");
                    data = data.Replace("{FEAddress2}", feAddress.Address2 != null ? feAddress.Address2 : "");
                    data = data.Replace("{FECitySateZip}", feAddress.City + "," + feAddress.State + " " + feAddress.ZipCode);
                    data = data.Replace("{FEPhone}", FranchisePhone);
                    data = data.Replace("{FEEmail}", SessionManager.CurrentFranchise.AdminEmail);
                    data = data.Replace("{FEWebsite}", "www.tailoredliving.com");
                    //  }
                }
            }
            catch (Exception)
            {
                return "";
            }
            return data;
        }

        public string InstallerSheetFooter_TailorLiving(int id)
        {
            string Path = string.Format("/Templates/Base/Brand/TL/TL_Installer_Template-footer.html");
            string srcFilePath = HostingEnvironment.MapPath(Path);
            string data = System.IO.File.ReadAllText(srcFilePath);
            data = data.Replace("{printeddate}", TimeZoneManager.ToLocal(DateTime.UtcNow).GlobalDateFormat());
            return data;
        }

        public string InstallerSheet_ConcreteCraft(int orderId)
        {
            string Path = string.Format("/Templates/Base/Brand/CC/CC_Installer_Template-bodycontent.html");
            string srcFilePath = HostingEnvironment.MapPath(Path);
            string data = System.IO.File.ReadAllText(srcFilePath);
            try
            {
                int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;

                var order = ordermgr.GetOrder(orderId, FranchiseId);

                if (order != null && !string.IsNullOrEmpty(order.InstallerInvoiceNotes) && order.InstallerInvoiceNotes != "")
                    order.InstallerInvoiceNotes = order.InstallerInvoiceNotes.Replace(Environment.NewLine, "<br />").Replace("\r", "<br />").Replace("\n", "<br />");

                var Opportunity = ordermgr.GetOpportunityByOrderId(orderId);
                var customer = ordermgr.GetCustomerByOrderId(orderId);
                //var BillingAddress = ordermgr.GetBillingAddressByOrderId(orderId);
                var InstallationAddress = ordermgr.GetInstallationAddressByOrderId(orderId);
                var disclaimer = ordermgr.GetFranchiseDisclaimer();

                var Measurementres = MeasurementMgr.Get(InstallationAddress.AddressId);
                List<MeasurementLineItemTL> MeasurementData = new List<MeasurementLineItemTL>();
                if (Measurementres != null)
                    MeasurementData = Measurementres.MeasurementTL;

                if (customer.HomePhone != null && customer.HomePhone.Length > 0)
                {
                    customer.HomePhone = Convert.ToInt64(customer.HomePhone).ToString(phoneFormat);
                    if (customer.PreferredTFN != null && customer.PreferredTFN == "H")
                        customer.HomePhone = "*" + customer.HomePhone;
                }
                else
                    customer.HomePhone = "";

                if (customer.CellPhone != null && customer.CellPhone.Length > 0)
                {
                    customer.CellPhone = Convert.ToInt64(customer.CellPhone).ToString(phoneFormat);
                    if (customer.PreferredTFN != null && customer.PreferredTFN == "C")
                        customer.CellPhone = "*" + customer.CellPhone;
                }
                else
                    customer.CellPhone = "";

                if (customer.WorkPhone != null && customer.WorkPhone.Length > 0)
                {
                    customer.WorkPhone = Convert.ToInt64(customer.WorkPhone).ToString(phoneFormat);
                    if (customer.PreferredTFN != null && customer.PreferredTFN == "W")
                        customer.WorkPhone = "*" + customer.WorkPhone;
                }
                else
                    customer.WorkPhone = "";

                if (customer.CellPhone != "")
                    data = data.Replace("{displayAccountCellPhone}", "");
                else
                    data = data.Replace("{displayAccountCellPhone}", "none");

                if (customer.HomePhone != "")
                    data = data.Replace("{displayAccountHomePhone}", "");
                else
                    data = data.Replace("{displayAccountHomePhone}", "none");

                if (customer.WorkPhone != "")
                    data = data.Replace("{displayAccountWorkPhone}", "");
                else
                    data = data.Replace("{displayAccountWorkPhone}", "none");

                //Read lead html and append the data
                data = data.Replace("{AccountCustomerName}", order.AccountName);
                data = data.Replace("{AccountHomePhone}", customer.HomePhone);
                data = data.Replace("{AccountCellPhone}", customer.CellPhone);
                data = data.Replace("{AccountWorkPhone}", customer.WorkPhone);
                data = data.Replace("{AccountEmail}", customer.PrimaryEmail);
                data = data.Replace("{OppSideMark}", order != null && order.SideMark != null ? order.SideMark : "");

                data = data.Replace("{InstallationAddress1}", InstallationAddress.Address1);
                data = data.Replace("{InstallationAddress2}", InstallationAddress.Address2);
                data = data.Replace("{InstallationCityStateZip}", InstallationAddress.City + ", " + InstallationAddress.State + " " + InstallationAddress.ZipCode);

                data = data.Replace("{BalanceDue}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", order.BalanceDue));

                data = data.Replace("{HeaderLevelComments}", order != null && !string.IsNullOrEmpty(order.InstallerInvoiceNotes) && order.InstallerInvoiceNotes != "" ? "<br /><b>Comments : </b>" + order.InstallerInvoiceNotes : "");

                if (order.QuoteLinesVM != null && order.QuoteLinesVM.Count > 0)
                {
                    var products = (from ql in order.QuoteLinesVM where ql.ProductTypeId == 1 || ql.ProductTypeId == 2 || ql.ProductTypeId == 5 select ql).ToList();

                    if (products != null && products.Count > 0)
                    {
                        StringBuilder sbProduct = new StringBuilder();
                        sbProduct.Append("<table class='table table-invoiceprint table-installer_grid' >");
                        sbProduct.Append("<thead style='display: table-header-group;'>");
                        sbProduct.Append("<tr>");
                        sbProduct.Append("<th>Qty</th>");
                        sbProduct.Append("<th style = 'width: 850px;' >Vendor Name - Product Name</th >");
                        //sbProduct.Append("<th style='width: 150px;'>Product Name</th>");
                        //sbProduct.Append("<th style = 'width:700px;'></th>");
                        //sbProduct.Append("<th style='width: 360px; '>Product Description</th>");
                        sbProduct.Append("<th>Instld</th>");
                        sbProduct.Append("<th style='width:80px;'>Case Mgt</th>");
                        sbProduct.Append("</tr>");
                        sbProduct.Append("</thead>");
                        sbProduct.Append("<tbody>");
                        foreach (var p in products)
                        {
                            if (string.IsNullOrEmpty(p.CancelReason))
                            {
                                sbProduct.Append("<tr style='page -break-after: always'>");
                                sbProduct.Append("<td>" + p.Quantity + "</td>");
                                sbProduct.Append("<td>" + p.VendorName + " - " + p.ProductName + "</td>");
                                sbProduct.Append("<col><td class='inbound' colspan='2'><div class='instaler_desc'><div class='installer-column'><strong>Description</strong></div><div class='installer-column_1'>");
                                //sbProduct.Append("<td></td>");
                                sbProduct.Append(p.Description + "</div><div class='clearfix'></div></div><div class='instaler_desc'><div class='installer-column'>");
                                if ((p.Memo != null && !string.IsNullOrEmpty(p.Memo)) || (!string.IsNullOrEmpty(p.CustomerNotes) && p.CustomerNotes != ""))
                                {
                                    sbProduct.Append("<i><strong>Comments</strong>");
                                    sbProduct.Append("</div> <div class='installer-column_1'>");
                                    if ((p.Memo != null && !string.IsNullOrEmpty(p.Memo)))
                                    {
                                        sbProduct.Append(p.Memo);
                                    }
                                    if (!string.IsNullOrEmpty(p.CustomerNotes) && p.CustomerNotes != "")
                                    {
                                        sbProduct.Append(p.CustomerNotes);
                                    }
                                }
                                sbProduct.Append("</div></i><div class='clearfix'></div></div></td></col>");
                                sbProduct.Append("<td style='border-bottom: 1px solid #ddd; border-left: 1px solid #ddd;'><div class='invoice_txtbox printinstaler_row'><input type='checkbox' class='option-input checkbox'></div></td>");
                                sbProduct.Append("<td style='border-bottom:1px solid #ddd;'><div class='case_mgt'></div></td>");
                                sbProduct.Append("</tr>");
                            }
                        }

                        sbProduct.Append("</tbody>");
                        sbProduct.Append("</table>");

                        data = data.Replace("{ProductSection}", sbProduct.ToString());
                    }
                    else
                        data = data.Replace("{ProductSection}", "");
                }
                else
                    data = data.Replace("{ProductSection}", "");
            }
            catch (Exception ex) { return "Internal Server Error"; }
            return data;
        }

        public string InstallerSheetHeader_ConcreteCraft(int orderId)
        {
            string Path = string.Format("/Templates/Base/Brand/CC/CC_Installer_Template-header.html");
            string srcFilePath = HostingEnvironment.MapPath(Path);
            string data = System.IO.File.ReadAllText(srcFilePath);

            try
            {
                int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;

                var salesAgentInfo = ordermgr.GetSalesAgentByOrderId(orderId);
                var order = ordermgr.GetOrderByOrderId(orderId);

                var TerritoryDisplayData = ordermgr.GetTerritoryDisplayData(orderId);

                data = data.Replace("{InvoiceNumber}", order != null ? order.OrderNumber.ToString() : "0");
                data = data.Replace("{Invoicedate}", order?.ContractedDate?.GlobalDateFormat());
                data = data.Replace("{SalesPersonName}", salesAgentInfo != null ? salesAgentInfo.FirstName + " " + salesAgentInfo.LastName : "");

                var feAddress = SessionManager.CurrentFranchise.Address;
                string FranchisePhone = SessionManager.CurrentFranchise.BusinessPhone;

                string phoneFormat = "(###) ###-####";
                if (FranchisePhone != null && FranchisePhone.Length > 0)
                    FranchisePhone = Convert.ToInt64(FranchisePhone).ToString(phoneFormat);
                else
                    FranchisePhone = "";

                if (TerritoryDisplayData != null && TerritoryDisplayData.UseFranchiseName == false)
                {
                    if (TerritoryDisplayData.Phone != null && TerritoryDisplayData.Phone.Length > 0)
                        TerritoryDisplayData.Phone = Convert.ToInt64(TerritoryDisplayData.Phone).ToString(phoneFormat);
                    else
                        TerritoryDisplayData.Phone = "";

                    TerritoryDisplayData.Displayname = TerritoryDisplayData.Displayname + (TerritoryDisplayData.LicenseNumber != null && TerritoryDisplayData.LicenseNumber != "" ? " - " + TerritoryDisplayData.LicenseNumber : "");

                    data = data.Replace("{FEName}", TerritoryDisplayData.Displayname);
                    data = data.Replace("{FEAddress1}", TerritoryDisplayData.Address1 != null ? TerritoryDisplayData.Address1 : "");
                    data = data.Replace("{FEAddress2}", TerritoryDisplayData.Address2 != null ? TerritoryDisplayData.Address2 : "");
                    data = data.Replace("{FECitySateZip}", TerritoryDisplayData.City + "," + TerritoryDisplayData.State + " " + TerritoryDisplayData.ZipCode);
                    data = data.Replace("{FEPhone}", TerritoryDisplayData.Phone);
                    data = data.Replace("{FEEmail}", salesAgentInfo != null ? salesAgentInfo.PrimaryEmail : SessionManager.CurrentFranchise.AdminEmail);
                    data = data.Replace("{FEWebsite}", TerritoryDisplayData.WebSiteURL != "" ? TerritoryDisplayData.WebSiteURL : "www.concretecraft.com");
                }
                else
                {
                    //var TerritoryDisplay = ordermgr.GetTerritoryDisplay(orderId);
                    //if (TerritoryDisplay != null)
                    //{
                    //    data = data.Replace("{FEName}", TerritoryDisplay.Name);
                    //    data = data.Replace("{FEAddress1}", feAddress.Address1 != null ? feAddress.Address1 : "");
                    //    data = data.Replace("{FEAddress2}", feAddress.Address2 != null ? feAddress.Address2 : "");
                    //    data = data.Replace("{FECitySateZip}", feAddress.City + "," + feAddress.State + " " + feAddress.ZipCode);
                    //    data = data.Replace("{FEPhone}", FranchisePhone);
                    //    data = data.Replace("{FEEmail}", SessionManager.CurrentFranchise.AdminEmail);
                    //    data = data.Replace("{FEWebsite}", "www.concretecraft.com");
                    //}
                    //else
                    //{
                    if (TerritoryDisplayData != null)
                        data = data.Replace("{FEName}", SessionManager.CurrentFranchise.Name + (TerritoryDisplayData.LicenseNumber != null && TerritoryDisplayData.LicenseNumber != "" ? " - " + TerritoryDisplayData.LicenseNumber : ""));
                    else
                        data = data.Replace("{FEName}", SessionManager.CurrentFranchise.Name);
                    data = data.Replace("{FEAddress1}", feAddress.Address1 != null ? feAddress.Address1 : "");
                    data = data.Replace("{FEAddress2}", feAddress.Address2 != null ? feAddress.Address2 : "");
                    data = data.Replace("{FECitySateZip}", feAddress.City + "," + feAddress.State + " " + feAddress.ZipCode);
                    data = data.Replace("{FEPhone}", FranchisePhone);
                    data = data.Replace("{FEEmail}", SessionManager.CurrentFranchise.AdminEmail);
                    data = data.Replace("{FEWebsite}", "www.concretecraft.com");
                    // }
                }
            }
            catch (Exception)
            {
                return "";
            }
            return data;
        }

        public string InstallerSheetFooter_ConcreteCraft(int id)
        {
            string Path = string.Format("/Templates/Base/Brand/CC/CC_Installer_Template-footer.html");
            string srcFilePath = HostingEnvironment.MapPath(Path);
            string data = System.IO.File.ReadAllText(srcFilePath);
            data = data.Replace("{printeddate}", TimeZoneManager.ToLocal(DateTime.UtcNow).GlobalDateFormat());
            return data;
        }

        public PdfDocument createInstallerPDFDoc(int orderId)
        {
            string data = InstallerSheet(orderId);
            string headerdata = InstallerSheetHeader(orderId);
            string footerdata = InstallerSheetFooter(orderId);

            string fileDownloadName = string.Format("InstallerSheet_{0}.pdf", orderId);

            string baseUrl = Serializer.Leads.LeadSheetReport.TemplateImageFilePath();

            // instantiate a html to pdf converter object
            HtmlToPdf converter = new HtmlToPdf();

            converter.Options.DrawBackground = false;
            //converter.Options.CssMediaType = HtmlToPdfCssMediaType.Print;

            converter.Options.PdfPageSize = PdfPageSize.Letter;
            converter.Options.PdfPageOrientation = PdfPageOrientation.Landscape;

            converter.Options.MarginTop = 20;
            converter.Options.MarginBottom = 10;
            converter.Options.MarginLeft = 10;
            converter.Options.MarginRight = 10;

            converter.Options.DisplayHeader = true;
            converter.Header.DisplayOnFirstPage = true;
            converter.Header.DisplayOnOddPages = true;
            converter.Header.DisplayOnEvenPages = true;
            converter.Header.Height = 125;

            PdfHtmlSection headerHtml = new PdfHtmlSection(headerdata, baseUrl);
            headerHtml.AutoFitHeight = HtmlToPdfPageFitMode.AutoFit;
            converter.Header.Add(headerHtml);

            converter.Options.DisplayFooter = true;
            converter.Footer.DisplayOnFirstPage = true;
            converter.Footer.DisplayOnEvenPages = true;
            converter.Footer.DisplayOnOddPages = true;
            converter.Footer.Height = 15;

            PdfHtmlSection footerHtml = new PdfHtmlSection(footerdata, baseUrl);
            footerHtml.AutoFitHeight = HtmlToPdfPageFitMode.AutoFit;
            converter.Footer.Add(footerHtml);

            // page numbers can be added using a PdfTextSection object
            PdfTextSection text = new PdfTextSection(300, 0, "Page {page_number} of {total_pages}  ", new System.Drawing.Font("Arial", 8));
            text.HorizontalAlign = PdfTextHorizontalAlign.Center;
            converter.Footer.Add(text);

            // create a new pdf document converting an url
            PdfDocument doc = converter.ConvertHtmlString(data, baseUrl);

            // get image path
            string imgFile = HostingEnvironment.MapPath("~/Templates/Base/Brand/BB/images/Donotleavebehind.png");

            PdfTemplate template = doc.AddTemplate(doc.Pages[0].ClientRectangle.Width, doc.Pages[0].ClientRectangle.Height);
            PdfImageElement img = new PdfImageElement(
                doc.Pages[0].ClientRectangle.Width - (doc.Pages[0].ClientRectangle.Width - 200),
                doc.Pages[0].ClientRectangle.Height - (doc.Pages[0].ClientRectangle.Height), imgFile);// , 300, 300 400, 250,
            img.Transparency = 40;
            template.Background = true;
            template.Add(img);

            return doc;
        }
    }
}