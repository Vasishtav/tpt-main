﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Validation;
using DocumentFormat.OpenXml.Wordprocessing;
using HFC.CRM.Data;
using HFC.CRM.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace HFC.CRM.Serializer.Jobs
{
    using Aspose.Words.Tables;

    using Table = DocumentFormat.OpenXml.Wordprocessing.Table;
    using HFC.CRM.Core.Common;
    using HFC.CRM.Managers;

    public class Quote_old
    {
        //protected CRMContext CRMDBContext = ContextFactory.Current.Context;
        private static void FillHeader(MainDocumentPart mainPart, Franchise franchise, string quoteNumber, string reportName)
        {
            if (mainPart.HeaderParts != null && mainPart.HeaderParts.Count() > 0)
            {
                foreach (var header in mainPart.HeaderParts.Select(s => s.Header))
                {
                    foreach (var run in header.Descendants<SdtRun>())
                    {
                        if (run.SdtProperties.GetFirstChild<SdtAlias>().Val.Value == "FranchiseName")
                        {
                            OpenXmlUtil.FillContentControl(run, franchise.Name);
                        }
                        //else if (run.SdtProperties.GetFirstChild<SdtAlias>().Val.Value == "Number")
                        //{
                        //    OpenXmlUtil.FillContentControl(run, quoteNumber);
                        //}
                        else if (run.SdtProperties.GetFirstChild<SdtAlias>().Val.Value == "ReportName")
                        {
                            OpenXmlUtil.FillContentControl(run, string.Format("{0} #{1}", reportName, quoteNumber));
                        }
                    }
                    foreach (var block in header.Descendants<SdtBlock>())
                    {
                        if (block.SdtProperties.GetFirstChild<SdtAlias>().Val.Value == "FranchiseName")
                        {
                            OpenXmlUtil.FillContentControl(block, franchise.Name);
                        }
                        //else if (block.SdtProperties.GetFirstChild<SdtAlias>().Val.Value == "Number")
                        //{
                        //    OpenXmlUtil.FillContentControl(block, quoteNumber);
                        //}
                        else if (block.SdtProperties.GetFirstChild<SdtAlias>().Val.Value == "ReportName")
                        {
                            OpenXmlUtil.FillContentControl(block, string.Format("{0} #{1}", reportName, quoteNumber));
                        }
                    }
                }
            }
        }

        private static void FillFooter(MainDocumentPart mainPart, Franchise franchise)
        {
            if (mainPart.FooterParts != null && mainPart.FooterParts.Count() > 0 && franchise != null)
            {
                List<string> footerInfo = new List<string>();
                //if (franchise.Address != null && !string.IsNullOrEmpty(franchise.Address.ToString()))
                //    footerInfo.Add(franchise.Address.ToString());
                if (!string.IsNullOrEmpty(franchise.TollFreeNumber))
                    footerInfo.Add(string.Format("P: {0}", RegexUtil.FormatUSPhone(franchise.TollFreeNumber)));
                if (!string.IsNullOrEmpty(franchise.FaxNumber))
                    footerInfo.Add(string.Format("F: {0}", RegexUtil.FormatUSPhone(franchise.FaxNumber)));
                if (!string.IsNullOrEmpty(franchise.SupportEmail))
                    footerInfo.Add(string.Format("E: {0}", franchise.SupportEmail));

                foreach (var footer in mainPart.FooterParts.Select(s => s.Footer))
                {
                    foreach (var run in footer.Descendants<SdtRun>())
                    {
                        if (run.SdtProperties.GetFirstChild<SdtAlias>().Val.Value == "Contact")
                        {
                            OpenXmlUtil.FillContentControl(run, string.Join(" | ", footerInfo));
                        }
                        else if (run.SdtProperties.GetFirstChild<SdtAlias>().Val.Value == "Website")
                        {
                            if (!string.IsNullOrEmpty(franchise.Website))
                            {
                                HyperlinkRelationship relation = footer.FooterPart.AddHyperlinkRelationship(new Uri("http://" + franchise.Website, UriKind.RelativeOrAbsolute), true);
                                OpenXmlUtil.FillContentControl(run, franchise.Website, relation.Id);
                            }
                            else
                                OpenXmlUtil.FillContentControl(run, "");
                        }
                        else if (run.SdtProperties.GetFirstChild<SdtAlias>().Val.Value == "Disclaimer")
                        {
                            OpenXmlUtil.FillContentControl(run, "An independently owned and operated franchise");
                        }
                    }
                    foreach (var block in footer.Descendants<SdtBlock>())
                    {
                        if (block.SdtProperties.GetFirstChild<SdtAlias>().Val.Value == "Contact")
                        {
                            OpenXmlUtil.FillContentControl(block, string.Join(" | ", footerInfo));
                        }
                        else if (block.SdtProperties.GetFirstChild<SdtAlias>().Val.Value == "Website")
                        {
                            if (!string.IsNullOrEmpty(franchise.Website))
                            {
                                HyperlinkRelationship relation = footer.FooterPart.AddHyperlinkRelationship(new Uri("http://" + franchise.Website, UriKind.RelativeOrAbsolute), true);
                                OpenXmlUtil.FillContentControl(block, franchise.Website, relation.Id);
                            }
                            else
                                OpenXmlUtil.FillContentControl(block, "");
                        }
                        else if (block.SdtProperties.GetFirstChild<SdtAlias>().Val.Value == "Disclaimer")
                        {
                            OpenXmlUtil.FillContentControl(block, "An independently owned and operated franchise");
                        }
                    }

                }
            }
        }
        
          public static string GetSource(int sourceId)
        {
            var source = CacheManager.SourceCollection.FirstOrDefault(f => f.SourceId == sourceId);
            if (source != null)
                return source.Name;
            else
                return string.Empty;
        }

        /// <summary>
        /// Creates an MSR word or PDF document
        /// </summary>
        /// <param name="franchise">Franchise to run report for</param>
        /// <param name="job">Job Data, will use SalesPerson, BillingAddress, InstallAddress, and JobSource</param>
        /// <param name="quote">Quote data, will use JobItems and JobItems.Options</param>                
        /// <param name="type">Export format</param> 
        /// 
        public static MemoryStream Serialize(CRMContextEx db, Franchise franchise, Job job, JobQuote quote, Data.Invoice invoice = null, ExportTypeEnum type = ExportTypeEnum.PDF)
        {
            // We are not using Jobs anymore, so it requires a new implementation.
            throw new NotImplementedException();
        }
            //        public static MemoryStream Serialize(CRMContextEx db, Franchise franchise, Job job, JobQuote quote,  Data.Invoice invoice = null, ExportTypeEnum type = ExportTypeEnum.PDF)
            //        {
            //            var ExportMgr = new ExportManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
            //            //JobsManager jobMgr = new JobsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

            //            var saleMadeStatusIds = new int [1, 2]; // null; //jobMgr.GetJobStatusesAfterSaleMade();
            //            var reportName = "Quote";
            //            if (saleMadeStatusIds.Contains(job.JobStatusId))
            //            {
            //                reportName = "Sale";
            //                if (invoice != null)
            //                {
            //                    reportName = "Invoice";
            //                }
            //            }
            //            var shortLegal = string.Empty;
            //            var longLegal = string.Empty;

            //            var invoiceOption = ExportMgr.GetFranchiseInvoiceOptions();
            //            if (invoiceOption != null)
            //            {
            //                shortLegal = invoiceOption.ShortLegal;
            //                longLegal = invoiceOption.LongLegal;

            //            }

            //            string BrandedPath=string.Format("/Templates/Base{0}/QuoteTemplate_new.docx", AppConfigManager.BrandedPath.Replace(@"\",@"/") );
            //            string srcFilePath = HostingEnvironment.MapPath(BrandedPath);

            //            var destination = new MemoryStream();

            //            using (FileStream fs = File.OpenRead(srcFilePath))
            //            {
            //                fs.CopyTo(destination);
            //            }

            //            using (WordprocessingDocument document = WordprocessingDocument.Open(destination, true))
            //            {
            //                document.ChangeDocumentType(WordprocessingDocumentType.Document);

            //                MainDocumentPart mainPart = document.MainDocumentPart;

            //                var number = job.JobNumber.ToString();
            //                if (invoice != null)
            //                {
            //                    number = invoice.InvoiceNumber;
            //                }

            //                FillHeader(mainPart, franchise, string.Format("{0}", number), reportName);
            //                FillFooter(mainPart, franchise);

            //                List<SdtBlock> sdtBlockList = mainPart.Document.Descendants<SdtBlock>().ToList();
            //                List<SdtRun> sdtRunList = mainPart.Document.Descendants<SdtRun>().ToList();
            //                foreach (var block in sdtBlockList)
            //                {
            //                    var alias = block.SdtProperties.GetFirstChild<SdtAlias>();
            //                    if (alias == null)
            //                        continue;

            //                    var prop = alias.Val.Value;
            //                    if (prop == "Name")
            //                        OpenXmlUtil.FillContentControl(block, job.Customer.FullName);
            //                    else if (prop == "Company")
            //                        OpenXmlUtil.FillContentControl(block, job.Customer.CompanyName);
            //                    else if (prop == "ShortLegal")
            //                        OpenXmlUtil.FillContentControl(block, shortLegal);
            //                    else if (prop == "LongLegal")
            //                        OpenXmlUtil.FillContentControl(block, longLegal);

            //                    else if (prop == "Address")
            //                    {
            //                        var addr = job.BillingAddress.Address1;
            //                        if (!string.IsNullOrEmpty(job.BillingAddress.Address2))
            //                            addr += "\n" + job.BillingAddress.Address2;
            //                        OpenXmlUtil.FillContentControl(block, addr);
            //                    }
            //                    else if (prop == "CityZip")
            //                        OpenXmlUtil.FillContentControl(block, string.Format("{0}, {1} {2}", job.BillingAddress.City, job.BillingAddress.State, job.BillingAddress.ZipCode));
            //                    else if (prop == "Phone")
            //                    {
            //                        var phone = string.Format("H: {0}", RegexUtil.FormatUSPhone(job.Customer.HomePhone));
            //                        if (job.Customer.PreferredTFN == "C" && !string.IsNullOrEmpty(job.Customer.CellPhone))
            //                            phone = string.Format("C: {0}", RegexUtil.FormatUSPhone(job.Customer.CellPhone));
            //                        else if (job.Customer.PreferredTFN == "W" && !string.IsNullOrEmpty(job.Customer.WorkPhone))
            //                            phone = string.Format("W: {0}{1}", RegexUtil.FormatUSPhone(job.Customer.WorkPhone), string.IsNullOrEmpty(job.Customer.WorkPhoneExt) ? "" : (" x" + job.Customer.WorkPhoneExt));
            //                        else if (job.Customer.PreferredTFN == "F" && !string.IsNullOrEmpty(job.Customer.FaxPhone))
            //                            phone = string.Format("F: {0}", RegexUtil.FormatUSPhone(job.Customer.FaxPhone));
            //                        OpenXmlUtil.FillContentControl(block, phone);
            //                    }
            //                    else if (prop == "InstallAddress")
            //                    {
            //                        var addr = job.InstallAddress.Address1;
            //                        if (!string.IsNullOrEmpty(job.InstallAddress.Address2))
            //                            addr += "\n" + job.BillingAddress.Address2;
            //                        OpenXmlUtil.FillContentControl(block, addr);
            //                    }
            //                    else if (prop == "InstallCityZip")
            //                    {
            //                        OpenXmlUtil.FillContentControl(block, string.Format("{0}, {1} {2}", job.InstallAddress.City, job.InstallAddress.State, job.InstallAddress.ZipCode));
            //                    }

            //                }
            //                foreach (var run in sdtRunList)
            //                {
            //                    var alias = run.SdtProperties.GetFirstChild<SdtAlias>();
            //                    if (alias == null)
            //                        continue;

            //                    var prop = alias.Val.Value;
            //                    if (prop == "QuoteDate")
            //                    {
            //                        if (quote.Job == null)
            //                        {
            //                            quote.Job = job;
            //                        }
            //                        if (quote.Job != null)
            //                        {
            //                            DateTime? date = quote.Job.QuoteDateUTC ?? quote.CreatedOnUtc;
            //                            date = (invoice != null && job.Invoices != null && quote.Job.ContractedOnUtc.HasValue) ? quote.Job.ContractedOnUtc : date;
            //                            if (date.HasValue)
            //                            {
            //                                OpenXmlUtil.FillContentControl(run, date.Value.ToString("MMMM dd, yyyy"));
            //                            }
            //                            else
            //                            {
            //                                OpenXmlUtil.FillContentControl(run, string.Empty);
            //                            }
            //                        }
            //                    }
            //                    else if (prop == "JobSource")
            //                    {
            //                        var jobsource = job.Source != null ? job.Source.Name : "";

            //                        if (jobsource != null && jobsource.ToLower().Contains("Allocate to all".ToLower()))
            //                        {
            //                            var leadMgr = new LeadsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            //                            var _lead = leadMgr.Get(new List<int> { job.LeadId }).FirstOrDefault();

            //                            var allSources = _lead.LeadSources.ToList();
            //                            var alls = allSources.Aggregate(string.Empty, (current, s) => current + (GetSource(s.SourceId) + ","));

            //                            OpenXmlUtil.FillContentControl(run, alls.TrimEnd(','));
            //                        }
            //                        else
            //                        {
            //                            OpenXmlUtil.FillContentControl(run, jobsource);
            //                        }
            //                    }
            //                    else if (prop == "Consultant")
            //                        OpenXmlUtil.FillContentControl(run, job.SalesPerson != null ? job.SalesPerson.FullName : "");
            //                    else if (prop == "ReportName")
            //                    {
            //                        OpenXmlUtil.FillContentControl(run, reportName);
            //                    }
            //                    else if (prop == "Tax")
            //                    {
            //                        OpenXmlUtil.FillContentControl(run, string.Format("{0:P}", quote.TaxPercent));
            //                    }
            //                    else if (prop == "ShortLegal")
            //                        OpenXmlUtil.FillContentControl(run, shortLegal);
            //                    else if (prop == "LongLegal")
            //                        OpenXmlUtil.FillContentControl(run, longLegal);
            //                }

            //                Table lineTable = null,
            //                    summaryTable = null, taxTable = null, additionalItemsTable = null, paymentTable = null, longLegalTable = null;

            //                foreach (var table in mainPart.Document.Descendants<Table>())
            //                {
            //                    var prop = table.Descendants<TableProperties>().FirstOrDefault();
            //                    if (prop != null)
            //                    {
            //                        var cap = prop.Descendants<TableCaption>().FirstOrDefault();
            //                        if (cap != null && cap.Val == "Quote Items")
            //                            lineTable = table;
            //                        else if (cap != null && cap.Val == "Summary Table")
            //                            summaryTable = table;
            //                         else if (cap != null && cap.Val == "Additional Items Table")
            //                            additionalItemsTable = table;
            //                         else if (cap != null && cap.Val == "Payment Table")
            //                            paymentTable = table;
            //                         else if (cap != null && cap.Val == "Tax Table")
            //                            taxTable = table;
            //                        else if (cap != null && cap.Val == "longLegalTable")
            //                            longLegalTable = table;

            //                    }
            //                }

            //                if (longLegalTable != null)
            //                {
            //                    var row = longLegalTable.Descendants<TableRow>().ElementAt(0);
            //                   var cell = row.Descendants<TableCell>().ElementAt(0);
            //                   var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                   var run = new Run(new Text(longLegal));
            //                   if (p != null) p.Append(run);
            //                   else cell.Append(new Paragraph(run));
            //                }

            //                var additems = ExportMgr.FillJobQuote(db, quote);
            //                if (taxTable != null && quote.SaleAdjustments != null && quote.SaleAdjustments.Count > 0)
            //                {
            //                    int rowNum = 1;
            //                    TableCell cell;
            //                    decimal cost = 0;

            //                    foreach (var item in quote.SaleAdjustments)
            //                    {
            //                        if (item.TypeEnum != SaleAdjTypeEnum.Tax)
            //                        {
            //                            continue;
            //                        }

            //                        TableRow row = null;
            //                        var count = taxTable.Descendants<TableRow>().Count();
            //                        if (count > rowNum)
            //                        {
            //                            row = taxTable.Descendants<TableRow>().ElementAt(rowNum);
            //                        }

            //                        #region add row if it doesn't exist
            //                        if (row == null)
            //                        {
            //                            row = new TableRow(
            //                                    new TableCell(
            //                                         new Paragraph(
            //                                            new ParagraphProperties(
            //                                                new Justification() { Val = JustificationValues.Center }
            //                                            )
            //                                        )
            //                                    ),
            //                                    new TableCell(
            //                                        new Paragraph(
            //                                            new ParagraphProperties(
            //                                                new Justification() { Val = JustificationValues.Center }
            //                                            )
            //                                        )
            //                                    )
            //                                );
            //                            taxTable.Append(row);
            //                        }
            //                        #endregion

            //                        cell = row.Descendants<TableCell>().ElementAt(0);
            //                        if (cell != null)
            //                        {
            //                            var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                            var run = new Run(new Text(string.Format("{0} ({1:P})", item.Category, item.Percent / 100)));
            //                            if (p != null) p.Append(run);
            //                            else cell.Append(new Paragraph(run));
            //                        }

            //                        cell = row.Descendants<TableCell>().ElementAt(1);
            //                        if (cell != null)
            //                        {
            //                            var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                             //(Tax1/total%) X Total Tax$ = $Tax1
            //                             //     quote.TaxPercent / 100


            //                            var c = quote.TaxPercent == 0 || item.Percent == 0 ? 0 :((item.Percent / 100) / (quote.TaxPercent / 100)) * quote.Taxtotal;
            //                            var run = new Run(new Text(string.Format("{0:C}", c)));
            //                            //var run = new Run(new Text(string.Format("{0:C}", item.Percent * cost / 100)));
            //                            if (p != null) p.Append(run);
            //                            else cell.Append(new Paragraph(run));
            //                        }
            //                        rowNum++;
            //                    }
            //                }


            //                if (paymentTable != null)
            //                {
            //                    List<Payment>  payments  = new List<Payment>();
            //                    payments = invoice == null ? ExportMgr.GetPaymentsFromJob(job.JobId) : ExportMgr.GetPayments(invoice.InvoiceId);

            //                    if (payments != null && payments.Count > 0)
            //                    {
            //                        int rowNum = 1;
            //                        TableCell cell;

            //                        foreach (var item in payments.OrderBy(v => v.PaymentId))
            //                        {
            //                            TableRow row = null;
            //                            var count = paymentTable.Descendants<TableRow>().Count();
            //                            if (count > rowNum)
            //                            {
            //                                row = paymentTable.Descendants<TableRow>().ElementAt(rowNum);
            //                            }

            //                            #region add row if it doesn't exist
            //                            if (row == null)
            //                            {
            //                                row = new TableRow(
            //                                        new TableCell(
            //                                            new Paragraph(
            //                                                new ParagraphProperties(
            //                                                    new Justification() { Val = JustificationValues.Center }
            //                                                )
            //                                            )
            //                                        ),
            //                                        new TableCell(
            //                                             new Paragraph(
            //                                                new ParagraphProperties(
            //                                                    new Justification() { Val = JustificationValues.Center }
            //                                                )
            //                                            )
            //                                        ),
            //                                        new TableCell(
            //                                            new Paragraph(
            //                                                new ParagraphProperties(
            //                                                    new Justification() { Val = JustificationValues.Center }
            //                                                )
            //                                            )
            //                                        )
            //                                    );
            //                                paymentTable.Append(row);
            //                            }
            //                            #endregion

            //                            cell = row.Descendants<TableCell>().ElementAt(0);
            //                            if (cell != null)
            //                            {
            //                                var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                                var run = new Run(new Text(item.Memo));
            //                                if (p != null)
            //                                    p.Append(run);
            //                                else
            //                                    cell.Append(new Paragraph(run));
            //                            }
            //                            cell = row.Descendants<TableCell>().ElementAt(1);
            //                            if (cell != null)
            //                            {
            //                                var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                                var run = new Run(new Text(string.Format("{0:d}", item.PaymentDate)));
            //                                if (p != null)
            //                                    p.Append(run);
            //                                else
            //                                    cell.Append(new Paragraph(run));
            //                            }
            //                            cell = row.Descendants<TableCell>().ElementAt(2);
            //                            if (cell != null)
            //                            {
            //                                var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                                var run = new Run(new Text(string.Format("{0:C}", item.Amount)));
            //                                if (p != null)
            //                                    p.Append(run);
            //                                else
            //                                    cell.Append(new Paragraph(run));
            //                            }

            //                            rowNum++;
            //                        }
            //                    }

            //                }

            //                //var additems = ExportMgr.FillJobQuote(quote);
            //                if (additionalItemsTable != null && quote.SaleAdjustments != null && quote.SaleAdjustments.Count > 0 )
            //                {
            //                    int rowNum = 1;
            //                    TableCell cell;

            //                    decimal cost = 0;
            //                    if (quote.JobItems != null && quote.JobItems.Count>0)
            //                    {
            //                        cost = quote.JobItems.Sum(v => v.Subtotal);
            //                    }

            //                    foreach (var item in quote.SaleAdjustments)
            //                    {
            //                        if (item.TypeEnum == SaleAdjTypeEnum.Tax)
            //                        {
            //                            continue;
            //                        }

            //                        TableRow row = null;
            //                        var count = additionalItemsTable.Descendants<TableRow>().Count();

            //                        if (count > rowNum)
            //                        {
            //                            row = additionalItemsTable.Descendants<TableRow>().ElementAt(rowNum);
            //                        }


            //                        #region add row if it doesn't exist
            //                        if (row == null)
            //                        {
            //                            row = new TableRow(
            //                                    new TableCell(
            //                                        new Paragraph(
            //                                            new ParagraphProperties(
            //                                                new Justification() { Val = JustificationValues.Center }
            //                                            )
            //                                        )
            //                                    ),
            //                                    new TableCell(
            //                                         new Paragraph(
            //                                            new ParagraphProperties(
            //                                                new Justification() { Val = JustificationValues.Center }
            //                                            )
            //                                        )
            //                                    ),
            //                                    new TableCell(
            //                                         new Paragraph(
            //                                            new ParagraphProperties(
            //                                                new Justification() { Val = JustificationValues.Center }
            //                                            )
            //                                        )
            //                                    ),
            //                                    new TableCell(
            //                                         new Paragraph(
            //                                            new ParagraphProperties(
            //                                                new Justification() { Val = JustificationValues.Center }
            //                                            )
            //                                        )
            //                                    )
            //                                );
            //                            additionalItemsTable.Append(row);
            //                        }
            //                        #endregion

            //                        cell = row.Descendants<TableCell>().ElementAt(0);
            //                        if (cell != null)
            //                        {
            //                            var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                            var run = new Run(new Text(item.TypeEnum.ToString()));
            //                            if (p != null)
            //                                p.Append(run);
            //                            else
            //                                cell.Append(new Paragraph(run));
            //                        }
            //                        cell = row.Descendants<TableCell>().ElementAt(1);
            //                        if (cell != null)
            //                        {
            //                            var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                            var run = new Run(new Text(item.Category));
            //                            if (p != null)
            //                                p.Append(run);
            //                            else
            //                                cell.Append(new Paragraph(run));
            //                        }

            //                        cell = row.Descendants<TableCell>().ElementAt(2);
            //                        if (cell != null)
            //                        {
            //                            var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                            decimal valueAmount = item.Amount;
            //                            if (item.TypeEnum == SaleAdjTypeEnum.Discount || item.TypeEnum == SaleAdjTypeEnum.Surcharge)
            //                            {
            //                                if (valueAmount == 0)
            //                                {
            //                                    valueAmount = item.Percent * cost / 100;
            //                                }
            //                            }

            //                            var run = new Run(new Text(string.Format("{0:C}", valueAmount)));
            //                            if (p != null)
            //                                p.Append(run);
            //                            else
            //                                cell.Append(new Paragraph(run));
            //                        }
            //                        cell = row.Descendants<TableCell>().ElementAt(3);
            //                        if (cell != null)
            //                        {
            //                            var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                            var run = new Run(new Text(item.IsTaxable ? "Yes" : "No"));
            //                            if (p != null)
            //                                p.Append(run);
            //                            else
            //                                cell.Append(new Paragraph(run));
            //                        }
            //                        rowNum++;
            //                    }
            //                }
            //                //var tt = ExportMgr.FillInvoice(invoice.InvoiceId);
            //                if (lineTable != null && quote.JobItems != null && quote.JobItems.Count > 0)
            //                {
            //                    //var tblProp = lineTable.Descendants<TableProperties>().FirstOrDefault();
            //                    //var cellMarginDef = tblProp.Descendants<TableCellMarginDefault>().FirstOrDefault();
            //                    //TableCellMargin cellMargin = null;
            //                    //if (cellMarginDef != null)
            //                    //{
            //                    //    cellMargin = new TableCellMargin
            //                    //    {
            //                    //        BottomMargin = new BottomMargin { Width = cellMarginDef.BottomMargin.Width.InnerText, Type = TableWidthUnitValues.Dxa },
            //                    //        TopMargin = new TopMargin { Width = cellMarginDef.TopMargin.Width.InnerText, Type = TableWidthUnitValues.Dxa },
            //                    //        LeftMargin = new LeftMargin { Width = cellMarginDef.TableCellLeftMargin.Width.InnerText, Type = TableWidthUnitValues.Dxa },
            //                    //        RightMargin = new RightMargin { Width = cellMarginDef.TableCellRightMargin.Width.InnerText, Type = TableWidthUnitValues.Dxa }
            //                    //    };
            //                    //}

            //                    int rowNum = 1;
            //                    TableCell cell;
            //                    foreach (var item in quote.JobItems.OrderBy(o => o.SortOrder))
            //                    {
            //                        TableRow row = null;
            //                        var count = lineTable.Descendants<TableRow>().Count();
            //                        if (count > rowNum)
            //                        {
            //                            row = lineTable.Descendants<TableRow>().ElementAt(rowNum);
            //                        }

            //                        #region add row if it doesn't exist
            //                        if (row == null)
            //                        {
            //                            if (AppConfigManager.BrandedPath.Contains("tl") || AppConfigManager.BrandedPath.Contains("cc"))
            //                            {

            //                                row = new TableRow(
            //                                                                   new TableCell(
            //                                                                        new Paragraph(
            //                                                                           new ParagraphProperties(
            //                                                                               new Justification() { Val = JustificationValues.Center }
            //                                                                           )
            //                                                                       )
            //                                                                   ),
            //                                                                   new TableCell(
            //                                                                       new Paragraph(
            //                                                                           new ParagraphProperties(
            //                                                                               new Justification() { Val = JustificationValues.Center }
            //                                                                           )
            //                                                                       )
            //                                                                   ),
            //                                                                   new TableCell(
            //                                                                        new Paragraph(
            //                                                                           new ParagraphProperties(
            //                                                                               new Justification() { Val = JustificationValues.Center }
            //                                                                           )
            //                                                                       )
            //                                                                   ),

            //                                                                   new TableCell(
            //                                                                        new Paragraph(
            //                                                                           new ParagraphProperties(
            //                                                                               new Justification() { Val = JustificationValues.Center }
            //                                                                           )
            //                                                                       )
            //                                                                   ),
            //                                                                   new TableCell(
            //                                                                       new Paragraph(
            //                                                                           new ParagraphProperties(
            //                                                                               new Justification() { Val = JustificationValues.Center }
            //                                                                           )
            //                                                                       )
            //                                                                   ),
            //                                                                   new TableCell(
            //                                                                       new Paragraph(
            //                                                                           new ParagraphProperties(
            //                                                                               new Justification() { Val = JustificationValues.Center }
            //                                                                           )
            //                                                                       )
            //                                                                   ),
            //                                                                   new TableCell(
            //                                                                       new Paragraph(
            //                                                                           new ParagraphProperties(
            //                                                                               new Justification() { Val = JustificationValues.Center }
            //                                                                           )
            //                                                                       )
            //                                                                   ),
            //                                                                   new TableCell(
            //                                                                       new Paragraph(
            //                                                                           new ParagraphProperties(
            //                                                                               new Justification() { Val = JustificationValues.Center }
            //                                                                           )
            //                                                                       )
            //                                                                   )
            //                                                               );

            //                            }
            //                            else
            //                            {
            //                                row = new TableRow(
            //                                  new TableCell(
            //                                       new Paragraph(
            //                                          new ParagraphProperties(
            //                                              new Justification() { Val = JustificationValues.Center }
            //                                          )
            //                                      )
            //                                  ),
            //                                  new TableCell(
            //                                      new Paragraph(
            //                                          new ParagraphProperties(
            //                                              new Justification() { Val = JustificationValues.Center }
            //                                          )
            //                                      )
            //                                  ),
            //                                  new TableCell(
            //                                       new Paragraph(
            //                                          new ParagraphProperties(
            //                                              new Justification() { Val = JustificationValues.Center }
            //                                          )
            //                                      )
            //                                  ),

            //                                  new TableCell(
            //                                       new Paragraph(
            //                                          new ParagraphProperties(
            //                                              new Justification() { Val = JustificationValues.Center }
            //                                          )
            //                                      )
            //                                  ),
            //                                  new TableCell(
            //                                      new Paragraph(
            //                                          new ParagraphProperties(
            //                                              new Justification() { Val = JustificationValues.Center }
            //                                          )
            //                                      )
            //                                  ),
            //                                  new TableCell(
            //                                      new Paragraph(
            //                                          new ParagraphProperties(
            //                                              new Justification() { Val = JustificationValues.Center }
            //                                          )
            //                                      )
            //                                  ),
            //                                  new TableCell(
            //                                      new Paragraph(
            //                                          new ParagraphProperties(
            //                                              new Justification() { Val = JustificationValues.Center }
            //                                          )
            //                                      )
            //                                  ),
            //                                  new TableCell(
            //                                      new Paragraph(
            //                                          new ParagraphProperties(
            //                                              new Justification() { Val = JustificationValues.Center }
            //                                          )
            //                                      )
            //                                  )
            //                                  ,
            //                                  new TableCell(
            //                                      new Paragraph(
            //                                          new ParagraphProperties(
            //                                              new Justification() { Val = JustificationValues.Center }
            //                                          )
            //                                      )
            //                                  )

            //                              );
            //                            }

            //                            lineTable.Append(row);
            //                        }
            //                        #endregion

            //                        if (AppConfigManager.BrandedPath.Contains("tl") || AppConfigManager.BrandedPath.Contains("cc"))
            //                        {

            //                            #region Fill in options
            //                            if (item.Options != null && item.Options.Count > 0)
            //                            {
            //                                var opt = item.Options.FirstOrDefault(f => f.Name == "Color");
            //                                if (opt != null)
            //                                {
            //                                    cell = row.Descendants<TableCell>().ElementAt(2);
            //                                    if (cell != null)
            //                                    {
            //                                        var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                                        var run = new Run(new Text(opt.Value));
            //                                        if (p != null)
            //                                            p.Append(run);
            //                                        else
            //                                            cell.Append(new Paragraph(run));
            //                                    }
            //                                }
            //                            }

            //                            #endregion

            //                            cell = row.Descendants<TableCell>().ElementAt(0);
            //                            if (cell != null)
            //                            {
            //                                var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                                var run = new Run(new Text(item.CategoryName));
            //                                if (p != null)
            //                                    p.Append(run);
            //                                else
            //                                    cell.Append(new Paragraph(run));
            //                            }

            //                            cell = row.Descendants<TableCell>().ElementAt(1);
            //                            if (cell != null)
            //                            {
            //                                var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                                var run = new Run(new Text(item.ProductType));
            //                                if (p != null)
            //                                    p.Append(run);
            //                                else
            //                                    cell.Append(new Paragraph(run));
            //                            }

            //                            cell = row.Descendants<TableCell>().ElementAt(3);
            //                            if (cell != null)
            //                            {
            //                                var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                                var run = new Run(new Text(string.Empty));
            //                                //var run = new Run(new Text(string.Format("{0:C}", item.JobberPrice)));
            //                                if (p != null)
            //                                    p.Append(run);
            //                                else
            //                                    cell.Append(new Paragraph(run));
            //                            }
            //                            cell = row.Descendants<TableCell>().ElementAt(4);
            //                            if (cell != null)
            //                            {
            //                                var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                                var run = new Run(new Text(string.Format("{0:C}", item.SalePrice)));
            //                                if (p != null)
            //                                    p.Append(run);
            //                                else
            //                                    cell.Append(new Paragraph(run));
            //                            }
            //                            cell = row.Descendants<TableCell>().ElementAt(5);
            //                            if (cell != null)
            //                            {
            //                                var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                                var run = new Run(new Text(item.Quantity.ToString()));
            //                                if (p != null)
            //                                    p.Append(run);
            //                                else
            //                                    cell.Append(new Paragraph(run));
            //                            }

            //                            cell = row.Descendants<TableCell>().ElementAt(6);
            //                            if (cell != null)
            //                            {
            //                                var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                                var run = new Run(new Text(string.Format("{0:C}", item.Subtotal)));
            //                                if (p != null)
            //                                    p.Append(run);
            //                                else
            //                                    cell.Append(new Paragraph(run));
            //                            }
            //                            cell = row.Descendants<TableCell>().ElementAt(7);
            //                            if (cell != null)
            //                            {
            //                                var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                                var run = new Run(new Text(item.IsTaxable ? "Yes" : "No"));
            //                                if (p != null)
            //                                    p.Append(run);
            //                                else
            //                                    cell.Append(new Paragraph(run));
            //                            }

            //                            // insert Job Details row
            //                            if (rowNum == 1)
            //                            {
            //                                row = lineTable.Descendants<TableRow>().ElementAt(2);
            //                            }
            //                            else
            //                            {
            //                                row = (TableRow)lineTable.Descendants<TableRow>().ElementAt(2).Clone();
            //                                lineTable.Append(row);
            //                            }

            //                            cell = row.Descendants<TableCell>().ElementAt(1);
            //                            if (cell != null)
            //                            {
            //                                var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                                p.Remove();
            //                                var run = new Run(new Text(item.Notes));
            //                                cell.Append(new Paragraph(run));
            //                            }
            //                            rowNum = rowNum + 2;

            //                        }
            //                        else
            //                        {

            //                            #region Fill in options
            //                            if (item.Options != null && item.Options.Count > 0)
            //                            {
            //                                var opt = item.Options.FirstOrDefault(f => f.Name == "Room Location");
            //                                if (opt != null)
            //                                {
            //                                    cell = row.Descendants<TableCell>().ElementAt(3);
            //                                    if (cell != null)
            //                                    {
            //                                        var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                                        var run = new Run(new Text(opt.Value));
            //                                        if (p != null)
            //                                            p.Append(run);
            //                                        else
            //                                            cell.Append(new Paragraph(run));
            //                                    }
            //                                }
            //                                //opt = item.Options.FirstOrDefault(f => f.Name == "Mount");
            //                                //if (opt != null && !string.IsNullOrEmpty(opt.Value))
            //                                //{
            //                                //    cell = row.Descendants<TableCell>().ElementAt(1);
            //                                //    if (cell != null)
            //                                //    {
            //                                //        var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                                //        var run = new Run(new Text(opt.Value.Substring(0, 1)));
            //                                //        if (p != null)
            //                                //            p.Append(run);
            //                                //        else
            //                                //            cell.Append(new Paragraph(run));
            //                                //    }
            //                                //}
            //                                //opt = item.Options.FirstOrDefault(f => f.Name == "Controls");
            //                                //if (opt != null && !string.IsNullOrEmpty(opt.Value))
            //                                //{
            //                                //    cell = row.Descendants<TableCell>().ElementAt(2);
            //                                //    if (cell != null)
            //                                //    {
            //                                //        var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                                //        var run = new Run(new Text(opt.Value.Substring(0, 1)));
            //                                //        if (p != null)
            //                                //            p.Append(run);
            //                                //        else
            //                                //            cell.Append(new Paragraph(run));
            //                                //    }
            //                                //}
            //                                opt = item.Options.FirstOrDefault(f => f.Name == "Color");
            //                                if (opt != null)
            //                                {
            //                                    cell = row.Descendants<TableCell>().ElementAt(2);
            //                                    if (cell != null)
            //                                    {
            //                                        var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                                        var run = new Run(new Text(opt.Value));
            //                                        if (p != null)
            //                                            p.Append(run);
            //                                        else
            //                                            cell.Append(new Paragraph(run));
            //                                    }
            //                                }
            //                            }
            //                            #endregion

            //                            cell = row.Descendants<TableCell>().ElementAt(0);
            //                            if (cell != null)
            //                            {
            //                                var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                                var run = new Run(new Text(item.ProductType));
            //                                if (p != null)
            //                                    p.Append(run);
            //                                else
            //                                    cell.Append(new Paragraph(run));
            //                            }

            //                            cell = row.Descendants<TableCell>().ElementAt(1);
            //                            if (cell != null)
            //                            {
            //                                var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                                var run = new Run(new Text(item.ProductName));
            //                                if (p != null)
            //                                    p.Append(run);
            //                                else
            //                                    cell.Append(new Paragraph(run));
            //                            }

            //                            cell = row.Descendants<TableCell>().ElementAt(4);
            //                            if (cell != null)
            //                            {
            //                                var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                                var run = new Run(new Text(string.Empty));
            //                                //var run = new Run(new Text(string.Format("{0:C}", item.JobberPrice)));
            //                                if (p != null)
            //                                    p.Append(run);
            //                                else
            //                                    cell.Append(new Paragraph(run));
            //                            }
            //                            cell = row.Descendants<TableCell>().ElementAt(5);
            //                            if (cell != null)
            //                            {
            //                                var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                                var run = new Run(new Text(string.Format("{0:C}", item.SalePrice)));
            //                                if (p != null)
            //                                    p.Append(run);
            //                                else
            //                                    cell.Append(new Paragraph(run));
            //                            }
            //                            cell = row.Descendants<TableCell>().ElementAt(6);
            //                            if (cell != null)
            //                            {
            //                                var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                                var run = new Run(new Text(item.Quantity.ToString()));
            //                                if (p != null)
            //                                    p.Append(run);
            //                                else
            //                                    cell.Append(new Paragraph(run));
            //                            }

            //                            cell = row.Descendants<TableCell>().ElementAt(7);
            //                            if (cell != null)
            //                            {
            //                                var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                                var run = new Run(new Text(string.Format("{0:C}", item.Subtotal)));
            //                                if (p != null)
            //                                    p.Append(run);
            //                                else
            //                                    cell.Append(new Paragraph(run));
            //                            }
            //                            cell = row.Descendants<TableCell>().ElementAt(8);
            //                            if (cell != null)
            //                            {
            //                                var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                                var run = new Run(new Text(item.IsTaxable ? "Yes" : "No"));
            //                                if (p != null)
            //                                    p.Append(run);
            //                                else
            //                                    cell.Append(new Paragraph(run));
            //                            }
            //                            rowNum++;

            //                        }
            //                    }
            //                }
            //                if (summaryTable != null)
            //                {
            //                    decimal cost = 0;
            //                    if (quote.JobItems != null && quote.JobItems.Count > 0)
            //                    {
            //                        cost = quote.JobItems.Sum(v => v.Subtotal);
            //                    }
            //                    var footerRow = summaryTable.Descendants<TableRow>().ElementAt(0);

            //                    var cell = footerRow.Descendants<TableCell>().LastOrDefault();
            //                    if (cell != null)
            //                    {
            //                        var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                        var run = new Run(new Text(string.Format("{0:C}", quote.Subtotal)));
            //                        if (p != null)
            //                            p.Append(run);
            //                        else
            //                            cell.Append(new Paragraph(run));
            //                    }

            //                    footerRow = summaryTable.Descendants<TableRow>().ElementAt(1);
            //                    cell = footerRow.Descendants<TableCell>().LastOrDefault();
            //                    if (cell != null)
            //                    {
            //                        var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                        var val = quote.SurchargeTotal > 0 ? quote.SurchargeTotal : 0;
            //                        var run = new Run(new Text(string.Format("{0:C}", val)));
            //                        if (p != null)
            //                            p.Append(run);
            //                        else
            //                            cell.Append(new Paragraph(run));
            //                    }

            //                    decimal additionalDiscount = 0;
            //                    foreach (var item in quote.SaleAdjustments)
            //                    {
            //                        if (item.TypeEnum == SaleAdjTypeEnum.Discount)
            //                        {
            //                            decimal valueAmount = item.Amount;
            //                            if (valueAmount == 0)
            //                            {
            //                                valueAmount = item.Percent * cost / 100;
            //                            }
            //                            additionalDiscount = additionalDiscount + valueAmount;
            //                        }
            //                    }

            //                    footerRow = summaryTable.Descendants<TableRow>().ElementAt(2);
            //                    cell = footerRow.Descendants<TableCell>().LastOrDefault();
            //                    if (cell != null)
            //                    {
            //                        var p = cell.Descendants<Paragraph>().FirstOrDefault();

            //                        var val = additionalDiscount;
            //                        var run = new Run(new Text(string.Format("{0:C}", val)));
            //                        if (p != null)
            //                            p.Append(run);
            //                        else
            //                            cell.Append(new Paragraph(run));
            //                    }

            //                    footerRow = summaryTable.Descendants<TableRow>().ElementAt(3);
            //                    cell = footerRow.Descendants<TableCell>().LastOrDefault();
            //                    if (cell != null)
            //                    {
            //                        var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                        var val = quote.Subtotal + quote.SurchargeTotal - additionalDiscount;
            //                        var run = new Run(new Text(string.Format("{0:C}", val)));

            //                        //var run = new Run(new Text(quote.SurchargeTotal > 0 ? string.Format("{0:C}", quote.Subtotal + quote.SurchargeTotal - quote.DiscountTotal) : ""));
            //                        if (p != null)
            //                            p.Append(run);
            //                        else
            //                            cell.Append(new Paragraph(run));
            //                    }

            //                    if (quote.Taxtotal > 0)
            //                    {
            //                        var columnCount = footerRow.Descendants<TableCell>().Count();
            //                        footerRow = summaryTable.Descendants<TableRow>().ElementAt(4);
            //                        cell = footerRow.Descendants<TableCell>().ElementAt(columnCount - 2);
            //                        if (cell != null)
            //                        {
            //                            var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                            var run = p.Descendants<Run>().LastOrDefault();
            //                            if (run != null) run.Descendants<Text>().First().Text = string.Format("Tax Total ({0:P})", quote.TaxPercent / 100);
            //                        }

            //                        cell = footerRow.Descendants<TableCell>().LastOrDefault();
            //                        if (cell != null)
            //                        {
            //                            var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                            var run = new Run(new Text(string.Format("{0:C}", quote.Taxtotal)));
            //                            if (p != null) p.Append(run);
            //                            else cell.Append(new Paragraph(run));
            //                        }
            //                    }
            //                    else
            //                    {
            //                        var columnCount = footerRow.Descendants<TableCell>().Count();
            //                        footerRow = summaryTable.Descendants<TableRow>().ElementAt(4);
            //                        cell = footerRow.Descendants<TableCell>().ElementAt(columnCount - 2);
            //                        if (cell != null)
            //                        {
            //                            var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                            var run = p.Descendants<Run>().LastOrDefault();
            //                            if (run != null) run.Descendants<Text>().First().Text = string.Format("Tax Total ({0:P})", 0);
            //                        }
            //                    }

            //                    footerRow = summaryTable.Descendants<TableRow>().ElementAt(5);
            //                    cell = footerRow.Descendants<TableCell>().LastOrDefault();
            //                    if (cell != null)
            //                    {
            //                        var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                        var run = new Run(new Text(string.Format("{0:C}", quote.NetTotal)));
            //                        if (p != null)
            //                            p.Append(run);
            //                        else
            //                            cell.Append(new Paragraph(run));
            //                    }

            //                    footerRow = summaryTable.Descendants<TableRow>().ElementAt(6);
            //                    cell = footerRow.Descendants<TableCell>().LastOrDefault();
            //                    if (cell != null)
            //                    {
            //                        var val = quote.NetTotal;
            //                        var p = cell.Descendants<Paragraph>().FirstOrDefault();

            //                        if ((job.Invoices != null && job.Invoices.Count > 0) || (invoice != null && invoice.Job != null )) // Since the print comes from Job and from Invoice.
            //                        {
            //                            Job _job;
            //                            _job = ((invoice != null && invoice.Job != null)) ? invoice.Job : job; // Since the print comes from Job and from Invoice that can be null
            //                            if (_job.Invoices.FirstOrDefault().Payments != null && _job.Invoices.FirstOrDefault().Payments.Count > 0)
            //                            {
            //                                val = _job.Balance;
            //                            }
            //                        }

            //                        var run = new Run(new Text(string.Format("{0:C}", val)));
            //                        if (p != null)
            //                            p.Append(run);
            //                        else
            //                            cell.Append(new Paragraph(run));
            //                    }

            //                    decimal totalDiscount = 0;
            //                    foreach (var item in quote.JobItems)
            //                    {
            //                        decimal valueAmount = item.DiscountAmount;

            //                        if (valueAmount == 0)
            //                        {
            //                            valueAmount = item.DiscountPercent * cost / 100;
            //                        }
            //                        totalDiscount = totalDiscount + valueAmount;
            //                    }

            //                    foreach (var item in quote.SaleAdjustments)
            //                    {
            //                            if (item.TypeEnum == SaleAdjTypeEnum.Discount)
            //                            {
            //                                decimal valueAmount = item.Amount;
            //                                if (valueAmount == 0)
            //                                {
            //                                    valueAmount = item.Percent * cost / 100;
            //                                }
            //                                totalDiscount = totalDiscount + valueAmount;
            //                            }
            //                    }

            //                    footerRow = summaryTable.Descendants<TableRow>().ElementAt(7);
            //                    cell = footerRow.Descendants<TableCell>().LastOrDefault();
            //                    if (cell != null)
            //                    {
            //                        var p = cell.Descendants<Paragraph>().FirstOrDefault();
            //                        var run = new Run(new Text(string.Format("{0:C}", totalDiscount)));
            //                        if (p != null)
            //                            p.Append(run);
            //                        else
            //                            cell.Append(new Paragraph(run));
            //                    }
            //                }

            //                mainPart.Document.Save();

            //#if DEBUG
            //                OpenXmlValidator validator = new OpenXmlValidator();
            //                var errors = validator.Validate(document);
            //#endif

            //                document.Close();
            //            }

            //            var outputStream = new MemoryStream();
            //            if (type == ExportTypeEnum.PDF)
            //            {
            //                var license = new Aspose.Words.License();
            //                license.SetLicense("Aspose.Words.lic");

            //                var doc = new Aspose.Words.Document(destination);
            //                doc.Save(outputStream, Aspose.Words.SaveFormat.Pdf);        
            //            }
            //            else
            //                outputStream = destination;

            //            return outputStream;
            //        }

        }
}
