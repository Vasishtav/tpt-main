﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Validation;
using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.DTO.Search;
using HFC.CRM.Managers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

/// <summary>
/// The Jobs namespace.
/// </summary>
namespace HFC.CRM.Serializer.Jobs
{

    /// <summary>
    /// Class Excel.
    /// </summary>
    public class Excel
    {


        /// <summary>
        /// Exports the jobs data into an Excel file
        /// </summary>
        /// <param name="jobCollection">The job collection.</param>
        /// <returns>MemoryStream.</returns>
        public static MemoryStream BasicJobInfo(List<JobSearchExportDTO> jobCollection, List<Address> addresses, List<LeadSource> leadSources)
        {
            MemoryStream outputStream = new MemoryStream();

            try
            {
                //create spreadsheet  -- create a spreadsheet decument from an IO stream
                SpreadsheetDocument spreadsheet = OpenXmlUtil.CreateWorkbook(outputStream);
                Worksheet worksheet = spreadsheet.AddWorksheet("Jobs");
                spreadsheet.AddBasicStyles();

                Row headRow;
                if (AppConfigManager.ApplicationTitle.ToLower().Contains("budget blinds"))
                {
                    //add header row
                    headRow = worksheet.AddHeaderRow(new OpenXmlColumnData("Job Number"),
                                                new OpenXmlColumnData("Customer Name"),
                                                new OpenXmlColumnData("Pref. Contact Number"),
                                                new OpenXmlColumnData("Job Status"),
                                                new OpenXmlColumnData("Job Source"),
                                                new OpenXmlColumnData("Sales Agent"),
                                                new OpenXmlColumnData("Email"),
                                                new OpenXmlColumnData("Product Subtotal"),
                                                new OpenXmlColumnData("Discount Total"),
                                                new OpenXmlColumnData("Surcharge Total"),
                                                new OpenXmlColumnData("Tax Total"),
                                                new OpenXmlColumnData("Est. Gross Profit"),
                                                new OpenXmlColumnData("Total"),
                                                new OpenXmlColumnData("Balance"),
                                                new OpenXmlColumnData("Creation Date"),
                                                new OpenXmlColumnData("Contracted Date"),
                                                new OpenXmlColumnData("Billing Address", 35),
                                                new OpenXmlColumnData("Install Address", 35),
                                                new OpenXmlColumnData("Manufacturer", 35),
                                                new OpenXmlColumnData("Type", 35),
                                                new OpenXmlColumnData("Product", 35),
                                                new OpenXmlColumnData("Qty"),
                                                new OpenXmlColumnData("Sale Price"),
                                                new OpenXmlColumnData("Cost"),
                                                new OpenXmlColumnData("Discount"),
                                                new OpenXmlColumnData("Line Item Subtotal"));
                }
                else
                {
                    headRow = worksheet.AddHeaderRow(new OpenXmlColumnData("Job Number"),
                                               new OpenXmlColumnData("Customer Name"),
                                               new OpenXmlColumnData("Pref. Contact Number"),
                                               new OpenXmlColumnData("Job Status"),
                                               new OpenXmlColumnData("Job Source"),
                                               new OpenXmlColumnData("Sales Agent"),
                                               new OpenXmlColumnData("Email"),
                                               new OpenXmlColumnData("Product Subtotal"),
                                               new OpenXmlColumnData("Discount Total"),
                                               new OpenXmlColumnData("Surcharge Total"),
                                               new OpenXmlColumnData("Tax Total"),
                                               new OpenXmlColumnData("Est. Gross Profit"),
                                               new OpenXmlColumnData("Total"),
                                               new OpenXmlColumnData("Balance"),
                                               new OpenXmlColumnData("Creation Date"),
                                               new OpenXmlColumnData("Contracted Date"),
                                               new OpenXmlColumnData("Billing Address", 35),
                                               new OpenXmlColumnData("Install Address", 35),
                                                new OpenXmlColumnData("Category", 35),
                                                new OpenXmlColumnData("Product", 35),
                                               new OpenXmlColumnData("Qty"),
                                               new OpenXmlColumnData("Sale Price"),
                                               new OpenXmlColumnData("Cost"),
                                               new OpenXmlColumnData("Discount"),
                                               new OpenXmlColumnData("Line Item Subtotal"));
                }
                //Add data
                foreach (var job in jobCollection)
                {
                    var row = worksheet.AddRow();
                    AddData(row, job, addresses, leadSources);

                    //var primQuote = job.JobQuotes.FirstOrDefault(f => f.IsPrimary);
                    //if (primQuote != null)
                    //{
                    //    if (primQuote.JobItems != null && primQuote.JobItems.Count > 0)
                    //    {
                    //        foreach (var item in primQuote.JobItems)
                    //        {
                    //            var row = worksheet.AddRow();
                    //            AddData(row, job, primQuote);
                    //            AddData(row, item);
                    //        }
                    //    }
                    //    else
                    //    {
                    //        var row = worksheet.AddRow();
                    //       AddData(row, job, primQuote);
                    //    }
                    //}
                }

                worksheet.Save();
#if DEBUG
                OpenXmlValidator validator = new OpenXmlValidator();
                var errors = validator.Validate(spreadsheet);
#endif

                spreadsheet.Close();
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
            }

            return outputStream;
        }

        /// <summary>
        /// Adds the data.
        /// </summary>
        /// <param name="row">The row.</param>
        /// <param name="item">The item.</param>
        //private static void AddData(Row row, JobItem item)
        //{
        //    if (AppConfigManager.ApplicationTitle.ToLower().Contains("budget blinds"))
        //    {
        //        if (item.Manufacturer != null)
        //            row.AddCell(item.Manufacturer);
        //        else
        //            row.AddCell("");
        //        if (item.ProductType != null)
        //            row.AddCell(item.ProductType);
        //        else
        //            row.AddCell("");
        //        if (item.ProductName != null)
        //            row.AddCell(item.ProductName);
        //        else
        //            row.AddCell("");
        //    }

        //    if (AppConfigManager.ApplicationTitle.ToLower().Contains("tailored living"))
        //    {
        //        if (!string.IsNullOrEmpty(item.CategoryName))
        //            row.AddCell(item.CategoryName);
        //        else
        //            row.AddCell("");
        //        if (item.ProductType != null)
        //            row.AddCell(item.ProductType);
        //        else
        //            row.AddCell("");

        //    }

        //    row.AddCell(item.Quantity);
        //    row.AddCell(item.SalePrice);
        //    row.AddCell(item.UnitCost);
        //    row.AddCell(item.DiscountAmount);
        //    row.AddCell(item.Subtotal);
        //}

        /// <summary>
        /// Adds the data.
        /// </summary>
        /// <param name="row">The row.</param>
        /// <param name="job">The job.</param>
        private static void AddData(Row row, JobSearchExportDTO job, List<Address> addresses, List<LeadSource> leadSourcesList)
        {
            row.AddCell(job.JobNumber);

            //if (job.CustomerPersonId != null)
            //{
                row.AddCell(job.CustomerFullName);

                if (job.PreferredTFN == "C")
                    row.AddCell(RegexUtil.FormatUSPhone(job.CellPhone));
                else if (job.PreferredTFN == "W")
                    row.AddCell(RegexUtil.FormatUSPhone(job.WorkPhone));
                else
                    row.AddCell(RegexUtil.FormatUSPhone(job.HomePhone));
            //}
            //else
            //{
            //    row.AddCell("");
            //    row.AddCell("");
            //}

            row.AddCell(GetJobStatus(job.JobStatusId));

            var src = GetSource(job.SourceId);

            if (src.ToLower().Equals("allocate to all sources") == true)
            {
                //LeadsManager LeadMgr = new LeadsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                var leadsources = leadSourcesList.Where(v=>v.LeadId == job.LeadId).ToList();

                List<string> leadscr = new List<string>();
                foreach (var leadsrc in leadsources)
                {
                    leadscr.Add(GetSource(leadsrc.SourceId));
                }
                row.AddCell(string.Join(", ", leadscr));

            }
            else
            {
                row.AddCell(GetSource(job.SourceId));
            }

            if (!string.IsNullOrEmpty(job.SalesFullName))
            {
                row.AddCell(job.SalesFullName);
            }
            else
            {
                row.AddCell("");
            }

            //if (job.Lead != null)
            //{
            if (!string.IsNullOrEmpty(job.PrimaryEmail))
            {
                row.AddCell(job.PrimaryEmail);
            }
            else
            {
                row.AddCell("");
            }
            //}
            //else
            //{
            //    row.AddCell("");
            //}

            row.AddCell(job.Subtotal);
            row.AddCell(job.DiscountTotal);
            row.AddCell(job.SurchargeTotal);
            row.AddCell(job.Taxtotal);
            row.AddCell(job.NetProfit);
            row.AddCell(job.NetTotal);
            row.AddCell(job.Balance);

            if (SessionManager.CurrentFranchise != null && SessionManager.CurrentFranchise.FranchiseId > 0)
                row.AddCell(job.CreatedOnUtc);
            else
                row.AddCell(job.CreatedOnUtc);

            if (job.ContractedOnUtc != null)
            {
                DateTime date = (DateTime) job.ContractedOnUtc;
                if (date != null)
                    row.AddCell(date);

            }
               
            else
                row.AddCell("");


            if (job.BillingAddressId != null)
            {
                var billaddress = addresses.FirstOrDefault(v => v.AddressId == job.BillingAddressId);
                if (billaddress != null)
                {
                    row.AddTextCell(billaddress.ToString());
                }
                else
                {
                    row.AddCell("");
                }
            }
            else
                row.AddCell("");

            if (job.InstallAddressId != null)
            {
                var installAddress = addresses.FirstOrDefault(v => v.AddressId == job.InstallAddressId);
                if (installAddress != null)
                {
                    row.AddTextCell(installAddress.ToString());
                }
                else
                {
                    row.AddCell("");
                }
            }
            else
                row.AddCell("");

            if (AppConfigManager.ApplicationTitle.ToLower().Contains("budget blinds"))
            {
                if (job.Manufacturer != null)
                    row.AddCell(job.Manufacturer);
                else
                    row.AddCell("");
                if (job.ProductType != null)
                    row.AddCell(job.ProductType);
                else
                    row.AddCell("");
                if (job.ProductName != null)
                    row.AddCell(job.ProductName);
                else
                    row.AddCell("");
            }

            if (AppConfigManager.ApplicationTitle.ToLower().Contains("tailored living"))
            {
                if (!string.IsNullOrEmpty(job.CategoryName))
                    row.AddCell(job.CategoryName);
                else
                    row.AddCell("");
                if (job.ProductType != null)
                    row.AddCell(job.ProductType);
                else
                    row.AddCell("");

            }

            row.AddCell(job.Quantity);
            row.AddCell(job.SalePrice);
            row.AddCell(job.UnitCost);
            row.AddCell(job.DiscountAmount);
            row.AddCell(job.JobItemSubtotal);

        }

        /// <summary>
        /// Using this method to get the job status in case job didnt retrieve the status object
        /// </summary>
        /// <param name="jobStatusId">The job status identifier.</param>
        /// <returns>System.String.</returns>
        private static string GetJobStatus(int jobStatusId)
        {
            var status = CacheManager.JobStatusTypeCollection.FirstOrDefault(f => f.Id == jobStatusId);
            if (status != null)
                return status.ToString();
            else
                return string.Empty;
        }

        /// <summary>
        /// Gets the source.
        /// </summary>
        /// <param name="sourceId">The source identifier.</param>
        /// <returns>System.String.</returns>
        private static string GetSource(int? sourceId)
        {
            if (sourceId.HasValue && sourceId.Value > 0)
            {
                var src = CacheManager.SourceCollection.FirstOrDefault(f => f.SourceId == sourceId.Value);
                if (src != null)
                    return src.Name.ToString();
                else
                    return string.Empty;
            }
            else
                return string.Empty;
        }
    }
}
