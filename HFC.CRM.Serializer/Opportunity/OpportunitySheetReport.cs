﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Validation;
using DocumentFormat.OpenXml.Wordprocessing;
using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using DocumentFormat.OpenXml.Office2010.Word;
using HFC.CRM.Data.Context;
using System.Data.SqlClient;

namespace HFC.CRM.Serializer.Opportunity
{
    public class Zilloresponce
    {
        public string yearBuilt { get; set; }
        public string lotSizeSqFt { get; set; }
        public string finishedSqFt { get; set; }
        public string bathrooms { get; set; }
        public string bedrooms { get; set; }
        public string lastSoldDate { get; set; }
        public string lastSoldPrice { get; set; }
        public string zestimate { get; set; }
        public string zillowlink { get; set; }
    }
    public class OpportunitySheetReport
    {
        private static AccountsManager accountsMgr = new AccountsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        public static string OpportunitySheet(int id, HFC.CRM.Data.Calendar RecentAppointment, List<string> printoptoins, Zilloresponce zillow = null)
        {
            string data = string.Empty;
            if (SessionManager.BrandId == 1)
            {
                data = Serializer.Opportunity.OpportunitySheetReport.OpportunitySheet_BudgetBlinds(id, RecentAppointment, printoptoins, zillow);
            }
            else if (SessionManager.BrandId == 2)
            {
                data = Serializer.Opportunity.OpportunitySheetReport.OpportunitySheet_TailorLiving(id, RecentAppointment);
            }
            else
            {
                data = Serializer.Opportunity.OpportunitySheetReport.OpportunitySheet_ConcreteCraft(id, RecentAppointment);
            }
            return data;

        }
        public static string FilePath()
        {
            string Path = string.Empty;
            if (SessionManager.BrandId == 1)
            {
                Path = string.Format("/Templates/Base/Brand/BB/OpportunitySheet-Template.html");
            }
            else if(SessionManager.BrandId==2)
            {
                Path = string.Format("/Templates/Base/Brand/TL/OpportunitySheet-Template.html");
            }
            else
            {
                Path = string.Format("/Templates/Base/Brand/CC/OpportunitySheet-Template.html");
            }
            string srcFilePath = HostingEnvironment.MapPath(Path);
            return srcFilePath;
        }
        public static string OpportunitySheet_BudgetBlinds(int id, HFC.CRM.Data.Calendar RecentAppointment, List<string> printoptoins, Zilloresponce zillow = null)
        {
            string LeadData = string.Empty;
            string Path = string.Empty;           
      
            Path = string.Format("/Templates/Base/Brand/BB/OpportunitySheet.html");

            string srcFilePath = HostingEnvironment.MapPath(Path);
            
            AccountTP lead = accountsMgr.Get(id);

            string data = System.IO.File.ReadAllText(srcFilePath);

            string PrimaryContact = lead.PrimCustomer.FullName;
            string CompanyName = lead.PrimCustomer.CompanyName;
            AddressTP BillingAddress = null;
            if (lead.Addresses.Count > 0)
            {
                BillingAddress = lead.Addresses.First();

            }
            string BillingAddress1 = BillingAddress.Address1;
            string BillingAddress2 = BillingAddress.Address2;
            string BillingAddress_CityStateZip = BillingAddress.City + ',' + BillingAddress.State + ',' + BillingAddress.ZipCode;

            string PrimaryCellPhone = GetFormatPhone(lead.PrimCustomer.CellPhone);
            string PrimaryHomePhone = GetFormatPhone(lead.PrimCustomer.HomePhone);
            string PrimaryWorkPhone = GetFormatPhone(lead.PrimCustomer.WorkPhone);
            string PrimaryEmail = lead.PrimCustomer.PrimaryEmail;

            string PrimaryNotes = "";
            AddressTP InstallAddress = null;

            string InstallAddress1 = string.Empty;
            string InstallAddress2 = string.Empty;
            string InstallAddres_CityStateZip = string.Empty;
            if (lead.Addresses.Count > 1)
            {
                //Select secondary address
                InstallAddress = lead.Addresses.First();
                InstallAddress1 = InstallAddress.Address1;
                InstallAddress2 = InstallAddress.Address2;
                InstallAddres_CityStateZip = InstallAddress.City + ',' + InstallAddress.State + ',' + InstallAddress.ZipCode;
            }
                       
            CustomerTP SecCustomer;
            string Secondarycontact = "";
            string SecondaryCellPhone = "";
            if (lead.SecCustomer.Count > 1)
            {
                //Select secondary address
                SecCustomer = lead.SecCustomer.First();
                Secondarycontact = SecCustomer.FullName;
                SecondaryCellPhone = SecCustomer.CellPhone;
            }

            SourceList leadsources = null;

            string LeadSourceName = "";
            string LeadChannelName = "";
            if (lead.SourcesList.Count > 0)
            {
                leadsources = lead.SourcesList.First();
                LeadSourceName = leadsources.name;
                LeadChannelName = leadsources.ChannelName;
            }
            //BB - Qualification Questions
            string BB_QQ_NewHome = ""; string BB_QQ_ExistingHome = ""; string BB_QQ_EstimatedClosingDate = ""; string BB_QQ_ProjectLeadTimePreference = "";
            string BB_QQ_ValPak = ""; string BB_QQ_YellowPages = ""; string BB_QQ_RSVP = ""; string BB_QQ_Van = ""; string BB_QQ_Internet = "";
            string BB_QQ_Sources1 = ""; string BB_QQ_Sources2 = ""; string BB_QQ_Sources3 = ""; string BB_QQ_FaceBookPaid = ""; string BB_QQ_Referel = "";
            string BB_QQ_Budget = "";
            string BB_QQ_AnyAppointment = ""; string BB_QQ_ReceivedOtherQuotesYes = ""; string BB_QQ_ReceivedOtherQuotesNo = ""; string BB_QQ_ReceivedOtherQuotesSpecific = "";

            //BB - Project Needs

            string BB_PN_HowManyWindows = "";
            string BB_PN_Room_Living = ""; string BB_PN_Room_Dining = ""; string BB_PN_Room_Family = ""; string BB_PN_Room_BedRooms = "";
            string BB_PN_Room_Kitchen = ""; string BB_PN_Room_Bath = ""; string BB_PN_Room_Other = ""; string BB_PN_Room_OtherNote = "";

            string BB_PN_treatment_Wood = ""; string BB_PN_treatment_FauxWood = ""; string BB_PN_treatment_Cellular = ""; string BB_PN_treatment_Shades = ""; string BB_PN_treatment_RollerShades = ""; string BB_PN_treatment_Draperies = "";
            string BB_PN_treatment_Romans = ""; string BB_PN_treatment_Verticals = ""; string BB_PN_treatment_Shutters = ""; string BB_PN_treatment_WovenWoods = ""; string BB_PN_treatment_SolarShades = "";

            string BB_PN_Drapery_Operational = ""; string BB_PN_Drapery_Decorative = "";
            string BB_PN_FrabricPreferences = ""; string BB_PN_SolidSOrPrints = ""; string BB_PN_PreferredStyle = "";

            string BB_PN_Notes;

            var leadquestionAns = lead.AccountQuestionAns;

            foreach (var leadqa in leadquestionAns)
            {
                #region QualificationQuestions
                if (leadqa.QuestionId == 1001 && leadqa.Answer != null && leadqa.Answer != "")
                {
                    if (leadqa.Answer == "New Home?")
                    {
                        BB_QQ_NewHome = "Checked";
                        BB_QQ_ExistingHome = "";
                    }
                    else if (leadqa.Answer == "Existing Home?")
                    {
                        BB_QQ_NewHome = "";
                        BB_QQ_ExistingHome = "Checked";
                    }
                }
                if (leadqa.QuestionId == 1003 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_QQ_EstimatedClosingDate = GetFormatDate(leadqa.Answer);
                }
                if (leadqa.QuestionId == 1004 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_QQ_ProjectLeadTimePreference = GetFormatDate(leadqa.Answer);
                }
                #endregion QualificationQuestions
                #region BB
                //How did u hear about BB
                if (leadqa.QuestionId == 1005 && (leadqa.Answer != null || leadqa.Answer != ""))
                {
                    BB_QQ_ValPak = "Checked";
                }
                if (leadqa.QuestionId == 1006 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_QQ_YellowPages = "Checked";
                }
                if (leadqa.QuestionId == 1007 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_QQ_RSVP = "Checked";
                }
                if (leadqa.QuestionId == 1008 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_QQ_Van = "Checked";
                }
                if (leadqa.QuestionId == 1009 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_QQ_Internet = "Checked";
                }
                if (leadqa.QuestionId == 1010 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_QQ_Sources1 = "Checked";
                }
                if (leadqa.QuestionId == 1011 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_QQ_Sources2 = "Checked";
                }
                if (leadqa.QuestionId == 1012 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_QQ_Sources3 = "Checked";
                }
                if (leadqa.QuestionId == 1013 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_QQ_FaceBookPaid = "Checked";
                }
                if (leadqa.QuestionId == 1014 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_QQ_Referel = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1015 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_QQ_Budget = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1016 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_QQ_AnyAppointment = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1017 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_QQ_ReceivedOtherQuotesYes = "Checked";
                }
                if (leadqa.QuestionId == 1018 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_QQ_ReceivedOtherQuotesNo = "Checked";
                }
                if (leadqa.QuestionId == 1019 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_QQ_ReceivedOtherQuotesSpecific = leadqa.Answer;
                }
                

                #endregion BB
                #region ProjectNeeeds
                if (leadqa.QuestionId == 1021 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_PN_HowManyWindows = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1023 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_PN_Room_Living = "Checked";
                }
                if (leadqa.QuestionId == 1023 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_PN_Room_Dining = "Checked";
                }
                if (leadqa.QuestionId == 1024 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_PN_Room_Dining = "Checked";
                }
                if (leadqa.QuestionId == 1025 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_PN_Room_Family = "Checked";
                }
                if (leadqa.QuestionId == 1026 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_PN_Room_BedRooms = "Checked";
                }
                if (leadqa.QuestionId == 1027 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_PN_Room_Kitchen = "Checked";
                }
                if (leadqa.QuestionId == 1028 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_PN_Room_Bath = "Checked";
                }
                if (leadqa.QuestionId == 1029 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_PN_Room_Other = "Checked";
                }
                if (leadqa.QuestionId == 1030 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_PN_Room_OtherNote = leadqa.Answer;
                }
                #endregion  ProjectNeeeds
                #region ProjectNeeeds Treatments
                if (leadqa.QuestionId == 1031 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_PN_treatment_Wood = "Checked";
                }
                if (leadqa.QuestionId == 1032 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_PN_treatment_FauxWood = "Checked";
                }
                if (leadqa.QuestionId == 1033 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_PN_treatment_Cellular = "Checked";
                }
                if (leadqa.QuestionId == 1034 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_PN_treatment_Shades = "Checked";
                }
                if (leadqa.QuestionId == 1035 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_PN_treatment_RollerShades = "Checked";
                }
                if (leadqa.QuestionId == 1036 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_PN_treatment_Draperies = "Checked";
                }
                if (leadqa.QuestionId == 1037 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_PN_treatment_Romans = "Checked";
                }
                if (leadqa.QuestionId == 1038 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_PN_treatment_Verticals = "Checked";
                }
                if (leadqa.QuestionId == 1039 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_PN_treatment_Shutters = "Checked";
                }
                if (leadqa.QuestionId == 1040 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_PN_treatment_WovenWoods = "Checked";
                }
                if (leadqa.QuestionId == 1041 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_PN_treatment_SolarShades = "Checked";
                }
                #endregion ProjectNeeeds Treatments
                #region ProjectNeeeds OperationOrDecorative
                if (leadqa.QuestionId == 1042 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    if (leadqa.Answer == "Operational")
                    {
                        BB_PN_Drapery_Operational = "Checked";
                    }
                    else if (leadqa.Answer == "Decorative")
                    {
                        BB_PN_Drapery_Decorative = "Checked";
                    }
                }
                if (leadqa.QuestionId == 1044 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_PN_FrabricPreferences = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1045 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_PN_SolidSOrPrints = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1046 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    BB_PN_PreferredStyle = leadqa.Answer;
                }
                #endregion

            }

            //Read lead html and append the data
            if (PrimaryContact != null)
            {
                //{{Contactfullname}}
                data = data.Replace("{{Contactfullname}}", PrimaryContact);
            }
            else
            {
                data = data.Replace("{{Contactfullname}}", "");
            }
            if (CompanyName != null)
            {
                data = data.Replace("{{CompanyName}}", CompanyName);
            }
            else
            {
                data = data.Replace("{{CompanyName}}", "");
            }
            if (BillingAddress1 != null)
            {
                data = data.Replace("{{BillingAddress1}}", BillingAddress1);
            }
            else
            {
                data = data.Replace("{{BillingAddress1}}", "");
            }
            if (BillingAddress2 != null)
            {
                data = data.Replace("{{BillingAddress2}}", BillingAddress2);
            }
            else
            {
                data = data.Replace("{{BillingAddress2}}", "");
            }
            if (BillingAddress_CityStateZip != null)
            {
                data = data.Replace("{{BillingAddress_CityStateZip}}", BillingAddress_CityStateZip);
            }
            else
            {
                data = data.Replace("{{BillingAddress_CityStateZip}}", "");
            }
            if (PrimaryCellPhone != null)
            {
                data = data.Replace("{{PrimaryCellPhone}}", PrimaryCellPhone);
            }
            else
            {
                data = data.Replace("{{PrimaryCellPhone}}", "");
            }
            if (PrimaryHomePhone != null)
            {
                data = data.Replace("{{PrimaryHomePhone}}", PrimaryHomePhone);
            }
            else
            {
                data = data.Replace("{{PrimaryHomePhone}}", "");
            }
            if (PrimaryWorkPhone != null)
            {
                data = data.Replace("{{PrimaryWorkPhone}}", PrimaryWorkPhone);
            }
            else
            {
                data = data.Replace("{{PrimaryWorkPhone}}", "");
            }
            if (PrimaryEmail != null)
            {
                data = data.Replace("{{PrimaryEmail}}", PrimaryEmail);
            }
            else
            {
                data = data.Replace("{{PrimaryEmail}}", "");
            }
            if (PrimaryNotes != null)
            {
                data = data.Replace("{{PrimaryNotes}}", PrimaryNotes);
            }
            else
            {
                data = data.Replace("{{PrimaryNotes}}", "");
            }
            if (InstallAddress1 != null)
            {
                data = data.Replace("{{InstallAddress1}}", InstallAddress1);
            }
            else data = data.Replace("{{InstallAddress1}}", "");
            if (InstallAddress2 != null)
            {
                data = data.Replace("{{InstallAddress2}}", InstallAddress2);
            }
            else
            {
                data = data.Replace("{{InstallAddress2}}", "");
            }

            if (InstallAddres_CityStateZip != null)
            {
                data = data.Replace("{{InstallAddres_CityStateZip}}", InstallAddres_CityStateZip);
            }
            else
            {
                data = data.Replace("{{InstallAddres_CityStateZip}}", "");
            }
            string leadType = "";
            if (leadType != null)
            {
                data = data.Replace("{{leadType}}", leadType);
            }
            if (Secondarycontact != null)
            {
                data = data.Replace("{{Secondarycontact}}", Secondarycontact);
            }
            else
            {
                data = data.Replace("{{Secondarycontact}}", "");
            }
            if (SecondaryCellPhone != null)
            {
                data = data.Replace("{{SecondaryCellPhone}}", SecondaryCellPhone);
            }
            else
            {
                data = data.Replace("{{SecondaryCellPhone}}", "");
            }
            if (LeadSourceName != null)
            {
                data = data.Replace("{{LeadSourceName}}", LeadSourceName);
            }
            else
            {
                data = data.Replace("{{LeadSourceName}}", "");
            }

            if (BB_QQ_NewHome != null)
            {
                data = data.Replace("{{BB_QQ_NewHome}}", BB_QQ_NewHome);
                data = data.Replace("{{BB_QQ_ExistingHome}}", BB_QQ_ExistingHome);
            }

            if (BB_QQ_EstimatedClosingDate != null)
            {
                data = data.Replace("{{BB_QQ_EstimatedClosingDate}}", BB_QQ_EstimatedClosingDate);
            }
            data = data.Replace("{{BB_QQ_ProjectLeadTimePreference}}", BB_QQ_ProjectLeadTimePreference);

            data = data.Replace("{{BB_QQ_ProjectLeadTimePreference}}", BB_QQ_ProjectLeadTimePreference);
            data = data.Replace("{{ChkValpak}}", BB_QQ_ValPak);
            data = data.Replace("{{ChkYellowPages}}", BB_QQ_YellowPages);
            data = data.Replace("{{ChkRSVP}}", BB_QQ_RSVP);
            data = data.Replace("{{ChkVan}}", BB_QQ_Van);
            data = data.Replace("{{ChkInternet}}", BB_QQ_Internet);
            data = data.Replace("{{ChkSources1}}", BB_QQ_Sources1);
            data = data.Replace("{{ChkSources2}}", BB_QQ_Sources2);
            data = data.Replace("{{ChkSources3}}", BB_QQ_Sources3);
            data = data.Replace("{{ChkFacebookPaid}}", BB_QQ_FaceBookPaid);
            data = data.Replace("{{BB_QQ_Referel}}", BB_QQ_Referel);
            data = data.Replace("{{BB_QQ_Budget}}", BB_QQ_Budget);
            data = data.Replace("{{BB_QQ_AnyAppointment}}", BB_QQ_AnyAppointment);

            data = data.Replace("{{BB_QQ_ReceivedOtherQuotesYes}}", BB_QQ_ReceivedOtherQuotesYes);
            data = data.Replace("{{BB_QQ_ReceivedOtherQuotesNo}}", BB_QQ_ReceivedOtherQuotesNo);
            data = data.Replace("{{BB_QQ_ReceivedOtherQuotesSpecific}}", BB_QQ_ReceivedOtherQuotesSpecific);

            data = data.Replace("{{BB_PN_HowManyWindows}}", BB_PN_HowManyWindows);
            data = data.Replace("{{ChkLiving}}", BB_PN_Room_Living);
            data = data.Replace("{{ChkDining}}", BB_PN_Room_Dining);
            data = data.Replace("{{ChkFamily}}", BB_PN_Room_Family);
            data = data.Replace("{{ChkBed}}", BB_PN_Room_BedRooms);
            data = data.Replace("{{ChkKitchen}}", BB_PN_Room_Kitchen);
            data = data.Replace("{{ChkBath}}", BB_PN_Room_Bath);
            data = data.Replace("{{ChkOther}}", BB_PN_Room_Other);
            data = data.Replace("{{BB_PN_Room_OtherNote}}", BB_PN_Room_OtherNote);

            data = data.Replace("{{ChkWood}}", BB_PN_treatment_Wood);
            data = data.Replace("{{ChkFauxWood}}", BB_PN_treatment_FauxWood);
            data = data.Replace("{{ChkCellular}}", BB_PN_treatment_Cellular);
            data = data.Replace("{{ChkShades}}", BB_PN_treatment_Shades);
            data = data.Replace("{{ChkRollarShades}}", BB_PN_treatment_RollerShades);
            data = data.Replace("{{ChkDraperies}}", BB_PN_treatment_Draperies);
            data = data.Replace("{{ChkRomans}}", BB_PN_treatment_Romans);
            data = data.Replace("{{ChkVerticals}}", BB_PN_treatment_Verticals);
            data = data.Replace("{{ChkShutters}}", BB_PN_treatment_Shutters);
            data = data.Replace("{{ChkWovenWoods}}", BB_PN_treatment_WovenWoods);
            data = data.Replace("{{ChkSolarShades}}", BB_PN_treatment_SolarShades);

            data = data.Replace("{{OptOpetational}}", BB_PN_Drapery_Operational);
            data = data.Replace("{{OptDecorative}}", BB_PN_Drapery_Decorative);

            data = data.Replace("{{BB_PN_FrabricPreferences}}", BB_PN_FrabricPreferences);
            data = data.Replace("{{BB_PN_SolidSOrPrints}}", BB_PN_SolidSOrPrints);
            data = data.Replace("{{BB_PN_PreferredStyle}}", BB_PN_PreferredStyle);

            //Sales Appointment Information
            string SalesAgentName = "";
            string BookedBy = "";
            string AppointmentDate = "";
            string AppointmentTime = "";

            if (RecentAppointment != null)
            {
                if (RecentAppointment.AssignedName != null && RecentAppointment.AssignedName != "")
                {
                    SalesAgentName = RecentAppointment.AssignedName;
                }
                if (RecentAppointment.OrganizerName != null && RecentAppointment.OrganizerName != "")
                {
                    BookedBy = RecentAppointment.OrganizerName;
                }

                if (RecentAppointment.StartDate != null && RecentAppointment.StartDate.ToString() != "")
                {
                    //AppointmentDate = RecentAppointment.StartDate.ToString("dd/mm/yyyy");
                    //AppointmentTime = RecentAppointment.StartDate.ToString("hh:mm:ss");
                    AppointmentDate = GetFormatDate(Convert.ToString(RecentAppointment.StartDate));
                    AppointmentTime = GetFormatTime(Convert.ToString(RecentAppointment.StartDate));
                }
               
            }
            data = data.Replace("{{SalesAgentName}}", SalesAgentName);
            data = data.Replace("{{BookedBy}}", BookedBy);
            data = data.Replace("{{ApptDate}}", AppointmentDate);
            data = data.Replace("{{ApptTime}}", AppointmentTime);

            string accountnotes = printAccountNotes(lead.AccountId);
            data = data.Replace("{{accountnotes}}", accountnotes.ToString());

            return data;
        }
        public static string OpportunitySheet_TailorLiving(int id, HFC.CRM.Data.Calendar RecentAppointment)
        {
            string LeadData = string.Empty;
            string Path = string.Empty;

            Path = string.Format("/Templates/Base/Brand/TL/LeadSheet.html");

            string srcFilePath = HostingEnvironment.MapPath(Path);

            AccountTP lead = accountsMgr.Get(id);

            string data = System.IO.File.ReadAllText(srcFilePath);

            string PrimaryContact = lead.PrimCustomer.FullName;
            string CompanyName = lead.PrimCustomer.CompanyName;
            AddressTP BillingAddress = null;
            if (lead.Addresses.Count > 0)
            {
                BillingAddress = lead.Addresses.First();

            }
            string BillingAddress1 = BillingAddress.Address1;
            string BillingAddress2 = BillingAddress.Address2;
            string BillingAddress_CityStateZip = BillingAddress.City + ',' + BillingAddress.State + ',' + BillingAddress.ZipCode;

            string PrimaryCellPhone = lead.PrimCustomer.CellPhone;
            string PrimaryHomePhone = lead.PrimCustomer.HomePhone;
            string PrimaryWorkPhone = lead.PrimCustomer.WorkPhone;
            string PrimaryEmail = lead.PrimCustomer.PrimaryEmail;

            string PrimaryNotes = "";
            AddressTP InstallAddress = null;

            string InstallAddress1 = "";
            string InstallAddress2 = "";
            string InstallAddres_CityStateZip = "";

            if (lead.Addresses.Count > 1)
            {
                //Select secondary address
                InstallAddress = lead.Addresses.First();
                InstallAddress1 = InstallAddress.Address1;
                InstallAddress2 = InstallAddress.Address2;
                InstallAddres_CityStateZip = InstallAddress.City + ',' + InstallAddress.State + ',' + InstallAddress.ZipCode;
            }

            InstallAddress = null;
            if (lead.Addresses.Count > 1)
            {
                //Select secondary address
                InstallAddress = lead.Addresses.First();

            }
            CustomerTP SecCustomer;
            string Secondarycontact = "";
            string SecondaryCellPhone = "";
            if (lead.SecCustomer.Count > 1)
            {
                //Select secondary address
                SecCustomer = lead.SecCustomer.First();
                Secondarycontact = SecCustomer.FullName;
                SecondaryCellPhone = SecCustomer.CellPhone;
            }

            SourceList leadsources = null;

            string LeadSourceName = "";
            string LeadChannelName = "";
            if (lead.SourcesList.Count > 0)
            {
                leadsources = lead.SourcesList.First();
                LeadSourceName = leadsources.name;
                LeadChannelName = leadsources.ChannelName;
            }
            //BB - Qualification Questions
            string TL_QQ_NewHome = ""; string TL_QQ_ExistingHome = ""; string TL_QQ_EstimatedClosingDate = ""; string TL_QQ_ProjectLeadTimePreference = "";
            string TL_QQ_ValPak = ""; string TL_QQ_YellowPages = ""; string TL_QQ_RSVP = ""; string TL_QQ_Van = ""; string TL_QQ_Internet = "";
            string TL_QQ_Sources1 = ""; string TL_QQ_Sources2 = ""; string TL_QQ_Sources3 = ""; string TL_QQ_FaceBookPaid = ""; string TL_QQ_Referel = "";
            string TL_QQ_Budget = "";
            string TL_QQ_AnyAppointment = ""; string TL_QQ_ReceivedOtherQuotesYes = ""; string TL_QQ_ReceivedOtherQuotesNo = ""; string TL_QQ_ReceivedOtherQuotesSpecific = "";

            //BB - Project Needs
            string TL_GarageCabinetry = "";
            string TL_GarageFlooring = ""; string TL_HomeOffice = ""; string TL_Closet = ""; string TL_Laundry = "";
            string TL_Pantry = ""; string TL_Other = "";
            string TL_ProjectDetails = "";
            string TL_PastGarageCabinetry = ""; string TL_PastHomeOffice = ""; string TL_PastCloset = ""; string TL_PastSpecifics = "";
            string TL_WorkStations = ""; string TL_Drawers = ""; string TL_Workbench = ""; string TL_Quantity = ""; string TL_NeedsQuantity = "";
            string TL_OtherNeedsFlooring = ""; string TL_OtherNeedsLaundry = ""; string TL_OtherNeedsSpecifics = ""; 
            string TL_TaskLightingYes = ""; string TL_TaskLightingNo = ""; string TL_TaskLightingSpecifics = "";
            string TL_Jewellery = ""; string TL_Filing = ""; string TL_OtherDrawersSpecifics = "";
            string TL_FinishPrefernceSpecifics="";
            string TL_PN_Notes;

            var leadquestionAns = lead.AccountQuestionAns;

            foreach (var leadqa in leadquestionAns)
            {
                #region QualificationQuestions
                if (leadqa.QuestionId == 1047 && leadqa.Answer != null)
                {
                    if (leadqa.Answer == "New Home?")
                    {
                        TL_QQ_NewHome = "Checked";
                        TL_QQ_ExistingHome = "";
                    }
                    else if (leadqa.Answer == "Existing Home?")
                    {
                        TL_QQ_NewHome = "";
                        TL_QQ_ExistingHome = "Checked";
                    }
                }
                if (leadqa.QuestionId == 1049 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_EstimatedClosingDate = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1050 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_ProjectLeadTimePreference = leadqa.Answer;
                }
                #endregion QualificationQuestions
                #region Tailored Living
                //How did u hear about BB
                if (leadqa.QuestionId == 1051 && (leadqa.Answer != null || leadqa.Answer != ""))
                {
                    TL_QQ_ValPak = "Checked";
                }
                if (leadqa.QuestionId == 1052 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_YellowPages = "Checked";
                }
                if (leadqa.QuestionId == 1053 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_RSVP = "Checked";
                }
                if (leadqa.QuestionId == 1054 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_Van = "Checked";
                }
                if (leadqa.QuestionId == 1055 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_Internet = "Checked";
                }
                if (leadqa.QuestionId == 1056 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_Sources1 = "Checked";
                }
                if (leadqa.QuestionId == 1057 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_Sources2 = "Checked";
                }
                if (leadqa.QuestionId == 1058 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_Sources3 = "Checked";
                }
                if (leadqa.QuestionId == 1059 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_FaceBookPaid = "Checked";
                }
               
                if (leadqa.QuestionId == 1060 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_Referel = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1061 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_Budget = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1062 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_AnyAppointment = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1063 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    if (leadqa.Answer == "Yes")
                    {
                        TL_QQ_ReceivedOtherQuotesYes = "Checked";
                        TL_QQ_ReceivedOtherQuotesNo = "";
                    }
                    else if (leadqa.Answer == "No")
                    {
                        TL_QQ_ReceivedOtherQuotesYes = "";
                        TL_QQ_ReceivedOtherQuotesNo = "Checked";
                    }
                }
                if (leadqa.QuestionId == 1065 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_ReceivedOtherQuotesSpecific = leadqa.Answer;
                }
                #endregion Tailored Living
                #region DetailsInformation
                if (leadqa.QuestionId == 1068 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_GarageCabinetry = "Checked";
                }
                if (leadqa.QuestionId == 1069 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_GarageFlooring = "Checked";
                }
                if (leadqa.QuestionId == 1070 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_HomeOffice = "Checked";
                }
                if (leadqa.QuestionId == 1071 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_Closet = "Checked";
                }
                if (leadqa.QuestionId == 1072 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_Laundry = "Checked";
                }
                if (leadqa.QuestionId == 1073 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_Pantry = "Checked";
                }
                if (leadqa.QuestionId == 1074 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_Other = leadqa.Answer;
                }

                if (leadqa.QuestionId == 1076 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_ProjectDetails = leadqa.Answer;
                }

                //System Installed in the past
                if (leadqa.QuestionId == 1125 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_PastGarageCabinetry = "Checked";
                }

                if (leadqa.QuestionId == 1126 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_PastHomeOffice = "Checked";
                }
                if (leadqa.QuestionId == 1127 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_PastCloset = "Checked";
                }

                //if (leadqa.QuestionId == 11 && (leadqa.Answer != null && leadqa.Answer != ""))
                //{
                //    TL_PastSpecifics = leadqa.Answer;
                //}

                if (leadqa.QuestionId == 1128 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_WorkStations = "Checked";
                }
                if (leadqa.QuestionId == 1129 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_Drawers = "Checked";
                }
                if (leadqa.QuestionId == 1130 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_Workbench = "Checked";
                }
                if (leadqa.QuestionId == 1131 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_Quantity = "Checked";
                }
                if (leadqa.QuestionId == 1131 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_NeedsQuantity = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1079 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_OtherNeedsFlooring = "Checked";
                }
                if (leadqa.QuestionId == 1080 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_OtherNeedsLaundry = "Checked";
                }
                if (leadqa.QuestionId == 1081 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_OtherNeedsSpecifics = leadqa.Answer;
                }

                if (leadqa.QuestionId == 1083 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    if (leadqa.Answer == "Yes")
                    {
                        TL_TaskLightingYes = "Checked";
                        TL_TaskLightingNo = "";
                    }
                    else if (leadqa.Answer == "No")
                    {
                        TL_TaskLightingYes = "";
                        TL_TaskLightingNo = "Checked";
                    }
                }
                if (leadqa.QuestionId == 1085 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_TaskLightingSpecifics = leadqa.Answer;
                }

                if (leadqa.QuestionId == 1086 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_Jewellery  = "Checked";
                }
                if (leadqa.QuestionId == 1087 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_Filing = "Checked";
                }
                if (leadqa.QuestionId == 1088 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_OtherDrawersSpecifics = leadqa.Answer;
                }


                if (leadqa.QuestionId == 1089 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_FinishPrefernceSpecifics = leadqa.Answer;
                }
                
                #endregion  DetailsInformation
            }

            //Read lead html and append the data
            if (PrimaryContact != null)
            {
                //{{Contactfullname}}
                data = data.Replace("{{Contactfullname}}", PrimaryContact);
            }
            else
            {
                data = data.Replace("{{Contactfullname}}", "");
            }
            if (CompanyName != null)
            {
                data = data.Replace("{{CompanyName}}", CompanyName);
            }
            else
            {
                data = data.Replace("{{CompanyName}}", "");
            }
            if (BillingAddress1 != null)
            {
                data = data.Replace("{{BillingAddress1}}", BillingAddress1);
            }
            else
            {
                data = data.Replace("{{BillingAddress1}}", "");
            }
            if (BillingAddress2 != null)
            {
                data = data.Replace("{{BillingAddress2}}", BillingAddress2);
            }
            else
            {
                data = data.Replace("{{BillingAddress2}}", "");
            }
            if (BillingAddress_CityStateZip != null)
            {
                data = data.Replace("{{BillingAddress_CityStateZip}}", BillingAddress_CityStateZip);
            }
            else
            {
                data = data.Replace("{{BillingAddress_CityStateZip}}", "");
            }
            if (PrimaryCellPhone != null)
            {
                data = data.Replace("{{PrimaryCellPhone}}", PrimaryCellPhone);
            }
            else
            {
                data = data.Replace("{{PrimaryCellPhone}}", "");
            }
            if (PrimaryHomePhone != null)
            {
                data = data.Replace("{{PrimaryHomePhone}}", PrimaryHomePhone);
            }
            else
            {
                data = data.Replace("{{PrimaryHomePhone}}", "");
            }
            if (PrimaryWorkPhone != null)
            {
                data = data.Replace("{{PrimaryWorkPhone}}", PrimaryWorkPhone);
            }
            else
            {
                data = data.Replace("{{PrimaryWorkPhone}}", "");
            }
            if (PrimaryEmail != null)
            {
                data = data.Replace("{{PrimaryEmail}}", PrimaryEmail);
            }
            else
            {
                data = data.Replace("{{PrimaryEmail}}", "");
            }
            if (PrimaryNotes != null)
            {
                data = data.Replace("{{PrimaryNotes}}", PrimaryNotes);
            }
            else
            {
                data = data.Replace("{{PrimaryNotes}}", "");
            }
            if (InstallAddress1 != null)
            {
                data = data.Replace("{{InstallAddress1}}", InstallAddress1);
            }
            else data = data.Replace("{{InstallAddress1}}", "");
            if (InstallAddress2 != null)
            {
                data = data.Replace("{{InstallAddress2}}", InstallAddress2);
            }
            else
            {
                data = data.Replace("{{InstallAddress2}}", "");
            }

            if (InstallAddres_CityStateZip != null)
            {
                data = data.Replace("{{InstallAddres_CityStateZip}}", InstallAddres_CityStateZip);
            }
            else
            {
                data = data.Replace("{{InstallAddres_CityStateZip}}", "");
            }
            string leadType = "";
            if (leadType != null)
            {
                data = data.Replace("{{leadType}}", leadType);
            }
            if (Secondarycontact != null)
            {
                data = data.Replace("{{Secondarycontact}}", Secondarycontact);
            }
            else
            {
                data = data.Replace("{{Secondarycontact}}", "");
            }
            if (SecondaryCellPhone != null)
            {
                data = data.Replace("{{SecondaryCellPhone}}", SecondaryCellPhone);
            }
            else
            {
                data = data.Replace("{{SecondaryCellPhone}}", "");
            }
            if (LeadSourceName != null)
            {
                data = data.Replace("{{LeadSourceName}}", LeadSourceName);
            }
            else
            {
                data = data.Replace("{{LeadSourceName}}", "");
            }

            if (TL_QQ_NewHome != null)
            {
                data = data.Replace("{{TL_QQ_NewHome}}", TL_QQ_NewHome);
                data = data.Replace("{{TL_QQ_ExistingHome}}", TL_QQ_ExistingHome);
            }

            if (TL_QQ_EstimatedClosingDate != null)
            {
                data = data.Replace("{{TL_QQ_EstimatedClosingDate}}", TL_QQ_EstimatedClosingDate);
            }
            data = data.Replace("{{TL_QQ_ProjectLeadTimePreference}}", TL_QQ_ProjectLeadTimePreference);

            data = data.Replace("{{TL_QQ_ProjectLeadTimePreference}}", TL_QQ_ProjectLeadTimePreference);
            data = data.Replace("{{ChkValpak}}", TL_QQ_ValPak);
            data = data.Replace("{{ChkYellowPages}}", TL_QQ_YellowPages);
            data = data.Replace("{{ChkRSVP}}", TL_QQ_RSVP);
            data = data.Replace("{{ChkVan}}", TL_QQ_Van);
            data = data.Replace("{{ChkInternet}}", TL_QQ_Internet);
            data = data.Replace("{{ChkSources1}}", TL_QQ_Sources1);
            data = data.Replace("{{ChkSources2}}", TL_QQ_Sources2);
            data = data.Replace("{{ChkSources3}}", TL_QQ_Sources3);

            data = data.Replace("{{TL_QQ_Referel}}", TL_QQ_Referel);
            data = data.Replace("{{TL_QQ_Budget}}", TL_QQ_Budget);
            data = data.Replace("{{TL_QQ_AnyAppointment}}", TL_QQ_AnyAppointment);

            data = data.Replace("{{TL_QQ_ReceivedOtherQuotesYes}}", TL_QQ_ReceivedOtherQuotesYes);
            data = data.Replace("{{TL_QQ_ReceivedOtherQuotesNo}}", TL_QQ_ReceivedOtherQuotesNo);
            data = data.Replace("{{TL_QQ_ReceivedOtherQuotesSpecific}}", TL_QQ_ReceivedOtherQuotesSpecific);

            data = data.Replace("{{ChkGarageCabinetry}}", TL_GarageCabinetry);
            data = data.Replace("{{ChkGarageFlooring}}", TL_GarageFlooring);
            data = data.Replace("{{ChkHomeOffice}}", TL_HomeOffice);
            data = data.Replace("{{ChkCloset}}", TL_Closet);
            data = data.Replace("{{ChkLaundry}}", TL_Laundry);
            data = data.Replace("{{ChkPantry}}", TL_Pantry);
            data = data.Replace("{{ChkOther}}", TL_Other);

            //data = data.Replace("{{ChkOther}}", TL_Other); textbox

            //data = data.Replace("{{ChkOther}}", TL_ProjectDetails); Project details

            data = data.Replace("{{ChkPastGarageCabinetry}}", TL_PastGarageCabinetry);
            data = data.Replace("{{ChkPastHomeOffice}}", TL_PastHomeOffice);
            data = data.Replace("{{ChkPastCloset}}", TL_PastCloset);
            data = data.Replace("{{PastSpecifics}}", TL_PastSpecifics);


            data = data.Replace("{{ChkWorkStations}}", TL_WorkStations);
            data = data.Replace("{{ChkWorkbench}}", TL_Workbench);
            data = data.Replace("{{ChkDrawers}}", TL_Drawers);
            data = data.Replace("{{NeedsQuantity}}", TL_NeedsQuantity);


            data = data.Replace("{{ChkOtherNeedsFlooring}}", TL_OtherNeedsFlooring);
            data = data.Replace("{{ChkOtherNeedsLaundry}}", TL_OtherNeedsLaundry);
            data = data.Replace("{{OtherNeedsSpecifics}}", TL_OtherNeedsSpecifics);

           

            data = data.Replace("{{ChkTaskLightingYes}}", TL_TaskLightingYes);
            data = data.Replace("{{ChkTaskLightingNo}}", TL_TaskLightingNo);
            data = data.Replace("{{TaskLightingSpecifics}}", TL_TaskLightingSpecifics);

            data = data.Replace("{{ChkJewellery}}", TL_Jewellery);
            data = data.Replace("{{ChkFiling}}", TL_Filing);
            data = data.Replace("{{OtherDrawerSpecifics}}", TL_OtherDrawersSpecifics);

            data = data.Replace("{{FinishPreferenceSpecifics}}", TL_FinishPrefernceSpecifics);

            //Sales Appointment Information


            string SalesAgentName = "";
            string BookedBy = "";
            string AppointmentDate = "";
            string AppointmentTime = "";

            if (RecentAppointment != null)
            {

                if (RecentAppointment.AssignedName != null && RecentAppointment.AssignedName != "")
                {
                    SalesAgentName = RecentAppointment.AssignedName;
                }
                if (RecentAppointment.OrganizerName != null && RecentAppointment.OrganizerName != "")
                {
                    BookedBy = RecentAppointment.OrganizerName;
                }

                if (RecentAppointment.StartDate != null && RecentAppointment.StartDate.ToString() != "")
                {
                    // AppointmentDate = RecentAppointment.StartDate.ToString();
                    //AppointmentDate = RecentAppointment.StartDate.ToString("dd/mm/yyyy");
                    //AppointmentTime = RecentAppointment.StartDate.ToString("hh:mm:ss");
                    AppointmentDate = GetFormatDate(Convert.ToString(RecentAppointment.StartDate));
                    AppointmentTime = GetFormatTime(Convert.ToString(RecentAppointment.StartDate));
                }
            }

            data = data.Replace("{{SalesAgentName}}", SalesAgentName);
            data = data.Replace("{{BookedBy}}", BookedBy);
            data = data.Replace("{{ApptDate}}", AppointmentDate);
            data = data.Replace("{{ApptTime}}", AppointmentTime);

            string accountnotes = printAccountNotes(lead.AccountId);
            data = data.Replace("{{accountnotes}}", accountnotes.ToString());
            return data;
        }
        public static string OpportunitySheet_ConcreteCraft(int id, HFC.CRM.Data.Calendar RecentAppointment)
        {
            string LeadData = string.Empty;
            string Path = string.Empty;

            Path = string.Format("/Templates/Base/Brand/CC/LeadSheet.html");

            string srcFilePath = HostingEnvironment.MapPath(Path);

            AccountTP lead = accountsMgr.Get(id);

            string data = System.IO.File.ReadAllText(srcFilePath);

            string PrimaryContact = lead.PrimCustomer.FullName;
            string CompanyName = lead.PrimCustomer.CompanyName;
            AddressTP BillingAddress = null;
            if (lead.Addresses.Count > 0)
            {
                BillingAddress = lead.Addresses.First();

            }
            string BillingAddress1 = BillingAddress.Address1;
            string BillingAddress2 = BillingAddress.Address2;
            string BillingAddress_CityStateZip = BillingAddress.City + ',' + BillingAddress.State + ',' + BillingAddress.ZipCode;

            string PrimaryCellPhone = lead.PrimCustomer.CellPhone;
            string PrimaryHomePhone = lead.PrimCustomer.HomePhone;
            string PrimaryWorkPhone = lead.PrimCustomer.WorkPhone;
            string PrimaryEmail = lead.PrimCustomer.PrimaryEmail;

            string PrimaryNotes = "";
            AddressTP InstallAddress = null;

            string InstallAddress1 = "";
            string InstallAddress2 = "";
            string InstallAddres_CityStateZip = "";

            if (lead.Addresses.Count > 1)
            {
                //Select secondary address
                InstallAddress = lead.Addresses.First();
                InstallAddress1 = InstallAddress.Address1;
                InstallAddress2 = InstallAddress.Address2;
                InstallAddres_CityStateZip = InstallAddress.City + ',' + InstallAddress.State + ',' + InstallAddress.ZipCode;
            }

            InstallAddress = null;
            if (lead.Addresses.Count > 1)
            {
                //Select secondary address
                InstallAddress = lead.Addresses.First();

            }
            CustomerTP SecCustomer;
            string Secondarycontact = "";
            string SecondaryCellPhone = "";
            if (lead.SecCustomer.Count > 1)
            {
                //Select secondary address
                SecCustomer = lead.SecCustomer.First();
                Secondarycontact = SecCustomer.FullName;
                SecondaryCellPhone = SecCustomer.CellPhone;
            }

            SourceList leadsources = null;

            string LeadSourceName = "";
            string LeadChannelName = "";
            if (lead.SourcesList.Count > 0)
            {
                leadsources = lead.SourcesList.First();
                LeadSourceName = leadsources.name;
                LeadChannelName = leadsources.ChannelName;
            }
            //BB - Qualification Questions
            string CC_QQ_NewHome = ""; string CC_QQ_ExistingHome = ""; string CC_QQ_EstimatedClosingDate = ""; string CC_QQ_ProjectLeadTimePreference = "";
            string CC_QQ_ValPak = ""; string CC_QQ_YellowPages = ""; string CC_QQ_RSVP = ""; string CC_QQ_Van = ""; string CC_QQ_Internet = "";
            string CC_QQ_Sources1 = ""; string CC_QQ_Sources2 = ""; string CC_QQ_Sources3 = ""; string CC_QQ_FaceBookPaid = ""; string CC_QQ_Referel = "";
            string CC_QQ_Budget = "";
            string CC_QQ_AnyAppointment = ""; string CC_QQ_ReceivedOtherQuotesYes = ""; string CC_QQ_ReceivedOtherQuotesNo = ""; string CC_QQ_ReceivedOtherQuotesSpecific = "";
            //BB - Project Needs
            string CC_StampedConcrete = "";string CC_ResurfaceConcrete = ""; string CC_StainedConcrete = "";  string CC_ColorRestoreSystem = "";
            string CC_ColorSealSystem = ""; string CC_PlainConcrete = ""; string CC_CleanAndReseal = ""; string CC_ColoredConcrete = ""; 
            string CC_ProjectDetails = "";
            string CC_ExistingSlabYes = ""; string CC_ExistingSlabNo = ""; string CC_AprroximateSizeOrShape = ""; 
            string CC_OntheSurface = ""; 
            string CC_PN_Notes;

            var leadquestionAns = lead.AccountQuestionAns;

            foreach (var leadqa in leadquestionAns)
            {
                #region QualificationQuestions
                if (leadqa.QuestionId == 1090 && leadqa.Answer != null)
                {
                    if (leadqa.Answer == "New Home?")
                    {
                        CC_QQ_NewHome = "Checked";
                        CC_QQ_ExistingHome = "";
                    }
                    else if (leadqa.Answer == "Existing Home?")
                    {
                        CC_QQ_NewHome = "";
                        CC_QQ_ExistingHome = "Checked";
                    }
                }
                if (leadqa.QuestionId == 1092 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_EstimatedClosingDate = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1093 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_ProjectLeadTimePreference = leadqa.Answer;
                }
                #endregion QualificationQuestions
                #region Tailored Living
                //How did u hear about BB
                if (leadqa.QuestionId == 1094 && (leadqa.Answer != null || leadqa.Answer != ""))
                {
                    CC_QQ_ValPak = "Checked";
                }
                if (leadqa.QuestionId == 1095 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_YellowPages = "Checked";
                }
                if (leadqa.QuestionId == 1096 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_RSVP = "Checked";
                }
                if (leadqa.QuestionId == 1097 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_Van = "Checked";
                }
                if (leadqa.QuestionId == 1098 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_Internet = "Checked";
                }
                if (leadqa.QuestionId == 1099 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_Sources1 = "Checked";
                }
                if (leadqa.QuestionId == 1100 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_Sources2 = "Checked";
                }
                if (leadqa.QuestionId == 1101 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_Sources3 = "Checked";
                }
                if (leadqa.QuestionId == 1102 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_FaceBookPaid = "Checked";
                }

                if (leadqa.QuestionId == 1103 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_Referel = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1104 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_Budget = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1105 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_AnyAppointment = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1106 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    if (leadqa.Answer == "Yes")
                    {
                        CC_QQ_ReceivedOtherQuotesYes = "Checked";
                        CC_QQ_ReceivedOtherQuotesNo = "";
                    }
                    else if (leadqa.Answer == "No")
                    {
                        CC_QQ_ReceivedOtherQuotesYes = "";
                        CC_QQ_ReceivedOtherQuotesNo = "Checked";
                    }
                }

                if (leadqa.QuestionId == 1108 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_ReceivedOtherQuotesSpecific = leadqa.Answer;
                }
                //Other Comments?
                #endregion Tailored Living
                #region Project Needs 
                if (leadqa.QuestionId == 1111 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_StampedConcrete = "Checked";
                }
                if (leadqa.QuestionId == 1112 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_ResurfaceConcrete = "Checked";
                }
                if (leadqa.QuestionId == 1113 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_StainedConcrete = "Checked";
                }
                if (leadqa.QuestionId == 1114 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_ColorRestoreSystem = "Checked";
                }
                if (leadqa.QuestionId == 1115 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_ColorSealSystem = "Checked";
                }
                if (leadqa.QuestionId == 1116 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_PlainConcrete = "Checked";
                }
                if (leadqa.QuestionId == 1117 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_CleanAndReseal = "Checked";
                }
                if (leadqa.QuestionId == 1118 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_ColoredConcrete = "Checked";
                }

                if (leadqa.QuestionId == 1119 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_ProjectDetails = leadqa.Answer;
                }

               
                if (leadqa.QuestionId == 1121 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    if (leadqa.Answer == "Yes")
                    {
                        CC_ExistingSlabYes = "Checked";
                        CC_ExistingSlabNo = "";
                    }
                    else if (leadqa.Answer == "No")
                    {
                        CC_ExistingSlabYes = "";
                        CC_ExistingSlabNo = "Checked";
                    }

                }
                if (leadqa.QuestionId == 1123 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_AprroximateSizeOrShape = leadqa.Answer;
                }

                if (leadqa.QuestionId == 1124 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_OntheSurface = leadqa.Answer;

                }

                #endregion  Project Needs 
            }

            //Read lead html and append the data
            if (PrimaryContact != null)
            {
                //{{Contactfullname}}
                data = data.Replace("{{Contactfullname}}", PrimaryContact);
            }
            else
            {
                data = data.Replace("{{Contactfullname}}", "");
            }
            if (CompanyName != null)
            {
                data = data.Replace("{{CompanyName}}", CompanyName);
            }
            else
            {
                data = data.Replace("{{CompanyName}}", "");
            }
            if (BillingAddress1 != null)
            {
                data = data.Replace("{{BillingAddress1}}", BillingAddress1);
            }
            else
            {
                data = data.Replace("{{BillingAddress1}}", "");
            }
            if (BillingAddress2 != null)
            {
                data = data.Replace("{{BillingAddress2}}", BillingAddress2);
            }
            else
            {
                data = data.Replace("{{BillingAddress2}}", "");
            }
            if (BillingAddress_CityStateZip != null)
            {
                data = data.Replace("{{BillingAddress_CityStateZip}}", BillingAddress_CityStateZip);
            }
            else
            {
                data = data.Replace("{{BillingAddress_CityStateZip}}", "");
            }
            if (PrimaryCellPhone != null)
            {
                data = data.Replace("{{PrimaryCellPhone}}", PrimaryCellPhone);
            }
            else
            {
                data = data.Replace("{{PrimaryCellPhone}}", "");
            }
            if (PrimaryHomePhone != null)
            {
                data = data.Replace("{{PrimaryHomePhone}}", PrimaryHomePhone);
            }
            else
            {
                data = data.Replace("{{PrimaryHomePhone}}", "");
            }
            if (PrimaryWorkPhone != null)
            {
                data = data.Replace("{{PrimaryWorkPhone}}", PrimaryWorkPhone);
            }
            else
            {
                data = data.Replace("{{PrimaryWorkPhone}}", "");
            }
            if (PrimaryEmail != null)
            {
                data = data.Replace("{{PrimaryEmail}}", PrimaryEmail);
            }
            else
            {
                data = data.Replace("{{PrimaryEmail}}", "");
            }
            if (PrimaryNotes != null)
            {
                data = data.Replace("{{PrimaryNotes}}", PrimaryNotes);
            }
            else
            {
                data = data.Replace("{{PrimaryNotes}}", "");
            }
            if (InstallAddress1 != null)
            {
                data = data.Replace("{{InstallAddress1}}", InstallAddress1);
            }
            else data = data.Replace("{{InstallAddress1}}", "");
            if (InstallAddress2 != null)
            {
                data = data.Replace("{{InstallAddress2}}", InstallAddress2);
            }
            else
            {
                data = data.Replace("{{InstallAddress2}}", "");
            }

            if (InstallAddres_CityStateZip != null)
            {
                data = data.Replace("{{InstallAddres_CityStateZip}}", InstallAddres_CityStateZip);
            }
            else
            {
                data = data.Replace("{{InstallAddres_CityStateZip}}", "");
            }
            string leadType = "";
            if (leadType != null)
            {
                data = data.Replace("{{leadType}}", leadType);
            }
            if (Secondarycontact != null)
            {
                data = data.Replace("{{Secondarycontact}}", Secondarycontact);
            }
            else
            {
                data = data.Replace("{{Secondarycontact}}", "");
            }
            if (SecondaryCellPhone != null)
            {
                data = data.Replace("{{SecondaryCellPhone}}", SecondaryCellPhone);
            }
            else
            {
                data = data.Replace("{{SecondaryCellPhone}}", "");
            }
            if (LeadSourceName != null)
            {
                data = data.Replace("{{LeadSourceName}}", LeadSourceName);
            }
            else
            {
                data = data.Replace("{{LeadSourceName}}", "");
            }
            if (CC_QQ_NewHome != null)
            {
                data = data.Replace("{{CC_QQ_NewHome}}", CC_QQ_NewHome);
                data = data.Replace("{{CC_QQ_ExistingHome}}", CC_QQ_ExistingHome);
            }

            if (CC_QQ_EstimatedClosingDate != null)
            {
                data = data.Replace("{{CC_QQ_EstimatedClosingDate}}", CC_QQ_EstimatedClosingDate);
            }
            data = data.Replace("{{CC_QQ_ProjectLeadTimePreference}}", CC_QQ_ProjectLeadTimePreference);

            data = data.Replace("{{CC_QQ_ProjectLeadTimePreference}}", CC_QQ_ProjectLeadTimePreference);
            data = data.Replace("{{ChkValpak}}", CC_QQ_ValPak);
            data = data.Replace("{{ChkYellowPages}}", CC_QQ_YellowPages);
            data = data.Replace("{{ChkRSVP}}", CC_QQ_RSVP);
            data = data.Replace("{{ChkVan}}", CC_QQ_Van);
            data = data.Replace("{{ChkInternet}}", CC_QQ_Internet);
            data = data.Replace("{{ChkSources1}}", CC_QQ_Sources1);
            data = data.Replace("{{ChkSources2}}", CC_QQ_Sources2);
            data = data.Replace("{{ChkSources3}}", CC_QQ_Sources3);
            data = data.Replace("{{ChkFacebookPaid}}", CC_QQ_FaceBookPaid);

            data = data.Replace("{{CC_QQ_Referel}}", CC_QQ_Referel);
            data = data.Replace("{{CC_QQ_Budget}}", CC_QQ_Budget);
            data = data.Replace("{{CC_QQ_AnyAppointment}}", CC_QQ_AnyAppointment);

            data = data.Replace("{{CC_QQ_ReceivedOtherQuotesYes}}", CC_QQ_ReceivedOtherQuotesYes);
            data = data.Replace("{{CC_QQ_ReceivedOtherQuotesNo}}", CC_QQ_ReceivedOtherQuotesNo);
            data = data.Replace("{{CC_QQ_ReceivedOtherQuotesSpecific}}", CC_QQ_ReceivedOtherQuotesSpecific);

            data = data.Replace("{{ChkStampedConcrete}}", CC_StampedConcrete);
            data = data.Replace("{{ChkResurfaceConcrete}}", CC_ResurfaceConcrete);
            data = data.Replace("{{ChkStainedConcrete}}", CC_StainedConcrete);
            data = data.Replace("{{ChkColorRestoreSystem}}", CC_ColorRestoreSystem);

            data = data.Replace("{{ChkColorSealSystem}}", CC_ColorSealSystem);
            data = data.Replace("{{ChkPlainConcrete}}", CC_PlainConcrete);
            data = data.Replace("{{ChkCleanAndReseal}}", CC_CleanAndReseal);
            data = data.Replace("{{ChkColoredConcrete}}", CC_ColoredConcrete);

            data = data.Replace("{{ProjectDetails}}", CC_ProjectDetails);

            data = data.Replace("{{CC_QQ_ExistingSlabYes}}", CC_ExistingSlabYes);
            data = data.Replace("{{CC_QQ_ExistingSlabNo}}", CC_ExistingSlabNo);
            data = data.Replace("{{CC_QQ_AprroximateSizeOrShape}}", CC_AprroximateSizeOrShape);

            data = data.Replace("{{CC_QQ_OntheSurface}}", CC_OntheSurface);
            
            //Sales Appointment Information
            string SalesAgentName = "";
            string BookedBy = "";
            string AppointmentDate = "";
            string AppointmentTime = "";


            if (RecentAppointment != null)
            {
                if (RecentAppointment.AssignedName != null && RecentAppointment.AssignedName != "")
                {
                    SalesAgentName = RecentAppointment.AssignedName;
                }
                if (RecentAppointment.OrganizerName != null && RecentAppointment.OrganizerName != "")
                {
                    BookedBy = RecentAppointment.OrganizerName;
                }

                if (RecentAppointment.StartDate != null && RecentAppointment.StartDate.ToString() != "")
                {
                   
                    AppointmentDate = GetFormatDate(Convert.ToString(RecentAppointment.StartDate));
                    AppointmentTime = GetFormatTime(Convert.ToString(RecentAppointment.StartDate));

                }
            }

            data = data.Replace("{{SalesAgentName}}", SalesAgentName);
            data = data.Replace("{{BookedBy}}", BookedBy);
            data = data.Replace("{{ApptDate}}", AppointmentDate);
            data = data.Replace("{{ApptTime}}", AppointmentTime);

            string accountnotes = printAccountNotes(lead.AccountId);
            data = data.Replace("{{accountnotes}}", accountnotes.ToString());

            return data;
        }

        public static string printAccountNotes(int id = 0)
        {
            //Append Lead AccountNotes - Internal and external
            var accountnotes = accountsMgr.PrintAccountNotes(id);
            StringBuilder accountNotesdata = new StringBuilder();
            if (accountnotes != null)
            {

                foreach (var items in accountnotes)
                {

                    accountNotesdata.Append("<tr>");
                    accountNotesdata.Append("<td>" + items.LeadId + "</td>");
                    accountNotesdata.Append("<td>" + items.Title + "</td>");
                    accountNotesdata.Append("<td>" + items.Category + "</td>");
                    accountNotesdata.Append("<td>" + items.Note + "</td>");
                    accountNotesdata.Append("</tr>");
                }
            }
            return accountNotesdata.ToString();
        }
        private static string GetFormatPhone(string TargetPhone)
        {
            string phoneFormat = "(###) ###-####";
            if (TargetPhone != null && TargetPhone != "" && TargetPhone.Length > 9)
            {
                return Convert.ToString(Convert.ToInt64(TargetPhone).ToString(phoneFormat));
            }
            else
            {
                return TargetPhone;
            }
        }

        private static string GetFormatDate(string TargetDate)
        {   //Format the date as MM/DD/YYYY and return
            if (TargetDate != null && TargetDate != "" && TargetDate.Length > 0)
            {
                DateTime AppDate = Convert.ToDateTime(TargetDate);
                string ApptMonth = Convert.ToString(AppDate.Month);
                string ApptDay = Convert.ToString(AppDate.Day);
                string ApptYear = Convert.ToString(AppDate.Year);
                //Format date time AM/PM
                string ApptFullDate = (ApptMonth + "/" + ApptDay + "/" + ApptYear);
                return ApptFullDate;
            }
            else
            {
                return TargetDate;
            }
        }
        private static string GetFormatTime(string TargetDate)
        {   //Format Time as HH:MM:SS AM/PM and return
            if (TargetDate != null && TargetDate != "" && TargetDate.Length > 0)
            {
                var AppTime = TargetDate.Split(' ');
                string ApptTime = AppTime[1] + " " + AppTime[2];
                return (ApptTime);
            }
            else
            {
                return TargetDate;
            }
        }
    }
}
