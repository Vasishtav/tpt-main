﻿using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// The Serializer namespace.
/// </summary>
namespace HFC.CRM.Serializer
{
    /// <summary>
    /// Class OpenXmlData.
    /// </summary>
    public class OpenXmlData
    {
        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>The value.</value>
        public string Value { get; set; }
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The type.</value>
        public CellValues? Type {get; set;}
        /// <summary>
        /// Gets or sets the style.
        /// </summary>
        /// <value>The style.</value>
        public StyleTypeEnum? Style { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="OpenXmlData"/> class.
        /// </summary>
        public OpenXmlData() 
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenXmlData"/> class.
        /// </summary>
        /// <param name="value">The value.</param>
        public OpenXmlData(string value)
            : this()
        {
            Value = value;

        }
            
    }
}
