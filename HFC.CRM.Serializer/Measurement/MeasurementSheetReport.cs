﻿using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace HFC.CRM.Serializer.Measurement
{
   public class MeasurementSheetReport
    {
      private static  MeasurementManager MeasurementMgr = new MeasurementManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        
        public static string MeasurementSheet(int id, bool[] TLMeasurementOptions = null)
        {
            string data = string.Empty;
            if (SessionManager.BrandId == 1)
            {
                data = MeasurementSheet_BudgetBlinds(id, TLMeasurementOptions);
            }
            else if (SessionManager.BrandId == 2)
            {
                data = MeasurementSheet_TailorLiving(id, TLMeasurementOptions);
            }
            else
            {
                data =MeasurementSheet_ConcreteCraft(id, TLMeasurementOptions);
            }
            return data;

        }
        public static string MeasurementSheet_BudgetBlinds(int id, bool[] TLMeasurementOptions = null)
        {
         
            string Path = string.Empty;
            

            Path = string.Format("/Templates/Base/Brand/BB/MeasurementSheet.html");

            string srcFilePath = HostingEnvironment.MapPath(Path);

           
            var measurement = MeasurementMgr.Get(id);

            


            string data = System.IO.File.ReadAllText(srcFilePath);

            if(measurement==null)
            {
                data = "";
                return data;
            }
            var account = MeasurementMgr.GetAccountInfo(measurement.OpportunityId);
            List<Address> InstallAddresslist = MeasurementMgr.GetInstallationAddress(measurement.OpportunityId);
            Address InstallAddress = null;

            HFC.CRM.Data.Calendar Installer = MeasurementMgr.GetInstaller(measurement.OpportunityId);


            string PrimaryContact = account.PrimCustomer.FullName;
            string CompanyName = account.PrimCustomer.CompanyName;
          

            string PrimaryCellPhone = account.PrimCustomer.CellPhone;
            string PrimaryHomePhone = account.PrimCustomer.HomePhone;
            string PrimaryWorkPhone = account.PrimCustomer.WorkPhone;
            string PrimaryEmail = account.PrimCustomer.PrimaryEmail;

            string PrimaryNotes = "";
            
            string InstallAddress1 = string.Empty;
            string InstallAddress2 = string.Empty;
            string InstallAddres_CityStateZip = string.Empty;

            
            if (InstallAddresslist.Count > 0)
            {
                //Select Installation address
                InstallAddress = InstallAddresslist.First();
                InstallAddress1 = InstallAddress.Address1;
                InstallAddress2 = InstallAddress.Address2;
                InstallAddres_CityStateZip = InstallAddress.City + ',' + InstallAddress.State + ',' + InstallAddress.ZipCode;

            }


            CustomerTP SecCustomer;
            string Secondarycontact = "";
            string SecondaryCellPhone = "";
            if (account.SecCustomer.Count > 1)
            {
                //Select secondary address
                SecCustomer = account.SecCustomer.First();
                Secondarycontact = SecCustomer.FullName;
                SecondaryCellPhone = SecCustomer.CellPhone;
            }
            string SalesAgentName = "";
            string BookedBy = "";
            string AppointmentDate = "";
            string AppointmentTime = "";

            if (Installer != null)
            {

                if (Installer.AssignedName != null && Installer.AssignedName != "")
                {
                    SalesAgentName = Installer.AssignedName;
                }
                if (Installer.OrganizerName != null && Installer.OrganizerName != "")
                {
                    BookedBy = Installer.OrganizerName;
                }

                if (Installer.StartDate != null && Installer.StartDate.ToString() != "")
                {
                    AppointmentDate = Installer.StartDate.ToString("MM'/'dd'/'yyyy");
                    AppointmentTime = Installer.StartDate.ToString("hh:mm:ss");
                }
            }

            data = data.Replace("{{SalesAgentName}}", SalesAgentName);
            data = data.Replace("{{BookedBy}}", BookedBy);
            data = data.Replace("{{ApptDate}}", AppointmentDate);
            data = data.Replace("{{ApptTime}}", AppointmentTime);


            data = data.Replace("{{Contactfullname}}", PrimaryContact);
            data = data.Replace("{{CompanyName}}", CompanyName);
            data = data.Replace("{{InstallAddress1}}", InstallAddress1);
            data = data.Replace("{{InstallAddress2}}", InstallAddress2);
            data = data.Replace("{{InstallAddres_CityStateZip}}", InstallAddres_CityStateZip);
            data = data.Replace("{{PrimaryCellPhone}}", PrimaryCellPhone);
            data = data.Replace("{{PrimaryHomePhone}}", PrimaryHomePhone);
            data = data.Replace("{{PrimaryWorkPhone}}", PrimaryWorkPhone);
            data = data.Replace("{{PrimaryEmail}}", PrimaryEmail);
            data = data.Replace("{{Secondarycontact}}", Secondarycontact);
            data = data.Replace("{{SecondaryCellPhone}}", SecondaryCellPhone);



            List<MeasurementLineItemBB> bblineitems = null;
            int measurementindex = 1;
            StringBuilder measurementdata = new StringBuilder();
            if (measurement.MeasurementBB != null) {
                bblineitems = measurement.MeasurementBB.ToList();
                foreach (var items in bblineitems)
                {
                        measurementdata.Append("<tr>");
                        measurementdata.Append("<td>" + items.RoomLocation + "</td>");
                        measurementdata.Append("<td>" + items.RoomName + "</td>");
                        measurementdata.Append("<td>" + items.WindowLocation + "</td>");
                        measurementdata.Append("<td>" + items.MountTypeName + "</td>");
                        measurementdata.Append("<td>" + items.Width + "  " + items.FranctionalValueWidth + "</td>");
                        measurementdata.Append("<td>" + items.Height + "  " + items.FranctionalValueHeight + "</td>");
                        measurementdata.Append("</tr>");
                        measurementindex++;
                }
            }
           
            data = data.Replace("{{MeasurmentData}}", measurementdata.ToString());

            return data;
        }
        public static string MeasurementSheet_TailorLiving(int id, bool[] TLMeasurementOptions = null)
        {
            string Path = string.Empty;


            Path = string.Format("/Templates/Base/Brand/TL/MeasurementSheet.html");

            string srcFilePath = HostingEnvironment.MapPath(Path);


            var measurement = MeasurementMgr.Get(id);




            string data = System.IO.File.ReadAllText(srcFilePath);

            var account = MeasurementMgr.GetAccountInfo(measurement.OpportunityId);
            List<Address> InstallAddresslist = MeasurementMgr.GetInstallationAddress(measurement.OpportunityId);
            Address InstallAddress = null;

            HFC.CRM.Data.Calendar Installer = MeasurementMgr.GetInstaller(measurement.OpportunityId);


            string PrimaryContact = account.PrimCustomer.FullName;
            string CompanyName = account.PrimCustomer.CompanyName;


            string PrimaryCellPhone = account.PrimCustomer.CellPhone;
            string PrimaryHomePhone = account.PrimCustomer.HomePhone;
            string PrimaryWorkPhone = account.PrimCustomer.WorkPhone;
            string PrimaryEmail = account.PrimCustomer.PrimaryEmail;

            string PrimaryNotes = "";

            string InstallAddress1 = string.Empty;
            string InstallAddress2 = string.Empty;
            string InstallAddres_CityStateZip = string.Empty;


            if (InstallAddresslist.Count > 0)
            {
                //Select Installation address
                InstallAddress = InstallAddresslist.First();
                InstallAddress1 = InstallAddress.Address1;
                InstallAddress2 = InstallAddress.Address2;
                InstallAddres_CityStateZip = InstallAddress.City + ',' + InstallAddress.State + ',' + InstallAddress.ZipCode;

            }


            CustomerTP SecCustomer;
            string Secondarycontact = "";
            string SecondaryCellPhone = "";
            if (account.SecCustomer.Count > 1)
            {
                //Select secondary address
                SecCustomer = account.SecCustomer.First();
                Secondarycontact = SecCustomer.FullName;
                SecondaryCellPhone = SecCustomer.CellPhone;
            }
            string SalesAgentName = "";
            string BookedBy = "";
            string AppointmentDate = "";
            string AppointmentTime = "";

            if (Installer != null)
            {

                if (Installer.AssignedName != null && Installer.AssignedName != "")
                {
                    SalesAgentName = Installer.AssignedName;
                }
                if (Installer.OrganizerName != null && Installer.OrganizerName != "")
                {
                    BookedBy = Installer.OrganizerName;
                }

                if (Installer.StartDate != null && Installer.StartDate.ToString() != "")
                {
                    AppointmentDate = Installer.StartDate.ToString("MM'/'dd'/'yyyy");
                    AppointmentTime = Installer.StartDate.ToString("hh:mm:ss");
                }
            }

            data = data.Replace("{{SalesAgentName}}", SalesAgentName);
            data = data.Replace("{{BookedBy}}", BookedBy);
            data = data.Replace("{{ApptDate}}", AppointmentDate);
            data = data.Replace("{{ApptTime}}", AppointmentTime);


            data = data.Replace("{{Contactfullname}}", PrimaryContact);
            data = data.Replace("{{CompanyName}}", CompanyName);
            data = data.Replace("{{InstallAddress1}}", InstallAddress1);
            data = data.Replace("{{InstallAddress2}}", InstallAddress2);
            data = data.Replace("{{InstallAddres_CityStateZip}}", InstallAddres_CityStateZip);
            data = data.Replace("{{PrimaryCellPhone}}", PrimaryCellPhone);
            data = data.Replace("{{PrimaryHomePhone}}", PrimaryHomePhone);
            data = data.Replace("{{PrimaryWorkPhone}}", PrimaryWorkPhone);
            data = data.Replace("{{PrimaryEmail}}", PrimaryEmail);
            data = data.Replace("{{Secondarycontact}}", Secondarycontact);
            data = data.Replace("{{SecondaryCellPhone}}", SecondaryCellPhone);



            List<MeasurementLineItemTL> TLlineitems = null;
            int measurementindex = 1;

            StringBuilder measurementdata = new StringBuilder();
            if (measurement.MeasurementTL != null)
            {
                TLlineitems = measurement.MeasurementTL.ToList();
                foreach (var items in TLlineitems)
                {
                        if(AddRow(items.System, TLMeasurementOptions))
                        {
                            measurementdata.Append("<tr>");
                            measurementdata.Append("<td>" + items.System + "</td>");
                            measurementdata.Append("<td>" + items.SystemDescription + "</td>");
                            measurementdata.Append("<td>" + items.StorageType + "</td>");
                            measurementdata.Append("<td>" + items.StorageDescription + "</td>");
                            measurementdata.Append("<td>" + items.Width + "  " + items.FranctionalValueWidth + "</td>");
                            measurementdata.Append("<td>" + items.Height + "  " + items.FranctionalValueHeight + "</td>");
                            measurementdata.Append("<td>" + items.Depth + "  " + items.FranctionalValueDepth + "</td>");
                            measurementdata.Append("</tr>");
                            measurementindex++;
                        }
                }
            }
            data = data.Replace("{{MeasurmentData}}", measurementdata.ToString());

            return data;
        }
        private static bool AddRow(string system, bool[] TLMeasurementOptions = null)
        {
            bool addrow = false;
            //TLMeasurementOptions[0] = Garage;
            //TLMeasurementOptions[1] = Closet;
            //TLMeasurementOptions[2] = Office;
            //TLMeasurementOptions[3] = FloorMeasurement;
            //TLMeasurementOptions[4] = ConcreteCraft;
            if (system=="Garage")
            {
                if(TLMeasurementOptions[0]==true)
                {
                    addrow = true;
                }
            }
            if (system == "Closet")
            {
                if (TLMeasurementOptions[1] == true)
                {
                    addrow = true;
                }
            }
            if (system == "Office")
            {
                if (TLMeasurementOptions[2] == true)
                {
                    addrow = true;
                }
            }
            if (system == "Floor")
            {
                if (TLMeasurementOptions[3] == true)
                {
                    addrow = true;
                }
            }
            if (system == "ConcreteCraft")
            {
                if (TLMeasurementOptions[4] == true)
                {
                    addrow = true;
                }
            }
            return addrow;
        }
        public static string MeasurementSheet_ConcreteCraft(int id, bool[] TLMeasurementOptions = null)
        {
            //Concere craft print will be added later
            string LeadData = string.Empty;
            string Path = string.Empty;
            string data = string.Empty;
            return data;
        }
        public static string FilePath()
        {
            string Path = string.Empty;
            if (SessionManager.BrandId == 1)
            {
                Path = string.Format("/Templates/Base/Brand/BB/MeasurementSheet-Template.html");
            }
            else if (SessionManager.BrandId == 2)
            {
                Path = string.Format("/Templates/Base/Brand/TL/MeasurementSheet-Template.html");
            }
            else
            {
                Path = string.Format("/Templates/Base/Brand/CC/MeasurementSheet-Template.html");
            }
            string srcFilePath = HostingEnvironment.MapPath(Path);
            return srcFilePath;
        }
    }
}
