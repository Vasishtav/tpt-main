﻿/* NOTES
 * This is an adaptation of Mika Wendelius "Creating basic Excel workbook with Open XML"
 * http://www.codeproject.com/Articles/371203/Creating-basic-Excel-workbook-with-Open-XML
 * Created by Quan Tran
 * Last Edited 11/26/2013
 */
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using X = DocumentFormat.OpenXml.Spreadsheet;
using W = DocumentFormat.OpenXml.Wordprocessing;
using System.IO;
using System.Linq;

/// <summary>
/// The Serializer namespace.
/// </summary>
namespace HFC.CRM.Serializer
{
    /// <summary>
    /// Class OpenXmlUtil.
    /// </summary>
    public class OpenXmlUtil
    {
        /// <summary>
        /// Creates a basic workbook
        /// </summary>
        /// <param name="outputStream">The output stream.</param>
        /// <param name="autoSave">if set to <c>true</c> [automatic save].</param>
        /// <returns>SpreadsheetDocument.</returns>
        public static SpreadsheetDocument CreateWorkbook(MemoryStream outputStream, bool autoSave = false)
        {
            //create spreadsheet  -- create a spreadsheet decument from an IO stream
            SpreadsheetDocument spreadSheet = SpreadsheetDocument.Create(outputStream, SpreadsheetDocumentType.Workbook, autoSave);

            // Workbook
            spreadSheet.AddWorkbookPart();
            spreadSheet.WorkbookPart.Workbook = new X.Workbook();
            spreadSheet.WorkbookPart.Workbook.Save();

            //// Shared string table
            //SharedStringTablePart sharedStringTablePart = spreadSheet.WorkbookPart.AddNewPart<SharedStringTablePart>();
            //sharedStringTablePart.SharedStringTable = new SharedStringTable();
            //sharedStringTablePart.SharedStringTable.Save();

            // Sheets collection            
            spreadSheet.WorkbookPart.Workbook.Sheets = new X.Sheets();
            spreadSheet.WorkbookPart.Workbook.Save();

            // Stylesheet
            WorkbookStylesPart workbookStylesPart = spreadSheet.WorkbookPart.AddNewPart<WorkbookStylesPart>();
            workbookStylesPart.Stylesheet = new X.Stylesheet();
            workbookStylesPart.Stylesheet.Save();

            return spreadSheet;
        }

        /// <summary>
        /// Converts a column number to column name (i.e. A, B, C..., AA, AB...)
        /// </summary>
        /// <param name="columnIndex">Index of the column</param>
        /// <returns>Column name</returns>
        public static string ColumnNameFromIndex(uint columnIndex)
        {
            uint remainder;
            string columnName = "";

            while (columnIndex > 0)
            {
                remainder = (columnIndex - 1) % 26;
                columnName = System.Convert.ToChar(65 + remainder).ToString() + columnName;
                columnIndex = (uint)((columnIndex - remainder) / 26);
            }

            return columnName;
        }

        /// <summary>
        /// Replaces the content control placeholder and removes the control once a text is filled
        /// </summary>
        /// <param name="sdtBlock">Content control block</param>
        /// <param name="replaceText">Text to replace</param>
        /// <param name="linkId">Hyperlink relation Id to use to create hyperlinks</param>
        public static void FillContentControl(W.SdtBlock sdtBlock, string replaceText, string linkId = null)
        {
            var replacedRun = new W.Run();

            W.SdtProperties ccProperties = sdtBlock.SdtProperties;
            var runProperties = ccProperties.Descendants<W.RunProperties>().FirstOrDefault();

            W.RunStyle style = null;
            if (sdtBlock.SdtContentBlock != null)
            {
                style = sdtBlock.SdtContentBlock.Descendants<W.RunStyle>().FirstOrDefault();
            }

            if (style != null && style.Val != "PlaceholderText")
                replacedRun.Append(new W.RunProperties(style.CloneNode(true)));
            else if (runProperties != null)
                replacedRun.Append(runProperties.CloneNode(true));

            replacedRun.Append(new W.Text(replaceText));

            if (sdtBlock.Parent is W.Paragraph)
            {
                if (!string.IsNullOrEmpty(linkId))
                {
                    var link = new W.Hyperlink(new W.ProofError() { Type = W.ProofingErrorValues.GrammarStart })
                    {
                        History = new OnOffValue(true),
                        Id = linkId
                    };
                    replacedRun.RunProperties = new W.RunProperties(new W.RunStyle() { Val = "Hyperlink" });

                    link.Append(replacedRun);
                    sdtBlock.InsertBeforeSelf(link);
                }
                else
                    sdtBlock.InsertBeforeSelf(replacedRun);
            }
            else
            {
                var paragraph = new W.Paragraph();
                if (!string.IsNullOrEmpty(linkId))
                {
                    paragraph.ParagraphProperties = new W.ParagraphProperties(new W.Justification() { Val = W.JustificationValues.Center });

                    var link = new W.Hyperlink(new W.ProofError() { Type = W.ProofingErrorValues.GrammarStart })
                    {
                        History = new OnOffValue(true),
                        Id = linkId
                    };
                    replacedRun.RunProperties = new W.RunProperties(new W.RunStyle() { Val = "Hyperlink" });
                    link.Append(replacedRun);

                    paragraph.Append(link);
                }
                else
                    paragraph.Append(replacedRun);

                sdtBlock.InsertBeforeSelf(paragraph);
            }
            sdtBlock.Remove();
        }

        public static void FillContentControl(W.SdtRun sdtRun, string replaceText, string linkId = null)
        {
            var replacedRun = new W.Run();

            W.SdtProperties ccProperties = sdtRun.SdtProperties;
            var runProperties = ccProperties.Descendants<W.RunProperties>().FirstOrDefault();

            W.RunStyle style = null;
            if (sdtRun.SdtContentRun != null)
            {
                style = sdtRun.SdtContentRun.Descendants<W.RunStyle>().FirstOrDefault();
            }

            if (style != null && style.Val != "PlaceholderText")
                replacedRun.Append(new W.RunProperties(style.CloneNode(true)));
            else if (runProperties != null)
                replacedRun.Append(runProperties.CloneNode(true));

            replacedRun.Append(new W.Text(replaceText));

            if (sdtRun.Parent is W.Paragraph)
            {
                if (!string.IsNullOrEmpty(linkId))
                {
                    var link = new W.Hyperlink(new W.ProofError() { Type = W.ProofingErrorValues.GrammarStart })
                    {
                        History = new OnOffValue(true),
                        Id = linkId
                    };
                    replacedRun.RunProperties = new W.RunProperties(new W.RunStyle() { Val = "Hyperlink" });
                    link.Append(replacedRun);

                    sdtRun.InsertBeforeSelf(link);
                }
                else
                    sdtRun.InsertBeforeSelf(replacedRun);
            }
            else
            {
                var paragraph = new W.Paragraph();
                if (!string.IsNullOrEmpty(linkId))
                {
                    paragraph.ParagraphProperties = new W.ParagraphProperties(new W.Justification() { Val = W.JustificationValues.Center });
                    var link = new W.Hyperlink(new W.ProofError() { Type = W.ProofingErrorValues.GrammarStart })
                    {
                        History = new OnOffValue(true),
                        Id = linkId
                    };
                    replacedRun.RunProperties = new W.RunProperties(new W.RunStyle() { Val = "Hyperlink" });
                    link.Append(replacedRun);

                    paragraph.Append(link);
                }
                else
                    paragraph.Append(replacedRun);

                if (sdtRun.Parent != null)
                    sdtRun.InsertBeforeSelf(paragraph);
            }

            if (sdtRun.Parent != null)
                sdtRun.Remove();
        }

    }
}
