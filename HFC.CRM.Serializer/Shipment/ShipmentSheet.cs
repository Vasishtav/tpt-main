﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Extensions;
using HFC.CRM.Data;
using HFC.CRM.DTO.VendorInvoice;
using HFC.CRM.Managers;
using SelectPdf;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace HFC.CRM.Serializer.Shipment
{
    public class ShipmentSheet
    {
        ShipNoticeManager Shipment_Mgr = new ShipNoticeManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        public string ShipmentsSheet(ShipNotice shippingdata)
        {
            string data = string.Empty;
            data = ShipmentSheetBody(shippingdata);
            return data;
        }
        public string ShipmentSheetHeader(ShipNotice shippingdata)
        {
            string data = string.Empty;
            data = ShipmentHeader(shippingdata);
            return data;
        }
        public string ShipmentSheetFooter()
        {
            string data = string.Empty;
            data = ShipmentFooter();
            return data;
        }

        public string ShipmentSheetBody(ShipNotice shippingdata)
        {
            string Path = string.Empty;
            Path = string.Format("/Templates/Base/Brand/BB/ShippingNotice.html");
            string srcFilePath = HostingEnvironment.MapPath(Path);
            string data = System.IO.File.ReadAllText(srcFilePath);
            try
            {
                data = data.Replace("{VendorName}", shippingdata.VendorName);
                data = data.Replace("{ShipId}", shippingdata.ShipmentId);
                if (shippingdata.ShippedDate != null)
                {
                    DateTime shipdate = (DateTime)shippingdata.ShippedDate;
                    data = data.Replace("{ShippedDate}",shipdate.GlobalDateFormat());
                }
                else
                    data = data.Replace("{ShippedDate}", "");
                if (shippingdata.EstDeliveryDate != null)
                {
                    DateTime DeliveryDate = (DateTime)shippingdata.EstDeliveryDate;
                    data = data.Replace("{EstDeliveryDate}", DeliveryDate.GlobalDateFormat());
                }
                else
                    data = data.Replace("{EstDeliveryDate}", "");
                data = data.Replace("{ShippedVia}", shippingdata.ShippedViaTP);
                data = data.Replace("{TrackingNumber}",String.Join(",", shippingdata.TrackingList.Select(list=> list.Key!=""|| list.Key != null)));
                data = data.Replace("{Routing}", shippingdata.Routing);
                data = data.Replace("{MasterPONumber}", shippingdata.MasterPONumber.ToString());
                data = data.Replace("{ShiptoAccount}", shippingdata.ShiptoAccount);
                data = data.Replace("{ShippedTo}", shippingdata.ShippedTo);
                data = data.Replace("{BOL}", shippingdata.BOL);
                data = data.Replace("{TotalQuantityShipped}", shippingdata.TotalQuantityShipped);
                data = data.Replace("{TotalBoxesTP}", shippingdata.TotalBoxesTP.ToString());
                data = data.Replace("{Weight}", shippingdata.Weight);
                data = data.Replace("{Status}", shippingdata.Status);
                data = data.Replace("{TotalQuantityReceived}", shippingdata.TotalQuantityReceived);
                data = data.Replace("{TotalQuantityShortShipped}", shippingdata.TotalQuantityShortShipped);
                data = data.Replace("{TotalQuantityDamaged}", shippingdata.TotalQuantityDamaged);
                data = data.Replace("{TotalQuantityMisShipped}", shippingdata.TotalQuantityMisShipped);
                data = data.Replace("{RemakeReference}", shippingdata.RemakeReference);
                data = data.Replace("{CaseID}", "");
                data = data.Replace("{UpdatedByUserName}", shippingdata.UpdatedByUserName);
                if (shippingdata.CheckedinDate != null)
                {
                    DateTime CheckedinDate = (DateTime)shippingdata.CheckedinDate;
                    data = data.Replace("{CheckedinDate}", CheckedinDate.GlobalDateFormat());
                }
                else
                    data = data.Replace("{CheckedinDate}", "");

                StringBuilder ListShipmentDetails = new StringBuilder();

                ListShipmentDetails.Append("<table class='table table - bordered invoice_tablegrid'>");
                ListShipmentDetails.Append("<thead class='expand_lhead'>");
                ListShipmentDetails.Append("<tr class='invoice_headsection'>");
                ListShipmentDetails.Append("<th></th>");
                ListShipmentDetails.Append("<th>VPO/xVPO</th>");
                ListShipmentDetails.Append("<th>PO Line #</th>");
                ListShipmentDetails.Append("<th>Item</th>");
                ListShipmentDetails.Append("<th>Item Description</th>");
                ListShipmentDetails.Append("<th>QTY Shipped</th>");
                ListShipmentDetails.Append("<th>UoM</th>");
                ListShipmentDetails.Append("<th>Box ID</th>");
                ListShipmentDetails.Append("<th>Sales Order-Sidemark</th>");
                ListShipmentDetails.Append("<th>SO Line #</th>");
                ListShipmentDetails.Append("<th>QTY Recv'd</th>");
                ListShipmentDetails.Append("<th>QTY Damaged</th>");
                ListShipmentDetails.Append("<th>QTY Mis-Shipped</th>");
                ListShipmentDetails.Append("<th>QTY Short-Shipped</th>");
                ListShipmentDetails.Append("<th>Tracking Info</th>");
                ListShipmentDetails.Append("</tr>");
                ListShipmentDetails.Append("</thead>");
                ListShipmentDetails.Append("<tbody>");

                if (shippingdata.ListShipmentDetails != null && shippingdata.ListShipmentDetails.Count > 0)
                {
                    int RowNumber = 1;
                    foreach (var item in shippingdata.ListShipmentDetails)
                    {
                        ListShipmentDetails.Append("<tr class='invoice_headsection-2' style='page-break-inside: avoid'>");                       
                        ListShipmentDetails.Append("<td>" + RowNumber + "</td>");
                        ListShipmentDetails.Append("<td>" + shippingdata.MasterPONumber + "</td>");
                        ListShipmentDetails.Append("<td>" + item.POItem + "</td>");
                        ListShipmentDetails.Append("<td>" + item.ProductNumber + "</td>");
                        ListShipmentDetails.Append("<td>" + item.ItemDescription + "</td>");
                        ListShipmentDetails.Append("<td>" + item.QuantityShipped + "</td>");
                        ListShipmentDetails.Append("<td>" + item.UomDefinition + "</td>");
                        ListShipmentDetails.Append("<td>" + item.BoxId + "</td>");
                        ListShipmentDetails.Append("<td>" +item.OrderNumber+" - "+ item.SideMark + "</td>");
                        ListShipmentDetails.Append("<td>" + item.LineNumber + "</td>");
                        ListShipmentDetails.Append("<td>" + item.QuantityReceived + "</td>");
                        ListShipmentDetails.Append("<td><input type='checkbox' class='option-input checkbox'></td>");
                        ListShipmentDetails.Append("<td><input type='checkbox' class='option-input checkbox'></td>");
                        ListShipmentDetails.Append("<td><input type='checkbox' class='option-input checkbox'></td>");
                        ListShipmentDetails.Append("<td>" + item.TrackingId + "</td>");
                        ListShipmentDetails.Append("</tr>");
                        //for incrementing row number
                        RowNumber++;
                    }

                }
                ListShipmentDetails.Append("</tbody>");
                ListShipmentDetails.Append("</table>");

                data = data.Replace("{ListShipmentDetails}", ListShipmentDetails.ToString());

            }
            catch (Exception ex)
            {
                return data;
            }
            return data;
        }

        public string ShipmentHeader(ShipNotice shippingdata)
        {
            string LeadData = string.Empty;
            string Path = string.Empty;
            Path = string.Format("/Templates/Base/Brand/BB/BB_Shipment_Template-header.html");

            string srcFilePath = HostingEnvironment.MapPath(Path);
            string data = System.IO.File.ReadAllText(srcFilePath);

            data = data.Replace("{ShiptoAccount}", shippingdata.ShiptoAccount);
            data = data.Replace("{ShipId}", shippingdata.ShipmentId);

            return data;
        }

        public string ShipmentFooter()
        {
            string Path = string.Empty;
            Path = string.Format("/Templates/Base/Brand/BB/BB_Shipment_Template-footer.html");
            string srcFilePath = HostingEnvironment.MapPath(Path);

            string data = System.IO.File.ReadAllText(srcFilePath);
            data = data.Replace("{printeddate}", TimeZoneManager.ToLocal(DateTime.UtcNow).GlobalDateFormat());

            return data;
        }

        public PdfDocument createShipmentDoc(int id)
        {
            // Get Shipment details
            ShipNotice shippingdata = Shipment_Mgr.GetValue(id);

            //Generate Header, Body, Footer html 
            string data = ShipmentsSheet(shippingdata);
            string headerdata = ShipmentSheetHeader(shippingdata);
            string footerdata = ShipmentSheetFooter();

            return GeneratePdfDocument(headerdata, data, footerdata);
        }

        public PdfDocument GeneratePdfDocument(string headerdata, string bodydata, string footerdata)
        {

            string baseUrl = Serializer.Leads.LeadSheetReport.TemplateImageFilePath();

            // instantiate a html to pdf converter object 
            HtmlToPdf converter = new HtmlToPdf();

            converter.Options.PdfPageSize = PdfPageSize.Letter;
            converter.Options.PdfPageOrientation = PdfPageOrientation.Portrait;

            converter.Options.MarginTop = 20;
            converter.Options.MarginBottom = 30;
            converter.Options.MarginLeft = 10;
            converter.Options.MarginRight = 10;

            converter.Options.DisplayHeader = true;
            converter.Header.DisplayOnFirstPage = true;
            converter.Header.DisplayOnOddPages = true;
            converter.Header.DisplayOnEvenPages = true;
            converter.Header.Height = 60;

            PdfHtmlSection headerHtml = new PdfHtmlSection(headerdata, baseUrl);
            headerHtml.AutoFitHeight = HtmlToPdfPageFitMode.AutoFit;
            converter.Header.Add(headerHtml);

            converter.Options.DisplayFooter = true;
            converter.Footer.DisplayOnFirstPage = true;
            converter.Footer.DisplayOnEvenPages = true;
            converter.Footer.DisplayOnOddPages = true;
            converter.Footer.Height = 50;

            PdfHtmlSection footerHtml = new PdfHtmlSection(footerdata, baseUrl);
            footerHtml.AutoFitHeight = HtmlToPdfPageFitMode.AutoFit;
            converter.Footer.Add(footerHtml);

            // page numbers can be added using a PdfTextSection object
            PdfTextSection text = new PdfTextSection(300, 25, "Page {page_number} of {total_pages}  ", new System.Drawing.Font("Arial", 8));
            text.HorizontalAlign = PdfTextHorizontalAlign.Center;
            converter.Footer.Add(text);

            PdfDocument doc = converter.ConvertHtmlString(bodydata, baseUrl);

            return doc;
        }

    }
}
