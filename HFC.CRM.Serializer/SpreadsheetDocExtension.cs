﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HFC.CRM.Data;
using DocumentFormat.OpenXml;

/// <summary>
/// The Serializer namespace.
/// </summary>
namespace HFC.CRM.Serializer
{
    /// <summary>
    /// Class SpreadsheetDocExtension.
    /// </summary>
    public static class SpreadsheetDocExtension
    {
        /// <summary>
        /// Adds a new worksheet to the workbook
        /// </summary>
        /// <param name="spreadsheet">Spreadsheet to use</param>
        /// <param name="name">Name of the worksheet</param>
        /// <returns>True if succesful</returns>
        public static Worksheet AddWorksheet(this SpreadsheetDocument spreadsheet, string name)
        {
            Sheets sheets = spreadsheet.WorkbookPart.Workbook.GetFirstChild<Sheets>();
            
            // Add the worksheetpart
            Worksheet worksheet = new Worksheet(new SheetData());
            WorksheetPart worksheetPart = spreadsheet.WorkbookPart.AddNewPart<WorksheetPart>();
            worksheetPart.Worksheet = worksheet;
            worksheetPart.Worksheet.Save();

            // Add the sheet and make relation to workbook
            Sheet sheet = new Sheet()
            {
                Id = spreadsheet.WorkbookPart.GetIdOfPart(worksheetPart),
                SheetId = (uint)(spreadsheet.WorkbookPart.Workbook.Sheets.Count() + 1),
                Name = name
            };
            sheets.Append(sheet);
            spreadsheet.WorkbookPart.Workbook.Save();

            return worksheet;
        }

        /// <summary>
        /// Add basic style xml
        /// </summary>
        /// <param name="spreadsheet">The spreadsheet.</param>
        /// <returns>The stylesheet of the current spreadsheet</returns>
        public static Stylesheet AddBasicStyles(this SpreadsheetDocument spreadsheet)
        {
            Stylesheet stylesheet = spreadsheet.WorkbookPart.WorkbookStylesPart.Stylesheet;
            
            //Long Date
            var dateLongFormat = new NumberingFormat
            {
                NumberFormatId = (uint)StandardFormatCodes.DateMMDDYYYYHHMMTT,
                FormatCode = StandardFormatCodes.DateMMDDYYYYHHMMTT.Description()
            };
            //US Currency
            var currencyFormat = new NumberingFormat
            {
                NumberFormatId = (uint)StandardFormatCodes.CurrencyDecimalRed,
                FormatCode = string.Format("\"{0}\"#,##0.00;[Red](\"{0}\"#,##0.00)", System.Globalization.CultureInfo.CurrentUICulture.NumberFormat.CurrencySymbol)
            };

            var numberingFormatParent = new NumberingFormats
                (
                    currencyFormat,
                    dateLongFormat
                );

            stylesheet.InsertAt<NumberingFormats>(numberingFormatParent, 0);

            //Fonts
            var fonts = new Fonts(
                new Font()
                {
                    FontSize = new FontSize { Val = 11 },
                    FontName = new FontName { Val = "Calibri" }
                },
                new Font() //header font, white text and bold
                {
                    FontSize = new FontSize { Val = 11 },
                    FontName = new FontName { Val = "Calibri" },
                    //Color = new Color { Rgb = "00FFFFFF" },
                    //Bold = new Bold { Val = true  }
                },

                  new Font() //header font, white text and bold
                {
                    FontSize = new FontSize { Val = 11 },
                    FontName = new FontName { Val = "Calibri" },
                   
                    Bold = new Bold { Val = true  }
                }
            );
            stylesheet.InsertAt<Fonts>(fonts, 1);

            // Fills (x:fills)
            var fills = new Fills(          
                new Fill()
                {
                    PatternFill = new PatternFill() { PatternType = PatternValues.None }
                },
                new Fill()
                {
                    PatternFill = new PatternFill() { PatternType = PatternValues.Gray125 }
                },
                new Fill() //header fill
                { 
                    PatternFill = new PatternFill()
                    { 
                        PatternType = PatternValues.Solid,
                        ForegroundColor = new ForegroundColor { Rgb = "007a7a7a" },   //4F81BD                
                        BackgroundColor = new BackgroundColor { Indexed = 64 }
                    }
                }
            );            
            stylesheet.InsertAt<Fills>(fills, 2);

            // Borders (x:borders)
            //stylesheet.InsertAt<Borders>(new Borders(), 3);
            //stylesheet.GetFirstChild<Borders>().InsertAt<Border>(
            //   new Border()
            //   {
            //       LeftBorder = new LeftBorder(),
            //       RightBorder = new RightBorder(),
            //       TopBorder = new TopBorder(),
            //       BottomBorder = new BottomBorder(),
            //       DiagonalBorder = new DiagonalBorder()
            //   }, 0);



            var borders = new Borders(
                new Border()
               {
                   LeftBorder = new LeftBorder(),
                   RightBorder = new RightBorder(),
                   TopBorder = new TopBorder(),
                   BottomBorder = new BottomBorder(),
                   DiagonalBorder = new DiagonalBorder()
               },

              new Border()
               {
                   LeftBorder = new LeftBorder() { Style = BorderStyleValues.None },
                   RightBorder = new RightBorder() { Style = BorderStyleValues.None },
                   TopBorder = new TopBorder() { Style = BorderStyleValues.None },
                   BottomBorder = new BottomBorder() { Style = BorderStyleValues.Thick },
                   DiagonalBorder = new DiagonalBorder() { }
               },

                new Border()
                {
                    LeftBorder = new LeftBorder() { Style = BorderStyleValues.None },
                    RightBorder = new RightBorder() { Style = BorderStyleValues.None },
                    TopBorder = new TopBorder() { Style = BorderStyleValues.Thick },
                    BottomBorder = new BottomBorder() { Style = BorderStyleValues.None},
                    DiagonalBorder = new DiagonalBorder() { }
                }
               );



             stylesheet.InsertAt<Borders>(borders, 3);

            // Cell style formats (x:CellStyleXfs)
            stylesheet.InsertAt<CellStyleFormats>(new CellStyleFormats(), 4);
            stylesheet.GetFirstChild<CellStyleFormats>().InsertAt<CellFormat>(
               new CellFormat
               {
                   NumberFormatId = 0,
                   FontId = 0,
                   FillId = 0,
                   BorderId = 0
               }, 0);


            // Cell formats (x:CellXfs) 
            stylesheet.InsertAt<CellFormats>(new CellFormats(), 5);

            // General text
            stylesheet.GetFirstChild<CellFormats>().InsertAt<CellFormat>(
               new CellFormat()
               {
                   FormatId = 0,
                   NumberFormatId = 0
               }, (int)StyleTypeEnum.General);

            // Text cell type
            stylesheet.GetFirstChild<CellFormats>().InsertAt<CellFormat>(
               new CellFormat()
               {
                   FormatId = 0,
                   NumberFormatId = (uint)StandardFormatCodes.AtSymbol
               }, (int)StyleTypeEnum.Text);

            // Currency
            stylesheet.GetFirstChild<CellFormats>().InsertAt<CellFormat>(
               new CellFormat()
               {
                   ApplyNumberFormat = true,
                   FormatId = 0,
                   NumberFormatId = currencyFormat.NumberFormatId,
                   FontId = 0,
                   FillId = 0,
                   BorderId = 0
               }, (int)StyleTypeEnum.Currency);

            // Long Date
            stylesheet.GetFirstChild<CellFormats>().InsertAt<CellFormat>(
               new CellFormat()
               {
                   ApplyNumberFormat = true,
                   FormatId = 0,
                   NumberFormatId = dateLongFormat.NumberFormatId,
                   FontId = 0,
                   FillId = 0,
                   BorderId = 0
               }, (int)StyleTypeEnum.LongDate);

            // Numbers 2 decimal
            stylesheet.GetFirstChild<CellFormats>().InsertAt<CellFormat>(
               new CellFormat()
               {
                   ApplyNumberFormat = true,
                   FormatId = 0,
                   NumberFormatId = (uint)StandardFormatCodes.NumberDecimal,
                   FontId = 0,
                   FillId = 0,
                   BorderId = 0
               }, (int)StyleTypeEnum.NumberTwoDecimal);            

            // Percentage 2 decimal
            stylesheet.GetFirstChild<CellFormats>().InsertAt<CellFormat>(
               new CellFormat()
               {
                   ApplyNumberFormat = true,
                   FormatId = 0,
                   NumberFormatId = (uint)StandardFormatCodes.PercentTwoDecimals,
                   FontId = 0,
                   FillId = 0,
                   BorderId = 0
               }, (int)StyleTypeEnum.PercentTwoDecimal);

            // header cell with blue fill and white text
            stylesheet.GetFirstChild<CellFormats>().InsertAt<CellFormat>(
               new CellFormat()
               {         
                   ApplyFill = true,
                   ApplyFont = true,
                   FormatId = 0,
                   NumberFormatId = 0,
                   BorderId = 0,
                   FontId = 1,
                   FillId = 2
               }, (int)StyleTypeEnum.HeaderStyle);

            //  FormatId = 0,
              //     NumberFormatId = (uint)StandardFormatCodes.AtSymbol
             
           
            stylesheet.GetFirstChild<CellFormats>().InsertAt<CellFormat>(
               new CellFormat()
               {
                  
                   FormatId = 0,
                   NumberFormatId = 0,
                   BorderId = 1,
                   FontId = 2,
                   FillId = 0
                
               }, (int)StyleTypeEnum.TextInBold);

            stylesheet.GetFirstChild<CellFormats>().InsertAt<CellFormat>(
              new CellFormat()
              {

                  FormatId = 0,
                  NumberFormatId = 0,
                  BorderId = 1,
                  FontId = 2,
                  FillId = 0

              }, (int)StyleTypeEnum.TextInBoldBorderBottomOnly);

            stylesheet.GetFirstChild<CellFormats>().InsertAt<CellFormat>(
              new CellFormat()
              {

                  FormatId = 0,
                  NumberFormatId = 0,
                  BorderId = 2,
                  FontId = 2,
                  FillId = 0

              }, (int)StyleTypeEnum.TextInBoldBorderTopOnly);


            stylesheet.GetFirstChild<CellFormats>().InsertAt<CellFormat>(
             new CellFormat()
             {

                 ApplyNumberFormat = true,
                 FormatId = 0,
                 NumberFormatId = currencyFormat.NumberFormatId,
                 FontId = 2,
                 FillId = 0,
                 BorderId = 2            
             }, (int)StyleTypeEnum.CurrencyInBoldBorderTopOnly);

            stylesheet.Save();

            return stylesheet;
        }

        /// <summary>
        /// Adds the header row.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="columnData">The column data.</param>
        /// <returns>Row.</returns>
        public static Row AddHeaderRow(this Worksheet worksheet, params OpenXmlColumnData[] columnData)
        {
            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
            var columns = new Columns();
            var row = new Row();

            uint columnIndex = 0;
            foreach (var col in columnData)
            {
                columns.Append(new Column { CustomWidth = true, Width = new DoubleValue(col.Width > 0 ? col.Width : 20d), Min = columnIndex + 1, Max = columnIndex + 1 });
                row.AddCell(new OpenXmlData(col.Name) { 
                    Style = StyleTypeEnum.HeaderStyle, 
                    Type = CellValues.String
                });
                columnIndex++;
            }
            worksheet.PrependChild(columns);
            sheetData.Append(row);

         
            //if (style != null)
            //    row.StyleIndex = (uint)style;

            return row;
        }


        public static Row AddHeaderInBoldRow(this Worksheet worksheet, params OpenXmlColumnData[] columnData)
        {
            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
            var columns = new Columns();
            var row = new Row();

            uint columnIndex = 0;
            foreach (var col in columnData)
            {
                columns.Append(new Column { CustomWidth = true, Width = new DoubleValue(col.Width > 0 ? col.Width : 20d), Min = columnIndex + 1, Max = columnIndex + 1 });
                row.AddCell(new OpenXmlData(col.Name)
                {
                    Style = StyleTypeEnum.TextInBold,
                    Type = CellValues.String
                });
                columnIndex++;
            }
            worksheet.PrependChild(columns);
            sheetData.Append(row);


            //if (style != null)
            //    row.StyleIndex = (uint)style;

            return row;
        }

        public static Row AddRow(this Worksheet worksheet, params OpenXmlColumnData[] columnData)
        {
            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
            var columns = new Columns();
            var row = new Row();

            uint columnIndex = 0;
            foreach (var col in columnData)
            {
                columns.Append(new Column { CustomWidth = true, Width = new DoubleValue(col.Width > 0 ? col.Width : 20d), Min = columnIndex + 1, Max = columnIndex + 1 });
                row.AddCell(new OpenXmlData(col.Name)
                {
                    Style = StyleTypeEnum.Text,
                    Type = CellValues.String
                });
                columnIndex++;
            }
            worksheet.PrependChild(columns);
            sheetData.Append(row);
            return row;
        }



        public static Row AddTotalRow(this Worksheet worksheet, OpenXmlColumnData columnData1, OpenXmlData columnData2)
        {
            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
            var columns = new Columns();
            var row = new Row();

            uint columnIndex = 0;
           
                columns.Append(new Column { CustomWidth = true, Width = new DoubleValue(columnData1.Width > 0 ? columnData1.Width : 20d), Min = columnIndex + 1, Max = columnIndex + 1 });
                row.AddCell(new OpenXmlData(columnData1.Name)
                {
                    Style = StyleTypeEnum.Text,
                    Type = CellValues.String
                });
                columnIndex++;
                columns.Append(new Column { CustomWidth = true, Min = columnIndex + 1, Max = columnIndex + 1 });
                row.AddCell(new OpenXmlData(columnData2.Value)
                {
                    Style = StyleTypeEnum.Currency,
                    Type = CellValues.String
                });
                columnIndex++;
            
            worksheet.PrependChild(columns);
            sheetData.Append(row);
            return row;
        }
        /// <summary>
        /// Adds the row.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="style">The style.</param>
        /// <returns>Row.</returns>
        public static Row AddRow(this Worksheet worksheet, StyleTypeEnum? style = null)
        {
            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
            var row = new Row();

            //if (style != null)
            //    row.StyleIndex = (uint)style;

            sheetData.Append(row);
            return row;
        }


        /// <summary>
        /// Adds the cell.
        /// </summary>
        /// <param name="row">The row.</param>
        /// <param name="data">The data.</param>
        public static void AddCell(this Row row, OpenXmlData data)
        {           
            Cell cell = new Cell();
            cell.CellValue = new CellValue(data.Value);
            if(data.Type.HasValue)
                cell.DataType = new EnumValue<CellValues>(data.Type);

            if (data.Style != null)
                cell.StyleIndex = (uint)data.Style;
            //else
            //    cell.StyleIndex = row.StyleIndex;
            row.Append(cell);
        }

        /// <summary>
        /// Adds the cell.
        /// </summary>
        /// <param name="row">The row.</param>
        /// <param name="value">The value.</param>
        public static void AddCell(this Row row, string value)
        {
            row.AddCell(new OpenXmlData(value ?? "") { Style =StyleTypeEnum.General, Type = CellValues.String });
        }

        /// <summary>
        /// Adds the text cell.
        /// </summary>
        /// <param name="row">The row.</param>
        /// <param name="value">The value.</param>
        public static void AddTextCell(this Row row, string value)
        {
            row.AddCell(new OpenXmlData(value ?? "") { Style = StyleTypeEnum.Text, Type = CellValues.String});
        }

        public static void AddTextInBoldCell(this Row row, string value)
        {
            row.AddCell(new OpenXmlData(value ?? "") { Style = StyleTypeEnum.TextInBoldBorderTopOnly, Type = CellValues.String });
        }

        public static void AddCurrencyInBoldCell(this Row row, decimal value)
        {
            row.AddCell(new OpenXmlData(value.ToString()) { Type = CellValues.Number, Style = StyleTypeEnum.CurrencyInBoldBorderTopOnly });
        }

        /// <summary>
        /// Adds the cell.
        /// </summary>
        /// <param name="row">The row.</param>
        /// <param name="value">The value.</param>
        public static void AddCell(this Row row, DateTime value)
        {
            row.AddCell(new OpenXmlData(value.ToOADate().ToString()) { Style = StyleTypeEnum.LongDate });
        }

        /// <summary>
        /// currency formatted
        /// </summary>
        /// <param name="row">The row.</param>
        /// <param name="value">The value.</param>
        public static void AddCell(this Row row, decimal value)
        {
            row.AddCell(new OpenXmlData(value.ToString()) { Type = CellValues.Number, Style = StyleTypeEnum.Currency });
        }

        /// <summary>
        /// Adds the cell.
        /// </summary>
        /// <param name="row">The row.</param>
        /// <param name="value">The value.</param>
        public static void AddCell(this Row row, int value)
        {
            row.AddCell(new OpenXmlData(value.ToString()) { Type = CellValues.Number,Style = StyleTypeEnum.General });
        }

        /// <summary>
        /// Adds the cell.
        /// </summary>
        /// <param name="row">The row.</param>
        /// <param name="value">The value.</param>
        public static void AddCell(this Row row, float value)
        {
            row.AddCell(new OpenXmlData(value.ToString()) { Type = CellValues.Number, Style = StyleTypeEnum.General });
        }

        /// <summary>
        /// Adds the cell with formula.
        /// </summary>
        /// <param name="row">The row.</param>
        /// <param name="formula">The formula.</param>
        public static void AddCellWithFormula(this Row row,string formula)
        {

            row.AddCellWithFormula(new OpenXmlData(formula) { Type = CellValues.Number, Style = StyleTypeEnum.Currency } );
        }

        /// <summary>
        /// Adds the cell with formula.
        /// </summary>
        /// <param name="row">The row.</param>
        /// <param name="data">The data.</param>
       public static void AddCellWithFormula(this Row row, OpenXmlData data)
       {
            Cell cell = new Cell();

            CellFormula formula = new CellFormula() { Text = data.Value };
            cell.Append(formula);

            if (data.Type.HasValue)
                cell.DataType = new EnumValue<CellValues>(data.Type);

            if (data.Style != null)
                cell.StyleIndex = (uint)data.Style;
            //else
            //    cell.StyleIndex = row.StyleIndex;
          
            row.Append(cell);
        }
      
    }
}
