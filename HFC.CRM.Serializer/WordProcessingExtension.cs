﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Serializer
{
    public static class WordProcessingExtension
    {
        public static void FillHeader(this MainDocumentPart mainPart, Franchise franchise)
        {
            if (mainPart.HeaderParts != null && mainPart.HeaderParts.Count() > 0)
            {
                foreach (var header in mainPart.HeaderParts.Select(s => s.Header))
                {
                    foreach (var run in header.Descendants<SdtRun>())
                    {
                        if (run.SdtProperties.GetFirstChild<SdtAlias>().Val.Value == "FranchiseName")
                        {
                            OpenXmlUtil.FillContentControl(run, franchise.Name);
                        }
                    }
                    foreach (var block in header.Descendants<SdtBlock>())
                    {
                        if (block.SdtProperties.GetFirstChild<SdtAlias>().Val.Value == "FranchiseName")
                        {
                            OpenXmlUtil.FillContentControl(block, franchise.Name);
                        }
                    }
                }
            }
        }

        public static void FillFooter(this MainDocumentPart mainPart, Franchise franchise)
        {
            if (mainPart.FooterParts != null && mainPart.FooterParts.Count() > 0 && franchise != null)
            {
                List<string> footerInfo = new List<string>();                
                if (franchise.Address != null && !string.IsNullOrEmpty(franchise.Address.ToString()))
                    footerInfo.Add(franchise.Address.ToString());
                if (!string.IsNullOrEmpty(franchise.TollFreeNumber))
                    footerInfo.Add(string.Format("P: {0}", RegexUtil.FormatUSPhone(franchise.TollFreeNumber)));
                if (!string.IsNullOrEmpty(franchise.FaxNumber))
                    footerInfo.Add(string.Format("F: {0}", RegexUtil.FormatUSPhone(franchise.FaxNumber)));

                foreach (var footer in mainPart.FooterParts.Select(s => s.Footer))
                {
                    foreach (var run in footer.Descendants<SdtRun>())
                    {
                        if (run.SdtProperties.GetFirstChild<SdtAlias>().Val.Value == "Contact")
                        {
                            OpenXmlUtil.FillContentControl(run, string.Join(" - ", footerInfo));
                        }
                        else if (run.SdtProperties.GetFirstChild<SdtAlias>().Val.Value == "Website")
                        {
                            if (!string.IsNullOrEmpty(franchise.Website))
                            {
                                HyperlinkRelationship relation = footer.FooterPart.AddHyperlinkRelationship(new Uri("http://" + franchise.Website, UriKind.RelativeOrAbsolute), true);
                                OpenXmlUtil.FillContentControl(run, franchise.Website, relation.Id);
                            }
                            else
                                OpenXmlUtil.FillContentControl(run, "");
                        }
                    }
                    foreach (var block in footer.Descendants<SdtBlock>())
                    {
                        if (block.SdtProperties.GetFirstChild<SdtAlias>().Val.Value == "Contact")
                        {
                            OpenXmlUtil.FillContentControl(block, string.Join(" - ", footerInfo));
                        }
                        else if (block.SdtProperties.GetFirstChild<SdtAlias>().Val.Value == "Website")
                        {
                            if (!string.IsNullOrEmpty(franchise.Website))
                            {
                                HyperlinkRelationship relation = footer.FooterPart.AddHyperlinkRelationship(new Uri("http://" + franchise.Website, UriKind.RelativeOrAbsolute), true);
                                OpenXmlUtil.FillContentControl(block, franchise.Website, relation.Id);
                            }
                            else
                                OpenXmlUtil.FillContentControl(block, "");
                        }
                    }

                }
            }
        }

    }
}
