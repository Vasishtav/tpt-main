﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// The Serializer namespace.
/// </summary>
namespace HFC.CRM.Serializer
{
    /// <summary>
    /// Enum StandardFormatCodes
    /// </summary>
    public enum StandardFormatCodes : uint
    {
        /// <summary>
        /// The general
        /// </summary>
        General = 0,
        /// <summary>
        /// 1 = 0, No decimal
        /// </summary>
        NumberWhole = 1,
        /// <summary>
        /// 2 = 0.00, a decimal number
        /// </summary>
        NumberDecimal = 2,
        /// <summary>
        /// 3 = #,##0, No decimal with comma
        /// </summary>
        NumberWholeWithComma = 3,
        /// <summary>
        /// 4 = #,##0.00, a decimal number with comma
        /// </summary>
        NumberDecimalWithComma = 4,
        /// <summary>
        /// 9 = 0%, percent without decimal
        /// </summary>
        PercentZeroDecimal = 9,
        /// <summary>
        /// 10 = 0.00%, percent with decimal
        /// </summary>
        PercentTwoDecimals = 10,
        /// <summary>
        /// 11 = 0.00E+00, Scientific number
        /// </summary>
        NumberScientific = 11,
        /// <summary>
        /// 12 = # ?/?, 1 digit Fraction
        /// </summary>
        FractionOneDigit = 12,
        /// <summary>
        /// 13 = # ??/??, 2 digits Fraction
        /// </summary>
        FractionTwoDigits = 13,
        /// <summary>
        /// 14 = mm-dd-yy date format
        /// </summary>
        DateMMDDYY = 14,
        /// <summary>
        /// 15 = d-mmm-yy date format
        /// </summary>
        DateDMMMYY = 15,
        /// <summary>
        /// 16 = d-mmm date format
        /// </summary>
        DateDMMM = 16,
        /// <summary>
        /// 17 = mmm-yy
        /// </summary>
        DateMMMYY = 17,
        /// <summary>
        /// 18 = h:mm AM/PM
        /// </summary>
        DateHMMTT = 18,
        /// <summary>
        /// 19 = h:mm:ss AM/PM
        /// </summary>
        DateHMMSSTT = 19,
        /// <summary>
        /// 20 = h:mm
        /// </summary>
        DateHMM = 20,
        /// <summary>
        /// 21 = h:mm:ss
        /// </summary>
        DateHMMSS = 21,
        /// <summary>
        /// 22 = m/d/yy h:mm
        /// </summary>
        DateMDYYHMM = 22,
        /// <summary>
        /// 37 = #,##0 ;(#,##0)
        /// </summary>
        CurrencyWhole = 37,
        /// <summary>
        /// 38 = #,##0 ;[Red](#,##0)
        /// </summary>
        CurrencyWholeRed = 38,
        /// <summary>
        /// 39 = #,##0.00;(#,##0.00)
        /// </summary>
        CurrencyDecimal = 39,
        /// <summary>
        /// 40 = #,##0.00;[Red](#,##0.00)
        /// </summary>
        [Description("#,##0.00;[Red](#,##0.00)")]
        CurrencyDecimalRed = 40,
        /// <summary>
        /// 45 = mm:ss
        /// </summary>
        DateMMSS = 45,
        /// <summary>
        /// 46 = [h]:mm:ss
        /// </summary>
        DateOptionalH_MMSS = 46,
        /// <summary>
        /// 47 = mmss.0
        /// </summary>
        DateMMSS0 = 47,
        /// <summary>
        /// 48 = ##0.0E+0
        /// </summary>
        NumberScientificLarge = 48,
        /// <summary>
        /// 49 = @
        /// </summary>
        AtSymbol = 49,


        //Custom format id start at 164 and increment with every new custom format
        /// <summary>
        /// 164 = dd/mm/yyyy hh:mm AM/PM
        /// </summary>
        [Description("MM/dd/yyyy hh:mm AM/PM")]
        DateMMDDYYYYHHMMTT = 164
    }

    /// <summary>
    /// Enum StyleTypeEnum
    /// </summary>
    public enum StyleTypeEnum : uint
    {
        /// <summary>
        /// The general
        /// </summary>
        General = 0,
        /// <summary>
        /// The text
        /// </summary>
        Text = 1,
        /// <summary>
        /// The currency
        /// </summary>
        Currency = 2,
        /// <summary>
        /// The long date
        /// </summary>
        LongDate = 3,
        /// <summary>
        /// The number two decimal
        /// </summary>
        NumberTwoDecimal = 4,
        /// <summary>
        /// The percent two decimal
        /// </summary>
        PercentTwoDecimal = 5,
        /// <summary>
        /// The header style
        /// </summary>
        HeaderStyle = 6,
       /// <summary>
       /// The text in bold
       /// </summary>
        TextInBold = 7,
        /// <summary>
        /// The text in bold and thick black bottom borderline in the cell
        /// </summary>
        TextInBoldBorderBottomOnly = 8,
        /// <summary>
        /// The text in bold and thick black top borderline in the cell
        /// </summary>
        TextInBoldBorderTopOnly = 9,
        /// <summary>
        ///  The currency in bold
        /// </summary>
        CurrencyInBoldBorderTopOnly = 10
    }
}
