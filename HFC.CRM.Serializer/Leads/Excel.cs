﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Validation;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using HFC.CRM.DTO.Search;


namespace HFC.CRM.Serializer.Leads
{
    /// <summary>
    /// Dependent on Session to convert dates to correct franchise local date
    /// </summary>
    public class Excel
    {
        /// <summary>
        ///  Exports the leads data into an Excel file
        /// </summary>
        /// <param name="outputStream"></param>
        /// <param name="leadCollection"></param>
        /// <returns></returns>
        public static MemoryStream BasicLeadInfo(List<LeadSeachResultModel> leadCollection, List<Person> secPersons)
        {
            MemoryStream outputStream = new MemoryStream();

            try
            {
                //create spreadsheet  -- create a spreadsheet decument from an IO stream
                SpreadsheetDocument spreadsheet = OpenXmlUtil.CreateWorkbook(outputStream);
                Worksheet worksheet = spreadsheet.AddWorksheet("Leads");
                spreadsheet.AddBasicStyles();


                //add header row
                var headRow = worksheet.AddHeaderRow(
                    new OpenXmlColumnData("Created Date"),
                    new OpenXmlColumnData("Lead Number"),
                    new OpenXmlColumnData("Lead Status"),
                    new OpenXmlColumnData("Address 1"),
                    new OpenXmlColumnData("Address 2"),
                    new OpenXmlColumnData("City"),
                    new OpenXmlColumnData("State"),
                    new OpenXmlColumnData("Zip"),
                    new OpenXmlColumnData("Prim First Name"),
                    new OpenXmlColumnData("Prim Last Name"),
                    new OpenXmlColumnData("Prim Cell"),
                    new OpenXmlColumnData("Prim Home"),
                    new OpenXmlColumnData("Prim Fax"),
                    new OpenXmlColumnData("Prim Work"),
                    new OpenXmlColumnData("Prim Email"),
                    new OpenXmlColumnData("Sec First Name"),
                    new OpenXmlColumnData("Sec Last Name"),
                    new OpenXmlColumnData("Sec Cell"),
                    new OpenXmlColumnData("Sec Home"),
                    new OpenXmlColumnData("Sec Fax"),
                    new OpenXmlColumnData("Sec Work"),
                    new OpenXmlColumnData("Sec Email"));
                worksheet.Save();



                //Add data
                foreach (var lead in leadCollection)
                {
                    var row = worksheet.AddRow();

                    if (SessionManager.CurrentFranchise != null && SessionManager.CurrentFranchise.FranchiseId > 0)
                        row.AddCell((DateTime) lead.CreatedOnUtc);
                    else
                        row.AddCell((DateTime) lead.CreatedOnUtc);

                    row.AddCell(lead.LeadNumber);
                    row.AddCell(GetLeadStatus(lead.LeadStatusId));

                    row.AddCell(lead.Address1);
                    row.AddCell(lead.Address2);
                    row.AddCell(lead.City);
                    row.AddCell(lead.State);
                    row.AddCell(lead.ZipCode);

                    row.AddCell(lead.FirstName);
                    row.AddCell(lead.LastName);
                    row.AddCell(RegexUtil.FormatUSPhone(lead.CellPhone));
                    row.AddCell(RegexUtil.FormatUSPhone(lead.HomePhone));
                    row.AddCell(RegexUtil.FormatUSPhone(lead.FaxPhone));
                    row.AddCell(RegexUtil.FormatUSPhone(lead.WorkPhone));
                    row.AddCell(lead.PrimaryEmail ?? "");

                    if (lead.SecPersonId != null)
                    {
                        var secP = secPersons.FirstOrDefault(v => v.PersonId == lead.SecPersonId);
                        if (secP != null)
                        {
                            row.AddCell(secP.FirstName);
                            row.AddCell(secP.LastName);
                            row.AddCell(RegexUtil.FormatUSPhone(secP.CellPhone));
                            row.AddCell(RegexUtil.FormatUSPhone(secP.HomePhone));
                            row.AddCell(RegexUtil.FormatUSPhone(secP.FaxPhone));
                            row.AddCell(RegexUtil.FormatUSPhone(secP.WorkPhone));
                            row.AddCell(secP.PrimaryEmail ?? "");
                        }
                        else
                        {
                            row.AddCell("");
                            row.AddCell("");
                            row.AddCell("");
                            row.AddCell("");
                            row.AddCell("");
                            row.AddCell("");
                            row.AddCell("");
                        }
                    }
                    else
                    {
                        row.AddCell("");
                        row.AddCell("");
                        row.AddCell("");
                        row.AddCell("");
                        row.AddCell("");
                        row.AddCell("");
                        row.AddCell("");
                    }


                }

                worksheet.Save();
#if DEBUG
                OpenXmlValidator validator = new OpenXmlValidator();
                var errors = validator.Validate(spreadsheet);
#endif
                spreadsheet.Close();
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
            }

            return outputStream;
        }

        private static string GetLeadStatus(short statusId)
        {
            var status = CacheManager.LeadStatusTypeCollection.FirstOrDefault(f => f.Id == statusId);
            if (status != null)
                return status.ToString();
            else
                return string.Empty;
        }
   
    }

}
