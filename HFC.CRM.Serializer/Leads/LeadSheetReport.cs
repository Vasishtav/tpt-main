﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Validation;
using DocumentFormat.OpenXml.Wordprocessing;
using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using DocumentFormat.OpenXml.Office2010.Word;
using HFC.CRM.Data.Context;
using System.Data.SqlClient;

using datacal = HFC.CRM.Data;

using RestSharp;
using HFC.CRM.DTO;
using Newtonsoft.Json;
using Dapper;
using System.Xml;
using System.Net;
using System.Globalization;
using System.Web;
using HFC.CRM.Core.Logs;
using HFC.CRM.DTO.Calandar;
using HFC.CRM.Core.Extensions;

namespace HFC.CRM.Serializer.Leads
{
    public class LeadSheetReport
    {
        static string error = @"An error occurred.If the problem persists, please contact your system administrator.";
        private static LeadsManager leadsMng = new LeadsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private static CalendarManager calendarMgr = new CalendarManager(SessionManager.CurrentUser);
        private static AccountsManager accountsMgr = new AccountsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private static OpportunitiesManager oppMgr = new OpportunitiesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private static NoteManager noteMgr = new NoteManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private static MeasurementManager measurementMgr = new MeasurementManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        #region Print Touch point

        public static string GetLeadCustomerContentForInstallationPacket(int id)
        {
            string data = string.Empty;
            try
            {
                //data = GetLeadCustomerContentHandler(id, printoptoins, sourceType);
                data = GetOpportunityInformation(id);
                return data;
            }
            catch (Exception ex)
            {
                data = LeadSheetReport.error;
                return data;
            }
        }

        public static string GetLeadCustomerContent(int id, List<string> printoptoins
           , string sourceType = "Lead")
        {
            string data = string.Empty;
            try
            {
                data = GetLeadCustomerContentHandler(id, printoptoins, sourceType);

                return data;
            }
            catch (Exception ex)
            {
                data = LeadSheetReport.error;
                return data;
            }
        }

        private static string GetTemplateName(string sourceType)
        {
            string path = string.Empty;

            if (sourceType == "Lead")
            {
                if (SessionManager.BrandId == 1)
                {
                    path = string.Format("/Templates/Base/Brand/BB/LeadSheet.html");
                }
                else if (SessionManager.BrandId == 2)
                {
                    path = string.Format("/Templates/Base/Brand/TL/LeadSheet.html");
                }
                else
                {
                    path = string.Format("/Templates/Base/Brand/CC/LeadSheet.html");
                }
            }
            else if (sourceType == "Account")
            {
                if (SessionManager.BrandId == 1)
                {
                    path = string.Format("/Templates/Base/Brand/BB/AccountSheet.html");
                }
                else if (SessionManager.BrandId == 2)
                {
                    path = string.Format("/Templates/Base/Brand/TL/AccountSheet.html");
                }
                else
                {
                    path = string.Format("/Templates/Base/Brand/CC/AccountSheet.html");
                }
            }
            else
            {
                if (SessionManager.BrandId == 1)
                {
                    path = string.Format("/Templates/Base/Brand/BB/OpportunitySheet.html");
                }
                else if (SessionManager.BrandId == 2)
                {
                    path = string.Format("/Templates/Base/Brand/TL/OpportunitySheet.html");
                }
                else
                {
                    path = string.Format("/Templates/Base/Brand/CC/OpportunitySheet.html");
                }
            }

            return path;
        }

        private static string UpdateQualifyBB(string data, List<Type_Question> leadquestionAns)
        {
            string BB_QQ_NewHome = ""; string BB_QQ_ExistingHome = "";
            string BB_QQ_EstimatedClosingDate = ""; string BB_QQ_ProjectLeadTimePreference = "";
            string BB_QQ_ValPak = ""; string BB_QQ_YellowPages = "";
            string BB_QQ_RSVP = ""; string BB_QQ_Van = ""; string BB_QQ_Internet = "";
            string BB_QQ_Sources1 = ""; string BB_QQ_Sources2 = "";
            string BB_QQ_Sources3 = ""; string BB_QQ_FaceBookPaid = "";
            string BB_QQ_Referel = ""; string BB_QQ_Budget = "";
            string BB_QQ_AnyAppointment = ""; string BB_QQ_ReceivedOtherQuotesYes = "";
            string BB_QQ_ReceivedOtherQuotesNo = ""; string BB_QQ_ReceivedOtherQuotesSpecific = "";

            //BB - Project Needs
            string BB_PN_HowManyWindows = "";
            string BB_PN_Color = "";
            string BB_PN_Room_Living = ""; string BB_PN_Room_Dining = "";
            string BB_PN_Room_Family = ""; string BB_PN_Room_BedRooms = "";
            string BB_PN_Room_Kitchen = ""; string BB_PN_Room_Bath = "";
            string BB_PN_Room_Other = ""; string BB_PN_Room_OtherNote = "";
            string BB_PN_treatment_Wood = ""; string BB_PN_treatment_FauxWood = "";
            string BB_PN_treatment_Cellular = ""; string BB_PN_treatment_Shades = "";
            string BB_PN_treatment_RollerShades = ""; string BB_PN_treatment_Draperies = "";
            string BB_PN_treatment_Romans = ""; string BB_PN_treatment_Verticals = "";
            string BB_PN_treatment_Shutters = ""; string BB_PN_treatment_WovenWoods = "";
            string BB_PN_treatment_SolarShades = ""; string BB_PN_Drapery_Operational = "";
            string BB_PN_Drapery_Decorative = ""; string BB_PN_FrabricPreferences = "";
            string BB_PN_SolidSOrPrints = ""; string BB_PN_PreferredStyle = "";
            string BB_PN_Notes; string BB_PN_AdditionalInfo = "";
            string BB_PN_Need_Idea = "";
            string BB_QQ_tallladderYes = "", BB_QQ_tallladderNo = "", BB_QQ_tallladderSpecific = "";
            string BB_QQ_WiFiconnectionYes = "", BB_QQ_WiFiconnectionNo = "";

            //var leadquestionAns = lead.LeadQuestionAns;
            if (leadquestionAns != null)
            {
                foreach (var leadqa in leadquestionAns)
                {
                    #region QualificationQuestions

                    if (leadqa.QuestionId == 1001 && leadqa.Answer != null && leadqa.Answer != "")
                    {
                        if (leadqa.Answer == "New Home?")
                        {
                            BB_QQ_NewHome = "Checked";
                            BB_QQ_ExistingHome = "";
                        }
                        else if (leadqa.Answer == "Existing Home?")
                        {
                            BB_QQ_NewHome = "";
                            BB_QQ_ExistingHome = "Checked";
                        }
                    }
                    if (leadqa.QuestionId == 1003 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        BB_QQ_EstimatedClosingDate = GetFormatDate(leadqa.Answer);
                    }
                    if (leadqa.QuestionId == 1004 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        BB_QQ_ProjectLeadTimePreference = GetFormatDate(leadqa.Answer);
                    }

                    #endregion QualificationQuestions

                    #region BB

                    //How did u hear about BB
                    if (leadqa.QuestionId == 1005 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        if (leadqa.Answer == "false")
                        {
                            BB_QQ_ValPak = "";
                        }
                        else
                        {
                            BB_QQ_ValPak = "Checked";
                        }
                    }
                    if (leadqa.QuestionId == 1006 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        if (leadqa.Answer == "false")
                        {
                            BB_QQ_YellowPages = "";
                        }
                        else
                        {
                            BB_QQ_YellowPages = "Checked";
                        }
                    }
                    if (leadqa.QuestionId == 1007 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        if (leadqa.Answer == "false")
                        {
                            BB_QQ_RSVP = "";
                        }
                        else
                        {
                            BB_QQ_RSVP = "Checked";
                        }
                    }
                    if (leadqa.QuestionId == 1008 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        if (leadqa.Answer == "false")
                        {
                            BB_QQ_Van = "";
                        }
                        else
                        {
                            BB_QQ_Van = "Checked";
                        }
                    }
                    if (leadqa.QuestionId == 1009 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        if (leadqa.Answer == "false")
                        {
                            BB_QQ_Internet = "";
                        }
                        else
                        {
                            BB_QQ_Internet = "Checked";
                        }
                    }
                    if (leadqa.QuestionId == 1010 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        if (leadqa.Answer == "false")
                        {
                            BB_QQ_Sources1 = "";
                        }
                        else
                        {
                            BB_QQ_Sources1 = "Checked";
                        }
                    }
                    if (leadqa.QuestionId == 1011 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        if (leadqa.Answer == "false")
                        {
                            BB_QQ_Sources2 = "";
                        }
                        else
                        {
                            BB_QQ_Sources2 = "Checked";
                        }
                    }
                    if (leadqa.QuestionId == 1012 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        if (leadqa.Answer == "false")
                        {
                            BB_QQ_Sources3 = "";
                        }
                        else
                        {
                            BB_QQ_Sources3 = "Checked";
                        }
                    }
                    if (leadqa.QuestionId == 1013 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        if (leadqa.Answer == "false")
                        {
                            BB_QQ_FaceBookPaid = "";
                        }
                        else
                        {
                            BB_QQ_FaceBookPaid = "Checked";
                        }
                    }
                    if (leadqa.QuestionId == 1014 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        BB_QQ_Referel = leadqa.Answer;
                    }
                    if (leadqa.QuestionId == 1132 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        BB_QQ_Budget = leadqa.Answer;
                    }
                    if (leadqa.QuestionId == 1016 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        BB_QQ_AnyAppointment = leadqa.Answer;
                    }
                    if (leadqa.QuestionId == 1017 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        if (leadqa.Answer == "No")
                        {
                            BB_QQ_ReceivedOtherQuotesNo = "Checked";
                        }
                        else
                        {
                            BB_QQ_ReceivedOtherQuotesYes = "Checked";
                        }
                    }
                    //if (leadqa.QuestionId == 1018 && (leadqa.Answer != null && leadqa.Answer != ""))
                    //{
                    //    BB_QQ_ReceivedOtherQuotesNo = "Checked";
                    //}
                    if (leadqa.QuestionId == 1019 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        BB_QQ_ReceivedOtherQuotesSpecific = leadqa.Answer;
                    }

                    #endregion BB

                    #region ProjectNeeeds

                    if (leadqa.QuestionId == 1021 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        BB_PN_HowManyWindows = leadqa.Answer;
                    }
                    if (leadqa.QuestionId == 1022 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        BB_PN_Color = leadqa.Answer;
                    }
                    if (leadqa.QuestionId == 1023 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        if (leadqa.Answer == "false")
                        {
                            BB_PN_Room_Living = "";
                        }
                        else
                        {
                            BB_PN_Room_Living = "Checked";
                        }
                    }
                    if (leadqa.QuestionId == 1024 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        if (leadqa.Answer == "false")
                        {
                            BB_PN_Room_Dining = "";
                        }
                        else
                        {
                            BB_PN_Room_Dining = "Checked";
                        }
                    }
                    if (leadqa.QuestionId == 1025 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        if (leadqa.Answer == "false")
                        {
                            BB_PN_Room_Family = "";
                        }
                        else
                        {
                            BB_PN_Room_Family = "Checked";
                        }
                    }
                    if (leadqa.QuestionId == 1026 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        if (leadqa.Answer == "false")
                        {
                            BB_PN_Room_BedRooms = "";
                        }
                        else
                        {
                            BB_PN_Room_BedRooms = "Checked";
                        }
                    }
                    if (leadqa.QuestionId == 1027 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        if (leadqa.Answer == "false")
                        {
                            BB_PN_Room_Kitchen = "";
                        }
                        else
                        {
                            BB_PN_Room_Kitchen = "Checked";
                        }
                    }
                    if (leadqa.QuestionId == 1028 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        if (leadqa.Answer == "false")
                        {
                            BB_PN_Room_Bath = "";
                        }
                        else
                        {
                            BB_PN_Room_Bath = "Checked";
                        }
                    }
                    if (leadqa.QuestionId == 1029 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        if (leadqa.Answer == "false")
                        {
                            BB_PN_Room_Other = "";
                        }
                        else
                        {
                            BB_PN_Room_Other = "Checked";
                        }
                    }
                    if (leadqa.QuestionId == 1030 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        BB_PN_Room_OtherNote = leadqa.Answer;
                    }

                    #endregion ProjectNeeeds

                    #region ProjectNeeeds Treatments

                    if (leadqa.QuestionId == 1031 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        if (leadqa.Answer == "false")
                        {
                            BB_PN_treatment_Wood = "";
                        }
                        else
                        {
                            BB_PN_treatment_Wood = "Checked";
                        }
                    }
                    if (leadqa.QuestionId == 1032 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        if (leadqa.Answer == "false")
                        {
                            BB_PN_treatment_FauxWood = "";
                        }
                        else
                        {
                            BB_PN_treatment_FauxWood = "Checked";
                        }
                    }
                    if (leadqa.QuestionId == 1033 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        if (leadqa.Answer == "false")
                        {
                            BB_PN_treatment_Cellular = "";
                        }
                        else
                        {
                            BB_PN_treatment_Cellular = "Checked";
                        }
                    }
                    if (leadqa.QuestionId == 1034 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        if (leadqa.Answer == "false")
                        {
                            BB_PN_treatment_Shades = "";
                        }
                        else
                        {
                            BB_PN_treatment_Shades = "Checked";
                        }
                    }
                    if (leadqa.QuestionId == 1035 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        if (leadqa.Answer == "false")
                        {
                            BB_PN_treatment_RollerShades = "";
                        }
                        else
                        {
                            BB_PN_treatment_RollerShades = "Checked";
                        }
                    }
                    if (leadqa.QuestionId == 1036 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        if (leadqa.Answer == "false")
                        {
                            BB_PN_treatment_Draperies = "";
                        }
                        else
                        {
                            BB_PN_treatment_Draperies = "Checked";
                        }
                    }
                    if (leadqa.QuestionId == 1037 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        if (leadqa.Answer == "false")
                        {
                            BB_PN_treatment_Romans = "";
                        }
                        else
                        {
                            BB_PN_treatment_Romans = "Checked";
                        }
                    }
                    if (leadqa.QuestionId == 1038 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        if (leadqa.Answer == "false")
                        {
                            BB_PN_treatment_Verticals = "";
                        }
                        else
                        {
                            BB_PN_treatment_Verticals = "Checked";
                        }
                    }
                    if (leadqa.QuestionId == 1039 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        if (leadqa.Answer == "false")
                        {
                            BB_PN_treatment_Shutters = "";
                        }
                        else
                        {
                            BB_PN_treatment_Shutters = "Checked";
                        }
                    }
                    if (leadqa.QuestionId == 1040 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        if (leadqa.Answer == "false")
                        {
                            BB_PN_treatment_WovenWoods = "";
                        }
                        else
                        {
                            BB_PN_treatment_WovenWoods = "Checked";
                        }
                    }
                    if (leadqa.QuestionId == 1041 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        if (leadqa.Answer == "false")
                        {
                            BB_PN_treatment_SolarShades = "";
                        }
                        else
                        {
                            BB_PN_treatment_SolarShades = "Checked";
                        }
                    }
                    if (leadqa.QuestionId == 1140 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        if (leadqa.Answer == "false")
                        {
                            BB_PN_Need_Idea = "";
                        }
                        else
                        {
                            BB_PN_Need_Idea = "Checked";
                        }
                    }

                    #endregion ProjectNeeeds Treatments

                    #region Tall ladder & WIFI connection
                    if (leadqa.QuestionId == 1141 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        if (leadqa.Answer == "No")
                            BB_QQ_tallladderNo = "Checked";
                        else
                            BB_QQ_tallladderYes = "Checked";
                    }

                    if (leadqa.QuestionId == 1142 && (leadqa.Answer != null && leadqa.Answer != ""))
                        BB_QQ_tallladderSpecific = leadqa.Answer;

                    if (leadqa.QuestionId == 1143 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        if (leadqa.Answer == "No")
                            BB_QQ_WiFiconnectionNo = "Checked";
                        else
                            BB_QQ_WiFiconnectionYes = "Checked";
                    }
                    #endregion

                    #region ProjectNeeeds OperationOrDecorative

                    if (leadqa.QuestionId == 1042 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        if (leadqa.Answer == "Operational")
                        {
                            BB_PN_Drapery_Operational = "Checked";
                        }
                        else if (leadqa.Answer == "Decorative")
                        {
                            BB_PN_Drapery_Decorative = "Checked";
                        }
                    }
                    if (leadqa.QuestionId == 1044 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        BB_PN_FrabricPreferences = leadqa.Answer;
                    }
                    if (leadqa.QuestionId == 1045 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        BB_PN_SolidSOrPrints = leadqa.Answer;
                    }
                    if (leadqa.QuestionId == 1046 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        BB_PN_PreferredStyle = leadqa.Answer;
                    }
                    if (leadqa.QuestionId == 1137 && (leadqa.Answer != null && leadqa.Answer != ""))
                    {
                        BB_PN_AdditionalInfo = leadqa.Answer;
                    }

                    #endregion ProjectNeeeds OperationOrDecorative
                }

                if (BB_QQ_NewHome != null)
                {
                    data = data.Replace("{{BB_QQ_NewHome}}", BB_QQ_NewHome);
                    data = data.Replace("{{BB_QQ_ExistingHome}}", BB_QQ_ExistingHome);
                }

                if (BB_QQ_EstimatedClosingDate != null)
                {
                    data = data.Replace("{{BB_QQ_EstimatedClosingDate}}", BB_QQ_EstimatedClosingDate);
                }
                data = data.Replace("{{BB_QQ_ProjectLeadTimePreference}}", BB_QQ_ProjectLeadTimePreference);

                data = data.Replace("{{BB_QQ_ProjectLeadTimePreference}}", BB_QQ_ProjectLeadTimePreference);
                data = data.Replace("{{ChkValpak}}", BB_QQ_ValPak);
                data = data.Replace("{{ChkYellowPages}}", BB_QQ_YellowPages);
                data = data.Replace("{{ChkRSVP}}", BB_QQ_RSVP);
                data = data.Replace("{{ChkVan}}", BB_QQ_Van);
                data = data.Replace("{{ChkInternet}}", BB_QQ_Internet);
                data = data.Replace("{{ChkSources1}}", BB_QQ_Sources1);
                data = data.Replace("{{ChkSources2}}", BB_QQ_Sources2);
                data = data.Replace("{{ChkSources3}}", BB_QQ_Sources3);
                data = data.Replace("{{ChkFacebookPaid}}", BB_QQ_FaceBookPaid);
                data = data.Replace("{{BB_QQ_Referel}}", BB_QQ_Referel);
                data = data.Replace("{{BB_QQ_Budget}}", BB_QQ_Budget);
                data = data.Replace("{{BB_QQ_AnyAppointment}}", BB_QQ_AnyAppointment);

                data = data.Replace("{{BB_QQ_ReceivedOtherQuotesYes}}", BB_QQ_ReceivedOtherQuotesYes);
                data = data.Replace("{{BB_QQ_ReceivedOtherQuotesNo}}", BB_QQ_ReceivedOtherQuotesNo);
                data = data.Replace("{{BB_QQ_ReceivedOtherQuotesSpecific}}", BB_QQ_ReceivedOtherQuotesSpecific);

                data = data.Replace("{{BB_PN_HowManyWindows}}", BB_PN_HowManyWindows);
                data = data.Replace("{{BB_PN_Color}}", BB_PN_Color);
                data = data.Replace("{{ChkLiving}}", BB_PN_Room_Living);
                data = data.Replace("{{ChkDining}}", BB_PN_Room_Dining);
                data = data.Replace("{{ChkFamily}}", BB_PN_Room_Family);
                data = data.Replace("{{ChkBed}}", BB_PN_Room_BedRooms);
                data = data.Replace("{{ChkKitchen}}", BB_PN_Room_Kitchen);
                data = data.Replace("{{ChkBath}}", BB_PN_Room_Bath);
                data = data.Replace("{{ChkOther}}", BB_PN_Room_Other);
                data = data.Replace("{{BB_PN_Room_OtherNote}}", BB_PN_Room_OtherNote);

                data = data.Replace("{{ChkWood}}", BB_PN_treatment_Wood);
                data = data.Replace("{{ChkFauxWood}}", BB_PN_treatment_FauxWood);
                data = data.Replace("{{ChkCellular}}", BB_PN_treatment_Cellular);
                data = data.Replace("{{ChkShades}}", BB_PN_treatment_Shades);
                data = data.Replace("{{ChkRollarShades}}", BB_PN_treatment_RollerShades);
                data = data.Replace("{{ChkDraperies}}", BB_PN_treatment_Draperies);
                data = data.Replace("{{ChkRomans}}", BB_PN_treatment_Romans);
                data = data.Replace("{{ChkVerticals}}", BB_PN_treatment_Verticals);
                data = data.Replace("{{ChkShutters}}", BB_PN_treatment_Shutters);
                data = data.Replace("{{ChkWovenWoods}}", BB_PN_treatment_WovenWoods);
                data = data.Replace("{{ChkSolarShades}}", BB_PN_treatment_SolarShades);
                data = data.Replace("{{ChkNeedIdeas}}", BB_PN_Need_Idea);

                data = data.Replace("{{BB_QQ_tallladderYes}}", BB_QQ_tallladderYes);
                data = data.Replace("{{BB_QQ_tallladderNo}}", BB_QQ_tallladderNo);
                data = data.Replace("{{BB_QQ_tallladderSpecific}}", BB_QQ_tallladderSpecific);
                data = data.Replace("{{BB_QQ_WiFiconnectionYes}}", BB_QQ_WiFiconnectionYes);
                data = data.Replace("{{BB_QQ_WiFiconnectionNo}}", BB_QQ_WiFiconnectionNo);

                data = data.Replace("{{OptOpetational}}", BB_PN_Drapery_Operational);
                data = data.Replace("{{OptDecorative}}", BB_PN_Drapery_Decorative);

                data = data.Replace("{{BB_PN_FrabricPreferences}}", BB_PN_FrabricPreferences);
                data = data.Replace("{{BB_PN_SolidSOrPrints}}", BB_PN_SolidSOrPrints);
                data = data.Replace("{{BB_PN_PreferredStyle}}", BB_PN_PreferredStyle);
                data = data.Replace("{{BB_PN_AdditionalInfo}}", BB_PN_AdditionalInfo);
            }

            return data;
        }

        private static string UpdateQualifyTL(string data, List<Type_Question> leadquestionAns)
        {
            //BB - Qualification Questions
            string TL_QQ_NewHome = ""; string TL_QQ_ExistingHome = ""; string TL_QQ_EstimatedClosingDate = ""; string TL_QQ_ProjectLeadTimePreference = "";
            string TL_QQ_ValPak = ""; string TL_QQ_YellowPages = ""; string TL_QQ_RSVP = ""; string TL_QQ_Van = ""; string TL_QQ_Internet = "";
            string TL_QQ_Sources1 = ""; string TL_QQ_Sources2 = ""; string TL_QQ_Sources3 = ""; string TL_QQ_FaceBookPaid = ""; string TL_QQ_Referel = "";
            string TL_QQ_Budget = "";
            string TL_QQ_AnyAppointment = ""; string TL_QQ_ReceivedOtherQuotesYes = ""; string TL_QQ_ReceivedOtherQuotesNo = ""; string TL_QQ_ReceivedOtherQuotesSpecific = "";

            //BB - Project Needs
            string TL_GarageCabinetry = "";
            string TL_GarageFlooring = ""; string TL_HomeOffice = ""; string TL_Closet = ""; string TL_Laundry = "";
            string TL_Pantry = ""; string TL_Other = ""; string TL_Other_text = "";
            string TL_ProjectDetails = "";
            string TL_PastGarageCabinetry = ""; string TL_PastHomeOffice = ""; string TL_PastCloset = ""; string TL_PastSpecifics = "";
            string TL_WorkStations = ""; string TL_Drawers = ""; string TL_Workbench = ""; string TL_Quantity = ""; string TL_NeedsQuantity = "";
            string TL_OtherNeedsFlooring = ""; string TL_OtherNeedsLaundry = ""; string TL_OtherNeedsSpecifics = "";
            string TL_TaskLightingYes = ""; string TL_TaskLightingNo = ""; string TL_TaskLightingSpecifics = "";
            string TL_Jewellery = ""; string TL_Filing = ""; string TL_OtherDrawersSpecifics = "";
            string TL_FinishPrefernceSpecifics = "";
            string TL_PN_Notes; string Additional_Info = "";

            foreach (var leadqa in leadquestionAns)
            {
                #region QualificationQuestions

                if (leadqa.QuestionId == 1047 && leadqa.Answer != null)
                {
                    if (leadqa.Answer == "New Home?")
                    {
                        TL_QQ_NewHome = "Checked";
                        TL_QQ_ExistingHome = "";
                    }
                    else if (leadqa.Answer == "Existing Home?")
                    {
                        TL_QQ_NewHome = "";
                        TL_QQ_ExistingHome = "Checked";
                    }
                }
                if (leadqa.QuestionId == 1049 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_EstimatedClosingDate = leadqa.Answer;
                }
                //else
                //{
                //    TL_QQ_EstimatedClosingDate = " ";
                //}
                if (leadqa.QuestionId == 1050 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_ProjectLeadTimePreference = leadqa.Answer;
                }

                #endregion QualificationQuestions

                #region Tailored Living

                //How did u hear about BB
                if (leadqa.QuestionId == 1051 && (leadqa.Answer != null || leadqa.Answer != ""))
                {
                    TL_QQ_ValPak = "Checked";
                }
                if (leadqa.QuestionId == 1052 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_YellowPages = "Checked";
                }
                if (leadqa.QuestionId == 1053 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_RSVP = "Checked";
                }
                if (leadqa.QuestionId == 1054 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_Van = "Checked";
                }
                if (leadqa.QuestionId == 1055 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_Internet = "Checked";
                }
                if (leadqa.QuestionId == 1056 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_Sources1 = "Checked";
                }
                if (leadqa.QuestionId == 1057 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_Sources2 = "Checked";
                }
                if (leadqa.QuestionId == 1058 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_Sources3 = "Checked";
                }
                if (leadqa.QuestionId == 1059 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_FaceBookPaid = "Checked";
                }

                if (leadqa.QuestionId == 1060 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_Referel = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1061 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_Budget = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1062 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_AnyAppointment = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1063 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    if (leadqa.Answer == "Yes")
                    {
                        TL_QQ_ReceivedOtherQuotesYes = "Checked";
                        TL_QQ_ReceivedOtherQuotesNo = "";
                    }
                    else if (leadqa.Answer == "No")
                    {
                        TL_QQ_ReceivedOtherQuotesYes = "";
                        TL_QQ_ReceivedOtherQuotesNo = "Checked";
                    }
                }
                if (leadqa.QuestionId == 1065 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_ReceivedOtherQuotesSpecific = leadqa.Answer;
                }

                #endregion Tailored Living

                #region DetailsInformation

                if (leadqa.QuestionId == 1068 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    if (leadqa.Answer == "true")
                        TL_GarageCabinetry = "Checked";
                    else
                        TL_GarageCabinetry = "";
                }
                if (leadqa.QuestionId == 1069 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    if (leadqa.Answer == "true")
                        TL_GarageFlooring = "Checked";
                    else TL_GarageFlooring = "";
                }
                if (leadqa.QuestionId == 1070 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    if (leadqa.Answer == "true")
                        TL_HomeOffice = "Checked";
                    else TL_HomeOffice = "";
                }
                if (leadqa.QuestionId == 1071 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    if (leadqa.Answer == "true")
                        TL_Closet = "Checked";
                    else TL_Closet = "";
                }
                if (leadqa.QuestionId == 1072 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    if (leadqa.Answer == "true")
                        TL_Laundry = "Checked";
                    else
                        TL_Laundry = "";
                }
                if (leadqa.QuestionId == 1073 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    if (leadqa.Answer == "true")
                        TL_Pantry = "Checked";
                    else
                        TL_Pantry = "";
                }
                if (leadqa.QuestionId == 1074 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    //TL_Other = leadqa.Answer;
                    if (leadqa.Answer == "true")
                        TL_Other = "Checked";
                    else
                        TL_Other = "";
                }

                if (leadqa.QuestionId == 1075 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    //TL_Other = leadqa.Answer;
                    TL_Other_text = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1076 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_ProjectDetails = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1138 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    Additional_Info = leadqa.Answer;
                }
                //System Installed in the past
                if (leadqa.QuestionId == 1077 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_PastSpecifics = leadqa.Answer;
                }

                if (leadqa.QuestionId == 1125 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_PastGarageCabinetry = "Checked";
                }

                if (leadqa.QuestionId == 1126 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_PastHomeOffice = "Checked";
                }
                if (leadqa.QuestionId == 1127 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_PastCloset = "Checked";
                }

                //if (leadqa.QuestionId == 11 && (leadqa.Answer != null && leadqa.Answer != ""))
                //{
                //    TL_PastSpecifics = leadqa.Answer;
                //}

                if (leadqa.QuestionId == 1128 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_WorkStations = "Checked";
                }
                if (leadqa.QuestionId == 1129 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_Drawers = "Checked";
                }
                if (leadqa.QuestionId == 1130 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_Workbench = "Checked";
                }
                if (leadqa.QuestionId == 1131 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_Quantity = "Checked";
                }
                if (leadqa.QuestionId == 1131 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_NeedsQuantity = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1079 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_OtherNeedsFlooring = "Checked";
                }
                if (leadqa.QuestionId == 1080 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_OtherNeedsLaundry = "Checked";
                }
                if (leadqa.QuestionId == 1081 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_OtherNeedsSpecifics = leadqa.Answer;
                }

                if (leadqa.QuestionId == 1083 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    if (leadqa.Answer == "Yes")
                    {
                        TL_TaskLightingYes = "Checked";
                        TL_TaskLightingNo = "";
                    }
                    else if (leadqa.Answer == "No")
                    {
                        TL_TaskLightingYes = "";
                        TL_TaskLightingNo = "Checked";
                    }
                }
                if (leadqa.QuestionId == 1085 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_TaskLightingSpecifics = leadqa.Answer;
                }

                if (leadqa.QuestionId == 1086 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_Jewellery = "Checked";
                }
                if (leadqa.QuestionId == 1087 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_Filing = "Checked";
                }
                if (leadqa.QuestionId == 1088 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_OtherDrawersSpecifics = leadqa.Answer;
                }

                if (leadqa.QuestionId == 1089 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_FinishPrefernceSpecifics = leadqa.Answer;
                }

                #endregion DetailsInformation
            }

            if (TL_QQ_NewHome != null)
            {
                data = data.Replace("{{TL_QQ_NewHome}}", TL_QQ_NewHome);
                data = data.Replace("{{TL_QQ_ExistingHome}}", TL_QQ_ExistingHome);
            }

            if (TL_QQ_EstimatedClosingDate != null)
            {
                //{ { TL_QQ_EstimatedClosingDate} }
                //data = data.Replace("{{TL_QQ_EstimatedClosingDate}}", TL_QQ_EstimatedClosingDate);
                data = data.Replace("{{TL_QQ_EstimatedClosingDate}}", TL_QQ_EstimatedClosingDate);
            }
            data = data.Replace("{{TL_QQ_ProjectLeadTimePreference}}", TL_QQ_ProjectLeadTimePreference);

            data = data.Replace("{{TL_QQ_ProjectLeadTimePreference}}", TL_QQ_ProjectLeadTimePreference);
            data = data.Replace("{{ChkValpak}}", TL_QQ_ValPak);
            data = data.Replace("{{ChkYellowPages}}", TL_QQ_YellowPages);
            data = data.Replace("{{ChkRSVP}}", TL_QQ_RSVP);
            data = data.Replace("{{ChkVan}}", TL_QQ_Van);
            data = data.Replace("{{ChkInternet}}", TL_QQ_Internet);
            data = data.Replace("{{ChkSources1}}", TL_QQ_Sources1);
            data = data.Replace("{{ChkSources2}}", TL_QQ_Sources2);
            data = data.Replace("{{ChkSources3}}", TL_QQ_Sources3);

            data = data.Replace("{{TL_QQ_Referel}}", TL_QQ_Referel);
            data = data.Replace("{{TL_QQ_Budget}}", TL_QQ_Budget);
            data = data.Replace("{{TL_QQ_AnyAppointment}}", TL_QQ_AnyAppointment);

            data = data.Replace("{{TL_QQ_ReceivedOtherQuotesYes}}", TL_QQ_ReceivedOtherQuotesYes);
            data = data.Replace("{{TL_QQ_ReceivedOtherQuotesNo}}", TL_QQ_ReceivedOtherQuotesNo);
            data = data.Replace("{{TL_QQ_ReceivedOtherQuotesSpecific}}", TL_QQ_ReceivedOtherQuotesSpecific);

            data = data.Replace("{{ChkGarageCabinetry}}", TL_GarageCabinetry);
            data = data.Replace("{{ChkGarageFlooring}}", TL_GarageFlooring);
            data = data.Replace("{{ChkHomeOffice}}", TL_HomeOffice);
            data = data.Replace("{{ChkCloset}}", TL_Closet);
            data = data.Replace("{{ChkLaundry}}", TL_Laundry);
            data = data.Replace("{{ChkPantry}}", TL_Pantry);
            data = data.Replace("{{ChkOther}}", TL_Other);
            data = data.Replace("{{TL_Other_text}}", TL_Other_text);
            //data = data.Replace("{{ChkOther}}", TL_Other); textbox

            data = data.Replace("{{TL_ProjectDetails}}", TL_ProjectDetails);
            data = data.Replace("{{Additional_Info}}", Additional_Info);

            data = data.Replace("{{ChkPastGarageCabinetry}}", TL_PastGarageCabinetry);
            data = data.Replace("{{ChkPastHomeOffice}}", TL_PastHomeOffice);
            data = data.Replace("{{ChkPastCloset}}", TL_PastCloset);
            data = data.Replace("{{PastSpecifics}}", TL_PastSpecifics);

            data = data.Replace("{{ChkWorkStations}}", TL_WorkStations);
            data = data.Replace("{{ChkWorkbench}}", TL_Workbench);
            data = data.Replace("{{ChkDrawers}}", TL_Drawers);
            data = data.Replace("{{NeedsQuantity}}", TL_NeedsQuantity);

            data = data.Replace("{{ChkOtherNeedsFlooring}}", TL_OtherNeedsFlooring);
            data = data.Replace("{{ChkOtherNeedsLaundry}}", TL_OtherNeedsLaundry);
            data = data.Replace("{{OtherNeedsSpecifics}}", TL_OtherNeedsSpecifics);

            data = data.Replace("{{ChkTaskLightingYes}}", TL_TaskLightingYes);
            data = data.Replace("{{ChkTaskLightingNo}}", TL_TaskLightingNo);
            data = data.Replace("{{TaskLightingSpecifics}}", TL_TaskLightingSpecifics);

            data = data.Replace("{{ChkJewellery}}", TL_Jewellery);
            data = data.Replace("{{ChkFiling}}", TL_Filing);
            data = data.Replace("{{OtherDrawerSpecifics}}", TL_OtherDrawersSpecifics);

            data = data.Replace("{{FinishPreferenceSpecifics}}", TL_FinishPrefernceSpecifics);

            // TODO: need to fetch data from the qualify questions.....
            data = data.Replace("{{TL_PN_FrabricPreferences}}", "");
            data = data.Replace("{{TL_PN_SolidSOrPrints}}", "");
            data = data.Replace("{{TL_PN_PreferredStyle}}", "");
            data = data.Replace("{{TL_PN_HowManyWindows}}", "");
            data = data.Replace("{{TL_PN_Room_OtherNote}}", "");

            return data;
        }

        private static string UpdateQualifyCC(string data, List<Type_Question> leadquestionAns)
        {
            //CC - Qualification Questions
            string CC_QQ_NewHome = ""; string CC_QQ_ExistingHome = ""; string CC_QQ_EstimatedClosingDate = ""; string CC_QQ_ProjectLeadTimePreference = "";
            string CC_QQ_ValPak = ""; string CC_QQ_YellowPages = ""; string CC_QQ_RSVP = ""; string CC_QQ_Van = ""; string CC_QQ_Internet = "";
            string CC_QQ_Sources1 = ""; string CC_QQ_Sources2 = ""; string CC_QQ_Sources3 = ""; string CC_QQ_FaceBookPaid = ""; string CC_QQ_Referel = "";
            string CC_QQ_Budget = "";
            string CC_QQ_AnyAppointment = ""; string CC_QQ_ReceivedOtherQuotesYes = ""; string CC_QQ_ReceivedOtherQuotesNo = ""; string CC_QQ_ReceivedOtherQuotesSpecific = "";
            //CC - Project Needs
            string CC_NewPour = "";
            string CC_StampedConcrete = ""; string CC_ResurfaceConcrete = ""; string CC_StainedConcrete = ""; string CC_ColorRestoreSystem = "";
            string CC_ColorSealSystem = ""; string CC_PlainConcrete = ""; string CC_CleanAndReseal = ""; string CC_ColoredConcrete = "";
            string CC_ProjectDetails = "";
            string CC_ExistingSlabYes = ""; string CC_ExistingSlabNo = ""; string CC_AprroximateSizeOrShape = "";
            string CC_OntheSurface = "";
            string CC_PN_Notes;
            string CC_Additional_Info = ""; string CC_Project_Details = "";

            foreach (var leadqa in leadquestionAns)
            {
                #region QualificationQuestions

                if (leadqa.QuestionId == 1090 && leadqa.Answer != null)
                {
                    if (leadqa.Answer == "New Home?")
                    {
                        CC_QQ_NewHome = "Checked";
                        CC_QQ_ExistingHome = "";
                    }
                    else if (leadqa.Answer == "Existing Home?")
                    {
                        CC_QQ_NewHome = "";
                        CC_QQ_ExistingHome = "Checked";
                    }
                }
                if (leadqa.QuestionId == 1092 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_EstimatedClosingDate = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1093 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_ProjectLeadTimePreference = leadqa.Answer;
                }

                #endregion QualificationQuestions

                #region Tailored Living

                //How did u hear about CC
                if (leadqa.QuestionId == 1094 && (leadqa.Answer != null || leadqa.Answer != ""))
                {
                    CC_QQ_ValPak = "Checked";
                }
                if (leadqa.QuestionId == 1095 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_YellowPages = "Checked";
                }
                if (leadqa.QuestionId == 1096 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_RSVP = "Checked";
                }
                if (leadqa.QuestionId == 1097 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_Van = "Checked";
                }
                if (leadqa.QuestionId == 1098 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_Internet = "Checked";
                }
                if (leadqa.QuestionId == 1099 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_Sources1 = "Checked";
                }
                if (leadqa.QuestionId == 1100 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_Sources2 = "Checked";
                }
                if (leadqa.QuestionId == 1101 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_Sources3 = "Checked";
                }
                if (leadqa.QuestionId == 1102 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_FaceBookPaid = "Checked";
                }

                if (leadqa.QuestionId == 1103 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_Referel = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1104 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_Budget = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1105 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_AnyAppointment = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1106 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    if (leadqa.Answer == "Yes")
                    {
                        CC_QQ_ReceivedOtherQuotesYes = "Checked";
                        CC_QQ_ReceivedOtherQuotesNo = "";
                    }
                    else if (leadqa.Answer == "No")
                    {
                        CC_QQ_ReceivedOtherQuotesYes = "";
                        CC_QQ_ReceivedOtherQuotesNo = "Checked";
                    }
                }

                if (leadqa.QuestionId == 1108 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_ReceivedOtherQuotesSpecific = leadqa.Answer;
                }
                //Other Comments?

                #endregion Tailored Living

                #region Project Needs

                if (leadqa.QuestionId == 1135 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_NewPour = "Checked";
                }
                if (leadqa.QuestionId == 1111 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_StampedConcrete = "Checked";
                }
                if (leadqa.QuestionId == 1112 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_ResurfaceConcrete = "Checked";
                }
                if (leadqa.QuestionId == 1113 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_StainedConcrete = "Checked";
                }
                if (leadqa.QuestionId == 1114 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_ColorRestoreSystem = "Checked";
                }
                if (leadqa.QuestionId == 1115 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_ColorSealSystem = "Checked";
                }
                if (leadqa.QuestionId == 1116 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_PlainConcrete = "Checked";
                }
                if (leadqa.QuestionId == 1117 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_CleanAndReseal = "Checked";
                }
                if (leadqa.QuestionId == 1118 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_ColoredConcrete = "Checked";
                }

                if (leadqa.QuestionId == 1119 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_ProjectDetails = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1139 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_Additional_Info = leadqa.Answer;
                }

                if (leadqa.QuestionId == 1121 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    if (leadqa.Answer == "Yes")
                    {
                        CC_ExistingSlabYes = "Checked";
                        CC_ExistingSlabNo = "";
                    }
                    else if (leadqa.Answer == "No")
                    {
                        CC_ExistingSlabYes = "";
                        CC_ExistingSlabNo = "Checked";
                    }
                }
                if (leadqa.QuestionId == 1123 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_AprroximateSizeOrShape = leadqa.Answer;
                }

                if (leadqa.QuestionId == 1124 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_OntheSurface = leadqa.Answer;
                }

                #endregion Project Needs

            }

            if (CC_QQ_NewHome != null)
            {
                data = data.Replace("{{CC_QQ_NewHome}}", CC_QQ_NewHome);
                data = data.Replace("{{CC_QQ_ExistingHome}}", CC_QQ_ExistingHome);
            }

            if (CC_QQ_EstimatedClosingDate != null)
            {
                data = data.Replace("{{CC_QQ_EstimatedClosingDate}}", CC_QQ_EstimatedClosingDate);
            }
            data = data.Replace("{{CC_QQ_ProjectLeadTimePreference}}", CC_QQ_ProjectLeadTimePreference);

            data = data.Replace("{{CC_QQ_ProjectLeadTimePreference}}", CC_QQ_ProjectLeadTimePreference);
            data = data.Replace("{{ChkValpak}}", CC_QQ_ValPak);
            data = data.Replace("{{ChkYellowPages}}", CC_QQ_YellowPages);
            data = data.Replace("{{ChkRSVP}}", CC_QQ_RSVP);
            data = data.Replace("{{ChkVan}}", CC_QQ_Van);
            data = data.Replace("{{ChkInternet}}", CC_QQ_Internet);
            data = data.Replace("{{ChkSources1}}", CC_QQ_Sources1);
            data = data.Replace("{{ChkSources2}}", CC_QQ_Sources2);
            data = data.Replace("{{ChkSources3}}", CC_QQ_Sources3);
            data = data.Replace("{{ChkFacebookPaid}}", CC_QQ_FaceBookPaid);

            data = data.Replace("{{CC_QQ_Referel}}", CC_QQ_Referel);
            data = data.Replace("{{CC_QQ_Budget}}", CC_QQ_Budget);
            data = data.Replace("{{CC_QQ_AnyAppointment}}", CC_QQ_AnyAppointment);

            data = data.Replace("{{CC_QQ_ReceivedOtherQuotesYes}}", CC_QQ_ReceivedOtherQuotesYes);
            data = data.Replace("{{CC_QQ_ReceivedOtherQuotesNo}}", CC_QQ_ReceivedOtherQuotesNo);
            data = data.Replace("{{CC_QQ_ReceivedOtherQuotesSpecific}}", CC_QQ_ReceivedOtherQuotesSpecific);

            data = data.Replace("{{ChkStampedConcrete}}", CC_StampedConcrete);
            data = data.Replace("{{ChkResurfaceConcrete}}", CC_ResurfaceConcrete);
            data = data.Replace("{{ChkStainedConcrete}}", CC_StainedConcrete);
            data = data.Replace("{{ChkColorRestoreSystem}}", CC_ColorRestoreSystem);

            data = data.Replace("{{ChkColorSealSystem}}", CC_ColorSealSystem);
            data = data.Replace("{{ChkPlainConcrete}}", CC_PlainConcrete);
            data = data.Replace("{{ChkCleanAndReseal}}", CC_CleanAndReseal);
            data = data.Replace("{{ChkColoredConcrete}}", CC_ColoredConcrete);
            data = data.Replace("{{ChkNewPour}}", CC_NewPour);

            data = data.Replace("{{ProjectDetails}}", CC_ProjectDetails);

            data = data.Replace("{{CC_QQ_ExistingSlabYes}}", CC_ExistingSlabYes);
            data = data.Replace("{{CC_QQ_ExistingSlabNo}}", CC_ExistingSlabNo);
            data = data.Replace("{{CC_QQ_AprroximateSizeOrShape}}", CC_AprroximateSizeOrShape);

            data = data.Replace("{{CC_QQ_OntheSurface}}", CC_OntheSurface);

            // TODO: need to fetch data from the qualify questions.....
            data = data.Replace("{{CC_PN_FrabricPreferences}}", "");
            data = data.Replace("{{CC_PN_SolidSOrPrints}}", "");
            data = data.Replace("{{CC_PN_PreferredStyle}}", "");
            data = data.Replace("{{CC_PN_HowManyWindows}}", "");
            data = data.Replace("{{CC_PN_Room_OtherNote}}", "");
            data = data.Replace("{{CC_PN_Color}}", "");

            data = data.Replace("{{CC_ProjectDetails}}", CC_ProjectDetails);
            data = data.Replace("{{CC_Additional_Info}}", CC_Additional_Info);

            return data;
        }

        private static CustomerTP GetSecondaryCustomerForLead(int leadId)
        {
            var addcm = new AdditionalContactManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var temp = addcm.GetLeadContacts(leadId, false);
            CustomerTP result = null;
            if (temp != null && temp.Count > 0)
            {
                result = temp.OrderByDescending(so => so.CreatedBy).FirstOrDefault();
            }

            return result;
        }

        private static CustomerTP GetSecondaryCustomerForAccount(int accountId)
        {
            var addcm = new AdditionalContactManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var temp = addcm.GetAccountContacts(accountId, false);
            CustomerTP result = null;
            if (temp != null && temp.Count > 0)
            {
                result = temp.OrderByDescending(so => so.CreatedBy).FirstOrDefault();
            }

            return result;
        }

        private static string GetLeadInformation(int leadId)
        {
            var path = GetTemplateName("Lead");
            string srcFilePath = HostingEnvironment.MapPath(path);

            var data = System.IO.File.ReadAllText(srcFilePath);

            Lead lead = leadsMng.Get(leadId);

            string PrimaryContact = lead.PrimCustomer.FullName;
            string CompanyName = lead.PrimCustomer.CompanyName;
            AddressTP BillingAddress = null;
            if (lead.Addresses.Count > 0)
            {
                BillingAddress = lead.Addresses.First();
            }
            string BillingAddress1 = BillingAddress.Address1;
            string BillingAddress2 = BillingAddress.Address2;
            //--added newly for showing location & cross street info of billing address.
            string BillingLocation = BillingAddress.Location;
            string BillingCrossStreet = BillingAddress.CrossStreet;
            //--end.
            // string BillingAddress_CityStateZip = BillingAddress.City + ',' + BillingAddress.State + ',' + BillingAddress.ZipCode;
            string BillingAddress_CityStateZip = GetCombinedAddress(city: BillingAddress.City
                , state: BillingAddress.State, zip: BillingAddress.ZipCode);

            string PrimaryCellPhone = GetFormatPhone(lead.PrimCustomer.CellPhone);
            string PrimaryHomePhone = GetFormatPhone(lead.PrimCustomer.HomePhone);
            string PrimaryWorkPhone = GetFormatPhone(lead.PrimCustomer.WorkPhone);
            string PrimaryEmail = lead.PrimCustomer.PrimaryEmail;

            string PrimaryNotes = "";

            var notelst = noteMgr.GetLeadNotes(new Note { LeadId = leadId }, false);
            if (notelst != null && notelst.Count > 0)
                PrimaryNotes = notelst.Select(x => x.Notes).FirstOrDefault();

            AddressTP InstallAddress = null;

            string InstallAddress1 = string.Empty;
            string InstallAddress2 = string.Empty; 
            //-- added to get location & cross street of installation address. 
            string InstallLocation = string.Empty; string InstallCrossstreet = string.Empty;
            //-- end
            string InstallAddres_CityStateZip = string.Empty;
            if (lead.Addresses.Count > 0)
            {
                //Select secondary address
                InstallAddress = lead.Addresses.First();
                InstallAddress1 = InstallAddress.Address1;
                InstallAddress2 = InstallAddress.Address2;
                //InstallAddres_CityStateZip = InstallAddress.City + ',' + InstallAddress.State + ',' + InstallAddress.ZipCode;
                InstallAddres_CityStateZip = GetCombinedAddress(city: InstallAddress.City
                    , state: InstallAddress.State, zip: InstallAddress.ZipCode);
                //-- assign value to location & cross street.
                InstallLocation = InstallAddress.Location;
                InstallCrossstreet = InstallAddress.CrossStreet;
                //-- end
            }

            CustomerTP SecCustomer;
            string Secondarycontact = "";
            string SecondaryCellPhone = "";
            var sc = GetSecondaryCustomerForLead(leadId);
            if (sc != null)
            {
                Secondarycontact = sc.FullName;
                SecondaryCellPhone = GetFormatPhone(sc.CellPhone);
            }

            //if (lead.SecCustomer.Count > 1)
            //{
            //    Select secondary address
            //    SecCustomer = lead.SecCustomer.First();
            //    Secondarycontact = SecCustomer.FullName;
            //    SecondaryCellPhone = SecCustomer.CellPhone;
            //}
            string Commercial_Type = lead.CommercialValue;
            string Industry = lead.IndustryValue;

            SourceList leadsources = null;

            string LeadSourceName = "";
            string LeadChannelName = "";
            if (lead.SourcesList.Count > 0)
            {
                leadsources = lead.SourcesList.First();
                LeadSourceName = leadsources.name;
                LeadChannelName = leadsources.ChannelName;
            }

            //Read lead html and append the data
            if (PrimaryContact != null)
            {
                //{{Contactfullname}}
                data = data.Replace("{{Contactfullname}}", PrimaryContact);
            }
            else
            {
                data = data.Replace("{{Contactfullname}}", "");
            }
            if (CompanyName != null)
            {
                data = data.Replace("{{CompanyName}}", CompanyName);
            }
            else
            {
                data = data.Replace("{{CompanyName}}", "");
            }
            if (BillingAddress1 != null && BillingAddress1 != "")
            {
                data = data.Replace("{{BillingAddress1}}", BillingAddress1);
            }
            else
            {
                data = data.Replace("{{BillingAddress1}}", "");
            }
            if (BillingAddress2 != null)
            {
                data = data.Replace("{{BillingAddress2}}", BillingAddress2);
            }
            else
            {
                data = data.Replace("{{BillingAddress2}}", "");
            }
            if (BillingAddress_CityStateZip != null)
            {
                data = data.Replace("{{BillingAddress_CityStateZip}}", BillingAddress_CityStateZip);
            }
            else
            {
                data = data.Replace("{{BillingAddress_CityStateZip}}", "");
            }
            //--location & cross street
            if(BillingLocation != null && BillingLocation != "")
            {
                data = data.Replace("{{BillingLocation}}", BillingLocation);
            }
            else
            {
                data = data.Replace("{{BillingLocation}}", "");
            }
            if (BillingCrossStreet != null && BillingCrossStreet != "")
            {
                data = data.Replace("{{BillingCrossStreet}}", BillingCrossStreet);
            }
            else
            {
                data = data.Replace("{{BillingCrossStreet}}", "");
            }
            //--end
            if (PrimaryCellPhone != null)
            {
                data = data.Replace("{{PrimaryCellPhone}}", PrimaryCellPhone);
            }
            else
            {
                data = data.Replace("{{PrimaryCellPhone}}", "");
            }
            if (PrimaryHomePhone != null)
            {
                data = data.Replace("{{PrimaryHomePhone}}", PrimaryHomePhone);
            }
            else
            {
                data = data.Replace("{{PrimaryHomePhone}}", "");
            }
            if (PrimaryWorkPhone != null)
            {
                data = data.Replace("{{PrimaryWorkPhone}}", PrimaryWorkPhone);
            }
            else
            {
                data = data.Replace("{{PrimaryWorkPhone}}", "");
            }
            if (PrimaryEmail != null)
            {
                data = data.Replace("{{PrimaryEmail}}", PrimaryEmail);
            }
            else
            {
                data = data.Replace("{{PrimaryEmail}}", "");
            }
            if (PrimaryNotes != null)
            {
                data = data.Replace("{{PrimaryNotes}}", PrimaryNotes);
            }
            else
            {
                data = data.Replace("{{PrimaryNotes}}", "");
            }
            if (InstallAddress1 != null)
            {
                data = data.Replace("{{InstallAddress1}}", InstallAddress1);
            }
            else data = data.Replace("{{InstallAddress1}}", "");
            if (InstallAddress2 != null)
            {
                data = data.Replace("{{InstallAddress2}}", InstallAddress2);
            }
            else
            {
                data = data.Replace("{{InstallAddress2}}", "");
            }

            if (InstallAddres_CityStateZip != null)
            {
                data = data.Replace("{{InstallAddres_CityStateZip}}", InstallAddres_CityStateZip);
            }
            else
            {
                data = data.Replace("{{InstallAddres_CityStateZip}}", "");
            }
            //--installation location & cross street
            if(InstallLocation != null && InstallLocation != "")
            {
                data = data.Replace("{{InstallLocation}}", InstallLocation);
            }
            else
            {
                data = data.Replace("{{InstallLocation}}", "");
            }
            if(InstallCrossstreet != null && InstallCrossstreet != "")
            {
                data = data.Replace("{{InstallCrossstreet}}", InstallCrossstreet);
            }
            else
            {
                data = data.Replace("{{InstallCrossstreet}}", "");
            }
            //--end
            string leadType = "";
            if (leadType != null)
            {
                data = data.Replace("{{leadType}}", leadType);
            }
            if (Secondarycontact != null)
            {
                data = data.Replace("{{Secondarycontact}}", Secondarycontact);
            }
            else
            {
                data = data.Replace("{{Secondarycontact}}", "");
            }
            if (SecondaryCellPhone != null)
            {
                data = data.Replace("{{SecondaryCellPhone}}", SecondaryCellPhone);
            }
            else
            {
                data = data.Replace("{{SecondaryCellPhone}}", "");
            }
            if (LeadSourceName != null)
            {
                data = data.Replace("{{LeadSourceName}}", LeadSourceName);
            }
            else
            {
                data = data.Replace("{{LeadSourceName}}", "");
            }

            if (lead.AccountName != null && lead.AccountName != "")
                data = data.Replace("{{LeadReferralInfo}}", lead.AccountName);
            else
                data = data.Replace("{{LeadReferralInfo}}", "");

            if (lead.LeadType != null && lead.LeadType != "")
                data = data.Replace("{{LeadType}}", lead.LeadType);
            else
                data = data.Replace("{{LeadType}}", "");

            if (lead.KeyAcct != null && lead.KeyAcct != "")
                data = data.Replace("{{KeyAccount}}", lead.KeyAcct);
            else
                data = data.Replace("{{KeyAccount}}", "");

            if (lead.RelatedAccountName != null && lead.RelatedAccountName != "")
                data = data.Replace("{{ParentAccount}}", lead.RelatedAccountName);
            else
                data = data.Replace("{{ParentAccount}}", "");

            if (lead.LegacyAccount != null && lead.LegacyAccount == true)
                data = data.Replace("{{LegacyAccount}}", "Checked");
            else
                data = data.Replace("{{LegacyAccount}}", "");

            if (lead.IsCommercial == true)
                data = data.Replace("{{Commercial}}", "Checked");
            else
                data = data.Replace("{{Commercial}}", "");

            if (Commercial_Type != null)
                data = data.Replace("{{Commercial_Type}}", Commercial_Type);
            else
                data = data.Replace("{{Commercial_Type}}", "");
            if (Industry != null)
                data = data.Replace("{{Industry}}", Industry);
            else
                data = data.Replace("{{Industry}}", "");

            // Qualify
            if (SessionManager.BrandId == 1)
            {
                data = UpdateQualifyBB(data, lead.LeadQuestionAns);
                //data = UpdateQualify(data, lead.LeadQuestionAns);
            }
            else if (SessionManager.BrandId == 2)
            {
                data = UpdateQualifyTL(data, lead.LeadQuestionAns);
            }
            else
            {
                data = UpdateQualifyCC(data, lead.LeadQuestionAns);
            }

            //Sales Appointment Information
            //string SalesAgentName = "";
            //string BookedBy = "";
            //string AppointmentDate = "";
            //string AppointmentTime = "";

            var recentAppointment = calendarMgr.GetRecentAppointment(leadId);

            data = UpdateRecentAppointment(data, recentAppointment);

            //if (RecentAppointment != null)
            //{
            //    if (RecentAppointment.AssignedName != null && RecentAppointment.AssignedName != "")
            //    {
            //        SalesAgentName = RecentAppointment.AssignedName;
            //    }
            //    if (RecentAppointment.OrganizerName != null && RecentAppointment.OrganizerName != "")
            //    {
            //        BookedBy = RecentAppointment.OrganizerName;
            //    }

            //    if (RecentAppointment.StartDate != null && RecentAppointment.StartDate.ToString() != "")
            //    {
            //        //AppointmentDate = RecentAppointment.StartDate.ToString("dd/mm/yyyy");
            //        //AppointmentTime = RecentAppointment.StartDate.ToString("hh:mm:ss");

            //        // TODO : this is currenlty not returning the proper result. In calendar we are gettting
            //        // the right time, however here it is getting some other time. Kiran already aware of this
            //        // issue, and says that Rajkumar can help us to solve this issue.
            //        // var newdate = TimeZoneManager.ToLocal(DateTime.Parse(RecentAppointment.StartDate.ToString()));

            //        var newdate = TimeZoneManager.ToLocal(RecentAppointment.StartDate.DateTime).ToString();

            //        //AppointmentDate = GetFormatDate(Convert.ToString( RecentAppointment.StartDate));
            //        //AppointmentTime = GetFormatTime(Convert.ToString(RecentAppointment.StartDate));

            //        AppointmentDate = GetFormatDate(Convert.ToString( newdate));
            //        AppointmentTime = GetFormatTime(Convert.ToString(newdate));
            //    }

            //}
            //data = data.Replace("{{SalesAgentName}}", SalesAgentName);
            //data = data.Replace("{{BookedBy}}", BookedBy);
            //data = data.Replace("{{ApptDate}}", AppointmentDate);
            //data = data.Replace("{{ApptTime}}", AppointmentTime);

            //Zillow details
            if (lead.Addresses != null && lead.Addresses.Count > 0 &&
                lead.Addresses.FirstOrDefault().CountryCode2Digits == "US")
            {
                data = data.Replace("{{displayZillow}}", "block");
                var zillow = GetZillow(leadId);
                if (zillow.yearBuilt == null && zillow.lotSizeSqFt == null && zillow.finishedSqFt == null && zillow.bedrooms == null
                    && zillow.bathrooms == null && zillow.lastSoldDate == null && zillow.lastSoldPrice == null && zillow.zestimate == null)
                {
                    data = data.Replace("{{displayZillowData}}", "none");
                    data = data.Replace("{{displayZillowNoInfo}}", "block");
                }
                else
                {
                    data = data.Replace("{{yearBuilt}}", zillow.yearBuilt);
                    data = data.Replace("{{lotSizeSqFt}}", zillow.lotSizeSqFt);
                    data = data.Replace("{{finishedSqFt}}", zillow.finishedSqFt);
                    data = data.Replace("{{bedrooms}}", zillow.bedrooms);
                    data = data.Replace("{{bathrooms}}", zillow.bathrooms);
                    data = data.Replace("{{lastSoldDate}}", zillow.lastSoldDate);
                    data = data.Replace("{{lastSoldPrice}}", zillow.lastSoldPrice);
                    data = data.Replace("{{zestimate}}", zillow.zestimate);

                    data = data.Replace("{{displayZillowData}}", "block");
                    data = data.Replace("{{displayZillowNoInfo}}", "none");
                }
            }
            else
            {
                data = data.Replace("{{displayZillow}}", "none");
            }

            return data;
        }

        private static string UpdateRecentAppointment(string data, CalendarVM recentAppointment)
        {
            string salesAgentName = "";
            string bookedBy = "";
            string appointmentDate = "";
            string appointmentTime = "";

            if (recentAppointment != null)
            {
                if (!string.IsNullOrEmpty(recentAppointment.AssignedName))
                {
                    salesAgentName = recentAppointment.AssignedName;
                }
                if (!string.IsNullOrEmpty(recentAppointment.OrganizerName))
                {
                    bookedBy = recentAppointment.OrganizerName;
                }

                if (recentAppointment.StartDate != null && recentAppointment.StartDate.ToString() != "")
                {
                    //handled in CalendarManager
                    //var newdate = TimeZoneManager.ToLocal(recentAppointment.StartDate.DateTime).ToString();
                    var newdate = recentAppointment.StartDate;
                    appointmentDate = GetFormatDate(Convert.ToString(newdate));
                    appointmentTime = GetFormatTime(Convert.ToString(newdate));
                }
            }
            data = data.Replace("{{SalesAgentName}}", salesAgentName);
            data = data.Replace("{{BookedBy}}", bookedBy);
            data = data.Replace("{{ApptDate}}", appointmentDate);
            data = data.Replace("{{ApptTime}}", appointmentTime);

            return data;
        }

        private static string GetAccountInformation(int accountId)
        {
            var path = GetTemplateName("Account");
            string srcFilePath = HostingEnvironment.MapPath(path);

            var data = System.IO.File.ReadAllText(srcFilePath);

            //Lead lead = leadsMng.Get(leadId);
            AccountTP lead = accountsMgr.Get(accountId);

            string PrimaryContact = lead.PrimCustomer.FullName;
            string CompanyName = lead.PrimCustomer.CompanyName;
            AddressTP BillingAddress = null;
            if (lead.Addresses.Count > 0)
            {
                BillingAddress = lead.Addresses.First();
            }
            string BillingAddress1 = BillingAddress.Address1;
            string BillingAddress2 = BillingAddress.Address2;
            string BillingAddress_CityStateZip = BillingAddress.City + ',' + BillingAddress.State + ',' + BillingAddress.ZipCode;

            string PrimaryCellPhone = GetFormatPhone(lead.PrimCustomer.CellPhone);
            string PrimaryHomePhone = GetFormatPhone(lead.PrimCustomer.HomePhone);
            string PrimaryWorkPhone = GetFormatPhone(lead.PrimCustomer.WorkPhone);
            string PrimaryEmail = lead.PrimCustomer.PrimaryEmail;

            string PrimaryNotes = "";
            var notelst = noteMgr.GetAccountNotes(new Note { AccountId = accountId }, false);
            if (notelst != null && notelst.Count > 0)
                PrimaryNotes = notelst.Select(x => x.Notes).FirstOrDefault();

            AddressTP InstallAddress = null;

            string InstallAddress1 = string.Empty;
            string InstallAddress2 = string.Empty;
            string InstallAddres_CityStateZip = string.Empty;
            if (lead.Addresses.Count > 1)
            {
                //Select secondary address
                InstallAddress = lead.Addresses.First();
                InstallAddress1 = InstallAddress.Address1;
                InstallAddress2 = InstallAddress.Address2;
                InstallAddres_CityStateZip = InstallAddress.City + ',' + InstallAddress.State + ',' + InstallAddress.ZipCode;
            }

            CustomerTP SecCustomer;
            string Secondarycontact = "";
            string SecondaryCellPhone = "";
            if (lead.SecCustomer.Count > 1)
            {
                //Select secondary address
                SecCustomer = lead.SecCustomer.First();
                Secondarycontact = SecCustomer.FullName;
                SecondaryCellPhone = SecCustomer.CellPhone;
            }

            SourceList leadsources = null;

            string LeadSourceName = "";
            string LeadChannelName = "";
            if (lead.SourcesList.Count > 0)
            {
                leadsources = lead.SourcesList.First();
                LeadSourceName = leadsources.name;
                LeadChannelName = leadsources.ChannelName;
            }

            //Read lead html and append the data
            if (PrimaryContact != null)
            {
                //{{Contactfullname}}
                data = data.Replace("{{Contactfullname}}", PrimaryContact);
            }
            else
            {
                data = data.Replace("{{Contactfullname}}", "");
            }
            if (CompanyName != null)
            {
                data = data.Replace("{{CompanyName}}", CompanyName);
            }
            else
            {
                data = data.Replace("{{CompanyName}}", "");
            }
            if (BillingAddress1 != null)
            {
                data = data.Replace("{{BillingAddress1}}", BillingAddress1);
            }
            else
            {
                data = data.Replace("{{BillingAddress1}}", "");
            }
            if (BillingAddress2 != null)
            {
                data = data.Replace("{{BillingAddress2}}", BillingAddress2);
            }
            else
            {
                data = data.Replace("{{BillingAddress2}}", "");
            }
            if (BillingAddress_CityStateZip != null)
            {
                data = data.Replace("{{BillingAddress_CityStateZip}}", BillingAddress_CityStateZip);
            }
            else
            {
                data = data.Replace("{{BillingAddress_CityStateZip}}", "");
            }
            if (PrimaryCellPhone != null)
            {
                data = data.Replace("{{PrimaryCellPhone}}", PrimaryCellPhone);
            }
            else
            {
                data = data.Replace("{{PrimaryCellPhone}}", "");
            }
            if (PrimaryHomePhone != null)
            {
                data = data.Replace("{{PrimaryHomePhone}}", PrimaryHomePhone);
            }
            else
            {
                data = data.Replace("{{PrimaryHomePhone}}", "");
            }
            if (PrimaryWorkPhone != null)
            {
                data = data.Replace("{{PrimaryWorkPhone}}", PrimaryWorkPhone);
            }
            else
            {
                data = data.Replace("{{PrimaryWorkPhone}}", "");
            }
            if (PrimaryEmail != null)
            {
                data = data.Replace("{{PrimaryEmail}}", PrimaryEmail);
            }
            else
            {
                data = data.Replace("{{PrimaryEmail}}", "");
            }
            if (PrimaryNotes != null)
            {
                data = data.Replace("{{PrimaryNotes}}", PrimaryNotes);
            }
            else
            {
                data = data.Replace("{{PrimaryNotes}}", "");
            }
            if (InstallAddress1 != null)
            {
                data = data.Replace("{{InstallAddress1}}", InstallAddress1);
            }
            else data = data.Replace("{{InstallAddress1}}", "");
            if (InstallAddress2 != null)
            {
                data = data.Replace("{{InstallAddress2}}", InstallAddress2);
            }
            else
            {
                data = data.Replace("{{InstallAddress2}}", "");
            }

            if (InstallAddres_CityStateZip != null)
            {
                data = data.Replace("{{InstallAddres_CityStateZip}}", InstallAddres_CityStateZip);
            }
            else
            {
                data = data.Replace("{{InstallAddres_CityStateZip}}", "");
            }
            string leadType = "";
            if (leadType != null)
            {
                data = data.Replace("{{leadType}}", leadType);
            }
            if (Secondarycontact != null)
            {
                data = data.Replace("{{Secondarycontact}}", Secondarycontact);
            }
            else
            {
                data = data.Replace("{{Secondarycontact}}", "");
            }
            if (SecondaryCellPhone != null)
            {
                data = data.Replace("{{SecondaryCellPhone}}", SecondaryCellPhone);
            }
            else
            {
                data = data.Replace("{{SecondaryCellPhone}}", "");
            }
            if (LeadSourceName != null)
            {
                data = data.Replace("{{LeadSourceName}}", LeadSourceName);
            }
            else
            {
                data = data.Replace("{{LeadSourceName}}", "");
            }

            // Qualify
            if (SessionManager.BrandId == 1)
            {
                data = UpdateQualifyBB(data, lead.AccountQuestionAns);
                //data = UpdateQualify(data, lead.AccountQuestionAns);
            }
            else if (SessionManager.BrandId == 2)
            {
                data = UpdateQualifyTL(data, lead.AccountQuestionAns);
            }
            else
            {
                data = UpdateQualifyCC(data, lead.AccountQuestionAns);
            }

            //Sales Appointment Information
            string SalesAgentName = "";
            string BookedBy = "";
            string AppointmentDate = "";
            string AppointmentTime = "";

            var RecentAppointment = calendarMgr.GetRecentAppointmentForAccount(accountId);

            if (RecentAppointment != null)
            {
                if (RecentAppointment.AssignedName != null && RecentAppointment.AssignedName != "")
                {
                    SalesAgentName = RecentAppointment.AssignedName;
                }
                if (RecentAppointment.OrganizerName != null && RecentAppointment.OrganizerName != "")
                {
                    BookedBy = RecentAppointment.OrganizerName;
                }

                if (RecentAppointment.StartDate != null && RecentAppointment.StartDate.ToString() != "")
                {
                    AppointmentDate = GetFormatDate(Convert.ToString(RecentAppointment.StartDate));
                    AppointmentTime = GetFormatTime(Convert.ToString(RecentAppointment.StartDate));
                }
            }
            data = data.Replace("{{SalesAgentName}}", SalesAgentName);
            data = data.Replace("{{BookedBy}}", BookedBy);
            data = data.Replace("{{ApptDate}}", AppointmentDate);
            data = data.Replace("{{ApptTime}}", AppointmentTime);

            //Zillow details
            var zillow = GetZillowForAccount(accountId);

            data = data.Replace("{{yearBuilt}}", zillow.yearBuilt);
            data = data.Replace("{{lotSizeSqFt}}", zillow.lotSizeSqFt);
            data = data.Replace("{{finishedSqFt}}", zillow.finishedSqFt);
            data = data.Replace("{{bedrooms}}", zillow.bedrooms);
            data = data.Replace("{{bathrooms}}", zillow.bathrooms);
            data = data.Replace("{{lastSoldDate}}", zillow.lastSoldDate);
            data = data.Replace("{{lastSoldPrice}}", zillow.lastSoldPrice);
            data = data.Replace("{{zestimate}}", zillow.zestimate);

            return data;
        }

        private static string GetCombinedAddress(
            string address1 = ""
            , string address2 = ""
            , string city = ""
            , string state = ""
            , string zip = "")
        {
            var temp = address1;
            temp += string.IsNullOrEmpty(temp) ? address2 : ", " + address2;
            temp += string.IsNullOrEmpty(temp) ? city : ", " + city;
            temp += string.IsNullOrEmpty(temp) ? state : ", " + state;
            temp += string.IsNullOrEmpty(temp) ? zip : ", " + zip;

            return temp;
        }

        private static string GetOpportunityInformation(int opportunityId)
        {
            var path = GetTemplateName("Opportunity");
            string srcFilePath = HostingEnvironment.MapPath(path);

            var data = System.IO.File.ReadAllText(srcFilePath);

            var opportunity = oppMgr.Get(opportunityId);
            AccountTP account = accountsMgr.Get(opportunity.AccountId);

            string PrimaryContact = account.PrimCustomer.FullName;
            string CompanyName = account.PrimCustomer.CompanyName;
            AddressTP BillingAddress = null;

            if (opportunity.BillingAddressId != null)
            {
                BillingAddress = oppMgr.GetAddress((int)opportunity.BillingAddressId);
                //Commenting out the code since if user select Related/parent address in billing address
                //value is null.So we gone with above method now. 
                //BillingAddress = account.Addresses
                //    .Where(a => a.AddressId == (int)opportunity.BillingAddressId)
                //                                    .FirstOrDefault();
            }
            else
            {
                if (account.Addresses.Count > 0)
                {
                    BillingAddress = account.Addresses.First();
                }
            }

            string BillingAddress1 = "";
            string BillingAddress2 = "";
            string BillingAddress_CityStateZip = "";
            //--added newly for showing location & cross street info of billing address.
            string BillingLocation = "";
            string BillingCrossStreet = "";
            //--end.
            if (BillingAddress != null)
            {
                BillingAddress1 = BillingAddress.Address1;
                BillingAddress2 = BillingAddress.Address2;
                //string BillingAddress_CityStateZip = BillingAddress.City + ',' + BillingAddress.State + ',' + BillingAddress.ZipCode;
                BillingAddress_CityStateZip = GetCombinedAddress(city: BillingAddress.City,
                   state: BillingAddress.State, zip: BillingAddress.ZipCode);
                //-- assign value to location & cross street.
                BillingLocation = BillingAddress.Location;
                BillingCrossStreet = BillingAddress.CrossStreet;
                //--end
            }

            //string PrimaryCellPhone = GetFormatPhone(lead.PrimCustomer.CellPhone);
            //string PrimaryHomePhone = GetFormatPhone(lead.PrimCustomer.HomePhone);
            //string PrimaryWorkPhone = GetFormatPhone(lead.PrimCustomer.WorkPhone);
            //string PrimaryEmail = lead.PrimCustomer.PrimaryEmail;

            string PrimaryCellPhone = GetFormatPhone(account.PrimCustomer.CellPhone);
            string PrimaryHomePhone = GetFormatPhone(account.PrimCustomer.HomePhone);
            string PrimaryWorkPhone = GetFormatPhone(account.PrimCustomer.WorkPhone);
            string PrimaryEmail = account.PrimCustomer.PrimaryEmail;

            string PrimaryNotes = "";
            var notelst = noteMgr.GetOpportunityNotes(new Note { OpportunityId = opportunityId }, false);
            if (notelst != null && notelst.Count > 0)
                PrimaryNotes = notelst.Select(x => x.Notes).FirstOrDefault();

            AddressTP InstallAddress = null;

            string InstallAddress1 = string.Empty;
            string InstallAddress2 = string.Empty;
            string InstallAddres_CityStateZip = string.Empty;
            //-- added to get location & cross street of installation address. 
            string InstallLocation = string.Empty; string InstallCrossstreet = string.Empty;
            //-- end

            if (opportunity.InstallationAddressId != null)
            {
                InstallAddress = opportunity.Addresses
                    .Where(a => a.AddressId == (int)opportunity.InstallationAddressId)
                                                    .FirstOrDefault();
                if (InstallAddress != null)
                {
                    InstallAddress1 = InstallAddress.Address1;
                    InstallAddress2 = InstallAddress.Address2;
                    //InstallAddres_CityStateZip = InstallAddress.City
                    //    + ',' + InstallAddress.State + ',' + InstallAddress.ZipCode;
                    InstallAddres_CityStateZip = GetCombinedAddress(city: InstallAddress.City
                        , state: InstallAddress.State, zip: InstallAddress.ZipCode);
                    //-- assign value to location & cross street.
                    InstallLocation = InstallAddress.Location;
                    InstallCrossstreet = InstallAddress.CrossStreet;
                    //-- end
                }
            }
            else
            {
                if (account.Addresses.Count > 1)
                {
                    //Select secondary address
                    InstallAddress = account.Addresses.First();
                    InstallAddress1 = InstallAddress.Address1;
                    InstallAddress2 = InstallAddress.Address2;
                    //InstallAddres_CityStateZip = InstallAddress.City
                    //    + ',' + InstallAddress.State + ',' + InstallAddress.ZipCode;
                    InstallAddres_CityStateZip = GetCombinedAddress(city: InstallAddress.City
                       , state: InstallAddress.State, zip: InstallAddress.ZipCode);
                    //-- assign value to location & cross street.
                    InstallLocation = InstallAddress.Location;
                    InstallCrossstreet = InstallAddress.CrossStreet;
                    //-- end
                }
            }

            CustomerTP SecCustomer;
            string Secondarycontact = "";
            string SecondaryCellPhone = "";
            var sc = GetSecondaryCustomerForAccount(opportunity.AccountId);
            if (sc != null)
            {
                Secondarycontact = sc.FullName;
                SecondaryCellPhone = sc.CellPhone;
            }

            //if (lead.SecCustomer.Count > 1)
            //{
            //    //Select secondary address
            //    SecCustomer = lead.SecCustomer.First();
            //    Secondarycontact = SecCustomer.FullName;
            //    SecondaryCellPhone = SecCustomer.CellPhone;
            //}

            //newly added.
            string Commercial = "";
            string Commercial_Type = account.CommercialValue;
            string Industry = account.IndustryValue;

            SourceList leadsources = null;

            //string LeadSourceName = "";
            //string LeadChannelName = "";
            //if (lead.SourcesList.Count > 0)
            //{
            //    leadsources = lead.SourcesList.First();
            //    LeadSourceName = leadsources.name;
            //    LeadChannelName = leadsources.ChannelName;
            //}

            // Now the source shold come from Opportunit, not Lead. TP-2463

            //Read lead html and append the data
            if (PrimaryContact != null)
            {
                //{{Contactfullname}}
                data = data.Replace("{{Contactfullname}}", PrimaryContact);
            }
            else
            {
                data = data.Replace("{{Contactfullname}}", "");
            }
            if (CompanyName != null)
            {
                data = data.Replace("{{CompanyName}}", CompanyName);
            }
            else
            {
                data = data.Replace("{{CompanyName}}", "");
            }
            if (BillingAddress1 != null)
            {
                data = data.Replace("{{BillingAddress1}}", BillingAddress1);
            }
            else
            {
                data = data.Replace("{{BillingAddress1}}", "");
            }
            if (BillingAddress2 != null)
            {
                data = data.Replace("{{BillingAddress2}}", BillingAddress2);
            }
            else
            {
                data = data.Replace("{{BillingAddress2}}", "");
            }
            if (BillingAddress_CityStateZip != null)
            {
                data = data.Replace("{{BillingAddress_CityStateZip}}", BillingAddress_CityStateZip);
            }
            else
            {
                data = data.Replace("{{BillingAddress_CityStateZip}}", "");
            }
            //--location & cross street
            if (BillingLocation != null && BillingLocation != "")
            {
                data = data.Replace("{{BillingLocation}}", BillingLocation);
            }
            else
            {
                data = data.Replace("{{BillingLocation}}", "");
            }
            if (BillingCrossStreet != null && BillingCrossStreet != "")
            {
                data = data.Replace("{{BillingCrossStreet}}", BillingCrossStreet);
            }
            else
            {
                data = data.Replace("{{BillingCrossStreet}}", "");
            }
            //--end
            if (PrimaryCellPhone != null)
            {
                data = data.Replace("{{PrimaryCellPhone}}", PrimaryCellPhone);
            }
            else
            {
                data = data.Replace("{{PrimaryCellPhone}}", "");
            }
            if (PrimaryHomePhone != null)
            {
                data = data.Replace("{{PrimaryHomePhone}}", PrimaryHomePhone);
            }
            else
            {
                data = data.Replace("{{PrimaryHomePhone}}", "");
            }
            if (PrimaryWorkPhone != null)
            {
                data = data.Replace("{{PrimaryWorkPhone}}", PrimaryWorkPhone);
            }
            else
            {
                data = data.Replace("{{PrimaryWorkPhone}}", "");
            }
            if (PrimaryEmail != null)
            {
                data = data.Replace("{{PrimaryEmail}}", PrimaryEmail);
            }
            else
            {
                data = data.Replace("{{PrimaryEmail}}", "");
            }
            if (PrimaryNotes != null)
            {
                data = data.Replace("{{PrimaryNotes}}", PrimaryNotes);
            }
            else
            {
                data = data.Replace("{{PrimaryNotes}}", "");
            }
            if (InstallAddress1 != null)
            {
                data = data.Replace("{{InstallAddress1}}", InstallAddress1);
            }
            else data = data.Replace("{{InstallAddress1}}", "");
            if (InstallAddress2 != null)
            {
                data = data.Replace("{{InstallAddress2}}", InstallAddress2);
            }
            else
            {
                data = data.Replace("{{InstallAddress2}}", "");
            }

            if (InstallAddres_CityStateZip != null)
            {
                data = data.Replace("{{InstallAddres_CityStateZip}}", InstallAddres_CityStateZip);
            }
            else
            {
                data = data.Replace("{{InstallAddres_CityStateZip}}", "");
            }
            //--installation location & cross street
            if (InstallLocation != null && InstallLocation != "")
            {
                data = data.Replace("{{InstallLocation}}", InstallLocation);
            }
            else
            {
                data = data.Replace("{{InstallLocation}}", "");
            }
            if (InstallCrossstreet != null && InstallCrossstreet != "")
            {
                data = data.Replace("{{InstallCrossstreet}}", InstallCrossstreet);
            }
            else
            {
                data = data.Replace("{{InstallCrossstreet}}", "");
            }
            //--end
            string leadType = "";
            if (leadType != null)
            {
                data = data.Replace("{{leadType}}", leadType);
            }
            if (Secondarycontact != null)
            {
                data = data.Replace("{{Secondarycontact}}", Secondarycontact);
            }
            else
            {
                data = data.Replace("{{Secondarycontact}}", "");
            }
            if (SecondaryCellPhone != null)
            {
                data = data.Replace("{{SecondaryCellPhone}}", SecondaryCellPhone);
            }
            else
            {
                data = data.Replace("{{SecondaryCellPhone}}", "");
            }

            //if (LeadSourceName != null)
            //{
            //    data = data.Replace("{{LeadSourceName}}", LeadSourceName);
            //}
            //else
            //{
            //    data = data.Replace("{{LeadSourceName}}", "");
            //}

            var opportunitySourceName = opportunity.SourcesList.Count > 0 ?
                opportunity.SourcesList.First().name : "";
            data = ReplaceTemplateKeyword(data, "OpportunitySourceName", opportunitySourceName);

            if (account.AccountName != null && account.AccountName != "")
                data = data.Replace("{{LeadReferralInfo}}", account.AccountName);
            else
                data = data.Replace("{{LeadReferralInfo}}", "");

            if (account.LeadType != null && account.LeadType != "")
                data = data.Replace("{{LeadType}}", account.LeadType);
            else
                data = data.Replace("{{LeadType}}", "");

            if (account.KeyAcct != null && account.KeyAcct != "")
                data = data.Replace("{{KeyAccount}}", account.KeyAcct);
            else
                data = data.Replace("{{KeyAccount}}", "");

            if (account.RelatedAccountName != null && account.RelatedAccountName != "")
                data = data.Replace("{{ParentAccount}}", account.RelatedAccountName);
            else
                data = data.Replace("{{ParentAccount}}", "");

            if (account.LegacyAccount != null && account.LegacyAccount == true)
                data = data.Replace("{{LegacyAccount}}", "Checked");
            else
                data = data.Replace("{{LegacyAccount}}", "");

            //newly added.
            if (account.IsCommercial == true)
            {
                Commercial = "Checked";
            }
            else
            {
                Commercial = "";
            }
            if (Commercial_Type != null)
            {
                data = data.Replace("{{Commercial_Type}}", Commercial_Type);
            }
            else
            {
                data = data.Replace("{{Commercial_Type}}", "");
            }
            if (Industry != null)
            {
                data = data.Replace("{{Industry}}", Industry);
            }
            else
            {
                data = data.Replace("{{Industry}}", "");
            }

            data = ReplaceTemplateKeyword(data, "KeyAccount", account.KeyAcct);

            var legacyAccount = account.LegacyAccount ? "checked" : "";
            data = ReplaceTemplateKeyword(data, "LegacyAccount", legacyAccount);

            data = data.Replace("{{Commercial}}", Commercial);

            var keyAccount = account.KeyAcct;

            // Qualify
            if (SessionManager.BrandId == 1)
            {
                data = UpdateQualifyBB(data, opportunity.OpportunityQuestionAns);
                //data = UpdateQualify(data, opportunity.OpportunityQuestionAns);
            }
            else if (SessionManager.BrandId == 2)
            {
                data = UpdateQualifyTL(data, opportunity.OpportunityQuestionAns);
            }
            else
            {
                data = UpdateQualifyCC(data, opportunity.OpportunityQuestionAns);
            }

            //Sales Appointment Information
            //string SalesAgentName = "";
            //string BookedBy = "";
            //string AppointmentDate = "";
            //string AppointmentTime = "";

            var recentAppointment = calendarMgr.GetRecentAppointmentForOpportunity(opportunityId);
            data = UpdateRecentAppointment(data, recentAppointment);

            //if (RecentAppointment != null)
            //{
            //    if (RecentAppointment.AssignedName != null && RecentAppointment.AssignedName != "")
            //    {
            //        SalesAgentName = RecentAppointment.AssignedName;
            //    }
            //    if (RecentAppointment.OrganizerName != null && RecentAppointment.OrganizerName != "")
            //    {
            //        BookedBy = RecentAppointment.OrganizerName;
            //    }

            //    if (RecentAppointment.StartDate != null && RecentAppointment.StartDate.ToString() != "")
            //    {
            //        //AppointmentDate = GetFormatDate(Convert.ToString(RecentAppointment.StartDate.DateTime));
            //        //AppointmentTime = GetFormatTime(Convert.ToString(RecentAppointment.StartDate.DateTime));

            //        var newdate = TimeZoneManager.ToLocal(RecentAppointment.StartDate.DateTime).ToString();

            //        AppointmentDate = GetFormatDate(Convert.ToString(newdate));
            //        AppointmentTime = GetFormatTime(Convert.ToString(newdate));
            //    }

            //}
            //data = data.Replace("{{SalesAgentName}}", SalesAgentName);
            //data = data.Replace("{{BookedBy}}", BookedBy);
            //data = data.Replace("{{ApptDate}}", AppointmentDate);
            //data = data.Replace("{{ApptTime}}", AppointmentTime);

            //Zillow details
            var zillow = GetZillowForOpportunity(opportunityId);
            if (zillow != null)
            {
                if (zillow.yearBuilt == null && zillow.lotSizeSqFt == null && zillow.finishedSqFt == null && zillow.bedrooms == null
                   && zillow.bathrooms == null && zillow.lastSoldDate == null && zillow.lastSoldPrice == null && zillow.zestimate == null)
                {
                    data = data.Replace("{{displayZillowData}}", "none");
                    data = data.Replace("{{displayZillowNoInfo}}", "block");
                }
                else
                {
                    data = data.Replace("{{yearBuilt}}", zillow.yearBuilt);
                    data = data.Replace("{{lotSizeSqFt}}", zillow.lotSizeSqFt);
                    data = data.Replace("{{finishedSqFt}}", zillow.finishedSqFt);
                    data = data.Replace("{{bedrooms}}", zillow.bedrooms);
                    data = data.Replace("{{bathrooms}}", zillow.bathrooms);
                    data = data.Replace("{{lastSoldDate}}", zillow.lastSoldDate);
                    data = data.Replace("{{lastSoldPrice}}", zillow.lastSoldPrice);
                    data = data.Replace("{{zestimate}}", zillow.zestimate);

                    data = data.Replace("{{displayZillowData}}", "block");
                    data = data.Replace("{{displayZillowNoInfo}}", "none");
                }
            }
            else
            {
                data = data.Replace("{{displayZillow}}", "none");
            }

            //data = data.Replace("{{yearBuilt}}", zillow.yearBuilt);
            //data = data.Replace("{{lotSizeSqFt}}", zillow.lotSizeSqFt);
            //data = data.Replace("{{finishedSqFt}}", zillow.finishedSqFt);
            //data = data.Replace("{{bedrooms}}", zillow.bedrooms);
            //data = data.Replace("{{bathrooms}}", zillow.bathrooms);
            //data = data.Replace("{{lastSoldDate}}", zillow.lastSoldDate);
            //data = data.Replace("{{lastSoldPrice}}", zillow.lastSoldPrice);
            //data = data.Replace("{{zestimate}}", zillow.zestimate);

            return data;
        }

        private static string ReplaceTemplateKeyword(string data, string keyword, string value)
        {
            if (string.IsNullOrEmpty(data) ||
                string.IsNullOrEmpty(keyword)) return data;

            var tempValue = string.IsNullOrEmpty(value) ? "" : value;

            return data.Replace("{{" + keyword + "}}", tempValue);
        }

        private static Zillow GetZillow(int leadId)
        {
            Zillow zillow = new Zillow();
            var addresses = leadsMng.GetAddressTP(leadId);

            // TODO: verify whether this will throw error
            var zillowaddress = addresses.FirstOrDefault();
            if (zillowaddress != null)
            {
                var parm1 = zillowaddress.City + ", " + zillowaddress.State + ' ' + zillowaddress.ZipCode;
                zillow = GetZillowInformation(zillowaddress.Address1, parm1);
            }

            return zillow;

            //using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            //{
            //    connection.Open();
            //    var str = @"select a.* from crm.leads l
            //            inner join crm.LeadAddresses  la on la.LeadId=l.LeadId
            //            inner join crm.addresses a on a.AddressId=la.AddressId
            //            where l.LeadId=@leadid";
            //    var zillowaddress = connection.Query<HFC.CRM.Data.AddressTP>(str, new { LeadId = id }).FirstOrDefault();
            //    connection.Close();
            //    if (zillowaddress != null)
            //    {
            //        var parm1 = zillowaddress.City + ", " + zillowaddress.State + ' ' + zillowaddress.ZipCode;
            //        zillow = GetZillowInformation(zillowaddress.Address1, parm1);
            //    }
            //}
        }

        private static Zillow GetZillowForAccount(int accountId)
        {
            Zillow zillow = new Zillow();
            var zillowaddress = accountsMgr.GetAddressTP(accountId);
            if (zillowaddress != null)
            {
                var parm1 = zillowaddress.City + ", " + zillowaddress.State + ' ' + zillowaddress.ZipCode;
                zillow = GetZillowInformation(zillowaddress.Address1, parm1);
            }
            return zillow;
        }

        private static Zillow GetZillowForOpportunity(int opportunityId)
        {
            Zillow zillow = new Zillow();
            var zillowaddress = oppMgr.GetAddressTP(opportunityId);
            if (zillowaddress != null)
            {
                var parm1 = zillowaddress.City + ", " + zillowaddress.State + ' ' + zillowaddress.ZipCode;
                zillow = GetZillowInformation(zillowaddress.Address1, parm1);
            }
            return zillow;
        }

        public static Zillow GetZillowInformation(string address, string citystatezip)
        {
            Zillow z = new Zillow();

            var client = new RestClient(AppConfigManager.GetZillowURL + AppConfigManager.GetZillowKey + "&address=" + address + "&citystatezip=" + citystatezip + "");
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(response.Content);

                XmlNodeList nodeListMessage = doc.GetElementsByTagName("message");

                XmlNodeList nodelistResponse = doc.GetElementsByTagName("response");

                string code = "";

                if (nodeListMessage.Count > 0)
                {
                    code = nodeListMessage[0]["code"].InnerText;
                }

                if (code == "0" && nodelistResponse.Count > 0 && nodelistResponse[0]["results"] != null && nodelistResponse[0]["results"].ChildNodes.Count > 0)
                {
                    z.yearBuilt = nodelistResponse[0]["results"].ChildNodes[0]["yearBuilt"] != null ? nodelistResponse[0]["results"].ChildNodes[0]["yearBuilt"].InnerText : "";
                    z.lotSizeSqFt = nodelistResponse[0]["results"].ChildNodes[0]["lotSizeSqFt"] != null ? nodelistResponse[0]["results"].ChildNodes[0]["lotSizeSqFt"].InnerText : "";
                    z.finishedSqFt = nodelistResponse[0]["results"].ChildNodes[0]["finishedSqFt"] != null ? nodelistResponse[0]["results"].ChildNodes[0]["finishedSqFt"].InnerText : "";
                    z.bedrooms = nodelistResponse[0]["results"].ChildNodes[0]["bedrooms"] != null ? nodelistResponse[0]["results"].ChildNodes[0]["bedrooms"].InnerText : "";
                    z.bathrooms = nodelistResponse[0]["results"].ChildNodes[0]["bathrooms"] != null ? nodelistResponse[0]["results"].ChildNodes[0]["bathrooms"].InnerText : "";
                    z.lastSoldDate = nodelistResponse[0]["results"].ChildNodes[0]["lastSoldDate"] != null ? nodelistResponse[0]["results"].ChildNodes[0]["lastSoldDate"].InnerText : "";
                    z.lastSoldPrice = nodelistResponse[0]["results"].ChildNodes[0]["lastSoldPrice"] != null ? nodelistResponse[0]["results"].ChildNodes[0]["lastSoldPrice"].InnerText : "";
                    z.zestimate = nodelistResponse[0]["results"].ChildNodes[0]["zestimate"] != null && nodelistResponse[0]["results"].ChildNodes[0]["zestimate"]["amount"] != null ? nodelistResponse[0]["results"].ChildNodes[0]["zestimate"]["amount"].InnerText : "";
                    z.zillowlink = nodelistResponse[0]["results"].ChildNodes[0]["links"] != null && nodelistResponse[0]["results"].ChildNodes[0]["links"]["homedetails"] != null ? nodelistResponse[0]["results"].ChildNodes[0]["links"]["homedetails"].InnerText : "";

                    z.lotSizeSqFt = z.lotSizeSqFt != "" ? string.Format("{0:n0}", Convert.ToInt32(z.lotSizeSqFt)) : "";
                    z.finishedSqFt = z.finishedSqFt != "" ? string.Format("{0:n0}", Convert.ToInt32(z.finishedSqFt)) : "";

                    z.lastSoldPrice = z.lastSoldPrice != "" ? Convert.ToDecimal(z.lastSoldPrice).ToString("C0", CultureInfo.CurrentCulture) : "$0";
                    z.zestimate = z.zestimate != "" ? Convert.ToDecimal(z.zestimate).ToString("C0", CultureInfo.CurrentCulture) : "$0";
                    return z;
                }
                else
                {
                    return z;
                }
            }
            else
            {
                return z;
            }
        }

        public static string GetGoogleMapForLead(int leadId)
        {
            EventLogger.LogEvent(JsonConvert.SerializeObject(SessionManager.CurrentFranchise), "GetGoogleMapForLead - call started", LogSeverity.Information);
            string currentFranchiseAddress = SessionManager.CurrentFranchise.Address.ToString();
            string result = string.Empty;
            var addresses = leadsMng.GetAddressTP(leadId);
            EventLogger.LogEvent(JsonConvert.SerializeObject(addresses), "GetGoogleMapForLead - lead address info", LogSeverity.Information);

            if (addresses.Count > 0)
            {
                var address = addresses.First();
                var toaddress = address.Address1 + "," + address.City
                        + "," + address.State + ","
                        + address.ZipCode;

                EventLogger.LogEvent(leadId + " " + currentFranchiseAddress + " " + toaddress, "GetGoogleMapForLead", LogSeverity.Information);
                result = PrintGoogleMap(currentFranchiseAddress, toaddress);
            }

            return result;
        }

        public static string GetGoogleMapForOpportunity(int opportunityId)
        {
            string currentFranchiseAddress = SessionManager.CurrentFranchise.Address.ToString();
            string result = string.Empty;
            var address = oppMgr.GetAddressTP(opportunityId);
            if (address != null)
            {
                var toaddress = address.Address1 + "," + address.City
                        + "," + address.State + ","
                        + address.ZipCode;
                result = PrintGoogleMap(currentFranchiseAddress, toaddress);
            }

            return result;
        }

        private static string GetLeadCustomerContentHandler(int id, List<string> printoptoins
            , string sourceType = "Lead")
        {
            string LeadData = string.Empty;
            string data = string.Empty;

            if (printoptoins == null || printoptoins.Count == 0) return data;

            var leadDetails = printoptoins.ToList().Where(c => c.Contains("leadDetails"))
                .FirstOrDefault();
            var googleMap = printoptoins.ToList().Where(c => c.Contains("googleMap"))
               .FirstOrDefault();
            var existingMeasurements = printoptoins.ToList().Where(c => c.Contains("existingMeasurements"))
               .FirstOrDefault();

            if (!string.IsNullOrEmpty(leadDetails))
            {
                if (sourceType == "Lead")
                {
                    data = GetLeadInformation(id);
                }
                else if (sourceType == "Opportunity" || sourceType == "Quote")
                {
                    // data = GetAccountInformation(id);
                    data = GetOpportunityInformation(id);
                }
            }

            // The notes should be in a seperate pge, as per suggestion from Kannan

            return data;
        }

        private static string GetExistingMeasurementsTemplate()
        {
            string path = string.Empty;
            if (SessionManager.BrandId == 1)
            {
                path = string.Format("/Templates/Base/Brand/BB/MeasurementSheet.html");
            }
            else if (SessionManager.BrandId == 2)
            {
                path = string.Format("/Templates/Base/Brand/TL/MeasurementSheet.html");
            }
            else
            {
                // TODO: currently we don't have a template for Concrete craft.
                path = string.Format("");
                return path;
            }

            string srcFilePath = HostingEnvironment.MapPath(path);
            return srcFilePath;
        }

        public static string GetExistingMeasurements(int opportunityId)
        {
            string path = GetExistingMeasurementsTemplate();
            if (string.IsNullOrEmpty(path)) return string.Empty;

            var opportunity = oppMgr.Get(opportunityId);

            string data = System.IO.File.ReadAllText(path);
            var account = measurementMgr.GetAccountInfo(opportunityId);
            List<Address> InstallAddresslist = measurementMgr.GetInstallationAddress(opportunityId);

            HFC.CRM.Data.Calendar Installer = measurementMgr.GetInstaller(opportunityId);
            string PrimaryContact = account.PrimCustomer.FullName;
            string CompanyName = account.PrimCustomer.CompanyName;

            string PrimaryCellPhone = account.PrimCustomer.CellPhone;
            string PrimaryHomePhone = account.PrimCustomer.HomePhone;
            string PrimaryWorkPhone = account.PrimCustomer.WorkPhone;
            string PrimaryEmail = account.PrimCustomer.PrimaryEmail;

            string PrimaryNotes = "";
            var notelst = noteMgr.GetOpportunityNotes(new Note { OpportunityId = opportunityId }, false);
            if (notelst != null && notelst.Count > 0)
                PrimaryNotes = notelst.Select(x => x.Notes).FirstOrDefault();

            string InstallAddress1 = string.Empty;
            string InstallAddress2 = string.Empty;
            string InstallAddres_CityStateZip = string.Empty;

            CustomerTP SecCustomer;
            string Secondarycontact = "";
            string SecondaryCellPhone = "";
            if (account.SecCustomer.Count >= 1)
            {
                //Select secondary address
                SecCustomer = account.SecCustomer.First();
                Secondarycontact = SecCustomer.FullName;
                SecondaryCellPhone = SecCustomer.CellPhone;
            }
            string SalesAgentName = "";
            string BookedBy = "";
            string AppointmentDate = "";
            string AppointmentTime = "";

            //if (Installer != null)
            //{
            //    if (Installer.AssignedName != null && Installer.AssignedName != "")
            //    {
            //        SalesAgentName = Installer.AssignedName;
            //    }
            //    if (Installer.OrganizerName != null && Installer.OrganizerName != "")
            //    {
            //        BookedBy = Installer.OrganizerName;
            //    }

            //    if (Installer.StartDate != null && Installer.StartDate.ToString() != "")
            //    {
            //        AppointmentDate = Installer.StartDate.ToString("dd/mm/yyyy");
            //        AppointmentTime = Installer.StartDate.ToString("hh:mm:ss");
            //    }
            //}

            //data = data.Replace("{{SalesAgentName}}", SalesAgentName);
            //data = data.Replace("{{BookedBy}}", BookedBy);
            //data = data.Replace("{{ApptDate}}", AppointmentDate);
            //data = data.Replace("{{ApptTime}}", AppointmentTime);

            data = data.Replace("{{Contactfullname}}", PrimaryContact);
            data = data.Replace("{{CompanyName}}", CompanyName);

            data = data.Replace("{{PrimaryCellPhone}}", PrimaryCellPhone);
            data = data.Replace("{{PrimaryHomePhone}}", PrimaryHomePhone);
            data = data.Replace("{{PrimaryWorkPhone}}", PrimaryWorkPhone);
            data = data.Replace("{{PrimaryEmail}}", PrimaryEmail);
            data = data.Replace("{{Secondarycontact}}", Secondarycontact);
            data = data.Replace("{{SecondaryCellPhone}}", SecondaryCellPhone);

            var recentAppointment = calendarMgr.GetRecentAppointmentForOpportunity(opportunityId);

            data = UpdateRecentAppointment(data, recentAppointment);

            var installAddress = account.Addresses
               .Where(a => a.AddressId == opportunity.InstallationAddressId)
               .FirstOrDefault();
            if (installAddress != null)
            {
                InstallAddress1 = installAddress.Address1;
                InstallAddress2 = installAddress.Address2;
                InstallAddres_CityStateZip = installAddress.City
                    + ',' + installAddress.State + ','
                    + installAddress.ZipCode;
            }

            data = data.Replace("{{InstallAddress1}}", InstallAddress1);
            data = data.Replace("{{InstallAddress2}}", InstallAddress2);
            data = data.Replace("{{InstallAddres_CityStateZip}}", InstallAddres_CityStateZip);

            var measurement = measurementMgr.Get((int)opportunity.InstallationAddressId);
            if (measurement == null)
            {
                data = data.Replace("{{MeasurmentData}}", "");
                return data;
            }

            if (SessionManager.BrandId == 1)
            {
                data = GetMeasurementLineItemsBB(measurement.MeasurementBB, data);
            }
            else if (SessionManager.BrandId == 2)
            {
                data = GetMeasurementLineItemsTL(measurement.MeasurementTL, data);
            }
            else
            {
                // TODO: Yet to implement for Concrete craft.
            }

            return data;
        }

        private static string GetMeasurementLineItemsBB(List<MeasurementLineItemBB> lineitems, string data)
        {
            List<MeasurementLineItemBB> bblineitems = null;
            int measurementindex = 1;
            StringBuilder measurementdata = new StringBuilder();
            if (lineitems != null)
            {
                //bblineitems = measurement.MeasurementBB.ToList();
                foreach (var items in lineitems)
                {
                    measurementdata.Append("<tr>");
                    measurementdata.Append("<td>" + items.RoomLocation + "</td>");
                    measurementdata.Append("<td>" + items.RoomName + "</td>");
                    measurementdata.Append("<td>" + items.WindowLocation + "</td>");
                    measurementdata.Append("<td>" + items.MountTypeName + "</td>");
                    measurementdata.Append("<td>" + items.Width + "  " + items.FranctionalValueWidth + "</td>");
                    measurementdata.Append("<td>" + items.Height + "  " + items.FranctionalValueHeight + "</td>");
                    measurementdata.Append("</tr>");
                    measurementindex++;
                }
            }

            data = data.Replace("{{MeasurmentData}}", measurementdata.ToString());

            return data;
        }

        private static string GetMeasurementLineItemsTL(List<MeasurementLineItemTL> lineitems, string data)
        {
            List<MeasurementLineItemBB> bblineitems = null;
            int measurementindex = 1;
            StringBuilder measurementdata = new StringBuilder();

            bool[] TLMeasurementOptions = new bool[5];

            TLMeasurementOptions[0] = true;
            TLMeasurementOptions[1] = true;
            TLMeasurementOptions[2] = true;
            TLMeasurementOptions[3] = true;
            TLMeasurementOptions[4] = true;

            if (lineitems != null)
            {
                //bblineitems = measurement.MeasurementBB.ToList();
                foreach (var item in lineitems)
                {
                    if (AddRow(item.System, TLMeasurementOptions))
                    {
                        measurementdata.Append("<tr>");
                        measurementdata.Append("<td>" + item.System + "</td>");
                        measurementdata.Append("<td>" + item.SystemDescription + "</td>");
                        measurementdata.Append("<td>" + item.StorageType + "</td>");
                        measurementdata.Append("<td>" + item.StorageDescription + "</td>");
                        measurementdata.Append("<td>" + item.Width + "  " + item.FranctionalValueWidth + "</td>");
                        measurementdata.Append("<td>" + item.Height + "  " + item.FranctionalValueHeight + "</td>");
                        measurementdata.Append("<td>" + item.Depth + "  " + item.FranctionalValueDepth + "</td>");

                        measurementdata.Append("<td>" + item.Qty + "</td>");
                        measurementdata.Append("<td>" + item.CustomData + "</td>");
                        measurementdata.Append("<td>" + item.Comments + "</td>");

                        measurementdata.Append("</tr>");
                        measurementindex++;
                    }
                }
            }

            data = data.Replace("{{MeasurmentData}}", measurementdata.ToString());

            return data;
        }

        private static bool AddRow(string system, bool[] TLMeasurementOptions = null)
        {
            bool addrow = false;
            if (system == "Garage")
            {
                if (TLMeasurementOptions[0] == true)
                {
                    addrow = true;
                }
            }
            if (system == "Closet")
            {
                if (TLMeasurementOptions[1] == true)
                {
                    addrow = true;
                }
            }
            if (system == "Office")
            {
                if (TLMeasurementOptions[2] == true)
                {
                    addrow = true;
                }
            }
            if (system == "Floor")
            {
                if (TLMeasurementOptions[3] == true)
                {
                    addrow = true;
                }
            }
            if (system == "ConcreteCraft")
            {
                if (TLMeasurementOptions[4] == true)
                {
                    addrow = true;
                }
            }
            return addrow;
        }

        #endregion Print Touch point

        public static string LeadSheet(int id, datacal.Calendar RecentAppointment, List<string> printoptoins, Zillow zillow = null)
        {
            string data = string.Empty;
            try
            {
                if (SessionManager.BrandId == 1)
                {
                    data = Serializer.Leads.LeadSheetReport.LeadSheet_BudgetBlinds(id, RecentAppointment, printoptoins, zillow);
                }
                else if (SessionManager.BrandId == 2)
                {
                    data = Serializer.Leads.LeadSheetReport.LeadSheet_TailorLiving(id, RecentAppointment, printoptoins);
                }
                else
                {
                    data = Serializer.Leads.LeadSheetReport.LeadSheet_ConcreteCraft(id, RecentAppointment, printoptoins);
                }
                return data;
            }
            catch (Exception ex)
            {
                data = LeadSheetReport.error;
                return data;
            }
        }

        public static string TemplateImageFilePath()
        {
            string Path = string.Empty;
            if (SessionManager.BrandId == 1)
            {
                Path = string.Format("/Templates/Base/Brand/BB/");
            }
            else if (SessionManager.BrandId == 2)
            {
                Path = string.Format("/Templates/Base/Brand/TL/");
            }
            else
            {
                Path = string.Format("/Templates/Base/Brand/CC/");
            }
            string srcPath = HostingEnvironment.MapPath(Path);
            return srcPath;
        }

        public static string LeadSheet_BudgetBlinds(int id, datacal.Calendar RecentAppointment, List<string> printoptoins, Zillow zillow = null)
        {
            string LeadData = string.Empty;
            string Path = string.Empty;

            Path = string.Format("/Templates/Base/Brand/BB/LeadSheet.html");

            string srcFilePath = HostingEnvironment.MapPath(Path);

            Lead lead = leadsMng.Get(id);

            string data = string.Empty;

            bool PrintOrder = false;
            bool addleadcustomer = false;
            bool AddPrintInternalNotes = false;
            bool AddPrintExternalNotes = false;
            bool AddPrintDirectionNotes = false;
            bool AddGoogleMap = false;
            bool AddMeasurementForm = false;
            bool AddOrder = false;
            bool AddInstallationChecklist = false;
            bool AddWarrantyInformation = false;
            if (printoptoins != null)
            {
                PrintOrder = printoptoins.ToList().Where(c => c.Contains("PrintOrder")).SingleOrDefault().Contains("true");
                if (PrintOrder)
                {
                    addleadcustomer = printoptoins.ToList().Where(c => c.Contains("AddleadCustomer")).SingleOrDefault().Contains("true");
                    AddGoogleMap = printoptoins.ToList().Where(c => c.Contains("AddGoogleMap")).SingleOrDefault().Contains("true");
                    AddMeasurementForm = printoptoins.ToList().Where(c => c.Contains("AddPrintExistingMeasurements")).SingleOrDefault().Contains("true");
                    AddPrintInternalNotes = printoptoins.ToList().Where(c => c.Contains("AddPrintInternalNotes")).SingleOrDefault().Contains("true");
                    AddPrintExternalNotes = printoptoins.ToList().Where(c => c.Contains("AddPrintExternalNotes")).SingleOrDefault().Contains("true");
                    AddPrintDirectionNotes = printoptoins.ToList().Where(c => c.Contains("AddPrintDirectionNotes")).SingleOrDefault().Contains("true");

                    //AddOrder = printoptoins.ToList().Where(c => c.Contains("AddOrder")).SingleOrDefault().Contains("true");
                    //AddInstallationChecklist = printoptoins.ToList().Where(c => c.Contains("AddInstallationChecklist")).SingleOrDefault().Contains("true");
                    //AddWarrantyInformation = printoptoins.ToList().Where(c => c.Contains("AddWarrantyInformation")).SingleOrDefault().Contains("true");
                }
                else
                {
                    addleadcustomer = printoptoins.ToList().Where(c => c.Contains("AddleadCustomer")).SingleOrDefault().Contains("true");
                    AddPrintInternalNotes = printoptoins.ToList().Where(c => c.Contains("AddPrintInternalNotes")).SingleOrDefault().Contains("true");
                    AddPrintExternalNotes = printoptoins.ToList().Where(c => c.Contains("AddPrintExternalNotes")).SingleOrDefault().Contains("true");
                    AddPrintDirectionNotes = printoptoins.ToList().Where(c => c.Contains("AddPrintDirectionNotes")).SingleOrDefault().Contains("true");

                    AddGoogleMap = printoptoins.ToList().Where(c => c.Contains("AddGoogleMap")).SingleOrDefault().Contains("true");
                    AddMeasurementForm = printoptoins.ToList().Where(c => c.Contains("AddMeasurementForm")).SingleOrDefault().Contains("true");
                }
            }

            if (addleadcustomer)
            {
                data = System.IO.File.ReadAllText(srcFilePath);
                string PrimaryContact = lead.PrimCustomer.FullName;
                string CompanyName = lead.PrimCustomer.CompanyName;
                AddressTP BillingAddress = null;
                if (lead.Addresses.Count > 0)
                {
                    BillingAddress = lead.Addresses.First();
                }
                string BillingAddress1 = BillingAddress.Address1;
                string BillingAddress2 = BillingAddress.Address2;
                string BillingAddress_CityStateZip = BillingAddress.City + ',' + BillingAddress.State + ',' + BillingAddress.ZipCode;

                string PrimaryCellPhone = GetFormatPhone(lead.PrimCustomer.CellPhone);
                string PrimaryHomePhone = GetFormatPhone(lead.PrimCustomer.HomePhone);
                string PrimaryWorkPhone = GetFormatPhone(lead.PrimCustomer.WorkPhone);
                string PrimaryEmail = lead.PrimCustomer.PrimaryEmail;

                string PrimaryNotes = "";
                var notelst = noteMgr.GetLeadNotes(new Note { LeadId = id }, false);
                if (notelst != null && notelst.Count > 0)
                    PrimaryNotes = notelst.Select(x => x.Notes).FirstOrDefault();

                AddressTP InstallAddress = null;

                string InstallAddress1 = string.Empty;
                string InstallAddress2 = string.Empty;
                string InstallAddres_CityStateZip = string.Empty;
                if (lead.Addresses.Count > 1)
                {
                    //Select secondary address
                    InstallAddress = lead.Addresses.First();
                    InstallAddress1 = InstallAddress.Address1;
                    InstallAddress2 = InstallAddress.Address2;
                    InstallAddres_CityStateZip = InstallAddress.City + ',' + InstallAddress.State + ',' + InstallAddress.ZipCode;
                }

                CustomerTP SecCustomer;
                string Secondarycontact = "";
                string SecondaryCellPhone = "";
                if (lead.SecCustomer.Count > 1)
                {
                    //Select secondary address
                    SecCustomer = lead.SecCustomer.First();
                    Secondarycontact = SecCustomer.FullName;
                    SecondaryCellPhone = SecCustomer.CellPhone;
                }

                SourceList leadsources = null;

                string LeadSourceName = "";
                string LeadChannelName = "";
                if (lead.SourcesList.Count > 0)
                {
                    leadsources = lead.SourcesList.First();
                    LeadSourceName = leadsources.name;
                    LeadChannelName = leadsources.ChannelName;
                }
                //BB - Qualification Questions
                string BB_QQ_NewHome = ""; string BB_QQ_ExistingHome = ""; string BB_QQ_EstimatedClosingDate = ""; string BB_QQ_ProjectLeadTimePreference = "";
                string BB_QQ_ValPak = ""; string BB_QQ_YellowPages = ""; string BB_QQ_RSVP = ""; string BB_QQ_Van = ""; string BB_QQ_Internet = "";
                string BB_QQ_Sources1 = ""; string BB_QQ_Sources2 = ""; string BB_QQ_Sources3 = ""; string BB_QQ_FaceBookPaid = ""; string BB_QQ_Referel = "";
                string BB_QQ_Budget = "";
                string BB_QQ_AnyAppointment = ""; string BB_QQ_ReceivedOtherQuotesYes = ""; string BB_QQ_ReceivedOtherQuotesNo = ""; string BB_QQ_ReceivedOtherQuotesSpecific = "";

                //BB - Project Needs

                string BB_PN_HowManyWindows = "";
                string BB_PN_Color = "";
                string BB_PN_Room_Living = ""; string BB_PN_Room_Dining = ""; string BB_PN_Room_Family = ""; string BB_PN_Room_BedRooms = "";
                string BB_PN_Room_Kitchen = ""; string BB_PN_Room_Bath = ""; string BB_PN_Room_Other = ""; string BB_PN_Room_OtherNote = "";

                string BB_PN_treatment_Wood = ""; string BB_PN_treatment_FauxWood = ""; string BB_PN_treatment_Cellular = ""; string BB_PN_treatment_Shades = ""; string BB_PN_treatment_RollerShades = ""; string BB_PN_treatment_Draperies = "";
                string BB_PN_treatment_Romans = ""; string BB_PN_treatment_Verticals = ""; string BB_PN_treatment_Shutters = ""; string BB_PN_treatment_WovenWoods = ""; string BB_PN_treatment_SolarShades = "";

                string BB_PN_Drapery_Operational = ""; string BB_PN_Drapery_Decorative = "";
                string BB_PN_FrabricPreferences = ""; string BB_PN_SolidSOrPrints = ""; string BB_PN_PreferredStyle = "";

                string BB_PN_Notes;

                var leadquestionAns = lead.LeadQuestionAns;
                if (leadquestionAns != null)
                {
                    foreach (var leadqa in leadquestionAns)
                    {
                        #region QualificationQuestions

                        if (leadqa.QuestionId == 1001 && leadqa.Answer != null && leadqa.Answer != "")
                        {
                            if (leadqa.Answer == "New Home?")
                            {
                                BB_QQ_NewHome = "Checked";
                                BB_QQ_ExistingHome = "";
                            }
                            else if (leadqa.Answer == "Existing Home?")
                            {
                                BB_QQ_NewHome = "";
                                BB_QQ_ExistingHome = "Checked";
                            }
                        }
                        if (leadqa.QuestionId == 1003 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            BB_QQ_EstimatedClosingDate = GetFormatDate(leadqa.Answer);
                        }
                        if (leadqa.QuestionId == 1004 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            BB_QQ_ProjectLeadTimePreference = GetFormatDate(leadqa.Answer);
                        }

                        #endregion QualificationQuestions

                        #region BB

                        //How did u hear about BB
                        if (leadqa.QuestionId == 1005 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            if (leadqa.Answer == "false")
                            {
                                BB_QQ_ValPak = "";
                            }
                            else
                            {
                                BB_QQ_ValPak = "Checked";
                            }
                        }
                        if (leadqa.QuestionId == 1006 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            if (leadqa.Answer == "false")
                            {
                                BB_QQ_YellowPages = "";
                            }
                            else
                            {
                                BB_QQ_YellowPages = "Checked";
                            }
                        }
                        if (leadqa.QuestionId == 1007 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            if (leadqa.Answer == "false")
                            {
                                BB_QQ_RSVP = "";
                            }
                            else
                            {
                                BB_QQ_RSVP = "Checked";
                            }
                        }
                        if (leadqa.QuestionId == 1008 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            if (leadqa.Answer == "false")
                            {
                                BB_QQ_Van = "";
                            }
                            else
                            {
                                BB_QQ_Van = "Checked";
                            }
                        }
                        if (leadqa.QuestionId == 1009 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            if (leadqa.Answer == "false")
                            {
                                BB_QQ_Internet = "";
                            }
                            else
                            {
                                BB_QQ_Internet = "Checked";
                            }
                        }
                        if (leadqa.QuestionId == 1010 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            if (leadqa.Answer == "false")
                            {
                                BB_QQ_Sources1 = "";
                            }
                            else
                            {
                                BB_QQ_Sources1 = "Checked";
                            }
                        }
                        if (leadqa.QuestionId == 1011 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            if (leadqa.Answer == "false")
                            {
                                BB_QQ_Sources2 = "";
                            }
                            else
                            {
                                BB_QQ_Sources2 = "Checked";
                            }
                        }
                        if (leadqa.QuestionId == 1012 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            if (leadqa.Answer == "false")
                            {
                                BB_QQ_Sources3 = "";
                            }
                            else
                            {
                                BB_QQ_Sources3 = "Checked";
                            }
                        }
                        if (leadqa.QuestionId == 1013 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            if (leadqa.Answer == "false")
                            {
                                BB_QQ_FaceBookPaid = "";
                            }
                            else
                            {
                                BB_QQ_FaceBookPaid = "Checked";
                            }
                        }
                        if (leadqa.QuestionId == 1014 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            BB_QQ_Referel = leadqa.Answer;
                        }
                        if (leadqa.QuestionId == 1015 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            BB_QQ_Budget = leadqa.Answer;
                        }
                        if (leadqa.QuestionId == 1016 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            BB_QQ_AnyAppointment = leadqa.Answer;
                        }
                        if (leadqa.QuestionId == 1017 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            if (leadqa.Answer == "No")
                            {
                                BB_QQ_ReceivedOtherQuotesNo = "Checked";
                            }
                            else
                            {
                                BB_QQ_ReceivedOtherQuotesYes = "Checked";
                            }
                        }
                        //if (leadqa.QuestionId == 1018 && (leadqa.Answer != null && leadqa.Answer != ""))
                        //{
                        //    BB_QQ_ReceivedOtherQuotesNo = "Checked";
                        //}
                        if (leadqa.QuestionId == 1019 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            BB_QQ_ReceivedOtherQuotesSpecific = leadqa.Answer;
                        }

                        #endregion BB

                        #region ProjectNeeeds

                        if (leadqa.QuestionId == 1021 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            BB_PN_HowManyWindows = leadqa.Answer;
                        }
                        if (leadqa.QuestionId == 1022 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            BB_PN_Color = leadqa.Answer;
                        }
                        if (leadqa.QuestionId == 1023 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            if (leadqa.Answer == "false")
                            {
                                BB_PN_Room_Living = "";
                            }
                            else
                            {
                                BB_PN_Room_Living = "Checked";
                            }
                        }
                        if (leadqa.QuestionId == 1024 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            if (leadqa.Answer == "false")
                            {
                                BB_PN_Room_Dining = "";
                            }
                            else
                            {
                                BB_PN_Room_Dining = "Checked";
                            }
                        }
                        if (leadqa.QuestionId == 1025 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            if (leadqa.Answer == "false")
                            {
                                BB_PN_Room_Family = "";
                            }
                            else
                            {
                                BB_PN_Room_Family = "Checked";
                            }
                        }
                        if (leadqa.QuestionId == 1026 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            if (leadqa.Answer == "false")
                            {
                                BB_PN_Room_BedRooms = "";
                            }
                            else
                            {
                                BB_PN_Room_BedRooms = "Checked";
                            }
                        }
                        if (leadqa.QuestionId == 1027 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            if (leadqa.Answer == "false")
                            {
                                BB_PN_Room_Kitchen = "";
                            }
                            else
                            {
                                BB_PN_Room_Kitchen = "Checked";
                            }
                        }
                        if (leadqa.QuestionId == 1028 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            if (leadqa.Answer == "false")
                            {
                                BB_PN_Room_Bath = "";
                            }
                            else
                            {
                                BB_PN_Room_Bath = "Checked";
                            }
                        }
                        if (leadqa.QuestionId == 1029 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            if (leadqa.Answer == "false")
                            {
                                BB_PN_Room_Other = "";
                            }
                            else
                            {
                                BB_PN_Room_Other = "Checked";
                            }
                        }
                        if (leadqa.QuestionId == 1030 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            BB_PN_Room_OtherNote = leadqa.Answer;
                        }

                        #endregion ProjectNeeeds

                        #region ProjectNeeeds Treatments

                        if (leadqa.QuestionId == 1031 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            if (leadqa.Answer == "false")
                            {
                                BB_PN_treatment_Wood = "";
                            }
                            else
                            {
                                BB_PN_treatment_Wood = "Checked";
                            }
                        }
                        if (leadqa.QuestionId == 1032 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            if (leadqa.Answer == "false")
                            {
                                BB_PN_treatment_FauxWood = "";
                            }
                            else
                            {
                                BB_PN_treatment_FauxWood = "Checked";
                            }
                        }
                        if (leadqa.QuestionId == 1033 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            if (leadqa.Answer == "false")
                            {
                                BB_PN_treatment_Cellular = "";
                            }
                            else
                            {
                                BB_PN_treatment_Cellular = "Checked";
                            }
                        }
                        if (leadqa.QuestionId == 1034 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            if (leadqa.Answer == "false")
                            {
                                BB_PN_treatment_Shades = "";
                            }
                            else
                            {
                                BB_PN_treatment_Shades = "Checked";
                            }
                        }
                        if (leadqa.QuestionId == 1035 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            if (leadqa.Answer == "false")
                            {
                                BB_PN_treatment_RollerShades = "";
                            }
                            else
                            {
                                BB_PN_treatment_RollerShades = "Checked";
                            }
                        }
                        if (leadqa.QuestionId == 1036 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            if (leadqa.Answer == "false")
                            {
                                BB_PN_treatment_Draperies = "";
                            }
                            else
                            {
                                BB_PN_treatment_Draperies = "Checked";
                            }
                        }
                        if (leadqa.QuestionId == 1037 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            if (leadqa.Answer == "false")
                            {
                                BB_PN_treatment_Romans = "";
                            }
                            else
                            {
                                BB_PN_treatment_Romans = "Checked";
                            }
                        }
                        if (leadqa.QuestionId == 1038 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            if (leadqa.Answer == "false")
                            {
                                BB_PN_treatment_Verticals = "";
                            }
                            else
                            {
                                BB_PN_treatment_Verticals = "Checked";
                            }
                        }
                        if (leadqa.QuestionId == 1039 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            if (leadqa.Answer == "false")
                            {
                                BB_PN_treatment_Shutters = "";
                            }
                            else
                            {
                                BB_PN_treatment_Shutters = "Checked";
                            }
                        }
                        if (leadqa.QuestionId == 1040 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            if (leadqa.Answer == "false")
                            {
                                BB_PN_treatment_WovenWoods = "";
                            }
                            else
                            {
                                BB_PN_treatment_WovenWoods = "Checked";
                            }
                        }
                        if (leadqa.QuestionId == 1041 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            if (leadqa.Answer == "false")
                            {
                                BB_PN_treatment_SolarShades = "";
                            }
                            else
                            {
                                BB_PN_treatment_SolarShades = "Checked";
                            }
                        }

                        #endregion ProjectNeeeds Treatments

                        #region ProjectNeeeds OperationOrDecorative

                        if (leadqa.QuestionId == 1042 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            if (leadqa.Answer == "Operational")
                            {
                                BB_PN_Drapery_Operational = "Checked";
                            }
                            else if (leadqa.Answer == "Decorative")
                            {
                                BB_PN_Drapery_Decorative = "Checked";
                            }
                        }
                        if (leadqa.QuestionId == 1044 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            BB_PN_FrabricPreferences = leadqa.Answer;
                        }
                        if (leadqa.QuestionId == 1045 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            BB_PN_SolidSOrPrints = leadqa.Answer;
                        }
                        if (leadqa.QuestionId == 1046 && (leadqa.Answer != null && leadqa.Answer != ""))
                        {
                            BB_PN_PreferredStyle = leadqa.Answer;
                        }

                        #endregion ProjectNeeeds OperationOrDecorative
                    }
                }
                //Read lead html and append the data
                if (PrimaryContact != null)
                {
                    //{{Contactfullname}}
                    data = data.Replace("{{Contactfullname}}", PrimaryContact);
                }
                else
                {
                    data = data.Replace("{{Contactfullname}}", "");
                }
                if (CompanyName != null)
                {
                    data = data.Replace("{{CompanyName}}", CompanyName);
                }
                else
                {
                    data = data.Replace("{{CompanyName}}", "");
                }
                if (BillingAddress1 != null)
                {
                    data = data.Replace("{{BillingAddress1}}", BillingAddress1);
                }
                else
                {
                    data = data.Replace("{{BillingAddress1}}", "");
                }
                if (BillingAddress2 != null)
                {
                    data = data.Replace("{{BillingAddress2}}", BillingAddress2);
                }
                else
                {
                    data = data.Replace("{{BillingAddress2}}", "");
                }
                if (BillingAddress_CityStateZip != null)
                {
                    data = data.Replace("{{BillingAddress_CityStateZip}}", BillingAddress_CityStateZip);
                }
                else
                {
                    data = data.Replace("{{BillingAddress_CityStateZip}}", "");
                }
                if (PrimaryCellPhone != null)
                {
                    data = data.Replace("{{PrimaryCellPhone}}", PrimaryCellPhone);
                }
                else
                {
                    data = data.Replace("{{PrimaryCellPhone}}", "");
                }
                if (PrimaryHomePhone != null)
                {
                    data = data.Replace("{{PrimaryHomePhone}}", PrimaryHomePhone);
                }
                else
                {
                    data = data.Replace("{{PrimaryHomePhone}}", "");
                }
                if (PrimaryWorkPhone != null)
                {
                    data = data.Replace("{{PrimaryWorkPhone}}", PrimaryWorkPhone);
                }
                else
                {
                    data = data.Replace("{{PrimaryWorkPhone}}", "");
                }
                if (PrimaryEmail != null)
                {
                    data = data.Replace("{{PrimaryEmail}}", PrimaryEmail);
                }
                else
                {
                    data = data.Replace("{{PrimaryEmail}}", "");
                }
                if (PrimaryNotes != null)
                {
                    data = data.Replace("{{PrimaryNotes}}", PrimaryNotes);
                }
                else
                {
                    data = data.Replace("{{PrimaryNotes}}", "");
                }
                if (InstallAddress1 != null)
                {
                    data = data.Replace("{{InstallAddress1}}", InstallAddress1);
                }
                else data = data.Replace("{{InstallAddress1}}", "");
                if (InstallAddress2 != null)
                {
                    data = data.Replace("{{InstallAddress2}}", InstallAddress2);
                }
                else
                {
                    data = data.Replace("{{InstallAddress2}}", "");
                }

                if (InstallAddres_CityStateZip != null)
                {
                    data = data.Replace("{{InstallAddres_CityStateZip}}", InstallAddres_CityStateZip);
                }
                else
                {
                    data = data.Replace("{{InstallAddres_CityStateZip}}", "");
                }
                string leadType = "";
                if (leadType != null)
                {
                    data = data.Replace("{{leadType}}", leadType);
                }
                if (Secondarycontact != null)
                {
                    data = data.Replace("{{Secondarycontact}}", Secondarycontact);
                }
                else
                {
                    data = data.Replace("{{Secondarycontact}}", "");
                }
                if (SecondaryCellPhone != null)
                {
                    data = data.Replace("{{SecondaryCellPhone}}", SecondaryCellPhone);
                }
                else
                {
                    data = data.Replace("{{SecondaryCellPhone}}", "");
                }
                if (LeadSourceName != null)
                {
                    data = data.Replace("{{LeadSourceName}}", LeadSourceName);
                }
                else
                {
                    data = data.Replace("{{LeadSourceName}}", "");
                }

                if (BB_QQ_NewHome != null)
                {
                    data = data.Replace("{{BB_QQ_NewHome}}", BB_QQ_NewHome);
                    data = data.Replace("{{BB_QQ_ExistingHome}}", BB_QQ_ExistingHome);
                }

                if (BB_QQ_EstimatedClosingDate != null)
                {
                    data = data.Replace("{{BB_QQ_EstimatedClosingDate}}", BB_QQ_EstimatedClosingDate);
                }
                data = data.Replace("{{BB_QQ_ProjectLeadTimePreference}}", BB_QQ_ProjectLeadTimePreference);

                data = data.Replace("{{BB_QQ_ProjectLeadTimePreference}}", BB_QQ_ProjectLeadTimePreference);
                data = data.Replace("{{ChkValpak}}", BB_QQ_ValPak);
                data = data.Replace("{{ChkYellowPages}}", BB_QQ_YellowPages);
                data = data.Replace("{{ChkRSVP}}", BB_QQ_RSVP);
                data = data.Replace("{{ChkVan}}", BB_QQ_Van);
                data = data.Replace("{{ChkInternet}}", BB_QQ_Internet);
                data = data.Replace("{{ChkSources1}}", BB_QQ_Sources1);
                data = data.Replace("{{ChkSources2}}", BB_QQ_Sources2);
                data = data.Replace("{{ChkSources3}}", BB_QQ_Sources3);
                data = data.Replace("{{ChkFacebookPaid}}", BB_QQ_FaceBookPaid);
                data = data.Replace("{{BB_QQ_Referel}}", BB_QQ_Referel);
                data = data.Replace("{{BB_QQ_Budget}}", BB_QQ_Budget);
                data = data.Replace("{{BB_QQ_AnyAppointment}}", BB_QQ_AnyAppointment);

                data = data.Replace("{{BB_QQ_ReceivedOtherQuotesYes}}", BB_QQ_ReceivedOtherQuotesYes);
                data = data.Replace("{{BB_QQ_ReceivedOtherQuotesNo}}", BB_QQ_ReceivedOtherQuotesNo);
                data = data.Replace("{{BB_QQ_ReceivedOtherQuotesSpecific}}", BB_QQ_ReceivedOtherQuotesSpecific);

                data = data.Replace("{{BB_PN_HowManyWindows}}", BB_PN_HowManyWindows);
                data = data.Replace("{{BB_PN_Color}}", BB_PN_Color);
                data = data.Replace("{{ChkLiving}}", BB_PN_Room_Living);
                data = data.Replace("{{ChkDining}}", BB_PN_Room_Dining);
                data = data.Replace("{{ChkFamily}}", BB_PN_Room_Family);
                data = data.Replace("{{ChkBed}}", BB_PN_Room_BedRooms);
                data = data.Replace("{{ChkKitchen}}", BB_PN_Room_Kitchen);
                data = data.Replace("{{ChkBath}}", BB_PN_Room_Bath);
                data = data.Replace("{{ChkOther}}", BB_PN_Room_Other);
                data = data.Replace("{{BB_PN_Room_OtherNote}}", BB_PN_Room_OtherNote);

                data = data.Replace("{{ChkWood}}", BB_PN_treatment_Wood);
                data = data.Replace("{{ChkFauxWood}}", BB_PN_treatment_FauxWood);
                data = data.Replace("{{ChkCellular}}", BB_PN_treatment_Cellular);
                data = data.Replace("{{ChkShades}}", BB_PN_treatment_Shades);
                data = data.Replace("{{ChkRollarShades}}", BB_PN_treatment_RollerShades);
                data = data.Replace("{{ChkDraperies}}", BB_PN_treatment_Draperies);
                data = data.Replace("{{ChkRomans}}", BB_PN_treatment_Romans);
                data = data.Replace("{{ChkVerticals}}", BB_PN_treatment_Verticals);
                data = data.Replace("{{ChkShutters}}", BB_PN_treatment_Shutters);
                data = data.Replace("{{ChkWovenWoods}}", BB_PN_treatment_WovenWoods);
                data = data.Replace("{{ChkSolarShades}}", BB_PN_treatment_SolarShades);

                data = data.Replace("{{OptOpetational}}", BB_PN_Drapery_Operational);
                data = data.Replace("{{OptDecorative}}", BB_PN_Drapery_Decorative);

                data = data.Replace("{{BB_PN_FrabricPreferences}}", BB_PN_FrabricPreferences);
                data = data.Replace("{{BB_PN_SolidSOrPrints}}", BB_PN_SolidSOrPrints);
                data = data.Replace("{{BB_PN_PreferredStyle}}", BB_PN_PreferredStyle);

                //Sales Appointment Information
                string SalesAgentName = "";
                string BookedBy = "";
                string AppointmentDate = "";
                string AppointmentTime = "";

                if (RecentAppointment != null)
                {
                    if (RecentAppointment.AssignedName != null && RecentAppointment.AssignedName != "")
                    {
                        SalesAgentName = RecentAppointment.AssignedName;
                    }
                    if (RecentAppointment.OrganizerName != null && RecentAppointment.OrganizerName != "")
                    {
                        BookedBy = RecentAppointment.OrganizerName;
                    }

                    if (RecentAppointment.StartDate != null && RecentAppointment.StartDate.ToString() != "")
                    {
                        //AppointmentDate = RecentAppointment.StartDate.ToString("dd/mm/yyyy");
                        //AppointmentTime = RecentAppointment.StartDate.ToString("hh:mm:ss");
                        AppointmentDate = GetFormatDate(Convert.ToString(RecentAppointment.StartDate));
                        AppointmentTime = GetFormatTime(Convert.ToString(RecentAppointment.StartDate));
                    }
                }
                data = data.Replace("{{SalesAgentName}}", SalesAgentName);
                data = data.Replace("{{BookedBy}}", BookedBy);
                data = data.Replace("{{ApptDate}}", AppointmentDate);
                data = data.Replace("{{ApptTime}}", AppointmentTime);

                //Zillow details

                data = data.Replace("{{yearBuilt}}", zillow.yearBuilt);
                data = data.Replace("{{lotSizeSqFt}}", zillow.lotSizeSqFt);
                data = data.Replace("{{finishedSqFt}}", zillow.finishedSqFt);
                data = data.Replace("{{bedrooms}}", zillow.bedrooms);
                data = data.Replace("{{bathrooms}}", zillow.bathrooms);
                data = data.Replace("{{lastSoldDate}}", zillow.lastSoldDate);
                data = data.Replace("{{lastSoldPrice}}", zillow.lastSoldPrice);
                data = data.Replace("{{zestimate}}", zillow.zestimate);
            }

            //Include Notes
            if (AddPrintInternalNotes || AddPrintExternalNotes || AddPrintDirectionNotes)
            {
                string leadnotes = printLeadNotes(lead.LeadId, AddPrintInternalNotes, AddPrintExternalNotes, AddPrintDirectionNotes);
                if (leadnotes.Length == 0) { leadnotes = "  "; }
                data = data.Replace("{{leadnotes}}", leadnotes.ToString());
            }
            else
            {
                data = data.Replace("{{leadnotes}}", " ");
            }
            //Include GoogleMap
            if (AddGoogleMap)
            {
                string currentFranchiseAddress = SessionManager.CurrentFranchise.Address.ToString();
                string toaddress = string.Empty;
                if (lead.Addresses.Count > 0)
                {
                    if (lead.Addresses.First().Address1.ToString().Length > 1)
                    {
                        toaddress = lead.Addresses.First().Address1 + "," + lead.Addresses.First().City + "," + lead.Addresses.First().State + "," + lead.Addresses.First().ZipCode;
                        string googlemap = PrintGoogleMap(currentFranchiseAddress, toaddress);
                        if (googlemap != null)
                        {
                            data = data + googlemap;
                            data = googlemap;
                        }
                    }
                }
            }

            //Include Measurement form - BB
            if (AddMeasurementForm)
            {
                string measrementdata = printLeadMeasurements(id);
                if (measrementdata.ToString().Length > 0)
                {
                    data = data + measrementdata;
                }
            }

            return data;
        }

        public static string LeadSheet_TailorLiving(int id, HFC.CRM.Data.Calendar RecentAppointment, List<string> printoptoins)
        {
            string LeadData = string.Empty;
            string Path = string.Empty;

            Path = string.Format("/Templates/Base/Brand/TL/LeadSheet.html");

            string srcFilePath = HostingEnvironment.MapPath(Path);

            Lead lead = leadsMng.Get(id);

            string data = System.IO.File.ReadAllText(srcFilePath);

            string PrimaryContact = lead.PrimCustomer.FullName;
            string CompanyName = lead.PrimCustomer.CompanyName;
            AddressTP BillingAddress = null;
            if (lead.Addresses.Count > 0)
            {
                BillingAddress = lead.Addresses.First();
            }
            string BillingAddress1 = BillingAddress.Address1;
            string BillingAddress2 = BillingAddress.Address2;
            string BillingAddress_CityStateZip = BillingAddress.City + ',' + BillingAddress.State + ',' + BillingAddress.ZipCode;

            string PrimaryCellPhone = lead.PrimCustomer.CellPhone;
            string PrimaryHomePhone = lead.PrimCustomer.HomePhone;
            string PrimaryWorkPhone = lead.PrimCustomer.WorkPhone;
            string PrimaryEmail = lead.PrimCustomer.PrimaryEmail;

            string PrimaryNotes = "";
            var notelst = noteMgr.GetLeadNotes(new Note { LeadId = id }, false);
            if (notelst != null && notelst.Count > 0)
                PrimaryNotes = notelst.Select(x => x.Notes).FirstOrDefault();

            AddressTP InstallAddress = null;

            string InstallAddress1 = "";
            string InstallAddress2 = "";
            string InstallAddres_CityStateZip = "";

            if (lead.Addresses.Count > 1)
            {
                //Select secondary address
                InstallAddress = lead.Addresses.First();
                InstallAddress1 = InstallAddress.Address1;
                InstallAddress2 = InstallAddress.Address2;
                InstallAddres_CityStateZip = InstallAddress.City + ',' + InstallAddress.State + ',' + InstallAddress.ZipCode;
            }

            InstallAddress = null;
            if (lead.Addresses.Count > 1)
            {
                //Select secondary address
                InstallAddress = lead.Addresses.First();
            }
            CustomerTP SecCustomer;
            string Secondarycontact = "";
            string SecondaryCellPhone = "";
            if (lead.SecCustomer.Count > 1)
            {
                //Select secondary address
                SecCustomer = lead.SecCustomer.First();
                Secondarycontact = SecCustomer.FullName;
                SecondaryCellPhone = SecCustomer.CellPhone;
            }

            SourceList leadsources = null;

            string LeadSourceName = "";
            string LeadChannelName = "";
            if (lead.SourcesList.Count > 0)
            {
                leadsources = lead.SourcesList.First();
                LeadSourceName = leadsources.name;
                LeadChannelName = leadsources.ChannelName;
            }
            //BB - Qualification Questions
            string TL_QQ_NewHome = ""; string TL_QQ_ExistingHome = ""; string TL_QQ_EstimatedClosingDate = ""; string TL_QQ_ProjectLeadTimePreference = "";
            string TL_QQ_ValPak = ""; string TL_QQ_YellowPages = ""; string TL_QQ_RSVP = ""; string TL_QQ_Van = ""; string TL_QQ_Internet = "";
            string TL_QQ_Sources1 = ""; string TL_QQ_Sources2 = ""; string TL_QQ_Sources3 = ""; string TL_QQ_FaceBookPaid = ""; string TL_QQ_Referel = "";
            string TL_QQ_Budget = "";
            string TL_QQ_AnyAppointment = ""; string TL_QQ_ReceivedOtherQuotesYes = ""; string TL_QQ_ReceivedOtherQuotesNo = ""; string TL_QQ_ReceivedOtherQuotesSpecific = "";

            //BB - Project Needs
            string TL_GarageCabinetry = "";
            string TL_GarageFlooring = ""; string TL_HomeOffice = ""; string TL_Closet = ""; string TL_Laundry = "";
            string TL_Pantry = ""; string TL_Other = "";
            string TL_ProjectDetails = "";
            string TL_PastGarageCabinetry = ""; string TL_PastHomeOffice = ""; string TL_PastCloset = ""; string TL_PastSpecifics = "";
            string TL_WorkStations = ""; string TL_Drawers = ""; string TL_Workbench = ""; string TL_Quantity = ""; string TL_NeedsQuantity = "";
            string TL_OtherNeedsFlooring = ""; string TL_OtherNeedsLaundry = ""; string TL_OtherNeedsSpecifics = "";
            string TL_TaskLightingYes = ""; string TL_TaskLightingNo = ""; string TL_TaskLightingSpecifics = "";
            string TL_Jewellery = ""; string TL_Filing = ""; string TL_OtherDrawersSpecifics = "";
            string TL_FinishPrefernceSpecifics = "";
            string TL_PN_Notes;

            var leadquestionAns = lead.LeadQuestionAns;

            foreach (var leadqa in leadquestionAns)
            {
                #region QualificationQuestions

                if (leadqa.QuestionId == 1047 && leadqa.Answer != null)
                {
                    if (leadqa.Answer == "New Home?")
                    {
                        TL_QQ_NewHome = "Checked";
                        TL_QQ_ExistingHome = "";
                    }
                    else if (leadqa.Answer == "Existing Home?")
                    {
                        TL_QQ_NewHome = "";
                        TL_QQ_ExistingHome = "Checked";
                    }
                }
                if (leadqa.QuestionId == 1049 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_EstimatedClosingDate = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1050 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_ProjectLeadTimePreference = leadqa.Answer;
                }

                #endregion QualificationQuestions

                #region Tailored Living

                //How did u hear about BB
                if (leadqa.QuestionId == 1051 && (leadqa.Answer != null || leadqa.Answer != ""))
                {
                    TL_QQ_ValPak = "Checked";
                }
                if (leadqa.QuestionId == 1052 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_YellowPages = "Checked";
                }
                if (leadqa.QuestionId == 1053 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_RSVP = "Checked";
                }
                if (leadqa.QuestionId == 1054 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_Van = "Checked";
                }
                if (leadqa.QuestionId == 1055 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_Internet = "Checked";
                }
                if (leadqa.QuestionId == 1056 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_Sources1 = "Checked";
                }
                if (leadqa.QuestionId == 1057 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_Sources2 = "Checked";
                }
                if (leadqa.QuestionId == 1058 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_Sources3 = "Checked";
                }
                if (leadqa.QuestionId == 1059 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_FaceBookPaid = "Checked";
                }

                if (leadqa.QuestionId == 1060 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_Referel = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1061 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_Budget = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1062 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_AnyAppointment = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1063 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    if (leadqa.Answer == "Yes")
                    {
                        TL_QQ_ReceivedOtherQuotesYes = "Checked";
                        TL_QQ_ReceivedOtherQuotesNo = "";
                    }
                    else if (leadqa.Answer == "No")
                    {
                        TL_QQ_ReceivedOtherQuotesYes = "";
                        TL_QQ_ReceivedOtherQuotesNo = "Checked";
                    }
                }
                if (leadqa.QuestionId == 1065 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_QQ_ReceivedOtherQuotesSpecific = leadqa.Answer;
                }

                #endregion Tailored Living

                #region DetailsInformation

                if (leadqa.QuestionId == 1068 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_GarageCabinetry = "Checked";
                }
                if (leadqa.QuestionId == 1069 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_GarageFlooring = "Checked";
                }
                if (leadqa.QuestionId == 1070 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_HomeOffice = "Checked";
                }
                if (leadqa.QuestionId == 1071 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_Closet = "Checked";
                }
                if (leadqa.QuestionId == 1072 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_Laundry = "Checked";
                }
                if (leadqa.QuestionId == 1073 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_Pantry = "Checked";
                }
                if (leadqa.QuestionId == 1074 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_Other = leadqa.Answer;
                }

                if (leadqa.QuestionId == 1076 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_ProjectDetails = leadqa.Answer;
                }

                //System Installed in the past
                if (leadqa.QuestionId == 1125 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_PastGarageCabinetry = "Checked";
                }

                if (leadqa.QuestionId == 1126 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_PastHomeOffice = "Checked";
                }
                if (leadqa.QuestionId == 1127 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_PastCloset = "Checked";
                }

                //if (leadqa.QuestionId == 11 && (leadqa.Answer != null && leadqa.Answer != ""))
                //{
                //    TL_PastSpecifics = leadqa.Answer;
                //}

                if (leadqa.QuestionId == 1128 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_WorkStations = "Checked";
                }
                if (leadqa.QuestionId == 1129 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_Drawers = "Checked";
                }
                if (leadqa.QuestionId == 1130 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_Workbench = "Checked";
                }
                if (leadqa.QuestionId == 1131 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_Quantity = "Checked";
                }
                if (leadqa.QuestionId == 1131 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_NeedsQuantity = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1079 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_OtherNeedsFlooring = "Checked";
                }
                if (leadqa.QuestionId == 1080 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_OtherNeedsLaundry = "Checked";
                }
                if (leadqa.QuestionId == 1081 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_OtherNeedsSpecifics = leadqa.Answer;
                }

                if (leadqa.QuestionId == 1083 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    if (leadqa.Answer == "Yes")
                    {
                        TL_TaskLightingYes = "Checked";
                        TL_TaskLightingNo = "";
                    }
                    else if (leadqa.Answer == "No")
                    {
                        TL_TaskLightingYes = "";
                        TL_TaskLightingNo = "Checked";
                    }
                }
                if (leadqa.QuestionId == 1085 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_TaskLightingSpecifics = leadqa.Answer;
                }

                if (leadqa.QuestionId == 1086 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_Jewellery = "Checked";
                }
                if (leadqa.QuestionId == 1087 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_Filing = "Checked";
                }
                if (leadqa.QuestionId == 1088 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_OtherDrawersSpecifics = leadqa.Answer;
                }

                if (leadqa.QuestionId == 1089 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    TL_FinishPrefernceSpecifics = leadqa.Answer;
                }

                #endregion DetailsInformation
            }

            //Read lead html and append the data
            if (PrimaryContact != null)
            {
                //{{Contactfullname}}
                data = data.Replace("{{Contactfullname}}", PrimaryContact);
            }
            else
            {
                data = data.Replace("{{Contactfullname}}", "");
            }
            if (CompanyName != null)
            {
                data = data.Replace("{{CompanyName}}", CompanyName);
            }
            else
            {
                data = data.Replace("{{CompanyName}}", "");
            }
            if (BillingAddress1 != null)
            {
                data = data.Replace("{{BillingAddress1}}", BillingAddress1);
            }
            else
            {
                data = data.Replace("{{BillingAddress1}}", "");
            }
            if (BillingAddress2 != null)
            {
                data = data.Replace("{{BillingAddress2}}", BillingAddress2);
            }
            else
            {
                data = data.Replace("{{BillingAddress2}}", "");
            }
            if (BillingAddress_CityStateZip != null)
            {
                data = data.Replace("{{BillingAddress_CityStateZip}}", BillingAddress_CityStateZip);
            }
            else
            {
                data = data.Replace("{{BillingAddress_CityStateZip}}", "");
            }
            if (PrimaryCellPhone != null)
            {
                data = data.Replace("{{PrimaryCellPhone}}", PrimaryCellPhone);
            }
            else
            {
                data = data.Replace("{{PrimaryCellPhone}}", "");
            }
            if (PrimaryHomePhone != null)
            {
                data = data.Replace("{{PrimaryHomePhone}}", PrimaryHomePhone);
            }
            else
            {
                data = data.Replace("{{PrimaryHomePhone}}", "");
            }
            if (PrimaryWorkPhone != null)
            {
                data = data.Replace("{{PrimaryWorkPhone}}", PrimaryWorkPhone);
            }
            else
            {
                data = data.Replace("{{PrimaryWorkPhone}}", "");
            }
            if (PrimaryEmail != null)
            {
                data = data.Replace("{{PrimaryEmail}}", PrimaryEmail);
            }
            else
            {
                data = data.Replace("{{PrimaryEmail}}", "");
            }
            if (PrimaryNotes != null)
            {
                data = data.Replace("{{PrimaryNotes}}", PrimaryNotes);
            }
            else
            {
                data = data.Replace("{{PrimaryNotes}}", "");
            }
            if (InstallAddress1 != null)
            {
                data = data.Replace("{{InstallAddress1}}", InstallAddress1);
            }
            else data = data.Replace("{{InstallAddress1}}", "");
            if (InstallAddress2 != null)
            {
                data = data.Replace("{{InstallAddress2}}", InstallAddress2);
            }
            else
            {
                data = data.Replace("{{InstallAddress2}}", "");
            }

            if (InstallAddres_CityStateZip != null)
            {
                data = data.Replace("{{InstallAddres_CityStateZip}}", InstallAddres_CityStateZip);
            }
            else
            {
                data = data.Replace("{{InstallAddres_CityStateZip}}", "");
            }
            string leadType = "";
            if (leadType != null)
            {
                data = data.Replace("{{leadType}}", leadType);
            }
            if (Secondarycontact != null)
            {
                data = data.Replace("{{Secondarycontact}}", Secondarycontact);
            }
            else
            {
                data = data.Replace("{{Secondarycontact}}", "");
            }
            if (SecondaryCellPhone != null)
            {
                data = data.Replace("{{SecondaryCellPhone}}", SecondaryCellPhone);
            }
            else
            {
                data = data.Replace("{{SecondaryCellPhone}}", "");
            }
            if (LeadSourceName != null)
            {
                data = data.Replace("{{LeadSourceName}}", LeadSourceName);
            }
            else
            {
                data = data.Replace("{{LeadSourceName}}", "");
            }

            if (TL_QQ_NewHome != null)
            {
                data = data.Replace("{{TL_QQ_NewHome}}", TL_QQ_NewHome);
                data = data.Replace("{{TL_QQ_ExistingHome}}", TL_QQ_ExistingHome);
            }

            if (TL_QQ_EstimatedClosingDate != null)
            {
                data = data.Replace("{{TL_QQ_EstimatedClosingDate}}", TL_QQ_EstimatedClosingDate);
            }
            data = data.Replace("{{TL_QQ_ProjectLeadTimePreference}}", TL_QQ_ProjectLeadTimePreference);

            data = data.Replace("{{TL_QQ_ProjectLeadTimePreference}}", TL_QQ_ProjectLeadTimePreference);
            data = data.Replace("{{ChkValpak}}", TL_QQ_ValPak);
            data = data.Replace("{{ChkYellowPages}}", TL_QQ_YellowPages);
            data = data.Replace("{{ChkRSVP}}", TL_QQ_RSVP);
            data = data.Replace("{{ChkVan}}", TL_QQ_Van);
            data = data.Replace("{{ChkInternet}}", TL_QQ_Internet);
            data = data.Replace("{{ChkSources1}}", TL_QQ_Sources1);
            data = data.Replace("{{ChkSources2}}", TL_QQ_Sources2);
            data = data.Replace("{{ChkSources3}}", TL_QQ_Sources3);

            data = data.Replace("{{TL_QQ_Referel}}", TL_QQ_Referel);
            data = data.Replace("{{TL_QQ_Budget}}", TL_QQ_Budget);
            data = data.Replace("{{TL_QQ_AnyAppointment}}", TL_QQ_AnyAppointment);

            data = data.Replace("{{TL_QQ_ReceivedOtherQuotesYes}}", TL_QQ_ReceivedOtherQuotesYes);
            data = data.Replace("{{TL_QQ_ReceivedOtherQuotesNo}}", TL_QQ_ReceivedOtherQuotesNo);
            data = data.Replace("{{TL_QQ_ReceivedOtherQuotesSpecific}}", TL_QQ_ReceivedOtherQuotesSpecific);

            data = data.Replace("{{ChkGarageCabinetry}}", TL_GarageCabinetry);
            data = data.Replace("{{ChkGarageFlooring}}", TL_GarageFlooring);
            data = data.Replace("{{ChkHomeOffice}}", TL_HomeOffice);
            data = data.Replace("{{ChkCloset}}", TL_Closet);
            data = data.Replace("{{ChkLaundry}}", TL_Laundry);
            data = data.Replace("{{ChkPantry}}", TL_Pantry);
            data = data.Replace("{{ChkOther}}", TL_Other);

            //data = data.Replace("{{ChkOther}}", TL_Other); textbox

            //data = data.Replace("{{ChkOther}}", TL_ProjectDetails); Project details

            data = data.Replace("{{ChkPastGarageCabinetry}}", TL_PastGarageCabinetry);
            data = data.Replace("{{ChkPastHomeOffice}}", TL_PastHomeOffice);
            data = data.Replace("{{ChkPastCloset}}", TL_PastCloset);
            data = data.Replace("{{PastSpecifics}}", TL_PastSpecifics);

            data = data.Replace("{{ChkWorkStations}}", TL_WorkStations);
            data = data.Replace("{{ChkWorkbench}}", TL_Workbench);
            data = data.Replace("{{ChkDrawers}}", TL_Drawers);
            data = data.Replace("{{NeedsQuantity}}", TL_NeedsQuantity);

            data = data.Replace("{{ChkOtherNeedsFlooring}}", TL_OtherNeedsFlooring);
            data = data.Replace("{{ChkOtherNeedsLaundry}}", TL_OtherNeedsLaundry);
            data = data.Replace("{{OtherNeedsSpecifics}}", TL_OtherNeedsSpecifics);

            data = data.Replace("{{ChkTaskLightingYes}}", TL_TaskLightingYes);
            data = data.Replace("{{ChkTaskLightingNo}}", TL_TaskLightingNo);
            data = data.Replace("{{TaskLightingSpecifics}}", TL_TaskLightingSpecifics);

            data = data.Replace("{{ChkJewellery}}", TL_Jewellery);
            data = data.Replace("{{ChkFiling}}", TL_Filing);
            data = data.Replace("{{OtherDrawerSpecifics}}", TL_OtherDrawersSpecifics);

            data = data.Replace("{{FinishPreferenceSpecifics}}", TL_FinishPrefernceSpecifics);

            //Sales Appointment Information

            string SalesAgentName = "";
            string BookedBy = "";
            string AppointmentDate = "";
            string AppointmentTime = "";

            if (RecentAppointment != null)
            {
                if (RecentAppointment.AssignedName != null && RecentAppointment.AssignedName != "")
                {
                    SalesAgentName = RecentAppointment.AssignedName;
                }
                if (RecentAppointment.OrganizerName != null && RecentAppointment.OrganizerName != "")
                {
                    BookedBy = RecentAppointment.OrganizerName;
                }

                if (RecentAppointment.StartDate != null && RecentAppointment.StartDate.ToString() != "")
                {
                    // AppointmentDate = RecentAppointment.StartDate.ToString();
                    //AppointmentDate = RecentAppointment.StartDate.ToString("dd/mm/yyyy");
                    //AppointmentTime = RecentAppointment.StartDate.ToString("hh:mm:ss");
                    AppointmentDate = GetFormatDate(Convert.ToString(RecentAppointment.StartDate));
                    AppointmentTime = GetFormatTime(Convert.ToString(RecentAppointment.StartDate));
                }
            }

            data = data.Replace("{{SalesAgentName}}", SalesAgentName);
            data = data.Replace("{{BookedBy}}", BookedBy);
            data = data.Replace("{{ApptDate}}", AppointmentDate);
            data = data.Replace("{{ApptTime}}", AppointmentTime);

            string leadnotes = printLeadNotes(lead.LeadId);
            if (leadnotes.Length == 0) { leadnotes = "  "; }
            data = data.Replace("{{leadnotes}}", leadnotes.ToString());
            return data;
        }

        public static string LeadSheet_ConcreteCraft(int id, HFC.CRM.Data.Calendar RecentAppointment, List<string> printoptoins)
        {
            string LeadData = string.Empty;
            string Path = string.Empty;

            Path = string.Format("/Templates/Base/Brand/CC/LeadSheet.html");

            string srcFilePath = HostingEnvironment.MapPath(Path);

            Lead lead = leadsMng.Get(id);

            string data = System.IO.File.ReadAllText(srcFilePath);

            string PrimaryContact = lead.PrimCustomer.FullName;
            string CompanyName = lead.PrimCustomer.CompanyName;
            AddressTP BillingAddress = null;
            if (lead.Addresses.Count > 0)
            {
                BillingAddress = lead.Addresses.First();
            }
            string BillingAddress1 = BillingAddress.Address1;
            string BillingAddress2 = BillingAddress.Address2;
            string BillingAddress_CityStateZip = BillingAddress.City + ',' + BillingAddress.State + ',' + BillingAddress.ZipCode;

            string PrimaryCellPhone = lead.PrimCustomer.CellPhone;
            string PrimaryHomePhone = lead.PrimCustomer.HomePhone;
            string PrimaryWorkPhone = lead.PrimCustomer.WorkPhone;
            string PrimaryEmail = lead.PrimCustomer.PrimaryEmail;

            string PrimaryNotes = "";
            var notelst = noteMgr.GetLeadNotes(new Note { LeadId = id }, false);
            if (notelst != null && notelst.Count > 0)
                PrimaryNotes = notelst.Select(x => x.Notes).FirstOrDefault();

            AddressTP InstallAddress = null;

            string InstallAddress1 = "";
            string InstallAddress2 = "";
            string InstallAddres_CityStateZip = "";

            if (lead.Addresses.Count > 1)
            {
                //Select secondary address
                InstallAddress = lead.Addresses.First();
                InstallAddress1 = InstallAddress.Address1;
                InstallAddress2 = InstallAddress.Address2;
                InstallAddres_CityStateZip = InstallAddress.City + ',' + InstallAddress.State + ',' + InstallAddress.ZipCode;
            }

            InstallAddress = null;
            if (lead.Addresses.Count > 1)
            {
                //Select secondary address
                InstallAddress = lead.Addresses.First();
            }
            CustomerTP SecCustomer;
            string Secondarycontact = "";
            string SecondaryCellPhone = "";
            if (lead.SecCustomer.Count > 1)
            {
                //Select secondary address
                SecCustomer = lead.SecCustomer.First();
                Secondarycontact = SecCustomer.FullName;
                SecondaryCellPhone = SecCustomer.CellPhone;
            }

            SourceList leadsources = null;

            string LeadSourceName = "";
            string LeadChannelName = "";
            if (lead.SourcesList.Count > 0)
            {
                leadsources = lead.SourcesList.First();
                LeadSourceName = leadsources.name;
                LeadChannelName = leadsources.ChannelName;
            }
            //BB - Qualification Questions
            string CC_QQ_NewHome = ""; string CC_QQ_ExistingHome = ""; string CC_QQ_EstimatedClosingDate = ""; string CC_QQ_ProjectLeadTimePreference = "";
            string CC_QQ_ValPak = ""; string CC_QQ_YellowPages = ""; string CC_QQ_RSVP = ""; string CC_QQ_Van = ""; string CC_QQ_Internet = "";
            string CC_QQ_Sources1 = ""; string CC_QQ_Sources2 = ""; string CC_QQ_Sources3 = ""; string CC_QQ_FaceBookPaid = ""; string CC_QQ_Referel = "";
            string CC_QQ_Budget = "";
            string CC_QQ_AnyAppointment = ""; string CC_QQ_ReceivedOtherQuotesYes = ""; string CC_QQ_ReceivedOtherQuotesNo = ""; string CC_QQ_ReceivedOtherQuotesSpecific = "";
            //BB - Project Needs
            string CC_StampedConcrete = ""; string CC_ResurfaceConcrete = ""; string CC_StainedConcrete = ""; string CC_ColorRestoreSystem = "";
            string CC_ColorSealSystem = ""; string CC_PlainConcrete = ""; string CC_CleanAndReseal = ""; string CC_ColoredConcrete = "";
            string CC_ProjectDetails = "";
            string CC_ExistingSlabYes = ""; string CC_ExistingSlabNo = ""; string CC_AprroximateSizeOrShape = "";
            string CC_OntheSurface = "";
            string CC_PN_Notes;

            var leadquestionAns = lead.LeadQuestionAns;

            foreach (var leadqa in leadquestionAns)
            {
                #region QualificationQuestions

                if (leadqa.QuestionId == 1090 && leadqa.Answer != null)
                {
                    if (leadqa.Answer == "New Home?")
                    {
                        CC_QQ_NewHome = "Checked";
                        CC_QQ_ExistingHome = "";
                    }
                    else if (leadqa.Answer == "Existing Home?")
                    {
                        CC_QQ_NewHome = "";
                        CC_QQ_ExistingHome = "Checked";
                    }
                }
                if (leadqa.QuestionId == 1092 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_EstimatedClosingDate = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1093 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_ProjectLeadTimePreference = leadqa.Answer;
                }

                #endregion QualificationQuestions

                #region Tailored Living

                //How did u hear about BB
                if (leadqa.QuestionId == 1094 && (leadqa.Answer != null || leadqa.Answer != ""))
                {
                    CC_QQ_ValPak = "Checked";
                }
                if (leadqa.QuestionId == 1095 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_YellowPages = "Checked";
                }
                if (leadqa.QuestionId == 1096 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_RSVP = "Checked";
                }
                if (leadqa.QuestionId == 1097 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_Van = "Checked";
                }
                if (leadqa.QuestionId == 1098 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_Internet = "Checked";
                }
                if (leadqa.QuestionId == 1099 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_Sources1 = "Checked";
                }
                if (leadqa.QuestionId == 1100 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_Sources2 = "Checked";
                }
                if (leadqa.QuestionId == 1101 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_Sources3 = "Checked";
                }
                if (leadqa.QuestionId == 1102 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_FaceBookPaid = "Checked";
                }

                if (leadqa.QuestionId == 1103 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_Referel = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1104 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_Budget = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1105 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_AnyAppointment = leadqa.Answer;
                }
                if (leadqa.QuestionId == 1106 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    if (leadqa.Answer == "Yes")
                    {
                        CC_QQ_ReceivedOtherQuotesYes = "Checked";
                        CC_QQ_ReceivedOtherQuotesNo = "";
                    }
                    else if (leadqa.Answer == "No")
                    {
                        CC_QQ_ReceivedOtherQuotesYes = "";
                        CC_QQ_ReceivedOtherQuotesNo = "Checked";
                    }
                }

                if (leadqa.QuestionId == 1108 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_QQ_ReceivedOtherQuotesSpecific = leadqa.Answer;
                }
                //Other Comments?

                #endregion Tailored Living

                #region Project Needs

                if (leadqa.QuestionId == 1111 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_StampedConcrete = "Checked";
                }
                if (leadqa.QuestionId == 1112 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_ResurfaceConcrete = "Checked";
                }
                if (leadqa.QuestionId == 1113 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_StainedConcrete = "Checked";
                }
                if (leadqa.QuestionId == 1114 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_ColorRestoreSystem = "Checked";
                }
                if (leadqa.QuestionId == 1115 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_ColorSealSystem = "Checked";
                }
                if (leadqa.QuestionId == 1116 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_PlainConcrete = "Checked";
                }
                if (leadqa.QuestionId == 1117 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_CleanAndReseal = "Checked";
                }
                if (leadqa.QuestionId == 1118 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_ColoredConcrete = "Checked";
                }

                if (leadqa.QuestionId == 1119 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_ProjectDetails = leadqa.Answer;
                }

                if (leadqa.QuestionId == 1121 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    if (leadqa.Answer == "Yes")
                    {
                        CC_ExistingSlabYes = "Checked";
                        CC_ExistingSlabNo = "";
                    }
                    else if (leadqa.Answer == "No")
                    {
                        CC_ExistingSlabYes = "";
                        CC_ExistingSlabNo = "Checked";
                    }
                }
                if (leadqa.QuestionId == 1123 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_AprroximateSizeOrShape = leadqa.Answer;
                }

                if (leadqa.QuestionId == 1124 && (leadqa.Answer != null && leadqa.Answer != ""))
                {
                    CC_OntheSurface = leadqa.Answer;
                }

                #endregion Project Needs

            }

            //Read lead html and append the data
            if (PrimaryContact != null)
            {
                //{{Contactfullname}}
                data = data.Replace("{{Contactfullname}}", PrimaryContact);
            }
            else
            {
                data = data.Replace("{{Contactfullname}}", "");
            }
            if (CompanyName != null)
            {
                data = data.Replace("{{CompanyName}}", CompanyName);
            }
            else
            {
                data = data.Replace("{{CompanyName}}", "");
            }
            if (BillingAddress1 != null)
            {
                data = data.Replace("{{BillingAddress1}}", BillingAddress1);
            }
            else
            {
                data = data.Replace("{{BillingAddress1}}", "");
            }
            if (BillingAddress2 != null)
            {
                data = data.Replace("{{BillingAddress2}}", BillingAddress2);
            }
            else
            {
                data = data.Replace("{{BillingAddress2}}", "");
            }
            if (BillingAddress_CityStateZip != null)
            {
                data = data.Replace("{{BillingAddress_CityStateZip}}", BillingAddress_CityStateZip);
            }
            else
            {
                data = data.Replace("{{BillingAddress_CityStateZip}}", "");
            }
            if (PrimaryCellPhone != null)
            {
                data = data.Replace("{{PrimaryCellPhone}}", PrimaryCellPhone);
            }
            else
            {
                data = data.Replace("{{PrimaryCellPhone}}", "");
            }
            if (PrimaryHomePhone != null)
            {
                data = data.Replace("{{PrimaryHomePhone}}", PrimaryHomePhone);
            }
            else
            {
                data = data.Replace("{{PrimaryHomePhone}}", "");
            }
            if (PrimaryWorkPhone != null)
            {
                data = data.Replace("{{PrimaryWorkPhone}}", PrimaryWorkPhone);
            }
            else
            {
                data = data.Replace("{{PrimaryWorkPhone}}", "");
            }
            if (PrimaryEmail != null)
            {
                data = data.Replace("{{PrimaryEmail}}", PrimaryEmail);
            }
            else
            {
                data = data.Replace("{{PrimaryEmail}}", "");
            }
            if (PrimaryNotes != null)
            {
                data = data.Replace("{{PrimaryNotes}}", PrimaryNotes);
            }
            else
            {
                data = data.Replace("{{PrimaryNotes}}", "");
            }
            if (InstallAddress1 != null)
            {
                data = data.Replace("{{InstallAddress1}}", InstallAddress1);
            }
            else data = data.Replace("{{InstallAddress1}}", "");
            if (InstallAddress2 != null)
            {
                data = data.Replace("{{InstallAddress2}}", InstallAddress2);
            }
            else
            {
                data = data.Replace("{{InstallAddress2}}", "");
            }

            if (InstallAddres_CityStateZip != null)
            {
                data = data.Replace("{{InstallAddres_CityStateZip}}", InstallAddres_CityStateZip);
            }
            else
            {
                data = data.Replace("{{InstallAddres_CityStateZip}}", "");
            }
            string leadType = "";
            if (leadType != null)
            {
                data = data.Replace("{{leadType}}", leadType);
            }
            if (Secondarycontact != null)
            {
                data = data.Replace("{{Secondarycontact}}", Secondarycontact);
            }
            else
            {
                data = data.Replace("{{Secondarycontact}}", "");
            }
            if (SecondaryCellPhone != null)
            {
                data = data.Replace("{{SecondaryCellPhone}}", SecondaryCellPhone);
            }
            else
            {
                data = data.Replace("{{SecondaryCellPhone}}", "");
            }
            if (LeadSourceName != null)
            {
                data = data.Replace("{{LeadSourceName}}", LeadSourceName);
            }
            else
            {
                data = data.Replace("{{LeadSourceName}}", "");
            }
            ////if (CC_QQ_NewHome != null)
            ////{
            ////    data = data.Replace("{{CC_QQ_NewHome}}", CC_QQ_NewHome);
            ////    data = data.Replace("{{CC_QQ_ExistingHome}}", CC_QQ_ExistingHome);
            ////}

            ////if (CC_QQ_EstimatedClosingDate != null)
            ////{
            ////    data = data.Replace("{{CC_QQ_EstimatedClosingDate}}", CC_QQ_EstimatedClosingDate);
            ////}
            ////data = data.Replace("{{CC_QQ_ProjectLeadTimePreference}}", CC_QQ_ProjectLeadTimePreference);

            ////data = data.Replace("{{CC_QQ_ProjectLeadTimePreference}}", CC_QQ_ProjectLeadTimePreference);
            ////data = data.Replace("{{ChkValpak}}", CC_QQ_ValPak);
            ////data = data.Replace("{{ChkYellowPages}}", CC_QQ_YellowPages);
            ////data = data.Replace("{{ChkRSVP}}", CC_QQ_RSVP);
            ////data = data.Replace("{{ChkVan}}", CC_QQ_Van);
            ////data = data.Replace("{{ChkInternet}}", CC_QQ_Internet);
            ////data = data.Replace("{{ChkSources1}}", CC_QQ_Sources1);
            ////data = data.Replace("{{ChkSources2}}", CC_QQ_Sources2);
            ////data = data.Replace("{{ChkSources3}}", CC_QQ_Sources3);
            ////data = data.Replace("{{ChkFacebookPaid}}", CC_QQ_FaceBookPaid);

            ////data = data.Replace("{{CC_QQ_Referel}}", CC_QQ_Referel);
            ////data = data.Replace("{{CC_QQ_Budget}}", CC_QQ_Budget);
            ////data = data.Replace("{{CC_QQ_AnyAppointment}}", CC_QQ_AnyAppointment);

            ////data = data.Replace("{{CC_QQ_ReceivedOtherQuotesYes}}", CC_QQ_ReceivedOtherQuotesYes);
            ////data = data.Replace("{{CC_QQ_ReceivedOtherQuotesNo}}", CC_QQ_ReceivedOtherQuotesNo);
            ////data = data.Replace("{{CC_QQ_ReceivedOtherQuotesSpecific}}", CC_QQ_ReceivedOtherQuotesSpecific);

            ////data = data.Replace("{{ChkStampedConcrete}}", CC_StampedConcrete);
            ////data = data.Replace("{{ChkResurfaceConcrete}}", CC_ResurfaceConcrete);
            ////data = data.Replace("{{ChkStainedConcrete}}", CC_StainedConcrete);
            ////data = data.Replace("{{ChkColorRestoreSystem}}", CC_ColorRestoreSystem);

            ////data = data.Replace("{{ChkColorSealSystem}}", CC_ColorSealSystem);
            ////data = data.Replace("{{ChkPlainConcrete}}", CC_PlainConcrete);
            ////data = data.Replace("{{ChkCleanAndReseal}}", CC_CleanAndReseal);
            ////data = data.Replace("{{ChkColoredConcrete}}", CC_ColoredConcrete);

            ////data = data.Replace("{{ProjectDetails}}", CC_ProjectDetails);

            ////data = data.Replace("{{CC_QQ_ExistingSlabYes}}", CC_ExistingSlabYes);
            ////data = data.Replace("{{CC_QQ_ExistingSlabNo}}", CC_ExistingSlabNo);
            ////data = data.Replace("{{CC_QQ_AprroximateSizeOrShape}}", CC_AprroximateSizeOrShape);

            ////data = data.Replace("{{CC_QQ_OntheSurface}}", CC_OntheSurface);

            //Sales Appointment Information
            string SalesAgentName = "";
            string BookedBy = "";
            string AppointmentDate = "";
            string AppointmentTime = "";

            if (RecentAppointment != null)
            {
                if (RecentAppointment.AssignedName != null && RecentAppointment.AssignedName != "")
                {
                    SalesAgentName = RecentAppointment.AssignedName;
                }
                if (RecentAppointment.OrganizerName != null && RecentAppointment.OrganizerName != "")
                {
                    BookedBy = RecentAppointment.OrganizerName;
                }

                if (RecentAppointment.StartDate != null && RecentAppointment.StartDate.ToString() != "")
                {
                    AppointmentDate = GetFormatDate(Convert.ToString(RecentAppointment.StartDate));
                    AppointmentTime = GetFormatTime(Convert.ToString(RecentAppointment.StartDate));
                }
            }

            data = data.Replace("{{SalesAgentName}}", SalesAgentName);
            data = data.Replace("{{BookedBy}}", BookedBy);
            data = data.Replace("{{ApptDate}}", AppointmentDate);
            data = data.Replace("{{ApptTime}}", AppointmentTime);

            string leadnotes = printLeadNotes(lead.LeadId);
            if (leadnotes.Length == 0) { leadnotes = "  "; }
            data = data.Replace("{{leadnotes}}", leadnotes.ToString());

            return data;
        }
        public static string printLeadNotes(int id = 0, bool internalnotes = false, bool externalnotes = false, bool directionnotes = false)
        {
            //Append Lead Notes - Internal and external
            var leadnotes = leadsMng.PrintLeadNotes(id, internalnotes, externalnotes);
            StringBuilder leadNotesdata = new StringBuilder();
            if (leadnotes != null)
            {
                foreach (var items in leadnotes)
                {
                    leadNotesdata.Append("<tr>");
                    leadNotesdata.Append("<td>" + items.LeadId + "</td>");
                    leadNotesdata.Append("<td>" + items.Title + "</td>");
                    leadNotesdata.Append("<td>" + items.Category + "</td>");
                    leadNotesdata.Append("<td>" + items.Note + "</td>");
                    leadNotesdata.Append("</tr>");
                }
            }
            return leadNotesdata.ToString();
        }

        private static string printAccountNotes(int id = 0)
        {
            //Append Lead AccountNotes - Internal and external
            var accountnotes = accountsMgr.PrintAccountNotes(id);
            StringBuilder accountNotesdata = new StringBuilder();
            if (accountnotes != null)
            {
                foreach (var items in accountnotes)
                {
                    accountNotesdata.Append("<tr>");
                    accountNotesdata.Append("<td>" + items.LeadId + "</td>");
                    accountNotesdata.Append("<td>" + items.Title + "</td>");
                    accountNotesdata.Append("<td>" + items.Category + "</td>");
                    accountNotesdata.Append("<td>" + items.Note + "</td>");
                    accountNotesdata.Append("</tr>");
                }
            }
            return accountNotesdata.ToString();
        }

        private static string GetNotes(List<Note> source
            , bool internalnotes = false
            , bool externalnotes = false
            , bool directionnotes = false
            , int id = 0)
        {
            //
            var notes = new List<Note>();
            if (internalnotes)
            {
                var obj = source.Where(n => n.TypeEnum == NoteTypeEnum.Internal).ToList();
                notes.AddRange(obj);
            }
            if (externalnotes)
            {
                var obj = source.Where(n => n.TypeEnum == NoteTypeEnum.External).ToList();
                notes.AddRange(obj);
            }
            if (directionnotes)
            {
                var obj = source.Where(n => n.TypeEnum == NoteTypeEnum.Direction).ToList();
                notes.AddRange(obj);
            }

            StringBuilder leadNotesdata = new StringBuilder();
            if (notes != null)
            {
                notes = notes.OrderByDescending(x => x.CreatedOn).ToList();

                foreach (var items in notes)
                {
                    leadNotesdata.Append("<tr>");
                    // Notes – do not need to display the ID for the list - David
                    // leadNotesdata.Append("<td>" + id + "</td>");
                    leadNotesdata.Append("<td>" + items.Title + "</td>");
                    leadNotesdata.Append("<td>" + items.CategoryName + "</td>");
                    leadNotesdata.Append("<td>" + items.Notes + "</td>");
                    leadNotesdata.Append("</tr>");
                }
            }
            return leadNotesdata.ToString();
        }

        public static string printLeadNotesTP(int id = 0
            , bool internalnotes = false
            , bool externalnotes = false
            , bool directionnotes = false)
        {
            if (internalnotes == false
                && externalnotes == false
                && directionnotes == false)
            {
                return "";
            }

            var temp = noteMgr.GetLeadNotes(new Note { LeadId = id }, false);
            var result = GetNotes(temp, internalnotes, externalnotes, directionnotes, id);
            return result;
        }

        public static string printAccountNotesTP(int id = 0
            , bool internalnotes = false
            , bool externalnotes = false
            , bool directionnotes = false)
        {
            if (internalnotes == false
                && externalnotes == false
                && directionnotes == false)
            {
                return "";
            }

            var temp = noteMgr.GetAccountNotes(new Note { AccountId = id }, false);
            var result = GetNotes(temp, internalnotes, externalnotes, directionnotes, id);
            return result;
        }

        public static string printOpportunityNotesTP(int id = 0
            , bool internalnotes = false
            , bool externalnotes = false
            , bool directionnotes = false)
        {
            if (internalnotes == false
                && externalnotes == false
                && directionnotes == false)
            {
                return "";
            }

            var temp = noteMgr.GetOpportunityNotes(new Note { OpportunityId = id }, false);
            var result = GetNotes(temp, internalnotes, externalnotes, directionnotes, id);
            return result;
        }

        public static string printOrderNotesTP(int id = 0
            , bool internalnotes = false
            , bool externalnotes = false
            , bool directionnotes = false)
        {
            if (internalnotes == false
                && externalnotes == false
                && directionnotes == false)
            {
                return "";
            }

            var temp = noteMgr.GetOrderNotes(new Note { OrderId = id }, false);
            var result = GetNotes(temp, internalnotes, externalnotes, directionnotes, id);
            return result;
        }

        public static string PrintGoogleMap(string fromaddress, string toaddress)
        {
            try
            {
                EventLogger.LogEvent(fromaddress + " " + toaddress, "PrintGoogleMap - Mathod call started", LogSeverity.Information);
                string Path = string.Empty;
                string filename = "GoogleMap.jpg";
                //Path = string.Format("/Templates/Base/Brand/BB/images/" + filename);

                string srcFilePath = "";// HostingEnvironment.MapPath(Path);

                //string googlemapdata = System.IO.File.ReadAllText(srcFilePath);

                //googlemapdata = googlemapdata.Replace("{{fromaddress}}", fromaddress);
                //googlemapdata = googlemapdata.Replace("{{toaddress}}", toaddress);

                string query = @"<iframe width='1000' height='450' frameborder='0' style='border:0' src = 'https://www.google.com/maps/embed/v1/directions?key=AIzaSyDfsot6QpaUS36_TiWl_AXObEVsIClTLxE&origin={0}&destination={1}&avoid=tolls|highways' allowfullscreen></iframe>";
                //  fromaddress = "Los angels, CA 90001";
                // toaddress = "118 N Montana Ave,Absarokee,MT,59001";
                var html = String.Format(query, HttpUtility.UrlEncode(fromaddress), HttpUtility.UrlEncode(toaddress));
                //var htmlToImageConv = new NReco.ImageGenerator.HtmlToImageConverter();
                //var jpegBytes = htmlToImageConv.GenerateImage(html, "jpeg");

                //File.WriteAllBytes(srcFilePath, jpegBytes);

                string directionurl = "https://maps.googleapis.com/maps/api/directions/json?units=imperial&origin={0}&destination={1}&key=" + AppConfigManager.googleapikey;
                directionurl = String.Format(directionurl, HttpUtility.UrlEncode(fromaddress), HttpUtility.UrlEncode(toaddress));

                var client = new RestClient(directionurl);
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);

                EventLogger.LogEvent(response.Content, "PrintGoogleMap - After receive the direction data", LogSeverity.Information);
                MapDirection md = JsonConvert.DeserializeObject<MapDirection>(response.Content);
                StringBuilder sb = new StringBuilder();
                string distance = "";
                string duration = "";

                string startaddress = "";
                string startcityzip = "";
                string endaddress = "";
                string endcityzip = "";

                if (md != null && md.status == "OK")
                {
                    distance = md.routes[0].legs[0].distance.text;
                    duration = md.routes[0].legs[0].duration.text;
                    string[] startad = md.routes[0].legs[0].start_address.Split(',');
                    string[] endad = md.routes[0].legs[0].end_address.Split(',');
                    if (startad.Length == 4)
                    {
                        startaddress = startad[0];
                        startcityzip = startad[1] + ", " + startad[2] + ", " + startad[3];
                    }
                    else
                    {
                        startcityzip = md.routes[0].legs[0].start_address;
                    }

                    if (endad.Length == 4)
                    {
                        endaddress = endad[0];
                        endcityzip = endad[1] + ", " + endad[2] + ", " + endad[3];
                    }
                    else
                    {
                        endcityzip = md.routes[0].legs[0].end_address;
                    }

                    foreach (var step in md.routes[0].legs[0].steps)
                    {
                        sb.Append("<div>");
                        sb.Append(Googlemaneuver(step.maneuver));
                        sb.Append("<div style='float: left; margin: 0px 10px 0 35px;'>" + step.html_instructions + "</div>");
                        sb.Append("<div>" + step.duration.text + "( " + step.distance.text + " )</div>");
                        sb.Append("</div>");
                        sb.Append("<div style='clear: both'></div>");
                    }
                }

                // This is called while invoking from order pages
                Path = string.Format("/Templates/Base/Brand/BB/GoogleMap.html");

                srcFilePath = HostingEnvironment.MapPath(Path);

                string googlemapdata1 = System.IO.File.ReadAllText(srcFilePath);

                googlemapdata1 = googlemapdata1.Replace("{{LeadAddress}}", fromaddress + " to " + toaddress);
                googlemapdata1 = googlemapdata1.Replace("{{date}}", DateTime.Now.GlobalDateFormat());
                googlemapdata1 = googlemapdata1.Replace("{{distance}}", distance);
                googlemapdata1 = googlemapdata1.Replace("{{duration}}", duration);
                googlemapdata1 = googlemapdata1.Replace("{{direction}}", sb.ToString());
                googlemapdata1 = googlemapdata1.Replace("{{Imagepath}}", html);
                googlemapdata1 = googlemapdata1.Replace("{{startaddress}}", startaddress);
                googlemapdata1 = googlemapdata1.Replace("{{startcityzip}}", startcityzip);
                googlemapdata1 = googlemapdata1.Replace("{{endaddress}}", endaddress);
                googlemapdata1 = googlemapdata1.Replace("{{endcityzip}}", endcityzip);

                // Message, Method name, Log type
                EventLogger.LogEvent(googlemapdata1, "PrintGoogleMap - method ended", LogSeverity.Information);
                return googlemapdata1;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return "";
            }
        }

        public static string printLeadMeasurements(int id = 0)
        {
            leadsMng = new LeadsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            int opportunityId = leadsMng.printMeasurements(id);
            if (opportunityId > 0)
            {
                string data = Serializer.Measurement.MeasurementSheetReport.MeasurementSheet_BudgetBlinds(opportunityId);
                return data;
            }
            return "";
        }

        private static string GetFormatPhone(string TargetPhone)
        {
            string phoneFormat = "(###) ###-####";
            if (TargetPhone != null && TargetPhone != "" && TargetPhone.Length > 9)
            {
                return Convert.ToString(Convert.ToInt64(TargetPhone).ToString(phoneFormat));
            }
            else
            {
                return TargetPhone;
            }
        }

        private static string GetFormatDate(string TargetDate)
        {   //Format the date as MM/DD/YYYY and return
            if (TargetDate != null && TargetDate != "" && TargetDate.Length > 0)
            {
                try
                {
                    // return Convert.ToDateTime(TargetDate).ToShortDateString();
                    return Convert.ToDateTime(TargetDate).GlobalDateFormat();
                }
                catch (Exception) { return TargetDate; }
            }
            else
            {
                return TargetDate;
            }
        }

        private static string GetFormatTime(string TargetDate)
        {   //Format Time as HH:MM:SS AM/PM and return
            if (TargetDate != null && TargetDate != "" && TargetDate.Length > 0)
            {
                try
                {
                    return Convert.ToDateTime(TargetDate).ToShortTimeString();
                }
                catch (Exception)
                {
                    return TargetDate;
                }
            }
            else
            {
                return TargetDate;
            }
        }

        public static MemoryStream LeadSheets(Franchise franchise, Job job, ExportTypeEnum type = ExportTypeEnum.PDF)
        {
            string Path = string.Empty;
            if (AppConfigManager.ApplicationTitle.ToLower().Contains("budget blinds"))
                Path = string.Format("/Templates/Base/Brand/BB/LeadSheet.docx");
            else if (AppConfigManager.ApplicationTitle.ToLower().Contains("tailored"))
                Path = string.Format("/Templates/Base/Brand/TL/LeadSheet.docx");
            else Path = string.Format("/Templates/Base/Brand/CC/LeadSheet.docx");

            string srcFilePath = HostingEnvironment.MapPath(Path);

            MemoryStream destination = new MemoryStream();

            using (FileStream fs = File.OpenRead(srcFilePath))
            {
                fs.CopyTo(destination);
            }

            using (WordprocessingDocument document = WordprocessingDocument.Open(destination, true))
            {
                document.ChangeDocumentType(WordprocessingDocumentType.Document);

                MainDocumentPart mainPart = document.MainDocumentPart;

                Table appointmentTable = null;

                foreach (var table in mainPart.Document.Descendants<Table>())
                {
                    var prop = table.Descendants<TableProperties>().FirstOrDefault();
                    if (prop != null)
                    {
                        var cap = prop.Descendants<TableCaption>().FirstOrDefault();

                        if (cap != null && cap.Val == "Appointments Table")
                            appointmentTable = table;
                    }
                }

                List<SdtBlock> sdtBlockList = mainPart.Document.Descendants<SdtBlock>().ToList();
                List<SdtRun> sdtRunList = mainPart.Document.Descendants<SdtRun>().ToList();
                foreach (var block in sdtBlockList)
                {
                    var alias = block.SdtProperties.GetFirstChild<SdtAlias>();
                    if (alias == null)
                        continue;

                    var prop = alias.Val.Value;

                    if (prop == "JobNumber")
                        OpenXmlUtil.FillContentControl(block, job.JobNumber.ToString());
                    if (prop == "SaleAgent")
                    {
                        if (job.SalesPerson != null)
                        {
                            if (!string.IsNullOrEmpty(job.SalesPerson.FullName))
                                OpenXmlUtil.FillContentControl(block, job.SalesPerson.FullName);
                        }
                        else
                            OpenXmlUtil.FillContentControl(block, string.Empty);
                    }

                    if (prop == "JobInstallAddress" && job.InstallAddress != null)
                    {
                        string address = string.Empty;
                        if (!string.IsNullOrEmpty(job.InstallAddress.Address1))
                            address += job.InstallAddress.Address1.ToString();
                        if (!string.IsNullOrEmpty(job.InstallAddress.Address2))
                            address += ", " + job.InstallAddress.Address1.ToString();
                        if (!string.IsNullOrEmpty(job.InstallAddress.City))
                            address += ", " + job.InstallAddress.City.ToString();
                        if (!string.IsNullOrEmpty(job.InstallAddress.State))
                            address += ", " + job.InstallAddress.State.ToString();
                        if (!string.IsNullOrEmpty(job.InstallAddress.ZipCode))
                            address += job.InstallAddress.ZipCode.ToString();
                        if (prop == "JobInstallAddress" && !string.IsNullOrEmpty(address))
                            OpenXmlUtil.FillContentControl(block, address.ToString());
                    }
                    else if (prop == "JobInstallAddress" && job.InstallAddress == null)
                        OpenXmlUtil.FillContentControl(block, string.Empty);

                    if (prop == "ProjectDetails")
                    {
                        var projectDetailsResult = string.Empty;
                        var projectDetails = string.Empty;
                        foreach (var detail in job.JobQuestionAnswers)
                        {
                            if (string.IsNullOrEmpty(detail.Answer)) continue;

                            var question = CacheManager.QuestionTypeCollection.FirstOrDefault(f => f.QuestionId == detail.QuestionId);

                            if (question != null && question.Question.ToLower() == "project details" && AppConfigManager.ApplicationTitle.ToLower().Contains("budget blinds"))
                            {
                                if (!string.IsNullOrEmpty(question.SubQuestion))
                                    projectDetails += question.SubQuestion + " : " + detail.Answer + " |";
                                else
                                    projectDetails += detail.Answer + " |";
                            }
                            else if (question != null && question.Question.ToLower() == "other information" && AppConfigManager.ApplicationTitle.ToLower().Contains("tailored living"))
                            {
                                if (!string.IsNullOrEmpty(question.SubQuestion))
                                    projectDetails += question.SubQuestion + " : " + detail.Answer + " |";
                                else
                                    projectDetails += detail.Answer + " |";
                            }
                        }

                        if (!string.IsNullOrEmpty(projectDetails))
                            OpenXmlUtil.FillContentControl(block, projectDetails.TrimEnd('|'));
                        else
                            OpenXmlUtil.FillContentControl(block, string.Empty);
                    }

                    if (prop == "JobType")
                    {
                        var primaryQuote = job.JobQuotes.First(jq => jq.IsPrimary);

                        var productTypes = string.Join(", ", primaryQuote.JobItems.Select(ji => ji.ProductType).Distinct());

                        if (!string.IsNullOrEmpty(productTypes))
                            OpenXmlUtil.FillContentControl(block, productTypes);
                        else
                            OpenXmlUtil.FillContentControl(block, string.Empty);
                    }

                    if (prop == "TreatementsInterestedIn")
                    {
                        var treatmentsInterestedIn = string.Empty;

                        foreach (var detail in job.JobQuestionAnswers)
                        {
                            if (string.IsNullOrEmpty(detail.Answer)) continue;
                            var question = CacheManager.QuestionTypeCollection.FirstOrDefault(f => f.QuestionId == detail.QuestionId);

                            if (question != null && question.Question.ToLower() == "treatments interested in")
                            {
                                if (!string.IsNullOrEmpty(question.SubQuestion) && !question.SubQuestion.ToLower().Equals(detail.Answer.ToLower()))
                                    treatmentsInterestedIn += question.SubQuestion + " : " + detail.Answer + ",";
                                else
                                    treatmentsInterestedIn += detail.Answer + ",";
                            }
                        }

                        if (!string.IsNullOrEmpty(treatmentsInterestedIn))
                            OpenXmlUtil.FillContentControl(block, treatmentsInterestedIn.TrimEnd(','));
                        else
                            OpenXmlUtil.FillContentControl(block, string.Empty);
                    }

                    if (prop == "InterestedIn")
                    {
                        var interestedid = string.Empty;
                        foreach (var detail in job.JobQuestionAnswers)
                        {
                            var question = CacheManager.QuestionTypeCollection.FirstOrDefault(f => f.QuestionId == detail.QuestionId);
                            if (question != null && question.Question.ToLower() == "interested in" && detail.Answer != string.Empty)
                            {
                                if (!string.IsNullOrEmpty(question.SubQuestion) && !question.SubQuestion.ToLower().Equals(detail.Answer.ToLower()))
                                    interestedid += question.SubQuestion + " : " + detail.Answer + ",";
                                else
                                    interestedid += detail.Answer + ",";
                            }
                        }

                        if (!string.IsNullOrEmpty(interestedid))
                            OpenXmlUtil.FillContentControl(block, interestedid.TrimEnd(','));
                        else
                            OpenXmlUtil.FillContentControl(block, string.Empty);
                    }

                    if (prop == "CustomerFullName")
                    {
                        if (job.Lead.PrimCustomer != null && !string.IsNullOrEmpty(job.Lead.PrimCustomer.FullName))
                            OpenXmlUtil.FillContentControl(block, job.Lead.PrimCustomer.FullName);
                        else
                            OpenXmlUtil.FillContentControl(block, string.Empty);
                    }
                    if (prop == "CustomerCompany")
                    {
                        if (job.Lead != null && job.Lead.PrimCustomer != null && !string.IsNullOrEmpty(job.Lead.PrimCustomer.CompanyName))
                            OpenXmlUtil.FillContentControl(block, job.Lead.PrimCustomer.CompanyName);
                        else
                            OpenXmlUtil.FillContentControl(block, string.Empty);
                    }
                    if (prop == "AdditionContactName")
                    {
                        foreach (var secPerson in job.Lead.SecCustomer)
                        {
                            if (job.Lead != null && job.Lead.SecCustomer != null && !string.IsNullOrEmpty(secPerson.FullName))
                                OpenXmlUtil.FillContentControl(block, secPerson.FullName);
                            else
                                OpenXmlUtil.FillContentControl(block, string.Empty);
                        }
                    }

                    if (prop == "AdditionContactCell")
                    {
                        foreach (var secPerson in job.Lead.SecCustomer)
                        {
                            if (job.Lead != null && job.Lead.SecCustomer != null && !string.IsNullOrEmpty(secPerson.FullName))
                                OpenXmlUtil.FillContentControl(block, secPerson.CellPhone);
                            else
                                OpenXmlUtil.FillContentControl(block, string.Empty);
                        }
                    }

                    if (prop == "LeadSource")
                    {
                        if (job.Lead != null)
                        {
                            var leadSourcesIds = job.Lead.LeadSources.Select(s => s.SourceId).ToList();
                            var leadSourcesNames = CacheManager.SourceCollection.Where(s => leadSourcesIds.Contains(s.SourceId)).Select(s => s.Name);

                            OpenXmlUtil.FillContentControl(block, string.Join(",", leadSourcesNames));
                        }
                        else
                            OpenXmlUtil.FillContentControl(block, string.Empty);
                    }

                    if (prop == "ContactName")
                    {
                        foreach (var secPerson in job.Lead.SecCustomer)
                        {
                            if (job.Lead != null && job.Lead.SecCustomer != null && !string.IsNullOrEmpty(secPerson.FullName))
                                OpenXmlUtil.FillContentControl(block, secPerson.FullName);
                            else
                                OpenXmlUtil.FillContentControl(block, string.Empty);
                        }
                    }

                    if (prop == "InstallAddressAttention")
                    {
                        if (job.InstallAddress != null && !string.IsNullOrEmpty(job.InstallAddress.AttentionText))
                        {
                            OpenXmlUtil.FillContentControl(block, job.InstallAddress.AttentionText);
                        }
                        else
                            OpenXmlUtil.FillContentControl(block, string.Empty);
                    }

                    if (prop == "Territory")
                    {
                        if (job.Territory != null)
                        {
                            OpenXmlUtil.FillContentControl(block, job.Territory.Name);
                        }
                        else
                            OpenXmlUtil.FillContentControl(block, string.Empty);
                    }

                    if (prop == "Today")
                        OpenXmlUtil.FillContentControl(block, DateTime.Now.ToShortDateString());
                    if (prop == "Hint" && !string.IsNullOrEmpty(job.Hint))
                        OpenXmlUtil.FillContentControl(block, job.Hint.ToString());
                    else if (prop == "Hint" && string.IsNullOrEmpty(job.Hint))
                        OpenXmlUtil.FillContentControl(block, string.Empty);
                    if (prop == "Location" && !string.IsNullOrEmpty(job.InstallAddress.Location))
                        OpenXmlUtil.FillContentControl(block, job.InstallAddress.Location);
                    else if (prop == "Location" && string.IsNullOrEmpty(job.InstallAddress.Location))
                        OpenXmlUtil.FillContentControl(block, string.Empty);

                    if (prop == "JobDescription")
                    {
                        OpenXmlUtil.FillContentControl(block, job.Description);
                    }

                    if (prop == "JobSource")
                    {
                        if (job.Lead != null && job.Source != null)
                        {
                            var jobsource = job.Source.Name;

                            if (jobsource != null && jobsource.Equals("Allocate to all Sources"))
                            {
                                var allSources = job.Lead.LeadSources.ToList();
                                var alls = string.Empty;

                                foreach (var s in allSources)
                                {
                                    alls += GetSource(s.SourceId) + ",";
                                }
                                OpenXmlUtil.FillContentControl(block, alls.TrimEnd(','));
                            }
                            else
                            {
                                OpenXmlUtil.FillContentControl(block, jobsource);
                            }
                        }
                        else
                            OpenXmlUtil.FillContentControl(block, string.Empty);
                    }

                    if (prop == "JobBillingAddress")
                    {
                        if (job.Lead != null && job.BillingAddress != null)
                        {
                            string address = string.Empty;
                            if (!string.IsNullOrEmpty(job.BillingAddress.Address1))
                                address += job.BillingAddress.Address1.ToString();
                            if (!string.IsNullOrEmpty(job.BillingAddress.Address2))
                                address += " " + job.BillingAddress.Address2.ToString();
                            if (!string.IsNullOrEmpty(job.BillingAddress.City))
                                address += " " + job.BillingAddress.City.ToString();
                            if (!string.IsNullOrEmpty(job.BillingAddress.State))
                                address += " " + job.BillingAddress.State.ToString();
                            if (!string.IsNullOrEmpty(job.BillingAddress.ZipCode))
                                address += " " + job.BillingAddress.ZipCode.ToString();

                            OpenXmlUtil.FillContentControl(block, address);
                        }
                        else
                            OpenXmlUtil.FillContentControl(block, string.Empty);
                    }

                    if (prop == "CustomerHomePhone")
                    {
                        if (job.Lead != null && job.Lead.PrimCustomer != null && !string.IsNullOrEmpty(job.Lead.PrimCustomer.HomePhone))
                            OpenXmlUtil.FillContentControl(block, RegexUtil.FormatUSPhone(job.Lead.PrimCustomer.HomePhone));
                        else
                            OpenXmlUtil.FillContentControl(block, string.Empty);
                    }

                    if (prop == "CustomerCellPhone")
                    {
                        if (job.Lead != null && job.Lead.PrimCustomer != null && !string.IsNullOrEmpty(job.Lead.PrimCustomer.CellPhone))
                            OpenXmlUtil.FillContentControl(block, RegexUtil.FormatUSPhone(job.Lead.PrimCustomer.CellPhone));
                        else
                            OpenXmlUtil.FillContentControl(block, string.Empty);
                    }

                    if (prop == "CustomerWorkPhone")
                    {
                        if (job.Lead != null && job.Lead.PrimCustomer != null && !string.IsNullOrEmpty(job.Lead.PrimCustomer.WorkPhone))
                        {
                            string workPhone = job.Lead.PrimCustomer.WorkPhone;
                            workPhone = string.Format("{0}{1}", RegexUtil.FormatUSPhone(job.Customer.WorkPhone), string.IsNullOrEmpty(job.Customer.WorkPhoneExt) ? "" : (" x" + job.Customer.WorkPhoneExt));
                            OpenXmlUtil.FillContentControl(block, workPhone);
                        }
                        else
                            OpenXmlUtil.FillContentControl(block, string.Empty);
                    }

                    if (prop == "Today")
                        OpenXmlUtil.FillContentControl(block, DateTime.Now.ToShortDateString());
                    if (prop == "Hint" && !string.IsNullOrEmpty(job.Hint))
                        OpenXmlUtil.FillContentControl(block, job.Hint.ToString());
                    else if (prop == "Hint" && string.IsNullOrEmpty(job.Hint))
                        OpenXmlUtil.FillContentControl(block, string.Empty);
                    if (prop == "Location" && !string.IsNullOrEmpty(job.InstallAddress.Location))
                        OpenXmlUtil.FillContentControl(block, job.InstallAddress.Location);
                    else if (prop == "Location" && string.IsNullOrEmpty(job.InstallAddress.Location))
                        OpenXmlUtil.FillContentControl(block, string.Empty);

                    if (prop == "CustomerOtherPhone")
                    {
                        if (job.Lead != null && job.Lead.PrimCustomer != null && !string.IsNullOrEmpty(job.Lead.PrimCustomer.FaxPhone))
                            OpenXmlUtil.FillContentControl(block, RegexUtil.FormatUSPhone(job.Lead.PrimCustomer.FaxPhone));
                        else
                            OpenXmlUtil.FillContentControl(block, string.Empty);
                    }

                    if (prop == "CustomerEmail")
                    {
                        if (job.Lead != null && job.Lead.PrimCustomer != null && !string.IsNullOrEmpty(job.Lead.PrimCustomer.PrimaryEmail))
                            OpenXmlUtil.FillContentControl(block, job.Lead.PrimCustomer.PrimaryEmail);
                        else
                            OpenXmlUtil.FillContentControl(block, string.Empty);
                    }

                    if (prop == "LeadPrimaryNote")
                    {
                        if (job.Lead != null && job.Lead.LeadNotes != null)
                        {
                            var primaryNote = job.Lead.LeadNotes.Where(x => x.TypeEnum == NoteTypeEnum.Primary).FirstOrDefault();
                            if (primaryNote != null)
                                OpenXmlUtil.FillContentControl(block, primaryNote.Notes);
                            else
                                OpenXmlUtil.FillContentControl(block, string.Empty);
                        }
                        else
                            OpenXmlUtil.FillContentControl(block, string.Empty);
                    }

                    if (prop == "LeadNotes")
                    {
                        if (job.Lead != null && job.Lead.LeadNotes != null)
                        {
                            var leadNotes = job.Lead.LeadNotes.Where(x => x.TypeEnum != NoteTypeEnum.Primary).ToList();
                            if (leadNotes.Count() > 0)
                            {
                                var allNotes = string.Empty;
                                foreach (var note in leadNotes)
                                    allNotes += note.Notes + "|";
                                OpenXmlUtil.FillContentControl(block, allNotes.TrimEnd('|'));
                            }
                            else
                                OpenXmlUtil.FillContentControl(block, string.Empty);
                        }
                        else
                            OpenXmlUtil.FillContentControl(block, string.Empty);
                    }

                    if (prop == "Concept" && job.JobConcept != null)
                        OpenXmlUtil.FillContentControl(block, job.JobConcept.Concept.ToString());
                    else if (prop == "Concept" && job.JobConcept == null)
                        OpenXmlUtil.FillContentControl(block, string.Empty);

                    if (prop == "JobDescription" && job.Description != null)
                        OpenXmlUtil.FillContentControl(block, job.Description);
                    else if (prop == "JobDescription" && job.Description == null)
                        OpenXmlUtil.FillContentControl(block, string.Empty);

                    if (prop == "NotesMarkedAsDirection")
                    {
                        var directionNote = job.JobNotes.Where(x => x.TypeEnum == NoteTypeEnum.Direction).ToList();
                        if (directionNote.Count > 0)
                        {
                            var allnotes = string.Empty;
                            foreach (var note in directionNote)
                                allnotes += note.Message + "|";
                            OpenXmlUtil.FillContentControl(block, allnotes.TrimEnd('|'));
                        }
                        else
                            OpenXmlUtil.FillContentControl(block, string.Empty);
                    }
                }

                foreach (var run in sdtRunList)
                {
                    var alias = run.SdtProperties.GetFirstChild<SdtAlias>();
                    if (alias == null)
                        continue;

                    var prop = alias.Val.Value;

                    if (prop == "ProjectDetails")
                    {
                        var projectDetailsResult = string.Empty;
                        var projectDetails = string.Empty;
                        foreach (var detail in job.JobQuestionAnswers)
                        {
                            if (string.IsNullOrEmpty(detail.Answer)) continue;

                            var question = CacheManager.QuestionTypeCollection.FirstOrDefault(f => f.QuestionId == detail.QuestionId);

                            if (question != null && question.Question.ToLower() == "project details" && AppConfigManager.ApplicationTitle.ToLower().Contains("budget blinds"))
                            {
                                if (!string.IsNullOrEmpty(question.SubQuestion))
                                    projectDetails += question.SubQuestion + " : " + detail.Answer + " |";
                                else
                                    projectDetails += detail.Answer + " |";
                            }
                            else if (question != null && question.Question.ToLower() == "other information" && !AppConfigManager.ApplicationTitle.ToLower().Contains("budget blinds"))
                            {
                                if (!string.IsNullOrEmpty(question.SubQuestion))
                                    projectDetails += question.SubQuestion + " : " + detail.Answer + " |";
                                else
                                    projectDetails += detail.Answer + " |";
                            }
                        }

                        if (!string.IsNullOrEmpty(projectDetails))
                            OpenXmlUtil.FillContentControl(run, projectDetails.TrimEnd('|'));
                        else
                            OpenXmlUtil.FillContentControl(run, string.Empty);
                    }

                    if (prop == "CustomerFullName")
                    {
                        if (job.Lead.PrimCustomer != null && !string.IsNullOrEmpty(job.Lead.PrimCustomer.FullName))
                            OpenXmlUtil.FillContentControl(run, job.Lead.PrimCustomer.FullName);
                        else
                            OpenXmlUtil.FillContentControl(run, string.Empty);
                    }

                    if (prop == "JobNumber")
                        OpenXmlUtil.FillContentControl(run, job.JobNumber.ToString());
                    if (prop == "SaleAgent")
                    {
                        if (job.SalesPerson != null)
                        {
                            if (!string.IsNullOrEmpty(job.SalesPerson.FullName))
                                OpenXmlUtil.FillContentControl(run, job.SalesPerson.FullName);
                        }
                        else
                            OpenXmlUtil.FillContentControl(run, string.Empty);
                    }

                    if (prop == "CustomerHomePhone")
                    {
                        if (job.Lead != null && job.Lead.PrimCustomer != null && !string.IsNullOrEmpty(job.Lead.PrimCustomer.HomePhone))
                            OpenXmlUtil.FillContentControl(run, RegexUtil.FormatUSPhone(job.Lead.PrimCustomer.HomePhone));
                        else
                            OpenXmlUtil.FillContentControl(run, string.Empty);
                    }

                    if (prop == "CustomerCellPhone")
                    {
                        if (job.Lead != null && job.Lead.PrimCustomer != null && !string.IsNullOrEmpty(job.Lead.PrimCustomer.CellPhone))
                            OpenXmlUtil.FillContentControl(run, RegexUtil.FormatUSPhone(job.Lead.PrimCustomer.CellPhone));
                        else
                            OpenXmlUtil.FillContentControl(run, string.Empty);
                    }

                    if (prop == "CustomerWorkPhone")
                    {
                        if (job.Lead != null && job.Lead.PrimCustomer != null && !string.IsNullOrEmpty(job.Lead.PrimCustomer.WorkPhone))
                        {
                            string workPhone = job.Lead.PrimCustomer.WorkPhone;
                            workPhone = string.Format("{0}{1}", RegexUtil.FormatUSPhone(job.Customer.WorkPhone), string.IsNullOrEmpty(job.Customer.WorkPhoneExt) ? "" : (" x" + job.Customer.WorkPhoneExt));
                            OpenXmlUtil.FillContentControl(run, workPhone);
                        }
                        else
                            OpenXmlUtil.FillContentControl(run, string.Empty);
                    }
                    if (prop == "CustomerOtherPhone")
                    {
                        if (job.Lead != null && job.Lead.PrimCustomer != null && !string.IsNullOrEmpty(job.Lead.PrimCustomer.FaxPhone))
                            OpenXmlUtil.FillContentControl(run, RegexUtil.FormatUSPhone(job.Lead.PrimCustomer.FaxPhone));
                        else
                            OpenXmlUtil.FillContentControl(run, string.Empty);
                    }

                    if (prop == "JobStatusParent" && job.JobStatus.ParentId != null)
                    {
                        var parentid = job.JobStatus.ParentId ?? 0;
                        var parentJobStatus = GetJobStatus(parentid);

                        OpenXmlUtil.FillContentControl(run, parentJobStatus);
                    }
                    else if (prop == "JobStatusParent" && job.JobStatus.Parent == null)
                        OpenXmlUtil.FillContentControl(run, string.Empty);

                    if (prop == "JobStatusChild" && !string.IsNullOrEmpty(job.JobStatus.Name))
                        OpenXmlUtil.FillContentControl(run, job.JobStatus.Name.ToString());
                    else if (prop == "JobStatusChild" && string.IsNullOrEmpty(job.JobStatus.Name))
                        OpenXmlUtil.FillContentControl(run, string.Empty);

                    if (prop == "JobDescription" && job.Description != null)
                        OpenXmlUtil.FillContentControl(run, job.Description);
                    else if (prop == "JobDescription" && job.Description == null)
                        OpenXmlUtil.FillContentControl(run, string.Empty);

                    if (prop == "AdditionContactName")
                    {
                        foreach (var secPerson in job.Lead.SecCustomer)
                        {
                            if (job.Lead != null && job.Lead.SecCustomer != null && !string.IsNullOrEmpty(secPerson.FullName))
                                OpenXmlUtil.FillContentControl(run, secPerson.FullName);
                            else
                                OpenXmlUtil.FillContentControl(run, string.Empty);
                        }
                    }

                    if (prop == "ContactName")
                    {
                        foreach (var secPerson in job.Lead.SecCustomer)
                        {
                            if (job.Lead != null && job.Lead.SecCustomer != null && !string.IsNullOrEmpty(secPerson.FullName))
                                OpenXmlUtil.FillContentControl(run, secPerson.FullName);
                            else
                                OpenXmlUtil.FillContentControl(run, string.Empty);
                        }
                    }

                    if (prop == "TreatementsInterestedIn")
                    {
                        var treatmentsInterestedIn = string.Empty;
                        foreach (var detail in job.JobQuestionAnswers)
                        {
                            var question = CacheManager.QuestionTypeCollection.FirstOrDefault(f => f.QuestionId == detail.QuestionId);

                            if (question != null && question.Question.ToLower() == "treatments interested in")
                                treatmentsInterestedIn += detail.Answer + ",";
                        }

                        if (!string.IsNullOrEmpty(treatmentsInterestedIn))
                            OpenXmlUtil.FillContentControl(run, treatmentsInterestedIn.TrimEnd(','));
                        else
                            OpenXmlUtil.FillContentControl(run, string.Empty);
                    }

                    if (prop == "Territory")
                    {
                        if (job.Territory != null)
                        {
                            OpenXmlUtil.FillContentControl(run, job.Territory.Name);
                        }
                        else
                            OpenXmlUtil.FillContentControl(run, string.Empty);
                    }

                    if (prop == "SaleAgent")
                    {
                        if (job.SalesPerson != null)
                        {
                            if (!string.IsNullOrEmpty(job.SalesPerson.FullName))
                                if (prop == "SaleAgent" && !string.IsNullOrEmpty(job.SalesPerson.FullName))
                                    OpenXmlUtil.FillContentControl(run, job.SalesPerson.FullName);
                        }
                        else if (job.SalesPerson == null)
                            OpenXmlUtil.FillContentControl(run, string.Empty);
                    }

                    if (prop == "JobType")
                    {
                        var primaryQuote = job.JobQuotes.First(jq => jq.IsPrimary);

                        var productTypes = string.Join(", ", primaryQuote.JobItems.Select(ji => ji.ProductType).Distinct());

                        if (!string.IsNullOrEmpty(productTypes))
                            OpenXmlUtil.FillContentControl(run, productTypes);
                        else
                            OpenXmlUtil.FillContentControl(run, string.Empty);
                    }

                    if (prop == "Today")
                        OpenXmlUtil.FillContentControl(run, DateTime.Now.ToShortDateString());
                    if (prop == "Hint" && !string.IsNullOrEmpty(job.Hint))
                        OpenXmlUtil.FillContentControl(run, job.Hint.ToString());
                    else if (prop == "Hint" && string.IsNullOrEmpty(job.Hint))
                        OpenXmlUtil.FillContentControl(run, string.Empty);
                    if (prop == "Location" && !string.IsNullOrEmpty(job.InstallAddress.Location))
                        OpenXmlUtil.FillContentControl(run, job.InstallAddress.Location);
                    else if (prop == "Location" && string.IsNullOrEmpty(job.InstallAddress.Location))
                        OpenXmlUtil.FillContentControl(run, string.Empty);
                    if (prop == "NotesMarkedAsInternal")
                    {
                        var internalNote = job.JobNotes.Where(x => x.TypeEnum == NoteTypeEnum.Internal).ToList();
                        if (internalNote.Count() > 0)
                        {
                            string allnotes = string.Empty;
                            foreach (var note in internalNote)
                                allnotes += note.Message + "|";
                            OpenXmlUtil.FillContentControl(run, allnotes.TrimEnd('|'));
                        }
                        else
                            OpenXmlUtil.FillContentControl(run, string.Empty);
                    }
                }

                int rowNum = 1;

                var historyItems =
                    (from item in
                        job.Calendars.Where(c => c.IsDeleted != true && c.IsCancelled != true)
                            .OrderBy(o => o.CreatedOnUtc)
                     let apptType =
                         CacheManager.AppointmentTypes.SingleOrDefault(
                             x => x.AppointmentTypeId == (int)item.AptTypeEnum) ??
                         CacheManager.AppointmentTypes.SingleOrDefault(x => x.AppointmentTypeId == 1)
                     select
                         new KeyValuePair<DateTime, string>(item.StartDate.DateTime,
                             string.Format("{0}, {1}, {2} ({3})", apptType.Name, item.Message,
                                 String.Format("{0:g}", item.StartDate.DateTime), item.AssignedName)
                                 .TrimEnd(' ')
                                 .TrimEnd(','))).ToList();

                historyItems.AddRange(job.JobNotes.Where(
                    n => n.TypeEnum == NoteTypeEnum.Direction)
                    .Select(
                        n =>
                            new KeyValuePair<DateTime, string>(n.CreatedOn.Date,
                                string.Format("{0}, {1:g} ({2})", n.Message, n.CreatedOn.Date, n.CreatorFullName))));

                historyItems = historyItems.OrderBy(o => o.Key).ToList();

                AddRow(appointmentTable, string.Format("Lead Creation Date: {0:g}", job.Lead.CreatedOnUtc), ref rowNum);

                foreach (var historyItem in historyItems)
                {
                    AddRow(appointmentTable, historyItem.Value, ref rowNum);
                }

                mainPart.Document.Save();
#if DEBUG
                OpenXmlValidator validator = new OpenXmlValidator();
                var errors = validator.Validate(document);
#endif
                document.Close();
            }

            var outputStream = new MemoryStream();
            if (type == ExportTypeEnum.PDF)
            {
                var license = new Aspose.Words.License();
                license.SetLicense("Aspose.Words.lic");

                var doc = new Aspose.Words.Document(destination);
                doc.Save(outputStream, Aspose.Words.SaveFormat.Pdf);
            }
            else
                outputStream = destination;
            return outputStream;
        }

        private static void AddRow(Table appointmentTable, string content, ref int rowNum)
        {
            rowNum++;

            TableRow row = null;
            var count = appointmentTable.Descendants<TableRow>().Count();
            if (count > rowNum)
            {
                row = appointmentTable.Descendants<TableRow>().ElementAt(rowNum);
            }

            if (row == null)
            {
                row = new TableRow(
                    new TableCell(
                        new Paragraph(
                            new ParagraphProperties(
                                new Justification() { Val = JustificationValues.Left }
                                ))
                        ));

                appointmentTable.Append(row);
            }
            var cell = row.Descendants<TableCell>().ElementAt(0);
            if (cell == null) return;
            var p = cell.Descendants<Paragraph>().FirstOrDefault();
            var run = new Run(new Text(content));
            if (p != null) p.Append(run);
            else cell.Append(new Paragraph(run));
        }

        public static string GetSource(int sourceid)
        {
            var source = CacheManager.SourceCollection.FirstOrDefault(f => f.SourceId == sourceid);
            if (source != null)
                return source.Name;
            else
                return string.Empty;
        }

        public static string GetJobStatus(int jobStatusId)
        {
            var status = CacheManager.JobStatusTypeCollection.FirstOrDefault(f => f.Id == jobStatusId);
            if (status != null)
                return status.ToString();
            else
                return string.Empty;
        }

        //18
        private static string Googlemaneuver(string maneuver)
        {
            switch (maneuver)
            {
                case "ferry":
                    return GetHtml("ferry");
                    break;

                case "ferry-train":
                    return GetHtml("ferry-train");
                    break;

                case "merge":
                    return GetHtml("merge");
                    break;

                case "straight":
                    return GetHtml("straight");
                    break;

                case "fork -left":
                    return GetHtml("fork -left");
                    break;

                case "ramp-left":
                    return GetHtml("ramp-left");
                    break;

                case "roundabout-left":
                    return GetHtml("roundabout-left");
                    break;

                case "turn-left":
                    return GetHtml("turn-left");
                    break;

                case "turn-sharp-left":
                    return GetHtml("turn-sharp-left");
                    break;

                case "turn-slight-left":
                    return GetHtml("turn-slight-left");
                    break;

                case "uturn-left":
                    return GetHtml("uturn-left");
                    break;

                case "fork-right":
                    return GetHtml("fork-right");
                    break;

                case "ramp-right":
                    return GetHtml("ramp-right");
                    break;

                case "roundabout-right":
                    return GetHtml("roundabout-right");
                    break;

                case "turn-right":
                    return GetHtml("turn-right");
                    break;

                case "turn -sharp-right":
                    return GetHtml("turn -sharp-right");
                    break;

                case "turn-slight-right":
                    return GetHtml("turn-slight-right");
                    break;

                case "uturn-right":
                    return GetHtml("uturn-right");
                    break;

                case "keep-left":
                    return GetHtml("turn-left");
                    break;

                case "keep-right":
                    return GetHtml("turn-right");
                    break;

                default:
                    return "";
                    break;
            }
        }

        public static string GetHtml(string classname)
        {
            return @"<div class='dir-tt dir-tt-" + classname + "'><img height='630' width='19' src='images/maneuvers-2x.png'/></div>";
        }
    }
}