﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Validation;
using DocumentFormat.OpenXml.Wordprocessing;
using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using DocumentFormat.OpenXml.Office2010.Word;
using HFC.CRM.Data.Context;
using System.Data.SqlClient;
using System.Data;
using Dapper;
using SelectPdf;
using System.Globalization;
using HFC.CRM.Core.Extensions;

namespace HFC.CRM.Serializer
{
    public class PurchaseOrderSheet : DataManager
    {
        static string error = @"An error occurred.If the problem persists, please contact your system administrator.";
        PurchaseOrderManager poManager = new PurchaseOrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        QuotesManager quotemgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private static ShipNoticeManager shipMngr = new ShipNoticeManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        #region VPO Print
        public string VPOSheet(int OrderId, MasterPurchaseOrder mpo, VPODetail vpo)
        {
            string data = string.Empty;
            data = VPOSheetBody(OrderId, mpo, vpo);
            return data;
        }
        public string VPOSheetHeader(int orderId, MasterPurchaseOrder mpo, VPODetail vpo)
        {
            string data = string.Empty;
            data = VPOHeader(orderId, mpo, vpo);
            return data;
        }
        public string VPOSheetFooter(int orderId)
        {
            string data = string.Empty;
            data = MPOFooter(orderId);
            return data;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="MPO"></param>
        /// <returns></returns>
        public string VPOSheetBody(int orderId, MasterPurchaseOrder MPO, VPODetail vpo)
        {
            string Path = string.Empty;
            Path = string.Format("/Templates/Base/Brand/BB/BB_MPO_Template-bodycontent.html");
            string srcFilePath = HostingEnvironment.MapPath(Path);
            string data = System.IO.File.ReadAllText(srcFilePath);
            try
            {
                int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                var Opportunity = quotemgr.GetOpportunityByQuoteKey(MPO.QuoteId);
                var customer = quotemgr.GetCustomerByQuoteKey(MPO.QuoteId);
                var BillingAddress = quotemgr.GetBillingAddressByQuoteKey(MPO.QuoteId);
                var InstallationAddress = quotemgr.GetInstallationAddressByQuoteKey(MPO.QuoteId);
                var Quote = quotemgr.GetQuoteInfo(MPO.QuoteId);
                if (BillingAddress == null) BillingAddress = InstallationAddress;

                data = data.Replace("{AccountName}", MPO.AccountName);
                data = data.Replace("{Opportunity}", MPO.OpportunityName);
                data = data.Replace("{SalesOrder}", MPO.OrderNumber);
                data = data.Replace("{ShiptoAddress}", MPO.Address);
                data = data.Replace("{mpoNumber}", " <label><b>MPO #: </b>" + MPO.MasterPONumber + "</label>");




                if (customer.HomePhone != null && customer.HomePhone.Length > 0)
                {
                    string phoneFormat = "(###) ###-####";
                    customer.HomePhone = Convert.ToInt64(customer.HomePhone).ToString(phoneFormat);
                }
                else
                    customer.HomePhone = "";

                if (customer.CellPhone != null && customer.CellPhone.Length > 0)
                {
                    string phoneFormat = "(###) ###-####";
                    customer.CellPhone = Convert.ToInt64(customer.CellPhone).ToString(phoneFormat);
                }
                else
                    customer.CellPhone = "";

                //Read lead html and append the data
                data = data.Replace("{AccountCustomerName}", customer.FirstName + " " + customer.LastName);
                data = data.Replace("{AccountHomePhone}", customer.HomePhone);
                data = data.Replace("{AccountCellPhone}", customer.CellPhone);
                data = data.Replace("{AccountEmail}", customer.PrimaryEmail);
                data = data.Replace("{OppSideMark}", Quote != null && Quote.SideMark != null ? Quote.SideMark : "");

                data = data.Replace("{BillingAddress1}", BillingAddress.Address1);
                data = data.Replace("{BillingAddress2}", BillingAddress.Address2);
                data = data.Replace("{BillingCityStateZip}", BillingAddress.City + ", " + BillingAddress.State + " " + BillingAddress.ZipCode);

                data = data.Replace("{InstallationAddress1}", InstallationAddress.Address1);
                data = data.Replace("{InstallationAddress2}", InstallationAddress.Address2);
                data = data.Replace("{InstallationCityStateZip}", InstallationAddress.City + ", " + InstallationAddress.State + " " + InstallationAddress.ZipCode);



                StringBuilder vpoDetails = new StringBuilder();




                vpoDetails.Append("<table class='table table-quoteprint'>");
                vpoDetails.Append("<thead style='display: table-header-group;'>");
                vpoDetails.Append("<tr>");
                if (SessionManager.BrandId == 1)
                {
                    vpoDetails.Append("<th>Window Name</th>");
                }
                    //vpoDetails.Append("<th>Line</th>");
                vpoDetails.Append("<th>Product</th>");
                vpoDetails.Append("<th>Description</th>");
                vpoDetails.Append("<th>QTY</th>");
                vpoDetails.Append("<th>Est. Ship Date</th>");
                vpoDetails.Append("<th>PO Status</th>");
                vpoDetails.Append("</tr>");
                vpoDetails.Append("</thead>");
                vpoDetails.Append("<tbody>");

                var core = MPO.CoreProductVPOs.Where(x => x.PurchaseOrdersDetailId == vpo.PurchaseOrdersDetailId)
                    .FirstOrDefault();
                if (core != null)
                {
                    var corelist = MPO.CoreProductVPOs.Where(x => x.PICPO == core.PICPO && x.VendorName == core.VendorName).ToList();
                    if (corelist != null && corelist.Count > 0)
                    {

                        foreach (var item in corelist)
                        {
                            vpoDetails.Append("<tr style='border-bottom:1px solid #ddd;'>");
                            //if (item.PICPO != 0)
                            //{
                            //    vpoDetails.Append("<td>" + item.PICPO.ToString() + "</td>");
                            //}
                            //else
                            //{
                            //    vpoDetails.Append("<td></td>");
                            //}
                            if (SessionManager.BrandId == 1)
                            {
                                vpoDetails.Append("<td>" + item.WindowName + "</td>");
                            }
                            vpoDetails.Append("<td>" + item.ProductName + "</td>");
                            vpoDetails.Append("<td>" + item.Description + "</td>");
                            vpoDetails.Append("<td>" + item.Quantity.ToString() + "</td>");
                            if (item.ShipDate.HasValue)
                            {
                                vpoDetails.Append("<td>" + TimeZoneManager.ToLocal(item.ShipDate.Value)
                                                      .GlobalDateFormat());
                            }
                            else
                            {
                                vpoDetails.Append("<td>" + "");
                            }

                            vpoDetails.Append("<td>" + item.Status + "</td>");
                        }

                    }

                }
                var myprd = MPO.MyProductVPOs.Where(x => x.PurchaseOrdersDetailId == vpo.PurchaseOrdersDetailId)
                    .FirstOrDefault();
                if (myprd != null)
                {
                    var myprdlist = MPO.MyProductVPOs.Where(x => x.PICPO == myprd.PICPO).ToList();
                    if (myprdlist != null && myprdlist.Count > 0)
                    {
                        foreach (var item in myprdlist)
                        {
                            vpoDetails.Append("<tr style='border-bottom:1px solid #ddd;'>");
                            //if (item.PICPO != null)
                            //{
                            //    vpoDetails.Append("<td>" + item.PONumber.ToString() + "</td>");
                            //}
                            //else
                            //{
                            //    vpoDetails.Append("<td></td>");
                            //}
                            if (SessionManager.BrandId == 1)
                            {
                                vpoDetails.Append("<td>" + item.WindowName + "</td>");
                            }
                            vpoDetails.Append("<td>" + item.ProductName + "</td>");
                            vpoDetails.Append("<td>" + item.Description + "</td>");
                            vpoDetails.Append("<td>" + item.Quantity.ToString() + "</td>");
                            if (item.ShipDate.HasValue)
                            {
                                vpoDetails.Append("<td>" + TimeZoneManager.ToLocal(item.ShipDate.Value)
                                                      .GlobalDateFormat() + "</td>");
                            }
                            else
                            {
                                vpoDetails.Append("<td>" + "" + "</td>");
                            }

                            vpoDetails.Append("<td>" + item.Status + "</td>");
                            vpoDetails.Append("</tr>");
                        }

                    }
                }




                vpoDetails.Append("</tbody>");
                vpoDetails.Append("</table>");

                data = data.Replace("{VPODetails}", vpoDetails.ToString());

            }
            catch (Exception ex)
            {
                return data;
            }
            return data;
        }

        /// <summary>
        /// /
        /// </summary>
        /// <param name="orderId"></param
        /// <param name="mpo"></param>
        /// <returns></returns>
        public string VPOHeader(int orderId, MasterPurchaseOrder mpo, VPODetail vpo)
        {
            string LeadData = string.Empty;
            string Path = string.Empty;
            if (SessionManager.BrandId == 1)
            {
                Path = string.Format("/Templates/Base/Brand/BB/BB_VPO_Template-header.html");
            }
            else if (SessionManager.BrandId == 2)
            {
                Path = string.Format("/Templates/Base/Brand/TL/TL_VPO_Template-header.html");
            }
            else
            {
                Path = string.Format("/Templates/Base/Brand/CC/CC_VPO_Template-header.html");
            }


            string srcFilePath = HostingEnvironment.MapPath(Path);
            int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
            if (poManager == null)
            {
                poManager = new PurchaseOrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            }
            string data = System.IO.File.ReadAllText(srcFilePath);

            string submitted;
            if (mpo.SubmittedDate.HasValue)
            {
                submitted = TimeZoneManager.ToLocal(mpo.SubmittedDate.Value).GlobalDateFormat();
            }
            else
            {
                submitted = string.Empty;
            }

            string str = string.Empty;
            if (vpo.PONumber != null)
            {
                str =
                    "<h1>Vendor PO#: " + vpo.PONumber.ToString() + "</h1>";
            }
            else if (vpo.PICPO != null && vpo.PICPO != 0)
            {
                str =
                    "<h1>Vendor PO#: " + vpo.PICPO.ToString() + "</h1>";
            }
            else
            {
                str =
                    "<h1>Vendor PO#: </h1>";

            }

            str = str + " <label><b>Vendor: </b>" + vpo.VendorName + "</b></label><label><b>Submitted Date: </b>" + submitted + " </label>";
            data = data.Replace("{header}", str);

            return data;
        }


        #endregion

        public string MPOSheet(int OrderId, MasterPurchaseOrder mpo)
        {
            string data = string.Empty;
            data = MPOSheetBody(OrderId, mpo);
            return data;
        }
        public string MPOSheetHeader(int orderId, MasterPurchaseOrder mpo)
        {
            string data = string.Empty;
            data = MPOHeader(orderId, mpo);
            return data;
        }
        public string MPOSheetFooter(int orderId)
        {
            string data = string.Empty;
            data = MPOFooter(orderId);
            return data;
        }

        /// <summary>
        /// Body is same for all the Brands
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="MPO"></param>
        /// <returns></returns>
        public string MPOSheetBody(int orderId, MasterPurchaseOrder MPO)
        {
            string Path = string.Empty;
            Path = string.Format("/Templates/Base/Brand/BB/BB_MPO_Template-bodycontent.html");
            string srcFilePath = HostingEnvironment.MapPath(Path);
            string data = System.IO.File.ReadAllText(srcFilePath);
            try
            {
                var Opportunity = quotemgr.GetOpportunityByQuoteKey(MPO.QuoteId);
                var customer = quotemgr.GetCustomerByQuoteKey(MPO.QuoteId);
                var BillingAddress = quotemgr.GetBillingAddressByQuoteKey(MPO.QuoteId);
                var InstallationAddress = quotemgr.GetInstallationAddressByQuoteKey(MPO.QuoteId);
                var Quote = quotemgr.GetQuoteInfo(MPO.QuoteId);
                if (BillingAddress == null) BillingAddress = InstallationAddress;

                int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;


                data = data.Replace("{AccountName}", MPO.AccountName);
                data = data.Replace("{Opportunity}", MPO.OpportunityName);
                data = data.Replace("{SalesOrder}", MPO.OrderNumber);

                data = data.Replace("{ShiptoAddress}", MPO.Address);
                data = data.Replace("{mpoNumber}", string.Empty);

                if (customer.HomePhone != null && customer.HomePhone.Length > 0)
                {
                    string phoneFormat = "(###) ###-####";
                    customer.HomePhone = Convert.ToInt64(customer.HomePhone).ToString(phoneFormat);
                }
                else
                    customer.HomePhone = "";

                if (customer.CellPhone != null && customer.CellPhone.Length > 0)
                {
                    string phoneFormat = "(###) ###-####";
                    customer.CellPhone = Convert.ToInt64(customer.CellPhone).ToString(phoneFormat);
                }
                else
                    customer.CellPhone = "";

                //Read lead html and append the data
                data = data.Replace("{AccountCustomerName}", customer.FirstName + " " + customer.LastName);
                data = data.Replace("{AccountHomePhone}", customer.HomePhone);
                data = data.Replace("{AccountCellPhone}", customer.CellPhone);
                data = data.Replace("{AccountEmail}", customer.PrimaryEmail);
                data = data.Replace("{OppSideMark}", Quote != null && Quote.SideMark != null ? Quote.SideMark : "");

                data = data.Replace("{BillingAddress1}", BillingAddress.Address1);
                data = data.Replace("{BillingAddress2}", BillingAddress.Address2);
                data = data.Replace("{BillingCityStateZip}", BillingAddress.City + ", " + BillingAddress.State + " " + BillingAddress.ZipCode);

                data = data.Replace("{InstallationAddress1}", InstallationAddress.Address1);
                data = data.Replace("{InstallationAddress2}", InstallationAddress.Address2);
                data = data.Replace("{InstallationCityStateZip}", InstallationAddress.City + ", " + InstallationAddress.State + " " + InstallationAddress.ZipCode);




                StringBuilder vpoDetails = new StringBuilder();




                vpoDetails.Append("<table class='table table-quoteprint'>");
                vpoDetails.Append("<thead style='display: table-header-group;'>");
                vpoDetails.Append("<tr>");
                vpoDetails.Append("<th>PO #</th>");
                vpoDetails.Append("<th>Vendor</th>");
                vpoDetails.Append("<th>Total QTY</th>");
                vpoDetails.Append("<th>Est. Ship Date</th>");
                vpoDetails.Append("<th>PO Status</th>");
                vpoDetails.Append("</tr>");
                vpoDetails.Append("</thead>");
                vpoDetails.Append("<tbody>");

                if (MPO.CoreProductVPOs != null && MPO.CoreProductVPOs.Count > 0)
                {
                    var coreProductVPOs = MPO.CoreProductVPOs
                        .GroupBy(x => new { x.PICPO, x.VendorName, x.ShipDate, x.Status }).Select(z =>
                              new VPODetail
                              {
                                  PICPO = z.Key.PICPO,
                                  VendorName = z.Key.VendorName,
                                  ShipDate = z.Key.ShipDate,
                                  Status = z.Key.Status,
                                  Quantity = z.Sum(a => a.Quantity)
                              }).ToList();

                    foreach (var item in coreProductVPOs)
                    {
                        vpoDetails.Append("<tr style='border-bottom:1px solid #ddd;'>");
                        if (item.PICPO != 0)
                        {
                            vpoDetails.Append("<td>" + item.PICPO.ToString() + "</td>");
                        }
                        else
                        {
                            vpoDetails.Append("<td></td>");
                        }

                        vpoDetails.Append("<td>" + item.VendorName + "</td>");
                        vpoDetails.Append("<td>" + item.Quantity.ToString() + "</td>");
                        if (item.ShipDate.HasValue)
                        {
                            vpoDetails.Append("<td>" + TimeZoneManager.ToLocal(item.ShipDate.Value)
                                                 .GlobalDateFormat());
                        }
                        else
                        {
                            vpoDetails.Append("<td>" + "");
                        }

                        vpoDetails.Append("<td>" + item.Status + "</td>");
                    }

                }

                if (MPO.MyProductVPOs != null && MPO.MyProductVPOs.Count > 0)
                {
                    var myProductVPOs = MPO.MyProductVPOs
                        .GroupBy(x => new { x.PONumber, x.VendorName, x.ShipDate, x.Status }).Select(z =>
                            new VPODetail
                            {
                                PONumber = z.Key.PONumber,
                                VendorName = z.Key.VendorName,
                                ShipDate = z.Key.ShipDate,
                                Status = z.Key.Status,
                                Quantity = z.Sum(a => a.Quantity)
                            }).ToList();

                    foreach (var item in myProductVPOs)
                    {
                        vpoDetails.Append("<tr style='border-bottom:1px solid #ddd;'>");
                        if (item.PICPO != null)
                        {
                            vpoDetails.Append("<td>" + item.PONumber.ToString() + "</td>");
                        }
                        else
                        {
                            vpoDetails.Append("<td></td>");
                        }
                        vpoDetails.Append("<td>" + item.VendorName + "</td>");
                        vpoDetails.Append("<td>" + item.Quantity.ToString() + "</td>");
                        if (item.ShipDate.HasValue)
                        {
                            vpoDetails.Append("<td>" + TimeZoneManager.ToLocal(item.ShipDate.Value)
                                                 .GlobalDateFormat() + "</td>");
                        }
                        else
                        {
                            vpoDetails.Append("<td>" + "" + "</td>");
                        }

                        vpoDetails.Append("<td>" + item.Status + "</td>");
                        vpoDetails.Append("</tr>");
                    }

                }
                vpoDetails.Append("</tbody>");
                vpoDetails.Append("</table>");

                data = data.Replace("{VPODetails}", vpoDetails.ToString());

            }
            catch (Exception ex)
            {
                return data;
            }
            return data;
        }

        /// <summary>
        /// Header will be same for all brand MPO's just get them based on brand ID and update the MPO info
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="mpo"></param>
        /// <returns></returns>
        public string MPOHeader(int orderId, MasterPurchaseOrder mpo)
        {
            string LeadData = string.Empty;
            string Path = string.Empty;
            if (SessionManager.BrandId == 1)
            {
                Path = string.Format("/Templates/Base/Brand/BB/BB_VPO_Template-header.html");
            }
            else if (SessionManager.BrandId == 2)
            {
                Path = string.Format("/Templates/Base/Brand/TL/TL_VPO_Template-header.html");
            }
            else
            {
                Path = string.Format("/Templates/Base/Brand/CC/CC_VPO_Template-header.html");
            }

            string srcFilePath = HostingEnvironment.MapPath(Path);
            int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
            if (poManager == null)
            {
                poManager = new PurchaseOrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            }
            string data = System.IO.File.ReadAllText(srcFilePath);

            string created = mpo.CreateDate.GlobalDateFormat();
            string submitted = mpo.SubmittedDate != null ? mpo.SubmittedDate?.GlobalDateFormat() : "";
           
            var str =
                "<h1>Master Purchase Order#: " + mpo.MasterPONumber.ToString() + "</h1>";
            str = str + " <label><b>Created Date: </b>" + created + "</b></label><label><b>Submitted Date: </b>" + submitted + " </label>";
            data = data.Replace("{header}", str);

            return data;
        }

        /// <summary>
        /// /Footer will be same for all VPO's and MPO's
        /// </summary>
        /// <param name="QuoteKey"></param>
        /// <returns></returns>
        public string MPOFooter(int QuoteKey)
        {
            string Path = string.Empty;

            if (SessionManager.BrandId == 1)
            {
                Path = string.Format("/Templates/Base/Brand/BB/BB_MPO_Template-footer.html");
            }
            else if (SessionManager.BrandId == 2)
            {
                Path = string.Format("/Templates/Base/Brand/TL/TL_MPO_Template-footer.html");
            }
            else
            {
                Path = string.Format("/Templates/Base/Brand/CC/CC_MPO_Template-footer.html");
            }
            string srcFilePath = HostingEnvironment.MapPath(Path);

            string data = System.IO.File.ReadAllText(srcFilePath);
            data = data.Replace("{printeddate}", TimeZoneManager.ToLocal(DateTime.UtcNow).GlobalDateFormat());

            return data;
        }


        public PdfDocument createMPOPDFDoc(int purchaseOrderId)
        {
            string fileDownloadName = "";
            fileDownloadName = string.Format("masterPurchaseOrder_{0}.pdf", purchaseOrderId);

            if (poManager == null)
            {
                poManager = new PurchaseOrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            }

            var MPO = poManager.GetMasterPurchaseOrder(purchaseOrderId);

            string data = MPOSheet(purchaseOrderId, MPO);
            string headerdata = MPOSheetHeader(purchaseOrderId, MPO);
            string footerdata = MPOSheetFooter(purchaseOrderId);

            string baseUrl = Serializer.Leads.LeadSheetReport.TemplateImageFilePath();

            // instantiate a html to pdf converter object 
            HtmlToPdf converter = new HtmlToPdf();

            converter.Options.PdfPageSize = PdfPageSize.Letter;
            converter.Options.PdfPageOrientation = PdfPageOrientation.Portrait;

            converter.Options.MarginTop = 20;
            converter.Options.MarginBottom = 30;
            converter.Options.MarginLeft = 10;
            converter.Options.MarginRight = 10;

            converter.Options.DisplayHeader = true;
            converter.Header.DisplayOnFirstPage = true;
            converter.Header.DisplayOnOddPages = true;
            converter.Header.DisplayOnEvenPages = true;
            converter.Header.Height = 100;

            PdfHtmlSection headerHtml = new PdfHtmlSection(headerdata, baseUrl);
            headerHtml.AutoFitHeight = HtmlToPdfPageFitMode.AutoFit;
            converter.Header.Add(headerHtml);

            converter.Options.DisplayFooter = true;
            converter.Footer.DisplayOnFirstPage = true;
            converter.Footer.DisplayOnEvenPages = true;
            converter.Footer.DisplayOnOddPages = true;
            converter.Footer.Height = 50;

            PdfHtmlSection footerHtml = new PdfHtmlSection(footerdata, baseUrl);
            footerHtml.AutoFitHeight = HtmlToPdfPageFitMode.AutoFit;
            converter.Footer.Add(footerHtml);

            // page numbers can be added using a PdfTextSection object
            PdfTextSection text = new PdfTextSection(300, 25, "Page {page_number} of {total_pages}  ", new System.Drawing.Font("Arial", 8));
            text.HorizontalAlign = PdfTextHorizontalAlign.Center;
            converter.Footer.Add(text);

            PdfDocument doc = converter.ConvertHtmlString(data, baseUrl);

            // save pdf document 
            //byte[] pdf = doc.Save();

            // close pdf document 
            //doc.Close();

            //Guid fileId = new Guid();
            //OrderManager mgr = new OrderManager();
            //fileId = mgr.UploadFile(id, pdf, fileDownloadName);
            return doc;
        }



        public PdfDocument VPOPrintDocument(int OrderId, MasterPurchaseOrder MPO, VPODetail vpo)
        {
            string data = VPOSheet(OrderId, MPO, vpo);
            string headerdata = VPOSheetHeader(OrderId, MPO, vpo);
            string footerdata = VPOSheetFooter(OrderId);

            string baseUrl = Serializer.Leads.LeadSheetReport.TemplateImageFilePath();

            // instantiate a html to pdf converter object 
            HtmlToPdf converter = new HtmlToPdf();

            converter.Options.PdfPageSize = PdfPageSize.Letter;
            converter.Options.PdfPageOrientation = PdfPageOrientation.Portrait;

            converter.Options.MarginTop = 20;
            converter.Options.MarginBottom = 30;
            converter.Options.MarginLeft = 10;
            converter.Options.MarginRight = 10;

            converter.Options.DisplayHeader = true;
            converter.Header.DisplayOnFirstPage = true;
            converter.Header.DisplayOnOddPages = true;
            converter.Header.DisplayOnEvenPages = true;
            converter.Header.Height = 100;

            PdfHtmlSection headerHtml = new PdfHtmlSection(headerdata, baseUrl);
            headerHtml.AutoFitHeight = HtmlToPdfPageFitMode.AutoFit;
            converter.Header.Add(headerHtml);

            converter.Options.DisplayFooter = true;
            converter.Footer.DisplayOnFirstPage = true;
            converter.Footer.DisplayOnEvenPages = true;
            converter.Footer.DisplayOnOddPages = true;
            converter.Footer.Height = 50;

            PdfHtmlSection footerHtml = new PdfHtmlSection(footerdata, baseUrl);
            footerHtml.AutoFitHeight = HtmlToPdfPageFitMode.AutoFit;
            converter.Footer.Add(footerHtml);

            // page numbers can be added using a PdfTextSection object
            PdfTextSection text = new PdfTextSection(300, 25, "Page {page_number} of {total_pages}  ", new System.Drawing.Font("Arial", 8));
            text.HorizontalAlign = PdfTextHorizontalAlign.Center;
            converter.Footer.Add(text);

            PdfDocument doc = converter.ConvertHtmlString(data, baseUrl);
            return doc;

        }


        public PdfDocument createVPOPDFDoc(int OrderId, int VPOId = 0)
        {
            string fileDownloadName = "";
            PdfDocument new_doc = new PdfDocument();
            fileDownloadName = string.Format("VpoPrint_{0}.pdf", OrderId);

            if (poManager == null)
            {
                poManager = new PurchaseOrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            }

            var MPO = poManager.GetMasterPurchaseOrder(OrderId);



            if (VPOId == 0)
            {
                if (MPO.CoreProductVPOs != null && MPO.CoreProductVPOs.Count > 0)
                {
                    var coreList = MPO.CoreProductVPOs.GroupBy(x => new { x.VendorName, x.PICPO }).Select(x => x.First()).ToList();
                    foreach (var core in coreList)
                    {
                        var doc = VPOPrintDocument(OrderId, MPO, core);
                        new_doc.Append(doc);
                    }

                }

                if (MPO.MyProductVPOs != null && MPO.MyProductVPOs.Count > 0)
                {
                    var myprdList = MPO.MyProductVPOs.GroupBy(x => new { x.PONumber, x.VendorName }).Select(x => x.First()).ToList();
                    foreach (var myprd in myprdList)
                    {
                        var doc = VPOPrintDocument(OrderId, MPO, myprd);
                        new_doc.Append(doc);
                    }
                }
            }
            else
            {
                var core = MPO.CoreProductVPOs.Where(x => x.PurchaseOrdersDetailId == VPOId).FirstOrDefault();
                var myprod = MPO.MyProductVPOs.Where(x => x.PurchaseOrdersDetailId == VPOId).FirstOrDefault();

                if (core != null)
                {
                    var doc = VPOPrintDocument(OrderId, MPO, core);
                    new_doc.Append(doc);
                }
                if (myprod != null)
                {
                    var doc = VPOPrintDocument(OrderId, MPO, myprod);
                    new_doc.Append(doc);
                }
            }


            return new_doc;
        }

        public static string GetShippingContent(int id, int sourceType)
        {
            string data = string.Empty;
            try
            {
                data = GetShipCustomernotice(id, sourceType);

                return data;

            }
            catch (Exception ex)
            {
                data = PurchaseOrderSheet.error;
                return data;
            }

        }
        private static string GetShipCustomernotice(int id, int sourceType)
        {
            string ShipData = string.Empty;
            string data = string.Empty;
            if (id > 0)
            {
                data = GetshipInformation(id, sourceType);
            }
            else
            {
                // data = GetAccountInformation(id);
                data = null;
            }
            return data;
        }

        private static string GetshipInformation(int id, int sourceType)
        {

            //var path = GetTemplateName("Lead");
            var path = GetTemplateName(sourceType);
            string srcFilePath = HostingEnvironment.MapPath(path);

            var data = System.IO.File.ReadAllText(srcFilePath);

            //Lead lead = leadsMng.Get(leadId);
            ShipNotice shippingdata = shipMngr.GetValue(id);


            if (shippingdata != null)
            {
                if (shippingdata.ShipmentId != null)
                {
                    data = data.Replace("{{ShipId}}", shippingdata.ShipmentId);
                }
                else
                {
                    data = data.Replace("{{ShipId}}", "");
                }
                if (shippingdata.Weight != null)
                {
                    data = data.Replace("{{weight}}", shippingdata.Weight);
                }
                else
                    data = data.Replace("{{weight}}", "");
            }
            return data;
        }

        private static string GetTemplateName(int sourceType)
        {
            string path = string.Empty;

            //if (sourceType == "Lead")
            //{
            if (sourceType == 1)
            {
                path = string.Format("/Templates/Base/Brand/BB/ShippingNotice.html");
            }
            else if (sourceType == 2)
            {
                path = string.Format("/Templates/Base/Brand/TL/ShippingNotice.html");
            }
            else
            {
                path = string.Format("/Templates/Base/Brand/CC/ShippingNotice.html");
            }
            //}



            return path;
        }

    }
}
