﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Extensions;
using HFC.CRM.DTO.VendorInvoice;
using HFC.CRM.Managers;
using SelectPdf;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace HFC.CRM.Serializer.PurchaseOrder
{
    public class VendorInvoiceSheet
    {
        VendorInvoiceManager Vendor_invoivce = new VendorInvoiceManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        public string VInvoiceSheet(VendorInvoiceDTO VendorInvoiceData)
        {
            string data = string.Empty;
            data = VInvoiceSheetBody(VendorInvoiceData);
            return data;
        }
        public string VendorInvoiceSheetHeader(VendorInvoiceDTO VendorInvoiceData)
        {
            string data = string.Empty;
            data = VInvoiceHeader(VendorInvoiceData);
            return data;
        }
        public string VendorInvoiceSheetFooter()
        {
            string data = string.Empty;
            data = VInvoiceFooter();
            return data;
        }

        public string VInvoiceSheetBody(VendorInvoiceDTO VendorInvoiceData)
        {
            string Path = string.Empty;
            Path = string.Format("/Templates/Base/Brand/BB/BB_VendorInvoice_Template-bodycontent.html");
            string srcFilePath = HostingEnvironment.MapPath(Path);
            string data = System.IO.File.ReadAllText(srcFilePath);
            try
            {
                data = data.Replace("{SoldTo}", VendorInvoiceData.SoldTo);
                data = data.Replace("{BillTo}", VendorInvoiceData.BillTo);
                data = data.Replace("{CustomerID}", VendorInvoiceData.CustomerID);
                data = data.Replace("{Currency}", VendorInvoiceData.Currency);
                data = data.Replace("{Terms}", VendorInvoiceData.Terms);
                data = data.Replace("{RemitBy}", VendorInvoiceData.RemitBy?.GlobalDateFormat());

                data = data.Replace("{vpoNumber}", VendorInvoiceData.PurchaseOrder);
                data = data.Replace("{OrderSubtotal}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", VendorInvoiceData.Subtotal ?? 0));
                data = data.Replace("{Discount}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", VendorInvoiceData.Discount ?? 0));
                data = data.Replace("{Freight}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", VendorInvoiceData.Freight ?? 0));
                data = data.Replace("{ProcessingFee}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", VendorInvoiceData.ProcessingFee ?? 0));
                data = data.Replace("{MicCharge}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", VendorInvoiceData.MiscCharge ?? 0));
                data = data.Replace("{Tax}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", VendorInvoiceData.Tax ?? 0));
                data = data.Replace("{TotalAmount}", string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", VendorInvoiceData.TotalAmount ?? 0));

                if (VendorInvoiceData != null && !string.IsNullOrEmpty(VendorInvoiceData.VendorComments) && VendorInvoiceData.VendorComments != "")
                    data = data.Replace("{pagebreakafter}", "page-break-after: always;");
                else
                    data = data.Replace("{pagebreakafter}", "");

                data = data.Replace("{HeaderLevelComments}", VendorInvoiceData != null && !string.IsNullOrEmpty(VendorInvoiceData.VendorComments) && VendorInvoiceData.VendorComments != "" ? "<h3 style='display:inline'>Vendor Comments :</h3>" + VendorInvoiceData.VendorComments : "");

                StringBuilder viDetails = new StringBuilder();

                viDetails.Append("<table class='table table - bordered invoice_tablegrid'>");
                viDetails.Append("<thead class='expand_lhead'>");
                viDetails.Append("<tr class='invoice_headsection'>");
                viDetails.Append("<th style='width:80px;'>PO<br/>Line #</th>");
                viDetails.Append("<th>Product Number</th>");
                viDetails.Append("<th>Item Description</th>");
                viDetails.Append("<th>QTY</th>");
                viDetails.Append("<th>Cost</th>");
                viDetails.Append("<th>Discount</th>");
                viDetails.Append("<th>Amount</th>");
                viDetails.Append("<th style='width:200px;'>Vendor Comments</th>");
                viDetails.Append("</tr>");
                viDetails.Append("</thead>");
                viDetails.Append("<tbody>");

                if (VendorInvoiceData != null && VendorInvoiceData.VendorInvoiceDetail.Count > 0)
                {
                    foreach (var vid in VendorInvoiceData.VendorInvoiceDetail)
                    {
                        viDetails.Append("<tr class='invoice_headsection-2' style='page-break-inside: avoid'>");
                        viDetails.Append("<td>" + vid.POLineNum + "</td>");
                        viDetails.Append("<td>" + vid.ProductNumber + "</td>");
                        viDetails.Append("<td>" + vid.ItemDescription + "</td>");
                        viDetails.Append("<td>" + vid.QTY + "</td>");
                        viDetails.Append("<td>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", vid.Cost ?? 0) + "</td>");
                        viDetails.Append("<td>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", vid.Discount ?? 0) + "</td>");
                        viDetails.Append("<td>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", vid.Amount ?? 0) + "</td>");
                        viDetails.Append("<td>" + vid.Comment + "</td>");
                        viDetails.Append("</tr>");
                    }
                }
                viDetails.Append("</tbody>");
                viDetails.Append("</table>");

                data = data.Replace("{VendorInvoiceDetails}", viDetails.ToString());

            }
            catch (Exception ex)
            {
                return data;
            }
            return data;
        }

        public string VInvoiceHeader(VendorInvoiceDTO VendorInvoiceData)
        {
            string LeadData = string.Empty;
            string Path = string.Empty;
            //if (SessionManager.BrandId == 1)
            //{
            Path = string.Format("/Templates/Base/Brand/BB/BB_VendorInvoice_Template-header.html");
            //}
            //else if (SessionManager.BrandId == 2)
            //{
            //    Path = string.Format("/Templates/Base/Brand/TL/TL_VPO_Template-header.html");
            //}
            //else
            //{
            //    Path = string.Format("/Templates/Base/Brand/CC/CC_VPO_Template-header.html");
            //}

            string srcFilePath = HostingEnvironment.MapPath(Path);
            string data = System.IO.File.ReadAllText(srcFilePath);

            data = data.Replace("{InvoiceNumber}", VendorInvoiceData.InvoiceNo);
            data = data.Replace("{Vendor}", VendorInvoiceData.Vendor);
            data = data.Replace("{InvoiceDate}", VendorInvoiceData.InvoiceDate?.GlobalDateFormat());

            return data;
        }

        public string VInvoiceFooter()
        {
            string Path = string.Empty;

            //if (SessionManager.BrandId == 1)
            //{
            Path = string.Format("/Templates/Base/Brand/BB/BB_VendorInvoice_Template-footer.html");
            //}
            //else if (SessionManager.BrandId == 2)
            //{
            //    Path = string.Format("/Templates/Base/Brand/TL/TL_MPO_Template-footer.html");
            //}
            //else
            //{
            //    Path = string.Format("/Templates/Base/Brand/CC/CC_MPO_Template-footer.html");
            //}
            string srcFilePath = HostingEnvironment.MapPath(Path);

            string data = System.IO.File.ReadAllText(srcFilePath);
            data = data.Replace("{printeddate}", TimeZoneManager.ToLocal(DateTime.UtcNow).GlobalDateFormat());

            return data;
        }

        public PdfDocument createVendorInvoiceDoc(string invoiceNumber)
        {
            // Get VendorInvoice details
            var VendorInvoiceData = Vendor_invoivce.getdetail(invoiceNumber);

            //Generate Header, Body, Footer html 
            string data = VInvoiceSheet(VendorInvoiceData);
            string headerdata = VendorInvoiceSheetHeader(VendorInvoiceData);
            string footerdata = VendorInvoiceSheetFooter();

            return GeneratePdfDocument(headerdata, data, footerdata);
        }

        public string VendorInvoiceFiledownloadname(string invoiceNumber)
        {
            // Get VendorInvoice details
            var VendorInvoiceData = Vendor_invoivce.getdetail(invoiceNumber);
            return "Invoice " + VendorInvoiceData.InvoiceNo + " - " + VendorInvoiceData.Vendor + ".pdf";
        }

        public PdfDocument GeneratePdfDocument(string headerdata, string bodydata, string footerdata)
        {

            string baseUrl = Serializer.Leads.LeadSheetReport.TemplateImageFilePath();

            // instantiate a html to pdf converter object 
            HtmlToPdf converter = new HtmlToPdf();

            converter.Options.PdfPageSize = PdfPageSize.Letter;
            converter.Options.PdfPageOrientation = PdfPageOrientation.Portrait;

            converter.Options.MarginTop = 20;
            converter.Options.MarginBottom = 30;
            converter.Options.MarginLeft = 10;
            converter.Options.MarginRight = 10;

            converter.Options.DisplayHeader = true;
            converter.Header.DisplayOnFirstPage = true;
            converter.Header.DisplayOnOddPages = true;
            converter.Header.DisplayOnEvenPages = true;
            converter.Header.Height = 60;

            PdfHtmlSection headerHtml = new PdfHtmlSection(headerdata, baseUrl);
            headerHtml.AutoFitHeight = HtmlToPdfPageFitMode.AutoFit;
            converter.Header.Add(headerHtml);

            converter.Options.DisplayFooter = true;
            converter.Footer.DisplayOnFirstPage = true;
            converter.Footer.DisplayOnEvenPages = true;
            converter.Footer.DisplayOnOddPages = true;
            converter.Footer.Height = 50;

            PdfHtmlSection footerHtml = new PdfHtmlSection(footerdata, baseUrl);
            footerHtml.AutoFitHeight = HtmlToPdfPageFitMode.AutoFit;
            converter.Footer.Add(footerHtml);

            // page numbers can be added using a PdfTextSection object
            PdfTextSection text = new PdfTextSection(300, 25, "Page {page_number} of {total_pages}  ", new System.Drawing.Font("Arial", 8));
            text.HorizontalAlign = PdfTextHorizontalAlign.Center;
            converter.Footer.Add(text);

            PdfDocument doc = converter.ConvertHtmlString(bodydata, baseUrl);

            return doc;
        }

    }
}
