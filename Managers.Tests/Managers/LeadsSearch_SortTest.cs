﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Membership;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using HFC.CRM.DTO.Search;
using HFC.CRM.Managers;
using HFC.CRM.Web.Controllers.API;
using HFC.CRM.Web.Controllers.API.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ManagerApp.Tests.Managers
{
    [TestClass()]
    public class LeadsSearch_SortTests
    {
        //public int leadId;
        //public LeadsController controller { get; set; }
        //public User user { get; set; }

        
        //public CRMContext DB { get; set; }

        //[TestInitialize()]
        //public void Init()
        //{
        //    // Mock HttpContext (creates session & User properties)
        //    HttpContext.Current = HttpContextFactory.FakeHttpContext();
        //    ContextFactory.Current.Init(new WebContextProvider());
        //    DB = ContextFactory.Current.Context;
        //    // Set current FE & User
        //    user = LocalMembership.GetUser("admin", "Person", "Roles", "OAuthUsers");
        //    SessionManager.CurrentUser = user;
        //}

     
        //[TestMethod]
        //public void LeadsSort()
        //{
            
        //    var fes = DB.Franchises.Include("Leads").Where(x=> x.Leads.Count() > 10).OrderBy(x => x.Leads.Count()).Select(x => x.FranchiseId).Take(10).ToList();
        //    foreach (var fran in fes)
        //    {
        //        var franchise = DB.Franchises.FirstOrDefault(x => x.FranchiseId == fran);
        //        SessionManager.CurrentFranchise = franchise;
        //        using (var sm = new SearchManager(user, franchise))
        //        {
        //            System.Diagnostics.Trace.WriteLine(String.Format("Start test " + fran)); 
        //            foreach (var search in SearchSetting.GetAll())
        //                foreach (var order in OrderSetting.GetAll())
        //                    foreach (OrderByEnum orderDesc in Enum.GetValues(typeof (OrderByEnum)))
        //                    {
                                
        //                        System.Diagnostics.Trace.WriteLine(String.Format("Start test - {0}, {1}, {2}, {3}", search.SearchFilter, search.SearchTerm,  order.OrderBy, orderDesc));
        //                        var res = new List<LeadSeachResultModel>();
        //                        var list1 = sm.GetLeads(searchFilter: search.SearchFilter, searchTerm: search.SearchTerm,
        //                            orderBy: order.OrderBy, orderByDirection: orderDesc, pageIndex: 1,
        //                            pageSize: 5);
        //                        var list2 = sm.GetLeads(searchFilter: search.SearchFilter, searchTerm: search.SearchTerm,
        //                           orderBy: order.OrderBy, orderByDirection: orderDesc, pageIndex: 2,
        //                           pageSize: 5);

        //                        list1.Wait(TimeSpan.FromMinutes(5));
        //                        res.AddRange(list1.Result);
                                
        //                        list2.Wait(TimeSpan.FromMinutes(5));
        //                        res.AddRange(list2.Result);
                                
                                
        //                        var res2 = order.Order(res, orderDesc);
        //                        Assert.AreEqual(res.Count(), res2.Count(), 
        //                            String.Format("ERROR COUNT WRONG - {0}, {1}, {2}, {3}", search.SearchFilter, search.SearchTerm,  order.OrderBy, orderDesc) );

                          
                                
        //                        for (var i = 0; i < res.Count(); i++)
        //                        {
        //                            if (order.OrderBy == "addresses")
        //                            {
        //                                if (!String.IsNullOrEmpty(res[i].City) && !String.IsNullOrEmpty(res2[i].City))
        //                                {
        //                                    Assert.AreEqual(res[i].City + res[i].ZipCode + res[i].Address1,
        //                                        res2[i].City + res2[i].ZipCode + res2[i].Address1,
        //                                        String.Format("ERROR ORDER WRONG - {0}, {1}, {2}, {3}",
        //                                            search.SearchFilter,
        //                                            search.SearchTerm, order.OrderBy, orderDesc));
        //                                }
        //                            }
        //                            else
        //                            {
        //                                Assert.AreEqual(res[i].LeadId, res2[i].LeadId,
        //                                    String.Format("ERROR ORDER WRONG - {0}, {1}, {2}, {3}", search.SearchFilter,
        //                                        search.SearchTerm, order.OrderBy, orderDesc));
        //                            }
        //                        }
        //                    }
        //        }
        //    }
            
        //}

        ////fix LeadsSort test before fix this.
        //[TestMethod]
        //public void LeadsExport()
        //{

        //    var fes = DB.Franchises.Include("Leads").Where(x => x.Leads.Count() > 10).OrderBy(x => x.Leads.Count()).Select(x => x.FranchiseId).Take(10).ToList();
        //    foreach (var fran in fes)
        //    {
        //        var franchise = DB.Franchises.FirstOrDefault(x => x.FranchiseId == fran);
        //        SessionManager.CurrentFranchise = franchise;
        //        using (var sm = new SearchManager(user, franchise))
        //        {
        //            System.Diagnostics.Trace.WriteLine(String.Format("Start test " + fran));
        //            foreach (var search in SearchSetting.GetAll())
        //                foreach (var order in OrderSetting.GetAll())
        //                    foreach (OrderByEnum orderDesc in Enum.GetValues(typeof(OrderByEnum)))
        //                    {

        //                        System.Diagnostics.Trace.WriteLine(String.Format("Start test - {0}, {1}, {2}, {3}", search.SearchFilter, search.SearchTerm, order.OrderBy, orderDesc));
        //                        var res = new List<LeadSeachResultModel>();
        //                        var res2 = new List<LeadSeachResultModel>();
        //                        var list1 = sm.GetLeads(searchFilter: search.SearchFilter, searchTerm: search.SearchTerm,
        //                            orderBy: order.OrderBy, orderByDirection: orderDesc, pageIndex: 1,
        //                            pageSize: 5);
        //                        var list2 = sm.GetLeads(searchFilter: search.SearchFilter, searchTerm: search.SearchTerm,
        //                           orderBy: order.OrderBy, orderByDirection: orderDesc, pageIndex: 2,
        //                           pageSize: 5);
        //                        var list3 = sm.GetLeads(searchFilter: search.SearchFilter, searchTerm: search.SearchTerm,
        //                            orderBy: order.OrderBy, orderByDirection: orderDesc, pageIndex: 1,
        //                            pageSize: 10);
        //                        list1.Wait(TimeSpan.FromMinutes(5));
        //                        res.AddRange(list1.Result);

        //                        list2.Wait(TimeSpan.FromMinutes(5));
        //                        res.AddRange(list2.Result);

        //                        list3.Wait(TimeSpan.FromMinutes(5));
        //                        res2.AddRange(list3.Result);

        //                        //Assert.AreEqual(res.Count(), res2.Count(),
        //                        //    String.Format("ERROR COUNT WRONG - {0}, {1}, {2}, {3}", search.SearchFilter, search.SearchTerm, order.OrderBy, orderDesc));
                                



        //                        for (var i = 0; i < res.Count(); i++)
        //                        {
        //                            if (order.OrderBy == "addresses")
        //                            {
        //                                if (!String.IsNullOrEmpty(res[i].City) && !String.IsNullOrEmpty(res2[i].City))
        //                                {
        //                                    Assert.AreEqual(res[i].City + res[i].ZipCode + res[i].Address1,
        //                                        res2[i].City + res2[i].ZipCode + res2[i].Address1,
        //                                        String.Format("ERROR ORDER WRONG - {0}, {1}, {2}, {3}",
        //                                            search.SearchFilter,
        //                                            search.SearchTerm, order.OrderBy, orderDesc));
        //                                }
        //                            }
        //                            else
        //                            {
        //                                Assert.AreEqual(res[i].LeadId, res2[i].LeadId,
        //                                    String.Format("ERROR ORDER WRONG - {0}, {1}, {2}, {3}", search.SearchFilter,
        //                                        search.SearchTerm, order.OrderBy, orderDesc));
        //                            }
        //                        }
        //                    }
        //        }
        //    }

        //}
        
        //[TestCleanup]
        //public void cleanup()
        //{
            
        //}


        //public class SearchSetting
        //{
        //    public string SearchTerm { get; set; }

        //    public SearchFilterEnum SearchFilter { get; set; }

        //    public static List<SearchSetting> GetAll()
        //    {
        //        var res = new List<SearchSetting>();
        //        //res.Add(new SearchSetting() { SearchFilter = SearchFilterEnum.Phone_Number, SearchTerm = "" });
        //        //res.Add(new SearchSetting() { SearchFilter = SearchFilterEnum.Address, SearchTerm = " " });
        //        res.Add(new SearchSetting() { SearchFilter = SearchFilterEnum.Auto_Detect, SearchTerm = " " });
        //        res.Add(new SearchSetting() { SearchFilter = SearchFilterEnum.Customer_Name, SearchTerm = " " });
        //        res.Add(new SearchSetting() { SearchFilter = SearchFilterEnum.Email, SearchTerm = "@" });
        //        res.Add(new SearchSetting() { SearchFilter = SearchFilterEnum.Number, SearchTerm = " " });
        //        res.Add(new SearchSetting() { SearchFilter = SearchFilterEnum.Phone_Number, SearchTerm = " " });
                
        //        return res;
        //    }

        //    //public 
        //}

        //public class OrderSetting
        //{
        //    public string OrderBy { get; set; }


        //    public static List<OrderSetting> GetAll()
        //    {
        //        var res = new List<OrderSetting>();
        //        res.Add(new OrderSetting() { OrderBy = "primperson" });
        //        res.Add(new OrderSetting() { OrderBy = "addresses" });
        //        res.Add(new OrderSetting() { OrderBy = "leadStatusid" });
        //        return res;

        //    }

        //    public List<LeadSeachResultModel> Order(List<LeadSeachResultModel> list, OrderByEnum OrderDirection)
        //    {
        //        switch (OrderBy)
        //        {
        //            case "addresses":
        //                return OrderDirection == OrderByEnum.Asc
        //                    ? list.OrderBy(x => (!String.IsNullOrEmpty(x.City) && String.IsNullOrEmpty(x.ZipCode) && String.IsNullOrEmpty(x.Address1) ?  x.City+x.ZipCode+x.Address1 : "")).ToList()
        //                    : list.OrderByDescending(x => (!String.IsNullOrEmpty(x.City) && String.IsNullOrEmpty(x.ZipCode) && String.IsNullOrEmpty(x.Address1) ? x.City + x.ZipCode + x.Address1 : "")).ToList();

        //            case "primperson":
        //                return OrderDirection == OrderByEnum.Asc
        //                    ? list.OrderBy(x => x.FirstName).ToList()
        //                    : list.OrderByDescending(x => x.FirstName).ToList();
        //            case "leadStatusid":
        //                return OrderDirection == OrderByEnum.Asc
        //                    ? list.OrderBy(x => x.LeadStatusId).ToList()
        //                    : list.OrderByDescending(x => x.LeadStatusId).ToList();

                 
        //            default:
        //                throw new Exception("Order  " + OrderBy + " not found!");
        //        }
        //    }
        //}   
    }
}

