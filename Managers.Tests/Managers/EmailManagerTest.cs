﻿namespace HFC.CRM.Managers.Tests
{
    using HFC.CRM.Core.Common;
    using HFC.CRM.Core.Membership;
    using HFC.CRM.Data;
    using HFC.CRM.Data.Context;
    using HFC.CRM.Managers;
    using HFC.CRM.Web.Controllers.API.Tests;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System.Collections.Generic;
    using System.Web;

    [TestClass]
    public class EmailManagerTest
    {
        public User user { get; set; }
        public Franchise fran { get; set; }
        public CRMContext DB { get; set; }

        [TestInitialize()]
        public void Init()
        {
            // Mock HttpContext (creates session & User properties)
            HttpContext.Current = HttpContextFactory.FakeHttpContext();
            ContextFactory.Current.Init(new WebContextProvider());
            DB = ContextFactory.Current.Context;
            // Set current FE & User // BB test US
            user = LocalMembership.GetUser("19001001", "Person", "Roles", "OAuthUsers");
            SessionManager.CurrentUser = user;

            FranchiseManager fm = new FranchiseManager(user);
            fran = fm.GetFEinfo(2);
            SessionManager.CurrentFranchise = fran;

        }

        [TestMethod]
        public void Test_SendEmail()
        {
            var ExchangeManager = new ExchangeManager();
            var res = ExchangeManager.SendEmail("synctest1@budgetblinds.com", "Subject - Test Email", "Body - Test Email", new List<string>() { "rrajkumar@mailinator.com" });
            Assert.IsTrue(res);
        }

        [TestMethod]
        public void Test_SendSMS()
        {
            //9497838417   V- 9496145664  2292990344 9497838417
            CommunicationManager Communication = new CommunicationManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var res = Communication.SendSms("9497838417", "Touchpoint - Test SMS",1,"US");
            Assert.IsTrue(res.status);
        }

        [TestMethod]
        public void Test_OptingInfo()
        {
            CommunicationManager Communication = new CommunicationManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var res = Communication.SendOptinMessage("9497838417", 1,"US");
            Assert.IsTrue(res != null);
        }

    }
}