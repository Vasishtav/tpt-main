﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Text;

//using System.Web.Mvc;
//using System.Web;
//using System.Web.Script.Serialization;
//using System.Data;
//using System.Data.Common;
//using System.Data.Entity;
//using System.Data.SqlClient;
//using System.Data.Entity.Infrastructure;
//using System.Data.Entity.Core.Objects;
//using System.Threading.Tasks;
//using System.Security.Principal;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using System.Web.SessionState;
//using System.Runtime.InteropServices;
//using System.Reflection;


//using System.Collections.Specialized;
//using System.Web.Routing;
//using System.Web.Security;
//using System.Configuration;

//using HFC.CRM.Managers;

//using HFC.CRM.Data;
//using HFC.CRM.Core;
//using HFC.CRM.Core.Common;
//using HFC.CRM.Web;
//using HFC.CRM.Web.Controllers;
//using api=HFC.CRM.Web.Controllers.API;
//using HFC.CRM.Core.Membership;

//using FluentAssertions;
//using FluentAssertions.Mvc;

//using WebMatrix.WebData;
//using Moq;

//namespace HFC.CRM.Managers.Tests
//{
//    public class HttpContextFactory
//    {
//        public static HttpContext Current
//        {
//            get
//            {
//                //return GetMockedHttpContext();
//                return new System.Web.HttpContext(new HttpRequest("", "localhost:8898", ""), new HttpResponse(new StringWriter()) );
//            }
//        }
//        public static HttpContext FakeHttpContext()
//        {
//            var httpRequest = new HttpRequest("", "http://stackoverflow/", "");
//            var stringWriter = new StringWriter();
//            var httpResponce = new HttpResponse(stringWriter);
//            var httpContext = new HttpContext(httpRequest, httpResponce);

//            var sessionContainer = new HttpSessionStateContainer("id", new SessionStateItemCollection(),
//                                                    new HttpStaticObjectsCollection(), 10, true,
//                                                    HttpCookieMode.AutoDetect,
//                                                    SessionStateMode.InProc, false);

//            httpContext.Items["AspSession"] = typeof(HttpSessionState).GetConstructor(
//                                        BindingFlags.NonPublic | BindingFlags.Instance,
//                                        null, CallingConventions.Standard,
//                                        new[] { typeof(HttpSessionStateContainer) },
//                                        null)
//                                .Invoke(new object[] { sessionContainer });

//            return httpContext;
//        }
//        public static HttpContext GetMockedHttpContext()
//        {
//            var context = new Mock<HttpContext>();
//            var request = new Mock<HttpRequest>("", @"localhost:8898", "");
//            var response = new Mock<HttpResponse>();
//            var session = new Mock<HttpSessionState>();
//            var server = new Mock<HttpServerUtilityBase>();
//            var user = new Mock<IPrincipal>();
//            var identity = new Mock<IIdentity>();
//            var urlHelper = new Mock<UrlHelper>();

//            var routes = new RouteCollection();
//            //MvcApplication.RegisterRoutes(routes);
//            var requestContext = new Mock<RequestContext>();
//            var sessionContainer = new HttpSessionStateContainer("id", new SessionStateItemCollection(),
//                                            new HttpStaticObjectsCollection(), 10, true,
//                                            HttpCookieMode.AutoDetect,
//                                            SessionStateMode.InProc, false);
//            //context.Items["AspSession"] = typeof(HttpSessionState).GetConstructor(
//            //                    BindingFlags.NonPublic | BindingFlags.Instance,
//            //                    null, CallingConventions.Standard,
//            //                    new[] { typeof(HttpSessionStateContainer) },
//            //                    null)
//            //            .Invoke(new object[] { sessionContainer });

//            //requestContext.Setup(x => x.HttpContext).Returns(context.Object);
//            context.Setup(ctx => ctx.Request).Returns(request.Object);
//            context.Setup(ctx => ctx.Response).Returns(response.Object);
//            //context.Setup(ctx => ctx.Session).Returns()
//            //context.Setup(ctx => ctx.Session).Returns(session.Object);
//            //context.Setup(ctx => ctx.Server).Returns(server.Object);
//            context.Setup(ctx => ctx.User).Returns(user.Object);
//            user.Setup(ctx => ctx.Identity).Returns(identity.Object);
//            identity.Setup(id => id.IsAuthenticated).Returns(true);
//            identity.Setup(id => id.Name).Returns("admin");
//            request.Setup(req => req.Url).Returns(new Uri("localhost:8898"));
//            request.Setup(req => req.RequestContext).Returns(requestContext.Object);
//            requestContext.Setup(x => x.RouteData).Returns(new RouteData());
//            request.SetupGet(req => req.Headers).Returns(new NameValueCollection());

//            return context.Object;
//        }

//    }
//    [TestClass]
//    public class ControlPanel
//    {

        
//        //public CRMContext db { get; set; }
//        public int? FranchiseId { get; set; }
//        MigrationManager MigrationManager { get; set; }
//        AccountingController Accounting { get; set; }

//        [TestInitialize()]
//        public void Init()
//        {
//            Franchise fran = new Franchise() { FranchiseId = 36 };
//            HttpContext.Current = HttpContextFactory.Current;
//            User aUser = LocalMembership.GetUser("admin", "Person", "Roles", "OAuthUsers");
//            SessionManager.SecurityUser = aUser;

//            api.AddressController addressController = new api.AddressController();
//            Accounting = new AccountingController();



//        }

//        [TestMethod]
//        public void MigrateJobs()
//        {
//            Accounting.PurchaseOrders().Should().BeViewResult().WithViewName("PurchaseOrders");
            
//        }
 
//    }
//}
