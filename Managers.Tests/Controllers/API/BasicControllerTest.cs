﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HFC.CRM.Web.Controllers.API;
using HFC.CRM.Data;
using System.Web;
using HFC.CRM.Data.Context;
using HFC.CRM.Core.Membership;
using HFC.CRM.Web.Controllers.API.Tests;
using HFC.CRM.Core.Common;
using HFC.CRM.Managers;
using Newtonsoft.Json;
using HFC.CRM.DTO;
using HFC.CRM.DTO.Address;
using System.Web.Http.Results;
using HFC.CRM.Web;
using System.Collections.Generic;
using System.Linq;
using HFC.CRM.Web.Models;
using System.Dynamic;
using System.Web.Routing;
using System.Web.Http;
using System.Reflection;
using System.IO;

namespace ManagerApp.Tests.Controllers.API
{
    [TestClass]
    public class BasicControllerTest
    {
        public User user { get; set; }
        public Franchise fran { get; set; }
        public CRMContext DB { get; set; }
        public int AccountId, AddressId, OpportunityId, Quotekey;

        [TestInitialize()]
        public void Init()
        {
            // Mock HttpContext (creates session & User properties)
            HttpContext.Current = HttpContextFactory.FakeHttpContext();
            ContextFactory.Current.Init(new WebContextProvider());
            DB = ContextFactory.Current.Context;
            // Set current FE & User // BB test US
            user = LocalMembership.GetUser("19001001", "Person", "Roles", "OAuthUsers");
            SessionManager.CurrentUser = user;

            FranchiseManager fm = new FranchiseManager(user);
            fran = fm.GetFEinfo(2);
            SessionManager.CurrentFranchise = fran;

        }

        Lead l = new Lead()
        {
            LeadStatusId = 1,
            SourcesTPId = 62,
            LeadSources = new List<LeadSource>(),
            PrimCustomer = new CustomerTP()
            {
                CellPhone = "9638527410",
                FirstName = "Rajkumar",
                LastName = DateTime.UtcNow.ToString("MM/dd/yyy HHmmss"),
                PreferredTFN = " ",
                PrimaryEmail = "rrajkumar" + DateTime.UtcNow.ToString("MMddyyy") + "@mailinator.com"
            },
            Addresses = new List<AddressTP>() {
                new AddressTP() {
                    Address1= "5605 N MACARTHUR BLVD",
                    Address2 = "",
                    AddressType = "false",
                    AttentionText = "",
                    City = "IRVING",
                    CountryCode2Digits = "US",
                    CrossStreet = "",
                    IsResidential = false,
                    IsValidated = true,
                    Location = "",
                    State = "TX",
                    ZipCode = "75038"
                }
            },
        };

        AccountTP aTP = new AccountTP()
        {
            AccountStatusId = 1,
            AccountTypeId = 10008,
            AllowFranchisetoReassign = true,
            CampaignId = 0,
            SourcesTPId = 62,
            PrimCustomer = new CustomerTP()
            {
                CellPhone = "9638527410",
                FirstName = "Rajkumar",
                LastName = DateTime.UtcNow.ToString("MM/dd/yyy HHmmss"),
                PreferredTFN = " ",
                PrimaryEmail = "rrajkumar" + DateTime.UtcNow.ToString("MMddyyy") + "@mailinator.com"
            },
            Addresses = new List<AddressTP>() {
                new AddressTP() {
                    Address1= "5605 N MACARTHUR BLVD",
                    Address2 = "",
                    AddressType = "false",
                    AttentionText = "",
                    City = "IRVING",
                    CountryCode2Digits = "US",
                    CrossStreet = "",
                    IsResidential = false,
                    IsValidated = true,
                    Location = "",
                    State = "TX",
                    ZipCode = "75038"
                }
            },
        };

        MeasurementHeader mh = new MeasurementHeader()
        {
            InstallationAddressId = 3635,
            MeasurementBB = new List<MeasurementLineItemBB>() {
                    new MeasurementLineItemBB()
                    {
                        id = 1,
                        WindowLocation = "Window 1 / W1",
                        RoomLocation = "Family  Room",
                        RoomName = "Family  Room - W1",
                        MountTypeName = "OB",
                        FranctionalValueWidth = null,
                        Width = "36",
                        FranctionalValueHeight = null,
                        Height = "52"
                    }
                }
        };

        Opportunity opp = new Opportunity()
        {
            AccountId = 1,
            BillingAddressId = 56,
            InstallationAddressId = 56,
            OpportunityName = "unittest opp " + DateTime.UtcNow.ToString("MM/dd/yyy HHmmss"),
            OpportunityStatusId = 1,
            SalesAgentId = 38,
            SideMark = "unittest side mark " + DateTime.UtcNow.ToString("MM/dd/yyy HHmmss"),
            SourcesTPId = 38,
            InstallationAddress = "InstallationAddress"
        };

        Quote q = new Quote()
        {
            QuoteKey = 0,
            QuoteID = 0,
            QuoteName = "Test Quote " + DateTime.UtcNow.ToString("MM/dd/yyy HHmmss"),
            AccountId = "1",
            PrimaryQuote = true,
            QuoteStatusId = 1,
            SideMark = "unittest side mark " + DateTime.UtcNow.ToString("MM/dd/yyy HHmmss"),
        };

        CoreProductVM cpql = new CoreProductVM()
        {
            Description = "undefined - 1\" Basic (6ga) Cordless Lift and Lock",
            GGroup = 1000,
            Height = 52,
            MeasurementSetId = 1,
            ModelDescription = "1\" Basic (6ga) Cordless Lift and Lock",
            ModelId = "SC_922_SC",
            MountType = "OB",
            OpportunityId = 3249,
            PCategory = "ALUMINUM BLINDS",
            PicProduct = 905086,
            PicVendor = "CBG",
            ProductName = "ALUMINUM BLINDS",
            Quantity = 1,
            QuoteKey = 3273,
            RoomName = "Family  Room - W1",
            VendorId = 9945,
            VendorName = "CUSTOM BRANDS GROUP",
            Width = 36,
            PromptAnswers = "{\"Data1\":{\"value\":\"1\",\"colorValue\":\"\"},\"Data10\":{\"value\":\"SC_922_SC\",\"colorValue\":\"\"},\"Data20\":{\"value\":\"\",\"colorValue\":\"\"},\"Data30\":{\"value\":\"2019-07-04\",\"colorValue\":\"\"},\"Data40\":{\"value\":\"2019-07-04\",\"colorValue\":\"\"},\"Data50\":{\"value\":\"184\",\"colorValue\":\"\"},\"Data60\":{\"value\":\"\",\"colorValue\":\"\"},\"Data70\":{\"value\":\"19001001\",\"colorValue\":\"\"},\"Data80\":{\"value\":[],\"colorValue\":\"\"},\"Data90\":{\"value\":\"\",\"colorValue\":\"\"},\"Data100\":{\"value\":\"\",\"colorValue\":\"\"},\"Data110\":{\"value\":\"\",\"colorValue\":\"\"},\"Data120\":{\"value\":\"N\",\"colorValue\":\"\"},\"Data1001\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1002\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1003\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1004\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1005\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1006\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1007\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1008\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1009\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1010\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1011\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1012\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1013\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1014\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1015\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1016\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1017\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1018\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1019\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1020\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1021\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1022\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1023\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1024\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1025\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1026\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1027\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1028\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1029\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1030\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1031\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1032\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1033\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1034\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1035\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1036\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1037\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1038\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1039\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1040\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1041\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1042\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1043\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1044\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1045\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1046\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1047\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1048\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1049\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data1050\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2001\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2002\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2003\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2004\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2005\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2006\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2007\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2008\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2009\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2010\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2011\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2012\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2013\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2014\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2015\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2016\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2017\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2018\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2019\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2020\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2021\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2022\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2023\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2024\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2025\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2026\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2027\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2028\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2029\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2030\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2031\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2032\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2033\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2034\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2035\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2036\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2037\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2038\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2039\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2040\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2041\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2042\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2043\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2044\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2045\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2046\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2047\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2048\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2049\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data2050\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data3001\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data3002\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data3003\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data3004\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data3005\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data3006\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data3007\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data3008\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data3009\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data3010\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data3011\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data3012\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data3013\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data3014\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data3015\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data3016\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data3017\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data3018\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data3019\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data3020\":{\"value\":\"0\",\"colorValue\":\"\"},\"Data4000\":{\"value\":\"\",\"colorValue\":\"\"},\"Data4001\":{\"value\":\"1515612\",\"colorValue\":\"\"},\"Data4002\":{\"value\":\"184\",\"colorValue\":\"\"},\"Data4003\":{\"value\":\"BB\",\"colorValue\":\"\"},\"Data4004\":{\"value\":\"\",\"colorValue\":\"\"},\"Data4005\":{\"value\":\"\",\"colorValue\":\"\"},\"Data4006\":{\"value\":\"\",\"colorValue\":\"\"},\"Data4007\":{\"value\":\"\",\"colorValue\":\"\"},\"Data4008\":{\"value\":\"\",\"colorValue\":\"\"},\"Data4009\":{\"value\":\"\",\"colorValue\":\"\"},\"Data4010\":{\"value\":\"BUB\",\"colorValue\":\"\"},\"Data4011\":{\"value\":\"50\",\"colorValue\":\"\"},\"Data4012\":{\"value\":\"09\",\"colorValue\":\"\"},\"Data4013\":{\"value\":\"PIC\",\"colorValue\":\"\"},\"Data4014\":{\"value\":\"\",\"colorValue\":\"\"},\"Data4015\":{\"value\":\"\",\"colorValue\":\"\"},\"Data4016\":{\"value\":\"\",\"colorValue\":\"\"},\"Data4017\":{\"value\":\"\",\"colorValue\":\"\"},\"Data4018\":{\"value\":\"\",\"colorValue\":\"\"},\"Data4019\":{\"value\":\"\",\"colorValue\":\"\"},\"Data200\":{\"value\":\"0\",\"colorValue\":\"\"},\"Prompt11\":{\"value\":\"Family  Room - W1\",\"colorValue\":\"\"},\"Prompt1\":{\"value\":\"BAS6CLLK_ESB6\",\"colorValue\":\"\"},\"Prompt13\":{\"value\":\"ESB6\",\"colorValue\":\"\"},\"Prompt14\":{\"value\":\"1\",\"colorValue\":\"\"},\"Prompt15\":{\"value\":\"CLL\",\"colorValue\":\"\"},\"Prompt78\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt79\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt80\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt81\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt82\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt83\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt84\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt85\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt86\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt87\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt88\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt89\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt90\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt91\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt92\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt93\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt94\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt95\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt96\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt97\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt98\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt99\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt100\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt101\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt102\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt103\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt104\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt105\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt106\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt107\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt108\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt109\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt110\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt111\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt112\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt113\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt114\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt115\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt116\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt117\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt118\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt119\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt120\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt121\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt122\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt123\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt124\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt125\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt126\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt127\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt128\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt129\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt130\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt131\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt132\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt133\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt134\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt135\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt137\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt138\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt139\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt140\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt141\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt142\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt143\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt144\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt145\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt146\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt147\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt148\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt149\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt150\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt151\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt152\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt153\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt154\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt155\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt156\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt157\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt158\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt159\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt160\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt161\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt162\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt163\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt164\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt165\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt166\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt167\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt168\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt169\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt170\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt171\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt172\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt173\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt174\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt175\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt176\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt177\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt178\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt179\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt180\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt181\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt182\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt183\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt184\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt185\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt186\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt187\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt188\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt189\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt190\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt191\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt192\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt193\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt194\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt2\":{\"value\":\"184\",\"colorValue\":\"\"},\"Prompt3\":{\"value\":\"E_AL\",\"colorValue\":\"\"},\"Prompt71\":{\"value\":\"PIC\",\"colorValue\":\"\"},\"Prompt4\":{\"value\":\"BCAB\",\"colorValue\":\"\"},\"Prompt195\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt196\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt136\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt5\":{\"value\":\"BB\",\"colorValue\":\"\"},\"Prompt76\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt6\":{\"value\":\"1515612\",\"colorValue\":\"\"},\"Prompt7\":{\"value\":\"BAS6\",\"colorValue\":\"\"},\"Prompt8\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt9\":{\"value\":\"B6001\",\"colorValue\":\"\"},\"Prompt10\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt12\":{\"value\":\"ESB6\",\"colorValue\":\"\"},\"Prompt16\":{\"value\":\"MAN\",\"colorValue\":\"\"},\"Prompt17\":{\"value\":\"B6001\",\"colorValue\":\"\"},\"Prompt70\":{\"value\":\"E_AL--\",\"colorValue\":\"B6001\"},\"Prompt18\":{\"value\":\"36\",\"colorValue\":\"\"},\"Prompt19\":{\"value\":\"52\",\"colorValue\":\"\"},\"Prompt20\":{\"value\":\"O\",\"colorValue\":\"\"},\"Prompt21\":{\"value\":\"1\",\"colorValue\":\"\"},\"Prompt22\":{\"value\":\"0\",\"colorValue\":\"\"},\"Prompt23\":{\"value\":\"L\",\"colorValue\":\"\"},\"Prompt24\":{\"value\":\" \",\"colorValue\":\"\"},\"Prompt25\":{\"value\":\"0\",\"colorValue\":\"\"},\"Prompt26\":{\"value\":\" \",\"colorValue\":\"\"},\"Prompt27\":{\"value\":\" \",\"colorValue\":\"\"},\"Prompt28\":{\"value\":\"0\",\"colorValue\":\"\"},\"Prompt29\":{\"value\":\" \",\"colorValue\":\"\"},\"Prompt30\":{\"value\":\" \",\"colorValue\":\"\"},\"Prompt31\":{\"value\":\"WT\",\"colorValue\":\"\"},\"Prompt32\":{\"value\":\"TRD\",\"colorValue\":\"\"},\"Prompt33\":{\"value\":\"STD\",\"colorValue\":\"\"},\"Prompt34\":{\"value\":\"N\",\"colorValue\":\"\"},\"Prompt35\":{\"value\":\" \",\"colorValue\":\"\"},\"Prompt36\":{\"value\":\"0\",\"colorValue\":\"\"},\"Prompt37\":{\"value\":\"N\",\"colorValue\":\"\"},\"Prompt38\":{\"value\":\"0\",\"colorValue\":\"\"},\"Prompt39\":{\"value\":\" \",\"colorValue\":\"\"},\"Prompt40\":{\"value\":\"0\",\"colorValue\":\"\"},\"Prompt41\":{\"value\":\"N\",\"colorValue\":\"\"},\"Prompt42\":{\"value\":\" \",\"colorValue\":\"\"},\"Prompt43\":{\"value\":\"0\",\"colorValue\":\"\"},\"Prompt44\":{\"value\":\" \",\"colorValue\":\"\"},\"Prompt45\":{\"value\":\" \",\"colorValue\":\"\"},\"Prompt46\":{\"value\":\"0\",\"colorValue\":\"\"},\"Prompt47\":{\"value\":\"0\",\"colorValue\":\"\"},\"Prompt48\":{\"value\":\"0\",\"colorValue\":\"\"},\"Prompt49\":{\"value\":\" \",\"colorValue\":\"\"},\"Prompt50\":{\"value\":\"0\",\"colorValue\":\"\"},\"Prompt51\":{\"value\":\"0\",\"colorValue\":\"\"},\"Prompt52\":{\"value\":\" \",\"colorValue\":\"\"},\"Prompt53\":{\"value\":\"0\",\"colorValue\":\"\"},\"Prompt54\":{\"value\":\"0\",\"colorValue\":\"\"},\"Prompt55\":{\"value\":\"N\",\"colorValue\":\"\"},\"Prompt56\":{\"value\":\"N\",\"colorValue\":\"\"},\"Prompt57\":{\"value\":\" \",\"colorValue\":\"\"},\"Prompt58\":{\"value\":\" \",\"colorValue\":\"\"},\"Prompt59\":{\"value\":\" \",\"colorValue\":\"\"},\"Prompt60\":{\"value\":\" \",\"colorValue\":\"\"},\"Prompt61\":{\"value\":\"N\",\"colorValue\":\"\"},\"Prompt62\":{\"value\":\"0\",\"colorValue\":\"\"},\"Prompt63\":{\"value\":\"N\",\"colorValue\":\"\"},\"Prompt64\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt65\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt66\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt67\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt68\":{\"value\":\"N\",\"colorValue\":\"\"},\"Prompt69\":{\"value\":\"\",\"colorValue\":\"\"},\"Prompt77\":{\"value\":\"\",\"colorValue\":\"\"}}"

        };

        [TestMethod]
        public void Test_CreateLead()
        {
            LeadsController lc = new LeadsController();
            var res = lc.Post(l);
            var obj = res.GetType().GetProperty("Content").GetValue(res, null);
            int LeadId = (int)obj.GetType().GetProperty("LeadId").GetValue(obj, null);
            Assert.IsTrue(LeadId > 0);
        }


        [TestMethod]
        public void Test_CreateAccount()
        {
            AccountsController ac = new AccountsController();
            var res = ac.Post(aTP);
            var obj = res.GetType().GetProperty("Content").GetValue(res, null);
            int accid = (int)obj.GetType().GetProperty("accountId").GetValue(obj, null);
            AccountId = accid;

            Test_pGetAddressId();
            Test_pCreateMeasurement();
            Init();
            var bO = Test_pCreateOpportunity();
            if (!bO)
                Assert.IsTrue(false, "Create Opportunity Failed");

            Assert.IsTrue(accid > 0);
        }

        private void Test_pGetAddressId()
        {
            MeasurementController mc = new MeasurementController();
            var res = mc.InstallationAddresses(AccountId);
            var obj = res.GetType().GetProperty("Content").GetValue(res, null);
            var data = obj.GetType().GetProperty("data").GetValue(obj, null);

            var lstobj = data as List<object>;
            var datastr = JsonConvert.SerializeObject(lstobj[0]);
            var objd = JsonConvert.DeserializeObject<Addressinfo>(datastr);
            AddressId = objd.InstallAddressId;
            Assert.IsTrue(AddressId > 0);
        }

        private void Test_pCreateMeasurement()
        {
            mh.InstallationAddressId = AddressId;
            HttpContext.Current = HttpContextFactory.FakeHttpContext();
            var formType = HttpContext.Current.Request.Form.GetType();
            formType.GetMethod("MakeReadWrite", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).Invoke(HttpContext.Current.Request.Form, null);
            HttpContext.Current.Request.Form["dto"] = JsonConvert.SerializeObject(mh);
            HttpContext.Current.Request.Form["Files"] = "[]";
            formType.GetMethod("MakeReadOnly", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).Invoke(HttpContext.Current.Request.Form, null);

            MeasurementController mc = new MeasurementController();
            var res = mc.Post();
            var obj = (((JsonResult<List<MeasurementLineItemBB>>)res).Content).ToList();
            Assert.IsTrue(obj.Count > 0);
        }

        private bool Test_pCreateOpportunity()
        {
            if (AccountId == 0)
                Assert.IsTrue(false, "AccountId is 0");
            opp.AccountId = AccountId;
            opp.InstallationAddressId = AddressId;
            opp.BillingAddressId = AddressId;
            OpportunitiesController oppc = new OpportunitiesController();
            var res = oppc.Post(opp);
            var obj = res.GetType().GetProperty("Content").GetValue(res, null);
            int oppid = (int)obj.GetType().GetProperty("OpportunityId").GetValue(obj, null);
            OpportunityId = oppid;
            var bQ = Test_pCreateQuote();
            if (!bQ)
                Assert.IsTrue(false, "Create Quote Failed");

            return oppid > 0;
        }

        private bool Test_pCreateQuote()
        {
            if (OpportunityId == 0)
                Assert.IsTrue(false, "OpportunityId is 0");

            q.OpportunityId = OpportunityId;
            QuotesController qc = new QuotesController();
            var res = qc.SaveQuote(0, q);
            var obj = res.GetType().GetProperty("Content").GetValue(res, null);
            int quoteKey = (int)obj.GetType().GetProperty("quoteKey").GetValue(obj, null);
            Quotekey = quoteKey;

            var bQl = Test_pCreateQuoteLine();
            if (!bQl)
                Assert.IsTrue(false, "Create Quote Line Failed");

            q.QuoteKey = quoteKey;
            q.QuoteStatusId = 3;
            res = qc.SaveQuote(0, q);

            var ores = qc.ConvertQuoteToOrder(quoteKey);

            return Quotekey > 0;
        }

        private bool Test_pCreateQuoteLine()
        {
            if (Quotekey == 0)
                Assert.IsTrue(false, "Quotekey is 0");

            cpql.OpportunityId = OpportunityId;
            cpql.QuoteKey = Quotekey;

            QuotesController qc = new QuotesController();
            var res = qc.validateAndSaveConfigurator(cpql);
            var obj = res.GetType().GetProperty("Content").GetValue(res, null);
            int quoteKey = (int)obj.GetType().GetProperty("QuoteKey").GetValue(obj, null);
            return quoteKey > 0;
        }

        [TestMethod]
        public void Test_CreateMeasurement()
        {
            HttpContext.Current = HttpContextFactory.FakeHttpContext();
            var formType = HttpContext.Current.Request.Form.GetType();
            formType.GetMethod("MakeReadWrite", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).Invoke(HttpContext.Current.Request.Form, null);
            HttpContext.Current.Request.Form["dto"] = JsonConvert.SerializeObject(mh);
            HttpContext.Current.Request.Form["Files"] = "[]";
            formType.GetMethod("MakeReadOnly", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).Invoke(HttpContext.Current.Request.Form, null);

            MeasurementController mc = new MeasurementController();
            var res = mc.Post();
            var obj = (((JsonResult<List<MeasurementLineItemBB>>)res).Content).ToList();
            Assert.IsTrue(obj.Count > 0);
        }

        [TestMethod]
        public void Test_CreateOpportunity()
        {
            OpportunitiesController oppc = new OpportunitiesController();
            var oppres = oppc.Post(opp);
            var obj = oppres.GetType().GetProperty("Content").GetValue(oppres, null);
            int oppid = (int)obj.GetType().GetProperty("OpportunityId").GetValue(obj, null);
            OpportunityId = oppid;
            Assert.IsTrue(oppid > 0);
        }

        [TestMethod]
        public void Test_CreateQuote()
        {
            // Manually pass OpportunityId
            if (OpportunityId == 0)
                Assert.IsTrue(false, "OpportunityId is 0");

            q.OpportunityId = OpportunityId;
            QuotesController qc = new QuotesController();
            var oppres = qc.SaveQuote(0, q);
            var obj = oppres.GetType().GetProperty("Content").GetValue(oppres, null);
            int quoteKey = (int)obj.GetType().GetProperty("quoteKey").GetValue(obj, null);
            Quotekey = quoteKey;
            Assert.IsTrue(Quotekey > 0);
        }

        [TestMethod]
        public void Test_CreateQuoteLine()
        {
            // Manually pass OpportunityId and Quotekey
            if (OpportunityId == 0 || Quotekey == 0)
                Assert.IsTrue(false, "OpportunityId or Quotekey is 0");

            cpql.OpportunityId = OpportunityId;
            cpql.QuoteKey = Quotekey;

            QuotesController qc = new QuotesController();
            var res = qc.validateAndSaveConfigurator(cpql);
            var obj = res.GetType().GetProperty("Content").GetValue(res, null);
            int quoteKey = (int)obj.GetType().GetProperty("QuoteKey").GetValue(obj, null);
            Assert.IsTrue(quoteKey > 0);
        }

        [TestMethod]
        public void Test_GetAddressId()
        {
            AccountId = 1231;
            MeasurementController mc = new MeasurementController();
            var res = mc.InstallationAddresses(AccountId);
            var obj = res.GetType().GetProperty("Content").GetValue(res, null);
            var data = obj.GetType().GetProperty("data").GetValue(obj, null);

            var lstobj = data as List<object>;
            var datastr = JsonConvert.SerializeObject(lstobj[0]);
            var objd = JsonConvert.DeserializeObject<Addressinfo>(datastr);
            var AddressId = objd.InstallAddressId;
            Assert.IsTrue(AddressId > 0);
        }

        public class Addressinfo
        {
            public int AccountId { get; set; }
            public int InstallAddressId { get; set; }
            public bool IsValidated { get; set; }
            public string Name { get; set; }
            public string AccountName { get; set; }
        }

    }
}
