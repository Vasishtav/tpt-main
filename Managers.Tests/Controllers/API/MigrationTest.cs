﻿//using System;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using System.IO;
//using System.Linq;
//using System.Web;
//using System.Net;
//using System.ComponentModel;
//using System.Reflection;
//using System.Threading.Tasks;
//using System.Windows;
//using System.Security.Principal;
//using System.Web.SessionState;
//using HFC.CRM.Data;
//using System.Data.Entity;
//using HFC.CRM.Core.Common;
//using HFC.CRM.Core.Membership;
//using HFC.CRM.Managers;
//using HFC.CRM.Managers.Maestro;
//using Moq;

//namespace HFC.CRM.Web.Controllers.API.Tests
//{
//    [TestClass()]
//    public class MigrationTest
//    {

//        public User User { get; set; }
//        public Franchise Franchise { get; set; }
//        public MigrationManager MiG { get; set; }
//        public System.Collections.Generic.List<int> leadIdList;
//        public Lead TestLead { get; set; }

//        [TestInitialize()]
//        public void Init()
//        {

//            HttpContext.Current = HttpContextFactory.FakeHttpContext();
//            User = LocalMembership.GetUser("admin", "Person", "Roles", "OAuthUsers");
//            SessionManager.CurrentUser = User;

//        }


//        [TestMethod()]
//        public void MigrateFran()
//        {
//            Franchise fran;
//            int franid = 0;
//            string dbname = "";
//            MiG = new MigrationManager();            

//            using (var db = new CRMContextEx())
//            {
//                try
//                {
//                    MiG = InitMiG(db, MigrationInit(db, 2070, "hfcct_ctreston_codb"), "hfcct_ctreston_codb");
//                    MiG.MigrateFranchise();

//                    //MiG = InitMiG(db, MigrationInit(db, 2069, "hfcct_ct20061021_a_codb"), "hfcct_ct20061021_a_codb");
//                    //MiG.MigrateFranchise();
//                    //MiG = InitMiG(db, MigrationInit(db, 2071, "hfcct_ct20070115_codb"), "hfcct_ct20070115_codb");
//                    //MiG.MigrateFranchise();
//                    //MiG = InitMiG(db, MigrationInit(db, 2069, "hfcct_ct20070120_a_codb"), "hfcct_ct20070120_a_codb");
//                    //MiG.MigrateFranchise();
//                    //MiG = InitMiG(db, MigrationInit(db, 2082, "hfcct_TL20100902_codb"), "hfcct_TL20100902_codb");
//                    //MiG.MigrateFranchise();
//                    //MiG = InitMiG(db, MigrationInit(db, 2074, "hfcct_TL20100903_codb"), "hfcct_TL20100903_codb");
//                    //MiG.MigrateFranchise();
//                    //MiG = InitMiG(db, MigrationInit(db, 2085, "hfcct_TL20100904_codb"), "hfcct_TL20100904_codb");
//                    //MiG.MigrateFranchise();
//                    //MiG = InitMiG(db, MigrationInit(db, 2080, "hfcct_TL20100906_codb"), "hfcct_TL20100906_codb");
//                    //MiG.MigrateFranchise();
//                    //MiG = InitMiG(db, MigrationInit(db, 2083, "hfcct_TL20101004_codb"), "hfcct_TL20101004_codb");
//                    //MiG.MigrateFranchise();
//                    //MiG = InitMiG(db, MigrationInit(db, 2072, "hfcct_TL20101101_codb"), "hfcct_TL20101101_codb");
//                    //MiG.MigrateFranchise();
//                    //MiG = InitMiG(db, MigrationInit(db, 2075, "hfcct_TL20101108_codb"), "hfcct_TL20101108_codb");
//                    //MiG.MigrateFranchise();
//                    //MiG = InitMiG(db, MigrationInit(db, 2078, "hfcct_TL20101113_codb"), "hfcct_TL20101113_codb");
//                    //MiG.MigrateFranchise();
//                    //MiG = InitMiG(db, MigrationInit(db, 2077, "hfcct_TL20101202_codb"), "hfcct_TL20101202_codb");
//                    //MiG.MigrateFranchise();
//                    //MiG = InitMiG(db, MigrationInit(db, 2084, "hfcct_TL20101203_codb"), "hfcct_TL20101203_codb");
//                    //MiG.MigrateFranchise();
//                    //MiG = InitMiG(db, MigrationInit(db, 2086, "hfcct_TL20101204_codb"), "hfcct_TL20101204_codb");
//                    //MiG.MigrateFranchise();
//                    //MiG = InitMiG(db, MigrationInit(db, 2073, "hfcct_TL20101208_codb"), "hfcct_TL20101208_codb");
//                    //MiG.MigrateFranchise();
//                    //MiG = InitMiG(db, MigrationInit(db, 2081, "hfcct_TL20130101_codb"), "hfcct_TL20130101_codb");
//                    //MiG.MigrateFranchise();
//                    //MiG = InitMiG(db, MigrationInit(db, 2076, "hfcct_TL20130102_codb"), "hfcct_TL20130102_codb");
//                    //MiG.MigrateFranchise();
//                    //MiG = InitMiG(db, MigrationInit(db, 2079, "hfcct_TL20131106_codb"), "hfcct_TL20131106_codb");
//                    //MiG.MigrateFranchise();
//                    //MiG = InitMiG(db, MigrationInit(db, 2087, "hfcct_TL20131109_codb"), "hfcct_TL20131109_codb");
//                    //MiG.MigrateFranchise();
//                }
//                catch (Exception ex)
//                {
//                    var err = ex.InnerException;
//                }
//            }
//        }

//        private MigrationManager InitMiG(CRMContext db,Franchise fran, string dbname)
//        {
//            var MiG = new MigrationManager(fran, dbname);
//            MiG.TargetPath = @"C:\_Code\BBTL\src\HFC.CRM.Web\files\";
//            MiG.RepositoryPath = @"https://securea.servicetycoon.com/resourcesa/";
//            return MiG;        
//        }

//        private Data.Franchise MigrationInit(CRMContext db,int franid, string dbname)
//        {
//            Franchise fran;
//            fran = db.Franchises.Include(i => i.Address).FirstOrDefault(f => f.FranchiseId == franid);
//            SessionManager.CurrentFranchise = fran;
//            MiG = new MigrationManager(fran, dbname);
//            return fran;
//        }
//    }
//}

