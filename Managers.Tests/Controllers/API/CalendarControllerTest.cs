﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HFC.CRM.Web.Controllers.API;
using HFC.CRM.Data;
using System.Web;
using HFC.CRM.Data.Context;
using HFC.CRM.Core.Membership;
using HFC.CRM.Web.Controllers.API.Tests;
using HFC.CRM.Core.Common;
using HFC.CRM.Managers;
using Newtonsoft.Json;
using HFC.CRM.DTO;
using HFC.CRM.DTO.Address;
using System.Web.Http.Results;
using HFC.CRM.Web;
using System.Collections.Generic;
using System.Linq;
using HFC.CRM.Web.Models;
using System.Dynamic;
using System.Web.Routing;
using System.Web.Http;

namespace ManagerApp.Tests.Controllers.API
{
    [TestClass]
    public class CalendarControllerTest
    {
        public User user { get; set; }
        public Franchise fran { get; set; }
        public CRMContext DB { get; set; }
        public int CalendarId;

        [TestInitialize()]
        public void Init()
        {
            // Mock HttpContext (creates session & User properties)
            HttpContext.Current = HttpContextFactory.FakeHttpContext();
            ContextFactory.Current.Init(new WebContextProvider());
            DB = ContextFactory.Current.Context;
            // Set current FE & User // BB test US
            user = LocalMembership.GetUser("19001001", "Person", "Roles", "OAuthUsers");
            SessionManager.CurrentUser = user;

            FranchiseManager fm = new FranchiseManager(user);
            fran = fm.GetFEinfo(2);
            SessionManager.CurrentFranchise = fran;

        }

        CalendarModel cm = new CalendarModel()
        {
            id = 0,
            AccountId = null,
            AccountName = "",
            AccountPhone = null,
            AddRecurrence = false,
            AdditionalPeople = new List<string>() { "synctest8@budgetblinds.com" },
            AppointmentTypeColor = null,
            AppointmentTypeName = "Sales/Design Appointment",
            AptTypeEnum = AppointmentTypeEnumTP.Appointment,
            AssignedName = "synctest8 synctest8",
            AssignedPersonId = 109,
            Attendees = null,
            AttendeesTP = null,
            CalName = "Rajkumar R / kbs",
            CaseId = null,
            EnableBorders = false,
            EventRecurring = null,
            EventType = EventTypeEnum.Single,
            IsCancelled = null,
            IsDeletable = false,
            IsPrivate = false,
            IsPrivateAccess = false,
            JobId = null,
            JobNumber = null,
            LastUpdatedByPersonId = null,
            LeadId = 1044,
            LeadNumber = null,
            Location = "IL 32104" + DateTime.UtcNow.ToString("MM/dd/yyy HHmmss"),
            Message = "Email: rrajkumar@kolorblue.com",
            OpportunityId = null,
            OpportunityName = null,
            OrderId = null,
            OrganizerEmail = "rrajkumar@mailinator.com",
            OrganizerName = "BB Test",
            OrganizerPersonId = 2,
            OriginalStartDate = null,
            PersonId = 109,
            PhoneNumber = "",
            QuickDispositionValue = null,
            RecurringEventId = null,
            ReminderMethods = null,
            ReminderMinute = 15,
            RevisionSequence = 0,
            UserRoleColor = null,
            VendorCaseId = null,
            allDay = false,
            className = null,
            durationEditable = false,
            editable = false,
            emailType = null,
            start = DateTime.Now.Date.AddHours(10),
            end = DateTime.Now.Date.AddHours(11),
            startEditable = false,
            title = "Rajkumar R / kbs - Test" + DateTime.UtcNow.ToString("MM/dd/yyy HHmmss")
        };

        CalendarModel cmRec1 = new CalendarModel()
        {
            EventType = EventTypeEnum.Series,
            RecurringEventId = null,
            AptTypeEnum = AppointmentTypeEnumTP.Appointment,
            AppointmentTypeName = null,
            CalName = null,
            EventRecurring = new EventRecurring
            {
                RecurringEventId = 0,
                EndsAfterXOccurrences = null,
                RecursEvery = 1,
                LastUpdatedUtc = null,
                LastUpdatedByPersonId = null,
                EndsOn = DateTime.Now.Date.AddDays(10),
                DayOfWeekEnum = DayOfWeekEnum.Sunday,
                MonthEnum = MonthEnum.January,
                DayOfMonth = 1,
                PatternEnum = RecurringPatternEnum.Daily,
                DayOfWeekIndex = DayOfWeekIndexEnum.First,
                StartDate = DateTime.Now.Date.AddHours(10),
                EndDate = DateTime.Now.Date.AddHours(11),
                Creator = null,
                LastUpdater = null
            },
            OriginalStartDate = null,
            EnableBorders = false,
            QuickDispositionValue = null,
            start = DateTime.Now.Date.AddHours(10),
            end = DateTime.Now.Date.AddHours(11),
            id = 0,
            startEditable = false,
            durationEditable = false,
            className = null,
            Attendees = null,
            AttendeesTP = null,
            title = "Test - " + DateTime.UtcNow.ToString("MM/dd/yyy HHmmss"),
            AppointmentTypeColor = null,
            UserRoleColor = null,
            editable = false,
            allDay = false,
            Location = "88888" + DateTime.UtcNow.ToString("MM/dd/yyy HHmmss"),
            Message = "Phone: 4444444444\nEmail: charu.benjwal@gohfc.com" + DateTime.UtcNow.ToString("MM/dd/yyy HHmmss"),
            LeadId = 1044,
            AccountId = null,
            AccountName = null,
            AccountPhone = null,
            OpportunityId = null,
            OpportunityName = null,
            OrderId = null,
            CaseId = null,
            VendorCaseId = null,
            IsPrivateAccess = false,
            LeadNumber = null,
            JobId = null,
            JobNumber = null,
            IsPrivate = false,
            LastUpdatedByPersonId = null,
            RevisionSequence = 0,
            OrganizerPersonId = null,
            OrganizerEmail = "rrajkumar@mailinator.com",
            OrganizerName = "BB Test",
            AssignedPersonId = 109,
            AssignedName = null,
            AdditionalPeople = new List<string>() { "synctest8@budgetblinds.com" },
            ReminderMinute = 15,
            IsDeletable = false,
            IsCancelled = null,
            ReminderMethods = null,
            PhoneNumber = null,
            emailType = null,
            PersonId = 109,
            RRRule = null,
            AddRecurrence = true,
            RecurrenceException = null
        };


        /// <summary>
        /// Event add
        /// </summary>
        [TestMethod]
        public void Test_CalendarPost()
        {
            CalendarController cc = new CalendarController();
            var res = cc.Post(cm);
            var obj = res.GetType().GetProperty("Content").GetValue(res, null);
            int calid = (int)obj.GetType().GetProperty("Id").GetValue(obj, null);
            Assert.IsTrue(calid > 0);
        }

        [TestMethod]
        public void Test_CalendarRecPost()
        {
            CalendarController cc = new CalendarController();
            var res = cc.Post(cmRec1);
            var obj = res.GetType().GetProperty("Content").GetValue(res, null);
            int calid = (int)obj.GetType().GetProperty("Id").GetValue(obj, null);
            Assert.IsTrue(calid > 0);
        }

        /// <summary>
        /// Get List of data based on Date range
        /// </summary>
        [TestMethod]
        public void Test_ReadSelection()
        {
            DateTime date = DateTime.UtcNow;
            var firstDayOfMonth = new DateTime(date.Year, 1, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
            CalTasksController ct = new CalTasksController();
            dynamic res = ct.ReadSelection("38,2", firstDayOfMonth, lastDayOfMonth);
            var obj = (((JsonResult<IQueryable<TaskViewModel>>)res).Content).ToList();
            var objCount = obj.Count;
            Assert.IsTrue(objCount > 0);
        }

        [TestMethod]
        public void Test_CalendarGet()
        {
            if (CalendarId == 0)
                CalendarId = 1;
            CalendarFilter cf = new CalendarFilter()
            {
                CalendarEdit = true,
                CalendarId = CalendarId,
                includeLookup = true
            };
            List<CalendarModel> events = null, recurMasters = null;
            var res = cf.GetCalendar(out events, out recurMasters);
            Assert.IsTrue(events != null && events.Count > 0);
        }

    }
}
