﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HFC.CRM.Web.Controllers.API;
using HFC.CRM.Data;
using System.Web;
using HFC.CRM.Data.Context;
using HFC.CRM.Core.Membership;
using HFC.CRM.Web.Controllers.API.Tests;
using HFC.CRM.Core.Common;
using HFC.CRM.Managers;
using Newtonsoft.Json;
using HFC.CRM.DTO;
using HFC.CRM.DTO.Address;
using System.Web.Http.Results;

namespace ManagerApp.Tests.Controllers.API
{
    [TestClass]
    public class AddressValidationControllerTest
    {
        public User user { get; set; }
        public Franchise fran { get; set; }
        public CRMContext DB { get; set; }

        [TestInitialize()]
        public void Init()
        {
            // Mock HttpContext (creates session & User properties)
            HttpContext.Current = HttpContextFactory.FakeHttpContext();
            ContextFactory.Current.Init(new WebContextProvider());
            DB = ContextFactory.Current.Context;
            // Set current FE & User // BB test US
            user = LocalMembership.GetUser("19001001", "Person", "Roles", "OAuthUsers");
            SessionManager.CurrentUser = user;

            FranchiseManager fm = new FranchiseManager(user);
            fran = fm.GetFEinfo(2);
            SessionManager.CurrentFranchise = fran;

        }

        [TestMethod]
        public void Test_SmartyStreetSuggession()
        {

            AddressValidationController av = new AddressValidationController();
            var res = av.SmartyStreetSuggession("5605 N. MacArthur", "Irving, TX");
            var obj = ((JsonResult<SmartyStreetResponse>)res).Content;
            var objsug = obj.suggestions.Length;
            Assert.IsTrue(objsug > 0);
        }

        [TestMethod]
        public void Test_ValidateCustomerAddress()
        {
            ValidateAddress req = new ValidateAddress()
            {
                address1 = "3756 PARK PL",
                address2 = "",
                City = "Addison",
                StateOrProvinceCode = "TX",
                PostalCode = "75001",
                CountryCode = "US"
            };

            AddressValidationController av = new AddressValidationController();
            var res = av.ValidateCustomerAddress(req);
            var obj = ((JsonResult<ValidateAddressResponse>)res).Content;
            var objsug = obj.AddressResults.Count;
            Assert.IsTrue(objsug > 0);
        }
    }
}
