﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using HFC.CRM.Data;
//using HFC.CRM.Core.Membership;
//using HFC.CRM.Core.Common;
//using HFC.CRM.Managers;
//using System.Web;
//using HFC.CRM.Web.Controllers.API.Tests;
//using HFC.CRM.Data.Context;

//namespace ManagerApp.Tests._2._0.Managers
//{
//    [TestClass]
//    public class SearchManagerTest
//    {
//        public User user { get; set; }
//        public Franchise fran { get; set; }
//        public SearchManager searchMng { get; set; }

//        [TestInitialize()]
//        public void Init()
//        {
//            try
//            {
//                HttpContext.Current = HttpContextFactory.FakeHttpContext();
//                ContextFactory.Current.Init(new WebContextProvider());
//                //// Set current FE & User
//                user = LocalMembership.GetUser("sa@bb.com", "Person", "Roles", "OAuthUsers");
//                fran = new Franchise()
//                {
//                    FranchiseId = 2384,
//                    FranchiseGuid = new Guid("A9629B38-13E6-44D0-91AD-B7B124B3E222"),
//                    Name = "Budget Blinds of Stafford",
//                    Code = "20120310"
//                };

//                SessionManager.CurrentUser = user;
//                SessionManager.CurrentFranchise = fran;
//                searchMng = new SearchManager(user, fran);
//            }
//            catch (Exception ex)
//            {
              
//            }
         
//        }

//        #region Jobs
//        [TestMethod]
//        public void GetNewJobs()
//        {
           
//                var jobs = searchMng.GetJobs(new List<int>() {27}, null, null, null, null, null, null,
//                    SearchFilterEnum.Auto_Detect, null,
//                    "createdonutc", OrderByEnum.Desc, 1, 25);
//                Assert.AreEqual(jobs.Result.Any(), true);
            
//        }
        
//        [TestMethod]
//        public void GetSalesJobs()
//        {
//            var jobs = searchMng.GetJobs(null, new List<int>() { 992817 }, null, null, null, null, null, SearchFilterEnum.Auto_Detect, null,
//                "createdonutc", OrderByEnum.Desc, 1, 25);
//            Assert.AreEqual(jobs.Result.Any(), true);
//        }

//        [TestMethod]
//        public void GetAllJobs()
//        {
//           var jobs = searchMng.GetJobs(null, null, null, null, null, null, null, SearchFilterEnum.Auto_Detect, null,
//                "createdonutc", OrderByEnum.Desc, 1, 25);
//            Assert.AreEqual(jobs.Result.Any(), true);
//        }

//        [TestMethod]
//        public void GetAdressJobs()
//        {
//            var jobs = searchMng.GetJobs(null, null, null, null, null, null, null, SearchFilterEnum.Address, "22026",
//                "createdonutc", OrderByEnum.Desc, 1, 25);
//            Assert.AreEqual(jobs.Result.Any(), true);
//        }
//          [TestMethod]
//        public void GetCustomer_NameJobs()
//        {
//            var jobs = searchMng.GetJobs(null, null, null, null, null, null, null, SearchFilterEnum.Customer_Name, "Sandra",
//                "createdonutc", OrderByEnum.Desc, 1, 25);
//            Assert.AreEqual(jobs.Result.Any(), true);
//        }
//          [TestMethod]
//        public void GetEmailJobs()
//        {
//            var jobs = searchMng.GetJobs(null, null, null, null, null, null, null, SearchFilterEnum.Email, "Sara",
//                "createdonutc", OrderByEnum.Desc, 1, 25);
//            Assert.AreEqual(jobs.Result.Any(), true);
//        }
//          [TestMethod]
//        public void GetPhone_NumberJobs()
//        {
//            var jobs = searchMng.GetJobs(null, null, null, null, null, null, null, SearchFilterEnum.Phone_Number, "529",
//                "createdonutc", OrderByEnum.Desc, 1, 25);
//            Assert.AreEqual(jobs.Result.Any(), true);
//        }
//          [TestMethod]
//        public void GetNumberJobs()
//        {
//            var jobs = searchMng.GetJobs(null, null, null, null, null, null, null, SearchFilterEnum.Number, "1618",
//                "createdonutc", OrderByEnum.Desc, 1, 25);
            
//            Assert.AreEqual(jobs.Result.Any(), true);
//        }
//          [TestMethod]
//        public void GetJobsNonExistsJobsByNumber()
//        {
//            var jobs = searchMng.GetJobs(null, null, null, null, null, null, null, SearchFilterEnum.Number, "-100",
//                "createdonutc", OrderByEnum.Desc, 1, 25);
              

//            Assert.AreEqual(jobs.Result.Any(), false);
//        }
//          [TestMethod]
//        public void GetJobsNonExistsJobsByAutoSearch()
//        {
//            var jobs = searchMng.GetJobs(null, null, null, null, null, null, null, SearchFilterEnum.Auto_Detect, "-1azsw00454",
//                "createdonutc", OrderByEnum.Desc, 1, 25);
//            Assert.AreEqual(jobs.Result.Any(), false);
//        }

//        #endregion

//        #region Leads

//        [TestMethod]
//        public void GetNewLeads()
//        {
//            var leads = searchMng.GetLeads(new List<int>() {1 }, null, null, null, null, null, null, SearchFilterEnum.Auto_Detect, null,
//                "createdonutc", OrderByEnum.Desc, 1, 25);
//            Assert.AreEqual(leads.Result.Any(), true);
//        }

//        [TestMethod]
//        public void GetAdressLeads()
//        {
//            var leads = searchMng.GetLeads(null, null, null, null, null, null, null, SearchFilterEnum.Address, "22026",
//                "createdonutc", OrderByEnum.Desc, 1, 25);
//            Assert.AreEqual(leads.Result.Any(), true);
//        }
//          [TestMethod]
//        public void GetCustomer_NameLeads()
//        {
//            var leads = searchMng.GetLeads(null, null, null, null, null, null, null, SearchFilterEnum.Customer_Name, "Sandra",
//                "createdonutc", OrderByEnum.Desc, 1, 25);
//            Assert.AreEqual(leads.Result.Any(), true);
//        }
//          [TestMethod]
//        public void GetEmailLeads()
//        {
//            var leads = searchMng.GetLeads(null, null, null, null, null, null, null, SearchFilterEnum.Email, "Sara",
//                "createdonutc", OrderByEnum.Desc, 1, 25);
//            Assert.AreEqual(leads.Result.Any(), true);
//        }
//          [TestMethod]
//        public void GetPhone_NumberLeads()
//        {
//            var leads = searchMng.GetLeads(null, null, null, null, null, null, null, SearchFilterEnum.Phone_Number, "529",
//                "createdonutc", OrderByEnum.Desc, 1, 25);
//            Assert.AreEqual(leads.Result.Any(), true);
//        }
//          [TestMethod]
//        public void GetNumberLeads()
//        {
//            var leads = searchMng.GetLeads(null, null, null, null, null, null, null, SearchFilterEnum.Number, "20",
//                "createdonutc", OrderByEnum.Desc, 1, 25);
//            Assert.AreEqual(leads.Result.Any(), true);
//        }
//          [TestMethod]
//        public void GetJobsNonExistsLeadsByNumber()
//        {
//            var leads = searchMng.GetLeads(null, null, null, null, null, null, null, SearchFilterEnum.Number, "-100",
//                "createdonutc", OrderByEnum.Desc, 1, 25);
//            Assert.AreEqual(leads.Result.Any(), false);
//        }
//          [TestMethod]
//        public void GetJobsNonExistsLeadsByAutoSearch()
//        {
//            var leads = searchMng.GetLeads(null, null, null, null, null, null, null, SearchFilterEnum.Auto_Detect, "-1azsw00454",
//                "createdonutc", OrderByEnum.Desc, 1, 25);
//            Assert.AreEqual(leads.Result.Any(), false);
//        }

//        #endregion

//        #region Invoices

//        [TestMethod]
//        public void GetOpenInvoices()
//        {
//            var invoices = searchMng.GetInvoices(null, null, null, null, new List<int>() { (int)InvoiceStatusEnum.Open }, null, SearchFilterEnum.Auto_Detect, null, null, 1, 25, "createdon", OrderByEnum.Desc);
//            Assert.AreEqual(invoices.Result.Any(), true);
//        }

//        [TestMethod]
//        public void GetCustomer_NameInvoices()
//        {
//            var invoices = searchMng.GetInvoices(null, null, null, null, null, "Sher", SearchFilterEnum.Customer_Name, null, null, 1, 25, "createdon", OrderByEnum.Desc);
//            Assert.AreEqual(invoices.Result.Any(), true);
//        }
        
//        [TestMethod]
//        public void GetEmailInvoices()
//        {
//            var invoices = searchMng.GetInvoices(null, null, null, null, null, "gmail.com", SearchFilterEnum.Email, null, null, 1, 25, "createdon", OrderByEnum.Desc);
//            Assert.AreEqual(invoices.Result.Any(), true);
//        }
        
//        [TestMethod]
//        public void GetPhone_NumberInvoices()
//        {
//            var invoices = searchMng.GetInvoices(null, null, null, null, null, "862", SearchFilterEnum.Phone_Number, null, null, 1, 25, "createdon", OrderByEnum.Desc);
//            Assert.AreEqual(invoices.Result.Any(), true);
//        }

//        [TestMethod]
//        public void GetJobsNonExistsInvoicesByAutoSearch()
//        {
//            var invoices = searchMng.GetInvoices(null, null, null, null, null, "-1azsw00454", SearchFilterEnum.Auto_Detect, null, null, 1, 25, "createdon", OrderByEnum.Desc);
//            Assert.AreEqual(invoices.Result.Any(), false);
//        }


//        #endregion
//    }
//}
