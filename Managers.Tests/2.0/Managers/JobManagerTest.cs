﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HFC.CRM.Data;
using HFC.CRM.Core.Membership;
using HFC.CRM.Core.Common;
using HFC.CRM.Managers;
using System.Web;
using HFC.CRM.Web.Controllers.API.Tests;
using HFC.CRM.Data.Context;

namespace ManagerApp.Tests._2._0.Managers
{
    [TestClass]
    public class JobManagerTest
    {
        public User user { get; set; }
        public Franchise fran { get; set; }

        [TestInitialize()]
        public void Init()
        {
            HttpContext.Current = HttpContextFactory.FakeHttpContext();
            ContextFactory.Current.Init(new WebContextProvider());
            //// Set current FE & User
            user = LocalMembership.GetUser("sa@bb.com", "Person", "Roles", "OAuthUsers");
            fran = new Franchise()
            {
                FranchiseId = 33,
                FranchiseGuid = new Guid("83D9B16E-952B-48F0-B92E-9704BB11A482"),
                Name = "QA Test",
                Code = "123a"
            };

            SessionManager.CurrentUser = user;
            SessionManager.CurrentFranchise = fran;
        }

        [TestMethod]
        public void GetLeadInfo()
        {
           var jobMgr = new JobsManager(user, fran);
           var detailsModel = jobMgr.GetJobDetails(36650);
           Assert.AreEqual(36650, detailsModel.JobId);
           var shortModel = jobMgr.GetJobsShortInfo(36650);
           Assert.AreEqual(shortModel.Count > 0, true);
        }
    }
}
