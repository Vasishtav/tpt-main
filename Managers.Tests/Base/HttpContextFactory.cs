﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using System.IO;
using System.Linq;
using System.Web;
using System.Reflection;
using System.Security.Principal;
using System.Web.SessionState;
using HFC.CRM.Data;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Membership;
using HFC.CRM.Managers;
using Moq;

namespace HFC.CRM.Web.Controllers.API.Tests
{
    public class HttpContextFactory
    {
        private static HttpContextBase _context;
        public static HttpContextBase Current
        {
            get
            {
                if (_context != null)
                    return _context;

                if (HttpContext.Current == null)
                    throw new InvalidOperationException("HttpContext not available");

                return new HttpContextWrapper(HttpContext.Current);
            }
        }
        public static void SetCurrentContext(HttpContextBase context)
        {
            _context = context;
        }
        public static HttpContext FakeHttpContext()
        {
            var httpRequest = new HttpRequest("", "http://stackoverflow/", "");
            var stringWriter = new StringWriter();
            var httpResponse = new HttpResponse(stringWriter);
            var httpContext = new HttpContext(httpRequest, httpResponse);
            //var User = new Mock<IPrincipal>();
            //var identity = new Mock<IIdentity>();

            var sessionContainer = new HttpSessionStateContainer("id",
                new SessionStateItemCollection(),
                new HttpStaticObjectsCollection(), 10, true,
                HttpCookieMode.AutoDetect,
                SessionStateMode.InProc, false);

            httpContext.Items["AspSession"] = typeof(HttpSessionState).GetConstructor(
                BindingFlags.NonPublic | BindingFlags.Instance,
                null, CallingConventions.Standard,
                new[] { typeof(HttpSessionStateContainer) },
                null).Invoke(new object[] { sessionContainer });

            var identity = new Mock<IIdentity>();
            var user = new Mock<IPrincipal>();
            user.Setup(ctx => ctx.Identity).Returns(identity.Object);
            identity.Setup(id => id.IsAuthenticated).Returns(true);
            identity.Setup(id => id.Name).Returns("admin");
            httpContext.User = user.Object;

            return httpContext;
        }
    }
}
