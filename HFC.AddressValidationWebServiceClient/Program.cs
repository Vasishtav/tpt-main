// This code was built using Visual Studio 2005
using System;
using System.Web.Services.Protocols;
using AddressValidationWebServiceClient.AddressValidationServiceWebReference;
using System.Configuration;
using System.Collections.Generic;
using System.Text;

namespace AddressValidationWebServiceClient
{
    public class FEDExService
    {
        public static FEDExResponse AddressValidation(FEDExRequest req)
        {
            FEDExResponse res = new FEDExResponse();
            res.Status = "true";
            res.Message = "";
            AddressValidationRequest request = CreateAddressValidationRequest(req);
            AddressValidationService service = new AddressValidationService();
            service.Url = ConfigurationManager.AppSettings["FEDExurl"].ToString() + "/addressvalidation";
            try
            {
                AddressValidationReply reply = service.addressValidation(request);
                if (reply.HighestSeverity == NotificationSeverityType.SUCCESS || reply.HighestSeverity == NotificationSeverityType.NOTE || reply.HighestSeverity == NotificationSeverityType.WARNING)
                {
                    //res.Message = "";// reply.HighestSeverity.ToString();
                    res.AddressResults = AddressValidationResponse(reply);
                    if (res.AddressResults != null && res.AddressResults.Count > 0)
                    {
                        res.AddressResults[0].address2 = req.address2;
                        if (res.AddressResults[0].State != OperationalAddressStateType.STANDARDIZED.ToString())
                        {
                            res.Status = "false";
                        }
                    }
                }
                else
                {
                    res.Status = "error";
                    res.Message = reply != null && reply.Notifications != null && reply.Notifications.Length > 0 ? reply.Notifications[0].Message : "";
                    //StringBuilder sb = new StringBuilder();
                    //foreach (Notification notification in reply.Notifications)
                    //    sb.Append(notification.Message);
                }
            }
            catch (SoapException e)
            {
                res.Status = "error";
                res.Message = e.Detail.InnerText;
            }
            catch (Exception e)
            {
                res.Status = "error";
                res.Message = e.Message;
            }
            return res;
        }

        private static AddressValidationRequest CreateAddressValidationRequest(FEDExRequest req)
        {
            AddressValidationRequest request = new AddressValidationRequest();
            request.WebAuthenticationDetail = new WebAuthenticationDetail();
            request.WebAuthenticationDetail.UserCredential = new WebAuthenticationCredential();
            request.WebAuthenticationDetail.UserCredential.Key = ConfigurationManager.AppSettings["FEDExKey"].ToString();
            request.WebAuthenticationDetail.UserCredential.Password = ConfigurationManager.AppSettings["FEDExPassword"].ToString();
            request.WebAuthenticationDetail.ParentCredential = new WebAuthenticationCredential();
            request.WebAuthenticationDetail.ParentCredential.Key = ConfigurationManager.AppSettings["FEDExKey"].ToString();
            request.WebAuthenticationDetail.ParentCredential.Password = ConfigurationManager.AppSettings["FEDExPassword"].ToString();

            request.ClientDetail = new ClientDetail();
            request.ClientDetail.AccountNumber = ConfigurationManager.AppSettings["FEDExAccountNumber"].ToString();
            request.ClientDetail.MeterNumber = ConfigurationManager.AppSettings["FEDExMeterNumber"].ToString();

            request.TransactionDetail = new TransactionDetail();
            request.TransactionDetail.CustomerTransactionId = Guid.NewGuid().ToString();
            request.Version = new VersionId();
            request.InEffectAsOfTimestamp = DateTime.Now;
            request.InEffectAsOfTimestampSpecified = true;
            SetAddress(request, req);
            return request;
        }

        private static void SetAddress(AddressValidationRequest request, FEDExRequest req)
        {
            request.AddressesToValidate = new AddressToValidate[3];
            request.AddressesToValidate[0] = new AddressToValidate();
            request.AddressesToValidate[0].ClientReferenceId = Guid.NewGuid().ToString();
            request.AddressesToValidate[0].Address = new Address();
            request.AddressesToValidate[0].Address.StreetLines = new String[1] { req.address1 };
            if (req.CountryCode == "US")
                request.AddressesToValidate[0].Address.PostalCode = "";
            else
                request.AddressesToValidate[0].Address.PostalCode = req.PostalCode;
            request.AddressesToValidate[0].Address.City = req.City;
            request.AddressesToValidate[0].Address.StateOrProvinceCode = req.StateOrProvinceCode;
            request.AddressesToValidate[0].Address.CountryCode = req.CountryCode;
        }

        private static List<FEDExAddressResults> AddressValidationResponse(AddressValidationReply reply)
        {
            List<FEDExAddressResults> AddressResults = new List<FEDExAddressResults>();

            AddressValidationResult result = reply.AddressResults[0];

            //foreach (AddressValidationResult result in reply.AddressResults)
            //{
            FEDExAddressResults ar = new FEDExAddressResults();
            ar.Classification = result.ClientReferenceId;
            ar.ClassificationSpecified = result.ClassificationSpecified;
            ar.StateSpecified = result.StateSpecified;

            if (result.ClassificationSpecified) { ar.Classification = result.Classification.ToString(); }

            if (result.StateSpecified) { ar.State = result.State.ToString(); }

            if (result.State != OperationalAddressStateType.STANDARDIZED)
            {
                AddressResults.Add(ar);
                return AddressResults;
            }

            ar.address1 = "";
            ar.address2 = "";
            Address address = result.EffectiveAddress;
            int i = 0;
            if (address.StreetLines != null)
            {
                foreach (String street in address.StreetLines)
                {
                    if (i == 0)
                    {
                        ar.address1 = street;
                    }
                    if (i > 0)
                    {
                        ar.address2 += street;
                    }
                    i++;
                }
            }
            ar.City = address.City;
            ar.StateOrProvinceCode = address.StateOrProvinceCode;
            ar.PostalCode = address.PostalCode;
            ar.CountryCode = address.CountryCode;
            AddressResults.Add(ar);
            // }
            return AddressResults;
        }

    }

    public class FEDExRequest
    {
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string City { get; set; }
        public string StateOrProvinceCode { get; set; }
        public string PostalCode { get; set; }
        public string CountryCode { get; set; }
    }

    public class FEDExResponse
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public List<FEDExAddressResults> AddressResults { get; set; }
    }

    public class FEDExAddressResults
    {
        public string ClientReferenceId { get; set; }
        public bool ClassificationSpecified { get; set; }
        public string Classification { get; set; }
        public bool StateSpecified { get; set; }
        public string State { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string City { get; set; }
        public string StateOrProvinceCode { get; set; }
        public string PostalCode { get; set; }
        public string CountryCode { get; set; }
    }

}