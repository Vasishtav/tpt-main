﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    
    public class VendorCaseDetailInfo : BaseEntity
    {
        public int VendorCaseId { get; set; }
        public int CaseId { get; set; }
        public string VendorCaseNumber { get; set; }
        public int VendorId{ get; set; }
        public string IssueDescription { get; set; }
        public string Description { get; set; }
        public string Typevalue { get; set; }
        public string CaseReason { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string VendorName { get; set; }
        public bool ExpediteReq { set; get; }
        public bool ExpediteApproved { set; get; }
        public bool ReqTripCharge { set; get; }
        public int VPO{ get; set; }
        public String QuoteLineNumber { get; set; }
        public int? Status { get; set; }
        public int? TripChargeApproved{ get; set; }
        public int? Resolution { get; set; }
        public int CaseLineId { get; set; }
        public int VendorCaseDetailId { get; set; }
        public string ResolutionName { get; set; }
        public string StatusName { get; set; }
        public string TripChargeName { get; set; }
        public decimal? Amount { get; set; }
    }
}
