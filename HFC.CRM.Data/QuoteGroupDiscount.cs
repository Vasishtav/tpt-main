﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.QuoteGroupDiscount")]
    public class QuoteGroupDiscount
    {
        [Key]
        public int QuoteGroupDiscountId { get; set; }
        public int QuoteKey { get; set; }
        public int ProductTypeId { get; set; }
        public int ProductGroupId { get; set; }
        public string ProductGroupDesc { get; set; }
        public decimal? Discount { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public int? LastUpdatedBy { get; set; }
    }
}