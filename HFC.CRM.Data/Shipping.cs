﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    public class Shipping
    {
        public int Id { get; set; }
       
        public AddressTP ShipAddress { get; set; }
        public List<ShipTo> Shiptoo { get; set; }
        public List<ShipToLocation> ShipToLocations { get; set; } 
    }

    //just for testing,
    public class ShipDetails
    {
        public int id { get; set; }
        public string TerritoryID { get; set; }
        public string ShipToLocation { get; set; }
        public string ShipToName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string AccountNo { get; set; }
    }
}
