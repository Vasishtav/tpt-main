﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
   public class MeasurementLineItemBase
    {
        public string Width { get; set; }
        public string FranctionalValueWidth { get; set; }
        public string Height { get; set; }
        public string FranctionalValueHeight { get; set; }
        public string Comments { get; set; }   
             
    }
}
