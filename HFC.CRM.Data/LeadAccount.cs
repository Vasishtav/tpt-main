﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("Crm.Account")]
    public partial class LeadAccount
    {
        [Key]
        public int AccountId { get; set; }
        public Nullable<byte> AccountStatusId { get; set; }
        public int LeadId { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public Nullable<int> UpdatedBy { get; set; }

        public virtual Lead Lead { get; set; }
        public virtual Type_AccountStatus Type_AccountStatus { get; set; }
        public int AccountTypeId { get; set; }

        public bool? IsPerpetual { get; set; }
    }
}

               
     