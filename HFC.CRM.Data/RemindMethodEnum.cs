//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HFC.CRM.Data
{
    using System;
    
    [Flags]
    public enum RemindMethodEnum : byte
    {
        Popup = 1,
        Email = 2,
        SMS = 4
    }
}
