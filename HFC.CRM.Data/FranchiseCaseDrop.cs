﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    public class FranchiseCaseDropModel
    {
      
        public int? VPO_PICPO_PODId { get; set; }
        public string MPO_MasterPONum_POId { get; set; }
        public int? SalesOrderId { get; set; }
        public int PurchaseOrdersDetailId { get; set; }
        public int PurchaseOrderId { get; set; }
        public int OrderId { get; set; }
        public string AccountName { get; set; }
        public string CellPhone { get; set; }
        public string PrimaryEmail { get; set; }
        public string SideMark { get; set; }
        public int QuoteLineId { get; set; }
        public int QuoteLineNumber { get; set; }
        public int noOfLines { get; set; }
        public int FranchiseId { get; set; }
        public string FranchiseName { get; set; }
        public List<int> Orderlist { get; set; }
    }
}
