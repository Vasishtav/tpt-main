﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    /// <summary>
    /// Class ImageUtil.
    /// </summary>
    public class ImageUtil
    {
        /// <summary>
        /// Saves a thumbnail of source image in jpeg format
        /// </summary>
        /// <param name="srcStream">The source stream.</param>
        /// <param name="destFilePath">The dest file path.</param>
        /// <param name="quality">The quality.</param>
        /// <param name="tnWidth">Thumbnail width</param>
        /// <param name="tnHeight">Thumbnail height</param>
        /// <returns>Source bitmap image</returns>
        public static Bitmap SaveThumbnail(Stream srcStream, string destFilePath, long quality = 70, int tnWidth = 120, int tnHeight = 120)
        {
            var img = new Bitmap(srcStream);
            var thmImg = Resize(img, tnWidth, tnHeight);
            Save(thmImg, destFilePath, quality);
            return img;
        }

        /// <summary>
        /// Saves as JPG
        /// </summary>
        /// <param name="imgToSave">The img to save.</param>
        /// <param name="destFilePath">The dest file path.</param>
        /// <param name="quality">The quality.</param>
        public static void Save(Bitmap imgToSave, string destFilePath, long quality = 70)
        {
            // Encoder parameter for image quality
            EncoderParameter qualityParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);

            // Jpeg image codec
            ImageCodecInfo jpegCodec = GetEncoderInfo("image/jpeg");

            if (jpegCodec == null)
                throw new NullReferenceException("Jpeg Codec missing");

            EncoderParameters encoderParams = new EncoderParameters(1);
            encoderParams.Param[0] = qualityParam;

            imgToSave.Save(destFilePath, jpegCodec, encoderParams);
        }

        /// <summary>
        /// Resizes the specified source file path.
        /// </summary>
        /// <param name="srcFilePath">The source file path.</param>
        /// <param name="newWidth">The new width.</param>
        /// <param name="newHeight">The new height.</param>
        /// <param name="fixedSize">Size of the fixed.</param>
        /// <returns>System.Drawing.Bitmap.</returns>
        public static Bitmap Resize(string srcFilePath, int newWidth, int newHeight, bool fixedSize = true)
        {            
            return Resize(new Bitmap(srcFilePath), newWidth, newHeight, fixedSize);
        }

        /// <summary>
        /// Resizes the specified source stream.
        /// </summary>
        /// <param name="srcStream">The source stream.</param>
        /// <param name="newWidth">The new width.</param>
        /// <param name="newHeight">The new height.</param>
        /// <param name="fixedSize">Size of the fixed.</param>
        /// <returns>System.Drawing.Bitmap.</returns>
        public static Bitmap Resize(Stream srcStream, int newWidth, int newHeight, bool fixedSize = true)
        {
            return Resize(new Bitmap(srcStream), newWidth, newHeight, fixedSize);
        }

        /// <summary>
        /// Resizes the specified source BMP.
        /// </summary>
        /// <param name="srcBmp">The source BMP.</param>
        /// <param name="canvasWidth">Width of the canvas.</param>
        /// <param name="canvasHeight">Height of the canvas.</param>
        /// <param name="fixedSize">Size of the fixed.</param>
        /// <returns>System.Drawing.Bitmap.</returns>
        public static Bitmap Resize(Bitmap srcBmp, int canvasWidth, int canvasHeight, bool fixedSize = true)
        {            
            int posX = 0, posY = 0;
            int newHeight = 0, newWidth = 0;

            float ratioX = srcBmp.Width / (float)srcBmp.Height;
            float ratioY = srcBmp.Height / (float)srcBmp.Width;

            if (fixedSize)
            {
                float ratio = Math.Max(ratioX, ratioY);

                //canvas view
                if (srcBmp.Width > srcBmp.Height)
                {
                    newHeight = canvasHeight;
                    newWidth = (int)(canvasWidth * ratio);

                    posX = (int)((newWidth - canvasWidth) / -2.0f);
                }
                else //portrait view
                {
                    newWidth = canvasWidth;
                    newHeight = (int)(canvasHeight * ratio);

                    posY = (int)((newHeight - canvasHeight) / -2.0f);
                }
            }
            else
            {
                float ratio = Math.Min(ratioX, ratioY);
                if (srcBmp.Width > srcBmp.Height)
                {
                    newWidth = canvasWidth;
                    newHeight = (int)(canvasHeight * ratio);
                }
                else //portrait view
                {
                    newHeight = canvasHeight;
                    newWidth = (int)(canvasWidth * ratio);
                }
            }

            Bitmap target = new Bitmap(canvasWidth, canvasHeight);

            using (Graphics graphics = Graphics.FromImage(target))
            {
                graphics.CompositingQuality = CompositingQuality.HighSpeed;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.Clear(Color.White);
                graphics.DrawImage(srcBmp, posX, posY, newWidth, newHeight);
            }

            return target;            
        }

        /// <summary>
        /// Gets the encoder information.
        /// </summary>
        /// <param name="mimeType">Type of the MIME.</param>
        /// <returns>System.Drawing.Imaging.ImageCodecInfo.</returns>
        private static ImageCodecInfo GetEncoderInfo(string mimeType)
        {
            // Get image codecs for all image formats
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();

            // Find the correct image codec
            for (int i = 0; i < codecs.Length; i++)
                if (codecs[i].MimeType == mimeType)
                    return codecs[i];
            return null;
        }

    }
}
