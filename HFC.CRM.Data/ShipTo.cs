﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
   public class ShipTo
    {
        public int Id { get; set; }
        public string ShiptoName { get; set; }
    }
    public class ShipToLocation
    {
        public int Id { get; set; }
        public string ShipToLocationName { get; set; }
    }
}
