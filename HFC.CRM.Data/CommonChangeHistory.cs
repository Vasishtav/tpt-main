﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("[CRM].[CommonChangeHistory]")]
    public class CommonChangeHistory
    {
        [Key]
        public int ChangeHistoryId { get; set; }
        public string TableCode { get; set; }
        public int PrimaryTablePrimaryKey { get; set; }
        public int? SecondaryTablePrimaryKey { get; set; }
        public string ChangedFieldName { get; set; }
        public string ChangedValue { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        [NotMapped]
        public string PersonName { get; set; }

        public CommonChangeHistory() { }

        public CommonChangeHistory(string tableCode, DateTime createdOn
            , int createdBy, int primaryTableKey)
        {
            TableCode = tableCode;
            CreatedOn = createdOn;
            CreatedBy = createdBy;
            PrimaryTablePrimaryKey = primaryTableKey;
        }

        public CommonChangeHistory(string tableCode, DateTime createdOn
            , int createdBy, int primaryTableKey, int SecondaryTableKey) 
            : this(tableCode, createdOn, createdBy, primaryTableKey)  
        {
            SecondaryTablePrimaryKey = SecondaryTableKey;
        }

    }

}
