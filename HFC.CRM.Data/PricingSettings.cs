﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.PricingSettings")]
    public partial class PricingSettings : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int FranchiseId { get; set; }
        public int PricingStrategyTypeId { get; set; }
        public int? VendorId { get; set; }
        public int? ProductCategoryId { get; set; }
        public string RetailBasis { get; set; }
        public decimal? MarkUp { get; set; } = 0;
        public string MarkUpFactor { get; set; }
        public decimal? Discount { get; set; }
        public string DiscountFactor { get; set; }
        public string PriceRoundingOpt { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> ImpersonatorPersonId { get; set; }
    }
}
