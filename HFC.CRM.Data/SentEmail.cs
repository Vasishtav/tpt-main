/***************************************************************************\
Module Name:  SentEmail class
Project: Tochpoint
Created on: 07 November 2017
Created By:
Copyright:
Description: Storing history records
Change History:
Date  By  Description

\***************************************************************************/

namespace HFC.CRM.Data
{
    using Dapper;
    using System;
    using System.Collections.Generic;

    public partial class SentEmail
    {
        public long SentEmailId { get; set; }
        public Nullable<int> LeadId { get; set; }
        public Nullable<int> JobId { get; set; }
        public string Recipients { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public Nullable<System.DateTime> SentAt { get; set; }
        public int FranchiseId { get; set; }
        public Nullable<short> TemplateId { get; set; }
        public Nullable<int> SentByPersonId { get; set; }
        public string CCAddress { get; set; }
        public string BCCAddress { get; set; }
        public string FromAddress { get; set; }
        public int? AccountId { get; set; }
        public int? OpportunityId { get; set; }
        public int? OrderId { get; set; }
        public virtual EmailTemplate EmailTemplate { get; set; }
        public virtual Franchise Franchise { get; set; }
        public virtual Job Job { get; set; }
        public virtual Lead Lead { get; set; }
        public virtual Person Person { get; set; }
        [NotMapped]
        public int? CalendarId { get; set; }
        [NotMapped]
        public int? OrderStatus { get; set; }
        [NotMapped]
        public bool? SendReviewEmail { get; set; }

        public bool EnableSendReviewEmail
        {
            get
            {
                if (OrderStatus != null && OrderStatus == 5 && SendReviewEmail != true)
                {
                    return true;
                }
                return false;
            }
        }
        public bool IsSent { get; set; }
        [NotMapped]
        public string ShowStatus { get; set; }
        [NotMapped]
        public int? OrderNumber { get; set; }

        [NotMapped]
        public string MessageType { get; set; }

        [NotMapped]
        public int TextingStatus { get; set; }
        [NotMapped]
        public bool IsCancelled { get; set; }
        [NotMapped]
        public bool IsDeleted { get; set; }
        //new
        public int? LogType { get; set; }
        public DateTime? SentDate { get; set; }
        public string UserName { get; set; }
        public string Description { get; set; }
        public string CellPhone { get; set; }
        public string Errorinfo { get; set; }
        public int? Status { get; set; }
        public int? TextingTemplateTypeId { get; set; }
        public string Type { get; set; }

        //added newly to show in grid because grid filter was not working, so made as single value 
        public string RecipientValue { get; set; }
        public DateTime? DateValue { get; set; }
        public string SubjectDescription { get; set; }
        //added to show texting status in pop-up
        public string StatusText
        {
            get
            {
                var result = "";
                if (Status.HasValue)
                {
                    var temp = Status.Value;
                    if (temp == 1)
                        result = "Sent";
                    else if (temp == 2)
                        result = "Received";
                    else
                        result = "Error";
                }

                return result;
            }
        }
    }
}
