﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    public class HFCUS_ALL
    {
        public int OBJECTID { get; set; }
        public string PARENT { get; set; }
        public string ZIP { get; set; }
        public string PO_NAME { get; set; }
        public string STATE { get; set; }
        public string ST_FIPS { get; set; }
        public string ZIP_1 { get; set; }
        public string LABEL { get; set; }
        public string HHLD_COUNT { get; set; }
        public string OWNER_OCC { get; set; }
        public string MEDIAN_INC { get; set; }
        public string MED_HOME_V { get; set; }
        public string TOTAL_POP { get; set; }
        public string ZIP_12 { get; set; }
        public string REGION { get; set; }
        public string BBTERRNUM { get; set; }
        public string BBISOLD { get; set; }
        public string BBICONTRAC { get; set; }
        public string BBIFENUM { get; set; }
        public string WNWBB { get; set; }
        public string WNWBBNUM { get; set; }
        public string KABB { get; set; }
        public string CTTERRNUM { get; set; }
        public string CTSOLD { get; set; }
        public string CTCONTRAC { get; set; }
        public string CTFENUM { get; set; }
        public string WNWCT { get; set; }
        public string WNWCTNUM { get; set; }
        public string COUNTY { get; set; }
    }

    public class vwTPT_HFCGIS
    {
        public string ZIP { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string PO_NAME { get; set; }
        public string BB_MinionCode { get; set; }
        public string BBICONTRAC { get; set; }
        public int BBIFENUM { get; set; }
        public string BB_DirName { get; set; }
        public string BBTERRNUM { get; set; }
        public string TL_MinionCode { get; set; }
        public string TLCONTRAC { get; set; }
        public string TLFENUM { get; set; }
        public string TL_DirName { get; set; }
        public string TLTERRNUM { get; set; }
        public string CC_MinionCode { get; set; }
        public string CCCONTRAC { get; set; }
        public string CCFENUM { get; set; }
        public string CC_DirName { get; set; }
        public string CCTERRNUM { get; set; }
    }
}
