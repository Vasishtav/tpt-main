﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.FranchiseProducts")]
    public class FranchiseProducts : BaseEntity
    {
        [Key]
        public int ProductKey { get; set; }
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public int? PICProductID { get; set; }
        public int? VendorID { get; set; }
        public string VendorName { get; set; }
        public string VendorProductSKU { get; set; }
        public string Description { get; set; }
        public int? ProductCategory { get; set; }
        public decimal? Cost { get; set; }
        public decimal? SalePrice { get; set; }
        public int? ProductStatus { get; set; }
        public int? ProductSubCategory { get; set; }
        public string MarkUp { get; set; }
        public string MarkupType { get; set; }
        public decimal? Discount { get; set; }
        public string DiscountType { get; set; }
        public decimal? CalculatedSalesPrice { get; set; }
        public int FranchiseId { get; set; }
        public int ProductType { get; set; }
        [NotMapped]
        public string ProductCategoryName { get; set; }
        [NotMapped]
        public string SubCategoryName { get; set; }
        [NotMapped]
        public string ProductGroupDesc { get; set; }
        [NotMapped]
        public DateTime? DiscontinuedDate{get;set;}
        [NotMapped]
        public DateTime? PhasedoutDate { get; set; }
        
        public string Model { get; set; }
        public string Collection { get; set; }
        public string Color { get; set; }
        [NotMapped]
        public int VendorType { get; set; }
        [NotMapped]
        public Type_ProductCategory TypeProductCategory { get; set; }
        [NotMapped]
        public Guid TempId { get; set; }
        public bool Taxable { get; set; }
        public bool MultipartSurfacing { get; set; }
        public string SurfaceLaborSet { get; set; }
        public string MultipleSurfaceProductSet { get; set; }
        public string MultipartAttributeSet { get; set; }
        public int MasterSurfacingProductID { get; set; }
        [NotMapped]
        public MultipartAttributes Attributes { get; set; }
        [NotMapped]
        public List<MultipleSurfaceProduct> MultipleSurfaceProduct { get; set; }
        [NotMapped]
        public ProductSurfaceSetup ProductSurfaceSetup { get; set; }
    }
}
