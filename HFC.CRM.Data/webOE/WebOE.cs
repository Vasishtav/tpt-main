﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data.webOE
{
    public class WebOE
    {
        public string FranchiseGuid { get; set; }
        public WebOECompany Company { get; set; }
        public WebOEVendor Vendor { get; set; }
        public WebOEOrder Order { get; set; }
        public List<WebOEOrderItem> OrderItems { get; set; }
    }

    public class WebOEResult
    {
        public string CompanyId { get; set; }
        public string OrderId { get; set; }
        public string OrderItemId { get; set; }
        public string VendorOrderId { get; set; }
        public bool Process { get; set; }
        public bool Commit { get; set; }
        public int SessionId { get; set; }
    }

}
