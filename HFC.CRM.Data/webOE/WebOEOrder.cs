﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data.webOE
{
    public class WebOEOrder
    {
        public string POrderNoBB { get; set; }
        public DateTime POrderDateBB { get; set; }
        public string AltPOrderNo { get; set; }
        public string CustomerPOrderNo { get; set; }
        public string CustomerName { get; set; }
        public string SaleRep { get; set; }
        public string SideMark { get; set; }
        public string BillToName { get; set; }
        public string BillToAddress1 { get; set; }
        public string BillToAddress2 { get; set; }
        public string BillToCity { get; set; }
        public string BillToState { get; set; }
        public string BillToZip { get; set; }
        public string BillToCountry { get; set; }
        public short IsDropShip { get; set; }
        public string ShipToName { get; set; }
        public string ShipToAddress1 { get; set; }
        public string ShipToAddress2 { get; set; }
        public string ShipToCity { get; set; }
        public string ShipToState { get; set; }
        public string ShipToZip { get; set; }
        public string ShipToCountry { get; set; }
        public float Cost { get; set; }
        public short Origin { get; set; }
        public string UniqueId { get; set; }
        public string OrderUniqueId { get; set; }
        public string InternalRefNumber { get; set; }
    }
}
