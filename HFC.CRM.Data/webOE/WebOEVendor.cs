﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data.webOE
{
    public class WebOEVendor
    {
        public string Name { get; set; }
        public string AccoutNumber { get; set; }
        
        public string BillToName { get; set; }
        public string BillToAddress1 { get; set; }
        public string BillToAddress2 { get; set; }
        public string BillToCity { get; set; }
        public string BillToState { get; set; }
        public string BillToZip { get; set; }
        public string BillToCountry { get; set; }
        
        public string ShipToName { get; set; }
        public string ShipToAddress1 { get; set; }
        public string ShipToAddress2 { get; set; }
        public string ShipToCity { get; set; }
        public string ShipToState { get; set; }
        public string ShipToZip { get; set; }
        public string ShipToCountry { get; set; }
        public string ShipVia { get; set; }

        public string Terms { get; set; }
        public short OOPProcess { get; set; }
        public short OOPPrintPO { get; set; }
        public string OOPVendorID { get; set; }
        public string UniqueId { get; set; }
    }



}
