﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data.webOE
{
    public class WebOEOrderItem
    {
        public int Quantity { get; set; }
        public string ProductName { get; set; }
        public string ProductType { get; set; }
        public string Manufacturer { get; set; }
        public string Style { get; set; }
        public short LineItem { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public string Notes { get; set; }
        public float BeforeDiscountRetailPrice { get; set; }
        public float AfterDiscountRetailPrice { get; set; }
        public float CustomerDiscount { get; set; }
        public float VendorDiscount { get; set; }
        public float RetailPrice { get; set; }
        public float DiscountedPrice { get; set; }
        public string Discount { get; set; }
        public float DiscountAmount { get; set; }
        public float Price { get; set; }
        public float Cost { get; set; }
        public List<KeyValuePair<string, string>> OptionsData { get; set; }
    }
}
