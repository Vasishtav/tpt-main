﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.TaxSettings")]
    public class TaxSettings
    {
        [Key]
        public int Id { get; set; }

        public int? FranchiseId { get; set; }
        public int? TerritoryId { get; set; }
        public int? ShipToLocationId { get; set; }
        public bool UseCustomerAddress { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int? UpdatedBy { get; set; }

        [NotMapped]
        public string ShipToName { get; set; }

        [NotMapped]
        public string Address { get; set; }

        [NotMapped]
        public int TempId { get; set; }

        [NotMapped]
        public string TerritoryName { get; set; }

        [NotMapped]
        public int? AddressId { get; set; }
        public int TaxType { get; set; }
        public decimal UseTaxPercentage { get; set; }
        [NotMapped]
        public string TaxTypeName { get; set; }
        //public Franchise Franchise { get; set; }
    }


    [Table("CRM.TaxSettingsAudit")]
    public class TaxSettingsAudit
    {
        [Key]
        public int Id { get; set; }
        public int TaxSettingsId { get; set; }
        public decimal OldUseTaxValue { get; set; }
        public decimal NewUseTaxValue { get; set; }
        public string OldTaxSettingsJson { get; set; }
        public string NewTaxSettingsJson { get; set; }
        public bool UseTaxChanged { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        [NotMapped]
        public string UpdatedByPerson { get; set; }
        [NotMapped]
        public string TerritoryName { get; set; }
    }
}