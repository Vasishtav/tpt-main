﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    public class SourceList
    {
        public int SourceId { get; set; }
        public string ChannelName { get; set; }
        public string name { get; set; }
        public bool IsPrimarySource { get; set; }
        public string SourceName { get; set; }
        public string AllowFranchisetoReassign { get; set; }
        public string OriginationName { get; set; }

    }
}
