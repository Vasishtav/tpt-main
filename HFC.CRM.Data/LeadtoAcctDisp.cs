﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    public class LeadtoAcctDisp:BaseEntity
    {
        public int AccountId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string AccountType { get; set; }
        public int AddressId { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string CountryCode2Digits { get; set; }
        public string ZipCode { get; set; }
        public bool IsValidated { get; set; }
        public string HomePhone { get; set; } 
        public string CellPhone { get; set; }
        public string WorkPhone { get; set; }
        public string FaxPhone { get; set; }
        public string PrimaryEmail { get; set; }
        public string PreferedNumber { get; set; }
        public string PreferredTFN { get; set; }
        public string FullAddress { get; set; }
        public int AccountTypeId { get; set; }
    }
}
