﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("[CRM].[VendorTerritoryAccount]")]
    public partial class VendorTerritoryAccount
    {
        [Key]
        public int Id { get; set; }
        [NotMapped]
        public int PreviousId { get; set; }
        public int? FranchiseId { get; set; }
        public int VendorId { get; set; }
        public int TerritoryId { get; set; }
        public string AccountNumber { get; set; }
        public int? ShipToLocation { get; set; }
        [NotMapped]
        public string ShipLocName { get; set; }
        [NotMapped]
        public string ShipToName { get; set; }
        [NotMapped]
        public string FeCode { get; set; }
        [NotMapped]
        public bool IsActive { get; set; }

    }
}
