﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    public class PICVendorProductVM
    {

        public string VendorName { get; set; }
        public int? PICVendorId { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string PICProductGroupId { get; set; }
        public string PICProductGroupName { get; set; }
        public List<PICProduct> PICProductList { get; set; }



        //dynamic picVendor =[new
        //{
        //    VendorName = "",
        //    PICVendorId = "",
        //    ProductGroup = [new
        //    {
        //        GGroup = "",
        //        Description = "",
        //        Products =[new
        //        {
        //            Product = "",
        //            Description = ""
        //        }]
        //    }]
        //}];
    }

    public class PICVendor
    {
        public string VendorName { get; set; }
        public string PICVendorId { get; set; }
        public int ProductId { get; set; }
        public List<PICProductGroup> PICProductGroupList { get; set; }

    }
    public class PICProductGroup
    {
        public string ProductName { get; set; }
        public string PICProductGroupId { get; set; }
        public List<PICProduct> PICProductList { get; set; }
    }
    public class PICProduct
    {
        public string PICProductId { get; set; }
        public string ProductId { get; set; }
        public string ProductName { get; set; }
    }
}
