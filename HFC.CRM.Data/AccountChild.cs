﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
   public class AccountChild
    {
        public int AccountId { get; set; }
        public string CompanyName { get; set; }
        public string Name { get; set; }
        public string AccountType { get; set; }
        public string AccountStatus { get; set; }
        //public string Address { get; set; }
        //public string City { get; set; }
        //public string State { get; set; }
        //public int ZipCode { get; set; }
        public AddressTP Address { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public int AccountTypeId { get; set; }
    }
}
