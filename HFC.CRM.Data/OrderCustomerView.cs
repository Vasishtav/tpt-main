
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Net.Mail;

namespace HFC.CRM.Data
{
    [Table("crm.OrderCustomerView")]
    public partial class OrderCustomerView
    {
        [Key]
        public int OrderCustomerViewId { get; set; }
        public int OrderId { get; set; }
        public bool TermsAccepted { get; set; }
        public String TermsVersion { get; set; }
        public DateTime? TermsAcceptedDate { get; set; }
        public string CustomerSignStreamId { get; set; }
        public string CustomerSignJson { get; set; }
        public DateTime? CustomerSignedDate { get; set; }
        public string SalesRepSignStreamId { get; set; }
        public string SalesRepSignJson { get; set; }
        public DateTime? SalesRepSignedDate { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }

        [NotMapped]
        public CVHeader cVHeader { get; set; }
        [NotMapped]
        public OpportunityInformation opportunity { get; set; }
        [NotMapped]
        public List<ProductSummary> products { get; set; }
        [NotMapped]
        public List<AdditionalItems> services { get; set; }
        [NotMapped]
        public List<DiscountInformation> discounts { get; set; }
        [NotMapped]
        public List<PaymentInformation> payment { get; set; }
        [NotMapped]
        public TermsAndConditions terms { get; set; }
        public OrderSummary orderSummary { get; set; }

    }

    public class CVHeader
    {
        public string InvoiceNumber { get; set; }
        public DateTimeOffset? InvoiceDate { get; set; }
        public string SalesPersonName { get; set; }
    }
    public class OpportunityInformation
    {
        public BillToAddress billToAddress { get; set; }
        public InstallationAddress installationAddress { get; set; }
    }

    public class BillToAddress
    {
        public string AccountCustomerName { get; set; }
        public string BillingAddress1 { get; set; }
        public string BillingAddress2 { get; set; }
        public string BillingCityStateZip { get; set; }
        public string AccountCellPhone { get; set; }
        public string AccountHomePhone { get; set; }
        public string AccountWorkPhone { get; set; }
        public string AccountEmail { get; set; }
        public string OppSideMark { get; set; }
    }

    public class InstallationAddress
    {
        public string AccountCustomerName { get; set; }
        public string InstallationAddress1 { get; set; }
        public string InstallationAddress2 { get; set; }
        public string InstallationCityStateZip { get; set; }
    }

    public class ProductSummary
    {
        public string ProductName { get; set; }
        public string ProductDesc { get; set; }
        public int? QTY { get; set; }
        public decimal? Total { get; set; }
    }

    public class AdditionalItems
    {
        public string Product { get; set; }
        public string Memo { get; set; }
        public bool isTaxable { get; set; }
        public decimal? UnitPrice { get; set; }
        public int? QTY { get; set; }
        public decimal? Total { get; set; }
    }

    public class DiscountInformation
    {
        public string DiscountSummary { get; set; }
        public string Memo { get; set; }
    }

    public class PaymentInformation
    {
        public DateTimeOffset? PaymentDate { get; set; }
        public string Method { get; set; }
        public string Memo { get; set; }
        public decimal? Amount { get; set; }
    }

    public class TermsAndConditions
    {
        public string TC { get; set; }
    }

    public class OrderSummary
    {
        public decimal? ProductSubtotal { get; set; }
        public decimal? AdditionalCharges { get; set; }
        public decimal? SpecialDiscounts { get; set; }
        public decimal? TotalAmount { get; set; }
        public decimal? TaxTotal { get; set; }
        public decimal? GrandTotal { get; set; }
        public decimal? TotalPayments { get; set; }
        public decimal? BalanceDue { get; set; }
    }

    public partial class OrderCustomerViewSign
    {
        public string SignJson { get; set; }
    }
}