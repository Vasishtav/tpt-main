﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.LeadsourceMapping")]
    public class LeadsourceMapping
    {
        [Key]
        public int Id { get; set; }
        public int LMDBSourceID { get; set; }
        public string LMDBSourceDescription { get; set; }
        public int SourcesTPId { get; set; }
        public bool AllowFranchisetoReassign { get; set; }
        public string LeadType { get; set; }
    }
}
