
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HFC.CRM.Data
{
    [Table("CRM.NewsUpdates")]
    public partial class NewsUpdates : BaseEntity
    {
        [Key]
        public int NewsUpdatesId { get; set; }
        public string Headline { get; set; }
        public bool IsActive { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string Message { get; set; }
        public string ShortMessage { get; set; }
        public int SortOrder { get; set; }

        [NotMapped]
        public List<Type_Brands> Brands { get; set; }

        [NotMapped]
        public bool BB { get; set; }

        [NotMapped]
        public bool TL { get; set; }

        [NotMapped]
        public bool CC { get; set; }

    }

    [Table("CRM.Type_Brands")]
    public partial class Type_Brands 
    {
        public int Id { get; set; }
        public string Brand { get; set; }

        [NotMapped]
        public bool IsApplicable { get; set; }
    }
 }
