﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    using Dapper;
    using System;
    using System.Collections.Generic;
    [Table("CRM.GlobalDocuments")]
    public partial class GlobalDocuments
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public int? Category { get; set; }
        public int? Concept { get; set; }
        public string streamId { get; set; }
        public bool? IsEnabled { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }

        [NotMapped]
        public int Quantity { get; set; }
        [NotMapped]
        public string Concept_Title { get; set; }
        [NotMapped]
        public string Category_Title { get; set; }


    }
}
