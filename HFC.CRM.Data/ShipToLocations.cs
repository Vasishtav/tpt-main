﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.ShipToLocations")]
    public class ShipToLocations
    {
        public int Id { get; set; }
        public int ShipToLocation { get; set; }
        [NotMapped]
        public string ShipToLocationName { get; set; }
        public string ShipToName { get; set; }
        public int? AddressId { get; set; }
        [NotMapped]
        public AddressTP Addresses { get; set; }
        [NotMapped]
        public List<int> TerritoriesId { get; set; }
        [NotMapped]
        public List<int> SelectedRelatedTerritoriesId { get; set; }
        public bool? Active { get; set; }
        public bool? Deleted { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public int? LastUpdatedBy { get; set; }
        public int? FranchiseId { get; set; }
        [NotMapped]
        public string Address { get; set; }
        [NotMapped]
        public int? TerritoryId { get; set; }
        [NotMapped]
        public bool IsValidated { get; set; }
        public string PhoneNumber { get; set; }
    }
}
