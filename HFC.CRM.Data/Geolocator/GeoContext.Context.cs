﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HFC.CRM.Data.Geolocator
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class GeoLocatorEntities : DbContext
    {
        public GeoLocatorEntities() : base("GeoLocatorEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<TerritoryZipCode> TerritoryZipCodes { get; set; }
        public virtual DbSet<TerritoryPostalCode> TerritoryPostalCodes { get; set; }
        public virtual DbSet<ZipPostalCode> ZipPostalCodes { get; set; }
    }
}
