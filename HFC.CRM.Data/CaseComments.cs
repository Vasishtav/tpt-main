﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.CaseComments")]
    public class CaseComments
    {
        [Key]
        public long Id { get; set; }
        public int CaseId { get; set; }
        public int PersonId { get; set; }
        public int ParentId { get; set; }
        public bool HasChildId { get; set; }
        public string HTMLComment { get; set; }
        public bool IsReply { get; set; }
        public bool IsEdit { get; set; }
        //public bool IsDelete { get; set; }
        public Nullable <DateTime> DeletedOn { get; set; }
        public int BelongsTo { get; set; }
        public Nullable <DateTime> CreatedOn { get; set; }
        public Nullable <DateTime> LastUpdatedOn { get; set; }

        [NotMapped]
        public string UserName { get; set; }
    }
}
