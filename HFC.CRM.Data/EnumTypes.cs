﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Collections.Generic;
using HFC.CRM.Data.Extensions.EnumAttr;

namespace HFC.CRM.Data
{
    public enum ResponseTypeEnum
    {
        [Description("text/html")]
        Html,
        [Description("application/json")]
        Json,
        [Description("application/x-www-form-urlencoded")]
        Form,
        [Description("application/xml")]
        Xml,
        [Description("application/javascript")]
        Javascript,
        [Description("text/plain")]
        Text,
        [Description("image/jpeg")]
        Jpeg,
        [Description("image/png")]
        Png,
        [Description("image/gif")]
        Gif,
        [Description("application/octet-stream")]
        Stream,
        [Description("application/vnd.ms-excel")]
        Excel,
        [Description("application/msword")]
        MSWord,
        [Description("application/pdf")]
        PDF,
        [Description("application/zip")]
        Zip
    }

    public enum CommercialTypes
    {
        All = 0,
        Commercial = 1,
        Residential = 2,
        InspiredDrapes = 3
    }

    public enum TimeZoneEnum
    {
        [Description("Unassigned")]
        NA,
        [Description("Coordinated Universal Time"), IsNotUIVisible]
        UTC,
        [Description("Pacific Standard Time")]
        PST,
        [Description("Eastern Standard Time")]
        EST,
        [Description("Central Standard Time")]
        CST,
        [Description("Mountain Standard Time")]
        MST,
        //[Description("Atlantic Standard Time")]
        // AST,
        [Description("Hawaiian Standard Time")]
        HAST,
        [Description("Alaska Time Zone")]
        ATZ,
        [Description("US Mountain Standard Time")]
        USMST,
        [Description("Newfoundland Standard Time")]
        NST
    }

    public enum MobileBrowserEnum
    {
        Unknown,
        Android,
        BlackBerry,
        iOS,
        Opera,
        IE
    }

    public enum SearchFilterEnum
    {
        [Description("Auto Detect")]
        Auto_Detect,
        [Description("Address")]
        Address,
        [Description("Customer Name")]
        Customer_Name,
        [Description("Email")]
        Email,
        [Description("Job/Invoice Number")]
        Number,
        [Description("Phone Number")]
        Phone_Number
    }

    public enum SearchDuplicateLeadEnum
    {
        CellPhone,
        HomePhone,
        PrimaryEmail,
        FaxPhone,
        WorkPhone,
        SecondaryEmail,
        All

    }
    public enum SearchDuplicateOpportunityEnum
    {
        CellPhone,
        HomePhone,
        PrimaryEmail,
        FaxPhone,
        WorkPhone,
        SecondaryEmail,
        All

    }
    public enum SearchDuplicateAccountEnum
    {
        CellPhone,
        HomePhone,
        PrimaryEmail,
        FaxPhone,
        WorkPhone,
        SecondaryEmail,
        All

    }

    public enum AdminFilterEnum
    {
        [Description("Auto Detect")]
        Auto_Detect,
        [Description("Allow Suspended")]
        Allow_Suspended
    }

    public enum InvoiceSearchFilterEnum
    {
        [Description("Auto Detect")]
        Auto_Detect,
        [Description("Customer Name")]
        Customer_Name,
        [Description("Email")]
        Email,
        [Description("Job Number")]
        Number,
        [Description("Invoice Number")]
        Invoice_Number,
        [Description("Phone Number")]
        Phone_Number,
    }


    public enum ExportTypeEnum
    {
        [Description("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")]
        Excel,
        [Description("html/text")]
        CSV,
        [Description("application/pdf")]
        PDF,
        /// <summary>
        /// Word is MS Word in .docx extension
        /// </summary>
        [Description("application/vnd.openxmlformats-officedocument.wordprocessingml.document")]
        Word
    }

    #region Extensions

    public static class DayOfWeekEnumExtension
    {
        private static readonly IEnumerable<DayOfWeekEnum> availableValues = Enum.GetValues(typeof(DayOfWeekEnum)).Cast<DayOfWeekEnum>();

        public static DayOfWeekEnum[] ToArray(this DayOfWeekEnum dow)
        {
            return availableValues.Where(v => dow.HasFlag(v)).OrderBy(o => o).ToArray();
        }

        public static List<DayOfWeekEnum> ToList(this DayOfWeekEnum dow)
        {
            return availableValues.Where(v => dow.HasFlag(v)).OrderBy(o => o).ToList();
        }
    }

    public static class EnumExtension
    {
        public static string Description(this Enum value)
        {
            var enumType = value.GetType();
            var field = enumType.GetField(value.ToString());
            var attributes = field.GetCustomAttributes(typeof(DescriptionAttribute),
                                                       false);
            return attributes.Length == 0
                ? value.ToString()
                : ((DescriptionAttribute)attributes[0]).Description;
        }

        /// <summary>
        /// This will convert an Enum to a dictionary object for JSON serializing so it can be used like DayOfWeek.Sunday, which will have a value of 6, or DayOfWeek.Monday (value of 0)
        /// </summary>
        /// <typeparam name="TValue">Required, the enum data type</typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Dictionary<string, TValue> ToDictionary<TValue>(this Enum value, params TValue[] skip)
        {
            var result = new Dictionary<string, TValue>();
            foreach (var e in Enum.GetValues(value.GetType()))
            {
                if (!skip.Contains((TValue)e))
                {
                    result.Add(e.ToString(), (TValue)e);
                }
            }
            return result;
        }

        public enum PurchaseOrderStatus
        {
            [Description("Open")]
            Open = 1,
            [Description("Submitted")]
            Submitted = 2,
            [Description("Shipped")]
            Shipped = 3,
            [Description("Partial Shipped Back Ordered")]
            Partial_Shipped_Back_Ordered = 4,
            [Description("Invoiced")]
            Invoiced = 5,
            [Description("Printed")]
            Printed = 6,
            [Description("Closed")]
            Closed = 7,
            [Description("Canceled")]
            Canceled = 8,
            [Description("Processing")]
            Processing = 9,
            [Description("Error")]
            Error = 10,
            [Description("ACKNOWLEDGED")]
            ACKNOWLEDGED = 11

        }


    }

    #endregion
}
