﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.VendorCase")]
    public class VendorCase : BaseEntity
    {
        [Key]
        public int VendorCaseId { get; set; }
        public int CaseId { get; set; }
        public string VendorCaseNumber { get; set; }
        public int? Status { get; set; }
        public int? VendorId { get; set; }
        //public DateTime CreatedOn { get; set; }
        //public int CreatedBy { get; set; }
        //public DateTime? LastUpdatedOn { get; set; }
        //public int? LastUpdatedBy { get; set; }
    }

    public class VendorFranchiseDetails
    {
        public int FranchiseId { get; set; }
        public string FranchiseName { get; set; }
        public bool Valid { get; set; }
    }

    public class VendorCalendar
    {
        public int VendorCaseId { get; set; }
        public int CaseId { get; set; }
        public int VendorCaseNumber { get; set; }
        public int AccountId { get; set; }
        public int OpportunityId { get; set; }
        public int OrderId { get; set; }
        public bool IsNotifyemails { get; set; }
        public bool IsNotifyText { get; set; }
    }
}
