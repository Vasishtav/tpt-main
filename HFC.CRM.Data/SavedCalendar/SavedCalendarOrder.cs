﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
namespace HFC.CRM.Data.SavedCalendar
{
    [Table("CRM.SavedCalendarOrder")]
    public partial class SavedCalendarOrder
    {
        [Key]
        public int Id { get; set; }
        public int SaveCalendarId { get; set; }
        public int LoginUserid { get; set; }
        public int SequenceOrder { get; set; }
        public string ViewTitle { get; set; }
        public bool IsDefault { get; set; }
        public int IsDeleted { get; set; }

        [NotMapped]
        public bool IsBussinessHour { get; set; }
    }
}
