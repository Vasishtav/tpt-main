﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
namespace HFC.CRM.Data.SavedCalendar
{
    [Table("CRM.SavedCalendarUsers")]
    public partial class SavedCalendarUsers
    {
        [Key]
         public int Id { get; set; }
        public int SaveCalendarId { get; set; }
        public int SelectedUserid { get; set; }
        public int SelectedRoleid { get; set; }
        public int FranchiseId { get; set; }
    }
}
