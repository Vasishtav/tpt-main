﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace HFC.CRM.Data.SavedCalendar
{
    [Table("CRM.SavedCalendar")]
    public partial class SavedCalendar
    {
        [Key]
        public int SaveCalendarId { get; set; }
        public int LoginUserid { get; set; }
        public string ViewTitle { get; set; }
        public string Viewmode { get; set; }
        public string EmailId { get; set; }
        public int FranchiseId { get; set; }
        [NotMapped]
        public bool IsDefault { get; set; }
        [NotMapped]
        public string active { get; set; }
        public bool IsBussinessHour { get; set; }
        [NotMapped]
        public List<SavedCalendarUsers> SavedCalendarUsers { get;set;}
        [NotMapped]
        public List<SavedCalendarOrder> SavedCalendarOrder { get; set; }
    }
}
