
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HFC.CRM.Data
{

    public partial class ChangeTracking
    {
        public int ChangeTrackingId { get; set; }
        public Nullable<int> LeadId { get; set; }
        public Nullable<int> FranchiseId { get; set; }
        public Nullable<int> JobId { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public string Status { get; set; }
        public string SubStatus { get; set; }
        public Nullable<int> AdminLogInId { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public string Disposition { get; set; }

        public virtual CustomerTP Customer { get; set; }
        public virtual Franchise Franchise { get; set; }
        public virtual Job Job { get; set; }
        public virtual Lead Lead { get; set; }
    }
}
