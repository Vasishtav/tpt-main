﻿namespace HFC.CRM.Data.Context
{
    public interface IContextProvider
    {
        CRMContext Context { get; }

        void Destroy();
    }
}