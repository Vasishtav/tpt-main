﻿namespace HFC.CRM.Data.Context
{
    using System;
    using System.Web.Configuration;

    public class ContextFactory
    {
        [ThreadStatic]
        private static volatile ContextFactory instance;
        private static readonly object syncRoot = new Object();

        private IContextProvider contextProvider;

        private ContextFactory() { }


        public void Init(IContextProvider provider)
        {
            this.contextProvider = provider;
        }

        public CRMContext Context
        {
            get
            {
                if (this.contextProvider == null)
                {
                    ContextFactory.Current.Init(new WebContextProvider());
                }

                return this.contextProvider.Context;
            }
        }

        public void Destroy()
        {
            if (this.contextProvider != null)
            {
                this.contextProvider.Destroy();
            }


        }

        public static string CrmConnectionString
        {
            get { return WebConfigurationManager.ConnectionStrings["CRMConnectionString"].ConnectionString; }
        }

        public static string CrmGeoLocatorConnectionString
        {
            get { return WebConfigurationManager.ConnectionStrings["GeoLocatorEntities"].ConnectionString; }
        }

        public static string MigrationConstr
        {
            get { return WebConfigurationManager.ConnectionStrings["MigrationConstr"].ConnectionString; }
        }

        public static ContextFactory Current
        {
            get
            {
                lock (syncRoot)
                {
                    if (instance == null)
                        instance = new ContextFactory();
                }


                return instance;
            }
        }

        public static CRMContextEx Create()
        {
            return new CRMContextEx();
        }
    }
}