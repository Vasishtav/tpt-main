﻿namespace HFC.CRM.Data.Context
{
    public interface IContextFactory
    {
        CRMContext GetDbContext();

        void Destroy();
    }
}