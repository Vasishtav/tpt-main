﻿namespace HFC.CRM.Data.Context
{
    using System;

    public class ThreadStaticContextProvider : IContextProvider
    {
        [ThreadStatic]
        private static CRMContext _context;

        public CRMContext Context
        {
            get
            {
                return _context ?? (_context = new CRMContextEx());
            }
        }

        public void Destroy()
        {
            if (_context != null)
            {
                _context.Dispose();
                _context = null;
            }
        }
    }
}