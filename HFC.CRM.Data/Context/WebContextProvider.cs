﻿namespace HFC.CRM.Data.Context
{
    using System.Web;

    public class WebContextProvider: IContextProvider
    {
        private const string ItemsKey = "__CRMContext__";

        public CRMContext Context
        {
            get
            {
                if (HttpContext.Current == null)
                {
                    return new CRMContextEx();
                }
                var context = (CRMContext)HttpContext.Current.Items[ItemsKey];
                if (context == null) HttpContext.Current.Items[ItemsKey] = context = new CRMContextEx();
                return context;
            }
        }

        public void Destroy()
        {
            var context = (CRMContext)HttpContext.Current.Items[ItemsKey];
            if (context != null)
            {
                context.Dispose();
                HttpContext.Current.Items.Remove(ItemsKey);
            }
        }
    }
}