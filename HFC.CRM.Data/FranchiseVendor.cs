﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{

    public enum VendorType
    {
        AlianceVendor = 1,
        NonAlianceVendor = 2,
        FranchiseVendor = 3
    }

    [Table("Acct.FranchiseVendors")]
    public partial class FranchiseVendor : BaseEntity
    {
        [Key]
        public int VendorIdPk { get; set; }
        public int VendorId { get; set; }
        public string Name { get; set; }
        public string AccountRep { get; set; }
        public string AccountRepPhone { get; set; }
        public string AccountRepEmail { get; set; }
        public string AccountNo { get; set; }
        public string Terms { get; set; }
        public string CreditLimit { get; set; }
        public string OrderingMethod { get; set; }
        public string ContactDetails { get; set; }
        public int? AddressId { get; set; }
        //public bool? IsActive { get; set; }
        public int VendorType { get; set; }
        public int FranchiseId { get; set; }
        public int VendorStatus { get; set; }
        public int Currency { get; set; }
        public string RetailBasis { get; set; }
        [NotMapped]
        public string PICVendorId { get; set; }

        //public VendorCurrencyModel VendorCurrencyModel { get; set; }
        [NotMapped]
        public AddressTP VendorAddress { get; set; }
        //[NotMapped]
       // public VendorType VendorType2 { get; set; }
        [NotMapped]
        public int SelectedVendorType { get; set; }
        [NotMapped]
        public string AddressCityZip { get; set; }  /*just added to display value in kendo*/
        [NotMapped]
        public string VendorName { get; set; }
        [NotMapped]
        public string OrderingName { get; set; }
        [NotMapped]
        public string AcctStatus { get; set; }
        [NotMapped]
        public string CurrencyValueCode { get; set; }
        [NotMapped]
        public Boolean IsAlliance { get; set; }
        [NotMapped]
        //public List<ShipToLocationsMap> Shipvalue { get; set; }
        public List<VendorTerritoryAccount> Shipvalue { get; set; }
        //public int FranchiseId { get; set; }
        //public string Currency { get; set; }
        //public string RetailBasis { get; set; }
        [NotMapped]
        public bool? Beta { get; set; }
        public bool BulkPurchase { get; set; }
        public bool ShipVia { get; set; }
        public string BPAccountNo { get; set; }
        public int? BPShipLocation { get; set; }
        public string Carrier { get; set; }
        public string ShipViaAccountNo { get; set; }

        [NotMapped]
        public int? ShipAddressId { get; set; }

    }



}

