﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{

    public class FranchiseCaseList
    {

        public int CaseId { get; set; }
        public int CaseNo { get; set; }
        public string VendorCaseNo { get; set; }
        public string VendorCaseNumber { get; set; }
        public int PurchaseOrderId { get; set; }
        public int MPO { get; set; }
        public int VPO { get; set; }
        public int VendorId { get; set; }
        public string Vendor { get; set; }
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public string StatusName { get; set; }
        public int Type { get; set; }
        public int InstallerId { get; set; }
        public int SalesAgentId { get; set; }
        public string TypeName { get; set; }
        public DateTime DateTimeOpened { get; set; }
        public string CaseOwner { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string DateTimeOpen { get; set; }
        public string QuoteLineNumber { get; set; }
        public int FranchiseId { get; set; }
        public string FranchiseName { get; set; }
        public bool ConvertToVendor { get; set; }
        public int ProductTypeId { get; set; }
    }




}
