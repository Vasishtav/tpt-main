﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
namespace HFC.CRM.Data
{
   [Table("[CRM].[Type_OrderStatus]")]
    public class Type_OrderStatusTP : BaseEntity
    {
        public int OrderStatusId { get; set; }
        public string OrderStatus { get; set; }
        public bool IsActive { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
