﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    public class EmailUrlParam
    {
        #region Public Properties

        /// <summary>
        /// Email Type
        /// </summary>
        public int EmailType { get; set; }

        /// <summary>
        /// Get or set body, usually in HTML format.  Remember to html encode and decode.
        /// </summary>
        public string body { get; set; }

        /// <summary>
        /// Get or set subject 
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Get or set attachment url
        /// </summary>
        public string attachment { get; set; }

        /// <summary>
        /// Get or set Bcc email addresses 
        /// </summary>
        public List<string> bccAddresses { get; set; }

        /// <summary>
        /// Get or set cc email addresses
        /// </summary>
        public List<string> ccAddresses { get; set; }

        public string fromAddresses { get; set; }

        /// <summary>
        /// Get or set a start date
        /// </summary>
        public DateTime startDate { get; set; }

        /// <summary>
        /// Get or Set To email addresses
        /// </summary>
        public List<string> toAddresses { get; set; }

        
       public string ToAddresses { get; set; }
        public string BccAddresses { get; set; }
        public string AttachmentText { get; set; }
        public string AttachmentLink { get; set; }
        public string Body { get; set; }

        #endregion
    }

}
