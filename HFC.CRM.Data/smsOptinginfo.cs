﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("[CRM].[smsOptinginfo]")]
    public class smsOptinginfo
    {
        [Key]
        public int Id { get; set; }
        public string Phonenumber { get; set; }
        public bool? Optin { get; set; }
        public bool? Optout { get; set; }
        public int? BrandId { get; set; }
        public bool? IsOptinmessagesent { get; set; }
        public string Errorinfo { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
    }
}
