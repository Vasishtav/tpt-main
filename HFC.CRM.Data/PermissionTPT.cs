﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    public class Permissionset_VM
    {
        public System.Guid RoleId { get; set; }
        public int PermissionSetId { get; set; }
        public string PermissionSetName { get; set; }
        public string Description { get; set; }
        public int? FranchiseId { get; set; }
        public System.DateTime CreatedOnUtc { get; set; }
        public Nullable<System.DateTime> LastUpdatedOnUtc { get; set; }
        public List<PermissionTPT> PermissionSets { get; set; }
        public bool IsActive { get; set; }
    }
    public class PermissionTPT
    {
        public int id { get; set; }  // Refence to Module GroupId, module id , if special permission -> ModuleId + 1000
        public int? ModuleId { get; set; }
        public int? PermissionTypeId { get; set; }
        public string Name { get; set; } // Combination of module, special permission and module group
        public string Module { get; set; }
        public int? ModuleGroupId { get; set; }
        public int? parentId { get; set; } // ReportsTo Parent (Id)
        public bool IsSpecialPermission { get; set; }
        public bool Create { get; set; }
        public bool Read { get; set; }
        public bool Update { get; set; }
        public bool Delete { get; set; }
        public bool SpecialPermission { get; set; } // special permission access
        public int? ParentGroupId { get; set; }
        
    }

    public class UserRolesPermissionTPT
    {
        public int ModuleId { get; set; }
        public string Module { get; set; }
        public string ModuleCode { get; set; }
        public bool IsSpecialPermission { get; set; }
        public bool CanCreate { get; set; }
        public bool CanRead { get; set; }
        public bool CanUpdate { get; set; }
        public bool CanDelete { get; set; }
        public List<UserRolesSpecialPermissionTPT> SpecialPermission { get; set; }
    }

    public class UserRolesSpecialPermissionTPT
    {
        public int PermissionTypeId { get; set; }
        public string PermissionName { get; set; }
        public string PermissionCode { get; set; }
        public bool CanAccess { get; set; } // special permission access
    }

}
