﻿using Dapper;
using System;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
  [Table("CRM.ShipToLocationsMap")]

   public class ShipToLocationsMap
    {
        public int Id { get; set; }
        public int FranchiseId { get; set; }
        public int? TerritoryId { get; set; }
        public int? ShipToLocation { get; set; }
        [NotMapped]
        public string ShipToName { get; set; }
        [NotMapped]
        public string AccountNumber { get; set; }
        [NotMapped]
        public string AddressId { get; set; }
        [NotMapped]
        public int VendorId { get; set; }
        [NotMapped]
        public string Addressvalue { get {
                var temp = Address1;
                temp = string.IsNullOrEmpty(temp) ? Address2 : Address1+ ", " + Address2;
                temp = string.IsNullOrEmpty(temp) ? City : temp + "," + City;
                temp = string.IsNullOrEmpty(temp) ? State : temp + "," + State;
                temp = string.IsNullOrEmpty(temp) ? ZipCode : temp + "," + ZipCode;
                return temp;

            } }
        [NotMapped]
        public int PreviousId { get; set; }
        [NotMapped]
        public bool? Active { get; set; }
        [NotMapped]
        public string Address1 { get; set; }
        [NotMapped]
        public string Address2 { get; set; }
        [NotMapped]
        public string City { get; set; }
        [NotMapped]
        public string State { get; set; }
        [NotMapped]
        public string ZipCode { get; set; }
        [NotMapped]
        public string TerritoryName { get; set; }
        [NotMapped]
        public bool IsAlliance { get; set; }
    }
}
