﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.EmailSettings")]
    public class EmailSettings : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int FranchiseId { get; set; }
        public int EmailType { get; set; }
        public Guid FromEmailId { get; set; }
        public bool? CopyAssignedSales { get; set; }
        public bool? CopyAssignedInstaller { get; set; }

        [NotMapped]
        public string EmailTypeName { get; set; }
        [NotMapped]
        public string Email { get; set; }
    }
}
