﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.QuotelineDetail")]
    public class QuoteLineDetail
    {
        [Key]
        public int QuoteLineDetailId { get; set; }
        public int QuoteLineId { get; set; }
        public int Quantity { get; set; }
        public decimal? BaseCost { get; set; } 
        public decimal? BaseResalePrice { get; set; } = 0;
        public decimal? BaseResalePriceWOPromos { get; set; } = 0;
        public string PricingCode { get; set; }
        public string PricingRule { get; set; }
        public decimal? MarkupFactor { get; set; } = 0;
        public string Rounding { get; set; }
        public decimal? SuggestedResale { get; set; }
        public decimal? Discount { get; set; } = 0;
        public decimal? unitPrice { get; set; }
        public decimal? Cost { get; set; }
        public bool ManualUnitPrice { get; set; }
        public decimal? DiscountAmount { get; set; } = 0;
        public decimal? ExtendedPrice { get; set; } = 0;
        public decimal NetCharge { get; set; } = 0;
        public decimal? MarginPercentage { get; set; } = 0;
        public int? PricingId { get; set; }
        public string ProductOrOption { get; set; }
        public string SelectedPrice { get; set; }
        public int? CounterId { get; set; }
        public bool? ValidConfiguration { get; set; }
        public string ProductErrors { get; set; }
        public DateTime? LastValidatedon { get; set; }        
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public int? LastUpdatedBy { get; set; }
    }
}
