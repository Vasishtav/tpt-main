﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.HFCProducts")]
    public class HFCProducts : BaseEntity
    {
        [Key]
        public int ProductKey { get; set; }

        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public int? PICProductID { get; set; }
        public int? VendorID { get; set; }
        public string VendorName { get; set; }
        public string VendorProductSKU { get; set; }
        public string Description { get; set; }
        public string ProductCategory { get; set; }
        public string ProductCollection { get; set; }
        public string ProductModelID { get; set; }
        public string ProductGroup { get; set; }
        public string ProductGroupDesc { get; set; }
        public bool? Active { get; set; }
        public DateTime? DiscontinuedDate { get; set; }
        public DateTime? PhasedoutDate { get; set; }

        [NotMapped]
        public string PICVendorId { get; set; }

        public DateTime? TS_product { get; set; }

        public int WidthPrompt { get; set; }
        public int HeightPrompt { get; set; }
        public int RoomPrompt { get; set; }
        public int MountPrompt { get; set; }
        public int FabricPrompt { get; set; }
        public int ColorPrompt { get; set; }
        public int BrandId { get; set; }
        public int? ProductCategoryId { get; set; }
        public HFCVendor Vendor { get; set; }
        [NotMapped]
        public string Model { get; set; }
        [NotMapped]
        public String ModelDescription { get; set; }
        [NotMapped]
        public string WarningMessage { get; set; }
        [NotMapped]
        public DateTime? ModelDiscontinuedDate { get; set; }
        [NotMapped]
        public DateTime? ModelPhasedoutDate { get; set; }
    }
}