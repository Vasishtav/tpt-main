﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    /// <summary>
    /// Class MarketingCampaign.
    /// </summary>
    [MetadataType(typeof(MarketingCampaign_Metadata))]
    public partial class MarketingCampaign
    {

    }

    /// <summary>
    /// Class MarketingCampaign_Metadata.
    /// </summary>
    public class MarketingCampaign_Metadata
    {
        /// <summary>
        /// Gets or sets the franchise identifier.
        /// </summary>
        /// <value>The franchise identifier.</value>
        [Required(ErrorMessage="Franchise is required")]
        [Range(1, int.MaxValue, ErrorMessage="Franchise Id must be greater than 0")]
        public int FranchiseId { get; set; }

        /// <summary>
        /// Gets or sets the source identifier.
        /// </summary>
        /// <value>The source identifier.</value>
        [Required(ErrorMessage = "Source is required")]
        [Range(1, int.MaxValue, ErrorMessage = "Source Id must be greater than 0")]
        public short SourceId { get; set; }

        /// <summary>
        /// Gets or sets the amount.
        /// </summary>
        /// <value>The amount.</value>
        [Range(0, double.MaxValue, ErrorMessage="Amount must be a positive number")]
        public decimal Amount { get; set; }

        /// <summary>
        /// Gets or sets the label.
        /// </summary>
        /// <value>The label.</value>
        [Required]
        public string Label { get; set; }    
    }
}
