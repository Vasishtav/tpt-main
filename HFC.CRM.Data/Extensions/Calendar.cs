﻿using Dapper;
using HFC.CRM.Data.Metadata;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    /// <summary>
    /// Class Calendar.
    /// </summary>
    [MetadataType(typeof(CalendarMetadata))]
    public partial class Calendar
    {
        /// <summary>
        /// Gets or sets the is editable.
        /// </summary>
        /// <value>The is editable.</value>
        [NotMapped]
        public bool IsEditable { get; set; }
        /// <summary>
        /// Gets or sets the is deletable.
        /// </summary>
        /// <value>The is deletable.</value>
        [NotMapped]
        public bool IsDeletable { get; set; }
        /// <summary>
        /// Gets or sets the name of the color class.
        /// </summary>
        /// <value>The name of the color class.</value>
        [NotMapped]
        public string ColorClassName { get; set; }

        /// <summary>
        /// Equalses the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns>System.Boolean.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Calendar)
                return CalendarGuid.Equals(((Calendar)obj).CalendarGuid);
            else
                return false;
        }

        /// <summary>
        /// Gets the hash code.
        /// </summary>
        /// <returns>System.Int32.</returns>
        public override int GetHashCode()
        {
            return CalendarGuid.GetHashCode();
        }
    }


    /// <summary>
    /// Class EventRecurring.
    /// </summary>
    [MetadataType(typeof(EventRecurringMetadata))]
    [JsonObject(IsReference = true)]
    public partial class EventRecurring
    {
        //[JsonIgnore]
        //public bool IsComputed { get; set; }

        //[JsonIgnore]        
        //public List<CalculatedOccurrence> AllRecurringDateRange { get; set; }

        //[JsonIgnore]
        //public DateTimeOffset? ComputedEndsOn { get; private set; }

        /// <summary>
        /// Gets the first occurrence.
        /// </summary>
        /// <value>The first occurrence.</value>
        public CalculatedOccurrence FirstOccurrence
        {
            get
            {
                if (EndsOn.HasValue && EndDate.UtcDateTime == EndsOn.Value.UtcDateTime)
                {
                    return new CalculatedOccurrence
                    {
                        OccurrenceNumber = 1,
                        StartDate = StartDate,
                        EndDate = EndDate
                    };
                }
                else
                {
                    _currOccurrenceNum = 0;
                    return GetNextOccurrence();
                }
            }
        }

        /// <summary>
        /// Gets or sets the next start date.
        /// </summary>
        /// <value>The next start date.</value>
        private DateTimeOffset NextStartDate { get; set; }
        /// <summary>
        /// Gets or sets the next end date.
        /// </summary>
        /// <value>The next end date.</value>
        private DateTimeOffset NextEndDate { get; set; }

        /// <summary>
        /// The _curr occurrence number
        /// </summary>
        private int _currOccurrenceNum = 0;
        /// <summary>
        /// The is daylight saving time
        /// </summary>
        private bool IsDaylightSavingTime;

        /// <summary>
        /// Returns the next occurring start date after StartsOn
        /// </summary>
        /// <returns>Returns null if there are no more recurring start date</returns>
        public CalculatedOccurrence GetNextOccurrence()
        {
            if (EndsAfterXOccurrences.HasValue && _currOccurrenceNum >= EndsAfterXOccurrences.Value)
                return null;

            //important to start at 0, so we can calculate true first occurrence, if start date isn't the real first occurrence
            //example: 11/27/2013 is start date but if recurring settings is set to weekly on Tuesday and Thursday, then the first occurrence is thursday 11/28/2013
            //so we need to run this and get first occurrence
            if (_currOccurrenceNum == 0)
            {
                NextStartDate = this.StartDate;
                NextEndDate = this.EndDate;
                IsDaylightSavingTime = NextStartDate.LocalDateTime.IsDaylightSavingTime();
            }

            int daysToAdd = this.RecursEvery;
            if (this.PatternEnum == RecurringPatternEnum.Daily && _currOccurrenceNum == 0)
            {
                daysToAdd = 0;
            }
            if (this.PatternEnum == RecurringPatternEnum.Weekly)
            {
                if (this.DayOfWeekEnum.HasValue)
                {
                    var doWList = this.DayOfWeekEnum.Value.ToList();
                    if (_currOccurrenceNum == 0 && (DayOfWeekEnum)Math.Pow(2, (int)NextStartDate.DayOfWeek) >= doWList.First())
                    {
                        daysToAdd = 0;
                    }
                    else
                    {
                        var nextDate = NextStartDate.GetNextDayOfWeekDate(doWList, this.RecursEvery);
                        daysToAdd = nextDate.Subtract(NextStartDate).Days;
                    }
                }
                else //only has 1 day of week selected so we can just multiply by week with recur number
                    daysToAdd = 7 * this.RecursEvery;
            }
            else if (this.PatternEnum == RecurringPatternEnum.Monthly)
            {
                if (_currOccurrenceNum == 0)
                {
                    if (this.DayOfWeekIndex.HasValue)
                    {
                        var firstocc = NextStartDate.GetRelativeDate(this.DayOfWeekIndex.Value, this.DayOfWeekEnum.Value);
                        //var dowIndex = NextStartDate.GetDoWIndex();
                        //if (dowIndex == this.DayOfWeekIndex.Value && (DayOfWeekEnum)Math.Pow(2, (int)NextStartDate.DayOfWeek) == this.DayOfWeekEnum)
                        //{
                        //    daysToAdd = 0;
                        //}
                        if (NextStartDate.Day < firstocc.Day) //still within the current month so just get the day diff to the first occurrence start day
                        {
                            daysToAdd = firstocc.Day - NextStartDate.Day;
                        }
                        else
                        {
                            var nextDate = NextStartDate.AddMonths(this.RecursEvery).GetRelativeDate(this.DayOfWeekIndex.Value, this.DayOfWeekEnum.Value);
                            daysToAdd = nextDate.Subtract(NextStartDate).Days;
                        }
                    }
                    else if (this.DayOfMonth.HasValue)
                    {
                        if (NextStartDate.Day < this.DayOfMonth.Value) //still within the current month so just get the day diff to the first occurrence start day
                        {
                            daysToAdd = this.DayOfMonth.Value - NextStartDate.Day;
                        }
                        else if (NextStartDate.Day > this.DayOfMonth.Value) //already passed day of current month so we can move to next occurrence
                        {
                            var nextDate = NextStartDate.AddMonths(this.RecursEvery).SetDay(this.DayOfMonth.Value);
                            daysToAdd = nextDate.Subtract(NextStartDate).Days;
                        }
                        else
                            daysToAdd = 0;
                    }
                }
                else
                {
                    var nextDate = NextStartDate.AddMonths(this.RecursEvery);

                    if (this.DayOfWeekIndex.HasValue)
                        nextDate = nextDate.GetRelativeDate(this.DayOfWeekIndex.Value, this.DayOfWeekEnum.Value);
                    else if (this.DayOfMonth.HasValue)
                        nextDate = nextDate.SetDay(this.DayOfMonth.Value);

                    daysToAdd = nextDate.Subtract(NextStartDate).Days;
                }
            }
            else if (this.PatternEnum == RecurringPatternEnum.Yearly)
            {
                if (_currOccurrenceNum == 0)
                {
                    if (this.DayOfWeekIndex.HasValue)
                    {
                        var firstocc = NextStartDate.SetMonth((int)this.MonthEnum).GetRelativeDate(this.DayOfWeekIndex.Value, this.DayOfWeekEnum.Value);
                        if (NextStartDate.Year == firstocc.Year && NextStartDate <= firstocc) //still within the current month so just get the day diff to the first occurrence start day
                        {
                            daysToAdd = firstocc.Subtract(NextStartDate).Days;
                        }
                        else
                        {
                            var nextDate = NextStartDate.AddYears(this.RecursEvery).SetMonth((int)this.MonthEnum).GetRelativeDate(this.DayOfWeekIndex.Value, this.DayOfWeekEnum.Value);
                            daysToAdd = nextDate.Subtract(NextStartDate).Days;
                        }
                    }
                    else if (this.DayOfMonth.HasValue)
                    {
                        if (NextStartDate.Month <= (int)this.MonthEnum)
                        {
                            if (NextStartDate.Month == (int)this.MonthEnum && NextStartDate.Day > this.DayOfMonth.Value) //still within the current year and current month so just get the day diff to the first occurrence start day
                            {
                                var nextDate = NextStartDate.AddYears(this.RecursEvery).SetMonth((int)this.MonthEnum).SetDay(this.DayOfMonth.Value);
                                daysToAdd = nextDate.Subtract(NextStartDate).Days;
                            }
                            else
                            {
                                var nextDate = NextStartDate.SetMonth((int)this.MonthEnum).SetDay(this.DayOfMonth.Value);
                                daysToAdd = nextDate.Subtract(NextStartDate).Days;
                            }
                        }
                        else  //already passed day of current year and month so we can move to next occurrence
                        {
                            var nextDate = NextStartDate.AddYears(this.RecursEvery).SetMonth((int)this.MonthEnum).SetDay(this.DayOfMonth.Value);
                            daysToAdd = nextDate.Subtract(NextStartDate).Days;
                        }

                    }
                }
                else
                {
                    var nextDate = NextStartDate.AddYears(this.RecursEvery).SetMonth((int)this.MonthEnum);

                    if (this.DayOfWeekIndex.HasValue)
                        nextDate = nextDate.GetRelativeDate(this.DayOfWeekIndex.Value, this.DayOfWeekEnum.Value);
                    else if (this.DayOfMonth.HasValue)
                        nextDate = nextDate.SetDay(this.DayOfMonth.Value);

                    daysToAdd = nextDate.Subtract(NextStartDate).Days;
                }
            }

            if (EndsOn.HasValue && NextEndDate.AddDays(daysToAdd).Date > EndsOn?.Date) //if (EndsOn.HasValue && NextEndDate.AddDays(daysToAdd).UtcDateTime > EndsOn.Value.UtcDateTime)
            {
                return null;
            }
            else
            {
                _currOccurrenceNum++;
                NextStartDate = NextStartDate.AddDays(daysToAdd);
                NextEndDate = NextEndDate.AddDays(daysToAdd);

                //detect if DST and PST changes
                if (NextStartDate.DateTime.IsDaylightSavingTime() != IsDaylightSavingTime)
                {
                    TimeSpan correctOffset;
                    //check and update offset for DST, increase by 1 hour (or -1:00 from UTC) if going from DST to standard time
                    if (!NextStartDate.DateTime.IsDaylightSavingTime() && IsDaylightSavingTime)
                    {
                        correctOffset = NextStartDate.Offset.Add(TimeSpan.FromHours(-1));
                    }
                    else
                    {
                        correctOffset = NextStartDate.Offset.Add(TimeSpan.FromHours(1));
                    }

                    NextStartDate = new DateTimeOffset(NextStartDate.Ticks, correctOffset);
                    NextEndDate = new DateTimeOffset(NextEndDate.Ticks, correctOffset);
                    IsDaylightSavingTime = !IsDaylightSavingTime;
                }

                return new CalculatedOccurrence
                {
                    StartDate = NextStartDate,
                    EndDate = NextEndDate,
                    OccurrenceNumber = _currOccurrenceNum
                };
            }
        }

        /// <summary>
        /// Gets the first occurrence that has a start date greater than the compareDate
        /// </summary>
        /// <param name="compareDateUtc">The compare date UTC.</param>
        /// <returns>HFC.CRM.Data.CalculatedOccurrence.</returns>
        public CalculatedOccurrence GetFirstOccurrenceGreaterThan(DateTime compareDateUtc)
        {
            CalculatedOccurrence occur = null;

            //reset this number so we can start loop over
            _currOccurrenceNum = 0;
            do
            {
                occur = GetNextOccurrence();
            }
            while (occur != null && occur.StartDate.UtcDateTime < compareDateUtc);

            return occur;
        }

        /// <summary>
        /// Gets the occurrence at.
        /// </summary>
        /// <param name="originalStartDate">The original start date.</param>
        /// <returns>HFC.CRM.Data.CalculatedOccurrence.</returns>
        public CalculatedOccurrence GetOccurrenceAt(DateTimeOffset originalStartDate)
        {
            CalculatedOccurrence occur = null;

            //reset this number so we can start loop over
            _currOccurrenceNum = 0;
            do
            {
                occur = GetNextOccurrence();
            }
            while (occur != null && occur.StartDate != originalStartDate);

            return occur;
        }

        public CalculatedOccurrence checkOccurrencebyDate(DateTime originalStartDate)
        {
            try
            {
                CalculatedOccurrence occur = null;

                //reset this number so we can start loop over
                _currOccurrenceNum = 0;
                do
                {
                    occur = GetNextOccurrence();
                    if (occur != null && occur.StartDate.DateTime == originalStartDate)
                        return occur;
                }
                while (occur != null && occur.StartDate.DateTime <= originalStartDate);

                if (occur != null && occur.StartDate.DateTime == originalStartDate)
                    return occur;
                else
                    return null;
                return occur;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }

}


namespace HFC.CRM.Data.Metadata
{
    /// <summary>
    /// Class CalendarMetadata.
    /// </summary>
    public class CalendarMetadata
    {
        /// <summary>
        /// Gets or sets the calendar unique identifier.
        /// </summary>
        /// <value>The calendar unique identifier.</value>
        [JsonIgnore]
        public Guid CalendarGuid { get; set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        /// <value>The start date.</value>
        [Display(Name = "Start Date")]
        public System.DateTimeOffset StartDate { get; set; }
        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        /// <value>The end date.</value>
        [Display(Name = "End Date")]
        public System.DateTimeOffset EndDate { get; set; }
        /// <summary>
        /// Gets or sets the is deleted.
        /// </summary>
        /// <value>The is deleted.</value>
        [JsonIgnore]
        public bool IsDeleted { get; set; }
    }

    /// <summary>
    /// Class EventRecurringMetadata.
    /// </summary>
    public class EventRecurringMetadata
    {
        /// <summary>
        /// Gets or sets the created on UTC.
        /// </summary>
        /// <value>The created on UTC.</value>
        [JsonIgnore]
        public DateTime CreatedOnUtc { get; set; }
        /// <summary>
        /// Gets or sets the created by person identifier.
        /// </summary>
        /// <value>The created by person identifier.</value>
        [JsonIgnore]
        public int CreatedByPersonId { get; set; }

        /// <summary>
        /// Gets or sets the is deleted.
        /// </summary>
        /// <value>The is deleted.</value>
        [JsonIgnore]
        public bool IsDeleted { get; set; }
        /// <summary>
        /// Gets or sets the calendars.
        /// </summary>
        /// <value>The calendars.</value>
        [JsonIgnore]
        public ICollection<Calendar> Calendars { get; set; }
    }

}