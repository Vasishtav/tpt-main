﻿namespace HFC.CRM.Data
{
    public partial class Role
    {
        /// <summary>
        /// Shoulds the serialize created on UTC.
        /// </summary>
        /// <returns>System.Boolean.</returns>
        public bool ShouldSerializeCreatedOnUtc() { return false; }
        /// <summary>
        /// Shoulds the serialize permissions.
        /// </summary>
        /// <returns>System.Boolean.</returns>
        public bool ShouldSerializePermissions() { return Permissions.Count > 0; }
        /// <summary>
        /// Shoulds the serialize users.
        /// </summary>
        /// <returns>System.Boolean.</returns>
        /// <summary>
        /// Shoulds the serialize is deletable.
        /// </summary>
        /// <returns>System.Boolean.</returns>
        public bool ShouldSerializeIsDeletable() { return false; }

        public bool ShouldSerializeUsers() { return false; }

        public bool IsActive { get; set; }
        public string DisplayName { get; set; }
        public int RoleType { get; set; }
    }  
}