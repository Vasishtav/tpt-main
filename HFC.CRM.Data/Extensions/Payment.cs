﻿using HFC.CRM.Data.Metadata;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [MetadataType(typeof(PaymentMetadata))]
    public partial class Payment
    {

    }
}

namespace HFC.CRM.Data.Metadata
{
    public class PaymentMetadata
    {
        [JsonIgnore]
        public Guid PaymentGuid { get; set; }
        
        [Required]
        public string PaymentMethod { get; set; }

        [Range(0, double.MaxValue, ErrorMessage="Amount must be greater than 0")]
        public decimal Amount { get; set; }

        [Range(0, int.MaxValue, ErrorMessage="Invoice Id must be greater than 0")]
        public int InvoiceId { get; set; }
    }
}