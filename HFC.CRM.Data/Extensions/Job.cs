﻿using HFC.CRM.Data.Metadata;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    /// <summary>
    /// 
    /// </summary>
    [MetadataType(typeof(JobMetadata))]
    public partial class Job
    {
        /// <summary>
        /// Gets or sets cost permission
        /// </summary>
        public Permission CostPermission { get; set; }
        /// <summary>
        /// Gets or sets basic job permission
        /// </summary>
        public Permission Permission { get; set; }
        /// <summary>
        /// Gets or sets permission to re-assigning sales agents on a job
        /// </summary>
        public bool CanDistribute { get; set; }        
        /// <summary>
        /// Gets or sets permission to update job status position
        /// </summary>
        public bool CanUpdateStatus { get; set; }
        /// <summary>
        /// Helper method for Json.Net to only include this property if it's not empty
        /// </summary>
        public bool ShouldSerializeJobQuestionAnswers() { return JobQuestionAnswers != null && JobQuestionAnswers.Count > 0; }
        /// <summary>
        /// Helper method for Json.Net to only include this property if it's not empty
        /// </summary>
        public bool ShouldSerializePhysicalFiles() { return PhysicalFiles != null && PhysicalFiles.Count > 0; }
        /// <summary>
        /// Helper method for Json.Net to only include this property if it's not empty
        /// </summary>
        public bool ShouldSerializeTasks() { return Tasks != null && Tasks.Count > 0; }
        /// <summary>
        /// Helper method for Json.Net to only include this property if it's not empty
        /// </summary>
        public bool ShouldSerializeCalendars() { return Calendars != null && Calendars.Count > 0; }
        /// <summary>
        /// Helper method for Json.Net to only include this property if it's not empty
        /// </summary>
        public bool ShouldSerializeEditHistories() { return EditHistories != null && EditHistories.Count > 0; }

        

    }
    
}

namespace HFC.CRM.Data.Metadata
{
    public class JobMetadata
    {
        [JsonIgnore]
        public Guid JobGuid { get; set; }

        [JsonIgnore]
        public bool IsDeleted { get; set; }
    }
}