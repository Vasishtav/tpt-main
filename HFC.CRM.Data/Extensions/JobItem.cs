﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    /// <summary>
    /// 
    /// </summary>
    public partial class JobItem
    {
        /// <summary>
        /// Creates a clone of the current job item
        /// </summary>
        /// <param name="exactCopy">Default is true.  If false, then identities will be set to 0 or null and CreatedOnUtc and LastUpdated set to DateTime.Now/param>
        /// <returns></returns>
        public JobItem Clone(bool exactCopy = true)
        {
            JobItem clone = this.Copy();

            if(!exactCopy)
            {
                clone.JobItemId = 0;
                clone.QuoteId = 0;
                clone.LastUpdated = clone.CreatedOnUtc = DateTime.Now;
                if (clone.Options != null && clone.Options.Count > 0)
                {
                    foreach (var opt in clone.Options)
                    {
                        opt.OptionId = 0;
                        opt.JobItemId = 0;
                    }
                }
            }

            return clone;
        }

        /// <summary>
        /// Concatenate product category, type and name 
        /// </summary>
        public override string ToString()
        {
            return ToString(false);
        }

        /// <summary>
        /// Concatenate product category, type and name; if complete is true then qty, and sale price is added
        /// </summary>
        public string ToString(bool complete)
        {
            List<string> arr = new List<string>();
            if (!string.IsNullOrEmpty(this.CategoryName))
                arr.Add(this.CategoryName);
            if (!string.IsNullOrEmpty(this.ProductType))
                arr.Add(this.ProductType);
            if (!string.IsNullOrEmpty(this.ProductName))
                arr.Add(this.ProductName);

            if (complete)
            {
                arr.Add("Qty: " + this.Quantity);
                arr.Add("Sale Price: " + this.SalePrice);
            }
            return string.Join(", ", arr);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public partial class JobItemOption
    {
        /// <summary>
        /// Gets or set option display order sequence
        /// </summary>
        [Newtonsoft.Json.JsonIgnore]
        public int DisplayOrder { get; set; }
        /// <summary>
        /// Gets or set if this option is a list type
        /// </summary>
        public bool IsList { get; set; }
        /// <summary>
        /// Gets or set read only
        /// </summary>
        public bool IsReadOnly { get; set; }
        /// <summary>
        /// Gets or set minimum value
        /// </summary>
        public decimal? MinValue { get; set; }
        /// <summary>
        /// Gets or set maximum value
        /// </summary>
        public decimal? MaxValue { get; set; }
        /// <summary>
        /// Gets or set if this option affects pricing if changed
        /// </summary>
        public bool AffectsPrice { get; set; }
        /// <summary>
        /// Gets or set list of available options to choose from
        /// </summary>
        public List<string> AvailableOptions { get; set; }
        /// <summary>
        /// Gets or set the default value
        /// </summary>
        public string DefaultValue { get; set; }
    }

}
