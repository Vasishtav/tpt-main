﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HFC.CRM.Data
{
    public partial class Territory
    {
        public bool ShouldSerializeFranchise() { return false; }
        public bool ShouldSerializeJobs() { return false; }
        public bool ShouldSerializeLeads() { return false; }
        public bool ShouldSerializeZipCodes() { return false; }
    }
    
}

