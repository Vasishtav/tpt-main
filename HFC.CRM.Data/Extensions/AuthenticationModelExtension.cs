﻿using System;
using System.Linq;
using System.Web.Security;

namespace HFC.CRM.Data
{

    /// <summary>
    /// Class User.
    /// </summary>
    

    /// <summary>
    /// Class Type_Color.
    /// </summary>
    public partial class Type_Color
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Type_Color"/> class.
        /// </summary>
        /// <param name="generateRandom">The generate random.</param>
        public Type_Color(bool generateRandom)
        {
            if (generateRandom)
            {
                Random rand = new Random((int)DateTime.Now.Ticks);
                Red = (byte)rand.Next(0, 255);
                Green = (byte)rand.Next(0, 255);
                Blue = (byte)rand.Next(0, 255);
            }
        }        
    }

}
