﻿using HFC.CRM.Cache.Cache;

namespace HFC.CRM.Data
{
    public partial class Type_Royalty
    {
        public bool ShouldSerializeRoyaltyTemplates() { return false; }
        public bool ShouldSerializeFranchiseRoyalties() { return false; }
    }
    
}

