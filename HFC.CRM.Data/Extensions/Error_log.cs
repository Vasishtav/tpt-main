﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    public partial class Log_Error
    {

        public override string ToString()
        {
            return CreatedOnUtc + "<br/>" + Obj + "<br/>" +
                   Message + "<br/>" + Source + "<br/>" + Trace;
        }
    }
}
