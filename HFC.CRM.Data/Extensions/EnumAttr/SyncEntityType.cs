﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data.Extensions.EnumAttr
{

    public enum SyncEntityType
    {
        None,
        Account,
        Customer,
        Vendor,
        ItemNonInventory,
        ItemSalesTax,
        Invoice,
        Payment,
        PayMethod,
        Bill,
        SalesTaxReturnLine,
        SalesTaxCode
    }
}
