﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//namespace HFC.CRM.Data.Extensions
//changed namespace to be in data, since this will likely be more available
namespace HFC.CRM.Data
{
    /// <summary>
    /// Class DateTimeExtension.
    /// </summary>
    public static class DateTimeExtension
    {
        /// <summary>
        /// Sets the day for this date, if the day exceeds the number of days in the month, then it will be set to the last day of the month
        /// </summary>
        /// <param name="dateToSet">The date to set.</param>
        /// <param name="dayOfMonth">Day to set</param>
        /// <returns>System.DateTime.</returns>
        public static DateTime SetDay(this DateTime dateToSet, int dayOfMonth)
        {
            int daysInMonth = DateTime.DaysInMonth(dateToSet.Year, dateToSet.Month);
            if (dayOfMonth > daysInMonth)
                dayOfMonth = daysInMonth;
            return dateToSet.AddDays(dayOfMonth - dateToSet.Day);
        }

        /// <summary>
        /// Sets the day for this date, if the day exceeds the number of days in the month, then it will be set to the last day of the month
        /// </summary>
        /// <param name="dateToSet">The date to set.</param>
        /// <param name="dayOfMonth">Day to set</param>
        /// <returns>System.DateTimeOffset.</returns>
        public static DateTimeOffset SetDay(this DateTimeOffset dateToSet, int dayOfMonth)
        {
            int daysInMonth = DateTime.DaysInMonth(dateToSet.Year, dateToSet.Month);
            if (dayOfMonth > daysInMonth)
                dayOfMonth = daysInMonth;
            return dateToSet.AddDays(dayOfMonth - dateToSet.Day);
        }

        public static DateTimeOffset SetMonth(this DateTimeOffset dateToSet, int Month)
        {
            return dateToSet.AddMonths(Month - dateToSet.Month);
        }

        /// <summary>
        /// Gets the start date of current week for this date
        /// </summary>
        /// <param name="dt">The dt.</param>
        /// <param name="startOfWeek">Sunday is the default start of a week</param>
        /// <returns>System.DateTime.</returns>
        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek = DayOfWeek.Sunday)
        {
            int diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
                diff += 7;

            return dt.AddDays(-1 * diff);
        }

        /// <summary>
        /// Gets the start date of current week for this date
        /// </summary>
        /// <param name="dt">The dt.</param>
        /// <param name="startOfWeek">Sunday is the default start of a week</param>
        /// <returns>System.DateTimeOffset.</returns>
        public static DateTimeOffset StartOfWeek(this DateTimeOffset dt, DayOfWeek startOfWeek = DayOfWeek.Sunday)
        {
            int diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
                diff += 7;

            return dt.AddDays(-1 * diff);
        }

        /// <summary>
        /// Gets the relative date for this date's month
        /// </summary>
        /// <param name="aDate">A date.</param>
        /// <param name="index">The position of the day of week, possible values are First, Second, Third, Fourth, or Last</param>
        /// <param name="dayOfWeek">Day of the week, Sunday to Saturday</param>
        /// <returns>System.DateTime.</returns>
        public static DateTime GetRelativeDate(this DateTime aDate, DayOfWeekIndexEnum index, DayOfWeekEnum dayOfWeek)
        {
            DateTime _1stDayOfMonth = aDate.SetDay(1);

            int dayOfWeekAsInt = (int)(Math.Log((byte)dayOfWeek) / Math.Log(2));
            int deltaDayOfWeek = 0;

            if (index == DayOfWeekIndexEnum.Last)
            {
                //start from the last day of the week and move backward until we hit the selected day of week
                //example: 10/31/2013 is last day of month which is Thursday
                //and selected DoW is Tuesday
                //then deltaDayOfWeek = dayOfWeekEnumAsInt - (int)lastDayOfMonth.DayOfWeek = 2 - 4 = -2;
                //moves the day back by 2 for 10/29/2013 which is last Tuesday of month

                //example: 10/31/2013 is last day and selected DoW is Saturday
                //then deltaDayOfWeek = dayOfWeekEnumAsInt - (int)lastDayOfMonth.DayOfWeek = (6 - 4) - 7 = 2 - 7 = -5;
                //moves the day back by 5 for 10/26/2013 which is last Saturday of month

                DateTime lastDayOfMonth = _1stDayOfMonth.AddMonths(1).AddDays(-1);

                deltaDayOfWeek = dayOfWeekAsInt - (int)lastDayOfMonth.DayOfWeek;
                if (deltaDayOfWeek > 0)
                    deltaDayOfWeek = deltaDayOfWeek - 7;
                return lastDayOfMonth.AddDays(deltaDayOfWeek);
            }
            else
            {
                //selected day of week index minus 1 then multiply by 7 to get the selected index + number of days to get the index date.
                //example: 10/1/2013 starts on tuesday and we want the first sunday of the month.
                //so numOfDaysTo1stDoWIndexFrom1stDayOfMonth = dayOfWeekEnumAsInt - (int)_1stDayOfMonth.DayOfWeek = 0 - 2 = -2 + 7 = 5;
                //then 7 * (recurring.DayOfWeekIndex - 1) = 7 * (1 - 1) = 7 * 0 = 0; so theres no additional day to increment
                // then + numOfDaysTo1stDoWIndexFrom1stDayOfMonth = 0 + 5 = 5;
                // so the resulting date is 10/1/2013 + 5 days = 10/6/2013 = 1st Sunday of the month

                //example 2: 10/1/2013 starts on tuesday and we want the third sunday of the month.
                //so numOfDaysTo1stDoWIndexFrom1stDayOfMonth = 0 - 2 = -2 + 7 = 5;
                //then 7 * (recurring.DayOfWeekIndex - 1) = 7 * (3 - 1) = 7 * 2 = 14; so theres 14 additional days to increment
                // then + numOfDaysTo1stDoWIndexFrom1stDayOfMonth = 14 + 5 = 19;
                // so the resulting date is 10/1/2013 + 19 days = 10/20/2013 = 3rd Sunday of the month

                deltaDayOfWeek = dayOfWeekAsInt - (int)_1stDayOfMonth.DayOfWeek;
                //negative, so the 1st day of month day of week is ahead of the selected day of week so add 7 to get the next 1st day of week for the month
                if (deltaDayOfWeek < 0)
                    deltaDayOfWeek += 7;

                return _1stDayOfMonth.AddDays((7 * ((byte)index - 1)) + deltaDayOfWeek);
            }
        }

        /// <summary>
        /// Gets the relative date for this date's month
        /// </summary>
        /// <param name="aDate">A date.</param>
        /// <param name="index">The position of the day of week, possible values are First, Second, Third, Fourth, or Last</param>
        /// <param name="dayOfWeek">Day of the week, Sunday to Saturday</param>
        /// <returns>System.DateTimeOffset.</returns>
        public static DateTimeOffset GetRelativeDate(this DateTimeOffset aDate, DayOfWeekIndexEnum index, DayOfWeekEnum dayOfWeek)
        {
            DateTimeOffset _1stDayOfMonth = aDate.SetDay(1);

            int dayOfWeekAsInt = (int)(Math.Log((byte)dayOfWeek) / Math.Log(2));
            int deltaDayOfWeek = 0;

            if (index == DayOfWeekIndexEnum.Last)
            {
                //start from the last day of the week and move backward until we hit the selected day of week
                //example: 10/31/2013 is last day of month which is Thursday
                //and selected DoW is Tuesday
                //then deltaDayOfWeek = dayOfWeekEnumAsInt - (int)lastDayOfMonth.DayOfWeek = 2 - 4 = -2;
                //moves the day back by 2 for 10/29/2013 which is last Tuesday of month

                //example: 10/31/2013 is last day and selected DoW is Saturday
                //then deltaDayOfWeek = dayOfWeekEnumAsInt - (int)lastDayOfMonth.DayOfWeek = (6 - 4) - 7 = 2 - 7 = -5;
                //moves the day back by 5 for 10/26/2013 which is last Saturday of month

                DateTimeOffset lastDayOfMonth = _1stDayOfMonth.AddMonths(1).AddDays(-1);

                deltaDayOfWeek = dayOfWeekAsInt - (int)lastDayOfMonth.DayOfWeek;
                if (deltaDayOfWeek > 0)
                    deltaDayOfWeek = deltaDayOfWeek - 7;
                return lastDayOfMonth.AddDays(deltaDayOfWeek);
            }
            else
            {
                //selected day of week index minus 1 then multiply by 7 to get the selected index + number of days to get the index date.
                //example: 10/1/2013 starts on tuesday and we want the first sunday of the month.
                //so numOfDaysTo1stDoWIndexFrom1stDayOfMonth = dayOfWeekEnumAsInt - (int)_1stDayOfMonth.DayOfWeek = 0 - 2 = -2 + 7 = 5;
                //then 7 * (recurring.DayOfWeekIndex - 1) = 7 * (1 - 1) = 7 * 0 = 0; so theres no additional day to increment
                // then + numOfDaysTo1stDoWIndexFrom1stDayOfMonth = 0 + 5 = 5;
                // so the resulting date is 10/1/2013 + 5 days = 10/6/2013 = 1st Sunday of the month

                //example 2: 10/1/2013 starts on tuesday and we want the third sunday of the month.
                //so numOfDaysTo1stDoWIndexFrom1stDayOfMonth = 0 - 2 = -2 + 7 = 5;
                //then 7 * (recurring.DayOfWeekIndex - 1) = 7 * (3 - 1) = 7 * 2 = 14; so theres 14 additional days to increment
                // then + numOfDaysTo1stDoWIndexFrom1stDayOfMonth = 14 + 5 = 19;
                // so the resulting date is 10/1/2013 + 19 days = 10/20/2013 = 3rd Sunday of the month

                deltaDayOfWeek = dayOfWeekAsInt - (int)_1stDayOfMonth.DayOfWeek;
                //negative, so the 1st day of month day of week is ahead of the selected day of week so add 7 to get the next 1st day of week for the month
                if (deltaDayOfWeek < 0)
                    deltaDayOfWeek += 7;

                return _1stDayOfMonth.AddDays((7 * ((byte)index - 1)) + deltaDayOfWeek);
            }
        }

        /// <summary>
        /// Gets the index of the do w.
        /// </summary>
        /// <param name="aDate">A date.</param>
        /// <returns>HFC.CRM.Data.DayOfWeekIndexEnum.</returns>
        public static DayOfWeekIndexEnum GetDoWIndex(this DateTimeOffset aDate)
        {
            var startOfWeek = aDate.StartOfWeek();
            var firstWeek = aDate.SetDay(1).StartOfWeek();
            var index = (firstWeek.Subtract(startOfWeek).Days / 7);
            if (firstWeek.AddDays(1).Day == 1)
                index++;

            return (DayOfWeekIndexEnum)index;
        }

        /// <summary>
        /// Gets the next selected day of week date that is greater than this date
        /// </summary>
        /// <param name="startsOn">The starts on.</param>
        /// <param name="selectedDayOfWeeks">List of selected day of weeks, example: Monday | Wednesday | Friday</param>
        /// <param name="recursEvery">Number of weeks to increment for the next available week</param>
        /// <returns>System.DateTime.</returns>
        public static DateTime GetNextDayOfWeekDate(this DateTime startsOn, List<DayOfWeekEnum> selectedDayOfWeeks, int recursEvery)
        {
            if (selectedDayOfWeeks == null || selectedDayOfWeeks.Count == 0)
                throw new ArgumentNullException("selectedDayOfWeeks", "selectedDayOfWeeks can not be empty");

            DateTime startOfWeekOfStartsOn = startsOn.StartOfWeek();

            DayOfWeekEnum startsOnDoWConvertedToEnum = (DayOfWeekEnum)Math.Pow(2, (int)startsOn.DayOfWeek);

            //get the first selected DoW that is on the same or greater day of week as starts on day of week;
            //example: startson 10/1/2013 (Tuesday) and selectedDayOfWeeks is Monday | Wednesday
            //then the next selected is Wednesday
            DayOfWeekEnum? nextSelectedDoW = selectedDayOfWeeks.FirstOrDefault(f => f > startsOnDoWConvertedToEnum);
            //can also use this method to pick lowest value Enum: (DayOfWeekEnum)((byte)allSelectedDoWs & -(byte)allSelectedDoWs); //this will pick first out of multiple bitwise values like Sunday | Monday | Tuesday will return Sunday            

            if (nextSelectedDoW.HasValue && (int)nextSelectedDoW > 0)
            {
                int nextSelectedDoWAsInt = (int)(Math.Log((byte)nextSelectedDoW.Value) / Math.Log(2));
                return startOfWeekOfStartsOn.AddDays(nextSelectedDoWAsInt);
            }
            else //there is no more selected DoW for the current week, so we need to move to next recurring week + the first selected DoW
            {
                int firstSelectedDoW = (int)(Math.Log((byte)selectedDayOfWeeks.First()) / Math.Log(2));
                return startOfWeekOfStartsOn.AddDays((7 * recursEvery) + firstSelectedDoW);
            }
        }

        /// <summary>
        /// Gets the next selected day of week date that is greater than this date
        /// </summary>
        /// <param name="startsOn">The starts on.</param>
        /// <param name="selectedDayOfWeeks">List of selected day of weeks, example: Monday | Wednesday | Friday</param>
        /// <param name="recursEvery">Number of weeks to increment for the next available week</param>
        /// <returns>System.DateTimeOffset.</returns>
        public static DateTimeOffset GetNextDayOfWeekDate(this DateTimeOffset startsOn, List<DayOfWeekEnum> selectedDayOfWeeks, int recursEvery)
        {
            if (selectedDayOfWeeks == null || selectedDayOfWeeks.Count == 0)
                throw new ArgumentNullException("selectedDayOfWeeks", "selectedDayOfWeeks can not be empty");

            DateTimeOffset startOfWeekOfStartsOn = startsOn.StartOfWeek();

            DayOfWeekEnum startsOnDoWConvertedToEnum = (DayOfWeekEnum)Math.Pow(2, (int)startsOn.DayOfWeek);

            //get the first selected DoW that is on the same or greater day of week as starts on day of week;
            //example: startson 10/1/2013 (Tuesday) and selectedDayOfWeeks is Monday | Wednesday
            //then the next selected is Wednesday
            DayOfWeekEnum? nextSelectedDoW = selectedDayOfWeeks.FirstOrDefault(f => f > startsOnDoWConvertedToEnum);
            //can also use this method to pick lowest value Enum: (DayOfWeekEnum)((byte)allSelectedDoWs & -(byte)allSelectedDoWs); //this will pick first out of multiple bitwise values like Sunday | Monday | Tuesday will return Sunday            

            if (nextSelectedDoW.HasValue && (int)nextSelectedDoW > 0)
            {
                int nextSelectedDoWAsInt = (int)(Math.Log((byte)nextSelectedDoW.Value) / Math.Log(2));
                return startOfWeekOfStartsOn.AddDays(nextSelectedDoWAsInt);
            }
            else //there is no more selected DoW for the current week, so we need to move to next recurring week + the first selected DoW
            {
                int firstSelectedDoW = (int)(Math.Log((byte)selectedDayOfWeeks.First()) / Math.Log(2));
                return startOfWeekOfStartsOn.AddDays((7 * recursEvery) + firstSelectedDoW);
            }
        }

        /// <summary>
        /// Returns a string representation of the Date in ISO 8601 standard
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns>System.String.</returns>
        public static string ToISOString(this DateTime? date)
        {
            return ToPatternString(date, "o");
        }

        /// <summary>
        /// Returns a string representation of the Date in ISO 8601 standard
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns>System.String.</returns>
        public static string ToISOString(this DateTime date)
        {
            return ToPatternString(date, "o");
        }

        /// <summary>
        /// To the pattern string.
        /// </summary>
        /// <param name="utcDate">The UTC date.</param>
        /// <param name="pattern">pattern of o for ISO 8601 standard</param>
        /// <returns>System.String.</returns>
        private static string ToPatternString(DateTime? utcDate, string pattern)
        {
            if (utcDate.HasValue)
                return utcDate.Value.ToString(pattern);
            else
                return "";
        }

        /// <summary>
        /// Returns a string representation of the Date in ISO 8601 standard
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns>System.String.</returns>
        public static string ToISOString(this DateTimeOffset? date)
        {
            return ToPatternString(date, "o");
        }

        /// <summary>
        /// Returns a string representation of the Date in ISO 8601 standard
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns>System.String.</returns>
        public static string ToISOString(this DateTimeOffset date)
        {
            return ToPatternString(date, "o");
        }

        /// <summary>
        /// To the pattern string.
        /// </summary>
        /// <param name="utcDate">The UTC date.</param>
        /// <param name="pattern">pattern of o for ISO 8601 standard</param>
        /// <returns>System.String.</returns>
        private static string ToPatternString(DateTimeOffset? utcDate, string pattern)
        {
            if (utcDate.HasValue)
                return utcDate.Value.ToString(pattern);
            else
                return "";
        }
    }
}
