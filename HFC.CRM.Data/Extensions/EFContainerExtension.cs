﻿using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Core.EntityClient;
using System.Web;

namespace HFC.CRM.Data.Extensions
{
    public partial class EFContainerExtension
    {
        private static Dictionary<string, string> _connectionStringCache = new Dictionary<string,string>();

        /// <summary>
        /// Generates entity framework connection string for EDMX (Model/Database first) designer
        /// It will append the metadata resources required to map the entities
        /// </summary>
        /// <param name="conStrName">Connection string in web.config</param>
        /// <param name="modelPathName">Path to the assembly with your edmx model name</param>
        /// <returns></returns>
        public static string GetConnectionString(string conStrName, string modelPathName = null, string assemblyName = null)
        {
            string _cnCache = "";
            if(_connectionStringCache.ContainsKey(conStrName))
                _cnCache = _connectionStringCache[conStrName];
            if (!string.IsNullOrEmpty(_cnCache))
                return _cnCache;

            if (ConfigurationManager.ConnectionStrings[conStrName] == null)
                throw new SettingsPropertyNotFoundException("Connection string '"+conStrName+"' not found");

            if (string.IsNullOrEmpty(modelPathName))
                modelPathName = conStrName;

            string baseConnectionString = ConfigurationManager.ConnectionStrings[conStrName].ConnectionString;

            EntityConnectionStringBuilder entityBuilder = new EntityConnectionStringBuilder();
            entityBuilder.Provider = "System.Data.SqlClient";        
    
            if(string.IsNullOrEmpty(assemblyName))
                entityBuilder.Metadata = string.Format(@"res://*/{0}.csdl|res://*/{0}.ssdl|res://*/{0}.msl", modelPathName);
            else
                entityBuilder.Metadata = string.Format(@"res://{1}/{0}.csdl|res://{1}/{0}.ssdl|res://{1}/{0}.msl", modelPathName, assemblyName);

            if (!baseConnectionString.ToLower().Contains("multipleactiveresultsets"))
                baseConnectionString += ";MultipleActiveResultSets=True";

            entityBuilder.ProviderConnectionString = baseConnectionString;

            _cnCache = entityBuilder.ToString();
            _connectionStringCache.Add(conStrName, _cnCache);
            return _cnCache;
        }
    }
}
