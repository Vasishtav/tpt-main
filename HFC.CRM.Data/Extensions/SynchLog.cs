﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Services.Description;
using HFC.CRM.Data.Extensions.EnumAttr;


namespace HFC.CRM.Data
{
    public partial class SynchLog
    {
        public override string ToString()
        {
            return SynchOn + " " + Enum.GetName(typeof(SyncEntityType), ObjectType) + " " +
                (Message == null ?
                "Success" :
                Message)
                + String.Format(" ({0}, {1}, {2})", ObjectID, ObjectRef, QbId);

        }
    }
}
