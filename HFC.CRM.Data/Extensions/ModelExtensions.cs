﻿using HFC.CRM.Data.Metadata;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace HFC.CRM.Data
{
    //Uncomment if we want to start using scalar functions
    //public partial class CRMContext
    //{
    //    [DbFunctionAttribute("HFC.CRM.Data.Store", "fnsHasDuplicateEmail")]
    //    public static bool fnsHasDuplicateEmail(int? FranchiseId, string PrimEmail, string SecEmail)
    //    {
    //        throw new Exception("Only works in Linq to Entities");
    //    }
    //}

    /// <summary>
    /// Class Type_LeadStatus.
    /// </summary>
    [MetadataType(typeof(Type_LeadStatusMetadata))]
    public partial class Type_LeadStatus 
    {
        /// <summary>
        /// Returns "Parent - Child" status if there is a parent status
        /// </summary>
        /// <returns>System.String.</returns>
        public override string ToString()
        {
            if (Parent == null)
            {
                return Name;
            }
            else
            {
                return string.Format("{0} - {1}", Parent.Name, Name);
            }
        }
    }

    [MetadataType(typeof(PermissionMetadata))]
    public partial class Permission
    {
        
    }

    /// <summary>
    /// Class Type_JobStatus.
    /// </summary>
    [MetadataType(typeof(Type_JobStatusMetadata))]
    public partial class Type_JobStatus
    {
        /// <summary>
        /// Returns "Parent - Child" status if there is a parent status
        /// </summary>
        /// <returns>System.String.</returns>
        public override string ToString()
        {
            if (Parent == null)
            {
                return Name;
            }
            else
            {
                return string.Format("{0} - {1}", Parent.Name, Name);
            }
        }
    }

    /// <summary>
    /// Class Type_Source.
    /// </summary>
    [MetadataType(typeof(Type_SourceMetadata))]
    public partial class Type_Source 
    {
        /// <summary>
        /// Shoulds the serialize marketing campaigns.
        /// </summary>
        /// <returns>System.Boolean.</returns>
        public bool ShouldSerializeMarketingCampaigns() { return MarketingCampaigns != null && MarketingCampaigns.Count > 0; }

        /// <summary>
        /// Returns "Parent - Child" source if there is a parent source
        /// </summary>
        /// <returns>System.String.</returns>
        public override string ToString()
        {
            if (Parent == null)
            {
                return Name ?? string.Empty;
            }
            else
            {
                return string.Format("{0} - {1}", Parent.Name ?? string.Empty, Name ?? string.Empty);
            }
        }
    }

    /// <summary>
    /// Class Type_State.
    /// </summary>
    [MetadataType(typeof(Type_StateMetadata))]
    public partial class Type_State
    {
        private string _countryISO2;
        /// <summary>
        /// Gets or sets country ISO 2 digit format
        /// </summary>
        public string CountryISO2
        {
            get
            {
                if (Type_Country != null)
                    return Type_Country.ISOCode2Digits;
                else
                    return _countryISO2;
            }
            set { _countryISO2 = value; }
        }
    }

    /// <summary>
    /// Class LeadSource.
    /// </summary>
    [MetadataType(typeof(LeadSourceMetadata))]
    public partial class LeadSource {}

    /// <summary>
    /// Class JobNote.
    /// </summary>
    public partial class JobNote
    {
        /// <summary>
        /// Gets or sets the full name of the creator.
        /// </summary>
        /// <value>The full name of the creator.</value>
        public string CreatorFullName { get; set; }

        /// <summary>
        /// Gets or set the avatar image source of a user per note
        /// </summary>
        public string AvatarSrc { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public partial class LeadNote
    {
        /// <summary>
        /// Gets or sets the full name of the creator.
        /// </summary>
        /// <value>The full name of the creator.</value>
        public string CreatorFullName { get; set; }
                
        /// <summary>
        /// Gets or set the avatar image source of a user per note
        /// </summary>
        public string AvatarSrc { get;  set; }
    }

    /// <summary>
    /// Class Task.
    /// </summary>
    [MetadataType(typeof(TaskMetadata))]
    public partial class Task
    {
        /// <summary>
        /// Gets or sets the is editable.
        /// </summary>
        /// <value>The is editable.</value>
        public bool IsEditable { get; set; }
        /// <summary>
        /// Gets or sets the is deletable.
        /// </summary>
        /// <value>The is deletable.</value>
        public bool IsDeletable { get; set; }
        /// <summary>
        /// Gets or sets the name of the color class.
        /// </summary>
        /// <value>The name of the color class.</value>
        public string ColorClassName { get; set; }
    }

    /// <summary>
    /// Class Lead.
    /// </summary>
    [MetadataType(typeof(LeadMetadata))]
    public partial class Lead
    {
        /// <summary>
        /// Gets or sets the can delete.
        /// </summary>
        /// <value>The can delete.</value>

        [NotMapped]
        public bool CanDelete { get; set; }
        /// <summary>
        /// Gets or sets the can update.
        /// </summary>
        /// <value>The can update.</value>
        [NotMapped]
        public bool CanUpdate { get; set; }
        /// <summary>
        /// Gets or sets the can update status.
        /// </summary>
        /// <value>The can update status.</value>
        [NotMapped]
        public bool CanUpdateStatus { get; set; }
        /// <summary>
        /// Campaign Name to store the value of Campaign for display purpose
        /// </summary>
        [NotMapped]
        public string CampaignName { get; set; }
        [NotMapped]
        public bool SkipDuplicateCheck { get; set; }
        /// <summary>
        /// Shoulds the serialize calendars.
        /// </summary>
        /// <returns>System.Boolean.</returns>
        public bool ShouldSerializeCalendars() { return Calendars != null && Calendars.Count > 0; }
        /// <summary>
        /// Shoulds the serialize tasks.
        /// </summary>
        /// <returns>System.Boolean.</returns>
        public bool ShouldSerializeTasks() { return Tasks != null && Tasks.Count > 0; }
        /// <summary>
        /// Shoulds the serialize lead sources.
        /// </summary>
        /// <returns>System.Boolean.</returns>
        public bool ShouldSerializeLeadSources() { return LeadSources != null && LeadSources.Count > 0; }
        /// <summary>
        /// Shoulds the serialize addresses.
        /// </summary>
        /// <returns>System.Boolean.</returns>
        public bool ShouldSerializeAddresses() { return Addresses != null && Addresses.Count > 0; }
        /// <summary>
        /// Shoulds the serialize jobs.
        /// </summary>
        /// <returns>System.Boolean.</returns>
        public bool ShouldSerializeJobs() { return Jobs != null && Jobs.Count > 0; }
        /// <summary>
        /// Shoulds the serialize lead notes.
        /// </summary>
        /// <returns>System.Boolean.</returns>
        public bool ShouldSerializeLeadNotes() { return LeadNotes != null && LeadNotes.Count > 0; }
    }
    
    /// <summary>
    /// Class Person.
    /// </summary>
    [MetadataType(typeof(PersonMetadata))]
    public partial class Person
    {
        /// <summary>
        /// Gets the full name.
        /// </summary>
        /// <value>The full name.</value>
        [NotMapped]
        public string FullName 
        { 
            get 
            { 
                return string.Join(" ", new string[] { FirstName, LastName }).Trim(); 
            } 
        }
    }
    ///<summary>
    ///Class CustomerTP
    /// </summary>
    [MetadataType(typeof(CustomerTPMetadata))]
    public partial class CustomerTP
    {
        /// <summary>
        /// Gets the full name.
        /// </summary>
        /// <value>The full name.</value>
        [NotMapped]
        public string FullName
        {
            get
            {
                return string.Join(" ", new string[] { FirstName, LastName }).Trim();
            }
        }
    }


    /// <summary>
    /// Class Address.
    /// </summary>
    [MetadataType(typeof(AddressMetadata))]
    public partial class Address
    {
        [NotMapped]
        public string AddressType {
            get
            {
                return this.IsResidential ? "true" : "false";
            }
            set
            {
                this.IsResidential = value.Equals("true") ? true : false;
            }
        }

        /// <summary>
        /// Concatenate all address information into one line
        /// </summary>
        /// <returns>System.String.</returns>
        /// 
        public override string ToString()
        {
            return ToString(false);
        }

        /// <summary>
        /// Concatenate all address information into multiple lines
        /// </summary>
        /// <param name="isMultiline">The is multiline.</param>
        /// <param name="justCityStateZip">The just city state zip.</param>
        /// <param name="inHtml">&lt;br&gt; used as new lines if true</param>
        /// <returns>System.String.</returns>
        public string ToString(bool isMultiline, bool justCityStateZip = false, bool inHtml = false)
        {
            List<string> concatList = new List<string>();

            if (justCityStateZip)
            {
                if (!string.IsNullOrEmpty(City))
                    concatList.Add(City);

                if (!string.IsNullOrEmpty(State) && !string.IsNullOrEmpty(ZipCode))
                    concatList.Add(string.Format("{0} {1}", State, ZipCode));
                else if (!string.IsNullOrEmpty(State))
                    concatList.Add(State);
                else if (!string.IsNullOrEmpty(ZipCode))
                    concatList.Add(ZipCode);

                return string.Join(", ", concatList);
            }

            if (!string.IsNullOrEmpty(Address1))
                concatList.Add(Address1);
            if (!string.IsNullOrEmpty(Address2))
                concatList.Add(Address2);
            
            if (isMultiline)
            {
                string loc = "";
                if (!string.IsNullOrEmpty(City))
                    loc = City;

                if (!string.IsNullOrEmpty(State) && !string.IsNullOrEmpty(ZipCode))
                    loc += string.Format("{0}{1} {2}", loc.Length == 0 ? "" : ", ", State, ZipCode);
                else if (!string.IsNullOrEmpty(State))
                    loc += string.Format("{0}{1}", loc.Length == 0 ? "" : ", ", State);
                else if (!string.IsNullOrEmpty(ZipCode))
                    loc += string.Format("{0}{1}", loc.Length == 0 ? "" : ", ", ZipCode);

                 if (!string.IsNullOrEmpty(loc))
                    concatList.Add(loc);

                 if (inHtml)
                     return string.Join("<br/>", concatList);
                 else
                    return string.Join("\n", concatList);
            }
            else
            {
                if (!string.IsNullOrEmpty(City))
                    concatList.Add(City);

                if (!string.IsNullOrEmpty(State) && !string.IsNullOrEmpty(ZipCode))
                    concatList.Add(string.Format("{0} {1}", State, ZipCode));
                else if (!string.IsNullOrEmpty(State))
                    concatList.Add(State);
                else if (!string.IsNullOrEmpty(ZipCode))
                    concatList.Add(ZipCode);

                return string.Join(", ", concatList);
            }
        }

        /// <summary>
        /// Equalses the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns>System.Boolean.</returns>
        public override bool Equals(object obj)
        {
            var compareAddress = obj as Address;
            var sameAddress1 = (string.IsNullOrEmpty(this.Address1) && string.IsNullOrEmpty(compareAddress.Address1)) || 
                                string.Equals(this.Address1, compareAddress.Address1, StringComparison.InvariantCultureIgnoreCase);
            var sameAddress2 = (string.IsNullOrEmpty(this.Address2) && string.IsNullOrEmpty(compareAddress.Address2)) ||
                                string.Equals(this.Address2, compareAddress.Address2, StringComparison.InvariantCultureIgnoreCase);
            var sameCity = (string.IsNullOrEmpty(this.City) && string.IsNullOrEmpty(compareAddress.City)) ||
                               string.Equals(this.City, compareAddress.City, StringComparison.InvariantCultureIgnoreCase);
            var sameState = (string.IsNullOrEmpty(this.State) && string.IsNullOrEmpty(compareAddress.State)) ||
                               string.Equals(this.State, compareAddress.State, StringComparison.InvariantCultureIgnoreCase);
            var sameZip = (string.IsNullOrEmpty(this.ZipCode) && string.IsNullOrEmpty(compareAddress.ZipCode)) ||
                               string.Equals(this.ZipCode, compareAddress.ZipCode, StringComparison.InvariantCultureIgnoreCase);

            return sameAddress1 && sameAddress2 && sameCity && sameState && sameZip;
        }

        /// <summary>
        /// Gets the hash code.
        /// </summary>
        /// <returns>System.Int32.</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    /// <summary>
    /// Class Product.
    /// </summary>
    public partial class Product
    {
        public decimal UnitCost { get; set; }
    }
    
    /// <summary>
    /// Class Type_Color.
    /// </summary>
    public partial class Type_Color
    {
        /// <summary>
        /// Format to #0000FF hex string
        /// </summary>
        /// <returns>System.String.</returns>
        public string BGColorToHex()
        {
            return ((this.Red << 16) | (this.Green << 8) | (this.Blue << 0)).ToString("X2");
        }

        /// <summary>
        /// Format to #0000FF hex string
        /// </summary>
        /// <returns>System.String.</returns>
        public string FGColorToHex()
        {
            return ((this.FGRed << 16) | (this.FGGreen << 8) | (this.FGBlue << 0)).ToString("X2");
        }

        /// <summary>
        /// Format to rgb(r,g,b) or rgba if alpha is less than 1
        /// </summary>
        /// <returns>System.String.</returns>
        public string BGColorToRGB()
        {
            if(this.Alpha < 1)
                return string.Format("rgba({0},{1},{2},{3})", this.Red, this.Green, this.Blue, this.Alpha);
            else
                return string.Format("rgb({0},{1},{2})", this.Red, this.Green, this.Blue);
        }

        /// <summary>
        /// Format to rgb(r,g,b) or rgba if alpha is less than 1
        /// </summary>
        /// <returns>System.String.</returns>
        public string FGColorToRGB()
        {
            if(this.Alpha < 1)
                return string.Format("rgba({0},{1},{2},{3})", this.FGRed, this.FGGreen, this.FGBlue, this.FGAlpha);
            else
                return string.Format("rgb({0},{1},{2})", this.FGRed, this.FGGreen, this.FGBlue);
        }

        /// <summary>
        /// To the string.
        /// </summary>
        /// <returns>System.String.</returns>
        public override string ToString()
        {
            return ToHtmlString();
        }

        /// <summary>
        /// To the string.
        /// </summary>
        /// <param name="appendImportantAttr">The append important attribute.</param>
        /// <returns>System.String.</returns>
        public string ToString(bool appendImportantAttr)
        {
            return ToHtmlString(appendImportantAttr);
        }

        /// <summary>
        /// To the HTML string.
        /// </summary>
        /// <param name="appendImportantAttr">The append important attribute.</param>
        /// <returns>System.String.</returns>
        private string ToHtmlString(bool appendImportantAttr = false)
        {
            return string.Format(".color-{0} {{background-color:{1}{3};border-color:{1}{3};color:{2}{3}}} .color-{0}-n-arrow{{ border-bottom-color:{1}{3} }} .color-{0}-n-arrow:after{{ border-bottom-color:{1}{3} }}",
                this.ColorId, this.BGColorToRGB(), this.FGColorToRGB(),
                appendImportantAttr ? " !important" : "");
        }
    }

    //[MetadataType(typeof(EventReminderMetadata))]
    //public partial class EventReminder
    //{
        
    //}

    /// <summary>
    /// Class EventToPerson.
    /// </summary>
    [MetadataType(typeof(EventToPersonMetadata))]
    public partial class EventToPerson
    {

    }

    /// <summary>
    /// Class fntSalesRepSummary.
    /// </summary>
    public partial class fntSalesRepSummary
    {
        /// <summary>
        /// Gets or sets the color hexadecimal.
        /// </summary>
        /// <value>The color hexadecimal.</value>
        public string ColorHex { get; set; }
    }

    /// <summary>
    /// Class PhysicalFile.
    /// </summary>
    [MetadataType(typeof(PhysicalFileMetadata))]
    public partial class PhysicalFile
    {
        /// <summary>
        /// Gets or sets the full name of the added by.
        /// </summary>
        /// <value>The full name of the added by.</value>
        public string AddedByFullName
        {
            get;
            set;
        }
    }

    /// <summary>
    /// Class OAuthUser.
    /// </summary>
    [MetadataType(typeof(OAuthUserMetadata))]
    public partial class OAuthUser { }

    /// <summary>
    /// Class Territory.
    /// </summary>
    [MetadataType(typeof(TerritoryMetadata))]
    public partial class Territory
    {
        /// <summary>
        /// To the string.
        /// </summary>
        /// <returns>System.String.</returns>
        public override string ToString()
        {
            return string.Format("{0} - {1}", this.Code, this.Name ?? "");
        }
    }

    public partial class FranchiseRoyalty
    {
        [NotMapped]
        public string StartOnUtcText
        {
            get
            {
                return this.StartOnUtc.HasValue ? this.StartOnUtc.Value.ToString("dd MMM yyyy") : null;
            }
            
        }
        [NotMapped]
        public string EndOnUtcText
        {
            get
            {
                return this.EndOnUtc.HasValue ? this.EndOnUtc.Value.ToString("dd MMM yyyy") : null;
            }

        }
    }

    /// <summary>
    /// Class FranchiseTaxRule.
    /// </summary>
    public partial class FranchiseTaxRule
    {
        /// <summary>
        /// To the string.
        /// </summary>
        /// <returns>System.String.</returns>
        public override string ToString()
        {
            if (!string.IsNullOrEmpty(ZipCode))
                return ZipCode + " Tax";
            else if (!string.IsNullOrEmpty(City))
                return City + " Tax";
            else if (!string.IsNullOrEmpty(County))
                return County + " Tax";
            else
                return "Base Tax";
        }

        
        public bool IsBaseTax { get; set; }
    }

    public partial class QBSession
    {
        private System.Collections.Hashtable _properties;

        /// <summary>
        /// This lets programmer create additional properties on the session, such as error count.
        /// Using private properties to instantiate a new list, since we can't modify EF autogenerated file's constructor to instantiate
        /// </summary>
        public System.Collections.Hashtable Properties 
        {
            get
            {
                if (_properties == null)
                    _properties = new System.Collections.Hashtable();

                return _properties;
            }
        }
    }

    [MetadataType(typeof(InvoiceMetadata))]
    public partial class Invoice
    {
        public Permission BasePermission { get; set; }
        public string Region { get; set; }
    }

    public partial class Vendor
    {
        public List<Product> Discounts { get; set; }
    }

}
