﻿using System;
using System.Linq;
using System.Web.Security;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HFC.CRM.Data
{

    public partial class User
    {
        //Not using this property, isauthenticated is already checked via HttpContext.Current.User.Identity.IsAuthenticated
        //public bool IsAuthenticated { get; set; }

        /// <summary>
        /// Gets the is online.
        /// </summary>
        /// <value>The is online.</value>
        [NotMapped]
        public bool IsOnline
        {
            get
            {
                DateTime DateActive = DateTime.Now.Subtract(TimeSpan.FromMinutes(Convert.ToDouble(Membership.UserIsOnlineTimeWindow)));
                if (LastActivityDateUtc.HasValue)
                    return LastActivityDateUtc.Value.CompareTo(DateActive) > 0;
                else
                    return false;
            }
        }

        /// <summary>
        /// Checks if user is in array of roles
        /// </summary>
        /// <param name="roles">Can be list of role names or role Guid</param>
        public bool IsInRole(params Role[] roles)
        {
            if (roles != null && roles.Length > 0 && Roles != null && Roles.Count > 0)
            {
                return (from current in Roles
                        from compare in roles
                        where current.RoleId == compare.RoleId
                        select current).Any();
            }
            else
                return false;
        }

        /// <summary>
        /// Determines whether [is in role] [the specified role names].
        /// </summary>
        /// <param name="roleNames">The role names.</param>
        public bool IsInRole(params string[] roleNames)
        {
            if (roleNames != null && roleNames.Length > 0 && Roles != null && Roles.Count > 0)
            {
                return (from current in Roles
                        from compare in roleNames
                        where string.Equals(current.RoleName, compare, StringComparison.InvariantCultureIgnoreCase)
                        select current).Any();
            }
            else
                return false;
        }

        /// <summary>
        /// Determines whether [is in role] [the specified role guids].
        /// </summary>
        /// <param name="roleGuids">The role guids.</param>
        public bool IsInRole(params Guid[] roleGuids)
        {
            if (roleGuids != null && roleGuids.Length > 0 && Roles != null && Roles.Count > 0)
            {
                return (from current in Roles
                        from compare in roleGuids
                        where current.RoleId == compare
                        select current).Any();
            }
            else
                return false;
        }

        public bool IsAdminInRole()
        {
            return IsInRole(new Guid("D6BAAB7B-D94F-41D3-88E9-601FD1C27398"), new Guid("DEE9A8FF-ED85-40CB-89E2-FE54D5CC8E49"), new Guid("05608BDD-5A3F-4CE7-81F6-0D5F3E35F2FB"), new Guid("D9A5866F-FC77-4E34-9810-D436274D6D76"));
        }

        public bool ShouldSerializeFranchise() { return false; }

        public bool ShouldSerializePassword() { return false; }
    }

}