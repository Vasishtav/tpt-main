﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    /// <summary>
    /// Sale adjustment for a quote, ie: Installation, Discounts, Tax, etc.
    /// </summary>
    public partial class JobSaleAdjustment
    {
        /// <summary>
        /// Creates a clone of the current job item
        /// </summary>
        /// <param name="exactCopy">Default is true.  If false, then identities will be set to 0 or null and CreatedOnUtc and LastUpdated set to DateTime.UtcNow</param>
        /// <returns></returns>
        public JobSaleAdjustment Clone(bool exactCopy = true)
        {
            JobSaleAdjustment clone = this.Copy();

            if (!exactCopy)
            {
                clone.SaleAdjustmentId = 0;
                clone.QuoteId = 0;
                clone.LastUpdated = clone.CreatedOnUtc = DateTime.Now;                
            }

            return clone;
        }
    }
}
