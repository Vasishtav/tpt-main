﻿using System;
using System.ComponentModel.DataAnnotations;
using HFC.CRM.Data.Constants;
using HFC.CRM.Data.Metadata;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace HFC.CRM.Data
{
    /// <summary>
    /// Class Franchise.
    /// </summary>
    [MetadataType(typeof(FranchiseMetadata))]
    public partial class Franchise
    {
        /// <summary>
        /// Gets 2 digit ISO country code
        /// </summary>
        /// <value>The country code.</value>
        /// 

        [NotMapped]
        public string CountryCode
        {
            get
            {
                if (Address != null)
                    return Address.CountryCode2Digits ?? "US";
                else
                    return "US";
            }
        }
        /// <summary>
        /// Gets the state text.
        /// </summary>
        /// <value>The state text.</value>
        [NotMapped]
        public string StateText
        {
            get
            {
                if (CountryCode.Equals("US", StringComparison.CurrentCultureIgnoreCase))
                    return "State";
                else
                    return "Province";
            }
        }
        /// <summary>
        /// Gets the zip text.
        /// </summary>
        /// <value>The zip text.</value>

        [NotMapped]
        public string ZipText
        {
            get
            {
                if (CountryCode.Equals("US", StringComparison.CurrentCultureIgnoreCase))
                    return "Zip Code";
                else
                    return "Postal Code";
            }
        }

        /// <summary>
        /// Gets the phone mask.
        /// </summary>
        /// <value>The phone mask.</value>
        [NotMapped]
        public string PhoneMask
        {
            get
            {
                return "(999) 999-9999";
            }
        }

        /// <summary>
        /// Gets the zip mask.
        /// </summary>
        /// <value>The zip mask.</value>
        [NotMapped]
        public string ZipMask
        {
            get
            {
                if (CountryCode.Equals("US", StringComparison.CurrentCultureIgnoreCase))
                    return "99999?-9999";
                else if (CountryCode.Equals("CA", StringComparison.CurrentCultureIgnoreCase))
                    return "a9a? 9a9";
                else
                    return "";
            }
        }



        public int BrandId { get; set; }

        [NotMapped]
        public string owner
        {
            get
            {
                if (OwnerName != null && OwnerName != "")
                {
                    var ownerName = OwnerName.Trim();
                    if ((ownerName.StartsWith("{") && ownerName.EndsWith("}")) || //For object
                        (ownerName.StartsWith("[") && ownerName.EndsWith("]"))) //For array
                    {
                        string name = JsonConvert.DeserializeObject<List<OwnerVM>>(OwnerName)[0].FirstName + " " + JsonConvert.DeserializeObject<List<OwnerVM>>(OwnerName)[0].LastName;
                        return name;
                    }

                }
                return " ";
            }
        }

        [NotMapped]
        public string Brand
        {
            get
            {
                if (BrandId == 1)
                    return "Budget Blinds";
                else if (BrandId == 2)
                    return "Tailored Living";
                else if (BrandId == 3)
                    return "Concrete Craft";
                else
                    return "";
            }
        }

        [NotMapped]
        public string TimezoneValue
        {
            get
            {
                return TimezoneCode.ToString();
            }
        }


    }


}


namespace HFC.CRM.Data.Metadata
{
    /// <summary>
    /// Class FranchiseMetadata.
    /// </summary>
    public class FranchiseMetadata
    {
        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>The code.</value>
        [Required]
        public string Code { get; set; }
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [Required]
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the support email.
        /// </summary>
        /// <value>The support email.</value>
        [RegularExpression(RegexPatterns.Email, ErrorMessage = "Invalid email address pattern")]
        public string SupportEmail { get; set; }
        /// <summary>
        /// Gets or sets the admin email.
        /// </summary>
        /// <value>The admin email.</value>
        [RegularExpression(RegexPatterns.Email, ErrorMessage = "Invalid email address pattern")]
        public string AdminEmail { get; set; }
        /// <summary>
        /// Gets or sets the owner email.
        /// </summary>
        /// <value>The owner email.</value>
        [RegularExpression(RegexPatterns.Email, ErrorMessage = "Invalid email address pattern")]
        public string OwnerEmail { get; set; }
        public string OwnerName { get; set; }
    }
}