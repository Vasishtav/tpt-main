﻿using HFC.CRM.Data.Constants;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data.Metadata
{
    /// <summary>
    /// Class Type_LeadStatusMetadata.
    /// </summary>
    public class Type_LeadStatusMetadata
    {
        /// <summary>
        /// Gets or sets the children.
        /// </summary>
        [JsonIgnore]
        public ICollection<Type_LeadStatus> Children { get; set; }
        /// <summary>
        /// Gets or sets the parent.
        /// </summary>
        [JsonIgnore]
        public Type_LeadStatus Parent { get; set; }
    }

    /// <summary>
    /// Class Type_JobStatusMetadata.
    /// </summary>
    public class Type_JobStatusMetadata
    {
        /// <summary>
        /// Gets or sets the children.
        /// </summary>
        [JsonIgnore]
        public ICollection<Type_JobStatus> Children { get; set; }
        /// <summary>
        /// Gets or sets the parent.
        /// </summary>
        [JsonIgnore]
        public Type_JobStatus Parent { get; set; }
    }

    /// <summary>
    /// Class Type_SourceMetadata.
    /// </summary>
    public class Type_SourceMetadata
    {
        /// <summary>
        /// Gets or sets the children.
        /// </summary>
        [JsonIgnore]
        public ICollection<Type_Source> Children { get; set; }
        /// <summary>
        /// Gets or sets the parent.
        /// </summary>
        [JsonIgnore]
        public Type_Source Parent { get; set; }

        /// <summary>
        /// Gets or sets the jobs.
        /// </summary>
        /// <value>The jobs.</value>
        [JsonIgnore]
        public ICollection<Job> Jobs { get; set; }
        /// <summary>
        /// Gets or sets the franchise.
        /// </summary>
        /// <value>The franchise.</value>
        [JsonIgnore]
        public Franchise Franchise { get; set; }
        /// <summary>
        /// Gets or sets the lead sources.
        /// </summary>
        /// <value>The lead sources.</value>
        [JsonIgnore]
        public ICollection<LeadSource> LeadSources { get; set; }
    }

    public class PermissionMetadata
    {
        [JsonIgnore]
        public DateTime CreatedOnUtc { get; set; }
    }

    /// <summary>
    /// Class Type_StateMetadata.
    /// </summary>
    public class Type_StateMetadata
    {        
        /// <summary>
        /// Gets or sets the type_ country.
        /// </summary>
        /// <value>The type_ country.</value>
        [JsonIgnore]
        public Type_Country Type_Country { get; set; }
        
    }
    /// <summary>
    /// Class LeadSourceMetadata.
    /// </summary>
    public class LeadSourceMetadata
    {
        /// <summary>
        /// Gets or sets the source.
        /// </summary>
        /// <value>The source.</value>
        [JsonIgnore]
        public Type_Source Source { get; set; }
        /// <summary>
        /// Gets or sets the created on UTC.
        /// </summary>
        /// <value>The created on UTC.</value>
        [JsonIgnore]
        public DateTime CreatedOnUtc { get; set; }
    }
    /// <summary>
    /// Class AddressMetadata.
    /// </summary>
    public class AddressMetadata
    {
        /// <summary>
        /// Gets or sets the is deleted.
        /// </summary>
        /// <value>The is deleted.</value>
        [JsonIgnore]
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets the attention text.
        /// </summary>
        /// <value>The attention text.</value>
        [Display(Name="Attention")]
        public string AttentionText { get; set; }
        /// <summary>
        /// Gets or sets the address1.
        /// </summary>
        /// <value>The address1.</value>
        [Display(Name="Address Line 1")]
        public string Address1 { get; set; }
        /// <summary>
        /// Gets or sets the address2.
        /// </summary>
        /// <value>The address2.</value>
        [Display(Name = "Address Line 2")]
        public string Address2 { get; set; }
        /// <summary>
        /// Gets or sets the zip code.
        /// </summary>
        /// <value>The zip code.</value>
        [Display(Name="Zip Code")]
        public string ZipCode { get; set; }
        /// <summary>
        /// Gets or sets the country code2 digits.
        /// </summary>
        /// <value>The country code2 digits.</value>
        [Display(Name="Country")]
        public string CountryCode2Digits { get; set; }
        /// <summary>
        /// Gets or sets the cross street.
        /// </summary>
        /// <value>The cross street.</value>
        [Display(Name="Cross Street")]
        public string CrossStreet { get; set; }
    }

    /// <summary>
    /// Class LeadMetadata.
    /// </summary>
    public class LeadMetadata
    {
        /// <summary>
        /// Gets or sets the lead unique identifier.
        /// </summary>
        /// <value>The lead unique identifier.</value>
        [JsonIgnore]
        public Guid LeadGuid { get; set; }
        /// <summary>
        /// Gets or sets the lead status.
        /// </summary>
        /// <value>The lead status.</value>
        [JsonIgnore]
        public Type_LeadStatus LeadStatus { get; set; }
        /// <summary>
        /// Gets or sets the franchise identifier.
        /// </summary>
        /// <value>The franchise identifier.</value>
        [JsonIgnore]
        public int FranchiseId { get; set; }
        /// <summary>
        /// Gets or sets the territory identifier.
        /// </summary>
        /// <value>The territory identifier.</value>
        [JsonIgnore]
        public int? TerritoryId { get; set; }
        /// <summary>
        /// Gets or sets the is deleted.
        /// </summary>
        /// <value>The is deleted.</value>
        [JsonIgnore]
        public bool IsDeleted { get; set; }
        /// <summary>
        /// Gets or sets the created by person identifier.
        /// </summary>
        /// <value>The created by person identifier.</value>
        [JsonIgnore]
        public int? CreatedByPersonId { get; set; }
        /// <summary>
        /// Gets or sets the last updated by person identifier.
        /// </summary>
        /// <value>The last updated by person identifier.</value>
        [JsonIgnore]
        public int? LastUpdatedByPersonId { get; set; }
        /// <summary>
        /// Gets or sets the franchise.
        /// </summary>
        /// <value>The franchise.</value>
        [JsonIgnore]
        public Franchise Franchise { get; set; }

        /// <summary>
        /// Gets or sets the calendars.
        /// </summary>
        /// <value>The calendars.</value>
        [JsonProperty(DefaultValueHandling=DefaultValueHandling.Ignore)]
        public ICollection<Calendar> Calendars { get; set; }
        /// <summary>
        /// Gets or sets the edit histories.
        /// </summary>
        /// <value>The edit histories.</value>
        [JsonIgnore]
        public ICollection<EditHistory> EditHistories { get; set; }
    }

    /// <summary>
    /// Class PersonMetadata.
    /// </summary>
    public class PersonMetadata
    {
        /// <summary>
        /// Gets or sets the is deleted.
        /// </summary>
        /// <value>The is deleted.</value>
        [JsonIgnore]
        public bool IsDeleted { get; set; }
        /// <summary>
        /// Gets or sets the created on UTC.
        /// </summary>
        /// <value>The created on UTC.</value>
        [JsonIgnore]
        public DateTime CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>The first name.</value>
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>The last name.</value>
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        /// <summary>
        /// Gets or sets the home phone.
        /// </summary>
        /// <value>The home phone.</value>
        [Display(Name = "Home Phone")]
        public string HomePhone { get; set; }
        /// <summary>
        /// Gets or sets the cell phone.
        /// </summary>
        /// <value>The cell phone.</value>
        [Display(Name = "Cell Phone")]
        public string CellPhone { get; set; }
        /// <summary>
        /// Gets or sets the work phone.
        /// </summary>
        /// <value>The work phone.</value>
        [Display(Name = "Work Phone")]
        public string WorkPhone { get; set; }
        /// <summary>
        /// Gets or sets the work phone ext.
        /// </summary>
        /// <value>The work phone ext.</value>
        [Display(Name = "Work Extension")]
        public string WorkPhoneExt { get; set; }
        /// <summary>
        /// Gets or sets the fax phone.
        /// </summary>
        /// <value>The fax phone.</value>
        [Display(Name = "Fax Number")]
        public string FaxPhone { get; set; }
        /// <summary>
        /// Gets or sets the primary email.
        /// </summary>
        /// <value>The primary email.</value>
        
        [Display(Name = "Primary Email")]
        //[RegularExpression(RegexPatterns.Email, ErrorMessage = "Invalid email address pattern")]
        public string PrimaryEmail { get; set; }
        /// <summary>
        /// Gets or sets the secondary email.
        /// </summary>
        /// <value>The secondary email.</value>
        [Display(Name = "Secondary Email")]
        //[RegularExpression(RegexPatterns.Email, ErrorMessage = "Invalid email address pattern")]
        public string SecondaryEmail { get; set; }
        /// <summary>
        /// Gets or sets the unsubscribed on UTC.
        /// </summary>
        /// <value>The unsubscribed on UTC.</value>
        [Display(Name = "Unsubscribed Date")]
        public DateTime? UnsubscribedOnUtc { get; set; }
        /// <summary>
        /// Gets or sets the name of the company.
        /// </summary>
        /// <value>The name of the company.</value>
        [Display(Name = "Company")]
        public string CompanyName { get; set; }
        /// <summary>
        /// Gets or sets the work title.
        /// </summary>
        /// <value>The work title.</value>
        [Display(Name = "Title")]
        public string WorkTitle { get; set; }

    }
    ///<summary>
    ///Class CustomerTPMetadata
    ///</summary>
    public class CustomerTPMetadata
    {
        /// <summary>
        /// Gets or sets the is deleted.
        /// </summary>
        /// <value>The is deleted.</value>
        [JsonIgnore]
        public bool IsDeleted { get; set; }
        /// <summary>
        /// Gets or sets the created on UTC.
        /// </summary>
        /// <value>The created on UTC.</value>
        [JsonIgnore]
        public DateTime CreatedOn { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>The first name.</value>
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>The last name.</value>
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        /// <summary>
        /// Gets or sets the home phone.
        /// </summary>
        /// <value>The home phone.</value>
        [Display(Name = "Home Phone")]
        public string HomePhone { get; set; }
        /// <summary>
        /// Gets or sets the cell phone.
        /// </summary>
        /// <value>The cell phone.</value>
        [Display(Name = "Cell Phone")]
        public string CellPhone { get; set; }
        /// <summary>
        /// Gets or sets the work phone.
        /// </summary>
        /// <value>The work phone.</value>
        [Display(Name = "Work Phone")]
        public string WorkPhone { get; set; }
        /// <summary>
        /// Gets or sets the work phone ext.
        /// </summary>
        /// <value>The work phone ext.</value>
        [Display(Name = "Work Extension")]
        public string WorkPhoneExt { get; set; }
        /// <summary>
        /// Gets or sets the fax phone.
        /// </summary>
        /// <value>The fax phone.</value>
        [Display(Name = "Fax Number")]
        public string FaxPhone { get; set; }
        /// <summary>
        /// Gets or sets the primary email.
        /// </summary>
        /// <value>The primary email.</value>

        [Display(Name = "Primary Email")]
        //[RegularExpression(RegexPatterns.Email, ErrorMessage = "Invalid email address pattern")]
        public string PrimaryEmail { get; set; }
        /// <summary>
        /// Gets or sets the secondary email.
        /// </summary>
        /// <value>The secondary email.</value>
        [Display(Name = "Secondary Email")]
        //[RegularExpression(RegexPatterns.Email, ErrorMessage = "Invalid email address pattern")]
        public string SecondaryEmail { get; set; }
        /// <summary>
        /// Gets or sets the unsubscribed on UTC.
        /// </summary>
        /// <value>The unsubscribed on UTC.</value>
        [Display(Name = "Unsubscribed Date")]
        public DateTime? UnsubscribedOn { get; set; }
        /// <summary>
        /// Gets or sets the name of the company.
        /// </summary>
        /// <value>The name of the company.</value>
        [Display(Name = "Company")]
        public string CompanyName { get; set; }
        /// <summary>
        /// Gets or sets the work title.
        /// </summary>
        /// <value>The work title.</value>
        [Display(Name = "Title")]
        public string WorkTitle { get; set; }

    }


    /// <summary>
    /// Class TaskMetadata.
    /// </summary>
    public class TaskMetadata
    {
        /// <summary>
        /// Gets or sets the task unique identifier.
        /// </summary>
        /// <value>The task unique identifier.</value>
        [JsonIgnore]
        public Guid TaskGuid { get; set; }

        /// <summary>
        /// Gets or sets the due date.
        /// </summary>
        /// <value>The due date.</value>
        [Display(Name = "Due Date")]
        public DateTimeOffset? DueDate { get; set; }
        /// <summary>
        /// Gets or sets the completed date.
        /// </summary>
        /// <value>The completed date.</value>
        [Display(Name = "Completed On")]
        public DateTimeOffset? CompletedDate { get; set; }
        /// <summary>
        /// Gets or sets the organizer person identifier.
        /// </summary>
        /// <value>The organizer person identifier.</value>
        [Display(Name = "Organizer")]
        public int OrganizerPersonId { get; set; }
        /// <summary>
        /// Gets or sets the created by person identifier.
        /// </summary>
        /// <value>The created by person identifier.</value>
        [Display(Name = "Created By")]
        public int CreatedByPersonId { get; set; }
        //[Display(Name = "Responded On")]
        //public DateTime? RespondedOnUtc { get; set; }
        //[Display(Name = "Rejected Reason")]
        //public string RejectedReason { get; set; }
        /// <summary>
        /// Gets or sets the priority order.
        /// </summary>
        /// <value>The priority order.</value>
        [Display(Name = "Priority")]
        public int PriorityOrder { get; set; }
    }

    //public class EventReminderMetadata
    //{
    //    [JsonIgnore]
    //    public System.DateTime CreatedOnUtc { get; set; }
    //    [JsonIgnore]
    //    public Nullable<System.DateTime> DismissedOnUtc { get; set; }
    //    [JsonIgnore]
    //    public int RemindWhoPersonId { get; set; }        
    //    [JsonIgnore]
    //    public Nullable<System.DateTime> SnoozedOnUtc { get; set; }        
    //    [JsonIgnore]
    //    public System.DateTime RemindOnUtc { get; set; }    
    //    [JsonIgnore]
    //    public Person RemindWhoPerson { get; set; }        
    //    [JsonIgnore]
    //    public ICollection<Calendar> Calendars { get; set; }
    //    [JsonIgnore]
    //    public ICollection<Task> Tasks { get; set; }
    //}

    /// <summary>
    /// Class EventToPersonMetadata.
    /// </summary>
    public class EventToPersonMetadata
    {
        /// <summary>
        /// Gets or sets the created on UTC.
        /// </summary>
        /// <value>The created on UTC.</value>
        [JsonIgnore]
        public System.DateTime CreatedOnUtc { get; set; }
        /// <summary>
        /// Gets or sets the calendar.
        /// </summary>
        /// <value>The calendar.</value>
        [JsonIgnore]
        public virtual Calendar Calendar { get; set; }
        /// <summary>
        /// Gets or sets the person.
        /// </summary>
        /// <value>The person.</value>
        [JsonIgnore]
        public virtual Person Person { get; set; }
        /// <summary>
        /// Gets or sets the task.
        /// </summary>
        /// <value>The task.</value>
        [JsonIgnore]
        public virtual Task Task { get; set; }
    }

    
    /// <summary>
    /// Class PhysicalFileMetadata.
    /// </summary>
    public class PhysicalFileMetadata
    {
        /// <summary>
        /// Gets or sets the physical file unique identifier.
        /// </summary>
        /// <value>The physical file unique identifier.</value>
        [JsonIgnore]
        public Guid PhysicalFileGuid { get; set; }
        /// <summary>
        /// Gets or sets the last updated by person identifier.
        /// </summary>
        /// <value>The last updated by person identifier.</value>
        [JsonIgnore]
        public int? LastUpdatedByPersonId { get; set; }
        /// <summary>
        /// Gets or sets the last updated UTC.
        /// </summary>
        /// <value>The last updated UTC.</value>
        [JsonIgnore]
        public DateTime? LastUpdatedUtc { get; set; }
        /// <summary>
        /// Gets or sets the is relative URL path.
        /// </summary>
        /// <value>The is relative URL path.</value>
        [JsonIgnore]
        public bool IsRelativeUrlPath { get; set; }
    }

    /// <summary>
    /// Class OAuthUserMetadata.
    /// </summary>
    public class OAuthUserMetadata
    {
        /// <summary>
        /// Gets or sets the access token.
        /// </summary>
        /// <value>The access token.</value>
        [JsonIgnore]
        public string AccessToken { get; set; }
        /// <summary>
        /// Gets or sets the refresh token.
        /// </summary>
        /// <value>The refresh token.</value>
        [JsonIgnore]
        public string RefreshToken { get; set; }
        /// <summary>
        /// Gets or sets the token last updated on UTC.
        /// </summary>
        /// <value>The token last updated on UTC.</value>
        [JsonIgnore]
        public Nullable<System.DateTime> TokenLastUpdatedOnUtc { get; set; }
        /// <summary>
        /// Gets or sets the token expires in.
        /// </summary>
        /// <value>The token expires in.</value>
        [JsonIgnore]
        public Nullable<short> TokenExpiresIn { get; set; }
    }

    public class InvoiceMetadata
    {
        [JsonIgnore]
        public Guid InvoiceGuid { get; set; }
    }

    public class TerritoryMetadata
    {
        [JsonIgnore]
        public Franchise Franchise { get; set; }
    }
}
