﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    /// <summary>
    /// 
    /// </summary>
    public partial class JobQuote
    {        
        /// <summary>
        /// Run this method to calculate a job subtotal, surcharge, discount, tax, net total and profit based on it's JobItems and SaleAdjustments data
        /// </summary>
        /*public void CalculateTotals()
        {
            decimal subtotal = 0, costSubtotal = 0, taxTotal = 0, surchargeTotal = 0, surchargeTaxableTotal = 0, discountTotal = 0, discountTaxableTotal = 0, taxable_subtotal = 0, taxRate = 0;

            if (this.JobItems != null && this.JobItems.Count > 0)
            {
                subtotal = Math.Round(this.JobItems.Sum(s => (s.Quantity * s.SalePrice) - s.DiscountAmount), 4, MidpointRounding.ToEven);
                taxable_subtotal = Math.Round(this.JobItems.Where(w => w.IsTaxable).Sum(s => (s.Quantity * s.SalePrice) - s.DiscountAmount), 4, MidpointRounding.ToEven);
                costSubtotal = Math.Round(this.JobItems.Sum(s => s.Quantity * s.UnitCost), 4, MidpointRounding.ToEven);
            }
            if (this.SaleAdjustments != null && this.SaleAdjustments.Count > 0)
            {
                taxRate = this.SaleAdjustments.Where(w => w.TypeEnum == SaleAdjTypeEnum.Tax).Sum(s => s.Percent);
               
                var surchargeItems = this.SaleAdjustments.Where(w => w.TypeEnum == SaleAdjTypeEnum.Surcharge);
                var discountItems = this.SaleAdjustments.Where(w => w.TypeEnum == SaleAdjTypeEnum.Discount);

                surchargeTaxableTotal = surchargeItems.Where(w => w.IsTaxable).Sum(s => s.Amount + (subtotal * s.Percent/100));
                discountTaxableTotal = discountItems.Where(w => w.IsTaxable).Sum(s => s.Amount + (subtotal * s.Percent / 100));

                taxable_subtotal = taxable_subtotal + surchargeTaxableTotal - discountTaxableTotal;

                taxTotal = Math.Round((taxRate / 100.0M) * (taxable_subtotal), 4, MidpointRounding.ToEven);
               
                surchargeTotal = Math.Round(surchargeItems.Sum(s => s.Amount), 4, MidpointRounding.ToEven) + (subtotal * (surchargeItems.Sum(s => s.Percent) / 100));
                discountTotal = Math.Round(discountItems.Sum(s => s.Amount), 4, MidpointRounding.ToEven) + (subtotal * (discountItems.Sum(s => s.Percent) / 100));               
            }
            this.TaxPercent = taxRate;
            this.Taxtotal = taxTotal;
            this.Subtotal = subtotal;
            this.CostSubtotal = costSubtotal;
            this.SurchargeTotal = surchargeTotal;
            this.DiscountTotal = discountTotal;

            this.NetTotal = Math.Round(subtotal + taxTotal + surchargeTotal - discountTotal, 2, MidpointRounding.AwayFromZero);
            this.NetProfit = Math.Round(subtotal + surchargeTotal - costSubtotal - discountTotal, 2, MidpointRounding.AwayFromZero);

            this.Margin = subtotal + surchargeTotal - discountTotal == 0
                ? 0
                : Math.Round(
                    (subtotal + surchargeTotal - costSubtotal - discountTotal)
                    / ((subtotal + surchargeTotal - discountTotal) == 0
                        ? 1
                        : (subtotal + surchargeTotal - discountTotal)) * 100.0M,
                    2);
        }*/

        /// <summary>
        /// Run this method to calculate a job subtotal, surcharge, discount, tax, net total and profit based on it's JobItems and SaleAdjustments data
        /// </summary>(
        public void CalculateTotals()
        {
            decimal subtotal = 0, costSubtotal = 0, taxTotal = 0, surchargeTotal = 0, surchargeTaxableTotal = 0, discountTotal = 0, discountTaxableTotal = 0, taxable_subtotal = 0, taxRate = 0, totalLineDiscounts = 0;

            if (this.JobItems != null && this.JobItems.Count > 0)
            {
                subtotal = Math.Round(this.JobItems.Sum(s => (s.Subtotal)), 4, MidpointRounding.ToEven);
                taxable_subtotal = Math.Round(this.JobItems.Where(w => w.IsTaxable).Sum(s => (s.Subtotal)), 4, MidpointRounding.ToEven);
                costSubtotal = Math.Round(this.JobItems.Sum(s => s.Quantity * s.UnitCost), 4, MidpointRounding.ToEven);
                totalLineDiscounts = Math.Round(this.JobItems.Sum(s => s.DiscountAmount), 4, MidpointRounding.ToEven);
            }
            if (this.SaleAdjustments != null && this.SaleAdjustments.Count > 0)
            {
                taxRate = this.SaleAdjustments.Where(w => w.TypeEnum == SaleAdjTypeEnum.Tax).Sum(s => s.Percent);

                var surchargeItems = this.SaleAdjustments.Where(w => w.TypeEnum == SaleAdjTypeEnum.Surcharge);
                var discountItems = this.SaleAdjustments.Where(w => w.TypeEnum == SaleAdjTypeEnum.Discount);

                surchargeTaxableTotal = surchargeItems.Where(w => w.IsTaxable).Sum(s => s.Amount + (subtotal * s.Percent / 100));
                discountTaxableTotal = discountItems.Where(w => w.IsTaxable).Sum(s => s.Amount + (subtotal * s.Percent / 100));

                taxable_subtotal = taxable_subtotal + surchargeTaxableTotal - discountTaxableTotal;

                taxTotal = Math.Round((taxRate / 100.0M) * (taxable_subtotal), 4, MidpointRounding.ToEven);

                surchargeTotal = Math.Round(surchargeItems.Sum(s => s.Amount), 4, MidpointRounding.ToEven) + (subtotal * (surchargeItems.Sum(s => s.Percent) / 100));
                discountTotal = Math.Round(discountItems.Sum(s => s.Amount), 4, MidpointRounding.ToEven) + (subtotal * (discountItems.Sum(s => s.Percent) / 100));
            }

            this.TaxPercent = taxRate;
            this.Taxtotal = taxTotal;
            this.Subtotal = subtotal;
            this.CostSubtotal = costSubtotal;
            this.SurchargeTotal = surchargeTotal;
            this.DiscountTotal = discountTotal + totalLineDiscounts;

            this.NetTotal = Math.Round(subtotal + taxTotal + surchargeTotal - discountTotal, 2, MidpointRounding.AwayFromZero);
            this.NetProfit = Math.Round(subtotal + surchargeTotal - costSubtotal - discountTotal, 2, MidpointRounding.AwayFromZero);

            this.Margin = subtotal + surchargeTotal - discountTotal == 0
                ? 0
                : Math.Round(
                    (subtotal + surchargeTotal - costSubtotal - discountTotal)
                    / ((subtotal + surchargeTotal - discountTotal) == 0
                        ? 1
                        : (subtotal + surchargeTotal - discountTotal)) * 100.0M,
                    2);
        }
    }
}
