﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    /// <summary>
    /// Class ValidationResult.
    /// </summary>
    public static class ValidationResult
    {
        /// <summary>
        /// To the flatten string.
        /// </summary>
        /// <param name="validation">The validation.</param>
        /// <param name="topXresult">The top xresult.</param>
        /// <returns>System.String.</returns>
        public static string ToFlattenString(this DbEntityValidationResult validation, int topXresult = 3)
        {
            if (validation.IsValid)
                return null;
            else
            {
                if (validation.ValidationErrors.Count > topXresult)
                {
                    var errors = validation.ValidationErrors.Take(topXresult).Select(s => string.Format("{0}: {1}", s.PropertyName, s.ErrorMessage)).ToList();
                    errors.Add(string.Format("{0} error(s) not being shown.", validation.ValidationErrors.Count - topXresult));
                    return string.Join(",", errors);
                }
                else
                    return string.Join(",", validation.ValidationErrors.Select(s => string.Format("{0}: {1}", s.PropertyName, s.ErrorMessage)));
            }
        }
    }
}
