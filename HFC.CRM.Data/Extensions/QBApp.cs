﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    public partial class QBApp
    {

        //Murugan:
        //public bool SynchTaxLineItems { get; set; }

        //Do not user IsSyncLocked in code. Need to use IsSyncLockedExt
        public bool IsSyncLockedExt
        {
            get
            {
                return (IsSyncLocked == true && SyncLockedTime != null &&
                        DateTime.Now - SyncLockedTime.Value < TimeSpan.FromMinutes(30));
            }
            set
            {
                IsSyncLocked = value;
                if (value)
                {
                    SyncLockedTime = DateTime.Now;
                }
            }
        }
    }
}
