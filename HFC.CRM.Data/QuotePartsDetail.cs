﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("[CRM].[QuotePartsDetail]")]
    public class QuotePartsDetail
    {
        [Key]
        public int QuotePartsDetailID { get; set; }
        public int PartID { get; set; }
        public int? QuoteKey { get; set; }
        public int? QuoteLineId { get; set; }
        public int? QTY { get; set; }
        public string Category { get; set; }
        public string Color { get; set; }
        public string Style { get; set; }
        public string Size { get; set; }
        public string Labor { get; set; }
        public decimal? ResalePrice { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public int? LastUpdatedBy { get; set; }
    }
}
