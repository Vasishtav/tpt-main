﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.FranchiseCase")]
    public class FranchiseCaseModel : BaseEntity
    {
        [Key]
        public int CaseId { get; set; }
        public int CaseNumber { get; set; }
        public int Owner_PersonId { get; set; }
        public DateTime? IncidentDate { get; set; }
        public int? Status { get; set; }
        public DateTime? DateTimeOpened { get; set; }
        public DateTime? DateTimeClosed { get; set; }
        public string Description { get; set; }
        public int? VPO_PICPO_PODId { get; set; }
        public string MPO_MasterPONum_POId { get; set; }
        public string SalesOrderId { get; set; }
        [NotMapped]
        public string Name { get; set; }
        public List<FranchiseCaseViewModel> AdditionalInfo { get; set; }
        [NotMapped]
        public string AccountName { get; set; }
        [NotMapped]
        public string CellPhone { get; set; }
        [NotMapped]
        public string PrimaryEmail { get; set; }
        [NotMapped]
        public string SideMark { get; set; }
        [NotMapped]
        public string LastUPdatedName { get; set; }
        [NotMapped]
        public string StatusValue { get; set; }
        [NotMapped]
        public string MPOID { get; set; }
        [NotMapped]
        public string POID { get; set; }
        [NotMapped]
        public string SalesorderNumber { get; set; }
        [NotMapped]
        public string FranchiseName { get; set; }
        [NotMapped]
        public string LocalPhoneNumber { get; set; }
        [NotMapped]
        public int VendorCaseId { get; set; }
        [NotMapped]
        public string VendorCaseNumber { get; set; }
        [NotMapped]
        public string OwnerName { get; set; }

        [NotMapped]
        public int noOfLines { get; set; }
        [NotMapped]
        public int FranchiseId { get; set; }
        [NotMapped]
        public string VendorName { get; set; }
        //[NotMapped]
        //public string PrimaryEmail { get; set; }
        //public DateTime CreatedOn { get; set; }
        //public int CreatedBy { get; set; }
        //public DateTime? LastUpdatedOn { get; set; }
        //public int? LastUpdatedBy { get; set; }
    }
}
