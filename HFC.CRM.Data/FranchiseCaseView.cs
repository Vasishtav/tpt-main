﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
   
    public class FranchiseCaseViewModel : BaseEntity
    {
      
        public int Id { get; set; }
        public int VendorCaseNumber { get; set; }
        public int CaseId { get; set; }
        public int QuoteLineId { get; set; }
        public int? Type { get; set; }
        public int? ReasonCode { get; set; }
        public string IssueDescription { get; set; }
        public bool ExpediteReq { get; set; }
        public bool ExpediteApproved { get; set; }
        public bool ReqTripCharge { get; set; }
        public int? TripChargeApproved { get; set; }
        public int? Status { get; set; }
        public int? Resolution { get; set; }
        public int VPO { get; set; }
        public FranchiseCaseDropModel QuoteLineId2 { get; set; }
        // public int QuoteLineNumber { get; set; }
        public string QuoteLineNumber { get; set; }


    }
}
