using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace HFC.CRM.Data
{
    [Table("History.Texting")]
    public partial class TextingHistory
    {
        [Key]
        public long TextingId { get; set; }
        public string CellPhone { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public DateTime? SentAt { get; set; }
        public int? FranchiseId { get; set; }
        public int? SentByPersonId { get; set; }
        public int? LeadId { get; set; }
        public int? AccountId { get; set; }
        public int? OpportunityId { get; set; }
        public int? OrderId { get; set; }
       
        public string Errorinfo { get; set; }

        public int? CalendarId { get; set; }
        public int TemplateTypeId { get; set; }

        public int? Status { get; set; }
        [NotMapped]
        public string StatusText {
            get {
                var result = "";
                if (Status.HasValue)
                {
                    var temp = Status.Value;
                    if (temp == 1)
                    {
                        result = "Sent";
                    }
                    else if (temp == 2)
                    {
                        result = "Received";
                    }
                    else
                    {
                        result = "Error";
                    }
                }

                return result;
            }
        }
    }
}
