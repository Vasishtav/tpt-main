﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.MeasurementHeader")]
    public partial class MeasurementHeader
    {
        [Key]
        public int Id { get; set; }
        public Nullable<int> OpportunityId { get; set; }
        public string MeasurementSet { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreatedOnUtc { get; set; }
        public Nullable<int> CreatedByPersonId { get; set; }
        public System.DateTime LastUpdatedOnUtc { get; set; }
        public Nullable<int> LastUpdatedByPersonId { get; set; }
        public int? InstallationAddressId { get; set; }
        [NotMapped]
        public List<MeasurementLineItemBB> MeasurementBB { get; set; }
        [NotMapped]
        public List<MeasurementLineItemTL> MeasurementTL { get; set; }
        [NotMapped]
        public List<MeasurementLineItemCC> MeasurementCC { get; set; }
    }
}
