//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HFC.CRM.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class POLineItem
    {
        public int POLineItemId { get; set; }
        public int PurchaseOrderId { get; set; }
        public Nullable<int> JobItemId { get; set; }
        public Nullable<System.Guid> ProductGuid { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Subtotal { get; set; }
        public System.DateTimeOffset CreatedOn { get; set; }
        public Nullable<int> CreatedByPersonId { get; set; }
        public System.DateTimeOffset UpdatedOn { get; set; }
    
        public virtual Person Person { get; set; }
        public virtual JobItem JobItem { get; set; }
        public virtual PurchaseOrder PurchaseOrder { get; set; }
    }
}
