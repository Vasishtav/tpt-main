﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.CalendarTimeEntries")]
    public class CalendarTimeEntries : BaseEntity
    {
        public int Id { get; set; }
        public int CalendarId { get; set; }
        public int EntryType { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Notes { get; set; }
        public bool? Adjusted { get; set; }

        [NotMapped]
        public string EntryTypeValue
        {
            get
            {
                if (EntryType == 0)
                    return "Travel";
                else
                    return ((AppointmentTypeEnumTP)EntryType).GetDisplayName();
            }
        }
    }
}
