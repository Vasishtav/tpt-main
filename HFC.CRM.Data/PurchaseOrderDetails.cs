﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.PurchaseOrderDetails")]
    public class PurchaseOrderDetails
    {
        [Key]
        public int PurchaseOrdersDetailId { get; set; }
        public int PurchaseOrderId { get; set; }
        public int QuoteLineId { get; set; }
        public string PartNumber { get; set; }
        public DateTime? ShipDate { get; set; }
        public string Status { get; set; }
        public DateTime? PromiseByDate { get; set; }
        public int StatusId { get; set; }
        public int QuantityPurchased { get; set; }
        public string PickedBy { get; set; }
        public int? PONumber { get; set; }
        public string VendorNotes { get; set; }
        public string NotesVendor { get; set; }
        public string POResponse { get; set; }
        public string Errors { get; set; }
        public string VendorReference { get; set; }
        public string Warning { get; set; }
        public string Information { get; set; }
        public string CancelReason { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        [NotMapped]
        public QuoteLines QuoteLines { get; set; }
        [NotMapped]
        public QuoteLineDetail QuoteLineDetail { get; set; }
        [NotMapped]
        public QuoteLinesVM QuoteLinesVm { get; set; }
        [NotMapped]
        public string VendorPO { get; set; }
        [NotMapped]
        public string Vendor { get; set; }
        [NotMapped]
        public int VendorId { get; set; }
        [NotMapped]
        public int TotalLines { get; set; }
        [NotMapped]
        public int TotalQuantity { get; set; }
        [NotMapped]
        public string ProductName { get; set; }
        [NotMapped]
        public string ProductNumber { get; set; }
        [NotMapped]
        public decimal cost { get; set; }
        [NotMapped]
        public int ProductTypeId { get; set; }
        public int? PICPO { get; set; }
        public List<ShipNotice> ShipNotices { get; set; }
        [NotMapped]
        public int LineNumber { get; set; }
        public ShipNotice ShipNotice { get; set; }
        public VendorInvoice VendorInvoice { get; set; }
        public Nullable<int> ImpersonatorPersonId { get; set; }
        public DateTime? estShipDate { get; set; }
        public int QTY { get; set; }
        public decimal AckCost { get; set; }
        public decimal Discount { get; set; }
        public int item { get; set; }
        public int woItem { get; set; }
        public string UoM { get; set; }
        [NotMapped]
        public string UoMDesc { get; set; }

    }
}
