﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.CaseFeedbackDetails")]
    public class CaseFeedbackDetails:Case
    {
        [Key]
        public int CaseFeedbackId { get; set; }       
        public int RecordTypeId { get; set; }       
        public int ProductGroupId { get; set; }
        public string TouchpointURL { get; set; }
        public string TouchpointVersion { get; set; }
        public string BrowserDetails { get; set; }
        public string PICJson { get; set; }
        public int ChangeCaseId { get; set; }
        public int CloseReason { get; set; }
        public string FieldErrors { get; set; }
        public Nullable <DateTime> ClosedOn { get; set; }       
    }
}
