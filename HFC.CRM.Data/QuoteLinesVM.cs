﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    public class QuoteLinesVM
    {
        public int QuoteLineId { get; set; }
        public int QuoteKey { get; set; }
        public int OpportunityId { get; set; }
        public int? MeasurementsetId { get; set; }
        public int? MeasurementId { get; set; }
        public int? VendorId { get; set; }
        public int? ProductId { get; set; }
        public decimal? SuggestedResale { get; set; }
        public decimal? Discount { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public int? LastUpdatedBy { get; set; }
        public bool? IsActive { get; set; }
        public double? Width { get; set; }
        public string FranctionalValueWidth { get; set; }
        public double? Height { get; set; }
        public string FranctionalValueHeight { get; set; }
        public decimal? UnitPrice { get; set; }
        public int? Quantity { get; set; }
        public string Description { get; set; }
        public string MountType { get; set; }
        public decimal? ExtendedPrice { get; set; }
        public string Memo { get; set; }
        public string PICJson { get; set; }
        public string ProductName { get; set; }
        public string VendorName { get; set; }
        public int ProductTypeId { get; set; }
        public string RoomName { get; set; }
        public string RoomLocation { get; set; }
        public string WindowLocation { get; set; }
        public string DiscountType { get; set; }
        public string InternalNotes { get; set; }
        public string CustomerNotes { get; set; }
        public string VendorNotes { get; set; }
        public int PricingManagementId { get; set; }
        public int RuleNumber { get; set; }
        public string PricingCode { get; set; }
        public string PricingRule { get; set; }
        public string Markup { get; set; }
        public string RoundingRule { get; set; }
        public bool ManualUnitPrice { get; set; }
        public int QuoteLineNumber { get; set; }
        public decimal? Cost { get; set; }
        public string ModelDescription { get; set; }
        public int? ProductCategoryId { get; set; }
        public int? ProductSubCategoryId { get; set; }
        public string ProductCategory { get; set; }
        public string ProductSubCategory { get; set; }
        public decimal? BaseResalePrice { get; set; }
        public decimal? BaseResalePriceWOPromos { get; set; }
        public string CancelReason { get; set; }
        public string VendorDisplayname { get; set; }
        public DateTime? ValidatedOn { get; set; }
        public DateTime? ProductUpdatedOn { get; set; }
        public bool Valid { get; set; }
        public string PrintProductCategory { get; set; }
        public string ConfigErrors { get; set; }
        public string Color { get; set; }
        public string Fabric { get; set; }
        public int PICPO { get; set; }
        public bool vpoStatus { get; set; }
        public int? CounterId { get; set; }
        public bool? ValidConfiguration { get; set; }
        public string ProductErrors { get; set; }
        public DateTime? LastValidatedon { get; set; }
        public decimal SalePrice
        {
            get
            {
                decimal result = 0;
                if (UnitPrice > 0 && Quantity > 0)
                {
                    result = (decimal)UnitPrice * (decimal)Quantity;
                }

                return result;
            }
        }
        public bool IsGroupDiscountApplied { get; set; }
        public int PSProductCategoryId { get; set; }
        public string PSProductCategory { get; set; }
        public bool TaxExempt { get; set; }
    }

    public class QuoteMarginReviewVM
    {
        public int QuoteLineNumber { get; set; }
        public int QuoteLineId { get; set; }
        public int Quantity { get; set; }
        public string Product { get; set; }
        public string Vendor { get; set; }
        public int PricingStrategyId { get; set; }
        public int RuleNumber { get; set; }
        public string PricingCode { get; set; }
        public string PricingRule { get; set; }
        public decimal PricingMarkup { get; set; }
        public string MarkupFactor { get; set; }
        public string Markup { get; set; }
        public string RoundingRule { get; set; }
        public decimal? BaseCost { get; set; } = 0;
        public decimal? BaseResalePrice { get; set; } = 0;
        public decimal? BaseResalePriceWOPromos { get; set; } = 0;
        public decimal? SuggestedResale { get; set; } = 0;
        public string SelectedPrice { get; set; }
        public string Discount { get; set; }
        public string DiscountType { get; set; }
        public decimal? UnitPrice { get; set; } = 0;
        public decimal? DiscountAmount { get; set; } = 0;
        public decimal? ExtendedPrice { get; set; } = 0;
        public decimal? MarginPercentage { get; set; } = 0;
        public string VendorPromo { get; set; }
        public bool SuggestedResaleCheck { get; set; }
        public bool BaseResalePriceCheck { get; set; }
        public bool BaseResalePriceWoPromoscheck { get; set; }
        public string InternalNotes { get; set; }
        public int ProductTypeId { get; set; }
        public string PICJson { get; set; }
        public bool IsGroupDiscountApplied { get; set; }
        public int PSProductCategoryId { get; set; }
        public string PSProductCategory { get; set; }

    }
}