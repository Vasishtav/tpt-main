﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.CaseChangeDetails")]
    public class CaseAttachments:BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int CaseId { get; set; }
        public string FileIconType { get; set; }
        public string FileDetails { get; set; }
        public string FileName { get; set; }
        public string StreamId { get; set; }

    }
}
