﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    public class ShippingViewModel
    {

        public int Id { get; set; }
        public string ShipToLocationName { get; set; }
        public int ShipToLocation { get; set; }
        public string ShipToName { get; set; }
        public bool? Active { get; set; }
        public int AddressId { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public List<int> TerritoriesId { get; set; }
        public List<string> Territory { get; set; }
        public string TerritoriesName { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public int? LastUpdatedBy { get; set; }

        public bool IsValidated { get; set; }
        public string PhoneNumber { get; set; }
    }
}
