﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.Type_Owner")]
    public class Type_Owner
    {
        [Key]
        public int OwnerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<int> BrandId { get; set; }
        public System.DateTimeOffset CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }
}
