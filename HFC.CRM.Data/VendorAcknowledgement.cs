﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    public class VendorAcknowledgement
    {
        public int PurchaseOrderId { get; set; }
        public string PICPO { get; set; }
        public string vendorReference { get; set; }
        public VendorAcknowledgementHeader VendorAcknowledgementHeader{get;set;}
        public List<VendorAcknowledgementLineDetail> VendorAcknowledgementLineDetails { get; set; }
    }
    public class VendorAcknowledgementHeader
    {
        public string PONumber { get; set; }
        public DateTime? PODate { get; set; }
        public DateTime? AckDate { get; set; }
        public string SoldTo { get; set; }
        public string ShipTo { get; set; }
        public string AckStatus { get; set; }
        public string VendorRef { get; set; }
        public string Currency { get; set; }
        public string TaxInfo { get; set; }
        public string Terms { get; set; }
        public string VendorNote { get; set; }
        public string Comments { get; set; }
        public DateTime? EstShipDate { get; set; }
        public string PICResponse { get; set; }
        public int TotalLines { get; set; }
        public int TotalQTY { get; set; }
        public decimal TotalCost { get; set; }
    }

    public class VendorAcknowledgementLineDetail
    {
        public int POLine { get; set; }
        public int QTY { get; set; }
        public string UoM { get; set; }
        public int Item { get; set; }
        public string Description { get; set; }
        public decimal QuotePrice { get; set; }
        public decimal Discount { get; set; }
        public decimal Cost { get; set; }
        public string Status { get; set; }
        public DateTime? EstShipDate { get; set; }
        public int OrderNumber { get; set; }
        public int OrderId { get; set; }
        public string OrderSideMark { get; set; }
        public int OrderLineNumber { get; set; }
        public string PartNumber { get; set; }


    }
    public class POOrderQuoteDetails
    {
        public int PurchaseOrderId { get; set; }
        public int QuoteLineId { get; set; }
        public int OrderNumber { get; set; }
        public int OrderId { get; set; }
        public string OrderSideMark { get; set; }
        public int OrderLineNumber { get; set; }
        public decimal BaseCost { get; set; }
        public string ItemDescription { get; set; }
    }



}

