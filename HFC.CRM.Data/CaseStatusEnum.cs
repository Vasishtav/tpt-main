﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    public enum CaseFeedbackStatusEnum
    {
        Submitted = 12000,
        VendorReview = 12001,
        Closed = 12002
    }
    public enum CaseChangeStatusEnum
    {
        Submitted = 13000,
        Approved = 13001,
        Declined = 13002,
        Validating = 13003,
        Validated = 13004,
        InProduction = 13005
    }
    public enum CaseChangeTypeEnum
    {
        AddProduct = 16000,
        RemoveProduct = 16001,
        ProductPriceChange = 16002,
        ProductSpecificationChange = 16003
    }

    public enum CaseChangeDeclinedReasonEnum
    {
        AdditionalInfoNeeded = 17000,
        EffectiveDateIsTooEarly = 17001
    }
 
}
