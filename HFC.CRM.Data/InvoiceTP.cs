﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    public partial class InvoiceTP
    {
        public int InvoiceId { get; set; }
        public string InvoiceType { get; set; }
        public DateTimeOffset InvoiceDate { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal Balance { get; set; }
    }
}
