﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    public class SaveCalendar
    {
        public string ViewTitle { get; set; }
        public List<int> PersonIds { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string ViewType { get; set; }
        public bool IsDefault { get; set; }
        public bool isBussinessHour { get; set; }
    }
}
