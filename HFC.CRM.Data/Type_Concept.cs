//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;

namespace HFC.CRM.Data
{
    [Table("CRM.Type_Concept")]
    public partial class Type_Concept
    {
        public short Id { get; set; }
        public string Concept { get; set; }
        public string Abbreviation { get; set; }
    }
}
