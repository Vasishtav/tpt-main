﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HFC.CRM.Data
{
    using Dapper;
    using System;
    using System.Collections.Generic;
    [Table("CRM.Type_OpportunityStatus")]
    public partial class Type_OpportunityStatus
    {

        public Type_OpportunityStatus()
        {
            this.Children = new HashSet<Type_OpportunityStatus>();
        }
        [Key]
        public short OpportunityStatusId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<int> BrandId { get; set; }
        public Nullable<int> FranchiseId { get; set; }
        public Nullable<int> DisplayOrder { get; set; }
        public string ClassName { get; set; }
        public Nullable<bool> isDeleted { get; set; }
        public Nullable<System.DateTime> Createdon { get; set; }
        public Nullable<System.DateTime> Updatedon { get; set; }

        public Nullable<int> CreatedBy { get; set; }
        public Nullable<int> UpdatedBy { get; set; }

        public virtual Type_OpportunityStatus Parent { get; set; }
        public virtual ICollection<Type_OpportunityStatus> Children { get; set; }
        public virtual Franchise Franchise { get; set; }
    }
}
