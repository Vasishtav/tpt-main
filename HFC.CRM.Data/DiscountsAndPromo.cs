﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.DiscountsAndPromo")]
    public class DiscountsAndPromo
    {
        [Key]
        public int Id { get; set; }

        public int? QuoteLineDetailId { get; set; }
        public int QuoteLineId { get; set; }
        public int QuoteKey { get; set; }
        public string Description { get; set; }
        public decimal? Discount { get; set; }
        public decimal? Cost { get; set; }
        public DateTime? PromoStartDate { get; set; }
        public DateTime? PromoEndtDate { get; set; }
        public decimal? DiscountAmountPassed { get; set; }
        public decimal? DiscountPercentPassed { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public int? LastUpdatedBy { get; set; }

        [NotMapped]
        public bool exists { get; set; } = false;
    }
}