﻿using System;
using System.Text;
using System.IO;
using System.Web.UI;
using Newtonsoft.Json;
using System.Collections;
using System.Data.Entity.Infrastructure;
using System.Xml.Linq;
using System.ComponentModel;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text.RegularExpressions;

namespace HFC.CRM.Data
{
    /// <summary>
    /// Class Util.
    /// </summary>
    public class Util
    {
        //private static Random _randomGenerator;
        //public static Random RandomGenerator
        //{
        //    get {
        //        if (_randomGenerator == null)
        //            _randomGenerator = new Random();
        //        return _randomGenerator;
        //    }
        //}

        /// <summary>
        /// Casts the object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objToCast">The object to cast.</param>
        /// <returns>T.</returns>
        public static T CastObject<T>(object objToCast)
        {
            return (T)CastObject(objToCast, typeof(T));
        }

        /// <summary>
        /// Casts the object.
        /// </summary>
        /// <param name="objToCast">The object to cast.</param>
        /// <param name="type">The type.</param>
        /// <returns>System.Object.</returns>
        public static object CastObject(object objToCast, Type type)
        {
            //Check if type is Nullable
            if (type.IsGenericType &&
                type.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                //If the type is Nullable and the value is null
                //Just return null
                if (objToCast == null)
                {
                    return null;
                }
                //Type is Nullable and we have a value, override conversion type to underlying
                //type for the Nullable to avoid exception in Convert.ChangeType
                var nullableConverter = new System.ComponentModel.NullableConverter(type);
                type = nullableConverter.UnderlyingType;
            }

            if (type == typeof(string) || type == typeof(String))
            {
                if (objToCast != null)
                    return objToCast.ToString();
                else
                    return string.Empty;
            }
            else if (type == typeof(int) || type == typeof(Int32))
            {
                int num = 0;
                if (objToCast != null)
                    int.TryParse(objToCast.ToString(), out num);
                return num;
            }
            else if (type == typeof(byte) || type == typeof(Byte))
            {
                byte num = 0;
                if (objToCast != null)
                    byte.TryParse(objToCast.ToString(), out num);
                return num;
            }
            else if (type == typeof(short) || type == typeof(Int16))
            {
                short num = 0;
                if (objToCast != null)
                    short.TryParse(objToCast.ToString(), out num);
                return num;
            }
            else if (type == typeof(double) || type == typeof(Double))
            {
                double num = 0;
                if (objToCast != null)
                    double.TryParse(objToCast.ToString(), out num);
                return num;
            }
            else if (type == typeof(decimal) || type == typeof(Decimal))
            {
                decimal num = 0;
                if (objToCast != null)
                    decimal.TryParse(objToCast.ToString(), out num);
                return num;
            }
            else if (type == typeof(float))
            {
                float num = 0;
                if (objToCast != null)
                    float.TryParse(objToCast.ToString(), out num);
                return num;
            }
            else if (type == typeof(long) || type == typeof(Int64))
            {
                long num = 0;
                if (objToCast != null)
                    long.TryParse(objToCast.ToString(), out num);
                return num;
            }
            else if (type == typeof(Guid))
            {
                Guid id = new Guid();
                if (objToCast != null)
                    Guid.TryParse(objToCast.ToString(), out id);
                return id;
            }
            else if (type == typeof(DateTime))
            {
                DateTime date = new DateTime();
                if (objToCast != null)
                    DateTime.TryParse(objToCast.ToString(), out date);
                return date;
            }
            else if (type == typeof(bool) || type == typeof(Boolean))
            {
                bool val = false;
                if (objToCast != null)
                {
                    //if not successful, try with number instead
                    if (!bool.TryParse(objToCast.ToString().ToLower(), out val))
                    {
                        int num = 0;
                        int.TryParse(objToCast.ToString(), out num);
                        if (num > 0)
                            val = true;
                    }
                }
                return val;
            }
            else
                return objToCast;
        }

        /// <summary>
        /// Gets the ef changed properties.
        /// </summary>
        /// <param name="entry">The entry.</param>
        /// <param name="model">The model.</param>
        /// <returns>System.Xml.Linq.XElement.</returns>
        public static XElement GetEFChangedProperties(DbEntityEntry entry, object model = null)
        {
            XElement historyValues = null;

            if (entry != null && entry.State != EntityState.Unchanged)
            {      

                string typeName = entry.Entity.GetType().Name;

                historyValues = new XElement(typeName);
                var names = entry.State == EntityState.Deleted ? entry.OriginalValues.PropertyNames : entry.CurrentValues.PropertyNames;
                if (entry.State == EntityState.Modified)
                    historyValues.Add(new XAttribute("operation", "update"));
                else if(entry.State == EntityState.Added)
                    historyValues.Add(new XAttribute("operation", "insert"));
                else if (entry.State == EntityState.Deleted)
                    historyValues.Add(new XAttribute("operation", "delete"));

                foreach (var prop in names)
                {
                    string propDisplayName = "";
                    if (model != null)
                    {
                        System.Web.ModelBinding.ModelMetadata meta = System.Web.ModelBinding.ModelMetadataProviders.Current.GetMetadataForProperty(() => model, model.GetType(), prop);
                        if (meta != null)
                            propDisplayName = meta.DisplayName;
                    }
                    if (entry.Property(prop).IsModified)
                    {
                        if (entry.State == EntityState.Modified)
                        {
                            var temp = new XElement(prop, //new XAttribute("operation", "update"),
                                    new XElement("Original", entry.Property(prop).OriginalValue),
                                    new XElement("Current", entry.Property(prop).CurrentValue)
                                );
                            if (!string.IsNullOrEmpty(propDisplayName))
                                temp.Add(new XAttribute("name", propDisplayName));
                            historyValues.Add(temp);
                        }
                        else if (entry.State == EntityState.Added && entry.Property(prop).CurrentValue != null)
                        {                            
                            var temp = new XElement(prop, //new XAttribute("operation", "insert"),
                                    new XElement("Current", entry.Property(prop).CurrentValue)
                                );
                            if (!string.IsNullOrEmpty(propDisplayName))
                                temp.Add(new XAttribute("name", propDisplayName));
                            historyValues.Add(temp);                            
                        }
                        else if (entry.State == EntityState.Deleted)
                        {
                            var temp = new XElement(prop, //new XAttribute("operation", "delete"),
                                    new XElement("Original", entry.Property(prop).OriginalValue)
                                );
                            if (!string.IsNullOrEmpty(propDisplayName))
                                temp.Add(new XAttribute("name", propDisplayName));
                            historyValues.Add(temp);
                        }
                    }
                }
            }

            return historyValues;
        }

        /// <summary>
        /// All dates are serialized as ISO Standard 8601 format in UTC
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj">The object.</param>
        /// <returns>System.String.</returns>
        public static string JSONSerialize<T>(T obj)
        {
            string returnVal = "";
            try
            {
                //DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());                
                //using (MemoryStream ms = new MemoryStream())
                //{
                //    serializer.WriteObject(ms, obj);
                //    returnVal = Encoding.Default.GetString(ms.ToArray());
                //}
                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                settings.NullValueHandling = NullValueHandling.Ignore;
                settings.DateTimeZoneHandling = DateTimeZoneHandling.RoundtripKind;
                //settings.ReferenceResolver.IsReferenced(
                returnVal = JsonConvert.SerializeObject(obj, settings);
                
            }
            catch //(Exception /*exception*/)
            {
                returnVal = "";
            }
            return returnVal;
        }

        /// <summary>
        /// Jsons the serialize.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="output">The output.</param>
        /// <param name="obj">The object.</param>
        public static void JSONSerialize<T>(TextWriter output, T obj)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.DateTimeZoneHandling = DateTimeZoneHandling.RoundtripKind;
            //settings.ReferenceResolver.IsReferenced(
            var serializer = JsonSerializer.Create(settings);
            serializer.Serialize(output, obj);            
        }
        /// <summary>
        /// All dates are deserialized from ISO 8601 format and in UTC
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dataStr">The data string.</param>
        /// <returns>T.</returns>
        public static T JSONDeserialize<T>(string dataStr)
        {
            if (!string.IsNullOrEmpty(dataStr))
            {
                //JavaScriptSerializer serializer = new JavaScriptSerializer();
                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.DateTimeZoneHandling = DateTimeZoneHandling.RoundtripKind;
                return JsonConvert.DeserializeObject<T>(dataStr, settings);
            }
            else
                return default(T);
        }

        /// <summary>
        /// Jsons the deserialize.
        /// </summary>
        /// <param name="dataStr">The data string.</param>
        /// <param name="type">The type.</param>
        /// <returns>System.Object.</returns>
        public static object JSONDeserialize(string dataStr, Type type)
        {
            if (!string.IsNullOrEmpty(dataStr))
            {
                //JavaScriptSerializer serializer = new JavaScriptSerializer();
                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.DateTimeZoneHandling = DateTimeZoneHandling.RoundtripKind;
                return JsonConvert.DeserializeObject(dataStr, type, settings);
            }
            else
                return null;
        }

        //public static string RenderControl(Control ctrl)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    StringWriter tw = new StringWriter(sb);
        //    HtmlTextWriter hw = new HtmlTextWriter(tw);

        //    ctrl.RenderControl(hw);
        //    return sb.ToString();
        //}

        /// <summary>
        /// Sort the two date so they are in correct order
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns>Returns true if sorting was necessary</returns>
        public static bool SortDate(ref DateTime startDate, ref DateTime endDate)
        {
            if (startDate > endDate)
            {
                var temp = startDate;
                startDate = endDate;
                endDate = temp;
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Converts the date.
        /// </summary>
        /// <param name="dateToConvert">The date to convert.</param>
        /// <param name="toZone">To zone.</param>
        /// <param name="fromZone">From zone.</param>
        /// <returns>System.Nullable&lt;System.DateTime&gt;.</returns>
        public static DateTime? ConvertDate(DateTime? dateToConvert, TimeZoneEnum toZone, TimeZoneEnum fromZone = TimeZoneEnum.UTC)
        {
            if (dateToConvert.HasValue)
                return ConvertDate(dateToConvert.Value, toZone, fromZone);
            else
                return null;
        }

        /// <summary>
        /// Converts date between time zones
        /// </summary>
        /// <param name="dateToConvert">The date to convert.</param>
        /// <param name="toZone">The timezone to convert to</param>
        /// <param name="fromZone">From zone.</param>
        /// <returns>System.DateTime.</returns>
        public static DateTime ConvertDate(DateTime dateToConvert, TimeZoneEnum toZone, TimeZoneEnum fromZone = TimeZoneEnum.UTC)
        {
            if (fromZone == toZone)
                return dateToConvert;

            TimeZoneInfo frominfo =
                fromZone == TimeZoneEnum.UTC ? TimeZoneInfo.Utc :
                                                 TimeZoneInfo.FindSystemTimeZoneById(fromZone.Description());
            TimeZoneInfo toinfo =
                toZone == TimeZoneEnum.UTC ? TimeZoneInfo.Utc :
                                               TimeZoneInfo.FindSystemTimeZoneById(toZone.Description());
            
            //necessary to reset the DateTimeKind to unspecified
            dateToConvert = new DateTime(dateToConvert.Ticks);
            return TimeZoneInfo.ConvertTime(dateToConvert, frominfo, toinfo);        
        }

        /// <summary>
        /// Converts an epoch unix timestamp number to DateTime in UTC kind
        /// </summary>
        /// <param name="timestamp">Unix timestamp is milliseconds past epoch</param>
        /// <returns>UTC Date Time</returns>
        public static DateTime ConvertUnixTimestamp(long timestamp)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);            
            return dtDateTime.AddMilliseconds(timestamp);
        }

        /// <summary>
        /// Encodes the quoted printable.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>System.String.</returns>
        public static string EncodeQuotedPrintable(string value)
        {
            if (string.IsNullOrEmpty(value))
                return value;

            StringBuilder builder = new StringBuilder();

            byte[] bytes = Encoding.UTF8.GetBytes(value);
            foreach (byte v in bytes)
            {
                // The following are not required to be encoded:
                // - Tab (ASCII 9)
                // - Space (ASCII 32)
                // - Characters 33 to 126, except for the equal sign (61).

                if ((v == 9) || ((v >= 32) && (v <= 60)) || ((v >= 62) && (v <= 126)))
                {
                    builder.Append(Convert.ToChar(v));
                }
                else
                {
                    builder.Append('=');
                    builder.Append(v.ToString("X2"));
                }
            }

            char lastChar = builder[builder.Length - 1];
            if (char.IsWhiteSpace(lastChar))
            {
                builder.Remove(builder.Length - 1, 1);
                builder.Append('=');
                builder.Append(((int)lastChar).ToString("X2"));
            }

            return builder.ToString();
        }

        /// <summary>
        /// Decodes the quoted printable.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="charSet">The character set.</param>
        /// <returns>System.String.</returns>
        public static string DecodeQuotedPrintable(string input, string charSet)
        {
            Encoding enc;

            try
            {
                enc = Encoding.GetEncoding(charSet);
            }
            catch
            {
                enc = new UTF8Encoding();
            }

            var occurences = new Regex(@"(=[0-9A-Z]{2}){1,}", RegexOptions.Multiline);
            var matches = occurences.Matches(input);

            foreach (Match match in matches)
            {
                try
                {
                    byte[] b = new byte[match.Groups[0].Value.Length / 3];
                    for (int i = 0; i < match.Groups[0].Value.Length / 3; i++)
                    {
                        b[i] = byte.Parse(match.Groups[0].Value.Substring(i * 3 + 1, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
                    }
                    char[] hexChar = enc.GetChars(b);
                    input = input.Replace(match.Groups[0].Value, new String(hexChar));
                }
                catch
                { ;}
            }
            input = input.Replace("?=", "").Replace("\r\n", "");

            return input;
        }
                
    }
}
