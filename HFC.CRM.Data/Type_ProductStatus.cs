﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    public class Type_ProductStatus
    {
        public int ProductStatusId { get; set; }
        public string ProductStatus { get; set; }
        public bool IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTimeOffset CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTimeOffset? LastUpdatedOn { get; set; }
        public int? LastUpdatedBy { get; set; }
    }
}
