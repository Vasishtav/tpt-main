﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    public class CurrencyAdd 
    {
        public string Country { get; set; }
        public string FromType { get; set; }
        public string FromRate { get; set; }
        public string ToType { get; set; }
        public string ToRate { get; set; }
        public int FromTypeValue { get; set; }
        public int ToTypeValue { get; set; }
        public string DateValue { get; set; }
        public string EndDateValue { get; set; }
    }
    //public class TypeConversion
    //{
    //    public int Id { get; set; }
    //    public string value { get; set; }
    //}
}
