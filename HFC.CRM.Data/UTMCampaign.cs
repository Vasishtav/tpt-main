﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.UTMCampaign")]
    public class UTMCampaign  : BaseEntity
{  
    public int Id { get; set; }
    public string UTMCode { get; set; }
    public int SourcesHFCId { get; set; }
    public DateTime? StartDate { get; set; }
    public DateTime? EndDate { get; set; }
    public decimal? Amount { get; set; }
    public string Memo { get; set; }
    public bool? IsActive { get; set; }
    public bool? IsDeleted { get; set; }
    //public DateTime CreatedOn { get; set; }
    //public int CreatedBy { get; set; }
    //public DateTime? UpdatedOn { get; set; }
    //public int? UpdatedBy { get; set; }
}
}
