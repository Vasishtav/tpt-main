﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HFC.CRM.Data
{
    [Table("CRM.QuoteTax")]
    public partial class QuoteTax
    {
        [Key]
        public int Id { get; set; }
        public int QuoteKey { get; set; }
        public string TaxReponseObject { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime LastUpdatedOn { get; set; }
        public int? LastUpdatedBy { get; set; }
    }
}
