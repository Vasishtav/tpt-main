﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.Attachments")]
    public partial class AttachmentInfo
    {
        [Key]
        public int AttachmentId { get; set; }
        public Nullable<int> FranchiseId { get; set; }
        public Nullable<int> LeadId { get; set; }
        public Nullable<int> JobId { get; set; }
        public Nullable<int> QuoteId { get; set; }
        public Nullable<int> PhysicalFileId { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public Nullable<int> createdOn { get; set; }
        public string CreatedBy { get; set; }

        public virtual Franchise Franchise { get; set; }
        public virtual Job Job { get; set; }
        public virtual Lead Lead { get; set; }
        public virtual PhysicalFile PhysicalFile { get; set; }
        public virtual JobQuote JobQuote { get; set; }
    }
}
