//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HFC.CRM.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class QBAddress
    {
        public string QBAddressGuid { get; set; }
        public int AddressId { get; set; }
        public System.DateTimeOffset CreatedOn { get; set; }
    
        public virtual Address Address { get; set; }
    }
}
