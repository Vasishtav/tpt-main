﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.CaseFollow")]
    public class CaseFollow
    {
        [Key]
        public int id { get; set; }
        public int CaseId { get; set; }
        public int PersonId { get; set; }
        public bool IsFollow { get; set; }

    }
}
