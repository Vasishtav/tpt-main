﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.Linq;
using HFC.CRM.Cache.Cache;
using HFC.CRM.Cache.IoC;
using Newtonsoft.Json;

namespace HFC.CRM.Data
{
    public class CRMContextEx : CRMContext
    {
        private const int MaxChangedEntitiesPerCommit = 1;

        public CRMContextEx()
        {
            ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 600;
            ((IObjectContextAdapter)this).ObjectContext.ObjectMaterialized += (sender, e) => this.IgnoreDateTimeOffset(e.Entity);
        }

        private void IgnoreDateTimeOffset(object entity)
        {
            if (entity == null) return;

            var properties = entity.GetType().GetProperties().Where(x => x.PropertyType == typeof(DateTimeOffset) || x.PropertyType == typeof(DateTimeOffset?));

            foreach (var property in properties)
            {
                var dt = property.PropertyType == typeof (DateTimeOffset?) ? (DateTimeOffset?) property.GetValue(entity) : (DateTimeOffset) property.GetValue(entity);

                if (dt == null || (dt.Value.Offset.Hours == 0 && dt.Value.Offset.Minutes == 0 && dt.Value.Offset.Seconds == 0)) continue;

                property.SetValue(entity, new DateTimeOffset(dt.Value.Year, dt.Value.Month, dt.Value.Day, dt.Value.Hour, dt.Value.Minute, dt.Value.Second, TimeSpan.FromSeconds(0)));
            }
        }

        private void SetLastUpdated(object entity)
        {
            if (entity == null) return;

            var properties = entity.GetType().GetProperties().Where(x => x.PropertyType == typeof(DateTimeOffset) || x.PropertyType == typeof(DateTimeOffset?) || x.PropertyType == typeof(DateTime?) || x.PropertyType == typeof(DateTime));

            foreach (var property in properties)
            {
                if (property.Name == "LastUpdated" || property.Name == "LastUpdatedOnUtc" ||
                    property.Name == "LastUpdatedUtc" || property.Name == "ProviderLastUpdatedUtc" ||
                    property.Name == "TokenLastUpdatedOnUtc")
                {
                    var now = DateTime.Now;
                    if (property.PropertyType == typeof (DateTimeOffset) || property.PropertyType == typeof (DateTimeOffset?))
                    {
                        property.SetValue(entity, new DateTimeOffset(now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second, TimeSpan.Zero));
                    }
                    else
                    {
                        property.SetValue(entity, new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second));
                    }

                }
            }
        }

        private void UpdateAudit(object entity, bool isUpdated)
        {
            if (entity == null) return;

            var properties = entity.GetType().GetProperties().Where(x => x.PropertyType == typeof (DateTime?) || x.PropertyType == typeof (DateTime));

            foreach (var property in properties)
            {
                if (isUpdated && property.Name == "UpdatedAt")
                {
                    var now = DateTime.Now;
                    if (property.PropertyType == typeof (DateTime?))
                    {
                        property.SetValue(entity, now);
                    }
                }
                else if (!isUpdated && property.Name == "CreatedAt")
                {
                    var now = DateTime.Now;
                    if (property.PropertyType == typeof (DateTime))
                    {
                        property.SetValue(entity, now);
                    }
                }
            }
        }
        
        private void TrackChanges()
        {
            var changeTracker = ServiceLocator.Resolve<IChangeTracker>();

            var changedEntries = this.ChangeTracker.Entries().Where(t => t.State == EntityState.Added || t.State == EntityState.Modified).ToList();

            /*
            if (changedEntries.Count() > MaxChangedEntitiesPerCommit)
            {
                var changeInfo = changedEntries.Select(entry => entry.GetType().Name);

                var stackTrace = new StackTrace();

                var log = new {Stack = stackTrace.ToString(), Changes = changeInfo};

                var logStr = JsonConvert.SerializeObject(log);

                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(logStr));
            }
             */
            

            foreach (var changedEntry in changedEntries)
            {
                if (changedEntry.Entity.GetType().GetInterfaces().Contains(typeof(ICacheable)))
                {
                    if (changedEntry.State == EntityState.Added)
                    {
                        changeTracker.EntityInserted(changedEntry.Entity);
                    }
                    else if (changedEntry.State == EntityState.Modified)
                    {
                        changeTracker.EntityUpdated(changedEntry.Entity);
                    }
                }
            }
        }

        public override int SaveChanges()
        {
            foreach (DbEntityEntry ent in ChangeTracker.Entries().Where(p => p.State == EntityState.Added || p.State == EntityState.Modified))
            {
                this.IgnoreDateTimeOffset(ent.Entity);
                this.SetLastUpdated(ent.Entity);
                this.UpdateAudit(ent.Entity, ent.State == EntityState.Modified);
            }

            this.TrackChanges();
            
            return base.SaveChanges();
        }
    }
}