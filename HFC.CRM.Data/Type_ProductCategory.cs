﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.Type_ProductCategory")]
    public class Type_ProductCategory : BaseEntity
    {
        [Key]
        public int ProductCategoryId { get; set; }
        public string ProductCategory { get; set; }
        public int Parant { get; set; }
        public bool IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public int? PICGroupId { get; set; }
        public int? PICProductId { get; set; }
        public int BrandId { get; set; }
        public string SalesTaxCode { get; set; }
        public int ProductTypeId { get; set; }
        [NotMapped]
        public string temp_id { get; set; }
        [NotMapped]
        public string Parant_temp { get; set; }
    }
}
