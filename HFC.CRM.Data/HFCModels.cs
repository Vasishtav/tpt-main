﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.HFCModels")]
    public class HFCModels
    {
        [Key]
        public int ModelKey { get; set; }
        public string Model { get; set; }
        public string Description { get; set; }
        public int ProductId { get; set; }
        public int PICProductId { get; set; }
        public int? VendorId { get; set; }
        public bool Active { get; set; }
        public bool USActive { get; set; }
        public bool CAActive { get; set; }
        public string WarningMessage { get; set; }
        [NotMapped]
        public bool IsManualFLag{ get; set; }
        public DateTime? DiscontinuedDate { get; set; }
        public DateTime? PhasedoutDate { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public int? LastUpdatedBy { get; set; }
        [NotMapped]
        public string Vendor { get; set; }
        [NotMapped]
        public string PICVendorId { get; set; }
        [NotMapped]
        public string ProductName { get; set; }
        [NotMapped]
        public int ProductGroup { get; set; }
        [NotMapped]
        public string ProductGroupDesc { get; set; }
        [NotMapped]
        public string Prompts { get; set; }
        [NotMapped]
        public int BrandId { get; set; }
        [NotMapped]
        public string AuditTrail { get; set; }


    }
    [Table("CRM.HFCModelsArchive")]
    public class HFCModelsArchive
    {
        [Key]
        public int Id { get; set; }
        public int ModelKey { get; set; }
        public string OldModelJson { get; set; }
        public string NewModelJson { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
       
    }

    [Table("History.HFCModelFlags")]
    public class HFCModelFlags
    {
        [Key]
        public int Id { get; set; }
        public int ModelKey { get; set; }
        public string FieldName { get; set; }
        public bool Oldvalue { get; set; }
        public bool NewValue { get; set; }
        public DateTime ChangedOn { get; set; }
        public int ChangedBy { get; set; }

    }
}