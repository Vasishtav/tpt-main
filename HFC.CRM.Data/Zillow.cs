﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
  public partial  class Zillow
    {
        public string yearBuilt { get; set; }
        public string lotSizeSqFt { get; set; }
        public string finishedSqFt { get; set; }
        public string bathrooms { get; set; }
        public string bedrooms { get; set; }
        public string lastSoldDate { get; set; }
        public string lastSoldPrice { get; set; }
        public string zestimate { get; set; }
        public string zillowlink { get; set; }
    }
}
