
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HFC.CRM.Data
{
    [Table("CRM.Type_Channel")]
    public partial class Type_Channel
    {
        [Key]
        public int ChannelId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<int> BrandId { get; set; }
        public System.DateTimeOffset CreatedOn { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

        //public virtual Subsource Subsource { get; set; }
    }
}
