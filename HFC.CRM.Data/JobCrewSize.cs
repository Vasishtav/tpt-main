
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HFC.CRM.Data
{
    [Table("CRM.JobCrewSize")]
    public partial class JobCrewSize : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int BrandId { get; set; }
        public string HTML { get; set; }
    }
 }
