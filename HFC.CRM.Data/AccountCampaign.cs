﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
   public partial class AccountCampaign
   {
        public int AccountCampaignId { get; set; }
        public int MktCampaignId { get; set; }
        public int AccountId { get; set; }
        public System.DateTime CreatedOnUtc { get; set; }
        public bool IsManuallyAdded { get; set; }

        public virtual AccountTP AccountTP { get; set; }
    }
}
