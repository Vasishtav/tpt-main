﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace HFC.CRM.Data
{
    public class RegexUtil
    {
        public enum PhoneFormat { NoFormat, Hyphens, Parenthesis };

        /// <summary>
        /// Determine if the street is a valid address
        /// </summary>
        /// <param name="street"></param>
        /// <returns></returns>
        public static bool IsValidStreetAddress(string street)
        {
            if (!string.IsNullOrEmpty(street))
                return Regex.IsMatch(street, Data.Constants.RegexPatterns.StreetAddress, RegexOptions.IgnoreCase);
            else
                return false;
        }

        /// <summary>
        /// Determine if the PO Box is a valid address
        /// </summary>
        /// <param name="pobox"></param>
        /// <returns></returns>
        public static bool IsValidPOBox(string pobox)
        {
            if (!string.IsNullOrEmpty(pobox))
                return Regex.IsMatch(pobox, Data.Constants.RegexPatterns.PoBoxAddress, RegexOptions.IgnoreCase);
            else
                return false;
        }

        /// <summary>
        /// Determines whether [is valid email] [the specified email].
        /// </summary>
        /// <param name="email">The email.</param>
        public static bool IsValidEmail(string email)
        {
            bool result = false;
            if (!string.IsNullOrEmpty(email))
            {
                return Regex.IsMatch(email, Data.Constants.RegexPatterns.Email, RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
            }
            return result;
        }

        /// <summary>
        /// Determines whether [is valid us phone] [the specified phone].
        /// </summary>
        /// <param name="phone">The phone.</param>
        public static bool IsValidUSPhone(string phone)
        {
            var numbers = GetNumbersOnly(phone);

            return numbers.Length == 10;
        }

        /// <summary>
        /// Determines whether [is valid zip or postal] [the specified zipcode].
        /// </summary>
        /// <param name="zipcode">The zipcode.</param>
        public static bool IsValidZipOrPostal(string zipcode)
        {
            if (!string.IsNullOrEmpty(zipcode))
            {
                Regex zipreg = new Regex(HFC.CRM.Data.Constants.RegexPatterns.ZipOrPostalCode, RegexOptions.IgnoreCase);
                return zipreg.IsMatch(zipcode.Trim());
            }
            else
                return false;
        }

        /// <summary>
        /// Formats the us phone.
        /// </summary>
        /// <param name="phone">The phone.</param>
        /// <param name="format">The format.</param>
        /// <returns>System.String.</returns>
        public static string FormatUSPhone(string phone, PhoneFormat format = PhoneFormat.Parenthesis)
        {
            if (!string.IsNullOrEmpty(phone))
            {
                Regex notANumber = new Regex(@"[^\d]", RegexOptions.Multiline | RegexOptions.IgnoreCase);
                phone = notANumber.Replace(phone, "");
                if (phone.Length == 10)
                {
                    Regex exp = new Regex(@"(\d{3})(\d{3})(\d{4})");
                    if (format == PhoneFormat.Hyphens)
                    {
                        return exp.Replace(phone, "$1-$2-$3");
                    }
                    else if (format == PhoneFormat.Parenthesis)
                    {
                        return exp.Replace(phone, "($1) $2-$3");
                    }
                    else
                        return phone;
                }
                else
                    return phone;
            }
            else
                return phone;
        }

        /// <summary>
        /// Removes all special characters and letters, returns valid numbers only
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>System.String.</returns>
        public static string GetNumbersOnly(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                Regex notANumber = new Regex(@"[^\d]", RegexOptions.Multiline | RegexOptions.IgnoreCase);
                return notANumber.Replace(value, "");
            }
            else
                return null;
        }

        /// <summary>
        /// Extracts the RGB.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="Red">The red.</param>
        /// <param name="Green">The green.</param>
        /// <param name="Blue">The blue.</param>
        /// <param name="Alpha">The alpha.</param>
        /// <returns>System.Boolean.</returns>
        public static bool ExtractRGB(string value, out byte Red, out byte Green, out byte Blue, out decimal Alpha)
        {
            bool success = false;
            Red = 0;
            Green = 0;
            Blue = 0;
            Alpha = 1;

            //goto from https://regex101.com/r/yF0uY1/2 for regex rgb value match check.
            Regex exp = new Regex(@"^(rgb|hsl)(a?)[(]\s*([\d.]+\s*%?)\s*,\s*([\d.]+\s*%?)\s*,\s*([\d.]+\s*%?)\s*(?:,\s*([\d.]+)\s*)?[)]$", RegexOptions.IgnoreCase);
            //Regex exp = new Regex(@"\((?<r>\d{1,3}){1},\s*(?<g>\d{1,3}){1},\s*(?<b>\d{1,3}){1}(?:,(?<a>\d*(?:\.\d+))+)?\)", RegexOptions.IgnoreCase);
            var match = exp.Match(value);
            if (match.Success)
            {
                //success = true;
                //Red = Util.CastObject<byte>(match.Groups["r"].Value);
                //Green = Util.CastObject<byte>(match.Groups["g"].Value);
                //Blue = Util.CastObject<byte>(match.Groups["b"].Value);
                //if (!string.IsNullOrEmpty(match.Groups["a"].Value))
                //{
                //    Alpha = Util.CastObject<decimal>(match.Groups["a"].Value);
                //    if (Alpha < 0)
                //        Alpha = 1;
                //}
                success = true;
                Red = Util.CastObject<byte>(match.Groups["3"].Value);
                Green = Util.CastObject<byte>(match.Groups["4"].Value);
                Blue = Util.CastObject<byte>(match.Groups["5"].Value);
                if (!string.IsNullOrEmpty(match.Groups["6"].Value))
                {
                    Alpha = Util.CastObject<decimal>(match.Groups["6"].Value);
                    if (Alpha < 0)
                        Alpha = 1;
                }
            }
            return success;
        }

        /// <summary>
        /// Gets the mobile browser from user agent
        /// </summary>
        /// <param name="useragent">The useragent.</param>
        /// <returns>HFC.CRM.Data.MobileBrowserEnum.</returns>
        public static MobileBrowserEnum GetMobileBrowser(string useragent)
        {
            if (Regex.IsMatch(useragent, "Android", RegexOptions.IgnoreCase))
                return MobileBrowserEnum.Android;
            else if (Regex.IsMatch(useragent, "BlackBerry", RegexOptions.IgnoreCase))
                return MobileBrowserEnum.BlackBerry;
            else if (Regex.IsMatch(useragent, "iPhone|iPad|iPod", RegexOptions.IgnoreCase))
                return MobileBrowserEnum.iOS;
            else if (Regex.IsMatch(useragent, "Opera Mini", RegexOptions.IgnoreCase))
                return MobileBrowserEnum.Opera;
            else if (Regex.IsMatch(useragent, "IEMobile", RegexOptions.IgnoreCase))
                return MobileBrowserEnum.IE;
            else
                return MobileBrowserEnum.Unknown;
        }

        /// <summary>
        /// Determines whether [is mobile browser] [the specified useragent].
        /// </summary>
        /// <param name="useragent">The useragent.</param>
        public static bool IsMobileBrowser(string useragent)
        {
            return GetMobileBrowser(useragent) != MobileBrowserEnum.Unknown;
        }
    }
}
