﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.MasterPurchaseOrderExtn")]
    public class MasterPurchaseOrderExtn
    {
        [Key]
        public int MpoExtnId { get; set; }
        public int PurchaseOrderId { get; set; }
        public int AccountId { get; set; }
        public int OpportunityId { get; set; }
        public int QuoteId { get; set; }
        public int OrderId { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdateOn { get; set; }
    }
}