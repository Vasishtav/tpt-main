﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    public partial class PoAsync
    {
        [JsonProperty("valid")]
        public bool Valid { get; set; }

        [JsonProperty("error")]
        public List<string> Error { get; set; }

        [JsonProperty("counter")]
        public int Counter { get; set; }
        public IList<Itemobj> Items { get; set; }
        public Properties properties { get; set; }
    }
    public class PICSalesOrderVM
    {
        public bool valid { get; set; }
       // public List<string> error { get; set; }
        //public IList<Itemobj> Items { get; set; }
        public Properties properties { get; set; }
    }

    public class Itemobj
    {
        public int Item { get; set; }
        public IList<string> message { get; set; }
    }

    public class Properties
    {
        public PICOrder Order { get; set; }
        public List<PICItem> Items { get; set; }
        public Purchaseorder[] PurchaseOrders { get; set; }
    }

    public class PICOrder
    {
        public string wo { get; set; }
        public string status { get; set; }
        public string date { get; set; }
        public string dateRequired { get; set; }
        public string customer { get; set; }
        public string po { get; set; }
        public string poDate { get; set; }
        public string sideMark { get; set; }
        public string shName { get; set; }
        public string shAddress1 { get; set; }
        public string shAddress2 { get; set; }
        public string shAddress3 { get; set; }
        public string shAddress4 { get; set; }
        public string shCity { get; set; }
        public string shState { get; set; }
        public string shZip { get; set; }
        public string shCountry { get; set; }
        public string shPhone { get; set; }
        public string shFax { get; set; }
        public string shipVia { get; set; }
        public string contact { get; set; }
        public string site { get; set; }
        public string freight { get; set; }
        public string subTotal { get; set; }
        public string stateTax { get; set; }
        public string total { get; set; }
        public string value { get; set; }
        public string qtyEntered { get; set; }
        public string qtyPacked { get; set; }
        public string qtyShipped { get; set; }
        public string dateCreditOk { get; set; }
        public string datePrintWo { get; set; }
        public string dateReprintWo { get; set; }
        public string datePrintLbl { get; set; }
        public string datePacked { get; set; }
        public string dateShipped { get; set; }
        public string dateInvoice { get; set; }
        public string datePaid { get; set; }
        public string dateCancelled { get; set; }
        public string timeLastUpdate { get; set; }
        public string timeCreditOK { get; set; }
        public string timePrinted { get; set; }
        public string timeReprinted { get; set; }
        public string deliveryDate { get; set; }
        public string notes { get; set; }
        public string comm { get; set; }
        public string addressType { get; set; }
        public string source { get; set; }
        public string inName { get; set; }
        public string inAddress1 { get; set; }
        public string inAddress2 { get; set; }
        public string inAddress3 { get; set; }
        public string inAddress4 { get; set; }
        public string inCity { get; set; }
        public string inState { get; set; }
        public string inZip { get; set; }
        public string timeEntered { get; set; }
        public string dateSubmit { get; set; }
        public string advanceShipEmail { get; set; }
    }

    public partial class PICItem
    {
        public string wo { get; set; }
        public string item { get; set; }
        public string gGroup { get; set; }
        public string product { get; set; }
        public string model { get; set; }
        public string stock { get; set; }
        public string color { get; set; }
        public string qty { get; set; }
        public string qtyPacked { get; set; }
        public string qtyShipped { get; set; }
        public string reliefPurchase { get; set; }
        public string subTotal { get; set; }
        public string value { get; set; }
        public string qtyInvoiced { get; set; }
        public string qtyCancelled { get; set; }
        public string qtyPosted { get; set; }
        public Price[] Prices { get; set; }
        public Tax tax { get; set; }
    }

    public class Price
    {
        public string Description { get; set; }
        public string Retail { get; set; }
        public string Surch { get; set; }
        public string Misc { get; set; }
    }

    public class Purchaseorder
    {
        public string po { get; set; }
        public string status { get; set; }
        public string vendor { get; set; }
        public string date { get; set; }
        public string ordered { get; set; }
        public string received { get; set; }
    }

    public class PICVPOObject
    {
        public bool valid { get; set; }
        public string[] error { get; set; }
        public PICProperties properties { get; set; }
    }

    public class PICProperties
    {
        public VPurchaseorder PurchaseOrder { get; set; }
        public List<VPOItem> Items { get; set; }
    }

    public class VPurchaseorder
    {
        public string po { get; set; }
        public string status { get; set; }
        public string vendor { get; set; }
        public string wo { get; set; }
        public string date { get; set; }
        public string dateBackordered { get; set; }
        public string dateReceived { get; set; }
        public string dateInvoiced { get; set; }
        public string dateCancelled { get; set; }
        public string datePrinted { get; set; }
        public string datePaid { get; set; }
        public string dateClosed { get; set; }
        public string dueDate { get; set; }
        public string priceDiscGoods { get; set; }
        public string priceGoods { get; set; }
        public string shipName { get; set; }
        public string shipAddress1 { get; set; }
        public string shipAddress2 { get; set; }
        public string shipAddress3 { get; set; }
        public string shipAddress4 { get; set; }
        public string shipCity { get; set; }
        public string shipState { get; set; }
        public string shipZip { get; set; }
        public string shipCountry { get; set; }
        public string shipPhone { get; set; }
        public string shipFax { get; set; }
        public string comment { get; set; }
        public string addressType { get; set; }
        public string statusDescription { get; set; }
        public string vendorReference { get; set; }
        public string currency { get; set; }
        public string tax { get; set; }
        public string terms { get; set; }
        public string VendorMsg { get; set; }
        public string dateAcknowledged { get; set; }
    }

    public class VPOItem
    {
        public string po { get; set; }
        public string item { get; set; }
        public string stock { get; set; }
        public string status { get; set; }
        public string qty { get; set; }
        public string qtyOrdered { get; set; }
        public string qtyBackordered { get; set; }
        public string qtyReceived { get; set; }
        public string qtyInvoiced { get; set; }
        public string qtyCancelled { get; set; }
        public string qtyPosted { get; set; }
        public string wo { get; set; }
        public string woItem { get; set; }
        public string discGoods { get; set; }
        public string goods { get; set; }
        public string qtyReturned { get; set; }
        public string qtyClosed { get; set; }
        public string product { get; set; }
        public string model { get; set; }
        public string dateReceived { get; set; }
        public string dateBackorder { get; set; }
        public string dueDate { get; set; }
        public string dateClosed { get; set; }
        public string dateReturned { get; set; }
        public string estShipDate { get; set; }
        public string comment { get; set; }
        public string description { get; set; }
        public string statusDescription { get; set; }
        public string vendorReference { get; set; }
        public int? ediCustomerItem { get; set; }
        public string AckCost { get; set; }
        public string UoM { get; set; }
    }

    public partial class ErrorItem
    {
        public int Item { get; set; }
        public List<string> message { get; set; }
    }

    public partial class Error
    {
        public List<string> Order { get; set; }
        public List<ErrorItem> Items { get; set; }
    }
    public partial class Information
    {
        public List<ErrorItem> Items { get; set; }
    }
    public partial class Warning
    {
        public List<ErrorItem> Items { get; set; }
    }


    public partial class PoAsyncCounter
    {
        public bool valid { get; set; }

        public Error error { get; set; }
        public Information information { get; set; }
        public Warning warning { get; set; }
        public Properties Properties { get; set; }
    }
    public class DynErrorItem
    {
        public int Item { get; set; }
        public List<string> message { get; set; }
    }
    public  class PoAsyncCounterforError
    {
        public bool valid { get; set; }
        public Error error { get; set; }
        public Information information { get; set; }
        public Warning warning { get; set; }
    }
}
