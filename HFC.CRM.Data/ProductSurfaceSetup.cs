﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    //[Table("CRM.ProductSurfacingLabourSetup")]
    public class ProductSurfaceSetup 
    {
        public int MaximumJobSize { get; set; }
        public int MinimumJobSize { get; set; }
        public List<ProductSurfaceSetupForCC> ProductSurfaceSetupForCC { get; set; }
        public List<ProductSurfaceSetupForTL> ProductSurfaceSetupForTL { get; set; }

    }

    public class MultipleSurfaceProduct
    {
        [Key]
        public int Id { get; set; }
        public string ProductID { get; set; }
        public string VendorName { get; set; }
        public int VendorID { get; set; }
        public string VendorSKU { get; set; }
        public string UnitofMeasure { get; set; }
        public string Description { get; set; }
        public int ContainerSize { get; set; }
        public int Coverage { get; set; }
        public decimal Mixratio { get; set; }
        public int WasteReclaim { get; set; }
        public string WasteType { get; set; }
        public decimal CostPrice { get; set; }
        public decimal UnitPrice { get; set; }
    }

    public class ProductSurfaceSetupForCC
    {
        [Key]
        public int Id { get; set; }
        public int Day { get; set; }
        public string Process { get; set; }
        public int SuggestedCrewSize { get; set; }
        public int SuggestedHours { get; set; }
    }

    public class ProductSurfaceSetupForTL
    {
        [Key]
        public int Id { get; set; }
        public int Day { get; set; }
        public string Process { get; set; }
        public string FixedVariable { set; get; }
        public int VariableLaborBasis { set; get; }
        public decimal VariableLaborFactor { get; set; }
        public decimal FixedLaborHours { get; set; }
    }

    public class MultipartAttributes
    {
        public string color1 { get; set; }
        public string color2 { get; set; }
        public string color3 { get; set; }
        public string color4 { get; set; }
        public string color5 { get; set; }
        public string color6 { get; set; }
        public string color7 { get; set; }
        public string color8 { get; set; }
        public string color9 { get; set; }
        public string color10 { get; set; }
        public string color11 { get; set; }
        public string color12 { get; set; }
        public string color13 { get; set; }
        public string color14 { get; set; }
        public string color15 { get; set; }
        public string color16 { get; set; }
        public bool colorEnabled { get; set; }
        public string finishes1 { get; set; }
        public string finishes2 { get; set; }
        public string finishes3 { get; set; }
        public string finishes4 { get; set; }
        public string finishes5 { get; set; }
        public string finishes6 { get; set; }
        public string finishes7 { get; set; }
        public string finishes8 { get; set; }
        public string finishes9 { get; set; }
        public string finishes10 { get; set; }
        public string finishes11 { get; set; }
        public string finishes12 { get; set; }
        public string finishes13 { get; set; }
        public string finishes14 { get; set; }
        public string finishes15 { get; set; }
        public string finishes16 { get; set; }
        public bool finishesEnabled { get; set; }
    }
}
