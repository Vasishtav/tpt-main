﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    public class PICAccessToken
    {
        public AccessToken token { get; set; }


        public DateTime expires_at
        {
            get; set;
        }
    }

    public class AccessToken{
        public string access_token { get; set; }
        public int expires_in { get; set; }
        public string token_type { get; set; }
        public object scope { get; set; }
    }
}
