﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("Auth.PermissionSet")]
    public class PermissionSetTP:BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public string PermissionSetName { get; set; }

        public string Description { get; set; }
        public Nullable<int> FranchiseId { get; set; }

        public bool IsActive { get; set; }
       // public List<PermissionTPT> PermissionTPTS { get; set; }
    }
}
