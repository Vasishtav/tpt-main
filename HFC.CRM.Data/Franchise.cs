using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace HFC.CRM.Data
{

    [Table("CRM.Franchise")]
    public partial class Franchise
    {
        public Franchise()
        {
            this.Territories = new HashSet<Territory>();
            this.FranchiseOwners = new HashSet<FranchiseOwner>();
            this.Calendars = new HashSet<Calendar>();
            this.Tasks = new HashSet<Task>();
            this.AppConfigs = new HashSet<AppConfig>();
            this.Leads = new HashSet<Lead>();
            this.Users = new HashSet<User>();
            this.FranchiseProducts = new HashSet<FranchiseProduct>();
            this.FranchiseTaxRules = new HashSet<FranchiseTaxRule>();
            this.MarketingCampaigns = new HashSet<MarketingCampaign>();
            this.FranchiseSocials = new HashSet<FranchiseSocial>();
            this.Apps = new HashSet<QBApp>();
            this.ProductStyles = new HashSet<ProductStyle>();
            this.Type_OrderStatus = new HashSet<Type_OrderStatus>();
            this.ManufacturerContacts = new HashSet<ManufacturerContact>();
            this.ProductMutipliers = new HashSet<ProductMutiplier>();
            this.Type_Appointments = new HashSet<Type_Appointments>();
            this.Sources = new HashSet<Source>();
            this.FranchiseInvoiceOptions = new HashSet<FranchiseInvoiceOptions>();
            this.EmailTemplates = new HashSet<EmailTemplate>();
            this.UserWidgets = new HashSet<UserWidget>();
            this.SentEmails = new HashSet<SentEmail>();
            this.lstOwnerName = new List<OwnerVM>();
        }

        [Key]
        public int FranchiseId { get; set; }
        public System.Guid FranchiseGuid { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Website { get; set; }
        private System.DateTime _CreatedOnUtc = DateTime.Now;
        public System.DateTime CreatedOnUtc {
            get { return _CreatedOnUtc; }
            set { _CreatedOnUtc = DateTime.SpecifyKind(value, DateTimeKind.Unspecified); }
        }
        public bool IsDeleted { get; set; }
        public HFC.CRM.Data.TimeZoneEnum TimezoneCode { get; set; }
        public string LogoImgUrlRelativePath { get; set; }
        public Nullable<int> AddressId { get; set; }
        public string TollFreeNumber { get; set; }
        public string LocalPhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string SupportEmail { get; set; }
        public string AdminEmail { get; set; }
        public string OwnerEmail { get; set; }
        public int QuoteExpiryDays { get; set; }

        //start:Newly added field
        public string DBAName { get; set; }
        public string DMA { get; set; }
        public bool IsAddressSame { get; set; }
        public string AdminName { get; set; }
        public string CoachName { get; set; }
        public string Region { get; set; }
        public string BusinessPhone { get; set; }
        public string Currency { get; set; }
        public int OwnerId { get; set; }
        public List<OwnerVM> lstOwnerName{get;set;}
        // end

        private Nullable<System.DateTime> _LastUpdatedUtc;
        public Nullable<System.DateTime> LastUpdatedUtc {
            get { return _LastUpdatedUtc; }
            set {
                if (value == null)
                    _LastUpdatedUtc = null;
                else
                    _LastUpdatedUtc = DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified);
            }
        }
        public Nullable<int> LastUpdatedByPersonId { get; set; }
        public Nullable<bool> isSolaTechEnabled { get; set; }
        public Nullable<bool> IsSuspended { get; set; }
        public bool EnableTexting { get; set; }
        public bool EnableReminders { get; set; }
        public Nullable<bool> EnableUnreadMessagesCounter { get; set; }
        public Nullable<bool> AppTypeColor { get; set; }
        public Nullable<bool> EnableSaveSearch { get; set; }
        public Nullable<bool> RestrictInstallerRole { get; set; }
        public Nullable<bool> RestrictSalesRole { get; set; }

        public Nullable<int> MailingAddressId { get; set; }

        public int DeactivationReasonId { get; set; }
        public string DeactivationAdditionalReason { get; set; }
        [NotMapped]
        public int totalRecords { get; set; }
        [NotMapped]
        public bool? IsQBEnabled { get; set; }
        public bool FEPrintOption { get; set; }
        public int BuyerremorseDay {get;set;}

        public System.DateTime? TPTGoLiveDate { get; set; }
      //[NotMapped]
      public string OwnerName { get; set; }
        public virtual ICollection<Territory> Territories { get; set; }
        public virtual Address Address { get; set; }
        public virtual Address MailingAddress { get; set; }
        public virtual ICollection<FranchiseOwner> FranchiseOwners { get; set; }
        public virtual ICollection<Calendar> Calendars { get; set; }
        public virtual ICollection<Task> Tasks { get; set; }
        public virtual ICollection<AppConfig> AppConfigs { get; set; }
        public virtual ICollection<Lead> Leads { get; set; }
        public virtual ICollection<User> Users { get; set; }
        public virtual ICollection<FranchiseProduct> FranchiseProducts { get; set; }
        public virtual ICollection<FranchiseTaxRule> FranchiseTaxRules { get; set; }
        public virtual ICollection<MarketingCampaign> MarketingCampaigns { get; set; }
        public virtual ICollection<FranchiseSocial> FranchiseSocials { get; set; }
        public virtual ICollection<QBApp> Apps { get; set; }
        public virtual ICollection<ProductStyle> ProductStyles { get; set; }
        public virtual ICollection<Type_OrderStatus> Type_OrderStatus { get; set; }
        public virtual ICollection<ManufacturerContact> ManufacturerContacts { get; set; }
        public virtual ICollection<ProductMutiplier> ProductMutipliers { get; set; }
        public virtual ICollection<Type_Appointments> Type_Appointments { get; set; }
        public virtual ICollection<Source> Sources { get; set; }
        public virtual ICollection<FranchiseInvoiceOptions> FranchiseInvoiceOptions { get; set; }
        public virtual ICollection<EmailTemplate> EmailTemplates { get; set; }
        public virtual ICollection<UserWidget> UserWidgets { get; set; }
        public virtual ICollection<SentEmail> SentEmails { get; set; }
        public bool EnableCase { get; set; }
        public bool EnableVendorManagement { get; set; }
        public bool EnableElectronicSignAdmin { get; set; }
        public bool EnableElectronicSignFranchise { get; set; }
        public bool EnableTax { get; set; }
    }
}
