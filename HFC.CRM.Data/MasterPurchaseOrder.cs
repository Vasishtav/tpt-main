﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.MasterPurchaseOrder")]
    public class MasterPurchaseOrder
    {
        [Key]
        public int PurchaseOrderId { get; set; }

        public string MasterPONumber { get; set; }
        [NotMapped]
        public int AccountId { get; set; }
        [NotMapped]
        public int OpportunityId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? SubmittedDate { get; set; }
        public int? SubmittedBy { get; set; }
        [NotMapped]
        public int OrderId { get; set; }
        public int PICPO { get; set; }
        [NotMapped]
        public int QuoteId { get; set; }
        public int POStatusId { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdateOn { get; set; }
        public bool SubmittedTOPIC { get; set; }
        public bool ConfirmAddress { get; set; }
        public bool DropShip { get; set; }
        public int? ShipToLocation { get; set; }
        public int? TerritoryId { get; set; }
        public int? ShipAddressId { get; set; }
        public int? CounterId { get; set; }
        public string Message { get; set; }

        [NotMapped]
        public string AccountName { get; set; }

        [NotMapped]
        public string OpportunityName { get; set; }

        [NotMapped]
        public List<PurchaseOrderDetails> PurchaseOrderDetails { get; set; }

        [NotMapped]
        public string OrderNumber { get; set; }

        [NotMapped]
        public string Status { get; set; }

        [NotMapped]
        public int Quantity { get; set; }

        [NotMapped]
        public decimal Total { get; set; }

        public string PICResponse { get; set; }

        [NotMapped]
        public List<MPOHeader> MPOHeaderTable { get; set; }

        [NotMapped]
        public List<VPODetail> CoreProductVPOs { get; set; }

        [NotMapped]
        public List<VPODetail> MyProductVPOs { get; set; }

        [NotMapped]
        public List<VPODetail> VpoDetails { get; set; }

        [NotMapped]
        public string RequestObject { set; get; }

        [NotMapped]
        public string Address { set; get; }

        [NotMapped]
        public bool IsValidated { get; set; }

        public List<VendorTerritoryAccount> VendorTerritoryAccountList { get; set; }

        [NotMapped]
        public AddressTP DropShipAddress { get; set; }

        [NotMapped]
        public List<ShipToLocations> ShipToLocationList { get; set; }

        [NotMapped]
        public string SideMark { get; set; }

        // TP-1698: CLONE - Opportunities and related need record level permissions
        // Used to filter the records for specific user
        // based on sales agent id and installation user id
        [NotMapped]
        public int SalesAgentId { get; set; }

        [NotMapped]
        public int InstallerId { get; set; }

        public string CancelReason { get; set; }

        [NotMapped]
        public string Territory { get; set; }

        public Nullable<int> ImpersonatorPersonId { get; set; }

        public List<VPO> CoreProductGroupVpos { get; set; }
        public List<VPO> MyProductGroupVpos { get; set; }

        [NotMapped]
        public string SalesAgent { get; set; }

        [NotMapped]
        public string HomePhone { get; set; }

        [NotMapped]
        public string CellPhone { get; set; }

        [NotMapped]
        public string WorkPhone { get; set; }

        [NotMapped]
        public string PrimaryEmail { get; set; }

        [NotMapped]
        public string PreferredTFN { get; set; }

        [NotMapped]
        public int InstallationAddressId { get; set; }

        [NotMapped]
        public string OrderName { get; set; }

        [NotMapped]
        public AddressTP InstallAddress { get; set; }

        [NotMapped]
        public bool IsNotifyemails { get; set; }
        [NotMapped]
        public bool IsNotifyText { get; set; }
        public bool IsXMpo { get; set; }
        public int? VendorId { get; set; }
    }

    public class MPOHeader
    {
        public string VendorName { get; set; }
        public int TotalLines { get; set; }
        public int TotalQuantity { get; set; }
        public DateTime? ShipDate { get; set; }
        public string Status { get; set; }
        public int LineNumber { get; set; }
        public int PICPO { get; set; }
    }

    public class VPO
    {
        public string PurchaseOrder { get; set; }
        public string VendorName { get; set; }
        public string VendorOrderNumber { get; set; }
        public int PICPO { get; set; }
        public List<Details> Shipment { get; set; }
        public List<Details> VendorInvoice { get; set; }
        public List<VPODetail> VPODetails { get; set; }
    }

    public class VPODetail
    {
        public int PurchaseOrdersDetailId { get; set; }
        public int PurchaseOrderId { get; set; }
        public int QuoteLineId { get; set; }
        public string ProductName { get; set; }
        public string PartNumber { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }

        [NotMapped]
        public string NotesVendor { get; set; }

        public decimal Cost { get; set; }
        public DateTime? ShipDate { get; set; }
        public DateTime? PromiseByDate { get; set; }
        public string Status { get; set; }
        public int StatusId { get; set; }
        public string PONumber { get; set; }

        //public int PurchaseOrdersDetailId { get; set; }
        public string VendorPartNumber { get; set; }

        public int purchased { get; set; }
        public string PickedBy { get; set; }
        public int ProductTypeId { get; set; }
        public string VendorName { get; set; }

        [NotMapped]
        public int PICPO { get; set; }

        public string POResponse { get; set; }
        public string Errors { get; set; }
        public List<ShipNotice> ShipNotices { get; set; }
        public int LineNumber { get; set; }
        public string Warning { get; set; }
        public string Information { get; set; }
        public decimal? ExtendedPrice { get; set; }
        public string vendorReference { get; set; }
        public string CancelReason { get; set; }
        //mpo grid
        public double? Width { get; set; } = 0;
        public double? Height { get; set; } = 0;
        public string FranctionalValueWidth { get; set; }
        public string FranctionalValueHeight { get; set; }
        [NotMapped]
        public int OrderId { get; set; }
        public int? CounterId { get; set; }
        public bool? ValidConfiguration { get; set; }
        public string ProductErrors { get; set; }
        public DateTime? LastValidatedon { get; set; }
        [NotMapped]
        public string WindowName { get; set; }
    }

    public class Details
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}