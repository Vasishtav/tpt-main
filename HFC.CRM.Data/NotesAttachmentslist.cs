/***************************************************************************\
Module Name:  NotesAttachmentslist
Project: HFC
Created on: 15 Oct 2017
Created By:
Copyright:
Description: Save Notes and Attachment
Change History:
Date  By  Description

\***************************************************************************/

namespace HFC.CRM.Data
{
    using System;
    using System.Collections.Generic;

    public partial class NotesAttachmentslist
    {   
        //Added for Notes and Notes Attachment
        public int LeadId { get; set; }
        public string Title { get; set; }
        public string Note { get; set; }
        public string FileName { get; set; }
        public string Category { get; set; }
        public string FullFileName { get; set; }
        public string Message { get; set; }
        public int TypeEnum { get; set; }

        private System.DateTime _CreatedOnUtc = DateTime.Now;
        public System.DateTime CreatedOnUtc
        {
            get { return _CreatedOnUtc; }
            set { _CreatedOnUtc = DateTime.SpecifyKind(value, DateTimeKind.Unspecified); }
        }
        public int CreatedByPersonId { get; set; }

        public int Id { get; set; }
    }
}
