﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("Acct.HFCVendors")]
    public partial class HFCVendor : BaseEntity
    {
        [Key]
        public int VendorIdPk { get; set; }

        public int VendorId { get; set; }
        public int VendorType { get; set; }
        public string Name { get; set; }
        public string AccountRep { get; set; }
        public string AccountRepPhone { get; set; }
        public string AccountRepEmail { get; set; }
        public string OrderingMethod { get; set; }
        public string ContactDetails { get; set; }
        public int? AddressId { get; set; }
        public bool? IsActive { get; set; }
        public string PICVendorId { get; set; }

        [NotMapped]
        public AddressTP VendorAddress { get; set; }

        public string BBDisplayName { get; set; }
        public string TLDIsplayName { get; set; }
        public string CCDisplayName { get; set; }
    }
}