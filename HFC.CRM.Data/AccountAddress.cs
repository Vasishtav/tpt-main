﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    public partial class AccountAddress
    {
        public int AccountId { get; set; }
        public int AddressId { get; set; }

        public virtual AccountTP AccountTP { get; set; }
    }
}
