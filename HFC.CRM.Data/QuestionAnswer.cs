/***************************************************************************\
Module Name:  QuestionAnswer
Project: HFC
Created on: 15 Oct 2017
Created By:
Copyright:
Description: Store the Answers
Change History:
Date  By  Description

\***************************************************************************/

namespace HFC.CRM.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class QuestionAnswer
    {
        public int AnswerId { get; set; }
        public int JobId { get; set; }
        public int QuestionId { get; set; }
        public string Answer { get; set; }
        private System.DateTime _CreatedOnUtc = DateTime.Now;
    	public System.DateTime CreatedOnUtc { 
    		get { return _CreatedOnUtc; } 
    		set { _CreatedOnUtc = DateTime.SpecifyKind(value, DateTimeKind.Unspecified); } 
    	}
        public Nullable<int> LoggedByPersonId { get; set; }
        private Nullable<System.DateTime> _LastUpdatedOnUtc;
    	public Nullable<System.DateTime> LastUpdatedOnUtc { 
    		get { return _LastUpdatedOnUtc; } 
    		set { 
    			if(value == null) 
    				_LastUpdatedOnUtc = null; 
    			else 
    				_LastUpdatedOnUtc = DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified); 
    		} 
    	}
        public virtual Job Job { get; set; }
        public virtual Type_Question Questions { get; set; }
        public int LeadId { get; set; }
    }
}
