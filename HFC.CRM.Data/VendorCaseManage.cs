﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.VendorCaseManage")]
    public class VendorCaseManage : BaseEntity
    {
        [Key]
        public int VendorCaseId { get; set; }
        public int CaseLineId { get; set; }
        public int? TripChargeApproved { get; set; }
        public int? Resolution { get; set; }
        public int? Status { get; set; }
        //public DateTime CreatedOn { get; set; }
        //public int CreatedBy { get; set; }
        //public DateTime? UpdatedOn { get; set; }
        //public int? UpdatedBy { get; set; }
    }
}
