﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System;
using System.Collections.Generic;
namespace HFC.CRM.Data
{
    [Table("CRM.Opportunities")]
    public class Opportunity
    {
        [Key]
        public int OpportunityId { get; set; }
        public string OpportunityName { get; set; }
        [NotMapped]
        public string CampaignName { get; set; }
        [NotMapped]
        public bool IsCommercial { get; set; }
        public string SideMark { get; set; }
        public Nullable<int> SalesAgentId { get; set; }
        public Nullable<int> InstallerId { get; set; }
        public Nullable<int> InstallationAddressId { get; set; }
        public Nullable<int> BillingAddressId { get; set; }
        public string Description { get; set; }
        public int LeadId { get; set; }
        public int AccountId { get; set; }
        [NotMapped]
        public bool SkipDuplicateCheck { get; set; }

        public System.Guid OpportunityGuid { get; set; }
        public int OpportunityNumber { get; set; }
        public short OpportunityStatusId { get; set; }

        public Nullable<int> FranchiseId { get; set; }
        public Nullable<int> TerritoryId { get; set; }
        public string TerritoryType { get; set; }
        public Nullable<int> PersonId { get; set; }
        [NotMapped]
        public Nullable<int> SecPersonId { get; set; }


        public virtual ICollection<LeadAccount> LeadAccount { get; set; }
        public virtual ICollection<User> SalesAgent { get; set; }
        public virtual ICollection<User> Installer { get; set; }
        //public virtual ICollection<JobQuote> Quotes { get; set; }
        public virtual ICollection<Order> Orders { get; set; }

        [NotMapped]
        public string OpportunityStatusName { get; set; }

        public string InstallationAddress { get; set; }


        [NotMapped]
        public Nullable<int> CustomerId { get; set; }
        [NotMapped]
        public string OpportunitySourceId { get; set; }

        [NotMapped]
        public int SourcesTPId { get; set; }

        [NotMapped]
        public int SourcesTPIdFromCampaign { get; set; }

        public int? CampaignId { get; set; }
        public ICollection<Note> Note { get; set; }
        [NotMapped]
        public Nullable<int> SecCustomerId { get; set; }
        [NotMapped]
        public int QuickDisposition { get; set; }
        public Disposition Disposition { get; set; }
        public Nullable<int> DispositionId { get; set; }
        public Nullable<int> TypeId { get; set; }
        public bool IsDeleted { get; set; }
        private System.DateTime _CreatedOnUtc = DateTime.Now;
        public System.DateTime CreatedOnUtc
        {
            get { return _CreatedOnUtc; }
            set { _CreatedOnUtc = DateTime.SpecifyKind(value, DateTimeKind.Unspecified); }
        }
        [NotMapped]
        public bool CanUpdate { get; set; }
        [NotMapped]
        public bool CanDelete { get; set; }
        [NotMapped]
        public bool CanUpdateStatus { get; set; }

        public string ReferringCustomerId { get; set; }
        public Nullable<int> CreatedByPersonId { get; set; }
        private Nullable<System.DateTime> _LastUpdatedOnUtc;
        public Nullable<System.DateTime> LastUpdatedOnUtc
        {
            get { return _LastUpdatedOnUtc; }
            set
            {
                if (value == null)
                    _LastUpdatedOnUtc = null;
                else
                    _LastUpdatedOnUtc = DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified);
            }
        }
        public Nullable<int> LastUpdatedByPersonId { get; set; }
        public string Notes { get; set; }
        public virtual ICollection<AttachmentInfo> Attachments { get; set; }
        public virtual ICollection<PhysicalFile> PhysicalFiles { get; set; }
        public virtual ICollection<Calendar> Calendars { get; set; }
        public virtual Franchise Franchise { get; set; }
        [NotMapped]
        public virtual Person PrimPerson { get; set; }

        [NotMapped]
        public virtual ICollection<Person> SecPerson { get; set; }
        [NotMapped]
        public virtual ICollection<CustomerTP> SecCustomer { get; set; }
        public virtual Type_OpportunityStatus OpportunityStatus { get; set; }
        public virtual ICollection<Task> Tasks { get; set; }
        // Changed from referring Address to AddressTP - murugan
        public virtual ICollection<AddressTP> Addresses { get; set; }
        public virtual ICollection<EditHistory> EditHistories { get; set; }
        public virtual ICollection<Job> Jobs { get; set; }
        public virtual ICollection<OpportunityNote> OpportunityNotes { get; set; }
        public virtual ICollection<Note> OpportunityNotesTP { get; set; }

        public virtual ICollection<OpportunityAdditionalInfo> OpportunityAdditionalInfo { get; set; }
        public virtual Territory Territory { get; set; }
        public virtual ICollection<SourceList> SourcesList { get; set; }
        public virtual ICollection<OpportunitySource> OpportunitySources { get; set; }
        public virtual ICollection<SentEmail> SentEmails { get; set; }

        //Added for new notes and file attachment
        public virtual ICollection<NotesAttachmentslist> NotesAttachmentslist { get; set; }

        //Added for Qualification/Question Answers
        [NotMapped]
        public List<Type_Question> OpportunityQuestionAns { get; set; }

        [NotMapped]
        public string SelectedOpportunityStatus { get; set; }
        [NotMapped]
        public string selectedSalesAgent { get; set; }
        [NotMapped]
        public string selectedInstaller { get; set; }
        [NotMapped]
        public string selectedInstallationAddress { get; set; }
        [NotMapped]
        public string selectedAccount { get; set; }

        // Rajkumar
        [NotMapped]
        public virtual AccountTP AccountTP { get; set; }
        public virtual CustomerTP PrimCustomer { get; set; }
        [NotMapped]
        public virtual AddressTP AccountBillingAddressTP { get; set; }
        [NotMapped]
        public virtual AddressTP InstallationAddressTP { get; set; }

        // Tax Exempt
        //[NotMapped]
        public bool IsTaxExempt { get; set; }
        //[NotMapped]
        public string TaxExemptID { get; set; }

        public bool IsPSTExempt { get; set; }
        public bool IsGSTExempt { get; set; }
        public bool IsHSTExempt { get; set; }
        public bool IsVATExempt { get; set; }
        public bool IsNewConstruction { get; set; }

        [NotMapped]
        public int? RelatedAccountId { get; set; }
        public Nullable<int> ImpersonatorPersonId { get; set; }

        public int? TerritoryDisplayId { get; set; }

        [NotMapped]
        public string DisplayTerritoryName { get; set; }
        [NotMapped]
        public bool SendMsg { get; set; }
        [NotMapped]
        public bool UncheckNotifyviaText { get; set; }
        public DateTime? ReceivedDate { get; set; }


    }
}
