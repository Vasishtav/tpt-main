﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
namespace HFC.CRM.Data
{
    [Table("[CRM].[Type_ReverseReasonCode]")]
    public class Type_ReverseReasonCode : BaseEntity
    {
        [Key]
        public int ReasonCodeId { get; set; }
        public string Reason { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        

    }
}
