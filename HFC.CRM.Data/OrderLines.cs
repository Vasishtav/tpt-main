﻿using System;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.OrderLines")]
    public partial class OrderLines : BaseEntity
    {
        [Key]
        public int OrderLineID { get; set; }
        public int? QuoteKey { get; set; }
        public int? QuoteLineId { get; set; }
        public int? OrderID { get; set; }
    }
}
