﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.VendorCaseAttachmentDetails")]
    public class VendorCaseAttachmentDetails
    {
        [Key]
        public int VendorCaseAttachmentId { get; set; }
        public int VendorCaseId { get; set; }
        public int? VendorCaseDetailId { get; set; }
        public string fileIconType { get; set; }
        public string fileSize { get; set; }
        public string fileName { get; set; }
        public string color { get; set; }
        public string streamId { get; set; }
        public string streamId1 { get; set; }
    }
}
