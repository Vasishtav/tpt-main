﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    public class MeasurementLineItemBB : MeasurementLineItemBase
    {
        public int id { get; set; }
        public string RoomLocation { get; set; }
        public string RoomName { get; set; }
        public string WindowLocation { get; set; }
        public string MountTypeName { get; set; }
        public string fileName { get; set; }
        public string Stream_id { get; set; }
        public string thumb_Stream_id { get; set; }
        public int QuoteLineId { get; set; }
        public int QuoteLineNumber { get; set; }
        public bool isAdded { get; set; }
        public bool NotSave { get; set; }
    }

    public class MeasurementLineItemTL : MeasurementLineItemBase
    {
        public int id { get; set; }
        public string System { get; set; }
        public string SystemDescription { get; set; }
        public string StorageType { get; set; }
        public string StorageDescription { get; set; }
        public string Depth { get; set; }
        public string FranctionalValueDepth { get; set; }
        public string CustomData { get; set; }
        public int? Qty { get; set; }
        public string Area { get; set; }
        public string Moisture { get; set; }
        public List<SquareFeetData> SquareFeetAdd { get; set; }
        public List<SquareFeetData> SquareFeetSub { get; set; }
        public string fileName { get; set; }
        public string Stream_id { get; set; }
        public string thumb_Stream_id { get; set; }
        public bool NotSave { get; set; }
    }

    public class MeasurementLineItemCC
    {
        public int id { get; set; }
        public string System { get; set; }
        public string SystemDescription { get; set; }
        public string Area { get; set; }
        public string Moisture { get; set; }
        public int? Qty { get; set; }
        public string Comments { get; set; }
        public List<SquareFeetData> SquareFeetAdd { get; set; }
        public List<SquareFeetData> SquareFeetSub { get; set; }
        public string fileName { get; set; }
        public string Stream_id { get; set; }
        public string thumb_Stream_id { get; set; }
        public bool NotSave { get; set; }
    }
    public class measurementfiles
    {
        public int id { get; set; }
        public byte[] base64 { get; set; }
        public string name { get; set; }
    }

    public class SquareFeetData
    {
        public int id { get; set; }
        public string Description { get; set; }
        public string Shape { get; set; }
        public int Quantity { get; set; }
        public decimal Measurement1 { get; set; }
        public Nullable<decimal> Measurement2 { get; set; }
        public string Dimension1 { get; set; }
        public string Dimension2 { get; set; }
        public string Area { get; set; }
    }
}
