﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("crm.ShipmentDetail")]
    public class ShipmentDetail
    {
        [Key]
        public int ShipmentDetailId { get; set; }
        [NotMapped]
        public int LineNumber { get; set; }
        public int ShipNoticeId { get; set; }
        public string ShipmentId { get; set; }
        public int? QuoteLineId { get; set; }
        [NotMapped]
        public string ItemDescription { get; set; }
        public string POItem { get; set; }
        public string ProductNumber { get; set; }
        public int QuantityShipped { get; set; }
        public string BoxId { get; set; }
        public string TrackingId { get; set; }
        public string TrackingUrl { get; set; }
        public int Status { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
        [NotMapped]
        public Type_ShipmentStatus Type_ShipmentStatus { get; set; }
        [NotMapped]
        public int PICVpo { get; set; }
        public int QuantityReceived { get; set; }

        public string Notes { get; set; }

        [NotMapped]
        public string SideMark { get; set; }

        public string PackingSlip { get; set; }

        public string UomCode { get; set; }

        [NotMapped]
        public string UomDefinition { get; set; }

        public int? QuantityShortShipped { get; set; }

        public int? QuantityMisShipped { get; set; }

        public int QuantityDamaged { get; set; }

        [NotMapped]
        public IList<int> Statuses {get;set;}

        [NotMapped]
        public int SNO { get; set; }

        [NotMapped]
        public int OrderId { get; set; }
        [NotMapped]
        public int OrderNumber { get; set; }
    }

    [Table("crm.ShipmentStatus")]
    public class ShipmentStatus
    {
        public int ShipmentDetailId { get; set; }
        public int ShipmentStatusId { get; set; }
    }
}
