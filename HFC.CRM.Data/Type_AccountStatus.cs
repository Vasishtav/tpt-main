﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

using System.Collections.Generic;
namespace HFC.CRM.Data
{
    [Table("CRM.Type_AccountStatus")]
    public partial class Type_AccountStatus
    {

        public Type_AccountStatus()
        {
            this.Children = new HashSet<Type_AccountStatus>();
        }

        [Key]
        public int AccountStatusId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<int> BrandId { get; set; }
        public Nullable<int> FranchiseId { get; set; }
        public Nullable<int> DisplayOrder { get; set; }
        public string ClassName { get; set; }
        public Nullable<bool> isDeleted { get; set; }
        public Nullable<System.DateTime> Createdon { get; set; }
        public Nullable<System.DateTime> Updatedon { get; set; }

        public Nullable<int> CreatedBy { get; set; }
        public Nullable<int> UpdatedBy { get; set; }

        public virtual Type_AccountStatus Parent { get; set; }
        
        public virtual Franchise Franchise { get; set; }

        //Added model creation
        public virtual ICollection<Type_AccountStatus> Children { get; set; }
    }
}
