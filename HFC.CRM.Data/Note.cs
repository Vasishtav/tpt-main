using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace HFC.CRM.Data
{
    [Table("CRM.Note")]
    public partial class Note
    {
        [Key]
        public int NoteId { get; set; }
        public Nullable<int> AccountId { get; set; }
        public Nullable<int> LeadId { get; set; }
        public Nullable<int> QuoteId { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public Nullable<int> OpportunityId { get; set; }
        public Nullable<int> OrderId { get; set; }
        public string Title { get; set; }
        public NoteTypeEnum TypeEnum { get; set; }
        public string Notes { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public Nullable<int> UpdatedBy { get; set; }

        public bool IsDeleted { get; set; }

        // TODO: check whether it is right???
        public bool IsNote { get; set; }
        public System.Guid? AttachmentStreamId { get; set; }
        public int? FranchiseId { get; set; }

        private Nullable<bool> _isenabled;
        public Nullable<bool> IsEnabled {
            get {
                return _isenabled == true ? true : false;
            }
            set {
                _isenabled = value;
            } }

        [NotMapped]
        public string Categories { get; set; }
        //Added model cration

        // Todo : is these required??? - murugan?
        // public string CreatorFullName { get; set; }
        //public string AvatarSrc { get; set; }

        [NotMapped]
        public string CategoryName
        {
            get
            {
                //return TypeEnum.ToString();
                return TypeEnum.GetDisplayName();
            }
        }

        [NotMapped]
        public string CategoriesName
        {
            get
            {
                string temp = null;
                if (!string.IsNullOrEmpty(Categories))
                {
                    var typeenums = Categories.Split(',').Select(int.Parse).ToArray();
                    foreach (var item in typeenums)
                    {
                        var x = (NoteTypeEnum)item;
                        //temp += temp == null ? x.ToString() : ", " + x.ToString();
                        temp += temp == null ? x.GetDisplayName() : ", " + x.GetDisplayName();
                    }
                }
                
                //return TypeEnum.ToString();
                return temp;
            }
        }

        [NotMapped]
        public string FileName { get; set; }

        [NotMapped]
        public string NoteTypeDescription
        {
            get
            {
                return IsNote ? "Note" : "Attachment";
            }
        }

        [NotMapped]
        public string AccountName { get; set; }

        [NotMapped]
        public string OpportunityName { get; set; }

        [NotMapped]
        public System.DateTime DisplayNotesDate { get; set; }

        [NotMapped]
        public string DisplayUsername { get; set; }
    }
}
