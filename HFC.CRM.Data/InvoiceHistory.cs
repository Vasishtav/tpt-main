﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
namespace HFC.CRM.Data
{
    [Table("[CRM].[InvoiceHistory]")]
   public class InvoiceHistory:BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int InvoiceId { get; set; }
        public int OrderID { get; set; }
        public string InvoiceType { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public decimal? TotalAmount { get; set; }
        public decimal? Balance { get; set; }
        public Guid? Streamid { get; set; }
       
    }
}
