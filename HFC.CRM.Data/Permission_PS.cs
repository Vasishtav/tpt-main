﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
   public class Permission_PS
    {
        public int PermissionSetsId { get; set; }
        public string PermissionSetName { get; set; }
        public string Description { get; set; }
        public int ModuleId { get; set; }
        public string ModuleName { get; set; }
        public string ModuleCode { get; set; }
        public int? ModuleGroupId { get; set; }
        public string GroupName { get; set; }
        public int PermissionTypeId { get; set; }
        public string PermissionName { get; set; }
        public string PermissionCode { get; set; }
        public string PermissionDesc { get; set; }
        public bool IsSpecialPermission { get; set; }
        public bool IsAccessible { get; set; }
        public int? ParentGroupId { get; set; }
        public string ParentName { get; set; }

    }
}
