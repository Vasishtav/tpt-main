﻿
namespace HFC.CRM.Data
{
    using Dapper;
    using System;
    using System.Collections.Generic;
    [Table("CRM.franchiseCaseAttachmentDetails")]
   public partial class franchiseCaseAttachmentDetails 
    {

        [Key]
        public int Id { get; set; }
        public string Module { get; set; }
        public int ModuleId { get; set; }
        public int LineId { get; set; }
        public string fileIconType { get; set; }
        public string fileSize { get; set; }
        public string fileName { get; set; }
        public string color { get; set; }
        public string streamId { get; set; }
        public string streamId1 { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public int? LastUpdatedBy { get; set; }

    }
}
