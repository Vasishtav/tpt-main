﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    public class UserInPermissionVM
    {
        public System.Guid UserId { get; set; }
        public int PermissionSetId { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UserName { get; set; }

    }
}
