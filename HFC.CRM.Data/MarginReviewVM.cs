﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    public class MarginReviewVM
    {
        public int DetailLine { get; set; }
        public int QTY { get; set; }
        public string Vendor { get; set; }
        public string ProductOption { get; set; }
        public string BaseCost { get; set; }
        public string BaseResalePrice { get; set; }
        public string BaseResalePriceWoPromos { get; set; }
        public string PricingCode { get; set; }
        public string PricingRule { get; set; }
        public string MarkFactor { get; set; }
        public double MarkUpFactor { get; set; }
        public string Rounding { get; set; }
        public string DiscountFactor { get; set; }

    }
}
