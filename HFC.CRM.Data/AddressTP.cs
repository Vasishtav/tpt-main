/***************************************************************************\
Module Name:  Address Manager - Implementation
Project: HFC-Tochpoint
Created on: 14 Dec 2017
Created By: 
Copyright:
Description: Additional Address
Change History:
Date  By  Description

\***************************************************************************/
using Dapper;
using System;
using System.Collections.Generic;

namespace HFC.CRM.Data
{
    [Table("CRM.Addresses")]
    public partial class AddressTP : BaseEntity
    {
        [Key]
        public int AddressId { get; set; }
        public string AttentionText { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string CountryCode2Digits { get; set; }
        public int CreatedBy { get; set; }
        private System.DateTime _CreatedOnUtc = DateTime.Now;
    	public System.DateTime CreatedOnUtc { 
    		get { return _CreatedOnUtc; } 
    		set { _CreatedOnUtc = DateTime.SpecifyKind(value, DateTimeKind.Unspecified); } 
    	}
        public bool IsDeleted { get; set; }
        public bool IsResidential { get; set; }
        public string CrossStreet { get; set; }


        public bool IsValidated { get; set; }

        [NotMapped]
        public bool IsBusiness { get; set; }

        // [NotMapped]
        public string Location { get; set; }

        [NotMapped]
        public System.Guid AddressesGuid { get; set; }

        [NotMapped]
        public Nullable<int> LeadId { get; set; }

        [NotMapped]
        public Nullable<int> AccountId { get; set; }

        [NotMapped]
        public Nullable<int> OpportunityId { get; set; }

        [NotMapped]
        public CustomerSource AssociatedSource { get; set; }

        [NotMapped]
        public string AddressType { get; set; }
        public int ContactId { get; set; }

        [NotMapped]
        public string ContactName { get; set; }
        [NotMapped]
        public string FullAddress { get; set; }
        public bool IsInstallationAddress { get; set; }
    }
}
