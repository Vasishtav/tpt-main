﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.HistoryTable")]
    public class HistoryTable : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public string Table { get; set; }
        public int TableId { get; set; }
        public DateTime? Date { get; set; }
        public string Field { get; set; }
        public int UserId { get; set; }
        public string OriginalValue { get; set; }
        public string NewValue { get; set; }
        [NotMapped]
        public string UserName { get; set; }
        [NotMapped]
        public string FranTimezone { get; set; }
    }
}
