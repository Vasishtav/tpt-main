﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    public class KanbanViewModel : BaseEntity
    {
        public int? Id { get; set; }
        public int CaseId { get; set; }
        public int CaseNumber { get; set; }
        public string VendorCaseNumber { get; set; }
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public string StatusName { get; set; }
        public string Type { get; set; }

        public int Age { get; set; }
        public string CaseOwner { get; set; }
        public string UserColor { get; set; }
        public string tags { get; set; }

    }
}
   
