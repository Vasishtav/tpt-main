﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    public class FranchiseSetUpDemo
    {
        public int TerritoryId { get; set; }
        public string TerritoryName { get; set; }
        public int MinionId { get; set; }
        public string WebsiteUrl { get; set; }
    }
    public class OwnerDemo
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public class OwnerVM
    {
        public int OwnerNameId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }


    public class PermissionSet
    {
        public int PermissionSetId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public int? ReportsTo { get; set; }
        public bool Create { get; set; }
        public bool Read { get; set; }
        public bool Update { get; set; }
        public bool Delete { get; set; }
        public bool? IsAccessible { get; set; }
    }
}
