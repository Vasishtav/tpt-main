﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HFC.CRM.Data
{
    [Table("CRM.Type_DeactivationReason")]
    public partial class DeactivationReason
    {
        [Key]
        public int DeactivationReasonId { get; set; }
        public string Reason { get; set; }
    }
}
