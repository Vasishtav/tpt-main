﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("[QB].[TaxRateMapping]")]
    public class TaxRateMapping
    {
        public int FranchiseId { get; set; }
        public string TPTTaxName { get; set; }
        public string QBOTaxName { get; set; }
    }
}
