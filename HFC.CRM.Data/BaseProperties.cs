﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    public class BaseProperties
    {
        DateTime dt = DateTime.Now;
        public DateTime CreatedOn { get { return dt; } set { value = dt; } }
        public int CreatedBy { get; set; }
        public DateTime? LastUpdatedOn { get { return dt; } set { value = dt; } }
        public int? LastUpdatedBy { get; set; }
    }
}
