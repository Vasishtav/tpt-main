﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.CurrencyExchange")]
    public partial class CurrencyExchange
    {
        [Key]
        public int CurrencyExchangeId { get; set; }
        public int Year { get; set; }
        public int Period { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string CountryFrom { get; set; }
        public string CountryTo { get; set; }
        public int CalculationType { get; set; }
        public decimal Rate { get; set; }
    }
}
