﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    public partial class MeasurementConfiguration
    {
        public int Id { get; set; }
        public string System { get; set; }
        public string StorageType { get; set; }
        public Nullable<bool> Width { get; set; }
        public Nullable<bool> Height { get; set; }
        public Nullable<bool> Depth { get; set; }
        public Nullable<bool> Laptop { get; set; }
        public Nullable<bool> Desktop { get; set; }
        public Nullable<bool> Quantity { get; set; }
        public Nullable<bool> UseTP { get; set; }
        public Nullable<bool> Store { get; set; }
    }
}
