﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("History.QuoteLineCostChange")]
    public partial class QuoteLineCostChange
    {
        [Key]
        public int id { get; set; }

        public int QuoteKey { get; set; }
        public int QuoteLineId { get; set; }
        public decimal? OldCost { get; set; }
        public decimal? Cost { get; set; }
        public int QuoteLineNumber { get; set; }
        public int EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }

        [NotMapped]
        public string PersonName { get; set; }
    }
}