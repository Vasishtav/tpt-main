﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
   public partial class AccountSecondaryPerson
    {
        public int AccountId { get; set; }
        public int PersonId { get; set; }

        public virtual AccountTP AccountTP { get; set; }
    }
}
