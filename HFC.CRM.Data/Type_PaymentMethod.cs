﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
namespace HFC.CRM.Data
{
    [Table("[CRM].[Type_PaymentMethod]")]
    public class Type_PaymentMethod : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public string PaymentMethod { get; set; }
        public bool IsActive { get; set; }
        public bool? IsDeleted { get; set; }

    }
}
