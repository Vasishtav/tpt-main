﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("[CRM].[TaxDetails] ")]
    public class TaxDetails : BaseEntity
    {
        [Key]
        public int TaxDetailsId { get; set; }
        public int TaxId { get; set; }
        public string Jurisdiction { get; set; }
        public string JurisType { get; set; }
        public string TaxName { get; set; }
        public decimal? Rate { get; set; }
        public decimal? TaxableAmount { get; set; }
        public decimal? Amount { get; set; }
        public Tax Tax { get; set; }
    }
}
