﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.QuoteCoreProductDetails")]
    public partial class QuoteCoreProductDetails
    {
        [Key]
        public int QuoteCoreProductDetailsId { get; set; }
        public int QuotelineId { get; set; }
        public int QuoteKey { get; set; }
        public string Value { get; set; }
        public string ColorValue { get; set; }
        public string Description { get; set; }
        public string ValueDescription { get; set; }
        public string ColorValueDescription { get; set; }
    }
    public partial class VisiblePrompt
    {
        public string Value { get; set; }

        public string ColorValue { get; set; }

        public string Description { get; set; }

        public string ValueDescription { get; set; }

        public string ColorValueDescription { get; set; }
    }
}
