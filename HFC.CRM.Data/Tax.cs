﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.Tax")]
    public class Tax : BaseEntity
    {
        [Key]
        public int TaxId { get; set; }
        public int QuoteLineId { get; set; }
        public string Code { get; set; }
        public decimal? Rate { get; set; }
        public decimal? Amount { get; set; }
        public int QuoteKey { get; set; }
        public int FranchiseId { get; set; }
        public string Zipcode { get; set; }
        [NotMapped]
        public virtual List<TaxDetails> TaxDetails { get; set; }
    }

}
