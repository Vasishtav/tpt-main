﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

using System;
using System.Collections.Generic;

namespace HFC.CRM.Data
{
    [Table("CRM.MPOErrorEditConfigAudit")]
    public class MPOErrorEditConfigAudit 
    {
        [Key]
        public int Id { get; set; }

        public int PurchaseOrderId { get; set; }
        public int QuoteLineId { get; set; }
        public string PromptAnswerOLD { get; set; }
        public string PromptAnswers { get; set; }
        public string PICResponseOLD { get; set; }
        public string PICResponse { get; set; }
        public string old_quoteline { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int? Impersonatedby { get; set; }
    }
}