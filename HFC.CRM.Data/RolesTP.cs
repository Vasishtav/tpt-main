﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("Auth.Roles")]
    public class RolesTP
    {
        [Key]
        public System.Guid RoleId { get; set; }
        public string RoleName { get; set; }
        public string Description { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public bool IsDeletable { get; set; }
        public bool IsInheritable { get; set; }
        public int? FranchiseId { get; set; }
        public bool IsActive { get; set; }
        public string DisplayName { get; set; }
        public int SortOrder { get; set; }
        public int RoleType { get; set; }

    }
}
