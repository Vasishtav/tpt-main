﻿using System;
using System.Collections.Generic;
using System.Linq;
using HFC.CRM.Cache.Cache;

namespace HFC.CRM.Data
{
    public class GenericAuditRepository<T>: IGenericAuditRepository<T> where T : class, IHasAudit, IHasFranchiseId
    {
        private readonly CRMContextEx context;

        public GenericAuditRepository(CRMContextEx context)
        {
            this.context = context;
        }
        
        // TODO replace with one query

        public IEnumerable<T> GetAdded(long version, int? franchiseId)
        {
            var dt = new DateTime(version);
            return this.context.Set<T>().Where(e => e.CreatedAt > dt && (!franchiseId.HasValue || (e.FranchiseId == franchiseId || e.FranchiseId == null))).ToList();
        }

        public IEnumerable<T> GetUpdated(long version, int? franchiseId)
        {
            var dt = new DateTime(version);
            return this.context.Set<T>().Where(e => e.UpdatedAt > dt && (!franchiseId.HasValue || (e.FranchiseId == franchiseId || e.FranchiseId == null))).ToList();
        }

        public long GetVersion(int? franchiseId)
        {
            var set = this.context.Set<T>();
            return !set.Any()
                ? 0
                : this.context.Set<T>()
                    .Where(e => !franchiseId.HasValue || (e.FranchiseId == franchiseId || e.FranchiseId == null))
                    .Max(e => !e.UpdatedAt.HasValue ? e.CreatedAt : e.UpdatedAt.Value > e.CreatedAt ? e.UpdatedAt.Value : e.CreatedAt)
                    .Ticks;
        }
    }
}