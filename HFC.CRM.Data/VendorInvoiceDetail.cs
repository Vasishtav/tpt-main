﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("crm.VendorInvoiceDetail")]
    public class VendorInvoiceDetail
    {
        [Key]
        public int VendorInvoiceDetailId { get; set; }
        public int VendorInvoiceId { get; set; }
        public int? POLineNumber { get; set; }
        public int Quantity { get; set; }
        public decimal? Cost { get; set; }
        public string Name { get; set; }
        public string ProductNumber { get; set; }
        public string Comment { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public int? QuoteLineId { get; set; }
        public decimal? ItemDiscount { get; set; }
        public decimal? ItemAmount { get; set; }
        public DateTime? ShipDate { get; set; }
        public string FOB { get; set; }
        public decimal? AdjCost { get; set; }
        public decimal? AdjDiscount { get; set; }
        public decimal? AdjAmount { get; set; }
        public string AdjComments { get; set; }
    }

}
