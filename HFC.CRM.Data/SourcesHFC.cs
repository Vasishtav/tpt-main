﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.SourcesHFC")]
    public class SourcesHFC
    {
        [Key]
        public int SourcesHFCId { get; set; }
        public int OwnerId { get; set; }
        public int ChannelId { get; set; }
        public int SourceId { get; set; }
        public int SourcesTPId { get; set; }
        public bool AllowFranchisetoReassign { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public int? LMDBSourceID { get; set; }
        public string LeadType { get; set; }
    }

    public class sourceLeadType
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
