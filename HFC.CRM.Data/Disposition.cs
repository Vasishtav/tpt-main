
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HFC.CRM.Data
{

    //public class BaseEntity
    //{
    //    public DateTime? CreatedOn { get; set; }
    //    public Nullable<int> CreatedBy { get; set; }
    //    public DateTime? UpdatedOn { get; set; }
    //    public Nullable<int> UpdatedBy { get; set; }

    //}


    [Table("CRM.Disposition")]
    public partial class Disposition : BaseEntity
    {
        public Disposition()
        {

        }

        [Key]
        public int DispositionId { get; set; }
        public Nullable<int> FranchiseId { get; set; }
        public Nullable<byte> BrandId { get; set; }
        public Nullable<short> LeadStatusId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }


        //   private Nullable<System.DateTime> _LastUpdatedUtc;
        //public Nullable<System.DateTime> LastUpdatedUtc { 
        //	get { return _LastUpdatedUtc; } 
        //	set { 
        //		if(value == null) 
        //			_LastUpdatedUtc = null; 
        //		else 
        //			_LastUpdatedUtc = DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified); 
        //	} 
        //}
        //[NotMapped]

        //public virtual ICollection<Source> Sources { get; set; }
    }
}
