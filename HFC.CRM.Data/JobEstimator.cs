﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    public class JobEstimator
    {
        public int OpportunityId { get; set; }
        public int QuoteKey { get; set; }
        public int InstallationAddressId { get; set; }
        public int MeasurementId { get; set; }
        public float Area { get; set; }
        public bool SummariziedQuoteLine { get; set; }
        public List<Material> MaterialsList { get; set; }
        public List<Labor> LaborsList { get; set; }

    }

    public class Material
    {
        public int Id { get; set; }
        public string ProductName { get; set; }        
        public int ProductKey { get; set; }
        public string Product { get; set; }
        public float SuggestedQuantity { get; set; }
        public float Adjustment { get; set; }
        public float QuantityUsed { get; set; }
        public float UsedCost { get; set; }
        public float QuotePrice { get; set; }
        public float SuggestedOrderQuantity { get; set; }
        public float OrderQuantity { get; set; }
        public float OrderCost { get; set; }
    }

    public class Labor
    {        
        public int Id { get; set; }
        public string ProductName { get; set; }
        public int ProductKey { get; set; }
        public int Day { get; set; }
        public string Process { get; set; }
        public int SuggestedCrewSize { get; set; }
        public int SuggestedHours { get; set; }
        public int QuoteHours { get; set; }
        public float QuotePrice { get; set; }
        public float LaborCost { get; set; }

    }
}
