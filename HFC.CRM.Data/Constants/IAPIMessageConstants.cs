﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HFC.CRM.Data.Constants
{
    /// <summary>
    /// Class IAPIMessageConstants.
    /// </summary>
    public abstract class IAPIMessageConstants
    {
        /// <summary>
        /// The user not found
        /// </summary>
        public const string UserNotFound = "User not found";
        /// <summary>
        /// The synchronize account not found
        /// </summary>
        public const string SyncAccountNotFound = "User does not have an account to sync with";
        /// <summary>
        /// The invalid username or email
        /// </summary>
        public const string InvalidUsernameOrEmail = "User has an invalid/empty email or username";
        /// <summary>
        /// The empty synchronize list
        /// </summary>
        public const string EmptySyncList = "There are no new records to sync";
        /// <summary>
        /// The insufficient permission
        /// </summary>
        public const string InsufficientPermission = "Account does not exist or EWS does not have access permission";
        /// <summary>
        /// The success
        /// </summary>
        public const string Success = "Success";
    }
}