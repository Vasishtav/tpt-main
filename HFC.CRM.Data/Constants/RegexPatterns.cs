﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data.Constants
{
    /// <summary>
    /// Class RegexPatterns.
    /// </summary>
    public class RegexPatterns
    {
        /// <summary>
        /// The email
        /// </summary>
        public const string Email = @"[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}";
        /// <summary>
        /// The zip code
        /// </summary>
        public const string ZipCode = @"^[0-9]{5}[\s-]?([0-9]{4})?$";
        /// <summary>
        /// The postal code
        /// </summary>
        public const string PostalCode = @"^[a-zA-Z]\d[a-zA-Z][\s-]?(\d[a-zA-Z]\d)?$";
        /// <summary>
        /// The zip or postal code
        /// </summary>
        public const string ZipOrPostalCode = @"(^[a-zA-Z]\d[a-zA-Z][\s-]?(\d[a-zA-Z]\d)?$)|(^[0-9]{5}[\s-]?([0-9]{4})?$)";

        /// <summary>
        /// Validates only the street number and name, some examples are: 14808 Burin Ave, 10-213 Main St. (Canada civic number, aka 213 Main st Apt 10), or 10-213A Main St.
        /// 2 Capture groups, street number and name
        /// </summary>
        public const string StreetAddress = @"^([0-9a-z-]+)\s+(.+)";

        /// <summary>
        /// Validates PO Box address
        /// 1 Capture group, PO number
        /// </summary>
        public const string PoBoxAddress = @"^po\s*box\s+([0-9a-z-]+)";
    }
}
