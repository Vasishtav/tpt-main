﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data.Constants
{
    /// <summary>
    /// Class IMessageConstants.
    /// </summary>
    public abstract class IMessageConstants
    {
        /// <summary>
        /// The unauthorized
        /// </summary>
        public const string Unauthorized = "Sorry, you do not have permission";
        /// <summary>
        /// The failed by error
        /// </summary>
        public const string FailedByError = "An unexpected error occured, please try again later";
        /// <summary>
        /// The failed save changes
        /// </summary>
        public const string FailedSaveChanges = "Failed to save changes";
        /// <summary>
        /// The invalid file path
        /// </summary>
        public const string InvalidFilePath = "Action cancelled, invalid file path";
        /// <summary>
        /// The duplicate file name
        /// </summary>
        public const string DuplicateFileName = "Action cancelled, duplicate file name";
        /// <summary>
        /// The file does not exist
        /// </summary>
        public const string FileDoesNotExist = "Action cancelled, file does not exist";
        /// <summary>
        /// The success
        /// </summary>
        public const string Success = "Success";
        /// <summary>
        /// The lead does not exist
        /// </summary>
        public const string LeadDoesNotExist = "Action cancelled, lead does not exist";
        /// <summary>
        /// The job does not exist
        /// </summary>
        public const string JobDoesNotExist = "Action cancelled, job does not exist";
        /// <summary>
        /// The address does not exist
        /// </summary>
        public const string AddressDoesNotExist = "Action cancelled, address does not exist";
        /// <summary>
        /// The calendar does not exist
        /// </summary>
        public const string CalendarDoesNotExist = "Action cancelled, calendar does not exist";
        /// <summary>
        /// The data cannot be null or empty
        /// </summary>
        public const string DataCannotBeNullOrEmpty = "Action cancelled, data cannot be empty";
        /// <summary>
        /// The task does not exist
        /// </summary>
        public const string TaskDoesNotExist = "Action cancelled, task(s) does not exist";
        /// <summary>
        /// The entity requires subject
        /// </summary>
        public const string EntityRequiresSubject = "This entity requires a subject";
        /// <summary>
        /// The entity requires organizer
        /// </summary>
        public const string EntityRequiresOrganizer = "This entity is missing an organizer";
        /// <summary>
        /// The entity requires assigned
        /// </summary>
        public const string EntityRequiresLocation = "This entity requires a location";
        /// <summary>
        /// The entity requires assigned
        /// </summary>
        public const string EntityRequiresAssigner = "This entity is missing an Assigner";
        /// <summary>
        /// The entity requires assigned
        /// </summary>
        public const string EntityRequiresAttendees = "This entity requires at least one attendee";
        /// <summary>
        /// The entity requires attendee
        /// </summary>
        public const string EntityRequiresAttendee = "This entity requires at least one attendee";
        /// <summary>
        /// The entity organizer not in franchise
        /// </summary>
        public const string EntityOrganizerNotInFranchise = "Unable to update, this event's organizer does not exist within your franchise.  Please contact the original organizer to make changes.";
        /// <summary>
        /// The invalid revision sequence
        /// </summary>
        public const string InvalidRevisionSequence = "Invalid revision sequence, please get the latest calendar before trying again.";

        //for exchange and google managers
        /// <summary>
        /// The appointment does not exist
        /// </summary>
        public const string AppointmentDoesNotExist = "Appointment does not exist";
        /// <summary>
        /// The invalid access token
        /// </summary>
        public const string InvalidAccessToken = "Invalid access token";
        /// <summary>
        /// The synchronize not enabled
        /// </summary>
        public const string SyncNotEnabled = "Sync is not enabled or user's sync time starts on a later date";
        /// <summary>
        /// The push subscription not found
        /// </summary>
        public const string PushSubscriptionNotFound = "Push notification subscription was not found";
    }
}
