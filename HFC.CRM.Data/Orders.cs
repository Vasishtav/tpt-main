﻿using Dapper;
using System;
using System.Collections.Generic;
namespace HFC.CRM.Data
{
    [Table("[CRM].[Orders]")]
    public class Orders : BaseEntity
    {

        [Key]
        public int OrderID { get; set; }
        public int OpportunityId { get; set; }
        public int QuoteKey { get; set; }
        public int OrderNumber { get; set; }
        public string OrderName { get; set; }
        public int? OrderStatus { get; set; }
        public DateTime? ContractedDate { get; set; }
        public decimal? ProductSubtotal { get; set; }
        public decimal? AdditionalCharges { get; set; }
        public decimal? OrderDiscount { get; set; }
        public decimal? OrderSubtotal { get; set; }
        public decimal? Tax { get; set; }
        public decimal? OrderTotal { get; set; }
        public decimal? BalanceDue { get; set; }
        public bool TaxCalculated { get; set; }
        [NotMapped]
        public virtual List<OrderLines> OrderLines { get; set; }
        [NotMapped]
        public int AccountId { get; set; }
        public bool IsTaxExempt { get; set; }
        public string TaxExemptID { get; set; }
        public string CancelReason { get; set; }
        public Nullable<int> ImpersonatorPersonId { get; set; }
        public bool SendReviewEmail { get; set; }
        //       
        public DateTimeOffset? ContractedDateUpdatedOn { get; set; }       
        public int? ContractedDateUpdatedBy { get; set; }       
        public DateTime? PreviousContractedDate { get; set; }
        [NotMapped]
        public String ContractedDateUpdatedBy_Name { get; set; }
    }
}
