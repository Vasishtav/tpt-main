//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HFC.CRM.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Type_Question
    {
        public int QuestionId { get; set; }
        public string Question { get; set; }
        public string SubQuestion { get; set; }
        private System.DateTime _CreatedOnUtc = DateTime.Now;
    	public System.DateTime CreatedOnUtc { 
    		get { return _CreatedOnUtc; } 
    		set { _CreatedOnUtc = DateTime.SpecifyKind(value, DateTimeKind.Unspecified); } 
    	}
        public string SelectionType { get; set; }
        public int DisplayOrder { get; set; }
        public Nullable<int> FranchiseId { get; set; }
        public virtual Franchise Franchise { get; set; }

        //Added for Qualification/Question Answers
        public Nullable<int> BrandId { get; set; }
        public Nullable<int> AnswerId { get; set; }
        public string Answer { get; set; }
        public string Answers { get; set; }
        public string PastDate { get; set; }
        public bool IsPrimaryQuestion { get; set; }
    }
}
