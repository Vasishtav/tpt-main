﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("[CRM].[Payments]")]
    public class Payments : BaseEntity
    {

        [Key]
        public int PaymentID { get; set; }
        public int OpportunityID { get; set; }
        public int OrderID { get; set; }
        public int? InvoiceId { get; set; }
        public string Sidemark { get; set; }
        public decimal? Total { get; set; }
        public decimal? BalanceDue { get; set; }
        public bool? PayBalance { get; set; }
        public int? PaymentMethod { get; set; }
        public string VerificationCheck { get; set; }
        public DateTime? PaymentDate { get; set; }
        public decimal? Amount { get; set; }
        public string Memo { get; set; }
        [NotMapped]
        public string Method { get; set; }
        [NotMapped]
        public string Authorization { get; set; }

        public bool? Reversal { get; set; }
        [NotMapped]
        public decimal? RevesalAmount { get; set; }
        public int? ReasonCode { get; set; }
        [NotMapped]
        public string Reason { get; set; }
        public decimal? OverPaymentAmount { get; set; }

    }
}