﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.Disclaimer")]
    public class DisclaimerModel:BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int FranchiseId { get; set; }
        public string ShortDisclaimer { get; set; }
        public string LongDisclaimer { get; set; }
        //public DateTime CreatedOn { get; set; }
        //public int CreatedBy { get; set; }
        //public DateTime? LastUpdatedOn { get; set; }
        //public int? LastUpdatedBy { get; set; }
    }
}
