﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    class MeasurementView
    {
    }
    public class RoomLocationViewModel
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public string AbbrValue { get; set; }
    }

    public class MountTypeViewModel
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
    public class FractionalInchVModel
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
    public class StorageTypeViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
