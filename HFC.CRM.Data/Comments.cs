﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    using Dapper;
    using System;
    using System.Collections.Generic;
    [Table("CRM.Comments")]
    public partial class Comments
    {
        [Key]
        public long id { get; set; }
        public int module { get; set; }
        public int moduleId { get; set; }
        public int userId { get; set; }
        public int parentId { get; set; }
        public bool haschildId { get; set; }
        public string userImageUrl { get; set; }
        public string userName { get; set; }
        public string htmlComment { get; set; }
        public string htmlCurrentComment { get; set; }
        public bool isReply { get; set; }
        public bool isEdit { get; set; }
       
        public System.DateTime createdAt { get; set; }
        public System.DateTime updatedAt { get; set; }
        public System.DateTime deletedAt { get; set; }
        public bool isDeleted { get; set; }
        public int belongsTo { get; set; }
    }
}
