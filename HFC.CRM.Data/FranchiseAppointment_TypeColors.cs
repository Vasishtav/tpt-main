﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("[CRM].FranchiseAppointmentTypeColors")]
    public partial class FranchiseAppointment_TypeColors : BaseEntity
    {
        [Key]
        public int FranchiseAppointmentTypeColorId { get; set; }
        public Nullable<int> FranchiseId { get; set; }
        public string Appointment1 { get; set; }
        public string Appointment2 { get; set; }
        public string Appointment3 { get; set; }
        public string Appointment4 { get; set; }
        public string Sales_Design { get; set; }
        public string Installation { get; set; }
        public string DayOff { get; set; }
        public string Other { get; set; }
        public string Personal { get; set; }
        public string Meeting_Training { get; set; }
        public string Service { get; set; }
        public string Vacation { get; set; }
        public string Holiday { get; set; }
        public string Followup { get; set; }
        public string TimeBlock { get; set; }
        public bool EnableBorders { get; set; }
        public bool Enable { get; set; }
    }
}
