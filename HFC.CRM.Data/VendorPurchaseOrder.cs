﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.VendorPurchaseOrder")]
    public class VendorPurchaseOrder
    {
        [Key]
        public int VendorPurchaseOrderId { get; set; }
        public int PurchaseOrderId { get; set; }
        public int? PICPO { get; set; }
        public string VendorReference { get; set; }
        public int Status { get; set; }
        public string StatusDescription { get; set; }
        public DateTime? EstShipDate { get; set; }
        public DateTime? DateAcknowledged { get; set; }
        public string Currency { get; set; }
        public string Terms { get; set; }
        public string Comment { get; set; }
        public string VendorMSG { get; set; }
        public string ShipName { get; set; }
        public string ShipAddress1 { get; set; }
        public string ShipAddress2 { get; set; }
        public string ShipCity { get; set; }
        public string ShipState { get; set; }
        public string ShipZip { get; set; }
        public string ShipCountry { get; set; }
        public string ShipPhone { get; set; }
        public string ShipFax { get; set; }
        public string POResponse { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
    }
}
