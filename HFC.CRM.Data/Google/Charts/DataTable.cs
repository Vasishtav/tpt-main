﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data.Google.Charts
{
    //reference https://developers.google.com/chart/interactive/docs/reference#dataparam


    /// <summary>
    /// Class Column.
    /// </summary>
    public class Column
    {
        /// <summary>
        /// Get or set id, [Optional]
        /// </summary>
        /// <value>The identifier.</value>
        public string id { get; set; }

        /// <summary>
        /// Get or set label, [Optional]
        /// </summary>
        /// <value>The label.</value>
        public string label { get; set; }

        /// <summary>
        /// Gets or set type, [Required] 'boolean' (Example values: 'true' or 'false'); 'number' (Example values: v:7 , v:3.14, v:-55); 'string' (Example value: v:'hello'); 'date' (Example value: v:new Date(2008, 0, 15)); 'datetime' (Example value: v:new Date(2008, 0, 15, 14, 30, 45)); 'timeofday' (Example values: v:[8, 15, 0], v: [6, 12, 1, 144])
        /// </summary>
        /// <value>The type.</value>
        public string type { get; set; }
                
    }

    /// <summary>
    /// Class Row.
    /// </summary>
    public class Row
    {
        /// <summary>
        /// Gets or sets the p.
        /// </summary>
        /// <value>The p.</value>
        public object p { get; set; }
        /// <summary>
        /// Create a new array of cells
        /// </summary>
        /// <param name="cellCount">Required</param>
        public Row(int cellCount)
        {
            c = new Cell[cellCount];
        }
        /// <summary>
        /// Get or set cells
        /// </summary>
        /// <value>The c.</value>
        public Cell[] c { get; set; }
    }

    /// <summary>
    /// Class Cell.
    /// </summary>
    public class Cell
    {
        /// <summary>
        /// Gets or sets the p.
        /// </summary>
        /// <value>The p.</value>
        public object p { get; set; }
        /// <summary>
        /// Get or set value
        /// </summary>
        /// <value>The v.</value>
        public object v { get; set; }

        /// <summary>
        /// Get or set format
        /// </summary>
        /// <value>The f.</value>
        public string f { get; set; }
    }

    /// <summary>
    /// Class DataTable.
    /// </summary>
    public class DataTable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataTable"/> class.
        /// </summary>
        public DataTable()
        {
            cols = new List<Column>();
            rows = new List<Row>();
        }
        /// <summary>
        /// Gets or sets the cols.
        /// </summary>
        /// <value>The cols.</value>
        public List<Column> cols { get; set; }
        /// <summary>
        /// Gets or sets the rows.
        /// </summary>
        /// <value>The rows.</value>
        public List<Row> rows { get; set; }
    }
}
