
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HFC.CRM.Data
{
    [Table("CRM.Campaign")]
    public partial class Campaign : BaseEntity
    {
        [Key]
        public int CampaignId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> SourcesTPId { get; set; }
        //public Nullable<int> SubsourceId { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public string Memo { get; set; }
        public Nullable<int> FranchiseId { get; set; }

        //public virtual Subsource Subsource { get; set; }

        [NotMapped]
        public int ChannelId { get; set; }
        [NotMapped]
        public decimal MonthlyBudget { get; set; }

        public bool DeactivateOnEndDate { get; set; }
        public int RecurringType { get; set; }
        public int Duration { get; set; }

        [NotMapped]
        public string Channel { get; set; }
        [NotMapped]
        public string Source { get; set; }
    }
}
