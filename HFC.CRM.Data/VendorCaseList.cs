﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{

    public class VendorCaseList : BaseEntity
    {
        public int VendorCaseId { get; set; }
        public int CaseId { get; set; }
        public string VendorCaseNumber { get; set; }
        public int Status { get; set; }
        public int VendorId { get; set; }
        public int MPO { get; set; }
        public int VPO { get; set; }
        public string StatusName { get; set; }
        public string  IssueDescription { get; set; }
        public string TypeName { get; set; }
        public DateTime IncidentDate {get;set;}
        public string CaseOwner { get; set; }
        public string FranchiseName { get; set; }
        public string AccountName { get; set; }
        public TimeZoneEnum TimezoneEnum { get; set; }

        public string Timezone { get; set; }

    }
}
