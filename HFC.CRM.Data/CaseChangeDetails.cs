﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.CaseChangeDetails")]
    public class CaseChangeDetails : Case
    {
        [Key]
        public int CaseChangeId { get; set; }
        public int CaseId { get; set; }
        public int RecordTypeId { get; set; }
        public List<int> ProductGroupId { get; set; }
        public string PublishMethod { get; set; }
        public string AttachmentURL { get; set; }
        public long DeclinedReasonId { get; set; }
        public string Comments { get; set; }
        public Nullable<DateTime> EffectiveDate { get; set; }
        public bool IsAllSelected { get; set; }
    }
}
