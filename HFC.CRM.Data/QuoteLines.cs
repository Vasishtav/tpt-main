﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.QuoteLines")]
    public partial class QuoteLines
    {
        [Key]
        public int QuoteLineId { get; set; }

        public int QuoteKey { get; set; }
        public int? MeasurementsetId { get; set; }
        public int? VendorId { get; set; }
        public int? ProductId { get; set; }
        public decimal? SuggestedResale { get; set; }
        public decimal? Discount { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public int? LastUpdatedBy { get; set; }
        public bool? IsActive { get; set; }
        public double? Width { get; set; } = 0;
        public double? Height { get; set; } = 0;
        public string FranctionalValueWidth { get; set; }
        public string FranctionalValueHeight { get; set; }
        public decimal? UnitPrice { get; set; }
        public int? Quantity { get; set; }
        public string Description { get; set; }
        public string MountType { get; set; }
        public decimal? ExtendedPrice { get; set; } = 0;
        public string Memo { get; set; }
        public string PICJson { get; set; }
        public string PICPriceResponse { get; set; }
        public string DiscountType { get; set; }
        public int ProductTypeId { get; set; }
        public int? CurrencyExchangeId { get; set; }
        public decimal? ConversionRate { get; set; }
        public string VendorCurrency { get; set; }
        public int QuoteLineNumber { get; set; }
        public int? ConversionCalculationType { get; set; }
        [NotMapped]
        public string QuoteLineNumberr { get; set; }

        [NotMapped]
        public MeasurementLineItemBB LineItemBB { get; set; }

        public string VendorName { get; set; }
        public string ProductName { get; set; }
        public string RoomName { get; set; }
        public string WindowLocation { get; set; }

        [NotMapped]
        public virtual Tax SalesTax { get; set; }

        public int? ProductCategoryId { get; set; }
        public int? ProductSubCategoryId { get; set; }
        public string CancelReason { get; set; }

        [NotMapped]
        public QuoteLineDetail QuoteLineDetail { get; set; }

        public string InternalNotes { get; set; }
        public string CustomerNotes { get; set; }
        public string VendorNotes { get; set; }
        public string ModelDescription { get; set; }
        public DateTime? ValidatedOn { get; set; }

        [NotMapped]
        public FranchiseProducts FranchiseProducts { get; set; }

        public string Color { get; set; }
        public string Fabric { get; set; }

        public bool IsGroupDiscountApplied { get; set; }
        public bool TaxExempt { get; set; }

        [NotMapped]
        public int OrderId { get; set; }

        //public string JobEstimatorSet { get; set; }
    }
}