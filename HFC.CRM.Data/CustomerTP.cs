
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Net.Mail;

namespace HFC.CRM.Data
{
    public enum CustomerSource
    {
        Lead = 1,
        Account = 2,
        Opportunity = 3
    }

    [Table("Crm.Customer")]
    public partial class CustomerTP
    {
        public CustomerTP()
        {
            //this.Attachments = new HashSet<Attachment>();
            //this.ChangeTrackings = new HashSet<ChangeTracking>();
            //this.Notes1 = new HashSet<Note>();
        }
        [Key]
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MI { get; set; }
        public string HomePhone { get; set; }
        public string CellPhone { get; set; }
        public string CompanyName { get; set; }
        public string WorkTitle { get; set; }
        public string WorkPhone { get; set; }
        public string WorkPhoneExt { get; set; }
        public string FaxPhone { get; set; }
        public string PrimaryEmail { get; set; }
        public string SecondaryEmail { get; set; }

        public bool IsDeleted { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string PreferredTFN { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public Nullable<bool> IsAccount { get; set; }
        public string Notes { get; set; }
        // public Nullable<System.DateTime> UnsubscribedOn { get; set; }
        private Nullable<System.DateTime> _UnsubscribedOn;
        public bool IsReceiveEmails { get; set; }
        public Nullable<System.DateTime> UnsubscribedOn
        {
            get { return _UnsubscribedOn; }
            set
            {
                if (value == null)
                    _UnsubscribedOn = null;
                else
                    _UnsubscribedOn = DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified);
            }
        }
        public Nullable<System.Guid> CustomerGuid { get; set; }
        public Nullable<int> FranchiseId { get; set; }


        // These properteis required to be used for linkedlist common component.
        [NotMapped]
        public CustomerSource AssociatedSource { get; set; }

        [NotMapped]
        public int? LeadId { get; set; }

        [NotMapped]
        public int? AccountId { get; set; }

        [NotMapped]
        public int? OpportunityId { get; set; }

        [NotMapped]
        public bool IsDeleted2
        {
            get
            {
                return this.IsDeleted;
            }
        }

        [NotMapped]
        public bool IsPrimaryCustomer { get; set; }

        [NotMapped]
        public string TextOptin { get; set; }
    }
}
