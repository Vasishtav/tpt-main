﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    public class Case:BaseEntity
    {
        [Key]
        public int CaseId { get; set; }
        public string CaseNumber { get; set; }
        //public int CaseTypeId { get; set; }
        public string CaseType { get; set; }
        public int FranchiseId { get; set; }
        public int VendorId { get; set; }
        public string Details { get; set; }
        public int StatusId { get; set; }
        //public DateTime CreatedOn { get; set; }
        //public int CreatedBy { get; set; }
        //public DateTime LastUpdatedOn { get; set; }
        //public int LastUpdatedBy { get; set; }
    }
}
