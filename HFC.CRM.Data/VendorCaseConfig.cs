﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{

    [Table("CRM.VendorCaseConfig")]
    public class VendorCaseConfig : BaseEntity
    {

        [Key]
        public int Id { get; set; }
        public int VendorId { get; set; }
        [NotMapped]
        public string VendorName { get; set; }
        public bool CaseEnabled { get; set; }
        public string CaseLogin { get; set; }
        public string VendorSupportEmail { get; set; }

        public int PersonId { get; set; }
        [NotMapped]
        public int VendorKey { get; set; }
    }
}
