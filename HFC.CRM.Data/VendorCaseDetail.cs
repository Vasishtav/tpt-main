﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.VendorCaseDetail")]
    public class VendorCaseDetail : BaseEntity
    {
        [Key]
        public int VendorCaseDetailId { get; set; }
        public int CaseLineId { get; set; }
        public int VendorCaseId { get; set; }
        public int? TripChargeApproved { get; set; }
        public int? Resolution { get; set; }
        public decimal? Amount { get; set; }
        public int? Status { get; set; }
        [NotMapped]
        public bool ExpediteApproved { get; set; }
       // public bool ExpediteApproved { get; set; }

        //public DateTime CreatedOn { get; set; }
        //public int CreatedBy { get; set; }
        //public DateTime? LastUpdatedOn { get; set; }
        //public int? LastUpdatedBy { get; set; }
    }
}
