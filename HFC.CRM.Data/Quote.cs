﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.Quote")]
    public partial class Quote
    {
        //sale date
        public DateTime? SaleDate { get; set; }
        public DateTime? PreviousSaleDate { get; set; }
        public int? SaleDateCreatedBy { get; set; }
        [NotMapped]
        public DateTime? SaleDateCreatedOn { get; set; }
        [Key]
        public int QuoteKey { get; set; }

        public int QuoteID { get; set; }
        public int OpportunityId { get; set; }
        public string QuoteName { get; set; }

        //TP-1715	CLONE - Quote Name Autofill with Opportunity Name
        [NotMapped]
        public string QuoteNameAutoGenerated { get; set; }

        [NotMapped]
        public string OpportunityName { get; set; }

        [NotMapped]
        public string AccountName { get; set; }

        public bool? PrimaryQuote { get; set; }
        public int QuoteStatusId { get; set; }
        public decimal? Discount { get; set; } = 0;
        public string DiscountType { get; set; }
        public decimal? TotalDiscounts { get; set; } = 0;
        public decimal? NetCharges { get; set; } = 0;
        public DateTime? CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public int? LastUpdatedBy { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public decimal? AdditionalCharges { get; set; }
        public decimal CoreProductMarginPercent { get; set; }
        public decimal CoreProductMargin { get; set; }

        public decimal MyProductMargin { get; set; }
        public decimal MyProductMarginPercent { get; set; }
        public decimal ServiceMargin { get; set; }

        public decimal ServiceMarginPercent { get; set; }

        public int? QuoteNumber { get; set; }
        public decimal? TotalMargin { get; set; } = 0;
        public decimal? ProductSubTotal { get; set; } = 0;
        public decimal? QuoteSubTotal { get; set; } = 0;
        public decimal? NVR { get; set; } = 1;
        public int? DiscountId { get; set; } = 0;
        public decimal? TotalMarginPercent { get; set; } = 0;
        public int? MeasurementId { get; set; }

        [NotMapped]
        public string QuoteStatus { get; set; }

        [NotMapped]
        public string AccountId { get; set; }

        [NotMapped]
        public bool OrderExists { get; set; }

        [NotMapped]
        public virtual ICollection<QuoteLines> QuoteLines { get; set; }

        [NotMapped]
        public int NumberOfQuotelines { get; set; }

        [NotMapped]
        public int? Quantity { get; set; }

        [NotMapped]
        public string SalesAgent { get; set; }

        public bool IsTaxExempt { get; set; }
        public bool IsPSTExempt { get; set; }
        public bool IsGSTExempt { get; set; }
        public bool IsHSTExempt { get; set; }
        public bool IsVATExempt { get; set; }
        public bool IsNewConstruction { get; set; }
        public string TaxExemptID { get; set; }
        public string SideMark { get; set; }

        public List<TaxDetails> TaxDetails { get; set; }

        public string QuoteLevelNotes { get; set; }
        public string OrderInvoiceLevelNotes { get; set; }
        public string InstallerInvoiceNotes { get; set; }

        [NotMapped]
        public string Territory { get; set; }

        [NotMapped]
        public int SalesAgentId { get; set; }

        [NotMapped]
        public int InstallerId { get; set; }
    }
}