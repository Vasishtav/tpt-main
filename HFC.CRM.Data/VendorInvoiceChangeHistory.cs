﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("[CRM].[VendorInvoiceChangeHistory]")]
    public class VendorInvoiceChangeHistory
    {
        [Key]
        public int VendorInvoiceChangeHistoryId { get; set; }
        public int VendorInvoiceId { get; set; }
        public int? VendorInvoiceDetailId { get; set; }
        public int PersonId { get; set; }
        public string Change { get; set; }
        public string AdjValue { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
