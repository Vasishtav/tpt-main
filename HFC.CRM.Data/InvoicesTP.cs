﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("[CRM].[Invoices]")]
    public class InvoicesTP : BaseEntity
    {
        [Key]
        public int InvoiceId { get; set; }
        public int OrderID { get; set; }
        public string InvoiceType { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public decimal? TotalAmount { get; set; }
        public decimal? Balance { get; set; }
        public Guid? Streamid { get; set; }

        [NotMapped]
        public string Region { get; set; }
        [NotMapped]
        public virtual Orders Orders { get; set; }
        [NotMapped]
        public virtual List<Payments> Payments { get; set; }
        [NotMapped]
        public virtual Quote Quote { get; set; }
        [NotMapped]
        public virtual Opportunity Opportunity { get; set; }
        [NotMapped]
        public virtual Lead Lead { get; set; }

    }

}
