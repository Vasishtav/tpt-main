﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.FranchiseCaseAddInfo")]
    public class FranchiseCaseAddInfoModel : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public string VendorCaseNumber { get; set; }
        public int CaseId { get; set; }
        public int QuoteLineId { get; set; }
        [NotMapped]
        public string QuoteLineNumber { get; set; }
        public int? Type { get; set; }
        [NotMapped]
        public string Typevalue { get; set; }
        public int? ReasonCode { get; set; }
        [NotMapped]
        public string CaseReason { get; set; }
        public string IssueDescription { get; set; }
        public bool ExpediteReq { get; set; }
        public bool ExpediteApproved { get; set; }
        public bool ReqTripCharge { get; set; }
        public int? TripChargeApproved { get; set; }

        public decimal? Amount { get; set; }
        public int? Status { get; set; }
        [NotMapped]
        public string StatusName { get; set; }
        public int? Resolution { get; set; }
        [NotMapped]
        public string ResolutionName { get; set; }
        [NotMapped]
        public int VendorId { get; set; }
        [NotMapped]
        public string ProductId { get; set; }
        [NotMapped]
        public string VendorName { get; set; }
        [NotMapped]
        public string ProductName { get; set; }
        [NotMapped]
        public string Description { get; set; }
        [NotMapped]
        public int VPO { get; set; }
        [NotMapped]
        public string ReasonCodeValue { get; set; }
        [NotMapped]
        public string Todo { get; set; }
        [NotMapped]
        public int PurchaseOrderId { get; set; }
        [NotMapped]
        public int MPO { get; set; }
        [NotMapped]
        public int AccountId { get; set; }
        [NotMapped]
        public string AccountName { get; set; }
        [NotMapped]
        public string TripChargeApprovedName { get; set; }
        public bool? ConvertToVendor { get; set; }
        [NotMapped]
        public string VendorReference { get; set; }
        [NotMapped]
        public string VendorCaseNo { get; set; }
        [NotMapped]
        public int NoofLines { get; set; }
        [NotMapped]
        public int ProductTypeId { get; set; }
        //[NotMapped]
        //public int Reasonvalue { get; set; }
        //public DateTime CreatedOn { get; set; }
        //public int CreatedBy { get; set; }
        //public DateTime? LastUpdatedOn { get; set; }
        //public int? LastUpdatedBy { get; set; }
    }
}
