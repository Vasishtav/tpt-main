//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HFC.CRM.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Type_Category
    {
        public Type_Category()
        {
            this.Products = new HashSet<Product>();
            this.Category_Color = new HashSet<Category_Color>();
        }
    
        public int Id { get; set; }
        public string Category { get; set; }
        public Nullable<int> FranchiseId { get; set; }
    
        public virtual ICollection<Product> Products { get; set; }
        public virtual Franchise Franchise { get; set; }
        public virtual ICollection<Category_Color> Category_Color { get; set; }
    }
}
