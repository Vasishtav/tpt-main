﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.SourcesTP")]
    public class SourcesTP
    {
        [Key]
        public int SourcesTPId { get; set; }
        public int OwnerId { get; set; }
        public int ChannelId { get; set; }
        public int SourceId { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }
}
