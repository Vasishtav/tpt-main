﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    public class HFCCanada
    {
        public int OBJECTID { get; set; }
        public string NAME { get; set; }
        public string KEY_ { get; set; }
        public int? OID_ { get; set; }
        public string ZIP { get; set; }
        public string PO_NAME { get; set; }
        public string STATE { get; set; }
        public decimal? HHLD_COUNT { get; set; }
        public decimal? OWNER_OCC { get; set; }
        public decimal? MEDIAN_INC { get; set; }
        public decimal? MED_HOME_V { get; set; }
        public decimal? TOTAL_POP { get; set; }
        public string ZIP_12 { get; set; }
        public string REGION { get; set; }
        public string BBTERRNUM { get; set; }
        public string BBISOLD { get; set; }
        public string BBICONTRAC { get; set; }
        public string BBIFENUM { get; set; }
        public string WNWBB { get; set; }
        public string WNWBBNUM { get; set; }
        public string KABB { get; set; }
        public string CTTERRNUM { get; set; }
        public string CTSOLD { get; set; }
        public string CTCONTRAC { get; set; }
        public string CTFENUM { get; set; }
        public string WNWCT { get; set; }
        public string WNWCTNUM { get; set; }
        public string CT_PREMIER { get; set; }
        public string CTPRESOLD { get; set; }
        public string CTPREC_N { get; set; }
        public string CTPRENUM { get; set; }
        public int? Shape { get; set; }
    }
}
