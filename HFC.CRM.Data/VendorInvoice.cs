﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("crm.VendorInvoice")]
    public class VendorInvoice
    {
        [Key]
        public int VendorInvoiceId { get; set; }
        public int PurchaseOrderId { get; set; }
        public int? PurchaseOrdersDetailId { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime? Date { get; set; }
        public string Terms { get; set; }
        public DateTime? RemitByDate { get; set; }
        public int Quantity { get; set; }
        public decimal? Subtotal { get; set; }
        public decimal? Tax { get; set; }
        public decimal? Freight { get; set; }
        public decimal? Discount { get; set; }
        public DateTime? DiscountDate { get; set; }
        public decimal? Total { get; set; }
        public string PICInvoiceResponse { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public string VendorName { get; set; }
        public int? PICAP { get; set; }
        public string PICVpo { get; set; }
        public string Currency { get; set; }
        public decimal? ProcessingFee { get; set; }
        public decimal? MiscCharge { get; set; }
        public string Comments { get; set; }
        public decimal? AdjSubtotal { get; set; }
        public decimal? AdjDiscount { get; set; }
        public decimal? AdjFreight { get; set; }
        public decimal? AdjProcessingFee { get; set; }
        public decimal? AdjMiscCharge { get; set; }
        public decimal? AdjTax { get; set; }
        public decimal? AdjTotal { get; set; }
        public string AdjComments { get; set; }
        public bool IsInvoiceRevised { get; set; }
    }

    public class AccountsPayable
    {
        public int ap { get; set; }
        public string vendor { get; set; }
        public int po { get; set; }
        public string invoice { get; set; }
        public string currency { get; set; }
        public decimal totalGoods { get; set; }
        public decimal tax { get; set; }
        public decimal freight { get; set; }
        public decimal total { get; set; }
        public decimal discount { get; set; }
        public string date { get; set; }
        public string dueDate { get; set; }
        public string discountDate { get; set; }
        public decimal discountRate { get; set; }
        public string warehouse { get; set; }
    }

    public class ItemAP
    {
        public int ap { get; set; }
        public int item { get; set; }
        public decimal quantity { get; set; }
        public decimal unitPrice { get; set; }
        public decimal exchange { get; set; }
        public decimal duty { get; set; }
        public decimal freight { get; set; }
        public int po { get; set; }
        public int poItem { get; set; }
        public int wo { get; set; }
        public int woItem { get; set; }
        public string comment { get; set; }
        public int? ediCustomerItem { get; set; }
    }

    public class Property
    {
        public AccountsPayable AccountsPayable { get; set; }
        public List<ItemAP> Items { get; set; }
    }

    public class RootObjectAP
    {
        public bool valid { get; set; }
        //public List<string> error { get; set; }
        public List<Property> properties { get; set; }
    }



}

