﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("QB.Apps")]
    public class QBApps : BaseEntity
    {
        [Key]
        public int AppId { get; set; }
        public Guid AppGuid { get; set; }
        public int FranchiseId { get; set; }
        public string OwnerID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool IsActive { get; set; }
        public bool SynchInvoices { get; set; }
        public bool SynchCost { get; set; }
        public bool SynchPayments { get; set; }
        public string TaxAgency { get; set; }
        public string QWC { get; set; }
        public bool IsLastNameFirst { get; set; }
        public string AppToken { get; set; }
        public string ConsumerKey { get; set; }
        public string ConsumerSecret { get; set; }
        public bool? IsSyncLocked { get; set; }
        public DateTime? SyncLockedTime { get; set; }
        public bool? SuppressSalesTax { get; set; }
        public bool? TaxDetailjurisdictions { get; set; }
        public string TaxCode { get; set; }
        public string AR { get; set; }
        public string Sales { get; set; }
        public string COG { get; set; }

        [NotMapped]
        public bool IsSyncLockedExt
        {
            get
            {
                return (IsSyncLocked == true && SyncLockedTime != null &&
                        DateTime.Now - SyncLockedTime.Value < TimeSpan.FromMinutes(30));
            }
            set
            {
                IsSyncLocked = value;
                if (value)
                {
                    SyncLockedTime = DateTime.Now;
                }
            }
        }
        [NotMapped]
        public int? BrandId { get; set; }
        [NotMapped]
        public string SynchronizationStatusUrl { get; set; }
        [NotMapped]
        public bool? dispalyTaxCode { get; set; }
        [NotMapped]
        public List<TaxRateMapping> TaxRateMapping { get; set; }

    }

}
