﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
namespace HFC.CRM.Data
{
   [Table("[CRM].[Type_PricingStrategyType]")]
    public class Type_PricingStrategyType
    {
        [Key]
        public int Id { get; set; }
        public string PricingStrategyType { get; set; }
        public bool IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public int? LastUpdatedBy { get; set; }

    }
}
