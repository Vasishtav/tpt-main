﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.FranchiseDiscount")]
    public class FranchiseDiscount:BaseEntity
    {
        [Key]
        public int DiscountIdPk { get; set; }
        public int DiscountId { get; set; }
        public string Name { get; set; }
        public int FranchiseId { get; set; }
        public string Description { get; set; }
        public int? Status { get; set; }
        public decimal? DiscountValue { get; set; }
        public string DiscountFactor { get; set; }
        //public DateTime? StartDate { get; set; }
        //public DateTime? EndDate { get; set; }
        //public int CreatedBy { get; set; }
        //public DateTime CreatedOn { get; set; }
        //public int? UpdatedBy { get; set; }
        //public DateTime? UpdatedOn { get; set; }
    }
}
