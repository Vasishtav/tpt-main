﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("CRM.TerritoryEmailSettings")]
    public class TerritoryEmailSettings : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int TerritoryId { get; set; }
        public bool UseFranchiseName { get; set; }
        public string LicenseNumber { get; set; }
        public int? FromEmailPersonId { get; set; }
        //public Guid FromEmailPersonId { get; set; }
        public int? ShipToLocationId { get; set; }
        [NotMapped]
        public string ShipToName { get; set; }
        [NotMapped]
        public int AddressId { get; set; }
        [NotMapped]
        public string TerritoryName { get; set; }
        [NotMapped]
        public string Displayname { get; set; }
        [NotMapped]
        public string Code { get; set; }
        [NotMapped]
        public string Minion { get; set; }
        [NotMapped]
        public string LocationAddress
        {
            get
            {
                string Result = "";
                if (Address1 != null && Address1 != "")
                {
                    Result = Address1;
                }
                if (Address2 != null && Address2 != "")
                {
                    if (Result != null && Result != "")
                        Result = Result + "," + Address2;
                    else
                        Result = Address2;
                }
                if (City != null && City != "")
                {
                    if (Result != null && Result != "")
                        Result = Result + "," + City;
                    else
                        Result = City;
                }
                if (State != null && State != "")
                {
                    if (Result != null && Result != "")
                        Result = Result + "," + State;
                    else
                        Result = State;
                }
                if (CountryCode2Digits != null && CountryCode2Digits != "")
                {
                    if (Result != null && Result != "")
                        Result = Result + "," + CountryCode2Digits;
                    else
                        Result = CountryCode2Digits;
                }
                if (ZipCode != null && ZipCode != "")
                {
                    if (Result != null && Result != "")
                        Result = Result + "," + ZipCode;
                    else
                        Result = ZipCode;
                }

                return Result;
            }
        }
        [NotMapped]
        public string PrimaryEmail { get; set; }
        [NotMapped]
        public string Address1 { get; set; }
        [NotMapped]
        public string Address2 { get; set; }
        [NotMapped]
        public string City { get; set; }
        [NotMapped]
        public string State { get; set; }
        [NotMapped]
        public string CountryCode2Digits { get; set; }
        [NotMapped]
        public string ZipCode { get; set; }


    }
}
