﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    public enum AppointmentTypeEnumTP : byte
    {
        [Display(Name = "Sales/Design Appointment")]
        Appointment = 1,
        [Display(Name = "Final Measurements")]
        FinalMeasurements = 2,
        Installation = 5,
        Service = 6,
        [Display(Name = "Follow-up")]
        Followup = 7,
        Personal = 8,
        Vacation = 9,
        Holiday = 10,
        [Display(Name = "Meeting/Training")]
        Meeting = 11,
        [Display(Name = "Time Block")]
        TimeBlock = 12

    }
}
