
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HFC.CRM.Data
{
    [Table("CRM.Subsource")]
    public partial class Subsource : BaseEntity
    {
        [Key]
        public int SubsourceId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<int> SourceId { get; set; }
        public Nullable<int> FranchiseId { get; set; }
        public Nullable<byte> BrandId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        
        //public virtual Subsource Subsource { get; set; }
    }
}
