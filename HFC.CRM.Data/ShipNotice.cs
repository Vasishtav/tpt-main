﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Data
{
    [Table("crm.ShipNotice")]
    public class ShipNotice
    {
        [Key]
        public int ShipNoticeId { get; set; }
        public string ShipmentId { get; set; }
        public int? PurchaseOrdersDetailId { get; set; }
        public DateTime? ShippedDate { get; set; }
        public DateTime? EstDeliveryDate { get; set; }
        public string ShippedVia { get; set; }
        public string BOL { get; set; }
        public string Weight { get; set; }
        public int TotalBoxes { get; set; }
        public int PurchaseOrderId { get; set; }
        public string VendorName { get; set; }
        public bool ReceivedComplete { get; set; }
        public string TrackingNumber { get; set; }
        public string TrackingURL { get; set; }
        public string PICVpo { get; set; }
        [NotMapped ]
        public string TotalQuantity { get; set; }
        public DateTime? ReceivedCompleteDate { get; set; }
        public string ShipNoticeJson { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        [NotMapped]
        public List<ShipmentDetail> ListShipmentDetails { get; set; }
        [NotMapped]
        public int MasterPONumber { get; set; }
        [NotMapped]
        public string ShiptoAccount { get; set; }
        [NotMapped]
        public string SideMark { get; set; }
        [NotMapped]
        public string ShippedTo { get; set; }
        [NotMapped]
        public string FullVendorName { get; set; }
        
        [NotMapped]
        public string UpdatedByUserName{ get; set; }

        [NotMapped]
        public int TotalBoxesTP { get; set; }
        
        [NotMapped]
        public string TotalQuantityReceived { get; set; }
        [NotMapped]
        public string TotalQuantityShipped { get; set; }
        [NotMapped]
        public string TotalQuantityShortShipped { get; set; }
        [NotMapped]
        public string TotalQuantityMisShipped { get;  set; }
        [NotMapped]
        public string TotalQuantityDamaged { get;  set; }

        [NotMapped]
        public string ShippedViaTP { get; set; }

        [NotMapped]
        public string Routing { get; set; }

        public DateTime? CheckedinDate { get; set; }

        public string RemakeReference { get; set; }

        [NotMapped]
        public string Status { get; set; }

        [NotMapped]
        public IList<string> AuditTrial { get; set; }

        [NotMapped]
        public List< KeyValuePair<string, string>> TrackingList { get; set; }

        public string ShippedViaText { get; set; }
    }
    public class SNShipNotice
    {
        public string shipmentNum { get; set; }
        public decimal weight { get; set; }
        public string shipVia { get; set; }
        public string shipViaName { get; set; }
        public string bOL { get; set; }
        public string vendor { get; set; }
        public string shippedDate { get; set; }
        public string deliveryDate { get; set; }
        public string trackingUrl { get; set;   }
        public string trackingNumber { get; set; }
        public string po { get; set; }
    }

    public class SNItem
    {
        public decimal qty { get; set; }
        public string boxId { get; set; }
        public string tracking { get; set; }
        public int poItem { get; set; }
        public string stockProduct { get; set; }
        public int? ediCustomerItem { get; set; }

        public string uom {get; set; }
        public string trackingUrl { get; set; }
    }

    public class SNProperty
    {
        public SNShipNotice ShipNotice { get; set; }
        public List<SNItem> Items { get; set; }
    }

    public class SNRootObject
    {
        public bool valid { get; set; }
        public List<string> error { get; set; }
        public List<SNProperty> properties { get; set; }
    }
}
