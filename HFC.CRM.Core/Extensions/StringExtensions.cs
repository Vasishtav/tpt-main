﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HFC.CRM.Core.Extensions
{
    public static class StringExtensions
    {
        public static string CapitalFirstLetter(this string s)
        {
            if (string.IsNullOrEmpty(s)) { return string.Empty; }
            char[] a = s.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }

        public static string CapitalFirstLetterOfEachWord(this string s)
        {

            List<string> list = new List<string>();
            if (string.IsNullOrEmpty(s)) { return string.Empty; }
            string[] words = s.Split(' ');
            foreach (string word in words)
            {
                list.Add(word.CapitalFirstLetter());
            }
            return string.Join(" ", list);
        }

        public static string GlobalDateFormat(this DateTime data)
        {
            var value = data.ToString("MM'/'dd'/'yyyy");
            return value;
        }

    }
}
