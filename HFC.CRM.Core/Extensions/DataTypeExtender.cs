﻿using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// The Core namespace.
/// </summary>
namespace HFC.CRM.Core
{
    /// <summary>
    /// Class DataTypeExtender.
    /// </summary>
    public static class DataTypeExtender
    {
        /// <summary>
        /// To the full string.
        /// </summary>
        /// <param name="src">The source.</param>
        /// <returns>System.String.</returns>
        public static string ToFullString(this Source src)
        {         
            if (src.ParentId > 0)
            {
                var parent = CacheManager.SourceCollection.FirstOrDefault(f => f.SourceId == src.ParentId);
                if (parent != null)
                    return string.Format("{0} - {1}", parent.Name, src.Name);
                else
                    return src.Name;
            }
            else
                return src.Name;                 
        }

        /// <summary>
        /// To the full string.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <returns>System.String.</returns>
        public static string ToFullString(this Type_LeadStatus status)
        {
            if (status.ParentId > 0)
            {
                var parent = CacheManager.LeadStatusTypeCollection.FirstOrDefault(f => f.Id == status.ParentId);
                if (parent != null)
                    return string.Format("{0} - {1}", parent.Name, status.Name);
                else
                    return status.Name;
            }
            else
                return status.Name;                 
        }

        /// <summary>
        /// To the full string.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <returns>System.String.</returns>
        public static string ToFullString(this Type_JobStatus status)
        {
            if (status.ParentId > 0)
            {
                var parent = CacheManager.JobStatusTypeCollection.FirstOrDefault(f => f.Id == status.ParentId);
                if (parent != null)
                    return string.Format("{0} - {1}", parent.Name, status.Name);
                else
                    return status.Name;
            }
            else
                return status.Name;
        }
    }
}
