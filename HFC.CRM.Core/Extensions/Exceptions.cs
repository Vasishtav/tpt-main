﻿//NOT USING THIS CLASS

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Diagnostics;

//namespace HFC.CRM.Core.Extensions
//{
//    public class GenericException : Exception
//    {
//        private string _trace;
//        public override string StackTrace
//        {
//            get
//            {
//                return _trace;
//            }
//        }

//        public GenericException(string message)
//            : base(message)
//        {
//            Source = System.Reflection.Assembly.GetExecutingAssembly().FullName;
            
//            var trace = new System.Diagnostics.StackTrace(true);
//            List<string> traceList = new List<string>();
//            foreach (var frame in trace.GetFrames().Skip(1))
//            {
//                var method = frame.GetMethod();
//                if (method.DeclaringType.Namespace.Contains("HFC.CRM"))
//                {
//                    traceList.Add(string.Format("at {0} in {1}:line {2}",
//                        method,
//                        frame.GetFileName(),
//                        frame.GetFileLineNumber()));
//                }
//            }
//            _trace = string.Join("\t ", traceList);
//        }
//    }

//    public class EmptyIdException : GenericException
//    {
//        public EmptyIdException(string message) : base(message) { }        
//    }
//}
