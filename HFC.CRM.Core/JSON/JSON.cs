﻿using System.IO;
using System.Runtime.Serialization.Json;

namespace HFC.CRM.Core
{
    public class JSON
    {
        public static string Serialize(object obj)
        {
            MemoryStream stream1 = new MemoryStream();
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(object));

            ser.WriteObject(stream1, obj);
            stream1.Position = 0;
            StreamReader sr = new StreamReader(stream1);
            return sr.ReadToEnd();
        }
    }
}