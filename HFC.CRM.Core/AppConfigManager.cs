﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HFC.CRM.Core.Common;
using System.Configuration;
using HFC.CRM.Core.Membership;
using System.Management;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using System.Data.Entity;
using System.Reflection;
using System.Web;

/// <summary>
/// The Core namespace.
/// </summary>
namespace HFC.CRM.Core
{
    /// <summary>
    /// Class AppConfigManager.
    /// </summary>
    public class AppConfigManager
    {
        /// <summary>
        /// The name of the key in web.config that holds the value of the url that sync calls are pointed to.
        /// </summary>
        private const string SyncUrlSettingsKeyName = "syncUrl";

        private const string PICUrlSettingsKeyName = "PICUrl";
        private const string PICUrlclientId = "PICUrlclientId";
        private const string PICUrlclientpwd = "PICUrlclientpwd";
        private const string zillowKey = "ZillowKey";
        private const string ZillowURL = "ZillowURL";

        /// <summary>
        /// Updates the configuration.
        /// </summary>
        /// <param name="configId">The configuration identifier.</param>
        /// <param name="value">The value.</param>
        /// <param name="description">The description.</param>
        /// <returns>System.String.</returns>
        ///

        ///
        ///      07/16/2015  There is no need for the config update method
        ///
        //        public static string UpdateConfig(int configId, string value, string description)
        //        {
        //            string result = "Unable to update config";
        //            if (configId > 0)
        //            {
        //                try
        //                {
        //                    using (var db = new CRMContextEx())
        //                    {
        //                        var config = db.AppConfigs.FirstOrDefault(f => f.ConfigId == configId);
        //                        if (config != null)
        //                        {
        //                            config.Value = value;
        //                            config.Description = description;
        //                            var entry = db.Entry(config);
        //                            if (entry.State == EntityState.Unchanged)
        //                                result = "Success";
        //                            //else
        //                            //{
        //                            //    string historyStr = "";
        //                            //    var history = Util.GetEFChangedProperties(entry);
        //                            //    if (history != null)
        //                            //        historyStr = history.ToString(System.Xml.Linq.SaveOptions.DisableFormatting);
        //                            //    if (!string.IsNullOrEmpty(historyStr))
        //                            //    {
        //                            //        var editHistoryId = HFC.CRM.Data.EditHistory.LogEditHistory(PageRequest.CurrentUser.PersonId, historyStr);
        //                            //        config.EditHistories.Add(new AppConfigHistory() { EditHistoryId = editHistoryId, ConfigId = configId });
        //                            //    }
        //                            //}
        //                            if (db.SaveChanges() > 0)
        //                            {
        //                                result = "Success";
        //                                var a = CacheManager.AppConfigCollection.FirstOrDefault(f => f.ConfigId == configId);
        //                                if (a != null)
        //                                {
        //                                    a.Value = config.Value;
        //                                    a.Description = config.Description;
        //                                }
        //                            }
        //                        }
        //                        else
        //                            result = "Unable to locate config";
        //                    }
        //                }
        //                catch (Exception ex)
        //                {
        //                    result = EventLogger.LogEvent(ex);
        //#if DEBUG
        //                    result = ex.Message;
        //#endif
        //                }
        //            }
        //            else
        //                result = "Config Id Missing";
        //            return result;
        //        }

        /// <summary>
        /// The _ application admin
        /// </summary>
        private static User _ApplicationAdmin;

        /// <summary>
        /// Gets the application admin.
        /// </summary>
        /// <value>The application admin.</value>
        public static User ApplicationAdmin
        {
            get
            {
                if (_ApplicationAdmin == null)
                {
                    _ApplicationAdmin = LocalMembership.GetUser("admin", "Roles");
                }
                return _ApplicationAdmin;
            }
        }

        /// <summary>
        /// Default Role is User
        /// </summary>
        /// <value>The default role.</value>
        public static Role DefaultRole
        {
            get
            {
                return CacheManager.RoleCollection.FirstOrDefault(f => !f.FranchiseId.HasValue && f.RoleName.Equals("user", StringComparison.InvariantCultureIgnoreCase));
            }
        }

        /// <summary>
        /// Default Role is User
        /// </summary>
        /// <value>The default owner role.</value>
        public static Role DefaultOwnerRole
        {
            get
            {
                return CacheManager.RoleCollection.FirstOrDefault(f => !f.FranchiseId.HasValue && f.RoleName.Equals("owner", StringComparison.InvariantCultureIgnoreCase));
            }
        }

        /// <summary>
        /// Default Role is Manager
        /// </summary>
        /// <value>The default owner role.</value>
        public static Role DefaultManagerRole
        {
            get
            {
                return CacheManager.RoleCollection.FirstOrDefault(f => !f.FranchiseId.HasValue && f.RoleName.Equals("manager", StringComparison.InvariantCultureIgnoreCase));
            }
        }

        /// <summary>
        /// Application Admin Role
        /// </summary>
        /// <value>The application admin role.</value>
        public static Role AppAdminRole
        {
            get
            {
                return CacheManager.RoleCollection.FirstOrDefault(f =>
                !f.FranchiseId.HasValue && f.RoleName.Equals(ContantStrings.GlobalAdmin, //"admin",
                StringComparison.InvariantCultureIgnoreCase));
            }
        }

        public static Role FranchiseAdminRole
        {
            get
            {
                return CacheManager.RoleCollection.FirstOrDefault(f =>
                !f.FranchiseId.HasValue && f.RoleName.Equals(ContantStrings.FranchiseAdmin, //"Franchise Admin",
                StringComparison.InvariantCultureIgnoreCase));
            }
        }

        public static Role AppVendorRole
        {
            get
            {
                return CacheManager.RoleCollection.FirstOrDefault(f =>
                !f.FranchiseId.HasValue && f.RoleName.Equals("Vendor", //"Vendor",
                StringComparison.InvariantCultureIgnoreCase));
            }
        }

        public static Role AppPicRole
        {
            get
            {
                return CacheManager.RoleCollection.FirstOrDefault(f =>
                !f.FranchiseId.HasValue && f.RoleName.Equals("Supply Chain Partner (PIC)", //"Vendor",
                StringComparison.InvariantCultureIgnoreCase));
            }
        }

        public static Role AppProductStrgMang
        {
            get
            {
                return CacheManager.RoleCollection.FirstOrDefault(f =>
                !f.FranchiseId.HasValue && f.RoleName.Equals("HFC Product Strategy Mgt (PSM)", //"Vendor",
                StringComparison.InvariantCultureIgnoreCase));
            }
        }

        /// <summary>
        /// Default Sales role
        /// </summary>
        /// <value>The default sales role.</value>
        public static Role DefaultSalesRole
        {
            get
            {
                return CacheManager.RoleCollection.FirstOrDefault(f => !f.FranchiseId.HasValue && f.RoleName.Equals("sales", StringComparison.InvariantCultureIgnoreCase));
            }
        }

        //salesagent with no margine
        public static Role DefaultSalesNoMargineRole
        {
            get
            {
                return CacheManager.RoleCollection.FirstOrDefault(f => !f.FranchiseId.HasValue && f.RoleName.Equals("Sales/Designer No Margins", StringComparison.InvariantCultureIgnoreCase));
            }
        }

        /// <summary>
        /// Gets the default install role.
        /// </summary>
        /// <value>The default install role.</value>
        public static Role DefaultInstallRole
        {
            get
            {
                return CacheManager.RoleCollection.FirstOrDefault(f => !f.FranchiseId.HasValue && f.RoleName.Equals("Installation", StringComparison.InvariantCultureIgnoreCase));
            }
        }

        public static Role DefaultFranchiseAdminRole
        {
            get
            {
                return CacheManager.RoleCollection.FirstOrDefault(f =>
                !f.FranchiseId.HasValue && f.RoleName.Equals(ContantStrings.FranchiseAdmin, //"Franchise Admin",
                StringComparison.InvariantCultureIgnoreCase));
            }
        }

        /// <summary>
        /// Gets the default operator role.
        /// </summary>
        /// <value>The default operator role.</value>
        public static Role DefaultOperatorRole
        {
            get
            {
                return CacheManager.RoleCollection.FirstOrDefault(f => !f.FranchiseId.HasValue && f.RoleName.Equals("operator", StringComparison.InvariantCultureIgnoreCase));
            }
        }

        /// <summary>
        /// Gets the default Account role.
        /// </summary>
        /// <value>The default Account role.</value>
        public static Role DefaultAccountRole
        {
            get
            {
                return CacheManager.RoleCollection.FirstOrDefault(f => !f.FranchiseId.HasValue && f.RoleName.Equals("Accounting", StringComparison.InvariantCultureIgnoreCase));
            }
        }

        /// <summary>
        /// Gets the default monitor role.
        /// </summary>
        /// <value>The default monitor role.</value>
        public static Role DefaultMonitorRole
        {
            get
            {
                return CacheManager.RoleCollection.FirstOrDefault(f => !f.FranchiseId.HasValue && f.RoleName.Equals("monitor", StringComparison.InvariantCultureIgnoreCase));
            }
        }

        public static Role DefaultCustomerServiceRole
        {
            get
            {
                return CacheManager.RoleCollection.FirstOrDefault(f => !f.FranchiseId.HasValue && f.RoleName.Equals("customer service", StringComparison.InvariantCultureIgnoreCase));
            }
        }

        public static Role AppHFCFinanceRole
        {
            get
            {
                return CacheManager.RoleCollection.FirstOrDefault(f => f.RoleName.Equals("HFC Finance", StringComparison.InvariantCultureIgnoreCase));
            }
        }

        public static Role AppHFCOperationsRole
        {
            get
            {
                return CacheManager.RoleCollection.FirstOrDefault(f => f.RoleName.Equals("HFC Operations", StringComparison.InvariantCultureIgnoreCase));
            }
        }

        public static Role AppHFCMarketingRole
        {
            get
            {
                return CacheManager.RoleCollection.FirstOrDefault(f => f.RoleName.Equals("HFC Marketing", StringComparison.InvariantCultureIgnoreCase));
            }
        }

        public static string GetEmailForProcurementNotification
        {
            get
            {
                return GetConfig<string>("ProcurementNotification", "Procurement", "ProcurementNoification");
            }
        }

        /// <summary>
        /// Gets the new lead status identifier.
        /// </summary>
        /// <value>The new lead status identifier.</value>
        public static short NewLeadStatusId
        {
            get
            {
                return GetConfig<short>("NewLeadStatusId", "System Settings");
            }
        }

        public static short NewOpportunityStatusId
        {
            get
            {
                return GetConfig<short>("NewOpportunityStatusId", "System Settings");
            }
        }

        public static short NewAccountStatusId
        {
            get
            {
                return GetConfig<short>("NewAccountStatusId", "System Settings");
            }
        }

        public static short ValidOpportunityStatusId
        {
            get
            {
                return GetConfig<short>("ValidOpportunityStatusId", "System Settings");
            }
        }

        public static short ValidLeadStatusId
        {
            get
            {
                return GetConfig<short>("ValidLeadStatusId", "Defaults");
            }
        }

        /// <summary>
        /// Gets the new job status identifier.
        /// </summary>
        /// <value>The new job status identifier.</value>
        public static short NewJobStatusId
        {
            get
            {
                return GetConfig<short>("NewJobStatusId", "System Settings");
            }
        }

        /// <summary>
        /// Gets a list of what is considered lost lead status ids
        /// </summary>
        /// <value>The lost lead statuses.</value>
        public static List<Type_LeadStatus> LostLeadStatuses
        {
            get
            {
                var parentId = GetConfig<short>("LostLeadStatusId", "System Settings");
                return CacheManager.LeadStatusTypeCollection
                    .Where(i => i.FranchiseId == SessionManager.CurrentFranchise.FranchiseId || !i.FranchiseId.HasValue)
                    .Where(w => w.Id == parentId || w.ParentId == parentId).ToList();
            }
        }

        /// <summary>
        /// Gets a list of what is considered lost job status ids
        /// </summary>
        /// <value>The lost job statuses.</value>
        public static List<Type_JobStatus> LostJobStatuses
        {
            get
            {
                var parentId = GetConfig<short>("LostJobStatusId", "System Settings");
                return CacheManager.JobStatusTypeCollection
                    .Where(i => i.FranchiseId == SessionManager.CurrentFranchise.FranchiseId || !i.FranchiseId.HasValue)
                    .Where(w => w.Id == parentId || w.ParentId == parentId).ToList();
            }
        }

        public static decimal DefaultJobberDiscountPercent
        {
            get
            {
                return GetConfig<decimal>("JobberDiscountPercent", "Order", "Defaults");
            }
        }

        public static decimal DefaultMarginPercent
        {
            get
            {
                return GetConfig<decimal>("MarginPercent", "Order", "Defaults");
            }
        }

        /// <summary>
        /// Gets the default avatar image
        /// </summary>
        public static string DefaultAvatarSrc
        {
            get
            {
                return GetConfig<string>("GenericAvatarImage", "System Settings");
            }
        }

        /// <summary>
        /// Gets where quick books logging is enabled
        /// </summary>
        public static bool EnableQuickbooksLogging
        {
            get
            {
                return GetConfig<bool>("EnableQuickbooksLogging", "Quickbooks");
            }
        }

        /// <summary>
        /// The _app version
        /// </summary>
        private static string _appVersion;

        /// <summary>
        /// Gets the application version.
        /// </summary>
        /// <value>The application version.</value>
        public static string AppVersion
        {
            get
            {
                if (string.IsNullOrEmpty(_appVersion))
                    _appVersion = getApplicationAssembly().GetName().Version.ToString() + " - " + RegexUtil.GetNumbersOnly(System.Environment.MachineName);
                //var s= _appVersion.Split('.');
                // var z = s[0] + "." + s[1];
                // return z;
                return _appVersion;
            }
        }

        public static string _SName;

        public static string SName
        {
            get
            {
                if (string.IsNullOrEmpty(_SName))
                    _SName = System.Environment.MachineName;
                return _SName;
            }
        }

        //public static List<string> CDNUrlCollection
        //{
        //    get
        //    {
        //        string col = Util.CastObject<string>(ConfigurationManager.AppSettings["CDNUrlCollection"]);
        //        if (!string.IsNullOrEmpty(col))
        //            return col.Split(new char[] { ',', ';', '|', '\t' }, StringSplitOptions.RemoveEmptyEntries).ToList();
        //        else
        //            return new List<string>();
        //    }
        //}

        //public static int UserActivityTimeInterval
        //{
        //    get { return GetConfig<int>("UserActivityTime", "Time Intervals"); }
        //}

        /// <summary>
        /// Gets the web oe site identifier.
        /// </summary>
        /// <value>The web oe site identifier.</value>
        public static int WebOESiteId
        {
            get { return AppConfigManager.GetConfig<int>("SiteId", "Solatech"); }
        }

        /// <summary>
        /// Gets the maximum task days.
        /// </summary>
        /// <value>The maximum task days.</value>
        public static int MaxTaskDays
        {
            get { return Math.Abs(AppConfigManager.GetConfig<int>("MaxTaskDays", "Calendar")); }
        }

        /// <summary>
        /// Gets the name of the company.
        /// </summary>
        /// <value>The name of the company.</value>
        public static string CompanyName
        {
            get { return GetConfig<string>("CompanyName", "System Settings"); }
        }

        /// <summary>
        /// Gets the name of the application.
        /// </summary>
        /// <value>The name of the application.</value>
        public static string ApplicationName
        {
            get { return GetConfig<string>("ApplicationName", "System Settings"); }
        }

        /// <summary>
        /// Gets the application title.
        /// </summary>
        /// <value>The application title.</value>
        public static string ApplicationTitle
        {
            get { return GetConfig<string>("ApplicationTitle", "System Settings"); }
        }

        /// <summary>
        /// The endpoint on the sync server to make the synchronization call to.
        /// </summary>
        public static string SyncUrl
        {
            get
            {
                string syncUrl;

                if ((syncUrl = ConfigurationManager.AppSettings[SyncUrlSettingsKeyName]) == null)
                    throw new ApplicationException("The syncUrl setting is missing from the configuration settings.");

                return syncUrl;
            }
        }

        public static string PICUrl
        {
            get
            {
                string PICUrl;

                if ((PICUrl = ConfigurationManager.AppSettings[PICUrlSettingsKeyName]) == null)
                    throw new ApplicationException("The PICurl setting is missing from the configuration settings.");

                return PICUrl;
            }
        }

        public static string GetPICUrlclientpwd
        {
            get
            {
                string PICclientpwd;

                if ((PICclientpwd = ConfigurationManager.AppSettings[PICUrlclientpwd]) == null)
                    throw new ApplicationException("The PICurl setting is missing from the configuration settings.");

                return PICclientpwd;
            }
        }

        public static string GetZillowKey
        {
            get
            {
                string zillowk;

                if ((zillowk = ConfigurationManager.AppSettings[zillowKey]) == null)
                    throw new ApplicationException("Missing Zillow Key in configuration Settings");

                return zillowk;
            }
        }

        public static string GetZillowURL
        {
            get
            {
                string zillowk;

                if ((zillowk = ConfigurationManager.AppSettings[ZillowURL]) == null)
                    throw new ApplicationException("Missing Zillow URL in configuration Settings");

                return zillowk;
            }
        }

        public static string GetPICUrlclientId
        {
            get
            {
                string PICclientId;

                if ((PICclientId = ConfigurationManager.AppSettings[PICUrlclientId]) == null)
                    throw new ApplicationException("The PICurl setting is missing from the configuration settings.");

                return PICclientId;
            }
        }

        /// <summary>
        /// Gets the SMTP server.
        /// </summary>
        /// <value>The SMTP server.</value>
        public static string SMTPServer
        {
            get { return GetConfig<string>("SMTPServer", "Email"); }
        }

        public static string HostDomain
        {
            get { return ConfigurationManager.AppSettings["HostDomain"].ToString(); }
        }

        public static string BrandedPath
        {
            get { return ConfigurationManager.AppSettings["BrandedPath"].ToString().Replace(@"\", @"/"); }
        }

        public static bool isMiniProfilerOn
        {
            get
            {
                return

              (ConfigurationManager.AppSettings["MiniProfiler"].ToString()) == "true" ? true : false;
            }
        }

        /// <summary>
        /// Gets the task reminder minutes.
        /// </summary>
        /// <value>The task reminder minutes.</value>
        public static short GetFranchiseTaskReminderMinutes(int franchiseId)
        {
            return GetConfig<short>("DefaultMinutes", "Tasks", "Reminders", franchiseId);
        }

        /// <summary>
        /// Gets the calendar reminder minutes.
        /// </summary>
        /// <value>The calendar reminder minutes.</value>
        public static short GetFranchiseCalendarReminderMinutes(int franchiseId)
        {
            return GetConfig<short>("DefaultMinutes", "Calendar", "Reminders", franchiseId);
        }

        public static bool EnableAPILogging
        {
            get
            {
                return GetConfig<bool>("APITrace", "LogRequestAndResponse", "APITrace", null);
            }
        }

        /// <summary>
        /// Gets default margin percent for this franchise
        /// </summary>
        /// <value>The margin percent.</value>
        public static decimal GetFranchiseMarginPercent(int franchiseId)
        {
            return GetConfig<decimal>("MarginPercent", "Order", "Defaults", franchiseId);
        }

        /// <summary>
        /// Gets default jobber discount percent for this franchise
        /// </summary>
        public static decimal GetFranchiseJobberDiscountPercent(int franchiseId)
        {
            return GetConfig<decimal>("JobberDiscountPercent", "Order", "Defaults", franchiseId);
        }

        public static bool IsHttpsEnable
        {
            get
            {
                if (ConfigurationManager.AppSettings["HttpsEnable"] == null)
                {
                    return false;
                }
                return

                    (ConfigurationManager.AppSettings["HttpsEnable"].ToString()) == "true" ? true : false;
            }
        }

        /// <summary>
        /// Gets the SMTP server port.
        /// </summary>
        /// <value>The SMTP server port.</value>
        public static string SMTPServerPort
        {
            get { return GetConfig<string>("SMTPServerPort", "Email"); }
        }

        /// <summary>
        /// Gets a single application configuration setting
        /// </summary>
        /// <param name="name">Name of the config setting</param>
        /// <param name="groupName">Group that this setting belongs to</param>
        /// <param name="subGroupName">Name of the sub group.</param>
        /// <param name="franchiseId">Optional Franchise Id to include, with base config setting as fall back if there is no franchise specific config</param>
        /// <returns>AppConfig.</returns>
        public static AppConfig GetConfig(string name, string groupName, string subGroupName = null, int? franchiseId = null)
        {
            if (CacheManager.AppConfigCollection != null)
            {
                if (!string.IsNullOrEmpty(subGroupName))
                {
                    return CacheManager.AppConfigCollection.Where(f =>
                        (!f.FranchiseId.HasValue || f.FranchiseId == franchiseId) &&
                        f.GroupName.Equals(groupName, StringComparison.CurrentCultureIgnoreCase) &&
                        subGroupName.Equals(f.SubGroupName, StringComparison.CurrentCultureIgnoreCase) &&
                        f.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase)
                    ).OrderByDescending(o => o.FranchiseId).FirstOrDefault();
                }
                else
                {
                    return CacheManager.AppConfigCollection.Where(f =>
                        (!f.FranchiseId.HasValue || f.FranchiseId == franchiseId) &&
                       f.GroupName.Equals(groupName, StringComparison.CurrentCultureIgnoreCase) &&
                       string.IsNullOrEmpty(f.SubGroupName) &&
                       f.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase)
                    ).OrderByDescending(o => o.FranchiseId).FirstOrDefault();
                }
            }
            else
                return null;
        }

        /// <summary>
        /// Gets a list of application configuration
        /// </summary>
        /// <param name="groupName">Optional, filter list by this group name</param>
        /// <param name="subGroupName">Optional, filter list by this sub group</param>
        /// <param name="franchiseId">Optional, will replace base config with franchise specific config of the same Config Name, Group, and Subgroup</param>
        /// <returns>List&lt;AppConfig&gt;.</returns>
        public static List<AppConfig> GetConfigs(string groupName = null, string subGroupName = null, int? franchiseId = null)
        {
            var linq = (from c in CacheManager.AppConfigCollection
                        where !c.FranchiseId.HasValue
                        select c);

            if (franchiseId.HasValue)
            {
                linq = (from baseCfg in CacheManager.AppConfigCollection
                        join franCfg in CacheManager.AppConfigCollection.Where(w => w.FranchiseId == franchiseId)
                           on new { baseCfg.Name, baseCfg.GroupName, baseCfg.SubGroupName } equals
                               new { franCfg.Name, franCfg.GroupName, franCfg.SubGroupName } into lftjoin
                        from sub in lftjoin.DefaultIfEmpty()
                        where !baseCfg.FranchiseId.HasValue
                        select sub == null ? baseCfg : sub);
            }

            if (!string.IsNullOrEmpty(groupName))
            {
                linq = linq.Where(f => f.GroupName.Equals(groupName, StringComparison.CurrentCultureIgnoreCase));
            }

            if (!string.IsNullOrEmpty(subGroupName))
            {
                linq = linq.Where(f => subGroupName.Equals(f.SubGroupName, StringComparison.CurrentCultureIgnoreCase));
            }

            return linq.ToList();
        }

        /// <summary>
        /// Gets a single application configuration setting with strongly typed data
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name">Name of the config setting</param>
        /// <param name="groupName">Group that this setting belongs to</param>
        /// <param name="subGroupName">Name of the sub group.</param>
        /// <param name="franchiseId">Optional Franchise Id to include, with base config setting as fall back if there is no franchise specific config</param>
        /// <returns>T.</returns>
        public static T GetConfig<T>(string name, string groupName, string subGroupName = null, int? franchiseId = null)
        {
            var config = GetConfig(name, groupName, subGroupName, franchiseId);
            if (config != null)
                return Util.CastObject<T>(config.Value);
            else
                return default(T);
        }

        private static Assembly getApplicationAssembly()
        {
            // Try the EntryAssembly, this doesn't work for ASP.NET classic pipeline (untested on integrated)
            Assembly ass = Assembly.GetEntryAssembly();

            // Look for web application assembly
            HttpContext ctx = HttpContext.Current;
            if (ctx != null)
                ass = getWebApplicationAssembly(ctx);

            // Fallback to executing assembly
            return ass ?? (Assembly.GetExecutingAssembly());
        }

        private static Assembly getWebApplicationAssembly(HttpContext context)
        {
            object app = context.ApplicationInstance;
            if (app == null) return null;

            Type type = app.GetType();
            while (type != null && type != typeof(object) && type.Namespace == "ASP")
                type = type.BaseType;

            return type.Assembly;
        }

        public static string googleapikey
        {
            get
            {
                string key = "";
                if (ConfigurationManager.AppSettings["googleapikey"] != null)
                    key = ConfigurationManager.AppSettings["googleapikey"];
                return key;
            }
        }

        //public static bool IsApplicationInADomain()
        //{
        //    ManagementObject ComputerSystem;
        //    using (ComputerSystem = new ManagementObject(String.Format("Win32_ComputerSystem.Name='{0}'", Environment.MachineName)))
        //    {
        //        ComputerSystem.Get();
        //        UInt16 DomainRole = (UInt16)ComputerSystem["DomainRole"];
        //        return (DomainRole != 0 & DomainRole != 2);
        //    }
        //}
    }
}