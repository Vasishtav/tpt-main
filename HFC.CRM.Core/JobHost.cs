﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading;
//using System.Web;
//using System.Web.Hosting;

///// <summary>
///// The Core namespace.
///// </summary>
//namespace HFC.CRM.Core
//{
//    /// <summary>
//    /// Class JobHost.
//    /// </summary>
//    public class JobHost : IRegisteredObject
//    {
//        /// <summary>
//        /// The _lock
//        /// </summary>
//        private readonly object _lock = new object();
//        /// <summary>
//        /// The _shutting down
//        /// </summary>
//        private bool _shuttingDown;
//        /// <summary>
//        /// Gets a value indicating whether this instance is busy.
//        /// </summary>
//        /// <value><c>true</c> if this instance is busy; otherwise, <c>false</c>.</value>
//        public bool IsBusy { get; private set; }

//        /// <summary>
//        /// Initializes a new instance of the <see cref="JobHost"/> class.
//        /// </summary>
//        public JobHost()
//        {
//            HostingEnvironment.RegisterObject(this);
//        }

//        /// <summary>
//        /// Requests a registered object to unregister.
//        /// </summary>
//        /// <param name="immediate">true to indicate the registered object should unregister from the hosting environment before returning; otherwise, false.</param>
//        public void Stop(bool immediate)
//        {
//            lock (_lock)
//            {
//                _shuttingDown = true;
//            }
//            HostingEnvironment.UnregisterObject(this);
//        }

//        /// <summary>
//        /// Does the work.
//        /// </summary>
//        /// <param name="work">The work.</param>
//        public void DoWork(Action work)
//        {
//            lock (_lock)
//            {
//                if (_shuttingDown)
//                {
//                    return;
//                }
//                if (!IsBusy)
//                {
//                    IsBusy = true;
//                    ThreadPool.QueueUserWorkItem(q => {
//                        work();
//                        IsBusy = false;
//                    });
//                }
//            }
//        }
//    }
//}