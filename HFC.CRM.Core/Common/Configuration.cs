﻿
namespace HFC.CRM.Core.Common
{
    public class Configuration
    {
        /// <summary>
        /// The name of the environment that the application is currently running under.
        /// </summary>
        public static string EnvironmentName
        {
            get
            {
                try
                {
                    return AppSettings.Get<string>(ContantStrings.EnvironmentNameAppSetting);
                }
                catch
                {
                    return "EnvironmentName setting is unavailable.";
                }
            }
        }

        /// <summary>
        /// The URL called by QuickBooks Web Connector to initiate sync operations
        /// </summary>
        public static string QuickBooksDesktopSyncUrl
        {
            get
            {
                try
                {
                    return AppSettings.Get<string>(ContantStrings.QuickBooksDesktopSyncUrlAppSetting);
                }
                catch
                {
                    return "QuickBooksDesktopSyncUrl setting is unavailable.";
                }
            }
        }

        /// <summary>
        /// The Application Insights Instrumentation Key.
        /// </summary>
        public static string InstrumentationKey
        {
            get
            {
                try
                {
                    return AppSettings.Get<string>(ContantStrings.InstrumentationKeyAppSetting);
                }
                catch
                {
                    return "InstrumentationKey setting is unavailable.";
                }
            }
        }

        /// <summary>
        /// The URL of the page that users can open to view the status of the synchronization. This includes the
        /// QuickBooks Sync url (QuickBooksDesktopSyncUrl).
        /// </summary>
        public static string SynchronizationStatusUrl
        {
            get
            {
                try
                {
                    string syncStatusUrlStub = AppSettings.Get<string>(ContantStrings.SynchronizationStatusUrlAppSetting);
                    string syncStatusUrl = string.Format("{0}{1}", QuickBooksDesktopSyncUrl, syncStatusUrlStub);
                    return syncStatusUrl;
                }
                catch
                {
                    return "SynchronizationStatusUrl setting is unavailable.";
                }
            }
        }
    }
}
