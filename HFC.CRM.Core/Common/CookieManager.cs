﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HFC.CRM.Data;

/// <summary>
/// The Common namespace.
/// </summary>
namespace HFC.CRM.Core.Common
{
    /// <summary>
    /// Summary description for CookieManager
    /// </summary>
    public class CookieManager
    {
        /// <summary>
        /// The User Id of the user to impersonate.  Setting this value will clear any current session franchise and user, then retrieved and set for the impersonated user.
        /// </summary>
        /// <value>The impersonate user identifier.</value>
        public static Guid? ImpersonateUserId
        {
            get 
            {
                return GetCookie<Guid?>("ImpersonateUserId");                
            }
            set
            {
                if (value != null && value != Guid.Empty)
                    SetCookie("ImpersonateUserId", value.Value.ToString());
                else
                    SetCookie("ImpersonateUserId", null);

                SessionManager.CurrentFranchise = null; //reset franchise
                SessionManager.CurrentUser = null; //reset user
            }
        }

        /// <summary>
        /// Gets the cookie.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name">The name.</param>
        /// <returns>T.</returns>
        public static T GetCookie<T>(string name)
        {            
            if (HttpContext.Current.Request.Cookies.AllKeys.Contains(name))            
                return Util.CastObject<T>(HttpContext.Current.Request.Cookies[name].Value);
            else
                return default(T);
        }

        /// <summary>
        /// Sets cookie
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        /// <param name="expireTime">Default 30 days</param>
        public static void SetCookie(string name, string value, TimeSpan? expireTime = null)
        {
            if (!expireTime.HasValue)
                expireTime = TimeSpan.FromDays(30);

            HttpCookie cookie = HttpContext.Current.Request.Cookies.Get(name);
                            
            if (cookie != null)
            {
                cookie.Value = value;                
            }
            else
            {
                cookie = new HttpCookie(name, value);                
            }
            
            cookie.Expires = DateTime.Now.Add(expireTime.Value);            
            HttpContext.Current.Response.Cookies.Add(cookie);
        }
    }
}