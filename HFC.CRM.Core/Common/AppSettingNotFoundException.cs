﻿using System;

namespace HFC.CRM.Core.Common
{
    public class AppSettingNotFoundException : Exception
    {
        public AppSettingNotFoundException()
        {
        }

        public AppSettingNotFoundException(string message)
            : base(message)
        {
        }

        public AppSettingNotFoundException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
