﻿namespace HFC.CRM.Core.Common
{
    public class ContantStrings
    {
        public const string Locked = "Locked";
        /// <summary>
        /// The failed by error
        /// </summary>

        public const string Unauthorized = "Sorry, you do not have permission";
        /// <summary>
        /// The failed by error
        /// </summary>
        public const string FailedByError = "An unexpected error occured, please try again later";
        /// <summary>
        /// The failed save changes
        /// </summary>
        public const string FailedSaveChanges = "Failed to save changes";
        /// <summary>
        /// The invalid file path
        /// </summary>
        public const string InvalidFilePath = "Action cancelled, invalid file path";
        /// <summary>
        /// The duplicate file name
        /// </summary>
        public const string DuplicateFileName = "Action cancelled, duplicate file name";
        /// <summary>
        /// The file does not exist
        /// </summary>
        public const string FileDoesNotExist = "Action cancelled, file does not exist";
        /// <summary>
        /// The success
        /// </summary>
        public const string Success = "Success";
        /// <summary>
        /// The lead does not exist
        /// </summary>
        public const string LeadDoesNotExist = "Action cancelled, lead does not exist";
        /// <summary>
        /// The account does not exist
        /// </summary>
        public const string AccountTPDoesNotExist = "Action cancelled, account does not exist";

        /// <summary>
        /// The opportunity does not exist
        /// </summary>
        public const string OpportunityDoesNotExist = "Action cancelled, opportunity does not exist";
        /// <summary>
        /// The job does not exist
        /// </summary>
        public const string JobDoesNotExist = "Action cancelled, job does not exist";
        /// <summary>
        /// The job does not exist
        /// </summary>
        public const string QuoteDoesNotExist = "Action cancelled, quote does not exist";
        /// <summary>
        /// The address does not exist
        /// </summary>
        public const string AddressDoesNotExist = "Action cancelled, address does not exist";
        /// <summary>
        /// The address does not exist
        /// </summary>
        public const string LeadsAddressOneLeft = "Action cancelled, lead must at minimum have one address";

        /// <summary>
        /// The Opportunitys address does not exist
        /// </summary>
        public const string OpportunitysAddressOneLeft = "Action cancelled, Opportunity must at minimum have one address";
        /// <summary>
        /// The Opportunitys address does not exist
        /// </summary>
        public const string AccountAddressOneLeft = "Action cancelled, Account must at minimum have one address";
        /// <summary>
        /// The calendar does not exist
        /// </summary>
        public const string CalendarDoesNotExist = "Action cancelled, calendar does not exist";
        /// <summary>
        /// The data cannot be null or empty
        /// </summary>
        public const string DataCannotBeNullOrEmpty = "Action cancelled, data can not be empty";
        /// <summary>
        /// The task does not exist
        /// </summary>
        public const string TaskDoesNotExist = "Action cancelled, task(s) does not exist";
        /// <summary>
        /// The entity requires subject
        /// </summary>
        public const string EntityRequiresSubject = "This entity requires a subject";
        /// <summary>
        /// The entity requires organizer
        /// </summary>
        public const string EntityRequiresOrganizer = "This entity is missing an organizer";
        /// <summary>
        /// The entity requires attendee
        /// </summary>
        public const string EntityRequiresAttendee = "This entity requires at least one attendee";
        /// <summary>
        /// The entity organizer not in franchise
        /// </summary>
        public const string EntityOrganizerNotInFranchise = "Unable to update, this event's organizer does not exist within your franchise.  Please contact the original organizer to make changes.";
        /// <summary>
        /// The invalid revision sequence
        /// </summary>
        public const string InvalidRevisionSequence = "Invalid revision sequence, please get the latest calendar before trying again.";

        //for exchange and google managers
        /// <summary>
        /// The appointment does not exist
        /// </summary>
        public const string AppointmentDoesNotExist = "Appointment does not exist";
        /// <summary>
        /// The invalid access token
        /// </summary>
        public const string InvalidAccessToken = "Invalid access token";
        public const string LeadSourceDuplicate = "Additional Source cannot include Primary Source";
        public const string OpportunitySourceDuplicate = "Additional Source cannot include Primary Source";
        public const string AccountSourceDuplicate = "Additional Source cannot include Primary Source";
        /// <summary>
        /// The synchronize not enabled
        /// </summary>
        public const string SyncNotEnabled = "Sync is not enabled or user's sync time starts on a later date";
        /// <summary>
        /// The push subscription not found
        /// </summary>
        public const string PushSubscriptionNotFound = "Push notification subscription was not found";

        /// <summary>
        /// The payment exist //
        /// </summary>
        public const string PaymentExist = "Action cancelled, payment exists";

        /// <summary>
        /// The pricing exist //
        /// </summary>
        public const string PricingExist = "Action cancelled, pricing setting already exists";

        public const string FePricing = "Action cancelled,FE Pricing Strategy already exists";
        public const string FePricingVendor = "Action cancelled,Pricing Strategy for Vendor already exists";
        public const string FePricingProduct = "Action cancelled,Pricing Strategy for Product already exists";


        public const string OrderDoesNotExist = "Action cancelled, Order does not exists";

        public const string TerritoryNotExist = "Action cancelled, territory does not exists";
        public const string TerritoryExist = "Action cancelled, territory exists";

        public const string PerosnNotsExist = "Action cancelled, person does not exists";
        public const string UsernameInUse = "Action cancelled, username already is in use";
        public const string VendornameInUse = "Action cancelled, vendor name  already in use";


        public const string DefaultSuffix = "TPT";
        public const string ConsumerKey = "consumerKey";
        public const string ConsumerSecret = "consumerSecret";
        /// <summary>
        ///  QBO Chart Of Accounts
        /// </summary>
        public class ChartOfAccounts
        {
            public const string Sales = "Sales";
            public const string COG = "Cost of Goods Sold";
            public const string AR = "Accounts Receivable";
            public const string GeneralTaxAgencyPayable = "General Tax Agency Payable";
            public const string SalesDiscount = "Sales Discount";
        }

        /// <summary>
        ///  QBO Account Types
        /// </summary>
        public class QBOAccountTypes
        {
            public const string Income = "Income";
            public const string CostOfGoodsSold = "CostOfGoodsSold";
            public const string AccountsReceivable = "AccountsReceivable";
            public const string Taxes = "Taxes";
            public const string Discount = "SalesDiscount";
        }


        /// <summary>
        /// MPOSTatus
        /// </summary>
        public class MPOPICStatus
        {
            public const string POSSentToVendor = "POs Sent to Vendors";
            public const string PartialShipped = "PARTIAL SHIPPED";
            public const string FullShipped = "FULL SHIPPED";
        }
        /// <summary>
        /// VPOSTatus
        /// </summary>
        public class VPOPICStatus
        {

            public const string Entered = "ENTERED";
            public const string SentToVendor = "SENT TO VENDOR";
            public const string PartialShipped = "PARTIAL SHIPPED/ BACKORDERED";
            public const string FullShipped = "FULL SHIPPED";
            public const string Invoiced = "INVOICED";
            public const string Acknowledged = "ACKNOWLEDGED";
        }

        /// <summary>
        /// The payment exist //
        /// </summary>
        public const string BBPath = @"/Brand/BB";

        /// <summary>
        /// The payment exist //
        /// </summary>
        public const string TLPath = @"/Brand/TL";

        /// <summary>
        /// The name of the key in the config file containing the current environment name.
        /// </summary>
        public const string EnvironmentNameAppSetting = "EnvironmentName";

        /// <summary>
        /// The name of the key in the config file containing the Application Insights instrumentation key.
        /// </summary>
        public const string InstrumentationKeyAppSetting = "InstrumentationKey";

        /// <summary>
        /// The name of the key in the config file containing the URL to use to begin synchronization.
        /// </summary>
        public const string QuickBooksDesktopSyncUrlAppSetting = "QuickBooksDesktopSyncUrl";

        /// <summary>
        /// The text of the error message to be used when an error is encountered during generation of the QWC file.
        /// </summary>
        public const string CannotCreateQbwcFile = "The QuickBooks Web Connector file cannot be created, either because the Franchise Id is invalid, or QuickBooks has not been setup for the Franchise.";

        /// <summary>
        /// The name of the key in the config file containing the URL to use to monitor sync status.
        /// </summary>
        public const string SynchronizationStatusUrlAppSetting = "SynchronizationStatusUrl";

        public const int AdminPersonId = 1107554;

        public const string LoginSuspended = "The user name or password provided is incorrect.";

        #region Security 
        public const string GlobalAdmin = "Global Admin";
        public const string FranchiseAdmin = "Franchise Admin";
        //public const string Sales = "Sales";


        public static string GetBrandName(int brandid)
        {
            if (brandid == 1) return "BB";
            else if (brandid == 2) return "TL";
            else if (brandid == 3) return "CC";
            else return "";
        }

        #endregion
        public enum PrintVersion { CondensedVersion = 1, ExcludeDiscount = 2, IncludeDiscount = 3 };

        public const string reportDataUnauthException= "Franchise Admin and Owner roles can view all available data. Sales role can only view data associated to themselves. All other roles cannot view report data.";
        public const string InternalServerError = "Internal Server Error";
        public const string icsAppointmentfilename = "invite.ics";

        public const string phoneFormat = "(###) ###-####";
        public const string BBSitelink = "http:\\budgetblinds.com";
        public const string TLSitelink = "http:\\taloredliving.com";
        public const string CCSitelink = "http:\\concretecraft.com";

        //added newly for the communication log
        public const int LogTypeId = 18001; //log type differentiate whether it's sms or e-mail from tpt.
        public const int LogTypeIdTwo = 18002;
        //this two is to differentiate whether it's sms from tpt or adding log from ui
        public const string AddType = "Texting";
        public const string AddTypeTwo = "AddLog";
    }
}
