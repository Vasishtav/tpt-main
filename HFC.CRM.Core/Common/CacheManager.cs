﻿using System;
using System.Collections.Generic;
using System.Linq;
using HFC.CRM.Core.Membership;
using HFC.CRM.Data;
using System.Data.Entity;
using System.Runtime.Caching;
using System.Web;
using Dapper;
/// <summary>
/// The Common namespace.
/// </summary>
namespace HFC.CRM.Core.Common
{
    using HFC.CRM.Data.Context;
    using System.Data.SqlClient;

    /// <summary>
    /// Summary description for CacheManager
    /// </summary>
    public class CacheManager
    {
        /// <summary>
        /// Gets or sets the _ permission collection.
        /// </summary>
        /// <value>The _ permission collection.</value>
        private static List<Permission> _PermissionCollection
        {
            get { return GetCache<List<Permission>>("CRM:PermissionCollection"); }
            set { SetCache("CRM:PermissionCollection", value); }
        }
        /// <summary>
        /// Gets or sets the permission collection.
        /// </summary>
        /// <value>The permission collection.</value>
        internal static List<Permission> PermissionCollection
        {
            get
            {
                //return _PermissionCollection ?? (_PermissionCollection = ContextFactory.Current.Context.Permissions.ToList());
                if (_PermissionCollection == null)
                {
                    using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                    {
                        connection.Open();
                        var query = "select * from [Auth].[Permissions]";
                        _PermissionCollection = connection.Query<Permission>(query).ToList();

                    }
                }

                return _PermissionCollection;
            }
            set
            {
                _PermissionCollection = value;
            }
        }

        /// <summary>
        /// Gets or sets the _ permission type collection.
        /// </summary>
        /// <value>The _ permission type collection.</value>
        private static List<Type_Permission> _PermissionTypeCollection
        {
            get { return GetCache<List<Type_Permission>>("CRM:PermissionTypeCollection"); }
            set { SetCache("CRM:PermissionTypeCollection", value); }
        }
        /// <summary>
        /// Gets or sets the permission type collection.
        /// </summary>
        /// <value>The permission type collection.</value>
        public static List<Type_Permission> PermissionTypeCollection
        {
            get
            {
                return _PermissionTypeCollection ?? (_PermissionTypeCollection = ContextFactory.Current.Context.Type_Permission.ToList());
            }
            set
            {
                _PermissionTypeCollection = value;
            }
        }

        private static List<Type_Widgets> _WidgetTypes
        {
            get { return GetCache<List<Type_Widgets>>("CRM:WidgetTypes"); }
            set { SetCache("CRM:WidgetTypes", value); }
        }

        public static List<Type_Widgets> WidgetTypes
        {
            get
            {
                return _WidgetTypes ?? (_WidgetTypes = ContextFactory.Current.Context.Type_Widgets.ToList());
            }
        }

        /// <summary>
        /// Gets or sets the _ role collection.
        /// </summary>
        /// <value>The _ role collection.</value>
        private static List<Role> _RoleCollection
        {
            get { return GetCache<List<Role>>("CRM:RoleCollection"); }
            set { SetCache("CRM:RoleCollection", value); }
        }
        /// <summary>
        /// Gets or sets the role collection.
        /// </summary>
        /// <value>The role collection.</value>
        public static List<Role> RoleCollection
        {
            get
            {
                //return ContextFactory.Current.Context.Roles.Include(i => i.UserWidgets.Select(s => s.Type_Widgets)).ToList();
                if (_RoleCollection == null)
                {

                    using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                    {
                        connection.Open();
                        var query = @"select * from [Auth].[Roles] where IsActive=1 
                                order by sortorder";
                        _RoleCollection = connection.Query<Role>(query).ToList();

                    }
                }
                return _RoleCollection;
                //return _RoleCollection ?? (_RoleCollection = ContextFactory.Current.Context.Roles.AsNoTracking().ToList());
            }
            set
            {
                _RoleCollection = value;
            }
        }

        /// <summary>
        /// Gets or sets the _ application configuration collection.
        /// </summary>
        /// <value>The _ application configuration collection.</value>
        private static List<AppConfig> _AppConfigCollection
        {
            get { return GetCache<List<Data.AppConfig>>("CRM:AppConfigCollection"); }
            set { SetCache("CRM:AppConfigCollection", value); }
        }
        /// <summary>
        /// Returns list of all app config
        /// </summary>
        /// <value>The application configuration collection.</value>
        internal static List<AppConfig> AppConfigCollection
        {
            get
            {
                return _AppConfigCollection ?? (_AppConfigCollection = ContextFactory.Current.Context.AppConfigs.ToList());
            }
            set
            {
                _AppConfigCollection = value;
            }
        }


        /// <summary>
        /// Gets or sets the _ job status type collection.
        /// </summary>
        /// <value>The _ job status type collection.</value>
        private static List<Type_JobStatus> _JobStatusTypeCollection
        {
            get { return GetCache<List<Type_JobStatus>>("CRM:JobStatusTypeCollection"); }
            set { SetCache("CRM:JobStatusTypeCollection", value); }
        }
        /// <summary>
        /// Gets or sets the job status type collection.
        /// </summary>
        /// <value>The job status type collection.</value>
        public static List<Type_JobStatus> JobStatusTypeCollection
        {
            get
            {
                return _JobStatusTypeCollection ?? (_JobStatusTypeCollection = ContextFactory.Current.Context.Type_JobStatus.Include(i => i.Children).Where(o => o.isDeleted != true).OrderBy(o => o.ParentId).ThenBy(o => o.DisplayOrder).AsNoTracking().ToList());
            }
            set
            {
                _JobStatusTypeCollection = value;
            }
        }

        /// <summary>
        /// Gets or sets the _ lead status type collection.
        /// </summary>
        /// <value>The _ lead status type collection.</value>
        private static List<Type_LeadStatus> _LeadStatusTypeCollection
        {
            get { return GetCache<List<Type_LeadStatus>>("CRM:LeadStatusTypeCollection"); }
            set { SetCache("CRM:LeadStatusTypeCollection", value); }
        }
        private static List<Type_OpportunityStatus> _OpportunityStatusTypeCollection
        {
            get { return GetCache<List<Type_OpportunityStatus>>("CRM:OpportunitStatusTypeCollection"); }
            set { SetCache("CRM:OpportunitStatusTypeCollection", value); }
        }
        private static List<Type_AccountStatus> _AccountStatusTypeCollection
        {
            get { return GetCache<List<Type_AccountStatus>>("CRM:AccountStatusTypeCollection"); }
            set { SetCache("CRM:AccountStatusTypeCollection", value); }
        }
        /// <summary>
        /// Gets or sets the order status type collection.
        /// </summary>
        /// <value>The order status type collection.</value>
        private static List<Type_OrderStatus> _OrderStatusTypeCollection
        {
            get { return GetCache<List<Type_OrderStatus>>("CRM:OrderStatusTypeCollection"); }
            set { SetCache("CRM:OrderStatusTypeCollection", value); }
        }

        /// <summary>
        /// Gets or sets the lead status type collection.
        /// </summary>
        /// <value>The lead status type collection.</value>
        public static List<Type_LeadStatus> LeadStatusTypeCollection
        {
            get
            {
                return _LeadStatusTypeCollection
                       ?? (_LeadStatusTypeCollection =
                           ContextFactory.Current.Context.Type_LeadStatus.Include(i => i.Children).Where(o => o.isDeleted != true).OrderBy(o => o.ParentId).ThenBy(o => o.DisplayOrder).AsNoTracking().ToList());
            }
            set
            {
                _LeadStatusTypeCollection = value;
            }
        }
        public static List<Type_OpportunityStatus> OpportunityStatusTypeCollection
        {
            get
            {
                return _OpportunityStatusTypeCollection
                       ?? (_OpportunityStatusTypeCollection =
                           ContextFactory.Current.Context.Type_OpportunityStatus.Include(i => i.Children).Where(o => o.isDeleted != true).OrderBy(o => o.OpportunityStatusId).ThenBy(o => o.DisplayOrder).AsNoTracking().ToList());
            }
            set
            {
                _OpportunityStatusTypeCollection = value;
            }
        }
        public static List<Type_AccountStatus> AccountStatusTypeCollection
        {
            get
            {
                return _AccountStatusTypeCollection
                       ?? (_AccountStatusTypeCollection =
                          ContextFactory.Current.Context.Type_AccountStatus.Include(i => i.Children).Where(o => o.isDeleted != true).OrderBy(o => o.AccountStatusId).ThenBy(o => o.DisplayOrder).AsNoTracking().ToList());
            }
            set
            {
                _AccountStatusTypeCollection = value;
            }
        }

        /// <summary>
        /// Gets or sets the lead status type collection.
        /// </summary>
        /// <value>The lead status type collection.</value>
        public static List<Type_OrderStatus> OrderStatusTypeCollection
        {
            get
            {
                // return _OrderStatusTypeCollection ?? (_OrderStatusTypeCollection = ContextFactory.Current.Context.Type_OrderStatus.OrderBy(o => o.ParentId).ThenBy(o => o.DisplayOrder).ToList());
                return _OrderStatusTypeCollection ?? (_OrderStatusTypeCollection = null);
            }
            set
            {
                _OrderStatusTypeCollection = value;
            }
        }

        private static List<Manufacturer> _MfgCollection
        {
            get { return GetCache<List<Manufacturer>>("CRM:ManufacturerCollection"); }
            set { SetCache("CRM:ManufacturerCollection", value); }
        }
        /// <summary>
        /// Gets a list of manufacturers
        /// </summary>
        public static List<Manufacturer> ManufacturerCollection
        {
            get
            {
                return _MfgCollection ?? (_MfgCollection = ContextFactory.Current.Context.Manufacturers.ToList());
            }
        }

        private static List<Type_Style> _ProductStyleCollection
        {
            get { return GetCache<List<Type_Style>>("CRM:ProductStyleCollection"); }
            set { SetCache("CRM:ProductStyleCollection", value); }
        }

        public static List<Type_Style> ProductStyleCollection
        {
            get
            {
                return _ProductStyleCollection ?? (_ProductStyleCollection = ContextFactory.Current.Context.Type_Styles.ToList());
            }
        }

        /// <summary>
        /// Gets or sets the _ product type collection.
        /// </summary>
        /// <value>The _ product type collection.</value>
        private static List<Type_Product> _ProductTypeCollection
        {
            get { return GetCache<List<Type_Product>>("CRM:ProductTypeCollection"); }
            set { SetCache("CRM:ProductTypeCollection", value); }
        }
        /// <summary>
        /// Gets the product type collection.
        /// </summary>
        /// <value>The product type collection.</value>
        public static List<Type_Product> ProductTypeCollection
        {
            get
            {
                return _ProductTypeCollection ?? (_ProductTypeCollection = ContextFactory.Current.Context.Type_Products.ToList());
            }
            set
            {
                _ProductTypeCollection = value;
            }

        }

        /// <summary>
        /// Gets or sets the _ product type collection.
        /// </summary>
        /// <value>The _ product type collection.</value>
        private static List<Vendor> _VendorCollection
        {
            get { return GetCache<List<Vendor>>("Acct:VendorCollection"); }
            set { SetCache("Acct:VendorCollection", value); }
        }
        /// <summary>
        /// Gets the product type collection.
        /// </summary>
        /// <value>The product type collection.</value>
        public static List<Vendor> VendorCollection
        {
            get
            {
                return _VendorCollection ?? (_VendorCollection = ContextFactory.Current.Context.Vendors.ToList());
            }
            set
            {
                _VendorCollection = value;
            }

        }



        /// <summary>
        /// Gets or sets the _ product category collection.
        /// </summary>
        /// <value>The _ product category collection.</value>
        private static List<Type_Category> _ProductCategoryCollection
        {
            get { return GetCache<List<Type_Category>>("CRM:ProductCategoryCollection"); }
            set { SetCache("CRM:ProductCategoryCollection", value); }
        }

        private static List<string> _QuestionCategoryCollection
        {
            get { return GetCache<List<string>>("CRM:QuestionCategoryCollection"); }
            set { SetCache("CRM:QuestionCategoryCollection", value); }
        }

        /// <summary>
        /// Gets the product category collection.
        /// </summary>
        /// <value>The product category collection.</value>
        public static List<Type_Category> ProductCategoryCollection
        {
            get
            {
                if (_ProductCategoryCollection == null)
                {
                    var categories = ContextFactory.Current.Context.Type_Category.Include(i => i.Category_Color.Select(cc => cc.Type_NamedColor)).AsNoTracking().ToList();
                    foreach (var typeCategory in categories)
                    {
                        typeCategory.Category_Color = typeCategory.Category_Color.Where(cc => cc.FranchiseId != null).ToList();
                    }

                    return categories;
                }
                return _ProductCategoryCollection;
            }

            set
            {
                _ProductCategoryCollection = value;
            }
        }

        /// <summary>
        /// Gets the product category collection.
        /// </summary>
        /// <value>The product category collection.</value>
        public static List<string> QuestionCategoryCollection
        {
            get
            {
                return _QuestionCategoryCollection ?? (_QuestionCategoryCollection = ContextFactory.Current.Context.Type_Questions.Select(q => q.Question).Distinct().ToList());
            }

            set
            {
                _QuestionCategoryCollection = value;
            }
        }

        private static List<Type_Appointments> _AppointmentTypes
        {
            get { return GetCache<List<Type_Appointments>>("CRM:AppointmentTypesCollection"); }
            set { SetCache("CRM:AppointmentTypesCollection", value); }
        }
        public static List<Type_Appointments> AppointmentTypes
        {
            get
            {
                if (_AppointmentTypes == null)
                {
                    _AppointmentTypes = ContextFactory.Current.Context.Type_Appointments.ToList();
                }
                return _AppointmentTypes;
            }

            set
            {
                _AppointmentTypes = value;
            }
        }
        /// <summary>
        /// Gets or sets the _ concept collection.
        /// </summary>
        /// <value>The _ concept collection.</value>
        private static List<Type_Concept> _ConceptCollection
        {
            get { return GetCache<List<Type_Concept>>("CRM:ConceptCollection"); }
            set { SetCache("CRM:ConceptCollection", value); }
        }
        /// <summary>
        /// Gets the concept collection.
        /// </summary>
        /// <value>The concept collection.</value>
        public static List<Type_Concept> ConceptCollection
        {
            get
            {
                //var brand = string.Empty;
                //switch (AppConfigManager.HostDomain)
                //{
                //    case "tailoredliving":
                //        brand = "tl";
                //        break;
                //    case "budgetblinds":
                //        brand = "bb";
                //        break;
                //    case "concretecraft":
                //        brand = "cc";
                //        break;
                //    default:
                //        break;
                //}
                //if (brand == "bb")
                //{
                //    return _ConceptCollection ?? (_ConceptCollection = ContextFactory.Current.Context.Type_Concepts.Where(w => w.Id ==1 || w.Id == 2 || w.Id==3).ToList());
                //}
                //if (brand == "cc")
                //{
                //    return _ConceptCollection ?? (_ConceptCollection = ContextFactory.Current.Context.Type_Concepts.Where(w => w.Id == 1 || w.Id == 2).ToList());
                //}
                //if (brand == "tl")
                //{
                //    return _ConceptCollection ?? (_ConceptCollection = ContextFactory.Current.Context.Type_Concepts.Where(w => w.Id == 1 || w.Id == 2).ToList());
                //}

                if (_ConceptCollection != null) return _ConceptCollection;

                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    // TODO: do we really need a brand filter here????
                    // replaced with brandidold
                    _ConceptCollection = connection.GetList<Type_Concept>("where BrandId = @BrandId", new { BrandId = SessionManager.BrandId }).ToList();//changed brandoldId to  BrandId


                    return _ConceptCollection;
                }
            // old query // return _ConceptCollection ?? (_ConceptCollection = ContextFactory.Current.Context.Type_Concepts.Take(3).ToList());
        }
    }

    /// <summary>
    /// Gets or sets the _ source type collection.
    /// </summary>
    /// <value>The _ source type collection.</value>
    private static List<Source> _SourceCollection
    {
        get { return GetCache<List<Source>>("CRM:SourceCollection"); }
        set { SetCache("CRM:SourceCollection", value); }
    }
    /// <summary>
    /// Gets or sets the source type collection.
    /// </summary>
    /// <value>The source type collection.</value>
    public static List<Source> SourceCollection
    {
        get
        {
            return _SourceCollection ?? (_SourceCollection = ContextFactory.Current.Context.Sources.AsNoTracking().ToList());
        }
        set
        {
            _SourceCollection = null;
        }
    }
    private static List<TimeZones> _TimeZoneCollection
    {
        get { return GetCache<List<TimeZones>>("CRM:TimeZoneCollection"); }
        set { SetCache("CRM:TimeZoneCollection", value); }
    }
    /// <summary>
    /// Gets or sets the TimeZones collection.
    /// </summary>
    /// <value>The sTimeZones collection.</value>
    public static List<TimeZones> TimeZoneCollection
    {
        get
        {
            //return _TimeZoneCollection ?? (_TimeZoneCollection = ContextFactory.Current.Context.TimeZones.AsNoTracking().ToList());
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = "select * from [CRM].[Timezones]";
                _TimeZoneCollection = connection.Query<TimeZones>(query).ToList();

            }
            return _TimeZoneCollection ?? (_TimeZoneCollection = _TimeZoneCollection);
        }
        set
        {
            _TimeZoneCollection = null;
        }
    }

    /// <summary>
    /// Gets or sets the _ question type collection.
    /// </summary>
    /// <value>The _ question type collection.</value>
    private static List<Type_Question> _QuestionTypeCollection
    {
        get { return GetCache<List<Type_Question>>("CRM:QuestionTypeCollection"); }
        set { SetCache("CRM:QuestionTypeCollection", value); }
    }

    /// <summary>
    /// Gets the question type collection.
    /// </summary>
    /// <value>The question type collection.</value>
    public static List<Type_Question> QuestionTypeCollection
    {
        get
        {
            return _QuestionTypeCollection ?? (_QuestionTypeCollection = ContextFactory.Current.Context.Type_Questions.OrderBy(o => o.Question).ThenBy(o => o.DisplayOrder).ToList());
        }
        set
        {
            _QuestionTypeCollection = value;
        }
    }


    private static List<Product> _ProductCollection
    {
        get { return GetCache<List<Product>>("CRM:ProductCollection"); }
        set { SetCache("CRM:ProductCollection", value); }
    }

    /// <summary>
    /// Gets the product collection.
    /// </summary>
    /// <value>prdoucts.</value>
    public static List<Product> ProductCollection
    {
        get
        {
            return _ProductCollection ?? (_ProductCollection = ContextFactory.Current.Context.Products.ToList());
        }
        set
        {
            _ProductCollection = value;
        }
    }

    /// <summary>
    /// Gets or sets the _ states collection.
    /// </summary>
    /// <value>The _ states collection.</value>
    private static List<Type_State> _StatesCollection
    {
        get { return GetCache<List<Type_State>>("CRM:StatesCollection"); }
        set { SetCache("CRM:StatesCollection", value); }
    }

    private static List<Type_OpportunityType> _OpportunictyTypeCollection
    {
        get { return GetCache<List<Type_OpportunityType>>("CRM:OpportunityTypeCollection"); }
        set { SetCache("CRM:OpportunityTypeCollection", value); }
    }

    /// <summary>
    /// Gets or sets the _ states collection.
    /// </summary>
    /// <value>The _ states collection.</value>
    private static List<Type_RoyaltyProfile> _RoyaltyProfilesCollection
    {
        get { return GetCache<List<Type_RoyaltyProfile>>("CRM:RoyaltyProfilesCollection"); }
        set { SetCache("CRM:RoyaltyProfilesCollection", value); }
    }

    /// <summary>
    /// Gets the states collection.
    /// </summary>
    /// <value>The states collection.</value>
    public static List<Type_State> StatesCollection
    {
        get
        {
            return _StatesCollection ?? (_StatesCollection = ContextFactory.Current.Context.Type_States.Include("Type_Country").ToList());
        }
    }

    public static List<Type_OpportunityType> OpportunityTypeCollection
    {
        get
        {
            if (_OpportunictyTypeCollection == null)
            {

                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var query = "select * from crm.Type_OpportunityTypes where IsEnabled = 1";
                    _OpportunictyTypeCollection = connection.Query<Type_OpportunityType>(query).ToList();

                }
            }

            return _OpportunictyTypeCollection; //?? (_StatesCollection = ContextFactory.Current.Context.Type_States.Include("Type_Country").ToList());
        }
    }

    /// <summary>
    /// Gets the states collection.
    /// </summary>
    /// <value>The states collection.</value>
    public static List<Type_RoyaltyProfile> RoyaltyProfilesCollection
    {
        get
        {
            // if _RoyaltyProfilesCollection IS NULL fill this with all the values from the db.
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                if (_RoyaltyProfilesCollection == null)

                    // TODO: do we really need brandid filter here???
                    return connection.GetList<Type_RoyaltyProfile>().Where(x => x.BrandId == SessionManager.BrandId).ToList();//Changed BrandoldId to BrandId 
                    else
                    return _RoyaltyProfilesCollection;
                // Filter based on brandid and return values specific to the current brand.
                connection.Close();
            }
        }
    }

    /// <summary>
    /// Gets or sets the _ color palettes.
    /// </summary>
    /// <value>The _ color palettes.</value>
    private static List<Type_Color> _ColorPalettes
    {
        get { return GetCache<List<Type_Color>>("CRM:ColorPalettes"); }
        set { SetCache("CRM:ColorPalettes", value); }
    }
    /// <summary>
    /// Gets the color palettes.
    /// </summary>
    /// <value>The color palettes.</value>
    public static List<Type_Color> ColorPalettes
    {
        get
        {
            return _ColorPalettes ?? (_ColorPalettes = ContextFactory.Current.Context.Type_Colors.ToList());
        }
    }

    /// <summary>
    /// Refresh the color palettes.
    public static void RefreshColorPalettes()
    {
            _ColorPalettes = ContextFactory.Current.Context.Type_Colors.ToList();
    }

    /// <summary>
    /// Gets or sets the _ user collection.
    /// </summary>
    /// <value>The _ user collection.</value>
    private static List<User> _UserCollection
    {
        get { return GetCache<List<User>>("CRM:UserCollection"); }

        //15 minute absolute timeout so this can be as fresh as possible without re-querying DB often
        set
        {
            if (value == null)
            {
                RemoveCache("CRM:UserCollection");
            }
            else
            {
                SetCache("CRM:UserCollection", value, DateTime.Now.AddMinutes(15));
            }

        }
    }
    /// <summary>
    /// All User collection for quick reference, has 15 minute expire time
    /// </summary>
    /// <value>The user collection.</value>
    public static List<User> UserCollection
    {
        get
        {
            return _UserCollection ?? (_UserCollection = LocalMembership.GetUsers(null, "OAuthUsers", "Roles", "Person"));
        }
        set
        {
            _UserCollection = value;
        }
    }
    /// <summary>
    /// Only clears CRM related cache
    /// </summary>
    public static void ClearCache()
    {
        //clear cache properties here
        _AppConfigCollection = null;
        _ColorPalettes = null;
        _ConceptCollection = null;
        _JobStatusTypeCollection = null;
        _LeadStatusTypeCollection = null;
        _OpportunityStatusTypeCollection = null;
        _MfgCollection = null;
        _OrderStatusTypeCollection = null;
        _PermissionCollection = null;
        _PermissionTypeCollection = null;
        _ProductCategoryCollection = null;
        _ProductCollection = null;
        _ProductStyleCollection = null;
        _ProductTypeCollection = null;
        _QuestionTypeCollection = null;
        _RoleCollection = null;
        _RoyaltyProfilesCollection = null;
        _SourceCollection = null;
        _StatesCollection = null;
        _UserCollection = null;
        _VendorCollection = null;
    }

    /// <summary>
    /// Gets the cache.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="name">The name.</param>
    /// <returns>T.</returns>
    public static T GetCache<T>(string name)
    {
        var cache = MemoryCache.Default;
        if (cache[name] != null)
            return Util.CastObject<T>(cache[name]);
        else
            return default(T);
    }

    /// <summary>
    /// Sets session
    /// </summary>
    /// <param name="name">The name.</param>
    /// <param name="value">The value.</param>
    /// <param name="absoluteExpire">The absolute expire.</param>
    /// <param name="slidingExpire">The sliding expire.</param>
    private static void _SetCache(string name, object value, DateTime? absoluteExpire = null, TimeSpan? slidingExpire = null)
    {
        var cache = MemoryCache.Default;
        if (value == null)
        {
            cache.Remove(name);
        }
        else
        {
            var policy = new CacheItemPolicy();
            policy.Priority = CacheItemPriority.Default;
            if (slidingExpire.HasValue)
            {
                policy.SlidingExpiration = slidingExpire.Value;
                policy.AbsoluteExpiration = ObjectCache.InfiniteAbsoluteExpiration;
            }
            else
            {
                if (!absoluteExpire.HasValue)
                    absoluteExpire = DateTime.Now.AddHours(10);
                policy.AbsoluteExpiration = absoluteExpire.Value;
                policy.SlidingExpiration = ObjectCache.NoSlidingExpiration;
            }
            if (cache[name] != null)
                cache.Set(name, value, policy);
            else
                cache.Add(name, value, policy);
        }
    }

    /// <summary>
    /// Sets cache with default of 10 hour absolute expire
    /// </summary>
    /// <param name="name">The name.</param>
    /// <param name="value">The value.</param>
    public static void SetCache(string name, object value)
    {
        _SetCache(name, value);
    }

    /// <summary>
    /// Removes the cache.
    /// </summary>
    /// <param name="name">The name.</param>
    public static void RemoveCache(string name)
    {
        _SetCache(name, null);
    }

    /// <summary>
    /// Sets the cache.
    /// </summary>
    /// <param name="name">The name.</param>
    /// <param name="value">The value.</param>
    /// <param name="absoluteExpire">The absolute expire.</param>
    public static void SetCache(string name, object value, DateTime absoluteExpire)
    {
        _SetCache(name, value, absoluteExpire);
    }

    /// <summary>
    /// Sets the cache.
    /// </summary>
    /// <param name="name">The name.</param>
    /// <param name="value">The value.</param>
    /// <param name="slidingExpire">The sliding expire.</param>
    public static void SetCache(string name, object value, TimeSpan slidingExpire)
    {
        _SetCache(name, value, null, slidingExpire);
    }


    /// <summary>
    /// Gets the lead status.
    /// </summary>
    /// <param name="statusId">The status identifier.</param>
    /// <returns>Type_LeadStatus.</returns>
    public static Type_LeadStatus GetLeadStatus(int statusId)
    {
        if (statusId > 0)
            return LeadStatusTypeCollection.SingleOrDefault(s => s.Id == statusId);
        else
            return null;
    }

    /// <summary>
    /// Gets the lead status string.
    /// </summary>
    /// <param name="statusId">The status identifier.</param>
    /// <returns>System.String.</returns>
    public static string GetLeadStatusString(int statusId)
    {
        var src = GetLeadStatus(statusId);
        if (src != null)
            return src.ToString();
        else
            return string.Empty;
    }

    /// <summary>
    /// Gets the job status.
    /// </summary>
    /// <param name="statusId">The status identifier.</param>
    /// <returns>Type_JobStatus.</returns>
    public static Type_JobStatus GetJobStatus(int statusId)
    {
        if (statusId > 0)
            return JobStatusTypeCollection.SingleOrDefault(s => s.Id == statusId);
        else
            return null;
    }

    /// <summary>
    /// Gets the job status string.
    /// </summary>
    /// <param name="statusId">The status identifier.</param>
    /// <returns>System.String.</returns>
    public static string GetJobStatusString(int statusId)
    {
        var src = GetJobStatus(statusId);
        if (src != null)
            return src.ToString();
        else
            return string.Empty;
    }

    /// <summary>
    /// Gets the source.
    /// </summary>
    /// <param name="sourceId">The source identifier.</param>
    /// <returns>Type_Source.</returns>
    public static Source GetSource(int sourceId)
    {
        if (sourceId > 0)
            return SourceCollection.SingleOrDefault(s => s.SourceId == sourceId);
        else
            return null;
    }

    /// <summary>
    /// Gets the source string.
    /// </summary>
    /// <param name="sourceId">The source identifier.</param>
    /// <returns>System.String.</returns>
    public static string GetSourceString(int sourceId)
    {
        var src = GetSource(sourceId);
        if (src != null)
            return src.ToString();
        else
            return string.Empty;
    }

    /// <summary>
    /// Gets the category.
    /// </summary>
    /// <param name="categoryId">The category identifier.</param>
    /// <returns>Type_Category.</returns>
    public static Type_Category GetCategory(int categoryId)
    {
        if (categoryId > 0)
            return ProductCategoryCollection.SingleOrDefault(s => s.Id == categoryId);
        else
            return null;
    }

    /// <summary>
    /// Gets the name of the category.
    /// </summary>
    /// <param name="categoryId">The category identifier.</param>
    /// <returns>System.String.</returns>
    public static string GetCategoryName(int categoryId)
    {
        var cat = GetCategory(categoryId);
        if (cat != null)
            return cat.Category;
        else
            return string.Empty;
    }
    /// <summary>
    /// Gets the type of the product.
    /// </summary>
    /// <param name="typeId">The type identifier.</param>
    /// <returns>Type_Product.</returns>
    public static Type_Product GetProductType(int typeId)
    {
        if (typeId > 0)
            return ProductTypeCollection.SingleOrDefault(s => s.ProductTypeId == typeId);
        else
            return null;
    }

    /// <summary>
    /// Gets the name of the product type.
    /// </summary>
    /// <param name="typeId">The type identifier.</param>
    /// <returns>System.String.</returns>
    public static string GetProductTypeName(int typeId)
    {
        var prodtype = GetProductType(typeId);
        if (prodtype != null)
            return prodtype.TypeName;
        else
            return string.Empty;
    }

}

    
}