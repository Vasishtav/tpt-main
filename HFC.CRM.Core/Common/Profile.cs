﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Core.Common
{
    public class Profile
    {
        public string code { get; set; }
        public string accesstoken { get; set; }
        public long accesstokenexpiresat { get; set; }
        public string refreshtoken { get; set; }
        public long refreshtokenexpiresat { get; set; }

        public string RealmId { get;set; }
        
        public string OAuthAccessToken { get; set; }
        
        public string OAuthAccessTokenSecret  { get; set; }
        
        public int DataSource { get; set; }
        
    }
}
