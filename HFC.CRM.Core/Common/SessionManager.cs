﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Caching;

using StackExchange.Profiling;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Reflection;

using DevDefined.OAuth.Framework;
using HFC.CRM.Core.Membership;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using System.Data.SqlClient;
using Dapper;
/// <summary>
/// The Common namespace.
/// </summary>
namespace HFC.CRM.Core.Common
{
    /// <summary>
    /// Manages Sessions
    /// 
    /// </summary>
    public class SessionManager
    {
        private const string currentImpersonateUserKey = "CRM_Current_User_{0}_{1}";
        private const string currentUserKey = "CRM_Current_User_{0}";
        private const string currentSecurityUserKey = "CRM_Current_SecurityUser_{0}";
        private const string currentUserQbSyncUserLog = "CRM_Current_QbSyncUserLog_{0}";
        private const string currentUserQbduplicateCustomersLog = "CRM_Current_QbduplicateCustomersLog_{0}";
        private const string currentUserQbSyncCustomersLog = "CRM_Current_QbSyncCustomersLog_{0}";
        private const string currentUserQbSyncQBCustomersLog = "CRM_Current_QbSyncCustomersLog_{0}";
        private const string currentUserQbSyncCanContinue = "CRM_Current_QbSyncCanContinue_{0}";
        private const string currentUserQbSyncPrecheckLog = "CRM_Current_QbSyncPrecheckLog_{0}";
        private const string currentUserQbEmailKey = "CRM_Current_qbEmail_{0}";
        private const string currentUserQbRequestToken = "CRM_Current_qbRequestToken_{0}";
        private const string currentUserQBToken = "CRM_Current_qbToken_{0}";
        private const string currentUserQBTokenApi = "CRM_Current_qbToken_Api";
        private const string currentUserOpenIdResponseKey = "CRM_Current_OpenIdResponse_{0}";
        private const string currentUserProfile = "CRM_Current_Profile_{0}";
        private const string currentFranchiseKey = "CRM_Current_Franchise_{0}_{1}";
        // private const string currentFranchiseRequestCacheKey = "CRM_Current_Franchise";
        // private const string currentUserRequestCacheKey = "CRM_Current_User";
        private const string cacheTime = "CRM_CacheTime";
        private const string cacheLogTime = "CRM_CacheLogTime";
        private const string ConficStatusCache = "CRM_Current_ConficStatusCache_{0}";

        private static readonly object LockObj = new object();
        private static int _franchiseId;
        private static int _brandId = 1;
        //Added for Qualification/Question Answers
        private static int _leadId = 1;
        private static int _accountId = 1;

        private static Dictionary<string, object> _myCache;

        public static string UserName
        {
            get
            {
                if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated)
                    return HttpContext.Current.User.Identity.Name;
                return null;
            }
        }

        public static string firstlastname
        {
            get
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();

                    var query = @"select * from crm.person where personid in ( select personid from Auth.Users where UserName = @UserName)";
                    var fn = connection.Query<Person>(query, new { UserName = HttpContext.Current.User.Identity.Name }).FirstOrDefault();
                    connection.Close();

                    return fn.FirstName + " " + fn.LastName;
                }
            }
        }

        public static int BrandId
        {
            get
            {

                if (CurrentFranchise != null)
                {
                    //return CurrentFranchise.BrandId;

                    using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                    {
                        connection.Open();

                        var query = @"select * from crm.franchise where FranchiseId = @franId";
                        var fn = connection.Query<Franchise>(query, new { franid = CurrentFranchise.FranchiseId }).FirstOrDefault();
                        connection.Close();

                        return fn.BrandId;
                    }
                }

                // This should be the case at all. if it arrived here, we are doing something wrong.
                return 1;
            }
            set
            {
                var dummy = value;
                // no need t set the selected brand in the session manager, as this just stores
                // the brand selected by the last user, which is a bug.
                // _brandId = value;
            }
        }

        private static int _brandidold = 1;
        public static int BrandIdOld
        {
            get
            {
                return _brandidold;
            }
            set
            {
                if (value < 1) return;
                _brandidold = value;
            }
        }

        //Added for Qualification/Question Answers
        public static int LeadId
        {
            get
            {
                return _leadId;
            }
            set
            {
                _leadId = value;
            }
        }
        public static int AccountId
        {
            get
            {
                return _accountId;
            }
            set
            {
                _accountId = value;
            }
        }
        public static int OpportunityId
        {
            get
            {
                return _leadId;
            }
            set
            {
                _leadId = value;
            }
        }

        /*public static int FranchiseId
        {
            get
            {
                if (_franchiseId != 0)
                    return _franchiseId;
                if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated)
                    _identityName = HttpContext.Current.User.Identity.Name;
                return _franchiseId;
            }
            set { _franchiseId = value; }
        }*/

        /// <summary>
        /// Gets or sets the current user.
        /// </summary>
        /// <value>The current user.</value>
        public static User CurrentUser
        {
            get
            {
                if (String.IsNullOrEmpty(UserName))
                    return null;

                string cacheKey = null;

                if (CookieManager.ImpersonateUserId.HasValue && CookieManager.ImpersonateUserId != Guid.Empty && ControlPanelPermission.CanRead)
                    cacheKey = string.Format(currentImpersonateUserKey, UserName, CookieManager.ImpersonateUserId.Value.ToString());
                else
                    cacheKey = string.Format(currentUserKey, UserName);

                var currentUser = GetFromCache<User>(cacheKey);

                if (currentUser != null)
                    return currentUser;

                if (CookieManager.ImpersonateUserId.HasValue && CookieManager.ImpersonateUserId != Guid.Empty && ControlPanelPermission.CanRead)
                    currentUser = LocalMembership.GetUser(CookieManager.ImpersonateUserId.Value, "Person", "Roles", "OAuthUsers", "UserWidgets");
                else
                    currentUser = LocalMembership.GetUser(UserName, "Person", "Roles", "OAuthUsers", "UserWidgets");

                SetCache(cacheKey, currentUser);

                return currentUser;
            }
            set
            {
                if (!String.IsNullOrEmpty(UserName))
                {
                    string cacheKey;

                    if (CookieManager.ImpersonateUserId.HasValue && CookieManager.ImpersonateUserId != Guid.Empty && ControlPanelPermission.CanRead)
                        cacheKey = string.Format(currentImpersonateUserKey, UserName, CookieManager.ImpersonateUserId.Value.ToString());
                    else
                        cacheKey = string.Format(currentUserKey, UserName);

                    SetCache(cacheKey, value);
                }
            }
        }
        /// <summary>
        /// Gets or sets the security user.
        /// </summary>
        /// <value>The security user.</value>
        public static User SecurityUser
        {
            get
            {
                if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    var cacheKey = string.Format(currentSecurityUserKey, HttpContext.Current.User.Identity.Name);

                    var securityUser = GetFromCache<User>(cacheKey);

                    if (securityUser != null) return securityUser;

                    securityUser = LocalMembership.GetUser(HttpContext.Current.User.Identity.Name, "Person", "Roles", "OAuthUsers");

                    if (securityUser != null)
                    {
                        SetCache(cacheKey, securityUser);
                        return securityUser;
                    }
                }

                return new User();
            }

            set
            {
                if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    var cacheKey = string.Format(currentSecurityUserKey, HttpContext.Current.User.Identity.Name);
                    SetCache(cacheKey, value);
                }
            }
        }

        /// <summary>
        /// Gets or sets the current franchise.
        /// </summary>
        /// <value>The current franchise.</value>
        public static Franchise CurrentFranchise
        {
            get
            {
                using (MiniProfiler.Current.Step("SessionManager.CurrentFranchise"))
                {
                    if (String.IsNullOrEmpty(UserName))
                        return null;

                    var franId = CurrentUser?.FranchiseId;
                    if (franId == null || franId == 0)
                        return null;

                    var cacheKey = string.Format(currentFranchiseKey, UserName, franId.Value);
                    var currentFranchise = GetFromCache<Franchise>(cacheKey);

                    if (currentFranchise != null)
                    {
                        if (currentFranchise.FranchiseId != CurrentUser.FranchiseId)
                            throw new UnauthorizedAccessException();
                        return currentFranchise;
                    }

                    using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                    {
                        connection.Open();
                        var franchise = connection.Get<Franchise>(franId);
                        franchise.Address = connection.Get<Address>(franchise.AddressId);
                        franchise.MailingAddress = connection.Get<Address>(franchise.MailingAddressId);
                        franchise.Territories = connection.GetList<Territory>("where franchiseid = @franchiseId", new { franchiseId = franchise.FranchiseId }).ToList();
                        foreach (var territory in franchise.Territories)
                        {
                            territory.FranchiseRoyalties = connection.GetList<FranchiseRoyalty>(
                                "where TerritoryId = @territoryId", new { territoryId = territory.TerritoryId }).ToList();
                        }

                        franchise.FranchiseOwners = connection.GetList<FranchiseOwner>("where franchiseid = @franchiseId", new { franchiseId = franchise.FranchiseId }).ToList();
                        foreach (var fowner in franchise.FranchiseOwners)
                        {
                            fowner.User = connection.Get<User>(fowner.UserId);
                            fowner.User.Address = connection.Get<Address>(fowner.User.AddressId);
                            fowner.User.Person = connection.Get<Person>(fowner.User.PersonId);
                        }

                        franchise.Users = connection.GetList<User>("where franchiseid = @franchiseId", new { franchiseId = franchise.FranchiseId }).ToList();
                        foreach (var user in franchise.Users)
                        {
                            user.Person = connection.Get<Person>(user.PersonId);
                            user.Address = connection.Get<Address>(user.AddressId);
                        }

                        connection.Close();

                        SetCache(cacheKey, franchise);

                        if (franchise.FranchiseId != CurrentUser.FranchiseId)
                            throw new UnauthorizedAccessException();

                        return franchise;
                    }
                }
            }
            set
            {
                if (!String.IsNullOrEmpty(UserName) && CurrentUser != null)
                {
                    var cacheKey = string.Format(currentFranchiseKey, UserName, CurrentUser.FranchiseId);
                    SetCache(cacheKey, value);
                }
            }
        }
        public static string QBToken
        {
            get
            {

                var cacheKey = string.Format(currentUserQBToken, UserName);

                var token = GetFromCache<string>(cacheKey);
                if (String.IsNullOrEmpty(token))
                {
                    token = Guid.NewGuid().ToString();
                    QBToken = token;
                }
                return token;
            }
            set
            {
                if (!String.IsNullOrEmpty(UserName))
                {
                    var cacheKey = string.Format(currentUserQBToken, UserName);
                    SetCache(cacheKey, value, TimeSpan.FromDays(1));
                }
            }
        }

        public static string QBTokenApi
        {
            get
            {
                var token = GetFromCache<string>(currentUserQBTokenApi);
                return token;
            }
            set
            {
                SetCache(currentUserQBTokenApi, value, TimeSpan.FromDays(1));
            }
        }

        public static Profile Profile
        {
            get
            {
                Profile profile = null;

                if (!String.IsNullOrEmpty(UserName) && CurrentUser != null)
                {
                    var cacheKey = string.Format(currentUserProfile, CurrentUser.FranchiseId);
                    profile = GetFromCache<Profile>(cacheKey);
                }
                return profile;

            }

            set
            {
                if (!String.IsNullOrEmpty(UserName) && CurrentUser != null)
                {
                    var cacheKey = string.Format(currentUserProfile, CurrentUser.FranchiseId);
                    SetCache(cacheKey, value, TimeSpan.FromHours(1));
                }
            }
        }

        public static string QbEmail
        {
            get
            {
                string email = null;

                if (!String.IsNullOrEmpty(UserName) && CurrentUser != null)
                {
                    var cacheKey = string.Format(currentUserQbEmailKey, CurrentUser.FranchiseId);
                    email = GetFromCache<string>(cacheKey);
                    return email;
                }
                return email;

            }

            set
            {
                if (!String.IsNullOrEmpty(UserName) && CurrentUser != null)
                {
                    var cacheKey = string.Format(currentUserQbEmailKey, CurrentUser.FranchiseId);
                    SetCache(cacheKey, value, TimeSpan.FromDays(1));
                }
            }
        }

        public static List<string> QbSyncUserLog
        {

            get
            {
                if (!String.IsNullOrEmpty(QBTokenApi))
                {
                    var cacheKey = string.Format(currentUserQbSyncUserLog, QBTokenApi);
                    return GetFromCache<List<string>>(cacheKey);
                }
                return null;
            }
            set
            {
                if (!String.IsNullOrEmpty(QBTokenApi))
                {
                    var cacheKey = string.Format(currentUserQbSyncUserLog, QBTokenApi);
                    SetCache(cacheKey, value, TimeSpan.FromDays(1));
                }

            }
        }

        public static void SetQbSyncUserLog(string ownerId, List<string> value)
        {
            var cacheKey = string.Format(currentUserQbSyncUserLog, ownerId);
            SetCache(cacheKey, value, TimeSpan.FromDays(1));
        }

        public static List<string> GetQbSyncUserLog(string ownerId)
        {
            var cacheKey = string.Format(currentUserQbSyncUserLog, ownerId);
            return GetFromCache<List<string>>(cacheKey);
        }

        public static void SetQbduplicateCustomersLog(string ownerId, string value)
        {
            var cacheKey = string.Format(currentUserQbduplicateCustomersLog, ownerId);
            SetCache(cacheKey, value, TimeSpan.FromDays(1));
        }

        public static string GetConfigstatus(string userId)
        {
            var cacheKey = string.Format(ConficStatusCache, userId);
            return GetFromCache<string>(cacheKey);
        }

        public static void SetConfigstatus(string userId, string value)
        {
            var cacheKey = string.Format(ConficStatusCache, userId);
            SetCache(cacheKey, value, TimeSpan.FromDays(1));
        }

        public static string GetQbduplicateCustomersLog(string ownerId)
        {
            var cacheKey = string.Format(currentUserQbduplicateCustomersLog, ownerId);
            return GetFromCache<string>(cacheKey);
        }

        public static string QbSyncCustomersLog
        {

            get
            {
                if (!String.IsNullOrEmpty(QBTokenApi))
                {
                    var cacheKey = string.Format(currentUserQbSyncCustomersLog, QBTokenApi);
                    return GetFromCache<string>(cacheKey);
                }
                return null;
            }
            set
            {
                if (!String.IsNullOrEmpty(QBTokenApi))
                {
                    var cacheKey = string.Format(currentUserQbSyncCustomersLog, QBTokenApi);
                    SetCache(cacheKey, value, TimeSpan.FromDays(1));
                }

            }
        }

        public static string QbSyncQBCustomersLog
        {

            get
            {
                if (!String.IsNullOrEmpty(QBTokenApi))
                {
                    var cacheKey = string.Format(currentUserQbSyncQBCustomersLog, QBTokenApi);
                    return GetFromCache<string>(cacheKey);
                }
                return null;
            }
            set
            {
                if (!String.IsNullOrEmpty(QBTokenApi))
                {
                    var cacheKey = string.Format(currentUserQbSyncQBCustomersLog, QBTokenApi);
                    SetCache(cacheKey, value, TimeSpan.FromDays(1));
                }

            }
        }

        public static string QbSyncCanContinue
        {

            get
            {
                if (!String.IsNullOrEmpty(QBTokenApi))
                {
                    var cacheKey = string.Format(currentUserQbSyncCanContinue, QBTokenApi);
                    return GetFromCache<string>(cacheKey) ?? "Yes";
                }
                return "Yes";
            }
            set
            {
                if (!String.IsNullOrEmpty(QBTokenApi))
                {
                    var cacheKey = string.Format(currentUserQbSyncCanContinue, QBTokenApi);
                    SetCache(cacheKey, value, TimeSpan.FromDays(1));
                }

            }
        }

        public static Dictionary<string, List<string>> QbSyncPreCheckLog
        {

            get
            {
                if (!String.IsNullOrEmpty(QBTokenApi))
                {
                    var cacheKey = string.Format(currentUserQbSyncPrecheckLog, QBTokenApi);
                    return GetFromCache<Dictionary<string, List<string>>>(cacheKey);
                }
                return null;
            }
            set
            {
                if (!String.IsNullOrEmpty(QBTokenApi))
                {
                    var cacheKey = string.Format(currentUserQbSyncPrecheckLog, QBTokenApi);
                    SetCache(cacheKey, value, TimeSpan.FromMinutes(15));
                }

            }
        }

        public static IToken QBRequestToken
        {
            get
            {
                IToken email = null;

                if (!String.IsNullOrEmpty(QBTokenApi))
                {
                    var cacheKey = string.Format(currentUserQbRequestToken, QBTokenApi);
                    email = GetFromCache<IToken>(cacheKey);
                }
                return email;

            }

            set
            {
                if (!String.IsNullOrEmpty(QBTokenApi))
                {
                    var cacheKey = string.Format(currentUserQbRequestToken, QBTokenApi);
                    SetCache(cacheKey, value, TimeSpan.FromHours(1));
                }
            }
        }

        public static bool OpenIdResponse
        {
            get
            {
                string email = null;

                if (!String.IsNullOrEmpty(QBTokenApi))
                {
                    var cacheKey = string.Format(currentUserOpenIdResponseKey, QBTokenApi);
                    bool res;
                    bool.TryParse(GetFromCache<string>(cacheKey), out res);
                    return res;
                }
                return false;

            }

            set
            {
                if (!String.IsNullOrEmpty(QBTokenApi))
                {
                    var cacheKey = string.Format(currentUserOpenIdResponseKey, QBTokenApi);
                    SetCache(cacheKey, value.ToString(), TimeSpan.FromHours(1));
                }
            }
        }


        public static DateTime CacheTime
        {
            get
            {
                var keys = GetKeys();
                DateTime t;
                var time = GetFromCache<string>(cacheTime);
                if (time == null || !DateTime.TryParse(time, out t))
                {
                    t = DateTime.Now;
                    SetCache<string>(cacheTime, t.ToString(), TimeSpan.FromDays(1));
                }
                return t;
            }
        }

        public static DateTime CacheLogTime
        {
            get
            {
                var keys = GetKeys();
                DateTime t;
                var time = GetFromCache<string>(cacheLogTime);
                if (time == null || !DateTime.TryParse(time, out t))
                {
                    t = DateTime.Now;
                    SetCache<string>(cacheLogTime, t.ToString(), TimeSpan.FromDays(1));
                }
                return t;
            }
            set
            {

                SetCache(cacheLogTime, value.ToString(), TimeSpan.FromHours(1));

            }
        }

        public static List<string> GetCacheSize()
        {
            var keys = GetKeys();
            var res = new List<string>();
            long total = 0;
            foreach (var key in keys)
            {
                var size = GetObjectSize(GetFromCache<object>(key));
                res.Add(key + " - " + GetObjectSize(GetFromCache<object>(key)));
                total += size;
            }
            res.Insert(0, "Total - " + total);
            return res;
        }

        public static int GetSizeOfObject(object obj)
        {
            object Value = null;
            int size = 0;
            Type type = obj.GetType();
            PropertyInfo[] info = type.GetProperties();
            foreach (PropertyInfo property in info)
            {
                Value = property.GetValue(obj, null);
                try
                {
                    size += GetObjectSize(Value);
                }
                catch { }
            }
            return size;
        }

        private static List<string> GetKeys()
        {
            var res = new List<string>();
            var CacheEnum = HttpContext.Current.Cache.GetEnumerator();
            while (CacheEnum.MoveNext())
            {
                res.Add(((System.Collections.DictionaryEntry)CacheEnum.Current).Key.ToString());
            }
            return res;
        }



        private static int GetObjectSize(object TestObject)
        {
            try
            {
                BinaryFormatter bf = new BinaryFormatter();
                MemoryStream ms = new MemoryStream();
                byte[] Array;
                bf.Serialize(ms, TestObject);
                Array = ms.ToArray();
                return Array.Length;
            }
            catch
            {
                try
                {
                    return GetSizeOfObject(TestObject);
                }
                catch { }
                return -1;
            }
        }

        /// <summary>
        /// Gets or sets the pending o authentication authorization.
        /// </summary>
        /// <value>The pending o authentication authorization.</value>
        public static object PendingOAuthAuthorization { get; set; }

        /// <summary>
        /// Gets or sets the authorization secret.
        /// </summary>
        /// <value>The authorization secret.</value>
        public static string AuthorizationSecret { get; set; }

        /// <summary>
        /// Gets the control panel permission.
        /// </summary>
        /// <value>The control panel permission.</value>
        public static Permission ControlPanelPermission
        {
            get
            {
                // TODO : verify whether this does not break anything - murugan
                //var canCUD = SecurityUser.IsInRole(AppConfigManager.AppAdminRole.RoleId, AppConfigManager.DefaultOperatorRole.RoleId);
                var permission = new Permission
                {
                    //CanRead = SecurityUser.IsInRole(AppConfigManager.AppAdminRole.RoleId,
                    //    AppConfigManager.DefaultOperatorRole.RoleId,
                    //    AppConfigManager.DefaultMonitorRole.RoleId),

                    //CanRead = SecurityUser.IsInRole(AppConfigManager.AppAdminRole.RoleId),
                    CanRead = SecurityUser.IsAdminInRole(),

                    //CanUpdate = canCUD,
                    //CanCreate = canCUD,
                    //CanDelete = canCUD
                };

                permission.CanAccess = permission.CanRead;
                return permission;
            }
        }

        public static Permission VendorPanelPermission
        {
            get
            {
                // TODO : verify whether this does not break anything - murugan
                //var canCUD = SecurityUser.IsInRole(AppConfigManager.AppAdminRole.RoleId, AppConfigManager.DefaultOperatorRole.RoleId);
                var permission = new Permission
                {
                    //CanRead = SecurityUser.IsInRole(AppConfigManager.AppAdminRole.RoleId,
                    //    AppConfigManager.DefaultOperatorRole.RoleId,
                    //    AppConfigManager.DefaultMonitorRole.RoleId),

                    CanRead = SecurityUser.IsInRole(AppConfigManager.AppVendorRole.RoleId),

                    //CanUpdate = canCUD,
                    //CanCreate = canCUD,
                    //CanDelete = canCUD
                };

                permission.CanAccess = permission.CanRead;
                return permission;
            }
        }

        public static Permission PicPanelPermission
        {
            get
            {
                var permission = new Permission
                {
                    CanRead = SecurityUser.IsInRole(AppConfigManager.AppPicRole.RoleId),
                };
                permission.CanAccess = permission.CanRead;
                return permission;
            }
        }

        public static Permission ProductStrgMangPermission
        {
            get
            {
                var permission = new Permission
                {
                    CanRead = SecurityUser.IsInRole(AppConfigManager.AppProductStrgMang.RoleId),
                };
                permission.CanAccess = permission.CanRead;
                return permission;
            }
        }

        public static void Clear()
        {
            //QBRequestToken = null;
            //QbEmail = null;
            //Profile = null;
            CurrentUser = null;
            SecurityUser = null;
            CurrentFranchise = null;
            QBToken = null;
        }

        public static T GetFromCache<T>(string key, Func<T> loader = null) where T : class
        {
            if (HttpContext.Current == null)
            {
                if (_myCache == null) _myCache = new Dictionary<string, object>();
                return _myCache[key] as T;
            }

            var result = HttpContext.Current.Cache[key] as T;

            if (result == null && loader != null)
            {
                result = loader();
                SetCache<T>(key, result);
            }

            return result;

        }

        public static T GetFromRequestCache<T>(string key) where T : class
        {
            lock (LockObj)
            {
                var result = HttpContext.Current.Items[key] as T;
                return result;
            }
        }

        public static void SetCache<T>(string key, T value, TimeSpan? ts = null)
        {
            lock (LockObj)
            {
                if (HttpContext.Current == null)
                {
                    if (_myCache == null) _myCache = new Dictionary<string, object>();
                    _myCache[key] = value;
                    return;
                }

                if (HttpContext.Current.Cache[key] != null)
                {
                    HttpContext.Current.Cache.Remove(key);
                }

                if (value != null)
                {
                    if (ts == null)
                    {
                        ts = TimeSpan.FromSeconds(60);
                    }

                    HttpContext.Current.Cache.Add(key, value, null, System.Web.Caching.Cache.NoAbsoluteExpiration, ts.Value,
                        CacheItemPriority.Normal, null);
                }
            }
        }

        public static void SetRequestCache<T>(string key, T value)
        {
            lock (LockObj)
            {
                if (HttpContext.Current.Items[key] != null)
                {
                    HttpContext.Current.Items.Remove(key);
                }

                if (value != null)
                {
                    HttpContext.Current.Items.Add(key, value);
                }
            }
        }

    }
}