﻿//JUST AN IDEA - NOT TESTED YET

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Web;
//using System.IO;
//using System.Text.RegularExpressions;
//using HFC.CRM.Data;

//namespace HFC.CRM.Core.Modules
//{
//    /// <summary>
//    /// WORK IN PROGRESS!! NOT TESTED YET!
//    /// </summary>
//    public class CDN : IHttpModule
//    {
//        public void Dispose()
//        {
//            //throw new NotImplementedException();
//        }

//        public void Init(HttpApplication context)
//        {
//            context.PostRequestHandlerExecute += new EventHandler(context_PostRequestHandlerExecute);
//        }

//        void context_PostRequestHandlerExecute(object sender, EventArgs e)
//        {
//            if (HFC.CRM.Core.AppConfigManager.CDNUrlCollection != null && HFC.CRM.Core.AppConfigManager.CDNUrlCollection.Count > 0)
//            {
//                var watcher = new StreamWatcher(HttpContext.Current.Response.Filter);
//                HttpContext.Current.Response.Filter = watcher;
//            }
//        }

        
//    }

//    internal class StreamWatcher : Stream
//    {
//        private Stream CurrentFilter;

//        public StreamWatcher(Stream filter)
//        {
//            CurrentFilter = filter;
//        }

//        #region Stream Interface Properties and Methods

//        public override bool CanRead
//        {
//            get { return true; }
//        }

//        public override bool CanSeek
//        {
//            get { return true; }
//        }

//        public override bool CanWrite
//        {
//            get { return true; }
//        }

//        public override void Flush()
//        {
//            CurrentFilter.Flush();
//        }

//        public override long Length
//        {
//            get { return CurrentFilter.Length; }
//        }

//        public override long Position
//        {
//            get
//            {
//                return CurrentFilter.Position;
//            }
//            set
//            {
//                CurrentFilter.Position = value;
//            }
//        }

//        public override int Read(byte[] buffer, int offset, int count)
//        {
//            return CurrentFilter.Read(buffer, offset, count);
//        }

//        public override long Seek(long offset, SeekOrigin origin)
//        {
//            return CurrentFilter.Seek(offset, origin);
//        }

//        public override void SetLength(long value)
//        {
//            CurrentFilter.SetLength(value);
//        }

//        public override void Write(byte[] buffer, int offset, int count)
//        {
//            byte[] data = new byte[count];
//            Buffer.BlockCopy(buffer, offset, data, 0, count);
//            string html = System.Text.Encoding.Default.GetString(buffer);

//            TransformHtml(ref html);

//            byte[] outdata = System.Text.Encoding.Default.GetBytes(html);
//            CurrentFilter.Write(outdata, 0, outdata.GetLength(0));

//        }

//        #endregion

//        private void TransformHtml(ref string html)
//        {
//            string regExPatternForImgTags = @"]* [src|href]=\""([^\""]*)\""[^>]*>"; //this will match all the  tags with src attribute, you may need to extend this. 

//            MatchEvaluator evaluator = new MatchEvaluator(PrependCDNUrl);

//            html = Regex.Replace(html, regExPatternForImgTags, evaluator);
//        }

//        private string PrependCDNUrl(Match match)
//        {      
//            var src = match.Groups[0].Value;
//            //do nothing if its an absolute path
//            if (src.StartsWith("//") || src.StartsWith("http"))
//            {
//                return match.Value;
//            }
//            else
//            {
//                var rand = new Random();
//                var randomindex = rand.Next(0, AppConfigManager.CDNUrlCollection.Count - 1);
//                if (randomindex >= AppConfigManager.CDNUrlCollection.Count)
//                    randomindex = 0;
//                var cdnurl = HFC.CRM.Core.AppConfigManager.CDNUrlCollection[randomindex];
//                string val = match.Value;
//                val = val.Replace(string.Format("src=\"{0}\"", src), string.Format("src=\"{0}/{1}\"", cdnurl.TrimEnd('/'), src.TrimStart('/')));
//                return val;
//            }
//        }
//    }
//}
