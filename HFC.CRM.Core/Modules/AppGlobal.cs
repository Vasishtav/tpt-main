﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Membership;

/// <summary>
/// The Modules namespace.
/// </summary>
namespace HFC.CRM.Core.Modules
{
    /// <summary>
    /// Class AppGlobal.
    /// </summary>
    public class AppGlobal : IHttpModule
    {
        //protected void Application_Start(object sender, EventArgs e)
        //{

        //}

        //protected void Session_Start(object sender, EventArgs e)
        //{

        //}

        //protected void Application_BeginRequest(object sender, EventArgs e)
        //{
            
        //}

        //protected void Application_AuthenticateRequest(object sender, EventArgs e)
        //{
            
        //}
        
        //protected void Application_Error(object sender, EventArgs e)
        //{

        //}

        //protected void Session_End(object sender, EventArgs e)
        //{

        //}

        //protected void Application_End(object sender, EventArgs e)
        //{

        //}
        /// <summary>
        /// The acceptable headers
        /// </summary>
        private List<string> AcceptableHeaders = new List<string>() 
        {
            "application/json", 
            "text/javascript",
            "text/html",
            "application/xhtml+xml",
            "application/xml"
        };

        /// <summary>
        /// Disposes of the resources (other than memory) used by the module that implements <see cref="T:System.Web.IHttpModule" />.
        /// </summary>
        public void Dispose()
        {
            //throw new NotImplementedException();
        }

        /// <summary>
        /// Initializes a module and prepares it to handle requests.
        /// </summary>
        /// <param name="context">An <see cref="T:System.Web.HttpApplication" /> that provides access to the methods, properties, and events common to all application objects within an ASP.NET application</param>
        public void Init(HttpApplication context)
        {
            context.PostRequestHandlerExecute += context_PostRequestHandlerExecute;
        }

        //we handle updating user last activity here, after session and cache has been loaded
        /// <summary>
        /// Handles the PostRequestHandlerExecute event of the context control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        void context_PostRequestHandlerExecute(object sender, EventArgs e)
        {
            //var acceptList = HttpContext.Current.Request.Headers["Accept"].Split(',');
            //if (acceptList.Any(a => AcceptableHeaders.Contains(a.ToLower())) && !string.IsNullOrEmpty(HttpContext.Current.Request.CurrentExecutionFilePathExtension))
            //{
            //    if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated)
            //    {
            //        //only log if activity hasn't been updated in last 10 minute, so we dont update DB on every single request 
            //        if (DateTime.UtcNow.Subtract(SessionManager.UserLastActivityDateUtc).TotalSeconds > AppConfig.UserActivityTimeInterval)
            //        {
            //            SessionManager.UserLastActivityDateUtc = DateTime.UtcNow;
            //            MembershipProvider mem = new MembershipProvider();
            //            mem.LogActivityDate(HttpContext.Current.User.Identity.Name);
            //        }
            //    }
            //}
        }
        
    }
}
