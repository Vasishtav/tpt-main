﻿using System;
using System.Collections.Generic;
using System.Linq;
using HFC.CRM.Data;
using System.Web;
using System.Diagnostics;
using System.IO;
using RestSharp;
using Dapper;

/// <summary>
/// The Logs namespace.
/// </summary>
namespace HFC.CRM.Core.Logs
{
    using Dapper;
    using HFC.CRM.Data.Context;
    using Newtonsoft.Json;
    using System.Data.SqlClient;

    /// <summary>
    /// Enum LogSeverity
    /// </summary>
    public enum LogSeverity { Information, Warning, Error }

    /// <summary>
    /// Class EventLogger.
    /// </summary>
    public class EventLogger
    {
        /// <summary>
        /// Logs exception to database and returns a friendly error message for user
        /// </summary>
        /// <param name="ex">The ex.</param>
        /// <param name="username">The username.</param>
        /// <param name="franchiseId">The franchise identifier.</param>
        /// <returns>System.String.</returns>
        public static string LogEvent(Exception ex, string username = null, int franchiseId = 0, string owner = "", object obj = null)
        {
            System.Diagnostics.StackTrace stackTrace = new System.Diagnostics.StackTrace(ex);
            string msg = ex.Message;
            var inner = ex.InnerException;
            var res = LogEvent(msg, ex.Source, LogSeverity.Error, ex.StackTrace, ex.GetType().ToString(), username, franchiseId, owner, obj);
            while (inner != null)
            {
                //msg += string.Format(" \nInner Exception: {0}", inner.Message);
                //msg = inner.Message;
                if (inner.InnerException == null && inner.GetType() == typeof(Intuit.Ipp.Exception.ValidationException))
                {
                    var vEx = (Intuit.Ipp.Exception.ValidationException)inner;
                    msg = String.Join("\r\n",
                        vEx.InnerExceptions.Select(x => x.ErrorCode + " " + x.Element + " " + x.Message + " " + x.Detail));

                    //LogEvent(msg, "[InnerException]" + inner != null ? inner.Source : "", LogSeverity.Error, inner != null ? inner.StackTrace : null, inner != null ? inner.GetType().ToString() : "",
                    //username, franchiseId, owner, obj);
                    LogEvent(msg, "[InnerException]" + inner != null ? inner.Source : "", LogSeverity.Error, inner != null ? JsonConvert.SerializeObject(ex) : null, inner != null ? inner.GetType().ToString() : "",
                    username, franchiseId, owner, obj);
                }
                LogEvent(inner.Message, "[InnerException]" + inner.Source, LogSeverity.Error, inner.StackTrace, inner.GetType().ToString(), username, franchiseId, owner, obj);
                break;
            }
            return res;
        }

        /// <summary>
        /// Logs exception to database and returns a friendly error message for user
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="source">The source.</param>
        /// <param name="severity">The severity.</param>
        /// <param name="trace">The trace.</param>
        /// <param name="type">The type.</param>
        /// <param name="username">The username.</param>
        /// <param name="franchiseId">The franchise identifier.</param>
        /// <returns>System.String.</returns>
        public static string LogEvent(string message, string source = null, LogSeverity severity = LogSeverity.Information, string trace = null, string type = null, string username = null, int franchiseId = 0, string owner = "", object obj = null)
        {
            if (String.IsNullOrEmpty(source))
                source = "";
            if (string.IsNullOrEmpty(username)) username = HttpContext.Current != null && HttpContext.Current.User != null ? HttpContext.Current.User.Identity.Name : null;
            if (string.IsNullOrEmpty(type)) type = System.Reflection.Assembly.GetExecutingAssembly().FullName;

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var error = new Log_Error()
                {
                    OwnerId = owner,
                    Type = type,
                    Message = message,
                    Source = source,
                    Trace = trace,
                    UserName = username,
                    CreatedOnUtc = DateTime.UtcNow,
                    Severity = severity.ToString()
                };

                if (obj != null)
                {
                    try
                    {
                        error.Obj = JsonConvert.SerializeObject(obj);// JSON.Serialize(obj);
                    }
                    catch { }
                }
                connection.Insert<Log_Error>(error);
            }

            //using (var db = ContextFactory.Create())
            //{
            //    var error = new Log_Error()
            //    {
            //        OwnerId = owner,
            //        Type = type,
            //        Message = message,
            //        Source = source,
            //        Trace = trace,
            //        UserName = username,
            //        CreatedOnUtc = DateTime.Now,
            //        Severity = severity.ToString()
            //    };

            //    if (obj != null)
            //    {
            //        error.Obj = JSON.Serialize(obj);
            //    }

            //    db.Log_Error.Add(error);
            //    db.SaveChanges();
            //}

            // TODO: will this cause deployment issues???
            // https://stackoverflow.com/questions/29851891/angular-on-top-of-asp-net-mvc-displaying-errors
#if DEBUG
            return message;  //+ Environment.NewLine + "Trace: " +trace;
#endif

            return "An error occurred. If the problem persists, please contact your system administrator.";
        }

        public static void LogEvent(Exception ex, string source)
        {
            LogEvent(ex.Message, ex.Source + " | " + source, LogSeverity.Error, ex.StackTrace, ex.GetType().ToString());
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="request"></param>
        /// <param name="response"></param>
        /// <param name="elapsedMilliseconds"></param>
        /// <param name="apistr"></param>
        /// <param name="group"></param>
        public static void LogRequest(RestRequest request, IRestResponse response, long elapsedMilliseconds, string apistr, string group)
        {
            var query = @"INSERT INTO CRM.APITraceLog ([APITraceLogId] ,[API] ,[Group] ,[Type] ,[URL] ,[input] ,[Response] ,[CreatedOn] ,[ElapsedTime]) VALUES (@APITraceLogId ,@API ,@Group ,@Type ,@URL ,@input ,@Response ,@CreatedOn ,@ElapsedTime);";

            var _restClient = new RestClient(response.ResponseUri);
            var requestToLog = new
            {
                resource = request.Resource,
                // Parameters are custom anonymous objects in order to have the parameter type as a nice string
                // otherwise it will just show the enum value
                parameters = request.Parameters.Select(parameter => new
                {
                    name = parameter.Name,
                    value = parameter.Value,
                    type = parameter.Type.ToString()
                }),
                // ToString() here to have the method as a nice string otherwise it will just show the enum value
                method = request.Method.ToString(),
                // This will generate the actual Uri used in the request
                uri = _restClient.BuildUri(request),
            };

            var responseToLog = new
            {
                statusCode = response.StatusCode,
                content = response.Content,
                headers = response.Headers,
                // The Uri that actually responded (could be different from the requestUri if a redirection occurred)
                responseUri = response.ResponseUri,
                errorMessage = response.ErrorMessage,
            };

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var api = new APITraceLog();
                api.Type = request.Method.ToString();
                api.URL = response.ResponseUri.ToString();
                if (AppConfigManager.EnableAPILogging)
                {
                    api.input = JsonConvert.SerializeObject(requestToLog);
                    api.Response = JsonConvert.SerializeObject(responseToLog);
                }
                api.ElapsedTime = elapsedMilliseconds;
                api.APITraceLogId = Guid.NewGuid();
                api.API = apistr;
                api.Group = group;
                connection.Open();
                //connection.Insert(api);
                connection.Execute(query, new { APITraceLogId = api.APITraceLogId, API = api.API, Group = api.Group, Type = api.Type, URL = api.URL, input = api.input, Response = api.Response, CreatedOn = DateTime.UtcNow, ElapsedTime = api.ElapsedTime });
                connection.Close();
            }
        }

        /// <summary>
        /// Gets the logs.
        /// </summary>
        /// <param name="franchiseId">The franchise identifier.</param>
        /// <param name="totalRecord">The total record.</param>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <returns>List&lt;Log_Error&gt;.</returns>
        //public static List<Log_Error> GetLogs(int? franchiseId, out int totalRecord, int pageIndex = 0, int pageSize = 25)
        //{
        //    List<Log_Error> result = new List<Log_Error>();
        //    totalRecord = 0;
        //    var db = ContextFactory.Current.Context;
        //    if (franchiseId.HasValue)
        //    {
        //        totalRecord = db.Log_Error.Count(c => c.FranchiseId == franchiseId);
        //        result = db.Log_Error.Where(w => w.FranchiseId == franchiseId).OrderByDescending(o => o.CreatedOnUtc).Skip(pageIndex * pageSize).Take(pageSize).ToList();
        //    }
        //    else
        //    {
        //        totalRecord = db.Log_Error.Count();
        //        result = db.Log_Error.OrderByDescending(o => o.CreatedOnUtc).Skip(pageIndex * pageSize).Take(pageSize).ToList();
        //    }
        //    return result;
        //}

        /// <summary>
        /// Only use this if SQL connection isn't available, Not Tested yet
        /// </summary>
        /// <param name="ex">The ex.</param>
        /// <param name="message">The message.</param>
        /// <param name="severity">The severity.</param>
        /// <param name="source">The source.</param>
        /// <param name="trace">The trace.</param>
        /// <param name="type">The type.</param>
        public static void WriteToSystemEventLog(Exception ex, string message, LogSeverity severity, string source, string trace, string type)
        {
            string sSource = "HFC CRM";
            string sLog = "Application";

            try
            {
                if (!EventLog.SourceExists(sSource)) //needs account permission to check logs on computer
                    EventLog.CreateEventSource(sSource, sLog);

                var logType = EventLogEntryType.Information;
                switch (severity)
                {
                    case LogSeverity.Error: logType = EventLogEntryType.Error; break;
                    case LogSeverity.Warning: logType = EventLogEntryType.Warning; break;
                    default: break;
                }
                EventLog.WriteEntry(sSource, string.Format("Exception that triggered Event Logging: {4} \n\nSource: {0}\nType: {1}\nOriginal Message: {2}\nTrace: {3}",
                    source, type, message, trace, ex.Message), logType);
            }
            catch (Exception exp)
            {
                using (var sw = new StreamWriter(HttpContext.Current.Server.MapPath("/errorlog.log"), true))
                {
                    sw.AutoFlush = true;
                    sw.WriteLine(string.Format("{5} - {6}:\nException that triggered Event Logging: {4} \n\nSource: {0}\nType: {1}\nOriginal Message: {2}\nTrace: {3}",
                    source, type, message, trace, ex.Message, DateTime.Now, exp.Message));
                    sw.Flush();
                    sw.Close();
                }
            } //log to file?
        }

        public static void LogHistoryChanges_Audit(string TempData, int? LeadId = null, int? AccountId = null, int? OpportunityId = null,
            int? OrderId = null, int? PricingId = null, int? PurchaseOrderId = null, int? PurchaseOrdersDetailId = null, int? ProductId = null)
        {
            try
            {
                using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    string query = @"INSERT INTO [History].[AuditTrail] ([LeadId],[AccountId],[OpportunityId]
                                 ,[OrderId],[TempData],[PricingId],[PurchaseOrderId],[PurchaseOrdersDetailId],[ProductId])
                                 VALUES (@LeadId,@AccountId,@OpportunityId
                                 ,@OrderId,@TempData,@PricingId,@PurchaseOrderId,@PurchaseOrdersDetailId,@ProductId)";

                    con.Execute(query, new { LeadId, AccountId, OpportunityId, OrderId, TempData, PricingId, PurchaseOrderId, PurchaseOrdersDetailId, ProductId });
                }
            }
            catch (Exception ex)
            {
                LogEvent(ex);
            }
        }
    }

    /// <summary>
    /// Class ApiEventLogger.
    /// </summary>
    public class ApiEventLogger
    {
        /// <summary>
        /// Logs exception to database and returns a friendly error message for user
        /// </summary>
        /// <param name="ex">The ex.</param>
        /// <param name="username">The username.</param>
        /// <returns>System.String.</returns>
        public static string LogEvent(Exception ex, string username = null)
        {
            string msg = ex.Message;
            var inner = ex.InnerException;
            while (inner != null)
            {
                //msg += string.Format(" \nInner Exception: {0}", inner.Message);
                msg = inner.Message;
                inner = inner.InnerException;
            }
            return LogEvent(msg, ex.Source, LogSeverity.Error, ex.StackTrace, ex.GetType().ToString(), username);
        }

        /// <summary>
        /// Logs exception to database and returns a friendly error message for user
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="source">The source.</param>
        /// <param name="severity">The severity.</param>
        /// <param name="trace">The trace.</param>
        /// <param name="type">The type.</param>
        /// <param name="username">The username.</param>
        /// <returns>System.String.</returns>
        public static string LogEvent(string message, string source = null, LogSeverity severity = LogSeverity.Information, string trace = null, string type = null, string username = null)
        {
            if (string.IsNullOrEmpty(username)) username = HttpContext.Current != null && HttpContext.Current.User != null ? HttpContext.Current.User.Identity.Name : null;
            if (string.IsNullOrEmpty(type)) type = System.Reflection.Assembly.GetExecutingAssembly().FullName;
            var error = new ApiEventLog() { Type = type, Message = message, Source = source, Trace = trace, UserName = username, CreatedOnUtc = DateTime.Now, Severity = severity.ToString() };

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Insert<ApiEventLog>(error);
            }
            return "An error occurred. If the problem persists, please contact your system administrator.";
        }
    }
}