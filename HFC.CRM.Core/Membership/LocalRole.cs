﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using HFC.CRM.Core.Common;
using System.Data.Entity;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;

/// <summary>
/// The Membership namespace.
/// </summary>
namespace HFC.CRM.Core.Membership
{
    using HFC.CRM.Data.Context;

    /// <summary>
    /// Class LocalRole.
    /// </summary>
    public class LocalRole : System.Web.Security.RoleProvider
    {
        /// <summary>
        /// Updates a role.
        /// </summary>
        /// <param name="roleId">The role identifier.</param>
        /// <param name="name">The name.</param>
        /// <param name="description">The description.</param>
        /// <returns>System.String.</returns>
        public static string UpdateRole(Guid roleId, string name, string description)
        {
            string result = "Unable to update role";
            if (roleId != Guid.Empty)
            {
                if (!string.IsNullOrEmpty(name))
                {
                    try
                    {var role = new Role() { RoleId = roleId };
                        ContextFactory.Current.Context.Roles.Attach(role);
                        role.RoleName = name;
                        role.Description = description;
                        if (ContextFactory.Current.Context.SaveChanges() > 0)
                        {
                            result = "Success";
                            CacheManager.RoleCollection = null; //clear cache so it can be reloaded with new values
                        }
                    }
                    catch (Exception ex)
                    {
                        result = EventLogger.LogEvent(ex);
#if DEBUG
                        result = ex.Message;
#endif
                    }
                }
                else
                    result = "Role Name can not be empty";
            }
            else
                result = "Role Id can not be empty";

            return result;
        }

        /// <summary>
        /// Gets or sets the name of the application to store and retrieve role information for.
        /// </summary>
        /// <value>The name of the application.</value>
        /// <returns>The name of the application to store and retrieve role information for.</returns>
        public override string ApplicationName
        {
            get
            {
                return this.GetType().Assembly.GetName().Name.ToString();
            }
            set
            {
                this.ApplicationName = this.GetType().Assembly.GetName().Name.ToString();
            }
        }

        /// <summary>
        /// Adds a new role to the data source for the configured applicationName.
        /// </summary>
        /// <param name="roleName">The name of the role to create.</param>
        public override void CreateRole(string roleName)
        {
            CreateRole(roleName, 1, null);
        }

        /// <summary>
        /// Gets the roles.
        /// </summary>
        /// <param name="franchiseId">The franchise identifier.</param>
        /// <param name="includeUsers">if set to <c>true</c> [include users].</param>
        /// <returns>List&lt;Role&gt;.</returns>
        public static List<Role> GetRoles(int? franchiseId = null, bool includeUsers = false)
        {
            if (franchiseId.HasValue && franchiseId.Value <= 0) return null;
            if (includeUsers)
            {
                var roles = ContextFactory.Current.Context.Roles.Where(w => w.FranchiseId == franchiseId || w.IsInheritable).ToList();

                foreach (var role in roles)
                {
                    role.Users = ContextFactory.Current.Context.Entry(role).Collection(c => c.Users).Query().Where(w => w.FranchiseId == franchiseId).Include(i => i.Person).ToList();
                    role.Users = role.Users.OrderBy(o => o.Person.FullName).ToList();
                    role.User = role.Users.OrderBy(o => o.Person.FullName).ToList();
                }
                return roles.OrderBy(o => o.RoleName).ToList();
            }
            else
            {
                return (from r in ContextFactory.Current.Context.Roles where r.FranchiseId == franchiseId || r.IsInheritable select r).ToList().OrderBy(o => o.RoleName).ToList();
            }
        }

        /// <summary>
        /// Gets the role.
        /// </summary>
        /// <param name="roleId">The role identifier.</param>
        /// <returns>Role.</returns>
        public static Role GetRole(Guid roleId)
        {
            return GetRole(roleId, null);
        }

        /// <summary>
        /// Gets the role.
        /// </summary>
        /// <param name="rolename">The rolename.</param>
        /// <returns>Role.</returns>
        public static Role GetRole(string rolename)
        {
            return GetRole(null, rolename);
        }

        /// <summary>
        /// Gets the role.
        /// </summary>
        /// <param name="roleId">The role identifier.</param>
        /// <param name="roleName">Name of the role.</param>
        /// <returns>Role.</returns>
        private static Role GetRole(Guid? roleId = null, string roleName = null)
        {
            if (!roleId.HasValue && string.IsNullOrEmpty(roleName)) return null;
            if (roleId.HasValue) return ContextFactory.Current.Context.Roles.FirstOrDefault(f => f.RoleId == roleId);
            return ContextFactory.Current.Context.Roles.FirstOrDefault(f => f.RoleName == roleName);
        }

        /// <summary>
        /// Creates a new role and Key_FranchiseRole so we can map the role to specific franchise
        /// </summary>
        /// <param name="roleName">Name of the role.</param>
        /// <param name="franchiseId">The franchise identifier.</param>
        /// <param name="desc">The desc.</param>
        /// <exception cref="System.ArgumentNullException">Role name can not be empty or null</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">Invalid franchise id</exception>
        public void CreateRole(string roleName, int? franchiseId = null, string desc = null)
        {
            if (string.IsNullOrEmpty(roleName)) throw new ArgumentNullException("Role name can not be empty or null");
            if (franchiseId.HasValue && franchiseId.Value <= 0) throw new ArgumentOutOfRangeException("Invalid franchise id");
            var role = new Role { FranchiseId = franchiseId, RoleName = roleName, Description = desc };
            ContextFactory.Current.Context.Roles.Add(role);

            if (ContextFactory.Current.Context.SaveChanges() > 0 && CacheManager.RoleCollection.Exists(e => e.FranchiseId == franchiseId && e.RoleId == role.RoleId)) CacheManager.RoleCollection.Add(role);
        }

        /// <summary>
        /// Note: Admin and User roles are system defaults and cannot not be deleted
        /// </summary>
        /// <param name="roleName">Name of the role.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public bool DeleteRole(string roleName)
        {
            return DeleteRole(roleName, null);
        }

        /// <summary>
        /// Note: Admin and User roles are system defaults and cannot not be deleted
        /// </summary>
        /// <param name="roleId">The role identifier.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public bool DeleteRole(Guid roleId)
        {
            return DeleteRole(null, roleId);
        }

        /// <summary>
        /// Deletes the role.
        /// </summary>
        /// <param name="roleName">Name of the role.</param>
        /// <param name="roleId">The role identifier.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        private bool DeleteRole(string roleName = null, Guid? roleId = null)
        {
            if (string.IsNullOrEmpty(roleName) && !roleId.HasValue) return false;
            Role role = null;
            if (roleId.HasValue) role = ContextFactory.Current.Context.Roles.FirstOrDefault(f => f.IsDeletable == true && f.RoleId == roleId.Value);
            else if (!string.IsNullOrEmpty(roleName)) role = ContextFactory.Current.Context.Roles.FirstOrDefault(f => f.IsDeletable == true && f.RoleName == roleName);

            if (role != null)
            {
                ContextFactory.Current.Context.Roles.Remove(role);
                if (ContextFactory.Current.Context.SaveChanges() > 0)
                {
                    CacheManager.RoleCollection.RemoveAll(r => r.RoleId == role.RoleId);
                    return true;
                }
                return false;
            }
            
            return false;
        }

        /// <summary>
        /// Gets an array of user names in a role where the user name contains the specified user name to match.
        /// </summary>
        /// <param name="roleName">The role to search in.</param>
        /// <param name="usernameToMatch">The user name to search for.</param>
        /// <returns>A string array containing the names of all the users where the user name matches <paramref name="usernameToMatch" /> and the user is a member of the specified role.</returns>
        public override string[] FindUsersInRole(string roleName, string usernameToMatch = null)
        {
            string[] result = null;

            if (!string.IsNullOrEmpty(roleName))
            {
                if (string.IsNullOrEmpty(usernameToMatch))
                {
                    result = (from r in ContextFactory.Current.Context.Roles from u in r.Users where string.Compare(r.RoleName, roleName, true) == 0 select u.UserName).ToArray();
                }
                else
                {
                    if ((from r in ContextFactory.Current.Context.Roles from u in r.Users where string.Compare(r.RoleName, roleName, true) == 0 && string.Compare(u.UserName, usernameToMatch, true) == 0 select u.UserName).Any()) result = new string[] { usernameToMatch };
                }
            }

            return result;
        }

        /// <summary>
        /// Gets a list of all the roles for the configured applicationName.
        /// </summary>
        /// <returns>A string array containing the names of all the roles stored in the data source for the configured applicationName.</returns>
        public override string[] GetAllRoles()
        {
            return ContextFactory.Current.Context.Roles.Select(s => s.RoleName).ToArray();
        }

        /// <summary>
        /// Gets a list of the roles that a specified user is in for the configured applicationName.
        /// </summary>
        /// <param name="username">The user to return a list of roles for.</param>
        /// <returns>A string array containing the names of all the roles that the specified user is in for the configured applicationName.</returns>
        public override string[] GetRolesForUser(string username)
        {
            if (!string.IsNullOrEmpty(username))
            {
                return (from r in ContextFactory.Current.Context.Roles from u in r.Users where string.Compare(u.UserName, username, true) == 0 select r.RoleName).ToArray();
            }
            else
                return null;
        }

        /// <summary>
        /// Same as FindUsersInRole
        /// </summary>
        /// <param name="roleName">The name of the role to get the list of users for.</param>
        /// <returns>A string array containing the names of all the users who are members of the specified role for the configured applicationName.</returns>
        public override string[] GetUsersInRole(string roleName)
        {
            return FindUsersInRole(roleName, null);
        }

        /// <summary>
        /// Gets the users in role.
        /// </summary>
        /// <param name="roleId">The role identifier.</param>
        /// <returns>List&lt;System.String&gt;.</returns>
        public static List<string> GetUsersInRole(Guid roleId)
        {
            var result = (from r in ContextFactory.Current.Context.Roles from u in r.Users where r.RoleId == roleId select u.UserName).ToList();
            return result;
        }

        /// <summary>
        /// Gets a value indicating whether the specified user is in the specified role for the configured applicationName.
        /// </summary>
        /// <param name="username">The user name to search for.</param>
        /// <param name="roleName">The role to search in.</param>
        /// <returns>true if the specified user is in the specified role for the configured applicationName; otherwise, false.</returns>
        public override bool IsUserInRole(string username, string roleName)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(roleName))
                return false;

            var user = FindUsersInRole(roleName, username);
            return user != null && user.Length > 0;
        }

        /// <summary>
        /// Removes the users from roles.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="roleIds">The role ids.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public static bool RemoveUsersFromRoles(Guid userId, List<Guid> roleIds)
        {
            return RemoveUsersFromRoles(new List<Guid>() { userId }, roleIds);
        }
        /// <summary>
        /// Removes the users from roles.
        /// </summary>
        /// <param name="userIds">The user ids.</param>
        /// <param name="roleId">The role identifier.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public static bool RemoveUsersFromRoles(List<Guid> userIds, Guid roleId)
        {
            return RemoveUsersFromRoles(userIds, new List<Guid>() { roleId });
        }
        /// <summary>
        /// Removes the users from roles.
        /// </summary>
        /// <param name="userIds">The user ids.</param>
        /// <param name="roleIds">The role ids.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public static bool RemoveUsersFromRoles(List<Guid> userIds, List<Guid> roleIds)
        {
            bool result = false;

            if (userIds.Count > 0 && roleIds.Count > 0)
            {
                try
                {
                    int count = 0;
                    foreach (var uid in userIds)
                    {
                        foreach (var rid in roleIds)
                        {
                            //Do not delete User role, this is a system default role
                            //Also do not delete admin with admin role
                            if (rid == AppConfigManager.DefaultRole.RoleId || (uid == AppConfigManager.ApplicationAdmin.UserId && rid == AppConfigManager.AppAdminRole.RoleId)) result = false;
                            else count += ContextFactory.Current.Context.Database.ExecuteSqlCommand(@"DELETE Auth.UsersInRoles WHERE UserId=@p0 AND RoleId=@p1", uid, rid);
                        }
                    }

                    if (count > 0) result = true;
                }
                catch (Exception ex) { EventLogger.LogEvent(ex); }
            }
            return result;
        }

        /// <summary>
        /// Adds the users to roles.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="roleIds">The role ids.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public static bool AddUsersToRoles(Guid userId, List<Guid> roleIds)
        {
            return AddUsersToRoles(new List<Guid>() { userId }, roleIds);
        }
        /// <summary>
        /// Adds the users to roles.
        /// </summary>
        /// <param name="userIds">The user ids.</param>
        /// <param name="roleId">The role identifier.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public static bool AddUsersToRoles(List<Guid> userIds, Guid roleId)
        {
            return AddUsersToRoles(userIds, new List<Guid>() { roleId });
        }
        /// <summary>
        /// Adds the users to roles.
        /// </summary>
        /// <param name="userIds">The user ids.</param>
        /// <param name="roleIds">The role ids.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public static bool AddUsersToRoles(List<Guid> userIds, List<Guid> roleIds)
        {
            bool result = false;

            if (userIds != null && userIds.Count > 0 && roleIds != null && roleIds.Count > 0)
            {
                try
                {
                    int count = 0;
                    foreach (var uid in userIds)
                    {
                        foreach (var rid in roleIds)
                        {
                            //dont need to add User role, this is automatically created for user already
                            if (rid != AppConfigManager.DefaultRole.RoleId)
                                count +=
                                    ContextFactory.Current.Context.Database.ExecuteSqlCommand(
                                        @"IF NOT EXISTS(SELECT UserId FROM Auth.UsersInRoles WHERE UserId = @p0 AND RoleId = @p1) INSERT INTO Auth.UsersInRoles(UserId,RoleId) VALUES(@p0, @p1)",
                                        uid,
                                        rid);
                        }
                    }

                    if (count > 0) result = true;
                }
                catch (Exception ex) { EventLogger.LogEvent(ex); }
            }
            return result;
        }

        /// <summary>
        /// Gets a value indicating whether the specified role name already exists in the role data source for the configured applicationName.
        /// </summary>
        /// <param name="roleName">The name of the role to search for in the data source.</param>
        /// <returns>true if the role name already exists in the data source for the configured applicationName; otherwise, false.</returns>
        public override bool RoleExists(string roleName)
        {
            if (string.IsNullOrEmpty(roleName)) return false;
            return ContextFactory.Current.Context.Roles.Any(a => a.RoleName == roleName);
        }

        #region Not Supported Methods

        /// <summary>
        /// NOT IMPLEMENTED, CONSIDER USING OTHER OVERLOADED METHOD
        /// </summary>
        /// <param name="usernames">A string array of user names to be removed from the specified roles.</param>
        /// <param name="roleNames">A string array of role names to remove the specified user names from.</param>
        /// <exception cref="System.NotSupportedException">Consider using the other overloaded method</exception>
        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotSupportedException("Consider using the other overloaded method");
            //if (usernames.Length > 0 && roleNames.Length > 0)
            //{
            //    using (var db = new AuthContext())
            //    {
            //        foreach (var name in usernames)
            //        {
            //            XElement elem = new XElement("root", roleNames.Select(s => new XElement("role", s)));
            //            db.spUserInRoles_Delete(null, null, name, elem.ToString(SaveOptions.DisableFormatting));
            //        }
            //    }
            //}
        }

        /// <summary>
        /// NOT IMPLEMENTED, CONSIDER USING OTHER OVERLOADED METHOD
        /// </summary>
        /// <param name="usernames">A string array of user names to be added to the specified roles.</param>
        /// <param name="roleNames">A string array of the role names to add the specified user names to.</param>
        /// <exception cref="System.NotSupportedException">Consider using the other overloaded method</exception>
        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotSupportedException("Consider using the other overloaded method");
            //if (usernames.Length > 0 && roleNames.Length > 0)
            //{
            //    using (var db = new AuthContext())
            //    {
            //        foreach (var name in usernames)
            //        {
            //            XElement elem = new XElement("root", roleNames.Select(s => new XElement("role", s)));
            //            db.spUserInRoles_Insert(null, name, elem.ToString(SaveOptions.DisableFormatting));
            //        }
            //    }
            //}
        }

        /// <summary>
        /// NOT IMPLEMENTED, USE OVERLOADED METHOD INSTEAD
        /// </summary>
        /// <param name="roleName">The name of the role to delete.</param>
        /// <param name="throwOnPopulatedRole">If true, throw an exception if <paramref name="roleName" /> has one or more members and do not delete <paramref name="roleName" />.</param>
        /// <returns>true if the role was successfully deleted; otherwise, false.</returns>
        /// <exception cref="System.NotSupportedException">Consider using other DeleteRole overloaded method</exception>
        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotSupportedException("Consider using other DeleteRole overloaded method");
        }
        
        #endregion
    }
}
