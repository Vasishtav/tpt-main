﻿using System;
using System.Collections.Generic;
using System.Linq;

using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using System.Data.Entity;
using HFC.CRM.Data;

/// <summary>
/// The Membership namespace.
/// </summary>
namespace HFC.CRM.Core.Membership
{
    using Dapper;
    using HFC.CRM.Data.Context;
    using System.Data.SqlClient;

    /// <summary>
    /// Class PermissionProvider.
    /// </summary>
    public class PermissionProviderTP
    {
        

        /// <summary>
        /// Gets a special permission, these are not based on any roles but can be for a user
        /// </summary>
        /// <param name="userToCheck">The user to check.</param>
        /// <param name="name">The name.</param>
        /// <param name="subsection">The subsection.</param>
        /// <returns>Permission.</returns>
        [Obsolete]
        public static Permission GetSpecialPermission(User userToCheck, string name, string subsection = null)
        {
            var permission = new Permission();

            if (userToCheck == null)
                return permission;

            Type_Permission permType = null;
            if(!string.IsNullOrEmpty(subsection))
                permType = CacheManager.PermissionTypeCollection
                    .FirstOrDefault(f => string.Compare(f.Name, name, true) == 0 && string.Compare(f.Subsection, subsection, true) == 0);
            else
                permType = CacheManager.PermissionTypeCollection
                    .FirstOrDefault(f => string.Compare(f.Name, name, true) == 0 && string.IsNullOrEmpty(f.Subsection));

            if (permType != null)
            {
                var linq = CacheManager.PermissionCollection.Where(p => p.PermissionTypeId == permType.PermissionTypeId);

                permission = linq.FirstOrDefault(p => p.PersonId == userToCheck.PersonId);
                //get permission for specific person if it doesnt exist then get franchise specific or base permission
                if (permission == null) 
                {
                    //order by descending so if there is a franchise custom permission it gets used first
                    permission = linq.Where(f => f.PersonId == null).OrderByDescending(o => o.FranchiseId).FirstOrDefault() ?? new Permission();

                    // check again if there is any permission record at all for this special permission
                    // if not then create a new permission object, all CRUD will default to false instead of passing back null and possibly crash
                }
            }  

            return permission;
        }

        /// <summary>
        /// Gets the permission.
        /// </summary>
        /// <param name="userToCheck">The user to check.</param>
        /// <param name="name">The name.</param>
        /// <param name="subsection">The subsection.</param>
        /// <returns>Permission.</returns>
        [Obsolete]
        public static Permission GetPermission(User userToCheck, string name, string subsection = null)
        {
            
            //select both the base and franchise specific permissions
            List<int> permTypes = null;
            if (string.IsNullOrEmpty(subsection))
            {
                permTypes = CacheManager.PermissionTypeCollection
                        .Where(f => string.Compare(f.Name, name, true) == 0 && string.IsNullOrEmpty(f.Subsection))
                        .Select(s => s.PermissionTypeId).ToList();
            }
            else //add subsection if any
            {
                permTypes = CacheManager.PermissionTypeCollection
                    .Where(f => string.Compare(f.Name, name, true) == 0 && string.Compare(f.Subsection, subsection, true) == 0) //or specific permission with a subsection
                    .Select(s => s.PermissionTypeId).ToList();
            }                
            
            return GetPermission(userToCheck, permTypes);
        }

        public static Permission GetPermissionTP(User userToCheck, string permissionName)
        {
            //select both the base and franchise specific permissions
            List<int> permTypes = null;
            
            // Though we are calling to list, most of the time the result 
            // will be only one records for each permission.
            permTypes = CacheManager.PermissionTypeCollection
                    .Where(f => string.Compare(f.Name, permissionName, true) == 0)
                    .Select(s => s.PermissionTypeId).ToList();

            return GetPermissionTP(userToCheck, permTypes);
        }


        public static IEnumerable<PermissionTP> GetUserPermissions(User user)
        {
            var result = new List<PermissionTP>();

            foreach (var permission in CacheManager.PermissionTypeCollection)
            {
                var p = GetPermissionTP(user, permission.Name);//, permission.Subsection);
                if (p != null)
                {
                    result.Add(new PermissionTP
                    {
                        Name = permission.Name,
                        CanAccess = p.CanAccess,
                        GroupName = permission.GroupName,
                        PermissionId = p.PermissionId,
                        PermissionTypeId = p.PermissionTypeId
                    });
                    //result.Add(new 
                    //           {
                    //               p.CanAccess,
                    //               p.CanCreate,
                    //               p.CanDelete,
                    //               p.CanUpdate,
                    //               p.CanRead,
                    //               permission.Name,
                    //               permission.Subsection,
                    //               permission.GroupName
                    //           });
                }
            }

            return result;
        }

        ///// <summary>
        ///// Gets highest permission from permission type
        ///// </summary>
        ///// <param name="userToCheck">Will use SessionManager.CurrentUser if not supplied</param>
        ///// <returns></returns>
        //public static Permission GetPermission(int permissionTypeId, User userToCheck = null)
        //{
        //    return GetPermission(new List<int>() { permissionTypeId }, userToCheck);
        //}

        /// <summary>
        /// Gets highest permission from permission type
        /// </summary>
        /// <param name="userToCheck">The user to check.</param>
        /// <param name="permissionTypeIds">The permission type ids.</param>
        /// <returns>Permission.</returns>
        [Obsolete]
        private static Permission GetPermission(User userToCheck, List<int> permissionTypeIds)
        {
            var result = new Permission();
            if (userToCheck == null)
                return result;
            var owners = new List<Guid?>();
            if (SessionManager.CurrentFranchise != null && SessionManager.CurrentFranchise.FranchiseOwners != null)
            {
                owners = SessionManager.CurrentFranchise.FranchiseOwners.Select(v => v.UserId).ToList();
            } 

            //we always return true for all CRUD for admin, system default so we don't let anyone change this by database
            if (userToCheck.IsInRole(AppConfigManager.AppAdminRole.RoleId) || owners.Contains(userToCheck.UserId))
            {
                result.CanCreate = true;
                result.CanRead = true;
                result.CanUpdate = true;
                result.CanDelete = true;
            }
            else if (permissionTypeIds != null && permissionTypeIds.Count > 0)  
            {
                try
                {
                    var rolePerm = (from p in CacheManager.PermissionCollection
                                    where permissionTypeIds.Contains(p.PermissionTypeId) &&  //get permission for the requested permission type                                         
                                        (!p.FranchiseId.HasValue || p.FranchiseId == userToCheck.FranchiseId) && //select both base and custom permission
                                        (!p.RoleId.HasValue || (p.RoleId.HasValue && userToCheck.Roles.Select(s => s.RoleId).Contains(p.RoleId.Value))) //select permission for the requested role of user
                                select p).ToList();

                    var userPerm = CacheManager.PermissionCollection.FirstOrDefault(p => permissionTypeIds.Contains(p.PermissionTypeId) && p.PersonId == userToCheck.PersonId);

                    //if there is a user specific permission, this takes precedence over role permission
                    if (userPerm != null)
                    {
                        result = userPerm;
                    }
                    else if (rolePerm != null && rolePerm.Count > 0) //run through all role permission and retrieve the highest CRUD permission                    
                    {
                        //we will want to split into franchise custom vs base permission
                        //if they don't have a franchise custom permission then use base permission
                        var FranPerm = rolePerm.Where(w => w.FranchiseId.HasValue).ToList();
                        var BasePerm = rolePerm.Where(w => !w.FranchiseId.HasValue).ToList();

                        // we use Max to group similar parent permissions, ie:                        
                        // if we want to see if user has permission to view Settings we grab all "Settings" permission, this will return
                        // Settings > Users
                        // Settings > Roles
                        // Settings > Permissions //etc..
                        // so now we have 3 permission types and at least one must grant read access to allow read access to Settings menu
                        // example usage in /Shared/_Layout.cshtml
                        // @if (PermissionProvider.GetPermission("Settings").CanRead) 
                        result.CanCreate = FranPerm != null && FranPerm.Count > 0 ? FranPerm.Max(m => m.CanCreate) : BasePerm.Max(m => m.CanCreate);
                        result.CanRead = FranPerm != null && FranPerm.Count > 0 ? FranPerm.Max(m => m.CanRead) : BasePerm.Max(m => m.CanRead);
                        result.CanUpdate = FranPerm != null && FranPerm.Count > 0 ? FranPerm.Max(m => m.CanUpdate) : BasePerm.Max(m => m.CanUpdate);
                        result.CanDelete = FranPerm != null && FranPerm.Count > 0 ? FranPerm.Max(m => m.CanDelete) : BasePerm.Max(m => m.CanDelete);
                    }
                    
                }
                catch (Exception ex) { EventLogger.LogEvent(ex); }
            }

            return result;
        }

        private static Permission GetPermissionTP(User userToCheck, List<int> permissionTypeIds)
        {
            var result = new Permission();
            if (userToCheck == null)
                return result;

            var owners = new List<Guid?>();
            if (SessionManager.CurrentFranchise != null && SessionManager.CurrentFranchise.FranchiseOwners != null)
            {
                owners = SessionManager.CurrentFranchise.FranchiseOwners.Select(v => v.UserId).ToList();
            }

            //we always return true for all CRUD for admin, system default so we don't let anyone change this by database
            if (userToCheck.IsInRole(AppConfigManager.AppAdminRole.RoleId) 
                || owners.Contains(userToCheck.UserId))
            {
                result.CanAccess = true;
                return result;
            }

            if (permissionTypeIds == null || permissionTypeIds.Count <= 0)
            {
                return result;
            }
           
            try
            {
                var rolePerm = (from p in CacheManager.PermissionCollection
                                where permissionTypeIds.Contains(p.PermissionTypeId) &&  //get permission for the requested permission type                                         
                                    (!p.FranchiseId.HasValue 
                                        || p.FranchiseId == userToCheck.FranchiseId) && //select both base and custom permission
                                    (!p.RoleId.HasValue || (p.RoleId.HasValue 
                                        && userToCheck.Roles.Select(s => s.RoleId)
                                        .Contains(p.RoleId.Value))) //select permission for the requested role of user
                                select p).ToList();

                //var userPerm = CacheManager.PermissionCollection.FirstOrDefault(p => 
                //    permissionTypeIds.Contains(p.PermissionTypeId) && 
                //    p.PersonId == userToCheck.PersonId);

                ////if there is a user specific permission, this takes precedence over role permission
                //if (userPerm != null)
                //{
                //    result = userPerm;
                //}
                //else 

                var mm = CacheManager.PermissionCollection.Where(x => x.CanAccess== true).ToList();

                if (rolePerm == null || rolePerm.Count < 1) return result;

                    //we will want to split into franchise custom vs base permission
                    //if they don't have a franchise custom permission then use base permission
                    var FranPerm = rolePerm.Where(w => w.FranchiseId.HasValue).ToList();
                    var BasePerm = rolePerm.Where(w => !w.FranchiseId.HasValue).ToList();

                // we use Max to group similar parent permissions, ie:                        
                // if we want to see if user has permission to view Settings we grab all "Settings" permission, this will return
                // Settings > Users
                // Settings > Roles
                // Settings > Permissions //etc..
                // so now we have 3 permission types and at least one must grant read access to allow read access to Settings menu
                // example usage in /Shared/_Layout.cshtml
                // @if (PermissionProvider.GetPermission("Settings").CanRead) 
                if (BasePerm != null && BasePerm.Count > 0)
                {
                    result.CanAccess = BasePerm.Max(m => m.CanAccess);
                }

                //result.CanCreate = FranPerm != null && FranPerm.Count > 0 ? FranPerm.Max(m => m.CanCreate) : BasePerm.Max(m => m.CanCreate);
                //result.CanRead = FranPerm != null && FranPerm.Count > 0 ? FranPerm.Max(m => m.CanRead) : BasePerm.Max(m => m.CanRead);
                //result.CanUpdate = FranPerm != null && FranPerm.Count > 0 ? FranPerm.Max(m => m.CanUpdate) : BasePerm.Max(m => m.CanUpdate);
                //result.CanDelete = FranPerm != null && FranPerm.Count > 0 ? FranPerm.Max(m => m.CanDelete) : BasePerm.Max(m => m.CanDelete);
            }
            catch (Exception ex) { EventLogger.LogEvent(ex); }

            return result;
        }


        /// <summary>
        /// Gets the permissions.
        /// </summary>
        /// <param name="franchiseId">if null then it will return base permissions</param>
        /// <param name="includes">The includes.</param>
        /// <returns>List&lt;Permission&gt;.</returns>
        public static List<Permission> GetPermissions(int? franchiseId, params string[] includes)
        {
            var result = new List<Permission>();
            
            try
            {
                var linq = ContextFactory.Current.Context.Permissions.Where(w => !w.FranchiseId.HasValue);
                if (franchiseId.HasValue && franchiseId.Value > 0) linq = ContextFactory.Current.Context.Permissions.Where(w => !w.FranchiseId.HasValue || w.FranchiseId == franchiseId);
                linq = includes.Aggregate(linq, (current, inc) => current.Include(inc));

                result = linq.ToList();
            }
            catch (Exception ex) { EventLogger.LogEvent(ex); }            

            return result;
        }

        /// <summary>
        /// Updates the permission.
        /// </summary>
        /// <param name="permissionId">The permission identifier.</param>
        /// <param name="canCreate">if set to <c>true</c> [can create].</param>
        /// <param name="canRead">if set to <c>true</c> [can read].</param>
        /// <param name="canUpdate">if set to <c>true</c> [can update].</param>
        /// <param name="canDelete">if set to <c>true</c> [can delete].</param>
        /// <returns>System.String.</returns>
        public static string UpdatePermission(int permissionId, bool canCreate, bool canRead, bool canUpdate, bool canDelete)
        {
            string result = "Unable to update permission";

            //if (franchiseId.HasValue && franchiseId <= 0)
            //    return "Invalid franchise";

            if (permissionId <= 0)
                return "Invalid permission id";
            
            try
            {
                string query = @"Update auth.Permissions set CanCreate=@cancreate, 
                        CanRead=@canread, CanUpdate=@canupdate CanDelete=@candelete
                        where Persmissionid=@id";

                using (SqlConnection connectionUpdate = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connectionUpdate.Execute(query, new {
                        cancreate = canCreate,
                        canread = canRead,
                        canupdate = canUpdate,
                        candelete = canDelete,
                        id = permissionId });
                }

                result = "Success";
            }
            catch (Exception ex)
            {
                result = EventLogger.LogEvent(ex);
            }
            
            return result;
        }
    }
}
