﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Xml.Linq;
using HFC.CRM.Data;

/// <summary>
/// The Membership namespace.
/// </summary>
namespace HFC.CRM.Core.Membership
{
    using Dapper;
    using HFC.CRM.Data.Context;
    using System.Data.SqlClient;

    /// <summary>
    /// Class LocalMembership.
    /// </summary>
    public partial class LocalMembership
    {
        /// <summary>
        /// Gets the franchise identifier.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <returns>System.Nullable&lt;System.Int32&gt;.</returns>
        public static int? GetFranchiseId(string username)
        {
            int? result = null;
            if (!string.IsNullOrEmpty(username))
            {

                result = CacheManager.UserCollection.Where(f => f.IsDeleted == false && f.UserName == username).Select(s => s.FranchiseId).SingleOrDefault();
            }

            return result;
        }

        /// <summary>
        /// Gets the franchise identifier.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns>System.Nullable&lt;System.Int32&gt;.</returns>
        public static int? GetFranchiseId(Guid userId)
        {
            int? result = null;
            if (userId != Guid.Empty)
            {
                result = CacheManager.UserCollection.Where(f => f.IsDeleted == false && f.UserId == userId).Select(s => s.FranchiseId).SingleOrDefault();
            }

            return result;
        }

        /// <summary>
        /// Gets user data
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="includes">The includes.</param>
        /// <returns>User.</returns>
        public static User GetUser(string username, params string[] includes)
        {
            return _GetUser(username: username, includes: includes);
        }

        /// <summary>
        /// Gets the user.
        /// </summary>
        /// <param name="personId">The person identifier.</param>
        /// <param name="includes">The includes.</param>
        /// <returns>User.</returns>
        public static User GetUser(int personId, params string[] includes)
        {
            return _GetUser(personId: personId, includes: includes);
        }

        /// <summary>
        /// Gets the user.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="includes">The includes.</param>
        /// <returns>User.</returns>
        public static User GetUser(Guid userId, params string[] includes)
        {
            return _GetUser(userId: userId, includes: includes);
        }

        /// <summary>
        /// _s the get user.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="personId">The person identifier.</param>
        /// <param name="username">The username.</param>
        /// <param name="includes">The includes.</param>
        /// <returns>User.</returns>
        private static User _GetUser(Guid? userId = null, int? personId = null, string username = null, params string[] includes)
        {
            var result = new User();
            if ((personId.HasValue && personId > 0) || !string.IsNullOrEmpty(username) || (userId.HasValue && userId.Value != Guid.Empty))
            {
                using (var db = ContextFactory.Create())
                {
                    var linq =
                        db.Users.Include(i => i.Roles)
                            .Include(i => i.UserWidgets.Select(s => s.Type_Widgets))
                            .Where(f => f.IsDeleted == false);
                    if (userId.HasValue) linq = linq.Where(w => w.UserId == userId.Value);
                    else if (personId.HasValue) linq = linq.Where(f => f.PersonId == personId.Value);
                    else if (!string.IsNullOrEmpty(username)) linq = linq.Where(f => f.UserName == username);

                    linq = includes.Aggregate(linq, (current, inc) => current.Include(inc));
                    result = linq.FirstOrDefault() ?? new User();
                    if (result.AddressId == null && result.Address == null && result.FranchiseId != null)
                    {
                        var id = Convert.ToInt32(result.FranchiseId);
                        var address = (from a in db.Addresses
                                       join f in db.Franchises on a.AddressId equals f.AddressId
                                       where f.FranchiseId == id
                                       select a).FirstOrDefault();
                        result.AddressId = address.AddressId;
                        result.Address = address;
                    }

                    using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                    {
                        var query = "";
                        if (result.AddressId != null)
                        {
                            query = @"Select * from [CRM].[Addresses] WHERE AddressId =@AddressId ";
                            var value = connection.Query<Address>(query, new { AddressId = result.AddressId }).FirstOrDefault();
                            if (result.Address != null && value != null)
                            {
                                if (result.Address.AddressId == value.AddressId)
                                    result.Address.IsValidated = value.IsValidated;
                            }
                        }

                        query = @"Select PermissionSetId from [Auth].[UserInPermission] WHERE UserId =@userId ";
                        var permissionRole = connection.Query<int>(query, new { userId = result.UserId }).ToList();
                        if (permissionRole != null)
                            result.PermissionSets = permissionRole;

                        // Entity Framework not includes this data, so we are adding manually here
                        query = @"select TimezoneCode,EnableEmailSignature,AddEmailSignAllEmails from CRM.Person where PersonId=@PersonId";
                        var person = connection.Query<Person>(query, new { result.PersonId }).FirstOrDefault();
                        if (result.Person != null && person != null)
                        {
                            result.Person.TimezoneCode = person.TimezoneCode != null && person.TimezoneCode != 0 ? (TimeZoneEnum)person.TimezoneCode : TimeZoneEnum.UTC;
                            result.Person.EnableEmailSignature = person.EnableEmailSignature;
                            result.Person.AddEmailSignAllEmails = person.AddEmailSignAllEmails;
                        }
                    }

                }
            }

            //var rolenames = string.Join(", ", result.Roles.Select(x => x.RoleName).ToArray());
            //EventLogger.LogEvent("List of roles assigned to user", "LocalMembership"
            //    , LogSeverity.Information, rolenames, null, result.UserName, SessionManager.CurrentFranchise.FranchiseId);

            return result;
        }

        /// <summary>
        /// Gets list of users without paging
        /// </summary>
        /// <param name="franchiseId">If null, then this method will return all users</param>
        /// <param name="includes">The includes.</param>
        /// <returns>List&lt;User&gt;.</returns>
        public static List<User> GetUsers(int? franchiseId = null, params string[] includes)
        {
            int totalRec = 0;
            return GetUsers(out totalRec, franchiseId, null, 20, includes);
        }

        /// <summary>
        /// Gets the users.
        /// </summary>
        /// <param name="totalRecords">The total records.</param>
        /// <param name="franchiseId">The franchise identifier.</param>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="includes">The includes.</param>
        /// <returns>List&lt;User&gt;.</returns>
        private static List<User> GetUsers(out int totalRecords, int? franchiseId, int? pageIndex, int pageSize, params string[] includes)
        {
            totalRecords = 0;
            List<User> result = null;

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var linq = db.Users.Where(w => w.IsDeleted == false);
                    var linqCount = db.Users.Where(w => w.IsDeleted == false);
                    if (franchiseId.HasValue)
                    {
                        linq = linq.Where(w => w.FranchiseId == franchiseId.Value);
                        linqCount = linqCount.Where(w => w.FranchiseId == franchiseId.Value);
                    }

                    //only do paging if pageIndex is not null
                    if (pageIndex.HasValue)
                    {
                        totalRecords = linqCount.Count();
                        linq = linq.OrderBy(o => o.UserName).Skip(pageIndex.Value * pageSize).Take(pageSize);
                    }
                    linq = includes.Aggregate(linq, (current, inc) => current.Include(inc));

                    result = linq.ToList();
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
            }

            return result;
        }

        /// <summary>
        /// Confirm user has an account and bind to OAuth if there isn't one already
        /// </summary>
        /// <param name="email">Confirms account by the user's unique email address</param>
        /// <param name="provider">The provider.</param>
        /// <param name="providerUserId">The provider user identifier.</param>
        /// <param name="accessToken">The access token.</param>
        /// <param name="expiresIn">Unit in Seconds</param>
        /// <param name="refreshToken">The refresh token.</param>
        /// <returns>Returns the confirmed user's username</returns>
        public static string ConfirmAndOrBindAccount(string email, string provider, string providerUserId, string accessToken, short expiresIn, string refreshToken)
        {
            string result = null;

            if (!string.IsNullOrEmpty(email))
            {
                try
                {
                    var db = ContextFactory.Current.Context;
                    var acct = (from u in db.Users
                                    //join p in db.UserPersons on u.UserId equals p.PersonGuid
                                from oa in u.OAuthUsers.DefaultIfEmpty()
                                where u.IsDeleted == false && u.Email == email
                                select new { u.UserId, u.UserName, u.IsLockedOut, u.IsApproved, u.OAuthUsers, u.IsDisabled }).SingleOrDefault();
                    //user has an account and approved that is not locked
                    if (acct != null && !acct.IsLockedOut && acct.IsApproved && acct.IsDisabled != true)
                    {
                        bool isBound = true, isActive = true;
                        //check if account is bound to the OAuth provider
                        if (acct.OAuthUsers != null)
                        {
                            var selectedProv = acct.OAuthUsers.SingleOrDefault(f => f.Provider == provider && f.ProviderUserId == providerUserId);
                            //oauth is not bound yet
                            if (selectedProv == null) isBound = false;
                            else
                            {
                                isActive = selectedProv.IsActive;
                                selectedProv.AccessToken = accessToken;
                                selectedProv.TokenExpiresIn = expiresIn;
                                selectedProv.TokenLastUpdatedOnUtc = DateTime.Now;
                                if (!string.IsNullOrEmpty(refreshToken)) selectedProv.RefreshToken = refreshToken;
                            }
                        }
                        else isBound = false;
                        //bind the account if its not
                        if (!isBound)
                        {
                            db.OAuthUsers.Add(new OAuthUser()
                            {
                                UserId = acct.UserId,
                                ProviderUserId = providerUserId,
                                Provider = provider,
                                IsActive = true,
                                RefreshToken = refreshToken,
                                AccessToken = accessToken,
                                TokenExpiresIn = expiresIn,
                                CreatedOnUtc = DateTime.Now,
                                SyncStartsOnUtc = DateTime.Now
                            });
                        }

                        //if there is some kind of error then exit as false
                        db.SaveChanges();
                        //if Oauth account is active then everything is ok to go
                        if (isActive) result = acct.UserName;
                    }
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException err)
                {
                    EventLogger.LogEvent(err);
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                }
            }

            return result;
        }

        /// <summary>
        /// Updates the user.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="IsProfileEdit">if set to <c>true</c> [is profile edit].</param>
        /// <returns>System.String.</returns>
        public static string UpdateUser(User user, bool IsProfileEdit = false, bool IsADUser = false, bool IsUpdatedPass = false)
        {
            if (user == null || user.Person == null)
                return "Invalid user data";

            if (IsProfileEdit == false && (string.IsNullOrEmpty(user.Email) || !RegexUtil.IsValidEmail(user.Email)))
                return "A valid email address is required";

            try
            {
                using (var db = ContextFactory.Create())
                {
                    if (
                        db.Users.Any(
                            a =>
                                a.FranchiseId == user.FranchiseId && a.IsDeleted == false && a.ColorId == user.ColorId &&
                                a.UserId != user.UserId))
                        return "An existing user already has this color, please pick another.";
                    // if (db.Users.Any(a => a.FranchiseId == user.FranchiseId && a.Email == user.Email && a.UserId != user.UserId && a.IsDeleted == false && a.FranchiseId == user.FranchiseId)) return "Action cancelled, duplicate email address found.";
                    if (
                        db.Users.Any(
                            a =>
                                (a.Email == user.Email || a.UserName == user.UserName) && a.UserId != user.UserId &&
                                a.IsDeleted == false)) return "Action cancelled, duplicate email address found.";

                    //Entity framework is smart enough to know which property has changed and will only generate
                    //The sql to reflect the changed property so we dont have to manually check
                    var _user =
                        db.Users.Include(i => i.Roles)
                            .Include(i => i.Person)
                            .Include(i => i.OAuthUsers)
                            .Include(i => i.Address)
                            .SingleOrDefault(f => f.IsDeleted == false && f.UserId == user.UserId);

                    if (_user == null) return "User does not exist";
                    _user.UserName = user.UserName;
                    _user.Person.FirstName = user.Person.FirstName;
                    _user.Person.LastName = user.Person.LastName;
                    _user.Person.WorkTitle = user.Person.WorkTitle;
                    _user.Person.CompanyName = user.Person.CompanyName;
                    _user.Person.HomePhone = RegexUtil.GetNumbersOnly(user.Person.HomePhone);
                    _user.Person.CellPhone = RegexUtil.GetNumbersOnly(user.Person.CellPhone);
                    _user.Person.WorkPhone = RegexUtil.GetNumbersOnly(user.Person.WorkPhone);
                    _user.Person.FaxPhone = RegexUtil.GetNumbersOnly(user.Person.FaxPhone);
                    _user.Person.WorkPhoneExt = user.Person.WorkPhoneExt;
                    _user.Person.PreferredTFN = user.Person.PreferredTFN;
                    //for unsubscribe only change it if original has date and current doesnt 
                    //OR
                    //original doesnt have date and current has date
                    //this is due to the fact that we're using a date instead of a boolean to enable/disable subscription
                    if ((_user.Person.UnsubscribedOnUtc.HasValue && !user.Person.UnsubscribedOnUtc.HasValue) ||
                        (!_user.Person.UnsubscribedOnUtc.HasValue && user.Person.UnsubscribedOnUtc.HasValue))
                        _user.Person.UnsubscribedOnUtc = user.Person.UnsubscribedOnUtc;

                    if (_user.Address != null && user.Address != null)
                    {
                        _user.Address.AttentionText = user.Address.AttentionText;
                        _user.Address.Address1 = user.Address.Address1;
                        _user.Address.Address2 = user.Address.Address2;
                        _user.Address.City = user.Address.City;
                        _user.Address.State = user.Address.State;
                        _user.Address.ZipCode = !string.IsNullOrEmpty(user.Address.ZipCode)
                            ? user.Address.ZipCode.ToUpper()
                            : null;
                        _user.Address.CountryCode2Digits = user.Address.CountryCode2Digits;
                        _user.Address.IsValidated = user.Address.IsValidated;
                        _user.Address.IsResidential = user.Address.IsBusiness;
                        _user.Address.CrossStreet = user.Address.CrossStreet;
                    }
                    else if (_user.Address == null && user.Address != null)
                    {
                        if (!string.IsNullOrEmpty(user.Address.ZipCode))
                            user.Address.ZipCode = user.Address.ZipCode.ToUpper();
                        _user.Address = user.Address;
                    }

                    _user.Comment = user.Comment;
                    _user.Domain = user.Domain;
                    _user.DomainPath = user.DomainPath;
                    _user.ColorId = user.ColorId;
                    _user.CalendarFirstHour = user.CalendarFirstHour;
                    _user.SyncStartsOnUtc = user.SyncStartsOnUtc;
                    _user.Person.PrimaryEmail = user.Person.PrimaryEmail;
                    _user.Person.SecondaryEmail = user.Person.SecondaryEmail;
                    _user.Email = user.Email;
                    _user.CalSync = user.CalSync;
                    _user.IsDisabled = user.IsDisabled;
                    _user.LastActivityDateUtc = DateTime.UtcNow;

                    if (IsProfileEdit != true) //ignore these fields if it is a profile edit
                    {
                        if (_user.IsLockedOut != user.IsLockedOut)
                        {
                            _user.IsLockedOut = user.IsLockedOut;
                            if (user.IsLockedOut == false)
                            {
                                _user.LastLockoutDateUtc = null;
                                _user.FailedPasswordAttemptCount = 0;
                            }
                            else _user.LastLockoutDateUtc = DateTime.Now;
                        }
                    }

                    if (IsUpdatedPass)
                    {
                        if (!string.IsNullOrEmpty(user.Password) && user.Domain != "bbi.corp")
                        {
                            _user.Password = Crypto.HashPassword(user.Password);
                            _user.LastPasswordChangedDateUtc = DateTime.Now;
                        }
                        if (_user.Domain != "bbi.corp" && IsADUser) /// Thus its being made into AD user...
                        {
                            _user.Password = string.Empty;
                            _user.LastPasswordChangedDateUtc = DateTime.Now;
                        }
                    }
                    else
                    {
                        _user.Password = user.Password;
                        _user.LastPasswordChangedDateUtc = user.LastPasswordChangedDateUtc;
                    }

                    if (user.OAuthUsers != null)
                    {
                        foreach (var item in _user.OAuthUsers)
                        {
                            var currentOAuth =
                                user.OAuthUsers.FirstOrDefault(
                                    f => f.Provider.Equals(item.Provider, StringComparison.CurrentCultureIgnoreCase));
                            if (currentOAuth != null) item.IsActive = currentOAuth.IsActive;
                        }
                    }

                    if (IsProfileEdit != true) //ignore roles modification if its a profile edit
                    {
                        //this will ensure we always have a user role
                        //if (user.Roles.All(a => a.RoleId != AppConfigManager.DefaultRole.RoleId))
                        //user.Roles.Add(AppConfigManager.DefaultRole);

                        var joinedRoles = from r in _user.Roles
                                          join ur in user.Roles on r.RoleId equals ur.RoleId
                                          select new { Original = r, Current = ur };
                        var removeRoles = _user.Roles.Except(joinedRoles.Select(s => s.Original)).ToList();
                        var addRoles = user.Roles.Except(joinedRoles.Select(s => s.Current)).ToList();

                        foreach (var role in removeRoles)
                        {
                            _user.Roles.Remove(
                                db.Roles.FirstOrDefault(d => d.RoleId == role.RoleId));
                        }
                        foreach (var role in addRoles)
                        {
                            _user.Roles.Add(
                                db.Roles.FirstOrDefault(d => d.RoleId == role.RoleId));
                        }
                    }

                    db.SaveChanges();

                    using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                    {
                        connection.Execute("update CRM.Person set " +
                            "TimezoneCode=@TimezoneCode," +
                            "EnableEmailSignature=@EnableEmailSignature," +
                            "AddEmailSignAllEmails=@AddEmailSignAllEmails where PersonId=@PersonId", new
                            {
                                TimezoneCode = Convert.ToInt32(user.Person.TimezoneCode),
                                EnableEmailSignature = user.Person.EnableEmailSignature,
                                AddEmailSignAllEmails = user.Person.AddEmailSignAllEmails,
                                PersonId = user.Person.PersonId
                            });
                    }

                    //Remove and add the PermissionSets
                    if (user.PermissionSets != null)
                    {
                        using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                        {
                            connection.Execute("delete from [Auth].[UserInPermission] where UserId=@userId", new { userId = user.UserId });
                            string qInsertup = @"insert into [Auth].[UserInPermission](UserId,PermissionSetId,CreatedOn,CreatedBy) values(@UserId,@PermissionSetId,@CreatedOn,@CreatedBy)";
                            foreach (var permissionid in user.PermissionSets)
                            {
                                connection.Execute(qInsertup, new { UserId = user.UserId, PermissionSetId = permissionid, CreatedOn = DateTime.UtcNow, CreatedBy = SessionManager.CurrentUser.PersonId });
                            }

                        }
                    }

                    if (user.Address != null)
                    {
                        using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                        {
                            var query = "";
                            query = @"UPDATE [CRM].[Addresses] SET IsValidated = @IsValidated WHERE AddressId =@AddressId ";
                            var result = connection.Query<Address>(query, new { IsValidated = user.Address.IsValidated, AddressId = user.Address.AddressId });
                        }
                    }
                    CacheManager.UserCollection = null; //clear so it can refresh                    
                    if (SessionManager.CurrentUser.PersonId == _user.PersonId) //that would force you to logout? Why?
                        //if your updating current user then refresh session for current user
                        SessionManager.CurrentUser = null;
                    return "Success";
                }
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        public static string Encrypt(string password)
        {
            return Crypto.HashPassword(password);
        }

        public static User CreateUser(User data)
        {
            try
            {
                if (string.IsNullOrEmpty(data.UserName)) throw new Exception();

                if (string.IsNullOrEmpty(data.Password)) throw new ArgumentNullException("Password should be initializzed");
                //if (!RegexUtil.IsValidEmail(data.Email)) throw new Exception();
                string hashedPassword = Crypto.HashPassword(data.Password);
                if (hashedPassword.Length > 128) throw new Exception();
                if (data.Person == null) throw new Exception();
                if (ContextFactory.Current.Context.Users.Any(a => (a.UserName == data.UserName))) throw new Exception(ContantStrings.UsernameInUse);

                data.Password = hashedPassword;
                data.UserId = Guid.NewGuid();
                if (data.Address != null)
                {
                    if (!string.IsNullOrEmpty(data.Address.ZipCode)) data.Address.ZipCode = data.Address.ZipCode.ToUpper();
                }
                //set default prefered home phone if none provided
                if (string.IsNullOrEmpty(data.Person.PreferredTFN))
                {
                    data.Person.PreferredTFN = "H";
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return data;
        }

        public static string CreateUser(User data, bool isADuser, out Guid UserId)
        {
            UserId = Guid.Empty;
            #region Validatation
            string hashedPassword = string.Empty;

            if (string.IsNullOrEmpty(data.UserName))
                return System.Web.Security.MembershipCreateStatus.InvalidUserName.ToString();

            if (string.IsNullOrEmpty(data.Password) && !isADuser)
                return System.Web.Security.MembershipCreateStatus.InvalidPassword.ToString();

            if (!RegexUtil.IsValidEmail(data.Email))
                return System.Web.Security.MembershipCreateStatus.InvalidEmail.ToString();
            if (!isADuser)
            {
                hashedPassword = Crypto.HashPassword(data.Password);
                if (hashedPassword.Length > 128)
                    return System.Web.Security.MembershipCreateStatus.InvalidPassword.ToString();
            }
            if (data.Person == null)
                return "Invalid contact information";

            #endregion

            try
            {
                var db = ContextFactory.Current.Context;
                if (db.Users.Any(a => (a.Email == data.Email || a.UserName == data.UserName) && a.IsDeleted == false)) return "Action cancelled, duplicate email address or username.";
                if (db.Users.Any(a => a.FranchiseId == data.FranchiseId && a.IsDeleted == false && a.ColorId == data.ColorId)) return "An existing user already has this color, please pick another.";

                /// This is SERIOUSLY ANNOYING FEATURE, Turned it off until someone asks for it back... lets see if anyone misses that.
                ///else if (db.Users.Any(a => a.FranchiseId == data.FranchiseId && a.IsDeleted == false && a.ColorId == data.ColorId))
                ///    return "An existing user already has this color, please pick another.";

                if (!isADuser)
                {
                    data.Password = hashedPassword;
                }

                data.UserId = Guid.NewGuid();
                if (data.Address != null)
                {
                    if (!string.IsNullOrEmpty(data.Address.ZipCode)) data.Address.ZipCode = data.Address.ZipCode.ToUpper();
                }
                //set default prefered home phone if none provided
                if (string.IsNullOrEmpty(data.Person.PreferredTFN)) data.Person.PreferredTFN = "H";

                if (data.Roles == null) data.Roles = new List<Role>();
                //this will ensure a user role is always added
                //if (data.Roles.All(a => a.RoleId != AppConfigManager.DefaultRole.RoleId)) data.Roles.Add(AppConfigManager.DefaultRole);

                //we need to attach each role so it doesn't attempt to insert, it will only need to add the IDs to Auth.UsersInRoles
                foreach (var role in data.Roles)
                {
                    db.Roles.Attach(role);
                }
                db.Users.Add(data);
                db.SaveChanges();

                UserId = data.UserId;

                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Execute("update CRM.Person set TimezoneCode=@TimezoneCode where PersonId=@PersonId"
                        , new { TimezoneCode = Convert.ToInt32(data.Person.TimezoneCode), PersonId = data.Person.PersonId }
                        );
                }

                if (data.PermissionSets != null)
                {
                    using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                    {
                        connection.Execute("delete from [Auth].[UserInPermission] where UserId=@userId", new { userId = UserId });
                        string qInsertup = @"insert into [Auth].[UserInPermission](UserId,PermissionSetId,CreatedOn,CreatedBy) values(@UserId,@PermissionSetId,@CreatedOn,@CreatedBy)";
                        foreach (var permissionid in data.PermissionSets)
                        {
                            connection.Execute(qInsertup, new { UserId = UserId, PermissionSetId = permissionid, CreatedOn = DateTime.UtcNow, CreatedBy = SessionManager.CurrentUser.PersonId });
                        }

                    }
                }
                CacheManager.UserCollection = null; //clear so it can refresh   

                return "Success";
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        public static string DeleteUser(Guid? userId = null, string username = null)
        {
            if ((!userId.HasValue && string.IsNullOrEmpty(username)) ||
                (userId.HasValue && userId.Value == AppConfigManager.ApplicationAdmin.UserId)) //make sure to not delete application admin
            {
                return "User is not deleteable";
            }

            try
            {
                User aUser = null;
                if (userId.HasValue) aUser = ContextFactory.Current.Context.Users.FirstOrDefault(f => f.IsDeleted == false && f.UserId == userId.Value);
                else if (!string.IsNullOrEmpty(username)) aUser = ContextFactory.Current.Context.Users.FirstOrDefault(f => f.IsDeleted == false && f.UserName == username);

                if (aUser != null)
                {
                    //aUser.IsDeleted = true;
                    aUser.IsDisabled = true;
                    aUser.LastActivityDateUtc = DateTime.UtcNow;
                }
                ContextFactory.Current.Context.SaveChanges();
                CacheManager.RemoveCache("CRM:UserCollection");
                return "Success";
            }
            catch (Exception ex) { return EventLogger.LogEvent(ex); }
        }

        public static string ActivateUser(Guid? userId = null, string username = null)
        {
            try
            {
                User aUser = null;
                if (userId.HasValue) aUser = ContextFactory.Current.Context.Users.FirstOrDefault(f => f.IsDisabled == true && f.UserId == userId.Value);
                else if (!string.IsNullOrEmpty(username)) aUser = ContextFactory.Current.Context.Users.FirstOrDefault(f => f.IsDisabled == true && f.UserName == username);
                if (aUser != null)
                {
                    aUser.IsDisabled = false;
                    aUser.LastActivityDateUtc = DateTime.Now;
                }
                ContextFactory.Current.Context.SaveChanges();
                CacheManager.RemoveCache("CRM:UserCollection");
                return "Success";
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }

        }



    }
}
