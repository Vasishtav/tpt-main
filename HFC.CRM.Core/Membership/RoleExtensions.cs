﻿using System.Linq;
using HFC.CRM.Core.Common;
using HFC.CRM.Data;

namespace HFC.CRM.Core.Membership
{
    public static class RoleExtensions
    {
        public static bool IsWidgetEnabled(this Role role, int franchiseId, string widgetName)
        {
            var widgetType = CacheManager.WidgetTypes.FirstOrDefault(w => w.Name == widgetName);

            if (widgetType == null)
            {
                return false;
            }

            if (role.UserWidgets.Any(w => w.FranchiseId == franchiseId && w.Type_Widgets.Name == widgetName && w.Show == false))
            {
                return false;
            }

            if (role.UserWidgets.Any(w => w.FranchiseId == franchiseId && w.Type_Widgets.Name == widgetName && w.Show == true)) // if role has permission to see this widget
            {
                return true;
            }

            if (role.UserWidgets.Any(w => w.FranchiseId == null && w.Type_Widgets.Name == widgetName && w.Show == true)) // if role has permission to see this widget - global
            {
                return true;
            }
            
            return false;
        }
    }
}