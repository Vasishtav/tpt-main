﻿using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using WebMatrix.WebData;

/// <summary>
/// The Membership namespace.
/// </summary>
namespace HFC.CRM.Core.Membership
{
    /// <summary>
    /// Class LocalMembership.
    /// </summary>
    public partial class LocalMembership : ExtendedMembershipProvider
    {
        /// <summary>
        /// The CRMDB context
        /// </summary>
        private CRMContext CRMDBContext = new CRMContext();

        /// <summary>
        /// When overridden in a derived class, returns all OAuth membership accounts associated with the specified user name.
        /// </summary>
        /// <param name="userName">The user name.</param>
        /// <returns>A list of all OAuth membership accounts associated with the specified user name.</returns>
        public override ICollection<OAuthAccountData> GetAccountsForUser(string userName)
        {
            ICollection<OAuthAccountData> result = null;
            if (!string.IsNullOrEmpty(userName))
            {
                result = (from u in CRMDBContext.Users
                            from p in u.OAuthUsers
                            where u.UserName == userName
                            select new OAuthAccountData(p.Provider, p.ProviderUserId)).ToList();  
            }
            return result;
        }

        /// <summary>
        /// The name of the application using the custom membership provider.
        /// </summary>
        /// <value>The name of the application.</value>
        /// <returns>The name of the application using the custom membership provider.</returns>
        public override string ApplicationName
        {
            get
            {
                return AppConfigManager.ApplicationName;
            }
            set { 
                //do nothing, application name is set by CRM.AppConfig table
            }
        }

        /// <summary>
        /// Processes a request to update the password for a membership user.
        /// </summary>
        /// <param name="username">The user to update the password for.</param>
        /// <param name="oldPassword">The current password for the specified user.</param>
        /// <param name="newPassword">The new password for the specified user.</param>
        /// <returns>true if the password was updated successfully; otherwise, false.</returns>
        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            using (var db = new CRMContextEx())
            {
                bool result = false;

                if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(oldPassword) && !string.IsNullOrEmpty(newPassword))
                {
                    try
                    {
                        User aUser = db.Users.FirstOrDefault(f => f.UserName == username);
                        if (aUser != null)
                        {
                            bool isValid = false;
                            if (!string.IsNullOrEmpty(aUser.Password)) isValid = Crypto.VerifyHashedPassword(aUser.Password, oldPassword);

                            if (isValid)
                            {
                                aUser.Password = Crypto.HashPassword(newPassword);
                                aUser.IsLockedOut = false;
                                aUser.LastLockoutDateUtc = null;
                                aUser.FailedPasswordAttemptCount = 0;
                                aUser.FailedPasswordAttemptStartDateUtc = null;
                                aUser.LastPasswordChangedDateUtc = DateTime.Now;

                                if (db.SaveChanges() > 0) result = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        EventLogger.LogEvent(ex);
                    }
                }

                return result;
            }
        }

        /// <summary>
        /// Indicates whether the membership provider is configured to allow users to reset their passwords.
        /// </summary>
        /// <value><c>true</c> if [enable password reset]; otherwise, <c>false</c>.</value>
        /// <returns>true if the membership provider supports password reset; otherwise, false. The default is true.</returns>
        public override bool EnablePasswordReset
        {
            get { return true; }
        }

        /// <summary>
        /// Disabled, password are strongly hashed, will be impossible to retrieve password.  Use Password Reset instead.
        /// </summary>
        /// <value><c>true</c> if [enable password retrieval]; otherwise, <c>false</c>.</value>
        /// <returns>true if the membership provider is configured to support password retrieval; otherwise, false. The default is false.</returns>
        public override bool EnablePasswordRetrieval
        {
            get { return false; }
        }


        /// <summary>
        /// Gets the number of users currently accessing the application.
        /// </summary>
        /// <returns>The number of users currently accessing the application.</returns>
        public override int GetNumberOfUsersOnline()
        {
            DateTime DateActive = DateTime.Now.Subtract(TimeSpan.FromMinutes(Convert.ToDouble(System.Web.Security.Membership.UserIsOnlineTimeWindow)));
           
            return CRMDBContext.Users.Where(Usr => Usr.IsDeleted == false && Usr.LastActivityDateUtc > DateActive).Count();            
        }

        /// <summary>
        /// Default to 25
        /// </summary>
        /// <value>The maximum invalid password attempts.</value>
        /// <returns>The number of invalid password or password-answer attempts allowed before the membership user is locked out.</returns>
        public override int MaxInvalidPasswordAttempts
        {
            get { return 25; }
        }

        /// <summary>
        /// Default to 1
        /// </summary>
        /// <value>The minimum required non alphanumeric characters.</value>
        /// <returns>The minimum number of special characters that must be present in a valid password.</returns>
        public override int MinRequiredNonAlphanumericCharacters
        {
            get { return 1; }
        }

        /// <summary>
        /// Default to 6
        /// </summary>
        /// <value>The minimum length of the required password.</value>
        /// <returns>The minimum length required for a password. </returns>
        public override int MinRequiredPasswordLength
        {
            get { return 6; }
        }

        /// <summary>
        /// Default to 30 minute window for 25 attempts
        /// </summary>
        /// <value>The password attempt window.</value>
        /// <returns>The number of minutes in which a maximum number of invalid password or password-answer attempts are allowed before the membership user is locked out.</returns>
        public override int PasswordAttemptWindow
        {
            get { return 30; }
        }

        /// <summary>
        /// Using a custom hashing algorithm
        /// </summary>
        /// <value>The password format.</value>
        /// <returns>One of the <see cref="T:System.Web.Security.MembershipPasswordFormat" /> values indicating the format for storing passwords in the data store.</returns>
        public override System.Web.Security.MembershipPasswordFormat PasswordFormat
        {
            get { return System.Web.Security.MembershipPasswordFormat.Hashed; }
        }

        /// <summary>
        /// 6 or greater characters
        /// Contains at least one digit
        /// Contains at least one special (non-alphanumeric) character
        /// </summary>
        /// <value>The password strength regular expression.</value>
        /// <returns>A regular expression used to evaluate a password.</returns>
        public override string PasswordStrengthRegularExpression
        {
            get { return @"(?=.{6,})(?=(.*\d){1,})(?=(.*\W){1,})"; }
        }

        /// <summary>
        /// Using Active Directory for managing user so this option is not needed
        /// </summary>
        /// <value><c>true</c> if [requires question and answer]; otherwise, <c>false</c>.</value>
        /// <returns>true if a password answer is required for password reset and retrieval; otherwise, false. The default is true.</returns>
        public override bool RequiresQuestionAndAnswer
        {
            get { return false; }
        }

        /// <summary>
        /// Default to true, username is their email address or AD domain name, ie: smith@gmail.com or smith@bbi.corp
        /// </summary>
        /// <value><c>true</c> if [requires unique email]; otherwise, <c>false</c>.</value>
        /// <returns>true if the membership provider requires a unique e-mail address; otherwise, false. The default is true.</returns>
        public override bool RequiresUniqueEmail
        {
            get { return true; }
        }

        /// <summary>
        /// Resets password to a random string
        /// </summary>
        /// <param name="username">The user to reset the password for.</param>
        /// <param name="answer">The password answer for the specified user.</param>
        /// <returns>The random password</returns>
        public override string ResetPassword(string username, string answer)
        {
            string result = null;

            if (EnablePasswordReset && !string.IsNullOrEmpty(username))
            {                
                try
                {
                    User aUser = CRMDBContext.Users.FirstOrDefault(f => f.UserName == username);
                    if (aUser != null)
                    {
                        bool isValid = !RequiresQuestionAndAnswer;
                        //if (RequiresQuestionAndAnswer && !string.IsNullOrEmpty(answer))
                        //{
                        //    if (string.IsNullOrEmpty(aUser.PasswordAnswer))
                        //        isValid = false;
                        //    else
                        //        isValid = Crypto.VerifyHashedPassword(aUser.PasswordAnswer, answer);
                        //}

                        if (isValid)                            
                        {
                            result = Crypto.GenerateSalt(8);
                            aUser.Password = Crypto.HashPassword(result);
                            aUser.IsLockedOut = false;
                            aUser.LastLockoutDateUtc = null;
                            aUser.FailedPasswordAttemptCount = 0;
                            aUser.FailedPasswordAttemptStartDateUtc = null;
                            aUser.LastPasswordChangedDateUtc = DateTime.Now;

                            //if it fails for some reason, reset the result so it doesnt return and get used
                            if (CRMDBContext.SaveChanges() <= 0)
                                result = null;
                        }
                    }                    
                }
                catch (Exception ex)
                {
                    result = EventLogger.LogEvent(ex);
#if DEBUG
                    result = ex.Message;
#endif
                }
            }

            return result;
        }

        /// <summary>
        /// Clears a lock so that the membership user can be validated.
        /// </summary>
        /// <param name="userName">The membership user whose lock status you want to clear.</param>
        /// <returns>true if the membership user was successfully unlocked; otherwise, false.</returns>
        public override bool UnlockUser(string userName)
        {
            bool result = false;

            if (!string.IsNullOrEmpty(userName))
            {
                try
                {
                    User aUser = CRMDBContext.Users.FirstOrDefault(f => f.UserName == userName);
                    if (aUser != null)
                    {
                        aUser.IsLockedOut = false;
                        aUser.LastLockoutDateUtc = null;
                        aUser.FailedPasswordAttemptCount = 0;
                        aUser.FailedPasswordAttemptStartDateUtc = null;

                        if (CRMDBContext.SaveChanges() > 0)
                            result = true;
                    }                    
                }
                catch (Exception ex) { EventLogger.LogEvent(ex); }
            }

            return result;
        }

        /// <summary>
        /// Verifies that the specified user name and password exist in the data source.
        /// </summary>
        /// <param name="username">The name of the user to validate.</param>
        /// <param name="password">The password for the specified user.</param>
        /// <returns>true if the specified username and password are valid; otherwise, false.</returns>
        public override bool ValidateUser(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                return false;
            }

            using (var db = new CRMContextEx())
            {

                bool VerificationSucceeded = false;

                try
                {
                    var aUser = db.Users.FirstOrDefault(Usr => Usr.IsDeleted == false && Usr.UserName == username);

                    if (aUser == null || !aUser.IsApproved || aUser.IsLockedOut || (aUser.IsDisabled ?? false))
                    {
                        return false;
                    }
                    else // normal validation
                    {
                        ActiveDirectoryMembership ADMembership = null;
                        if (AppConfigManager.GetConfig<bool>("Enabled", "Active Directory", "Authentication") && !string.IsNullOrEmpty(aUser.Domain))
                        {
                            //first validate user through AD
                            ADMembership = new ActiveDirectoryMembership(username, password);
                            VerificationSucceeded = ADMembership.IsAuthenticated();
                        }

                        //then check local user authentication
                        if (!VerificationSucceeded) VerificationSucceeded = (!string.IsNullOrEmpty(aUser.Password) && Crypto.VerifyHashedPassword(aUser.Password, password));

                        if (HttpContext.Current.Request != null && !string.IsNullOrEmpty(HttpContext.Current.Request.UserHostAddress)) aUser.LastKnownIPAddress = HttpContext.Current.Request.UserHostAddress;

                        if (VerificationSucceeded)
                        {
                            aUser.FailedPasswordAttemptCount = 0;
                            aUser.LastLoginDateUtc = DateTime.UtcNow;
                            aUser.LastActivityDateUtc = DateTime.UtcNow;
                            if (ADMembership != null)
                            {
                                aUser.Domain = ADMembership.Domain;
                                aUser.DomainPath = ADMembership.DomainPath;
                            }
                        }
                        else
                        {
                            int Failures = aUser.FailedPasswordAttemptCount;
                            if (Failures < MaxInvalidPasswordAttempts)
                            {
                                aUser.FailedPasswordAttemptCount += 1;
                                aUser.FailedPasswordAttemptStartDateUtc = DateTime.UtcNow;
                            }
                            else if (Failures >= MaxInvalidPasswordAttempts)
                            {
                                aUser.LastLockoutDateUtc = DateTime.UtcNow;
                                aUser.IsLockedOut = true;
                            }
                        }
                        db.SaveChanges();
                    }

                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                }

                return VerificationSucceeded;
            }
        }

        #region NOT IMPLEMENTED - USE EXTENDED VERSION INSTEAD
        /// <summary>
        /// [Obsolete("Use Extended Method Instead")]
        /// </summary>
        /// <param name="username">The name of the user to delete.</param>
        /// <param name="deleteAllRelatedData">true to delete data related to the user from the database; false to leave data related to the user in the database.</param>
        /// <returns>true if the user was successfully deleted; otherwise, false.</returns>
        /// <exception cref="System.NotImplementedException">Use extended method instead</exception>
        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException("Use extended method instead");
            //return DeleteUser(null, username);
        }

        /// <summary>
        /// When overridden in a derived class, returns the date and time when an incorrect password was most recently entered for the specified user account.
        /// </summary>
        /// <param name="userName">The user name of the account.</param>
        /// <returns>The date and time when an incorrect password was most recently entered for this user account, or <see cref="F:System.DateTime.MinValue" /> if an incorrect password has not been entered for this user account.</returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override DateTime GetLastPasswordFailureDate(string userName)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// When overridden in a derived class, returns the date and time when the password was most recently changed for the specified membership account.
        /// </summary>
        /// <param name="userName">The user name of the account.</param>
        /// <returns>The date and time when the password was more recently changed for membership account, or <see cref="F:System.DateTime.MinValue" /> if the password has never been changed for this user account.</returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override DateTime GetPasswordChangedDate(string userName)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// When overridden in a derived class, returns the number of times that the password for the specified user account was incorrectly entered since the most recent successful login or since the user account was created.
        /// </summary>
        /// <param name="userName">The user name of the account.</param>
        /// <returns>The count of failed password attempts for the specified user account.</returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override int GetPasswordFailuresSinceLastSuccess(string userName)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// When overridden in a derived class, returns an ID for a user based on a password reset token.
        /// </summary>
        /// <param name="token">The password reset token.</param>
        /// <returns>The user ID.</returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override int GetUserIdFromPasswordResetToken(string token)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// When overridden in a derived class, returns a value that indicates whether the user account has been confirmed by the provider.
        /// </summary>
        /// <param name="userName">The user name.</param>
        /// <returns>true if the user is confirmed; otherwise, false.</returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override bool IsConfirmed(string userName)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// When overridden in a derived class, resets a password after verifying that the specified password reset token is valid.
        /// </summary>
        /// <param name="token">A password reset token.</param>
        /// <param name="newPassword">The new password.</param>
        /// <returns>true if the password was changed; otherwise, false.</returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override bool ResetPasswordWithToken(string token, string newPassword)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Activates a pending membership account.
        /// </summary>
        /// <param name="accountConfirmationToken">A confirmation token to pass to the authentication provider.</param>
        /// <returns>true if the account is confirmed; otherwise, false.</returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override bool ConfirmAccount(string accountConfirmationToken)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Activates a pending membership account for the specified user.
        /// </summary>
        /// <param name="userName">The user name.</param>
        /// <param name="accountConfirmationToken">A confirmation token to pass to the authentication provider.</param>
        /// <returns>true if the account is confirmed; otherwise, false.</returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override bool ConfirmAccount(string userName, string accountConfirmationToken)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// When overridden in a derived class, creates a new user account using the specified user name and password, optionally requiring that the new account must be confirmed before the account is available for use.
        /// </summary>
        /// <param name="userName">The user name.</param>
        /// <param name="password">The password.</param>
        /// <param name="requireConfirmationToken">(Optional) true to specify that the account must be confirmed; otherwise, false. The default is false.</param>
        /// <returns>A token that can be sent to the user to confirm the account.</returns>
        /// <exception cref="System.NotImplementedException">Use the extended method instead</exception>
        public override string CreateAccount(string userName, string password, bool requireConfirmationToken)
        {
            throw new NotImplementedException("Use the extended method instead");
        }

        /// <summary>
        /// When overridden in a derived class, creates a new user profile and a new membership account.
        /// </summary>
        /// <param name="userName">The user name.</param>
        /// <param name="password">The password.</param>
        /// <param name="requireConfirmation">(Optional) true to specify that the user account must be confirmed; otherwise, false. The default is false.</param>
        /// <param name="values">(Optional) A dictionary that contains additional user attributes to store in the user profile. The default is null.</param>
        /// <returns>A token that can be sent to the user to confirm the user account.</returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override string CreateUserAndAccount(string userName, string password, bool requireConfirmation, IDictionary<string, object> values)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// When overridden in a derived class, deletes the specified membership account.
        /// </summary>
        /// <param name="userName">The user name.</param>
        /// <returns>true if the user account was deleted; otherwise, false.</returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override bool DeleteAccount(string userName)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// When overridden in a derived class, generates a password reset token that can be sent to a user in email.
        /// </summary>
        /// <param name="userName">The user name.</param>
        /// <param name="tokenExpirationInMinutesFromNow">(Optional) The time, in minutes, until the password reset token expires. The default is 1440 (24 hours).</param>
        /// <returns>A token to send to the user.</returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override string GeneratePasswordResetToken(string userName, int tokenExpirationInMinutesFromNow)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Adds a new membership user to the data source.
        /// </summary>
        /// <param name="username">The user name for the new user.</param>
        /// <param name="password">The password for the new user.</param>
        /// <param name="email">The e-mail address for the new user.</param>
        /// <param name="passwordQuestion">The password question for the new user.</param>
        /// <param name="passwordAnswer">The password answer for the new user</param>
        /// <param name="isApproved">Whether or not the new user is approved to be validated.</param>
        /// <param name="providerUserKey">The unique identifier from the membership data source for the user.</param>
        /// <param name="status">A <see cref="T:System.Web.Security.MembershipCreateStatus" /> enumeration value indicating whether the user was created successfully.</param>
        /// <returns>A <see cref="T:System.Web.Security.MembershipUser" /> object populated with the information for the newly created user.</returns>
        /// <exception cref="System.NotImplementedException">Use the extended method instead</exception>
        public override System.Web.Security.MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out System.Web.Security.MembershipCreateStatus status)
        {
            throw new NotImplementedException("Use the extended method instead");
        }

        /// <summary>
        /// When overridden in a derived class, returns the date and time when the specified user account was created.
        /// </summary>
        /// <param name="userName">The user name of the account.</param>
        /// <returns>The date and time the account was created, or <see cref="F:System.DateTime.MinValue" /> if the account creation date is not available.</returns>
        /// <exception cref="System.NotImplementedException">Use CreationDate property after retrieving user</exception>
        public override DateTime GetCreateDate(string userName)
        {
            throw new NotImplementedException("Use CreationDate property after retrieving user");
        }

        /// <summary>
        /// Processes a request to update the password question and answer for a membership user.
        /// </summary>
        /// <param name="username">The user to change the password question and answer for.</param>
        /// <param name="password">The password for the specified user.</param>
        /// <param name="newPasswordQuestion">The new password question for the specified user.</param>
        /// <param name="newPasswordAnswer">The new password answer for the specified user.</param>
        /// <returns>true if the password question and answer are updated successfully; otherwise, false.</returns>
        /// <exception cref="System.NotImplementedException">Password and answer is disabled</exception>
        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException("Password and answer is disabled");
        }

        /// <summary>
        /// Gets a collection of membership users where the e-mail address contains the specified e-mail address to match.
        /// </summary>
        /// <param name="emailToMatch">The e-mail address to search for.</param>
        /// <param name="pageIndex">The index of the page of results to return. <paramref name="pageIndex" /> is zero-based.</param>
        /// <param name="pageSize">The size of the page of results to return.</param>
        /// <param name="totalRecords">The total number of matched users.</param>
        /// <returns>A <see cref="T:System.Web.Security.MembershipUserCollection" /> collection that contains a page of <paramref name="pageSize" /><see cref="T:System.Web.Security.MembershipUser" /> objects beginning at the page specified by <paramref name="pageIndex" />.</returns>
        /// <exception cref="System.NotImplementedException">Use the extended version instead</exception>
        public override System.Web.Security.MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException("Use the extended version instead");
        }

        /// <summary>
        /// Gets a collection of membership users where the user name contains the specified user name to match.
        /// </summary>
        /// <param name="usernameToMatch">The user name to search for.</param>
        /// <param name="pageIndex">The index of the page of results to return. <paramref name="pageIndex" /> is zero-based.</param>
        /// <param name="pageSize">The size of the page of results to return.</param>
        /// <param name="totalRecords">The total number of matched users.</param>
        /// <returns>A <see cref="T:System.Web.Security.MembershipUserCollection" /> collection that contains a page of <paramref name="pageSize" /><see cref="T:System.Web.Security.MembershipUser" /> objects beginning at the page specified by <paramref name="pageIndex" />.</returns>
        /// <exception cref="System.NotImplementedException">Use the extended version instead</exception>
        public override System.Web.Security.MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException("Use the extended version instead");
        }

        /// <summary>
        /// Gets a collection of all the users in the data source in pages of data.
        /// </summary>
        /// <param name="pageIndex">The index of the page of results to return. <paramref name="pageIndex" /> is zero-based.</param>
        /// <param name="pageSize">The size of the page of results to return.</param>
        /// <param name="totalRecords">The total number of matched users.</param>
        /// <returns>A <see cref="T:System.Web.Security.MembershipUserCollection" /> collection that contains a page of <paramref name="pageSize" /><see cref="T:System.Web.Security.MembershipUser" /> objects beginning at the page specified by <paramref name="pageIndex" />.</returns>
        /// <exception cref="System.NotImplementedException">Use the extended version instead</exception>
        public override System.Web.Security.MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException("Use the extended version instead");
        }

        /// <summary>
        /// Gets the password for the specified user name from the data source.
        /// </summary>
        /// <param name="username">The user to retrieve the password for.</param>
        /// <param name="answer">The password answer for the user.</param>
        /// <returns>The password for the specified user name.</returns>
        /// <exception cref="System.NotImplementedException">Password retrieval is not possible, please use password reset instead</exception>
        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException("Password retrieval is not possible, please use password reset instead");
        }
        /// <summary>
        /// [Obsolete("Use Extended Method Instead")]
        /// </summary>
        /// <param name="username">The name of the user to get information for.</param>
        /// <param name="userIsOnline">true to update the last-activity date/time stamp for the user; false to return user information without updating the last-activity date/time stamp for the user.</param>
        /// <returns>A <see cref="T:System.Web.Security.MembershipUser" /> object populated with the specified user's information from the data source.</returns>
        /// <exception cref="System.NotImplementedException">Use the extended version instead</exception>
        public override System.Web.Security.MembershipUser GetUser(string username, bool userIsOnline)
        {
            throw new NotImplementedException("Use the extended version instead");
        }
        /// <summary>
        /// [Obsolete("Use Extended Method Instead")]
        /// </summary>
        /// <param name="providerUserKey">The unique identifier for the membership user to get information for.</param>
        /// <param name="userIsOnline">true to update the last-activity date/time stamp for the user; false to return user information without updating the last-activity date/time stamp for the user.</param>
        /// <returns>A <see cref="T:System.Web.Security.MembershipUser" /> object populated with the specified user's information from the data source.</returns>
        /// <exception cref="System.NotImplementedException">Use the extended version instead</exception>
        public override System.Web.Security.MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException("Use the extended version instead");
        }
        /// <summary>
        /// [Obsolete("Use Extended Method Instead")]
        /// </summary>
        /// <param name="email">The e-mail address to search for.</param>
        /// <returns>The user name associated with the specified e-mail address. If no match is found, return null.</returns>
        /// <exception cref="System.NotImplementedException">Use the extended version instead</exception>
        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException("Use the extended version instead");
        }
        /// <summary>
        /// [Obsolete("Use Extended Method Instead")]
        /// </summary>
        /// <param name="user">A <see cref="T:System.Web.Security.MembershipUser" /> object that represents the user to update and the updated information for the user.</param>
        /// <exception cref="System.NotImplementedException">Use extended method instead</exception>
        public override void UpdateUser(System.Web.Security.MembershipUser user)
        {
            throw new NotImplementedException("Use extended method instead");
        }

        #endregion

    }
}
