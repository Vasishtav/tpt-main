﻿using System;
using System.Collections.Generic;
using System.Linq;

using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using System.Data.Entity;
using HFC.CRM.Data;

/// <summary>
/// The Membership namespace.
/// </summary>
namespace HFC.CRM.Core.Membership
{
    using HFC.CRM.Data.Context;

    /// <summary>
    /// Class PermissionProvider.
    /// </summary>
    public class PermissionProviderNotUsed
    {
        //public enum CRUDEnum { Create, Read, Update, Delete }
        
        //obsolete. moved to CacheManager
        //public static List<Permission> PermissionCollection
        //{
        //    get
        //    {
        //        if (CacheManager.PermissionCollection == null)
        //        {
        //            using (var db = new AuthContext())
        //            {
        //                CacheManager.PermissionCollection = db.Permissions.ToList();
        //            }
        //        }
        //        return CacheManager.PermissionCollection;
        //    }
        //    private set
        //    {
        //        CacheManager.PermissionCollection = value;
        //    }
        //}
       
        //public static List<Type_Permission> PermissionTypeCollection
        //{
        //    get
        //    {
        //        if (CacheManager.PermissionTypeCollection == null)
        //        {
        //            using (var db = new AuthContext())
        //            {
        //                CacheManager.PermissionTypeCollection = db.Type_Permission.ToList();
        //            }
        //        }
        //        return CacheManager.PermissionTypeCollection;
        //    }
        //    private set
        //    {
        //        CacheManager.PermissionTypeCollection = value;
        //    }
        //}

        /// <summary>
        /// Gets a special permission, these are not based on any roles but can be for a user
        /// </summary>
        /// <param name="userToCheck">The user to check.</param>
        /// <param name="name">The name.</param>
        /// <param name="subsection">The subsection.</param>
        /// <returns>Permission.</returns>
        public static Permission GetSpecialPermission(User userToCheck, string name, string subsection = null)
        {
            var permission = new Permission();

            if (userToCheck == null)
                return permission;

            Type_Permission permType = null;
            if(!string.IsNullOrEmpty(subsection))
                permType = CacheManager.PermissionTypeCollection
                    .FirstOrDefault(f => string.Compare(f.Name, name, true) == 0 && string.Compare(f.Subsection, subsection, true) == 0);
            else
                permType = CacheManager.PermissionTypeCollection
                    .FirstOrDefault(f => string.Compare(f.Name, name, true) == 0 && string.IsNullOrEmpty(f.Subsection));

            if (permType != null)
            {
                var linq = CacheManager.PermissionCollection.Where(p => p.PermissionTypeId == permType.PermissionTypeId);

                permission = linq.FirstOrDefault(p => p.PersonId == userToCheck.PersonId);
                //get permission for specific person if it doesnt exist then get franchise specific or base permission
                if (permission == null) 
                {
                    //order by descending so if there is a franchise custom permission it gets used first
                    permission = linq.Where(f => f.PersonId == null).OrderByDescending(o => o.FranchiseId).FirstOrDefault() ?? new Permission();

                    // check again if there is any permission record at all for this special permission
                    // if not then create a new permission object, all CRUD will default to false instead of passing back null and possibly crash
                }
            }  

            return permission;
        }

        /// <summary>
        /// Gets the permission.
        /// </summary>
        /// <param name="userToCheck">The user to check.</param>
        /// <param name="name">The name.</param>
        /// <param name="subsection">The subsection.</param>
        /// <returns>Permission.</returns>
        public static Permission GetPermission(User userToCheck, string name, string subsection = null)
        {
            //select both the base and franchise specific permissions
            List<int> permTypes = null;
            if (string.IsNullOrEmpty(subsection))
            {
                permTypes = CacheManager.PermissionTypeCollection
                        .Where(f => string.Compare(f.Name, name, true) == 0 && string.IsNullOrEmpty(f.Subsection))
                        .Select(s => s.PermissionTypeId).ToList();
            }
            else //add subsection if any
            {
                permTypes = CacheManager.PermissionTypeCollection
                    .Where(f => string.Compare(f.Name, name, true) == 0 && string.Compare(f.Subsection, subsection, true) == 0) //or specific permission with a subsection
                    .Select(s => s.PermissionTypeId).ToList();
            }                
            
            return GetPermission(userToCheck, permTypes);
        }


        public static IEnumerable<object> GetUserPermissions(User user)
        {
            var result = new List<object>();

            foreach (var permission in CacheManager.PermissionTypeCollection)
            {
                var p = GetPermission(user, permission.Name, permission.Subsection);
                if (p != null)
                {
                    result.Add(new 
                               {
                                   p.CanCreate,
                                   p.CanDelete,
                                   p.CanUpdate,
                                   p.CanRead,
                                   permission.Name,
                                   permission.Subsection
                               });
                }
            }

            return result;
        } 

        ///// <summary>
        ///// Gets highest permission from permission type
        ///// </summary>
        ///// <param name="userToCheck">Will use SessionManager.CurrentUser if not supplied</param>
        ///// <returns></returns>
        //public static Permission GetPermission(int permissionTypeId, User userToCheck = null)
        //{
        //    return GetPermission(new List<int>() { permissionTypeId }, userToCheck);
        //}

        
        private static Permission GetPermission(User userToCheck, List<int> permissionTypeIds)
        {
            var result = new Permission();
            if (userToCheck == null)
                return result;

            var owners = new List<Guid?>();

            if (SessionManager.CurrentFranchise != null 
                && SessionManager.CurrentFranchise.FranchiseOwners != null)
            {
                owners = SessionManager.CurrentFranchise.FranchiseOwners
                    .Select(v => v.UserId).ToList();
            }

            //we always return true for all CRUD for admin, system default 
            // so we don't let anyone change this by database
            if (userToCheck.IsInRole(AppConfigManager.AppAdminRole.RoleId) 
                || owners.Contains(userToCheck.UserId))
            {
                result.CanCreate = true;
                result.CanRead = true;
                result.CanUpdate = true;
                result.CanDelete = true;

                return result;
            }

            if (permissionTypeIds != null && permissionTypeIds.Count > 0)  
            {
                try
                {
                    var rolePerm = (from p in CacheManager.PermissionCollection
                                    where permissionTypeIds.Contains(p.PermissionTypeId) &&  //get permission for the requested permission type                                         
                                        (!p.FranchiseId.HasValue || p.FranchiseId == userToCheck.FranchiseId) && //select both base and custom permission
                                        (!p.RoleId.HasValue || (p.RoleId.HasValue && userToCheck.Roles.Select(s => s.RoleId).Contains(p.RoleId.Value))) //select permission for the requested role of user
                                select p).ToList();

                    var userPerm = CacheManager.PermissionCollection.FirstOrDefault(p => permissionTypeIds.Contains(p.PermissionTypeId) && p.PersonId == userToCheck.PersonId);

                    //if there is a user specific permission, this takes precedence over role permission
                    if (userPerm != null)
                    {
                        result = userPerm;
                    }
                    else if (rolePerm != null && rolePerm.Count > 0) //run through all role permission and retrieve the highest CRUD permission                    
                    {
                        //we will want to split into franchise custom vs base permission
                        //if they don't have a franchise custom permission then use base permission
                        var FranPerm = rolePerm.Where(w => w.FranchiseId.HasValue).ToList();
                        var BasePerm = rolePerm.Where(w => !w.FranchiseId.HasValue).ToList();

                        // we use Max to group similar parent permissions, ie:                        
                        // if we want to see if user has permission to view Settings we grab all "Settings" permission, this will return
                        // Settings > Users
                        // Settings > Roles
                        // Settings > Permissions //etc..
                        // so now we have 3 permission types and at least one must grant read access to allow read access to Settings menu
                        // example usage in /Shared/_Layout.cshtml
                        // @if (PermissionProvider.GetPermission("Settings").CanRead) 
                        result.CanCreate = FranPerm != null && FranPerm.Count > 0 ? FranPerm.Max(m => m.CanCreate) : BasePerm.Max(m => m.CanCreate);
                        result.CanRead = FranPerm != null && FranPerm.Count > 0 ? FranPerm.Max(m => m.CanRead) : BasePerm.Max(m => m.CanRead);
                        result.CanUpdate = FranPerm != null && FranPerm.Count > 0 ? FranPerm.Max(m => m.CanUpdate) : BasePerm.Max(m => m.CanUpdate);
                        result.CanDelete = FranPerm != null && FranPerm.Count > 0 ? FranPerm.Max(m => m.CanDelete) : BasePerm.Max(m => m.CanDelete);
                    }
                    
                }
                catch (Exception ex) { EventLogger.LogEvent(ex); }
            }

            return result;
        }

        /// <summary>
        /// Gets the permissions.
        /// </summary>
        /// <param name="franchiseId">if null then it will return base permissions</param>
        /// <param name="includes">The includes.</param>
        /// <returns>List&lt;Permission&gt;.</returns>
        public static List<Permission> GetPermissions(int? franchiseId, params string[] includes)
        {
            throw new NotImplementedException("Yet to implement in Touchpoint");

            //var result = new List<Permission>();
            
            //try
            //{
            //    var linq = ContextFactory.Current.Context.Permissions.Where(w => !w.FranchiseId.HasValue);
            //    if (franchiseId.HasValue && franchiseId.Value > 0) linq = ContextFactory.Current.Context.Permissions.Where(w => !w.FranchiseId.HasValue || w.FranchiseId == franchiseId);
            //    linq = includes.Aggregate(linq, (current, inc) => current.Include(inc));

            //    result = linq.ToList();
            //}
            //catch (Exception ex) { EventLogger.LogEvent(ex); }            

            //return result;
        }

        /// <summary>
        /// Gets the highest CRUD operation from two permission sets
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>Permission.</returns>
        public static Permission MaxPermission(Permission left, Permission right)
        {
            var combined = new Permission
                           {
                               CanCreate = right.CanCreate ? right.CanCreate : left.CanCreate,
                               CanRead = right.CanRead ? right.CanRead : left.CanRead,
                               CanUpdate = right.CanUpdate ? right.CanUpdate : left.CanUpdate,
                               CanDelete = right.CanDelete ? right.CanDelete : left.CanDelete
                           };

            return combined;
        }

        /// <summary>
        /// Updates the permission.
        /// </summary>
        /// <param name="permissionId">The permission identifier.</param>
        /// <param name="franchiseId">The franchise identifier.</param>
        /// <param name="canCreate">if set to <c>true</c> [can create].</param>
        /// <param name="canRead">if set to <c>true</c> [can read].</param>
        /// <param name="canUpdate">if set to <c>true</c> [can update].</param>
        /// <param name="canDelete">if set to <c>true</c> [can delete].</param>
        /// <returns>System.String.</returns>
        public static string UpdatePermission(int permissionId, int? franchiseId, bool canCreate, bool canRead, bool canUpdate, bool canDelete)
        {
            string result = "Unable to update permission";
            if (franchiseId.HasValue && franchiseId <= 0)
                return "Invalid franchise";

            if (permissionId > 0)
            {
                try
                {
                    var perm = ContextFactory.Current.Context.Permissions.FirstOrDefault(f => f.PermissionId == permissionId && (franchiseId == null || f.FranchiseId == franchiseId));
                    if (perm == null) return "Permission not found for your franchise";

                    perm.CanCreate = canCreate;
                    perm.CanRead = canRead;
                    perm.CanUpdate = canUpdate;
                    perm.CanDelete = canDelete;

                    ContextFactory.Current.Context.SaveChanges();

                    var p = CacheManager.PermissionCollection.FirstOrDefault(f => f.PermissionId == permissionId);
                    if (p != null)
                    {
                        p.CanCreate = perm.CanCreate;
                        p.CanRead = perm.CanRead;
                        p.CanUpdate = perm.CanUpdate;
                        p.CanDelete = perm.CanDelete;
                    }
                    result = "Success";
                }
                catch (Exception ex)
                {
                    result = EventLogger.LogEvent(ex);
                }
            }
            else
                result = "Invalid permission id";
            return result;
        }

        /// <summary>
        /// Deletes the permission.
        /// </summary>
        /// <param name="permissionId">The permission identifier.</param>
        /// <param name="franchiseId">The franchise identifier.</param>
        /// <returns>System.String.</returns>
        public static string DeletePermission(int permissionId, int franchiseId)
        {
            if (franchiseId <= 0)
                return "Invalid franchise";
            if (permissionId <= 0)
                return "Invalid permission Id";
                        
            try
            {
                var perm = ContextFactory.Current.Context.Permissions.FirstOrDefault(f => f.FranchiseId == franchiseId && f.PermissionId == permissionId);
                if (perm == null) return "Permission not found for your franchise";

                ContextFactory.Current.Context.Permissions.Remove(perm);
                ContextFactory.Current.Context.SaveChanges();
                return "Success";
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        /// <summary>
        /// Creates the permission.
        /// </summary>
        /// <param name="roleIds">The role ids.</param>
        /// <param name="userIds">The user ids.</param>
        /// <param name="permissionTypeId">The permission type identifier.</param>
        /// <param name="canCreate">if set to <c>true</c> [can create].</param>
        /// <param name="canRead">if set to <c>true</c> [can read].</param>
        /// <param name="canUpdate">if set to <c>true</c> [can update].</param>
        /// <param name="canDelete">if set to <c>true</c> [can delete].</param>
        /// <param name="franchiseId">The franchise identifier.</param>
        /// <returns>System.String.</returns>
        public static string CreatePermission(List<Guid> roleIds, List<int> userIds, int permissionTypeId, bool canCreate, bool canRead, bool canUpdate, bool canDelete, int? franchiseId)
        {
            if (permissionTypeId <= 0)
                return "Invalid permission Id";

            var permType = CacheManager.PermissionTypeCollection.FirstOrDefault(f => f.PermissionTypeId == permissionTypeId);
            if (permType == null)
                return "Invalid permission type";

            if (permType.IsSpecialPermission || (roleIds != null && roleIds.Count > 0) || (userIds != null && userIds.Count > 0))
            {
                if (canCreate == false && canRead == false && canUpdate == false && canDelete == false)
                    return "At least one CRUD access type must be granted";
                                
                //if any of these 3 are granted, then read must also be granted
                if (canCreate || canUpdate || canDelete)
                    canRead = true;

                try
                {
                    if (permType.IsSpecialPermission)
                    {
                        var currentPermission = (from p in ContextFactory.Current.Context.Permissions
                            where p.PermissionTypeId == permissionTypeId && ((p.FranchiseId == franchiseId) || (!p.FranchiseId.HasValue && !franchiseId.HasValue))
                            orderby p.FranchiseId descending
                            select new { p.PermissionId, p.FranchiseId }).FirstOrDefault();
                        if (currentPermission != null && currentPermission.FranchiseId == franchiseId) return "Unable to create, duplicate permission found";
                        else
                        {
                            var perm = new Permission
                                       {
                                           PermissionTypeId = permissionTypeId,
                                           FranchiseId = franchiseId,
                                           CanCreate = canCreate,
                                           CanRead = canRead,
                                           CanUpdate = canUpdate,
                                           CanDelete = canDelete,
                                           CreatedOnUtc = DateTime.Now
                                       };
                            ContextFactory.Current.Context.Permissions.Add(perm);
                        }
                    }
                    else
                    {
                        if (roleIds != null)
                        {
                            var exist = (from r in roleIds
                                join p in ContextFactory.Current.Context.Permissions on r equals p.RoleId
                                where p.PermissionTypeId == permissionTypeId && ((p.FranchiseId == franchiseId) || (!p.FranchiseId.HasValue && !franchiseId.HasValue))
                                select p.RoleId.Value).ToList();
                            var ids = roleIds.Except(exist).ToList();
                            if (ids.Count == 0) return "Unable to create, duplicate permission found";

                            foreach (var id in ids)
                            {
                                var perm = new Permission()
                                           {
                                               PermissionTypeId = permissionTypeId,
                                               FranchiseId = franchiseId,
                                               CanCreate = canCreate,
                                               CanRead = canRead,
                                               CanUpdate = canUpdate,
                                               CanDelete = canDelete,
                                               CreatedOnUtc = DateTime.Now,
                                               RoleId = id
                                           };
                                ContextFactory.Current.Context.Permissions.Add(perm);
                            }
                        }
                        if (userIds != null)
                        {
                            var exist = (from u in userIds
                                join p in ContextFactory.Current.Context.Permissions on u equals p.PersonId
                                where p.PermissionTypeId == permissionTypeId && ((p.FranchiseId == franchiseId) || (!p.FranchiseId.HasValue && !franchiseId.HasValue))
                                select p.PersonId.Value).ToList();
                            var ids = userIds.Except(exist).ToList();
                            if (ids.Count == 0) return "Unable to create, duplicate permission found";
                            foreach (var id in ids)
                            {
                                var perm = new Permission()
                                           {
                                               PermissionTypeId = permissionTypeId,
                                               FranchiseId = franchiseId,
                                               CanCreate = canCreate,
                                               CanRead = canRead,
                                               CanUpdate = canUpdate,
                                               CanDelete = canDelete,
                                               CreatedOnUtc = DateTime.Now,
                                               PersonId = id
                                           };
                                ContextFactory.Current.Context.Permissions.Add(perm);
                            }
                        }
                    }

                    ContextFactory.Current.Context.SaveChanges();

                    CacheManager.PermissionCollection = null; //this will clear cache so it can be refreshed
                    return "Success";
                }
                catch (Exception ex)
                {
                    return EventLogger.LogEvent(ex);
                }
                
            }
            else
                return "Permission needs to be assigned to a user or role";

        }
    }
}
