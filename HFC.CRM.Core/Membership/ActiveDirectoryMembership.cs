﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.DirectoryServices;
using System.Web.Security;
using System.Security.Principal;
using System.Runtime.InteropServices;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using System.DirectoryServices.AccountManagement;

/// <summary>
/// The Membership namespace.
/// </summary>
namespace HFC.CRM.Core.Membership
{
    /// <summary>
    /// Class ActiveDirectoryMembership.
    /// </summary>
    public class ActiveDirectoryMembership
    {
        /// <summary>
        /// Enum UserAccountControlFlags
        /// </summary>
        [Flags]
        private enum UserAccountControlFlags
        {
            /// <summary>
            /// The script
            /// </summary>
            SCRIPT = 0x0001,
            /// <summary>
            /// The accountdisable
            /// </summary>
            ACCOUNTDISABLE = 0x0002,
            /// <summary>
            /// The homedi r_ required
            /// </summary>
            HOMEDIR_REQUIRED = 0x0008,
            /// <summary>
            /// The lockout
            /// </summary>
            LOCKOUT = 0x0010,
            /// <summary>
            /// The passw d_ notreqd
            /// </summary>
            PASSWD_NOTREQD = 0x0020,
            /// <summary>
            /// The passw d_ can t_ change
            /// </summary>
            PASSWD_CANT_CHANGE = 0x0040,
            /// <summary>
            /// The encrypte d_ tex t_ pw d_ allowed
            /// </summary>
            ENCRYPTED_TEXT_PWD_ALLOWED = 0x0080,
            /// <summary>
            /// The tem p_ duplicat e_ account
            /// </summary>
            TEMP_DUPLICATE_ACCOUNT = 0x0100,
            /// <summary>
            /// The norma l_ account
            /// </summary>
            NORMAL_ACCOUNT = 0x0200,
            /// <summary>
            /// The interdomai n_ trus t_ account
            /// </summary>
            INTERDOMAIN_TRUST_ACCOUNT = 0x0800,
            /// <summary>
            /// The workstatio n_ trus t_ account
            /// </summary>
            WORKSTATION_TRUST_ACCOUNT = 0x1000,
            /// <summary>
            /// The serve r_ trus t_ account
            /// </summary>
            SERVER_TRUST_ACCOUNT = 0x2000,
            /// <summary>
            /// The don t_ expir e_ password
            /// </summary>
            DONT_EXPIRE_PASSWORD = 0x10000,
            /// <summary>
            /// The mn s_ logo n_ account
            /// </summary>
            MNS_LOGON_ACCOUNT = 0x20000,
            /// <summary>
            /// The smartcar d_ required
            /// </summary>
            SMARTCARD_REQUIRED = 0x40000,
            /// <summary>
            /// The truste d_ fo r_ delegation
            /// </summary>
            TRUSTED_FOR_DELEGATION = 0x80000,
            /// <summary>
            /// The no t_ delegated
            /// </summary>
            NOT_DELEGATED = 0x100000,
            /// <summary>
            /// The us e_ de s_ ke y_ only
            /// </summary>
            USE_DES_KEY_ONLY = 0x200000,
            /// <summary>
            /// The don t_ re q_ preauth
            /// </summary>
            DONT_REQ_PREAUTH = 0x400000,
            /// <summary>
            /// The passwor d_ expired
            /// </summary>
            PASSWORD_EXPIRED = 0x800000,
            /// <summary>
            /// The truste d_ t o_ aut h_ fo r_ delegation
            /// </summary>
            TRUSTED_TO_AUTH_FOR_DELEGATION = 0x1000000
        }

        /// <summary>
        /// The _path
        /// </summary>
        private string _path = AppConfigManager.GetConfig<string>("DomainPath", "Active Directory", "Authentication");
        /// <summary>
        /// The _domain
        /// </summary>
        private string _domain = AppConfigManager.GetConfig<string>("DomainName", "Active Directory", "Authentication");
        //private string _filterAttribute = null;
        //private DirectoryEntry _dirEntry;
        //private DirectorySearcher _dirSearcher;
        /// <summary>
        /// The _username
        /// </summary>
        private string _username;
        /// <summary>
        /// The _password
        /// </summary>
        private string _password;
        //private string _franchiseCode;
        //private string firstName, lastName, email;
        /// <summary>
        /// The _ is authenticated
        /// </summary>
        private bool _IsAuthenticated = false;
        /// <summary>
        /// The is use impersonation
        /// </summary>
        private bool IsUseImpersonation = !string.IsNullOrEmpty(AppConfigManager.GetConfig<string>("ImpersonateUsername", "Active Directory", "Authentication"));

        /// <summary>
        /// Gets the domain path.
        /// </summary>
        /// <value>The domain path.</value>
        public string DomainPath { get { return _path; } }
        /// <summary>
        /// Gets the domain.
        /// </summary>
        /// <value>The domain.</value>
        public string Domain { get { return _domain; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="ActiveDirectoryMembership"/> class.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="domain">The domain.</param>
        /// <param name="username">The username.</param>
        /// <param name="pwd">The password.</param>
        public ActiveDirectoryMembership(string path, string domain, string username, string pwd)
        {
            _path = path;
            _username = username;
            _password = pwd;
            _domain = domain;
            //Init();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ActiveDirectoryMembership"/> class.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="pwd">The password.</param>
        public ActiveDirectoryMembership(string username, string pwd)
        {
            //parse full username to exclude domain if it is supplied
            if (!string.IsNullOrEmpty(username) && username.IndexOf('@') > 0)
                _username = username.Substring(0, username.IndexOf('@'));
            else
                _username = username;
            _password = pwd;

            //Init();
        }

        //Uncomment this to use search to retrieve user properties
        //private void Init()
        //{
        //    string domainAndUsername = _domain + @"\" + _username;
        //    _dirEntry = new DirectoryEntry(_path, domainAndUsername, _password);
        //    _dirSearcher = new DirectorySearcher(_dirEntry);
        //}

        /// <summary>
        /// Determines whether this instance is authenticated.
        /// </summary>
        /// <returns><c>true</c> if this instance is authenticated; otherwise, <c>false</c>.</returns>
        public bool IsAuthenticated()
        {
            bool isValid = false;
            try
            {
                if (impersonateValidUser())
                {
                    //If guest account is enabled in AD, this can return true for non-existant user so this method is less secure
                    //so we need to use FindByIdentity to look up user first and only validate if they exist
                    using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, Domain, _username, _password))
                    {
                        var usr = UserPrincipal.FindByIdentity(pc, _username);

                        ////TODO: Get GUID FROM usr
                        //var user = UserPrincipal.FindByIdentity(pc, _username);
                        if (usr != null)
                            _IsAuthenticated = isValid = pc.ValidateCredentials(_username, _password);
                    }
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex, username: _username);
            }
            finally
            {
                if (impersonationContext != null)
                    impersonationContext.Undo();
            }

            return isValid;
        }

        public bool IsValidUser()
        {
            bool isValid = false;
            try
            {
                if (impersonateValidUser())
                {
                    using (var domainContext = new PrincipalContext(ContextType.Domain, Domain))
                    {
                        using (UserPrincipal foundUser = UserPrincipal.FindByIdentity(domainContext, IdentityType.SamAccountName, _username))
                        {
                            isValid = (!string.IsNullOrEmpty(foundUser.SamAccountName)) ? true : false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex, username: _username);
            }
            finally
            {
                if (impersonationContext != null)
                    impersonationContext.Undo();
            }

            return isValid;
        }

        //public List<string> GetGroups()
        //{
        //    //DirectorySearcher search = new DirectorySearcher(_path);
        //    _dirSearcher.Filter = "(cn=" + _filterAttribute + ")";
        //    _dirSearcher.PropertiesToLoad.Clear();
        //    _dirSearcher.PropertiesToLoad.Add("memberOf");
        //    List<string> groupNames = new List<string>();

        //    try
        //    {
        //        SearchResult result = _dirSearcher.FindOne();
        //        int propertyCount = result.Properties["memberOf"].Count;
        //        string dn;
        //        int equalsIndex, commaIndex;

        //        for (int propertyCounter = 0; propertyCounter < propertyCount; propertyCounter++)
        //        {
        //            dn = (string)result.Properties["memberOf"][propertyCounter];
        //            equalsIndex = dn.IndexOf("=", 1);
        //            commaIndex = dn.IndexOf(",", 1);
        //            if (-1 == equalsIndex)
        //            {
        //                return null;
        //            }
        //            groupNames.Add(dn.Substring((equalsIndex + 1), (commaIndex - equalsIndex) - 1));                    
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception("Error obtaining group names. " + ex.Message);
        //    }
        //    return groupNames;
        //}

        //[Obsolete]
        //public User CreateUser()
        //{
        //    User newUser = null;

        //    if (_IsAuthenticated)
        //    {
        //        try
        //        {
        //            using (var db = new CRM.Data.CRMContext())
        //            {
        //                string HashedPassword = Crypto.HashPassword(_password);

        //                MembershipCreateStatus status;

        //                int franchiseId = 1; //default all user to corporate franchise?
        //                var franchise = db.Franchises.FirstOrDefault(f => f.Code == _franchiseCode);
        //                if (franchise != null)
        //                    franchiseId = franchise.FranchiseId; 

        //                //add domain to username to store in db
        //                string fullusername = _username;
        //                if (fullusername.IndexOf('@') <= 0 && !string.IsNullOrEmpty(AppConfigManager.ADDomainName))
        //                    fullusername = string.Format("{0}@{1}", fullusername, AppConfigManager.ADDomainName);

        //                var groups = GetGroups();

        //                //match any known role and add it in system
        //                var roleList = (from g in GetGroups()
        //                                join r in CacheManager.RoleCollection on g.ToLower() equals r.Role.RoleName.ToLower()
        //                                join mapRole in CacheManager.RoleCollection on r.Role.MappedToRoleId equals mapRole.RoleId into secRole
        //                                from sec in secRole.DefaultIfEmpty()
        //                                select sec == null ? r.Role.RoleId : sec.Role.RoleId
        //                                ).Distinct().ToList();

        //                newUser = LocalMembership.CreateUser(franchiseId, fullusername, firstName, lastName, HashedPassword,
        //                    email, out status, true, roleList, _domain, _path, "System Created via Active Directory");
        //            }
        //        }
        //        catch (Exception ex) { EventLogger.LogEvent(ex); }
        //    }

        //    return newUser;
        //}

        #region Impersonation Code

        /// <summary>
        /// Enum LogonSessionType
        /// </summary>
        enum LogonSessionType : uint
        {
            /// <summary>
            /// The interactive
            /// </summary>
            Interactive = 2,
            /// <summary>
            /// The network
            /// </summary>
            Network,
            /// <summary>
            /// The batch
            /// </summary>
            Batch,
            /// <summary>
            /// The service
            /// </summary>
            Service,
            /// <summary>
            /// The network cleartext
            /// </summary>
            NetworkCleartext = 8,
            /// <summary>
            /// The new credentials
            /// </summary>
            NewCredentials
        }
        /// <summary>
        /// Enum LogonProvider
        /// </summary>
        enum LogonProvider : uint
        {
            /// <summary>
            /// The default
            /// </summary>
            Default = 0, // default for platform (use this!)
            /// <summary>
            /// The win n T35
            /// </summary>
            WinNT35,     // sends smoke signals to authority
            /// <summary>
            /// The win n T40
            /// </summary>
            WinNT40,     // uses NTLM
            /// <summary>
            /// The win n T50
            /// </summary>
            WinNT50      // negotiates Kerb or NTLM
        }

        /// <summary>
        /// The impersonation context
        /// </summary>
        WindowsImpersonationContext impersonationContext;

        /// <summary>
        /// Logons the user.
        /// </summary>
        /// <param name="principal">The principal.</param>
        /// <param name="authority">The authority.</param>
        /// <param name="password">The password.</param>
        /// <param name="logonType">Type of the logon.</param>
        /// <param name="logonProvider">The logon provider.</param>
        /// <param name="token">The token.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        [DllImport("advapi32.dll", SetLastError = true)]
        static extern bool LogonUser(
          string principal,
          string authority,
          string password,
          LogonSessionType logonType,
          LogonProvider logonProvider,
          out IntPtr token);
        /// <summary>
        /// Closes the handle.
        /// </summary>
        /// <param name="handle">The handle.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool CloseHandle(IntPtr handle);

        /// <summary>
        /// Impersonates the valid user.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="domain">The domain.</param>
        /// <param name="password">The password.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        private bool impersonateValidUser(String userName, String domain, String password)
        {
            WindowsIdentity tempWindowsIdentity;
            IntPtr token = IntPtr.Zero;

            bool result = LogonUser(userName, domain, password,
                                    LogonSessionType.NewCredentials,
                                    //LogonSessionType.Network,
                                    LogonProvider.WinNT50,
                                    out token);
            if (result)
            {
                tempWindowsIdentity = new WindowsIdentity(token);
                impersonationContext = tempWindowsIdentity.Impersonate();
            }
            return result;
        }

        /// <summary>
        /// Impersonates the valid user.
        /// </summary>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        private bool impersonateValidUser()
        {
            //not using impersonation so return true
            if (IsUseImpersonation == false)
                return true;
            else //otherwise impersonate a user to validate AD user
                return impersonateValidUser(AppConfigManager.GetConfig<string>("ImpersonateUsername", "Active Directory", "Authentication"), _domain, AppConfigManager.GetConfig<string>("ImpersonateUserPassword", "Active Directory", "Authentication"));
        }
        #endregion

    }
}

