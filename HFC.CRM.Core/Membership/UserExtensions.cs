﻿using System.Data.Entity;
using System.Linq;
using HFC.CRM.Core.Common;
using HFC.CRM.Data;

namespace HFC.CRM.Core.Membership
{
    public static class UserExtensions
    {
        public static bool IsWidgetEnabled(this User user, int franchiseId, string widgetName)
        {
            var widgetType = CacheManager.WidgetTypes.FirstOrDefault(w => w.Name == widgetName);

            if (widgetType == null)
            {
                return false;
            }

            if (user.UserWidgets.Any(w => w.FranchiseId == franchiseId && w.Type_Widgets.Name == widgetName && w.Show == false))
            {
                return false;
            }

            if (user.UserWidgets.Any(w => w.FranchiseId == franchiseId && w.Type_Widgets.Name == widgetName && w.Show == true)) // if user has permission to see this widget
            { 
                return true;
            }

            using (var db = new Data.CRMContextEx())
            {
              
                foreach (var role in user.Roles)
                {

                    if (!role.UserWidgets.Any())
                    {
                        if (db.UserWidgets.Count(v => v.RoleId == role.RoleId && v.FranchiseId == franchiseId) > 0)
                        {
                            role.UserWidgets =
                                db.UserWidgets.Where(v => v.RoleId == role.RoleId && v.FranchiseId == franchiseId).Include(v=>v.Type_Widgets)
                                    .ToList();
                        }
                        else
                        {
                            
                                role.UserWidgets =
                                    db.UserWidgets.Where(v => v.RoleId == role.RoleId && v.FranchiseId == null).Include(v => v.Type_Widgets)
                                        .ToList();
                        }
                    }
                }
            }


            //user.Roles.
            
            if (user.Roles.Any(r => r.IsWidgetEnabled(franchiseId, widgetName))) // if user has any rol that has permission to see this widget
            {
                return true;
            }
            
            return false;
        }
    }
}