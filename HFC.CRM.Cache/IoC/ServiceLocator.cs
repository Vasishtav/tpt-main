﻿using Microsoft.Practices.Unity;

namespace HFC.CRM.Cache.IoC
{
    public static class ServiceLocator
    {
        private static UnityContainer container;
        public static void Init(UnityContainer unityContainer)
        {
            container = unityContainer;
        }

        public static T Resolve<T>()
        {
            return container.Resolve<T>();
        }
    }
}