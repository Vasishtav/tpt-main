﻿namespace HFC.CRM.Cache.Push
{
    public interface IPushService
    {
        void HandleEntryChanges<T>(T entry);
    }
}