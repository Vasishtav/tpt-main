﻿using System;
using HFC.CRM.Cache.Cache;
using System.Linq;
using Microsoft.AspNet.SignalR.Hubs;

namespace HFC.CRM.Cache.Push
{
    public class PushService : IPushService
    {
        private readonly IHubConnectionContext<dynamic> hubConnectionContext;

        public PushService(IHubConnectionContext<dynamic> hubConnectionContext)
        {
            this.hubConnectionContext = hubConnectionContext;
        }

        public void HandleEntryChanges<T>(T entry)
        {
            if (entry.GetType().GetInterfaces().Contains(typeof (IHasFranchiseId)))
            {
                var casted = entry as IHasFranchiseId;
                if (casted != null)
                {
                    this.hubConnectionContext.Group(FranchiseToGroupName(casted.FranchiseId)).entryUpdated(TypeToLookupName(entry.GetType()));
                }
            }
            else
            {
                this.hubConnectionContext.All.entryUpdated(TypeToLookupName(entry.GetType()));
            }
        }


        private string TypeToLookupName(Type type)
        {
            if (type.Name == "Type_LeadStatus")
            {
                return "leadstatus";
            }
            else if (type.Name == "Type_OrderStatus")
            {
                return "orderstatus";
            }
            else if (type.Name == "Type_Source")
            {
                return "source";
            }
            else if (type.Name == "Type_JobStatus")
            {
                return "jobstatus";
            }
            else if (type.Name == "Type_Concept")
            {
                return "concept";
            }
            else if (type.Name == "Source")
            {
                return "source";
            }

            return "";
        }

        private static string FranchiseToGroupName(int? franchiseId)
        {
            return string.Format("group_of_{0}_connections", franchiseId);
        }
    }
}