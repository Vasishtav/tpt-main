﻿namespace HFC.CRM.Cache.Cache
{
    public interface IChangeTracker
    {
        void EntityInserted<T>(T entity);
        void EntityUpdated<T>(T entity);
    }
}