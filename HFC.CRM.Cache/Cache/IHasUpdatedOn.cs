﻿using System;

namespace HFC.CRM.Cache.Cache
{
    public interface IHasUpdatedOn
    {
        DateTime? UpdatedOn { get; set; }
    }
}