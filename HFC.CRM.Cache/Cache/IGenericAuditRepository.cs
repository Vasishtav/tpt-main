﻿using System.Collections.Generic;

namespace HFC.CRM.Cache.Cache
{
    public interface IGenericAuditRepository<T> where T : IHasAudit
    {
        IEnumerable<T> GetAdded(long version, int? franchiseId);
        IEnumerable<T> GetUpdated(long version, int? franchiseId);
        long GetVersion(int? franchiseId);
    }
}