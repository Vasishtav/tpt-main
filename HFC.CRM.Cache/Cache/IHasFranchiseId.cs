﻿using System;

namespace HFC.CRM.Cache.Cache
{
    public interface IHasFranchiseId
    {
        Nullable<int> FranchiseId { get; set; }
    }
}