﻿using HFC.CRM.Cache.Push;
using System.Linq;

namespace HFC.CRM.Cache.Cache
{
    public class ChangeTracker: IChangeTracker
    {
        private readonly IPushService pushService;

        public ChangeTracker(IPushService pushService)
        {
            this.pushService = pushService;
        }

        public void EntityInserted<T>(T entity)
        {
            if (entity.GetType().GetInterfaces().Contains(typeof(ICacheable)))
            {
                this.pushService.HandleEntryChanges(entity);
            }
        }

        public void EntityUpdated<T>(T entity)
        {
            if (entity.GetType().GetInterfaces().Contains(typeof (ICacheable)))
            {
                this.pushService.HandleEntryChanges(entity);
            }
        }
    }
}