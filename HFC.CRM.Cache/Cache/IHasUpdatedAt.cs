﻿using System;

namespace HFC.CRM.Cache.Cache
{
    public interface IHasUpdatedAt
    {
        DateTime? UpdatedAt { get; set; }
    }
}