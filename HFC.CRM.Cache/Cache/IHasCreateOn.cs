﻿using System;

namespace HFC.CRM.Cache.Cache
{
    public interface IHasCreatedAt
    {
        DateTime CreatedAt { get; set; }
    }
}