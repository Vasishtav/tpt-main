﻿namespace HFC.CRM.Cache.Cache
{
    public interface IHasAudit : IHasCreatedAt, IHasUpdatedAt
    {
         
    }
}