//------------------------------------------------------------------------------
// <copyright file="CSSqlStoredProcedure.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Net;
using System.IO;

public partial class REST
{
    [Microsoft.SqlServer.Server.SqlProcedure(Name="REST_Get")]
    public static void Get (SqlString url, out SqlString retVal)
    {
        string sUrl = Convert.ToString(url);
        string feedData = string.Empty;

        try
        {

            HttpWebRequest request = null;
            HttpWebResponse response = null;
            Stream stream = null;
            StreamReader streamReader = null;

            request = (HttpWebRequest)WebRequest.Create(sUrl);
            request.Method = "GET"; // you have to change to
            //PUT/POST/GET/DELETE based on your scenerio…
            request.ContentLength = 0;
            response = (HttpWebResponse)request.GetResponse();
            stream = response.GetResponseStream();
            streamReader = new StreamReader(stream);
            feedData = streamReader.ReadToEnd();

            response.Close();
            stream.Dispose();
            streamReader.Dispose();
        }
        catch (Exception ex)
        {
            SqlContext.Pipe.Send(ex.Message.ToString());
        }

        retVal = feedData;
    }
}
