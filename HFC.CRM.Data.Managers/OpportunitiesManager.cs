﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using System.Web;
using System.Xml.Linq;
using HFC.CRM.Core;
using System.IO;
using HFC.CRM.Data;
using System.Text.RegularExpressions;
using Dapper;
using HFC.CRM.Data.Context;
using SaveOptions = System.Xml.Linq.SaveOptions;
using System.Threading.Tasks;
using HFC.CRM.DTO.Opportunity;
using StackExchange.Profiling;
using System.Data.SqlClient;
using HFC.CRM.DTO.Person;
using HFC.CRM.DTO.Address;
using HFC.CRM.DTO.Notes;
using HFC.CRM.DTO.Search;
using System.Data.Entity.Core.EntityClient;
using HFC.CRM.Managers.Exceptions;
using Newtonsoft.Json;

namespace HFC.CRM.Managers
{

    /// <summary>
    /// Class OpportunitiesManager.
    /// </summary>
    public class OpportunitiesManager : DataManager<Opportunity>
    {
        private LeadsManager LeadsMgr = new LeadsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        public OpportunitiesManager(User user, Franchise franchise) : this(user, user, franchise)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OpportunitiesManager"/> class.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="franchise">The franchise.</param>
        public OpportunitiesManager(User user, User authorizingUser, Franchise franchise)
        {
            this.User = user;
            this.AuthorizingUser = authorizingUser;
            this.Franchise = franchise;
        }

        private FranchiseManager _franMgr = new FranchiseManager(SessionManager.CurrentUser);
        private StatusChangesManager statusChangesMgr = new StatusChangesManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
        private EntityConnectionStringBuilder EntityCnxStringBuilder = new EntityConnectionStringBuilder
            (System.Configuration.ConfigurationManager.ConnectionStrings["CRMContext"].ConnectionString);

        #region Activity & History Log Methods

        /// <summary>
        /// Help parse XML to figure out what type of operation and form the message
        /// </summary>
        /// <param name="historyValue">The history value.</param>
        /// <returns>System.String.</returns>
        public static string ParseMessage(string historyValue)
        {
            var result = "Opportunity Data Changed";
            if (!string.IsNullOrEmpty(historyValue))
            {
                try
                {
                    StringBuilder message = new StringBuilder();
                    XElement historyXml = XElement.Parse(historyValue);

                    //var nameAtr = historyXml.Attribute("name");
                    //string name = nameAtr != null ? nameAtr.Value : historyXml.Name.LocalName;
                    //message.AppendFormat("{0}:\n", name);
                    if (historyXml.HasElements)
                    {
                        //parent operation
                        var operation = historyXml.Attribute("operation");
                        string opValue = "";
                        if (operation != null)
                        {
                            switch (operation.Value)
                            {
                                case "insert":
                                    opValue = "Added";
                                    break;

                                case "delete":
                                    opValue = "Deleted";
                                    break;

                                case "update":
                                    opValue = "Changed";
                                    break;

                                case "information":
                                    opValue = "";
                                    break;

                                default:
                                    break;
                            }
                        }
                        message.AppendLine((historyXml.Name + " " + opValue).Trim() + ":");
                        var elements = historyXml.Elements();
                        int i = 0;
                        foreach (var elem in elements)
                        {
                            CreateMessage(elem, ref message, operation);
                            if (elements.Count() > 1 && i < elements.Count())
                                message.Append("<br/>");
                            i++;
                        }
                    }
                    else
                    {
                        var operation = historyXml.Attribute("operation");
                        if (operation != null && operation.Value == "insert")
                            message.AppendFormat("{0} Created", historyXml.Name);
                        else if (operation != null && operation.Value == "delete")
                            message.AppendFormat("{0} Deleted", historyXml.Name);
                        else if (operation != null && operation.Value == "information")
                        {
                            message.AppendFormat(historyXml.Value);
                        }
                        else
                            message.AppendFormat("{0} Data Changed", historyXml.Name);
                    }

                    result = message.Length > 0
                        ? message.ToString()
                        : string.Format("Values Changed: <br/>{0}", historyXml.Value);
                }
                catch
                {
                }
            }

            return result;
        }

        /// <summary>
        /// Creates the message for Activity Log from XML
        /// </summary>
        /// <param name="child">The child.</param>
        /// <param name="message">The message.</param>
        /// <param name="operation">The operation.</param>
        public static void CreateMessage(XElement child, ref StringBuilder message, XAttribute operation = null)
        {
            XAttribute childOperation = null;
            if (operation == null)
                childOperation = child.Attribute("operation");

            var current = child.Element("Current");
            var original = child.Element("Original");
            var nameAtr = child.Attribute("name");
            string name = nameAtr != null ? nameAtr.Value : child.Name.LocalName;

            if ((childOperation != null && childOperation.Value.Equals("insert", StringComparison.OrdinalIgnoreCase)) ||
                (operation != null && operation.Value.Equals("insert", StringComparison.OrdinalIgnoreCase)))
            {
                if (current != null)
                {
                    if (operation != null)
                        message.AppendFormat("{0}: {1}", name, current.Value);
                    else
                        message.AppendFormat("{0} Added: {1}", name, current.Value);
                }
                else
                    message.AppendFormat("{0} Added", name);
            }
            else if ((childOperation != null && childOperation.Value.Equals("delete", StringComparison.OrdinalIgnoreCase)) ||
                     (operation != null && operation.Value.Equals("delete", StringComparison.OrdinalIgnoreCase)))
            {
                if (original != null)
                {
                    if (operation != null)
                        message.AppendFormat("{0}: {1}", name, original.Value);
                    else
                        message.AppendFormat("{0} Deleted: {1}", name, original.Value);
                }
                else
                    message.AppendFormat("{0} Deleted", name);
            }
            else
            {
                if (current != null && original != null)
                    message.AppendFormat("{0} changed from '{1}' to '{2}'", name, original.Value, current.Value);
                else if (current != null)
                    message.AppendFormat("{0} changed to {1}", name, current.Value);
                else
                    message.AppendFormat("{0} Changed", name);
            }
        }

        #endregion Activity & History Log Methods

        public static List<Type_OpportunityStatus> OpportunityStatusTypeCollection
        {
            get
            {
                return CacheManager.OpportunityStatusTypeCollection
                    .Where(w => !w.FranchiseId.HasValue || w.FranchiseId == SessionManager.CurrentFranchise.FranchiseId)
                    .ToList();
            }
        }

        /// <summary>
        /// Gets a list of Opportunity data by status Ids and created on date
        /// </summary>
        /// <param name="totalRecords">The total records.</param>
        /// <param name="opportunityStatusIds">List of status Ids to match</param>
        /// <param name="createdOnUtcStart">The created on UTC start.</param>
        /// <param name="createdOnUtcEnd">The created on UTC end.</param>
        /// <param name="searchFilter">The search filter.</param>
        /// <param name="searchTerm">The search term.</param>
        /// <param name="orderBy">The order by.</param>
        /// <param name="orderByDirection">The order by direction.</param>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="excludeOpportunityStatusNames"></param>
        /// <returns>List&lt;Opportunity&gt;.</returns>
        /// <exception cref="System.AccessViolationException">
        /// </exception>
        public List<Opportunity> Get(
            out int totalRecords,
            List<int> opportunityStatusIds = null,
            DateTime? createdOnUtcStart = null,
            DateTime? createdOnUtcEnd = null,
            SearchFilterEnum searchFilter = SearchFilterEnum.Auto_Detect,
            string searchTerm = null,
            string orderBy = null,
            OrderByEnum orderByDirection = OrderByEnum.Desc,
            int pageIndex = 0,
            int pageSize = 25,
            string[] excludeOpportunityStatusNames = null)
        {
            var result = new List<Opportunity>();
            totalRecords = 0;

            //if (!BasePermission.CanRead) return result;

            try
            {
                if (string.IsNullOrEmpty(orderBy))
                    orderBy = "CreatedOnUtc"; //set default to createdon date

                var opportunityIds = new List<int>();

                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var query = @"select l.* from crm.Opportunities l
                                where l.FranchiseId = @franchiseId and l.IsDeleted = 0";

                    if (createdOnUtcStart.HasValue)
                        query += @" and l.CreatedOnUtc >= @createdOnUtcStart ";
                    if (createdOnUtcEnd.HasValue)
                        query += @" and <=  @createdOnUtcEnd";
                }

                var linq = from l in CRMDBContext.Opportunities
                       .Include(i => i.Jobs)
                       .Include(i => i.OpportunityStatus)
                       .AsNoTracking()
                           where l.FranchiseId == Franchise.FranchiseId
                               && l.IsDeleted == false
                           select l;

                if (createdOnUtcStart.HasValue)
                    linq = linq.Where(l => DbFunctions.TruncateTime(l.CreatedOnUtc) >= createdOnUtcStart);

                if (createdOnUtcEnd.HasValue)
                    linq = linq.Where(l => DbFunctions.TruncateTime(l.CreatedOnUtc) <= createdOnUtcEnd);

                if (opportunityStatusIds != null && opportunityStatusIds.Count > 0)
                {
                    var ids = opportunityStatusIds.Cast<int?>().ToList();
                    linq = linq
                        .Where(
                            i => i.FranchiseId == SessionManager.CurrentFranchise.FranchiseId || !i.FranchiseId.HasValue)
                        .Where(w => ids.Contains(w.OpportunityStatusId) || ids.Contains(w.OpportunityStatus.OpportunityStatusId));
                }

                if (excludeOpportunityStatusNames != null)
                {
                    var excludeOpportunityStatuseIds =
                        CacheManager.OpportunityStatusTypeCollection.Where(
                            ls =>
                                (ls.FranchiseId == null || ls.FranchiseId == SessionManager.CurrentFranchise.FranchiseId) &&
                                excludeOpportunityStatusNames.Contains(ls.Name)).Select(ls => ls.OpportunityStatusId).ToList();
                    excludeOpportunityStatuseIds.AddRange(
                        CacheManager.OpportunityStatusTypeCollection.Where(
                            ls =>
                                (ls.FranchiseId == null || ls.FranchiseId == SessionManager.CurrentFranchise.FranchiseId) &&
                                excludeOpportunityStatuseIds.Any(es => es == ls.OpportunityStatusId)).Select(ls => ls.OpportunityStatusId).ToList());

                    if (excludeOpportunityStatuseIds.Any())
                    {
                        linq = linq.Where(l => excludeOpportunityStatuseIds.All(id => id != l.OpportunityStatusId));
                    }
                }

                if (!string.IsNullOrEmpty(searchTerm))
                {
                    // TODO: is this required in TP???
                    //if (!SearchPermission.CanRead)
                    //    throw new UnauthorizedAccessException(Unauthorized);

                    SearchFilterEnum realSearchFilter = searchFilter;
                    int anumber = 0;
                    Match JobNumMatch = null;
                    if (searchFilter == SearchFilterEnum.Auto_Detect)
                    {
                        //jobs that has starting then numbers hyphen and another number, ie: 2304-1 (meaning job number - quote number);
                        Regex jobQuoteExp = new Regex(@"^(\d+)-(\d+)$",
                            System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                        JobNumMatch = jobQuoteExp.Match(searchTerm);

                        if (searchFilter == SearchFilterEnum.Auto_Detect)
                        {
                            var isNumber = int.TryParse(searchTerm, out anumber);
                            var jobMatch = JobNumMatch != null && JobNumMatch.Success;
                            linq = from l in linq
                                   from a in l.Addresses
                                   from jobs in l.Jobs.DefaultIfEmpty()
                                   let p1 = l.PrimCustomer
                                   let p2 = l.SecCustomer

                                   where p1.FirstName.Contains(searchTerm) ||
                                         p1.LastName.Contains(searchTerm) ||
                                         (p1.FirstName + " " + p1.LastName).Contains(searchTerm) ||
                                         //p2.FirstName.Contains(searchTerm) ||
                                         //p2.LastName.Contains(searchTerm) ||
                                         //(p2.FirstName + " " + p2.LastName).Contains(searchTerm) ||
                                         p1.PrimaryEmail.Contains(searchTerm) ||
                                         p1.SecondaryEmail.Contains(searchTerm) ||
                                         //p2.PrimaryEmail.Contains(searchTerm) ||
                                         //p2.SecondaryEmail.Contains(searchTerm) ||
                                         a.Address1.Contains(searchTerm) ||
                                         a.Address2.Contains(searchTerm) ||
                                         a.City.Contains(searchTerm) ||
                                         a.State.Contains(searchTerm) ||
                                         a.ZipCode.Contains(searchTerm) ||
                                         a.AttentionText.Contains(searchTerm) ||
                                         p1.CompanyName.Contains(searchTerm) ||
                                         //p2.CompanyName.Contains(searchTerm) ||
                                         p1.HomePhone.Contains(searchTerm) ||
                                         p1.CellPhone.Contains(searchTerm) ||
                                         p1.WorkPhone.Contains(searchTerm) ||
                                         p1.FaxPhone.Contains(searchTerm) ||
                                         //p2.HomePhone.Contains(searchTerm) ||
                                         //p2.CellPhone.Contains(searchTerm) ||
                                         //p2.FaxPhone.Contains(searchTerm) ||
                                         //p2.WorkPhone.Contains(searchTerm) ||
                                         (isNumber
                                             ? (jobMatch
                                                 ? jobs.JobNumber == anumber
                                                 : l.OpportunityNumber == anumber || jobs.JobNumber == anumber)
                                             : false)

                                   select l;
                        }
                    }

                    switch (realSearchFilter)
                    {
                        case SearchFilterEnum.Address:
                            //TODO, this will not match if the searchterm is a mix of address1, city, state, etc.
                            linq = from l in linq
                                   from a in l.Addresses
                                   where a.Address1.StartsWith(searchTerm) ||
                                         a.Address2.StartsWith(searchTerm) ||
                                         a.City.StartsWith(searchTerm) ||
                                         a.State.StartsWith(searchTerm) ||
                                         a.ZipCode.StartsWith(searchTerm)
                                   select l;
                            break;

                        case SearchFilterEnum.Number:
                            if (int.TryParse(searchTerm, out anumber))
                            {
                                if (JobNumMatch != null && JobNumMatch.Success)
                                    linq = from l in linq from a in l.Jobs where a.JobNumber == anumber select l;
                                else
                                    linq = from l in linq
                                           from a in l.Jobs
                                           where l.OpportunityNumber == anumber || a.JobNumber == anumber
                                           select l;
                            }
                            break;

                        case SearchFilterEnum.Email:
                            linq = from l in linq
                                   let p1 = l.PrimCustomer
                                   let p2 = l.SecCustomer
                                   where p1.PrimaryEmail.Contains(searchTerm) ||
                                         p1.SecondaryEmail.Contains(searchTerm)
                                   //||
                                   //p2.PrimaryEmail.Contains(searchTerm) ||
                                   //p2.SecondaryEmail.Contains(searchTerm)
                                   select l;
                            break;

                        case SearchFilterEnum.Phone_Number:
                            string parsedPhone = RegexUtil.GetNumbersOnly(searchTerm);
                            linq = linq.Where(w =>
                                w.PrimCustomer.HomePhone == parsedPhone ||
                                w.PrimCustomer.CellPhone == parsedPhone ||
                                w.PrimCustomer.WorkPhone == parsedPhone ||
                                w.PrimCustomer.FaxPhone == parsedPhone);
                            //||
                            //w.SecCustomer.HomePhone == parsedPhone ||
                            //w.SecCustomer.CellPhone == parsedPhone ||
                            //w.SecCustomer.FaxPhone == parsedPhone ||
                            //w.SecCustomer.WorkPhone == parsedPhone);
                            break;

                        case SearchFilterEnum.Customer_Name:
                            linq = from l in linq
                                   let p1 = l.PrimCustomer
                                   let p2 = l.SecCustomer
                                   where p1.FirstName.StartsWith(searchTerm) ||
                                         p1.LastName.StartsWith(searchTerm) ||
                                         (p1.FirstName + " " + p1.LastName).Contains(searchTerm)
                                   //||
                                   //p2.FirstName.StartsWith(searchTerm) ||
                                   //p2.LastName.StartsWith(searchTerm) ||
                                   //(p2.FirstName + " " + p2.LastName).Contains(searchTerm)
                                   select l;
                            break;
                    }
                }

                totalRecords = linq.Select(s => s.OpportunityId).Count();

                if (totalRecords > 0)
                {
                    switch (orderBy.ToLower())
                    {
                        case "primperson":
                            linq = orderByDirection == OrderByEnum.Asc
                                ? linq.OrderBy(o => o.PrimCustomer.FirstName)
                                : linq.OrderByDescending(o => o.PrimCustomer.FirstName);
                            break;

                        case "opportunitynumber":
                            linq = orderByDirection == OrderByEnum.Asc
                                ? linq.OrderBy(o => o.OpportunityNumber)
                                : linq.OrderByDescending(o => o.OpportunityNumber);
                            break;

                        case "opportunitystatus":
                            linq = orderByDirection == OrderByEnum.Asc
                                ? linq.OrderBy(o => o.OpportunityStatus.Name)
                                : linq.OrderByDescending(o => o.OpportunityStatus.Name);
                            break;

                        case "addresses":
                            if (orderByDirection == OrderByEnum.Asc)
                                linq = from l in linq
                                       let a = l.Addresses.FirstOrDefault()
                                       orderby a.City ascending, a.State ascending, a.ZipCode ascending
                                       select l;
                            else
                                linq = from l in linq
                                       let a = l.Addresses.FirstOrDefault()
                                       orderby a.City descending, a.State descending, a.ZipCode descending
                                       select l;
                            break;

                        case "opportunitysources":
                            if (orderByDirection == OrderByEnum.Asc)
                                linq = from l in linq
                                       let s = l.OpportunitySources.FirstOrDefault()
                                       orderby s.Source.Name
                                       select l;
                            else
                                linq = from l in linq
                                       let s = l.OpportunitySources.FirstOrDefault()
                                       orderby s.Source.Name descending
                                       select l;
                            break;

                        default:
                            linq = orderByDirection == OrderByEnum.Asc
                                ? linq.OrderBy(o => o.CreatedOnUtc)
                                : linq.OrderByDescending(o => o.CreatedOnUtc);
                            break;
                    }

                    opportunityIds = linq.Select(s => s.OpportunityId).Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }

                result = Get(opportunityIds).ToList();
                if (result != null && result.Count > 0)
                {
                    //we need to re-sort again since there is no sql order by when we get the top 25 list
                    switch (orderBy.ToLower())
                    {
                        case "primperson":
                            result = orderByDirection == OrderByEnum.Asc
                                ? result.OrderBy(o => o.PrimCustomer != null ? o.PrimCustomer.FirstName : string.Empty)
                                    .ToList()
                                : result.OrderByDescending(
                                    o => o.PrimCustomer != null ? o.PrimCustomer.FirstName : string.Empty).ToList();
                            break;

                        case "opportunitynumber":
                            result = orderByDirection == OrderByEnum.Asc
                                ? result.OrderBy(o => o.OpportunityNumber).ToList()
                                : result.OrderByDescending(o => o.OpportunityNumber).ToList();
                            break;

                        case "opportunitystatus":
                            if (orderByDirection == OrderByEnum.Asc)
                                result = (from r in result
                                          join s in OpportunityStatusTypeCollection on r.OpportunityStatusId equals s.OpportunityStatusId
                                          orderby s.Name
                                          select r).ToList();
                            else
                                result = (from r in result
                                          join s in OpportunityStatusTypeCollection on r.OpportunityStatusId equals s.OpportunityStatusId
                                          orderby s.Name descending
                                          select r).ToList();
                            break;

                        case "addresses":
                            if (orderByDirection == OrderByEnum.Asc)
                                result = (from l in result
                                          let a = l.Addresses.FirstOrDefault()
                                          orderby a != null ? a.City : string.Empty ascending,
                                              a != null ? a.State : string.Empty ascending,
                                              a != null ? a.ZipCode : string.Empty ascending
                                          select l).ToList();
                            else
                                result = (from l in result
                                          let a = l.Addresses.FirstOrDefault()
                                          orderby a != null ? a.City : string.Empty descending,
                                              a != null ? a.State : string.Empty descending,
                                              a != null ? a.ZipCode : string.Empty descending
                                          select l).ToList();
                            break;

                        case "opportunitysources":
                            if (orderByDirection == OrderByEnum.Asc)
                                result = (from l in result
                                          let s = l.OpportunitySources.FirstOrDefault()
                                          orderby s != null && s.Source != null ? s.Source.Name : string.Empty
                                          select l).ToList();
                            else
                                result = (from l in result
                                          let s = l.OpportunitySources.FirstOrDefault()
                                          orderby s != null && s.Source != null ? s.Source.Name : string.Empty descending
                                          select l).ToList();
                            break;

                        default:
                            result = orderByDirection == OrderByEnum.Asc
                                ? result.OrderBy(o => o.CreatedOnUtc).ToList()
                                : result.OrderByDescending(o => o.CreatedOnUtc).ToList();
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }

            // filter opportunity's jobs
            foreach (var opportunity in result)
            {
                opportunity.Jobs = opportunity.Jobs.Where(j => j.IsDeleted != true).ToList();
            }

            return result;
        }

        public List<Opportunity> GetDuplicateOpportunitys(SearchDuplicateOpportunityEnum searchType, string searchTerm, Opportunity dto = null)
        {
            //
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                //var linq = from l in CRMDBContext.Opportunities.AsNoTracking()
                //where l.FranchiseId == Franchise.FranchiseId && l.IsDeleted == false
                //select l;
                var linq = "select l.*, lc.customerid, c.FirstName, c.LastName from crm.Opportunities l join crm.opportunitycustomers lc on lc.OpportunityId = l.OpportunityId join crm.Customer c on lc.customerid = c.CustomerId where l.FranchiseId = @franchiseId and lc.isprimarycustomer = 1";
                //var linq = connection.Query<Opportunity>(query, new { franchiseId = Franchise.FranchiseId });
                string parsedPhone = RegexUtil.GetNumbersOnly(searchTerm);

                switch (searchType)
                {
                    case SearchDuplicateOpportunityEnum.CellPhone:
                        linq += @"and c.CellPhone = @parsedPhone";
                        break;

                    case SearchDuplicateOpportunityEnum.HomePhone:
                        linq += @" and c.HomePhone = @parsedPhone";
                        break;

                    case SearchDuplicateOpportunityEnum.PrimaryEmail:
                        linq += @" and c.HomePhone = @parsedPhone";
                        break;

                    case SearchDuplicateOpportunityEnum.FaxPhone:
                        linq += @" and c.FaxPhone = @parsedPhone";
                        break;

                    case SearchDuplicateOpportunityEnum.WorkPhone:
                        linq += @" and c.WorkPhone = @parsedPhone";
                        break;

                    case SearchDuplicateOpportunityEnum.SecondaryEmail:
                        linq += @" and c.SecondaryEmail = @parsedPhone";
                        break;

                    case SearchDuplicateOpportunityEnum.All:
                        linq += @" and c.CellPhone = @parsedPhone or";
                        linq += @" c.HomePhone = @parsedPhone or";
                        //linq += @" c.HomePhone = @parsedPhone or ";
                        linq += @" c.FaxPhone  = @parsedPhone or ";
                        linq += @" c.WorkPhone = @parsedPhone or ";
                        linq += @" c.SecondaryEmail = @parsedPhone ";
                        break;

                    default:
                        return new List<Opportunity>();
                }
                var list = connection.Query<Opportunity>(linq, new { franchiseId = Franchise.FranchiseId, parsedPhone = parsedPhone }).ToList();

                //var list = connection.GetList<Opportunity>(linq, new { franchiseId = Franchise.FranchiseId,parsedPhone = parsedPhone }).ToList();

                foreach (var item in list)
                {
                    var query1 = "select a.* from crm.Addresses a inner join CRM.OpportunityAddresses la on a.AddressId = la.AddressId where la.OpportunityId = @opportunityId";
                    item.Addresses = connection.Query<AddressTP>(query1, new { opportunityId = item.OpportunityId }).ToList();
                    var query2 = "select c.* from crm.Customer c where c.CustomerId = @customerId";
                    item.PrimCustomer = connection.Query<CustomerTP>(query2, new { customerId = item.CustomerId }).FirstOrDefault();
                    var query3 = "select j.* from crm.Jobs j where j.JobId = @jobid";
                    item.Jobs = connection.Query<Job>(query3, new { jobid = item.OpportunityId }).ToList();
                }
                //linq = linq.Include(i => i.Jobs)
                //    .Include(i => i.PrimCustomer)
                //    .Include(i => i.Addresses);

                connection.Close();
                return list;
                //return linq.ToList();
            }
        }

        //added to update the status of opportunity when it is been converted to order.
        public string update(int id, short statusId)
        {
            if (id != 0)
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    var Query = "select * from CRM.Opportunities where OpportunityId=@OpportunityId and FranchiseId=@FranchiseId";
                    var result = connection.Query<Opportunity>(Query,
                        new
                        {
                            OpportunityId = id,
                            FranchiseId = SessionManager.CurrentUser.FranchiseId,
                        }).FirstOrDefault();
                    if (result != null)
                    {
                        result.OpportunityStatusId = statusId;
                        result.LastUpdatedOnUtc = DateTime.Now;
                        result.LastUpdatedByPersonId = User.PersonId;
                        var Updatevalue = connection.Update(result);
                        return "Success";
                    }
                    else return "Failed";
                }
            }
            return null;
        }

        /// <summary>
        /// Updates the status.
        /// </summary>
        /// <param name="opportunityId">The opportunity identifier.</param>
        /// <param name="statusId">The status identifier.</param>
        /// <returns>System.String.</returns>
        public string UpdateStatus(int opportunityId, short statusId)
        {
            if (opportunityId <= 0 && statusId <= 0)
                return DataCannotBeNullOrEmpty;

            // TODO: is this required in TP???
            //if (!StatusPermission.CanUpdate)
            //    return Unauthorized;

            try
            {
                var originalOpportunityInfo =
                    CRMDBContext.Opportunities.Where(
                        w => w.OpportunityId == opportunityId && w.IsDeleted == false && w.FranchiseId == Franchise.FranchiseId)
                        .Select(s => new { s.OpportunityStatusId }).FirstOrDefault();
                if (originalOpportunityInfo == null)
                    return OpportunityDoesNotExist;

                var old = OpportunityStatusTypeCollection.FirstOrDefault(f => f.OpportunityStatusId == originalOpportunityInfo.OpportunityStatusId);
                var current = OpportunityStatusTypeCollection.FirstOrDefault(f => f.OpportunityStatusId == statusId);

                var history =
                    string.Format(
                        "<Opportunity><OpportunityStatusId name=\"Status\" operation=\"update\"><Original id=\"{0}\">{1}</Original><Current id=\"{2}\">{3}</Current></OpportunityStatusId></Opportunity>",
                        old.OpportunityStatusId, old.Name, current.OpportunityStatusId, current.Name);

                var opportunity = new Opportunity { OpportunityId = opportunityId, OpportunityStatusId = originalOpportunityInfo.OpportunityStatusId };
                CRMDBContext.Opportunities.Attach(opportunity);

                opportunity.OpportunityStatusId = statusId;
                opportunity.LastUpdatedOnUtc = DateTime.Now;
                opportunity.LastUpdatedByPersonId = User.PersonId;
                opportunity.EditHistories.Add(new EditHistory()
                {
                    HistoryValue = history,
                    LoggedByPersonId = User.PersonId,
                    IPAddress = HttpContext.Current.Request.UserHostAddress
                });

                CRMDBContext.SaveChanges();
                return Success;
            }
            catch (Exception ex)
            {
#if DEBUG
                return ex.Message;
#else
                return EventLogger.LogEvent(ex);
#endif
            }
        }

        /// <summary>
        /// Creates the opportunity.
        /// </summary>
        /// <param name="opportunityId">The opportunity identifier.</param>
        /// <param name="data">The data.</param>
        /// <returns>System.String.</returns>
        public string CreateOpportunity(out int opportunityId, Opportunity data, int LeadId)
        {
            opportunityId = 0;

            if (data == null)
                return DataCannotBeNullOrEmpty;

            if (!data.FranchiseId.HasValue || data.FranchiseId <= 0)
                data.FranchiseId = Franchise.FranchiseId;
            else if (data.FranchiseId != Franchise.FranchiseId)
                return "Invalid franchise";

            if (data.OpportunityStatusId <= 0)
                data.OpportunityStatusId = AppConfigManager.NewOpportunityStatusId;

            data.CreatedByPersonId = User.PersonId;
            data.CreatedOnUtc = DateTime.UtcNow;
            data.ReceivedDate = DateTime.UtcNow;

            try
            {
                var opportunityStatus = statusChangesMgr.StatusChanges(3, data.OpportunityId, data.OpportunityStatusId);
                if (!opportunityStatus)
                    throw new ValidationException("Not a valid Opportunity Status");
                var opportunitynumber = this.spOpportunityNumber_New(Franchise.FranchiseId);

                if ((opportunitynumber == null || opportunitynumber.Value <= 0))
                    throw new Exception("Unable to generate a opportunity number");

                string[] TerritoryDetails = new string[2];

                Lead leadData = new Lead();
                if (LeadId > 0)
                {
                    data.LeadId = LeadId;
                    leadData = LeadsMgr.Get(LeadId);
                }

                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var transaction = connection.BeginTransaction();
                    try
                    {
                        if (LeadId == 0)
                        {
                            var address = connection.Get<AddressTP>(data.InstallationAddressId, transaction);
                            TerritoryDetails = TerritoryManager.GetTerritoryDetails(address.ZipCode, address.CountryCode2Digits).Split(';');
                        }

                        if (TerritoryDetails[0] != null && LeadId == 0)
                        {
                            if (TerritoryDetails[0] == "0")
                            {
                                data.TerritoryType = TerritoryDetails[1];
                                data.TerritoryId = null;
                            }
                            else
                            {
                                data.TerritoryType = TerritoryDetails[1];
                                data.TerritoryId = Convert.ToInt32(TerritoryDetails[0]);
                            }
                        }

                        // Campaign is not a required field.
                        if (data.CampaignId != null && data.CampaignId == 0) data.CampaignId = null;

                        data.OpportunityGuid = Guid.NewGuid();
                        data.CreatedByPersonId = User.PersonId;
                        data.OpportunityNumber = opportunitynumber ?? 0;
                        data.ImpersonatorPersonId = this.ImpersonatorPersonId;

                        if (data.TerritoryType.ToUpper().Contains("GRAY AREA"))
                            data.TerritoryId = null;
                        opportunityId = (int)connection.Insert(data, transaction);

                        // Save Qualify question answer
                        if (data.LeadId == 0 && data.OpportunityQuestionAns != null && data.OpportunityQuestionAns.Count > 0)
                            new TypeQuestionManager(User.PersonId, Franchise.FranchiseId).SaveOpportunityQuestionAnswers(opportunityId, data.OpportunityQuestionAns);

                        // Add Opportunity source
                        if (data.SourcesTPId > 0)
                            CreateOpportunitySource(data, opportunityId, connection, transaction);

                        if (LeadId > 0)
                        {
                            string copydataQ = @"
                            update CRM.Note set OpportunityId=@OpportunityId where LeadId=@LeadId
                            update CRM.Calendar set OpportunityId=@OpportunityId where LeadId=@LeadId
                            update CRM.Tasks set OpportunityId=@OpportunityId where LeadId=@LeadId
                            update History.SentEmails set OpportunityId=@OpportunityId where LeadId=@LeadId
                            
                            INSERT INTO [CRM].[QuestionAnswers] (QuestionId, Answer, OpportunityId, JobId, CreatedOnUtc, LoggedByPersonId)
                             select QuestionId,Answer,@OpportunityId,0,GETUTCDATE(),@LoggedByPersonId from [CRM].[QuestionAnswers] 
                             where LeadId=@LeadId";
                            connection.Query(copydataQ, new { LeadId = LeadId, OpportunityId = opportunityId, LoggedByPersonId=User.PersonId }, transaction);
                        }
                        //reoved texting update code from above query as we moved to both texting & email to  same table(History.SentEmails).
                        //update History.Texting set OpportunityId=@OpportunityId where LeadId=@LeadId

                        transaction.Commit();
                        //added for sidemark update
                        var UpdatedData = UpdateSidemark(opportunityId, data);
                        connection.Close();
                        EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(data), OpportunityId: opportunityId);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
                return Success;
            }
            catch (ValidationException ex)
            {
                return ex.Message;
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        public string UpdateSidemark(int opportunityId, Opportunity data)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    if (opportunityId > 0)
                    {
                        if (data.SideMark == "" || data.SideMark == null)
                        {
                            var sidemark = "";
                            if (data.AccountTP != null)
                                sidemark = data.OpportunityNumber + "-" + data.AccountTP.FullName;
                            else
                            {
                                var selectdata = @"select A.AccountId,Ac.CustomerId,Cu.FirstName + ' ' + Cu.LastName as FullName from CRM.Accounts A
                                            left join CRM.AccountCustomers Ac on A.AccountId = Ac.AccountId 
                                            left join CRM.Customer Cu on Ac.CustomerId = Cu.CustomerId
                                            where A.AccountId=@AccountId and Ac.IsPrimaryCustomer = 1";
                                var selectquery = connection.Query<AccountTP>(selectdata, new { AccountId = data.AccountId }).FirstOrDefault();
                                sidemark = data.OpportunityNumber + " - " + selectquery.FullName;
                            }
                            var query = @"update CRM.Opportunities set SideMark=@sidemark where OpportunityId=@OpportunityId and FranchiseId =@FranchiseId";
                            var result = connection.Query(query, new { sidemark = @sidemark, OpportunityId = opportunityId, FranchiseId = data.FranchiseId });
                        }
                    }
                    return "Successfull";
                }

                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }

            }
       }




        private void CreateOpportunitySource(Opportunity data, int opportunityId
            , SqlConnection connection, SqlTransaction transaction = null)
        {
            connection.Query(@"insert into crm.OpportunitySources (
                    SourceId, OpportunityId, CreatedOnUtc
                    , IsManuallyAdded, IsPrimarySource)
                    values(@sourceId, @opportunityId, @createdonutc
                    ,@isManuallyAdded, @isPrimarySource)"
                , new
                {
                    sourceId = data.SourcesTPId,
                    opportunityId = opportunityId,
                    createdonutc = DateTime.Now,
                    isManuallyAdded = true,
                    isPrimarySource = true
                }, transaction);
        }

        /// <summary>
        /// Gets a list of opportunity sources
        /// </summary>
        /// <param name="opportunityId"></param>
        /// <returns></returns>
        public object GetSources(int opportunityId)
        {
            //if (!BasePermission.CanRead) throw new UnauthorizedAccessException(Unauthorized);

            return (from w in CRMDBContext.Opportunities
                    from s in w.OpportunitySources
                    where w.OpportunityId == opportunityId && w.IsDeleted == false && w.FranchiseId == Franchise.FranchiseId
                    select
                        new
                        {
                            OpportunitySourceId = s.OpportunitySourceId,
                            SourceId = s.SourceId,
                            OpportunityId = s.OpportunityId,
                            CategorySourceId = s.Source.ParentId,
                            ParentId = s.Source.ParentId
                        }).ToList();
        }

        //Already Selected Sources//
        public object GetSelectedSources(int opportunityId)
        {
            //if (!BasePermission.CanRead) throw new UnauthorizedAccessException(Unauthorized);

            var OpportunitySources = (from w in CRMDBContext.Opportunities
                                      from s in w.OpportunitySources
                                      where w.OpportunityId == opportunityId && w.IsDeleted == false && w.FranchiseId == Franchise.FranchiseId
                                      select
                                          new
                                          {
                                              OpportunitySourceId = s.OpportunitySourceId,
                                              SourceId = s.SourceId,
                                              OpportunityId = s.OpportunityId,
                                              CategorySourceId = s.Source.ParentId,
                                              ParentId = s.Source.ParentId
                                          }).ToList();

            var ExceptionList = (from s in CRMDBContext.Jobs where s.OpportunityId == opportunityId select s.SourceId).ToList();

            if (OpportunitySources != null && ExceptionList != null)
            {
                var res = OpportunitySources.Where(x => ExceptionList.Contains(x.SourceId)).Select(n => n.SourceId).ToList();

                return res;
            }
            else
            {
                return null;
            }
        }

        public string UpdateSources(int opportunityId, List<int> sources)
        {
            //if (!BasePermission.CanUpdate)
            //    return Unauthorized;

            try
            {
                var opportunity =
                    CRMDBContext.Opportunities.Include(i => i.OpportunitySources)
                        .Include(i => i.EditHistories)
                        .FirstOrDefault(
                            a => a.IsDeleted == false && a.OpportunityId == opportunityId && a.FranchiseId == Franchise.FranchiseId);

                var toDelete = opportunity.OpportunitySources.Where(opportunitySource => sources.All(s => s != opportunitySource.SourceId)).ToList();
                foreach (var opportunitySource in toDelete)
                {
                    CRMDBContext.OpportunitySources.Remove(opportunitySource);
                }

                foreach (var sourceId in sources)
                {
                    if (opportunity.OpportunitySources.All(ls => ls.SourceId != sourceId))
                    {
                        CRMDBContext.OpportunitySources.Add(new OpportunitySource()
                        {
                            OpportunityId = opportunity.OpportunityId,
                            SourceId = sourceId,
                            CreatedOnUtc = DateTime.Now
                        });
                    }
                }

                /* var srcObj = CacheManager.SourceTypeCollection.FirstOrDefault(f => f.Id == sourceToAdd.SourceId);
                    XElement history =
                        new XElement("Opportunity", new XAttribute("operation", "insert"),
                            new XElement("Source",
                                new XElement("Current", new XAttribute("id", sourceToAdd.SourceId), srcObj.ToString())
                            )
                        );
*/

                opportunity.LastUpdatedByPersonId = User.PersonId;
                opportunity.LastUpdatedOnUtc = DateTime.Now;

                /*opportunity.EditHistories.Add(new EditHistory
                {
                    LoggedByPersonId = User.PersonId,
                    HistoryValue = history.ToString(SaveOptions.DisableFormatting),
                    IPAddress = HttpContext.Current.Request.UserHostAddress
                });*/

                CRMDBContext.SaveChanges();

                return Success;
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        /// <summary>
        /// Deletes the source.
        /// </summary>
        /// <param name="LSId">Opportunity Source Id</param>
        public string DeleteSource(int LSId)
        {
            if (LSId <= 0)
                return "Invalid opportunity source id";
            //if (!BasePermission.CanUpdate)
            //    return Unauthorized;

            try
            {
                var original = (from ls in CRMDBContext.OpportunitySources
                                join l in CRMDBContext.Opportunities on ls.OpportunityId equals l.OpportunityId
                                where ls.OpportunitySourceId == LSId && l.IsDeleted == false && l.FranchiseId == Franchise.FranchiseId
                                select ls).FirstOrDefault();
                if (original == null)
                    return "Source does not exist";

                var opportunity = new Opportunity { OpportunityId = original.OpportunityId, EditHistories = new List<EditHistory>() };
                CRMDBContext.Opportunities.Attach(opportunity);

                var srcObj = CacheManager.SourceCollection.FirstOrDefault(f => f.SourceId == original.SourceId);

                XElement history =
                    new XElement("Opportunity", new XAttribute("operation", "delete"),
                        new XElement("Source",
                            new XElement("Original", new XAttribute("id", original.SourceId), srcObj.ToString())
                            )
                        );

                CRMDBContext.OpportunitySources.Remove(original);

                var jobs = CRMDBContext.Jobs.Where(j => j.OpportunityId == opportunity.OpportunityId && j.SourceId == original.SourceId);
                foreach (var job in jobs)
                {
                    job.SourceId = null;
                }

                opportunity.LastUpdatedByPersonId = User.PersonId;
                opportunity.LastUpdatedOnUtc = DateTime.Now;
                opportunity.EditHistories.Add(new EditHistory
                {
                    LoggedByPersonId = User.PersonId,
                    HistoryValue = history.ToString(SaveOptions.DisableFormatting),
                    IPAddress = HttpContext.Current.Request.UserHostAddress
                });

                CRMDBContext.SaveChanges();
                return Success;
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        /// <summary>
        /// Deletes the opportunity.
        /// </summary>
        /// <param name="id">Opportunity Id</param>
        public override string Delete(int id)
        {
            //if (!BasePermission.CanDelete)
            //    return Unauthorized;

            if (id <= 0)
                return DataCannotBeNullOrEmpty;

            try
            {
                var opportunity =
                    CRMDBContext.Opportunities.Include(i => i.Jobs)
                        .Include(i => i.Tasks)
                        .Include(i => i.Calendars)
                        .FirstOrDefault(l => l.OpportunityId == id && !l.IsDeleted);
                if (opportunity == null)
                {
                    return "Opportunity not found";
                }

                if (opportunity.Jobs.Any(j => !j.IsDeleted))
                {
                    return "Opportunity can not be deleted while there are jobs related";
                }

                if (opportunity.Calendars.Any(t => !t.IsDeleted))
                {
                    return "Opportunity can not be deleted while there are appointments related";
                }

                if (opportunity.Tasks.Any(t => !t.IsDeleted))
                {
                    return "Opportunity can not be deleted while there are tasks related";
                }

                opportunity.LastUpdatedByPersonId = User.PersonId;
                opportunity.LastUpdatedOnUtc = DateTime.Now;
                opportunity.IsDeleted = true;
                opportunity.EditHistories.Add(new EditHistory()
                {
                    LoggedByPersonId = User.PersonId,
                    IPAddress = HttpContext.Current.Request.UserHostAddress,
                    HistoryValue = "<Opportunity operation=\"delete\" />"
                });

                //using (var jobsManager = new JobsManager(this.User, this.Franchise))
                //{
                //    opportunity.Jobs.ToList().ForEach(e => jobsManager.Delete(e.JobId));
                //}

                if (CRMDBContext.SaveChanges() > 0)
                    return Success;
                else
                    return FailedSaveChanges;
            }
            catch (Exception ex)
            {
#if DEBUG
                return ex.Message;
#else
                return EventLogger.LogEvent(ex);
#endif
            }
        }

        /// <summary>
        /// Updates the person.
        /// </summary>
        /// <param name="opportunityId">The opportunity identifier.</param>
        /// <param name="data">The data.</param>
        /// <returns>System.String.</returns>
        public string UpdatePerson(int opportunityId, Person data)
        {
            if (opportunityId <= 0 || data == null)
                return DataCannotBeNullOrEmpty;
            //if (!BasePermission.CanUpdate)
            //    return Unauthorized;

            try
            {
                var opportunitytoCheck =
                    CRMDBContext.Opportunities

                        // TODO : need to verify this code.
                        //.Where(
                        //w =>
                        //    w.OpportunityId == opportunityId && w.IsDeleted == false && w.FranchiseId == Franchise.FranchiseId &&
                        //    (w.PersonId == data.PersonId
                        //    || w.SecPersonId == data.PersonId))
                        .Select(s => new { Person = s.PrimCustomer }) //s.PersonId == data.PersonId ? s.PrimCustomer : s.SecCustomer})
                        .FirstOrDefault();

                if (opportunitytoCheck == null)
                    return OpportunityDoesNotExist;
                if (opportunitytoCheck.Person == null)
                    return "Person does not exist";

                //check for duplicate email
                if ((opportunitytoCheck.Person.PrimaryEmail != data.PrimaryEmail && !string.IsNullOrEmpty(data.PrimaryEmail)) ||
                    (opportunitytoCheck.Person.SecondaryEmail != data.SecondaryEmail &&
                     !string.IsNullOrEmpty(data.SecondaryEmail)))
                {
                    var exists =
                        CRMDBContext.Database.SqlQuery<bool>("SELECT [CRM].[fnsHasDuplicateEmail](@p0, @p1, @p2, @p3)",
                            Franchise.FranchiseId, data.PrimaryEmail, data.SecondaryEmail, data.PersonId)
                            .SingleOrDefault();
                    if (exists)
                    {
                        return "Duplicate email address found, unable to update person";
                    }
                }

                var opportunity = new Opportunity { OpportunityId = opportunityId };
                CRMDBContext.Opportunities.Attach(opportunity);

                opportunitytoCheck.Person.FirstName = data.FirstName;
                opportunitytoCheck.Person.LastName = data.LastName;
                opportunitytoCheck.Person.PrimaryEmail = data.PrimaryEmail;
                opportunitytoCheck.Person.MI = data.MI;
                if (opportunitytoCheck.Person.PrimaryEmail != data.SecondaryEmail)
                    opportunitytoCheck.Person.SecondaryEmail = data.SecondaryEmail;
                opportunitytoCheck.Person.HomePhone = RegexUtil.GetNumbersOnly(data.HomePhone);
                opportunitytoCheck.Person.CellPhone = RegexUtil.GetNumbersOnly(data.CellPhone);
                opportunitytoCheck.Person.WorkPhone = RegexUtil.GetNumbersOnly(data.WorkPhone);
                opportunitytoCheck.Person.WorkPhoneExt = data.WorkPhoneExt;
                opportunitytoCheck.Person.FaxPhone = RegexUtil.GetNumbersOnly(data.FaxPhone);
                opportunitytoCheck.Person.CompanyName = data.CompanyName;
                opportunitytoCheck.Person.WorkTitle = data.WorkTitle;
                opportunitytoCheck.Person.PreferredTFN = data.PreferredTFN;
                //for unsubscribe only change it if original has date and current doesnt
                //OR
                //original doesnt have date and current has date
                //this is due to the fact that we're using a date instead of a boolean to enable/disable subscription
                if ((opportunitytoCheck.Person.UnsubscribedOn.HasValue && !data.UnsubscribedOnUtc.HasValue) ||
                    (!opportunitytoCheck.Person.UnsubscribedOn.HasValue && data.UnsubscribedOnUtc.HasValue))
                    opportunitytoCheck.Person.UnsubscribedOn = data.UnsubscribedOnUtc;

                //add to history
                var entry = CRMDBContext.Entry(opportunitytoCheck.Person);
                if (entry.State != EntityState.Unchanged)
                {
                    opportunity.LastUpdatedOnUtc = DateTime.Now;
                    opportunity.LastUpdatedByPersonId = User.PersonId;
                    var changes = Util.GetEFChangedProperties(entry, opportunitytoCheck.Person);

                    opportunity.EditHistories.Add(new EditHistory()
                    {
                        IPAddress = HttpContext.Current.Request.UserHostAddress,
                        LoggedByPersonId = User.PersonId,
                        HistoryValue = changes.ToString(SaveOptions.DisableFormatting)
                    });

                    CRMDBContext.SaveChanges();
                    return Success;
                }
                else //no changes detected
                    return Success;
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        public string UpdateCustomer(int opportunityId, CustomerTP data)
        {
            if (opportunityId <= 0 || data == null)
                return DataCannotBeNullOrEmpty;
            //if (!BasePermission.CanUpdate)
            //    return Unauthorized;

            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var opportunityexist = connection.Query<Opportunity>("select * from CRM.Opportunities where OpportunityId = @opportunityid", new { opportunityid = opportunityId }).FirstOrDefault();
                    var customer = connection.Query<CustomerTP>("select * from CRM.Customer where CustomerId =@customerId", new { customerId = data.CustomerId }).FirstOrDefault();
                    if (opportunityexist == null)
                        return OpportunityDoesNotExist;
                    if (customer == null)
                    {
                        data.CreatedOn = DateTime.Now;
                        //TODO Created by has to be here.
                        var customerId = (int)connection.Insert(data);
                        connection.Query("insert into crm.OpportunityCustomers values(@opportunityid, @customerId,@isPrimaryCustomer)", new { opportunityid = opportunityId, customerId = customerId, isPrimaryCustomer = false });
                        return "Success";
                    }
                    else if (customer != null)
                    {
                        //check for duplicate email
                        //TODO Check this email validation will check in other customer.
                        if ((customer.PrimaryEmail != data.PrimaryEmail && !string.IsNullOrEmpty(data.PrimaryEmail)) ||
                            (customer.SecondaryEmail != data.SecondaryEmail &&
                             !string.IsNullOrEmpty(data.SecondaryEmail)))
                        {
                            using (var conn = new SqlConnection(EntityCnxStringBuilder.ProviderConnectionString))
                            {
                                conn.OpenAsync();
                                var exists = conn.Query<bool>(
                                    "[CRM].[fnsHasDuplicateEmail](@p0, @p1, @p2, @p3)",
                                    new
                                    {
                                        Franchise.FranchiseId,
                                        data.PrimaryEmail,
                                        data.SecondaryEmail,
                                        data.CustomerId
                                    }).FirstOrDefault();
                                if (exists)
                                {
                                    return "Duplicate email address found, unable to update person";
                                }
                            }
                        }

                        //var opportunity = new Opportunity { OpportunityId = opportunityId };
                        //CRMDBContext.Opportunities.Attach(opportunity);

                        customer.FirstName = data.FirstName;
                        customer.LastName = data.LastName;
                        customer.PrimaryEmail = data.PrimaryEmail;
                        customer.MI = data.MI;
                        if (customer.PrimaryEmail != data.SecondaryEmail)
                            customer.SecondaryEmail = data.SecondaryEmail;
                        customer.HomePhone = RegexUtil.GetNumbersOnly(data.HomePhone);
                        customer.CellPhone = RegexUtil.GetNumbersOnly(data.CellPhone);
                        customer.WorkPhone = RegexUtil.GetNumbersOnly(data.WorkPhone);
                        customer.WorkPhoneExt = data.WorkPhoneExt;
                        customer.FaxPhone = RegexUtil.GetNumbersOnly(data.FaxPhone);
                        customer.CompanyName = data.CompanyName;
                        customer.WorkTitle = data.WorkTitle;
                        customer.PreferredTFN = data.PreferredTFN;
                        //for unsubscribe only change it if original has date and current doesnt
                        //OR
                        //original doesnt have date and current has date
                        //this is due to the fact that we're using a date instead of a boolean to enable/disable subscription
                        if ((customer.UnsubscribedOn.HasValue && !data.UnsubscribedOn.HasValue) ||
                            (!customer.UnsubscribedOn.HasValue && data.UnsubscribedOn.HasValue))
                            customer.UnsubscribedOn = data.UnsubscribedOn;

                        var CustomerAffected = connection.Update(customer);
                        return Success;
                        //add to history
                        //TODO Handle History Later
                        //var entry = CRMDBContext.Entry(customer);
                        //if (entry.State != EntityState.Unchanged)
                        //{
                        //    opportunity.LastUpdatedOnUtc = DateTime.Now;
                        //    opportunity.LastUpdatedByPersonId = User.PersonId;
                        //    var changes = Util.GetEFChangedProperties(entry, customer);

                        //    opportunity.EditHistories.Add(new EditHistory()
                        //    {
                        //        IPAddress = HttpContext.Current.Request.UserHostAddress,
                        //        LoggedByPersonId = User.PersonId,
                        //        HistoryValue = changes.ToString(SaveOptions.DisableFormatting)
                        //    });

                        //    CRMDBContext.SaveChanges();
                        //    return Success;

                        //}
                        //else //no changes detected
                        //    return Success;
                    }
                    else
                        return Success;
                }
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        /// <summary>
        /// Adds the secondary customer.
        /// </summary>
        /// <param name="opportunityId">The opportunity identifier.</param>
        /// <param name="data">The data.</param>
        /// <param name="personId">The person identifier.</param>
        /// <returns>System.String.</returns>
        public string AddSecondaryCustomer(int opportunityId, Person data, out int personId)
        {
            personId = 0;

            if (opportunityId <= 0 || data == null)
                return DataCannotBeNullOrEmpty;
            //if (!BasePermission.CanUpdate)
            //    return Unauthorized;

            try
            {
                var opportunitytoCheck =
                    CRMDBContext.Opportunities.Where(
                        w => w.OpportunityId == opportunityId && w.IsDeleted == false && w.FranchiseId == Franchise.FranchiseId)
                        .Select(s => new { s.SecCustomerId }).FirstOrDefault();
                if (opportunitytoCheck == null)
                    return OpportunityDoesNotExist;
                if (opportunitytoCheck.SecCustomerId.HasValue)
                    return "Secondary person already exist, please use update instead of insert";

                //check for duplicate email
                if (!string.IsNullOrEmpty(data.PrimaryEmail) ||
                    !string.IsNullOrEmpty(data.SecondaryEmail))
                {
                    if (CRMDBContext.Database.SqlQuery<bool>("SELECT [CRM].[fnsHasDuplicateEmail](@p0, @p1, @p2, null)",
                        Franchise.FranchiseId, data.PrimaryEmail, data.SecondaryEmail).FirstOrDefault())
                    {
                        return "Duplicate email address found, unable to add person";
                    }
                }

                var opportunity = new Opportunity { OpportunityId = opportunityId };
                CRMDBContext.Opportunities.Attach(opportunity);

                data.PersonId = 0; //just to make sure we insert a new record;

                //TODO: verify whether it works well or not
                //opportunity.SecPerson = data;

                opportunity.LastUpdatedOnUtc = DateTime.Now;
                opportunity.LastUpdatedByPersonId = User.PersonId;

                XElement history =
                    new XElement("Opportunity",
                        new XElement("SecPerson", new XAttribute("name", "Secondary Person"),
                            new XAttribute("operation", "insert"),
                            new XElement("Current", data.FullName)
                            )
                        );

                opportunity.EditHistories.Add(new EditHistory()
                {
                    IPAddress = HttpContext.Current.Request.UserHostAddress,
                    LoggedByPersonId = User.PersonId,
                    HistoryValue = history.ToString(SaveOptions.DisableFormatting)
                });

                CRMDBContext.SaveChanges();
                personId = data.PersonId;

                return Success;
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        /// <summary>
        /// Updates the address.
        /// </summary>
        /// <param name="opportunityId">The opportunity identifier.</param>
        /// <param name="data">The data.</param>
        /// <returns>System.String.</returns>
        /// TODO: changed this method to Dapper
        public string UpdateAddress(int opportunityId, AddressTP data)
        {
            if (opportunityId <= 0 || data == null || data.AddressId <= 0)
                return DataCannotBeNullOrEmpty;
            //if (!BasePermission.CanUpdate)
            //    return Unauthorized;

            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var opportunityToCheck = connection.Query<Opportunity>("select * from CRM.Opportunities where OpportunityId = @opportunityid", new { opportunityid = opportunityId }).FirstOrDefault();
                    var address = connection.Query<Address>("select * from CRM.Addresses where AddressId =@addressId", new { addressId = data.AddressId }).FirstOrDefault();
                    //var opportunityToCheck = (from l in CRMDBContext.Opportunities
                    //    where l.OpportunityId == opportunityId && l.IsDeleted == false && l.FranchiseId == Franchise.FranchiseId
                    //    select new {Address = l.Addresses.FirstOrDefault(f => f.AddressId == data.AddressId)})
                    //    .FirstOrDefault();

                    if (opportunityToCheck == null)
                        return OpportunityDoesNotExist;
                    if (address == null)
                    {
                        address.CreatedOnUtc = DateTime.Now;
                        //TODO Created by has to be here.
                        var addressId = (int)connection.Insert(address);
                        connection.Query("insert into crm.OpportunityAddresses values(@opportunityid, @addressid)", new { opportunityid = opportunityId, addressid = addressId });
                        return "Success";
                    }

                    address.AttentionText = data.AttentionText;
                    address.Address1 = data.Address1;
                    address.Address2 = data.Address2;
                    address.City = data.City;
                    address.State = data.State;
                    if (!string.IsNullOrEmpty(data.ZipCode))
                        address.ZipCode = data.ZipCode.ToUpper();
                    else
                        address.ZipCode = null;
                    address.CrossStreet = data.CrossStreet;
                    address.CountryCode2Digits = string.IsNullOrEmpty(data.CountryCode2Digits)
                        ? Franchise.CountryCode
                        : data.CountryCode2Digits;
                    address.IsResidential = data.IsResidential;
                    //TODO Need to verify this code
                    //var changes = Util.GetEFChangedProperties(CRMDBContext.Entry(opportunityToCheck.Address), opportunityToCheck.Address);

                    //var opportunity = new Opportunity {OpportunityId = opportunityId};
                    //CRMDBContext.Opportunities.Attach(opportunity);
                    //opportunity.LastUpdatedOnUtc = DateTime.Now;
                    //opportunity.LastUpdatedByPersonId = User.PersonId;

                    //if (changes != null)
                    //{
                    //    opportunity.EditHistories.Add(new EditHistory()
                    //    {
                    //        IPAddress = HttpContext.Current.Request.UserHostAddress,
                    //        LoggedByPersonId = User.PersonId,
                    //        HistoryValue = changes.ToString(System.Xml.Linq.SaveOptions.DisableFormatting)
                    //    });
                    //}

                    //CRMDBContext.SaveChanges();
                    connection.Update(address);
                    connection.Close();

                    return Success;
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                return ex.Message;
#else
                return EventLogger.LogEvent(ex);
#endif
            }
        }

        public List<OpportunityNote> GetNotes(List<int> opportunityIds, bool includeHistory = false)
        {
            if (opportunityIds == null || opportunityIds.Count == 0)
                throw new ArgumentOutOfRangeException("Invalid Opportunity Ids");

            List<OpportunityNote> notes = new List<OpportunityNote>();
            int opportunityId = opportunityIds[0];
            if (includeHistory)
            {
                var Opportunities = (from W in CRMDBContext.Opportunities
                                     from n in W.OpportunityNotes
                                     from h in W.EditHistories
                                     where opportunityIds.Contains(W.OpportunityId) && W.FranchiseId == Franchise.FranchiseId && W.IsDeleted == false
                                     select new { Note = n, History = h }).ToList();

                notes = Opportunities.Select(s => s.Note).ToList();
                if (Opportunities.Count(s => s.History != null) > 0)
                {
                    notes = notes.Concat(Opportunities.Select(s => new OpportunityNote
                    {
                        CreatedByPersonId = s.History.LoggedByPersonId,
                        OpportunityId = s.Note.OpportunityId,
                        Message = OpportunitiesManager.ParseMessage(s.History.HistoryValue),
                        CreatedOn = s.History.CreatedOnUtc,
                        TypeEnum = NoteTypeEnum.History
                    })).ToList();
                }
            }
            else
            {
                //notes = (from W in CRMDBContext.Opportunities
                //         from n in W.OpportunityNotes
                //         where opportunityIds.Contains(W.OpportunityId) && W.FranchiseId == Franchise.FranchiseId && W.IsDeleted == false
                //         select n).ToList();

                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var query = @"select * from [CRM].[OpportunityNotes] where opportunityId=@opportunityId";
                    var opportunityNotes = connection.Query(query, new { opportunityId = opportunityId }).ToList();
                    connection.Close();
                    for (int i = 0; i < opportunityNotes.Count; ++i)
                    {
                        notes.Add(new OpportunityNote
                        {
                            NoteId = opportunityNotes[i].NoteId,
                            OpportunityId = opportunityNotes[i].OpportunityId,
                            Message = opportunityNotes[i].Message,
                            CreatedOn = opportunityNotes[i].CreatedOn,
                            CreatedByPersonId = opportunityNotes[i].CreatedByPersonId,
                            TypeEnum = opportunityNotes[i].TypeEnum,
                            Title = opportunityNotes[i].Title
                        });
                    }
                }

                notes = notes.OrderByDescending(o => o.CreatedOn).ToList();
                foreach (var note in notes)
                {
                    var person = CacheManager.UserCollection.FirstOrDefault(f => f.PersonId == note.CreatedByPersonId);
                    if (person != null)
                    {
                        note.CreatorFullName = person.Person.FullName;
                        note.AvatarSrc = person.AvatarSrc ?? AppConfigManager.DefaultAvatarSrc;
                    }
                    else
                    {
                        note.CreatorFullName = "Unknown";
                        note.AvatarSrc = AppConfigManager.DefaultAvatarSrc;
                    }
                }

                return notes;
            }
            return notes;
        }

        public override ICollection<Opportunity> Get(List<int> idList)
        {
            //if (!BasePermission.CanRead)
            //    throw new UnauthorizedAccessException(Unauthorized);
            if (idList == null)
                throw new ArgumentNullException("idList can not be null");
            if (idList.Count == 0)
                return new List<Opportunity>();

            var result = new List<Opportunity>();

            var pageSize = 5000;

            List<Opportunity> list;
            if (idList.Count() > pageSize)
            {
                list = new List<Opportunity>();

                do
                {
                    List<int> ld1;
                    if (idList.Count() < pageSize)
                    {
                        ld1 = idList.Take(idList.Count()).ToList();
                        idList.RemoveRange(0, idList.Count());
                    }
                    else
                    {
                        ld1 = idList.Take(pageSize).ToList();
                        idList.RemoveRange(0, pageSize);
                    }

                    list.AddRange(GetOpportunitysForOpportunityIds(ld1));
                } while (idList.Any());
            }
            else
            {
                //list = CRMDBContext.Opportunities.AsNoTracking()
                //    .Include(i => i.PrimCustomer)
                //    .Include(i => i.Jobs.Select(s => s.JobQuotes))
                //    .Include(i => i.Jobs.Select(s => s.JobNotes))
                //    .Include(i => i.SecPerson)
                //    .Include(i => i.OpportunitySources)
                //    .Include(i => i.OpportunitySources.Select(s => s.Source))
                //    .Include(i => i.Addresses)
                //    .Include(i => i.OpportunityNotes)
                //    .Where(w => idList.Contains(w.OpportunityId)).ToList();
                list = GetOpportunitysForOpportunityIds(idList);
            }

            //reference: http://stackoverflow.com/questions/11621611/entity-framework-trouble-with-load
            //for explicit loading, need to assign it to the navigation properties instead of calling .Load(), which only loads to the DbContext instead of populating the record
            //set up permission per opportunity

            //var canUpdate = BasePermission.CanUpdate;
            //var canDelete = BasePermission.CanDelete;
            // TODO: is this required in TP???
            //var canUpdateStatus = StatusPermission.CanUpdate;
            var canUpdateStatus = true;

            Object lockMe = new Object();

            Parallel.ForEach(list, rec =>
            {
                //rec.Addresses = entry.Collection(c => c.Addresses).Query().Take(1).ToList();
                if (rec.OpportunityNotes != null && rec.OpportunityNotes.Count > 0)
                {
                    foreach (var note in rec.OpportunityNotes)
                    {
                        var person =
                            CacheManager.UserCollection.FirstOrDefault(f => f.PersonId == note.CreatedByPersonId);
                        if (person != null)
                        {
                            note.CreatorFullName = person.Person.FullName;
                            note.AvatarSrc = person.AvatarSrc ?? AppConfigManager.DefaultAvatarSrc;
                        }
                        else
                        {
                            note.CreatorFullName = "";
                            note.AvatarSrc = AppConfigManager.DefaultAvatarSrc;
                        }
                    }
                    rec.OpportunityNotes = rec.OpportunityNotes.OrderByDescending(o => o.CreatedOn).ToList();

                    foreach (var job in rec.Jobs)
                    {
                        foreach (var note in job.JobNotes)
                        {
                            var person =
                                CacheManager.UserCollection.FirstOrDefault(F => F.PersonId == note.CreatedByPersonId);
                            if (person != null)
                            {
                                note.CreatorFullName = person.Person.FullName;
                                note.AvatarSrc = person.AvatarSrc ?? AppConfigManager.DefaultAvatarSrc;
                            }
                            else
                            {
                                note.CreatorFullName = "";
                                note.AvatarSrc = AppConfigManager.DefaultAvatarSrc;
                            }
                        }

                        job.JobNotes = job.JobNotes.OrderByDescending(o => o.CreatedOn).ToList();
                    }
                }
                //rec.CanUpdate = canUpdate;
                //rec.CanDelete = canDelete;
                rec.CanUpdateStatus = canUpdateStatus;

                lock (lockMe)
                {
                    result.Add(rec);
                }
            });

            return result;
        }

        private List<Opportunity> GetOpportunitysForOpportunityIds(List<int> opportunityIds)
        {
            return CRMDBContext.Opportunities.AsNoTracking()
                .Include(i => i.PrimCustomer)
                .Include(i => i.Jobs.Select(s => s.JobQuotes))
                .Include(i => i.Jobs.Select(s => s.JobNotes))
                .Include(i => i.SecCustomer)
                .Include(i => i.OpportunitySources)
                .Include(i => i.OpportunitySources.Select(s => s.Source))
                .Include(i => i.Addresses)
                .Include(i => i.OpportunityNotes)
                .Where(w => opportunityIds.Contains(w.OpportunityId)).ToList();
        }

        public void SetOpportunityStatus(int opportunityid)
        {
            int OpportunityStatusId = 0;
            bool Isdisposition = false;

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = "select * from [CRM].[Opportunities] where OpportunityId = @opportunityId";
                var opportunity = connection.Query<Opportunity>(query, new { opportunityId = opportunityid }).FirstOrDefault();
                connection.Close();
                if (opportunity != null)
                {
                    if (opportunity.OpportunityStatusId > 0)
                    {
                        if (opportunity.OpportunityStatusId == 6)
                        {
                            Isdisposition = true;
                        }
                    }
                }
            }

            if (Isdisposition == false)
            {
                //2   Measurement
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var query = "select * from [CRM].[MeasurementHeader] where OpportunityId = @opportunityId";
                    var opportunity = connection.Query<MeasurementHeader>(query, new { opportunityId = opportunityid }).FirstOrDefault();
                    connection.Close();
                    if (opportunity != null)
                    {
                        if (opportunity.OpportunityId > 0)
                        {
                            OpportunityStatusId = 2;
                        }
                    }
                }

                //3   Quote
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var query = "select * from [CRM].[Quote] where OpportunityId = @opportunityId";
                    var opportunity = connection.Query<Quote>(query, new { opportunityId = opportunityid }).FirstOrDefault();
                    connection.Close();
                    if (opportunity != null)
                    {
                        if (opportunity.OpportunityId > 0)
                        {
                            OpportunityStatusId = 3;
                        }
                    }
                }
                //4   Ordering
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var query = "select * from [CRM].[Orders] where OpportunityId = @opportunityId";
                    var opportunity = connection.Query<Orders>(query, new { opportunityId = opportunityid }).FirstOrDefault();
                    connection.Close();
                    if (opportunity != null)
                    {
                        if (opportunity.OpportunityId > 0 && opportunity.OrderStatus != 6 && opportunity.OrderStatus != 9)
                        {
                            OpportunityStatusId = 4;
                        }
                    }
                }

                //5   Order Accepted

                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var query = "select * from [CRM].[Payments] where OpportunityId = @opportunityId";
                    var opportunity = connection.Query<Payments>(query, new { opportunityId = opportunityid }).FirstOrDefault();

                    connection.Close();
                    if (opportunity != null)
                    {
                        var queryOrder = "select * from [CRM].[Orders] where OrderID = @OrderId";
                        var order = connection.Query<Orders>(queryOrder, new { OrderId = opportunity.OrderID }).FirstOrDefault();
                        if (opportunity.OpportunityID > 0 && order.OrderStatus != 6 && order.OrderStatus != 9)
                        {
                            OpportunityStatusId = 5;
                        }
                    }
                }

                //Set OpportunitystatusId
                if (OpportunityStatusId > 0)
                {
                    using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                    {
                        connection.Open();
                        string query1 = "update crm.Opportunities set OpportunityStatusId=@OpportunityStatusId where opportunityid = @opportunityid";
                        connection.Execute(query1, new { OpportunityStatusId = OpportunityStatusId, opportunityid = opportunityid });
                        connection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Get a opportunity and associated navigation properties
        /// </summary>
        /// <param name="id">Opportunity id</param>
        /// <exception cref="System.ArgumentNullException"></exception>
        /// <exception cref="System.AccessViolationException"></exception>
        public override Opportunity Get(int id)
        {
            if (id < 0)
                throw new ArgumentNullException(OpportunityDoesNotExist);
            //if (!BasePermission.CanRead)
            //    throw new AccessViolationException(Unauthorized);
            int opportunityId = id;
            //Set Opportunity status

            SetOpportunityStatus(id);

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @" select op.*, l.IsCommercial,te.Name as DisplayTerritoryName from  CRM.Opportunities op
                             left join CRM.Accounts l  on l.AccountId = op.AccountId
                             left join CRM.Territories te on op.TerritoryDisplayId = te.TerritoryId  where op.OpportunityId = @OpportunityId";
                var opportunity = connection.Query<Opportunity>(query, new { OpportunityId = id }).FirstOrDefault();

                if (opportunity != null && opportunity.FranchiseId != SessionManager.CurrentFranchise.FranchiseId) // Franchise.FranchiseId)
                {
                    throw new AccessViolationException(Unauthorized);
                }

                var resAcc = connection.Query<AccountTP>(@"select * from [CRM].[Accounts] where [AccountId] =@AccountId"
                    , new { AccountId = opportunity.AccountId }).FirstOrDefault();
                opportunity.RelatedAccountId = resAcc?.RelatedAccountId;

                query = @"select * from CRM.Customer c
                         join CRM.AccountCustomers ac on c.CustomerId=ac.CustomerId
                         where ac.AccountId=@AccountId";
                var Person = connection.Query<CustomerTP>(query, new { AccountId = opportunity.AccountId }).ToList();

                if (Person != null && Person.Count > 0)
                {
                    opportunity.PrimCustomer = Person.Where(x => x.IsPrimaryCustomer == true).FirstOrDefault();
                    opportunity.SecCustomer = Person.Where(x => x.IsPrimaryCustomer == false).ToList();
                }

                query = @"select * from CRM.Campaign Where CampaignId = @campaignId";
                var campaign = connection.Query<Campaign>(query, new { campaignId = opportunity.CampaignId }).FirstOrDefault();
                opportunity.CampaignName = campaign?.Name;

                query = @"select * from CRM.Addresses where AddressId = @InstallationAddressId";
                opportunity.Addresses = connection.Query<AddressTP>(query, new { InstallationAddressId = opportunity.InstallationAddressId }).ToList();

                query = @"select stp.SourcesTPId as SourceId,o.Name as OriginationName,s.Name as SourceName,c.Name as ChannelName,o.Name+ ' - ' +  c.Name + ' - ' + s.Name as name,
                    ls.IsPrimarySource from crm.SourcesTP stp
                    join crm.Type_Channel c on c.ChannelId = stp.channelid
                    join crm.sources s on s.SourceId = stp.SourceId
                    join crm.OpportunitySources ls on ls.SourceId = stp.SourcesTPId
                    join CRM.Type_Owner o on o.OwnerId = stp.OwnerId
                    where ls.OpportunityId = @OpportunityId";
                opportunity.SourcesList = connection.Query<SourceList>(query, new { OpportunityId = id }).ToList();
                opportunity.OpportunityStatus = connection.Query<Type_OpportunityStatus>("select * from CRM.Type_OpportunityStatus where OpportunityStatusId = @id", new { id = opportunity.OpportunityStatusId }).FirstOrDefault();
                opportunity.Disposition = connection.Query<Disposition>("select * from CRM.Disposition where DispositionId = @dispositionid", new { dispositionid = opportunity.DispositionId }).FirstOrDefault();

                query = "select * from crm.OpportunityNotes where opportunityId = @opportunityId";
                opportunity.OpportunityNotes = connection.Query<OpportunityNote>(query, new { opportunityId = id }).ToList();

                //Added for Qualification/Question Answers
                int BrandId = (int)SessionManager.BrandId;
                List<Type_Question> opportunityQuestion = HFC.CRM.Managers.TypeQuestionManager.GetTypeOpportunityQuestionsById(BrandId, opportunity.OpportunityId);
                opportunity.OpportunityQuestionAns = opportunityQuestion;

                //Added for new notes and file attachment
                try
                {
                    query = @"SELECT OpportunityId, '' AS 'FileName', '' AS 'FullFileName' 
                            , Message AS 'Note',  
                            (CASE TypeEnum WHEN 1 THEN 'Internal' WHEN 2 THEN 'Customer' WHEN 3 THEN 'Direction' END) AS 'Category'
                            , Title, TypeEnum, NoteId AS 'Id' 
                            FROM [CRM].[OpportunityNotes] 
                            WHERE OpportunityId = @opportunityId  
                            UNION ALL  
                            SELECT ATT.OpportunityId, PHY.FileName
                            , PHY.UrlPath,'',PHY.FileCategory
                            ,PHY.FileDescription, 0
                            , PHY.PhysicalFileId 
                            FROM [CRM].[PhysicalFiles] PHY  
                            INNER JOIN [CRM].[Attachments] ATT ON PHY.PhysicalFileId = ATT.PhysicalFileId  
                            WHERE ATT.OpportunityId = @opportunityId";
                    var opportunityNoteAttachment = connection.Query<NotesAttachmentslist>(query, new { opportunityId = id }).ToList();
                    //Set full file path
                    foreach (var attf in opportunityNoteAttachment)
                    {
                        string filename = attf.FullFileName;
                        if (filename.Length > 0)
                        {
                            string fullFilePath = HttpContext.Current.Server.MapPath(filename);
                            attf.FullFileName = fullFilePath;
                        }
                    }
                    //To sent to Opportunity Notes attachment object
                    opportunity.NotesAttachmentslist = opportunityNoteAttachment;
                }
                catch (Exception ex) { }

                if (opportunity != null)
                {
                    // TODO: is this required in TP???
                    opportunity.CanUpdateStatus = true;

                    var primarySource = opportunity.SourcesList.Where(x => x.IsPrimarySource == true).FirstOrDefault();

                    if (primarySource != null)
                        opportunity.SourcesTPId = primarySource.SourceId;
                    if (opportunity.CampaignId == null) opportunity.CampaignId = 0;
                    if (opportunity.CreatedOnUtc == opportunity.ReceivedDate)
                    {
                        if (opportunity.ReceivedDate != null)
                            opportunity.ReceivedDate = TimeZoneManager.ToLocal(((DateTimeOffset)opportunity.ReceivedDate).DateTime);
                    }
                    if (opportunity.CreatedOnUtc != null)
                        opportunity.CreatedOnUtc = TimeZoneManager.ToLocal(((DateTimeOffset)opportunity.CreatedOnUtc).DateTime);
                }
                return opportunity;
            }
        }

        /// <summary>
        /// Not implemented
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public override string Add(Opportunity data)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Not implemented
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public override string Update(Opportunity data)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var opportunityStatus = statusChangesMgr.StatusChanges(3, data.OpportunityId, data.OpportunityStatusId);
                    if (!opportunityStatus)
                        throw new ValidationException("Not a valid Opportunity Status");

                    var opportunity = connection.Query<Opportunity>("select * from CRM.Opportunities where OpportunityId = @opportunityid", new { opportunityid = data.OpportunityId }).FirstOrDefault();

                    opportunity.OpportunityStatusId = data.OpportunityStatusId;
                    opportunity.DispositionId = data.DispositionId;
                    opportunity.CampaignId = data.CampaignId;
                    opportunity.SideMark = data.SideMark;
                    opportunity.OpportunityName = data.OpportunityName;
                    opportunity.SideMark = data.SideMark;
                    opportunity.AccountId = data.AccountId;
                    opportunity.SalesAgentId = data.SalesAgentId;
                    opportunity.InstallerId = data.InstallerId;
                    opportunity.TerritoryDisplayId = data.TerritoryDisplayId;

                    opportunity.TerritoryId = data.TerritoryId;
                    opportunity.TerritoryType = data.TerritoryType;
                    opportunity.Territory = data.Territory;
                    opportunity.DisplayTerritoryName = data.DisplayTerritoryName;
                    opportunity.InstallationAddressId = data.InstallationAddressId;
                    opportunity.BillingAddressId = data.BillingAddressId;
                    opportunity.Description = data.Description;
                    opportunity.OpportunityStatusId = data.OpportunityStatusId;
                    opportunity.OpportunityId = data.OpportunityId;
                    if (data.TerritoryType.ToUpper().Contains("GRAY AREA"))
                        opportunity.TerritoryId = null;

                    var oSource = connection.Query<OpportunitySource>(@" SELECT * FROM crm.OpportunitySources os
                            WHERE os.IsPrimarySource = 1 and os.OpportunityId = @OpportunityId;"
                        , new { OpportunityId = data.OpportunityId }).FirstOrDefault();

                    if (oSource != null && oSource.SourceId != data.SourcesTPId)
                    {
                        oSource.SourceId = data.SourcesTPId;
                        oSource.Updatedon = DateTime.UtcNow;
                        oSource.UpdatedBy = SessionManager.CurrentUser.PersonId;
                        connection.Update(oSource);
                    }
                    else if (oSource == null)
                        CreateOpportunitySource(data, data.OpportunityId, connection);

                    opportunity.LastUpdatedOnUtc = DateTime.UtcNow;
                    opportunity.LastUpdatedByPersonId = User.PersonId;
                    opportunity.Description = data.Description;
                    opportunity.AccountId = data.AccountId;
                    opportunity.InstallationAddressId = data.InstallationAddressId;
                    opportunity.Description = data.Description;
                    opportunity.OpportunityStatusId = data.OpportunityStatusId;
                    opportunity.SalesAgentId = data.SalesAgentId;
                    opportunity.InstallerId = data.InstallerId;

                    // TP-2230: Mark Opportunity as tax exempt - Canadian Sales Tax
                    opportunity.IsTaxExempt = data.IsTaxExempt;
                    opportunity.TaxExemptID = data.TaxExemptID;
                    opportunity.IsPSTExempt = data.IsPSTExempt;
                    opportunity.IsGSTExempt = data.IsGSTExempt;
                    opportunity.IsHSTExempt = data.IsHSTExempt;
                    opportunity.IsVATExempt = data.IsVATExempt;
                    opportunity.IsNewConstruction = data.IsNewConstruction;
                    opportunity.ReceivedDate = data.ReceivedDate;

                    if (opportunity.CampaignId != null && opportunity.CampaignId == 0)
                        opportunity.CampaignId = null;

                    data.ImpersonatorPersonId = this.ImpersonatorPersonId;

                    connection.Update(opportunity);
                    //var SidemarkUpdate = UpdateSidemark(opportunity.OpportunityId, opportunity);
                    connection.Close();

                    if (data.OpportunityQuestionAns != null && data.OpportunityQuestionAns.Count > 0)
                        new TypeQuestionManager(User.PersonId, Franchise.FranchiseId).SaveOpportunityQuestionAnswers(data.OpportunityId, data.OpportunityQuestionAns);

                    EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(opportunity), OpportunityId: data.OpportunityId);
                    return Success;
                }
            }
            catch (ValidationException ex)
            {
                return ex.Message;
            }
        }

        public AddressTP GetAddressTP(int opportunityId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var str = @"select * from crm.Addresses
                            where  AddressId in(select InstallationAddressId from
                                crm.Opportunities where OpportunityId=@OpportunityId)";
                var zillowaddress = connection.Query<HFC.CRM.Data.AddressTP>(str,
                    new { OpportunityId = opportunityId }).FirstOrDefault();
                connection.Close();
                return zillowaddress;
            }
        }

        public List<Address> GetAddress(List<int> opportunityIds)
        {
            //if (!BasePermission.CanRead)
            //    throw new UnauthorizedAccessException(Unauthorized);
            List<Address> addrlist = new List<Address>();
            try
            {
                if (opportunityIds == null || opportunityIds.Count == 0)
                    return null;
                int opportunityId = opportunityIds[0];
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    //var query = @"select PersonId as SalesAgentId,FirstName + ' ' + LastName  as Name FROM [CRM].[Person] ";
                    connection.Open();
                    var query = @"select AccountId,InstallationAddressId from [CRM].[Opportunities] where opportunityId=@opportunityId";
                    var opportunity = connection.Query(query, new { opportunityId = opportunityId }).ToList();

                    int accountId = opportunity.First().AccountId;
                    int InstallationAddressId = opportunity.First().InstallationAddressId;
                    //query = @"select AddressId from [CRM].[AccountAddresses] where AccountId=@AccountId";

                    //var Address = connection.Query(query, new { AccountId = accountId }).ToList();
                    //int AddressId = Address.First().AddressId;

                    query = @"select * from CRM.Addresses where AddressId=@AddressId";
                    var addresslist = connection.Query(query, new { AddressId = InstallationAddressId }).ToList();

                    connection.Close();
                    for (int i = 0; i < addresslist.Count; ++i)
                    {
                        addrlist.Add(new Address
                        {
                            AddressId = addresslist[i].AddressId,
                            Address1 = addresslist[i].Address1,
                            Address2 = addresslist[i].Address2,
                            City = addresslist[i].City,
                            State = addresslist[i].State,
                            ZipCode = addresslist[i].ZipCode,
                            CountryCode2Digits = addresslist[i].CountryCode2Digits,
                            CreatedOnUtc = addresslist[i].CreatedOnUtc,
                            IsDeleted = addresslist[i].IsDeleted,
                            IsResidential = addresslist[i].IsResidential,
                            CrossStreet = addresslist[i].CrossStreet,
                            Location = addresslist[i].Location,
                            AddressesGuid = addresslist[i].AddressesGuid
                        });
                    }
                    return addrlist;
                }

                //return (from l in CRMDBContext.Opportunities
                //        from a in l.Addresses
                //        where opportunityIds.Contains(l.OpportunityId) && l.IsDeleted == false && l.FranchiseId == Franchise.FranchiseId
                //        select a).ToList();
            }
            catch (Exception ex)
            {
                //log error
            }
            return addrlist;
        }

        #region 2.0

        public OpportunityInfoDTO GetOpportunityInfo(int opportunityId)
        {
            using (MiniProfiler.Current.Step("GetOpportunityInfo"))
            {
                if (opportunityId <= 0) throw new ArgumentNullException(OpportunityDoesNotExist);
                //if (!BasePermission.CanRead) throw new UnauthorizedAccessException(Unauthorized);

                OpportunityInfoDTO opportunity;

                try
                {
                    using (SqlConnection conn = new SqlConnection(EntityCnxStringBuilder.ProviderConnectionString))
                    {
                        conn.Open();
                        opportunity =
                            conn.Query<OpportunityInfoDTO>("[CRM].[spGetOpportunityInfo] @opportunityId", new { opportunityId = opportunityId })
                                .FirstOrDefault();
                    }

                    var primePerson =
                        CRMDBContext.People.AsNoTracking().FirstOrDefault(v => v.PersonId == opportunity.PrimePersonId);

                    var primePersonDTO = new PersonDTO();
                    if (primePerson != null)
                    {
                        primePersonDTO.CellPhone = primePerson.CellPhone;
                        primePersonDTO.CompanyName = primePerson.CompanyName;
                        primePersonDTO.FaxPhone = primePerson.FaxPhone;
                        primePersonDTO.FullName = primePerson.FullName;
                        primePersonDTO.HomePhone = primePerson.HomePhone;
                        primePersonDTO.PersonId = primePerson.PersonId;
                        primePersonDTO.PreferredTFN = primePerson.PreferredTFN;
                        primePersonDTO.PrimaryEmail = primePerson.PrimaryEmail;
                        primePersonDTO.SecondaryEmail = primePerson.SecondaryEmail;
                        primePersonDTO.Title = primePerson.WorkTitle;
                        primePersonDTO.WorkPhone = primePerson.WorkPhone;
                    }

                    opportunity.PrimaryPerson = primePersonDTO;
                    return opportunity;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
            }
        }

        #endregion 2.0

        public int GetNewOpportunityCount()
        {
            if (Franchise == null)
            {
                return 0;
            }
            return
                CRMDBContext.Opportunities.Count(v => v.FranchiseId == Franchise.FranchiseId && v.OpportunityStatusId == 1 && v.IsDeleted == false);
        }

        public Nullable<int> spOpportunityNumber_New(Nullable<int> franchiseId)
        {
            using (var conn = new SqlConnection(EntityCnxStringBuilder.ProviderConnectionString))
            {
                conn.Open();
                var dto = conn.Query(
                    "[CRM].[spOpportunityNumber_New] @FranchiseId",
                    new
                    {
                        FranchiseId = franchiseId,
                    }).ToList();

                var opportunityNumber = dto[0].OpportunityNumber;
                return opportunityNumber;
            }
        }

        public string UpdateOpportunity(int opportunityId, int opportunityStatusId, int dispositionId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var opportunity = connection.Query<Opportunity>("select * from CRM.Opportunities where OpportunityId = @opportunityid", new { opportunityid = opportunityId }).FirstOrDefault();
                opportunity.OpportunityStatusId = Convert.ToInt16(opportunityStatusId);
                opportunity.DispositionId = Convert.ToInt16(dispositionId);
                connection.Update(opportunity);
                connection.Close();
                return Success;
            }
        }

        #region ---- Touhchpoint ----

        private string SaveNoteAttachment(int opportunityId, Opportunity data, SqlConnection connection, SqlTransaction transaction = null)
        {
            //================================================================
            //Added for Notes and Notes Attachment
            //To save Opportunities Notes and Opportunities Attachment
            //================================================================
            int createdBy = User.PersonId, TypeEnum = 0;
            string Query = "", tmpAttachFolder = "tmpAttachments", thmbUrl = "";
            DateTime createdDate = DateTime.Now;

            if (data.NotesAttachmentslist != null)
            {
                foreach (var item in data.NotesAttachmentslist)
                {
                    if (item.FullFileName != null && item.FullFileName != "")
                    {
                        try
                        {
                            //Full Path
                            string SourceFileName = HttpContext.Current.Server.MapPath(item.FullFileName.Substring(item.FullFileName.IndexOf("files"))).Replace("\\api", "");

                            //Remove File Name
                            string SourceFolder = SourceFileName.Substring(0, SourceFileName.LastIndexOf("\\"));

                            //Target Folder based on Opportunity Id
                            string TargetFolder = SourceFolder.Replace(tmpAttachFolder, Convert.ToString(opportunityId));

                            //Target File name
                            string TargetFile = TargetFolder + "\\" + Path.GetFileName(SourceFileName);

                            //Create Folder based Opportunity Id
                            if (!Directory.Exists(TargetFolder))
                                Directory.CreateDirectory(TargetFolder);

                            //Get url full path
                            string UrlPath = TargetFile.Substring(TargetFile.IndexOf("\\files")).Replace("\\", "/");

                            //IF file already exists - Rename to new file
                            if (System.IO.File.Exists(TargetFile))
                            {
                                //Append timestamp and save file instead of warning duplicate
                                int lastperioduf = TargetFile.LastIndexOf('.');
                                if (lastperioduf < 0)
                                    lastperioduf = TargetFile.Length;
                                TargetFile = TargetFile.Insert(lastperioduf,
                                    ((int)DateTime.Now.TimeOfDay.TotalSeconds).ToString());
                            }

                            //Check Target Folder and copy file
                            if (Directory.Exists(TargetFolder))
                            {
                                //Copy files
                                File.Copy(SourceFileName, TargetFile, true);
                            }
                            //Check File and delete
                            if (File.Exists(SourceFileName) && File.Exists(TargetFile))
                            {
                                //After copy suceess delete folder
                                File.Delete(SourceFileName);
                            }

                            //Insert into table PhysicalFiles
                            FileInfo targetPhyFile = new FileInfo(TargetFile);
                            if (item.Id == 0)
                            {
                                //To Save CRM.PhysicalFiles
                                Query = "INSERT INTO CRM.PhysicalFiles ";
                                Query = Query + "(FileName,MimeType,FileDescription,ThumbUrl,UrlPath,IsRelativeUrlPath,";
                                Query = Query + "CreatedOnUtc,AddedByPersonId,FileSize,LastUpdatedUtc,LastUpdatedByPersonId,FileCategory) ";
                                Query = Query + " VALUES ";
                                Query = Query + "(@FileName,@MimeType,@FileDescription,@ThumbUrl,@UrlPath,";
                                Query = Query + "@IsRelativeUrlPath,@CreatedOnUtc,@AddedByPersonId,@FileSize,";
                                Query = Query + "@LastUpdatedUtc,@LastUpdatedByPersonId,@FileCategory)";
                                Query = Query + "SELECT CAST(SCOPE_IDENTITY() AS INT);";
                                //
                                var newPhysicalFileId = connection.Query<int>(Query, new
                                {
                                    FileName = item.FileName,
                                    MimeType = System.Web.MimeMapping.GetMimeMapping(TargetFile),
                                    FileDescription = item.Title,
                                    ThumbUrl = thmbUrl,
                                    UrlPath = UrlPath,
                                    IsRelativeUrlPath = 1,
                                    CreatedOnUtc = createdDate,
                                    AddedByPersonId = createdBy,
                                    FileSize = targetPhyFile.Length,
                                    LastUpdatedUtc = createdDate,
                                    LastUpdatedByPersonId = createdBy,
                                    FileCategory = item.Category
                                }, transaction);
                                Query = "";
                                //Get Max Attachment ID
                                Query = "SELECT ISNULL(MAX(AttachmentId), 0) +1 FROM[CRM].[Attachments]";
                                var rtnNewId = connection.ExecuteScalar<int>(Query, transaction);
                                Query = "";

                                //Get new physical file id
                                int newPhyFileId = newPhysicalFileId.FirstOrDefault();

                                //To save Attachments
                                string attachmentNewID = Convert.ToString(rtnNewId);
                                //To Save Attachment
                                Query = "INSERT INTO CRM.Attachments ";
                                Query = Query + "(AttachmentId, OpportunityId,PhysicalFileId,createdOn,CreatedBy) ";
                                Query = Query + "VALUES (@AttachmentId,@OpportunityId, @PhysicalFileId,";
                                Query = Query + "@CreatedOn,@CreatedBy)";
                                connection.Execute(Query, new
                                {
                                    AttachmentId = attachmentNewID,
                                    OpportunityId = opportunityId,
                                    PhysicalFileId = newPhyFileId,
                                    createdOn = 1, //createdDate,
                                    CreatedBy = createdBy
                                }, transaction);
                                Query = "";
                            }
                            else if (item.Id > 0)
                            {
                                //To UPDATE CRM.PhysicalFiles
                                Query = "UPDATE CRM.PhysicalFiles SET ";
                                Query = Query + "FileName=@fileName,MimeType=@mimeType,FileDescription=@fileDescription,ThumbUrl=@thumbUrl,UrlPath=@urlPath,";
                                Query = Query + "FileSize=@fileSize,LastUpdatedUtc=@lastUpdatedUtc,LastUpdatedByPersonId=@lastUpdatedByPersonId,FileCategory=@fileCategory ";
                                Query = Query + " WHERE FileName<>@fileName AND FileDescription<>@fileDescription AND ";
                                Query = Query + " FileCategory<>@fileCategory AND PhysicalFileId=@physicalFileId";
                                //
                                var newPhysicalFileId = connection.Query<int>(Query, new
                                {
                                    fileName = item.FileName,
                                    mimeType = System.Web.MimeMapping.GetMimeMapping(TargetFile),
                                    fileDescription = item.Title,
                                    thumbUrl = thmbUrl,
                                    urlPath = UrlPath,
                                    fileSize = targetPhyFile.Length,
                                    lastUpdatedUtc = createdDate,
                                    lastUpdatedByPersonId = createdBy,
                                    fileCategory = item.Category,
                                    physicalFileId = item.Id
                                }, transaction);
                                Query = "";
                            }
                        }
                        catch (Exception ex) { }
                    }
                    else
                    {
                        //To save to Opportunities Notes
                        if (item.Category == "Internal") { TypeEnum = 1; }
                        else if (item.Category == "Customer") { TypeEnum = 2; }
                        else if (item.Category == "Direction") { TypeEnum = 3; }
                        else { TypeEnum = 0; }

                        if (item.Id == 0)
                        {
                            Query = "INSERT INTO CRM.OpportunityNotes ";
                            Query = Query + "(OpportunityId,Message,CreatedOn,CreatedByPersonId,TypeEnum,Title) ";
                            Query = Query + "VALUES (@OpportunityId,@Message,@CreatedOn,@CreatedByPersonId,@TypeEnum,@Title)";
                            //Stored to database
                            connection.Execute(Query, new
                            {
                                OpportunityId = opportunityId,
                                Message = item.Note,
                                createdOn = createdDate,
                                CreatedByPersonId = createdBy,
                                TypeEnum = TypeEnum,
                                Title = item.Title
                            }, transaction);
                        }
                        else if (item.Id > 0)
                        {
                            Query = "UPDATE CRM.OpportunityNotes SET ";
                            Query = Query + "Message=@message, TypeEnum=@typeEnum, Title=@title ";
                            Query = Query + "WHERE Message<>@message AND TypeEnum<>@typeEnum AND ";
                            Query = Query + "NoteId=@noteId AND Title<>@title";
                            //Stored to database
                            connection.Execute(Query, new
                            {
                                noteId = item.Id,
                                message = item.Note,
                                typeEnum = TypeEnum,
                                title = item.Title
                            }, transaction);
                        }
                    }
                }
            }
            return Success;
        }

        #endregion ---- Touhchpoint ----

        public List<OpportunitySource> GetOpportunitySources(ICollection<LeadSource> leadsources)
        {
            List<OpportunitySource> opsources = new List<OpportunitySource>();
            return opsources;
        }

        public Opportunity CopyLeadModelToOpportunity(Lead LeadModel, string opportunityName, int salesAgentId)
        {
            Opportunity opportunitymodel = new Opportunity();

            opportunitymodel.CampaignId = LeadModel.CampaignId;
            opportunitymodel.CampaignName = LeadModel.CampaignName;

            opportunitymodel.OpportunitySourceId = LeadModel.leadSourceId;
            opportunitymodel.SourcesTPId = LeadModel.SourcesTPId;
            opportunitymodel.SourcesTPIdFromCampaign = LeadModel.SourcesTPIdFromCampaign;

            opportunitymodel.OpportunityName = opportunityName;
            opportunitymodel.SalesAgentId = salesAgentId;

            opportunitymodel.TerritoryId = LeadModel.TerritoryId;
            opportunitymodel.TerritoryDisplayId = LeadModel.TerritoryDisplayId;
            opportunitymodel.TerritoryType = LeadModel.TerritoryType;

            //Set Questions Answers
            opportunitymodel.OpportunityQuestionAns = LeadModel.LeadQuestionAns;

            //Set Account to opportunity while lead convert
            using (SqlConnection connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = @"SELECT * FROM crm.Accounts a WHERE a.LeadId= @LeadId AND a.FranchiseId = @FranchiseId";
                AccountTP account = connection.Query<AccountTP>(query, new { LeadId = LeadModel.LeadId, FranchiseId=SessionManager.CurrentUser.FranchiseId }).FirstOrDefault();
                if (account != null)
                {
                    opportunitymodel.AccountId = account.AccountId;
                    if ((account.IsTaxExempt ||
                    account.IsPSTExempt ||
                    account.IsGSTExempt ||
                    account.IsHSTExempt ||
                    account.IsVATExempt) && account.AllOpportunityTaxExempt)
                    {
                    opportunitymodel.IsTaxExempt = account.IsTaxExempt;
                    opportunitymodel.TaxExemptID = account.TaxExemptID;
                    opportunitymodel.IsPSTExempt = account.IsPSTExempt;
                    opportunitymodel.IsGSTExempt = account.IsGSTExempt;
                    opportunitymodel.IsHSTExempt = account.IsHSTExempt;
                    opportunitymodel.IsVATExempt = account.IsVATExempt;
                    }
                    else
                    {
                            opportunitymodel.IsTaxExempt = false;
                            opportunitymodel.TaxExemptID = null;
                            opportunitymodel.IsPSTExempt = false;
                            opportunitymodel.IsGSTExempt = false;
                            opportunitymodel.IsHSTExempt = false;
                            opportunitymodel.IsVATExempt = false;
                    }
                }
            }

            if (LeadModel.Addresses.Count > 0)
            {
                var address = LeadModel.Addresses.FirstOrDefault();
                opportunitymodel.InstallationAddressId = address.AddressId;
                opportunitymodel.BillingAddressId = address.AddressId;
                opportunitymodel.InstallationAddress = "";
            }

            opportunitymodel.Description = "Opportunity Created From Lead";
            opportunitymodel.OpportunityStatusId = 3;

            return opportunitymodel;
        }

        public string ConvertLeadToOpportunity(int brandId, int LeadId, string newAccountStatus, string opportunityName, int salesAgentId, int accountId)
        {
            int opportunityId = 0;
            Lead model = LeadsMgr.Get(LeadId);
            Opportunity opportunitymodel = new Opportunity();

            opportunitymodel = CopyLeadModelToOpportunity(model, opportunityName, salesAgentId);

            //For New Opportunity converted from lead the status should be Qualified as per the specifications
            opportunitymodel.OpportunityStatusId = 1;//Qualified

            if (opportunitymodel.SourcesTPIdFromCampaign > 0)
                opportunitymodel.SourcesTPId = opportunitymodel.SourcesTPIdFromCampaign;
            //var status = CreateOpportunity(out opportunityId, opportunitymodel, LeadId, accountId);
            var status = CreateOpportunity(out opportunityId, opportunitymodel, LeadId);
            return status;
            //return ResponseResult(status, Json(new { OpportunityId = opportunityId }));
        }

        public string UpdateOpportunityStatus(int opportunityId, int opportunityStatus)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    //var quote = connection.Query<Quote>("select * from CRM.Quote where  QuoteKey = @QuoteKey", new { QuoteKey = quoteKey }).FirstOrDefault();
                    var query = @"Update CRM.Opportunities set OpportunityStatusId=@statusId where OpportunityId =@opportunityId";
                    var result = ExecuteIEnumerableObject<int>(ContextFactory.CrmConnectionString, query, new
                    {
                        statusId = opportunityStatus,
                        opportunityId = opportunityId
                    });
                    return "Sucessful";
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    throw e;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        /// <summary>
        /// Gets list of Opportunities for an Account
        /// </summary>
        /// <param name="accountId">AccountId to which matching opportunities
        /// should return</param>
        /// <returns></returns>
        public List<Opportunity> GetOpportunities(int accountId)
        {
            var opps = new List<Opportunity>();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                try
                {
                    var query = @"select * from crm.Opportunities
                                    where AccountId = @accountid";
                    opps = connection.Query<Opportunity>(query,
                        new { accountid = accountId }).ToList();
                }
                finally
                {
                    connection.Close();
                }
            }

            return opps;
        }

        public void UpdateTaxExemption(Opportunity data)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                try
                {
                    var query = @"update crm.Opportunities set
                                    IsTaxExempt = @istaxexempt
                                    , TaxExemptID = @taxexemptid
                                    , IsGSTExempt = @gst
                                    , IsHSTExempt = @hst
                                    , IsPSTExempt = @pst
                                    , IsVATExempt = @vat
                                where OpportunityId =@opportunityid;";
                    connection.Execute(query,
                        new
                        {
                            istaxexempt = data.IsTaxExempt,
                            taxexemptid = data.TaxExemptID,
                            gst = data.IsGSTExempt,
                            hst = data.IsHSTExempt,
                            pst = data.IsPSTExempt,
                            vat = data.IsVATExempt,
                            opportunityid = data.OpportunityId
                        });
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public AddressTP GetAddress(int AddressId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                try
                {
                    var query = @"select * from CRM.Addresses where AddressId = @AddressId";
                    var result = connection.Query<AddressTP>(query, new { AddressId = AddressId }).FirstOrDefault();
                    if (result != null)
                    {
                        return result;
                    }
                    else return null;
                }
                finally
                {
                    connection.Close();
                }
           }
        }

        //getting opportunity detail using opportunityid for pdf naming
        public string GetOppbyOppId(int OpportunityId, bool IsPacket)
        {
            string fileDownloadName = string.Empty;
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var query = @"select (select [CRM].[fnGetAccountNameByAccountId](op.AccountId)) as OpportunityName,op.CreatedOnUtc, op.OpportunityNumber from crm.Opportunities  op
                                          where op.OpportunityId = @OpportunityId";
                    var data = connection.Query<Opportunity>(query, new { OpportunityId = OpportunityId }).FirstOrDefault();
                    if (data.CreatedOnUtc != null)
                        data.CreatedOnUtc = TimeZoneManager.ToLocal(data.CreatedOnUtc);
                    if (IsPacket)
                        fileDownloadName = string.Format("OPP {0} - {1} - CustomerPacket.pdf", data.OpportunityNumber, data.OpportunityName.Replace("/", "-"));
                    else
                        fileDownloadName = string.Format("OPP {0} - {1} - {2}.pdf", data.OpportunityNumber, data.OpportunityName.Replace("/", "-"), data.CreatedOnUtc.ToString("MM-dd-yyyy"));

                    return fileDownloadName;
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    return fileDownloadName;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public dynamic GetSalesAgents()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = @"select distinct per.personid as SalesAgentId,FirstName + ' ' + LastName  as Name ,per.PrimaryEmail as Email
                                    from [Auth].[Users] usr
                                    join [CRM].[Person] per on per.personid = usr.personid
                                    join [Auth].[UsersInRoles] uir on usr.userid = uir.userid
                                    where usr.franchiseid = @FranchiseId and uir.roleid in( '9CFE899F-A036-4D69-A58B-A1BFBDD8DCA0','06547EFF-24AA-454B-A399-9017009B3FBD') and usr.IsDeleted!=1 and (usr.IsDisabled=0 or usr.IsDisabled is null)
                                    order by Name";
                var result = connection.Query(query, new { FranchiseId = Franchise.FranchiseId }).ToList();
                return result;
            }
        }

        public dynamic GetInstallers()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = @"select distinct per.personid as InstallerId,FirstName + ' ' + LastName  as Name ,per.PrimaryEmail as Email
                                from [Auth].[Users] usr
                                join [CRM].[Person] per on per.personid = usr.personid
                                join [Auth].[UsersInRoles] uir on usr.userid = uir.userid
                                where usr.franchiseid = @franchiseid and uir.roleid = '3F1154E2-0718-40DC-8F8E-75DCA2183AC1' and usr.IsDeleted!=1 and (usr.IsDisabled=0 or usr.IsDisabled is null)
                                order by Name";
                connection.Open();
                var result = connection.Query(query, new { franchiseid = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                connection.Close();
                return result;
            }
        }

        public dynamic OpportunitiesStatus()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = @"select OpportunityStatusID,Name from [CRM].[Type_OpportunityStatus]
                            order by Name";
                connection.Open();
                var result = connection.Query(query).ToList();
                connection.Close();
                return result;
            }
        }

        public dynamic GetAccounts()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = @"SELECT
                             Account.[AccountId] as accountId,
                              (select [CRM].[fnGetAccountNameByAccountId](Account.AccountId)) as  Name
                             FROM [crm].[Accounts] Account
                             INNER JOIN CRM.Type_AccountStatus tls ON tls.Accountstatusid = Account.Accountstatusid
                             INNER JOIN [CRM].[Addresses] a
                             on a.[AddressId]=(select top 1 la.AddressId from crm.AccountAddresses la where la.AccountId = Account.AccountId)
                             INNER JOIN [CRM].[Customer] p
                             on p.CustomerId=Account.[PersonId]
                             LEFT JOIN [CRM].[Customer] secondPerson
                             on secondPerson.CustomerId = Account.SecPersonId
                             WHERE Account.FranchiseId= @franchiseid AND ISNULL(Account.IsDeleted,0)=0 order by isnull(Account.LastUpdatedOnUtc,Account.CreatedOnUtc) desc";
                connection.Open();
                var result = connection.Query(query, new { franchiseid = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                connection.Close();
                return result;
            }
        }

        public dynamic getAccountName(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var result = connection.Query(@"select op.AccountId as accountId,
                                                (select [CRM].[fnGetAccountNameByAccountId](acc.AccountId)) as Name from[crm].AccountCustomers op
                                                Inner join[CRM].Customer customer on customer.CustomerId = op.CustomerId
                                                Inner join CRM.Accounts acc on op.AccountId=acc.AccountId
                                                where op.AccountId = @id and op.IsPrimaryCustomer = 1",
                                                new { id = id }).FirstOrDefault();
                connection.Close();
                return result;
            }
        }

        public dynamic getSalesAgentName(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                var result = connection.Query("select FirstName + ' ' + LastName  as Name FROM [CRM].[Person]  where PersonId=@id", new { id = id }).FirstOrDefault();
                connection.Close();
                return result;
            }
        }

        public dynamic getInstalltionAddressName(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select IsValidated, AddressId as InstallAddressId,
                            adr.Address1,adr.Address1,adr.City,adr.State,adr.ZipCode,
                            adr.CountryCode2Digits,
                            adr.ContactId,cus.FirstName+' '+cus.LastName as ContactName,
                            ISNULL(Address1,'') + ' ' + ISNULL(Address2,'') + ' ' + ISNULL(City,'') + ' ' + ISNULL(State,'') + ' ' + ISNULL(ZipCode,'') as Name
                            from [CRM].[Addresses] adr left join CRM.Customer cus
                            on adr.ContactId = cus.CustomerId where AddressId=@id";
                var result = connection.Query(query, new { id = id }).FirstOrDefault();
                connection.Close();
                return result;
            }
        }

        public dynamic InstallationAddresses(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = string.Empty;
                if (id > 0)
                {
                    query = @"select a.AccountId as RelatedAccountId,0 as AccountId, ad.IsValidated, ad.AddressId as InstallAddressId,
                                cu.FirstName+' '+cu.LastName +' - '+  ISNULL(Address1,'') + ' ' + ISNULL(Address2,'') + ' ' + ISNULL(City,'') + ' ' + ISNULL(State,'') + ' ' + ISNULL(ZipCode,'') as Name,
                                A.TaxExemptID,A.IsTaxExempt,A.IsPSTExempt,A.IsGSTExempt,A.IsHSTExempt,A.IsVATExempt
                                from CRM.Accounts A
                                join CRM.AccountCustomers accu on a.AccountId=accu.AccountId and accu.IsPrimaryCustomer=1
                                join CRM.Customer cu on accu.CustomerId=cu.CustomerId
                                JOIN CRM.AccountAddresses acad ON A.AccountId=acad.AccountId
                                JOIN CRM.Addresses ad on acad.AddressId=ad.AddressId and ad.IsDeleted=0
                                where a.AccountId = (select RelatedAccountId from CRM.Accounts where AccountId = @id)
                                union
                                select a.RelatedAccountId,a.AccountId, ad.IsValidated, ad.AddressId as InstallAddressId,
                                ISNULL(Address1,'') + ' ' + ISNULL(Address2,'') + ' ' + ISNULL(City,'') + ' ' + ISNULL(State,'') + ' ' + ISNULL(ZipCode,'') as Name,
                                A.TaxExemptID,A.IsTaxExempt,A.IsPSTExempt,A.IsGSTExempt,A.IsHSTExempt,A.IsVATExempt                                
                                from CRM.Accounts A
                                JOIN CRM.AccountAddresses acad ON A.AccountId=acad.AccountId
                                JOIN CRM.Addresses ad on acad.AddressId=ad.AddressId and ad.IsDeleted=0
                                where a.AccountId = @id
                                order by a.AccountId";
                    var result = connection.Query(query, new { id = id }).ToList();
                    connection.Close();
                    return result;
                }
                else
                {
                    query = @"select * from(select  acad.AccountId,ad.IsValidated, ad.AddressId as InstallAddressId,a.FranchiseId,     ROW_NUMBER() OVER(PARTITION BY ad.AddressId ORDER BY ad.AddressId DESC) rn,
                                       ISNULL(Address1,'') + ' ' + ISNULL(Address2,'') + ' ' + ISNULL(City,'') + ' ' + ISNULL(State,'') + ' ' + ISNULL(ZipCode,'') as Name
                                    from CRM.Addresses ad inner join crm.AccountAddresses acad on acad.AddressId = ad.AddressId left
                                    join crm.Accounts a on a.AccountId = acad.AccountId  ) a
                                    where a.Name is not null and FranchiseId = @FranchiseId and rn= 1
                                    order by Name";
                    var result = connection.Query(query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                    connection.Close();
                    return result;
                }
            }
        }

        public List<EmailPhone> getPhoneEmail(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                List<EmailPhone> data = new List<EmailPhone>();

                var result = connection.Query<EmailPhone>(@"select customer.PrimaryEmail,
								case when op.AccountId is not null
                          		then
                          		(select
                          		case when PreferredTFN='C' then CellPhone
                          		else case when PreferredTFN='H' then HomePhone
                          		else case when PreferredTFN='W' then WorkPhone
                          		else case when CellPhone is not null then CellPhone
                          		else case when HomePhone is not null then HomePhone
                          		else case when WorkPhone is not null then WorkPhone
                          		else ''
                          		end end end end end end
                          		from CRM.Customer cus
                          		join CRM.AccountCustomers acus on cus.CustomerId=acus.CustomerId and IsPrimaryCustomer=1
                          		where acus.AccountId=op.AccountId) end as AccountPhone,
                                (select [CRM].[fnGetAccountNameByAccountId](acc.AccountId)) as Name from[crm].AccountCustomers op
                                Inner join [CRM].Customer customer on customer.CustomerId = op.CustomerId
                                Inner join CRM.Accounts acc on op.AccountId=acc.AccountId
								Inner join CRM.Opportunities opp on opp.AccountId = acc.AccountId
                                where opp.OpportunityId =@id and op.IsPrimaryCustomer = 1",
                                                new { id = id }).FirstOrDefault();

                data.Add(result);
                var result1 = connection.Query<EmailPhone>(@" select Customer.PrimaryEmail,
                                                             (select 
                                                             case when PreferredTFN='C' then CellPhone
                                                             else case when PreferredTFN='H' then HomePhone 
                                                             else case when PreferredTFN='W' then WorkPhone
                                                             else case when CellPhone is not null then CellPhone 
                                                             else case when HomePhone is not null then HomePhone 
                                                             else case when WorkPhone is not null then WorkPhone 
                                                             else '' 
                                                             end end end end end end) as AccountPhone,'' as Name
                                                              from CRM.Customer Customer
                                                               where Customer.CustomerId in (
                                                             select  
                                                             case when ISNULL(insad.ContactId,0)>0 then ContactId else 
                                                             (select CustomerId from [CRM].[AccountCustomers] where AccountId=op.AccountId and IsPrimaryCustomer=1) end
                                                             from CRM.Opportunities op
                                                             join CRM.Addresses insad on op.BillingAddressId=insad.AddressId
                                                             where op.OpportunityId=@id
                                                             )",
                                               new { id = id }).FirstOrDefault();
                data.Add(result1);

                var result2 = connection.Query<EmailPhone>(@" 		  select Customer.PrimaryEmail,
                                                             (select 
                                                             case when PreferredTFN='C' then CellPhone
                                                             else case when PreferredTFN='H' then HomePhone 
                                                             else case when PreferredTFN='W' then WorkPhone
                                                             else case when CellPhone is not null then CellPhone 
                                                             else case when HomePhone is not null then HomePhone 
                                                             else case when WorkPhone is not null then WorkPhone 
                                                             else '' 
                                                             end end end end end end) as AccountPhone, '' as Name
                                                              from CRM.Customer Customer
                                                               where Customer.CustomerId in (
                                                             select  
                                                             case when ISNULL(insad.ContactId,0)>0 then ContactId else 
                                                             (select CustomerId from [CRM].[AccountCustomers] where AccountId=op.AccountId and IsPrimaryCustomer=1) end
                                                             from CRM.Opportunities op 
                                                             join CRM.Addresses insad on op.InstallationAddressId=insad.AddressId
                                                             where op.OpportunityId=@id
                                                             )",
                                              new { id = id }).FirstOrDefault();
                data.Add(result2);
                connection.Close();
                return data;
            }
        }

        public List<EmailPhone> getPhoneEmailforOrder(int id) {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                List<EmailPhone> data = new List<EmailPhone>();

                var result1 = connection.Query<EmailPhone>(@" select Customer.PrimaryEmail,
                             (select 
                             case when PreferredTFN='C' then CellPhone
                             else case when PreferredTFN='H' then HomePhone 
                             else case when PreferredTFN='W' then WorkPhone
                             else case when CellPhone is not null then CellPhone 
                             else case when HomePhone is not null then HomePhone 
                             else case when WorkPhone is not null then WorkPhone 
                             else '' 
                             end end end end end end) as AccountPhone,'' Name 
                              from CRM.Customer Customer
                               where Customer.CustomerId in (
                             select  
                             case when ISNULL(insad.ContactId,0)>0 then ContactId else 
                             (select CustomerId from [CRM].[AccountCustomers] where AccountId=op.AccountId and IsPrimaryCustomer=1) end
                             from CRM.Orders o
                             join CRM.Opportunities op on o.OpportunityId=op.OpportunityId
                             join CRM.Addresses insad on op.BillingAddressId=insad.AddressId
                             where o.OrderID=@id)",
                             new { id = id }).FirstOrDefault();
                data.Add(result1);

                var result2 = connection.Query<EmailPhone>(@" select Customer.PrimaryEmail,
                             (select 
                             case when PreferredTFN='C' then CellPhone
                             else case when PreferredTFN='H' then HomePhone 
                             else case when PreferredTFN='W' then WorkPhone
                             else case when CellPhone is not null then CellPhone 
                             else case when HomePhone is not null then HomePhone 
                             else case when WorkPhone is not null then WorkPhone 
                             else '' 
                             end end end end end end) as AccountPhone,'' Name 
                              from CRM.Customer Customer
                               where Customer.CustomerId in (
                             select  
                             case when ISNULL(insad.ContactId,0)>0 then ContactId else 
                             (select CustomerId from [CRM].[AccountCustomers] where AccountId=op.AccountId and IsPrimaryCustomer=1) end
                             from CRM.Orders o
                             join CRM.Opportunities op on o.OpportunityId=op.OpportunityId
                             join CRM.Addresses insad on op.InstallationAddressId=insad.AddressId
                             where o.OrderID=@id)",
                             new { id = id }).FirstOrDefault();

                data.Add(result2);
                connection.Close();
                return data;
            }
        }

        public class EmailPhone
        {
            public string PrimaryEmail { get; set; }
            public string AccountPhone { get; set; }
            public string Name { get; set; }
        }
    }
}