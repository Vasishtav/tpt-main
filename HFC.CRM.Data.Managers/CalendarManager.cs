﻿using HFC.CRM.Core.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using HFC.CRM.Core.Common;
using System.Web;
using System.Xml.Linq;
using HFC.CRM.Core.Membership;
using HFC.CRM.Data;
using HFC.CRM.Data.Constants;
using Dapper;
using HFC.CRM.Data.Context;
using System.Data.SqlClient;
using System.Dynamic;
using System.Text;
using HFC.CRM.DTO.Calandar;
using HFC.CRM.Data.Extensions;
using HFC.CRM.Data.SavedCalendar;
using HFC.CRM.DTO;
using HFC.CRM.Core;

/// <summary>
/// The Managers namespace.
/// </summary>
namespace HFC.CRM.Managers
{


    /// <summary>
    /// Class CalendarManager.
    /// </summary>
    public class CalendarManager : IMessageConstants
    {
        /// <summary>
        /// The CRMDB context
        /// </summary>
        private CRMContext CRMDBContext = ContextFactory.Current.Context;

        /// <summary>
        /// Gets the user.
        /// </summary>
        /// <value>The user.</value>
        public User User { get; private set; }

        /// <summary>
        /// Gets the authorizing user.
        /// </summary>
        /// <value>The authorizing user.</value>
        public User AuthorizingUser { get; private set; }

        /// <summary>
        /// Create a new instance of calendar manager to handle CRUD operations
        /// </summary>
        /// <param name="user">The user that is performing the CRUD operation and used to check authorization permission</param>
        public CalendarManager(User user) : this(user, user)
        {
            this.User = user;
        }

        /// <summary>
        /// Create a new instance of calendar manager to handle CRUD operations
        /// </summary>
        /// <param name="authorizingUser">Used to check authorization permission</param>
        /// <param name="user">The user that is performing the CRUD operation</param>
        /// <exception cref="System.ArgumentOutOfRangeException">User must belong to a franchise</exception>
        public CalendarManager(User authorizingUser, User user)
        {
            this.User = user;
            this.AuthorizingUser = authorizingUser;
            // TO DO vandor case throws an issue
            //if (!this.User.FranchiseId.HasValue || this.User.FranchiseId.Value <= 0)
            //    throw new ArgumentOutOfRangeException("User must belong to a franchise");
        }

        private string GetEventQuery()
        {
            var query = @"select distinct c.*,(SELECT Stuff((SELECT ', ' + PersonName FROM CRM.EventToPeople sep
						  left join Auth.Users su on su.PersonId=sep.PersonId
                          where CalendarId=c.CalendarId and isnull(su.IsDisabled,0)=0 order by EventPersonId asc FOR XML PATH('')), 1, 2, '')) AS AttendeesName
                          from crm.calendar c
						  left join CRM.EventToPeople ep on c.CalendarId=ep.CalendarId
						  left join Auth.Users u on u.PersonId=ep.PersonId
                          where c.FranchiseId = @franchiseId and isnull(u.IsDisabled,0)=0 and ISNULL(c.IsDeleted,0) != 1 ";
            return query;
        }

        private string GetCalendarTasksQuery()
        {
            var query = @"select distinct T.*,(SELECT Stuff((SELECT ', ' + PersonName FROM CRM.EventToPeople sep
                          left join Auth.Users su on su.PersonId=sep.PersonId
						  where TaskId=T.TaskId and isnull(su.IsDisabled,0)=0 FOR XML PATH('')), 1, 2, '')) AS AttendeesName
                          from [CRM].[Tasks]T 
						  left join CRM.EventToPeople ep on T.TaskId=ep.TaskId
						  left join Auth.Users u on u.PersonId=ep.PersonId
						  where T.FranchiseId=@franchiseId and isnull(u.IsDisabled,0)=0 and ISNULL(T.IsDeleted,0)!=1 and T.CompletedDate IS NULL ";
            return query;
        }

        private string GetEventQueryForDashboardByFEId()
        {
            var query = @"select distinct c.IsPrivate,c.CalendarId, c.Subject, c.Message,
                            o.InstallationAddressId,
                            ls.Id as LeadStatusId,
                            c.LeadId, c.AccountId, c.OpportunityId, c.OrderId,
                            c.StartDate as StartDateUTC, c.EndDate as EndDateUTC,
                            (SELECT Stuff((SELECT ', ' + PersonName FROM CRM.EventToPeople sep
							left join Auth.Users su on su.PersonId=sep.PersonId
                          where CalendarId=c.CalendarId and isnull(su.IsDisabled,0)=0 order by EventPersonId asc FOR XML PATH('')), 1, 2, '')) AS AttendeesNames,
                            cu.firstname + ' ' + cu.LastName as LeadName,
                            cua.firstname + ' ' + cua.LastName as AccountName,
                           at.name as AppointmentType,
                           o.OpportunityName as OppurtunityName,n.OrderName as OrderName,
                           (case  when n.OrderName is not null then   'Ord-'+n.OrderName
							        when o.OpportunityName is not null then   'Opp-'+o.OpportunityName
									when cua.firstname + ' ' + cua.LastName is not null then   'Acct-'+cua.firstname + ' ' + cua.LastName
									when  cu.firstname + ' ' + cu.LastName is not null then   'Lead-'+cu.firstname + ' ' + cu.LastName
							        else '' end )as Related
                          from crm.calendar c
                            left join crm.leadcustomers lc on lc.LeadId = c.LeadId and IsPrimaryCustomer = 1
	                        left join crm.customer cu on lc.CustomerId = cu.CustomerId
                            left join crm.accountcustomers ac on ac.AccountId = c.AccountId and ac.IsPrimaryCustomer = 1
	                        left join crm.customer cua on cua.CustomerId = ac.CustomerId
                            left join crm.type_appointments at on at.AppointmentTypeId = c.AptTypeEnum
                            left join crm.leads l on l.LeadId = c.LeadId
                            left join crm.type_leadstatus ls on ls.id = l.LeadStatusId
                            left join crm.Opportunities o on o.OpportunityId = c.OpportunityId
                            left join crm.Orders n on n.OrderID=c.OrderId
							left join CRM.EventToPeople ep on c.CalendarId=ep.CalendarId
							join Auth.Users u on u.PersonId=ep.PersonId
                            where c.FranchiseId = @franchiseId 
							and isnull(u.IsDisabled,0)=0
                            and c.EventTypeEnum != 2
                            and ISNULL(c.IsDeleted,0) != 1
                            and (c.StartDate between @sdate and @edate 
							or c.EndDate between @sdate and @edate
							or (c.StartDate<=@sdate and c.EndDate>=@edate))
                            and ISNULL(c.IsCancelled,0) != 1 order by StartDateUTC";
            return query;
        }

        private string GetEventQueryForDashboardByPersonId()
        {
            return @"select distinct c.IsPrivate,c.CalendarId, c.Subject, c.Message,
                            o.InstallationAddressId,
                            ls.Id as LeadStatusId,
                            c.LeadId, c.AccountId, c.OpportunityId, c.OrderId,
                            c.StartDate as StartDateUTC, c.EndDate as EndDateUTC,
                            (SELECT Stuff((SELECT ', ' + PersonName FROM CRM.EventToPeople sep
							left join Auth.Users su on su.PersonId=sep.PersonId
                          where CalendarId=c.CalendarId and isnull(su.IsDisabled,0)=0 order by EventPersonId asc FOR XML PATH('')), 1, 2, '')) AS AttendeesNames,
                            cu.firstname + ' ' + cu.LastName as LeadName,
                            cua.firstname + ' ' + cua.LastName as AccountName,
                           at.name as AppointmentType,
                           o.OpportunityName as OppurtunityName,n.OrderName as OrderName,
                           (case  when n.OrderName is not null then   'Ord-'+n.OrderName
							        when o.OpportunityName is not null then   'Opp-'+o.OpportunityName
									when cua.firstname + ' ' + cua.LastName is not null then   'Acct-'+cua.firstname + ' ' + cua.LastName
									when  cu.firstname + ' ' + cu.LastName is not null then   'Lead-'+cu.firstname + ' ' + cu.LastName
							        else '' end )as Related
                          from crm.calendar c
                            left join crm.leadcustomers lc on lc.LeadId = c.LeadId and IsPrimaryCustomer = 1
	                        left join crm.customer cu on lc.CustomerId = cu.CustomerId
                            left join crm.accountcustomers ac on ac.AccountId = c.AccountId and ac.IsPrimaryCustomer = 1
	                        left join crm.customer cua on cua.CustomerId = ac.CustomerId
                            left join crm.type_appointments at on at.AppointmentTypeId = c.AptTypeEnum
                            left join crm.leads l on l.LeadId = c.LeadId
                            left join crm.type_leadstatus ls on ls.id = l.LeadStatusId
                            left join crm.Opportunities o on o.OpportunityId = c.OpportunityId
                            left join crm.Orders n on n.OrderID=c.OrderId
							left join CRM.EventToPeople ep on c.CalendarId=ep.CalendarId
							left join Auth.Users u on u.PersonId=ep.PersonId
                            where c.FranchiseId = @franchiseId 
							and ep.PersonId=@personId
							and isnull(u.IsDisabled,0)=0
                            and c.EventTypeEnum != 2
                            and (c.StartDate between @sdate and @edate 
							or c.EndDate between @sdate and @edate
							or (c.StartDate<=@sdate and c.EndDate>=@edate))
							and ISNULL(c.IsDeleted,0) != 1
                            and ISNULL(c.IsCancelled,0) != 1";
        }

        private string GetRecurringEventDatabyFEId()
        {
            return @"select distinct c.* from CRM.Calendar c
            join CRM.EventRecurring er on c.RecurringEventId=er.RecurringEventId
			left join CRM.EventToPeople ep on c.CalendarId=ep.CalendarId
			left join Auth.Users u on u.PersonId=ep.PersonId
            where c.FranchiseId=@franchiseId 
            and c.EventTypeEnum=2
			and ISNULL(u.IsDisabled,0)=0
            and ISNULL(c.IsDeleted,0) != 1
            and ISNULL(c.IsCancelled,0) != 1
            
            select distinct er.* from CRM.Calendar c
            join CRM.EventRecurring er on c.RecurringEventId=er.RecurringEventId
			left join CRM.EventToPeople ep on c.CalendarId=ep.CalendarId
			left join Auth.Users u on u.PersonId=ep.PersonId
            where c.FranchiseId=@franchiseId 
            and c.EventTypeEnum=2
			and ISNULL(u.IsDisabled,0)=0
            and ISNULL(c.IsDeleted,0) != 1
            and ISNULL(c.IsCancelled,0) != 1

			select distinct cso.* from CRM.Calendar c
			join CRM.CalendarSingleOccurrences cso on c.RecurringEventId=cso.RecurringEventId
			left join CRM.EventToPeople ep on c.CalendarId=ep.CalendarId
			left join Auth.Users u on u.PersonId=ep.PersonId
			where c.FranchiseId=@franchiseId 
			and c.EventTypeEnum=2
			and ISNULL(u.IsDisabled,0)=0
            and ISNULL(c.IsDeleted,0) != 1
            and ISNULL(c.IsCancelled,0) != 1";
        }

        private string GetRecurringEventDatabyPersonId()
        {
            return @"select c.* from CRM.Calendar c
            join CRM.EventRecurring er on c.RecurringEventId=er.RecurringEventId
			join CRM.EventToPeople ep on c.CalendarId=ep.CalendarId
            where c.FranchiseId=@franchiseId 
			and ep.PersonId=@personId
            and c.EventTypeEnum=2
            and ISNULL(c.IsDeleted,0) != 1
            and ISNULL(c.IsCancelled,0) != 1
            
            select er.* from CRM.Calendar c
            join CRM.EventRecurring er on c.RecurringEventId=er.RecurringEventId
			join CRM.EventToPeople ep on c.CalendarId=ep.CalendarId
            where c.FranchiseId=@franchiseId 
			and ep.PersonId=@personId
            and c.EventTypeEnum=2
            and ISNULL(c.IsDeleted,0) != 1
            and ISNULL(c.IsCancelled,0) != 1

			select cso.* from CRM.Calendar c
			join CRM.CalendarSingleOccurrences cso on c.RecurringEventId=cso.RecurringEventId
			join CRM.EventToPeople ep on c.CalendarId=ep.CalendarId
			where c.FranchiseId=@franchiseId 
			and ep.PersonId=@personId
			and c.EventTypeEnum=2
            and ISNULL(c.IsDeleted,0) != 1
            and ISNULL(c.IsCancelled,0) != 1";
        }

        private string GetEventListByCalendarId()
        {
            return @"select distinct c.IsPrivate,c.CalendarId, c.Subject, c.Message,
                 o.InstallationAddressId,
                 ls.Id as LeadStatusId,
                 c.LeadId, c.AccountId, c.OpportunityId, c.OrderId,
                 c.StartDate as StartDateUTC, c.EndDate as EndDateUTC,
                 (SELECT Stuff((SELECT ', ' + PersonName FROM CRM.EventToPeople sep
							left join Auth.Users su on su.PersonId=sep.PersonId
                          where CalendarId=c.CalendarId and isnull(su.IsDisabled,0)=0 order by EventPersonId asc FOR XML PATH('')), 1, 2, '')) AS AttendeesNames,
                 cu.firstname + ' ' + cu.LastName as LeadName,
                 cua.firstname + ' ' + cua.LastName as AccountName,
                at.name as AppointmentType,
                o.OpportunityName as OppurtunityName,n.OrderName as OrderName,
                (case  when n.OrderName is not null then   'Ord-'+n.OrderName
	            	        when o.OpportunityName is not null then   'Opp-'+o.OpportunityName
	            			when cua.firstname + ' ' + cua.LastName is not null then   'Acct-'+cua.firstname + ' ' + cua.LastName
	            			when  cu.firstname + ' ' + cu.LastName is not null then   'Lead-'+cu.firstname + ' ' + cu.LastName
	            	        else '' end )as Related
               from crm.calendar c
                 left join crm.leadcustomers lc on lc.LeadId = c.LeadId and IsPrimaryCustomer = 1
                 left join crm.customer cu on lc.CustomerId = cu.CustomerId
                 left join crm.accountcustomers ac on ac.AccountId = c.AccountId and ac.IsPrimaryCustomer = 1
                 left join crm.customer cua on cua.CustomerId = ac.CustomerId
                 left join crm.type_appointments at on at.AppointmentTypeId = c.AptTypeEnum
                 left join crm.leads l on l.LeadId = c.LeadId
                 left join crm.type_leadstatus ls on ls.id = l.LeadStatusId
                 left join crm.Opportunities o on o.OpportunityId = c.OpportunityId
                 left join crm.Orders n on n.OrderID=c.OrderId
				 left join CRM.EventToPeople ep on c.CalendarId=ep.CalendarId
				 left join Auth.Users u on u.PersonId=ep.PersonId
        where c.CalendarId in @CalendarId and isnull(u.IsDisabled,0)=0 order by StartDateUTC";
        }

        private string GetTaskQueryForDashboard()
        {
            var query = @"select distinct t.IsPrivate, t.TaskId, t.Subject,
                            o.InstallationAddressId,
                            ls.Id as LeadStatusId,
                            t.LeadId, t.AccountId, t.OpportunityId, t.OrderId,
                            CAST(t.DueDate as datetime2)as DueDate, t.Message,
                            (SELECT Stuff((SELECT ', ' + PersonName FROM CRM.EventToPeople sep
							left join Auth.Users su on su.PersonId=sep.PersonId
                          where TaskId=t.TaskId and isnull(su.IsDisabled,0)=0 order by EventPersonId asc FOR XML PATH('')), 1, 2, '')) AS AttendeesNames,
                            cu.firstname + ' ' + cu.LastName as LeadName,
                            cua.firstname + ' ' + cua.LastName as AccountName,
                            o.OpportunityName as OppurtunityName,n.OrderName as OrderName,t.CompletedDate as completed,
                            (case  when n.OrderName is not null then   'Ord-'+n.OrderName
							        when o.OpportunityName is not null then   'Opp-'+o.OpportunityName
									when cua.firstname + ' ' + cua.LastName is not null then   'Acct-'+cua.firstname + ' ' + cua.LastName
									when  cu.firstname + ' ' + cu.LastName is not null then   'Lead-'+cu.firstname + ' ' + cu.LastName
							        else '' end )as Related
                            from crm.Tasks t
                            left join crm.leadcustomers lc on lc.LeadId = t.LeadId and IsPrimaryCustomer = 1
	                        left join crm.customer cu on lc.CustomerId = cu.CustomerId
                            left join crm.accountcustomers ac on ac.AccountId = t.AccountId and ac.IsPrimaryCustomer = 1
	                        left join crm.customer cua on cua.CustomerId = ac.CustomerId
                            left join crm.leads l on l.LeadId = t.LeadId
	                        left join crm.type_leadstatus ls on ls.id = l.LeadStatusId
                            left join crm.Opportunities o on o.OpportunityId = t.OpportunityId
                            left join crm.Orders n on n.OrderID=t.OrderId
							left join CRM.EventToPeople ep on t.TaskId=ep.TaskId
							left join Auth.Users u on u.PersonId=ep.PersonId
                          where t.FranchiseId = @franchiseId and ISNULL(t.IsDeleted,0) != 1
                            and CompletedDate is null
                            and t.DueDate=@dueDate
							and isnull(u.IsDisabled,0)=0";
            return query;
        }

        public CalendarVM GetRecentAppointment(int leadId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"SET DATEFORMAT mdy; SELECT TOP 1 * FROM
                                CRM.Calendar where LeadId=@LeadId order by CalendarId desc";

                var leadAppointments = connection.Query<Calendar>(query, new { LeadId = leadId }).ToList();
                connection.Close();

                CalendarVM CalVM = null;

                if (leadAppointments != null && leadAppointments.Count > 0)
                    CalVM = MapTo(leadAppointments.First());

                return CalVM;
            }
        }

        public CalendarVM GetRecentAppointmentForAccount(int accountId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select c.* from crm.Calendar c
                            where c.AccountId = @AccountId
							order by c.CalendarId desc";

                var leadAppointments = connection.Query<HFC.CRM.Data.Calendar>(query, new { AccountId = accountId }).ToList();

                connection.Close();

                CalendarVM CalVM = null;

                if (leadAppointments != null && leadAppointments.Count > 0)
                    CalVM = MapTo(leadAppointments.First());
                return CalVM;
            }
        }

        public CalendarVM GetRecentAppointmentForOpportunity(int opportunityId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select c.* from crm.Calendar c
                            where c.OpportunityId=@OpportunityId
							order by c.CalendarId desc";

                var opportunityAppointments = connection.Query<HFC.CRM.Data.Calendar>(query, new { OpportunityId = opportunityId }).ToList();
                connection.Close();

                CalendarVM CalVM = null;
                if (opportunityAppointments != null && opportunityAppointments.Count > 0)
                    CalVM = MapTo(opportunityAppointments.First());

                return CalVM;
            }
        }

        public List<AppointmentVM> GetAllEvents()
        {
            return GetEvents(GetEventQueryForDashboardByFEId(), GetRecurringEventDatabyFEId());
        }

        public List<AppointmentVM> GetMyEvents()
        {
            return GetEvents(GetEventQueryForDashboardByPersonId(), GetRecurringEventDatabyPersonId());
        }

        private List<AppointmentVM> GetEvents(string basequery, string recquery)
        {
            var result = new List<AppointmentVM>();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                result = connection.Query<AppointmentVM>(basequery,
                   new
                   {
                       franchiseId = SessionManager.CurrentFranchise.FranchiseId,
                       personId = SessionManager.CurrentUser.PersonId,
                       sdate = TimeZoneManager.GetUTCTodaystartdatetime(),
                       edate = TimeZoneManager.GetUTCTodayenddatetime()
                   }).ToList();
            }

            var recresult = GetRecurringEvents(recquery);
            if (recresult.Count > 0)
            {
                result.AddRange(recresult);
                result = result.GroupBy(x => x.CalendarId).Select(y => y.First()).ToList();
            }
            foreach (var item in result)
            {
                item.StartDate = TimeZoneManager.ToLocal(item.StartDateUTC);
                item.EndDate = TimeZoneManager.ToLocal(item.EndDateUTC);
                item.IsPrivateAccess = this.GetAuthorisedPrivateCalendar(item.CalendarId, item.IsPrivate, false);
            }

            if (result != null && result.Count > 0)
                result = result.OrderBy(x => x.StartDate).ToList();

            return result;
        }

        private List<AppointmentVM> GetRecurringEvents(string query)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                List<int> calId = new List<int>();
                var recDetails = connection.QueryMultiple(query, new
                {
                    franchiseId = SessionManager.CurrentFranchise.FranchiseId,
                    personId = SessionManager.CurrentUser.PersonId
                });

                var caldata = recDetails.Read<Calendar>().ToList();
                var calrecdata = recDetails.Read<EventRecurring>().ToList();
                var caloccdata = recDetails.Read<CalendarSingleOccurrence>().ToList();

                foreach (var c in caldata)
                {
                    c.EventRecurring = calrecdata.Where(x => x.RecurringEventId == c.RecurringEventId).FirstOrDefault();
                    DateTime currdate = TimeZoneManager.GetLocal().Date.Add(c.EventRecurring.StartDate.TimeOfDay);
                    var occ = c.EventRecurring.checkOccurrencebyDate(currdate);
                    if (occ != null)
                    {
                        var occdelete = caloccdata.Where(x => x.RecurringEventId == c.RecurringEventId).ToList();
                        if (occdelete != null && occdelete.Count > 0)
                        {
                            var del = occdelete.Where(x => TimeZoneManager.ToLocal(x.OriginalStartDate) == occ.StartDate).FirstOrDefault();
                            if (del == null)
                                calId.Add(c.CalendarId);
                        }
                        else
                            calId.Add(c.CalendarId);
                    }
                }

                if (calId.Count > 0)
                {
                    var eventdata = connection.Query<AppointmentVM>(GetEventListByCalendarId(), new { CalendarId = calId }).ToList();
                    foreach (var ed in eventdata)
                    {
                        ed.StartDateUTC = TimeZoneManager.GetLocal().Date.Add(ed.StartDateUTC.TimeOfDay);
                        ed.EndDateUTC = TimeZoneManager.GetLocal().Date.Add(ed.EndDateUTC.TimeOfDay);

                        if (TimeZoneManager.ToLocal(ed.StartDateUTC).Date != TimeZoneManager.GetLocal().Date)
                            ed.StartDateUTC = ed.StartDateUTC.AddDays(1);
                        if (TimeZoneManager.ToLocal(ed.EndDateUTC).Date != TimeZoneManager.GetLocal().Date)
                            ed.EndDateUTC = ed.EndDateUTC.AddDays(1);
                    }
                    return eventdata;
                }
                return new List<AppointmentVM>();
            }
        }

        public List<AppointmentVM> GetEventsForCase(int Id)
        {
            var query = @"select c.CalendarId, c.Subject,
                            c.StartDate as StartDateUTC, c.EndDate as EndDateUTC,
                            (SELECT Stuff((SELECT ', ' + PersonName FROM CRM.EventToPeople
                          where CalendarId=c.CalendarId FOR XML PATH('')), 1, 2, '')) AS AttendeesNames,c.IsAllDay
                          from crm.calendar c where c.CaseId=@CaseId and c.IsDeleted=0 and ISNULL(c.IsCancelled,0) != 1";

            var result = new List<AppointmentVM>();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<AppointmentVM>(query,
                   new
                   {
                       CaseId = Id
                   }).ToList();

                connection.Close();
            }
            foreach (var item in result)
            {
                item.StartDate = TimeZoneManager.ToLocal(item.StartDateUTC);
                item.EndDate = TimeZoneManager.ToLocal(item.EndDateUTC);
            }
            result = result.OrderBy(x => x.StartDate).ToList();
            return result;
        }

        public List<AppointmentVM> GetEventsForVendorCase(int Id)
        {
            var query = @"select c.CalendarId, c.Subject,
                            c.StartDate as StartDateUTC, c.EndDate as EndDateUTC,
                            (SELECT Stuff((SELECT ', ' + PersonName FROM CRM.EventToPeople
                          where CalendarId=c.CalendarId FOR XML PATH('')), 1, 2, '')) AS AttendeesNames
                          from crm.calendar c where c.VendorCaseId=@VendorCaseId and c.IsDeleted=0 and ISNULL(c.IsCancelled,0) != 1";

            var result = new List<AppointmentVM>();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<AppointmentVM>(query,
                   new
                   {
                       VendorCaseId = Id
                   }).ToList();

                connection.Close();
            }
            foreach (var item in result)
            {
                item.StartDate = TimeZoneManager.ToLocal(item.StartDateUTC);
                item.EndDate = TimeZoneManager.ToLocal(item.EndDateUTC);
            }

            return result;
        }

        public List<CalendarTasks> GetTasksForCase(int Id)
        {
            var query = @"select t.TaskId, t.Subject,
                            o.InstallationAddressId,
                            ls.Id as LeadStatusId,
                            t.LeadId, t.AccountId, t.OpportunityId, t.OrderId,t.CaseId,t.CompletedDate,
                            CAST(t.DueDate as datetime2)as DueDate, t.Message,
                            (SELECT Stuff((SELECT ', ' + PersonName FROM CRM.EventToPeople
                          where TaskId=t.TaskId FOR XML PATH('')), 1, 2, '')) AS AttendeesName,
                            cu.firstname + ' ' + cu.LastName as LeadName,
                            cua.firstname + ' ' + cua.LastName as AccountName,
                            o.OpportunityName as OppurtunityName,n.OrderName as OrderName , t.CompletedDate as completed,
                            (case  when n.OrderName is not null then   'Ord-'+n.OrderName
							        when o.OpportunityName is not null then   'Opp-'+o.OpportunityName
									when cua.firstname + ' ' + cua.LastName is not null then   'Acct-'+cua.firstname + ' ' + cua.LastName
									when  cu.firstname + ' ' + cu.LastName is not null then   'Lead-'+cu.firstname + ' ' + cu.LastName
							        else '' end )as Related
                          from crm.Tasks t
                            left join crm.leadcustomers lc on lc.LeadId = t.LeadId and IsPrimaryCustomer = 1
	                        left join crm.customer cu on lc.CustomerId = cu.CustomerId
                            left join crm.accountcustomers ac on ac.AccountId = t.AccountId and ac.IsPrimaryCustomer = 1
	                        left join crm.customer cua on cua.CustomerId = ac.CustomerId
                            left join crm.leads l on l.LeadId = t.LeadId
	                        left join crm.type_leadstatus ls on ls.id = l.LeadStatusId
                            left join crm.Opportunities o on o.OpportunityId = t.OpportunityId
                            left join crm.Orders n on n.OrderID=t.OrderId
                          where t.CaseId=@CaseId and t.IsDeleted <> 1";
            //var query = @"select c.TaskId, c.Subject,
            //                c.DueDate as DueDate, c.Message as Message,
            //                (SELECT Stuff((SELECT ', ' + PersonName FROM CRM.EventToPeople
            //              where TaskId=c.TaskId FOR XML PATH('')), 1, 2, '')) AS AttendeesName
            //              from crm.Tasks c where c.CaseId=@CaseId";

            var result = new List<CalendarTasks>();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<CalendarTasks>(query,
                   new
                   {
                       CaseId = Id
                   }).ToList();

                connection.Close();
            }
            //foreach (var item in result)
            //{
            //  //  item.DueDate = TimeZoneManager.ToLocal(item.DueDate);
            //}
            result = result.OrderBy(x => x.DueDate).ToList();
            return result;
        }

        public List<CalendarTasks> GetTasksForVendorCase(int Id)
        {
            var query = @"select t.TaskId, t.Subject,
                            o.InstallationAddressId,
                            ls.Id as LeadStatusId,
                            t.LeadId, t.AccountId, t.OpportunityId, t.OrderId,t.CaseId,t.VendorCaseId,t.CompletedDate,
                            CAST(t.DueDate as datetime2)as DueDate, t.Message,
                            (SELECT Stuff((SELECT ', ' + PersonName FROM CRM.EventToPeople
                          where TaskId=t.TaskId FOR XML PATH('')), 1, 2, '')) AS AttendeesName,
                            cu.firstname + ' ' + cu.LastName as LeadName,
                            cua.firstname + ' ' + cua.LastName as AccountName,
                            o.OpportunityName as OppurtunityName,n.OrderName as OrderName , t.CompletedDate as completed,
                            (case  when n.OrderName is not null then   'Ord-'+n.OrderName
							        when o.OpportunityName is not null then   'Opp-'+o.OpportunityName
									when cua.firstname + ' ' + cua.LastName is not null then   'Acct-'+cua.firstname + ' ' + cua.LastName
									when  cu.firstname + ' ' + cu.LastName is not null then   'Lead-'+cu.firstname + ' ' + cu.LastName
							        else '' end )as Related
                          from crm.Tasks t
                            left join crm.leadcustomers lc on lc.LeadId = t.LeadId and IsPrimaryCustomer = 1
	                        left join crm.customer cu on lc.CustomerId = cu.CustomerId
                            left join crm.accountcustomers ac on ac.AccountId = t.AccountId and ac.IsPrimaryCustomer = 1
	                        left join crm.customer cua on cua.CustomerId = ac.CustomerId
                            left join crm.leads l on l.LeadId = t.LeadId
	                        left join crm.type_leadstatus ls on ls.id = l.LeadStatusId
                            left join crm.Opportunities o on o.OpportunityId = t.OpportunityId
                            left join crm.Orders n on n.OrderID=t.OrderId
                          where t.VendorCaseId=@VendorCaseId and t.IsDeleted <> 1";
            //var query = @"select c.TaskId, c.Subject,
            //                c.DueDate as DueDate, c.Message as Message,
            //                (SELECT Stuff((SELECT ', ' + PersonName FROM CRM.EventToPeople
            //              where TaskId=c.TaskId FOR XML PATH('')), 1, 2, '')) AS AttendeesName
            //              from crm.Tasks c where c.CaseId=@CaseId";

            var result = new List<CalendarTasks>();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<CalendarTasks>(query,
                   new
                   {
                       VendorCaseId = Id
                   }).ToList();

                connection.Close();
            }
            foreach (var item in result)
            {
                item.DueDate = TimeZoneManager.ToLocal(item.DueDate);
            }

            return result;
        }

        public List<TaskVM> GetAllTasks()
        {
            var query = GetTaskQueryForDashboard();

            var result = new List<TaskVM>();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<TaskVM>(query,
                   new
                   {
                       franchiseId = SessionManager.CurrentFranchise.FranchiseId,
                       dueDate = TimeZoneManager.GetUTCTodaystartdatetime().Date
                   }).ToList();

                connection.Close();
                if (result != null)
                {
                    foreach (var task in result)
                    {
                        task.IsPrivateAccess = this.GetAuthorisedPrivateCalendar(task.TaskId, task.IsPrivate, true);
                    }
                    result = result.Where(x => x.IsPrivateAccess == true).ToList();
                }
            }
            return result;
        }

        public List<TaskVM> GetMyTasks()
        {
            var query = GetTaskQueryForDashboard();
            query += @" and t.taskId in (select taskid from crm.eventtopeople
						  where PersonId = @personId) ";

            var result = new List<TaskVM>();
            //List<CalendarTasksVM> resultVM = new List<CalendarTasksVM>();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<TaskVM>(query,
                   new
                   {
                       franchiseId = SessionManager.CurrentFranchise.FranchiseId,
                       personId = SessionManager.CurrentUser.PersonId,
                       dueDate = TimeZoneManager.GetUTCTodaystartdatetime().Date
                   }).ToList();
                if (result != null)
                {
                    foreach (var task in result)
                    {
                        task.IsPrivateAccess = this.GetAuthorisedPrivateCalendar(task.TaskId, task.IsPrivate, true);
                    }
                    result = result.Where(x => x.IsPrivateAccess == true).ToList();
                }
                connection.Close();
            }

            if (result != null && result.Count > 0)
                result = result.OrderBy(x => x.DueDate).ToList();

            return result;
        }

        private string GetTaskQuery()
        {
            var query = @"select t.IsPrivate,t.TaskId, t.Subject,
                            o.InstallationAddressId,
                            ls.Id as LeadStatusId,
                            t.LeadId, t.AccountId, t.OpportunityId, t.OrderId,
                            CAST(t.DueDate as datetime2)as DueDate, t.Message,
                            (SELECT Stuff((SELECT ', ' + PersonName FROM CRM.EventToPeople
                          where TaskId=t.TaskId FOR XML PATH('')), 1, 2, '')) AS AttendeesNames,
                            cu.firstname + ' ' + cu.LastName as LeadName,
                            cua.firstname + ' ' + cua.LastName as AccountName,
                            o.OpportunityName as OppurtunityName,n.OrderName as OrderName,t.CompletedDate as completed,
                            (case  when n.OrderName is not null then   'Ord-'+n.OrderName
							        when o.OpportunityName is not null then   'Opp-'+o.OpportunityName
									when cua.firstname + ' ' + cua.LastName is not null then   'Acct-'+cua.firstname + ' ' + cua.LastName
									when  cu.firstname + ' ' + cu.LastName is not null then   'Lead-'+cu.firstname + ' ' + cu.LastName
							        else '' end )as Related
                          from crm.Tasks t
                            left join crm.leadcustomers lc on lc.LeadId = t.LeadId and IsPrimaryCustomer = 1
	                        left join crm.customer cu on lc.CustomerId = cu.CustomerId
                            left join crm.accountcustomers ac on ac.AccountId = t.AccountId and ac.IsPrimaryCustomer = 1
	                        left join crm.customer cua on cua.CustomerId = ac.CustomerId
                            left join crm.leads l on l.LeadId = t.LeadId
	                        left join crm.type_leadstatus ls on ls.id = l.LeadStatusId
                            left join crm.Opportunities o on o.OpportunityId = t.OpportunityId
                            left join crm.Orders n on n.OrderID=t.OrderId
                          where t.FranchiseId = @franchiseId and ISNULL(t.IsDeleted,0) != 1 ";
            //and CompletedDate is null ";
            return query;
        }

        public List<TaskVM> GetAllOpenTasks()
        {
            var query = GetTaskQuery();
            query += @" and CompletedDate is null";

            var result = new List<TaskVM>();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<TaskVM>(query,
                   new
                   {
                       franchiseId = SessionManager.CurrentFranchise.FranchiseId
                   }).ToList();
                if (result != null)
                {
                    foreach (var task in result)
                    {
                        task.IsPrivateAccess = this.GetAuthorisedPrivateCalendar(task.TaskId, task.IsPrivate, true);
                    }
                    result = result.Where(x => x.IsPrivateAccess == true).ToList();
                }
                connection.Close();
            }

            return result;
        }

        public List<TaskVM> GetMyOpenTasks()
        {
            var query = GetTaskQuery();
            query += @"  and CompletedDate is null
                    and t.taskId in (select taskid from crm.eventtopeople
                    where PersonId = @personId) ";

            var result = new List<TaskVM>();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<TaskVM>(query,
                   new
                   {
                       franchiseId = SessionManager.CurrentFranchise.FranchiseId
                       ,
                       personId = SessionManager.CurrentUser.PersonId,
                   }).ToList();
                if (result != null)
                {
                    foreach (var task in result)
                    {
                        task.IsPrivateAccess = this.GetAuthorisedPrivateCalendar(task.TaskId, task.IsPrivate, true);
                    }
                    result = result.Where(x => x.IsPrivateAccess == true).ToList();
                }
                connection.Close();
            }

            return result;
        }

        public List<TaskVM> GetPastDueTasks()
        {
            var query = GetTaskQuery();
            query += @"  and CompletedDate is null
                and t.DueDate < @dueDate";

            var result = new List<TaskVM>();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<TaskVM>(query,
                   new
                   {
                       franchiseId = SessionManager.CurrentFranchise.FranchiseId
                       ,
                       dueDate = TimeZoneManager.GetUTCTodaystartdatetime().Date
                   }).ToList();
                if (result != null)
                {
                    foreach (var task in result)
                    {
                        task.IsPrivateAccess = this.GetAuthorisedPrivateCalendar(task.TaskId, task.IsPrivate, true);
                    }
                    result = result.Where(x => x.IsPrivateAccess == true).ToList();
                }
                connection.Close();
            }

            return result;
        }

        public List<TaskVM> GetMyPastDueTasks()
        {
            var query = GetTaskQuery();
            query += @"  and CompletedDate is null
                and t.DueDate < @dueDate
                and t.taskId in (select taskid from crm.eventtopeople
                    where PersonId = @personId)";

            var result = new List<TaskVM>();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<TaskVM>(query,
                   new
                   {
                       franchiseId = SessionManager.CurrentFranchise.FranchiseId
                       ,
                       dueDate = TimeZoneManager.GetUTCTodaystartdatetime().Date
                       ,
                       personId = SessionManager.CurrentUser.PersonId,
                   }).ToList();
                if (result != null)
                {
                    foreach (var task in result)
                    {
                        task.IsPrivateAccess = this.GetAuthorisedPrivateCalendar(task.TaskId, task.IsPrivate, true);
                    }
                    result = result.Where(x => x.IsPrivateAccess == true).ToList();
                }
                connection.Close();
            }

            return result;
        }

        public List<TaskVM> GetCompletedTasks()
        {
            var query = GetTaskQuery();
            query += @"  and CompletedDate is not null";

            var result = new List<TaskVM>();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<TaskVM>(query,
                   new
                   {
                       franchiseId = SessionManager.CurrentFranchise.FranchiseId
                   }).ToList();
                if (result != null)
                {
                    foreach (var task in result)
                    {
                        task.IsPrivateAccess = this.GetAuthorisedPrivateCalendar(task.TaskId, task.IsPrivate, true);
                    }
                    result = result.Where(x => x.IsPrivateAccess == true).ToList();
                }
                connection.Close();
            }

            return result;
        }

        public List<TaskVM> GetMyCompletedTasks()
        {
            var query = GetTaskQuery();
            query += @"  and CompletedDate is Not null
                and t.taskId in (select taskid from crm.eventtopeople
                    where PersonId = @personId)";

            var result = new List<TaskVM>();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<TaskVM>(query,
                   new
                   {
                       franchiseId = SessionManager.CurrentFranchise.FranchiseId
                       ,
                       dueDate = TimeZoneManager.GetUTCTodaystartdatetime().Date
                       ,
                       personId = SessionManager.CurrentUser.PersonId,
                   }).ToList();
                if (result != null)
                {
                    foreach (var task in result)
                    {
                        task.IsPrivateAccess = this.GetAuthorisedPrivateCalendar(task.TaskId, task.IsPrivate, true);
                    }
                    result = result.Where(x => x.IsPrivateAccess == true).ToList();
                }
                connection.Close();
            }

            return result;
        }

        public CalendarVM GetEvent(int calendarId)
        {
            var query = GetEventQuery();
            query += " and c.CalendarId = @id";

            var result = new Calendar();
            var resultvm = new CalendarVM();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<Calendar>(query,
                   new
                   {
                       id = calendarId
                       ,
                       franchiseId = SessionManager.CurrentFranchise.FranchiseId
                   }).FirstOrDefault();

                var queryAttendees = "select * from CRM.EventToPeople where CalendarId =@calendarid";
                var attendeees = connection.Query<EventToPerson>(queryAttendees
                    , new { calendarid = calendarId }).ToList();

                connection.Close();

                resultvm = MapTo(result);
                resultvm.Attendees = attendeees;
                var AppointmentTypeEnumTP = (AppointmentTypeEnumTP)Convert.ToInt32(resultvm.AptTypeEnum);
                resultvm.AppointmentType = AppointmentTypeEnumTP.GetDisplayName();
            }

            return resultvm;
        }

        public TextingSMS GetTextingModal(CalendarVM model)
        {
            TextingSMS smsvalue = new TextingSMS();
            smsvalue.AccountId = model.AccountId;
            smsvalue.LeadId = model.LeadId;
            smsvalue.OpportunityId = model.OpportunityId;
            smsvalue.OrderId = model.OrderId;
            smsvalue.CalendarId = model.CalendarId;
            smsvalue.Attendees = model.Attendees;
            smsvalue.StartDate = model.StartDate;
            smsvalue.FranchiseId = model.FranchiseId;
            smsvalue.AppointmentType = model.AppointmentType;
            smsvalue.IsAllDay = model.IsAllDay;
            return smsvalue;
        }

        public List<CalendarVM> GetEventsForLead(int leadId)
        {
            var query = GetEventQuery();
            query += " and c.Leadid = @leadId";
            var result = new List<Calendar>();
            var resultVM = new List<CalendarVM>();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<Calendar>(query,
                   new
                   {
                       leadId = leadId
                   ,
                       franchiseId = SessionManager.CurrentFranchise.FranchiseId
                   }).ToList();

                //TimeZoneManager.ConvertToLocalList(result);
                //foreach (var item in result)
                //{
                //    item.StartDate = TimeZoneManager.ToLocal(DateTime.Parse(item.StartDate.ToString()));
                //    item.EndDate = TimeZoneManager.ToLocal(DateTime.Parse(item.EndDate.ToString()));
                //}
                connection.Close();
            }
            foreach (var item in result)
            {

                CalendarVM CalVM = new CalendarVM();
                CalVM = MapTo(item);

                //CalVM.StartDate = DateTime.SpecifyKind(TimeZoneManager.ToLocal(item.StartDate.DateTime), DateTimeKind.Utc);
                //CalVM.EndDate = DateTime.SpecifyKind(TimeZoneManager.ToLocal(item.EndDate.DateTime), DateTimeKind.Utc);
                var AppointmentTypeEnumTP = (AppointmentTypeEnumTP)Convert.ToInt32(CalVM.AptTypeEnum);
                CalVM.AppointmentType = AppointmentTypeEnumTP.GetDisplayName();
                resultVM.Add(CalVM);
            }
            resultVM = resultVM.OrderBy(x => x.StartDate).ToList();
            return resultVM;
        }

        public List<CalendarVM> GetEventsForAccount(int accountId)
        {
            var query = GetEventQuery();
            query += " and c.AccountId = @accountId";
            var result = new List<Calendar>();
            var resultVM = new List<CalendarVM>();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<Calendar>(query,
                   new
                   {
                       accountId = accountId,
                       franchiseId = SessionManager.CurrentFranchise.FranchiseId
                   }).ToList();
                //TimeZoneManager.ConvertToLocalList(result);
                connection.Close();
            }
            foreach (var item in result)
            {
                CalendarVM CalVM = new CalendarVM();
                CalVM = MapTo(item);
                //CalVM.StartDate = DateTime.SpecifyKind(TimeZoneManager.ToLocal(item.StartDate.DateTime), DateTimeKind.Utc);
                //CalVM.EndDate = DateTime.SpecifyKind(TimeZoneManager.ToLocal(item.EndDate.DateTime), DateTimeKind.Utc);
                var AppointmentTypeEnumTP = (AppointmentTypeEnumTP)Convert.ToInt32(CalVM.AptTypeEnum);
                CalVM.AppointmentType = AppointmentTypeEnumTP.GetDisplayName();
                resultVM.Add(CalVM);
            }
            resultVM = resultVM.OrderBy(x => x.StartDate).ToList();
            return resultVM;
        }

        public List<CalendarVM> GetEventsForOpportunity(int opprtunityId)
        {
            var query = GetEventQuery();
            query += " and c.OpportunityId = @opportunityId";
            var result = new List<Calendar>();
            var resultVM = new List<CalendarVM>();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<Calendar>(query,
                   new
                   {
                       opportunityId = opprtunityId
                   ,
                       franchiseId = SessionManager.CurrentFranchise.FranchiseId
                   }).ToList();
                //TimeZoneManager.ConvertToLocalList(result);
                connection.Close();
            }
            foreach (var item in result)
            {
                CalendarVM CalVM = new CalendarVM();
                CalVM = MapTo(item);
                //CalVM.StartDate = DateTime.SpecifyKind(TimeZoneManager.ToLocal(item.StartDate.DateTime), DateTimeKind.Utc);
                //CalVM.EndDate = DateTime.SpecifyKind(TimeZoneManager.ToLocal(item.EndDate.DateTime), DateTimeKind.Utc);
                var AppointmentTypeEnumTP = (AppointmentTypeEnumTP)Convert.ToInt32(CalVM.AptTypeEnum);
                CalVM.AppointmentType = AppointmentTypeEnumTP.GetDisplayName();
                resultVM.Add(CalVM);
            }
            resultVM = resultVM.OrderBy(x => x.StartDate).ToList();
            return resultVM;
        }

        private string GetCommunicationQuery()
        {
            var query = @"select eh.* from history.SentEmails eh
                           where eh.FranchiseId = @franchiseId "; // and IsDeleted != 1 ";
            return query;
        }

        public List<SentEmail> GetCommunicationForLead(int leadId, string filterOptions)
        {
            //var query = GetCommunicationQuery();
            //query += " and eh.Leadid = @leadId";
            var query = ""; 
            if (filterOptions == "null" || filterOptions == null)
            {
                query = @"select SentEmailId,Type,DateValue,SubjectDescription,TemplateId,TextingTemplateTypeId,
                          case when len(ltrim(isnull(UserName,'')))>0 then UserName 
                          when TemplateId is not null or TextingTemplateTypeId is not null then
                          Recipient end as RecipientValue
                          from(select s.SentEmailId,s.SentAt,s.SentDate,s.LogType,s.UserName,
                          P.FirstName + '' +P.LastName as Recipient
                          ,s.Subject,s.Description,lv.Name as Type,s.SentByPersonId,  
                          s.TemplateId,s.TextingTemplateTypeId
                          from History.Sentemails s
                          left join CRM.Type_LookUpValues lv on lv.Id = s.LogType
                          left join CRM.Person p on s.SentByPersonId = p.PersonId
                          where s.LeadId= @leadId and s.FranchiseId = @franchiseId) as temp
                          unpivot(DateValue for ColumnAll1 in (SentAt,SentDate))as unpvt2
                          unpivot(SubjectDescription for ColumnAll2 in (Subject,Description))as unpvt3
                          union
                          select SentEmailId,Type,DateValue,SubjectDescription,TemplateId,TextingTemplateTypeId,
                          case when len(ltrim(isnull(UserName,'')))>0 then UserName 
                          when TemplateId is not null or TextingTemplateTypeId is not null then
                          Recipient end as RecipientValue
                          from(select s.SentEmailId,s.SentAt,s.SentDate,s.LogType,s.UserName,
                          P.FirstName + '' +P.LastName as Recipient
                          ,s.Subject,s.Description,lv.Name as Type, s.SentByPersonId,  
                          s.TemplateId,s.TextingTemplateTypeId
                          from History.SentEmails s 
                          left join crm.Customer c on c.CellPhone = s.CellPhone
                          left join crm.LeadCustomers lc on lc.CustomerId = c.CustomerId and lc.IsPrimaryCustomer = 1
                          left join CRM.Type_LookUpValues lv on lv.Id = s.LogType
                          left join CRM.Person p on s.SentByPersonId = p.PersonId
                          where lc.LeadId=@leadId and s.FranchiseId = @franchiseId
                          and ISNULL(s.TextingTemplateTypeId, 0) = 0) as temp
                          unpivot(DateValue for ColumnAll1 in (SentAt,SentDate))as unpvt2
                          unpivot(SubjectDescription for ColumnAll2 in (Subject,Description))as unpvt3 order by DateValue desc;";
            }
            else
            {
                query = @"select SentEmailId,Type,DateValue,SubjectDescription,TemplateId,TextingTemplateTypeId,
                         case when len(ltrim(isnull(UserName,'')))>0 then UserName 
                         when TemplateId is not null or TextingTemplateTypeId is not null then
                         Recipient end as RecipientValue
                          from(select s.SentEmailId,s.SentAt,s.SentDate,s.LogType,s.UserName,
                         P.FirstName + '' +P.LastName as Recipient
                         ,s.Subject,s.Description,lv.Name as Type,s.SentByPersonId,  
                         s.TemplateId,s.TextingTemplateTypeId
                         from History.Sentemails s
                         left join CRM.Type_LookUpValues lv on lv.Id = s.LogType
                         left join CRM.Person p on s.SentByPersonId = p.PersonId
                         where s.LeadId= @leadId and s.FranchiseId = @franchiseId and s.LogType in (select id from CRM.CSVToTable(@filteroptions))) as temp
                         unpivot(DateValue for ColumnAll1 in (SentAt,SentDate))as unpvt2
                         unpivot(SubjectDescription for ColumnAll2 in (Subject,Description))as unpvt3
                         union
                         select SentEmailId,Type,DateValue,SubjectDescription,TemplateId,TextingTemplateTypeId,
                         case when len(ltrim(isnull(UserName,'')))>0 then UserName 
                         when TemplateId is not null or TextingTemplateTypeId is not null then
                         Recipient end as RecipientValue
                          from(select s.SentEmailId,s.SentAt,s.SentDate,s.LogType,s.UserName,
                          P.FirstName + '' +P.LastName as Recipient
                         ,s.Subject,s.Description,lv.Name as Type, s.SentByPersonId,  
                         s.TemplateId,s.TextingTemplateTypeId
                         from History.SentEmails s 
                         left join crm.Customer c on c.CellPhone = s.CellPhone
                         left join crm.LeadCustomers lc on lc.CustomerId = c.CustomerId and lc.IsPrimaryCustomer = 1
                         left join CRM.Type_LookUpValues lv on lv.Id = s.LogType
                         left join CRM.Person p on s.SentByPersonId = p.PersonId
                         where lc.LeadId=@leadId and s.FranchiseId = @franchiseId and s.LogType in (select id from CRM.CSVToTable(@filteroptions))
                         and ISNULL(s.TextingTemplateTypeId, 0) = 0) as temp
                         unpivot(DateValue for ColumnAll1 in (SentAt,SentDate))as unpvt2
                         unpivot(SubjectDescription for ColumnAll2 in (Subject,Description))as unpvt3 order by DateValue desc;"; 
            }
            var result = new List<SentEmail>();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<SentEmail>(query,
                   new
                   {
                       leadId = leadId,
                       franchiseId = SessionManager.CurrentFranchise.FranchiseId,
                       filteroptions= filterOptions,
                   }).ToList();

                TimeZoneManager.ConvertToLocalList(result);
                //foreach (var item in result)
                //{
                //    item.StartDate = TimeZoneManager.ToLocal(DateTime.Parse(item.StartDate.ToString()));
                //    item.EndDate = TimeZoneManager.ToLocal(DateTime.Parse(item.EndDate.ToString()));
                //}
                connection.Close();
            }
            //foreach (var item in result)
            //{
            //    if (item.MessageType == "Email")
            //    {
            //        if (item.IsSent == true) item.ShowStatus = "Sent";
            //    }
            //    else
            //    {
            //        if (item.TextingStatus == 1)
            //        {
            //            item.ShowStatus = "Sent";
            //        }
            //        else if (item.TextingStatus == 2)
            //        {
            //            item.ShowStatus = "Received";
            //        }
            //        else // 3
            //        { 
            //            item.ShowStatus = "Error";
            //        }
            //    }

            //}
            result = GetCommunicationDatas(result); 
            return result;
        }

        public List<SentEmail> GetCommunicationForAccount(int accountId,string filterOptions)
        {
            //var query = GetCommunicationQuery();
            var query = "";
            if (filterOptions == "null" || filterOptions == null)
            {
                query = @"select SentEmailId,Type,DateValue,SubjectDescription,TemplateId,TextingTemplateTypeId,
                          case when len(ltrim(isnull(UserName,'')))>0 then UserName 
                          when TemplateId is not null or TextingTemplateTypeId is not null then
                          Recipient end as RecipientValue
                          from(select s.SentEmailId,s.SentAt,s.SentDate,s.LogType,s.UserName,
                          P.FirstName + '' +P.LastName as Recipient
                          ,s.Subject,s.Description,lv.Name as Type ,s.SentByPersonId,  
                          s.TemplateId,s.TextingTemplateTypeId 
                          from History.Sentemails s
                          left join CRM.Type_LookUpValues lv on lv.Id = s.LogType
                          left join CRM.Person p on s.SentByPersonId = p.PersonId
                          where s.AccountId = @accountId and s.FranchiseId =@franchiseId) as temp
                          unpivot(DateValue for ColumnAll1 in (SentAt,SentDate))as unpvt2
                          unpivot(SubjectDescription for ColumnAll2 in (Subject,Description))as unpvt3
                          Union
                          Select SentEmailId,Type,DateValue,SubjectDescription,TemplateId,TextingTemplateTypeId,
                          case when len(ltrim(isnull(UserName,'')))>0 then UserName 
                          when TemplateId is not null or TextingTemplateTypeId is not null then
                          Recipient end as RecipientValue
                          from(select s.SentEmailId,s.SentAt,s.SentDate,s.LogType,s.UserName,
                           P.FirstName + '' +P.LastName as Recipient
                          ,s.Subject,s.Description,lv.Name as Type, s.SentByPersonId,  
                          s.TemplateId,s.TextingTemplateTypeId
                          from history.SentEmails s
                          left join crm.Orders o on o.OrderID = s.OrderId
                          left join crm.Customer c on c.CellPhone = s.CellPhone
                          left join crm.AccountCustomers ac on ac.CustomerId = c.CustomerId and ac.IsPrimaryCustomer = 1
                          left join CRM.Calendar Ca on Ca.CalendarId=s.CalendarId 
                          left join CRM.Type_LookUpValues lv on lv.Id = s.LogType
                          left join CRM.Person p on s.SentByPersonId = p.PersonId
                          where ac.AccountId = @accountId and s.FranchiseId = @franchiseId and ISNULL(s.TextingTemplateTypeId, 0) = 0)as temp
                          unpivot(DateValue for ColumnAll1 in (SentAt,SentDate))as unpvt2
                          unpivot(SubjectDescription for ColumnAll2 in (Subject,Description))as unpvt3 order by DateValue desc;";
            }
            else
            {
                query = @"select SentEmailId,Type,DateValue,SubjectDescription,TemplateId,TextingTemplateTypeId,
                        case when len(ltrim(isnull(UserName,'')))>0 then UserName 
                        when TemplateId is not null or TextingTemplateTypeId is not null then
                        Recipient end as RecipientValue
                        from(select s.SentEmailId,s.SentAt,s.SentDate,s.LogType,s.UserName,
                        P.FirstName + '' +P.LastName as Recipient
                        ,s.Subject,s.Description,lv.Name as Type ,s.SentByPersonId,  
                        s.TemplateId,s.TextingTemplateTypeId 
                        from History.Sentemails s
                        left join CRM.Type_LookUpValues lv on lv.Id = s.LogType
                        left join CRM.Person p on s.SentByPersonId = p.PersonId
                        where s.AccountId = @accountId and s.FranchiseId = @franchiseId and s.LogType in (select id from CRM.CSVToTable(@filteroptions))) as temp
                        unpivot(DateValue for ColumnAll1 in (SentAt,SentDate))as unpvt2
                        unpivot(SubjectDescription for ColumnAll2 in (Subject,Description))as unpvt3
                        Union
                        Select SentEmailId,Type,DateValue,SubjectDescription,TemplateId,TextingTemplateTypeId,
                        case when len(ltrim(isnull(UserName,'')))>0 then UserName 
                        when TemplateId is not null or TextingTemplateTypeId is not null then
                        Recipient end as RecipientValue
                        from(select s.SentEmailId,s.SentAt,s.SentDate,s.LogType,s.UserName,
                         P.FirstName + '' +P.LastName as Recipient
                        ,s.Subject,s.Description,lv.Name as Type, s.SentByPersonId,  
                        s.TemplateId,s.TextingTemplateTypeId
                        from history.SentEmails s
                        left join crm.Orders o on o.OrderID = s.OrderId
                        left join crm.Customer c on c.CellPhone = s.CellPhone
                        left join crm.AccountCustomers ac on ac.CustomerId = c.CustomerId and ac.IsPrimaryCustomer = 1
                        left join CRM.Calendar Ca on Ca.CalendarId=s.CalendarId 
                        left join CRM.Type_LookUpValues lv on lv.Id = s.LogType
                        left join CRM.Person p on s.SentByPersonId = p.PersonId
                        where ac.AccountId = @accountId and s.FranchiseId = @franchiseId
                        and s.LogType in (select id from CRM.CSVToTable(@filteroptions))
                        and ISNULL(s.TextingTemplateTypeId, 0) = 0)as temp
                        unpivot(DateValue for ColumnAll1 in (SentAt,SentDate))as unpvt2
                        unpivot(SubjectDescription for ColumnAll2 in (Subject,Description))as unpvt3 order by DateValue desc;";
            }
            //query += " and eh.AccountId = @accountId";
            var result = new List<SentEmail>();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<SentEmail>(query,
                   new
                   {
                       accountId = accountId,
                       franchiseId = SessionManager.CurrentFranchise.FranchiseId,
                       filteroptions= filterOptions,
                   }).ToList();
                TimeZoneManager.ConvertToLocalList(result);
                connection.Close();
            }
            //foreach (var item in result)
            //{
            //    if (item.OrderNumber == 0)
            //        item.OrderNumber = null;
            //    if (item.IsSent == true) item.ShowStatus = "Sent";
            //    else item.ShowStatus = "Not Sent";
            //}
            result = GetCommunicationDatas(result);
            return result;
        }

        public List<SentEmail> GetCommunicationForOpportunity(int opportunityId,string filterOptions)
        {
            //var query = GetCommunicationQuery();
            //query += " and eh.OpportunityId = @opportunityId";
            var query = "";
            if (filterOptions == null || filterOptions == "null")
            {
                query = @"select SentEmailId,Type,DateValue,SubjectDescription,TemplateId,TextingTemplateTypeId,
                    case when len(ltrim(isnull(UserName,'')))>0 then UserName 
                    when TemplateId is not null or TextingTemplateTypeId is not null then
                    Recipient end as RecipientValue
                    from(
                    select s.SentEmailId,s.SentAt,s.SentDate,s.LogType,s.UserName,
                    P.FirstName + '' +P.LastName as Recipient
                    ,s.Subject,s.Description,lv.Name as Type,s.SentByPersonId,  
                    s.TemplateId,s.TextingTemplateTypeId 
                    from History.SentEmails s 
                    left join CRM.Type_LookUpValues lv on lv.Id = s.LogType 
                    left join CRM.Person p on s.SentByPersonId = p.PersonId
                    where s.OpportunityId = @opportunityId and s.FranchiseId = @franchiseId)as temp
                    unpivot(DateValue for ColumnAll1 in (SentAt,SentDate))as unpvt2
                    unpivot(SubjectDescription for ColumnAll2 in (Subject,Description))as unpvt3
                    union
                    Select SentEmailId,Type,DateValue,SubjectDescription,TemplateId,TextingTemplateTypeId,
                    case when len(ltrim(isnull(UserName,'')))>0 then UserName 
                    when TemplateId is not null or TextingTemplateTypeId is not null then
                    Recipient end as RecipientValue
                    from(
                    select  s.SentEmailId,s.SentAt,s.SentDate,s.LogType,s.UserName,
                    P.FirstName + '' +P.LastName as Recipient
                    ,s.Subject,s.Description,lv.Name as Type,s.SentByPersonId,  
                    s.TemplateId,s.TextingTemplateTypeId from history.SentEmails s
                    left join CRM.Orders o on o.OrderID = s.OrderId
                    left join crm.Customer c on c.CellPhone = s.CellPhone
                    left join crm.AccountCustomers ac on ac.CustomerId = c.CustomerId and ac.IsPrimaryCustomer = 1
                    left join crm.Opportunities op on op.AccountId = ac.AccountId
                    left join CRM.Calendar Ca on Ca.CalendarId=s.CalendarId
                    left join CRM.Type_LookUpValues lv on lv.Id = s.LogType
                    left join CRM.Person p on s.SentByPersonId = p.PersonId

                    where op.opportunityId = @opportunityId and s.FranchiseId = @franchiseId  and ISNULL(s.TextingTemplateTypeId, 0) = 0 )as temp
                    unpivot(DateValue for ColumnAll1 in (SentAt,SentDate))as unpvt2
                    unpivot(SubjectDescription for ColumnAll2 in (Subject,Description))as unpvt3 order by DateValue desc";
            }
            else
            {
                query = @"select SentEmailId,Type,DateValue,SubjectDescription,TemplateId,TextingTemplateTypeId,
                            case when len(ltrim(isnull(UserName,'')))>0 then UserName 
                            when TemplateId is not null or TextingTemplateTypeId is not null then
                            Recipient end as RecipientValue
                            from(
                            select s.SentEmailId,s.SentAt,s.SentDate,s.LogType,s.UserName,
                            P.FirstName + '' +P.LastName as Recipient
                            ,s.Subject,s.Description,lv.Name as Type,s.SentByPersonId,  
                            s.TemplateId,s.TextingTemplateTypeId 
                            from History.SentEmails s 
                            left join CRM.Type_LookUpValues lv on lv.Id = s.LogType 
                            left join CRM.Person p on s.SentByPersonId = p.PersonId
                            where s.OpportunityId = @opportunityId and s.FranchiseId =@franchiseId  
                            and s.LogType in (select id from CRM.CSVToTable(@filteroptions)))as temp
                            unpivot(DateValue for ColumnAll1 in (SentAt,SentDate))as unpvt2
                            unpivot(SubjectDescription for ColumnAll2 in (Subject,Description))as unpvt3
                            union
                            Select SentEmailId,Type,DateValue,SubjectDescription,TemplateId,TextingTemplateTypeId,
                            case when len(ltrim(isnull(UserName,'')))>0 then UserName 
                            when TemplateId is not null or TextingTemplateTypeId is not null then
                            Recipient end as RecipientValue
                            from(
                            select  s.SentEmailId,s.SentAt,s.SentDate,s.LogType,s.UserName,
                            P.FirstName + '' +P.LastName as Recipient
                            ,s.Subject,s.Description,lv.Name as Type,s.SentByPersonId,  
                            s.TemplateId,s.TextingTemplateTypeId from history.SentEmails s
                            left join CRM.Orders o on o.OrderID = s.OrderId
                            left join crm.Customer c on c.CellPhone = s.CellPhone
                            left join crm.AccountCustomers ac on ac.CustomerId = c.CustomerId and ac.IsPrimaryCustomer = 1
                            left join crm.Opportunities op on op.AccountId = ac.AccountId
                            left join CRM.Calendar Ca on Ca.CalendarId=s.CalendarId
                            left join CRM.Type_LookUpValues lv on lv.Id = s.LogType
                            left join CRM.Person p on s.SentByPersonId = p.PersonId
                            where op.opportunityId = @opportunityId  and s.FranchiseId = @franchiseId  and s.LogType in (select id from CRM.CSVToTable(@filteroptions))  and ISNULL(s.TextingTemplateTypeId, 0) = 0 )as temp
                            unpivot(DateValue for ColumnAll1 in (SentAt,SentDate))as unpvt2
                            unpivot(SubjectDescription for ColumnAll2 in (Subject,Description))as unpvt3 order by DateValue desc";
            }
            var result = new List<SentEmail>();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<SentEmail>(query,
                   new
                   {
                       opportunityId = opportunityId,
                       franchiseId = SessionManager.CurrentFranchise.FranchiseId,
                       filteroptions = filterOptions,
                   }).ToList();
                TimeZoneManager.ConvertToLocalList(result);
                connection.Close();
            }
            //foreach (var item in result)
            //{
            //    if (item.OrderNumber == 0)
            //        item.OrderNumber = null;
            //    if (item.IsSent == true) item.ShowStatus = "Sent";
            //    else item.ShowStatus = "Not Sent";
            //}
            result = GetCommunicationDatas(result);
            return result;
        }

        public List<SentEmail> GetCommunicationDatas(List<SentEmail> modal)
        {
            foreach (var item in modal)
            {
                if (item.MessageType == "Email")
                {
                    if (item.OrderNumber == 0)
                        item.OrderNumber = null;
                    if (item.IsSent == true) item.ShowStatus = "Sent";
                    else item.ShowStatus = "Not Sent";
                }
                else
                {
                    if (item.TextingStatus == 1)
                    {
                        item.ShowStatus = "Sent";
                    }
                    else if (item.TextingStatus == 2)
                    {
                        item.ShowStatus = "Received";
                    }
                    else // 3
                    {
                        item.ShowStatus = "Error";
                    }
                }

            }
            return modal;
        }

        //--- Code start for task ---

        public List<CalendarTasksVM> GetTasksForLead(int leadId)
        {
            var query = GetCalendarTasksQuery();
            query += " and T.LeadId=@leadId";
            var result = new List<CalendarTasks>();
            List<CalendarTasksVM> resultVM = new List<CalendarTasksVM>();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                result = connection.Query<CalendarTasks>(query,
                   new
                   {
                       leadId = leadId,
                       franchiseId = SessionManager.CurrentFranchise.FranchiseId
                   }).ToList();
            }
            foreach (var item in result)
            {
                CalendarTasksVM CalTaskVM = new CalendarTasksVM();
                CalTaskVM = MapToTasksModel(item);
                resultVM.Add(CalTaskVM);
            }
            resultVM = resultVM.OrderBy(x => x.DueDate).ToList();
            return resultVM;
        }

        public List<CalendarTasksVM> GetTasksForAccount(int accountId)
        {
            var query = GetCalendarTasksQuery();
            query += " and T.AccountId=@accountId";
            var result = new List<CalendarTasks>();
            List<CalendarTasksVM> resultVM = new List<CalendarTasksVM>();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                result = connection.Query<CalendarTasks>(query,
                   new
                   {
                       accountId = accountId,
                       franchiseId = SessionManager.CurrentFranchise.FranchiseId
                   }).ToList();
            }

            foreach (var item in result)
            {
                CalendarTasksVM CalTaskVM = new CalendarTasksVM();
                CalTaskVM = MapToTasksModel(item);
                resultVM.Add(CalTaskVM);
            }
            resultVM = resultVM.OrderBy(x => x.DueDate).ToList();
            return resultVM;
        }

        public List<CalendarTasksVM> GetTasksForOpportunity(int opprtunityId)
        {
            var query = GetCalendarTasksQuery();
            query += " and T.OpportunityId=@opportunityId";
            var result = new List<CalendarTasks>();
            List<CalendarTasksVM> resultVM = new List<CalendarTasksVM>();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                result = connection.Query<CalendarTasks>(query,
                   new
                   {
                       opportunityId = opprtunityId,
                       franchiseId = SessionManager.CurrentFranchise.FranchiseId
                   }).ToList();
            }
            foreach (var item in result)
            {
                CalendarTasksVM CalTaskVM = new CalendarTasksVM();
                CalTaskVM = MapToTasksModel(item);
                resultVM.Add(CalTaskVM);
            }
            resultVM = resultVM.OrderBy(x => x.DueDate).ToList();
            return resultVM;
        }

        public List<Calendar> GetCalendar(List<int> assignedPersonIds = null, DateTime? beginDate = null, DateTime? endDate = null, int? CalendarId = 0)
        {
            try
            {
                List<Calendar> result = null;
                List<CalendarVM> resultVM = new List<CalendarVM>();
                if (CalendarId > 0)
                {
                    result = EventListByCalendarId(User.FranchiseId, CalendarId);
                }
                else
                {
                    result = EventList(User.FranchiseId, assignedPersonIds, beginDate.Value, endDate.Value);
                }

                //Adding Recurrence Rule to calendar
                var reclistIds = result.Where(x => x.RecurringEventId != null && x.RecurringEventId != 0).Select(z => z.RecurringEventId.Value).Distinct().ToList();
                var calListIds = result.Select(z => z.CalendarId).Distinct().ToList();
                var list = RecurrenceList(reclistIds);
                var OccurrenceList = CalendarSingleOccurrenceList(reclistIds);
                foreach (var cal in result)
                {
                    if (cal.RecurringEventId != null && cal.EventTypeEnum == EventTypeEnum.Series)
                    {
                        var recevent = list.Where(a => a.RecurringEventId == (int)cal.RecurringEventId).FirstOrDefault();// RecurrenceByCalendarId((int)cal.RecurringEventId);
                        if (recevent != null)
                        {
                            cal.RRRule = this.RecurringKendoRule(recevent);
                        }

                        ///---------- single occurance -----------
                        /// Recurrence exception single occurance avoid
                        /// Get List of single occurance for the event recurrence, 
                        ///select original start date , 
                        ///convert UTC to FE local time, all the date to comma separated string
                        var OccurrenceStartDatelst = OccurrenceList.Where(a => a.RecurringEventId == cal.RecurringEventId).Select(x => x.OriginalStartDate).ToList();

                        if (OccurrenceStartDatelst != null && OccurrenceStartDatelst.Count > 0)
                        {
                            List<string> RecurrenceExceptionlst = new List<string>();
                            foreach (var occ in OccurrenceStartDatelst)
                            {
                                DateTime dt = TimeZoneManager.ToLocal(occ);
                                //dt = DateTime.SpecifyKind(dt, DateTimeKind.Utc);
                                RecurrenceExceptionlst.Add(dt.ToString("yyyyMMddTHHmmssZ"));
                            }
                            cal.RecurrenceException = string.Join(",", RecurrenceExceptionlst.ToArray());
                        }
                        ///---------- end single occurance -----------

                    }
                }

                //if (CalendarId <= 0)
                //{
                //    var result1 = result.ToList().GroupBy(x => x.PersonId).ToList();
                //    List<Calendar> result2 = new List<Calendar>();
                //    if (PersonIds == null || PersonIds == "")
                //    {
                //        PersonIds = User.Person.PersonId.ToString();
                //    }
                //    var results = PersonIds.Split(new string[] { "," }, StringSplitOptions.None);
                //    foreach (var test in result1)
                //    {
                //        var findresults = results.ToList().FindIndex(i => i.Contains(test.Key.ToString()));
                //        if (findresults != -1)
                //        {
                //            var Calendars = result.Where(t => t.PersonId == test.Key).ToList();
                //            foreach (var cal in Calendars)
                //            {
                //                var check = result2.Where(e => e.PersonId == cal.PersonId && e.CalendarId == cal.CalendarId).FirstOrDefault();
                //                if (check == null)
                //                {
                //                    result2.Add(cal);
                //                }
                //            }
                //        }
                //    }
                //    result = result2;
                //}

                /// Map attendees to each event and attendee calendar colour

                if (result != null && result.Count > 0 && CalendarId > 0)
                {
                    var etplist = EventToPersonList(calListIds);
                    foreach (var cal in result)
                    {
                        //bool iseditable = false, isdeletable = false;
                        //if its assigned to the the current user then its editable for them

                        //iseditable = true;
                        //isdeletable = true;
                        // TODO: is this required in TP???
                        //iseditable = AssignedPermission.CanUpdate && cal.OrganizerPersonId == User.PersonId;
                        //isdeletable = AssignedPermission.CanDelete && cal.OrganizerPersonId == User.PersonId;

                        //or if the current user is in any of the additional user, only allow edit if they are in the additional ppl, don't let them delete, only the person assigned can delete
                        //if (!iseditable)
                        //    iseditable = AssignedPermission.CanUpdate && cal.Attendees.Any(a => a.PersonId == User.PersonId);

                        //mask subject and desc if its private unless they are the organizer or part of attendee
                        //if (cal.IsPrivate.HasValue && cal.IsPrivate.Value && !cal.Attendees.Any(a => a.PersonId == User.PersonId))
                        //{
                        //    //cal.Subject = "Busy";
                        //    //cal.Message = null;
                        //    //cal.Location = null;
                        //}

                        //lastly use base permission if there is they are still not approved
                        // TODO: is this required in TP???
                        //if (!iseditable)
                        //    iseditable = BasePermission.CanUpdate; //permission for calender lets them edit
                        //if (!isdeletable)
                        //    isdeletable = BasePermission.CanDelete;

                        //var person = CacheManager.UserCollection.FirstOrDefault(f => f.PersonId == cal.AssignedPersonId);
                        //var eventpeople = CRMDBContext.EventToPeople.Where(e => e.CalendarId == cal.CalendarId).ToList();
                        cal.Attendees = etplist.Where(x => x.CalendarId == cal.CalendarId).ToList();
                        //cal.Attendees = eventpeople;
                        //if (person != null)
                        //    cal.ColorClassName = string.Format("color-{0}", person.ColorId);
                        //else
                        //{
                        //    var personOrgniser = CacheManager.UserCollection.FirstOrDefault(f => f.PersonId == cal.OrganizerPersonId);
                        //    if (personOrgniser != null)
                        //        cal.ColorClassName = string.Format("color-{0}", personOrgniser.ColorId);
                        //    else
                        //        cal.ColorClassName = "color-none";
                        //}
                        //cal.IsDeletable = isdeletable;
                        //cal.IsEditable = iseditable;
                    }
                }
                /// End Map attendees to each event and attendee calendar colour

                /// Event Recurring
                if (CalendarId > 0)
                {
                    var recurlist = CRMDBContext.EventRecurrings.ToList();
                    foreach (var item in result)
                    {
                        if (item.RecurringEventId != null)
                        {
                            item.EventRecurring = recurlist.Where(r => r.RecurringEventId == item.RecurringEventId && r.IsDeleted == false).FirstOrDefault();
                        }
                    }
                }

                /// end Event Recurring


                //foreach (var item in result)
                //{
                //    CalendarVM CalVM = new CalendarVM();
                //    CalVM = MapTo(item);
                //    var AppointmentTypeEnumTP = (AppointmentTypeEnumTP)Convert.ToInt32(CalVM.AptTypeEnum);
                //    CalVM.AppointmentType = AppointmentTypeEnumTP.GetDisplayName();
                //    resultVM.Add(CalVM);
                //}
                return result;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }
        }

        public string RecurringKendoRule(EventRecurring recevent)
        {
            StringBuilder rrule = new StringBuilder();

            #region Day

            //RRULE:FREQ=DAILY;COUNT=5;INTERVAL=3
            //Recurring Day Event
            if ((int)recevent.PatternEnum == 1)
            {
                rrule = rrule.Append("RRULE:FREQ=DAILY");

                if (recevent.RecursEvery > 0)
                {
                    rrule = rrule.Append(";INTERVAL=" + recevent.RecursEvery);
                }
            }

            #endregion Day

            #region Week

            //Recurring Week Event
            //RRULE:FREQ=WEEKLY;COUNT=15;INTERVAL=2;BYDAY=MO,WE
            if ((int)recevent.PatternEnum == 2)
            {
                rrule = rrule.Append("RRULE:FREQ=WEEKLY");

                if (recevent.RecursEvery > 0)
                {
                    rrule = rrule.Append(";INTERVAL=" + recevent.RecursEvery);
                }
                if (recevent.DayOfWeekEnum != null)
                {
                    string[] dayofweekenum = recevent.DayOfWeekEnum.ToString().Split(new char[] { '|' });
                    if (dayofweekenum[0].Contains(","))
                    {
                        dayofweekenum = dayofweekenum[0].ToString().Split(new char[] { ',' });
                    }
                    StringBuilder strdayofweekenum = new StringBuilder();
                    foreach (var dayofweek in dayofweekenum)
                    {
                        if (dayofweek.ToString() == "Sunday")
                        {
                            if (strdayofweekenum.ToString().Contains(",")) { }
                            strdayofweekenum = strdayofweekenum.Append("SU,");
                        }
                        if (dayofweek.ToString().Contains("Monday"))
                        {
                            strdayofweekenum = strdayofweekenum.Append("MO,");
                        }
                        if (dayofweek.ToString().Contains("Tuesday"))
                        {
                            strdayofweekenum = strdayofweekenum.Append("TU,");
                        }
                        if (dayofweek.ToString().Contains("Wednesday"))
                        {
                            strdayofweekenum = strdayofweekenum.Append("WE,");
                        }
                        if (dayofweek.ToString().Contains("Thursday"))
                        {
                            strdayofweekenum = strdayofweekenum.Append("TH,");
                        }
                        if (dayofweek.ToString().Contains("Friday"))
                        {
                            strdayofweekenum = strdayofweekenum.Append("FR,");
                        }
                        if (dayofweek.ToString().Contains("Saturday"))
                        {
                            strdayofweekenum = strdayofweekenum.Append("SA,");
                        }
                    }
                    if (strdayofweekenum.ToString().LastIndexOf(",") > 0)
                    {
                        strdayofweekenum = strdayofweekenum.Replace(",", "", strdayofweekenum.ToString().LastIndexOf(","), 1);
                    }
                    rrule = rrule.Append(";BYDAY=" + strdayofweekenum.ToString());
                }

                //if (recevent.EndsOn == null && recevent.EndsAfterXOccurrences == null && recevent.DayOfWeekEnum==null)
                //{
                //    rrule = rrule.Append(";BYDAY=SU,MMO,TU,WE,TH,FR,SA");
                //}
            }

            #endregion Week

            #region Month

            //Pattren 3 Monthly
            //RRULE:FREQ=MONTHLY;COUNT=10;BYMONTHDAY=23
            if ((int)recevent.PatternEnum == 3)
            {
                rrule = rrule.Append("RRULE:FREQ=MONTHLY");
                if (recevent.RecursEvery > 0)
                {
                    rrule = rrule.Append(";INTERVAL=" + recevent.RecursEvery);
                }

                //monthly flag 1
                if (recevent.DayOfMonth != null)
                {
                    rrule = rrule.Append(";BYMONTHDAY=" + recevent.DayOfMonth);
                }
                //monthly flag 2
                if (recevent.DayOfWeekEnum != null)
                {
                    string rdayofweekenum = "";
                    if (recevent.DayOfWeekEnum.ToString() == "Sunday") { rdayofweekenum = "SU"; }
                    if (recevent.DayOfWeekEnum.ToString() == "Monday") { rdayofweekenum = "MO"; }
                    if (recevent.DayOfWeekEnum.ToString() == "Tuesday") { rdayofweekenum = "TU"; }
                    if (recevent.DayOfWeekEnum.ToString() == "Wednesday") { rdayofweekenum = "WE"; }
                    if (recevent.DayOfWeekEnum.ToString() == "Thursday") { rdayofweekenum = "TH"; }
                    if (recevent.DayOfWeekEnum.ToString() == "Friday") { rdayofweekenum = "FR"; }
                    if (recevent.DayOfWeekEnum.ToString() == "Saturday") { rdayofweekenum = "SA"; }

                    rrule = rrule.Append(";BYDAY=" + rdayofweekenum);
                }

                //The First day of every# The <First> <Day> of Every
                //RRULE: FREQ = MONTHLY; INTERVAL = 2; BYDAY = WE; BYSETPOS = -1
                if (recevent.DayOfWeekIndex > 0)
                {
                    string rdayOfWeekIndex = "";
                    if (recevent.DayOfWeekIndex.ToString() == "First") { rdayOfWeekIndex = "1"; }
                    if (recevent.DayOfWeekIndex.ToString() == "Second") { rdayOfWeekIndex = "2"; }
                    if (recevent.DayOfWeekIndex.ToString() == "Third") { rdayOfWeekIndex = "3"; }
                    if (recevent.DayOfWeekIndex.ToString() == "Fourth") { rdayOfWeekIndex = "4"; }
                    if (recevent.DayOfWeekIndex.ToString() == "Last") { rdayOfWeekIndex = "-1"; }
                    rrule = rrule.Append(";BYSETPOS=" + rdayOfWeekIndex);
                }
            }

            #endregion Month

            #region Year

            //Pattren 4 Yearly
            //RRULE: FREQ = YEARLY; COUNT = 4; BYMONTHDAY = 7; BYMONTH = 2
            if ((int)recevent.PatternEnum == 4)
            {
                rrule = rrule.Append("RRULE:FREQ=YEARLY");
                if (recevent.RecursEvery > 0)
                {
                    rrule = rrule.Append(";INTERVAL=" + recevent.RecursEvery);
                }

                if ((int)recevent.MonthEnum > 0)
                {
                    rrule = rrule.Append(";BYMONTH=" + (int)recevent.MonthEnum);
                }

                //Yearly flag 1

                if (recevent.DayOfMonth > 0)
                {
                    rrule = rrule.Append(";BYMONTHDAY=" + recevent.DayOfMonth);
                }
                //yearly flag 2
                if (recevent.DayOfWeekEnum != null)
                {
                    string rdayofweekenum = "";
                    if (recevent.DayOfWeekEnum.ToString() == "Sunday") { rdayofweekenum = "SU"; }
                    if (recevent.DayOfWeekEnum.ToString() == "Monday") { rdayofweekenum = "MO"; }
                    if (recevent.DayOfWeekEnum.ToString() == "Tuesday") { rdayofweekenum = "TU"; }
                    if (recevent.DayOfWeekEnum.ToString() == "Wednesday") { rdayofweekenum = "WE"; }
                    if (recevent.DayOfWeekEnum.ToString() == "Thursday") { rdayofweekenum = "TH"; }
                    if (recevent.DayOfWeekEnum.ToString() == "Friday") { rdayofweekenum = "FR"; }
                    if (recevent.DayOfWeekEnum.ToString() == "Saturday") { rdayofweekenum = "SA"; }

                    rrule = rrule.Append(";BYDAY=" + rdayofweekenum);
                }

                //The First day of every# The <First> <Day> of Every
                //RRULE: FREQ = MONTHLY; INTERVAL = 2; BYDAY = WE; BYSETPOS = -1
                if (recevent.DayOfWeekIndex > 0)
                {
                    string rdayOfWeekIndex = "";
                    if (recevent.DayOfWeekIndex.ToString() == "First") { rdayOfWeekIndex = "1"; }
                    if (recevent.DayOfWeekIndex.ToString() == "Second") { rdayOfWeekIndex = "2"; }
                    if (recevent.DayOfWeekIndex.ToString() == "Third") { rdayOfWeekIndex = "3"; }
                    if (recevent.DayOfWeekIndex.ToString() == "Fourth") { rdayOfWeekIndex = "4"; }
                    if (recevent.DayOfWeekIndex.ToString() == "Last") { rdayOfWeekIndex = "-1"; }
                    rrule = rrule.Append(";BYSETPOS=" + rdayOfWeekIndex);
                }
            }

            #endregion Year

            //Range Common

            #region Range

            if (recevent.EndsAfterXOccurrences != null)
            {
                rrule = rrule.Append(";COUNT=" + recevent.EndsAfterXOccurrences);
            }
            if (recevent.EndsOn != null)
            {
                var enddate = ((DateTimeOffset)recevent.EndsOn).ToString("yyyyMMdd") + "T235900Z";
                rrule = rrule.Append(";UNTIL=" + enddate);
            }

            #endregion Range

            return rrule.ToString();
        }

        public List<Calendar> EventListByCalendarId(int? FranchiseId, int? CalendarId)
        {
            //          string query = @"select  c.*,
            //       case when c.AccountId is not null
            //then
            //(select
            //case when PreferredTFN='C' then CellPhone
            //else case when PreferredTFN='H' then HomePhone
            //else case when PreferredTFN='W' then WorkPhone
            //else case when CellPhone is not null then CellPhone
            //else case when HomePhone is not null then HomePhone
            //else case when WorkPhone is not null then WorkPhone
            //else ''
            //end end end end end end
            //from CRM.Customer cus
            //join CRM.AccountCustomers acus on cus.CustomerId=acus.CustomerId and IsPrimaryCustomer=1
            //where acus.AccountId=c.AccountId) end as AccountPhone,
            // case when c.LeadId is not null
            // then
            //(select
            //case when PreferredTFN='C' then CellPhone
            //else case when PreferredTFN='H' then HomePhone
            //else case when PreferredTFN='W' then WorkPhone
            //else case when CellPhone is not null then CellPhone
            //else case when HomePhone is not null then HomePhone
            //else case when WorkPhone is not null then WorkPhone
            //else ''
            //end end end end end end
            //from CRM.Customer cus
            //join CRM.LeadCustomers lcus on cus.CustomerId=lcus.CustomerId and IsPrimaryCustomer=1
            //where lcus.LeadId=c.LeadId)
            //end as [PhoneNumber],
            //      (select [CRM].[fnGetAccountNameByAccountId](c.AccountId)) as AccountName,
            //(select	oppo.OpportunityName from CRM.[Opportunities] oppo
            //where oppo.OpportunityId=c.OpportunityId) as [OpportunityName],
            //      ep.[EventPersonId] AS [EventPersonId],
            //      ep.[PersonId] AS [PersonId],
            //      ep.[PersonName] AS [PersonName],
            //      case when c.OrderId is not null and c.OrderId>0 then (select OrderName from CRM.Orders where OrderID=c.OrderId)
            //else case when c.OpportunityId is not null and c.OpportunityId>0 then (select OpportunityName from CRM.Opportunities where OpportunityId=c.OpportunityId)
            //else case when c.AccountId is not null and c.AccountId>0 then (select [CRM].[fnGetAccountNameByAccountId](c.AccountId))
            //else case when c.LeadId is not null and c.LeadId>0 then (select [CRM].[fnGetAccountNameByLeadId](c.LeadId))
            //else ''
            //end end end end as CalName       		
            //from CRM.Calendar c
            //      join CRM.EventToPeople ep on c.CalendarId=ep.CalendarId
            //      left join CRM.EventRecurring er on c.RecurringEventId=er.RecurringEventId
            //      where c.IsDeleted=0 and ISNULL(c.IsCancelled,0)=0 and ISNULL(er.IsDeleted,0)=0 
            //      and  c.FranchiseId=@FranchiseId and c.CalendarId=@CalendarId";

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var CalendarEvents = connection.Query<Calendar>("exec CRM.spGETCalendarData @FranchiseId,@CalendarId", new { FranchiseId = FranchiseId, CalendarId = CalendarId }).ToList();
                foreach (var eventCalendar in CalendarEvents)
                {
                    eventCalendar.IsPrivateAccess = this.GetAuthorisedPrivateCalendar(eventCalendar.CalendarId, eventCalendar.IsPrivate, false);
                }
                connection.Close();

                return CalendarEvents;
            }
        }

        public List<Calendar> EventList(int? FranchiseId, List<int> PersonIds, DateTime? StartDate, DateTime? EndDate)
        {
            List<Calendar> result = new List<Calendar>();
            //       string query = @"select  c.*,
            //                case when c.AccountId is not null
            //         then
            //         (select
            //         case when PreferredTFN='C' then CellPhone
            //         else case when PreferredTFN='H' then HomePhone
            //         else case when PreferredTFN='W' then WorkPhone
            //         else case when CellPhone is not null then CellPhone
            //         else case when HomePhone is not null then HomePhone
            //         else case when WorkPhone is not null then WorkPhone
            //         else ''
            //         end end end end end end
            //         from CRM.Customer cus
            //         join CRM.AccountCustomers acus on cus.CustomerId=acus.CustomerId and IsPrimaryCustomer=1
            //         where acus.AccountId=c.AccountId) end as AccountPhone,
            //          case when c.LeadId is not null
            //          then
            //         (select
            //         case when PreferredTFN='C' then CellPhone
            //         else case when PreferredTFN='H' then HomePhone
            //         else case when PreferredTFN='W' then WorkPhone
            //         else case when CellPhone is not null then CellPhone
            //         else case when HomePhone is not null then HomePhone
            //         else case when WorkPhone is not null then WorkPhone
            //         else ''
            //         end end end end end end
            //         from CRM.Customer cus
            //         join CRM.LeadCustomers lcus on cus.CustomerId=lcus.CustomerId and IsPrimaryCustomer=1
            //         where lcus.LeadId=c.LeadId)
            //         end as [PhoneNumber],
            //               (select [CRM].[fnGetAccountNameByAccountId](c.AccountId)) as AccountName,
            //         (select	oppo.OpportunityName from CRM.[Opportunities] oppo
            //         where oppo.OpportunityId=c.OpportunityId) as [OpportunityName],
            //               ep.[EventPersonId] AS [EventPersonId],
            //               ep.[PersonId] AS [PersonId],
            //               ep.[PersonName] AS [PersonName],
            //               case when c.OrderId is not null and c.OrderId>0 then (select OrderName from CRM.Orders where OrderID=c.OrderId)
            //         else case when c.OpportunityId is not null and c.OpportunityId>0 then (select OpportunityName from CRM.Opportunities where OpportunityId=c.OpportunityId)
            //         else case when c.AccountId is not null and c.AccountId>0 then (select [CRM].[fnGetAccountNameByAccountId](c.AccountId))
            //         else case when c.LeadId is not null and c.LeadId>0 then (select [CRM].[fnGetAccountNameByLeadId](c.LeadId))
            //         else ''
            //         end end end end as CalName           		
            //         from CRM.Calendar c
            //               join CRM.EventToPeople ep on c.CalendarId=ep.CalendarId
            //               left join CRM.EventRecurring er on c.RecurringEventId=er.RecurringEventId
            //               where c.IsDeleted=0 and ISNULL(c.IsCancelled,0)=0 and ISNULL(er.IsDeleted,0)=0 
            //and c.EventTypeEnum in (0,1)
            //               and  c.FranchiseId=@FranchiseId and ep.PersonId in @PersonIds and c.StartDate between @StartDate and @EndDate
            //union
            //select  c.*,
            //                case when c.AccountId is not null
            //         then
            //         (select
            //         case when PreferredTFN='C' then CellPhone
            //         else case when PreferredTFN='H' then HomePhone
            //         else case when PreferredTFN='W' then WorkPhone
            //         else case when CellPhone is not null then CellPhone
            //         else case when HomePhone is not null then HomePhone
            //         else case when WorkPhone is not null then WorkPhone
            //         else ''
            //         end end end end end end
            //         from CRM.Customer cus
            //         join CRM.AccountCustomers acus on cus.CustomerId=acus.CustomerId and IsPrimaryCustomer=1
            //         where acus.AccountId=c.AccountId) end as AccountPhone,
            //          case when c.LeadId is not null
            //          then
            //         (select
            //         case when PreferredTFN='C' then CellPhone
            //         else case when PreferredTFN='H' then HomePhone
            //         else case when PreferredTFN='W' then WorkPhone
            //         else case when CellPhone is not null then CellPhone
            //         else case when HomePhone is not null then HomePhone
            //         else case when WorkPhone is not null then WorkPhone
            //         else ''
            //         end end end end end end
            //         from CRM.Customer cus
            //         join CRM.LeadCustomers lcus on cus.CustomerId=lcus.CustomerId and IsPrimaryCustomer=1
            //         where lcus.LeadId=c.LeadId)
            //         end as [PhoneNumber],
            //               (select [CRM].[fnGetAccountNameByAccountId](c.AccountId)) as AccountName,
            //         (select	oppo.OpportunityName from CRM.[Opportunities] oppo
            //         where oppo.OpportunityId=c.OpportunityId) as [OpportunityName],
            //               ep.[EventPersonId] AS [EventPersonId],
            //               ep.[PersonId] AS [PersonId],
            //               ep.[PersonName] AS [PersonName],
            //               case when c.OrderId is not null and c.OrderId>0 then (select OrderName from CRM.Orders where OrderID=c.OrderId)
            //         else case when c.OpportunityId is not null and c.OpportunityId>0 then (select OpportunityName from CRM.Opportunities where OpportunityId=c.OpportunityId)
            //         else case when c.AccountId is not null and c.AccountId>0 then (select [CRM].[fnGetAccountNameByAccountId](c.AccountId))
            //         else case when c.LeadId is not null and c.LeadId>0 then (select [CRM].[fnGetAccountNameByLeadId](c.LeadId))
            //         else ''
            //         end end end end as CalName           		
            //         from CRM.Calendar c
            //               join CRM.EventToPeople ep on c.CalendarId=ep.CalendarId
            //               left join CRM.EventRecurring er on c.RecurringEventId=er.RecurringEventId
            //               where c.IsDeleted=0 and ISNULL(c.IsCancelled,0)=0 and ISNULL(er.IsDeleted,0)=0 
            //and c.EventTypeEnum in (2)
            //               and  c.FranchiseId=@FranchiseId and ep.PersonId in @PersonIds
            //               order by c.CalendarId";
            //query = query.Replace("@PersonIds", PersonIds);

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var CalendarEvents = connection.Query<Calendar>("exec CRM.spGETCalendarData @FranchiseId,@CalendarId,@PersonIds,@StartDate,@EndDate",
                    new { FranchiseId, CalendarId = 0, PersonIds = string.Join(",", PersonIds.ToArray()), StartDate, EndDate }).ToList();
                return CalendarEvents;
            }
        }

        public List<EventRecurring> RecurrenceList(List<int> EventRecurringId)
        {
            string query = @"select * from crm.eventrecurring  where RecurringEventId in @EventRecurringId and ISNULL(IsDeleted,0)=0";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var CalendarRecurringEvents = connection.Query<EventRecurring>(query, new { EventRecurringId = EventRecurringId }).ToList();
                connection.Close();
                return CalendarRecurringEvents;
            }
        }

        public List<CalendarSingleOccurrence> CalendarSingleOccurrenceList(List<int> EventRecurringId)
        {
            string query = @"select * from CRM.CalendarSingleOccurrences  where RecurringEventId in @EventRecurringId";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var SingleOccurrence = connection.Query<CalendarSingleOccurrence>(query, new { EventRecurringId = EventRecurringId }).ToList();
                return SingleOccurrence;
            }
        }

        public List<EventToPerson> EventToPersonList(List<int> CalendaridList)
        {
            string query = @"SELECT
                             etp.[EventPersonId] AS [EventPersonId],
                             etp.[TaskId] AS [TaskId],
                             etp.[CalendarId] AS [CalendarId],
                             etp.[PersonId] AS [PersonId],
                             etp.[CreatedOnUtc] AS [CreatedOnUtc],
                             etp.[PersonEmail] AS [PersonEmail],
                             etp.[RemindMethodEnum] AS [RemindMethodEnum],
                             etp.[PersonName] AS [PersonName],
                             etp.[RemindDate] AS [RemindDate],
                             etp.[DismissedDate] AS [DismissedDate],
                             etp.[SnoozedDate] AS [SnoozedDate],
                             etp.[RecurringNextStartDate] AS [RecurringNextStartDate],
                             etp.[RecurringNextEndDate] AS [RecurringNextEndDate]
                             FROM [CRM].[EventToPeople] AS etp
	                         where etp.CalendarId in @CalendarId order by EventPersonId asc";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var etpList = connection.Query<EventToPerson>(query, new { CalendarId = CalendaridList }).ToList();
                connection.Close();
                return etpList;
            }
        }

        /// <summary>
        /// Deletes a single, master recurring series, or an occurrence in a recurring series
        /// </summary>
        /// <param name="calendarId">Calendar Id to delete, must be master recurring calendar id if deleting an occurrence</param>
        /// <param name="lastUpdatedUtc">The last updated UTC.</param>
        /// <param name="occurStartDate">Optional, original start date of this occurrence</param>
        /// <returns>System.String.</returns>
        public string DeleteEvent(int calendarId, DateTime lastUpdatedUtc, DateTimeOffset? occurStartDate = null)
        {
            if (calendarId <= 0)
                return DataCannotBeNullOrEmpty;
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var sql = @"select * from CRM.Calendar where FranchiseId=@FranchiseId and CalendarId=@CalendarId";

                connection.Open();
                try
                {
                    var cal = connection.Query<Calendar>(sql, new { CalendarId = calendarId, franchiseid = User.FranchiseId }).FirstOrDefault();

                    if (cal == null)
                        return CalendarDoesNotExist;
                    if (cal.IsDeleted) //already marked as deleted
                        return Success;

                    // Delete single occurance - Add single occurance origianl start date in CalendarSingleOccurrence table
                    if (occurStartDate.HasValue)
                    {
                        if (cal.RecurringEventId.HasValue)
                        {
                            if (!cal.RecurringEventId.HasValue)
                                return
                                    "Action cancelled, calendar does not have any recurring event rule to perform delete";

                            var SingleOccurrence = new CalendarSingleOccurrence
                            {
                                DeletedOnUtc = DateTime.UtcNow,
                                OriginalStartDate = (DateTime)TimeZoneManager.ToUTC(occurStartDate),
                                RecurringEventId = cal.RecurringEventId.Value,
                                LastUpdatedUtc = DateTime.UtcNow
                            };
                            var occres = connection.Insert<CalendarSingleOccurrence>(SingleOccurrence);
                        }
                    }
                    else
                    {
                        cal.IsDeleted = true;

                        var ModifiedOccurrences = connection.Query<CalendarSingleOccurrence>("select * from CRM.CalendarSingleOccurrences where CalendarId=@CalendarId", new { CalendarId = calendarId }).FirstOrDefault();
                        if (ModifiedOccurrences != null)
                        {
                            ModifiedOccurrences.DeletedOnUtc = DateTime.Now;
                            ModifiedOccurrences.LastUpdatedUtc = lastUpdatedUtc;
                            connection.Update(ModifiedOccurrences);
                        }

                        //master recurring calendar so we can set it to deleted as well
                        // If calendar object is master data delete the recurring data
                        if (cal.RecurringEventId.HasValue && cal.RecurringEventId > 0 && cal.EventTypeEnum == EventTypeEnum.Series)
                        {
                            var recur = connection.Query<EventRecurring>("SELECT * FROM crm.EventRecurring er WHERE er.RecurringEventId = @RecurringEventId;", new { RecurringEventId = cal.RecurringEventId.Value }).FirstOrDefault();
                            recur.IsDeleted = true;
                            recur.LastUpdatedUtc = lastUpdatedUtc;
                            recur.LastUpdatedByPersonId = User.PersonId;
                            connection.Update(recur);
                        }
                    }

                    cal.LastUpdatedByPersonId = User.PersonId;
                    cal.LastUpdatedUtc = lastUpdatedUtc;

                    connection.Update(cal);

                    ExchangeManager mgr = new ExchangeManager();
                    {
                        mgr.SyncChannel_Up(cal.CalendarId);
                    }
                    return Success;

                }
                catch (Exception ex)
                {
                    return EventLogger.LogEvent(ex);
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public string CancelEvent(int calendarId, DateTime lastUpdatedUtc)
        {
            if (calendarId <= 0) return DataCannotBeNullOrEmpty;

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                try
                {
                    var query = @"Update crm.Calendar set IsCancelled = 1, 
                                lastUpdatedUtc = @lastupdatedutc,
                                lastUpdatedByPersonId = @personid    
                                where Calendarid = @calendarid";
                    connection.Execute(query, new
                    {
                        lastupdatedutc = lastUpdatedUtc,
                        personid = User.PersonId,
                        calendarid = calendarId
                    });

                    ExchangeManager mgr = new ExchangeManager();
                    {
                        mgr.SyncChannel_Up(calendarId);
                    }

                    return Success;
                }
                catch (Exception ex)
                {
                    return EventLogger.LogEvent(ex);
                }

            }
        }

        public string UpdateSavedCalendar(List<SavedCalendarOrder> savedcalviews)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                int sequenceorder = 1;
                foreach (var item in savedcalviews)
                {
                    item.SequenceOrder = sequenceorder;
                    connection.Update<SavedCalendarOrder>(item);
                    var qrysavedcalendar = @"Update crm.savedCalendar  set ViewTitle=@ViewTitle
                                            where SaveCalendarId=@SaveCalendarId";
                    var savedCalendarUsers = connection.Query<SavedCalendar>(qrysavedcalendar, new { SaveCalendarId = item.SaveCalendarId, ViewTitle = item.ViewTitle });
                    sequenceorder = sequenceorder + 1;
                }
                return "Update Success";
            }
            return "Error";
        }

        public List<SavedCalendar> GetSavedCalendar(int franchiseId)
        {
            var savecalquery = @"select * from crm.savedCalendar s inner join crm.SavedCalendarOrder o on s.SaveCalendarId=o.SaveCalendarid
                                            where s.LoginUserid=@LoginUserid  and s.FranchiseId=@FranchiseId and o.IsDeleted=0
                                             and o.LoginUserId=@LoginUserid
                                            order by o.SequenceOrder asc";
            int LoginUserid = SessionManager.CurrentUser.PersonId;
            if (franchiseId == 0) { franchiseId = (int)SessionManager.CurrentFranchise.FranchiseId; }
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var savedcallist = connection.Query<SavedCalendar>(savecalquery, new { LoginUserid = LoginUserid, FranchiseId = franchiseId }).AsEnumerable();
                if (savedcallist != null)
                {
                    var savedCalendarUsersquery = "select * from crm.SavedCalendarUsers where SaveCalendarId=@SaveCalendarId";
                    var savedCalendarOrderquery = "select * from crm.SavedCalendarOrder where SaveCalendarId=@SaveCalendarId";
                    foreach (var item in savedcallist)
                    {
                        var savedCalendarUsers = connection.Query<SavedCalendarUsers>(savedCalendarUsersquery, new { SaveCalendarId = item.SaveCalendarId }).AsEnumerable();
                        var savedCalendarOrder = connection.Query<SavedCalendarOrder>(savedCalendarOrderquery, new { SaveCalendarId = item.SaveCalendarId }).AsEnumerable();
                        item.SavedCalendarUsers = savedCalendarUsers.ToList();
                        item.SavedCalendarOrder = savedCalendarOrder.ToList();
                    }
                }
                return savedcallist.ToList();
            }
        }

        public CalendarVM GetCalendar(int calendarId, int franchiseId)
        {
            var calendar = CRMDBContext.Calendars.Include("Attendees").Include("EventRecurring").Include("EventRecurring.SingleOccurrences")
                   .FirstOrDefault(w => !w.IsDeleted && w.CalendarId == calendarId && w.FranchiseId == franchiseId);
            CalendarVM CalVM = MapTo(calendar);
            return CalVM;
        }

        /// <summary>
        /// Updates the event.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="donotUpdateReminders">if set to <c>true</c> [donot update reminders].</param>
        /// <returns>System.String.</returns>
        public string UpdateEvent(CalendarVM model, out string warning, bool donotUpdateReminders = false, System.DateTimeOffset? originalStartDate = null, bool isUpdateFromCalendar = false)
        {
            warning = null;

            if (model.OrganizerPersonId == null)
            {
                model.OrganizerPersonId = SessionManager.CurrentUser.PersonId;
            }
            if (model == null)
                return DataCannotBeNullOrEmpty;
            if (string.IsNullOrEmpty(model.Subject))
                return EntityRequiresSubject;
            if ((!model.OrganizerPersonId.HasValue || model.OrganizerPersonId <= 0) && string.IsNullOrEmpty(model.OrganizerEmail))
                return EntityRequiresOrganizer;
            if (model.Attendees == null || model.Attendees.Count == 0)
                return EntityRequiresAttendee;

            if (model.FranchiseId <= 0)
                model.FranchiseId = User.FranchiseId.Value;

            if (model.StartDate > model.EndDate) //the dates are swapped? lets switch it without alerting user
            {
                var tmp = model.StartDate;
                model.StartDate = model.EndDate;
                model.EndDate = tmp;
            }

            try
            {
                //important to also check franchise id, so user can't just pass any random calendar id and get some calendar that isnt in their franchise
                var original = CRMDBContext.Calendars.Include("Attendees").Include("EventRecurring").Include("EventRecurring.SingleOccurrences")
                    .FirstOrDefault(w => !w.IsDeleted && w.CalendarId == model.CalendarId && w.FranchiseId == model.FranchiseId);

                if (original == null)
                    return CalendarDoesNotExist;
                // if (original.RevisionSequence != model.RevisionSequence)
                //   return InvalidRevisionSequence;

                // TODO: is this required in TP???
                //if ((AssignedPermission.CanUpdate && original.OrganizerPersonId == User.PersonId) || BasePermission.CanUpdate)
                //{
                //}
                //else
                //    return Unauthorized;

                //auto accept if the assigned is also the creator
                int? originalLeadId = original.LeadId;

                //posted data may be encoded so decode and store the correct text
                original.Subject = HttpUtility.HtmlDecode(model.Subject);
                original.Message = HttpUtility.HtmlDecode(model.Message);
                original.EndDate = model.EndDate;
                original.StartDate = model.StartDate;
                original.IsAllDay = model.IsAllDay;
                original.IsPrivate = model.IsPrivate;
                if (!donotUpdateReminders)
                {
                    original.ReminderMinute = model.ReminderMinute;
                    original.RemindMethodEnum = model.RemindMethodEnum;
                    original.FirstRemindDate = model.FirstRemindDate;
                }
                original.IsCancelled = model.IsCancelled;
                original.Location = model.Location;
                original.RevisionSequence++;

                int? personToRemove = null;

                if (original.AssignedPersonId != model.AssignedPersonId)
                {
                    personToRemove = original.AssignedPersonId;
                }

                //var originAssignedPersonId = 0;
                //if (original.AssignedPersonId != model.AssignedPersonId)
                //{
                //    originAssignedPersonId = original.AssignedPersonId;
                //}

                original.AssignedPersonId = model.AssignedPersonId;
                original.AssignedName = this.CRMDBContext.People.FirstOrDefault(f => f.PersonId == model.AssignedPersonId).FullName;

                //dont update organizer email?
                //original.OrganizerEmail = model.OrganizerEmail;

                if (model.LeadId == null || model.LeadId <= 0)
                {
                    original.LeadId = null; // this will prevent foreign key exception since there is no leadId of 0
                    original.LeadNumber = null;
                }
                else
                {
                    original.LeadId = model.LeadId;
                    original.LeadNumber = model.LeadNumber;
                }
                var entry = CRMDBContext.Entry(original);
                var changes = Util.GetEFChangedProperties(entry, original);

                //manually check type and set history with friendly text instead of IDs
                if (original.AptTypeEnum != model.AptTypeEnum)
                {
                    original.AptTypeEnum = model.AptTypeEnum;
                    if (changes == null)
                        changes = new XElement("Calendar");

                    changes.Add(
                        new XElement("AptTypeEnum", new XAttribute("name", "Event Type"),
                            new XElement("Original", original.AptTypeEnum),
                            new XElement("Current", model.AptTypeEnum)));
                }

                if (original.OrganizerPersonId != model.OrganizerPersonId)
                {
                    original.OrganizerPersonId = model.OrganizerPersonId;
                    if (changes == null)
                        changes = new XElement("Calendar");

                    var old = CacheManager.UserCollection.FirstOrDefault(f => f.PersonId == original.OrganizerPersonId);
                    var cur = CacheManager.UserCollection.FirstOrDefault(f => f.PersonId == model.OrganizerPersonId);
                    if (old != null && cur != null)
                    {
                        changes.Add(new XElement("OrganizerPersonId", new XAttribute("name", "Organizer"),
                            new XElement("Original", new XAttribute("id", old.PersonId), old.Person.FullName),
                            new XElement("Current", new XAttribute("id", cur.PersonId), cur.Person.FullName)));
                    }
                }

                var peopleMatched = (from orig in original.Attendees
                                     join cur in model.Attendees on new { orig.PersonId, orig.PersonEmail } equals new { cur.PersonId, cur.PersonEmail }
                                     select new { Current = cur, Original = orig });
                var peopleToRemove = original.Attendees.Except(peopleMatched.Select(s => s.Original)).ToList();
                var peopleToAdd = model.Attendees.Except(peopleMatched.Select(s => s.Current)).ToList();

                var conflicts = peopleToAdd.Where(attendee => this.CRMDBContext.Calendars.Any(t => !t.IsDeleted && t.StartDate == model.StartDate && t.EndDate == model.EndDate && t.Attendees.Any(a => a.PersonId == attendee.PersonId))).ToList();
                if (conflicts.Any())
                {
                    warning = string.Format("This appointment conflicts with an existing appointment for {0}",
                        string.Join(", ", conflicts.Select(c => string.Format("{0} ({1})", c.PersonName, c.PersonEmail))));
                }

                foreach (var per in peopleMatched)
                {
                    //remind on changed so reset reminders
                    if (per.Original.RemindDate != per.Current.RemindDate && !donotUpdateReminders)
                    {
                        per.Original.RemindDate = per.Current.RemindDate;
                        per.Original.RemindMethodEnum = per.Current.RemindMethodEnum;
                        per.Original.DismissedDate = null;
                        per.Original.SnoozedDate = null;
                    }
                }

                foreach (var per in peopleToRemove)
                {
                    original.Attendees.Remove(per);
                }

                //if (personToRemove.HasValue)
                //{
                //    var person = original.Attendees.FirstOrDefault(p => p.PersonId == personToRemove);
                //    if (person != null)
                //    {
                //        original.Attendees.Remove(person);
                //    }
                //}

                foreach (var per in peopleToAdd)
                {
                    original.Attendees.Add(per);
                }

                if (original.EventRecurring != null && model.EventRecurring != null)
                {
                    original.EventRecurring.EndsAfterXOccurrences = model.EventRecurring.EndsAfterXOccurrences;
                    original.EventRecurring.EndsOn = model.EventRecurring.EndsOn;
                    original.EventRecurring.DayOfWeekEnum = model.EventRecurring.DayOfWeekEnum;
                    original.EventRecurring.PatternEnum = model.EventRecurring.PatternEnum;
                    original.EventRecurring.RecursEvery = model.EventRecurring.RecursEvery;
                    original.EventRecurring.StartDate = model.EventRecurring.StartDate;
                    original.EventRecurring.EndDate = model.EventRecurring.EndDate;
                    original.EventRecurring.LastUpdatedByPersonId = User.PersonId;
                    original.EventRecurring.LastUpdatedUtc = model.LastUpdatedUtc;
                    original.EventRecurring.DayOfWeekIndex = model.EventRecurring.DayOfWeekIndex;
                    original.EventRecurring.DayOfMonth = model.EventRecurring.DayOfMonth;
                    original.EventRecurring.MonthEnum = model.EventRecurring.MonthEnum;

                    if (!originalStartDate.HasValue)
                    {
                        originalStartDate = model.StartDate;
                    }

                    if (model.EventRecurring.SingleOccurrences != null)
                    {
                        if (original.EventRecurring.SingleOccurrences == null)
                            original.EventRecurring.SingleOccurrences = new List<CalendarSingleOccurrence>();

                        foreach (var occur in model.EventRecurring.SingleOccurrences)
                        {
                            //add deleted event if it isnt already deleted
                            if (!original.EventRecurring.SingleOccurrences.Any(a => a.OriginalStartDate == occur.OriginalStartDate))
                            {
                                original.EventRecurring.SingleOccurrences.Add(occur);
                            }
                        }
                    }
                }
                else if (model.EventRecurring != null)
                {
                    original.EventTypeEnum = EventTypeEnum.Series;
                    if (model.EventRecurring.CreatedByPersonId <= 0)
                    {
                        model.EventRecurring.CreatedByPersonId = User.PersonId;
                    }
                    original.EventRecurring = model.EventRecurring;
                }
                else if (!model.RecurringEventId.HasValue && model.EventRecurring == null && model.EventTypeEnum == EventTypeEnum.Series)// only clear recurring if there is no id and recurring data
                {
                    original.EventRecurring.IsDeleted = true;
                    original.EventTypeEnum = EventTypeEnum.Single;
                    original.RecurringEventId = null;
                }

                original.LastUpdatedByPersonId = model.LastUpdatedByPersonId;
                original.LastUpdatedUtc = model.LastUpdatedUtc;

                if (changes != null && (model.LeadId > 0 || originalLeadId > 0))
                {
                    if (model.LeadId > 0 && model.LeadId != originalLeadId) //if new leadid is diff from original then add the history on new lead
                    {
                        var leadToLog = new Lead() { LeadId = model.LeadId.Value };
                        CRMDBContext.Leads.Attach(leadToLog);

                        //leadToLog.EditHistories.Add(new EditHistory()
                        //{
                        //    LoggedByPersonId = model.LastUpdatedByPersonId,
                        //    IPAddress = HttpContext.Current != null && HttpContext.Current.Request != null ? HttpContext.Current.Request.UserHostAddress : null,
                        //    HistoryValue = changes.ToString(SaveOptions.DisableFormatting)
                        //});
                    }
                    if (originalLeadId > 0)
                    {
                        //if leadid is changed and different from original record the change on old lead
                        if (!model.LeadId.HasValue || model.LeadId <= 0)
                        {
                            var op = changes.Attribute("operation");
                            if (op != null)
                                op.Value = "delete";
                        }
                        var leadToLog = new Lead() { LeadId = originalLeadId.Value };
                        CRMDBContext.Leads.Attach(leadToLog);

                        //leadToLog.EditHistories.Add(new EditHistory()
                        //{
                        //    LoggedByPersonId = model.LastUpdatedByPersonId,
                        //    IPAddress = HttpContext.Current != null && HttpContext.Current.Request != null ? HttpContext.Current.Request.UserHostAddress : null,
                        //    HistoryValue = changes.ToString(SaveOptions.DisableFormatting)
                        //});
                    }
                }

                if (isUpdateFromCalendar && personToRemove.HasValue)
                {
                    var entity = CRMDBContext.EventToPeople_History.Where(v => v.PersonId == personToRemove.Value && v.CalendarId == original.CalendarId).FirstOrDefault();
                    if (entity == null)
                    {
                        var en = new EventToPeople_History();
                        en.CalendarId = original.CalendarId;
                        en.PersonId = personToRemove.Value;
                        CRMDBContext.EventToPeople_History.Add(en);
                    }
                }

                CRMDBContext.SaveChanges();

                return Success;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }
        }

        /// <summary>
        /// Inserts a new calendar event and returns calendar id
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="model">The model.</param>
        /// <param name="warning"></param>
        /// <returns>System.String.</returns>
        public string InsertEvent(out int id, CalendarVM model, out string warning, System.DateTimeOffset? originalStartDate = null, TimeZoneEnum? tze = null)
        {
            int recurId = 0;

            return InsertEvent(out id, out recurId, model, out warning, originalStartDate, tze);
        }

        /// <summary>
        /// Inserts a new calendar event and returns calendar id and recurring master id
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="recurringEventId">The recurring event identifier.</param>
        /// <param name="model">The model.</param>
        /// <param name="warning">The warning.</param>
        /// <returns>System.String.</returns>
        public string InsertEvent(out int id, out int recurringEventId, CalendarVM model, out string warning, System.DateTimeOffset? originalStartDate = null, TimeZoneEnum? tze = null)
        {
            warning = null;

            id = 0;
            recurringEventId = 0;

            if (model == null)
                return DataCannotBeNullOrEmpty;
            //if (string.IsNullOrEmpty(model.Subject))
            //    return EntityRequiresSubject;
            //if (string.IsNullOrEmpty(model.Location))
            //    return EntityRequiresLocation;
            if ((!model.AssignedPersonId.HasValue || model.AssignedPersonId <= 0))
                return EntityRequiresAttendees;
            //if (!BasePermission.CanCreate)
            //    return Unauthorized;

            if (model.FranchiseId <= 0)
                model.FranchiseId = User.FranchiseId.Value;

            if (model.StartDate > model.EndDate) //the dates are swapped? lets switch it without alerting user
            {
                var tmp = model.StartDate;
                model.StartDate = model.EndDate;
                model.EndDate = tmp;
            }

            try
            {
                model.CalendarGuid = Guid.NewGuid();

                if (model.LeadId <= 0)
                {
                    model.LeadId = null; // this will prevent foreign key exception since there is no leadId 0
                    model.LeadNumber = null;
                }

                //posted data may be encoded so decode and store the correct text
                model.Subject = HttpUtility.HtmlDecode(model.Subject.Trim());
                model.Message = HttpUtility.HtmlDecode(model.Message);
                model.OrganizerEmail = User.Person.PrimaryEmail;
                model.OrganizerName = User.Person.FullName;
                model.OrganizerPersonId = User.PersonId;
                model.AssignedName = this.CRMDBContext.People.FirstOrDefault(f => f.PersonId == model.AssignedPersonId).FullName;
                if (!originalStartDate.HasValue)
                {
                    originalStartDate = model.StartDate;
                }

                //if we are inserting a new calendar event because user opted to edit only "this occurrence" and not the entire series
                //then we only need to save the ID to the new calendar event.
                //otherwise create a new recurring event
                //if (model.CalendarId == 0)
                //{
                //    if (model.RecurringEventId.HasValue && model.RecurringEventId.Value > 0)
                //    {
                //        model.EventRecurring = null;
                //        model.ModifiedOccurrences = new List<CalendarSingleOccurrence>();
                //        model.ModifiedOccurrences.Add(new CalendarSingleOccurrence
                //        {
                //            OriginalStartDate = originalStartDate.Value,
                //            RecurringEventId = model.RecurringEventId.Value,
                //            LastUpdatedUtc = (DateTime)model.LastUpdatedUtc
                //        });
                //    }
                //    if (model.EventRecurring != null && model.EventRecurring.CreatedByPersonId <= 0)
                //    {
                //        model.EventRecurring.CreatedByPersonId = User.PersonId;
                //    }
                //}

                if (model.CalendarId > 0)
                {
                    //Update Event
                    id = model.CalendarId;
                    using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                    {

                        string qselect = "select * from CRM.Calendar where CalendarId=@CalendarId";
                        var CalOriginal = connection.Query<Calendar>(qselect, new { CalendarId = model.CalendarId }).FirstOrDefault();

                        if (CalOriginal.RecurringEventId != null && CalOriginal.RecurringEventId > 0)
                        {
                            CalOriginal.EventRecurring = connection.Query<EventRecurring>("select * from CRM.EventRecurring where RecurringEventId=@RecurringEventId", new { RecurringEventId = CalOriginal.RecurringEventId }).FirstOrDefault();

                            CalOriginal.ModifiedOccurrences = connection.Query<CalendarSingleOccurrence>("select * from CRM.CalendarSingleOccurrences where RecurringEventId=@RecurringEventId", new { RecurringEventId = CalOriginal.RecurringEventId }).ToList();
                        }


                        /// Recurrence Update

                        if (CalOriginal.EventRecurring != null && model.EventRecurring != null)
                        {
                            CalOriginal.EventRecurring.EndsAfterXOccurrences = model.EventRecurring.EndsAfterXOccurrences;
                            CalOriginal.EventRecurring.EndsOn = model.EventRecurring.EndsOn;
                            CalOriginal.EventRecurring.DayOfWeekEnum = model.EventRecurring.DayOfWeekEnum;
                            CalOriginal.EventRecurring.PatternEnum = model.EventRecurring.PatternEnum;
                            CalOriginal.EventRecurring.RecursEvery = model.EventRecurring.RecursEvery;
                            CalOriginal.EventRecurring.StartDate = model.EventRecurring.StartDate;
                            CalOriginal.EventRecurring.EndDate = model.EventRecurring.EndDate;
                            CalOriginal.EventRecurring.LastUpdatedByPersonId = User.PersonId;
                            CalOriginal.EventRecurring.LastUpdatedUtc = model.LastUpdatedUtc;
                            CalOriginal.EventRecurring.DayOfWeekIndex = model.EventRecurring.DayOfWeekIndex;
                            CalOriginal.EventRecurring.DayOfMonth = model.EventRecurring.DayOfMonth;
                            CalOriginal.EventRecurring.MonthEnum = model.EventRecurring.MonthEnum;
                            connection.Update(CalOriginal.EventRecurring);

                            if (CalOriginal.ModifiedOccurrences != null && CalOriginal.ModifiedOccurrences.Count > 0)
                            {
                                foreach (var moc in CalOriginal.ModifiedOccurrences)
                                {
                                    var oSD = TimeZoneManager.ToLocal(moc.OriginalStartDate).Date;
                                    oSD = oSD.Add(new TimeSpan(model.StartDate.Hour, model.StartDate.Minute, 0));
                                    moc.OriginalStartDate = TimeZoneManager.ToUTC(oSD);
                                    connection.Update(moc);
                                }
                            }

                        }
                        else if (model.EventRecurring != null)
                        {
                            // Insert Event recurring and update EventRecurringId to Calendar table
                            var EventRecurringId = connection.Insert<EventRecurring>(model.EventRecurring);

                            model.EventTypeEnum = EventTypeEnum.Series;
                            model.RecurringEventId = EventRecurringId;
                        }
                        else if (model.RecurringEventId == null && model.EventRecurring == null && model.EventTypeEnum == EventTypeEnum.Series)// only clear recurring if there is no id and recurring data
                        {
                            // delete event reccuring and update series to single
                            if (CalOriginal.EventRecurring != null)
                            {
                                CalOriginal.EventRecurring.IsDeleted = true;
                                var result = connection.Update<EventRecurring>(CalOriginal.EventRecurring);
                            }

                            // Delete the single occurance appointment in Db and EWS
                            ExchangeManager mgr = new ExchangeManager();
                            var caloccurencelst = connection.Query<Calendar>("select * from CRM.Calendar where RecurringEventId=@RecurringEventId and EventTypeEnum=1 and IsDeleted=0", new { RecurringEventId = CalOriginal.RecurringEventId }).ToList();
                            foreach (var occ in caloccurencelst)
                            {
                                occ.IsDeleted = true;
                                connection.Update(occ);
                                mgr.SyncChannel_Up(occ.CalendarId);
                            }

                            model.EventTypeEnum = EventTypeEnum.Single;
                        }


                        CalOriginal.LeadId = model.LeadId;
                        CalOriginal.AccountId = model.AccountId;
                        CalOriginal.OpportunityId = model.OpportunityId;
                        CalOriginal.OrderId = model.OrderId;
                        CalOriginal.CaseId = model.CaseId;
                        CalOriginal.VendorCaseId = model.VendorCaseId;
                        CalOriginal.Subject = model.Subject;
                        CalOriginal.Message = model.Message;
                        CalOriginal.StartDate = TimeZoneManager.ToUTC(model.StartDate);
                        CalOriginal.EndDate = TimeZoneManager.ToUTC(model.EndDate);
                        CalOriginal.EventTypeEnum = model.EventTypeEnum;
                        CalOriginal.AptTypeEnum = model.AptTypeEnum;
                        CalOriginal.IsCancelled = model.IsCancelled;
                        CalOriginal.IsAllDay = model.IsAllDay;
                        CalOriginal.LastUpdatedUtc = model.LastUpdatedUtc;
                        CalOriginal.IsDeleted = model.IsDeleted;
                        CalOriginal.LastUpdatedByPersonId = User.PersonId;
                        CalOriginal.OrganizerPersonId = model.OrganizerPersonId;
                        CalOriginal.OrganizerEmail = model.OrganizerEmail;
                        CalOriginal.OrganizerName = model.OrganizerName;
                        CalOriginal.RecurringEventId = model.RecurringEventId;
                        CalOriginal.IsPrivate = model.IsPrivate;
                        CalOriginal.Location = model.Location;
                        CalOriginal.ReminderMinute = model.ReminderMinute;
                        CalOriginal.RemindMethodEnum = model.RemindMethodEnum;
                        CalOriginal.AssignedPersonId = model.AssignedPersonId;
                        CalOriginal.AssignedName = model.AssignedName;
                        CalOriginal.PhoneNumber = model.PhoneNumber;

                        connection.Update(CalOriginal);

                        /// - Update attendee
                        string Query = "";
                        Query = "Delete  from Crm.EventToPeople where CalendarId=@CalendarId";
                        var deleteTask = connection.Query<int>(Query, new { CalendarId = model.CalendarId });

                        foreach (var attende in model.Attendees)
                        {
                            attende.CalendarId = model.CalendarId;
                            var newEventToTaskId = connection.Insert<EventToPerson>(attende);
                        }
                        /// End Update attendee



                    }
                }
                else
                {
                    //Insert Event
                    using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                    {
                        if (model.EventRecurring != null && model.EventRecurring.RecurringEventId == 0 && model.EventTypeEnum == EventTypeEnum.Series)
                        {
                            var EventRecurringId = connection.Insert<EventRecurring>(model.EventRecurring);
                            model.RecurringEventId = Int32.Parse(EventRecurringId.ToString());
                        }

                        var modelCalendar = MapFrom(model);
                        var CalendarId = connection.Insert<Calendar>(modelCalendar);
                        id = (int)CalendarId;

                        if (model.RecurringEventId != null && model.RecurringEventId > 0 && model.EventTypeEnum == EventTypeEnum.Occurrence)
                        {
                            var SingleOccurrence = new CalendarSingleOccurrence
                            {
                                CalendarId = id,
                                OriginalStartDate = (DateTimeOffset)TimeZoneManager.ToUTC(originalStartDate.Value),
                                RecurringEventId = model.RecurringEventId.Value,
                                LastUpdatedUtc = model.LastUpdatedUtc
                            };
                            var occres = connection.Insert<CalendarSingleOccurrence>(SingleOccurrence);
                        }

                        foreach (var attendee in model.Attendees)
                        {
                            attendee.CalendarId = Int32.Parse(CalendarId.ToString());
                            var attendeeId = connection.Insert<EventToPerson>(attendee);
                        }
                    }

                    if (model.RecurringEventId.HasValue)
                        recurringEventId = model.RecurringEventId.Value;
                }

                return Success;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }
        }

        public string SaveCalendar(SaveCalendar savecalmodel)
        {
            var LoginUserid = SessionManager.CurrentUser.PersonId;
            var ViewTitle = savecalmodel.ViewTitle;
            int ViewMode = 0;//= savecalmodel.ViewType;
            if (savecalmodel.ViewType == "day") { ViewMode = 1; }
            if (savecalmodel.ViewType == "week") { ViewMode = 2; }
            if (savecalmodel.ViewType == "month") { ViewMode = 3; }
            if (savecalmodel.ViewType == "workWeek") { ViewMode = 4; }
            var EmailId = SessionManager.CurrentUser.Email;
            var FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
            try
            {
                var Query = string.Empty;

                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    #region added by azhagu. Fix for the isse Maximum 5 tabs even after deleted 2

                    //Query = "Select * from crm.SavedCalendar where LogInUserId=@PersonId";
                    Query = @"select * from crm.savedCalendar s inner join crm.SavedCalendarOrder o on s.SaveCalendarId=o.SaveCalendarid
                                            where s.LoginUserid=@LoginUserid  and s.FranchiseId=@FranchiseId and o.IsDeleted=0
                                             and o.LoginUserId=@LoginUserid
                                            order by o.SequenceOrder asc";

                    #endregion added by azhagu. Fix for the isse Maximum 5 tabs even after deleted 2

                    var checkexistViews = connection.Query<SavedCalendar>(Query, new { LoginUserid = LoginUserid, FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();

                    if (checkexistViews != null)
                    {
                        if (checkexistViews.Count >= 5)
                        {
                            return "Maximum 5 tabs can be defined";
                        }
                    }
                    var savecalendar = new SavedCalendar();
                    savecalendar.LoginUserid = LoginUserid;
                    savecalendar.ViewTitle = ViewTitle;
                    savecalendar.Viewmode = ViewMode.ToString();
                    savecalendar.EmailId = EmailId;
                    savecalendar.FranchiseId = FranchiseId;
                    savecalendar.IsBussinessHour = savecalmodel.isBussinessHour;
                    var SaveCalendarid = connection.Insert(savecalendar);

                    //Insert Data Into SavedCalendarUsers
                    foreach (var user in savecalmodel.PersonIds)
                    {
                        Query = "";
                        Query = "INSERT INTO CRM.SavedCalendarUsers ";
                        Query = Query + "(SaveCalendarid,SelectedUserid,SelectedRoleid,LoginUserid) ";
                        Query = Query + "VALUES (@SaveCalendarid,@SelectedUserid,@SelectedRoleid, @LoginUserid)";
                        if (connection.State == 0)
                        {
                            connection.Execute(Query, new
                            {
                                SaveCalendarid = SaveCalendarid,
                                LoginUserid = LoginUserid,
                                SelectedUserid = user,
                                SelectedRoleid = user,
                            });
                        }
                    }
                    var insertedCalendarOrderId = 0;
                    //Insert data into SavedCalendarOrder
                    for (int id = 0; id <= 0; id++)
                    {
                        Query = "";
                        Query = "INSERT INTO CRM.SavedCalendarOrder ";
                        Query = Query + "(SaveCalendarid,SequenceOrder,ViewTitle,LoginUserid,IsDefault) ";
                        Query = Query + "VALUES (@SaveCalendarid,@SequenceOrder,@ViewTitle, @LoginUserid,@IsDefault)";
                        if (connection.State == 0)
                        {
                            insertedCalendarOrderId = connection.Execute(Query, new
                            {
                                SaveCalendarid = SaveCalendarid,
                                LoginUserid = LoginUserid,
                                ViewTitle = ViewTitle,
                                SequenceOrder = id,
                                IsDefault = savecalmodel.IsDefault,
                            });
                        }
                    }
                    // This will update the existing value IsDefault  to false if this view is true
                    if (insertedCalendarOrderId > 0 && savecalmodel.IsDefault)
                    {
                        var existDefaultViews = checkexistViews.Where(x => x.IsDefault == true);
                        foreach (var item in existDefaultViews)
                        {
                            Query = "Update CRM.SavedCalendarOrder set IsDefault = @isDefault  where SaveCalendarid=@savedCalendarId";
                            if (connection.State == 0)
                            {
                                connection.Execute(Query, new
                                {
                                    savedCalendarId = item.SaveCalendarId,
                                    isDefault = false,
                                });
                            }
                        }
                    }
                }
                return Success;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public List<Type_Appointments> GetTypeAppointments()
        {
            var appointmentTypes = CRMDBContext.Type_Appointments.ToList();
            return appointmentTypes;
        }

        public List<CalendarAccount> GetAccounts(int? id)
        {
            List<CalendarAccount> result = null;
            if (id == 0)
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    var query = string.Empty;

                    query = @"select a.AccountId,a.PersonId,c.FirstName +' ' +c.Lastname as FullName from crm.Accounts a
                    inner join crm.customer c on a.PersonId= c.Customerid
                    where a.FranchiseId=@id";

                    connection.Open();
                    result = connection.Query<CalendarAccount>(query, new { id = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                    connection.Close();
                    return result;
                }
            }
            else if (id == 1)
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    var query = string.Empty;

                    query = @"select a.LeadId as AccountId,a.PersonId,c.FirstName +' ' +c.Lastname as FullName from crm.Leads a
                        inner join crm.customer c on a.PersonId= c.Customerid
                        where a.FranchiseId=@id";

                    connection.Open();
                    result = connection.Query<CalendarAccount>(query, new { id = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                    connection.Close();
                    return result;
                }
            }
            else if (id == 2)
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    //Display Account and Leads in calendar
                    var query = string.Empty;

                    query = @"select a.LeadId as AccountId,a.PersonId,c.FirstName +' ' +c.Lastname +'-Lead' as FullName from crm.Leads a
                        inner join crm.customer c on a.PersonId= c.Customerid
                        where a.FranchiseId=@id";

                    connection.Open();
                    result = connection.Query<CalendarAccount>(query, new { id = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                    connection.Close();

                    query = @"select a.AccountId,a.PersonId,c.FirstName +' ' +c.Lastname +'-Account' as FullName from crm.Accounts a
                    inner join crm.customer c on a.PersonId= c.Customerid
                    where a.FranchiseId=@id";

                    connection.Open();
                    var accounresult = connection.Query<CalendarAccount>(query, new { id = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                    connection.Close();

                    var mergeleadaccount = result.Union(accounresult).AsQueryable();

                    return mergeleadaccount.ToList();
                }
            }
            else
            {
                return result;
            }
        }

        public List<string> GetAttendees(int? id, bool task = false)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = string.Empty;
                if (task == false)
                {
                    query = @"select PersonEmail as Email from [CRM].[EventToPeople] where calendarId=@id  ";
                }
                else
                {
                    query = @"select PersonEmail as Email  from [CRM].[EventToPeople] where TaskId=@id  ";
                }

                connection.Open();
                var result = connection.Query<string>(query, new { id = id }).ToList();
                connection.Close();
                return result;
            }
        }

        public List<SalesAgents> GetSalesAgents(int? id, bool task = false)
        {
            if (id > 0)
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    var query = string.Empty;
                    if (task == false)
                    {
                        query = @"select PersonId as OwnerID,PersonName as Name,PersonEmail as Email from [CRM].[EventToPeople] where calendarId=@id  ";
                    }
                    else
                    {
                        query = @"select PersonId as OwnerID,PersonName as Name,PersonEmail as Email from [CRM].[EventToPeople] where TaskId=@id  ";
                    }

                    connection.Open();
                    var result = connection.Query<SalesAgents>(query, new { id = id }).ToList();
                    connection.Close();
                    return result;
                }
            }
            else
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    var query = @"select * from(select PersonId as OwnerID,FirstName + ' ' + LastName
                        as Name,PrimaryEmail as Email FROM [CRM].[Person]) a  where a.Name is not null  ";
                    connection.Open();
                    var result = connection.Query<SalesAgents>(query).ToList();
                    connection.Close();
                    return result;
                }
            }
        }

        public string GetTaskBackGroundColor(int attendyId)
        {
            var users = SessionManager.CurrentFranchise.UserCollection().Select(s => new
            {
                s.UserId,
                s.Person.FullName,
                s.PersonId,
                s.ColorId,
                s.Email,
                color = string.Format("{1}", s.ColorId, CacheManager.ColorPalettes.FirstOrDefault(f => f.ColorId == s.ColorId).BGColorToRGB(), true ? " !important" : ""),
            }).ToList();
            //background-color:rgb(23,168,102) !important;border-color:rgb(23,168,102) !important;color:rgb(0,0,0) !important
            if (users.Where(u => u.PersonId == attendyId).FirstOrDefault() != null)
            {
                var color = users.Where(u => u.PersonId == attendyId).FirstOrDefault().color;
                return color;
            }
            else
            {
                return null;
            }
        }

        public string GetUserTextColor(int attendyId)
        {
            var users = SessionManager.CurrentFranchise.UserCollection().Select(s => new
            {
                s.UserId,
                s.Person.FullName,
                s.PersonId,
                s.ColorId,
                s.Email,
                color = string.Format("{1}", s.ColorId, CacheManager.ColorPalettes.FirstOrDefault(f => f.ColorId == s.ColorId).FGColorToRGB(), true ? " !important" : ""),
            }).ToList();
            //background-color:rgb(23,168,102) !important;border-color:rgb(23,168,102) !important;color:rgb(0,0,0) !important
            if (users.Where(u => u.PersonId == attendyId).FirstOrDefault() != null)
            {
                var color = users.Where(u => u.PersonId == attendyId).FirstOrDefault().color;
                return color;
            }
            else
            {
                return null;
            }
        }

        public List<SavedCalendarOrder> GetManagedCalendarListbyUserId(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = string.Empty;
                //select* from crm.SavedCalendarUsers where LoginUserid = 2467076
                //query = @"select * from crm.SavedCalendarOrder where LoginUserid=@id  and IsDeleted=0 order by SequenceOrder asc";

                query = @"select sco.*,sc.IsBussinessHour from crm.SavedCalendarOrder sco
                            join crm.savedcalendar sc on sc.SaveCalendarId = sco.SaveCalendarId
                            where sco.LoginUserid=@id and sc.loginuserid = @id and IsDeleted=0
                            order by SequenceOrder asc";

                connection.Open();
                var result = connection.Query<SavedCalendarOrder>(query, new { id = id }).ToList();
                connection.Close();
                return result;
            }
        }

        public int DeleteSavedCalendar(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = string.Empty;
                query = @"update crm.SavedCalendarOrder set IsDeleted=1 where Id=@id  ";
                connection.Open();
                int result = connection.Execute(query, new
                {
                    Id = id
                });
                connection.Close();
                return result;
            }
        }


        public bool GetAuthorisedPrivateCalendar(int eventId, bool? isPrivate, bool isTask)

        {
            var totalAttendees = this.GetAttendees(eventId, isTask);
            var currentUserEmail = SessionManager.CurrentUser.Email;
            if (isPrivate == true)
            {
                if (totalAttendees.Any(x => x.Contains(currentUserEmail)))
                    return true;
                else
                    return false;
            }
            else
                return true;
        }

        #region ----- Tochpoint/SendEmail -----

        public string SendEmail(CalendarVM model, EmailType emailType)
        {
            try
            {
                FranchiseManager _franMgr = new FranchiseManager(null);
                var franchise = _franMgr.GetFEinfo(model.FranchiseId);
                EmailManager emailMgr = new EmailManager(User, franchise);

                List<string> toAddresses = new List<string>();
                List<string> ccAddresses = new List<string>();
                List<string> bccAddresses = new List<string>();
                CustomerTP tocustomerAddress = new CustomerTP();

                TerritoryDisplay data = new TerritoryDisplay();

                if (model.OpportunityId != null && model.OpportunityId > 0)
                    data = emailMgr.GetTerritoryDisplayByOpp((int)model.OpportunityId);
                else if (model.AccountId != null && model.AccountId > 0)
                    data = emailMgr.GetTerritoryDisplayByAccountId((int)model.AccountId);
                else if (model.LeadId != null && model.LeadId > 0)
                    data = emailMgr.GetTerritoryDisplayByLeadId((int)model.LeadId);

                //BUILD - From email address
                string fromEmailAddress = "";
                if (data != null && data.Id == null)
                    fromEmailAddress = emailMgr.GetFranchiseEmail(franchise.FranchiseId, emailType);
                else
                {
                    if (franchise.AdminEmail != "")
                        fromEmailAddress = franchise.AdminEmail;
                    else if (franchise.OwnerEmail != "")
                        fromEmailAddress = franchise.OwnerEmail;
                }




                var EmailSettings = emailMgr.GetConfiguredEmails().Where(x => x.EmailType == (int)emailType).FirstOrDefault();
                if (EmailSettings != null)
                {
                    if (model.OpportunityId != null && model.OpportunityId > 0)
                    {
                        if (EmailSettings.CopyAssignedSales == true)
                        {
                            var salesemail = emailMgr.GetSalesPersonEmail((int)model.OpportunityId);
                            if (salesemail != null && !string.IsNullOrEmpty(salesemail.Email) && salesemail.IsDisabled != true)
                                //if (salesemail.Email != null && salesemail.Email != "" && salesemail.IsDisabled != true )
                                ccAddresses.Add(salesemail.Email);
                        }
                        if (EmailSettings.CopyAssignedInstaller == true)
                        {
                            var installeremail = emailMgr.GetInstallerEmail((int)model.OpportunityId);
                            if (installeremail != null && !string.IsNullOrEmpty(installeremail.Email) && installeremail.IsDisabled != true)
                                //if (installeremail.Email != null && installeremail.Email != "" && installeremail.IsDisabled != true)
                                ccAddresses.Add(installeremail.Email);
                        }
                    }
                }

                if (model.AccountId != null && model.AccountId > 0)
                {
                    tocustomerAddress = emailMgr.GetAccountEmail((int)model.AccountId);
                }
                else if (model.LeadId != null && model.LeadId > 0)
                {
                    tocustomerAddress = emailMgr.GetLeadEmail((int)model.LeadId);
                }

                if (tocustomerAddress != null)
                {
                    if (tocustomerAddress.PrimaryEmail != null && tocustomerAddress.PrimaryEmail.Trim() != "")
                    {
                        toAddresses.Add(tocustomerAddress.PrimaryEmail.Trim());
                    }
                    if (tocustomerAddress.SecondaryEmail != null && tocustomerAddress.SecondaryEmail.Trim() != "")
                    {
                        toAddresses.Add(tocustomerAddress.SecondaryEmail.Trim());
                    }
                }

                //If no from or to emails return
                if (toAddresses == null || toAddresses.Count == 0 || fromEmailAddress == "")
                {
                    return "Success";
                    //return "No valid from or to email";
                }

                //Preparing email Body with formating
                string TemplateSubject = "", bodyHtml = "";
                int FranchiseId = 0;

                string salesperson = "";

                foreach (var personemail in model.Attendees)
                {
                    var person = CacheManager.UserCollection.FirstOrDefault(f => string.Equals(f.Email, personemail.PersonEmail, StringComparison.InvariantCultureIgnoreCase) && f.FranchiseId == SessionManager.CurrentFranchise.FranchiseId);
                    if (person != null)
                    {
                        var user = LocalMembership.GetUser(person.PersonId, "Person", "Roles", "OAuthUsers", "UserWidgets");
                        var res = user.IsInRole(AppConfigManager.DefaultSalesRole.RoleId, AppConfigManager.DefaultSalesNoMargineRole.RoleId);
                        if (user.Person != null)
                        {
                            salesperson = user.Person.FullName;
                        }
                        if (res == true) break;
                    }
                }

                int BrandId = (int)SessionManager.BrandId;

                //Take Body Html from EmailTemplate Table
                var emailtemplate = emailMgr.GetHtmlBody(emailType, BrandId);
                TemplateSubject = emailtemplate.TemplateSubject;
                bodyHtml = emailtemplate.TemplateLayout;
                short TemplateEmailId = (short)emailtemplate.EmailTemplateId;

                var Intallationdate = model.StartDate;


                if (emailType == EmailType.AppointmentConfirmation)  //== 2)
                {
                    var mailMsg = emailMgr.BuildMailMessageAppointment(model,
                    salesperson, bodyHtml, TemplateSubject, TemplateEmailId,
                    fromEmailAddress, toAddresses, ccAddresses, bccAddresses);
                }
                if (emailType == EmailType.Installation) //== 5)
                {
                    var mailMsg = emailMgr.BuildMailMessageInstallSchedule(model,
                        bodyHtml, TemplateSubject, TemplateEmailId,
                        fromEmailAddress, toAddresses, ccAddresses, bccAddresses);
                }

                return "Success";
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return "Failure";
            }
        }

        #endregion ----- Tochpoint/SendEmail -----

        private Calendar MapFrom(CalendarVM model)
        {
            return new Calendar()
            {
                CalendarId = model.CalendarId,
                EventPersonId = model.EventPersonId,
                PersonId = model.PersonId,
                Subject = model.Subject,
                Message = model.Message,
                AddRecurrence = model.AddRecurrence,
                LeadId = model.LeadId,
                AccountId = model.AccountId,
                OpportunityId = model.OpportunityId,
                OrderId = model.OrderId,
                CreatedByPersonId = model.CreatedByPersonId,
                CreatedOnUtc = model.CreatedOnUtc,
                IsAllDay = model.IsAllDay,
                LastUpdatedUtc = model.LastUpdatedUtc,
                IsDeleted = model.IsDeleted,
                LastUpdatedByPersonId = model.LastUpdatedByPersonId,
                LeadNumber = model.LeadNumber,
                CalendarGuid = model.CalendarGuid,
                FranchiseId = model.FranchiseId,
                RecurringEventId = model.RecurringEventId,
                IsPrivate = model.IsPrivate,
                StartDate = TimeZoneManager.ToUTC(model.StartDate),
                EndDate = TimeZoneManager.ToUTC(model.EndDate),
                AptTypeEnum = model.AptTypeEnum,
                Location = model.Location,
                OrganizerPersonId = model.OrganizerPersonId,
                OrganizerEmail = model.OrganizerEmail,
                IsCancelled = model.IsCancelled,
                ReminderMinute = model.ReminderMinute,
                RemindMethodEnum = model.RemindMethodEnum,
                OrganizerName = model.OrganizerName,
                FirstRemindDate = model.FirstRemindDate,
                EventTypeEnum = model.EventTypeEnum,
                RevisionSequence = model.RevisionSequence,
                JobId = model.JobId,
                JobNumber = model.JobNumber,
                AssignedPersonId = model.AssignedPersonId,
                AssignedName = model.AssignedName,
                PhoneNumber = model.PhoneNumber,
                CreatedByPerson = model.CreatedByPerson,
                Franchise = model.Franchise,
                Attendees = model.Attendees,
                EventRecurring = model.EventRecurring,
                Organizer = model.Organizer,
                CalendarSyncs = model.CalendarSyncs,
                ModifiedOccurrences = model.ModifiedOccurrences,
                Job = model.Job,
                Lead = model.Lead,
                Assigned = model.Assigned,
                EventToPeople_History = model.EventToPeople_History,
                RRRule = model.RRRule,
                AttendeesName = model.AttendeesName,
                CaseId = model.CaseId,
                VendorCaseId = model.VendorCaseId
            };
        }

        private CalendarVM MapTo(Calendar model)
        {
            return new CalendarVM()
            {
                CalendarId = model.CalendarId,
                EventPersonId = model.EventPersonId,
                PersonId = model.PersonId,
                Subject = model.Subject,
                Message = model.Message,
                AddRecurrence = model.AddRecurrence,
                LeadId = model.LeadId,
                AccountId = model.AccountId,
                OpportunityId = model.OpportunityId,
                OrderId = model.OrderId,
                CreatedByPersonId = model.CreatedByPersonId,
                CreatedOnUtc = model.CreatedOnUtc,
                IsAllDay = model.IsAllDay,
                LastUpdatedUtc = model.LastUpdatedUtc,
                IsDeleted = model.IsDeleted,
                LastUpdatedByPersonId = model.LastUpdatedByPersonId,
                LeadNumber = model.LeadNumber,
                CalendarGuid = model.CalendarGuid,
                FranchiseId = model.FranchiseId,
                RecurringEventId = model.RecurringEventId,
                IsPrivate = model.IsPrivate,
                //StartDate = TimeZoneManager.ToLocal(model.StartDate.DateTime),
                //EndDate = TimeZoneManager.ToLocal(model.EndDate.DateTime),
                StartDate = DateTime.SpecifyKind(TimeZoneManager.ToLocal(model.StartDate.DateTime), DateTimeKind.Utc),
                EndDate = DateTime.SpecifyKind(TimeZoneManager.ToLocal(model.EndDate.DateTime), DateTimeKind.Utc),
                AptTypeEnum = model.AptTypeEnum,
                Location = model.Location,
                OrganizerPersonId = model.OrganizerPersonId,
                OrganizerEmail = model.OrganizerEmail,
                IsCancelled = model.IsCancelled,
                ReminderMinute = model.ReminderMinute,
                RemindMethodEnum = model.RemindMethodEnum,
                OrganizerName = model.OrganizerName,
                FirstRemindDate = model.FirstRemindDate,
                EventTypeEnum = model.EventTypeEnum,
                RevisionSequence = model.RevisionSequence,
                JobId = model.JobId,
                JobNumber = model.JobNumber,
                AssignedPersonId = model.AssignedPersonId,
                AssignedName = model.AssignedName,
                PhoneNumber = model.PhoneNumber,
                CreatedByPerson = model.CreatedByPerson,
                Franchise = model.Franchise,
                Attendees = model.Attendees,
                EventRecurring = model.EventRecurring,
                Organizer = model.Organizer,
                CalendarSyncs = model.CalendarSyncs,
                ModifiedOccurrences = model.ModifiedOccurrences,
                Job = model.Job,
                Lead = model.Lead,
                Assigned = model.Assigned,
                EventToPeople_History = model.EventToPeople_History,
                RRRule = model.RRRule,
                AttendeesName = model.AttendeesName,
                CaseId = model.CaseId,
                VendorCaseId = model.VendorCaseId,
                IsPrivateAccess = model.IsPrivateAccess == null ? true : model.IsPrivateAccess,
                AccountName = model.AccountName,
                AccountPhone = model.AccountPhone,
                OpportunityName = model.OpportunityName,
                RecurrenceException = model.RecurrenceException
            };
        }

        private CalendarTasksVM MapToTasksModel(CalendarTasks model)
        {
            return new CalendarTasksVM()
            {
                TaskId = model.TaskId,
                TaskGuid = model.TaskGuid,
                Subject = model.Subject,
                Message = model.Message,
                CreatedOnUtc = model.CreatedOnUtc,
                DueDate = ((DateTimeOffset)model.DueDate).Date,
                CompletedDate = model.CompletedDate != null ? TimeZoneManager.ToLocal(((DateTimeOffset)model.CompletedDate).DateTime) : DateTime.MinValue,
                LeadId = model.LeadId,
                OrganizerPersonId = model.OrganizerPersonId,
                OrganizerEmail = model.OrganizerEmail,
                CreatedByPersonId = model.CreatedByPersonId,
                IsDeleted = model.IsDeleted,
                LastUpdatedOnUtc = model.LastUpdatedOnUtc,
                LastUpdatedPersonId = model.LastUpdatedPersonId,
                PriorityOrder = model.PriorityOrder,
                LeadNumber = model.LeadNumber,
                FranchiseId = model.FranchiseId,
                IsPrivate = model.IsPrivate,
                ReminderMinute = model.ReminderMinute,
                RemindMethodEnum = model.RemindMethodEnum,
                //FirstRemindDate = TimeZoneManager.ToLocal(Convert.ToDateTime( model.FirstRemindDate)),
                RevisionSequence = model.RevisionSequence,
                AssignedPersonId = model.AssignedPersonId,
                AssignedName = model.AssignedName,
                PhoneNumber = model.PhoneNumber,
                AccountId = model.AccountId,
                OpportunityId = model.OpportunityId,
                OrderId = model.OrderId,
                AttendeesName = model.AttendeesName,
                IsPrivateAccess = model.IsPrivateAccess == null ? true : model.IsPrivateAccess,
            };
        }

        #region TouchPoint Connect

        // Get List of Events by person id
        public List<EventListDataTPConnectModel> GetEventListDataTPConnect(int PersonId, DateTime sdate, DateTime edate)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                string query = @"select distinct c.CalendarId,c.StartDate,c.EndDate,
                                case when c.AccountId is not null and c.AccountId>0 then c.AccountId else c.LeadId end as AccountId,
                                case when c.AccountId is not null and c.AccountId>0 then
                                (select cus.FirstName+' '+cus.LastName + case when cus.CompanyName is not null and cus.CompanyName!='' then ' / '+ cus.CompanyName else '' end  from CRM.Customer cus
                                inner join CRM.AccountCustomers acus on cus.CustomerId=acus.CustomerId and acus.IsPrimaryCustomer=1
                                where AccountId=c.AccountId)
                                else (select cus.FirstName+' '+cus.LastName + case when cus.CompanyName is not null and cus.CompanyName!='' then ' / '+ cus.CompanyName else '' end  from CRM.Customer cus
                                inner join CRM.LeadCustomers lcus on cus.CustomerId=lcus.CustomerId and lcus.IsPrimaryCustomer=1
                                where LeadId=c.LeadId) end as AccountName,
                                c.Location, c.Subject,c.AptTypeEnum,c.IsAllDay from [CRM].[Calendar] c
                                inner join CRM.EventToPeople etp on c.CalendarId=etp.CalendarId
                                where (etp.PersonId=@PersonId or c.CreatedByPersonId=@PersonId) and c.IsDeleted=0 and ISNULL(c.IsCancelled,0)=0
                                and c.EventTypeEnum != 2
                                and (c.StartDate between @sdate and @edate 
							    or c.EndDate between @sdate and @edate
							    or (c.StartDate<=@sdate and c.EndDate>=@edate))";

                var result = connection.Query<EventListDataTPConnectModel>(query, new { PersonId, sdate, edate }).ToList();
                return result;
            }
        }

        public List<EventListDataTPConnectModel> GetRecurringEventsTPConnect(int PersonId, DateTime recdate, TimeZoneEnum feTimeZone)
        {
            #region query
            string query = @"select c.* from CRM.Calendar c
                            join CRM.EventRecurring er on c.RecurringEventId=er.RecurringEventId
			                join CRM.EventToPeople ep on c.CalendarId=ep.CalendarId
                            where (ep.PersonId=@personId or c.CreatedByPersonId=@personId)
                            and c.EventTypeEnum=2
                            and ISNULL(c.IsDeleted,0) != 1
                            and ISNULL(c.IsCancelled,0) != 1
                            
                            select er.* from CRM.Calendar c
                            join CRM.EventRecurring er on c.RecurringEventId=er.RecurringEventId
			                join CRM.EventToPeople ep on c.CalendarId=ep.CalendarId
                            where (ep.PersonId=@personId or c.CreatedByPersonId=@personId)
                            and c.EventTypeEnum=2
                            and ISNULL(c.IsDeleted,0) != 1
                            and ISNULL(c.IsCancelled,0) != 1

			                select cso.* from CRM.Calendar c
			                join CRM.CalendarSingleOccurrences cso on c.RecurringEventId=cso.RecurringEventId
			                join CRM.EventToPeople ep on c.CalendarId=ep.CalendarId
			                where (ep.PersonId=@personId or c.CreatedByPersonId=@personId)
			                and c.EventTypeEnum=2
                            and ISNULL(c.IsDeleted,0) != 1
                            and ISNULL(c.IsCancelled,0) != 1";

            string eventquery = @"select distinct c.CalendarId,c.StartDate,c.EndDate,
                                case when c.AccountId is not null and c.AccountId>0 then c.AccountId else c.LeadId end as AccountId,
                                case when c.AccountId is not null and c.AccountId>0 then
                                (select cus.FirstName+' '+cus.LastName + case when cus.CompanyName is not null and cus.CompanyName!='' then ' / '+ cus.CompanyName else '' end  from CRM.Customer cus
                                inner join CRM.AccountCustomers acus on cus.CustomerId=acus.CustomerId and acus.IsPrimaryCustomer=1
                                where AccountId=c.AccountId)
                                else (select cus.FirstName+' '+cus.LastName + case when cus.CompanyName is not null and cus.CompanyName!='' then ' / '+ cus.CompanyName else '' end  from CRM.Customer cus
                                inner join CRM.LeadCustomers lcus on cus.CustomerId=lcus.CustomerId and lcus.IsPrimaryCustomer=1
                                where LeadId=c.LeadId) end as AccountName,
                                c.Location, c.Subject,c.AptTypeEnum,c.IsAllDay from [CRM].[Calendar] c
                                inner join CRM.EventToPeople etp on c.CalendarId=etp.CalendarId
                                where c.CalendarId in @CalendarId";
            #endregion

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                List<int> calId = new List<int>();
                var recDetails = connection.QueryMultiple(query, new
                {
                    personId = PersonId
                });

                var caldata = recDetails.Read<Calendar>().ToList();
                var calrecdata = recDetails.Read<EventRecurring>().ToList();
                var caloccdata = recDetails.Read<CalendarSingleOccurrence>().ToList();

                foreach (var c in caldata)
                {
                    c.EventRecurring = calrecdata.Where(x => x.RecurringEventId == c.RecurringEventId).FirstOrDefault();
                    DateTime currdate = recdate.Date.Add(c.EventRecurring.StartDate.TimeOfDay);
                    var occ = c.EventRecurring.checkOccurrencebyDate(currdate);
                    if (occ != null)
                    {
                        var occdelete = caloccdata.Where(x => x.RecurringEventId == c.RecurringEventId).ToList();
                        if (occdelete != null && occdelete.Count > 0)
                        {
                            var del = occdelete.Where(x => TimeZoneManager.ToLocal(x.OriginalStartDate) == occ.StartDate).FirstOrDefault();
                            if (del == null)
                                calId.Add(c.CalendarId);
                        }
                        else
                            calId.Add(c.CalendarId);
                    }
                }

                if (calId.Count > 0)
                {
                    var eventdata = connection.Query<EventListDataTPConnectModel>(eventquery, new { CalendarId = calId }).ToList();
                    foreach (var ed in eventdata)
                    {
                        ed.StartDate = recdate.Date.Add(ed.StartDate.TimeOfDay);
                        ed.EndDate = recdate.Date.Add(ed.EndDate.TimeOfDay);

                        if (TimeZoneManager.ToLocal(ed.StartDate, feTimeZone).Date != recdate.Date)
                            ed.StartDate = ed.StartDate.AddDays(1);
                        if (TimeZoneManager.ToLocal(ed.EndDate, feTimeZone).Date != recdate.Date)
                            ed.EndDate = ed.EndDate.AddDays(1);
                    }
                    return eventdata;
                }
                return new List<EventListDataTPConnectModel>();
            }
        }

        //Get Event Details by CalendarId
        public EventDetailsTPConnectModel GetViewEventDataTPConnect(int CalendarId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                string query = @"select c.CalendarId, c.Subject,c.Location,c.StartDate,c.EndDate,
                                 case when c.LeadId is null then '' else
                                 (select cus.FirstName+' '+cus.LastName + case when cus.CompanyName is not null and cus.CompanyName!='' then ' / '+ cus.CompanyName else '' end  from CRM.Customer cus
                                 inner join CRM.LeadCustomers lcus on cus.CustomerId=lcus.CustomerId and lcus.IsPrimaryCustomer=1
                                 where LeadId=c.LeadId) end as Lead,
                                 case when c.AccountId is null then '' else
                                 (select cus.FirstName+' '+cus.LastName + case when cus.CompanyName is not null and cus.CompanyName!='' then ' / '+ cus.CompanyName else '' end  from CRM.Customer cus
                                 inner join CRM.AccountCustomers acus on cus.CustomerId=acus.CustomerId and acus.IsPrimaryCustomer=1
                                 where AccountId=c.AccountId) end as Account,
                                 case when c.OpportunityId is null then '' else
                                 (select OpportunityName from CRM.Opportunities where OpportunityId=c.OpportunityId) end as Opportunity,
                                 case when c.OrderId is null then '' else
                                 (select OrderName from CRM.Orders where OrderID=c.OrderId) end as [Order],
                                 LeadId,AccountId,OpportunityId,OrderId,
                                 (SELECT Stuff((SELECT ', ' + Cast(PersonId as varchar(100)) FROM CRM.EventToPeople where CalendarId=c.CalendarId FOR XML PATH('')), 1, 2, '')) AS AttendeesId,
                                 c.AptTypeEnum,c.Message,c.ReminderMinute,
                                 case when c.RecurringEventId is null then 0 else 1 end as Recurring,
                                 isnull(c.IsPrivate,0) as IsPrivate,c.RecurringEventId,
                                 (SELECT Stuff((SELECT ', ' + PersonName FROM CRM.EventToPeople where CalendarId=c.CalendarId FOR XML PATH('')), 1, 2, '')) AS Attendees,
                                 c.CreatedOnUtc,c.LastUpdatedUtc,pc.FirstName+' '+pc.LastName as CreatedBy,pm.FirstName+' '+pm.LastName as LastUpdatedBy, c.IsAllDay
                                 from [CRM].[Calendar] c
                                 left join CRM.Person pc on c.CreatedByPersonId=pc.PersonId
                                 left join CRM.Person pm on c.LastUpdatedByPersonId=pm.PersonId
                                 where c.CalendarId=@CalendarId and c.FranchiseId=@FranchiseId";

                var result = connection.Query<EventDetailsTPConnectModel>(query, new { CalendarId, FranchiseId = User.FranchiseId }).FirstOrDefault();
                return result;
            }
        }

        #region Event LookUp

        //Get the Event Lead drop down info
        public dynamic GetEventLeadLookUp(int FranchiseId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                dynamic ell = new ExpandoObject();
                string query = @"select l.LeadId as Id, cus.FirstName+' '+cus.LastName as [Name]  from CRM.Customer cus
                                 inner join CRM.LeadCustomers lcus on cus.CustomerId=lcus.CustomerId and lcus.IsPrimaryCustomer=1
                                 inner join CRM.Leads l on lcus.LeadId=l.LeadId
                                 where l.FranchiseId=@FranchiseId and l.LeadStatusId!=31422";

                ell.Lead = connection.Query(query, new { FranchiseId }).ToList();
                return ell;
            }
        }

        //Get the Event Lead drop down info
        public dynamic GetSubjectLocation(int Id, string type)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                dynamic ell = new ExpandoObject();

                if (type == "lead")
                {
                    string query = @"select * from crm.Customer where customerid in (
                            select customerid from crm.LeadCustomers where LeadId = @LeadId and IsPrimaryCustomer = 1)";
                    var customer = connection.Query<CustomerTP>(query, new { LeadId = Id }).FirstOrDefault();
                    ell.PrimCustomer = customer;

                    query = @"select * from crm.Addresses where AddressId in (
                            select AddressId from crm.LeadAddresses where LeadId = @LeadId)";
                    var addresses = connection.Query<AddressTP>(query, new { LeadId = Id }).ToList();
                    ell.Addresses = addresses;
                }
                else if (type == "account")
                {

                    string query = "select * from crm.Customer where customerid in (select customerid from crm.AccountCustomers where AccountId = @AccountId and IsPrimaryCustomer = 1)";
                    var customer = connection.Query<CustomerTP>(query, new { AccountId = Id }).FirstOrDefault();
                    ell.PrimCustomer = customer;

                    query = "select * from crm.Addresses where AddressId in (select AddressId from crm.AccountAddresses where AccountId = @AccountId)";
                    var addresses = connection.Query<AddressTP>(query, new { AccountId = Id }).ToList();
                    ell.Addresses = addresses;

                }
                else if (type == "opportunity")
                {
                    var query = @" select op.*, l.IsCommercial,te.Name as DisplayTerritoryName from  CRM.Opportunities op
                             left join CRM.Accounts l  on l.AccountId = op.AccountId
                             left join CRM.Territories te on op.TerritoryDisplayId = te.TerritoryId  where op.OpportunityId = @OpportunityId";
                    var opportunity = connection.Query<Opportunity>(query, new { OpportunityId = Id }).FirstOrDefault();

                    query = "select * from crm.Customer where customerid in (select customerid from crm.AccountCustomers where AccountId = @AccountId and IsPrimaryCustomer = 1)";
                    var customer = connection.Query<CustomerTP>(query, new { AccountId = opportunity.AccountId }).FirstOrDefault();
                    ell.PrimCustomer = customer;

                    query = "select * from crm.Addresses where AddressId = @AddressId";
                    var addresses = connection.Query<AddressTP>(query, new { AddressId = opportunity.InstallationAddressId }).ToList();
                    ell.Addresses = addresses;
                }
                else if (type == "order")
                {
                    var query = @" select op.*, l.IsCommercial,te.Name as DisplayTerritoryName from [CRM].[Orders] o
                            join [CRM].[Opportunities] op on o.OpportunityId=op.OpportunityId
                            left join CRM.Accounts l  on l.AccountId = op.AccountId
                            left join CRM.Territories te on op.TerritoryDisplayId = te.TerritoryId  where o.OrderId = @OrderId";
                    var opportunity = connection.Query<Opportunity>(query, new { OrderId = Id }).FirstOrDefault();

                    query = "select * from crm.Customer where customerid in (select customerid from crm.AccountCustomers where AccountId = @AccountId and IsPrimaryCustomer = 1)";
                    var customer = connection.Query<CustomerTP>(query, new { AccountId = opportunity.AccountId }).FirstOrDefault();
                    ell.PrimCustomer = customer;

                    query = "select * from crm.Addresses where AddressId = @AddressId";
                    var addresses = connection.Query<AddressTP>(query, new { AddressId = opportunity.InstallationAddressId }).ToList();
                    ell.Addresses = addresses;

                }
                string Subject = "";
                string Location = "";

                if (ell.PrimCustomer != null)
                {
                    Subject = ell.PrimCustomer.FullName;
                    if (!string.IsNullOrEmpty(ell.PrimCustomer.CompanyName))
                    {
                        Subject = Subject == "" ? ell.PrimCustomer.CompanyName : Subject + " / " + ell.PrimCustomer.CompanyName;
                    }
                }

                if (ell.Addresses != null && ell.Addresses.Count > 0)
                {
                    if (!string.IsNullOrEmpty(ell.Addresses[0].Address1))
                        Location = ell.Addresses[0].Address1;
                    if (!string.IsNullOrEmpty(ell.Addresses[0].Address2))
                    {
                        Location = Location == "" ? ell.Addresses[0].Address2 : Location + ", " + ell.Addresses[0].Address2;
                    }
                    if (!string.IsNullOrEmpty(ell.Addresses[0].City))
                    {
                        Location = Location == "" ? ell.Addresses[0].City : Location + ", " + ell.Addresses[0].City;
                    }
                    if (!string.IsNullOrEmpty(ell.Addresses[0].State))
                    {
                        Location = Location == "" ? ell.Addresses[0].State : Location + ", " + ell.Addresses[0].State;
                    }
                    if (!string.IsNullOrEmpty(ell.Addresses[0].ZipCode))
                    {
                        Location = Location == "" ? ell.Addresses[0].ZipCode : Location + " " + ell.Addresses[0].ZipCode;
                    }
                }

                return new { Subject = Subject, Location = Location };
            }
        }

        //Get the Event Account, Opportunity, Order drop down info
        public dynamic GetEventLookUp(int FranchiseId, LookUpFilter filter)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                dynamic el = new ExpandoObject();

                // Get all the info
                if (filter.AccountId == 0 && filter.OpportunityId == 0 && filter.OrderId == 0)
                {
                    string queryacc = @"select acc.AccountId, cus.FirstName+' '+cus.LastName as [Name] from CRM.Customer cus
                                        inner join CRM.AccountCustomers acus on cus.CustomerId=acus.CustomerId and acus.IsPrimaryCustomer=1
                                        inner join CRM.Accounts acc on acus.AccountId=acc.AccountId
                                        where acc.FranchiseId=@FranchiseId";

                    string queryopp = @"select AccountId,OpportunityId, OpportunityName as [Name] from CRM.Opportunities where FranchiseId=@FranchiseId";

                    string queryorder = @"select opp.AccountId,o.OpportunityId,OrderID, OrderName as [Name] from CRM.Orders o
                                          inner join CRM.Opportunities opp on o.OpportunityId=opp.OpportunityId
                                          where FranchiseId=@FranchiseId";

                    el.Account = connection.Query(queryacc, new { FranchiseId }).ToList();
                    el.Opportunity = connection.Query(queryopp, new { FranchiseId }).ToList();
                    el.Order = connection.Query(queryorder, new { FranchiseId }).ToList();
                    return el;
                }

                // return account related info
                if (filter.AccountId != 0)
                {
                    string queryacc = @"select acc.AccountId, cus.FirstName+' '+cus.LastName as [Name] from CRM.Customer cus
                                        inner join CRM.AccountCustomers acus on cus.CustomerId=acus.CustomerId and acus.IsPrimaryCustomer=1
                                        inner join CRM.Accounts acc on acus.AccountId=acc.AccountId
                                        where acc.FranchiseId=@FranchiseId and acc.AccountId=@AccountId";

                    string queryopp = @"select acc.AccountId,OpportunityId, OpportunityName as [Name] from CRM.Opportunities opp
                                        inner join CRM.Accounts acc on opp.AccountId=acc.AccountId
                                        where acc.FranchiseId=@FranchiseId and acc.AccountId=@AccountId";

                    string queryorder = @"select acc.AccountId,o.OpportunityId,OrderID, OrderName as [Name] from CRM.Orders o
                                          inner join CRM.Opportunities opp on o.OpportunityId=opp.OpportunityId
                                          inner join CRM.Accounts acc on opp.AccountId=acc.AccountId
                                          where acc.FranchiseId=@FranchiseId and acc.AccountId=@AccountId";

                    el.Account = connection.Query(queryacc, new { FranchiseId, AccountId = filter.AccountId }).ToList();
                    el.Opportunity = connection.Query(queryopp, new { FranchiseId, AccountId = filter.AccountId }).ToList();
                    el.Order = connection.Query(queryorder, new { FranchiseId, AccountId = filter.AccountId }).ToList();
                    return el;
                }

                // get opportunity related info
                if (filter.OpportunityId != 0)
                {
                    string queryacc = @"select acc.AccountId, cus.FirstName+' '+cus.LastName as [Name] from CRM.Customer cus
                                        inner join CRM.AccountCustomers acus on cus.CustomerId=acus.CustomerId and acus.IsPrimaryCustomer=1
                                        inner join CRM.Accounts acc on acus.AccountId=acc.AccountId
                                        inner join CRM.Opportunities opp on acc.AccountId=opp.AccountId
                                        where opp.FranchiseId=@FranchiseId and opp.OpportunityId=@OpportunityId";

                    string queryopp = @"select opp.AccountId,OpportunityId, OpportunityName as [Name] from CRM.Opportunities opp
                                        where opp.FranchiseId=@FranchiseId and opp.OpportunityId=@OpportunityId";

                    string queryorder = @"select opp.AccountId,o.OpportunityId,OrderID, OrderName as [Name] from CRM.Orders o
                                          inner join CRM.Opportunities opp on o.OpportunityId=opp.OpportunityId
                                          where opp.FranchiseId=@FranchiseId and opp.OpportunityId=@OpportunityId";

                    el.Account = connection.Query(queryacc, new { FranchiseId, OpportunityId = filter.OpportunityId }).ToList();
                    el.Opportunity = connection.Query(queryopp, new { FranchiseId, OpportunityId = filter.OpportunityId }).ToList();
                    el.Order = connection.Query(queryorder, new { FranchiseId, OpportunityId = filter.OpportunityId }).ToList();
                    return el;
                }

                // Get order related info
                if (filter.OrderId != 0)
                {
                    string queryacc = @"select acc.AccountId, cus.FirstName+' '+cus.LastName as [Name] from CRM.Customer cus
                                        inner join CRM.AccountCustomers acus on cus.CustomerId=acus.CustomerId and acus.IsPrimaryCustomer=1
                                        inner join CRM.Accounts acc on acus.AccountId=acc.AccountId
                                        inner join CRM.Opportunities opp on acc.AccountId=opp.AccountId
                                        inner join CRM.Orders o on opp.OpportunityId=o.OpportunityId
                                        where opp.FranchiseId=@FranchiseId and o.OrderID=@OrderId";

                    string queryopp = @"select opp.AccountId,opp.OpportunityId, OpportunityName as [Name] from CRM.Opportunities opp
                                        inner join CRM.Orders o on opp.OpportunityId=o.OpportunityId
                                        where opp.FranchiseId=@FranchiseId and o.OrderID=@OrderId";

                    string queryorder = @"select opp.AccountId,o.OpportunityId,OrderID, OrderName as [Name] from CRM.Orders o
                                          inner join CRM.Opportunities opp on o.OpportunityId=opp.OpportunityId
                                          where opp.FranchiseId=@FranchiseId and o.OrderID=@OrderId";

                    el.Account = connection.Query(queryacc, new { FranchiseId, OrderId = filter.OrderId }).ToList();
                    el.Opportunity = connection.Query(queryopp, new { FranchiseId, OrderId = filter.OrderId }).ToList();
                    el.Order = connection.Query(queryorder, new { FranchiseId, OrderId = filter.OrderId }).ToList();
                    return el;
                }
                return null;
            }
        }

        public dynamic GetEventLookUp(int FranchiseId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                dynamic el = new ExpandoObject();

                string queryleads = @"select l.LeadId as Id, cus.FirstName+' '+cus.LastName as [Name]  from CRM.Customer cus
                                 inner join CRM.LeadCustomers lcus on cus.CustomerId=lcus.CustomerId and lcus.IsPrimaryCustomer=1
                                 inner join CRM.Leads l on lcus.LeadId=l.LeadId
                                 where l.FranchiseId=@FranchiseId and l.LeadStatusId!=31422";

                string queryacc = @"select acc.AccountId, cus.FirstName+' '+cus.LastName as [Name] from CRM.Customer cus
                                        inner join CRM.AccountCustomers acus on cus.CustomerId=acus.CustomerId and acus.IsPrimaryCustomer=1
                                        inner join CRM.Accounts acc on acus.AccountId=acc.AccountId
                                        where acc.FranchiseId=@FranchiseId";

                string queryopp = @"select AccountId,OpportunityId, OpportunityName as [Name] from CRM.Opportunities where FranchiseId=@FranchiseId";

                string queryorder = @"select opp.AccountId,o.OpportunityId,OrderID, OrderName as [Name] from CRM.Orders o
                                          inner join CRM.Opportunities opp on o.OpportunityId=opp.OpportunityId
                                          where FranchiseId=@FranchiseId";

                string queryAttendees = @"select distinct per.personid as Id,FirstName + ' ' + LastName  as [Name]
                                 from [Auth].[Users] usr
                                 join [CRM].[Person] per on per.personid = usr.personid
                                 join [Auth].[UsersInRoles] uir on usr.userid = uir.userid
                                 where usr.franchiseid = @FranchiseId and usr.IsDeleted!=1 and (usr.IsDisabled=0 or usr.IsDisabled is null)
                                 order by [Name]";
                string querytypes = @"select AppointmentTypeId as Id,[Name] from CRM.Type_Appointments";

                el.leads = connection.Query(queryleads, new { FranchiseId }).ToList();
                el.accounts = connection.Query(queryacc, new { FranchiseId }).ToList();
                el.opportunities = connection.Query(queryopp, new { FranchiseId }).ToList();
                el.orders = connection.Query(queryorder, new { FranchiseId }).ToList();
                el.types = connection.Query(querytypes).ToList();
                el.attendees = connection.Query(queryAttendees, new { FranchiseId }).ToList();
                return el;
            }
        }

        // Get the Event Attendees drop down info
        public dynamic GetEventAttendeesLookUp(int FranchiseId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                dynamic eal = new ExpandoObject();
                string query = @"select distinct per.personid as AttendeesId,FirstName + ' ' + LastName  as [Name]
                                 from [Auth].[Users] usr
                                 join [CRM].[Person] per on per.personid = usr.personid
                                 join [Auth].[UsersInRoles] uir on usr.userid = uir.userid
                                 where usr.franchiseid = @FranchiseId and usr.Domain is not null and usr.addressid is not null and ISNULL(usr.IsDeleted,0)=0 and ISNULL(usr.IsDisabled,0)=0 
                                 order by [Name]";

                eal.Attendees = connection.Query(query, new { FranchiseId }).ToList();
                return eal;
            }
        }

        // Get the Event Appointment type drop down info
        public dynamic GetEventAppointmentTypeLookUp()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                dynamic eal = new ExpandoObject();
                string query = @"select AppointmentTypeId,[Name] from CRM.Type_Appointments";

                eal.AppointmentType = connection.Query(query).ToList();
                return eal;
            }
        }

        // Get Person info
        public dynamic Getpersoninfo(int PersonId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                string query = @"select PersonId,FirstName,LastName,PrimaryEmail from CRM.Person where PersonId=@PersonId";
                return connection.Query(query, new { PersonId }).FirstOrDefault();
            }
        }

        // Get the Event Lead drop down info
        public dynamic GetEventLeadLookUp(int FranchiseId, string key)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                string query = @"select l.LeadId, cus.FirstName+' '+cus.LastName as [Name], cus.PrimaryEmail as Email  from CRM.Customer cus
                                inner join CRM.LeadCustomers lcus on cus.CustomerId=lcus.CustomerId and lcus.IsPrimaryCustomer=1
                                inner join CRM.Leads l on lcus.LeadId=l.LeadId
                                where l.FranchiseId=@FranchiseId and l.LeadStatusId<>31422 and (cus.FirstName +' '+ cus.LastName like @Key) order by l.CreatedOnUtc desc";

                var res = connection.Query(query, new { FranchiseId, Key = key + "%" }).ToList();
                return res;
            }
        }

        // Get the Event Account drop down info
        public dynamic GetEventAccountLookUp(int FranchiseId, string key)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                string query = @"select acc.AccountId, cus.FirstName+' '+cus.LastName as [Name],
                                    LTRIM(STUFF(
                                        COALESCE(', ' + NULLIF(ad.Address1, ''), '')  +
                                        COALESCE(', ' + NULLIF(ad.Address2, ''), '')  +
                                        COALESCE(', ' + NULLIF(ad.City, ''), '') +
                                        COALESCE(', ' + NULLIF(ad.[State], ''), '') +
                                        COALESCE(' ' + NULLIF(ad.ZipCode, ''), '') +
                                        COALESCE(', ' + NULLIF(ad.CountryCode2Digits , ''), ''),
                                        1, 1, '')) as Address
                                    from CRM.Customer cus
                                    inner join CRM.AccountCustomers acus on cus.CustomerId=acus.CustomerId and acus.IsPrimaryCustomer=1
                                    inner join CRM.Accounts acc on acus.AccountId=acc.AccountId
			                        inner join CRM.AccountAddresses aad on acc.AccountId =aad.AccountId and aad.IsPrimaryAddress=1
			                        inner join CRM.Addresses ad on ad.AddressId=aad.AddressId
                                    where acc.FranchiseId=@FranchiseId and (cus.FirstName +' '+ cus.LastName like @Key) order by acc.CreatedOnUtc desc";

                var res = connection.Query(query, new { FranchiseId, Key = key + "%" }).ToList();
                return res;
            }
        }

        // Get the Event Opportunities drop down info
        public dynamic GetEventOpportunitiesLookUp(int FranchiseId, string key)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                string query = @"select op.AccountId,op.OpportunityId, op.OpportunityName as [Name],
                                LTRIM(STUFF(
					                COALESCE(', ' + NULLIF(ad.Address1, ''), '')  +
					                COALESCE(', ' + NULLIF(ad.Address2, ''), '')  +
					                COALESCE(', ' + NULLIF(ad.City, ''), '') +
					                COALESCE(', ' + NULLIF(ad.[State], ''), '') +
					                COALESCE(' ' + NULLIF(ad.ZipCode, ''), '') +
					                COALESCE(', ' + NULLIF(ad.CountryCode2Digits , ''), ''),
					                1, 1, '')) as Address
                                from CRM.Opportunities op
						        inner join CRM.Addresses ad on ad.AddressId=op.InstallationAddressId
                                where FranchiseId=@FranchiseId and OpportunityName like @OpportunityName 
                                order by op.LastUpdatedOnUtc,op.CreatedOnUtc desc";

                var res = connection.Query(query, new { FranchiseId, OpportunityName = key + "%" }).ToList();
                return res;
            }
        }

        // Get the Event Order drop down info
        public dynamic GetEventOrderLookUp(int FranchiseId, string key)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                string query = @"select opp.AccountId,o.OpportunityId,OrderID, OrderName as [Name],
                                LTRIM(STUFF(
					                COALESCE(', ' + NULLIF(ad.Address1, ''), '')  +
					                COALESCE(', ' + NULLIF(ad.Address2, ''), '')  +
					                COALESCE(', ' + NULLIF(ad.City, ''), '') +
					                COALESCE(', ' + NULLIF(ad.[State], ''), '') +
					                COALESCE(' ' + NULLIF(ad.ZipCode, ''), '') +
					                COALESCE(', ' + NULLIF(ad.CountryCode2Digits , ''), ''),
					                1, 1, '')) as Address
                                from CRM.Orders o
                                inner join CRM.Opportunities opp on o.OpportunityId=opp.OpportunityId
                                inner join CRM.Addresses ad on ad.AddressId=opp.InstallationAddressId
                                where FranchiseId=@FranchiseId and OrderName like @OrderName 
                                order by o.LastUpdatedOn,o.CreatedOn desc";

                var res = connection.Query(query, new { FranchiseId, OrderName = key + "%" }).ToList();
                return res;
            }
        }

        // Get Event LookUp by AccountId
        public dynamic GetEventLookUpbyAccountId(int FranchiseId, int AccountId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                dynamic el = new ExpandoObject();
                string queryacc = @"select acc.AccountId, cus.FirstName+' '+cus.LastName as [Name] from CRM.Customer cus
                                        inner join CRM.AccountCustomers acus on cus.CustomerId=acus.CustomerId and acus.IsPrimaryCustomer=1
                                        inner join CRM.Accounts acc on acus.AccountId=acc.AccountId
                                        where acc.FranchiseId=@FranchiseId and acc.AccountId=@AccountId";

                string queryopp = @"select acc.AccountId,OpportunityId, OpportunityName as [Name] from CRM.Opportunities opp
                                        inner join CRM.Accounts acc on opp.AccountId=acc.AccountId
                                        where acc.FranchiseId=@FranchiseId and acc.AccountId=@AccountId";

                string queryorder = @"select acc.AccountId,o.OpportunityId,OrderID, OrderName as [Name] from CRM.Orders o
                                          inner join CRM.Opportunities opp on o.OpportunityId=opp.OpportunityId
                                          inner join CRM.Accounts acc on opp.AccountId=acc.AccountId
                                          where acc.FranchiseId=@FranchiseId and acc.AccountId=@AccountId";

                el.Account = connection.Query(queryacc, new { FranchiseId, AccountId = AccountId }).ToList();
                el.Opportunity = connection.Query(queryopp, new { FranchiseId, AccountId = AccountId }).ToList();
                el.Order = connection.Query(queryorder, new { FranchiseId, AccountId = AccountId }).ToList();
                return el;
            }
        }

        // Get Event LookUp by OpportunityId
        public dynamic GetEventLookUpbyOpportunityId(int FranchiseId, int OpportunityId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                dynamic el = new ExpandoObject();
                string queryacc = @"select acc.AccountId, cus.FirstName+' '+cus.LastName as [Name] from CRM.Customer cus
                                        inner join CRM.AccountCustomers acus on cus.CustomerId=acus.CustomerId and acus.IsPrimaryCustomer=1
                                        inner join CRM.Accounts acc on acus.AccountId=acc.AccountId
                                        inner join CRM.Opportunities opp on acc.AccountId=opp.AccountId
                                        where opp.FranchiseId=@FranchiseId and opp.OpportunityId=@OpportunityId";

                string queryopp = @"select opp.AccountId,OpportunityId, OpportunityName as [Name] from CRM.Opportunities opp
                                        where opp.FranchiseId=@FranchiseId and opp.OpportunityId=@OpportunityId";

                string queryorder = @"select opp.AccountId,o.OpportunityId,OrderID, OrderName as [Name] from CRM.Orders o
                                          inner join CRM.Opportunities opp on o.OpportunityId=opp.OpportunityId
                                          where opp.FranchiseId=@FranchiseId and opp.OpportunityId=@OpportunityId";

                el.Account = connection.Query(queryacc, new { FranchiseId, OpportunityId = OpportunityId }).ToList();
                el.Opportunity = connection.Query(queryopp, new { FranchiseId, OpportunityId = OpportunityId }).ToList();
                el.Order = connection.Query(queryorder, new { FranchiseId, OpportunityId = OpportunityId }).ToList();
                return el;
            }
        }

        // Get Event LookUp by OrderId
        public dynamic GetEventLookUpbyOrderId(int FranchiseId, int OrderId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                dynamic el = new ExpandoObject();
                string queryacc = @"select acc.AccountId, cus.FirstName+' '+cus.LastName as [Name] from CRM.Customer cus
                                        inner join CRM.AccountCustomers acus on cus.CustomerId=acus.CustomerId and acus.IsPrimaryCustomer=1
                                        inner join CRM.Accounts acc on acus.AccountId=acc.AccountId
                                        inner join CRM.Opportunities opp on acc.AccountId=opp.AccountId
                                        inner join CRM.Orders o on opp.OpportunityId=o.OpportunityId
                                        where opp.FranchiseId=@FranchiseId and o.OrderID=@OrderId";

                string queryopp = @"select opp.AccountId,opp.OpportunityId, OpportunityName as [Name] from CRM.Opportunities opp
                                        inner join CRM.Orders o on opp.OpportunityId=o.OpportunityId
                                        where opp.FranchiseId=@FranchiseId and o.OrderID=@OrderId";

                string queryorder = @"select opp.AccountId,o.OpportunityId,OrderID, OrderName as [Name] from CRM.Orders o
                                          inner join CRM.Opportunities opp on o.OpportunityId=opp.OpportunityId
                                          where opp.FranchiseId=@FranchiseId and o.OrderID=@OrderId";

                el.Account = connection.Query(queryacc, new { FranchiseId, OrderId = OrderId }).ToList();
                el.Opportunity = connection.Query(queryopp, new { FranchiseId, OrderId = OrderId }).ToList();
                el.Order = connection.Query(queryorder, new { FranchiseId, OrderId = OrderId }).ToList();
                return el;
            }
        }

        #endregion Event LookUp

        //Get Event Details for Update Event by CalendarId
        public UpdateEventTPConnectModel GetUpdateEventDataTPConnect(int CalendarId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                string query = @"select c.CalendarId, c.Subject,c.Location,
                                 LeadId,AccountId,OpportunityId,OrderId,
                                 c.Message,c.AptTypeEnum,
                                 c.StartDate,c.EndDate,
                                 c.ReminderMinute,
                                 c.RecurringEventId,
                                 case when c.RecurringEventId is null then 0 else 1 end as Recurring,
                                 (SELECT Stuff((SELECT ', ' + Cast(PersonId as varchar(100)) FROM CRM.EventToPeople where CalendarId=c.CalendarId FOR XML PATH('')), 1, 2, '')) AS Attendees,
                                 c.CreatedOnUtc,c.LastUpdatedUtc
                                 from [CRM].[Calendar] c
                                 where c.CalendarId=@CalendarId and c.FranchiseId=@FranchiseId";

                var result = connection.Query<UpdateEventTPConnectModel>(query, new { CalendarId, FranchiseId = User.FranchiseId }).FirstOrDefault();
                return result;
            }
        }

        public EventRecurring GetRecurrenceEventDataTPConnect(int RecurringEventId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                string query = @"SELECT [RecurringEventId]
                                 ,[StartDate]
                                 ,[EndDate]
                                 ,[EndsOn]
                                 ,[EndsAfterXOccurrences]
                                 ,[RecursEvery]
                                 ,[DayOfWeekEnum]
                                 ,[IsDeleted]
                                 ,[MonthEnum]
                                 ,[DayOfMonth]
                                 ,[PatternEnum]
                                 ,[DayOfWeekIndex]
                                 FROM [CRM].[EventRecurring] where RecurringEventId=@RecurringEventId";

                var result = connection.Query<EventRecurring>(query, new { RecurringEventId }).FirstOrDefault();
                return result;
            }
        }

        public List<CalendarTimeEntries> GetEventTimeEntries(int CalendarId)
        {
            string query = " select * from [CRM].[CalendarTimeEntries] where CalendarId=@CalendarId";
            List<CalendarTimeEntries> EventTimeEntriesTPModel = new List<CalendarTimeEntries>();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var result = connection.Query<CalendarTimeEntries>(query, new { CalendarId }).ToList();
                if (result != null && result.Count > 0)
                    EventTimeEntriesTPModel = result;
                else
                {
                    var cal = connection.Query<Calendar>("select AptTypeEnum from CRM.Calendar where CalendarId=@CalendarId", new { CalendarId }).FirstOrDefault();

                    CalendarTimeEntries et = new CalendarTimeEntries();
                    et.CalendarId = CalendarId;
                    et.EntryType = 0;
                    et.Adjusted = false;
                    et.CreatedBy = User.PersonId;
                    et.LastUpdatedBy = User.PersonId;

                    connection.Insert<CalendarTimeEntries>(et);
                    et.EntryType = (int)cal.AptTypeEnum;
                    connection.Insert<CalendarTimeEntries>(et);

                    return connection.Query<CalendarTimeEntries>(query, new { CalendarId }).ToList();
                }
            }
            return EventTimeEntriesTPModel;
        }

        public bool UpdateEventTimeEntries(CalendarTimeEntries m)
        {
            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var dbEntry = con.Get<CalendarTimeEntries>(m.Id);
                dbEntry.StartDate = m.StartDate;
                dbEntry.EndDate = m.EndDate;
                dbEntry.Notes = m.Notes;
                dbEntry.Adjusted = m.Adjusted;
                dbEntry.LastUpdatedOn = DateTime.UtcNow;
                dbEntry.LastUpdatedBy = User.PersonId;
                con.Update<CalendarTimeEntries>(dbEntry);
            }
            return true;
        }

        #endregion TouchPoint Connect
    }

    public class SalesAgents
    {
        public int OwnerID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }

    public class CalendarAppointments
    {
        public int CalendarAppointmentId { get; set; }
        public int CalendarId { get; set; }
        public int LeadId { get; set; }
        public int AccountId { get; set; }
        public int OpportunityId { get; set; }
        public int OrderId { get; set; }
    }

    public class CalendarAccount
    {
        public int AccountId { get; set; }
        public int PersonId { get; set; }
        public string FullName { get; set; }
    }

    public class CalendarOpportunity
    {
        public int OpportunityId { get; set; }
        public string OpportunityName { get; set; }
    }
}