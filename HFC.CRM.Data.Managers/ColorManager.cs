﻿using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using HFC.CRM.Data.Context;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;

namespace HFC.CRM.Managers 
{
    

    public class ColorsManager : DataManager
    {
        public ColorsManager(Franchise franchise, User user)
        {
            Franchise = franchise;
            AuthorizingUser = user;
        }

        // TODO: is this required in TP???
        //public Permission Account_BasePermission
        //{
        //    get { return PermissionProvider.GetPermission(this.AuthorizingUser, "Settings", "Account"); }
        //}

        public List<Type_NamedColor> GetColors()
        {
            return CRMDBContext.Type_NamedColor.Where(c => c.FranchiseId == this.Franchise.FranchiseId).AsNoTracking().ToList();
        }

        public List<Type_NamedColor> GetGategoryColors(int categoryId)
        {
            var allColors = CRMDBContext.Type_NamedColor.Where(c => c.Category_Color.Any(cc => cc.CategoryId == categoryId) && c.FranchiseId == this.Franchise.FranchiseId).AsNoTracking().ToList();

            return allColors;
        }

        public void SetGategoryColors(int categoryId, List<int> colorIds, bool removeCurrent = true)
        {
            using (var db = ContextFactory.Create())
            {
                var currentColors =
                    db.Category_Color.Where(
                        cc => cc.CategoryId == categoryId && cc.FranchiseId == this.Franchise.FranchiseId);
                if (removeCurrent)
                {
                    foreach (var categoryColor in currentColors)
                    {
                        db.Category_Color.Remove(categoryColor);
                    }

                    CRMDBContext.SaveChanges();
                }

                var newColors = db.Type_NamedColor.Where(c => colorIds.Contains(c.NamedColorId)).ToList();
                foreach (var typeNamedColor in newColors)
                {
                    db.Category_Color.Add(new Category_Color()
                    {
                        NamedColorId = typeNamedColor.NamedColorId,
                        CategoryId = categoryId,
                        CreatedOn = DateTime.Now,
                        FranchiseId = this.Franchise.FranchiseId
                    });
                }

                db.SaveChanges();

                // invalidate category/color cache
                CacheManager.ProductCategoryCollection = null;
            }
        }

        public string AddColor(string colorName, out int colorId)
        {
            colorId = 0;
            if (string.IsNullOrEmpty(colorName))
                return "Action cancelled, color name required";

            // TODO: is this required in TP???
            //if (!Account_BasePermission.CanCreate)
            //    return Unauthorized;

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var namedColor =
                        db.Type_NamedColor.Where(color => color.FranchiseId == this.Franchise.FranchiseId)
                            .ToList()
                            .SingleOrDefault(
                                color =>
                                    String.Equals(color.NamedColor, colorName,
                                        StringComparison.InvariantCultureIgnoreCase));
                    if (namedColor != null)
                        return "Action cancelled, duplicate color name found.";

                    var namedColorEntity = db.Type_NamedColor.Add(new Type_NamedColor()
                    {
                        FranchiseId = this.Franchise.FranchiseId,
                        NamedColor = colorName
                    });

                    db.SaveChanges();
                    colorId = namedColorEntity.NamedColorId;

                    return Success;
                }
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }
     }
}
