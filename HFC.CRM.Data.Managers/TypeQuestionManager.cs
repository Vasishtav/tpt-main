﻿
using System;
using System.Data.SqlClient;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using HFC.CRM.Data.Context;
using System.Data;
using HFC.CRM.Data;
using HFC.CRM.Core.Logs;
using HFC.CRM.Core.Common;

namespace HFC.CRM.Managers
{
    /// <summary>
    /// Class TypeQuestionManager.
    /// </summary>
    public class TypeQuestionManager
    {
        private int User { get; set; }
        private int AuthorizingUser { get; set; }
        private int Franchise { get; set; }

        /// <summary>
        /// Initialization of User and Franhise
        /// </summary>
        public TypeQuestionManager(int user, int franchise)
        {
            this.User = user;
            this.AuthorizingUser = user;
            this.Franchise = franchise;
        }

        /// <summary>
        /// Get list of Type_Question based on BrandId
        /// </summary>
        public static List<Type_Question> GetTypeQuestions(int brandId)
        {
            List<Type_Question> lstQa = null;
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                string Query = "SELECT QuestionId";
                Query = Query + ", Question";
                Query = Query + ", SubQuestion";
                Query = Query + ", CreatedOnUtc";
                Query = Query + ", SelectionType";
                Query = Query + ", DisplayOrder";
                Query = Query + ", FranchiseId";
                Query = Query + ", BrandId";
                Query = Query + ", '' AS 'Answer'";
                Query = Query + ", 0  AS 'AnswerId'";
                Query = Query + "FROM [CRM].[Type_Questions] ";
                Query = Query + "WHERE LeadQuestion=1 AND BrandId=@BrandId";
                lstQa = connection.Query<Type_Question>(Query,
                new
                {
                    BrandId = brandId
                }).OrderBy(ob => ob.DisplayOrder).ToList();
                connection.Close();
            }
            return lstQa;
        }
        public static List<Type_Question> GetOpportunityTypeQuestions(int brandId)
        {
            List<Type_Question> lstQa = null;
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                string Query = "SELECT QuestionId";
                Query = Query + ", Question";
                Query = Query + ", SubQuestion";
                Query = Query + ", CreatedOnUtc";
                Query = Query + ", SelectionType";
                Query = Query + ", DisplayOrder";
                Query = Query + ", FranchiseId";
                Query = Query + ", BrandId";
                Query = Query + ", '' AS 'Answer'";
                Query = Query + ", 0  AS 'AnswerId'";
                Query = Query + "FROM [CRM].[Type_Questions] ";
                Query = Query + "WHERE LeadQuestion=1 AND BrandId=@BrandId AND OpportunityQuestion=1";
                lstQa = connection.Query<Type_Question>(Query,
                new
                {
                    BrandId = brandId
                }).OrderBy(ob => ob.DisplayOrder).ToList();
                connection.Close();
            }
            return lstQa;
        }

        /// <summary>
        /// Get list of Type_Question based on BrandId
        /// </summary>
        public static List<Type_Question> GetTypeQuestionsById(int brandId, int leadId)
        {
            List<Type_Question> lstQa = null;
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                string Query = "SELECT ";
                Query = Query + "TYQ.QuestionId";
                Query = Query + ", TYQ.Question";
                Query = Query + ", TYQ.SubQuestion";
                Query = Query + ", TYQ.CreatedOnUtc";
                Query = Query + ", TYQ.SelectionType";
                Query = Query + ", TYQ.DisplayOrder";
                Query = Query + ", TYQ.FranchiseId";
                Query = Query + ", TYQ.BrandId";
                Query = Query + ", TYQ.IsPrimaryQuestion";
                Query = Query + ", ISNULL((CASE WHEN QA.Answer = 'True' THEN 'true' WHEN QA.Answer = 'False' THEN 'false' ELSE QA.Answer END), '') AS 'Answer'";
                Query = Query + ", ISNULL(QA.AnswerId, '') AS 'AnswerId'";
                Query = Query + ", ISNULL((CASE WHEN TYQ.SelectionType = 'date' THEN (CASE WHEN QA.Answer != '' THEN CONVERT(VARCHAR, CONVERT(DATE,QA.Answer), 101) ELSE QA.Answer END) ELSE '' END),'') AS 'Answers'";
                Query = Query + ", (CASE SelectionType WHEN 'date' THEN (CASE WHEN QA.Answer<>'' THEN (CASE WHEN CONVERT(VARCHAR,CONVERT(datetime,QA.Answer),111)<CONVERT(VARCHAR,GETDATE(),111) THEN 'true' ELSE 'false' END) ELSE 'false' END)  ELSE 'false' END) AS 'PastDate'";
                Query = Query + " FROM [CRM].[Type_Questions] TYQ ";
                Query = Query + "LEFT OUTER JOIN ";
                Query = Query + "(SELECT QuestionId, Answer, AnswerId FROM [CRM].[QuestionAnswers] WHERE LeadId = @LeadId) ";
                Query = Query + "AS QA on TYQ.QuestionId = QA.QuestionId ";
                Query = Query + "WHERE LeadQuestion = 1 AND BrandId = @BrandId";
                lstQa = connection.Query<Type_Question>(Query,
                new
                {
                    BrandId = brandId,
                    LeadId = leadId
                }).OrderBy(ob => ob.DisplayOrder).ToList();
                connection.Close();
            }
            return lstQa;
        }

        /// <summary>
        /// Get list of Lead details
        /// </summary>
        public static string GetLeadQuestions(int leadId)
        {
            if (leadId <= 0)
                throw new ArgumentNullException("Lead id should be greater than zero");

            string LeadDeatils = "";
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    //commented code to be removed later.
                    connection.Open();
                    //string Query = "SELECT TOP 1 ISNULL(CUS.FirstName, '') + ' ' + ISNULL(CUS.LastName, '') + ';' ";
                    //Query = Query + " + CONVERT(VARCHAR,LDS.LeadStatusId) + ';' + CONVERT(VARCHAR,ISNULL(LDS.DispositionId,0)) + ';' ";
                    //Query = Query + " + CONVERT(VARCHAR,LDS.LeadNumber) + ';' ";
                    //Query = Query + " + (ADR.Address1 + ' ' + ADR.Address2 + ' ' + ADR.City + ' ' + ADR.State + ' ' + ADR.ZipCode)";
                    //Query = Query + "FROM CRM.Customer CUS ";
                    //Query = Query + "INNER JOIN CRM.LeadCustomers LCS ON CUS.CustomerId = LCS.CustomerId ";
                    //Query = Query + "INNER JOIN CRM.Leads LDS ON LDS.LeadId = LCS.LeadId ";
                    //Query = Query + "INNER JOIN CRM.LeadAddresses LDR ON LDR.LeadId = LDS.LeadId ";
                    //Query = Query + "INNER JOIN CRM.Addresses ADR ON ADR.AddressId = LDR.AddressId ";
                    //Query = Query + "WHERE LCS.IsPrimaryCustomer = 1 AND LCS.LeadId=@LeadId ";
                    string Query = @"SELECT ISNULL(CUS.FirstName, '') 
                                    + ' ' + ISNULL(CUS.LastName, '') 
	                                + ';'  + CONVERT(VARCHAR,LDS.LeadStatusId) 
	                                + ';' + CONVERT(VARCHAR,ISNULL(LDS.DispositionId,0)) 
	                                + ';'  + CONVERT(VARCHAR,LDS.LeadNumber) 
	                                + ';'  + ISNULL(ADR.Address1,'') + ' ' + ISNULL(ADR.Address2,'') 
	                                + ' ' + ISNULL(ADR.City,'') + ' ' + ISNULL(ADR.State,'') + ' ' + ISNULL(ADR.ZipCode,'')
                                    + ';'  +  ISNULL(CUS.CompanyName, '') 
									+ ';'  +  CONVERT(VARCHAR,LDS.IsCommercial)
                                    +';'+ISNULL(CUS.PrimaryEmail,'')
                                    +';'+ISNULL(CUS.SecondaryEmail,'')   
                                    +';'+ Convert(VARCHAR,LDS.IsNotifyemails)
	                                FROM CRM.Customer CUS 
	                                INNER JOIN CRM.LeadCustomers LCS ON CUS.CustomerId = LCS.CustomerId and LCS.IsPrimaryCustomer=1
	                                INNER JOIN CRM.Leads LDS ON LDS.LeadId = LCS.LeadId 
	                                INNER JOIN CRM.LeadAddresses LDR ON LDR.LeadId = LDS.LeadId 
	                                INNER JOIN CRM.Addresses ADR ON ADR.AddressId = LDR.AddressId WHERE LCS.IsPrimaryCustomer = 1 
	                                AND LCS.LeadId=@LeadId";
                    //modified the query as it is returning null value if address fields are null.
                    //Execute and get details
                    LeadDeatils = connection.Query<string>(Query,
                    new
                    {
                        LeadId = leadId
                    }).FirstOrDefault();
                    connection.Close();
                }
                return LeadDeatils;
            }
            catch (Exception ex) { return LeadDeatils; }
        }

        public static List<Type_Question> GetTypeOpportunityQuestionsById(int brandId, int opportunityId)
        {
            List<Type_Question> lstQa = null;
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                string Query = "SELECT ";
                Query = Query + "TYQ.QuestionId";
                Query = Query + ", TYQ.Question";
                Query = Query + ", TYQ.SubQuestion";
                Query = Query + ", TYQ.CreatedOnUtc";
                Query = Query + ", TYQ.SelectionType";
                Query = Query + ", TYQ.DisplayOrder";
                Query = Query + ", TYQ.FranchiseId";
                Query = Query + ", TYQ.BrandId";
                Query = Query + ", ISNULL((CASE WHEN QA.Answer = 'True' THEN 'true' WHEN QA.Answer = 'False' THEN 'false' ELSE QA.Answer END), '') AS 'Answer'";
                Query = Query + ", ISNULL(QA.AnswerId, '') AS 'AnswerId'";
                Query = Query + "FROM [CRM].[Type_Questions] TYQ ";
                Query = Query + "Left JOIN ";
                Query = Query + "(SELECT QuestionId, Answer, AnswerId FROM [CRM].[QuestionAnswers] WHERE opportunityId = @opportunityId) ";
                Query = Query + "AS QA on TYQ.QuestionId = QA.QuestionId ";
                Query = Query + "WHERE OpportunityQuestion = 1 AND BrandId = @BrandId";
                lstQa = connection.Query<Type_Question>(Query,
                new
                {
                    BrandId = brandId,
                    opportunityId = opportunityId
                }).OrderBy(ob => ob.DisplayOrder).ToList();
                connection.Close();
            }
            return lstQa;
        }


        /// <summary>
        /// Adds the answer.
        /// </summary>
        /// Save the Answer details based on Question
        /// <param name="leadId">The leadId identifier.</param>
        /// <param name="jobId">The jobId identifier.</param>
        /// <param name="questionId">The question identifier.</param>
        /// <param name="answer">The answer.</param>
        /// <param name="answerId">The answer identifier.</param>
        /// <returns>System.String.</returns>
        public string SaveQuestionAnswers(int leadId, List<Type_Question> listAnswer, out int answerId)
        {
            answerId = 0;
            //To check parameter has valid data and return with message
            if (leadId <= 0 || listAnswer.Count == 0)
                return HFC.CRM.Core.Common.ContantStrings.DataCannotBeNullOrEmpty;

            int questionId = 0,
               jobId = 0;
            string Answered = "", QuestionType = "";

            try
            {
                //Save using stored procedure CRM.SaveQuestionAnswer
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    foreach (var item in listAnswer)
                    {
                        questionId = (int)item.QuestionId;
                        jobId = 0;
                        Answered = (string)item.Answer;
                        //If question type is date then set date format
                        QuestionType = (string)item.SelectionType;

                        //if(QuestionType == "date" && (Answered != null &&Answered.Length>0))
                        //{
                        //    Answered = Answered.Substring(0, 10);
                        //}
                        if (QuestionType == "date" && Answered == null)
                        {
                            Answered = "";
                        }
                        //
                        var Query = "IF NOT EXISTS(SELECT 1 FROM[CRM].[QuestionAnswers] WHERE QuestionId=@QuestionId AND LeadId=@LeadId) BEGIN ";
                        Query = Query + "INSERT INTO [CRM].[QuestionAnswers] ";
                        Query = Query + "(QuestionId, Answer, LeadId, JobId, ";
                        Query = Query + "CreatedOnUtc, LoggedByPersonId) ";
                        Query = Query + "VALUES ";
                        Query = Query + "(@QuestionId, @Answer, @LeadId, @JobId,";
                        Query = Query + "@CreatedOnUtc, @LoggedByPersonId);";
                        Query = Query + "END; ELSE BEGIN ";
                        Query = Query + "UPDATE [CRM].[QuestionAnswers] SET ";
                        Query = Query + "Answer = @Answer ";
                        Query = Query + ", LastUpdatedOnUtc = GETDATE() ";
                        Query = Query + "WHERE QuestionId = @QuestionId ";
                        Query = Query + "AND LeadId = @LeadId ";
                        Query = Query + "AND Answer<>@Answer; END ";
                        //
                        var AnswerId = connection.Query<int>(Query, new
                        {
                            QuestionId = questionId,
                            Answer = Answered,
                            LeadId = leadId,
                            JobId = jobId,
                            CreatedOnUtc = DateTime.Now,
                            LoggedByPersonId = User,
                        });
                        answerId = 1;
                    }
                    connection.Close();
                }
                return Convert.ToString(answerId);
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        public string SaveAccountQuestionAnswers(int accountId, List<Type_Question> listAnswer, out int answerId)
        {
            answerId = 0;
            //To check parameter has valid data and return with message
            if (accountId <= 0 || listAnswer == null)
                return HFC.CRM.Core.Common.ContantStrings.DataCannotBeNullOrEmpty;

            int questionId = 0,
               jobId = 0;
            string Answered = "", QuestionType = "";

            try
            {
                //Save using stored procedure CRM.SaveQuestionAnswer
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    foreach (var item in listAnswer)
                    {
                        questionId = (int)item.QuestionId;
                        jobId = 0;
                        Answered = (string)item.Answer;
                        //If question type is date then set date format
                        QuestionType = (string)item.SelectionType;
                        if (QuestionType == "date" && (Answered != null && Answered.Length > 0))
                        {
                            Answered = Answered.Substring(0, 10);
                        }
                        if (QuestionType == "date" && Answered == null)
                        {
                            Answered = "";
                        }
                        //
                        var Query = "IF NOT EXISTS(SELECT 1 FROM[CRM].[QuestionAnswers] WHERE QuestionId=@QuestionId AND AccountId=@AccountId) BEGIN ";
                        Query = Query + "INSERT INTO [CRM].[QuestionAnswers] ";
                        Query = Query + "(QuestionId, Answer, AccountId, JobId, ";
                        Query = Query + "CreatedOnUtc, LoggedByPersonId) ";
                        Query = Query + "VALUES ";
                        Query = Query + "(@QuestionId, @Answer, @AccountId, @JobId,";
                        Query = Query + "@CreatedOnUtc, @LoggedByPersonId);";
                        Query = Query + "END; ELSE BEGIN ";
                        Query = Query + "UPDATE [CRM].[QuestionAnswers] SET ";
                        Query = Query + "Answer = @Answer ";
                        Query = Query + ", LastUpdatedOnUtc = GETDATE() ";
                        Query = Query + "WHERE QuestionId = @QuestionId ";
                        Query = Query + "AND AccountId = @AccountId ";
                        Query = Query + "AND Answer<>@Answer; END ";
                        //
                        var AnswerId = connection.Query<int>(Query, new
                        {
                            QuestionId = questionId,
                            Answer = Answered,
                            AccountId = accountId,
                            JobId = jobId,
                            CreatedOnUtc = DateTime.Now,
                            LoggedByPersonId = User,
                        });
                        answerId = 1;
                    }
                    connection.Close();
                }
                return Convert.ToString(answerId);
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        /// <summary>
        /// Adds the answer.
        /// </summary>
        /// Save the Answer details based on Question
        /// <param name="leadId">The leadId identifier.</param>
        /// <param name="jobId">The jobId identifier.</param>
        /// <param name="questionId">The question identifier.</param>
        /// <param name="answer">The answer.</param>
        /// <param name="answerId">The answer identifier.</param>
        /// <returns>System.String.</returns>
        public string SaveOpportunityQuestionAnswers(int opportunityId, List<Type_Question> listAnswer)
        {
            //To check parameter has valid data and return with message
            if (opportunityId <= 0 || listAnswer == null)
                return HFC.CRM.Core.Common.ContantStrings.DataCannotBeNullOrEmpty;

            int questionId = 0, jobId = 0;
            string Answered = "";

            string Query = @"IF NOT EXISTS(SELECT 1 FROM[CRM].[QuestionAnswers] 
                                      WHERE QuestionId=@QuestionId AND OpportunityId=@OpportunityId) 
                                      BEGIN 
                                      INSERT INTO [CRM].[QuestionAnswers] (QuestionId, Answer, OpportunityId, JobId, CreatedOnUtc, LoggedByPersonId) 
                                      VALUES (@QuestionId, @Answer, @OpportunityId, @JobId,@CreatedOnUtc, @LoggedByPersonId);
                                      END; 
                                      ELSE 
                                      BEGIN 
                                      UPDATE [CRM].[QuestionAnswers] SET Answer = @Answer , LastUpdatedOnUtc = GETDATE() 
                                      WHERE QuestionId = @QuestionId AND OpportunityId = @opportunityId AND Answer<>@Answer; 
                                      END ";

            try
            {
                //Save using stored procedure CRM.SaveQuestionAnswer
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    foreach (var item in listAnswer)
                    {
                        questionId = item.QuestionId;
                        Answered = item.Answer;

                        var AnswerId = connection.Query<int>(Query, new
                        {
                            QuestionId = questionId,
                            Answer = Answered,
                            opportunityId = opportunityId,
                            JobId = jobId,
                            CreatedOnUtc = DateTime.Now,
                            LoggedByPersonId = User,
                        });
                    }
                }
                return ContantStrings.Success;
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        /// <summary>
        /// Adds the answer.
        /// </summary>
        /// Save the Answer details based on Question
        /// <param name="leadId">The leadId identifier.</param>
        /// <param name="jobId">The jobId identifier.</param>
        /// <param name="questionId">The question identifier.</param>
        /// <param name="answer">The answer.</param>
        /// <param name="answerId">The answer identifier.</param>
        /// <returns>System.String.</returns>
        public string AddAnswer(int leadId, int jobId, int questionId, string answer, out int answerId)
        {
            answerId = 0;
            //To check parameter has valid data and return with message
            if (leadId <= 0 || questionId <= 0 || string.IsNullOrEmpty(answer))
                return HFC.CRM.Core.Common.ContantStrings.DataCannotBeNullOrEmpty;

            try
            {
                //Save using stored procedure CRM.SaveQuestionAnswer
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var Query = "IF NOT EXISTS(SELECT 1 FROM[CRM].[QuestionAnswers] WHERE QuestionId=@QuestionId AND LeadId=@LeadId) BEGIN ";
                    Query = Query + "INSERT INTO [CRM].[QuestionAnswers] ";
                    Query = Query + "(QuestionId, Answer, LeadId, JobId, ";
                    Query = Query + "CreatedOnUtc, LoggedByPersonId) ";
                    Query = Query + "VALUES ";
                    Query = Query + "(@QuestionId, @Answer, @LeadId, @JobId,";
                    Query = Query + "@CreatedOnUtc, @LoggedByPersonId);";
                    Query = Query + "END; ELSE BEGIN ";
                    Query = Query + "UPDATE [CRM].[QuestionAnswers] SET ";
                    Query = Query + "Answer = @Answer ";
                    Query = Query + ", LastUpdatedOnUtc = GETDATE() ";
                    Query = Query + "WHERE QuestionId = @QuestionId ";
                    Query = Query + "AND LeadId = @LeadId ";
                    Query = Query + "AND Answer<>@Answer; END ";
                    //
                    var AnswerId = connection.Query<int>(Query, new
                    {
                        QuestionId = questionId,
                        Answer = answer,
                        LeadId = leadId,
                        JobId = jobId,
                        CreatedOnUtc = DateTime.Now,
                        LoggedByPersonId = User,
                    });
                    connection.Close();
                    return Convert.ToString(AnswerId);
                }
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        /// <summary>
        /// Updates the answers.
        /// </summary>
        /// <param name="leadId">The job identifier.</param>
        /// <param name="answerId">The answer identifier.</param>
        /// <param name="answer">The answer.</param>
        /// <returns>System.String.</returns>
        public string UpdateAnswers(int leadId, int answerId, string answer)
        {
            if (leadId <= 0 || answerId <= 0)
                return HFC.CRM.Core.Common.ContantStrings.DataCannotBeNullOrEmpty;
            try
            {
                //Save using stored procedure CRM.SaveQuestionAnswer
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var Query = "UPDATE [CRM].[QuestionAnswers] SET ";
                    Query = Query + " Answer=@Answer ";
                    Query = Query + " WHERE AnswerId=@AnswerId ";
                    Query = Query + " AND Answer<>@Answer ";
                    //
                    var retAnswerId = connection.Query<int>(Query, new
                    {
                        Answer = answer,
                        AnswerId = answerId
                    });
                    connection.Close();
                    return Convert.ToString(retAnswerId);
                }
            }
            catch { return ""; }
            return "";
        }
    }
}