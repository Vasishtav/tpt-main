﻿using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using HFC.CRM.DTO.Product;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HFC.CRM.Core.Logs;
using System.Data.SqlClient;
using Dapper;
using HFC.CRM.Core.Extensions;

namespace HFC.CRM.Managers
{
    public class SurfacingProductManagerTP : DataManager<SurfacingProducts>
    {
        //public override Permission BasePermission
        //{
        //    get
        //    {
        //        throw new NotImplementedException();
        //    }
        //}
        public SurfacingProductManagerTP()
        {
        }

        public SurfacingProductManagerTP(User user) : this(user, user)
        {
        }

        public SurfacingProductManagerTP(User user, User authorizingUser)
        {
            this.User = user;
            this.AuthorizingUser = authorizingUser;
            //this.Franchise = franchise;
        }

        public string SaveDiscount(FranchiseDiscount data)
        {
            var query = @"select DiscountIdPk from Crm.FranchiseDiscount where DiscountId=@DiscountId";
            var results = ExecuteIEnumerableObject<FranchiseDiscount>(ContextFactory.CrmConnectionString, query, new
            {
                DiscountId = data.DiscountId,
            }).FirstOrDefault();
            if (results != null && results.DiscountIdPk > 0)
            {
                data.DiscountIdPk = results.DiscountIdPk;
                var Check = CheckDupli(data);
                if (Check == "True")
                {
                    return "Discount Name Already Exists";
                }
                else
                {
                    data.DiscountIdPk = results.DiscountIdPk;
                    DiscountUpdate(data);
                }
            }
            else
            {
                var Check = CheckDupli(data);
                if (Check == "True")
                {
                    return "Discount Name Already Exists";
                }
                else
                {
                    var result = Insert<int, FranchiseDiscount>(ContextFactory.CrmConnectionString, data);
                    if (result != 0)
                    {
                        var output = data.DiscountId.ToString();
                        return output;
                    }
                }
            }
            return Success;
        }

        public string DiscountUpdate(FranchiseDiscount data)
        {
            if (data.FranchiseId != 0)
            {
                var query = @"select * from Crm.FranchiseDiscount where DiscountIdPk=@DiscountIdPk";
                var result = ExecuteIEnumerableObject<FranchiseDiscount>(ContextFactory.CrmConnectionString, query, new
                {
                    DiscountIdPk = data.DiscountIdPk,
                }).FirstOrDefault();
                if (result != null)
                {
                    data.CreatedBy = result.CreatedBy;
                    data.LastUpdatedBy = result.LastUpdatedBy;
                    var updatedata = Update<FranchiseDiscount>(ContextFactory.CrmConnectionString, data);
                }
            }

            return "Success";
        }

        public string CheckDupli(FranchiseDiscount data)
        {
            if (data.DiscountIdPk != 0)
            {
                var query = @"select * from Crm.FranchiseDiscount where Name=@Name and DiscountIdPk!=@IdPk";
                var resultvalue = ExecuteIEnumerableObject<FranchiseDiscount>(ContextFactory.CrmConnectionString, query, new
                {
                    Name = data.Name,
                    IdPk = data.DiscountIdPk
                }).ToList();
                if (resultvalue.Count != 0)
                {
                    return "True";
                }
                else if (resultvalue.Count == 0)
                {
                    return "False";
                }
            }
            else
            {
                if (data.DiscountId != 0)
                {
                    var query = @"select * from Crm.FranchiseDiscount where Name=@Name";
                    var resultvalue = ExecuteIEnumerableObject<FranchiseDiscount>(ContextFactory.CrmConnectionString, query, new
                    {
                        Name = data.Name
                    }).ToList();
                    if (resultvalue.Count != 0)
                    {
                        return "True";
                    }
                    else if (resultvalue.Count == 0)
                    {
                        return "False";
                    }
                }
            }

            return null;
        }

        public bool PrdtVenNameCheck(SurfacingProducts data)
        {
            var query = "";

            if (data.ProductKey != 0)
            {
                if (data.ProductType == 3)
                {
                    query = @"select * from CRM.SurfacingProducts where ProductName=@Name and
                              ProductKey!=@Key and ProductType=@Type";
                    var resultvalue = ExecuteIEnumerableObject<SurfacingProducts>(ContextFactory.CrmConnectionString, query, new
                    {
                        Type = data.ProductType,
                        Key = data.ProductKey,
                        VendorID = data.VendorID,
                        VendorName = data.VendorName,
                        Name = data.ProductName
                    }).ToList();
                    if (resultvalue.Count != 0)
                    {
                        return true;
                    }
                    else if (resultvalue.Count == 0)
                    {
                        return false;
                    }
                }
                else
                {
                    query = @"select * from CRM.SurfacingProducts where ProductName=@Name and ProductKey!=@Key  and ProductType=@Type and VendorID=@VendorID and VendorName=@VendorName ";
                    var result = ExecuteIEnumerableObject<SurfacingProducts>(ContextFactory.CrmConnectionString, query, new
                    {
                        Type = data.ProductType,
                        Key = data.ProductKey,
                        VendorID = data.VendorID,
                        VendorName = data.VendorName,
                        Name = data.ProductName
                    }).ToList();
                    if (result.Count != 0)
                    {
                        return true;
                    }
                    else if (result.Count == 0)
                    {
                        return false;
                    }
                }
            }
            else
            {
                if (data.ProductName != null || data.ProductName != "")
                {
                    if (data.ProductType == 3)
                    {
                        query = @"select * from CRM.SurfacingProducts where ProductName=@Name and ProductType=@Type";
                        var result1 = ExecuteIEnumerableObject<SurfacingProducts>(ContextFactory.CrmConnectionString, query, new
                        {
                            Type = data.ProductType,
                            Name = data.ProductName
                        }).ToList();

                        if (result1.Count != 0)
                        {
                            return true;
                        }
                        else if (result1.Count == 0)
                        {
                            return false;
                        }
                    }
                    else
                    {
                        query = @"select * from CRM.SurfacingProducts where ProductName=@Name and ProductType=@Type and VendorID=@VendorID and VendorName=@VendorName ";
                        var result = ExecuteIEnumerableObject<SurfacingProducts>(ContextFactory.CrmConnectionString, query, new
                        {
                            Type = data.ProductType,
                            Name = data.ProductName,
                            VendorID = data.VendorID,
                            VendorName = data.VendorName
                        }).ToList();

                        if (result.Count != 0)
                        {
                            return true;
                        }
                        else if (result.Count == 0)
                        {
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        public string SaveTempdata(SurfacingProducts model)
        {
            if (model.ProductID != 0)
            {
                Guid TempId = Guid.NewGuid();
                var jsonString = JsonConvert.SerializeObject(model);
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    var query = @"INSERT INTO [History].[TempStorage]([TempId],[TempData])VALUES (@TempId,@TempData)";
                    connection.Execute(query, new
                    {
                        TempId = TempId,
                        TempData = jsonString,
                    });
                }
                return TempId.ToString();
            }
            return null;
        }

        public SurfacingProducts GetTempProd(string gid)
        {
            if (gid != null || gid != "")
            {
                var query = @"select * from [History].[TempStorage] where TempId=@Id";
                var data = ExecuteIEnumerableObject<dynamic>(ContextFactory.CrmConnectionString, query, new
                {
                    Id = gid
                }).FirstOrDefault();
                if (data != null)
                {
                    var FP = JsonConvert.DeserializeObject<SurfacingProducts>(Convert.ToString(data.TempData));
                    return FP;
                }
            }
            return null;
        }

        //To save the product
        public string Save(SurfacingProducts data)
        {
            if (data.ProductKey == 0)
            {
                bool resultcheck = PrdtVenNameCheck(data);
                if (resultcheck == true && data.ProductType == 3)
                {
                    return "Service Name Already Exists";
                }
                else if (resultcheck == true)
                {
                    return "My Product Already Exists";
                }
                else
                {
                    if (data.ProductSurfaceSetup != null)
                    {
                        data.SurfaceLaborSet = JsonConvert.SerializeObject(data.ProductSurfaceSetup);
                    }
                    if (data.MultipleSurfaceProduct != null)
                    {
                        data.MultipleSurfaceProductSet = JsonConvert.SerializeObject(data.MultipleSurfaceProduct);
                    }
                    if (data.Attributes != null)
                    {
                        data.MultipartAttributeSet = JsonConvert.SerializeObject(data.Attributes);
                    }
                    var result = Insert<int, SurfacingProducts>(ContextFactory.CrmConnectionString, data);
                    data.ProductKey = result;
                    // Log the History data
                    EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(data), ProductId: data.ProductID);

                    var output = Convert.ToString(result);
                    return output;
                }
            }
            else if (data.ProductKey != 0)
            {
                bool resultcheckupdate = PrdtVenNameCheck(data);
                if (resultcheckupdate == true && data.ProductType == 3)
                {
                    return "Service Name Already Exists";
                }
                else if (resultcheckupdate == true)
                {
                    return "My Product Already Exists";
                }
                else
                {
                    if (data.ProductSurfaceSetup != null)
                    {
                        data.SurfaceLaborSet = JsonConvert.SerializeObject(data.ProductSurfaceSetup);
                    }
                    if (data.MultipleSurfaceProduct != null)
                    {
                        data.MultipleSurfaceProductSet = JsonConvert.SerializeObject(data.MultipleSurfaceProduct);
                    }
                    if (data.Attributes != null)
                    {
                        data.MultipartAttributeSet = JsonConvert.SerializeObject(data.Attributes);
                    }
                    else
                    {
                        data.MultipartAttributeSet = null;
                    }
                    ProductUpdate(data);
                }
            }

            return Success;
        }


        //update product
        public string ProductUpdate(SurfacingProducts data)
        {
            var query = @"Select * from CRM.SurfacingProducts where ProductKey=@ProductKey";
            var result = ExecuteIEnumerableObject<SurfacingProducts>(ContextFactory.CrmConnectionString, query, new
            {
                ProductKey = data.ProductKey
            }).FirstOrDefault();
            if (result != null)
            {
                data.CreatedOn = result.CreatedOn;
                data.CreatedBy = result.CreatedBy;
                data.LastUpdatedOn = DateTime.UtcNow;
                data.LastUpdatedBy = User.PersonId;
                if (data.ProductSurfaceSetup != null)
                {
                    data.SurfaceLaborSet = JsonConvert.SerializeObject(data.ProductSurfaceSetup);
                }
                if (data.MultipleSurfaceProduct != null)
                {
                    data.MultipleSurfaceProductSet = JsonConvert.SerializeObject(data.MultipleSurfaceProduct);
                }
                if (data.Attributes != null)
                {
                    data.MultipartAttributeSet = JsonConvert.SerializeObject(data.Attributes);
                }

                var datas = Update<SurfacingProducts>(ContextFactory.CrmConnectionString, data);
                // Log the History data
                EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(data), ProductId: data.ProductID);
            }

            return "Success";
        }

        public List<HistoryProductPricingLog> GetHistoryProductPricingLog(int ProductKey)
        {
            /// [History].[AuditTrail] maintain the Product history data
            List<HistoryProductPricingLog> Logdata = new List<HistoryProductPricingLog>();
            string query = @"  select TempData from [History].[AuditTrail] hat
                            join CRM.SurfacingProducts fp on hat.ProductId=fp.ProductID
                            where ProductKey=@ProductKey order by hat.CreatedOn asc";
            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var data = con.Query<HistoryProductPricingLog>(query, new { ProductKey }).ToList();

                for (int i = 0; i < data.Count; i++)
                {
                    HistoryProductPricingLog pl = new HistoryProductPricingLog();
                    if (i == 0)
                    {
                        // Default add the first record into list
                        var SurfacingProducts = JsonConvert.DeserializeObject<SurfacingProducts>(data[i].TempData);
                        pl.CreatedOn = TimeZoneManager.ToLocal(SurfacingProducts.LastUpdatedOn);
                        pl.UserName = GetUserName(SurfacingProducts.LastUpdatedBy);
                        pl.Cost = SurfacingProducts.Cost != null ? SurfacingProducts.Cost : 0;
                        pl.UnitPrice = SurfacingProducts.SalePrice != null ? SurfacingProducts.SalePrice : 0;
                        Logdata.Add(pl);
                    }
                    else
                    {
                        // compare the current record with last record in the list
                        var Previosdata = Logdata.Last();
                        var SurfacingProducts = JsonConvert.DeserializeObject<SurfacingProducts>(data[i].TempData);
                        SurfacingProducts.Cost = SurfacingProducts.Cost != null ? SurfacingProducts.Cost : 0;
                        SurfacingProducts.SalePrice = SurfacingProducts.SalePrice != null ? SurfacingProducts.SalePrice : 0;

                        if (Previosdata.Cost != SurfacingProducts.Cost || Previosdata.UnitPrice != SurfacingProducts.SalePrice)
                        {
                            pl.CreatedOn = TimeZoneManager.ToLocal(SurfacingProducts.LastUpdatedOn);
                            pl.UserName = GetUserName(SurfacingProducts.LastUpdatedBy);
                            pl.Cost = SurfacingProducts.Cost;
                            pl.UnitPrice = SurfacingProducts.SalePrice;
                            Logdata.Add(pl);
                        }
                    }
                }
            }

            return Logdata;
        }

        /// Get the Name of the person by PersonId
        /// This used for getting the Created by or Modified by
        public string GetUserName(int PersonId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var Query = @"select FirstName +' '+ LastName as Name from CRM.Person Where PersonId=@PersonId";
                string Value = connection.ExecuteScalar<string>(Query, new { PersonId });
                return Value;
            }
        }

        public override string Add(SurfacingProducts data)
        {
            throw new NotImplementedException();
        }

        public override string Delete(int id)
        {
            throw new NotImplementedException();
        }

        public override SurfacingProducts Get(int id)
        {
            throw new NotImplementedException();
        }

        public override ICollection<SurfacingProducts> Get(List<int> idList)
        {
            throw new NotImplementedException();
        }

        public override string Update(SurfacingProducts data)
        {
            throw new NotImplementedException();
        }

        //To Generate the product id automatically
        public int GetProductNumberNew()
        {
            return ExecuteIEnumerableObject<int>(ContextFactory.CrmConnectionString, "[CRM].[spSurfacingProductNumber_New]").FirstOrDefault();
        }

        //list to kendo
        public List<ProductListViewModel> GetListData()
        {
            //var query = @"select ProductKey,ProductID,ProductName,VendorId,VendorName,VendorProductSKU,ProductCategory,ProductSubCategory,Description,SalePrice,FranchiseId,ProductStatus from
            //             (select hp.ProductKey,ProductID,ProductName,VendorId ,
            //             case when exists(select VendorId from [Acct].[HFCVendors] where VendorId=hp.VendorID)
            //             then (select Name from [Acct].[HFCVendors] where VendorId=hp.VendorID)
            //             else case when exists(select VendorId from [Acct].[FranchiseVendors])
            //             then (select Name from [Acct].[FranchiseVendors] where VendorId=hp.VendorID) end end as VendorName,
            //             VendorProductSKU,pc.ProductCategory,'' as ProductSubCategory,Description,0 as SalePrice,0 as FranchiseId, isnull(hps.ProductStatusId,1) as ProductStatusId from[CRM].[HFCProducts] hp
            //             left join [CRM].[Type_ProductCategory] pc on hp.ProductCategory=pc.ProductCategoryId
            //             left join [CRM].[HFCProductStatus] hps on hp.ProductKey=hps.ProductKey and hps.FranchiseId = @franchiseid) as a
            //             left join [CRM].[Type_ProductStatus] ps on a.ProductStatusId=ps.ProductStatusId where ps.ProductStatusId=1
            //             union

            var query = @"select ProductKey,ProductID,ProductName,sp.VendorId,case when exists
                         (select VendorId from [Acct].[HFCVendors] where VendorId=sp.VendorID)
                         then (select Name from [Acct].[HFCVendors] where VendorId=sp.VendorID)
                         else case when exists(select VendorId from [Acct].[FranchiseVendors])
                         then (select Name from [Acct].[FranchiseVendors] where VendorId=sp.VendorID) end end as VendorName,
                         VendorProductSKU ,pc.ProductCategory,psc.ProductCategory as ProductSubCategory,Description,
                         SalePrice,sp.FranchiseId,ps.ProductStatus from [CRM].[SurfacingProducts] sp
                         --join Acct.FranchiseVendors fv on sp.VendorID=fv.VendorId
                         left join [CRM].[Type_ProductStatus] ps on sp.ProductStatus=ps.ProductStatusId
                         left join [CRM].[Type_ProductCategory] pc on sp.ProductCategory=pc.ProductCategoryId
                         left join [CRM].[Type_ProductCategory] psc on sp.ProductSubCategory=psc.ProductCategoryId
                         where ProductStatusId=1
                         union
                         select ProductKey,ProductID,ProductName,Null as VendorId ,'' as VendorName,'' as VendorProductSKU,
                         pc.ProductCategory,psc.ProductCategory as ProductSubCategory,sp.Description,sp.SalePrice,sp.FranchiseId, ps.ProductStatus from [CRM].[SurfacingProducts] sp
                         left join [CRM].[Type_ProductStatus] ps on sp.ProductStatus=ps.ProductStatusId
                         left join [CRM].[Type_ProductCategory] pc on sp.ProductCategory=pc.ProductCategoryId
                         left join [CRM].[Type_ProductCategory] psc on sp.ProductSubCategory=psc.ProductCategoryId
                         where sp.ProductStatus=1 and sp.ProductType=3";

            var result = ExecuteIEnumerableObject<ProductListViewModel>(ContextFactory.CrmConnectionString, query).ToList();
            if (result != null)
            {
                return result.OrderBy(x => x.ProductName).ToList();
            }

            return null;
        }

        public ProductViewModel GetProduct(int id)
        {
            ProductViewModel result = new ProductViewModel();

            var query = @"select ProductKey,ProductID,ProductName,(select hp.[PICProductID] from [CRM].[HFCProducts] hp
                              where ProductID=fp.[ProductID]) as [PICProductID],
                              case when exists(select VendorId from [Acct].[HFCVendors] where VendorId=fp.VendorID) then
                              (select Name from [Acct].[HFCVendors] where VendorId=fp.VendorID)
                              else case when exists(select VendorId from [Acct].[FranchiseVendors]) then
                              (select Name from [Acct].[FranchiseVendors] where VendorId=fp.VendorID) end end as VendorName,

                              case when exists(select VendorId from [Acct].[HFCVendors] where VendorId=fp.VendorID) then
                              (select VendorType from [Acct].[HFCVendors] where VendorId=fp.VendorID)
                              else case when exists(select VendorId from [Acct].[FranchiseVendors]) then
                              (select VendorType from [Acct].[FranchiseVendors] where VendorId=fp.VendorID) end end as VendorType,

                              [VendorID],VendorProductSKU,pc.ProductCategory,pc.ProductCategoryId,psc.ProductCategory as ProductSubCategory,Description,Taxable,[Cost],
                              SalePrice,FranchiseId,Model,Color,Collection,ps.ProductStatus,tpt.ProductType,
                              [MarkUp],[MarkupType],[Discount],[DiscountType],[CalculatedSalesPrice],
                              fp.SurfaceLaborSet,fp.MultipartSurfacing,fp.MultipleSurfaceProductSet,fp.MultipartAttributeSet,fp.BrandID
                              from [CRM].[SurfacingProducts] fp
                              left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
                              left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
                              left join [CRM].[Type_ProductCategory] psc on fp.ProductSubCategory=psc.ProductCategoryId
                              left join [CRM].[Type_ProductType] tpt on fp.ProductType=tpt.Id
                              where ProductKey=@Id";
            result = ExecuteIEnumerableObject<ProductViewModel>(ContextFactory.CrmConnectionString, query, new
            {
                Id = id
            }).FirstOrDefault();
            if (result != null && result.SurfaceLaborSet != null)
            {
                result.ProductSurfaceSetup = JsonConvert.DeserializeObject<ProductSurfaceSetup>(result.SurfaceLaborSet);
            }
            if (result != null && result.MultipleSurfaceProductSet != null)
            {
                result.MultipleSurfaceProduct = JsonConvert.DeserializeObject<List<MultipleSurfaceProduct>>(result.MultipleSurfaceProductSet);
            }
            if (result != null && result.MultipartAttributeSet != null)
            {
                result.Attributes = JsonConvert.DeserializeObject<MultipartAttributes>(result.MultipartAttributeSet);
            }
            //result.ProductType = "MYProduct";
            if (result == null)
            {
                var query1 = @"select fp.DiscountId as ProductID,fp.Name as ProductName,fp.Description,
                                  fp.DiscountValue as Discount,fp.DiscountFactor as DiscountType,
                                  ProductStatus,fp.Status,fp.DiscountIdPk as ProductKey from Crm.FranchiseDiscount fp
                                  left join [CRM].[Type_ProductStatus] ps on fp.Status=ps.ProductStatusId
                                  where fp.DiscountId=@Id";
                result = ExecuteIEnumerableObject<ProductViewModel>(ContextFactory.CrmConnectionString, query1, new
                {
                    Id = id
                }).FirstOrDefault();
                result.ProductType = "Discount";
                if (result.DiscountType == "1")
                    result.DiscountType = "%";
                else
                    result.DiscountType = "$";
            }

            return result;
        }

        public SurfacingProducts GetDetails(int id)
        {
            SurfacingProducts result = new SurfacingProducts();
            var query = @"select DISTINCT ds.*,fv.VendorType from CRM.SurfacingProducts  ds
                              left join Acct.FranchiseVendors fv on ds.VendorID=fv.VendorId
                               where ds.ProductKey=@ProductKey";
            result = ExecuteIEnumerableObject<SurfacingProducts>(ContextFactory.CrmConnectionString, query, new
            {
                ProductKey = id
            }).FirstOrDefault();

            if (result != null && result.SurfaceLaborSet != null)
            {
                result.ProductSurfaceSetup = JsonConvert.DeserializeObject<ProductSurfaceSetup>(result.SurfaceLaborSet);
            }
            if (result != null && result.MultipleSurfaceProductSet != null)
            {
                result.MultipleSurfaceProduct = JsonConvert.DeserializeObject<List<MultipleSurfaceProduct>>(result.MultipleSurfaceProductSet);
            }

            if (result != null && result.MultipartAttributeSet != null)
            {
                result.Attributes = JsonConvert.DeserializeObject<MultipartAttributes>(result.MultipartAttributeSet);
            }
            //result.ProductType = "MYProduct";
            if (result == null)
            {
                var query1 = @"select DiscountId as ProductID,Name as ProductName,Description,DiscountIdPk as ProductKey,
                                   DiscountValue as Discount,DiscountFactor as DiscountType,
                                   Status as ProductStatus from Crm.FranchiseDiscount where DiscountId=@Ids";
                result = ExecuteIEnumerableObject<SurfacingProducts>(ContextFactory.CrmConnectionString, query1, new
                {
                    Ids = id
                }).FirstOrDefault();
                result.ProductType = 4;
                //if (result.DiscountType == "1")
                //    result.DiscountType = "%";
            }

            return result;
        }

        public List<ProductListViewModel> GetListDetails(int id)
        {
            if (id == 1)//product id 1 is My core product
            {
                //var query = @"select ProductKey,ProductID,ProductName,VendorName,sku,ProductCategory,ProductSubCategory,Description,SalePrice,FranchiseId,ProductStatus from
                //        (select hp.ProductKey,ProductID,ProductName,VendorName,VendorProductSKU as sku,
                //        pc.ProductCategory,'' as ProductSubCategory,Description,0 as SalePrice,0 as FranchiseId, isnull(hps.ProductStatusId,1) as ProductStatusId from[CRM].[HFCProducts] hp
                //        left join [CRM].[Type_ProductCategory] pc on hp.ProductCategory=pc.ProductCategoryId
                //        left join [CRM].[HFCProductStatus] hps on hp.ProductKey=hps.ProductKey and hps.FranchiseId = @franchiseid) as a
                //        left join [CRM].[Type_ProductStatus] ps on a.ProductStatusId=ps.ProductStatusId where ps.ProductStatusId=1";
                var query = @"select ProductKey,ProductID,ProductName,VendorId,VendorName,VendorProductSKU,ProductCategory,ProductSubCategory,Description,SalePrice,FranchiseId,ProductStatus from
                             (select hp.ProductKey,ProductID,ProductName,fv.VendorId ,
                             case when exists(select VendorId from [Acct].[HFCVendors] where VendorId=hp.VendorID)
                             then (select Name from [Acct].[HFCVendors] where VendorId=hp.VendorID)
                             else case when exists(select VendorId from [Acct].[FranchiseVendors])
                             then (select Name from [Acct].[FranchiseVendors] where VendorId=hp.VendorID) end end as VendorName,
                             VendorProductSKU,pc.ProductCategory,'' as ProductSubCategory,Description,0 as SalePrice,0 as FranchiseId, isnull(hps.ProductStatusId,1) as ProductStatusId from[CRM].[HFCProducts] hp
                             join Acct.FranchiseVendors fv on hp.VendorID=fv.VendorId
                             left join [CRM].[Type_ProductCategory] pc on hp.ProductCategory=pc.ProductCategoryId
                             left join [CRM].[HFCProductStatus] hps on hp.ProductKey=hps.ProductKey and fv.VendorStatus=1
                             where fv.VendorStatus=1) as a
                             left join [CRM].[Type_ProductStatus] ps on a.ProductStatusId=ps.ProductStatusId where ps.ProductStatusId=1 ";
                var result = ExecuteIEnumerableObject<ProductListViewModel>(ContextFactory.CrmConnectionString, query).ToList();
                if (result != null)
                {
                    return result.OrderBy(x => x.ProductName).ToList();
                }
            }
            else if (id == 2) // my product
            {
                //var query = @"select ProductKey,ProductID,ProductName,VendorName,VendorProductSKU as sku,
                //              pc.ProductCategory,psc.ProductCategory as ProductSubCategory,Description,SalePrice,FranchiseId,ps.ProductStatus from[CRM].[SurfacingProducts] fp
                //              left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
                //              left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
                //              left join [CRM].[Type_ProductCategory] psc on fp.ProductSubCategory=psc.ProductCategoryId
                //              where FranchiseId = @franchiseid and ps.ProductStatusId=1";
                var query = @"select ProductKey,ProductID,ProductName,VendorId,
                              case when exists(select VendorId from [Acct].[HFCVendors] where VendorId=fp.VendorID)
                              then (select Name from [Acct].[HFCVendors] where VendorId=fp.VendorID)
                              else case when exists(select VendorId from [Acct].[FranchiseVendors])
                              then (select Name from [Acct].[FranchiseVendors] where VendorId=fp.VendorID) end end as VendorName,
                              VendorProductSKU,
                              pc.ProductCategory,psc.ProductCategory as ProductSubCategory,Description,SalePrice,FranchiseId,ps.ProductStatus from [CRM].[SurfacingProducts] fp
                              left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
                              left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
                              left join [CRM].[Type_ProductCategory] psc on fp.ProductSubCategory=psc.ProductCategoryId
                              where ps.ProductStatusId=1";
                var result = ExecuteIEnumerableObject<ProductListViewModel>(ContextFactory.CrmConnectionString, query).ToList();
                if (result != null)
                {
                    return result.OrderBy(x => x.ProductName).ToList();
                }
            }
            else if (id == 3)
            {
                var query = @"select fp.DiscountId as ProductKey,fp.DiscountId as ProductID, fp.Name as ProductName,fp.Description,fp.FranchiseId,
                              fp.Status,ps.ProductStatus
                              from [CRM].[FranchiseDiscount] fp
                              left join [CRM].[Type_ProductStatus] ps on fp.Status=ps.ProductStatusId
                              where fp.Status=1";
                var result = ExecuteIEnumerableObject<ProductListViewModel>(ContextFactory.CrmConnectionString, query).ToList();
                if (result != null)
                {
                    return result.OrderBy(x => x.ProductName).ToList();
                }
            }
            else
            {
                var query = @"select ProductKey,ProductID,ProductName,fp.VendorId,case when exists
                            (select VendorId from [Acct].[HFCVendors] where VendorId=fp.VendorID)
                            then (select Name from [Acct].[HFCVendors] where VendorId=fp.VendorID)
                            else case when exists(select VendorId from [Acct].[FranchiseVendors])
                            then (select Name from [Acct].[FranchiseVendors] where VendorId=fp.VendorID) end end as VendorName,
                            VendorProductSKU ,pc.ProductCategory,psc.ProductCategory as ProductSubCategory,Description,
                            SalePrice,fp.FranchiseId,ps.ProductStatus,fp.MultipleSurfaceProductSet from [CRM].[SurfacingProducts] fp
                            join Acct.FranchiseVendors fv on fp.VendorID=fv.VendorId
                            left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
                            left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
                            left join [CRM].[Type_ProductCategory] psc on fp.ProductSubCategory=psc.ProductCategoryId
                            where fv.VendorStatus=1 and ProductStatusId=1
                            union
                            select ProductKey,ProductID,ProductName,fp.VendorId,'Multiple' as VendorName,
                            VendorProductSKU ,pc.ProductCategory,psc.ProductCategory as ProductSubCategory,Description,
                            SalePrice,fp.FranchiseId,ps.ProductStatus,fp.MultipleSurfaceProductSet from [CRM].[SurfacingProducts] fp
                            left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
                            left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
                            left join [CRM].[Type_ProductCategory] psc on fp.ProductSubCategory=psc.ProductCategoryId
                            where ProductStatusId=1 and fp.VendorID is null and fp.MultipartSurfacing=1
                            union
                            select ProductKey,ProductID,ProductName,Null as VendorId ,'' as VendorName,'' as VendorProductSKU,
                            pc.ProductCategory,psc.ProductCategory as ProductSubCategory,fp.Description,fp.SalePrice,fp.FranchiseId, ps.ProductStatus,fp.MultipleSurfaceProductSet from [CRM].[SurfacingProducts] fp
                            left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
                            left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
                            left join [CRM].[Type_ProductCategory] psc on fp.ProductSubCategory=psc.ProductCategoryId
                            where fp.ProductStatus=1 and fp.ProductType=3";
                //Union
                //select ProductKey,ProductID,ProductName,Null as VendorId ,'' as VendorName,'' as VendorProductSKU,
                //pc.ProductCategory,'' as ProductSubCategory,fp.Description,fp.SalePrice,fp.FranchiseId, ps.ProductStatus from [CRM].[SurfacingProducts] fp
                //left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
                //left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
                //where  fp.FranchiseId= @franchiseid and fp.ProductStatus=1 and fp.ProductType=3";

                var result = ExecuteIEnumerableObject<ProductListViewModel>(ContextFactory.CrmConnectionString, query).ToList();
                if (result != null)
                {
                    return result.OrderBy(x => x.ProductName).ToList();
                }
            }
            return null;
        }

        public List<ProductListViewModel> GetListDataAll(int Id, int Value)
        {
            var query = "";
            if (Value == 1)
            {
                //query = @"select ProductKey,ProductID,ProductName,VendorId,VendorName,VendorProductSKU,ProductCategory,ProductSubCategory,Description,SalePrice,FranchiseId,ProductStatus from
                //         (select hp.ProductKey,ProductID,ProductName,VendorId ,
                //         case when exists(select VendorId from [Acct].[HFCVendors] where VendorId=hp.VendorID)
                //         then (select Name from [Acct].[HFCVendors] where VendorId=hp.VendorID)
                //         else case when exists(select VendorId from [Acct].[FranchiseVendors])
                //         then (select Name from [Acct].[FranchiseVendors] where VendorId=hp.VendorID) end end as VendorName,
                //         VendorProductSKU,pc.ProductCategory,'' as ProductSubCategory,Description,0 as SalePrice,0 as FranchiseId, isnull(hps.ProductStatusId,1) as ProductStatusId from[CRM].[HFCProducts] hp
                //         left join [CRM].[Type_ProductCategory] pc on hp.ProductCategory=pc.ProductCategoryId
                //         left join [CRM].[HFCProductStatus] hps on hp.ProductKey=hps.ProductKey and hps.FranchiseId = @franchiseid) as a
                //         left join [CRM].[Type_ProductStatus] ps on a.ProductStatusId=ps.ProductStatusId";
                query = @"select ProductKey,ProductID,ProductName,VendorId,VendorName,VendorProductSKU,ProductCategory,
                          ProductSubCategory,Description,SalePrice,FranchiseId,ProductStatus from
                          (select hp.ProductKey,ProductID,ProductName,fv.VendorId ,
                          case when exists(select VendorId from [Acct].[HFCVendors] where VendorId=hp.VendorID)
                          then (select Name from [Acct].[HFCVendors] where VendorId=hp.VendorID)
                          else case when exists(select VendorId from [Acct].[FranchiseVendors])
                          then (select Name from [Acct].[FranchiseVendors] where VendorId=hp.VendorID) end end as VendorName,
                          VendorProductSKU,pc.ProductCategory,'' as ProductSubCategory,Description,0 as SalePrice,0 as FranchiseId,
                          isnull(hps.ProductStatusId,1) as ProductStatusId from[CRM].[HFCProducts] hp
                          join Acct.FranchiseVendors fv on hp.VendorID=fv.VendorId
                          left join [CRM].[Type_ProductCategory] pc on hp.ProductCategory=pc.ProductCategoryId
                          left join [CRM].[HFCProductStatus] hps on hp.ProductKey=hps.ProductKey and fv.VendorStatus=1
                          where fv.VendorStatus in(1,2)) as a
                          left join [CRM].[Type_ProductStatus] ps on a.ProductStatusId=ps.ProductStatusId where ps.ProductStatusId in(1,2)";
            }
            else if (Value == 3)
            {
                query = @"select fp.DiscountId as ProductKey,fp.DiscountId as ProductID, fp.Name as ProductName,Null,Null,Null,Null,Null,
                          fp.Description,Null,fp.FranchiseId,ps.ProductStatus from Crm.FranchiseDiscount fp
                          left join [CRM].[Type_ProductStatus] ps on fp.Status=ps.ProductStatusId";
            }
            else
            {
                query = @"select ProductKey,ProductID,ProductName,fp.VendorId,case when exists(select VendorId
                          from [Acct].[HFCVendors] where VendorId=fp.VendorID)
                          then (select Name from [Acct].[HFCVendors] where VendorId=fp.VendorID)
                          else case when exists(select VendorId from [Acct].[FranchiseVendors])
                          then (select Name from [Acct].[FranchiseVendors] where VendorId=fp.VendorID) end end as VendorName,
                          VendorProductSKU ,pc.ProductCategory,psc.ProductCategory as ProductSubCategory,Description,
                          SalePrice,fp.FranchiseId,ps.ProductStatus,fp.MultipleSurfaceProductSet from[CRM].[SurfacingProducts] fp
                          join Acct.FranchiseVendors fv on fp.VendorID=fv.VendorId
                          left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
                          left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
                          left join [CRM].[Type_ProductCategory] psc on fp.ProductSubCategory=psc.ProductCategoryId
                          where fp.ProductStatus in(1,2) and fv.VendorStatus in(1,2) 
                          union
                          select ProductKey,ProductID,ProductName,fp.VendorId,'Multiple' as VendorName,
                          VendorProductSKU ,pc.ProductCategory,psc.ProductCategory as ProductSubCategory,Description,
                          SalePrice,fp.FranchiseId,ps.ProductStatus,fp.MultipleSurfaceProductSet from [CRM].[SurfacingProducts] fp
                          left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
                          left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
                          left join [CRM].[Type_ProductCategory] psc on fp.ProductSubCategory=psc.ProductCategoryId
                          where ProductStatusId=1 and fp.VendorID is null and fp.MultipartSurfacing=1
                          union
                          select ProductKey,ProductID,ProductName,Null as VendorId ,'' as VendorName,'' as VendorProductSKU,
                          pc.ProductCategory,psc.ProductCategory as ProductSubCategory,fp.Description,fp.SalePrice,fp.FranchiseId, ps.ProductStatus,fp.MultipleSurfaceProductSet from [CRM].[SurfacingProducts] fp
                          left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
                          left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
                          left join [CRM].[Type_ProductCategory] psc on fp.ProductSubCategory=psc.ProductCategoryId
                          where fp.ProductStatus=1 and fp.ProductType=3";
            }
            //var query = @"select ProductKey,ProductID,ProductName,VendorId,VendorName,VendorProductSKU,ProductCategory,ProductSubCategory,Description,SalePrice,FranchiseId,ProductStatus from
            //             (select hp.ProductKey,ProductID,ProductName,VendorId ,
            //             case when exists(select VendorId from [Acct].[HFCVendors] where VendorId=hp.VendorID)
            //             then (select Name from [Acct].[HFCVendors] where VendorId=hp.VendorID)
            //             else case when exists(select VendorId from [Acct].[FranchiseVendors])
            //             then (select Name from [Acct].[FranchiseVendors] where VendorId=hp.VendorID) end end as VendorName,
            //             VendorProductSKU,pc.ProductCategory,'' as ProductSubCategory,Description,0 as SalePrice,0 as FranchiseId, isnull(hps.ProductStatusId,1) as ProductStatusId from[CRM].[HFCProducts] hp
            //             left join [CRM].[Type_ProductCategory] pc on hp.ProductCategory=pc.ProductCategoryId
            //             left join [CRM].[HFCProductStatus] hps on hp.ProductKey=hps.ProductKey and hps.FranchiseId = @franchiseid) as a
            //             left join [CRM].[Type_ProductStatus] ps on a.ProductStatusId=ps.ProductStatusId
            //             union
            //             select ProductKey,ProductID,ProductName,VendorId,case when exists(select VendorId from [Acct].[HFCVendors] where VendorId=fp.VendorID)
            //             then (select Name from [Acct].[HFCVendors] where VendorId=fp.VendorID)
            //             else case when exists(select VendorId from [Acct].[FranchiseVendors])
            //             then (select Name from [Acct].[FranchiseVendors] where VendorId=fp.VendorID) end end as VendorName,
            //             VendorProductSKU ,pc.ProductCategory,psc.ProductCategory as ProductSubCategory,Description,SalePrice,FranchiseId,ps.ProductStatus from[CRM].[SurfacingProducts] fp
            //             left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
            //             left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
            //             left join [CRM].[Type_ProductCategory] psc on fp.ProductSubCategory=psc.ProductCategoryId
            //             where FranchiseId = @franchiseid
            //             union
            //             select fp.DiscountId as ProductKey,fp.DiscountId as ProductID, fp.Name as ProductName,Null,Null,Null,Null,Null,
            //             fp.Description,Null,fp.FranchiseId,ps.ProductStatus
            //             from Crm.FranchiseDiscount fp
            //             left join [CRM].[Type_ProductStatus] ps on fp.Status=ps.ProductStatusId
            //             where FranchiseId=@franchiseid";

            var result = ExecuteIEnumerableObject<ProductListViewModel>(ContextFactory.CrmConnectionString, query).ToList();
            if (result != null)
            {
                return result.OrderBy(x => x.ProductName).ToList();
            }
            else
                return null;
        }

        public List<ProductTypeModel> GetProductType()
        {
            var VendorStatusList = ExecuteIEnumerableObject<ProductTypeModel>(ContextFactory.CrmConnectionString, @"select * from CRM.Type_ProductType where Id =2").ToList();// my product
            return VendorStatusList;
        }

        public dynamic GetAllPICProducts()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                try
                {
                    var query =
                  @"SELECT hp.ProductKey, hp.ProductID, hp.ProductName, hp.PICProductID, hp.VendorID, hv.Name as VendorName, hp.VendorProductSKU, hp.Description, hp.ProductCategory, hp.ProductCollection,
                          hp.ProductModelID, hp.ProductGroup, hp.ProductGroupDesc,hv.PICVendorId,hp.DiscontinuedDate,hp.PhasedoutDate
                          FROM crm.HFCProducts AS hp
                          inner join acct.HFCVendors hv on HP.Vendorid = hv.Vendorid
                          inner join acct.FranchiseVendors fv on fv.vendorid = hp.vendorid and fv.vendortype = 1 and fv.vendorstatus = 1 and hp.Active=1
						  where isNULL(hv.IsActive,0)<>0
						  group by hp.ProductKey, hp.ProductID, hp.ProductName, hp.PICProductID, hp.VendorID, hv.Name , hp.VendorProductSKU, hp.Description, hp.ProductCategory, hp.ProductCollection,
                          hp.ProductModelID,  hp.ProductGroup, hp.ProductGroupDesc,hv.PICVendorId,hp.DiscontinuedDate,hp.PhasedoutDate order by VendorName,hp.ProductGroupDesc,hp.ProductName ASC;";

                    var result = connection.Query<HFCProducts>(query).ToList();
                    var pvpListDynamic = new List<dynamic>();

                    var vendorsproductGroup = result.Select(x => new { x.VendorID, x.ProductGroup, x.VendorName, x.ProductGroupDesc, x.PICVendorId }).Distinct().ToList();

                    foreach (var ven in vendorsproductGroup)
                    {
                        dynamic DyObj = new ExpandoObject();

                        DyObj.PICVendorId = ven.PICVendorId;
                        DyObj.VendorId = ven.VendorID;
                        DyObj.VendorName = ven.VendorName;
                        DyObj.PICProductGroupId = ven.ProductGroup;
                        DyObj.PICProductGroupName = ven.ProductGroupDesc;
                        var dyprdlist = new List<dynamic>();

                        foreach (var item in result)
                        {
                            if (item.VendorID == ven.VendorID && item.ProductGroup == ven.ProductGroup)
                            {
                                dynamic DypdObj = new ExpandoObject();
                                DypdObj.ProductName = item.ProductName;
                                DypdObj.PICProductId = item.PICProductID;
                                DypdObj.ProductId = item.ProductID;
                                DypdObj.PhasedoutDate = item.PhasedoutDate;
                                DypdObj.DiscontinuedDate = item.DiscontinuedDate;

                                if (!item.DiscontinuedDate.HasValue)
                                {
                                    DypdObj.IsActive = true;
                                }
                                else if ((item.DiscontinuedDate.HasValue && Convert.ToDateTime(item.DiscontinuedDate) > DateTime.Now))
                                {
                                    DypdObj.IsActive = true;
                                    DypdObj.Discontinued = "Product will be Discontinued from " + Convert.ToDateTime(item.DiscontinuedDate).GlobalDateFormat();
                                }
                                else if ((item.DiscontinuedDate.HasValue && Convert.ToDateTime(item.DiscontinuedDate) < DateTime.Now))
                                {
                                    DypdObj.IsActive = false;
                                    DypdObj.Discontinued = "Product is Discontinued on " + Convert.ToDateTime(item.DiscontinuedDate).GlobalDateFormat();
                                }
                                if (item.PhasedoutDate.HasValue)
                                {
                                    DypdObj.WarningPhasingOut = "Product is Phasing out from" + Convert.ToDateTime(item.PhasedoutDate.Value).GlobalDateFormat();
                                }
                                else
                                {
                                    DypdObj.WarningPhasingOut = "";
                                }
                                dyprdlist.Add(DypdObj);
                            }
                        }

                        DyObj.PICProductList = dyprdlist;
                        pvpListDynamic.Add(DyObj);
                    }
                    return pvpListDynamic;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public dynamic GetPICProductGroupForConfigurator(string franchiseCode, bool vendorconfig)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                try
                {
                    var query = "";
                    var result = new List<HFCProducts>();
                    if (!vendorconfig)
                    {
                        query = @"SELECT hp.ProductKey, hp.ProductID, hp.ProductName, hp.PICProductID, hp.VendorID, hv.Name as VendorName, hp.VendorProductSKU, hp.Description, hp.ProductCategory, hp.ProductCollection,
                          hp.ProductModelID, hp.ProductGroup, hp.ProductGroupDesc,hv.PICVendorId,hp.DiscontinuedDate,hp.PhasedoutDate
                          FROM crm.HFCProducts AS hp
                          inner join acct.HFCVendors hv on HP.Vendorid = hv.Vendorid
                          inner join acct.FranchiseVendors fv on fv.vendorid = hp.vendorid and fv.vendortype = 1 and fv.vendorstatus = 1 and hp.Active=1
						  INNER JOIN crm.Franchise f ON f.FranchiseId = fv.FranchiseId
						  where  f.Code = @franchiseCode AND isNULL(hv.IsActive,0)<>0
						  group by hp.ProductKey, hp.ProductID, hp.ProductName, hp.PICProductID, hp.VendorID, hv.Name , hp.VendorProductSKU, hp.Description, hp.ProductCategory, hp.ProductCollection,
                          hp.ProductModelID,  hp.ProductGroup, hp.ProductGroupDesc,hv.PICVendorId,hp.DiscontinuedDate,hp.PhasedoutDate order by VendorName,hp.ProductGroupDesc,hp.ProductName ASC;";

                        result = connection.Query<HFCProducts>(query,
                      new { franchiseCode = franchiseCode }).ToList();
                    }
                    else
                    {
                        query = @"SELECT hp.ProductKey, hp.ProductID, hp.ProductName, hp.PICProductID, hp.VendorID, hv.Name as VendorName, hp.VendorProductSKU, hp.Description, hp.ProductCategory, hp.ProductCollection,
                          hp.ProductModelID, hp.ProductGroup, hp.ProductGroupDesc,hv.PICVendorId,hp.DiscontinuedDate,hp.PhasedoutDate
                          FROM crm.HFCProducts AS hp
                          inner join acct.HFCVendors hv on HP.Vendorid = hv.Vendorid
                          inner join acct.FranchiseVendors fv on fv.vendorid = hp.vendorid and fv.vendortype = 1 and fv.vendorstatus = 1 and hp.Active=1
						  INNER JOIN crm.Franchise f ON f.FranchiseId = fv.FranchiseId
						  where  f.Code = @franchiseCode AND isNULL(hv.IsActive,0)<>0 and
                          hv.VendorId = (select VendorId from Acct.HFCVendors where VendorIdPk in (select VendorId from CRM.VendorCaseConfig where CaseLogin like @CaseLogin))
						  group by hp.ProductKey, hp.ProductID, hp.ProductName, hp.PICProductID, hp.VendorID, hv.Name , hp.VendorProductSKU, hp.Description, hp.ProductCategory, hp.ProductCollection,
                          hp.ProductModelID,  hp.ProductGroup, hp.ProductGroupDesc,hv.PICVendorId,hp.DiscontinuedDate,hp.PhasedoutDate order by VendorName,hp.ProductGroupDesc,hp.ProductName ASC;";

                        result = connection.Query<HFCProducts>(query,
                      new { franchiseCode = franchiseCode, CaseLogin = SessionManager.CurrentUser.UserName }).ToList();
                    }

                    var pvpListDynamic = new List<dynamic>();

                    var vendorsproductGroup = result.Select(x => new { x.VendorID, x.ProductGroup, x.VendorName, x.ProductGroupDesc, x.PICVendorId }).Distinct().ToList();

                    foreach (var ven in vendorsproductGroup)
                    {
                        dynamic DyObj = new ExpandoObject();

                        DyObj.PICVendorId = ven.PICVendorId;
                        DyObj.VendorId = ven.VendorID;
                        DyObj.VendorName = ven.VendorName;
                        DyObj.PICProductGroupId = ven.ProductGroup;
                        DyObj.PICProductGroupName = ven.ProductGroupDesc;
                        var dyprdlist = new List<dynamic>();

                        foreach (var item in result)
                        {
                            if (item.VendorID == ven.VendorID && item.ProductGroup == ven.ProductGroup)
                            {
                                dynamic DypdObj = new ExpandoObject();
                                DypdObj.ProductName = item.ProductName;
                                DypdObj.PICProductId = item.PICProductID;
                                DypdObj.ProductId = item.ProductID;
                                DypdObj.PhasedoutDate = item.PhasedoutDate;
                                DypdObj.DiscontinuedDate = item.DiscontinuedDate;

                                if (!item.DiscontinuedDate.HasValue)
                                {
                                    DypdObj.IsActive = true;
                                }
                                else if ((item.DiscontinuedDate.HasValue && Convert.ToDateTime(item.DiscontinuedDate) > DateTime.Now))
                                {
                                    DypdObj.IsActive = true;
                                    DypdObj.Discontinued = "Product will be Discontinued from " + Convert.ToDateTime(item.DiscontinuedDate).GlobalDateFormat();
                                }
                                else if ((item.DiscontinuedDate.HasValue && Convert.ToDateTime(item.DiscontinuedDate) < DateTime.Now))
                                {
                                    DypdObj.IsActive = false;
                                    DypdObj.Discontinued = "Product is Discontinued on " + Convert.ToDateTime(item.DiscontinuedDate).GlobalDateFormat();
                                }
                                if (item.PhasedoutDate.HasValue)
                                {
                                    DypdObj.WarningPhasingOut = "Product is Phasing out from" + Convert.ToDateTime(item.PhasedoutDate.Value).GlobalDateFormat();
                                }
                                else
                                {
                                    DypdObj.WarningPhasingOut = "";
                                }
                                dyprdlist.Add(DypdObj);
                            }
                        }

                        DyObj.PICProductList = dyprdlist;
                        pvpListDynamic.Add(DyObj);
                    }
                    return pvpListDynamic;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
    }
}