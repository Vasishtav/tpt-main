﻿using Dapper;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Managers
{
   public class StatusChangesManager : DataManager
    {
        public StatusChangesManager(Franchise franchise, User user)
        {
            Franchise = franchise;
            AuthorizingUser = user;
        }

        public bool StatusChanges(int masterId,int masterTypeId,int destinationStatusId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                if (masterId == 1)
                {
                    var query = "select l.LeadStatusId from CRM.Leads l where leadId = @leadid";
                    var lead = connection.Query<Lead>(query, new { leadid = masterTypeId }).FirstOrDefault();
                    if (lead == null)
                    {
                        return true;
                    }
                    else
                    {
                        var query1 = "select count(*) from CRM.Type_StatusChanges where (MasterStatusTypeId = @masterStatusId and SourceStatusId =@sourceStatusId) and DestinationStatusId=@destinationStatusId";
                        var result = connection.Query<int>(query1, new { masterStatusId = masterId, sourceStatusId = lead.LeadStatusId, destinationStatusId = destinationStatusId }).FirstOrDefault();
                        if (result <= 0)
                        {
                            return false;
                        }
                        else if (result >= 1)
                            return true;
                    }
                }
                if (masterId == 2)
                {
                    var query = "select a.AccountStatusId from CRM.Accounts a where AccountId = @accountId";
                    var account = connection.Query<AccountTP>(query, new { accountId = masterTypeId }).FirstOrDefault();
                    if (account == null)
                    {
                        return true;
                    }
                    else
                    {
                        var query1 = "select count(*) from CRM.Type_StatusChanges where (MasterStatusTypeId = @masterStatusId and SourceStatusId =@sourceStatusId) and DestinationStatusId=@destinationStatusId";
                        var result = connection.Query<int>(query1, new { masterStatusId = masterId, sourceStatusId = account.AccountStatusId, destinationStatusId = destinationStatusId }).FirstOrDefault();
                        if (result <= 0)
                        {
                            return false;
                        }
                        else if (result >= 1)
                            return true;
                    }
                }
                if (masterId == 3)
                {
                    var query = "select o.OpportunityStatusId from CRM.Opportunities o where OpportunityId = @opportunityId";
                    var opportunity = connection.Query<Opportunity>(query, new { opportunityId = masterTypeId }).FirstOrDefault();
                    if (opportunity == null)
                    {
                        return true;
                    }
                    else
                    {
                        var query1 = "select count(*) from CRM.Type_StatusChanges where (MasterStatusTypeId = @masterStatusId and SourceStatusId =@sourceStatusId) and DestinationStatusId=@destinationStatusId";
                        var result = connection.Query<int>(query1, new { masterStatusId = masterId, sourceStatusId = opportunity.OpportunityStatusId, destinationStatusId = destinationStatusId }).FirstOrDefault();
                        if (result <= 0)
                        {
                            return false;
                        }
                        else if (result >= 1)
                            return true;
                    }
                }
                connection.Close();
            }
            return false;
        }
        public List<int> ValidStatus(int masterStatusId,int selectedStatusId)
        {
            if(selectedStatusId ==0)
                selectedStatusId = 1;
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = "Select t.DestinationStatusId From CRM.Type_StatusChanges t where SourceStatusId = @sourceStatusId and MasterStatusTypeId = @masterStatusId";
                var result = connection.Query<int>(query, new { sourceStatusId= selectedStatusId, masterStatusId=masterStatusId }).ToList();
                if (result != null)
                    return result;
                connection.Close();
            }
            return null;
        }
    }
}
