﻿using Dapper;
using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Managers
{
    public class TaskEventsManager : DataManager
    {
        public TaskEventsManager(User user, Franchise franchise)
        {
            this.User = user;
            this.Franchise = franchise;
        }

        public List<Leads> GetLeadsList()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                string leadsquery = @"SELECT lead.[LeadId] as LeadId,
                                     (select [CRM].[fnGetAccountNameByLeadId](lead.LeadId)) as FullName,
                                     case when lead.[LeadId] is not null 
                                    		 then 
                                    		(select 
                                    		case when PreferredTFN='C' then CellPhone 
                                    		else case when PreferredTFN='H' then HomePhone 
                                    		else case when PreferredTFN='W' then WorkPhone 
                                    		else case when CellPhone is not null then CellPhone 
                                    		else case when HomePhone is not null then HomePhone 
                                    		else case when WorkPhone is not null then WorkPhone 
                                    		else '' 
                                    		end end end end end end
                                    		from CRM.Customer cus
                                    		join CRM.LeadCustomers lcus on cus.CustomerId=lcus.CustomerId and IsPrimaryCustomer=1
                                    		where lcus.LeadId=lead.[LeadId])
                                    		end as [PhoneNumber],
                                            lead.IsNotifyemails
                                     FROM crm.Leads lead 
                                     INNER JOIN CRM.Type_LeadStatus tls ON tls.id = lead.leadstatusid
                                     INNER JOIN [CRM].[Addresses] a on a.[AddressId]=(select top 1 la.AddressId from crm.LeadAddresses la where la.LeadId = lead.LeadId)
                                     JOIN CRM.LeadCustomers lcus on lead.LeadId=lcus.LeadId and IsPrimaryCustomer=1
                                     INNER JOIN [CRM].[Customer] p on p.CustomerId=lcus.CustomerId
                                     WHERE lead.LeadStatusId<>31422 AND lead.LeadStatusId <> 13 AND lead.LeadStatusId <> 2 AND lead.FranchiseId= @FranchiseId AND ISNULL(lead.IsDeleted,0)=0
                                     order by lead.[CreatedOnUtc] desc, [LastUpdatedOnUtc] desc";
                var result = connection.Query<Leads>(leadsquery, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                connection.Close();
                return result;
            }
        }

        public List<Accounts> GetAccountList()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                string accountquery = @"SELECT
                                Account.[AccountId],
                               (select [CRM].[fnGetAccountNameByAccountId](Account.AccountId)) as FullName,
			                    case when Account.[AccountId] is not null 
		                       then 
		                       (select 
		                       case when PreferredTFN='C' then CellPhone
		                       else case when PreferredTFN='H' then HomePhone 
		                       else case when PreferredTFN='W' then WorkPhone
		                       else case when CellPhone is not null then CellPhone 
		                       else case when HomePhone is not null then HomePhone 
		                       else case when WorkPhone is not null then WorkPhone 
		                       else '' 
		                       end end end end end end
		                       from CRM.Customer cus
		                       join CRM.AccountCustomers acus on cus.CustomerId=acus.CustomerId and IsPrimaryCustomer=1
		                       where acus.AccountId=Account.[AccountId]) end as PhoneNumber,
                               Account.IsNotifyemails
                                FROM[crm].[Accounts] Account
                                JOIN CRM.AccountCustomers acccus on Account.AccountId=acccus.AccountId and IsPrimaryCustomer=1
                                INNER JOIN[CRM].[Customer] p on p.CustomerId=acccus.CustomerId
                                WHERE Account.FranchiseId= @FranchiseId AND ISNULL(Account.IsDeleted,0)=0
                                 order by isnull(Account.LastUpdatedOnUtc, Account.CreatedOnUtc) desc";
                var result = connection.Query<Accounts>(accountquery, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                connection.Close();
                return result;
            }
        }

        public List<Opportunities> GetOpportunityList()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                string opportunityquery = @"select  op.OpportunityId,
                                                op.OpportunityName as FullName
                                                from [CRM].[Opportunities]  op
                                                WHERE op.FranchiseId= @FranchiseId
                                                order by op.OpportunityId desc";
                var result = connection.Query<Opportunities>(opportunityquery, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                connection.Close();
                return result;
            }
        }

        public List<Orderss> GetOrderList()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                string orderquery = @"select OrderId, OrderName as FullName
                                from [CRM].[Orders] o
                                join [CRM].[Opportunities] op on o.OpportunityId=op.OpportunityId
                                left join [CRM].[Person] p on op.SalesAgentId=p.PersonId
                                left join [CRM].[Accounts] ac on op.AccountId=ac.AccountId
                                left join [CRM].[Customer] cu on ac.PersonId=cu.CustomerId
                                left join [CRM].[Type_OrderStatus] os on o.OrderStatus=os.OrderStatusId
                                where 
                                op.FranchiseId=@FranchiseId and o.OrderStatus not in (6,9)";
                var result = connection.Query<Orderss>(orderquery, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                connection.Close();
                return result;
            }
        }

        public List<Opportunities> GetOpportunityListByAccount(int Id = 0)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                string opportunityByAccountsquery = @"select  op.OpportunityId,
                                            op.OpportunityName as FullName
                                            from [CRM].[Opportunities]  op
                                            WHERE op.FranchiseId= @FranchiseId and op.AccountId=@AccountId
                                            order by op.OpportunityId desc";
                var result = connection.Query<Opportunities>(opportunityByAccountsquery, new { AccountId = Id, FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                connection.Close();
                return result;
            }
        }

        public List<Orderss> GetOrderListByOpportunity(int Id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                string orderByOpportunity = @"select OrderId, OrderName as FullName from CRM.Orders WHERE OpportunityId=@OpportunityId";
                var result = connection.Query<Orderss>(orderByOpportunity, new { OpportunityId = Id, FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                connection.Close();
                return result;
            }
        }

        public List<Orderss> GetOrderListByAccount(int Id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                string orderByAccountsquery = @"select OrderID, OrderName as FullName from CRM.Orders o
                                                join CRM.Opportunities op on o.OpportunityId=op.OpportunityId
                                                where FranchiseId=@FranchiseId and  AccountId=@AccountId";
                connection.Open();
                var result = connection.Query<Orderss>(orderByAccountsquery, new { AccountId = Id, FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                connection.Close();
                return result;
            }
        }

        public dynamic GetCaseList(int Id, string type)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var Query = @"select distinct FC.CaseId,FC.CaseNumber
	                                                from [CRM].[FranchiseCase] FC with (nolock)
	                                                Cross apply
	                                                (select * from CRM.CSVToTable (SalesOrderId)) FranchiseSalesOrder
	                                                Inner join CRM.Orders O with (nolock) on O.OrderId = FranchiseSalesOrder.id
	                                                inner join CRM.Quote q on q.QuoteKey = o.QuoteKey
                                                    inner join CRM.Opportunities opp on opp.OpportunityId = q.OpportunityId
	                                                where opp.FranchiseId = @FranchiseId ";

                connection.Open();
                if (type == "order")
                {
                    var result = connection.Query(Query + "and O.OrderId = @OrderId",
                            new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId, OrderId = Id }).ToList();
                    connection.Close();
                    return result;
                }
                else if (type == "opportunity")
                {
                    var result = connection.Query(Query + "and opp.OpportunityId = @OpportunityId",
                            new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId, OpportunityId = Id }).ToList();
                    connection.Close();
                    return result;
                }
                else if (type == "account")
                {
                    var result = connection.Query(Query + "and opp.AccountId = @AccountId ",
                                                new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId, AccountId = Id }).ToList();
                    connection.Close();
                    return result;
                }
                else
                {
                    var result = connection.Query(Query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                    connection.Close();
                    return result;
                }
            }
        }

        public dynamic GetVendorCaseList(int Id, string type)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var Query = @"select distinct vc.VendorCaseId as CaseId,vc.VendorCaseNumber as CaseNumber
	                                        from [CRM].[FranchiseCase] FC with (nolock)
	                                        Cross apply
	                                        (select * from CRM.CSVToTable (SalesOrderId)) FranchiseSalesOrder
	                                        Inner join CRM.VendorCase vc on vc.CaseId = FC.CaseId
	                                        Inner join CRM.Orders O with (nolock) on O.OrderId = FranchiseSalesOrder.id
	                                        inner join CRM.Quote q on q.QuoteKey = o.QuoteKey
                                            inner join CRM.Opportunities opp on opp.OpportunityId = q.OpportunityId
	                                        where opp.FranchiseId = @FranchiseId ";
                connection.Open();
                if (type == "order")
                {
                    var result = connection.Query(Query + "and O.OrderId = @OrderId ",
                                                    new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId, OrderId = Id }).ToList();
                    connection.Close();
                    return result;
                }
                else if (type == "opportunity")
                {
                    var result = connection.Query(Query + "and opp.OpportunityId = @OpportunityId"
                                                    , new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId, OpportunityId = Id }).ToList();
                    connection.Close();
                    return result;
                }
                else if (type == "account")
                {
                    var result = connection.Query(Query + "and opp.AccountId = @AccountId",
                                         new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId, AccountId = Id }).ToList();
                    connection.Close();
                    return result;
                }
                else if (type == "case")
                {
                    var result = connection.Query(Query + "and fc.caseId= @CaseId ",
                                                    new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId, CaseId = Id }).ToList();
                    connection.Close();
                    return result;
                }
                else
                {
                    var result = connection.Query(Query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                    connection.Close();
                    return result;
                }
            }
        }


        public class Leads
        {
            public string FullName { get; set; }
            public int LeadId { get; set; }
            public string PhoneNumber { get; set; }
            public bool IsNotifyemails { get; set; }
        }

        public class Accounts
        {
            public string FullName { get; set; }
            public int AccountId { get; set; }
            public string PhoneNumber { get; set; }
            public bool IsNotifyemails { get; set; }
        }

        public class Opportunities
        {
            public string FullName { get; set; }
            public int OpportunityId { get; set; }
        }

        public class Orderss
        {
            public string FullName { get; set; }
            public int OrderId { get; set; }
        }
    }
}
