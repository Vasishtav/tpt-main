﻿using System;
using System.Configuration;
using System.Web.Profile;
using HFC.CRM.Core.Common;
using Intuit.Ipp.Core;
using Intuit.Ipp.Security;

namespace HFC.CRM.Managers.QBSync
{
    public class RestProfile : ProfileBase
    {
        private static string UserName;

        public static void SetUser(string user)
        {
            UserName = user;
        }
        public RestProfile() { }

        public static RestProfile GetRestProfile(string username)
        {
            return Create(username) as RestProfile;
        }

        //public static RestProfile GetRestProfile()
        //{
        //    //var user = ContextFactory.Current.Context.Users.FirstOrDefault(x => x.UserName == 
        //    //System.Security.Principal.IPrin User.Identity.Name);
            
        //    return Create(Membership.GetUser().UserName) as RestProfile;
        //}

        [SettingsAllowAnonymous(false)]
        public string RealmId
        {
            get { return base["RealmId"] as string; }
            set { base["RealmId"] = value; }
        }

        [SettingsAllowAnonymous(false)]
        public string OAuthAccessToken
        {
            get { return base["OAuthAccessToken"] as string; }
            set { base["OAuthAccessToken"] = value; }
        }

        [SettingsAllowAnonymous(false)]
        public string OAuthAccessTokenSecret
        {
            get { return base["OAuthAccessTokenSecret"] as string; }
            set { base["OAuthAccessTokenSecret"] = value; }
        }

        [SettingsAllowAnonymous(false)]
        public int DataSource
        {
            get { object dataSource = base["DataSource"]; if (!dataSource.Equals(null)) { return (int)dataSource; } else { return -1; } }
            set { base["DataSource"] = value; }
        }

        //public static ServiceContext GetServiceContext()
        //{
        //    var profile = GetRestProfile();
        //    var consumerKey = ConfigurationManager.AppSettings["consumerKey"].ToString();
        //    var consumerSecret = ConfigurationManager.AppSettings["consumerSecret"].ToString();
        //    OAuthRequestValidator oauthValidator = new OAuthRequestValidator(profile.OAuthAccessToken, profile.OAuthAccessTokenSecret, consumerKey, consumerSecret);
        //    //var svc = new ServiceContext(profile.RealmId, (IntuitServicesType)profile.DataSource, oauthValidator);
        //    var svc = new ServiceContext(profile.RealmId, (IntuitServicesType)profile.DataSource, oauthValidator);
        //    svc.IppConfiguration.BaseUrl.Qbo = ConfigurationManager.AppSettings["ServiceContext.BaseUrl.Qbo"];
        //    svc.IppConfiguration.MinorVersion.Qbo = "4";
        //    return svc;
        //}

        //public static ServiceContext GetServiceContext(string consumerKey, string consumerSecret, string username)
        //{
        //    var profile = GetRestProfile(username);
        //    if (String.IsNullOrEmpty(profile.OAuthAccessToken))
        //    {
        //        throw new QBSyncValidationException("Neet to authorize to QBO!");
        //    }
        //    OAuthRequestValidator oauthValidator = new OAuthRequestValidator(profile.OAuthAccessToken, profile.OAuthAccessTokenSecret, consumerKey, consumerSecret);
        //    //var svc = new ServiceContext(profile.RealmId, (IntuitServicesType)profile.DataSource, oauthValidator);
        //    var svc = new ServiceContext(profile.RealmId, (IntuitServicesType)profile.DataSource, oauthValidator);
        //    svc.IppConfiguration.BaseUrl.Qbo = ConfigurationManager.AppSettings["ServiceContext.BaseUrl.Qbo"];
        //    svc.IppConfiguration.MinorVersion.Qbo = "4";
        //    return svc;
        //}


        public static ServiceContext GetServiceContext(string consumerKey, string consumerSecret, string username, Profile profile)
        {
            if (String.IsNullOrEmpty(profile.OAuthAccessToken))
            {
                throw new QBSyncValidationException("Neet to authorize to QBO!");
            }
            OAuthRequestValidator oauthValidator = new OAuthRequestValidator(profile.OAuthAccessToken, profile.OAuthAccessTokenSecret, consumerKey, consumerSecret);
            //var svc = new ServiceContext(profile.RealmId, (IntuitServicesType)profile.DataSource, oauthValidator);
            var svc = new ServiceContext(profile.RealmId, (IntuitServicesType)profile.DataSource, oauthValidator);
            svc.IppConfiguration.BaseUrl.Qbo = ConfigurationManager.AppSettings["ServiceContext.BaseUrl.Qbo"];
            svc.IppConfiguration.MinorVersion.Qbo = "4";
            return svc;
        }




    }
}