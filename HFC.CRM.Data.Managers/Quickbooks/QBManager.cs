﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HFC.CRM.Data;
using HFC.CRM.Core;
using HFC.CRM.Core.Membership;
using HFC.CRM.Data.Constants;
using HFC.CRM.Managers;
using HFC.CRM.Data.Context;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using System.IO;
using System.Configuration;
using System.Data.SqlClient;
using Dapper;

namespace HFC.CRM.Managers.Quickbooks
{
    /// <summary>
    /// Quickbooks manager to handle business logic associated with quickbooks
    /// </summary>
    public class QBManager : DataManager
    {

        private string hostDomain = ConfigurationManager.AppSettings["hostDomain"];
        private bool isBB { get { return hostDomain.ToLower() == "budgetblinds"; } }

        public static bool HaveAnyWork(QBSession session)
        {
            //check estimates
            //check customer invoice
            //check customer sourcePayments
            //check purchase orders
            //check etc.. 

            return true;
        }

        // TODO: is this required in TP???
        /// <summary>
        /// Permission for creating Quickbooks WebConnector .qwc file
        /// </summary>
        //public Permission QWCPermission
        //{
        //    get { return PermissionProvider.GetPermission(AuthorizingUser, "Quickbooks", "WebConnector"); }
        //}

        // TODO: is this required in TP???
        /// <summary>
        /// Permission for viewing Quickbooks error/status log
        /// </summary>
        //public Permission SupportPermission
        //{
        //    get { return PermissionProvider.GetPermission(AuthorizingUser, "Quickbooks", "Support"); }
        //}


        // TODO: is this required in TP???
        /// <summary>
        /// Permission for viewing Quickbooks sync Queue
        /// </summary>
        //public Permission QueuePermission
        //{
        //    get { return PermissionProvider.GetPermission(AuthorizingUser, "Quickbooks", "Queue"); }
        //}

        /// <summary>
        /// Create a new instance of QB manager 
        /// </summary>
        /// <param name="user">The user that is performing the CRUD operation and used to check authorization permission</param>
        public QBManager(User user, Franchise franchise)
            : this(user, user, franchise)
        {

        }
        public QBManager() { }
        /// <summary>
        /// Create a new instance of QB manager 
        /// </summary>
        /// <param name="authorizingUser">Used to check authorization permission</param>
        /// <param name="user">The user that is performing the CRUD operation</param>
        /// <exception cref="System.ArgumentOutOfRangeException">User must belong to a franchise</exception>
        public QBManager(User authorizingUser, User user, Franchise franchise)
        {
            this.User = user;
            this.AuthorizingUser = authorizingUser;
            this.Franchise = franchise;
        }

        /// <summary>
        /// QBD authendication using QBApp table
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="ownerId"></param>
        /// <returns></returns>
        public QBApps IsValidQuickBooksLogin(string username, string password, string ownerId)
        {

            try
            {
                var app = ExecuteIEnumerableObjectSingle<QBApps>(ContextFactory.CrmConnectionString, "where OwnerID=@OwnerID and UserName=@UserName", new { OwnerID = ownerId, UserName = username }).FirstOrDefault();

                if (app == null)
                    return null; // Username & password was not found.

                // TODO: Refactor this to use one-way crypts!
                if (new Crypto().Decrypt(app.Password) == password)
                    return app; // Password matched.

                return null; // Password was incorrect.
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return null;
            }
        }

        /// <summary>
        /// Get QBApp info based on owner id
        /// </summary>
        /// <param name="OwnerID"></param>
        /// <returns></returns>
        public QBApps GetApp(string OwnerID)
        {
            try
            {
                var app = ExecuteIEnumerableObjectSingle<QBApps>(ContextFactory.CrmConnectionString, "where OwnerID=@OwnerID", new { OwnerID = OwnerID }).FirstOrDefault();
                return app;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return null;
            }

        }

        /// <summary>
        /// get QBApp info based on franchiseid
        /// </summary>
        /// <param name="feId"></param>
        /// <returns></returns>
        public QBApps GetQBApp(int feId)
        {
            try
            {
                var app = ExecuteIEnumerableObjectSingle<QBApps>(ContextFactory.CrmConnectionString, "where FranchiseId=@FranchiseId", new { FranchiseId = feId }).FirstOrDefault();

                if (app != null)
                {
                    if (!string.IsNullOrEmpty(app.Password))
                    {
                        app.Password = new Crypto().Decrypt(app.Password);
                    }
                    return app;
                }
                else
                    return null;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Franchise GetFranchise(int feId)
        {
            return GetData<Franchise>(ContextFactory.CrmConnectionString, feId);
        }

        public AddressTP GetAddress(int AddressId)
        {
            return GetData<AddressTP>(ContextFactory.CrmConnectionString, AddressId);
        }

        /// <summary>
        /// Insert / Update QB setup information
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public QBApps SaveQBApp(QBApps app)
        {
            try
            {
                var item = ExecuteIEnumerableObjectSingle<QBApps>(ContextFactory.CrmConnectionString, "where FranchiseId=@FranchiseId", new { FranchiseId = app.FranchiseId }).FirstOrDefault();
                var crypto = new Crypto();

                if (item != null)
                {
                    item.UserName = app.UserName;
                    item.Password = crypto.Encrypt(app.Password);
                    item.SynchInvoices = app.SynchInvoices;
                    item.SynchCost = app.SynchCost;
                    item.SynchPayments = app.SynchPayments;
                    item.IsLastNameFirst = app.IsLastNameFirst;
                    item.SuppressSalesTax = app.SuppressSalesTax;
                    item.CreatedOn = (DateTime)app.CreatedOn;
                    item.TaxDetailjurisdictions = app.TaxDetailjurisdictions;
                    item.AR = app.AR;
                    item.Sales = app.Sales;
                    item.COG = app.COG;

                    Update<QBApps>(ContextFactory.CrmConnectionString, item);
                }
                else
                {
                    var qbapp = new QBApps
                    {
                        AppGuid = Guid.NewGuid(),
                        FranchiseId = app.FranchiseId,
                        OwnerID = Guid.NewGuid().ToString(),
                        UserName = app.UserName,
                        Password = crypto.Encrypt(app.Password),
                        SynchInvoices = app.SynchInvoices,
                        SynchPayments = app.SynchPayments,
                        SynchCost = app.SynchCost,
                        SuppressSalesTax = app.SuppressSalesTax,
                        TaxDetailjurisdictions = app.TaxDetailjurisdictions,
                        AR = app.AR,
                        Sales = app.Sales,
                        COG = app.COG
                    };
                    Insert<QBApps>(ContextFactory.CrmConnectionString, qbapp);
                }
                return app;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateTaxCode(int FranchiseId, string TaxCode)
        {
            try
            {
                var item = ExecuteIEnumerableObjectSingle<QBApps>(ContextFactory.CrmConnectionString, "where FranchiseId=@FranchiseId", new { FranchiseId }).FirstOrDefault();
                if (item != null)
                {
                    item.TaxCode = TaxCode;
                    Update<QBApps>(ContextFactory.CrmConnectionString, item);
                }
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateTaxName(List<TaxRateMapping> TaxRateMapping)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    string qupdate = @"update QB.TaxRateMapping set QBOTaxName=@QBOTaxName 
                                where FranchiseId=@FranchiseId and TPTTaxName=@TPTTaxName";

                    foreach (var tm in TaxRateMapping)
                    {
                        connection.Execute(qupdate, new { tm.QBOTaxName, tm.FranchiseId, tm.TPTTaxName });
                    }

                }
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<TaxRateMapping> GetTaxName(int FranchiseId)
        {
            try
            {
                List<TaxRateMapping> TaxRateMapping = new List<Data.TaxRateMapping>();
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    string qselect = "select * from QB.TaxRateMapping where FranchiseId=@FranchiseId";

                    string qgettaxname = @"select DISTINCT td.TaxName from CRM.Opportunities op
                                           join CRM.Franchise f on f.FranchiseId=op.FranchiseId
                                           join CRM.Addresses ad on f.AddressId=ad.AddressId
                                           join CRM.Quote q on op.OpportunityId=q.OpportunityId
                                           join CRM.QuoteLines ql on q.QuoteKey=ql.QuoteKey
                                           join CRM.Tax t on ql.QuoteLineId=t.QuoteLineId
                                           join CRM.TaxDetails td on t.TaxId=td.TaxId
                                           where op.FranchiseId=@FranchiseId and ad.CountryCode2Digits='CA' and td.TaxName is not null";

                    string qInsertTaxName = "insert into QB.TaxRateMapping values (@FranchiseId,@TaxName,'')";

                    var preselectlst = connection.Query<TaxRateMapping>(qselect, new { FranchiseId }).ToList();

                    var taxnamelst = connection.Query<string>(qgettaxname, new { FranchiseId }).ToList();

                    if (preselectlst.Count == 0 && taxnamelst.Count > 0)
                    {
                        // In DB no records insert taxname
                        foreach (var tn in taxnamelst)
                        {
                            if (tn != null && tn != "")
                                connection.Execute(qInsertTaxName, new { FranchiseId, TaxName = tn });
                        }
                        return connection.Query<TaxRateMapping>(qselect, new { FranchiseId }).ToList();
                    }
                    else
                    {
                        // check if new taxname in tax details and insert
                        if (preselectlst.Count > 0 && taxnamelst.Count > 0)
                        {
                            var dbtaxnamelst = preselectlst.Select(x => x.TPTTaxName).ToList();
                            var newtaxnamelst = taxnamelst.Except(dbtaxnamelst);
                            foreach (var tn in newtaxnamelst)
                            {
                                if (tn != null && tn != "")
                                    connection.Execute(qInsertTaxName, new { FranchiseId, TaxName = tn });
                            }
                        }

                        return connection.Query<TaxRateMapping>(qselect, new { FranchiseId }).ToList();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        //public void SaveQBResponse(QBResponse response)
        //{
        //    if (response.Trans.Count == 0) return;
        //    var feID = CRMDBContext.QBApps.Where(x => x.OwnerID == response.Credential.OwnerID).FirstOrDefault().FranchiseId;
        //    foreach (var x in response.Trans)
        //    {
        //        CRMDBContext.SynchLogs.Add(new SynchLog
        //        {
        //            FranchiseId = feID,
        //            Message = x.StatusMessage,
        //            ObjectID = x.EntityID,
        //            ObjectType = (int)x.EntityType,
        //            ObjectRef = x.EntityRef,
        //            Status = int.Parse(x.StatusCode),
        //            SynchOn = x.SyncDate
        //        });
        //    }
        //    this.CRMDBContext.SaveChanges();
        //}

        /// <summary>
        /// Generates a new QB WebConnector .qwc file
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <returns>A memory stream containing an XML file that is consumed by QuickBooks Web Connector.</returns>
        public MemoryStream CreateQuickBooksWebConnectorFile(int franchiseId)
        {
            string serverAddress = Core.Common.Configuration.QuickBooksDesktopSyncUrl;
            var app = ExecuteIEnumerableObjectSingle<QBApps>(ContextFactory.CrmConnectionString, "where FranchiseId=@FranchiseId", new { FranchiseId = franchiseId }).FirstOrDefault();
            var franchise = ExecuteIEnumerableObjectSingle<Franchise>(ContextFactory.CrmConnectionString, "where FranchiseId=@FranchiseId", new { FranchiseId = franchiseId }).FirstOrDefault();

            if (app == null || franchise == null)
                return null;

            string statusPage = string.Format(Core.Common.Configuration.SynchronizationStatusUrl, app.OwnerID);

            string qwc = string.Format(
                @"<?xml version=""1.0""?>
                <QBWCXML>
                <AppName>{4}</AppName>
                <AppID></AppID>
                <AppURL>{0}/Svcs/HFCQuickBooksDesktopSync.asmx</AppURL>
                <AppDescription>QuickBooks Desktop Sync for {4}</AppDescription>
                <AppSupport>{5}</AppSupport>
                <UserName>{1}</UserName>
                <OwnerID>{{{2}}}</OwnerID>
                <FileID>{{{3}}}</FileID>
                <QBType>QBFS</QBType>
                <Style>Document</Style>
                <AuthFlags>0xF</AuthFlags>
                </QBWCXML>"
                , serverAddress, app.UserName, app.OwnerID, app.AppGuid, franchise.Name, statusPage);

            byte[] byteArray = Encoding.ASCII.GetBytes(qwc);
            MemoryStream stream = new MemoryStream(byteArray);

            return stream;
        }

        //private bool IsValidatedQBUser(string hash, string password)
        //{
        //    var crypter = new Crypto(Crypto.CryptoTypes.encTypeTripleDES);
        //    var _encrypt = crypter.Encrypt(password);
        //    if (_encrypt.Equals(hash))
        //    {
        //        return true;
        //    }
        //    else return false;
        //}

        public bool UpdateQBOSessioninfo(int FranchiseId, string info)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    string qupdate = @"update QB.Apps set AppToken=@info where FranchiseId=@FranchiseId";
                    connection.Execute(qupdate, new { info, FranchiseId });
                }
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<SynchLog> GetSyncLog(string OwnerId)
        {
            return ExecuteIEnumerableObjectSingle<SynchLog>(ContextFactory.CrmConnectionString, "where OwnerId=@OwnerId order by ObjectType,ObjectID asc", new { OwnerId }).ToList();
        }
    }
}
