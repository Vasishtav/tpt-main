﻿using HFC.CRM.Core;
using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace HFC.CRM.Managers.Quickbooks
{
    using HFC.CRM.Data.Context;

    /// <summary>
    /// Logging helper for Quickbooks events
    /// </summary>
    public class Logger
    {
        /// <summary>
        /// Writes an entry into DB if logging is enabled
        /// </summary>        
        public static void WriteEntry(string message, Dictionary<string, string> parameters = null, string sessionId = null, string ipAddress = null)
        {
            if (!AppConfigManager.EnableQuickbooksLogging) return;
            ContextFactory.Current.Context.QBLogs.Add(new QBLog
                          {
                              Message = message,
                              Parameters = parameters != null ? string.Join("&", parameters.Select(s => string.Format("{0}={1}", s.Key, s.Value))) : null,
                              Severity = "Log",
                              IPAddress = ipAddress,
                              SessionId = sessionId
                          });
            ContextFactory.Current.Context.SaveChanges();
        }

        /// <summary>
        /// Writes an entry into DB if logging is enabled
        /// </summary>     
        public static void WriteEntry(Exception ex, string sessionId = null, string ipAddress = null)
        {
            if (!AppConfigManager.EnableQuickbooksLogging) return;
            ContextFactory.Current.Context.QBLogs.Add(new QBLog { Message = ex.Message, Trace = ex.StackTrace, Severity = "Error", IPAddress = ipAddress, SessionId = sessionId });
            ContextFactory.Current.Context.SaveChanges();
        }
    }
}
