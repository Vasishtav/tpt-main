﻿using HFC.CRM.Core;
using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Managers.Quickbooks
{
    /// <summary>
    /// Helper class to authenticate quickbook QB WebConnector apps
    /// </summary>
    public class Authenticator
    {
        /// <summary>
        /// Verifies user and password is valid
        /// </summary>
        /// <returns>A new or existing QBSession</returns>
        public static QBApp Authenticate(string username, string password, string ipaddress = null)
        {            
            using (var db = new CRMContextEx())
            {
                var app = db.QBApps.SingleOrDefault(s => s.UserName == username && s.IsActive == true);
                if (app != null)
                {
                    Crypto crypter = new Crypto(Crypto.CryptoTypes.encTypeTripleDES);
                    if (password == crypter.Decrypt(app.Password))                                           
                        return app;                    
                    else
                        throw new UnauthorizedAccessException("Invalid username or password");
                }
                else
                    throw new UnauthorizedAccessException("Invalid username or inactive account");
            }
        }
    }
}
