﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HFC.CRM.Data.Extensions.EnumAttr;

namespace HFC.CRM.Managers.Quickbooks
{
    public class QBResponse
    {
        public Credential Credential { get; set; }
        public IList<SyncTran> Trans { get; set; }
    }

    public class Credential
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string OwnerID { get; set; }
        public string AppID { get; set; }
    }

    public sealed class SyncTran
    {
        public static SyncTran Make(int id, int syncBatchID, Guid ownerID, int entityID, int entityType, string entityRef, string qbID, string statusCode, string statusSeverity, string statusMessage,DateTime syncDate)
        {
            return new SyncTran
            {
                SyncBatchID = syncBatchID,
                OwnerID = ownerID,
                EntityID = entityID,
                EntityType = (SyncEntityType)entityType,
                EntityRef = entityRef,
                QBID = qbID,
                StatusCode = statusCode,
                StatusSeverity = statusSeverity,
                StatusMessage = statusMessage,
                SyncDate=syncDate
            };
        }

        public int SyncBatchID { get; set; }
        public Guid OwnerID { get; set; }
        public int EntityID { get; set; }
        public SyncEntityType EntityType { get; set; }
        public string EntityRef { get; set; }
        public string QBID { get; set; }
        public string StatusCode { get; set; }
        public string StatusSeverity { get; set; }
        public string StatusMessage { get; set; }
        public DateTime SyncDate { get; set; }
    }


}
