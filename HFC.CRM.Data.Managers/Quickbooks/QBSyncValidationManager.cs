﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.DTO.Sync;
using HFC.CRM.Managers.QBSync;

namespace HFC.CRM.Managers.Quickbooks
{
    public class QBSyncValidationManager
    {

        public static PreSyncCheckModel ValidateFeeds(List<InvoicesTP> feed, string ownerId)
        {
            var logs = new PreSyncCheckModel();
            var listForRemove = new List<InvoicesTP>();
            foreach (var invoice in feed)
            {
                try
                {
                    foreach (var error in ValidateFeed(invoice))
                    {
                        logs.AddInvoice(invoice, error);
                        listForRemove.Add(invoice);
                    }
                    foreach (var error in ValidateLead(invoice.Opportunity))
                    {
                        logs.AddOpportunity(invoice.Opportunity, error);
                        listForRemove.Add(invoice);
                    }
                }
                catch (Exception e)
                {
                    logs.AddInvoice(invoice, "Unknown error: ");
                    listForRemove.Add(invoice);
                    EventLogger.LogEvent(e, "", 0, ownerId, "ValidateFeed");
                }
            }

            List<List<CustomerTP>> DuplicateCustomerName = new List<List<CustomerTP>>();
            if (feed.Count > 0)
            {
                var customerlst = feed.Select(x => x.Opportunity.PrimCustomer).ToList();

                if (customerlst != null && customerlst.Count > 0)
                {
                    var customerGrpbyAccId = customerlst.GroupBy(x => x.AccountId).Select(y => y.First()).ToList();

                    if (customerGrpbyAccId != null && customerGrpbyAccId.Count > 0)
                    {
                        DuplicateCustomerName = customerGrpbyAccId.GroupBy(x => x.FullName).Where(x => x.Count() > 1).Select(x => x.ToList()).ToList();

                        if (DuplicateCustomerName.Count > 0)
                            logs.DuplicateCustomerName = DuplicateCustomerName;
                    }
                }
            }

            if (logs.DuplicateCustomerName == null)
                logs.DuplicateCustomerName = DuplicateCustomerName;

            foreach (var invoice in listForRemove)
            {
                if (feed.Contains(invoice))
                {
                    feed.Remove(invoice);
                }
            }
            return logs;
        }

        private static List<string> ValidateFeed(InvoicesTP feed)
        {
            var res = new List<string>();

            //if (feed.Orders == null || (feed.Orders.OrderLines != null && feed.Orders.OrderLines.Count == 0))
            //    res.Add("Invoice(s) have no order items: ");

            if (feed.Orders == null || (feed.Orders.OrderTotal <= 0))
                res.Add("Invoice(s) with zero or negative total: ");

            if (feed.Quote == null || (feed.Quote.QuoteLines != null && feed.Quote.QuoteLines.Count == 0))
                res.Add("Invoice(s) have no Quote items: ");

            if (feed.Quote == null || (feed.Quote.QuoteSubTotal <= 0))
                res.Add("Invoice(s) with zero or negative total: ");

            //if (feed.Quote.QuoteLines.Any(x => x.VendorId == null && x.ProductId == null))
            //{
            //    res.Add("Invoice(s) with items with empty vendor & product: ");
            //}
            if (feed.Payments.Any(x => x.VerificationCheck != null && x.VerificationCheck.Length > 21))
            {
                res.Add("Invoice(s) with a payment that contains an Authorization Code that exceeds 21 characters: ");
            }

            return res;
        }

        private static List<string> ValidateLead(Opportunity opportunity)
        {
            var res = new List<string>();
            var badSymbols = ":().";

            if (opportunity.PrimCustomer == null)
            {
                res.Add("Customer(s) information should not empty ");
            }
            if (opportunity.AccountBillingAddressTP == null)
            {
                res.Add("Customer(s) Billing Address should not empty ");
            }
            if (opportunity.InstallationAddressTP == null)
            {
                res.Add("Customer(s) Installation Address should not empty ");
            }

            if (opportunity.PrimCustomer != null)
            {
                if (opportunity.PrimCustomer.FirstName != null && opportunity.PrimCustomer.LastName != null)
                {
                    if (opportunity.PrimCustomer.FirstName.Length > 25 || opportunity.PrimCustomer.LastName.Length > 25
                        || opportunity.PrimCustomer.FirstName.Any(x => badSymbols.Contains(x)) ||
                        opportunity.PrimCustomer.LastName.Any(x => badSymbols.Contains(x)))
                    {
                        res.Add("Customer(s) with name(s) (first or last) exceeds 25 characters or name(s) contains special characters: ");
                    }
                    if (opportunity.PrimCustomer.FirstName.Length + opportunity.PrimCustomer.LastName.Length > 100)
                    {
                        res.Add("Customer(s) display name exceeds 100 characters: ");
                    }
                }

                if (CheckLength(opportunity.PrimCustomer.PrimaryEmail, 100))
                {
                    res.Add("Customer(s) primary email exceeds 100 characters: ");
                }

                if (CheckLength(opportunity.PrimCustomer.SecondaryEmail, 100))
                {
                    res.Add("Customer(s) with secondary email exceeds 100 characters: ");
                }

                if (CheckLength(opportunity.PrimCustomer.CellPhone, 20))
                {
                    res.Add("Customer(s) with cell phone exceeds 20 characters: ");
                }
                if (CheckLength(opportunity.PrimCustomer.FaxPhone, 20))
                {
                    res.Add("Customer(s) with fax phone exceeds 20 characters: ");
                }
                if (CheckLength(opportunity.PrimCustomer.HomePhone, 20))
                {
                    res.Add("Customer(s) with home phone exceeds 20 characters: ");
                }
                if (CheckLength(opportunity.PrimCustomer.WorkPhone, 20))
                {
                    res.Add("Customer(s) with work phone exceeds 20 characters: ");
                }
                if (CheckLength(opportunity.PrimCustomer.WorkPhoneExt, 20))
                {
                    res.Add("Customer(s) with work phone ext exceeds 20 characters: ");
                }
                if (CheckLength(opportunity.PrimCustomer.CompanyName, 50))
                {
                    res.Add("Customer(s) with company name exceeds 50 characters: ");
                }
            }

            if (opportunity.AccountBillingAddressTP != null)
            {
                if (opportunity.AccountBillingAddressTP.City != null && opportunity.AccountBillingAddressTP.City.Length > 255)
                {
                    res.Add("Customer(s) BillingAddress with city exceeds 255 characters: ");
                }
                if (opportunity.AccountBillingAddressTP.State != null && opportunity.AccountBillingAddressTP.State.Length > 255)
                {
                    res.Add("Customer(s) BillingAddress with state exceeds 255 characters: ");
                }
                if (opportunity.AccountBillingAddressTP.ZipCode != null && opportunity.AccountBillingAddressTP.ZipCode.Length > 30)
                {
                    res.Add("Customer(s) BillingAddress with zip exceeds 30  characters: ");
                }
                if ((opportunity.AccountBillingAddressTP.Address1 != null && opportunity.AccountBillingAddressTP.Address1.Length > 500) || (opportunity.AccountBillingAddressTP.Address2 != null && opportunity.AccountBillingAddressTP.Address2.Length > 500))
                {
                    res.Add("Customer(s) BillingAddress with Address exceeds 500 characters: ");
                }
            }

            if (opportunity.InstallationAddressTP != null)
            {
                if (opportunity.InstallationAddressTP.City != null && opportunity.InstallationAddressTP.City.Length > 255)
                {
                    res.Add("Customer(s) InstallationAddress with city exceeds 255 characters: ");
                }
                if (opportunity.InstallationAddressTP.State != null && opportunity.InstallationAddressTP.State.Length > 255)
                {
                    res.Add("Customer(s) InstallationAddress with state exceeds 255 characters: ");
                }
                if (opportunity.InstallationAddressTP.ZipCode != null && opportunity.InstallationAddressTP.ZipCode.Length > 30)
                {
                    res.Add("Customer(s) InstallationAddress with zip exceeds 30  characters: ");
                }
                if ((opportunity.InstallationAddressTP.Address1 != null && opportunity.InstallationAddressTP.Address1.Length > 500) || (opportunity.InstallationAddressTP.Address2 != null && opportunity.InstallationAddressTP.Address2.Length > 500))
                {
                    res.Add("Customer(s) InstallationAddress with Address exceeds 500 characters: ");
                }
            }

            return res;
        }

        private static bool CheckLength(string text, int length)
        {
            return (text != null && text.Length > length);

        }
    }



}
