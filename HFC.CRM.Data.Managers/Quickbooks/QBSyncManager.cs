﻿using HFC.CRM.Data;
using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Data;


using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Diagnostics;
using HFC.CRM.DTO.Sync;
using Newtonsoft.Json;
using RazorEngine.Compilation.ImpromptuInterface.InvokeExt;
using HFC.CRM.Managers.QBSync;
using System.Data.Entity;
using HFC.CRM.Data.Context;
using System.Threading.Tasks;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data.Extensions.EnumAttr;
using System.Data.SqlClient;
using Dapper;
using HFC.CRM.DTO.Lookup;
using System.Threading;

namespace HFC.CRM.Managers.Quickbooks
{
    public sealed class QBSyncManager : DataManager, IDisposable
    {
        private QBCredential _login;
        private CRMContext _crmdbContext;
        private Franchise franchise;
        private QBApps _app;
        public int brandid;

        //public string HostDomain = "";// = ConfigurationManager.AppSettings["HostDomain"];

        //private bool IsBudgetBlinds
        //{
        //    get { return HostDomain.ToLower() == "budgetblinds"; }
        //}

        public static QBSyncManager For(QBCredential login)
        {
            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var app = con.GetList<QBApps>("where OwnerID=@OwnerID", new { OwnerID = login.OwnerID }).FirstOrDefault();
                Franchise fe = new QBManager().GetFranchise(app.FranchiseId);
                return new QBSyncManager { _login = login, _crmdbContext = new CRMContextEx(), franchise = fe, _app = app, brandid = fe.BrandId };
            }
        }

        public void Dispose()
        {
            _crmdbContext.Dispose();
        }

        /// <summary>
        /// Check the QBO sync lock state in Database
        /// </summary>
        /// <returns></returns>
        public bool LockState()
        {
            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var app = con.GetList<QBApps>("where OwnerID=@OwnerID", new { OwnerID = _login.OwnerID }).FirstOrDefault();
                return app == null || (app.IsSyncLockedExt);
            }
        }

        /// <summary>
        /// set IsSyncLocked= true in QBApp table, if IsSyncLocked is true, the user wait to until unlock the state
        /// </summary>
        /// <returns></returns>
        public bool TryLock()
        {
            if (_login == null)
                return false;

            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var app = con.GetList<QBApps>("where OwnerID=@OwnerID", new { OwnerID = _login.OwnerID }).FirstOrDefault();

                if (app == null)
                    return true;

                if (app.IsSyncLockedExt != true)
                {
                    app.IsSyncLockedExt = true;
                    con.Update<QBApps>(app);
                    return true;
                }
                return false;
            }


        }

        /// <summary>
        /// set IsSyncLocked= false in QBApp table, if IsSyncLocked is false, the user can strat the sync
        /// </summary>
        public void UnLock()
        {
            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var app = con.GetList<QBApps>("where OwnerID=@OwnerID", new { OwnerID = _login.OwnerID }).FirstOrDefault();

                if (app == null) return;

                if (app.IsSyncLockedExt)
                {
                    app.IsSyncLockedExt = false;
                    con.Update<QBApps>(app);
                }
            }

        }

        /// <summary>
        /// QB destop get invoice data
        /// </summary>
        /// <returns></returns>
        public object Sync()
        {
            QBManager mgr = new QBManager();
            QBApps app = mgr.IsValidQuickBooksLogin(_login.Username, _login.Password, _login.OwnerID);

            if (app == null)
                return null;

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                // Load the list of entities tha thave already been synced.
                string previouslySyncedEntitiesQuery = @"SELECT [EntityID], [EntityType],[EntityRef],[QBID] FROM [TPTQuickBooksSync].[desktop].[SyncTrans] WHERE [OwnerID]=@OwnerID";

                var syncLogOldType = connection.Query<QBDSyncLog>(previouslySyncedEntitiesQuery, new { OwnerID = app.OwnerID }).ToList();

                // Convert the old style sync log to the new style sync log.
                List<QBSyncLog> syncLog = syncLogOldType.Select(s => new QBSyncLog { ObjectID = s.EntityID, ObjectType = s.EntityType, ObjectRef = s.EntityRef, QbId = 0 }).ToList();
                return Sync(syncLog);
            }


        }

        /// <summary>
        /// QBD model
        /// </summary>
        private class QBDSyncLog
        {
            public int EntityID { get; set; }
            public int EntityType { get; set; }
            public string EntityRef { get; set; }
            public string QBID { get; set; }
        }


        /// <summary>
        /// return the invoice to sync QBO and QBD except already syncked data
        /// </summary>
        /// <param name="synclog"></param>
        /// <returns></returns>
        public QBSyncRequest Sync(List<QBSyncLog> synclog)
        {
            QBManager mgr = new QBManager();
            QBApps app = _app;

            if (app == null)
                return null;

            var TaxRateMapping = mgr.GetTaxName(app.FranchiseId);
            if (TaxRateMapping != null && TaxRateMapping.Count > 0)
                app.TaxRateMapping = TaxRateMapping.Where(x => x.QBOTaxName != "").ToList();
            else
                app.TaxRateMapping = null;

            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    string query = @"select * from Auth.Users u
                                    join [CRM].[FranchiseOwners] o on u.UserId=o.UserId
                                    where o.FranchiseId=@FranchiseId";

                    var currentUser = connection.Query<User>(query, new { FranchiseId = app.FranchiseId }).FirstOrDefault();
                    var currentFranchise = connection.Get<Franchise>(app.FranchiseId);

                    if (currentUser == null)
                    {
                        throw new Exception(String.Format("Franchise: {0} has no owners", app.FranchiseId));
                    }

                    InvoiceManager invoiceManager = new InvoiceManager(currentUser, currentFranchise);
                    var allInvoices = invoiceManager.GetInvoicesByFranchiseId(currentFranchise.FranchiseId, app);

                    QBSyncValidationManager.ValidateFeeds(allInvoices, _login.OwnerID);

                    //Murugan:
                    //var salesTaxes = GetSalesTaxes(allInvoices, synclog);

                    QBSyncRequest results = new QBSyncRequest
                    {
                        ChartOfAccounts = GetChartOfAccounts(synclog),
                        Region = currentFranchise.CountryCode.ToUpper(),
                        OwnerID = Guid.Parse(_login.OwnerID),
                        BatchID = Guid.NewGuid(),
                        Customers = GetCustomers(allInvoices, synclog, app),
                        Vendors = GetVendors(allInvoices, synclog, app),
                        LineItems = GetLineItems(allInvoices, synclog),
                        // Murugan
                        SalesTax = GetSalesTaxes(allInvoices, synclog, app),  // pending
                        Invoices = GetInvoices(allInvoices, synclog, app),
                        PaymentMethods = getPaymentMethods(allInvoices, app, synclog),
                        Payments = GetPayments(allInvoices, app, synclog),
                        SyncOption = GetSyncOption(app),
                        AppToken = app.AppToken ?? string.Empty,
                        ConsumerKey = app.ConsumerKey ?? string.Empty,
                        ConsumerSecret = app.ConsumerSecret ?? string.Empty
                    };

                    return results;
                }
            }
            catch (Exception e)
            {
                EventLogger.LogEvent(e);
                //StackTrace stackTrace = new StackTrace();
                //var log = new { Exception = e, Stack = stackTrace.ToString() };
                //var logStr = JsonConvert.SerializeObject(log);
                //Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(logStr));
                throw e;
            }

        }

        /// <summary>
        /// Get Accounts data
        /// </summary>
        /// <param name="synclog"></param>
        /// <returns></returns>
        private List<QBAccount> GetChartOfAccounts(List<QBSyncLog> synclog)
        {
            var accounts = new List<QBAccount>();
            var syncedAccounts = synclog.Where(x => x.ObjectType == (int)SyncEntityType.Account).Select(x => x.ObjectID);
            string suffix = ContantStrings.GetBrandName(brandid);

            if (!syncedAccounts.Any())
            {
                if (_app.Sales == null || _app.Sales == "")
                    accounts.Add(new QBAccount { ID = 0, Name = string.Format("{0} {1}", ContantStrings.ChartOfAccounts.Sales, suffix), AccountType = ContantStrings.QBOAccountTypes.Income });
                else
                    accounts.Add(new QBAccount { ID = 0, Name = _app.Sales, AccountType = ContantStrings.QBOAccountTypes.Income });

                if (_app.COG == null || _app.COG == "")
                    accounts.Add(new QBAccount { ID = 1, Name = string.Format("{0} {1}", ContantStrings.ChartOfAccounts.COG, suffix), AccountType = ContantStrings.QBOAccountTypes.CostOfGoodsSold });
                else
                    accounts.Add(new QBAccount { ID = 1, Name = _app.COG, AccountType = ContantStrings.QBOAccountTypes.CostOfGoodsSold });

                if (_app.AR == null || _app.AR == "")
                    accounts.Add(new QBAccount { ID = 2, Name = string.Format("{0} {1}", ContantStrings.ChartOfAccounts.AR, suffix), AccountType = ContantStrings.QBOAccountTypes.AccountsReceivable });
                else
                    accounts.Add(new QBAccount { ID = 2, Name = _app.AR, AccountType = ContantStrings.QBOAccountTypes.AccountsReceivable });

                accounts.Add(new QBAccount { ID = 3, Name = string.Format("{0} {1}", ContantStrings.ChartOfAccounts.GeneralTaxAgencyPayable, suffix), AccountType = ContantStrings.QBOAccountTypes.Taxes });
                accounts.Add(new QBAccount { ID = 3, Name = string.Format("{0} {1}", ContantStrings.ChartOfAccounts.SalesDiscount, suffix), AccountType = ContantStrings.QBOAccountTypes.Discount });
            }

            return accounts;
        }

        /// <summary>
        /// Returns a unique set of customers in a given list of invoices.
        /// </summary>
        /// <param name="invoices">The list of invoices to extract customers from.</param>
        /// <param name="synclog"></param>
        /// <param name="app"></param>
        /// <returns></returns>
        private static List<QBOCustomer> GetCustomers(List<InvoicesTP> invoices, List<QBSyncLog> synclog, QBApps app)
        {
            List<QBOCustomer> customers = new List<QBOCustomer>();
            bool islastNameFirst = app.IsLastNameFirst;

            // If the invoice list is empty, then return an empty list of customers.
            if (invoices == null || invoices.Count == 0)
                return customers;

            foreach (InvoicesTP invoice in invoices)
            {
                try
                {
                    CustomerTP person = invoice.Opportunity.PrimCustomer;
                    AddressTP billingAddress = invoice.Opportunity.AccountBillingAddressTP;
                    AddressTP installAddress = invoice.Opportunity.InstallationAddressTP;
                    AccountTP account = invoice.Opportunity.AccountTP;

                    // TODO: This failure condition should at the very least be logged. Ideally this shows up on someone's rader as a bad invoice for correction;
                    // otherwise it will just happen forever every time a sync is attempted.
                    if (person == null || billingAddress == null || installAddress == null)
                    {
                        throw new Exception("Customer information should not empty");
                    }

                    QBSyncLog customer = synclog.LastOrDefault(x => x.ObjectID == person.CustomerId && x.ObjectType == (int)SyncEntityType.Customer);

                    if (customer == null)
                    {
                        customers.Add(
                            new QBOCustomer()
                            {
                                ID = person.CustomerId,
                                Name = FormatCustomerName(islastNameFirst, person.FirstName, person.LastName),
                                CompanyName = person.CompanyName,
                                FirstName = person.FirstName,
                                MiddleName = person.MI,
                                LastName = person.LastName,
                                BillAddr1 = billingAddress.Address1,
                                BillCity = billingAddress.City,
                                BillState = billingAddress.State,
                                BillPostalCode = billingAddress.ZipCode,
                                BillCountry = billingAddress.CountryCode2Digits,
                                ShipAddr1 = installAddress != null ? installAddress.Address1 : string.Empty,
                                ShipCity = installAddress != null ? installAddress.City : string.Empty,
                                ShipState = installAddress != null ? installAddress.State : string.Empty,
                                ShipPostalCode = installAddress != null ? installAddress.ZipCode : string.Empty,
                                ShipCountry = installAddress != null ? installAddress.CountryCode2Digits : string.Empty,
                                Phone = person.HomePhone,
                                CellPhone = person.CellPhone,
                                Fax = person.FaxPhone,
                                Email = person.PrimaryEmail ?? person.SecondaryEmail,
                                IsCommercial = account.IsCommercial,
                                AccountId = account.AccountId
                            });
                    }
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    //StackTrace stackTrace = new StackTrace();
                    //var log = new { Exception = e, Stack = stackTrace.ToString(), Item = invoice };
                    //string logStr = JsonConvert.SerializeObject(log);
                    //Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(logStr));
                }
            }
            return customers.GroupBy(x => x.AccountId).Select(x => x.FirstOrDefault()).ToList();
        }

        /// <summary>
        /// return the vendor list in Alience,non-alience and franchise(local) vendor
        /// </summary>
        /// <param name="feed"></param>
        /// <param name="synclog"></param>
        /// <param name="app"></param>
        /// <returns></returns>
        private List<QBVendor> GetVendors(List<InvoicesTP> feed, List<QBSyncLog> synclog, QBApps app)
        {
            var vendors = new List<QBVendor>();

            try
            {

                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    string vendorQ = @"SELECT hv.VendorIdPk,hv.VendorId,hv.Name,hv.AccountRep,hv.AccountRepEmail,hv.AccountRepPhone,hv.AddressId,
                                        case when fv.FranchiseId is not null then fv.VendorStatus else isnull(hv.IsActive,0) end VendorStatus,AccountNo 
                                        FROM [Acct].[HFCVendors] hv 
                                        left JOIN [Acct].[FranchiseVendors] fv ON hv.VendorId = fv.VendorId and fv.FranchiseId=@FranchiseId
                                        left join CRM.Type_VendorStatus vs on fv.VendorStatus=vs.Id
                                        where hv.VendorType = 1 and fv.VendorStatus=1
                                        union
                                        SELECT hv.VendorIdPk,hv.VendorId,hv.Name,fv.AccountRep,fv.AccountRepEmail,fv.AccountRepPhone,fv.AddressId,
                                        case when fv.FranchiseId is not null then fv.VendorStatus else isnull(hv.IsActive,0) end VendorStatus,AccountNo 
                                        FROM [Acct].[HFCVendors] hv 
                                        left join Acct.FranchiseVendors fv  ON hv.VendorId = fv.VendorId and  fv.FranchiseId=@FranchiseId
                                        left join CRM.Type_VendorStatus vs on fv.VendorStatus=vs.Id
                                        where hv.VendorType = 2 and fv.VendorStatus=1
                                        union
                                        SELECT VendorIdPk,VendorId,Name,AccountRep,AccountRepEmail,AccountRepPhone,AddressId,fv.VendorStatus,AccountNo 
                                        from Acct.FranchiseVendors fv 
                                        left join CRM.Type_VendorStatus vs on fv.VendorStatus=vs.Id
                                        where fv.FranchiseId=@FranchiseId and fv.VendorType=3 and fv.VendorStatus=1 Order by Name asc";

                    var vendorItems = connection.Query<FranchiseVendor>(vendorQ, new { FranchiseId = app.FranchiseId }).Where(x => x.VendorStatus == 1).ToList();

                    var syncedVendors = synclog.Where(x => x.ObjectType == (int)SyncEntityType.Vendor).Select(x => x.ObjectID).ToList();

                    if (syncedVendors == null || !syncedVendors.Contains(0))
                    {
                        vendors.Add(new QBVendor() { ID = 0, Name = "General Tax Agency" });
                    }

                    foreach (var item in vendorItems)
                    {
                        try
                        {
                            if (!syncedVendors.Contains(item.VendorId))
                            {
                                var vAddress = connection.Get<AddressTP>(item.AddressId);
                                vendors.Add(
                                   new QBVendor()
                                   {
                                       ID = item.VendorId,
                                       Name = item.Name.Trim(),
                                       BillAddr1 = vAddress != null ? vAddress.Address1 : string.Empty,
                                       BillCity = vAddress != null ? vAddress.City : string.Empty,
                                       BillState = vAddress != null ? vAddress.State : string.Empty,
                                       BillPostalCode = vAddress != null ? vAddress.ZipCode : string.Empty,
                                       BillCountry = vAddress != null ? vAddress.CountryCode2Digits : string.Empty,
                                       Phone = item.AccountRepPhone,
                                       AltPhone = item.AccountRepPhone,
                                       Fax = "",
                                       Email = item.AccountRepEmail,
                                       AccountNumber = item.AccountNo
                                   });
                            }

                        }
                        catch (Exception e)
                        {
                            EventLogger.LogEvent(e);
                            //StackTrace stackTrace = new StackTrace();
                            //var log = new { Exception = e, Stack = stackTrace.ToString(), Item = item };
                            //var logStr = JsonConvert.SerializeObject(log);
                            //Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(logStr));
                        }
                        finally
                        {

                        }
                    }
                }

                if (brandid != 1)
                {
                    var syncedVendors = synclog.Where(x => x.ObjectType == (int)SyncEntityType.Vendor && x.ObjectID == 0).Select(x => x.ObjectRef).ToList();

                    if (syncedVendors == null || !syncedVendors.Contains("General Vendor"))
                    {
                        vendors.Add(new QBVendor() { ID = 0, Name = "General Vendor" });
                    }
                }

                return vendors;

            }
            catch (Exception e)
            {
                EventLogger.LogEvent(e);
                return null;
                //StackTrace stackTrace = new StackTrace();
                //var log = new { Exception = e, Stack = stackTrace.ToString() };
                //var logStr = JsonConvert.SerializeObject(log);
                //Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(logStr));
            }
        }

        /// <summary>
        /// Return list of quote line items like product, addition items
        /// </summary>
        /// <param name="feed"></param>
        /// <param name="synclog"></param>
        /// <returns></returns>
        private static List<QBItemNonInventory> GetLineItems(List<InvoicesTP> feed, List<QBSyncLog> synclog)
        {
            var productsName = new List<string>();
            var products = new List<QBItemNonInventory>();

            var syncedProducts = synclog.Where(x => x.ObjectType == (int)SyncEntityType.ItemNonInventory).Select(x => x.ObjectID).ToList();
            var syncedProductsbyName = synclog.Where(x => x.ObjectType == (int)SyncEntityType.ItemNonInventory && x.ObjectID == 0).Select(x => x.ObjectRef).ToList();

            if (syncedProducts == null || !syncedProducts.Contains(0))
            {
                products.Add(new QBItemNonInventory() { ID = 0, Name = "Other", Desc = string.Empty });
                products.Add(new QBItemNonInventory() { ID = 0, Name = "Discounts", Desc = "Total Discounts" });
            }

            foreach (var item in feed)
            {
                try
                {
                    if (item.Quote == null || !item.Quote.QuoteLines.Any())
                    {
                        throw new Exception(String.Format("Quote {0} has no quote lines", item.Quote.QuoteKey));
                    }

                    foreach (var p in item.Quote.QuoteLines.ToList())
                    {
                        if (p.ProductId != null && p.ProductId > 0 && (syncedProducts == null || !syncedProducts.Contains((int)p.ProductId)))
                        {
                            var productName = string.IsNullOrEmpty(p.ProductName) ? string.Empty : p.ProductName;// new string(p.ProductName.Take(30).ToArray());
                            if (productName != string.Empty && !productsName.Contains(productName))
                            {
                                string desc = productName;
                                products.Add(new QBItemNonInventory() { ID = (int)p.ProductId, Name = productName.Trim(), Desc = desc }); productsName.Add(productName);
                            }
                        }
                        else
                        {
                            if (!syncedProductsbyName.Contains(p.ProductName))
                            {
                                var productName = string.IsNullOrEmpty(p.ProductName) ? string.Empty : p.ProductName;// new string(p.ProductName.Take(30).ToArray());
                                if (productName != string.Empty && !productsName.Contains(productName))
                                {
                                    Thread.Sleep(1000);
                                    string desc = productName;
                                    Random rnd = new Random();
                                    int ProductId = rnd.Next(-99999, -1);
                                    products.Add(new QBItemNonInventory() { ID = ProductId, Name = productName.Trim(), Desc = productName }); productsName.Add(productName);
                                }
                            }
                            ///throw new Exception("Quote line id (" + p.QuoteLineId + ") does not have product information");
                        }

                        //if (p.ProductTypeId == null)
                        //{
                        //    var productName = string.IsNullOrEmpty(p.ProductType) ? string.Empty : new string(p.ProductType.Take(30).ToArray());
                        //    products.Add(new QBItemNonInventory() { ID = 0, Name = productName, Desc = p.Description }); productsName.Add(productName);
                        //}
                    }
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    //StackTrace stackTrace = new StackTrace();
                    //var log = new { Exception = e, Stack = stackTrace.ToString(), Item = item };
                    //var logStr = JsonConvert.SerializeObject(log);
                    //Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(logStr));
                }
                finally
                {

                }
            }

            return products;
        }

        private static List<QBItemSalesTax> GetSalesTaxes(IEnumerable<InvoicesTP> feed, IEnumerable<QBSyncLog> synclog, QBApps app)
        {
            List<string> salesTaxesName = new List<string>();
            var salesTaxes = new List<QBItemSalesTax>();

            var syncedjurisdictionsbyName = synclog.Where(x => x.ObjectType == (int)SyncEntityType.ItemSalesTax).Select(x => x.ObjectRef).ToList();
            if (syncedjurisdictionsbyName == null || !syncedjurisdictionsbyName.Contains("TPTSALESTAX"))
            {
                salesTaxes.Add(new QBItemSalesTax()
                {
                    ID = 0,
                    Name = "TPTSALESTAX",
                    ItemDesc = "Sales Tax Calculated by Touchpoint",  /// MYC-942   
                });
            }

            if (app.TaxDetailjurisdictions != null && app.TaxDetailjurisdictions == true)
            {
                foreach (var item in feed)
                {
                    try
                    {
                        if (item.Quote == null || !item.Quote.QuoteLines.Any())
                        {
                            throw new Exception(String.Format("Quote {0} has no quote lines", item.Quote.QuoteKey));
                        }

                        foreach (var p in item.Quote.QuoteLines.ToList())
                        {
                            if (p.SalesTax != null && p.SalesTax.TaxDetails != null && p.SalesTax.TaxDetails.Any())
                            {
                                foreach (var td in p.SalesTax.TaxDetails)
                                {
                                    if (!syncedjurisdictionsbyName.Contains(td.Jurisdiction))
                                    {
                                        var Jurisdiction = string.IsNullOrEmpty(td.Jurisdiction) ? string.Empty : new string(td.Jurisdiction.Take(30).ToArray());
                                        if (Jurisdiction != string.Empty && !salesTaxesName.Contains(Jurisdiction))
                                        {
                                            string desc = Jurisdiction;
                                            salesTaxes.Add(new QBItemSalesTax() { ID = td.TaxDetailsId, Name = Jurisdiction.Trim(), ItemDesc = td.TaxName != null && td.TaxName != "" ? td.TaxName : Jurisdiction });
                                            salesTaxesName.Add(Jurisdiction);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        EventLogger.LogEvent(e);

                    }
                    finally
                    {

                    }
                }
            }
            return salesTaxes;

            //foreach (InvoicesTP item in feed)
            //{
            //    foreach (JobSaleAdjustment jobSaleAdjustment in item.JobQuote.SaleAdjustments.Where(x => x.TypeEnum == SaleAdjTypeEnum.Tax))
            //    {
            //        try
            //        {
            //            if (salesTaxesName.Contains(jobSaleAdjustment.Category)) continue; // If the adjustment has already been added, ignore the duplicate.
            //            salesTaxesName.Add(jobSaleAdjustment.Category);

            //            if (syncedSalesTaxes.Contains(jobSaleAdjustment.SaleAdjustmentId)) continue;// If the adjustment has already been added to the sync collection, then ignore the entry and continue.

            //            // MYC-942
            //            var zip = (!string.IsNullOrEmpty(item.Job.Lead.Addresses.FirstOrDefault().ZipCode)) ? string.Format("{0}", item.Job.Lead.Addresses.FirstOrDefault().ZipCode) : string.Empty;
            //            var percent = (!string.IsNullOrEmpty(jobSaleAdjustment.Percent.ToString())) ? string.Format("{0} | ", jobSaleAdjustment.Percent.ToString()) : string.Empty;
            //            var category = (!string.IsNullOrEmpty(jobSaleAdjustment.Category)) ? string.Format("{0} | ", jobSaleAdjustment.Category) : string.Empty;
            //            var memo = (!string.IsNullOrEmpty(jobSaleAdjustment.Memo)) ? jobSaleAdjustment.Memo : string.Empty;
            //            var memoCombined = string.Format("{0}{1}{2}", percent, category, zip);
            //            /// MYC-942       
            //            salesTaxes.Add(
            //                new QBItemSalesTax()
            //                {
            //                    ID = jobSaleAdjustment.SaleAdjustmentId,
            //                    Name = jobSaleAdjustment.Category,
            //                    ItemDesc = memoCombined,  /// MYC-942   
            //                    TaxRate = jobSaleAdjustment.Percent.ToString(),
            //                    InvoviceId = item.InvoiceId
            //                    //Zip = zip
            //                });
            //        }
            //        catch (Exception e)
            //        {
            //            StackTrace stackTrace = new StackTrace();
            //            var log = new { Exception = e, Stack = stackTrace.ToString(), Item = item };
            //            var logStr = JsonConvert.SerializeObject(log);
            //            Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(logStr));
            //        }
            //        finally
            //        {

            //        }
            //    }

            //}


        }

        /// <summary>
        /// return the list of invoice with quote line items(product, discount, sales tax)
        /// </summary>
        /// <param name="feed"></param>
        /// <param name="synclog"></param>
        /// <param name="app"></param>
        /// <returns></returns>
        private List<QBOInvoice> GetInvoices(List<InvoicesTP> feed, List<QBSyncLog> synclog, QBApps app)
        {
            var invoices = new List<QBOInvoice>();
            var syncedInvoices = synclog.Where(x => x.ObjectType == (int)SyncEntityType.Invoice).Select(x => x.ObjectID).ToList();
            var islastNameFirst = app.IsLastNameFirst;

            foreach (var item in feed)
            {
                try
                {
                    if (!syncedInvoices.Contains(item.InvoiceId))
                    {
                        var qbId = 0;
                        var idRelation = synclog.LastOrDefault(y => y.ObjectType == (int)SyncEntityType.Customer && y.ObjectID == item.Opportunity.PrimCustomer.CustomerId);
                        if (idRelation != null)
                        {
                            qbId = idRelation.QbId;
                        }

                        // Murugan, modified the GetInvoiceItems to take qb.app as parameter.
                        var _invoiceItems = GetInvoiceItems(item, app);//, salesTaxes);
                        if (_invoiceItems == null || !_invoiceItems.Any()) continue;
                        if (_invoiceItems.Sum(x => x.AmountDec) < 0)
                            continue;

                        invoices.Add(
                             new QBOInvoice()
                             {
                                 ID = item.InvoiceId,
                                 Customer = FormatCustomerName(islastNameFirst, item.Opportunity.PrimCustomer.FirstName, item.Opportunity.PrimCustomer.LastName),
                                 CustomerListID = item.Opportunity.PrimCustomer.CustomerId.ToString(),
                                 CustomerQbID = qbId,
                                 //ARAccount = brandid == 1 ? string.Format("{0} {1}", ContantStrings.ChartOfAccounts.AR, "BB") : brandid == 2 ? string.Format("{0} {1}", ContantStrings.ChartOfAccounts.AR, "TL") : string.Format("{0} {1}", ContantStrings.ChartOfAccounts.AR, "CC"),
                                 TxnDate = item.Orders.ContractedDate ?? item.Orders.CreatedOn,// item.Job.ContractedOnUtc ?? item.CreatedOn.Date, //item.CreatedOn.Date, //MYC-1122 
                                 RefNumber = item.Orders.OrderNumber.ToString().TrimSafely(),
                                 BillAddr1 = item.Opportunity.AccountBillingAddressTP != null ? item.Opportunity.AccountBillingAddressTP.Address1 : string.Empty,
                                 BillCity = item.Opportunity.AccountBillingAddressTP != null ? item.Opportunity.AccountBillingAddressTP.City : string.Empty,
                                 BillState = item.Opportunity.AccountBillingAddressTP != null ? item.Opportunity.AccountBillingAddressTP.State : string.Empty,
                                 BillPostalCode = item.Opportunity.AccountBillingAddressTP != null ? item.Opportunity.AccountBillingAddressTP.ZipCode : string.Empty,
                                 BillCountry = item.Opportunity.AccountBillingAddressTP != null ? item.Opportunity.AccountBillingAddressTP.CountryCode2Digits : string.Empty,
                                 ShipAddr1 = item.Opportunity.InstallationAddressTP != null ? item.Opportunity.InstallationAddressTP.Address1 : string.Empty,
                                 ShipCity = item.Opportunity.InstallationAddressTP != null ? item.Opportunity.InstallationAddressTP.City : string.Empty,
                                 ShipState = item.Opportunity.InstallationAddressTP != null ? item.Opportunity.InstallationAddressTP.State : string.Empty,
                                 ShipPostalCode = item.Opportunity.InstallationAddressTP != null ? item.Opportunity.InstallationAddressTP.ZipCode : string.Empty,
                                 ShipCountry = item.Opportunity.InstallationAddressTP != null ? item.Opportunity.InstallationAddressTP.CountryCode2Digits : string.Empty,
                                 ItemSalesTaxName = "0%", // TODO investigate is this nessery MYC-1101
                                 InvoiceItems = _invoiceItems
                             });
                    }
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    //StackTrace stackTrace = new StackTrace();
                    //var log = new { Exception = e, Stack = stackTrace.ToString(), Item = item };
                    //var logStr = JsonConvert.SerializeObject(log);
                    //Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(logStr));
                }
                finally
                {

                }
            }
            return invoices;
        }

        /// <summary>
        /// Prepare invoice items to sync
        /// </summary>
        /// <param name="invoice"></param>
        /// <param name="app"></param>
        /// <returns></returns>
        private List<QBInvoiceLineItem> GetInvoiceItems(InvoicesTP invoice, QBApps app)
        {
            try
            {
                var invoiceItems = new List<QBInvoiceLineItem>();
                if (invoice.Quote == null || !invoice.Quote.QuoteLines.Any())
                {
                    throw new Exception(string.Format("Invoice: {0} has no quotes", invoice.Quote.QuoteKey));
                }

                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    var frans = connection.Get<Franchise>(app.FranchiseId);
                    invoice.Region = frans != null ? frans.CountryCode.ToUpper() : "";
                }

                // Adding Products
                var products = (from ql in invoice.Quote.QuoteLines where ql.ProductTypeId == 1 || ql.ProductTypeId == 2 || ql.ProductTypeId == 5 select ql).ToList();

                foreach (var p in products)
                {
                    decimal Cost = 0m;
                    if (p != null && p.QuoteLineDetail != null)
                        Cost = p.QuoteLineDetail.BaseCost != null ? (decimal)p.QuoteLineDetail.BaseCost : 0;

                    /// Quote line items
                    invoiceItems.Add(new QBInvoiceLineItem
                    {
                        Name = p.ProductName.Trim(),
                        Desc = p.Description ?? string.Empty,
                        Quantity = p.Quantity.ToString(),
                        AmountDec = (decimal)p.ExtendedPrice,
                        ItemType = "ItemNonInventory",
                        Cost = Cost.ToString(),
                        Vendor = !string.IsNullOrEmpty(p.VendorName) && p.VendorName != "" ? p.VendorName : "General Vendor",
                        Taxable = p.SalesTax != null ? true.ToString() : false.ToString(),
                        IsSaleAdjustment = false
                    });

                }

                /// Additional Charges
                var Services = (from ql in invoice.Quote.QuoteLines where ql.ProductTypeId == 3 select ql).ToList();
                foreach (var s in Services)
                {
                    invoiceItems.Add(new QBInvoiceLineItem
                    {
                        Name = s.ProductName.Trim(),
                        Desc = s.Description,
                        Quantity = s.Quantity != null ? s.Quantity.ToString() : "",
                        AmountDec = (decimal)s.ExtendedPrice,
                        ItemType = "ItemNonInventory",
                        Cost = "0",
                        Vendor = string.Empty,
                        Taxable = s.FranchiseProducts != null ? s.FranchiseProducts.Taxable.ToString() : false.ToString(),
                        IsSaleAdjustment = false
                    });
                }

                var discountd = (from ql in invoice.Quote.QuoteLines where ql.ProductTypeId == 4 select ql).ToList();

                if (invoice.Quote.TotalDiscounts != null && invoice.Quote.TotalDiscounts > 0)
                {
                    /// Discounts
                    invoiceItems.Add(new QBInvoiceLineItem
                    {
                        Name = "Discounts",
                        Desc = "Total Discounts",
                        Quantity = string.Empty,
                        AmountDec = (decimal)invoice.Quote.TotalDiscounts * -1,
                        ItemType = "ItemNonInventory",
                        Cost = "0",
                        Vendor = string.Empty,
                        Taxable = false.ToString(),
                        IsSaleAdjustment = false
                    });
                }

                // SalesTax
                decimal SalesTaxcount = 0m;

                if ((app.TaxDetailjurisdictions != null && app.TaxDetailjurisdictions == true) || (app.TaxRateMapping != null && app.TaxRateMapping.Count > 0))
                {

                    List<QBOTaxData> TaxDetails = new List<QBOTaxData>();

                    foreach (var ql in invoice.Quote.QuoteLines.ToList())
                    {
                        if (ql.SalesTax != null && ql.SalesTax.TaxDetails != null && ql.SalesTax.TaxDetails.Any())
                        {
                            foreach (var td in ql.SalesTax.TaxDetails)
                            {
                                TaxDetails.Add(new QBOTaxData
                                {
                                    Jurisdiction = td.Jurisdiction,
                                    TaxName = td.TaxName,
                                    Amount = td.Amount != null ? (decimal)td.Amount : 0,
                                    Rate = td.Rate != null ? (decimal)td.Rate : 0
                                });
                            }
                        }
                    }

                    var TaxDetailsGroup = TaxDetails.GroupBy(x => x.Jurisdiction, (key, elements) =>
                                            new
                                            {
                                                Jurisdiction = key,
                                                elements = elements.ToList()
                                            }).ToList();


                    foreach (var td in TaxDetailsGroup)
                    {
                        invoiceItems.Add(new QBInvoiceLineItem
                        {
                            Name = td.Jurisdiction.Trim(),
                            Desc = td.elements.Select(x => x.TaxName).FirstOrDefault(),
                            Quantity = string.Empty,
                            AmountDec = td.elements.Sum(x => x.Amount),
                            ItemType = "ItemSalesTax",
                            Cost = "0",
                            Vendor = string.Empty,
                            Taxable = false.ToString(),
                            TaxRate = td.elements.Select(x => x.Rate).FirstOrDefault(),
                            IsSaleAdjustment = false
                        });
                    }
                }
                else
                {
                    foreach (var QuoteLines in invoice.Quote.QuoteLines)
                    {
                        SalesTaxcount += QuoteLines != null && QuoteLines.SalesTax != null && QuoteLines.SalesTax.Amount != null ? (decimal)QuoteLines.SalesTax.Amount : 0m;
                    }

                    if (SalesTaxcount > 0)
                    {
                        /// Sales Tax
                        invoiceItems.Add(new QBInvoiceLineItem
                        {
                            Name = "TPTSALESTAX",
                            Desc = "TouchPoint Calculated Sales Tax",
                            Quantity = string.Empty,
                            AmountDec = SalesTaxcount,
                            ItemType = "ItemSalesTax",
                            Cost = "0",
                            Vendor = string.Empty,
                            Taxable = false.ToString(),
                            IsSaleAdjustment = false
                        });
                    }

                }





                // QBO RULE Don't synch non-GAAP invoices. I.e. just discount, negative invoice, just tax etc.
                if (!invoiceItems.Any())
                    return invoiceItems;

                //// Murugan // We can return the Invoices without tax line items.
                if (invoice.Region.Equals("CA"))
                {
                    if (app.SuppressSalesTax == true) return invoiceItems;
                }


                return invoiceItems;
            }
            catch (Exception e)
            {
                //StackTrace stackTrace = new StackTrace();
                //var log = new { Exception = e, Stack = stackTrace.ToString(), Item = invoice };
                //var logStr = JsonConvert.SerializeObject(log);
                //Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(logStr));
                EventLogger.LogEvent(e);
                return null;
            }
        }

        /// <summary>
        /// return Payment methods
        /// </summary>
        /// <param name="feed"></param>
        /// <param name="app"></param>
        /// <param name="synclog"></param>
        /// <returns></returns>
        private List<QBPaymentMethod> getPaymentMethods(List<InvoicesTP> feed, QBApps app, List<QBSyncLog> synclog)
        {
            var paymentMethods = new List<QBPaymentMethod>();

            try
            {
                if (!app.SynchPayments) return paymentMethods;

                var syncedPaymentMethods = synclog.Where(x => x.ObjectType == (int)SyncEntityType.PayMethod).Select(x => x.ObjectRef).ToList();

                foreach (var item in feed)
                {
                    try
                    {
                        using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                        {
                            // var payments = connection.GetList<Payments>("where InvoiceId=@InvoiceId", new { InvoiceId = item.InvoiceId }).ToList();
                            foreach (var x in item.Payments)
                            {
                                var Paymethod = connection.Get<Type_PaymentMethod>(x.PaymentMethod);
                                if (!syncedPaymentMethods.Contains(Paymethod.PaymentMethod))
                                {
                                    paymentMethods.Add(new QBPaymentMethod() { ID = x.PaymentID, Name = Paymethod.PaymentMethod.Trim() });
                                    syncedPaymentMethods.Add(Paymethod.PaymentMethod);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        EventLogger.LogEvent(ex);
                    }

                }
            }
            catch (Exception e)
            {
                EventLogger.LogEvent(e);
            }
            return paymentMethods;
        }

        /// <summary>
        /// return payment list based on invoice
        /// </summary>
        /// <param name="feed"></param>
        /// <param name="app"></param>
        /// <param name="synclog"></param>
        /// <returns></returns>
        private static List<QBReceivePayment> GetPayments(List<InvoicesTP> feed, QBApps app, List<QBSyncLog> synclog)
        {
            var payments = new List<QBReceivePayment>();

            if (!app.SynchPayments) return payments;

            var syncedPayments = synclog.Where(x => x.ObjectType == (int)SyncEntityType.Payment).Select(x => x.ObjectID).ToList();

            var islastNameFirst = app.IsLastNameFirst;

            foreach (var item in feed)
            {
                try
                {
                    using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                    {
                        foreach (var x in item.Payments)
                        {
                            try
                            {
                                if (!syncedPayments.Contains(x.PaymentID))
                                {
                                    var qbId = 0;
                                    var idRelation = synclog.LastOrDefault(y => y.ObjectType == (int)SyncEntityType.Customer && y.ObjectID == item.Opportunity.PrimCustomer.CustomerId);
                                    if (idRelation != null)
                                    {
                                        qbId = idRelation.QbId;
                                    }

                                    var Paymethod = connection.Get<Type_PaymentMethod>(x.PaymentMethod);

                                    payments.Add(new QBReceivePayment()
                                    {
                                        ID = x.PaymentID,
                                        Customer = FormatCustomerName(islastNameFirst, item.Opportunity.PrimCustomer.FirstName, item.Opportunity.PrimCustomer.LastName),
                                        CustomerListID = item.Opportunity.PrimCustomer.CustomerId.ToString(),
                                        CustomerQbID = qbId,
                                        PaymentMethod = Paymethod.PaymentMethod,
                                        InvoiceNumber = item.Orders.OrderNumber.ToString().TrimSafely(), // Need to Modify Rajkumar
                                        TxnDate = Convert.ToDateTime(x.PaymentDate).Date,
                                        TotalAmount = Convert.ToDecimal(x.Amount).ToString("0.00"),
                                        PaymentAmount = Convert.ToDecimal(x.Amount).ToString("0.00"),
                                        RefNumber = x.PaymentID.ToString().TrimSafely()
                                    });
                                }
                            }
                            catch (Exception ex)
                            {
                                EventLogger.LogEvent(ex);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                }
            }
            return payments;
        }

        /// <summary>
        /// Standard method to format a customer's name as either "FIRST LAST", or "LAST,FIRST".
        /// </summary>
        /// <param name="isLastNameFirst">Should last name be put first?</param>
        /// <param name="firstName">Value for first name.</param>
        /// <param name="lastName">Value for last name.</param>
        /// <returns>A string containing the full name of the customer, formatted as indicated.</returns>
        private static string FormatCustomerName(bool isLastNameFirst, string firstName, string lastName)
        {
            return isLastNameFirst ?
                string.Format("{0}, {1}", lastName, firstName) :
                string.Format("{0} {1}", firstName, lastName);
        }

        private SyncOption GetSyncOption(QBApps app)
        {
            return new SyncOption { IncludeInvoice = app.SynchInvoices, IncludePayment = app.SynchPayments, IncludeVendorBill = app.SynchCost, LastNameFirst = app.IsLastNameFirst };
        }

        public static QBSyncState GetSyncState(string owner, Profile Profile)
        {
            using (var handler = For(new QBCredential() { OwnerID = owner }))
            {
                return new QBSyncState()
                {
                    IsAuthorized = Profile != null && !String.IsNullOrEmpty(Profile.accesstoken),
                    IsLoggedIn = Profile != null && !String.IsNullOrEmpty(Profile.accesstoken),
                    Log = SessionManager.GetQbSyncUserLog(owner),
                    IsLocked = handler.LockState(),
                    duplicateTPTCustomer = SessionManager.GetQbduplicateCustomersLog(owner)
                };
            }
        }

        public static PreSyncCheckModel PreSyncCheck(int id, string OwnerID)
        {
            try
            {
                EventLogger.LogEvent(id.ToString() + " " + OwnerID, "QBSyncManager PreSyncCheck call started", LogSeverity.Information);
                using (var handler = For(new QBCredential() { OwnerID = OwnerID }))
                {
                    QBApps app = new QBManager().GetQBApp(id);

                    if (app == null) return null;

                    using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                    {
                        string query = @"select * from Auth.Users u
                                    join [CRM].[FranchiseOwners] o on u.UserId=o.UserId
                                    where o.FranchiseId=@FranchiseId";

                        var currentUser = connection.Query<User>(query, new { FranchiseId = id }).FirstOrDefault();
                        var currentFranchise = connection.Get<Franchise>(id);

                        EventLogger.LogEvent("", "PreSyncCheck app franchise", LogSeverity.Information);

                        var feed = new InvoiceManager(currentUser, currentFranchise).GetInvoicesByFranchiseId(currentFranchise.FranchiseId, app);
                        EventLogger.LogEvent("", "PreSyncCheck app feed", LogSeverity.Information);
                        return QBSyncValidationManager.ValidateFeeds(feed, OwnerID);
                    }
                }
            }
            catch (Exception e)
            {
                EventLogger.LogEvent(e);
                throw;
            }
        }
    }


    public class QBCredential
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string OwnerID { get; set; }
        public string AppID { get; set; }
    }


    public class QBSyncLog
    {
        public int ObjectID;
        public int QbId;
        public int ObjectType;
        public string ObjectRef;
    }

    public static partial class ext
    {
        public static string TrimSafely(this string str)
        {
            StringBuilder sb = new StringBuilder();
            if (!string.IsNullOrEmpty(str))
            {
                foreach (char c in str)
                {
                    if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '_')
                    {
                        sb.Append(c);
                    }
                }
            }
            return sb.ToString();
        }
    }
}
