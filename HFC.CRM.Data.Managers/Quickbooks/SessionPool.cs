﻿using HFC.CRM.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Managers.Quickbooks
{
    /// <summary>
    /// Session pool for managing QB WebConnector sessions in memory and DB
    /// </summary>
    public class SessionPool
    {
        /// <summary>
        /// Part of singleton pattern
        /// </summary>
        static readonly SessionPool instance = new SessionPool();  

        /// <summary>
        /// Dictionary storage for all sessions
        /// </summary>
        private Dictionary<Guid, QBSession> sessionStore = new Dictionary<Guid, QBSession>();
    
        /// <summary>
        /// Static constructor
        /// </summary>
        static SessionPool()
        {
            // Explicit static constructor to tell C# compiler
            // not to mark type as beforefieldinit
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        SessionPool()
        {

        }

        /// <summary>
        /// Singleton instance
        /// </summary>
        public static SessionPool Instance
        {
            get
            {
                return instance;
            }
        }

        /// <summary>
        /// Add or update Session with SessionId as the key
        /// </summary>
        public void put(QBSession session)
        {
            if (session == null)
                return;

            if (sessionStore.Keys.Contains(session.SessionId))
            {
                sessionStore[session.SessionId] = session;
            }
            else
            {
                sessionStore.Add(session.SessionId, session);
            }
        }

        /// <summary>
        /// Creates a new session in DB and memory and also return it. if there is already an existing open connection, it will return that open connection instead.
        /// </summary>
        public QBSession put(int appId, string ipAddress = null)
        {
            if (appId <= 0)
                return null;

            using (var db = new CRMContextEx())
            {
                QBSession retSession = db.QBSessions.FirstOrDefault(s => s.AppId == appId && !s.ClosedOnUtc.HasValue);
                if (retSession == null)
                {
                    retSession = new QBSession
                    {
                        SessionId = Guid.NewGuid(),
                        AppId = appId,
                        IPAddress = ipAddress
                    };

                    db.QBSessions.Add(retSession);

                    db.SaveChanges();
                }

                put(retSession);
                return retSession;          
            }
        }
    
        /// <summary>
        /// Gets session
        /// </summary>
        public QBSession get(string sessionId)
        {
            Guid guid = Guid.Empty;
            Guid.TryParse(sessionId, out guid);
            return get(guid);
        }

        /// <summary>
        /// Gets session
        /// </summary>
        public QBSession get(Guid sessionId)
        {
            return sessionStore[sessionId];
        }                

        /// <summary>
        /// Removes a session by sessionId
        /// </summary>
        public void pop(string sessionId)
        {
            Guid guid = Guid.Empty;
            Guid.TryParse(sessionId, out guid);
            pop(guid);
        }

        /// <summary>
        /// Removes a session by sessionId
        /// </summary>
        public void pop(Guid sessionId)
        {
            using (var db = new CRMContextEx())
            {
                var qbSession = new QBSession { SessionId = sessionId };
                db.QBSessions.Attach(qbSession);

                qbSession.ClosedOnUtc = DateTime.Now;
                db.SaveChanges();
            }
            //only remove session from memory if DB was successfully 
            sessionStore.Remove(sessionId);
        }

    }
}
