﻿namespace HFC.CRM.Managers.AdvancedEmailManager.Attributes
{
    using System;

    public class CustomFieldAttribute: Attribute
    {
        public CustomFieldAttribute(string fieldName)
        {
            this.FieldName = fieldName;
        }

        public string FieldName { get; set; }

        public string FieldDescription { get; set; }
    }
}