﻿namespace HFC.CRM.Managers.AdvancedEmailManager
{
    public class EmailTemplateLayoutModel
    {
        public string Header { get; set; }

        public string Body { get; set; }

        public string Footer { get; set; }
    }
}