﻿namespace HFC.CRM.Managers.AdvancedEmailManager
{
    using HFC.CRM.Data;

    public class DatabaseEmailTemplateProvider: IEmailTemplateProvider
    {
        private readonly EmailTemplateManager _emailTemplateManager;

        public DatabaseEmailTemplateProvider(Franchise franchise, User user)
        {
            this._emailTemplateManager = new EmailTemplateManager(franchise, user);
        }

        public EmailTemplate Get(int emailTemplateId)
        {
            return this._emailTemplateManager.GetEmailTemplateById(emailTemplateId);
        }
    }
}