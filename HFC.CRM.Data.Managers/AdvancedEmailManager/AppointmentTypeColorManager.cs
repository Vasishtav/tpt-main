﻿namespace HFC.CRM.Managers.AdvancedEmailManager 
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.IO;
    using System.Linq;
    using System.Web.Hosting;

    using HFC.CRM.Core.Logs;
    using HFC.CRM.Core.Membership;
    using HFC.CRM.Data;
    using HFC.CRM.Managers.AdvancedEmailManager.Attributes;
    using HFC.CRM.Managers.AdvancedEmailManager.TemplateModels;
    using HFC.CRM.Core;
    using HFC.CRM.Managers.DTO;
    using Data.Context;

    public class AppointmentTypeColorManager : DataManager<FranchiseAppointment_TypeColors>
    {
        public AppointmentTypeColorManager(User user, Franchise franchise) : this(user, user, franchise)
        {
        }
        public AppointmentTypeColorManager(User user, User authorizingUser, Franchise franchise)
        {
            User = user;
            AuthorizingUser = authorizingUser;
            Franchise = franchise;
        }
        

        public void Seed()
        {
            if (!this.CRMDBContext.FranchiseAppointmentTypeColors.Any())
            {
                var aTColor = new FranchiseAppointmentTypeColor();

                aTColor.FranchiseId = null;
                aTColor.Appointment1 = "#63b8ff";
                aTColor.Appointment2 = "#b700ff";
                aTColor.Appointment3 = "#00ff00";
                aTColor.Appointment4 = "#8400ff";
                aTColor.Installation = "#b6fcd5";
                aTColor.DayOff = "#40e0d0";
                aTColor.Other = "#f25c00";
                aTColor.Personal = "#d1aaaa";
                aTColor.Meeting_Training = "#074a36";
                aTColor.Service = "#fac800";
                aTColor.Vacation = "#11f4b5";
                aTColor.Holiday = "#28ad1f";
                aTColor.Followup = "#008f77";

                this.CRMDBContext.FranchiseAppointmentTypeColors.Add(aTColor);

                this.CRMDBContext.SaveChanges();

            }
        }



        public FranchiseAppointment_TypeColors GetColors()
        {
            try
            {
                FranchiseAppointment_TypeColors colors = new FranchiseAppointment_TypeColors();

                var query = @"select * from CRM.[FranchiseAppointmentTypeColors] where FranchiseId = @FranchiseId or Franchiseid is null";

                var colorsList = ExecuteIEnumerableObject<FranchiseAppointment_TypeColors>(ContextFactory.CrmConnectionString,
                    query, new
                    {
                        FranchiseId = Franchise.FranchiseId
                    }).ToList();
                if (colorsList != null && colorsList.Count > 0)
                {
                    if (colorsList.Where(x => x.FranchiseId == Franchise.FranchiseId).FirstOrDefault() != null)
                    {
                        colors = colorsList.Where(x => x.FranchiseId == Franchise.FranchiseId).FirstOrDefault();
                    }
                    else
                    {
                        colors = colorsList.Where(x => x.FranchiseId == null).FirstOrDefault();
                        colors.FranchiseAppointmentTypeColorId = 0;
                    }
                    
                }
                return colors;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return null;
            }
        }

        public string ColorSave(FranchiseAppointment_TypeColors data)
        {
            FranchiseAppointment_TypeColors result = new FranchiseAppointment_TypeColors();

            data.FranchiseId = Franchise.FranchiseId;

            try
            {

                //if (!BasePermission.CanCreate)
                //    return Unauthorized;

                if (data.FranchiseAppointmentTypeColorId > 0)
                {
                    return ColorUpdate(data);

                }
                else
                {
                    
                    var save = Insert<int, FranchiseAppointment_TypeColors>(ContextFactory.CrmConnectionString, data);
                    if (save < 0)
                    {
                        return FailedByError;
                    }

                    return Success;
                }

            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return FailedByError;
            }
        }

        public string ColorUpdate(FranchiseAppointment_TypeColors data)
        {
            FranchiseAppointment_TypeColors check = new FranchiseAppointment_TypeColors();

            try
            {
                //if (!BasePermission.CanUpdate)
                //    return Unauthorized;

                if (data.FranchiseAppointmentTypeColorId != 0)
                {
                    var updquery = @"Select * from CRM.FranchiseAppointmentTypeColors where FranchiseAppointmentTypeColorId=@FranchiseAppointmentTypeColorId";
                    var result = ExecuteIEnumerableObject<FranchiseAppointment_TypeColors>(ContextFactory.CrmConnectionString, updquery, new
                    {
                        FranchiseAppointmentTypeColorId = data.FranchiseAppointmentTypeColorId
                    }).FirstOrDefault();
                    if (result != null)
                    {
                        data.CreatedOn = result.CreatedOn;
                        if (result.CreatedBy == 0)
                        {
                            data.CreatedBy = User.PersonId;
                        }
                        else data.CreatedBy = result.CreatedBy;
                        if (result.LastUpdatedBy == 0)
                        {
                            data.LastUpdatedBy = User.PersonId;
                        }
                        else data.LastUpdatedBy = result.LastUpdatedBy;
                        var datas = Update<FranchiseAppointment_TypeColors>(ContextFactory.CrmConnectionString, data);
                        return Success;
                    }
                    else
                    {
                        return FailedByError;
                    }
                }
                else
                {
                    return FailedByError;
                }
                //var result = Update(ContextFactory.CrmConnectionString, data);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return FailedByError;
            }
        }

        public override string Add(FranchiseAppointment_TypeColors data)
        {
            throw new NotImplementedException();
        }

        public override string Update(FranchiseAppointment_TypeColors data)
        {
            throw new NotImplementedException();
        }

        public override string Delete(int id)
        {
            throw new NotImplementedException();
        }




        public override ICollection<FranchiseAppointment_TypeColors> Get(List<int> idList)
        {
            throw new NotImplementedException();
        }

        public override FranchiseAppointment_TypeColors Get(int id)
        {
            throw new NotImplementedException();
        }

    }
}
