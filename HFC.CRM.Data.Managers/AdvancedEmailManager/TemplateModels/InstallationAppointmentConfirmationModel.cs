﻿namespace HFC.CRM.Managers.AdvancedEmailManager.TemplateModels
{
    using HFC.CRM.Managers.AdvancedEmailManager.Attributes;

    public class InstallationAppointmentConfirmationModel : BaseEmailTemplateModel
    {
        [CustomField("firstname")]
        public string FirstName { get; set; }

        [CustomField("instdate_first_date")]
        public string InstDateFirstDate { get; set; }

        [CustomField("instdate_first_time")]
        public string InstDateFirstTime { get; set; }

        [CustomField("inst_type_first")]
        public string InstTypeFirst { get; set; }

        [CustomField("instuser_first_firstname")]
        public string InstUserFirstFirstName { get; set; }

        [CustomField("job_outstanding")]
        public string JobOutstanding { get; set; }
    }
}