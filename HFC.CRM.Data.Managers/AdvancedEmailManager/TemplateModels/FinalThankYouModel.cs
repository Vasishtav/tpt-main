﻿namespace HFC.CRM.Managers.AdvancedEmailManager.TemplateModels
{
    using HFC.CRM.Managers.AdvancedEmailManager.Attributes;

    public class FinalThankYouModel: BaseEmailTemplateModel
    {
        [CustomField("firstname")]
        public string FirstName { get; set; }
    }
}