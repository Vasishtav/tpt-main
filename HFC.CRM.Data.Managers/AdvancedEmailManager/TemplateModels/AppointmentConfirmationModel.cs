﻿namespace HFC.CRM.Managers.AdvancedEmailManager.TemplateModels
{
    using HFC.CRM.Managers.AdvancedEmailManager.Attributes;

    public class AppointmentConfirmationModel : BaseEmailTemplateModel
    {
        [CustomField("firstname")]
        public string FirstName { get; set; }

        [CustomField("appointment_time_date")]
        public string AppointmentDate { get; set; }

        [CustomField("appointment_time_time")]
        public string AppointmentTime { get; set; }

        [CustomField("appointment_type")]
        public string AppointmentType { get; set; }

        [CustomField("appointment_username_firstname")]
        public string AppointmentUsernameFirstName { get; set; }

    }
}