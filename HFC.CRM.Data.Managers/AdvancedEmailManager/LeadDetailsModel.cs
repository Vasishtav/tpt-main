﻿namespace HFC.CRM.Managers.AdvancedEmailManager
{
    using HFC.CRM.Managers.AdvancedEmailManager.Attributes;

    public class LeadDetailsModel: BaseEmailTemplateModel
    {
        [CustomField("name")]
        public string name { get; set; }

        [CustomField("job_number")]
        public string job_number { get; set; }

        [CustomField("address")]
        public string Address { get; set; }

        [CustomField("address2")]
        public string Address2 { get; set; }

        [CustomField("city")]
        public string City { get; set; }

        [CustomField("workphone")]
        public string Workphone { get; set; }

        [CustomField("homephone")]
        public string Homephone { get; set; }

        [CustomField("cellphone")]
        public string Cellphone { get; set; }

        [CustomField("job_address")]
        public string JobAddress { get; set; }

        [CustomField("job_primary_source")]
        public string JobPrimarySource { get; set; }

        [CustomField("job_secondary_source")]
        public string JobSecondarySource { get; set; }

        [CustomField("interested_in")]
        public string InterestedIn { get; set; }

        [CustomField("how_many_windows")]
        public string How_many_windows { get; set; }

        [CustomField("how_soon_to_complete_the_project")]
        public string HowSoonToCompleteTheProject { get; set; }

        [CustomField("budget")]
        public string Budget { get; set; }

        [CustomField("job_description")]
        public string JobDescription { get; set; }
    }
}