﻿namespace HFC.CRM.Managers.AdvancedEmailManager
{
    using HFC.CRM.Managers.AdvancedEmailManager.Attributes;

    public class BlankTemplateModel: BaseEmailTemplateModel
    {
        [CustomField("FirstName")]
        public string FirstName { get; set; }

        [CustomField("LastName")]
        public string LastName { get; set; }

        public BlankTemplateModel(string firstName, string lastName)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
        }
    }
}