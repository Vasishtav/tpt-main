﻿namespace HFC.CRM.Managers.AdvancedEmailManager
{
    using HFC.CRM.Managers.AdvancedEmailManager.Attributes;

    public class ThankYouTemplateModel: BaseEmailTemplateModel
    {
        [CustomField("firstname")]
        public string FirstName { get; set; }

        [CustomField("lastname")]
        public string LastName { get; set; }

        public ThankYouTemplateModel(string firstName, string lastName)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
        }
    }
}