﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;

using FluentEmail;
using RazorEngine;
using RazorEngine.Configuration;
using RazorEngine.Templating;
using RazorEngine.Text;
using HFC.CRM.Core.Common;
using System.Reflection;

namespace HFC.CRM.Managers.AdvancedEmailManager
{
    
    public class CustomEmailManager
    {
        private readonly IEmailTemplateProvider _emailTemplateProvider;
        private readonly SentEmailManager _sentEmailManager;
      
        public CustomEmailManager(IEmailTemplateProvider emailTemplateProvider, SentEmailManager sentEmailManager)
        {
            this._emailTemplateProvider = emailTemplateProvider;
            _sentEmailManager = sentEmailManager;
            var config = new TemplateServiceConfiguration { Language = Language.CSharp, EncodedStringFactory = new HtmlEncodedStringFactory() };
            Engine.Razor = RazorEngineService.Create(config);
        }

        public void SendCustomMessage(Int16 emailTemplateTypeId, Int16 emailTemplateId, string[] toAddresses, string[] bccAddresses, string subject, string body, BaseEmailTemplateModel model, List<string> attachments = null, int? leadId = null, int? jobId = null)
        {
            var decodedBody = HttpUtility.HtmlDecode(body);

            //check is exists field in model
            decodedBody = CheckExistsFields(decodedBody, model);
            string compiledBody = string.Empty;

            if (emailTemplateTypeId == 9)
            {
                compiledBody = this.CompileBody(this.CorrectRazorSyntax(decodedBody), model) + "  " + "Job# " + ((AppointmentTemplateModel)model).JobNumber + ",  " + "Attendees: " + ((AppointmentTemplateModel)model).AssignedName + ", " + "Message: " + ((AppointmentTemplateModel)model).Message + ", " + "Date:" + ((AppointmentTemplateModel)model).StartDate;
            }
            else
            {
                // compile body first
                compiledBody = this.CompileBody(this.CorrectRazorSyntax(decodedBody), model);
                //var compiledBody = this.CompileBody(this.CorrectRazorSyntax(decodedBody), model) + "  " + "Job# " + ((AppointmentTemplateModel)model).JobNumber + ",  " + "Attendees: " + ((AppointmentTemplateModel)model).AssignedName + ", " + "Message: " + ((AppointmentTemplateModel)model).Message + ", " + "Date:" + ((AppointmentTemplateModel)model).StartDate;
            }
            // compile layout
            var compiledTemplate = this.CompileTemplateLayout(emailTemplateId, new EmailTemplateLayoutModel() { Body = compiledBody });

           SendMessage(SessionManager.CurrentUser.Person.PrimaryEmail, SessionManager.CurrentUser.Person.FullName, toAddresses, bccAddresses, subject, compiledTemplate, attachments);
            
           // track email
           this._sentEmailManager.AddSentEmail(string.Join(",", toAddresses), subject, compiledBody, templateId: emailTemplateId, leadId: leadId, jobId: jobId);
        }

        private void SendMessage(string fromAddress, string fromName, string[] toAddresses, string[] bccAddresses, string subject, string body, List<string> attachments = null)
        {
            var email = new Email(fromAddress, fromName);

            email.To(toAddresses.Select(v => new MailAddress(v)).ToList()).Subject(subject).BodyAsHtml().Body(body);
            email.BCC(bccAddresses.Select(v => new MailAddress(v)).ToList());
            if (attachments != null && attachments.Any())
            {
                email.Attach(attachments.Select(filename => new Attachment(filename)).ToList());
            }

            email.Send();
            
        }

        private string CompileBody(string body, BaseEmailTemplateModel model)
        {
            return Engine.Razor.RunCompile(body, "body" + model.GetHashCode(), null, model);
        }

        private string CompileTemplateLayout(int templateId, EmailTemplateLayoutModel model)
        {
            var template = this._emailTemplateProvider.Get(templateId);
            var type = (EmailTemplateType)template.EmailTemplateTypeId;
            return Engine.Razor.RunCompile(template.TemplateLayout, type.ToString() + model.GetHashCode(), null, model);
        }

        private string CorrectRazorSyntax(string body)
        {
            if (body == null)
            {
                body = "";
            }
            Func<string, string> convert = s =>
            {
                s = s.Replace("{", "@Model.");
                s = s.Replace("}", string.Empty);
                s = s.Replace(" ", string.Empty);
                return s;
            };

            var regex = new Regex("{.*?}");
            var matches = regex.Matches(body);

            return matches.Cast<Match>().Aggregate(body, (current, match) => current.Replace(match.Value, convert(match.Value)));
        }

        private string CheckExistsFields(string body, BaseEmailTemplateModel model)
        {
            if (body == null)
            {
                body = "";
            }
            var regex = new Regex("{.*?}");
            var matches = regex.Matches(body);

            PropertyInfo[] propertyInfos;
            Type type = model.GetType();

            foreach (var match in matches)
            {
                var fieldName = match.ToString();
                if (string.IsNullOrEmpty(fieldName))
                {
                    continue;
                }
                var matchedStr = fieldName;
                fieldName = fieldName.Replace("}", string.Empty);
                fieldName = fieldName.Replace("{", string.Empty);
                body = body.Replace(matchedStr, "{" + fieldName + "}");
            }

            matches = regex.Matches(body);

            propertyInfos = type.GetProperties();
            foreach (var match in matches)
            {
                var fieldName = match.ToString();
                if (string.IsNullOrEmpty(fieldName))
                {
                    continue;
                }
                var matchedStr = fieldName;
                if (matchedStr.Count(v=>v == '{') > 1)
                {
                    
                }
                fieldName = fieldName.Replace("}", string.Empty);
                fieldName = fieldName.Replace("{", string.Empty);
                fieldName = fieldName.Replace(" ", string.Empty);
                if (propertyInfos.Where(v => v.Name == fieldName).FirstOrDefault() == null)
                {
                    body = body.Replace(matchedStr, string.Empty);
                }
            }
            return body;
        }
    }
}