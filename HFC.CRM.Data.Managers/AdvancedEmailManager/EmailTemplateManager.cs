﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web.Hosting;

using HFC.CRM.Core.Logs;
using HFC.CRM.Core.Membership;
using HFC.CRM.Data;
using HFC.CRM.Managers.AdvancedEmailManager.Attributes;
using HFC.CRM.Managers.AdvancedEmailManager.TemplateModels;
using HFC.CRM.Core;

namespace HFC.CRM.Managers.AdvancedEmailManager 
{
    public class EmailTemplateManager : DataManager
    {
        public static Dictionary<EmailTemplateType, Type> TemplateModelsMap = new Dictionary<EmailTemplateType, Type>()
        {
            {
                EmailTemplateType.ThankYou,
                typeof(ThankYouTemplateModel)
            },
            {
                EmailTemplateType.AppointmentConfirmation,
                typeof(AppointmentConfirmationModel)
            },
            { EmailTemplateType.FinalThankYou, typeof(FinalThankYouModel) },
            {
                EmailTemplateType.InstallationAppointmentConfirmation,
                typeof(InstallationAppointmentConfirmationModel)
            },
            { EmailTemplateType.LeadDetails, typeof(LeadDetailsModel) },
            { EmailTemplateType.NewSaleThankYou, typeof(NewSaleThankYou) },
            { EmailTemplateType.Job, typeof(JobTemplateModel) },
             { EmailTemplateType.Appointment, typeof(AppointmentTemplateModel) },
              { EmailTemplateType.Reminder, typeof(ReminderTemplateModel) }
        };

        public EmailTemplateManager(Franchise franchise, User user)
        {
            this.Franchise = franchise;
            this.AuthorizingUser = user;
        }

        public List<EmailTemplate> GetEmailTemplatesAdmin()
        {
            var templateBrand = 0;
            if (AppConfigManager.BrandedPath.Contains("tl"))
            {
                templateBrand = 1;
            }
            if (AppConfigManager.BrandedPath.Contains("bb"))
            {
                templateBrand = 0;
            }
            if (AppConfigManager.BrandedPath.Contains("cc"))
            {
                templateBrand = 2;
            }
            var emails = this.CRMDBContext.EmailTemplates.Where(c => (c.FranchiseId == this.Franchise.FranchiseId || c.FranchiseId == null) && c.TemplateBrand == templateBrand && (bool)c.IsVisible).AsNoTracking().ToList();
            emails.RemoveAll(c => c.FranchiseId == null && c.EmailTemplateTypeId == (short)EmailTemplateType.Job);
            var updatedTypes = emails.Where(c => c.FranchiseId != null).Select(c => c.EmailTemplateTypeId).ToList();
            emails.RemoveAll(c => updatedTypes.Contains(c.EmailTemplateTypeId) && c.FranchiseId == null);
            return emails.OrderBy(c => c.EmailTemplateId).ToList();
        }

        public EmailTemplate GetEmailTemplateById(int emailTemplateId)
        {
            // try to get custom template, then global
            var emailTemplate = this.CRMDBContext.EmailTemplates.FirstOrDefault(t => t.FranchiseId == this.Franchise.FranchiseId && t.EmailTemplateId == emailTemplateId)
                                ?? this.CRMDBContext.EmailTemplates.FirstOrDefault(t => t.FranchiseId == null && t.EmailTemplateId == emailTemplateId);

            return emailTemplate;
        }

        public string UpdateTemplateType(int emailTemplateId, int emailTemplateTypeId)
        {
            try
            {
                var emailTemplate = this.CRMDBContext.EmailTemplates.FirstOrDefault(t => t.FranchiseId == this.Franchise.FranchiseId && t.EmailTemplateId == emailTemplateId);
                if (emailTemplate == null)
                {
                    throw new Exception(string.Format("Template id {0} is not registered in system", emailTemplateId));
                }
                emailTemplate.EmailTemplateTypeId = (short)emailTemplateTypeId;
                this.CRMDBContext.SaveChanges();

                return Success;
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        public string Update(int emailTemplateId, string subject, string body)
        {
            try
            {
                var emailTemplate = this.CRMDBContext.EmailTemplates.FirstOrDefault(t => t.FranchiseId == this.Franchise.FranchiseId && t.EmailTemplateId == emailTemplateId);
                if (emailTemplate == null)
                {
                    var globalTemplate = this.CRMDBContext.EmailTemplates.FirstOrDefault(t => t.FranchiseId == null && t.EmailTemplateId == emailTemplateId);
                    if (globalTemplate == null)
                    {
                        throw new Exception(string.Format("Template id {0} is not registered in system", emailTemplateId));
                    }

                    emailTemplate = new EmailTemplate()
                                    {
                                        EmailTemplateTypeId = globalTemplate.EmailTemplateTypeId,
                                        FranchiseId = this.Franchise.FranchiseId,
                                        TemplateBody = body,
                                        TemplateLayout = globalTemplate.TemplateLayout,
                                        TemplateSubject = subject,
                                        Description = ((EmailTemplateType)globalTemplate.EmailTemplateTypeId).ToString() + " template",
                                        isDeleted = false,
                                        TemplateBrand = globalTemplate.TemplateBrand,
                                        IsVisible = true
                                    };

                    this.CRMDBContext.EmailTemplates.Add(emailTemplate);
                }
                else
                {
                    emailTemplate.TemplateSubject = subject;
                    emailTemplate.TemplateBody = body;    
                }

                this.CRMDBContext.SaveChanges();

                return Success;
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        public string Add(EmailTemplate model)
        {
            try
            {

                var templateBrand = 0;
                if (AppConfigManager.BrandedPath.Contains("tl"))
                {
                    templateBrand = 1;
                }
                if (AppConfigManager.BrandedPath.Contains("bb"))
                {
                    templateBrand = 0;
                }
                if (AppConfigManager.BrandedPath.Contains("cc"))
                {
                    templateBrand = 2;
                }

                var blankTemplate = this.CRMDBContext.EmailTemplates.FirstOrDefault(t => t.FranchiseId == null && t.EmailTemplateTypeId == (short)EmailTemplateType.Job && t.TemplateBrand == templateBrand);
               
                if (blankTemplate == null)
                {
                    throw new Exception(string.Format("Template type {0} is not registered in system", EmailTemplateType.Job.ToString()));
                }

                var emailTemplate = new EmailTemplate()
                 {
                     EmailTemplateTypeId = (short)model.EmailTemplateTypeId,
                     FranchiseId = this.Franchise.FranchiseId,
                     TemplateBody = blankTemplate.TemplateBody,
                     TemplateLayout = blankTemplate.TemplateLayout,
                     TemplateSubject = blankTemplate.TemplateSubject,
                     Description = ((EmailTemplateType)model.EmailTemplateTypeId).ToString() + " template",
                     isDeleted = false,
                     IsVisible=true,
                     TemplateBrand = templateBrand
                 };

                this.CRMDBContext.EmailTemplates.Add(emailTemplate);

                this.CRMDBContext.SaveChanges();

                return Success;
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        public List<string> GetCustomFieldsByTemplateType(EmailTemplateType emailTemplateType)
        {
            Type t = TemplateModelsMap[emailTemplateType];

            var props = t.GetProperties();

            return (from prop in props
                    select prop.GetCustomAttributes(false)
                        into propattr
                        from attr in
                            (from row in propattr where row.GetType() == typeof(CustomFieldAttribute) select row).ToList()
                        select ((CustomFieldAttribute)attr).FieldName).ToList();
        }

        public void Seed()
        {
            //if (!this.CRMDBContext.EmailTemplates.Any())
            //{
            //    var layout = File.ReadAllText(HostingEnvironment.MapPath(("~/Templates/Email/BBTemplateLayout.cshtml")));
            //   // this.AddGlobal(EmailTemplateType.ThankYou, "Thank You", layout, "Hello {FirstName} {LastName}", false);
            //  //  this.AddGlobal(EmailTemplateType.FinalThankYou, "Template 2", layout, this.ReadBody("BB_Final_Thank_You.txt"), false);
            //    this.AddGlobal(EmailTemplateType.AppointmentConfirmation, "Appointment Confirmation", layout, this.ReadBody("BB_Appointment_Confirmation.txt"), 0);
            //   // this.AddGlobal(EmailTemplateType.LeadDetails, "Lead Details", layout, this.ReadBody("BB_Lead_Details.txt"), false);
            //   // this.AddGlobal(EmailTemplateType.NewSaleThankYou, "Sale Thank You", layout, this.ReadBody("BB_New_Sale_Thank_You.txt"), false);
            //    this.AddGlobal(EmailTemplateType.InstallationAppointmentConfirmation, "Installation Appointment Confirmation", layout, this.ReadBody("BB_Installation_Appointment_Confirmation.txt"), 0);
            //    this.AddGlobal(EmailTemplateType.Job, "Job", layout, this.ReadBody("BB_Blank.txt"), 0);

            //    layout = File.ReadAllText(HostingEnvironment.MapPath(("~/Templates/Email/TLTemplateLayout.cshtml")));
            //    //this.AddGlobal(EmailTemplateType.ThankYou, "Thank You", layout, "Hello {FirstName} {LastName}", true);
            //    //this.AddGlobal(EmailTemplateType.FinalThankYou, "Template 2", layout, this.ReadBody("BB_Final_Thank_You.txt"), true);
            //    this.AddGlobal(EmailTemplateType.AppointmentConfirmation, "Appointment Confirmation", layout, this.ReadBody("TL_Appointment_Confirmation.txt"), 1);
            //    //this.AddGlobal(EmailTemplateType.LeadDetails, "Lead Details", layout, this.ReadBody("BB_Lead_Details.txt"), true);
            //    //this.AddGlobal(EmailTemplateType.NewSaleThankYou, "Sale Thank You", layout, this.ReadBody("BB_New_Sale_Thank_You.txt"), true);
            //    this.AddGlobal(EmailTemplateType.InstallationAppointmentConfirmation, "Installation Appointment Confirmation", layout, this.ReadBody("TL_Installation_Appointment_Confirmation.txt"), 1);
            //    this.AddGlobal(EmailTemplateType.Job, "Job", layout, this.ReadBody("TL_Blank.txt"), 1);

            //    layout = File.ReadAllText(HostingEnvironment.MapPath(("~/Templates/Email/CCTemplateLayout.cshtml")));
            //    //this.AddGlobal(EmailTemplateType.ThankYou, "Thank You", layout, "Hello {FirstName} {LastName}", true);
            //    //this.AddGlobal(EmailTemplateType.FinalThankYou, "Template 2", layout, this.ReadBody("BB_Final_Thank_You.txt"), true);
            //    //this.AddGlobal(EmailTemplateType.AppointmentConfirmation, "Appointment Confirmation", layout, this.ReadBody("TL_Appointment_Confirmation.txt"), 2);
            //    //this.AddGlobal(EmailTemplateType.LeadDetails, "Lead Details", layout, this.ReadBody("BB_Lead_Details.txt"), true);
            //    //this.AddGlobal(EmailTemplateType.NewSaleThankYou, "Sale Thank You", layout, this.ReadBody("BB_New_Sale_Thank_You.txt"), true);
            //    //this.AddGlobal(EmailTemplateType.InstallationAppointmentConfirmation, "Installation Appointment Confirmation", layout, this.ReadBody("TL_Installation_Appointment_Confirmation.txt"), 2);
            //    this.AddGlobal(EmailTemplateType.Job, "Job", layout, this.ReadBody("TL_Blank.txt"), 2);
            //}
            //if (this.CRMDBContext.EmailTemplates.FirstOrDefault(v => v.TemplateBrand == 2) == null)
            //{
            //    var layout1 = File.ReadAllText(HostingEnvironment.MapPath(("~/Templates/Email/CCTemplateLayout.cshtml")));
            //    this.AddGlobal(EmailTemplateType.Job, "Job", layout1, this.ReadBody("TL_Blank.txt"), 2);
            //}
            //SetHRLineMargin();
        }
        
        public void Delete(int emailTemplateId)
        {
            var original = CRMDBContext.EmailTemplates.SingleOrDefault(f => f.EmailTemplateId == emailTemplateId);
            if (original != null)
            {
                this.CRMDBContext.EmailTemplates.Remove(original);

                this.CRMDBContext.SaveChanges();
            }
        }
    }
}
