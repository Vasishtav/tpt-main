﻿namespace HFC.CRM.Managers.AdvancedEmailManager
{
    using HFC.CRM.Managers.AdvancedEmailManager.Attributes;

    public class NewSaleThankYou: BaseEmailTemplateModel
    {
        [CustomField("name")]
        public string Name { get; set; }

        [CustomField("address")]
        public string Address { get; set; }

        [CustomField("city")]
        public string City { get; set; }

        [CustomField("state")]
        public string State { get; set; }

        [CustomField("zip")]
        public string Zip { get; set; }
    }
}