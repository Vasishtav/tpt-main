﻿namespace HFC.CRM.Managers.AdvancedEmailManager
{
    using HFC.CRM.Managers.AdvancedEmailManager.Attributes;

    public class AppointmentTemplateModel : BaseEmailTemplateModel
    {
        [CustomField("Subject")]
        public string Subject { get; set; }

        [CustomField("Location")]
        public string Location { get; set; }

        [CustomField("Message")]
        public string Message { get; set; }

        [CustomField("Start Date")]
        public string StartDate { get; set; }

        [CustomField("End Date")]
        public string EndDate { get; set; }

        [CustomField("Event Type")]
        public string EventType { get; set; }

        [CustomField("Appointment Type")]
        public string AppointmentType { get; set; }

        [CustomField("Job Number")]
        public string JobNumber { get; set; }

        [CustomField("Organizer First Name")]
        public string OrganizerFirstName { get; set; }

        [CustomField("Organizer Last Name")]
        public string OrganizerLastName { get; set; }

        //[CustomField("Organizer MI")]
        public string OrganizerMI { get; set; }

        [CustomField("Organizer Home Phone")]
        public string OrganizerHomePhone { get; set; }

        [CustomField("Organizer Cell Phone")]
        public string OrganizerCellPhone { get; set; }

        [CustomField("Organizer Company Name")]
        public string OrganizerCompanyName { get; set; }

        [CustomField("Organizer Work Title")]
        public string OrganizerWorkTitle { get; set; }

        [CustomField("Organizer Work Phone")]
        public string OrganizerWorkPhone { get; set; }

        [CustomField("Organizer Fax Phone")]
        public string OrganizerFaxPhone { get; set; }

        [CustomField("Organizer Email")]
        public string OrganizerEmail { get; set; }

        [CustomField("Assigned Name")]
        public string AssignedName { get; set; }

        [CustomField("Franchise Name")]
        public string FranchiseName { get; set; }

        [CustomField("First Name")]
        public string FirstName { get; set; }

        [CustomField("Last Name")]
        public string LastName { get; set; }

        //[CustomField("MI")]
        public string MI { get; set; }

        [CustomField("Home Phone")]
        public string HomePhone { get; set; }

        [CustomField("Cell Phone")]
        public string CellPhone { get; set; }

        [CustomField("Company Name")]
        public string CompanyName { get; set; }

        [CustomField("Work Title")]
        public string WorkTitle { get; set; }

        [CustomField("Work Phone")]
        public string WorkPhone { get; set; }

        [CustomField("Side Mark")]
        public string SideMark { get; set; }

        [CustomField("Hint")]
        public string Hint { get; set; }

        [CustomField("Job Description")]
        public string JobDescription { get; set; }

        [CustomField("Fax Phone")]
        public string FaxPhone { get; set; }

        [CustomField("Billing Address1")]
        public string BillingAddress1 { get; set; }

        [CustomField("Billing Address2")]
        public string BillingAddress2 { get; set; }

        [CustomField("Billing City")]
        public string BillingCity { get; set; }

        [CustomField("Billing State")]
        public string BillingState { get; set; }

        [CustomField("Billing ZipCode")]
        public string BillingZipCode { get; set; }

        [CustomField("Install Address1")]
        public string InstallAddress1 { get; set; }

        [CustomField("Install Address2")]
        public string InstallAddress2 { get; set; }

        [CustomField("Install City")]
        public string InstallCity { get; set; }

        [CustomField("Install State")]
        public string InstallState { get; set; }

        [CustomField("Install ZipCode")]
        public string InstallZipCode { get; set; }

        [CustomField("Subtotal")]
        public string Subtotal { get; set; }


        [CustomField("Discount Total")]
        public string DiscountTotal { get; set; }


        [CustomField("Additional Charges Total")]
        public string AdditionalChargesTotal { get; set; }

        [CustomField("Tax Total")]
        public string TaxTotal { get; set; }
        [CustomField("Total")]
        public string Total { get; set; }

        [CustomField("Balance Due")]
        public string BalanceDue { get; set; }
        [CustomField("Payments Total")]
        public string PaymentsTotal { get; set; }
        [CustomField("Surcharge Total")]
        public string SurchargeTotal { get; set; }
        [CustomField("Tax Percent")]
        public string TaxPercent { get; set; }
        [CustomField("Invoice Number")]
        public string InvoiceNumber { get; set; }

        public AppointmentTemplateModel(AppointmentTemplateDTO dto)
        {
            this.Subject = dto.Subject;
            this.Location = dto.Location;
            this.Message = dto.Message;
            this.StartDate = dto.StartDate;
            this.EndDate = dto.EndDate;
            this.EventType = dto.EventTypeEnum;
            this.AppointmentType = dto.AppointmentType;
            this.JobNumber = dto.JobNumber;
            this.OrganizerFirstName = dto.OrganizerFirstName;
            this.OrganizerLastName = dto.OrganizerLastName;
           // this.OrganizerMI = dto.OrganizerMI;
            this.OrganizerHomePhone = dto.OrganizerHomePhone;
            this.OrganizerCellPhone = dto.OrganizerCellPhone;
            this.OrganizerCompanyName = dto.OrganizerCompanyName;
            this.OrganizerWorkTitle = dto.OrganizerWorkTitle;
            this.OrganizerWorkPhone = dto.OrganizerWorkPhone;
            this.OrganizerFaxPhone = dto.OrganizerFaxPhone;
            this.OrganizerEmail = dto.OrganizerEmail;
            this.AssignedName = dto.AssignedName;
            this.FranchiseName = dto.FranchiseName;
            this.FirstName = dto.FirstName;
            this.LastName = dto.LastName;
            //this.MI = dto.MI;
            this.HomePhone = dto.HomePhone;
            this.CellPhone = dto.CellPhone;
            this.CompanyName = dto.CompanyName;
            this.WorkTitle = dto.WorkTitle;
            this.WorkPhone = dto.WorkPhone;
            this.SideMark = dto.SideMark;
            this.Hint = dto.Hint;
            this.JobDescription = dto.JobDescription;
            this.FaxPhone = dto.FaxPhone;
            this.BillingAddress1 = dto.BillingAddress1;
            this.BillingAddress2 = dto.BillingAddress2;
            this.BillingCity = dto.BillingCity;
            this.BillingState = dto.BillingState;
            this.BillingZipCode = dto.BillingZipCode;
            this.InstallAddress1 = dto.InstallAddress1;
            this.InstallAddress2 = dto.InstallAddress2;
            this.InstallCity = dto.InstallCity;
            this.InstallState = dto.InstallState;
            this.InstallZipCode = dto.InstallZipCode;
            this.Subtotal = dto.Subtotal;
            this.DiscountTotal = dto.DiscountTotal;
            this.AdditionalChargesTotal = dto.AdditionalChargesTotal;
            this.TaxTotal = dto.TaxTotal;
            this.Total = dto.Total;
            this.BalanceDue = dto.BalanceDue;
            this.PaymentsTotal = dto.PaymentsTotal;
            this.SurchargeTotal = dto.SurchargeTotal;
            this.TaxPercent = dto.TaxPercent;
            this.InvoiceNumber = dto.InvoiceNumber;
        }
    }
    public class AppointmentTemplateDTO
    {
        public string Subject { get; set; }

        public string Location { get; set; }

        public string Message { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }

        public string EventTypeEnum { get; set; }

        public string AppointmentType { get; set; }


        public string OrganizerFirstName { get; set; }

        public string OrganizerLastName { get; set; }

        //public string OrganizerMI { get; set; }

        public string OrganizerHomePhone { get; set; }

        public string OrganizerCellPhone { get; set; }

        public string OrganizerCompanyName { get; set; }

        public string OrganizerWorkTitle { get; set; }

        public string OrganizerWorkPhone { get; set; }

        public string OrganizerFaxPhone { get; set; }

        public string OrganizerEmail { get; set; }

        public string AssignedName { get; set; }

        public string FranchiseName { get; set; }

        public string JobNumber { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        //public string MI { get; set; }

        public string HomePhone { get; set; }

        public string CellPhone { get; set; }

        public string CompanyName { get; set; }

        public string WorkTitle { get; set; }

        public string WorkPhone { get; set; }

        public string SideMark { get; set; }

        public string Hint { get; set; }

        public string JobDescription { get; set; }

        public string FaxPhone { get; set; }

        public string BillingAddress1 { get; set; }

        public string BillingAddress2 { get; set; }

        public string BillingCity { get; set; }

        public string BillingState { get; set; }

        public string BillingZipCode { get; set; }

        public string InstallAddress1 { get; set; }

        public string InstallAddress2 { get; set; }

        public string InstallCity { get; set; }

        public string InstallState { get; set; }

        public string InstallZipCode { get; set; }

        public string Subtotal { get; set; }

        public string DiscountTotal { get; set; }

        public string AdditionalChargesTotal { get; set; }

        public string Total { get; set; }

        public string TaxTotal { get; set; }

        public string BalanceDue { get; set; }

        public string PaymentsTotal { get; set; }

        public string SurchargeTotal { get; set; }

        public string TaxPercent { get; set; }

        public string InvoiceNumber { get; set; }
    }
}