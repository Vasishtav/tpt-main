﻿namespace HFC.CRM.Managers.AdvancedEmailManager
{
    public enum EmailTemplateType
    {
        ThankYou = 1,

        AppointmentConfirmation = 2,

        FinalThankYou = 3,

        InstallationAppointmentConfirmation = 4,

        LeadDetails = 5,

        NewSaleThankYou = 6,

        Job = 7,

        Reminder = 8,

        Appointment = 9

    }
}