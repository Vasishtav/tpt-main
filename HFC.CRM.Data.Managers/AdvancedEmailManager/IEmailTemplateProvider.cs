﻿namespace HFC.CRM.Managers.AdvancedEmailManager
{
    using HFC.CRM.Data;

    public interface IEmailTemplateProvider
    {
        EmailTemplate Get(int emailTemplateId);
    }
}