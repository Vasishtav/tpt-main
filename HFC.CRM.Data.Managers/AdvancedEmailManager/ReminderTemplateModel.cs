﻿namespace HFC.CRM.Managers.AdvancedEmailManager
{
    using HFC.CRM.Managers.AdvancedEmailManager.Attributes;

    public class ReminderTemplateModel: BaseEmailTemplateModel
    {
 
        [CustomField("Subject")]
        public string Subject { get; set; }

        [CustomField("Location")]
        public string Location { get; set; }

        [CustomField("Message")]
        public string Message { get; set; }

        [CustomField("Start Date")]
        public string StartDate { get; set; }

        [CustomField("End Date")]
        public string EndDate { get; set; }

        [CustomField("Event Type")]
        public string EventType { get; set; }

        [CustomField("Appointment Type")]
        public string AppointmentType { get; set; }

        [CustomField("Job Number")]
        public string JobNumber { get; set; }

        [CustomField("Organizer First Name")]
        public string OrganizerFirstName { get; set; }

        [CustomField("Organizer Last Name")]
        public string OrganizerLastName { get; set; }

        //[CustomField("Organizer MI")]
        //public string OrganizerMI { get; set; }

        [CustomField("Organizer Home Phone")]
        public string OrganizerHomePhone { get; set; }

        [CustomField("Organizer Cell Phone")]
        public string OrganizerCellPhone { get; set; }

        [CustomField("Organizer Company Name")]
        public string OrganizerCompanyName { get; set; }

        [CustomField("Organizer Work Title")]
        public string OrganizerWorkTitle { get; set; }

        [CustomField("Organizer Work Phone")]
        public string OrganizerWorkPhone { get; set; }

        [CustomField("Organizer Fax Phone")]
        public string OrganizerFaxPhone { get; set; }

        [CustomField("Organizer Email")]
        public string OrganizerEmail { get; set; }

        [CustomField("Assigned Name")]
        public string AssignedName { get; set; }

        [CustomField("Franchise Name")]
        public string FranchiseName { get; set; }


        public ReminderTemplateModel(ReminderTemplateDTO dto)
        {
            this.Subject = dto.Subject;
            this.Location = dto.Location;
            this.Message = dto.Message;
            this.StartDate = dto.StartDate;
            this.EndDate = dto.EndDate;
            this.EventType = dto.EventTypeEnum;
            this.AppointmentType = dto.AppointmentType;
            this.JobNumber = dto.JobNumber;
            this.OrganizerFirstName = dto.OrganizerFirstName;
            this.OrganizerLastName = dto.OrganizerLastName;
            //this.OrganizerMI = dto.OrganizerMI;
            this.OrganizerHomePhone = dto.OrganizerHomePhone;
            this.OrganizerCellPhone = dto.OrganizerCellPhone;
            this.OrganizerCompanyName = dto.OrganizerCompanyName;
            this.OrganizerWorkTitle = dto.OrganizerWorkTitle;
            this.OrganizerWorkPhone = dto.OrganizerWorkPhone;
            this.OrganizerFaxPhone = dto.OrganizerFaxPhone;
            this.OrganizerEmail = dto.OrganizerEmail;
            this.AssignedName = dto.AssignedName;
            this.FranchiseName = dto.FranchiseName;
        }
    }

    public class ReminderTemplateDTO
    {
        public string Subject { get; set; }

        public string Location { get; set; }

        public string Message { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }

        public string EventTypeEnum { get; set; }

        public string AppointmentType { get; set; }

        public string JobNumber { get; set; }

        public string OrganizerFirstName { get; set; }

        public string OrganizerLastName { get; set; }

        //public string OrganizerMI { get; set; }

        public string OrganizerHomePhone { get; set; }

        public string OrganizerCellPhone { get; set; }

        public string OrganizerCompanyName { get; set; }

        public string OrganizerWorkTitle { get; set; }

        public string OrganizerWorkPhone { get; set; }

        public string OrganizerFaxPhone { get; set; }

        public string OrganizerEmail { get; set; }

        public string AssignedName { get; set; }

        public string FranchiseName { get; set; }
    }
}