﻿using Dapper;
using HFC.CRM.Data.Context;
using HFC.CRM.DTO;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Managers
{
    public static class FileTable
    {
        public static FileTableTb GetFileData(string stream_id)
        {

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                string strQuery = "select name as fileName,file_stream as bytes from FileTableTb where stream_id=@stream_id";
                var data = connection.Query<FileTableTb>(strQuery, new { stream_id = stream_id }).FirstOrDefault();
                return data;
            }
        }

        public static async Task<FileTableTb> GetFileDataAsync(string stream_id)
        {

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                string strQuery = "select name as fileName,file_stream as bytes from FileTableTb where stream_id=@stream_id";
                var data = await connection.QueryAsync<FileTableTb>(strQuery, new { stream_id = stream_id });
                return data.FirstOrDefault();
            }

        }

        public static  FileTableTb GetFileDataSync(string stream_id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                string strQuery = "select name as fileName,file_stream as bytes from FileTableTb where stream_id=@stream_id";
                var data = connection.Query<FileTableTb>(strQuery, new { stream_id = stream_id });
                return data.FirstOrDefault();
            }
        }
    }
}
