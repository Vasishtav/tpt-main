﻿using HFC.CRM.Core.Common;
using HFC.CRM.Core.Membership;
using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;
using HFC.CRM.Core.Logs;
using System.Net;
using System.Text.RegularExpressions;

using HFC.CRM.Core;
using HFC.CRM.Data.Context;
using Microsoft.Practices.EnterpriseLibrary.Common.Utility;
using SaveOptions = System.Xml.Linq.SaveOptions;

/// <summary>
/// The Managers namespace.
/// </summary>

namespace HFC.CRM.Managers
{
    using HFC.CRM.DTO.Job;
    using System.Data.SqlClient;
    using HFC.CRM.DTO.Notes;
    using HFC.CRM.DTO.Files;
    using System.Data.Entity;
    using StackExchange.Profiling;
    using Dapper;
    using System.Data.Entity.Core.EntityClient;

    /// <summary>
    /// Class JobsManager.
    /// </summary>
    public class JobsManager : DataManager<Job>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="JobsManager" /> class.
        /// </summary>
        public JobsManager(User user, Franchise franchise)
            : this(user, user, franchise)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="JobsManager" /> class.
        /// </summary>
        /// <param name="authorizingUser">User to use for permission checking, good for handling tasks for another user with less access</param>
        public JobsManager(User authorizingUser, User user, Franchise franchise)
        {
            this.User = user;
            this.AuthorizingUser = authorizingUser;
            this.Franchise = franchise;
        }

        /// <summary>
        /// Gets the base permission.
        /// </summary>
        /// <value>The base permission.</value>
        //public override Permission BasePermission
        //{
        //    get { return PermissionProvider.GetPermission(this.AuthorizingUser, "Jobs"); }
        //}

        /// <summary>
        /// Permission for re-assigning sales and installer
        /// </summary>
        /// <value>The distribution permission.</value>
        public Permission DistributionPermission
        {
            get { return PermissionProvider.GetPermission(this.AuthorizingUser, "Jobs", "Distribution"); }
        }

        /// <summary>
        /// Special permission for leads that are assigned to current user
        /// </summary>
        /// <value>The assigned permission.</value>
        public Permission AssignedPermission
        {
            get { return PermissionProvider.GetSpecialPermission(this.AuthorizingUser, "Jobs", "Assigned"); }
        }

        /// <summary>
        /// Gets Permission for job status
        /// </summary>
        public Permission StatusPermission
        {
            get { return PermissionProvider.GetPermission(this.AuthorizingUser, "Jobs", "Status"); }
        }

        /// <summary>
        /// Gets permission for line item cost
        /// </summary>
        public Permission CostPermission
        {
            get { return PermissionProvider.GetPermission(this.AuthorizingUser, "Jobs", "Cost"); }
        }

        private EntityConnectionStringBuilder EntityCnxStringBuilder = new EntityConnectionStringBuilder
            (System.Configuration.ConfigurationManager
                .ConnectionStrings["CRMContext"].ConnectionString);


        /// <summary>
        /// Gets a list of Lead data by status Ids and created on date
        /// </summary>
        /// <param name="totalRecords">The total records.</param>
        /// <param name="jobStatusIds">The job status ids.</param>
        /// <param name="salesPersonIds">The sales person ids.</param>
        /// <param name="createdOnUtcStart">The created on UTC start.</param>
        /// <param name="createdOnUtcEnd">The created on UTC end.</param>
        /// <param name="searchFilter">The search filter.</param>
        /// <param name="searchTerm">The search term.</param>
        /// <param name="orderBy">The order by.</param>
        /// <param name="orderByDirection">The order by direction.</param>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <returns>List&lt;Job&gt;.</returns>
        /// <exception cref="System.AccessViolationException"></exception>
        public List<Job> Get(
            out int totalRecords,
            List<int> jobStatusIds = null,
            List<int> salesPersonIds = null,
            DateTime? createdOnUtcStart = null,
            DateTime? createdOnUtcEnd = null,
            SearchFilterEnum searchFilter = SearchFilterEnum.Auto_Detect,
            string searchTerm = null,
            string orderBy = null,
            OrderByEnum orderByDirection = OrderByEnum.Desc,
            int pageIndex = 0,
            int pageSize = 25,
            string[] excludeJobStatusNames = null)
        {
            totalRecords = 0;

            if (!BasePermission.CanRead) return new List<Job>();


            if (string.IsNullOrEmpty(orderBy))
                orderBy = "jobnumber"; //set default to createdon date

            using (var db = ContextFactory.Create())
            {



                var linqBuilder = from j in db.Jobs.Include(i => i.JobStatus)
                    where j.IsDeleted == false && j.Lead.FranchiseId == Franchise.FranchiseId
                    select j;

                if (createdOnUtcStart.HasValue)
                    linqBuilder = linqBuilder.Where(l => DbFunctions.TruncateTime(l.CreatedOnUtc) >= createdOnUtcStart);

                if (createdOnUtcEnd.HasValue)
                    linqBuilder = linqBuilder.Where(l => DbFunctions.TruncateTime(l.CreatedOnUtc) <= createdOnUtcEnd);

                if (jobStatusIds != null && jobStatusIds.Count > 0)
                {
                    var ids = jobStatusIds.Cast<int?>().ToList();
                    linqBuilder =
                        linqBuilder.Where(j => ids.Contains(j.JobStatusId) || ids.Contains(j.JobStatus.ParentId));
                }

                if (salesPersonIds != null && salesPersonIds.Count > 0)
                {
                    var ids = salesPersonIds.Cast<int?>();
                    linqBuilder = linqBuilder.Where(j => ids.Contains(j.SalesPersonId));
                }

                if (excludeJobStatusNames != null)
                {
                    var excludeJobStatuseIds =
                        CacheManager.JobStatusTypeCollection.Where(
                            js =>
                                (js.FranchiseId == null || js.FranchiseId == SessionManager.CurrentFranchise.FranchiseId) &&
                                excludeJobStatusNames.Contains(js.Name)).Select(js => js.Id).ToList();
                    excludeJobStatuseIds.AddRange(
                        CacheManager.JobStatusTypeCollection.Where(
                            js =>
                                (js.FranchiseId == null || js.FranchiseId == SessionManager.CurrentFranchise.FranchiseId) &&
                                excludeJobStatuseIds.Any(es => es == js.ParentId))
                            .Select(js => js.Id)
                            .ToList());

                    if (excludeJobStatuseIds.Any())
                    {
                        linqBuilder = linqBuilder.Where(l => excludeJobStatuseIds.All(id => id != l.JobStatusId));
                    }
                }

                List<Job> result = new List<Job>();
                totalRecords = linqBuilder.Count();
                if (totalRecords <= 0) return result;

                switch (orderBy.ToLower())
                {
                    case "customer":
                        linqBuilder = orderByDirection == OrderByEnum.Asc
                            ? linqBuilder.OrderBy(o => o.Customer.FirstName)
                            : linqBuilder.OrderByDescending(o => o.Customer.FirstName);
                        break;
                    case "jobnumber":
                        linqBuilder = orderByDirection == OrderByEnum.Asc
                            ? linqBuilder.OrderBy(o => o.JobNumber)
                            : linqBuilder.OrderByDescending(o => o.JobNumber);
                        break;
                    case "salespersonid":
                        linqBuilder = orderByDirection == OrderByEnum.Asc
                            ? linqBuilder.OrderBy(o => o.SalesPerson.FirstName)
                            : linqBuilder.OrderByDescending(o => o.SalesPerson.FirstName);
                        break;
                    case "jobstatus":
                        linqBuilder = orderByDirection == OrderByEnum.Asc
                            ? linqBuilder.OrderBy(o => o.JobStatus.Name)
                            : linqBuilder.OrderByDescending(o => o.JobStatus.Name);
                        break;
                    case "nettotal":
                        if (orderByDirection == OrderByEnum.Asc)
                            linqBuilder = from j in linqBuilder
                                let q = j.JobQuotes.FirstOrDefault(f => f.IsPrimary)
                                orderby q.NetTotal
                                select j;
                        else
                            linqBuilder = from j in linqBuilder
                                let q = j.JobQuotes.FirstOrDefault(f => f.IsPrimary)
                                orderby q.NetTotal descending
                                select j;
                        break;
                    case "balance":
                        linqBuilder = orderByDirection == OrderByEnum.Asc
                            ? linqBuilder.OrderBy(o => o.Balance)
                            : linqBuilder.OrderByDescending(o => o.Balance);
                        break;
                    default:
                        linqBuilder = orderByDirection == OrderByEnum.Asc
                            ? linqBuilder.OrderBy(o => o.CreatedOnUtc)
                            : linqBuilder.OrderByDescending(o => o.CreatedOnUtc);
                        break;
                }

                List<int> jobIds = linqBuilder.Select(s => s.JobId).Skip(pageIndex*pageSize).Take(pageSize).ToList();

                result = Get(jobIds).ToList();


                if (!string.IsNullOrEmpty(searchTerm))
                {
                    SearchFilterEnum realSearchFilter = searchFilter;
                    int anumber = 0;
                    Match JobNumMatch = null;
                    if (searchFilter == SearchFilterEnum.Auto_Detect)
                    {
                        //jobs that has starting then numbers hyphen and another number, ie: 2304-1 (meaning job number - quote number);
                        Regex jobQuoteExp = new Regex(@"^(\d+)-(\d+)$",
                            System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                        JobNumMatch = jobQuoteExp.Match(searchTerm);

                        if (searchFilter == SearchFilterEnum.Auto_Detect)
                        {
                            var isNumber = int.TryParse(searchTerm, out anumber);
                            var jobMatch = JobNumMatch.Success;
                            result = (from job in result
                                let primCustomer = job.Lead.PrimCustomer
                                let secCustomer = job.Lead.SecCustomer
                                let a = job.Lead.Addresses

                                where
                                (primCustomer != null && primCustomer.FirstName != null &&
                                 primCustomer.FirstName.Contains(searchTerm)) ||
                                (primCustomer != null && primCustomer.LastName != null &&
                                 primCustomer.LastName.Contains(searchTerm)) ||
                                (primCustomer.FirstName + " " + primCustomer.LastName).Contains(searchTerm) ||
                                //(secPerson != null && secPerson.FirstName.Contains(searchTerm)) ||
                                //(secPerson != null && secPerson.LastName.Contains(searchTerm)) ||
                                //(secPerson != null &&
                                // (secPerson.FirstName + " " + secPerson.LastName).Contains(searchTerm)) ||
                                (primCustomer != null && primCustomer.PrimaryEmail != null &&
                                 primCustomer.PrimaryEmail.Contains(searchTerm)) ||
                                (primCustomer.SecondaryEmail != null &&
                                 primCustomer.SecondaryEmail.Contains(searchTerm)) ||
                                //(secPerson != null && secPerson.PrimaryEmail != null &&
                                // secPerson.PrimaryEmail.Contains(searchTerm)) ||
                                //(secPerson != null && secPerson.SecondaryEmail != null &&
                                // secPerson.SecondaryEmail.Contains(searchTerm)) ||
                                (job.BillingAddress != null && job.BillingAddress.Address1 != null &&
                                 job.BillingAddress.Address1.Contains(searchTerm)) ||
                                (job.InstallAddress != null && job.InstallAddress.Address1 != null &&
                                 job.InstallAddress.Address1.Contains(searchTerm)) ||
                                (primCustomer.HomePhone != null && primCustomer.HomePhone.Contains(searchTerm)) ||
                                (primCustomer.CellPhone != null && primCustomer.CellPhone.Contains(searchTerm)) ||
                                (primCustomer.WorkPhone != null && primCustomer.WorkPhone.Contains(searchTerm)) ||
                                (primCustomer.FaxPhone != null && primCustomer.FaxPhone.Contains(searchTerm)) ||
                                //(secPerson != null && secPerson.HomePhone != null &&
                                // secPerson.HomePhone.Contains(searchTerm)) ||
                                //(secPerson != null && secPerson.CellPhone != null &&
                                // secPerson.CellPhone.Contains(searchTerm)) ||
                                //(secPerson != null && secPerson.FaxPhone != null &&
                                // secPerson.FaxPhone.Contains(searchTerm)) ||
                                //(secPerson != null && secPerson.FaxPhone != null &&
                                // secPerson.WorkPhone.Contains(searchTerm))
                                //||
                                (isNumber &&
                                 (jobMatch
                                     ? job.JobNumber == anumber
                                     : job.Lead.LeadNumber == anumber || job.JobNumber == anumber))

                                select job).ToList();
                        }
                    }

                    switch (realSearchFilter)
                    {
                        case SearchFilterEnum.Address:
                            //TODO, this will not match if the searchterm is a mix of address1, city, state, etc.
                            result = (from l in result
                                from a in l.Lead.Addresses
                                where (a.Address1 != null && a.Address1.StartsWith(searchTerm)) ||
                                      (a.Address2 != null && a.Address2.StartsWith(searchTerm)) ||
                                      (a.City != null && a.City.StartsWith(searchTerm)) ||
                                      (a.State != null && a.State.StartsWith(searchTerm)) ||
                                      (a.ZipCode != null && a.ZipCode.StartsWith(searchTerm))
                                select l).ToList();
                            break;
                        case SearchFilterEnum.Number:
                            if (int.TryParse(searchTerm, out anumber))
                            {
                                if (JobNumMatch != null && JobNumMatch.Success)
                                    result = (from job in result where job.JobNumber == anumber select job).ToList();
                                else
                                    result =
                                        result.Where(job => job.JobNumber == anumber || job.Lead.LeadNumber == anumber)
                                            .ToList();

                            }
                            break;
                        case SearchFilterEnum.Email:
                            result = (from l in result
                                let p1 = l.Lead.PrimCustomer
                                let p2 = l.Lead.SecCustomer
                                where
                                (p1 != null && p1.PrimaryEmail != null && p1.PrimaryEmail.Contains(searchTerm)) ||
                                (p1 != null && p1.SecondaryEmail != null && p1.SecondaryEmail.Contains(searchTerm)) 
                                //||
                                //(p2 != null && p2.PrimaryEmail != null && p2.PrimaryEmail.Contains(searchTerm)) ||
                                //(p2 != null && p2.SecondaryEmail != null && p2.SecondaryEmail.Contains(searchTerm))
                                select l).ToList();
                            break;
                        case SearchFilterEnum.Phone_Number:
                            string parsedPhone = RegexUtil.GetNumbersOnly(searchTerm);
                            result = result.Where(w =>
                                (w.Lead.PrimCustomer != null && w.Lead.PrimCustomer.HomePhone == parsedPhone) ||
                                (w.Lead.PrimCustomer != null && w.Lead.PrimCustomer.CellPhone == parsedPhone) ||
                                (w.Lead.PrimCustomer != null && w.Lead.PrimCustomer.WorkPhone == parsedPhone) ||
                                (w.Lead.PrimCustomer != null && w.Lead.PrimCustomer.FaxPhone == parsedPhone))
                                //||
                                //(w.Lead.SecPerson != null && w.Lead.SecPerson.HomePhone == parsedPhone) ||
                                //(w.Lead.SecPerson != null && w.Lead.SecPerson.CellPhone == parsedPhone) ||
                                //(w.Lead.SecPerson != null && w.Lead.SecPerson.FaxPhone == parsedPhone) ||
                                //(w.Lead.SecPerson != null && w.Lead.SecPerson.WorkPhone == parsedPhone))
                                .ToList();
                            break;
                        case SearchFilterEnum.Customer_Name:
                            result = (from l in result
                                let p1 = l.Lead.PrimCustomer
                                let p2 = l.Lead.SecCustomer
                                      where (p1.FirstName != null && p1.FirstName.StartsWith(searchTerm)) ||
                                      (p1.LastName != null && p1.LastName.StartsWith(searchTerm)) ||
                                      (p1.FirstName + " " + p1.LastName).Contains(searchTerm) 
                                      //||
                                      //(p2 != null && p2.FirstName != null && p2.FirstName.StartsWith(searchTerm)) ||
                                      //(p2 != null && p2.LastName != null && p2.LastName.StartsWith(searchTerm)) ||
                                      //(p2 != null && (p2.FirstName + " " + p2.LastName).Contains(searchTerm))
                                select l).ToList();
                            break;
                    }
                }

                if (result != null && result.Count > 0)
                {
                    result = PrepareOrderBy(orderBy, orderByDirection, result);
                }

                return result;
            }
        }

        private List<Job> PrepareOrderBy(string orderBy, OrderByEnum orderByDirection, List<Job> result)
        {
            switch (orderBy.ToLower())
            {
                case "customer":
                    result = orderByDirection == OrderByEnum.Asc
                        ? result.OrderBy(o => o.Customer.FirstName).ToList()
                        : result.OrderByDescending(o => o.Customer.FirstName).ToList();
                    break;
                case "jobnumber":
                    result = orderByDirection == OrderByEnum.Asc
                        ? result.OrderBy(o => o.JobNumber).ToList()
                        : result.OrderByDescending(o => o.JobNumber).ToList();
                    break;
                case "salespersonid":
                    if (result.Count(x => x.SalesPersonId == null) == result.Count())
                        break;

                    if (orderByDirection == OrderByEnum.Asc)
                    {
                        var result1 = (from r in result
                            where r.SalesPerson != null
                            join lftjoin in CacheManager.UserCollection on r.SalesPersonId equals
                            lftjoin.PersonId into lft
                            from p in lft.DefaultIfEmpty()
                            orderby ((p != null && p.Person != null) ? p.Person.FirstName : "")
                            select r).ToList();
                        var resultNullList = (from r in result
                            where r.SalesPersonId == null
                            select r).ToList();

                        result = result1.Union(resultNullList).ToList();
                    }
                    else
                    {
                        var result1 = (from r in result
                            where r.SalesPerson != null
                            join lftjoin in CacheManager.UserCollection on r.SalesPersonId equals
                            lftjoin.PersonId into lft
                            from p in lft.DefaultIfEmpty()
                            orderby ((p != null && p.Person != null) ? p.Person.FirstName : "") descending
                            select r).ToList();
                        var resultNullList = (from r in result
                            where r.SalesPersonId == null
                            select r).ToList();

                        result = resultNullList.Union(result1).ToList();
                    }
                    break;
                case "jobstatus":
                    if (orderByDirection == OrderByEnum.Asc)
                    {
                        result = (from r in result
                            join s in CacheManager.JobStatusTypeCollection on r.JobStatusId equals s.Id
                            orderby s.Name
                            select r).ToList();
                    }
                    else
                    {
                        result = (from r in result
                            join s in CacheManager.JobStatusTypeCollection on r.JobStatusId equals s.Id
                            orderby s.Name descending
                            select r).ToList();
                    }
                    break;
                case "nettotal":
                    if (orderByDirection == OrderByEnum.Asc)
                        result = (from j in result
                            let q = j.JobQuotes.FirstOrDefault(f => f.IsPrimary == true)
                            orderby q.NetTotal
                            select j).ToList();
                    else
                        result = (from j in result
                            let q = j.JobQuotes.FirstOrDefault(f => f.IsPrimary == true)
                            orderby q.NetTotal descending
                            select j).ToList();
                    break;
                case "balance":
                    result = orderByDirection == OrderByEnum.Asc
                        ? result.OrderBy(o => o.Balance).ToList()
                        : result.OrderByDescending(o => o.Balance).ToList();
                    break;
                default:
                    result = orderByDirection == OrderByEnum.Asc
                        ? result.OrderBy(o => o.CreatedOnUtc).ToList()
                        : result.OrderByDescending(o => o.CreatedOnUtc).ToList();
                    break;
            }
            return result;
        }

        /// <summary>
        /// Gets a list of jobs by lead ids, (used by lead detail to retrieve list of jobs and quotes)
        /// </summary>
        /// <exception cref="System.ArgumentNullException"></exception>
        /// <exception cref="System.UnauthorizedAccessException"></exception>
        public List<Job> GetJobs(List<int> leadIds)
        {
            using (MiniProfiler.Current.Step("GetJobs"))
            {
                if (leadIds == null || leadIds.Count == 0) throw new ArgumentNullException(JobDoesNotExist);
                if (!BasePermission.CanRead) throw new UnauthorizedAccessException(Unauthorized);

                try
                {
                    using (var db = ContextFactory.Create())
                    {
                        var list =
                            db.Jobs.Include(i => i.PhysicalFiles).Include(i => i.SalesPerson)
                                .Include(i => i.JobQuestionAnswers).Where(
                                    w =>
                                        leadIds.Contains(w.LeadId) && w.Lead.FranchiseId == Franchise.FranchiseId &&
                                        w.IsDeleted == false).OrderByDescending(o => o.JobNumber).ToList();

                        if (list.Count <= 0) return list;

                        var jobIds = list.Select(S => S.JobId).ToList();
                        var activeQuotes =
                            db.JobQuotes.Include(i => i.JobItems)
                                .Include(i => i.Invoices)
                                .Include(i => i.Invoices.Select(x => x.Payments))
                                .Include(i => i.JobItems.Select(x => x.Options))
                                .Include(i => i.SaleAdjustments)
                                .Where(w => w.IsDeleted == false && jobIds.Contains(w.JobId)).ToList();

                        foreach (var job in list)
                        {
                            foreach (var file in job.PhysicalFiles)
                            {
                                var person =
                                    CacheManager.UserCollection.FirstOrDefault(
                                        f => f.PersonId == file.AddedByPersonId);
                                if (person != null) file.AddedByFullName = person.Person.FullName;
                            }
                            job.JobQuotes = activeQuotes.Where(w => w.JobId == job.JobId).ToList();

                            var firstOrDefault = activeQuotes.FirstOrDefault(p => p.JobId == job.JobId && p.IsPrimary);
                            if (firstOrDefault != null)
                                job.Invoices = (activeQuotes.FirstOrDefault(p => p.JobId == job.JobId) != null)
                                    ? firstOrDefault
                                        .Invoices.Where(x => x.IsDeleted == false)
                                        .ToList()
                                    : null;
                            job.CanDistribute = DistributionPermission.CanUpdate;
                            job.CanUpdateStatus = StatusPermission.CanUpdate;
                            job.Permission = new Permission
                            {
                                CanUpdate =
                                    BasePermission.CanUpdate ||
                                    (AssignedPermission.CanUpdate && (job.SalesPersonId == User.PersonId)),
                                CanDelete =
                                    BasePermission.CanDelete ||
                                    (AssignedPermission.CanDelete && (job.SalesPersonId == User.PersonId))
                            };
                            job.CostPermission = new Permission
                            {
                                CanRead = CostPermission.CanRead,
                                CanUpdate = CostPermission.CanUpdate
                            };
                            //clear job cost info incase we have a user with some technical knowledge that can edit the javascript to enable CostPermission.CanRead to true


                            foreach (var q in job.JobQuotes)
                            {
                                if (!CostPermission.CanRead)
                                {
                                    foreach (var item in q.JobItems)
                                    {
                                        item.UnitCost = 0;
                                        item.CostSubtotal = 0;
                                    }
                                    q.NetProfit = 0;
                                }

                                q.JobItems = q.JobItems.OrderBy(o => o.SortOrder).ToList();
                            }

                            decimal paymentSum = 0;
                            if (job.Invoices != null && job.Invoices.Count > 0)
                            {
                                paymentSum = job.Invoices.Where(inv => inv.Payments != null && inv.Payments.Count > 0)
                                    .Aggregate(paymentSum, (current, inv) => current + inv.Payments.Sum(v => v.Amount));
                            }
                            if (job.JobQuotes != null && job.JobQuotes.Count > 0)
                            {
                                job.Balance = job.JobQuotes.FirstOrDefault().NetTotal - paymentSum;
                            }
                        }

                        return list;
                    }
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
            }
        }

        /// <summary>
        /// Gets a list of jobs by lead id, this is for the lead list retrieving jobs per lead when toggled
        /// </summary>
        /// <exception cref="System.ArgumentNullException"></exception>
        /// <exception cref="System.UnauthorizedAccessException"></exception>
        public List<Job> GetJobs(int leadId)
        {
            if (leadId <= 0)
                throw new ArgumentNullException(LeadDoesNotExist);
            if (!BasePermission.CanRead)
                throw new UnauthorizedAccessException(Unauthorized);

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var list =
                        db.Jobs.Where(w =>
                                w.LeadId == leadId
                                && w.Lead.FranchiseId == Franchise.FranchiseId
                                && w.IsDeleted == false)
                            //.Include(i => i.Customer)
                            //.Include(i => i.BillingAddress)//don't need billing address and customer since this is only displaying basic job info
                            .Include(i => i.InstallAddress)
                            .Include(i => i.JobNotes)
                            .OrderByDescending(o => o.JobNumber)
                            .ToList();

                    if (list.Count <= 0) return list;

                    var jobIds = list.Select(s => s.JobId).ToList();

                    var quotes = db.JobQuotes
                        .Where(w => w.IsPrimary && jobIds.Contains(w.JobId)).ToList();

                    foreach (var job in list)
                    {
                        job.JobQuotes = quotes.Where(w => w.JobId == job.JobId).ToList();

                        job.CanDistribute = DistributionPermission.CanUpdate;
                        job.CanUpdateStatus = StatusPermission.CanUpdate;
                        job.Permission = new Permission
                        {
                            CanUpdate =
                                BasePermission.CanUpdate ||
                                (AssignedPermission.CanUpdate && (job.SalesPersonId == User.PersonId)),
                            CanDelete =
                                BasePermission.CanDelete ||
                                (AssignedPermission.CanDelete && (job.SalesPersonId == User.PersonId))
                        };
                        job.CostPermission = new Permission
                        {
                            CanRead = CostPermission.CanRead,
                            CanUpdate = CostPermission.CanUpdate
                        };
                        //clear job cost info incase we have a user with some technical knowledge that can edit the javascript to enable CostPermission.CanRead to true
                        if (!CostPermission.CanRead)
                        {
                            foreach (var q in job.JobQuotes)
                            {
                                foreach (var item in q.JobItems)
                                {
                                    item.UnitCost = 0;
                                    item.CostSubtotal = 0;
                                }
                                q.NetProfit = 0;
                            }
                        }
                        foreach (var note in job.JobNotes)
                        {
                            var person =
                                CacheManager.UserCollection.FirstOrDefault(F => F.PersonId == note.CreatedByPersonId);
                            if (person != null)
                            {
                                note.CreatorFullName = person.Person.FullName;
                                note.AvatarSrc = person.AvatarSrc ?? AppConfigManager.DefaultAvatarSrc;
                            }
                            else
                            {
                                note.CreatorFullName = "";
                                note.AvatarSrc = AppConfigManager.DefaultAvatarSrc;
                            }
                        }
                        job.JobNotes = job.JobNotes.OrderByDescending(o => o.CreatedOn).ToList();

                        // We can avoid another loop as we are anyway looping in the list.
                        decimal paymentSum = 0;
                        if (job.Invoices != null && job.Invoices.Count > 0)
                        {
                            paymentSum = job.Invoices.Where(inv => inv.Payments != null && inv.Payments.Count > 0)
                                .Aggregate(paymentSum, (current, inv) => current + inv.Payments.Sum(v => v.Amount));
                        }

                        if (job.JobQuotes == null || job.JobQuotes.Count <= 0) continue;

                        var firstOrDefault = job.JobQuotes.FirstOrDefault();

                        if (firstOrDefault != null)
                            job.Balance = firstOrDefault.NetTotal - paymentSum;
                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }
        }

        /// <summary>
        /// Get Jobitems by Id to compare it with existing job items to skip processing.
        /// </summary>
        /// <param name="jobItemId">Job Item Id</param>
        /// <returns></returns>
        public List<JobItemOption> GetJobItemOptions(int jobItemId)
        {
            try
            {
                using (var db = ContextFactory.Create())
                {
                    var jobitemOptions = (from ji in db.JobItemOptions
                        where ji.JobItemId == jobItemId
                        select ji).ToList();

                    return jobitemOptions.ToList();
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes a job and it's quotes
        /// </summary>
        /// <param name="id">Job Id</param>
        /// <returns></returns>
        public override string Delete(int id)
        {
            if (id <= 0)
                return "Invalid job id";

            using (var db = ContextFactory.Create())
            {
                var jobCheck =
                    (from l in
                        db.Jobs.Include(i => i.Invoices.Select(inv => inv.Payments))
                            .Include(i => i.JobQuotes.Select(items => items.JobItems))
                            .Include(i => i.Calendars)
                            .Include(i => i.Tasks)
                        where
                            l.JobId == id && l.IsDeleted == false && l.Lead.FranchiseId == Franchise.FranchiseId &&
                            l.Lead.IsDeleted == false
                        select l).SingleOrDefault();

                if (jobCheck == null)
                    return JobDoesNotExist;

                // if any payment related
                if (jobCheck.Invoices.Any() && jobCheck.Invoices.SelectMany(inv => inv.Payments).Any())
                {
                    return "Can't delete Job has Payments";
                }

                // if any payment related
                if (jobCheck.Invoices.Any(v => !v.IsDeleted))
                {
                    return "You must delete the attached invoice to proceed";
                }

                // if any appointmet related
                if (jobCheck.Calendars.Any(c => !c.IsDeleted))
                {
                    return "Can't delete Job has Appointments";
                }

                // if any task related
                if (jobCheck.Tasks.Any(t => !t.IsDeleted))
                {
                    return "Can't delete Job has Tasks";
                }

                //Base permission of can delete (like admin and managers) or assigned permission AND assigned person matches current user
                if (BasePermission.CanDelete ||
                    (AssignedPermission.CanDelete && (jobCheck.SalesPersonId == User.PersonId)))
                {
                    // need to delete all lineitems before
                    foreach (var quote in jobCheck.JobQuotes)
                    {
                        var ids = quote.JobItems.Select(j => j.JobItemId).ToList();
                        foreach (var jobItemId in ids)
                        {
                            DateTimeOffset lastUpdated;
                            var result = this.DeleteJobItem(quote.QuoteId, jobItemId, quote.LastUpdated, out lastUpdated,
                                true);
                            if (result != Success) return result;
                        }
                    }


                    jobCheck.IsDeleted = true;
                    jobCheck.LastUpdated = DateTime.Now;
                    jobCheck.LastUpdatedByPersonId = User.PersonId;

                    db.SaveChanges();

                    return Success;
                }

                return Unauthorized;
            }
        }

        /// <summary>
        /// Updates a job's contract date
        /// </summary>
        /// <param name="jobId"></param>
        /// <param name="contractDate"></param>
        /// <returns></returns>
        public string UpdateContractDate(int jobId, DateTime? contractDate)
        {
            if (jobId <= 0)
                return "Invalid job id";

            using (var db = ContextFactory.Create())
            {

                var jobCheck = (from l in db.Jobs
                    where
                        l.JobId == jobId && l.IsDeleted == false && l.Lead.FranchiseId == Franchise.FranchiseId &&
                        l.Lead.IsDeleted == false
                    select new {l.SalesPersonId, l.ContractedOnUtc}).SingleOrDefault();
                if (jobCheck == null)
                    return JobDoesNotExist;

                //Base permission of can delete (like admin and managers) or assigned permission AND assigned person matches current user
                if (BasePermission.CanUpdate ||
                    (AssignedPermission.CanUpdate && (jobCheck.SalesPersonId == User.PersonId)))
                {
                    var job = new Job
                    {
                        JobId = jobId,
                        ContractedOnUtc = jobCheck.ContractedOnUtc,
                        EditHistories = new List<EditHistory>()
                    };

                    db.Jobs.Attach(job);

                    job.ContractedOnUtc = contractDate;
                    job.LastUpdated = this.GetNow();
                    job.LastUpdatedByPersonId = User.PersonId;

                    var history = new XElement("Job", new XAttribute("operation", "update"),
                        new XElement("ContractDate",
                            new XElement("Original",
                                jobCheck.ContractedOnUtc.HasValue
                                    ? jobCheck.ContractedOnUtc.Value.ToShortDateString()
                                    : ""),
                            new XElement("Current", contractDate.HasValue ? contractDate.Value.ToShortDateString() : "")
                            )
                        );

                    job.EditHistories.Add(new EditHistory
                    {
                        LoggedByPersonId = User.PersonId,
                        IPAddress = HttpContext.Current.Request.UserHostAddress,
                        HistoryValue = history.ToString(SaveOptions.None)
                    });

                    db.SaveChanges();

                    return Success;
                }
                
                return Unauthorized;
            }
        }

        /// <summary>
        /// Updates a job's contract date
        /// </summary>
        /// <param name="JobId"></param>
        /// <param name="quoteDate"></param>
        /// <returns></returns>
        public string UpdateQuoteDate(int JobId, DateTime? quoteDate)
        {
            if (JobId <= 0)
                return "Invalid job id";

            using (var db = ContextFactory.Create())
            {
                var jobCheck = (from l in db.Jobs
                    where
                        l.JobId == JobId && l.IsDeleted == false && l.Lead.FranchiseId == Franchise.FranchiseId &&
                        l.Lead.IsDeleted == false
                    select new {l.SalesPersonId, l.QuoteDateUTC}).SingleOrDefault();
                if (jobCheck == null)
                    return JobDoesNotExist;

                //Base permission of can delete (like admin and managers) or assigned permission AND assigned person matches current user
                if (BasePermission.CanUpdate ||
                    (AssignedPermission.CanUpdate && (jobCheck.SalesPersonId == User.PersonId)))
                {
                    var job = new Job
                    {
                        JobId = JobId,
                        QuoteDateUTC = jobCheck.QuoteDateUTC,
                        EditHistories = new List<EditHistory>()
                    };
                    db.Jobs.Attach(job);

                    job.QuoteDateUTC = quoteDate;
                    job.LastUpdated = this.GetNow();
                    job.LastUpdatedByPersonId = User.PersonId;

                    var history = new XElement("Job", new XAttribute("operation", "update"),
                        new XElement("QuoteDate",
                            new XElement("Original",
                                jobCheck.QuoteDateUTC.HasValue ? jobCheck.QuoteDateUTC.Value.ToShortDateString() : ""),
                            new XElement("Current", quoteDate.HasValue ? quoteDate.Value.ToShortDateString() : "")
                            )
                        );

                    job.EditHistories.Add(new EditHistory
                    {
                        LoggedByPersonId = User.PersonId,
                        IPAddress = HttpContext.Current.Request.UserHostAddress,
                        HistoryValue = history.ToString(SaveOptions.None)
                    });

                    db.SaveChanges();

                    return Success;
                }
                return Unauthorized;
            }
        }

        /// <summary>
        /// Updates a job's contract date
        /// </summary>
        /// <param name="JobId"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public string UpdateDescription(int JobId, string description)
        {
            if (JobId <= 0)
                return "Invalid job id";
            using (var db = ContextFactory.Create())
            {
                var jobCheck = (from l in db.Jobs
                    where
                        l.JobId == JobId && l.IsDeleted == false && l.Lead.FranchiseId == Franchise.FranchiseId &&
                        l.Lead.IsDeleted == false
                    select new {l.SalesPersonId, l.ContractedOnUtc, l.Description}).SingleOrDefault();
                if (jobCheck == null)
                    return JobDoesNotExist;

                //Base permission of can delete (like admin and managers) or assigned permission AND assigned person matches current user
                if (BasePermission.CanUpdate ||
                    (AssignedPermission.CanUpdate && (jobCheck.SalesPersonId == User.PersonId)))
                {
                    var job = new Job
                    {
                        JobId = JobId,
                        ContractedOnUtc = jobCheck.ContractedOnUtc,
                        EditHistories = new List<EditHistory>()
                    };
                    db.Jobs.Attach(job);

                    job.Description = description;
                    job.LastUpdated = this.GetNow();
                    job.LastUpdatedByPersonId = User.PersonId;

                    var history = new XElement("Job", new XAttribute("operation", "update"),
                        new XElement("Description",
                            new XElement("Original", jobCheck.Description),
                            new XElement("Current", description)
                            )
                        );

                    job.EditHistories.Add(new EditHistory
                    {
                        LoggedByPersonId = User.PersonId,
                        IPAddress = HttpContext.Current.Request.UserHostAddress,
                        HistoryValue = history.ToString(SaveOptions.None)
                    });

                    db.SaveChanges();

                    return Success;
                }
                else
                    return Unauthorized;
            }
        }

        /// <summary>
        /// Updates a job's contract date
        /// </summary>
        /// <param name="jobId"></param>
        /// <param name="hint"></param>
        /// <returns></returns>
        public string UpdateHint(int jobId, string hint)
        {
            if (jobId <= 0)
                return "Invalid job id";

            using (var db = ContextFactory.Create())
            {
                var jobCheck = (from l in db.Jobs
                    where
                        l.JobId == jobId && l.IsDeleted == false && l.Lead.FranchiseId == Franchise.FranchiseId &&
                        l.Lead.IsDeleted == false
                    select new {l.SalesPersonId, l.ContractedOnUtc, l.Hint}).SingleOrDefault();
                if (jobCheck == null)
                    return JobDoesNotExist;

                //Base permission of can delete (like admin and managers) or assigned permission AND assigned person matches current user
                if (BasePermission.CanUpdate ||
                    (AssignedPermission.CanUpdate && (jobCheck.SalesPersonId == User.PersonId)))
                {
                    var job = new Job
                    {
                        JobId = jobId,
                        ContractedOnUtc = jobCheck.ContractedOnUtc,
                        EditHistories = new List<EditHistory>()
                    };
                    db.Jobs.Attach(job);

                    job.Hint = hint;
                    job.LastUpdated = this.GetNow();
                    job.LastUpdatedByPersonId = User.PersonId;

                    var history = new XElement("Job", new XAttribute("operation", "update"),
                        new XElement("Hint",
                            new XElement("Original", jobCheck.Hint),
                            new XElement("Current", hint)
                            )
                        );

                    job.EditHistories.Add(new EditHistory
                    {
                        LoggedByPersonId = User.PersonId,
                        IPAddress = HttpContext.Current.Request.UserHostAddress,
                        HistoryValue = history.ToString(SaveOptions.None)
                    });

                    db.SaveChanges();

                    return Success;
                }

                return Unauthorized;
            }
        }

        /// <summary>
        /// Updates a job's contract date
        /// </summary>
        /// <param name="JobId"></param>
        /// <param name="sidemark"></param>
        /// <returns></returns>
        public string UpdateSidemark(int JobId, string sidemark)
        {
            if (JobId <= 0)
                return "Invalid job id";
            using (var db = ContextFactory.Create())
            {
                var jobCheck = (from l in db.Jobs
                    where
                        l.JobId == JobId && l.IsDeleted == false && l.Lead.FranchiseId == Franchise.FranchiseId &&
                        l.Lead.IsDeleted == false
                    select new {l.SalesPersonId, l.ContractedOnUtc, l.SideMark}).SingleOrDefault();
                if (jobCheck == null)
                    return JobDoesNotExist;

                //Base permission of can delete (like admin and managers) or assigned permission AND assigned person matches current user
                if (BasePermission.CanUpdate ||
                    (AssignedPermission.CanUpdate && (jobCheck.SalesPersonId == User.PersonId)))
                {
                    var job = new Job
                    {
                        JobId = JobId,
                        ContractedOnUtc = jobCheck.ContractedOnUtc,
                        EditHistories = new List<EditHistory>()
                    };
                    db.Jobs.Attach(job);

                    job.SideMark = sidemark;
                    job.LastUpdated = this.GetNow();
                    job.LastUpdatedByPersonId = User.PersonId;

                    var history = new XElement("Job", new XAttribute("operation", "update"),
                        new XElement("Sidemark",
                            new XElement("Original", jobCheck.SideMark),
                            new XElement("Current", sidemark)
                            )
                        );

                    job.EditHistories.Add(new EditHistory
                    {
                        LoggedByPersonId = User.PersonId,
                        IPAddress = HttpContext.Current.Request.UserHostAddress,
                        HistoryValue = history.ToString(SaveOptions.None)
                    });

                    db.SaveChanges();

                    return Success;
                }
                else
                    return Unauthorized;
            }
        }

        /// <summary>
        /// Updates a job's contract date
        /// </summary>
        /// <param name="JobId"></param>
        /// <returns></returns>
        public string ConvertToSale(int JobId, out Job outJob)
        {
            outJob = null;

            if (JobId <= 0)
                return "Invalid job id";
            using (var db = ContextFactory.Create())
            {

                var jobCheck = (from l in db.Jobs.Include(i => i.JobStatus)
                    where
                        l.JobId == JobId && l.IsDeleted == false && l.Lead.FranchiseId == Franchise.FranchiseId &&
                        l.Lead.IsDeleted == false
                    select new {l.SalesPersonId, l.ContractedOnUtc, l.SideMark, l.JobStatus}).SingleOrDefault();
                if (jobCheck == null)
                    return JobDoesNotExist;

                //Base permission of can delete (like admin and managers) or assigned permission AND assigned person matches current user
                if (BasePermission.CanUpdate ||
                    (AssignedPermission.CanUpdate && (jobCheck.SalesPersonId == User.PersonId)))
                {
                    var job = new Job
                    {
                        JobId = JobId,
                        ContractedOnUtc = jobCheck.ContractedOnUtc,
                        EditHistories = new List<EditHistory>()
                    };

                    db.Jobs.Attach(job);

                    var saleMadeJobStatus =
                        SessionManager.CurrentFranchise.JobStatusTypeCollection()
                            .FirstOrDefault(f => f.Name == "Sale Made");
                    if (saleMadeJobStatus == null)
                    {
                        throw new Exception("Sale Made job status not found");
                    }

                    job.JobStatusId = saleMadeJobStatus.Id;

                    if (!job.ContractedOnUtc.HasValue)
                    {
                        job.ContractedOnUtc = DateTime.Now;
                    }

                    job.LastUpdated = this.GetNow();
                    job.LastUpdatedByPersonId = User.PersonId;

                    var history = new XElement("JobStatus",
                        new XAttribute("operation", "update"),
                        new XElement("JobStatus", new XElement("Original", jobCheck.JobStatus.Name),
                            new XElement("Current", "Sale Made")));

                    job.EditHistories.Add(new EditHistory
                    {
                        LoggedByPersonId = User.PersonId,
                        IPAddress = HttpContext.Current.Request.UserHostAddress,
                        HistoryValue = history.ToString(SaveOptions.None)
                    });

                    db.SaveChanges();

                    outJob = job;

                    return Success;

                }
                else
                {
                    return Unauthorized;
                }
            }
        }

        /// <summary>
        /// Updates the answers.
        /// </summary>
        /// <param name="jobId">The job identifier.</param>
        /// <param name="answerId">The answer identifier.</param>
        /// <param name="answer">The answer.</param>
        /// <returns>System.String.</returns>
        public string UpdateAnswers(int jobId, int answerId, string answer)
        {
            if (jobId <= 0 || answerId <= 0)
                return DataCannotBeNullOrEmpty;

            try
            {
                using (var db = ContextFactory.Create())
                {

                    var leadtoCheck = (from l in db.Jobs
                        where
                            l.JobId == jobId && l.IsDeleted == false && l.Lead.FranchiseId == Franchise.FranchiseId &&
                            l.Lead.IsDeleted == false
                        select
                            new
                            {
                                l.SalesPersonId,
                                l.InstallerPersonId,
                                QA = l.JobQuestionAnswers.FirstOrDefault(f => f.AnswerId == answerId)
                            }).FirstOrDefault();
                    if (leadtoCheck == null)
                        return JobDoesNotExist;
                    if (leadtoCheck.QA == null)
                        return "Record not found";

                    //Base permission of can update (like admin and managers) or assigned permission AND assigned person matches current user
                    if (BasePermission.CanUpdate ||
                        (AssignedPermission.CanUpdate && (leadtoCheck.SalesPersonId == User.PersonId)))
                    {
                        var job = new Job {JobId = jobId, EditHistories = new List<EditHistory>()};

                        db.Jobs.Attach(job);

                        var question =
                            CacheManager.QuestionTypeCollection.FirstOrDefault(
                                f => f.QuestionId == leadtoCheck.QA.QuestionId);
                        string cur = "Question and Answer";
                        if (question != null)
                            cur = string.Format("{0} {1}", question.Question, question.SubQuestion);

                        XElement history =
                            new XElement("Job",
                                new XElement("JobQuestionAnswer", new XAttribute("name", cur),
                                    new XAttribute("operation", "update"),
                                    new XElement("Original", leadtoCheck.QA.Answer),
                                    new XElement("Current", answer))
                                );

                        leadtoCheck.QA.Answer = answer;
                        job.LastUpdatedByPersonId = User.PersonId;
                        job.LastUpdated = this.GetNow();
                        job.EditHistories.Add(new EditHistory()
                        {
                            IPAddress = HttpContext.Current.Request.UserHostAddress,
                            LoggedByPersonId = User.PersonId,
                            HistoryValue = history.ToString(System.Xml.Linq.SaveOptions.DisableFormatting)
                        });

                        db.SaveChanges();
                        return Success;

                    }

                    return Unauthorized;
                }

            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }

        }

        /// <summary>
        /// Adds the answer.
        /// </summary>
        /// <param name="JobId">The job identifier.</param>
        /// <param name="questionId">The question identifier.</param>
        /// <param name="answer">The answer.</param>
        /// <param name="answerId">The answer identifier.</param>
        /// <returns>System.String.</returns>
        public string AddAnswer(int JobId, int questionId, string answer, out int answerId)
        {
            answerId = 0;
            if (JobId <= 0 || questionId <= 0 || string.IsNullOrEmpty(answer))
                return DataCannotBeNullOrEmpty;

            try
            {

                using (var db = ContextFactory.Create())
                {

                    var originaljob = (from j in db.Jobs
                        where
                            j.JobId == JobId && j.Lead.FranchiseId == Franchise.FranchiseId && j.IsDeleted == false &&
                            j.Lead.IsDeleted == false
                        select
                            new
                            {
                                j.SalesPersonId,
                                j.InstallerPersonId,
                                QAnswer = j.JobQuestionAnswers.FirstOrDefault(w => w.QuestionId == questionId)
                            })
                        .FirstOrDefault();
                    if (originaljob == null)
                        return JobDoesNotExist;
                    if (originaljob.QAnswer != null)
                        return "Duplicate question found for this job";

                    if (BasePermission.CanUpdate ||
                        (AssignedPermission.CanUpdate && (originaljob.SalesPersonId == User.PersonId)))
                    {
                        var job = new Job
                        {
                            JobId = JobId,
                            JobQuestionAnswers = new List<JobQuestionAnswer>(),
                            EditHistories = new List<EditHistory>()
                        };
                        db.Jobs.Attach(job);
                        job.LastUpdatedByPersonId = User.PersonId;
                        job.LastUpdated = this.GetNow();

                        var jobanswer = new JobQuestionAnswer
                        {
                            QuestionId = questionId,
                            Answer = answer,
                            LastUpdatedOnUtc = DateTime.Now,
                            LoggedByPersonId = User.PersonId
                        };
                        job.JobQuestionAnswers.Add(jobanswer);

                        var question =
                            CacheManager.QuestionTypeCollection.FirstOrDefault(f => f.QuestionId == questionId);
                        string cur = "Question and Answer";
                        if (question != null)
                            cur = string.Format("{0} {1}", question.Question, question.SubQuestion);

                        var history = new XElement("Job",
                            new XElement("JobQuestionAnswer", new XAttribute("name", cur),
                                new XAttribute("operation", "insert"),
                                new XElement("Current", answer))
                            );
                        job.EditHistories.Add(new EditHistory
                        {
                            LoggedByPersonId = User.PersonId,
                            HistoryValue = history.ToString(SaveOptions.DisableFormatting),
                            IPAddress = HttpContext.Current.Request.UserHostAddress
                        });

                        db.SaveChanges();
                        answerId = jobanswer.AnswerId;
                        return Success;
                    }
                    else
                        return Unauthorized;
                }
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        /// <summary>
        /// Updates the installer.
        /// </summary>
        /// <param name="jobId">The job identifier.</param>
        /// <param name="personId">The person identifier.</param>
        /// <returns>System.String.</returns>
        public string UpdateInstaller(int jobId, int personId)
        {
            //Base permission of can update (like admin and managers) or assigned permission AND assigned person matches current user
            if (!DistributionPermission.CanUpdate)
                return Unauthorized;
            if (jobId <= 0)
                return DataCannotBeNullOrEmpty;

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var job =
                        db.Jobs.Where(
                            w => w.JobId == jobId && w.IsDeleted == false && w.Lead.FranchiseId == Franchise.FranchiseId)
                            .Select(s => new {PersonId = s.InstallerPersonId}).FirstOrDefault();

                    if (job == null)
                        return JobDoesNotExist;

                    //original has installer assigned and the new assign installer is different person
                    //or if original doesnt have installer and has new installer to assign
                    if ((job.PersonId.HasValue && job.PersonId.Value != personId) ||
                        (!job.PersonId.HasValue && personId > 0))
                    {
                        string oldFullname = "Unassigned", newFullName = "Unassigned";
                        if (job.PersonId.HasValue)
                        {
                            var person =
                                CacheManager.UserCollection.FirstOrDefault(f => f.PersonId == job.PersonId.Value);
                            oldFullname = person != null ? person.Person.FullName : "";
                        }

                        if (personId > 0)
                        {
                            var current = CacheManager.UserCollection.FirstOrDefault(f => f.PersonId == personId);
                            if (current != null)
                                newFullName = current.Person.FullName;
                        }

                        var xmlElement =
                            new XElement("Job",
                                new XElement("InstallerPersonId", new XAttribute("name", "Installer"),
                                    new XAttribute("operation", "update"),
                                    new XElement("Original",
                                        new XAttribute("id",
                                            job.PersonId.HasValue ? job.PersonId.Value.ToString() : "null"),
                                        oldFullname),
                                    new XElement("Current",
                                        new XAttribute("id", personId > 0 ? personId.ToString() : "null"), newFullName)
                                    )
                                );

                        var jobModelToUpdate = new Job {JobId = jobId, InstallerPersonId = job.PersonId};
                        db.Jobs.Attach(jobModelToUpdate);

                        //set new installer
                        if (personId > 0)
                            jobModelToUpdate.InstallerPersonId = personId;
                        else
                            jobModelToUpdate.InstallerPersonId = null;

                        jobModelToUpdate.LastUpdated = this.GetNow();
                        jobModelToUpdate.LastUpdatedByPersonId = User.PersonId;
                        jobModelToUpdate.EditHistories.Add(new EditHistory()
                        {
                            HistoryValue = xmlElement.ToString(System.Xml.Linq.SaveOptions.DisableFormatting),
                            LoggedByPersonId = User.PersonId,
                            IPAddress = HttpContext.Current.Request.UserHostAddress
                        });

                        db.SaveChanges();
                        return Success;

                    }
                    else //no change so return success
                        return Success;
                }

            }
            catch (Exception ex)
            {
#if DEBUG
                return ex.Message;
#else
                return EventLogger.LogEvent(ex);
#endif
            }
        }

        /// <summary>
        /// Updates the sales representative.
        /// </summary>
        /// <param name="jobId">The job identifier.</param>
        /// <param name="personId">The person identifier.</param>
        /// <returns>System.String.</returns>
        public string UpdateSalesRepresentative(int jobId, int personId)
        {
            //Base permission of can update (like admin and managers) or assigned permission AND assigned person matches current user
            if (!DistributionPermission.CanUpdate)
                return Unauthorized;
            if (jobId <= 0)
                return DataCannotBeNullOrEmpty;

            try
            {
                using (var db = ContextFactory.Create())
                {

                    var job =
                        db.Jobs.Where(
                            w => w.JobId == jobId && w.IsDeleted == false && w.Lead.FranchiseId == Franchise.FranchiseId)
                            .Select(s => new {PersonId = s.SalesPersonId}).FirstOrDefault();

                    if (job == null)
                        return JobDoesNotExist;

                    //original has installer assigned and the new assign installer is different person
                    //or if original doesnt have installer and has new installer to assign
                    if ((job.PersonId.HasValue && job.PersonId.Value != personId) ||
                        (!job.PersonId.HasValue && personId > 0))
                    {
                        string oldFullname = "Unassigned", newFullName = "Unassigned";
                        if (job.PersonId.HasValue)
                        {
                            var person =
                                CacheManager.UserCollection.FirstOrDefault(f => f.PersonId == job.PersonId.Value);
                            oldFullname = person != null ? person.Person.FullName : "";
                        }

                        if (personId > 0)
                        {
                            var current = CacheManager.UserCollection.FirstOrDefault(f => f.PersonId == personId);
                            if (current != null)
                                newFullName = current.Person.FullName;
                        }

                        var xmlElement =
                            new XElement("Job",
                                new XElement("SalesPersonId", new XAttribute("name", "Sales Person"),
                                    new XAttribute("operation", "update"),
                                    new XElement("Original",
                                        new XAttribute("id",
                                            job.PersonId.HasValue ? job.PersonId.Value.ToString() : "null"),
                                        oldFullname),
                                    new XElement("Current",
                                        new XAttribute("id", personId > 0 ? personId.ToString() : "null"), newFullName)
                                    )
                                );

                        var jobModelToUpdate = new Job {JobId = jobId, SalesPersonId = job.PersonId};
                        db.Jobs.Attach(jobModelToUpdate);

                        //set new installer
                        if (personId > 0)
                            jobModelToUpdate.SalesPersonId = personId;
                        else
                            jobModelToUpdate.SalesPersonId = null;

                        jobModelToUpdate.LastUpdated = this.GetNow();
                        jobModelToUpdate.LastUpdatedByPersonId = User.PersonId;

                        jobModelToUpdate.EditHistories.Add(new EditHistory()
                        {
                            HistoryValue = xmlElement.ToString(System.Xml.Linq.SaveOptions.DisableFormatting),
                            LoggedByPersonId = User.PersonId,
                            IPAddress = HttpContext.Current.Request.UserHostAddress
                        });

                        db.SaveChanges();
                        return Success;
                    }
                    return Success;
                }

            }
            catch (Exception ex)
            {
#if DEBUG
                return ex.Message;
#else                    
                return EventLogger.LogEvent(ex);
#endif
            }

        }

        /// <summary>
        /// Updates the concept.
        /// </summary>
        /// <param name="jobId">The job identifier.</param>
        /// <param name="conceptId">The concept identifier.</param>
        /// <returns>System.String.</returns>
        public string UpdateConcept(int jobId, int conceptId)
        {
            if (jobId <= 0 || conceptId <= 0)
                return DataCannotBeNullOrEmpty;

            try
            {
                using (var db = ContextFactory.Create())
                {

                    //we pull the assigned sales and installer to check if they have permission to update
                    var originalInfo =
                        db.Jobs.Where(
                            w =>
                                w.JobId == jobId && w.IsDeleted == false && w.Lead.FranchiseId == Franchise.FranchiseId &&
                                w.Lead.IsDeleted == false)
                            .Select(s => new {s.SalesPersonId, s.InstallerPersonId, s.JobConceptId}).FirstOrDefault();

                    if (originalInfo == null)
                        return JobDoesNotExist;

                    //Base permission of can update (like admin and managers) or assigned permission AND assigned person matches current user
                    if (BasePermission.CanUpdate ||
                        (AssignedPermission.CanUpdate && (originalInfo.SalesPersonId == User.PersonId)))
                    {
                        var oldconcept =
                            CacheManager.ConceptCollection.FirstOrDefault(f => f.Id == originalInfo.JobConceptId);
                        var newconcept = CacheManager.ConceptCollection.FirstOrDefault(f => f.Id == conceptId);
                        var history =
                            new XElement("Job",
                                new XElement("JobConceptId", new XAttribute("name", "Concept"),
                                    new XAttribute("operation", "update"),
                                    new XElement("Original", new XAttribute("id", oldconcept.Id), oldconcept.Concept),
                                    new XElement("Current", new XAttribute("id", newconcept.Id), newconcept.Concept)
                                    )
                                );

                        var job = new Job {JobId = jobId, JobConceptId = originalInfo.JobConceptId};
                        db.Jobs.Attach(job);
                        job.JobConceptId = newconcept.Id;
                        job.LastUpdated = this.GetNow();
                        job.LastUpdatedByPersonId = User.PersonId;
                        job.EditHistories.Add(new EditHistory()
                        {
                            HistoryValue = history.ToString(SaveOptions.DisableFormatting),
                            LoggedByPersonId = User.PersonId,
                            IPAddress = HttpContext.Current.Request.UserHostAddress
                        });

                        db.SaveChanges();
                        return Success;

                    }
                    else
                        return Unauthorized;
                }

            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }

        }

        /// <summary>
        /// Uploads the file.
        /// </summary>
        /// <param name="JobId">The job identifier.</param>
        /// <param name="description"></param>
        /// <param name="Description">The description.</param>
        /// <param name="file">The file.</param>
        /// <param name="fileId">The file identifier.</param>
        /// <param name="filename">The filename.</param>
        /// <param name="url">The URL.</param>
        /// <param name="thmbUrl">The THMB URL.</param>
        /// <returns>System.String.</returns>
        public string UploadFile(int JobId, string description, string category, HttpPostedFileBase file, out int fileId,
            out string filename, out string url, out string thmbUrl)
        {
            filename = file.FileName;
            fileId = 0;
            url = null;
            thmbUrl = null;

            #region  -- File Attachment -- 
            /* Added condition for using same controller for File attachment 
                For any file attachment purpose. */
            if (JobId == -5)
            {
                //Temporary file attachment folder
                string tmpAttachFolder = "tmpAttachments";
                if (string.IsNullOrEmpty(filename))
                    return InvalidFilePath;

                //Build common base url and path base to store attached file
                string urlBaseuf = string.Format("/files/{0}/{1}", Franchise.Code, tmpAttachFolder);
                string pathBaseuf = HttpContext.Current.Server.MapPath(urlBaseuf);

                string filePathuf = string.Format("{0}\\{1}", pathBaseuf, filename);
                short? Widthuf = null, Heightuf = null;

                //IF file already exists - Rename to new file 
                if (System.IO.File.Exists(filePathuf))
                {
                    //Append timestamp and save file instead of warning duplicate
                    int lastperioduf = file.FileName.LastIndexOf('.');
                    if (lastperioduf < 0)
                        lastperioduf = file.FileName.Length;

                    filename = file.FileName.Insert(lastperioduf,
                        ((int)DateTime.Now.TimeOfDay.TotalSeconds).ToString());

                    filePathuf = string.Format("{0}\\{1}", pathBaseuf, filename);
                }

                //USED for linq since it doesnt like out or ref data type
                if (!Directory.Exists(pathBaseuf))
                    Directory.CreateDirectory(pathBaseuf);

                url = string.Format("{0}/{1}", urlBaseuf, filename);
                thmbUrl = null;

                //FOR image - create a thumbnail
                if (file.ContentType.Contains("image"))
                {
                    string thmbFilenameuf = "";
                    //Make sure extension is in jpg
                    int lastperioduf = filename.LastIndexOf('.');
                    if (lastperioduf > 0)
                        thmbFilenameuf = filename.Substring(0, lastperioduf) + "-thumb.jpg";
                    else
                        thmbFilenameuf = filename + "-thumb.jpg";

                    thmbUrl = string.Format("{0}/{1}", urlBaseuf, thmbFilenameuf);

                    try
                    {
                        //Adding Try/Catch, may not be a recognizable image
                        var imguf = ImageUtil.SaveThumbnail(file.InputStream,
                            HttpContext.Current.Server.MapPath(thmbUrl));
                        Heightuf = (short)imguf.Height;
                        Widthuf = (short)imguf.Width;
                    }
                    catch
                    {
                        thmbUrl = null;
                    }

                }

                //Save files to HDD
                file.SaveAs(filePathuf);

                //Return related files
                filename = filePathuf;
                thmbUrl = filePathuf;

                return Success;
            }
            #endregion  -- File Attachment --

            if (JobId <= 0)
                return DataCannotBeNullOrEmpty;
            if (string.IsNullOrEmpty(filename))
                return InvalidFilePath;
            try
            {
                using (var db = ContextFactory.Create())
                {
                    var jobToCheck = (from j in db.Jobs
                        where
                            j.JobId == JobId && j.IsDeleted == false && j.Lead.FranchiseId == Franchise.FranchiseId &&
                            j.Lead.IsDeleted == false
                        select new {j.JobNumber, j.SalesPersonId, j.InstallerPersonId}).FirstOrDefault();

                    if (jobToCheck == null)
                        return JobDoesNotExist;

                    string urlBase = string.Format("/files/{0}/{1}", Franchise.Code, jobToCheck.JobNumber);
                    string pathBase = HttpContext.Current.Server.MapPath(urlBase);

                    string filePath = string.Format("{0}\\{1}", pathBase, filename);
                    short? Width = null, Height = null;

                    //if file already exists then we rename new file 
                    if (System.IO.File.Exists(filePath))
                    {
                        //we'll just append timestamp and save file instead of warning duplicate
                        int lastperiod = file.FileName.LastIndexOf('.');
                        if (lastperiod < 0)
                            lastperiod = file.FileName.Length;

                        filename = file.FileName.Insert(lastperiod,
                            ((int) DateTime.Now.TimeOfDay.TotalSeconds).ToString());
                        filePath = string.Format("{0}\\{1}", pathBase, filename);
                    }

                    //used for linq since it doesnt like out or ref data type
                    string tempFileName = filename;
                    var existLinq = (from j in db.Jobs
                        from f in j.PhysicalFiles
                        where j.JobId == JobId && f.FileName == tempFileName
                        select f.PhysicalFileId);
                    if (existLinq.Any())
                        return DuplicateFileName;

                    //Base permission of can update (like admin and managers) or assigned permission AND assigned person matches current user
                    if (BasePermission.CanUpdate ||
                        (AssignedPermission.CanUpdate && (jobToCheck.SalesPersonId == User.PersonId)))
                    {
                        if (!Directory.Exists(pathBase))
                            Directory.CreateDirectory(pathBase);

                        url = string.Format("{0}/{1}", urlBase, filename);
                        thmbUrl = null;

                        //if it is an image then we create a thumbnail
                        if (file.ContentType.Contains("image"))
                        {
                            string thmbFilename = "";
                            //make sure extension is in jpg
                            int lastperiod = filename.LastIndexOf('.');
                            if (lastperiod > 0)
                                thmbFilename = filename.Substring(0, lastperiod) + "-thumb.jpg";
                            else
                                thmbFilename = filename + "-thumb.jpg";

                            thmbUrl = string.Format("{0}/{1}", urlBase, thmbFilename);

                            try
                            {
                                //put in a try, since this may not be a recognizable image
                                var img = ImageUtil.SaveThumbnail(file.InputStream,
                                    HttpContext.Current.Server.MapPath(thmbUrl));
                                Height = (short) img.Height;
                                Width = (short) img.Width;
                            }
                            catch
                            {
                                thmbUrl = null;
                            }
                        }

                        //save to HDD
                        file.SaveAs(filePath);

                        #region save to DB

                        var job = new Job
                        {
                            JobId = JobId,
                            PhysicalFiles = new List<PhysicalFile>(),
                            EditHistories = new List<EditHistory>()
                        };

                        db.Jobs.Attach(job);

                        db.SaveChanges();


                        var physicalFile = new PhysicalFile
                        {
                            FileName = filename,
                            PhysicalFileGuid = Guid.NewGuid(),
                            MimeType = file.ContentType,
                            FileDescription = description,
                            FileCategory = category,
                            UrlPath = url,
                            ThumbUrl = thmbUrl,
                            IsRelativeUrlPath = true,
                            CreatedOnUtc = DateTime.Now,
                            AddedByPersonId = User.PersonId,
                            FileSize = file.ContentLength,
                            Width = Width,
                            Height = Height
                        };
                        job.PhysicalFiles.Add(physicalFile);

                        db.SaveChanges();

                        XElement history =
                            new XElement("File", new XAttribute("operation", "insert"),
                                new XElement("Filename", new XAttribute("name", "File name"),
                                    new XElement("Current", filename)
                                    )
                                );


                        job.LastUpdatedByPersonId = User.PersonId;
                        job.LastUpdated = DateTime.Now;
                        job.EditHistories.Add(new EditHistory
                        {
                            LoggedByPersonId = User.PersonId,
                            IPAddress = HttpContext.Current.Request.UserHostAddress,
                            HistoryValue = history.ToString(System.Xml.Linq.SaveOptions.DisableFormatting)
                        });

                        db.SaveChanges();

                        if (physicalFile.PhysicalFileId > 0)
                            fileId = physicalFile.PhysicalFileId;

                        return Success;

                        #endregion
                    }
                    else
                        return Unauthorized;
                }

            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }

        }

        public string UploadFile(int JobId, string description, string category, byte[] fileBytes, string fileName,
            out int fileId, out string filename, out string url)
        {
            fileId = 0;
            url = null;
            filename = null;

            if (JobId <= 0)
                return DataCannotBeNullOrEmpty;

            if (string.IsNullOrEmpty(fileName))
                return InvalidFilePath;
            try
            {
                using (var db = ContextFactory.Create())
                {

                    var jobToCheck = (from j in db.Jobs
                        where
                            j.JobId == JobId && j.IsDeleted == false && j.Lead.FranchiseId == Franchise.FranchiseId &&
                            j.Lead.IsDeleted == false
                        select new {j.JobNumber, j.SalesPersonId, j.InstallerPersonId}).FirstOrDefault();

                    if (jobToCheck == null)
                        return JobDoesNotExist;

                    string urlBase = string.Format("/files/{0}/{1}", Franchise.Code, jobToCheck.JobNumber);
                    string pathBase = HttpContext.Current.Server.MapPath(urlBase);

                    string filePath = string.Format("{0}\\{1}", pathBase, fileName);

                    //if file already exists then we rename new file 
                    if (File.Exists(filePath))
                    {
                        //we'll just append timestamp and save file instead of warning duplicate
                        int lastperiod = fileName.LastIndexOf('.');
                        if (lastperiod < 0)
                            lastperiod = fileName.Length;

                        filename = fileName.Insert(lastperiod, ((int) DateTime.Now.TimeOfDay.TotalSeconds).ToString());
                        filePath = string.Format("{0}\\{1}", pathBase, filename);
                    }

                    //used for linq since it doesnt like out or ref data type
                    string tempFileName = fileName;
                    var existLinq = (from j in db.Jobs
                        from f in j.PhysicalFiles
                        where j.JobId == JobId && f.FileName == tempFileName
                        select f.PhysicalFileId);
                    if (existLinq.Any())
                        return DuplicateFileName;

                    //Base permission of can update (like admin and managers) or assigned permission AND assigned person matches current user
                    if (BasePermission.CanUpdate ||
                        (AssignedPermission.CanUpdate && (jobToCheck.SalesPersonId == User.PersonId)))
                    {
                        if (!Directory.Exists(pathBase))
                            Directory.CreateDirectory(pathBase);

                        url = string.Format("{0}/{1}", urlBase, fileName);

                        //save to HDD
                        File.WriteAllBytes(filePath, fileBytes);

                        #region save to DB

                        var job = new Job
                        {
                            JobId = JobId,
                            PhysicalFiles = new List<PhysicalFile>(),
                            EditHistories = new List<EditHistory>()
                        };
                        db.Jobs.Attach(job);

                        db.SaveChanges();

                        var physicalFile = new PhysicalFile
                        {
                            FileName = fileName,
                            PhysicalFileGuid = Guid.NewGuid(),
                            MimeType = "application/pdf",
                            FileDescription = description,
                            FileCategory = category,
                            UrlPath = url,
                            IsRelativeUrlPath = true,
                            CreatedOnUtc = DateTime.Now,
                            AddedByPersonId = User.PersonId,
                            FileSize = fileBytes.Length,
                        };

                        job.PhysicalFiles.Add(physicalFile);

                        db.SaveChanges();

                        var history =
                            new XElement("File", new XAttribute("operation", "insert"),
                                new XElement("Filename", new XAttribute("name", "File name"),
                                    new XElement("Current", fileName)
                                    )
                                );

                        job.LastUpdatedByPersonId = User.PersonId;
                        job.LastUpdated = DateTime.Now;
                        job.EditHistories.Add(new EditHistory
                        {
                            LoggedByPersonId = User.PersonId,
                            IPAddress = HttpContext.Current.Request.UserHostAddress,
                            HistoryValue = history.ToString(System.Xml.Linq.SaveOptions.DisableFormatting)
                        });

                        db.SaveChanges();

                        if (physicalFile.PhysicalFileId > 0)
                            fileId = physicalFile.PhysicalFileId;

                        return Success;

                        #endregion
                    }

                    return Unauthorized;
                }
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }

        }

        /// <summary>
        /// Deletes the file.
        /// </summary>
        /// <param name="jobId">The job identifier.</param>
        /// <param name="physicalFileId">The physical file identifier.</param>
        /// <returns>System.String.</returns>
        public string DeleteFile(int physicalFileId)
        {
            if (physicalFileId <= 0)
                return DataCannotBeNullOrEmpty;

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var info = (from l in db.Jobs
                        from pf in l.PhysicalFiles
                        where
                            pf.PhysicalFileId == physicalFileId && l.IsDeleted == false && l.Lead.IsDeleted == false &&
                            l.Lead.FranchiseId == Franchise.FranchiseId && pf.PhysicalFileId == physicalFileId
                        select new {l.JobId, l.SalesPersonId, l.InstallerPersonId, pf.FileName, pf.ThumbUrl, pf.UrlPath})
                        .FirstOrDefault();
                    if (info == null)
                        return FileDoesNotExist;

                    //Base permission of can update (like admin and managers) or assigned permission AND assigned person matches current user
                    if (BasePermission.CanUpdate ||
                        (AssignedPermission.CanUpdate && (info.SalesPersonId == User.PersonId)))
                    {
                        var file = new PhysicalFile {PhysicalFileId = physicalFileId};
                        var job = new Job {JobId = info.JobId, EditHistories = new List<EditHistory>()};
                        //job.PhysicalFiles.Add(file);

                        db.PhysicalFiles.Attach(file);
                        db.Jobs.Attach(job);

                        XElement history =
                            new XElement("File", new XAttribute("operation", "delete"),
                                new XElement("FileName", new XAttribute("name", "File name"),
                                    new XElement("Current", info.FileName))
                                );
                        job.LastUpdated = this.GetNow();
                        job.LastUpdatedByPersonId = User.PersonId;

                        job.EditHistories.Add(new EditHistory
                        {
                            LoggedByPersonId = User.PersonId,
                            IPAddress = HttpContext.Current.Request.UserHostAddress,
                            HistoryValue = history.ToString(System.Xml.Linq.SaveOptions.DisableFormatting)
                        });

                        //job.PhysicalFiles.Remove(file);
                        db.PhysicalFiles.Remove(file);

                        db.SaveChanges();

                        try
                        {
                            System.IO.File.Delete(HttpContext.Current.Server.MapPath(info.UrlPath));
                            if (!string.IsNullOrEmpty(info.ThumbUrl))
                                System.IO.File.Delete(HttpContext.Current.Server.MapPath(info.ThumbUrl));
                        }
                        catch
                        {
                        } //do something? 

                        return Success;

                    }
                    else
                        return Unauthorized;
                }

            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }

        }

        public List<PhysicalFile> GetFiles(int jobId)
        {
            using (var db = ContextFactory.Create())
            {
                return
                    db.Jobs.Include(i => i.Lead).
                        Include(i => i.PhysicalFiles).
                        Where(j => j.JobId == jobId && !j.IsDeleted && j.Lead.FranchiseId == this.Franchise.FranchiseId)
                        .
                        SelectMany(j => j.PhysicalFiles).
                        ToList();
            }
        }

        #region Job Note methods

        public List<JobNote> GetNotes(List<int> jobIds, bool includeHistory = false)
        {
            if (jobIds == null || jobIds.Count == 0)
                throw new ArgumentOutOfRangeException("Invalid Job Ids");

            using (var db = ContextFactory.Create())
            {
                List<JobNote> notes = (from W in db.Jobs
                    from n in W.JobNotes
                    where
                        jobIds.Contains(W.JobId) && W.Lead.FranchiseId == Franchise.FranchiseId && W.IsDeleted == false &&
                        W.Lead.IsDeleted == false
                    select n).ToList();

                if (includeHistory)
                {
                    var history = (from W in db.Jobs
                        from n in W.EditHistories
                        where
                            jobIds.Contains(W.JobId) && W.Lead.FranchiseId == Franchise.FranchiseId &&
                            W.IsDeleted == false &&
                            W.Lead.IsDeleted == false
                        select new
                        {
                            n.LoggedByPersonId,
                            W.JobId,
                            n.HistoryValue,
                            n.CreatedOnUtc,
                        }).ToList();

                    if (history != null && history.Count > 0)
                    {
                        notes = notes.Concat(history.Select(n => new JobNote
                        {
                            CreatedByPersonId = n.LoggedByPersonId,
                            JobId = n.JobId,
                            Message = LeadsManager.ParseMessage(n.HistoryValue),
                            CreatedOn = DateTime.SpecifyKind(n.CreatedOnUtc, DateTimeKind.Utc),
                            TypeEnum = NoteTypeEnum.History
                        }).ToList()).ToList();
                    }
                }

                notes = notes.OrderByDescending(o => o.CreatedOn).ToList();
                foreach (var note in notes)
                {
                    var person = CacheManager.UserCollection.FirstOrDefault(f => f.PersonId == note.CreatedByPersonId);
                    if (person != null)
                    {
                        note.CreatorFullName = person.Person.FullName;
                        note.AvatarSrc = person.AvatarSrc ?? AppConfigManager.DefaultAvatarSrc;
                    }
                    else
                    {
                        note.CreatorFullName = "Unknown";
                        note.AvatarSrc = AppConfigManager.DefaultAvatarSrc;
                    }
                }

                return notes;
            }
        }

        /// <summary>
        /// Adds a new job note
        /// </summary>
        /// <param name="note">Job note source</param>        
        public string AddNote(JobNote note)
        {
            if (note == null || note.JobId <= 0 || string.IsNullOrEmpty(note.Message))
                return DataCannotBeNullOrEmpty;

            try
            {
                if (note.CreatedOn == new DateTimeOffset())
                    note.CreatedOn = DateTimeOffset.Now;
                note.CreatedByPersonId = User.PersonId;

                using (var db = ContextFactory.Create())
                {
                    db.JobNotes.Add(note);
                    db.SaveChanges();
                }

                return Success;
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        /// <summary>
        /// Updates job note record
        /// </summary>
        /// <param name="noteId">Note Id to update</param>
        /// <param name="note">Note source</param>        
        public string UpdateNote(int noteId, JobNote note)
        {
            if (noteId <= 0)
                return "Invalid note id";
            if (note == null)
                return "Invalid note record";

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var originalNote = (from n in db.JobNotes
                        join j in db.Jobs on n.JobId equals j.JobId
                        join l in db.Leads on j.LeadId equals l.LeadId
                        where
                            n.NoteId == noteId && l.FranchiseId == Franchise.FranchiseId && l.IsDeleted == false &&
                            j.IsDeleted == false
                        select new {n.Message, n.TypeEnum}).FirstOrDefault();

                    if (originalNote != null)
                    {
                        var updatingnote = new JobNote
                        {
                            NoteId = noteId,
                            TypeEnum = originalNote.TypeEnum,
                            Message = originalNote.Message
                        };
                        db.JobNotes.Attach(updatingnote);

                        updatingnote.TypeEnum = note.TypeEnum;
                        if (!string.IsNullOrEmpty(note.Message))
                            updatingnote.Message = note.Message;

                        db.SaveChanges();
                        return Success;
                    }
                    else
                        return "Note does note exist";
                }
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        #endregion

        /// <summary>
        /// Updates the status.
        /// </summary>
        /// <param name="JobId">The job identifier.</param>
        /// <param name="statusId">The status identifier.</param>
        /// <returns>System.String.</returns>
        public string UpdateStatus(int JobId, int statusId)
        {
            if (JobId <= 0)
                return DataCannotBeNullOrEmpty;

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var originalInfo =
                        db.Jobs.Where(
                            w => w.JobId == JobId && w.IsDeleted == false && w.Lead.FranchiseId == Franchise.FranchiseId)
                            .Select(s => new {s.SalesPersonId, s.InstallerPersonId, s.JobStatusId, s.ContractedOnUtc})
                            .FirstOrDefault();

                    if (originalInfo == null)
                        return JobDoesNotExist;

                    //Base permission of can update (like admin and managers) or assigned permission AND assigned person matches current user
                    if (BasePermission.CanUpdate ||
                        (AssignedPermission.CanUpdate && (originalInfo.SalesPersonId == User.PersonId)))
                    {
                        /*Contract date should be required before changing Job status to a "SaleMade", "Complete", or "Service" category status. When user selects Sale Made or ....*/
                        var saleMadeStatusIds = this.GetJobStatusesAfterSaleMade();
                        var completeStatusIds = this.GetJobStatusesAfterComplete();

                        var old =
                            CacheManager.JobStatusTypeCollection.FirstOrDefault(f => f.Id == originalInfo.JobStatusId);
                        var current = CacheManager.JobStatusTypeCollection.FirstOrDefault(f => f.Id == statusId);
                        if (current == null) return "Status does not exist";

                        if (saleMadeStatusIds.Contains(statusId) && !originalInfo.ContractedOnUtc.HasValue)
                        {
                            return "You can not setup SalesMade status while Contracted date is not provided";
                        }

                        var history =
                            new XElement("Job",
                                new XElement("JobStatusId", new XAttribute("name", "Status"),
                                    new XAttribute("operation", "update"),
                                    new XElement("Original", new XAttribute("id", old.Id), old.Name),
                                    new XElement("Current", new XAttribute("id", current.Id), current.Name)
                                    )
                                );

                        var job = db.Jobs.FirstOrDefault(j => j.JobId == JobId);

                        job.JobStatusId = current.Id;

                        if (completeStatusIds.Contains(statusId) && job.IsCompleted != true)
                        {
                            job.IsCompleted = true;
                            job.CompletionDT = this.GetNow();
                        }

                        job.LastUpdated = this.GetNow();
                        job.LastUpdatedByPersonId = User.PersonId;

                        job.EditHistories.Add(new EditHistory()
                        {
                            HistoryValue = history.ToString(SaveOptions.DisableFormatting),
                            LoggedByPersonId = User.PersonId,
                            IPAddress = HttpContext.Current.Request.UserHostAddress
                        });

                        db.SaveChanges();
                        return Success;
                    }

                    else
                        return Unauthorized;
                }

            }
            catch (Exception ex)
            {
#if DEBUG
                return ex.Message;
#else                
                return EventLogger.LogEvent(ex); 
#endif
            }

        }

        /// <summary>
        /// Updates the address.
        /// </summary>
        /// <param name="JobId">The job identifier.</param>
        /// <param name="BillingAddressId">The billing address identifier.</param>
        /// <param name="InstallAddressId">The install address identifier.</param>
        /// <returns>System.String.</returns>
        public string UpdateAddress(int JobId, int? BillingAddressId = null, int? InstallAddressId = null)
        {
            var addrId = 0;
            if (BillingAddressId.HasValue && BillingAddressId.Value > 0)
                addrId = BillingAddressId.Value;
            else if (InstallAddressId.HasValue && InstallAddressId.Value > 0)
                addrId = InstallAddressId.Value;

            if (JobId <= 0 || addrId <= 0)
                return AddressDoesNotExist;

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var originalJob = (from j in db.Jobs
                        let currAddr = j.Lead.Addresses.FirstOrDefault(f => f.AddressId == addrId)
                        where
                            j.JobId == JobId && j.Lead.FranchiseId == Franchise.FranchiseId && j.IsDeleted == false &&
                            j.Lead.IsDeleted == false
                        select new
                        {
                            j.InstallAddressId,
                            j.BillingAddressId,
                            OriginalAddress = BillingAddressId.HasValue ? j.BillingAddress : j.InstallAddress,
                            CurrentAddress = currAddr
                        }).SingleOrDefault();

                    if (originalJob == null)
                        return JobDoesNotExist;

                    var job = new Job
                    {
                        JobId = JobId,
                        BillingAddressId = originalJob.BillingAddressId,
                        InstallAddressId = originalJob.InstallAddressId
                    };

                    db.Jobs.Attach(job);

                    XElement history = new XElement("Job");
                    bool hasChanges = false;

                    if (BillingAddressId.HasValue && BillingAddressId.Value > 0 &&
                        job.BillingAddressId != BillingAddressId.Value)
                    {
                        if (originalJob.CurrentAddress != null)
                        {
                            history.Add(new XElement("BillingAddressId", new XAttribute("name", "Billing Address"),
                                new XElement("Original", new XAttribute("id", job.BillingAddressId),
                                    originalJob.OriginalAddress.ToString()),
                                new XElement("Current", new XAttribute("id", originalJob.CurrentAddress.AddressId),
                                    originalJob.CurrentAddress.ToString())
                                ));
                            job.BillingAddressId = BillingAddressId.Value;
                        }
                        else
                            return AddressDoesNotExist;

                        hasChanges = true;
                    }
                    if (InstallAddressId.HasValue && InstallAddressId.Value > 0 &&
                        job.InstallAddressId != InstallAddressId.Value)
                    {
                        if (originalJob.CurrentAddress != null)
                        {
                            history.Add(new XElement("InstallAddressId", new XAttribute("name", "Install Address"),
                                new XElement("Original", new XAttribute("id", originalJob.InstallAddressId),
                                    originalJob.OriginalAddress.ToString()),
                                new XElement("Current", new XAttribute("id", originalJob.CurrentAddress.AddressId),
                                    originalJob.CurrentAddress.ToString())
                                ));
                            job.InstallAddressId = InstallAddressId.Value;
                        }
                        else
                            return AddressDoesNotExist;

                        hasChanges = true;
                    }

                    if (hasChanges)
                    {
                        job.LastUpdatedByPersonId = User.PersonId;
                        job.LastUpdated = this.GetNow();
                        job.EditHistories.Add(new EditHistory
                        {
                            HistoryValue = history.ToString(SaveOptions.DisableFormatting),
                            LoggedByPersonId = User.PersonId,
                            IPAddress = HttpContext.Current.Request.UserHostAddress
                        });
                        db.SaveChanges();

                        //update tax rules
                        if (job.InstallAddress != null)
                        {
                            var taxrule = Franchise.GetTaxRule(job.InstallAddress);

                            if (taxrule != null)
                            {
                                job =
                                    db.Jobs.Include(i => i.JobQuotes.Select(s => s.SaleAdjustments))
                                        .Include(i => i.JobQuotes.Select(s => s.JobItems))
                                        .FirstOrDefault(j => j.JobId == job.JobId);

                                foreach (var quote in job.JobQuotes)
                                {
                                    foreach (var jobSaleAdjustment in quote.SaleAdjustments)
                                    {
                                        if (jobSaleAdjustment.TypeEnum == SaleAdjTypeEnum.Tax)
                                        {
                                            jobSaleAdjustment.Percent = taxrule.Percent;
                                            jobSaleAdjustment.Category = taxrule.ToString();
                                            jobSaleAdjustment.Memo = !string.IsNullOrEmpty(taxrule.ZipCode) ||
                                                                     !string.IsNullOrEmpty(taxrule.County) ||
                                                                     !string.IsNullOrEmpty(taxrule.City)
                                                ? "Tax applied via matching a tax rule"
                                                : "Default tax rate";
                                        }
                                    }

                                    quote.CalculateTotals();
                                }

                                db.SaveChanges();
                            }
                        }
                    }
                    return Success;
                }
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }

        }

        #region Job/Quote Line Items and Sale Adjustment Methods

        /// <summary>
        /// Adds a new sale adjustment, could be tax, surcharge or discount if permission for current user allows it
        /// </summary>
        /// <param name="currentQuoteLastUpdated">Current Quote Last Updated date to use to check ATOMIC state of a quote</param>
        /// <param name="adjustment">Sale adjustment data to add</param>        
        public string AddSaleAdjustment(DateTimeOffset currentQuoteLastUpdated, JobSaleAdjustment adjustment)
        {
            if (adjustment == null)
                return DataCannotBeNullOrEmpty;
            if (adjustment.QuoteId <= 0)
                return "Quote Id is required";

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var existingJob = (from q in db.JobQuotes
                        where
                            q.QuoteId == adjustment.QuoteId && q.IsDeleted == false && q.Job.IsDeleted == false &&
                            q.Job.Lead.FranchiseId == Franchise.FranchiseId
                        select new {q.Job.SalesPersonId, Quote = q}).SingleOrDefault();

                    if (existingJob == null || existingJob.Quote == null)
                        return "Quote does not exist";
                    /*if (Math.Abs(existingJob.Quote.LastUpdated.Subtract(currentQuoteLastUpdated).TotalSeconds) >= 1)
                    return "The quote has been modified since your last view, please refresh before making additional changes.";*/

                    if (BasePermission.CanUpdate ||
                        (AssignedPermission.CanUpdate && existingJob.SalesPersonId == User.PersonId))
                    {
                        db.Entry(existingJob.Quote).Collection(c => c.SaleAdjustments).Load();
                        db.Entry(existingJob.Quote).Collection(c => c.JobItems).Load();

                        var history = new XElement("Quote",
                            new XElement("SaleAdjustment", new XAttribute("name", "Sale Adjustment"),
                                new XAttribute("operation", "insert"),
                                new XElement("Current",
                                    string.Format("{0}, {1:C}", adjustment.Category, adjustment.Amount))));

                        existingJob.Quote.Job = new Job
                        {
                            JobId = existingJob.Quote.JobId,
                            EditHistories = new List<EditHistory>()
                        };
                        db.Jobs.Attach(existingJob.Quote.Job);

                        existingJob.Quote.SaleAdjustments.Add(adjustment);

                        existingJob.Quote.CalculateTotals();

                        existingJob.Quote.Job.LastUpdated =
                            existingJob.Quote.LastUpdated = adjustment.LastUpdated = this.GetNow();
                        existingJob.Quote.Job.LastUpdatedByPersonId = User.PersonId;

                        existingJob.Quote.Job.EditHistories.Add(new EditHistory
                        {
                            LoggedByPersonId = User.PersonId,
                            IPAddress = HttpContext.Current.Request.UserHostAddress,
                            HistoryValue = history.ToString(SaveOptions.DisableFormatting)
                        });

                        db.SaveChanges();

                        return Success;
                    }
                    else
                        return Unauthorized;
                }
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        /// <summary>
        /// Updates a job or quote sale adjustment
        /// </summary>
        /// <param name="currentQuoteLastUpdated">Current Quote Last Updated date to use to check ATOMIC state of a quote</param>
        /// <param name="adjustment">Updated sale adjustment data</param>        
        public string UpdateSaleAdjustment(DateTimeOffset currentQuoteLastUpdated, JobSaleAdjustment adjustment)
        {
            if (adjustment == null || adjustment.SaleAdjustmentId <= 0)
                return DataCannotBeNullOrEmpty;
            if (adjustment.QuoteId <= 0)
                return "Quote Id is required";

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var existingJob = (from q in db.JobQuotes
                        where
                            q.QuoteId == adjustment.QuoteId && q.IsDeleted == false && q.Job.IsDeleted == false &&
                            q.Job.Lead.FranchiseId == Franchise.FranchiseId
                        select new {q.Job.SalesPersonId, Quote = q}).SingleOrDefault();

                    if (existingJob == null)
                        return "Quote does not exist";
                    /*if (Math.Abs(existingJob.Quote.LastUpdated.Subtract(currentQuoteLastUpdated).TotalSeconds) >= 1)
                    return "The quote has been modified since your last view, please refresh before making additional changes.";*/

                    if (BasePermission.CanUpdate ||
                        (AssignedPermission.CanUpdate && existingJob.SalesPersonId == User.PersonId))
                    {
                        db.Entry(existingJob.Quote).Collection(c => c.SaleAdjustments).Load();

                        var original =
                            existingJob.Quote.SaleAdjustments.SingleOrDefault(
                                f => f.SaleAdjustmentId == adjustment.SaleAdjustmentId);
                        if (Math.Abs(original.LastUpdated.Subtract(adjustment.LastUpdated).TotalSeconds) >= 1)
                            return
                                "This sale adjustment has been modified since your last view, please refresh before making additional changes.";

                        db.Entry(existingJob.Quote).Collection(c => c.JobItems).Load();

                        original.TypeEnum = adjustment.TypeEnum;
                        original.Category = adjustment.Category;
                        original.Memo = WebUtility.HtmlDecode(adjustment.Memo);
                        original.Percent = adjustment.Percent;
                        original.Amount = adjustment.Amount;
                        original.IsTaxable = adjustment.IsTaxable;
                        original.IsDebit = adjustment.IsDebit;
                        original.ApplyDiscountBeforeTax = adjustment.ApplyDiscountBeforeTax;

                        var history = Util.GetEFChangedProperties(db.Entry(original));

                        existingJob.Quote.Job = new Job
                        {
                            JobId = existingJob.Quote.JobId,
                            EditHistories = new List<EditHistory>()
                        };
                        db.Jobs.Attach(existingJob.Quote.Job);

                        existingJob.Quote.CalculateTotals();

                        existingJob.Quote.Job.LastUpdated =
                            existingJob.Quote.LastUpdated =
                                original.LastUpdated = adjustment.LastUpdated = this.GetNow();
                        existingJob.Quote.Job.LastUpdatedByPersonId = User.PersonId;

                        existingJob.Quote.Job.EditHistories.Add(new EditHistory
                        {
                            LoggedByPersonId = User.PersonId,
                            IPAddress = HttpContext.Current.Request.UserHostAddress,
                            HistoryValue = history != null ? history.ToString(SaveOptions.DisableFormatting) : null
                        });

                        db.SaveChanges();

                        return Success;
                    }
                    else
                        return Unauthorized;
                }
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        /// <summary>
        /// Deletes a job sale adjustment
        /// </summary>
        /// <param name="QuoteId">Quote identifier</param>
        /// <param name="currentQuoteLastUpdated">Current Quote Last Updated date to use to check ATOMIC state of a quote</param>
        /// <param name="SaleAdjustmentId">Sale adjustment identifier.</param>
        /// <param name="newLastUpdated">Returns a last updated date</param>        
        public string DeleteSaleAdjustment(int QuoteId, DateTimeOffset currentQuoteLastUpdated, int SaleAdjustmentId,
            out DateTimeOffset newLastUpdated)
        {
            newLastUpdated = DateTimeOffset.Now;
            if (QuoteId <= 0)
                return "Quote Id is required";
            if (SaleAdjustmentId <= 0)
                return "Sale adjustment id is required";

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var existingJob = (from q in db.JobQuotes
                        where
                            q.QuoteId == QuoteId && q.IsDeleted == false && q.Job.IsDeleted == false &&
                            q.Job.Lead.FranchiseId == Franchise.FranchiseId
                        select new {q.Job.SalesPersonId, Quote = q}).SingleOrDefault();

                    if (existingJob == null)
                        return JobDoesNotExist;
                    /*if (Math.Abs(existingJob.Quote.LastUpdated.Subtract(currentQuoteLastUpdated).TotalSeconds) >= 1)
                    return "The quote has been modified since your last view, please refresh before making additional changes.";*/

                    if (BasePermission.CanUpdate ||
                        (AssignedPermission.CanUpdate && existingJob.SalesPersonId == User.PersonId))
                    {
                        db.Entry(existingJob.Quote).Collection(c => c.SaleAdjustments).Load();

                        var original =
                            existingJob.Quote.SaleAdjustments.SingleOrDefault(
                                f => f.SaleAdjustmentId == SaleAdjustmentId);
                        if (original == null)
                            return "Sale adjustment record does not exist";

                        db.Entry(existingJob.Quote).Collection(c => c.JobItems).Load();

                        var history = new XElement("Quote",
                            new XElement("SaleAdjustment", new XAttribute("name", "Sale Adjustment"),
                                new XAttribute("operation", "delete"),
                                new XElement("Original", string.Format("{0}", original.Category))));

                        existingJob.Quote.Job = new Job
                        {
                            JobId = existingJob.Quote.JobId,
                            EditHistories = new List<EditHistory>()
                        };
                        db.Jobs.Attach(existingJob.Quote.Job);

                        existingJob.Quote.SaleAdjustments.Remove(original);
                        db.JobSaleAdjustments.Remove(original);
                        existingJob.Quote.CalculateTotals();

                        existingJob.Quote.Job.LastUpdated =
                            existingJob.Quote.LastUpdated = newLastUpdated = this.GetNow();
                        existingJob.Quote.Job.LastUpdatedByPersonId = User.PersonId;

                        existingJob.Quote.Job.EditHistories.Add(new EditHistory
                        {
                            LoggedByPersonId = User.PersonId,
                            IPAddress = HttpContext.Current.Request.UserHostAddress,
                            HistoryValue = history.ToString(SaveOptions.DisableFormatting)
                        });

                        db.SaveChanges();

                        return Success;
                    }
                    else
                        return Unauthorized;
                }
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }


        /// <summary>
        /// Adds a job or quote line item
        /// </summary>
        /// <param name="model">JobItem data to add</param>
        /// <param name="QuoteLastUpdated">Current Quote Last Updated date to use to check ATOMIC state of a quote</param>
        /// <param name="errors"></param>
        /// <param name="sessionId"></param>
        public string AddJobItem(JobItem model, DateTimeOffset QuoteLastUpdated, string[] errors, string sessionId,
            out DateTime? quoteDate)
        {
            quoteDate = null;
            if (model == null)
                return DataCannotBeNullOrEmpty;
            if (model.QuoteId <= 0)
                return "Quote Id is required";

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var existingJob = (from q in db.JobQuotes
                        where
                            q.QuoteId == model.QuoteId && q.IsDeleted == false && q.Job.IsDeleted == false &&
                            q.Job.Lead.FranchiseId == Franchise.FranchiseId
                        select new {q.Job.SalesPersonId, Quote = q}).SingleOrDefault();

                    if (existingJob == null)
                        return "Quote does not exist";
                    /*if (Math.Abs(existingJob.Quote.LastUpdated.Subtract(QuoteLastUpdated).TotalSeconds) >= 1)
                    return "The quote has been modified since your last view, please refresh before making additional changes.";*/

                    if (BasePermission.CanUpdate ||
                        (AssignedPermission.CanUpdate && existingJob.SalesPersonId == User.PersonId))
                    {
                        db.Entry(existingJob.Quote).Collection(c => c.JobItems).Load();
                        db.Entry(existingJob.Quote).Collection(c => c.SaleAdjustments).Load();

                        existingJob.Quote.Job = new Job
                        {
                            JobId = existingJob.Quote.JobId,
                            EditHistories = new List<EditHistory>()
                        };
                        db.Jobs.Attach(existingJob.Quote.Job);

                        if (!model.CreatedByPersonId.HasValue || model.CreatedByPersonId.Value <= 0)
                            model.CreatedByPersonId = User.PersonId;
                        if (model.Options != null && model.Options.Count > 0)
                        {
                            foreach (var opt in model.Options)
                            {
                                opt.OptionId = 0;
                                    //set them all to 0 so they can be inserted in case they are not already 0
                            }
                        }

                        // mng: MYC-2241 - Quote date defaults to today’s date after first line item is saved.
                        // We need to check whether the QuoteDateUTC contains value. If
                        // contains do't do anything, otherwise set to current date.
                        // TODO : this following code retuns wrong data, don't know 
                        // why, we need to figure it out.
                        //var jobTocheck = (from j in db.Jobs
                        //    where j.JobId == existingJob.Quote.Job.JobId
                        //    select j).SingleOrDefault();

                        var jobTocheck = this.GetJobDetails(existingJob.Quote.Job.JobId);
                        //var something = mng.QuoteDateUTC;

                        if (existingJob.Quote.JobItems.Count == 0 && 
                            (jobTocheck != null && jobTocheck.QuoteDateUTC == null))
                        {
                            existingJob.Quote.Job.QuoteDateUTC = DateTime.Now;
                            quoteDate = existingJob.Quote.Job.QuoteDateUTC;
                        }

                        existingJob.Quote.Job.LastUpdated =
                            existingJob.Quote.LastUpdated = model.LastUpdated = this.GetNow();
                        existingJob.Quote.Job.LastUpdatedByPersonId = model.LastUpdatedPersonId = User.PersonId;

                        existingJob.Quote.JobItems.Add(model);
                        existingJob.Quote.CalculateTotals();

                        var history = new XElement("Quote",
                            new XElement("JobItems", new XAttribute("name", "Job Item"),
                                new XAttribute("operation", "insert"),
                                new XElement("Current", model.ToString())
                                )
                            );

                        existingJob.Quote.Job.EditHistories.Add(new EditHistory
                        {
                            LoggedByPersonId = User.PersonId,
                            IPAddress = HttpContext.Current.Request.UserHostAddress,
                            HistoryValue = history.ToString(SaveOptions.DisableFormatting)
                        });

                        /* Add information abt saving line item to job notes */

                        var jobQuoteOrderNum =
                            db.JobQuotes.Where(
                                jq => jq.JobId == existingJob.Quote.Job.JobId && jq.IsDeleted != true)
                                .ToList()
                                .IndexOf(existingJob.Quote) + 1;
                        var jobItemOrderNum = existingJob.Quote.JobItems.ToList().IndexOf(model) + 1;

                        var lineItemRef = string.Format("Qt:{0},Ln:{1}", jobQuoteOrderNum, jobItemOrderNum);

                        existingJob.Quote.Job.EditHistories.Add(new EditHistory
                        {
                            LoggedByPersonId = User.PersonId,
                            IPAddress = HttpContext.Current.Request.UserHostAddress,
                            HistoryValue =
                                string.Format("<Quote operation=\"information\">{0}</Quote>",
                                    errors != null && errors.Length > 0
                                        ? string.Format("{0} saved with errors: {1}, sessionId: {2}", lineItemRef,
                                            string.Join(", ", errors), sessionId)
                                        : string.Format("{0} saved, sessionId: {1}", lineItemRef, sessionId)),
                            CreatedOnUtc = DateTime.Now
                        });

                        db.SaveChanges();

                        return Success;
                    }
                    else
                        return Unauthorized;
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ex.Message;
            }
        }

        /// <summary>
        /// Updates a job line item
        /// </summary>
        /// <param name="model">Job Item data to update</param>
        /// <param name="quoteLastUpdated">Current Quote Last Updated date to use to check ATOMIC state of a quote</param>
        /// <param name="errors"></param>
        /// <param name="sessionId"></param>        
        public string UpdateJobItem(JobItem model, DateTimeOffset quoteLastUpdated, string[] errors, string sessionId)
        {
            if (model == null)
                return DataCannotBeNullOrEmpty;
            if (model.QuoteId <= 0 || model.JobItemId <= 0)
                return JobDoesNotExist;

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var existingJob = (from q in db.JobQuotes
                        where q.QuoteId == model.QuoteId 
                            && q.IsDeleted == false 
                            && q.Job.IsDeleted == false 
                            && q.Job.Lead.FranchiseId == Franchise.FranchiseId
                        select new {q.Job.SalesPersonId, Quote = q}).SingleOrDefault();

                    if (existingJob == null)
                        return "Quote does not exist";
                    /*if (Math.Abs(existingJob.Quote.LastUpdated.Subtract(QuoteLastUpdated).TotalSeconds) >= 1)
                    return "The quote has been modified since your last view, please refresh before making additional changes.";*/

                    if (!BasePermission.CanUpdate 
                        && (!AssignedPermission.CanUpdate ||
                            existingJob.SalesPersonId != User.PersonId)) return Unauthorized;

                    db.Entry(existingJob.Quote).Collection(c => c.JobItems).Load();

                    var jobItem = existingJob.Quote.JobItems.SingleOrDefault(f => f.JobItemId == model.JobItemId);
                    if (jobItem == null)
                        return "Job item does not exist";

                    db.Entry(existingJob.Quote).Collection(c => c.SaleAdjustments).Load();
                    db.Entry(jobItem).Collection(c => c.Options).Load();

                    jobItem.Quantity = model.Quantity;
                    jobItem.ProductType = model.ProductType;
                    jobItem.CategoryName = WebUtility.HtmlDecode(model.CategoryName);
                    jobItem.Style = WebUtility.HtmlDecode(model.Style);
                    jobItem.Manufacturer = WebUtility.HtmlDecode(model.Manufacturer);
                    jobItem.Description = WebUtility.HtmlDecode(model.Description);
                    jobItem.Color = WebUtility.HtmlDecode(model.Color);
                    jobItem.IsTaxable = model.IsTaxable;
                    jobItem.SalePrice = model.SalePrice;
                    jobItem.JobberPrice = model.JobberPrice;
                    jobItem.ListPrice = model.ListPrice;
                    jobItem.JobberPriceOverriden = model.JobberPriceOverriden;
                    //jobItem.ProductPrice = model.ProductPrice;
                    //jobItem.ProductMSRP = model.ProductMSRP;
                    jobItem.DiscountRemark = model.DiscountRemark;
                    jobItem.DiscountAmount = model.DiscountAmount;
                    jobItem.DiscountPercent = model.DiscountPercent;
                    jobItem.DiscountType = model.DiscountType;
                    jobItem.ProductName = model.ProductName;
                    jobItem.Notes = model.Notes;

                    var history = Util.GetEFChangedProperties(db.Entry(jobItem));

                    //not logging these updates below
                    jobItem.ProductTypeId = model.ProductTypeId;
                    jobItem.StyleId = model.StyleId;
                    jobItem.ManufacturerId = model.ManufacturerId;
                    jobItem.CategoryId = model.CategoryId;
                    jobItem.SessionId = model.SessionId;
                    jobItem.IsSavedWithErrors = errors != null && errors.Length > 0;

                    var now = DateTime.Now;

                    jobItem.LastUpdated =
                        model.LastUpdated =
                            new DateTimeOffset(now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second,
                                TimeSpan.Zero);
                    jobItem.LastUpdatedPersonId = User.PersonId;
                    jobItem.JobberDiscountPercent = model.JobberDiscountPercent;
                    jobItem.MarginPercent = model.MarginPercent;
                    jobItem.CostSubtotal = model.CostSubtotal;
                    jobItem.Subtotal = model.Subtotal;
                    jobItem.UnitCost = model.UnitCost;

                    //original job item doesnt have options, but the current options has some value, so we will add it all
                    if (jobItem.Options != null && jobItem.Options.Count > 0)
                    {
                        var jobItemOptionsToRemove = new List<JobItemOption>();

                        foreach (var opt in jobItem.Options)
                        {
                            var curOpt =
                                model.Options.FirstOrDefault(
                                    f =>
                                        string.Equals(f.Name, opt.Name, StringComparison.InvariantCultureIgnoreCase));
                            if (curOpt != null)
                            {
                                opt.Value = curOpt.Value;
                            }
                            //model doesnt have options, so we need to remove original options
                            else
                            {
                                jobItemOptionsToRemove.Add(opt);
                            }
                        }

                        foreach (var jobItemOption in jobItemOptionsToRemove)
                        {
                            db.JobItemOptions.Remove(jobItemOption);
                        }

                        foreach (var opt in model.Options.Where(w => w.OptionId == 0))
                        {
                            if (
                                !jobItem.Options.Any(
                                    f =>
                                        string.Equals(f.Name, opt.Name, StringComparison.InvariantCultureIgnoreCase)))
                                jobItem.Options.Add(opt);
                        }
                    }
                    else if (model.Options != null && model.Options.Count > 0)
                    {
                        foreach (var opt in model.Options)
                        {
                            opt.OptionId = 0;
                        }
                        jobItem.Options = model.Options;
                    }

                    existingJob.Quote.Job = new Job
                    {
                        JobId = existingJob.Quote.JobId,
                        EditHistories = new List<EditHistory>()
                    };
                    db.Jobs.Attach(existingJob.Quote.Job);

                    existingJob.Quote.CalculateTotals();
                    existingJob.Quote.Job.LastUpdated = existingJob.Quote.LastUpdated = jobItem.LastUpdated;
                    existingJob.Quote.Job.LastUpdatedByPersonId = User.PersonId;

                    if (history != null)
                    {
                        existingJob.Quote.Job.EditHistories.Add(new EditHistory
                        {
                            LoggedByPersonId = User.PersonId,
                            IPAddress = HttpContext.Current.Request.UserHostAddress,
                            HistoryValue = history.ToString(SaveOptions.DisableFormatting)
                        });
                    }

                    /* Add information abt saving line item to job notes */

                    var jobQuoteOrderNum =
                        db.JobQuotes.Where(
                                jq => jq.JobId == existingJob.Quote.Job.JobId && jq.IsDeleted != true)
                            .ToList()
                            .IndexOf(existingJob.Quote) + 1;
                    var jobItemOrderNum = existingJob.Quote.JobItems.ToList().IndexOf(jobItem) + 1;

                    var lineItemRef = string.Format("Qt:{0},Ln:{1}", jobQuoteOrderNum, jobItemOrderNum);

                    existingJob.Quote.Job.EditHistories.Add(new EditHistory
                    {
                        LoggedByPersonId = User.PersonId,
                        IPAddress = HttpContext.Current.Request.UserHostAddress,
                        HistoryValue =
                            string.Format("<Quote operation=\"information\">{0}</Quote>",
                                errors != null && errors.Length > 0
                                    ? string.Format("{0} saved with errors: {1}, sessionId: {2}", lineItemRef,
                                        string.Join(", ", errors), model.SessionId)
                                    : string.Format("{0} saved, sessionId: {1}", lineItemRef, sessionId)),
                        CreatedOnUtc = DateTime.Now
                    });

                    db.SaveChanges();

                    return Success;
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ex.Message;
            }
        }

        /// <summary>
        /// Deletes a job line item.
        /// </summary>
        /// <param name="QuoteId">Quote Identifier</param>
        /// <param name="JobItemId">Job Item Identifier</param>
        /// <param name="QuoteLastUpdated">Current Quote Last Updated date to use to check ATOMIC state of a quote</param> 
        /// <param name="lastUpdated">Returns a last updated date</param>
        public string DeleteJobItem(int QuoteId, int JobItemId, DateTimeOffset QuoteLastUpdated,
            out DateTimeOffset lastUpdated, bool force = false)
        {
            lastUpdated = DateTimeOffset.Now;
            if (QuoteId <= 0)
                return "Quote Id is required";
            if (JobItemId <= 0)
                return "Job item id is required";

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var existingJob = (from q in db.JobQuotes
                        where
                            q.QuoteId == QuoteId && q.IsDeleted == false && q.Job.IsDeleted == false &&
                            q.Job.Lead.FranchiseId == Franchise.FranchiseId
                        select new {q.Job.SalesPersonId, Quote = q}).SingleOrDefault();

                    if (existingJob == null)
                        return "Quote does not exist";
                    /*if (Math.Abs(existingJob.Quote.LastUpdated.Subtract(QuoteLastUpdated).TotalSeconds) >= 1)
                    return "The quote has been modified since your last view, please refresh before making additional changes.";*/

                    if (BasePermission.CanUpdate ||
                        (AssignedPermission.CanUpdate && existingJob.SalesPersonId == User.PersonId))
                    {
                        var orderItems =
                            db.OrderItems.Include(i => i.Order)
                                .Where(oi => oi.JobItemId == JobItemId)
                                .ToList();
                        if (!force && orderItems.Any(oi => oi.Order.IsDeleted != true))
                        {
                            return "Job item could not be deleted until order that uses it exists. Delete order first";
                        }

                        var orders = orderItems.Select(oi => oi.Order).ToList();
                        var orderIds = orders.Select(o => o.OrderId).ToList();

                        var orderItemsToDelete =
                            db.OrderItems.Where(oi => orderIds.Contains(oi.OrderId ?? 0)).ToList();

                        foreach (var item in orderItemsToDelete)
                        {
                            db.OrderItems.Remove(item);
                        }

                        foreach (var order in orders)
                        {
                            db.Orders.Remove(order);
                        }

                        db.SaveChanges();

                        db.Entry(existingJob.Quote).Collection(c => c.JobItems).Load();

                        var jobItem = existingJob.Quote.JobItems.SingleOrDefault(f => f.JobItemId == JobItemId);
                        if (jobItem == null)
                            return "Job item does not exist or has already been deleted";

                        db.Entry(existingJob.Quote).Collection(c => c.SaleAdjustments).Load();

                        existingJob.Quote.JobItems.Remove(jobItem);
                        db.JobItems.Remove(jobItem);

                        if (!force)
                        {
                            existingJob.Quote.Job = new Job
                            {
                                JobId = existingJob.Quote.JobId,
                                EditHistories = new List<EditHistory>()
                            };
                            db.Jobs.Attach(existingJob.Quote.Job);

                            existingJob.Quote.CalculateTotals();

                            existingJob.Quote.Job.LastUpdated =
                                existingJob.Quote.LastUpdated = lastUpdated = this.GetNow();
                            existingJob.Quote.Job.LastUpdatedByPersonId = User.PersonId;

                            var history = new XElement("JobItems", new XAttribute("operation", "delete"),
                                new XAttribute("name", "Job Item"),
                                new XElement("Original", jobItem.ToString()));
                            existingJob.Quote.Job.EditHistories.Add(new EditHistory
                            {
                                LoggedByPersonId = User.PersonId,
                                IPAddress = HttpContext.Current.Request.UserHostAddress,
                                HistoryValue = history.ToString(SaveOptions.DisableFormatting)
                            });
                        }

                        db.SaveChanges();

                        return Success;
                    }
                    else
                        return Unauthorized;
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ex.Message;
            }
        }

        public string UpdateSortOrder(int quoteId, List<int> sortOrder)
        {
            if (quoteId <= 0)
                return "Quote Id is required";

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var existingJob = (from q in db.JobQuotes
                        where
                            q.QuoteId == quoteId && q.IsDeleted == false && q.Job.IsDeleted == false &&
                            q.Job.Lead.FranchiseId == Franchise.FranchiseId
                        select new {q.Job.SalesPersonId, Quote = q}).SingleOrDefault();

                    if (existingJob == null)
                        return "Quote does not exist";

                    if (BasePermission.CanUpdate ||
                        (AssignedPermission.CanUpdate && existingJob.SalesPersonId == User.PersonId))
                    {
                        db.Entry(existingJob.Quote).Collection(c => c.JobItems).Load();

                        foreach (var jobItem in existingJob.Quote.JobItems)
                        {
                            jobItem.SortOrder = sortOrder.IndexOf(jobItem.JobItemId);
                        }

                        db.SaveChanges();

                        return Success;
                    }

                    return Unauthorized;
                }
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        #endregion

        /// <summary>
        /// Updates the source.
        /// </summary>
        /// <param name="JobId">The job identifier.</param>
        /// <param name="sourceId">The source identifier.</param>
        /// <returns>System.String.</returns>
        public string UpdateSource(int JobId, int sourceId)
        {
            if (JobId <= 0 || sourceId <= 0)
                return DataCannotBeNullOrEmpty;

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var existingJob = (from l in db.Jobs
                        where l.JobId == JobId && l.IsDeleted == false && l.Lead.FranchiseId == Franchise.FranchiseId
                        select new {l.SourceId, l.SalesPersonId}).SingleOrDefault();
                    if (existingJob == null)
                        return JobDoesNotExist;

                    if (BasePermission.CanUpdate ||
                        (AssignedPermission.CanUpdate && existingJob.SalesPersonId == User.PersonId))
                    {
                        if (existingJob.SourceId == sourceId)
                            return "Action cancelled, original and current source is the same value";

                        var job = new Job {JobId = JobId, EditHistories = new List<EditHistory>()};

                        db.Jobs.Attach(job);

                        XElement history =
                            new XElement("Job", new XAttribute("operation", "update"),
                                new XElement("Source",
                                    new XElement("Original", new XAttribute("id", existingJob.SourceId ?? 0),
                                        CacheManager.GetSourceString(existingJob.SourceId ?? 0)),
                                    new XElement("Current", new XAttribute("id", sourceId),
                                        CacheManager.GetSourceString(sourceId))
                                    )
                                );
                        job.SourceId = sourceId;
                        job.LastUpdatedByPersonId = User.PersonId;
                        job.LastUpdated = DateTime.Now;
                        job.EditHistories.Add(new EditHistory
                        {
                            LoggedByPersonId = User.PersonId,
                            HistoryValue = history.ToString(SaveOptions.DisableFormatting),
                            IPAddress = HttpContext.Current.Request.UserHostAddress
                        });

                        db.SaveChanges();
                        return Success;
                    }
                    else
                        return Unauthorized;
                }
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        /// <summary>
        /// Updates the territory.
        /// </summary>
        /// <param name="JobId">The job identifier.</param>
        /// <param name="TerritoryId">The territory identifier.</param>
        /// <returns>System.String.</returns>
        public string UpdateTerritory(int JobId, int? TerritoryId)
        {
            if (JobId <= 0) // || (TerritoryId.HasValue && TerritoryId.Value <= 0))
                return DataCannotBeNullOrEmpty;

            Territory currentTerritory = null;
            if (TerritoryId.HasValue)
            {
                currentTerritory = this.Franchise.Territories.FirstOrDefault(f => f.TerritoryId == TerritoryId.Value);
                //if (currentTerritory == null)
                //   return "Unable to locate your assigned territory";
            }

            try
            {
                //var existingJob = (from l in CRMDBContext.Jobs
                //    where l.JobId == JobId && l.IsDeleted == false && l.Lead.FranchiseId == Franchise.FranchiseId
                //    select new {l.Territory, l.TerritoryId, l.SalesPersonId}).SingleOrDefault();

                using (var db = ContextFactory.Create())
                {
                    var existingJob =
                        db.Jobs.Include(i => i.Territory)
                            .SingleOrDefault(v => v.JobId == JobId && v.IsDeleted == false);

                    if (existingJob == null)
                        return JobDoesNotExist;

                    if (BasePermission.CanUpdate ||
                        (AssignedPermission.CanUpdate && existingJob.SalesPersonId == User.PersonId))
                    {
                        if (existingJob.TerritoryId == TerritoryId)
                            return "Action cancelled, original and current territory is the same value";

                        //var job = new Job {JobId = JobId, EditHistories = new List<EditHistory>()};
                        //CRMDBContext.Jobs.Attach(job);

                        XElement history =
                            new XElement("Job", new XAttribute("operation", "update"),
                                new XElement("Territory",
                                    new XElement("Original", new XAttribute("id", existingJob.TerritoryId ?? 0),
                                        existingJob.Territory != null ? existingJob.Territory.ToString() : "Unassigned"),
                                    new XElement("Current", new XAttribute("id", TerritoryId ?? 0),
                                        currentTerritory != null ? currentTerritory.ToString() : "Unassigned")
                                    )
                                );
                        if (currentTerritory == null)
                        {
                            existingJob.TerritoryId = null;
                        }
                        else
                        {
                            existingJob.TerritoryId = TerritoryId;
                        }

                        existingJob.LastUpdatedByPersonId = User.PersonId;

                        existingJob.LastUpdated = this.GetNow();

                        existingJob.EditHistories.Add(new EditHistory
                        {
                            LoggedByPersonId = User.PersonId,
                            HistoryValue = history.ToString(SaveOptions.DisableFormatting),
                            IPAddress = HttpContext.Current.Request.UserHostAddress
                        });
                        db.SaveChanges();
                        return Success;
                    }
                    else
                        return Unauthorized;
                }
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }


        #region Quote Methods

        /// <summary>
        /// Adds a new quote to a job
        /// </summary>
        /// <param name="JobId">Job Id this quote belongs to</param>
        /// <param name="isPrimary">Set this quote as the primary quote.  Default is false</param>        
        public JobQuote AddQuote(int JobId, bool isPrimary = false)
        {
            if (JobId <= 0)
                throw new ArgumentNullException("Job Id is required");

            using (var db = ContextFactory.Create())
            {
                var existingJob = (from l in db.Jobs
                    where l.JobId == JobId && l.IsDeleted == false && l.Lead.FranchiseId == Franchise.FranchiseId
                    select new {l.SalesPersonId, l.InstallAddress, MaxQuoteNumber = l.JobQuotes.Max(a => a.QuoteNumber)})
                    .SingleOrDefault();
                if (existingJob == null)
                    throw new NullReferenceException(JobDoesNotExist);

                if (BasePermission.CanCreate ||
                    (AssignedPermission.CanCreate && existingJob.SalesPersonId == User.PersonId))
                {
                    JobQuote quote = new JobQuote
                    {
                        JobId = JobId,
                        QuoteNumber = existingJob.MaxQuoteNumber + 1,
                        CreatedByPersonId = User.PersonId,
                        LastUpdated = DateTime.Now,
                        IsPrimary = isPrimary,
                        SaleAdjustments = new List<JobSaleAdjustment>()
                    };

                    var taxrule = Franchise.GetTaxRule(existingJob.InstallAddress);

                    if (taxrule != null)
                    {
                        quote.TaxPercent = taxrule.Percent;
                        quote.SaleAdjustments.Add(new JobSaleAdjustment
                        {
                            LastUpdated = quote.LastUpdated,
                            TypeEnum = SaleAdjTypeEnum.Tax,
                            Percent = taxrule.Percent,
                            Category = taxrule.ToString(),
                            Memo =
                                !string.IsNullOrEmpty(taxrule.ZipCode) || !string.IsNullOrEmpty(taxrule.County) ||
                                !string.IsNullOrEmpty(taxrule.City)
                                    ? "Tax applied via matching a tax rule"
                                    : "Default tax rate"
                        });
                    }
                    db.JobQuotes.Add(quote);
                    db.SaveChanges();

                    return quote;
                }
                else
                    throw new UnauthorizedAccessException(Unauthorized);
            }
        }

        /// <summary>
        /// Clones an existing quote and adds to the job
        /// </summary>
        /// <param name="JobId">Job Id this quote belongs to</param>
        /// <param name="quoteId">Quote Id of the quote to clone from</param>
        /// <returns></returns>
        public JobQuote CloneQuote(int quoteId)
        {
            if (quoteId <= 0)
                throw new ArgumentNullException("Quote Id is required");

            using (var db = ContextFactory.Create())
            {
                var existingJob = (from l in db.JobQuotes
                    where
                        l.QuoteId == quoteId && l.IsDeleted == false && l.Job.Lead.FranchiseId == Franchise.FranchiseId
                    select new
                    {
                        l.Job.SalesPersonId,
                        MaxQuoteNumber = l.Job.JobQuotes.Max(a => a.QuoteNumber),
                        Quote = l
                    }).SingleOrDefault();
                if (existingJob == null)
                    throw new NullReferenceException(JobDoesNotExist);

                if (BasePermission.CanCreate ||
                    (AssignedPermission.CanCreate && existingJob.SalesPersonId == User.PersonId))
                {
                    db.Entry(existingJob.Quote).Collection(c => c.JobItems).Load();
                    db.Entry(existingJob.Quote).Collection(c => c.SaleAdjustments).Load();

                    var clonedQuote = new JobQuote
                    {
                        IsPrimary = false,
                        JobId = existingJob.Quote.JobId,
                        QuoteNumber = existingJob.MaxQuoteNumber + 1,
                        CreatedByPersonId = User.PersonId,
                        LastUpdated = DateTime.Now,
                        CostSubtotal = existingJob.Quote.CostSubtotal,
                        DiscountTotal = existingJob.Quote.DiscountTotal,
                        JobItems = new List<JobItem>(),
                        SaleAdjustments = new List<JobSaleAdjustment>(),
                        NetProfit = existingJob.Quote.NetProfit,
                        NetTotal = existingJob.Quote.NetTotal,
                        Subtotal = existingJob.Quote.Subtotal,
                        SurchargeTotal = existingJob.Quote.SurchargeTotal,
                        TaxPercent = existingJob.Quote.TaxPercent,
                        Taxtotal = existingJob.Quote.Taxtotal
                    };

                    foreach (var item in existingJob.Quote.JobItems)
                    {
                        db.Entry(item).Collection(c => c.Options).Load();
                        var clone = item.Clone(false);
                        clone.CreatedByPersonId = User.PersonId;
                        clone.LastUpdated = clonedQuote.LastUpdated;

                        clonedQuote.JobItems.Add(clone);
                    }
                    foreach (var item in existingJob.Quote.SaleAdjustments)
                    {
                        var clone = item.Clone(false);

                        clone.LastUpdated = clonedQuote.LastUpdated;
                        clonedQuote.SaleAdjustments.Add(clone);
                    }

                    db.JobQuotes.Add(clonedQuote);
                    db.SaveChanges();

                    return clonedQuote;
                }
                else
                    throw new UnauthorizedAccessException(Unauthorized);
            }
        }

        /// <summary>
        /// Deletes a quote
        /// </summary>
        /// <param name="quoteId">Quote id of quote to delete</param>
        /// <returns></returns>
        public string DeleteQuote(int quoteId)
        {
            if (quoteId <= 0)
                return "Quote Id is required";

            using (var db = ContextFactory.Create())
            {
                var existingQuote = (from l in db.JobQuotes
                    where
                        l.QuoteId == quoteId && l.IsDeleted == false && l.Job.Lead.FranchiseId == Franchise.FranchiseId
                    select new
                    {
                        l.Job.SalesPersonId,
                        l.IsPrimary
                    }).SingleOrDefault();
                if (existingQuote == null)
                    return "Quote does not exist";
                if (existingQuote.IsPrimary)
                    return "Unable to delete a primary quote, please assign another primary quote before trying again";

                if (BasePermission.CanDelete ||
                    (AssignedPermission.CanDelete && existingQuote.SalesPersonId == User.PersonId))
                {
                    var quote = new JobQuote {QuoteId = quoteId};

                    db.JobQuotes.Attach(quote);
                    quote.IsDeleted = true;
                    quote.LastUpdated = DateTimeOffset.Now;

                    db.SaveChanges();

                    return Success;
                }
                else
                    return Unauthorized;
            }
        }

        /// <summary>
        /// Sets primary quote
        /// </summary>
        /// <param name="quoteId">Quote Id of quote to set as primary</param>
        /// <param name="lastUpdated">Outputs last updated date</param>
        public string SetPrimaryQuote(int quoteId, out DateTimeOffset lastUpdated)
        {
            lastUpdated = DateTimeOffset.Now;
            if (quoteId <= 0)
                return "Quote Id is required";
            try
            {
                using (var db = ContextFactory.Create())
                {
                    var existingQuote = (from l in db.JobQuotes
                        where
                            l.QuoteId == quoteId && l.IsDeleted == false &&
                            l.Job.Lead.FranchiseId == Franchise.FranchiseId
                        select new
                        {
                            Quote = l,
                            l.Job.SalesPersonId
                        }).SingleOrDefault();
                    if (existingQuote == null)
                        return "Quote does not exist";

                    if (BasePermission.CanUpdate ||
                        (AssignedPermission.CanUpdate && existingQuote.SalesPersonId == User.PersonId))
                    {
                        if (!existingQuote.Quote.IsPrimary)
                        {
                            //remove previous primary
                            var quotes =
                                db.JobQuotes.Where(
                                    w =>
                                        w.JobId == existingQuote.Quote.JobId && w.IsDeleted == false &&
                                        w.IsPrimary == true)
                                    .ToList();
                            foreach (var q in quotes)
                            {
                                q.IsPrimary = false;
                            }

                            existingQuote.Quote.IsPrimary = true;
                            existingQuote.Quote.LastUpdated = lastUpdated;

                            var job = new Job
                            {
                                JobId = existingQuote.Quote.JobId,
                                EditHistories = new List<EditHistory>()
                            };
                            db.Jobs.Attach(job);

                            var history = new XElement("Quote", new XAttribute("operation", "update"),
                                new XElement("Primary",
                                    new XElement("Current", "Quote " + existingQuote.Quote.QuoteNumber)
                                    )
                                );
                            job.EditHistories.Add(new EditHistory
                            {
                                LoggedByPersonId = User.PersonId,
                                IPAddress =
                                    HttpContext.Current != null ? HttpContext.Current.Request.UserHostAddress : null,
                                HistoryValue = history.ToString(SaveOptions.None)
                            });
                        }

                        db.SaveChanges();

                        return Success;
                    }
                    else
                        return Unauthorized;
                }
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        #endregion

        /// <summary>
        /// Gets a list of jobs
        /// </summary>
        /// <param name="idList">Job Id list</param>
        /// <returns></returns>
        public override ICollection<Job> Get(List<int> idList)
        {
            if (!BasePermission.CanRead)
                throw new UnauthorizedAccessException(Unauthorized);
            if (idList == null)
                throw new ArgumentNullException("idList can not be null");
            if (idList.Count == 0)
                return new List<Job>();

            var result = new List<Job>();

            var pageSize = 5000;
            using (var db = ContextFactory.Create())
            {

                var copyIdList = idList.ToList();
                List<Job> list;
                if (idList.Count() > pageSize)
                {
                    list = new List<Job>();

                    do
                    {
                        List<int> ld1;
                        if (idList.Count() < pageSize)
                        {
                            ld1 = idList.Take(idList.Count()).ToList();
                            idList.RemoveRange(0, idList.Count());
                        }
                        else
                        {
                            ld1 = idList.Take(pageSize).ToList();
                            idList.RemoveRange(0, pageSize);
                        }


                        list.AddRange(GetJobsForJobIds(db, ld1));
                    } while (idList.Any());
                }
                else
                {
                    //list = db.Jobs
                    //    .Include(i => i.Customer)
                    //    .Include(i => i.BillingAddress)
                    //    .Include(i => i.InstallAddress)
                    //    .Include(i => i.JobNotes)
                    //    .Include(i => i.SalesPerson)
                    //    .Include(i => i.Lead)
                    //    .Include(i => i.Lead.PrimPerson)
                    //    .Include(i => i.Lead.SecPerson)
                    //    .Include(i => i.Lead.Addresses)
                    //    .Where(w => idList.Contains(w.JobId))
                    //    .ToList();
                    list = GetJobsForJobIds(db, idList);
                }



                //var list = CRMDBContext.Jobs
                //                    .Include(i => i.Customer)
                //                    .Include(i => i.BillingAddress)
                //                    .Include(i => i.InstallAddress)
                //                    .Include(i => i.JobNotes)
                //                    .Include(i => i.SalesPerson)
                //                    .Include(i => i.Lead)
                //                    .Include(i => i.Lead.PrimPerson)
                //                    .Include(i => i.Lead.SecPerson)
                //                    .Include(i => i.Lead.Addresses)
                //                    .Where(w => idList.Contains(w.JobId))
                //                    .ToList();
                idList = copyIdList;
                var quotes = new List<JobQuote>();
                if (idList.Count() > pageSize)
                {
                    list = new List<Job>();

                    do
                    {
                        List<int> ld1;
                        if (idList.Count() < pageSize)
                        {
                            ld1 = idList.Take(idList.Count()).ToList();
                            idList.RemoveRange(0, idList.Count());
                        }
                        else
                        {
                            ld1 = idList.Take(pageSize).ToList();
                            idList.RemoveRange(0, pageSize);
                        }

                        quotes.AddRange(GetJobQuotesForJobIds(db, ld1));

                    } while (idList.Any());
                }
                else
                {
                    //quotes = db.JobQuotes
                    //    .Where(w => w.IsPrimary && idList.Contains(w.JobId))
                    //    .Include(i => i.JobItems)
                    //    .Include(i => i.Invoices)
                    //    .Include(i => i.Invoices.Select(x => x.Payments))
                    //    .Include(i => i.SaleAdjustments).ToList();
                    quotes = GetJobQuotesForJobIds(db, idList);
                }

                //reference: http://stackoverflow.com/questions/11621611/entity-framework-trouble-with-load
                //for explicit loading, need to assign it to the navigation properties instead of calling .Load(), which only loads to the DbContext instead of populating the record
                //setup permission per lead
                Object lockMe = new Object();
                var basePermissionCanUpdate = BasePermission.CanUpdate;
                var basePermissionCanDelete = BasePermission.CanDelete;
                var assignedPermissionCanUpdate = AssignedPermission.CanUpdate;
                var assignedPermissionCanDelete = AssignedPermission.CanDelete;
                var statusPermissionCanUpdate = StatusPermission.CanUpdate;
                var distributionPermissionCanUpdate = DistributionPermission.CanUpdate;
                var costPermissionCanRead = CostPermission.CanRead;
                var costPermissionCanUpdate = CostPermission.CanUpdate;

                Parallel.ForEach(list, rec =>
                {
                    rec.JobQuotes = quotes.Where(w => w.JobId == rec.JobId).ToList();
                    if (rec.JobNotes != null && rec.JobNotes.Count > 0)
                    {
                        foreach (var note in rec.JobNotes)
                        {
                            var person =
                                CacheManager.UserCollection.FirstOrDefault(f => f.PersonId == note.CreatedByPersonId);
                            if (person != null)
                            {
                                note.CreatorFullName = person.Person.FullName;
                                note.AvatarSrc = person.AvatarSrc ?? AppConfigManager.DefaultAvatarSrc;
                            }
                            else
                            {
                                note.CreatorFullName = "";
                                note.AvatarSrc = AppConfigManager.DefaultAvatarSrc;
                            }
                        }
                        rec.JobNotes = rec.JobNotes.OrderByDescending(o => o.CreatedOn).ToList();
                    }

                    rec.Permission = new Permission
                    {
                        CanUpdate =
                            basePermissionCanUpdate ||
                            (assignedPermissionCanUpdate && (rec.SalesPersonId == User.PersonId)),
                        CanDelete =
                            basePermissionCanDelete ||
                            (assignedPermissionCanDelete && (rec.SalesPersonId == User.PersonId))
                    };
                    rec.CanUpdateStatus = statusPermissionCanUpdate;
                    rec.CanDistribute = distributionPermissionCanUpdate;
                    rec.CostPermission = new Permission
                    {
                        CanRead = costPermissionCanRead,
                        CanUpdate = costPermissionCanUpdate
                    };
                    //clear job cost info incase we have a user with some technical knowledge that can edit the javascript to enable CostPermission.CanRead to true
                    if (!costPermissionCanRead)
                    {
                        foreach (var q in rec.JobQuotes)
                        {
                            foreach (var item in q.JobItems)
                            {
                                item.UnitCost = 0;
                                item.CostSubtotal = 0;
                            }
                            q.NetProfit = 0;
                        }
                    }
                    lock (lockMe)
                    {
                        result.Add(rec);
                    }
                });
            }

            return result;
        }

        private List<JobQuote> GetJobQuotesForJobIds(CRMContextEx db, List<int> jobIds)
        {
            return db.JobQuotes
                .Where(w => w.IsPrimary && jobIds.Contains(w.JobId))
                .Include(i => i.JobItems)
                .Include(i => i.Invoices)
                .Include(i => i.Invoices.Select(x => x.Payments))
                .Include(i => i.SaleAdjustments).ToList();
        }

        private List<Job> GetJobsForJobIds(CRMContextEx db, List<int> ld1)
        {
            return db.Jobs
                .Include(i => i.Customer)
                .Include(i => i.BillingAddress)
                .Include(i => i.InstallAddress)
                .Include(i => i.JobNotes)
                .Include(i => i.SalesPerson)
                .Include(i => i.Lead)
                .Include(i => i.Lead.PrimCustomer)
                .Include(i => i.Lead.SecCustomer)
                .Include(i => i.Lead.Addresses)
                .Where(w => ld1.Contains(w.JobId))
                .ToList();
        }

        /// <summary>
        /// Gets a single Job
        /// </summary>
        /// <param name="id">Job Id</param>
        /// <returns></returns>
        public List<Invoice> GetInvoices(int id)
        {
            if (id <= 0)
                throw new ArgumentNullException(JobDoesNotExist);
            if (!BasePermission.CanRead)
                throw new UnauthorizedAccessException(Unauthorized);

            
            using (var db = ContextFactory.Create())
            {
                return db.Invoices.Where(W => W.JobId == id && W.IsDeleted == false)
                    .Include(i => i.Payments)
                    .ToList();
            }
        }

        public Person GetPerson(int personId)
        {
            if (personId <= 0)
                throw new ArgumentNullException(PerosnNotsExist);
            if (!BasePermission.CanRead)
                throw new UnauthorizedAccessException(Unauthorized);
            using (var db = ContextFactory.Create())
            {
                var result = db.People.SingleOrDefault(x => x.PersonId == personId);
                return result;
            }
        }

        /// <summary>
        /// Gets a single Job
        /// </summary>
        /// <param name="id">Job Id</param>
        /// <returns></returns>
        public override Job Get(int id)
        {
            if (id <= 0)
                throw new ArgumentNullException(JobDoesNotExist);
            if (!BasePermission.CanRead)
                throw new UnauthorizedAccessException(Unauthorized);

            using (var db = ContextFactory.Create())
            {
                var job =
                    db.Jobs.Where(W => W.JobId == id && W.IsDeleted == false && W.Lead.IsDeleted == false)
                        .Include(i => i.Customer)
                        .Include(i => i.Lead)
                        .Include(i => i.JobNotes)
                        .Include(i => i.Invoices.Select(x => x.Payments))
                        .Include(i => i.BillingAddress)
                        .Include(i => i.InstallAddress).SingleOrDefault();

                if (job != null)
                {
                    if (job.Lead.FranchiseId != Franchise.FranchiseId)
                    {
                        throw new UnauthorizedAccessException(Unauthorized);
                    }

                    job.JobQuotes = db.JobQuotes
                        .Include(i => i.JobItems)
                        .Include(i => i.JobItems.Select(s => s.Options))
                        .Include(i => i.SaleAdjustments)
                        .Where(w => w.IsDeleted == false && w.JobId == job.JobId).ToList();

                    //job.JobNotes = job.JobNotes.OrderByDescending(o => o.CreatedOn).ToList();
                    //foreach (var note in job.JobNotes)
                    //{
                    //    var person = CacheManager.UserCollection.FirstOrDefault(f => f.PersonId == note.CreatedByPersonId);
                    //    if (person != null)
                    //    {
                    //        note.CreatorFullName = person.Person.FullName;
                    //        note.AvatarSrc = person.AvatarSrc ?? AppConfigManager.DefaultAvatarSrc;
                    //    }
                    //    else
                    //    {
                    //        note.CreatorFullName = "";
                    //        note.AvatarSrc = AppConfigManager.DefaultAvatarSrc;
                    //    }
                    //}
                    //foreach (var file in job.PhysicalFiles)
                    //{
                    //    var person = CacheManager.UserCollection.FirstOrDefault(f => f.PersonId == file.AddedByPersonId);
                    //    if (person != null)
                    //        file.AddedByFullName = person.Person.FullName;
                    //}
                    //clear job cost info incase we have a user with some technical knowledge that can edit the javascript to enable CostPermission.CanRead to true
                    if (!CostPermission.CanRead)
                    {
                        foreach (var q in job.JobQuotes)
                        {
                            foreach (var item in q.JobItems)
                            {
                                item.UnitCost = 0;
                                item.CostSubtotal = 0;
                            }
                            q.NetProfit = 0;
                        }
                    }
                    job.Permission = new Permission
                    {
                        CanUpdate =
                            BasePermission.CanUpdate ||
                            (AssignedPermission.CanUpdate && (job.SalesPersonId == User.PersonId)),
                        CanDelete =
                            BasePermission.CanDelete ||
                            (AssignedPermission.CanDelete && (job.SalesPersonId == User.PersonId))
                    };
                    job.CostPermission = new Permission
                    {
                        CanRead = CostPermission.CanRead,
                        CanUpdate = CostPermission.CanUpdate
                    };
                    job.CanUpdateStatus = StatusPermission.CanUpdate;
                    job.CanDistribute = DistributionPermission.CanUpdate;
                }

                return job;
            }

        }

        /// <summary>
        /// Not Implemented
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public override string Add(Job data)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Not Implemented
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public override string Update(Job data)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<int> GetJobStatusesAfterSaleMade()
        {
            using (MiniProfiler.Current.Step("GetJobStatusesAfterSaleMade"))
            {
                var typeJobStatuses =
                    SessionManager.CurrentFranchise.JobStatusTypeCollection()
                        .Where(f => f.Name == "Sale Made" || f.Name == "Complete" || f.Name == "Service")
                        // TODO move magic to config
                        .Select(f => f.Id).ToList();
                var saleMadeStatusIds =
                    SessionManager.CurrentFranchise.JobStatusTypeCollection().
                        Where(
                            f => f.isDeleted != true && typeJobStatuses.Any(t => f.ParentId.HasValue && f.ParentId == t))
                        .
                        Select(f => f.Id).
                        Concat(typeJobStatuses).
                        ToList();

                return saleMadeStatusIds;
            }
        }

        public IEnumerable<int> GetJobStatusesAfterComplete()
        {
            using (MiniProfiler.Current.Step("GetJobStatusesAfterComplete"))
            {
                var typeJobStatuses =
                    SessionManager.CurrentFranchise.JobStatusTypeCollection()
                        .Where(f => f.Id == 29) // TODO move magic to config
                        .Select(f => f.Id).ToList();
                var completeStatusIds =
                    SessionManager.CurrentFranchise.JobStatusTypeCollection().
                        Where(
                            f => f.isDeleted != true && typeJobStatuses.Any(t => f.ParentId.HasValue && f.ParentId == t))
                        .
                        Select(f => f.Id).
                        Concat(typeJobStatuses).
                        ToList();

                return completeStatusIds;
            }
        }

        public IEnumerable<int> GetJobStatusesAfterQuoteGiven()
        {
            using (MiniProfiler.Current.Step("GetJobStatusesAfterQuoteGiven"))
            {
                var typeJobStatuses =
                    SessionManager.CurrentFranchise.JobStatusTypeCollection().
                        Where(
                            f =>
                                f.Name == "Quote Given" || f.Name == "Lost Sale" || f.Name == "Sale Made" ||
                                f.Name == "Complete" || f.Name == "Service") // TODO move magic to config
                        .Select(f => f.Id).ToList();
                var statuses =
                    SessionManager.CurrentFranchise.JobStatusTypeCollection().
                        Where(
                            f => f.isDeleted != true && typeJobStatuses.Any(t => f.ParentId.HasValue && f.ParentId == t))
                        .
                        Select(f => f.Id).
                        Concat(typeJobStatuses).
                        ToList();

                return statuses;
            }
        }

        /// <summary>
        /// Changes a customer in a job
        /// </summary>
        /// <param name="JobId"></param>
        /// <param name="customerPersonId"></param>
        /// <returns></returns>
        public string ChangeCustomer(int JobId, int customerPersonId)
        {
            if (JobId <= 0 || customerPersonId <= 0)
                return "Invalid customer id";

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var existingJob = (from l in db.Jobs
                        where l.JobId == JobId && l.IsDeleted == false && l.Lead.FranchiseId == Franchise.FranchiseId
                        select new {l.CustomerPersonId, l.Lead.PrimCustomer, l.Lead.SecCustomer, l.SalesPersonId})
                        .SingleOrDefault();
                    if (existingJob == null)
                        return JobDoesNotExist;

                    if (BasePermission.CanUpdate ||
                        (AssignedPermission.CanUpdate && existingJob.SalesPersonId == User.PersonId))
                    {
                        //if (existingJob.CustomerPersonId == customerPersonId)
                        //    return "Action cancelled, original and current customer is the same value";
                        //if (customerPersonId != existingJob.PrimPerson.PersonId &&
                        //    (existingJob.SecPerson == null || existingJob.SecPerson.PersonId != customerPersonId))
                        //    return "Unable to change customer, they are not attached to a lead";

                        //var job = new Job {JobId = JobId, EditHistories = new List<EditHistory>()};
                        //db.Jobs.Attach(job);

                        //var originalPerson = existingJob.CustomerPersonId == existingJob.PrimPerson.PersonId
                        //    ? existingJob.PrimPerson
                        //    : existingJob.SecPerson;
                        //var newPerson = customerPersonId == existingJob.PrimPerson.PersonId
                        //    ? existingJob.PrimPerson
                        //    : existingJob.SecPerson;

                        //XElement history =
                        //    new XElement("Job", new XAttribute("operation", "update"),
                        //        new XElement("Customer",
                        //            new XElement("Original", new XAttribute("id", existingJob.CustomerPersonId),
                        //                originalPerson.FullName),
                        //            new XElement("Current", new XAttribute("id", customerPersonId), newPerson.FullName)
                        //            )
                        //        );

                        //job.CustomerPersonId = customerPersonId;
                        //job.LastUpdatedByPersonId = User.PersonId;
                        //job.LastUpdated = DateTime.Now;
                        //job.EditHistories.Add(new EditHistory
                        //{
                        //    LoggedByPersonId = User.PersonId,
                        //    HistoryValue = history.ToString(SaveOptions.DisableFormatting),
                        //    IPAddress = HttpContext.Current.Request.UserHostAddress
                        //});

                        db.SaveChanges();
                        return Success;
                    }
                    else
                        return Unauthorized;
                }
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        public Product GetCustomProduct(Guid guid)
        {
            try
            {
                using (var db = ContextFactory.Create())
                {
                    var product =
                        db.Products.FirstOrDefault(w => w.isCustom.Value && w.ProductType.FranchiseId == Franchise.FranchiseId &&
                                                        w.ProductGuid == guid);


                    return product;
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }

        }


        public string GetCustomProduct(int? productTypeId, string mfg, string product, string style, out int? prodTypeId,
            out int? mfgId, out Guid? productGuid, out int? styleId, out string prodTypeName)
        {
            var status = FailedSaveChanges;
            prodTypeId = null;
            mfgId = null;
            productGuid = null;
            styleId = null;
            prodTypeName = null;

            try
            {
                using (var db = ContextFactory.Create())
                {

                    var type = productTypeId.HasValue
                        ? db.Type_Products.FirstOrDefault(w => w.ProductTypeId == productTypeId)
                        : null;
                    var manufacturer = GetManufactuer(mfg);
                    var cusProduct = manufacturer != null && type != null
                        ? db.Products.FirstOrDefault(
                            w =>
                                w.Name == product && w.ManufacturerId == manufacturer.ManufacturerId &&
                                w.ProductTypeId == type.ProductTypeId)
                        : null;
                    var typeStyle = GetStyleType(style);

                    if (cusProduct == null)
                    {
                        cusProduct =
                            db.Products.Add(new Product()
                            {
                                ProductGuid = Guid.NewGuid(),
                                Manufacturer = manufacturer,
                                Name = product,
                                ProductType = type,
                                isCustom = true
                            });
                        db.SaveChanges();
                        //var cusProdStyle = CRMDBContext.ProductStyles.Where(w => w.isCustom.Value && w.ProductId == cusProduct.ProductId && w.StyleId == cusStyle.Id);
                    }

                    prodTypeId = cusProduct != null ? cusProduct.ProductTypeId : null;
                    prodTypeName = cusProduct != null && cusProduct.ProductType != null
                        ? cusProduct.ProductType.TypeName
                        : null;
                    mfgId = cusProduct != null ? cusProduct.ManufacturerId : null;
                    productGuid = cusProduct != null ? cusProduct.ProductGuid : (Guid?) null;
                    styleId = typeStyle.Id;

                    status = Success;
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex.Message);
                status = ex.Message;
            }


            return status;
        }

        private Manufacturer GetManufactuer(string mfg)
        {
            try
            {
                using (var db = ContextFactory.Create())
                {
                    var manufacturer = db.Manufacturers.Where(w => w.Name == mfg).FirstOrDefault();
                    if (manufacturer == null)
                    {
                        manufacturer =
                            db.Manufacturers.Add(new Manufacturer()
                            {
                                ManufacturerGuid = Guid.NewGuid(),
                                Name = mfg,
                                isCustom = true
                            });
                        db.SaveChanges();
                        return manufacturer;
                    }

                    return manufacturer;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private Type_Style GetStyleType(string name)
        {
            try
            {
                using (var db = ContextFactory.Create())
                {
                    var style = db.Type_Styles.FirstOrDefault(w => w.Name == name);
                    if (style == null)
                    {
                        style = db.Type_Styles.Add(new Type_Style() {Name = name, isCustom = true});
                        db.SaveChanges();
                        return style;
                    }
                    return style;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #region api 2.0

        public List<JobShortInfoDTO> GetJobsShortInfo(int leadId)
        {
            using (MiniProfiler.Current.Step("GetJobsShortInfo"))
            {
                if (leadId <= 0) throw new ArgumentNullException(LeadDoesNotExist);
                if (!BasePermission.CanRead) throw new UnauthorizedAccessException(Unauthorized);

                try
                {
                    using (SqlConnection conn = new SqlConnection(EntityCnxStringBuilder.ProviderConnectionString))
                    {
                        conn.Open();
                        return
                            conn.Query<JobShortInfoDTO>("[CRM].[spGetJobsShortInfo] @leadId", new {leadId = leadId})
                                .ToList();
                    }
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
            }
        }

        public JobDTO GetJobDetails(int jobId)
        {
            using (MiniProfiler.Current.Step("GetJobDetails"))
            {
                if (jobId <= 0) throw new ArgumentNullException(JobDoesNotExist);
                if (!BasePermission.CanRead) throw new UnauthorizedAccessException(Unauthorized);

                var job = new JobDTO();

                try
                {
                    using (var db = ContextFactory.Create())
                    {
                        using (SqlConnection conn = new SqlConnection(EntityCnxStringBuilder.ProviderConnectionString))
                        {
                            conn.Open();
                            job =
                                conn.Query<JobDTO>("[CRM].[spGetJobDetails] @jobId", new {jobId = jobId})
                                    .FirstOrDefault();

                            job.CanDistribute = DistributionPermission.CanUpdate;
                            job.CanUpdateStatus = StatusPermission.CanUpdate;
                            job.Permission = new Permission
                            {
                                CanUpdate =
                                    BasePermission.CanUpdate ||
                                    (AssignedPermission.CanUpdate && (job.SalesPersonId == User.PersonId)),
                                CanDelete =
                                    BasePermission.CanDelete ||
                                    (AssignedPermission.CanDelete && (job.SalesPersonId == User.PersonId))
                            };

                            job.SalesPerson = db.People.FirstOrDefault(u => u.PersonId == job.SalesPersonId);
                            job.InstallerPerson =
                                db.People.FirstOrDefault(u => u.PersonId == job.InstallerPersonId);
                            job.Customer = db.People.FirstOrDefault(u => u.PersonId == job.CustomerPersonId);

                            job.CostPermission = new Permission
                            {
                                CanRead = CostPermission.CanRead,
                                CanUpdate = CostPermission.CanUpdate
                            };

                            job.JobQuestionAnswers = new List<JobQuestionAnswersDTO>();


                            job.PhysicalFiles =
                                conn.Query<FileDTO>("[CRM].[spGetJobFiles] @JobId", new {jobId = jobId}).ToList();

                            job.JobQuestionAnswers =
                                conn.Query<JobQuestionAnswersDTO>("[CRM].[spGetJobQuestionAnswers] @JobId",
                                    new {jobId = jobId}).ToList();

                            var activeQuotes =
                                db.JobQuotes.
                                    Include(i => i.JobItems).
                                    Include(i => i.Invoices).
                                    Include(i => i.Invoices.Select(x => x.Payments)).
                                    Include(i => i.JobItems.Select(x => x.Options)).
                                    Include(i => i.SaleAdjustments).
                                    Where(w => w.IsDeleted == false && w.JobId == jobId).
                                    ToList();

                            job.JobQuotes = activeQuotes.Where(w => w.JobId == job.JobId).ToList();

                            job.PrimeQuote = activeQuotes.FirstOrDefault(q => q.QuoteId == job.QuoteId);

                            job.Invoices = (activeQuotes.FirstOrDefault(p => p.JobId == job.JobId) != null)
                                ? activeQuotes.FirstOrDefault(p => p.JobId == job.JobId && p.IsPrimary)
                                    .Invoices.Where(x => x.IsDeleted == false)
                                    .ToList()
                                : null;

                            var entity = db.Jobs.First(j => j.JobId == jobId);
                            if (job.CostPermission.CanRead == false)
                            {
                                job.JobQuotes.ForEach(x => x.JobItems.ForEach(y => y.UnitCost = 0));
                            }
                            job.Balance = entity.Balance;
                            job.LeadId = entity.LeadId;
                        }
                        return job;
                    }
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
            }
        }



        public JobQuoteDTO GetJobQouteDetails(int quoteId)
        {
            using (MiniProfiler.Current.Step("GetJobDetails"))
            {
                if (quoteId <= 0) throw new ArgumentNullException(JobDoesNotExist);
                if (!BasePermission.CanRead) throw new UnauthorizedAccessException(Unauthorized);

                var dto = new JobQuoteDTO();

                try
                {
                    using (SqlConnection conn = new SqlConnection(EntityCnxStringBuilder.ProviderConnectionString))
                    {
                        conn.Open();
                        dto =
                            conn.Query<JobQuoteDTO>("[CRM].[spGetJobQuoteDetails] @quoteId", new {quoteId = quoteId})
                                .FirstOrDefault();
                        dto.CanDistribute = DistributionPermission.CanUpdate;
                        dto.CanUpdateStatus = StatusPermission.CanUpdate;

                        dto.CostPermission = new Permission
                        {
                            CanRead = CostPermission.CanRead,
                            CanUpdate = CostPermission.CanUpdate
                        };

                        dto.SaleAdjustments = new List<JobSaleAdjustmentDTO>();
                        dto.JobItems = new List<JobItemDTO>();

                        dto.SaleAdjustments =
                            conn.Query<JobSaleAdjustmentDTO>("[CRM].[spGetJobSaleAdjustments] @QuoteId",
                                new {quoteId = quoteId}).ToList();
                        dto.JobItems =
                            conn.Query<JobItemDTO>("[CRM].[spGetJobItems] @QuoteId", new {quoteId = quoteId}).ToList();
                    }
                    return dto;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
            }
        }

        //spGetJobQuoteDetailsByJobIds
        public List<JobQuoteDTO> GetJobQuoteDetailsByJobIds(List<int> jobIds)
        {
            using (SqlConnection conn = new SqlConnection(EntityCnxStringBuilder.ProviderConnectionString))
            {
                conn.Open();
                return
                    conn.Query<JobQuoteDTO>("[CRM].[spGetJobQuoteDetailsByJobIds] @JobIds",
                        new {jobIds = string.Join(",", jobIds)}).ToList();

            }
        }

        public JobQuoteDTO QoutePrimeDetailsByJobId(int jobId)
        {
            using (MiniProfiler.Current.Step("GetJobDetails"))
            {
                if (jobId <= 0) throw new ArgumentNullException(JobDoesNotExist);
                if (!BasePermission.CanRead) throw new UnauthorizedAccessException(Unauthorized);

                using (var db = ContextFactory.Create())
                {
                    var quote =
                        db.JobQuotes.FirstOrDefault(w => w.JobId == jobId && w.IsPrimary && !w.IsDeleted);
                    var quoteId = quote.QuoteId;

                    try
                    {
                        JobQuoteDTO dto;
                        using (SqlConnection conn = new SqlConnection(EntityCnxStringBuilder.ProviderConnectionString))
                        {
                            conn.Open();
                            dto =
                                conn.Query<JobQuoteDTO>("[CRM].[spGetJobQuoteDetails] @quoteId", new {quoteId = quoteId})
                                    .FirstOrDefault();
                            dto.JobId = jobId;
                            dto.DisplayCost = true;
                            dto.CanDistribute = DistributionPermission.CanUpdate;
                            dto.CanUpdateStatus = StatusPermission.CanUpdate;

                            dto.CostPermission = new Permission
                            {
                                CanRead = CostPermission.CanRead,
                                CanUpdate = CostPermission.CanUpdate
                            };

                            //dto.InstallAddress =  CRMDBContext.Addresses.FirstOrDefault(v=>v.AddressId == )

                            dto.SaleAdjustments = new List<JobSaleAdjustmentDTO>();
                            dto.JobItems = new List<JobItemDTO>();

                            dto.SaleAdjustments =
                                conn.Query<JobSaleAdjustmentDTO>("[CRM].[spGetJobSaleAdjustments] @QuoteId",
                                    new {quoteId = quoteId}).ToList();
                            dto.JobItems =
                                conn.Query<JobItemDTO>("[CRM].[spGetJobItems] @QuoteId", new {quoteId = quoteId})
                                    .ToList();
                            decimal subtotal = 0,
                                costSubtotal = 0,
                                taxTotal = 0,
                                surchargeTotal = 0,
                                surchargeTaxableTotal = 0,
                                discountTotal = 0,
                                discountTaxableTotal = 0,
                                taxable_subtotal = 0,
                                taxRate = 0,
                                totalLineDiscounts = 0;
                            if (dto.JobItems != null && dto.JobItems.Count > 0)
                            {
                                subtotal = Math.Round(dto.JobItems.Sum(s => (s.Subtotal)), 4, MidpointRounding.ToEven);
                                taxable_subtotal =
                                    Math.Round(dto.JobItems.Where(w => w.IsTaxable).Sum(s => (s.Subtotal)), 4,
                                        MidpointRounding.ToEven);
                                costSubtotal = Math.Round(dto.JobItems.Sum(s => s.Quantity*s.UnitCost), 4,
                                    MidpointRounding.ToEven);
                                totalLineDiscounts = Math.Round(dto.JobItems.Sum(s => s.DiscountAmount), 4,
                                    MidpointRounding.ToEven);
                            }
                            if (dto.SaleAdjustments != null && dto.SaleAdjustments.Count > 0)
                            {
                                taxRate =
                                    dto.SaleAdjustments.Where(w => w.TypeEnum == (decimal) SaleAdjTypeEnum.Tax)
                                        .Sum(s => s.Percent);

                                var surchargeItems =
                                    dto.SaleAdjustments.Where(w => w.TypeEnum == (decimal) SaleAdjTypeEnum.Surcharge);
                                var discountItems =
                                    dto.SaleAdjustments.Where(w => w.TypeEnum == (decimal) SaleAdjTypeEnum.Discount);

                                surchargeTaxableTotal =
                                    surchargeItems.Where(w => w.IsTaxable).Sum(s => s.Amount + (subtotal*s.Percent/100));
                                discountTaxableTotal =
                                    discountItems.Where(w => w.IsTaxable).Sum(s => s.Amount + (subtotal*s.Percent/100));

                                taxable_subtotal = taxable_subtotal + surchargeTaxableTotal - discountTaxableTotal;

                                taxTotal = Math.Round((taxRate/100.0M)*(taxable_subtotal), 4, MidpointRounding.ToEven);

                                surchargeTotal =
                                    Math.Round(surchargeItems.Sum(s => s.Amount), 4, MidpointRounding.ToEven) +
                                    (subtotal*(surchargeItems.Sum(s => s.Percent)/100));
                                discountTotal =
                                    Math.Round(discountItems.Sum(s => s.Amount), 4, MidpointRounding.ToEven) +
                                    (subtotal*(discountItems.Sum(s => s.Percent)/100));
                            }

                            dto.TaxPercent = taxRate;
                            dto.TaxTotal = taxTotal;
                            dto.SubTotal = subtotal;
                            dto.CostSubtotal = costSubtotal;
                            dto.SurchargeTotal = surchargeTotal;
                            dto.DiscountTotal = discountTotal + totalLineDiscounts;

                            dto.NetTotal = Math.Round(subtotal + taxTotal + surchargeTotal - discountTotal, 2,
                                MidpointRounding.AwayFromZero);
                            dto.NetProfit = Math.Round(subtotal + surchargeTotal - costSubtotal - discountTotal, 2,
                                MidpointRounding.AwayFromZero);

                            dto.Margin = subtotal + surchargeTotal - discountTotal == 0
                                ? 0
                                : Math.Round(
                                    (subtotal + surchargeTotal - costSubtotal - discountTotal)
                                    /((subtotal + surchargeTotal - discountTotal) == 0
                                        ? 1
                                        : (subtotal + surchargeTotal - discountTotal))*100.0M,
                                    2);


                        }
                        return dto;
                    }
                    catch (Exception ex)
                    {
                        EventLogger.LogEvent(ex);
                        throw;
                    }
                }
            }
        }

        public List<FileDTO> GetJobFiles(int jobId)
        {
            using (MiniProfiler.Current.Step("GetJobFiles"))
            {
                if (jobId <= 0) throw new ArgumentNullException(JobDoesNotExist);
                if (!BasePermission.CanRead) throw new UnauthorizedAccessException(Unauthorized);

                var dto = new List<FileDTO>();

                try
                {
                    using (SqlConnection conn = new SqlConnection(EntityCnxStringBuilder.ProviderConnectionString))
                    {
                        conn.Open();
                        dto = conn.Query<FileDTO>("[CRM].[spGetJobFiles] @JobId", new {jobId = jobId}).ToList();
                    }
                    return dto;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
            }
        }

        public List<JobNoteDTO> GetJobNotes(int jobId)
        {
            using (MiniProfiler.Current.Step("GetJobNotes"))
            {
                if (jobId <= 0) throw new ArgumentNullException(JobDoesNotExist);
                if (!BasePermission.CanRead) throw new UnauthorizedAccessException(Unauthorized);

                try
                {
                    List<JobNoteDTO> dto;

                    using (SqlConnection conn = new SqlConnection(EntityCnxStringBuilder.ProviderConnectionString))
                    {
                        conn.Open();
                        dto = conn.Query<JobNoteDTO>("[CRM].[spGetJobNotes] @JobId", new {jobId}).ToList();
                    }

                    return dto;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
            }
        }

        public async Task<IEnumerable<JobNoteDTO>> GetJobNotes(List<int> jobIds)
        {
            using (MiniProfiler.Current.Step("GetJobNotes"))
            {
                if (!jobIds.Any()) return new List<JobNoteDTO>();
                if (!BasePermission.CanRead) throw new UnauthorizedAccessException(Unauthorized);

                try
                {
                    using (SqlConnection conn = new SqlConnection(EntityCnxStringBuilder.ProviderConnectionString))
                    {
                        await conn.OpenAsync();
                        return
                            await
                                conn.QueryAsync<JobNoteDTO>("[CRM].[spGetJobNotesXML] @JobIds",
                                    new {jobIds = string.Join(",", jobIds)});
                    }
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
            }
        }

        #endregion

        public int GetNewJobCount()
        {
            using (var db = ContextFactory.Create())
            {
                if (Franchise == null)
                    return 0;
                var counter = (from p in db.Leads.AsQueryable()
                    join u in db.Jobs.AsQueryable() on p.LeadId equals u.LeadId
                    where
                        p.FranchiseId == Franchise.FranchiseId && u.JobStatusId == 27 && u.IsDeleted == false &&
                        p.IsDeleted == false
                    select u.JobId).Count();
                return counter;
            }
        }
    }
}
