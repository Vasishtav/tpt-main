﻿using System.Data.Entity.Core.EntityClient;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.DTO.Search;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Dapper;

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity.Internal;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;

namespace HFC.CRM.Managers
{

    public class SearchManager : DataManager
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SearchManager"/> class.
        /// </summary>
        /// <param name="franchise">The franchise.</param>
        /// <param name="user"></param>
        public SearchManager(User user, Franchise franchise)
        {
            Franchise = franchise;
            AuthorizingUser = user;
        }

        private EntityConnectionStringBuilder EntityCnxStringBuilder = new EntityConnectionStringBuilder
            (System.Configuration.ConfigurationManager
                .ConnectionStrings["CRMContext"].ConnectionString);

        public async Task<IEnumerable<LeadSeachResultNewModel>> GetSearchLeads(bool LostLead)
        {

            try
            {
                //IEnumerable<LeadSeachResultModel> dto = Enumerable.Empty<LeadSeachResultModel>();

                using (SqlConnection conn = new SqlConnection(EntityCnxStringBuilder.ProviderConnectionString))
                {
                    return
                    await
               conn.QueryAsync<LeadSeachResultNewModel>(
                   "[CRM].[LeadSearch] @FranchiseId, @LostLead",
                   new
                   {
                       franchiseId = Franchise.FranchiseId,
                       LostLead = LostLead
                   });

                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }

        }

        public async Task<IEnumerable<AccountSeachResultModel>> ApplyLeadMatchingAccount(int accountId)
        {
            try
            {
                IEnumerable<AccountSeachResultModel> dto = Enumerable.Empty<AccountSeachResultModel>();

                using (SqlConnection conn = new SqlConnection(EntityCnxStringBuilder.ProviderConnectionString))
                {

                    return
                              await
                conn.QueryAsync<AccountSeachResultModel>(
                      "[CRM].[SeachLeadAccountById] @AccountId",
                        new
                        {
                            AccountId = accountId
                        });
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }

        }

        public async Task<IEnumerable<AccountSeachResultModel>> GetLeadMatchingAccounts(int leadid)
        {
            try
            {
                IEnumerable<AccountSeachResultModel> dto = Enumerable.Empty<AccountSeachResultModel>();

                using (SqlConnection conn = new SqlConnection(EntityCnxStringBuilder.ProviderConnectionString))
                {

                    return
                              await
                conn.QueryAsync<AccountSeachResultModel>(
                      "[CRM].[ConvertLeadAccountSearch] @LeadId",
                        new
                        {
                            LeadId = leadid
                        });
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }

        }

        public async Task<IEnumerable<AccountSeachResultNewModel>> GetSearchAccounts(bool includeInactiveMerged = false)
        {
            try
            {
                //IEnumerable<AccountSeachResultModel> dto = Enumerable.Empty<AccountSeachResultModel>();

                using (SqlConnection conn = new SqlConnection(EntityCnxStringBuilder.ProviderConnectionString))
                {
                    return await conn.QueryAsync<AccountSeachResultNewModel>("[CRM].[AccountSearch] @FranchiseId, @IncludeInactiveMerged"
                        , new
                        {
                            franchiseId = Franchise.FranchiseId,
                            IncludeInactiveMerged = includeInactiveMerged
                        });
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }

        }

        public async Task<IEnumerable<OrderSeachResultModel>> GetSearchOrders(int id, string selectedOrderStatus, string dateRangeValue)
        {
            try
            {
                IEnumerable<OrderSeachResultModel> dto = Enumerable.Empty<OrderSeachResultModel>();

                if (id == 0)
                {
                    using (SqlConnection conn = new SqlConnection(EntityCnxStringBuilder.ProviderConnectionString))
                    {
                        return await conn.QueryAsync<OrderSeachResultModel>("[CRM].[OrderSearch] @FranchiseId,@DateRangeId,@selectedOrderStatus",
                            new
                            {
                                franchiseId = Franchise.FranchiseId,
                                DateRangeId = dateRangeValue ?? "",
                                selectedOrderStatus = selectedOrderStatus ?? ""
                            });
                    }
                }
                else
                {
                    using (SqlConnection conn = new SqlConnection(EntityCnxStringBuilder.ProviderConnectionString))
                    {
                        return await conn.QueryAsync<OrderSeachResultModel>("[CRM].[OrderSearchByAccoutId] @FranchiseId,@AccountId", new { franchiseId = Franchise.FranchiseId, AccountId = id });
                    }
                }

            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }

        }

        public async Task<IEnumerable<PaymentSeachResultModel>> GetSearchAccountPayments(int id)
        {
            try
            {
                IEnumerable<PaymentSeachResultModel> dto = Enumerable.Empty<PaymentSeachResultModel>();


                using (SqlConnection conn = new SqlConnection(EntityCnxStringBuilder.ProviderConnectionString))
                {
                    return

                                        await
               conn.QueryAsync<PaymentSeachResultModel>(
                   "[CRM].[PaymentsSearchByAccoutId] @AccountId",
                   new
                   {
                       AccountId = id
                   });

                }


            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }

        }

        public async Task<IEnumerable<InvoiceSeachResultModel>> GetSearchAccountInvoices(int id)
        {
            try
            {
                IEnumerable<InvoiceSeachResultModel> dto = Enumerable.Empty<InvoiceSeachResultModel>();


                using (SqlConnection conn = new SqlConnection(EntityCnxStringBuilder.ProviderConnectionString))
                {
                    return

                                        await
               conn.QueryAsync<InvoiceSeachResultModel>(
                   "[CRM].[InvoiceSearchByAccoutId] @AccountId",
                   new
                   {
                       AccountId = id
                   });

                }


            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }

        }
        public async Task<IEnumerable<OpportunitySeachResultModel>> GetSearchOpportunitys(int Id, int opportunityId=0)
        {
            try
            {
                IEnumerable<OpportunitySeachResultModel> dto = Enumerable.Empty<OpportunitySeachResultModel>();

                using (SqlConnection conn = new SqlConnection(EntityCnxStringBuilder.ProviderConnectionString))
                {
                    if (opportunityId > 0)
                    {
                        var query = @"SELECT op.TerritoryType AS Territory,  op.OpportunityId,
                                        op.OpportunityName as OpportunityFullName,
                                        (select  Name from crm.Type_OpportunityStatus
                                         where OpportunityStatusId in(op.OpportunityStatusId)) as  OpportunityStatus,
                                        (select top 1 OrderTotal from [crm].[Orders] where OpportunityId in(op.OpportunityId) and OrderStatus !=9 and OrderStatus !=6) as OrderAmount,
                                        op.SideMark,
                                        op.OpportunityStatusId,
                                        op.SalesAgentId,
                                        op.InstallerId,
                                        op.InstallationAddressId,
                                        op.InstallationAddress,
                                        op.[Description],
                                        op.LeadId,
                                        op.AccountId as  AccountId,
                                        op.OpportunityNumber,
                                        [CRM].[fnGetAccountName_forOpportunitySearch](op.OpportunityId) as AccountName,
                                        (select FirstName+' '+LastName from [CRM].[Person] where PersonId in(op.SalesAgentId)) as SalesAgentName,
                                        (select top 1 ContractedDate from crm.orders where opportunityId in(op.OpportunityId) and OrderStatus !=9 and OrderStatus !=6) as ContractDate
                                        from [CRM].[Opportunities]  op
                                        left join [crm].[Accounts] a on op.AccountId=a.AccountId
                                        WHERE 
                                        a.AccountId=@AccountId and op.OpportunityId not in (select OpportunityId from CRM.Quote where QuoteStatusId =3 and OpportunityId=op.OpportunityId)
                                        order by op.OpportunityId desc";

                        var res = conn.Query<OpportunitySeachResultModel>(query, new { AccountId = Id }).ToList();
                        return res;
                    }
                    else
                    {
                        if (Id == 0)
                        {
                            return
                            await
                            conn.QueryAsync<OpportunitySeachResultModel>(
                            "[CRM].[OpportunitySearch] @FranchiseId",
                            new
                            {
                                franchiseId = SessionManager.CurrentFranchise.FranchiseId
                            });
                        }
                        else
                        {
                            return
                            await
                            conn.QueryAsync<OpportunitySeachResultModel>(
                            "[CRM].[OpportunitySearchByAccountId] @AccountId",
                            new
                            {
                                AccountId = Id
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }

        }

        public async Task<IEnumerable<InvoiceSearchDTO>> GetInvoices(
            List<int> jobStatusIds = null,
            List<int> installerPersonIds = null,
            List<int> salesPersonIds = null,
            List<int> leadStatusIds = null,
            List<int> invoiceStatuses = null,
            List<int> sourceIds = null,
            string searchTerm = null,
            SearchFilterEnum searchFilter = SearchFilterEnum.Auto_Detect,
            DateTimeOffset? createdOnStart = null,
            DateTimeOffset? createdOnEnd = null,
            int pageNum = 1,
            int pageSize = 25,
            string orderBy = null,
            OrderByEnum orderDirection = OrderByEnum.Desc)
        {

            //totalRecords = 0;
            var pageIndex = pageNum;
            pageIndex = pageIndex - 1;
            IEnumerable<InvoiceSearchDTO> dto = Enumerable.Empty<InvoiceSearchDTO>();
            using (SqlConnection conn = new SqlConnection(EntityCnxStringBuilder.ProviderConnectionString))
            {
                await conn.OpenAsync();
                try
                {
                    if (!string.IsNullOrEmpty(searchTerm))
                    {
                        SearchFilterEnum realSearchFilter = searchFilter;
                        int anumber = 0;

                        if (searchFilter == SearchFilterEnum.Auto_Detect)
                        {
                            dto =
                                await conn.QueryAsync<InvoiceSearchDTO>(
                                    "[crm].[InvoiceSearchAuto_Detect] @FranchiseId, @SearchTerm, @PageSize, @PageNumber, @SortOrder, @SortDirection, @leadStatusIds, @installerPersonIds, @salesPersonIds, @jobStatusIds, @InvoiceStatuses, @SourceIds, @createdOnUtcStart,@createdOnUtcEnd  ",
                                    new
                                    {
                                        franchiseId = Franchise.FranchiseId,
                                        searchTerm = searchTerm,
                                        pageSize = pageSize,
                                        pageNumber = pageIndex,
                                        sortOrder = orderBy,
                                        sortDirection = orderDirection.ToString(),
                                        leadStatusIds = leadStatusIds != null ? string.Join(",", leadStatusIds) : null,
                                        installerPersonIds =
                                            installerPersonIds != null ? string.Join(",", installerPersonIds) : null,
                                        salesPersonIds =
                                            salesPersonIds != null ? string.Join(",", salesPersonIds) : null,
                                        jobStatusIds = jobStatusIds != null ? string.Join(",", jobStatusIds) : null,
                                        invoiceStatuses =
                                            invoiceStatuses != null
                                                ? string.Join(",", invoiceStatuses)
                                                : null,
                                        sourceIds =
                                            sourceIds != null ? string.Join(",", sourceIds) : null,
                                        createdOnUtcStart =
                                            createdOnStart.HasValue ? (DateTime?)createdOnStart.Value.Date : null,
                                        createdOnUtcEnd =
                                            createdOnEnd.HasValue
                                                ? (DateTime?)createdOnEnd.Value.Date.AddDays(1)
                                                : null
                                    });

                        }
                        else if (realSearchFilter == SearchFilterEnum.Number)
                        {
                            if (int.TryParse(searchTerm, out anumber))
                            {
                                dto =
                                    await conn.QueryAsync<InvoiceSearchDTO>(
                                        "[crm].[InvoiceSearchNumer] @FranchiseId, @SearchTerm, @PageSize, @PageNumber, @SortOrder, @SortDirection, @leadStatusIds, @installerPersonIds, @salesPersonIds, @jobStatusIds, @InvoiceStatuses, @SourceIds, @createdOnUtcStart,@createdOnUtcEnd  ",
                                        new
                                        {
                                            franchiseId = Franchise.FranchiseId,
                                            searchTerm = anumber,
                                            pageSize = pageSize,
                                            pageNumber = pageIndex,
                                            sortOrder = orderBy,
                                            sortDirection = orderDirection.ToString(),
                                            leadStatusIds =
                                                leadStatusIds != null ? string.Join(",", leadStatusIds) : null,
                                            installerPersonIds =
                                                installerPersonIds != null ? string.Join(",", installerPersonIds) : null,
                                            salesPersonIds =
                                                salesPersonIds != null ? string.Join(",", salesPersonIds) : null,
                                            jobStatusIds = jobStatusIds != null ? string.Join(",", jobStatusIds) : null,
                                            invoiceStatuses =
                                                invoiceStatuses != null
                                                    ? string.Join(",", invoiceStatuses)
                                                    : null,
                                            sourceIds =
                                                sourceIds != null ? string.Join(",", sourceIds) : null,
                                            createdOnUtcStart =
                                                createdOnStart.HasValue ? (DateTime?)createdOnStart.Value.Date : null,
                                            createdOnUtcEnd =
                                                createdOnEnd.HasValue
                                                    ? (DateTime?)createdOnEnd.Value.Date.AddDays(1)
                                                    : null
                                        });
                            }
                        }
                        //else if (realSearchFilter == SearchFilterEnum.Invoice_Number)
                        //{
                        //    if (int.TryParse(searchTerm, out anumber))
                        //    {
                        //        dto =
                        //            await conn.QueryAsync<InvoiceSearchDTO>(
                        //                "[crm].[InvoiceSearchInvoiceNumer] @FranchiseId, @SearchTerm, @PageSize, @PageNumber, @SortOrder, @SortDirection, @jobStatusIds, @leadStatusIds, @salesPersonIds,@InvoiceStatuses, @createdOnUtcStart,@createdOnUtcEnd ",
                        //                new
                        //                {
                        //                    franchiseId = Franchise.FranchiseId,
                        //                    searchTerm = anumber,
                        //                    pageSize = pageSize,
                        //                    pageNumber = pageIndex,
                        //                    sortOrder = orderBy,
                        //                    sortDirection = orderDirection.ToString(),
                        //                    jobStatusIds = jobStatusIds != null ? string.Join(",", jobStatusIds) : null,
                        //                    leadStatusIds = leadStatusIds != null ? string.Join(",", leadStatusIds) : null,
                        //                    salesPersonIds =
                        //                        salesPersonIds != null ? string.Join(",", salesPersonIds) : null,
                        //                    invoiceStatuses =
                        //                        invoiceStatuses != null
                        //                            ? string.Join(",", invoiceStatuses)
                        //                            : null,
                        //                    createdOnUtcStart = createdOnStart,
                        //                    createdOnUtcEnd = createdOnEnd
                        //                });
                        //    }
                        //}
                        else if (realSearchFilter == SearchFilterEnum.Phone_Number)
                        {
                            string parsedPhone = RegexUtil.GetNumbersOnly(searchTerm);
                            dto =
                                await conn.QueryAsync<InvoiceSearchDTO>(
                                    "[crm].[InvoiceSearchPhoneNumber] @FranchiseId, @SearchTerm, @PageSize, @PageNumber, @SortOrder, @SortDirection, @leadStatusIds, @installerPersonIds, @salesPersonIds, @jobStatusIds, @InvoiceStatuses, @SourceIds, @createdOnUtcStart,@createdOnUtcEnd  ",
                                    new
                                    {
                                        franchiseId = Franchise.FranchiseId,
                                        searchTerm = parsedPhone,
                                        pageSize = pageSize,
                                        pageNumber = pageIndex,
                                        sortOrder = orderBy,
                                        sortDirection = orderDirection.ToString(),
                                        leadStatusIds = leadStatusIds != null ? string.Join(",", leadStatusIds) : null,
                                        installerPersonIds =
                                            installerPersonIds != null ? string.Join(",", installerPersonIds) : null,
                                        salesPersonIds =
                                            salesPersonIds != null ? string.Join(",", salesPersonIds) : null,
                                        jobStatusIds = jobStatusIds != null ? string.Join(",", jobStatusIds) : null,
                                        invoiceStatuses =
                                            invoiceStatuses != null
                                                ? string.Join(",", invoiceStatuses)
                                                : null,
                                        sourceIds =
                                            sourceIds != null ? string.Join(",", sourceIds) : null,
                                        createdOnUtcStart =
                                            createdOnStart.HasValue ? (DateTime?)createdOnStart.Value.Date : null,
                                        createdOnUtcEnd =
                                            createdOnEnd.HasValue
                                                ? (DateTime?)createdOnEnd.Value.Date.AddDays(1)
                                                : null
                                    });

                        }
                        else if (realSearchFilter == SearchFilterEnum.Email)
                        {
                            dto =
                                await conn.QueryAsync<InvoiceSearchDTO>(
                                    "[crm].[InvoiceSearchEmail] @FranchiseId, @SearchTerm, @PageSize, @PageNumber, @SortOrder, @SortDirection, @leadStatusIds, @installerPersonIds, @salesPersonIds, @jobStatusIds, @InvoiceStatuses, @SourceIds, @createdOnUtcStart,@createdOnUtcEnd  ",
                                    new
                                    {
                                        franchiseId = Franchise.FranchiseId,
                                        searchTerm = searchTerm,
                                        pageSize = pageSize,
                                        pageNumber = pageIndex,
                                        sortOrder = orderBy,
                                        sortDirection = orderDirection.ToString(),
                                        leadStatusIds = leadStatusIds != null ? string.Join(",", leadStatusIds) : null,
                                        installerPersonIds =
                                            installerPersonIds != null ? string.Join(",", installerPersonIds) : null,
                                        salesPersonIds =
                                            salesPersonIds != null ? string.Join(",", salesPersonIds) : null,
                                        jobStatusIds = jobStatusIds != null ? string.Join(",", jobStatusIds) : null,
                                        invoiceStatuses =
                                            invoiceStatuses != null
                                                ? string.Join(",", invoiceStatuses)
                                                : null,
                                        sourceIds =
                                            sourceIds != null ? string.Join(",", sourceIds) : null,
                                        createdOnUtcStart =
                                            createdOnStart.HasValue ? (DateTime?)createdOnStart.Value.Date : null,
                                        createdOnUtcEnd =
                                            createdOnEnd.HasValue
                                                ? (DateTime?)createdOnEnd.Value.Date.AddDays(1)
                                                : null
                                    });

                        }

                        else if (realSearchFilter == SearchFilterEnum.Customer_Name)
                        {
                            dto =
                                await conn.QueryAsync<InvoiceSearchDTO>(
                                    "[crm].[InvoiceSearchCustomerName] @FranchiseId, @SearchTerm, @PageSize, @PageNumber, @SortOrder, @SortDirection, @leadStatusIds, @installerPersonIds, @salesPersonIds, @jobStatusIds, @InvoiceStatuses, @SourceIds, @createdOnUtcStart,@createdOnUtcEnd  ",
                                    new
                                    {
                                        franchiseId = Franchise.FranchiseId,
                                        searchTerm = searchTerm,
                                        pageSize = pageSize,
                                        pageNumber = pageIndex,
                                        sortOrder = orderBy,
                                        sortDirection = orderDirection.ToString(),
                                        leadStatusIds = leadStatusIds != null ? string.Join(",", leadStatusIds) : null,
                                        installerPersonIds =
                                            installerPersonIds != null ? string.Join(",", installerPersonIds) : null,
                                        salesPersonIds =
                                            salesPersonIds != null ? string.Join(",", salesPersonIds) : null,
                                        jobStatusIds = jobStatusIds != null ? string.Join(",", jobStatusIds) : null,
                                        invoiceStatuses =
                                            invoiceStatuses != null
                                                ? string.Join(",", invoiceStatuses)
                                                : null,
                                        sourceIds =
                                            sourceIds != null ? string.Join(",", sourceIds) : null,
                                        createdOnUtcStart =
                                            createdOnStart.HasValue ? (DateTime?)createdOnStart.Value.Date : null,
                                        createdOnUtcEnd =
                                            createdOnEnd.HasValue
                                                ? (DateTime?)createdOnEnd.Value.Date.AddDays(1)
                                                : null
                                    });

                        }
                    }
                    else
                    {
                        dto =
                            await conn.QueryAsync<InvoiceSearchDTO>(
                                "[crm].[InvoiceSearchCustomerName] @FranchiseId, @SearchTerm, @PageSize, @PageNumber, @SortOrder, @SortDirection, @leadStatusIds, @installerPersonIds, @salesPersonIds, @jobStatusIds, @InvoiceStatuses,@SourceIds, @createdOnUtcStart,@createdOnUtcEnd  ",
                                new
                                {
                                    franchiseId = Franchise.FranchiseId,
                                    searchTerm = searchTerm,
                                    pageSize = pageSize,
                                    pageNumber = pageIndex,
                                    sortOrder = orderBy,
                                    sortDirection = orderDirection.ToString(),
                                    leadStatusIds = leadStatusIds != null ? string.Join(",", leadStatusIds) : null,
                                    installerPersonIds =
                                        installerPersonIds != null ? string.Join(",", installerPersonIds) : null,
                                    salesPersonIds =
                                        salesPersonIds != null ? string.Join(",", salesPersonIds) : null,
                                    jobStatusIds = jobStatusIds != null ? string.Join(",", jobStatusIds) : null,
                                    invoiceStatuses =
                                        invoiceStatuses != null
                                            ? string.Join(",", invoiceStatuses)
                                            : null,
                                    sourceIds =
                                        sourceIds != null ? string.Join(",", sourceIds) : null,
                                    createdOnUtcStart =
                                        createdOnStart.HasValue ? (DateTime?)createdOnStart.Value.Date : null,
                                    createdOnUtcEnd =
                                        createdOnEnd.HasValue ? (DateTime?)createdOnEnd.Value.Date.AddDays(1) : null
                                });
                    }
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
            }

            return dto;
        }

        public List<SaveSearchDTO> GetSavedSearch()
        {
            return
                CRMDBContext.SavedSearches.Where(v => v.UserId == AuthorizingUser.UserId)
                    .Select(
                        s =>
                            new SaveSearchDTO()
                            {
                                Name = s.Name,
                                SavedSearchID = s.SavedSearchID,
                                SearchParams = s.SearchParams
                            })
                    .ToList();
        }

        public bool DeleteSavedSearch(int id)
        {
            var entity = CRMDBContext.SavedSearches.FirstOrDefault(v => v.SavedSearchID == id);
            if (entity == null)
            {
                return false;
            }

            CRMDBContext.SavedSearches.Remove(entity);
            CRMDBContext.SaveChanges();
            return true;
        }

        public bool SavedSearch(SaveSearchDTO model)
        {
            try
            {
                var existEntity =
                    CRMDBContext.SavedSearches.FirstOrDefault(v => v.UserId == AuthorizingUser.UserId && v.Name == model.Name);
                if (existEntity != null)
                {
                    existEntity.SearchParams = model.SearchParams;
                    CRMDBContext.SaveChanges();
                    return true;
                }

                if (CRMDBContext.SavedSearches.Count(v => v.UserId == AuthorizingUser.UserId) >= 10)
                {
                    return false;
                }

                var entity = new SavedSearch();
                entity.Name = model.Name;
                entity.SearchParams = model.SearchParams;
                entity.UserId = AuthorizingUser.UserId;
                CRMDBContext.SavedSearches.Add(entity);
                CRMDBContext.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}
