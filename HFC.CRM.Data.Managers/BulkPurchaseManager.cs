﻿using Dapper;
using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using HFC.CRM.DTO.Order;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using HFC.CRM.Core.Logs;
using Newtonsoft.Json;
using HFC.CRM.DTO;
using HFC.CRM.DTO.BulkPurchase;

namespace HFC.CRM.Managers
{
    public class BulkPurchaseManager : DataManagerFE<Orders>
    {
        //private EmailManager emailMgr;
        //private MeasurementManager MeasurementMgr;
        //private QuotesManager QuotesMgr;
        //private AccountsManager accmgr;

        private PermissionManager permissionManager;

        public BulkPurchaseManager(User user, Franchise franchise) : base(user, franchise)
        {
            permissionManager = new PermissionManager(this.User, this.Franchise);
        }

        public IList<OrderBulkPurchaseDTO> GetOrders()
        {
            var permissions = permissionManager.GetUserRolesPermissions();
            var mPermissions = permissions.Find(x => x.ModuleCode == "SalesOrder");
            var ConvertOrder = mPermissions.SpecialPermission.Find(x => x.PermissionCode == "ConvertToMpo").CanAccess;
            var ConvertBuyerRemorse = mPermissions.SpecialPermission.Find(x => x.PermissionCode == "BuyerRemorseConvert").CanAccess;
            var ConvertBuyerOverride = mPermissions.SpecialPermission.Find(x => x.PermissionCode == "BuyerRemorseOverride").CanAccess;

            if (!ConvertOrder) throw new Exception("Convert To Mpo permission is required");

            // Vasishta guided that we should allows and not throw exception.
            //if (!(ConvertBuyerRemorse || ConvertBuyerOverride))
            //    throw new Exception("Either Byer Remorese Convert or Buyer Remorse Override is required");

            var queryCondition = "";
            //if (ConvertBuyerRemorse)
            //{
            //    queryCondition = " and o.OrderStatus not in(6, 9)";
            //}
            //else if(ConvertBuyerOverride)
            //{
            //    queryCondition = " and o.OrderStatus = 7";
            //}
            //else
            //{
            //    queryCondition = " and o.OrderStatus not in(6, 8, 9)";
            //}
            if (ConvertBuyerRemorse)
            {
                queryCondition = " and ol.OrderLineStatus not in(6, 9)";
            }
            else if (ConvertBuyerOverride)
            {
                queryCondition = " and ol.OrderLineStatus = 7";
            }
            else
            {
                queryCondition = " and ol.OrderLineStatus not in(6, 8, 9)";
            }


            var result = new List<OrderBulkPurchaseDTO>();
            //var query = @"select o.OrderID, o.OrderNumber, o.OrderName 
            //            , o.ProductSubtotal TotalCost
            //            , o.OrderTotal TotalSales
            //            , (select sum(isnull(Quantity, 0)) from crm.QuoteLines where QuoteKey = o.QuoteKey) TotalQuantity 
            //                , o.QuoteKey
            //                from crm.Orders o
            //                left join crm.Opportunities opp on opp.OpportunityId = o.OpportunityId
            //                where not exists(
            //                select 1 from crm.MasterPurchaseOrder mpo
            //                left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
            //                left join crm.Opportunities opp on opp.OpportunityId = mpox.OpportunityId
            //                where opp.FranchiseId = @franchiseId
            //                and mpox.OrderId = o.OrderID
            //            )
            //            and opp.FranchiseId = @franchiseId";
            //query += queryCondition + ";" ;
            //query += @" select ql.QuoteLineId, ql.QuoteKey, ql.Quantity, ql.RoomName
            //        , ql.VendorId
            //        , [CRM].[fnGetVendorNameByVendorId](ql.VendorId, @franchiseId) VendorName 
            //        , ql.ProductName, ql.Width, ql.Height, ql.MountType
            //        , ql.Color, ql.Fabric, ql.ExtendedPrice, ql.UnitPrice Cost
            //        , q.QuoteID, isnull(ql.FranctionalValueWidth, '') FranctionalValueWidth
            //        , isnull(ql.FranctionalValueHeight, '') FranctionalValueHeight
            //        from crm.QuoteLines ql 
            //        left join crm.Quote q on q.QuoteKey = ql.QuoteKey
            //        where ql.QuoteKey in (
            //        select o.QuoteKey
            //        from crm.Orders o
            //        left join crm.Opportunities opp on opp.OpportunityId = o.OpportunityId
            //        where not exists(
            //         select 1 from crm.MasterPurchaseOrder mpo
            //         left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
            //         left join crm.Opportunities opp on opp.OpportunityId = mpox.OpportunityId
            //         where opp.FranchiseId = @franchiseId
            //         and mpox.OrderId = o.OrderID
            //        )
            //        and opp.FranchiseId = @franchiseId";
            //query += queryCondition + @")
            //        Order by q.QuoteKey";

            //string query = string.Format(@"select distinct ql.QuoteKey, ord.OrderID, ord.OrderName, ord.OrderNumber, ord.CreatedOn
            //                from crm.QuoteLines ql 
            //                --left join crm.Quote q on q.QuoteKey = ql.QuoteKey
            //                left join acct.FranchiseVendors fv on fv.VendorId = ql.VendorId and fv.FranchiseId = @franchiseid
            //                left join crm.Orders ord on ord.QuoteKey = ql.QuoteKey --q.QuoteKey
            //                where ql.QuoteKey in (
            //                 select o.QuoteKey
            //                 from crm.Orders o
            //                 left join crm.Opportunities opp on opp.OpportunityId = o.OpportunityId
            //                 where not exists(
            //                   select 1 from crm.MasterPurchaseOrder mpo
            //                   left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
            //                   left join crm.Opportunities opp on opp.OpportunityId = mpox.OpportunityId
            //                   where opp.FranchiseId = @franchiseId
            //                   and mpox.OrderId = o.OrderID and mpo.POStatusId <> 8
            //                  )
            //                 and opp.FranchiseId = @franchiseId
            //                {0}) and isnull(fv.BulkPurchase, 0) = 1 and ql.ProductTypeId in (1,2)
            //                Order by ord.CreatedOn desc; 

            //                select ql.QuoteLineId, ql.QuoteKey, ql.Quantity, ql.RoomName
            //                , ql.VendorId
            //                , [CRM].[fnGetVendorNameByVendorId](ql.VendorId, @franchiseId) VendorName 
            //                , ql.ProductName, ql.Width, ql.Height, ql.MountType
            //                , ql.Color, ql.Fabric, ql.ExtendedPrice--, ql.UnitPrice Cost
            //                , q.QuoteID, isnull(ql.FranctionalValueWidth, '') FranctionalValueWidth
            //                , isnull(ql.FranctionalValueHeight, '') FranctionalValueHeight
            //                , ql.QuoteLineNumber , ord.OrderID, ord.OrderName, ord.OrderNumber, ord.CreatedOn
            //                , (qld.BaseCost * qld.Quantity) Cost, (qld.NetCharge) Sales
            //                from crm.QuoteLines ql 
            //                left join crm.Quote q on q.QuoteKey = ql.QuoteKey
            //                left join acct.FranchiseVendors fv on fv.VendorId = ql.VendorId and fv.FranchiseId = @franchiseId
            //                left join crm.Orders ord on ord.QuoteKey = q.QuoteKey
            //                left join crm.QuoteLineDetail qld on qld.QuoteLineId = ql.QuoteLineId
            //                where ql.ProductTypeId in (1,2) and ql.QuoteKey in (
            //                 select o.QuoteKey
            //                 from crm.Orders o
            //                 left join crm.Opportunities opp on opp.OpportunityId = o.OpportunityId
            //                 where not exists(
            //                   select 1 from crm.MasterPurchaseOrder mpo
            //                   left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
            //                   left join crm.Opportunities opp on opp.OpportunityId = mpox.OpportunityId
            //                   where opp.FranchiseId = @franchiseId
            //                   and mpox.OrderId = o.OrderID and mpo.POStatusId <> 8
            //                  )
            //                 and opp.FranchiseId = @franchiseId
            //                {0}) and isnull(fv.BulkPurchase, 0) = 1
            //                order by q.QuoteKey, ql.QuoteLineNumber;
            //                ", queryCondition);

            var query = @"select distinct ord.QuoteKey, ord.OrderID, ord.OrderName, ord.OrderNumber, ord.CreatedOn 
	                        from crm.QuoteLines ql 
	                        left join crm.Quote q on q.QuoteKey = ql.QuoteKey
	                        left join acct.FranchiseVendors fv on fv.VendorId = ql.VendorId 
		                        and fv.FranchiseId = @franchiseId
	                        left join crm.Orders ord on ord.QuoteKey = q.QuoteKey
	                        --left join crm.QuoteLineDetail qld on qld.QuoteLineId = ql.QuoteLineId
	                        left join crm.OrderLines ol on ol.QuoteLineId = ql.QuoteLineId
	                        left join crm.Opportunities opp on opp.OpportunityId = q.OpportunityId 
	                        where ql.ProductTypeId in (1,2) 
	                        and isnull(fv.BulkPurchase, 0) = 1
	                        and ol.orderlineStatus in (7,8)
	                        and ord.OrderStatus  IN (7)
	                        and opp.FranchiseId = @franchiseId
                        order by ord.CreatedOn desc

                        select ql.QuoteLineId, ql.QuoteKey, ql.Quantity, ql.RoomName
                        , ql.VendorId
                        , [CRM].[fnGetVendorNameByVendorId](ql.VendorId, 2) VendorName 
                        , ql.ProductName, ql.Width, ql.Height, ql.MountType
                        , ql.Color, ql.Fabric, ql.ExtendedPrice--, ql.UnitPrice Cost
                        , q.QuoteID, isnull(ql.FranctionalValueWidth, '') FranctionalValueWidth
                        , isnull(ql.FranctionalValueHeight, '') FranctionalValueHeight
                        , ql.QuoteLineNumber , ord.OrderID, ord.OrderName, ord.OrderNumber, ord.CreatedOn
                        , (qld.BaseCost * qld.Quantity) Cost, (qld.NetCharge) Sales
                        from crm.QuoteLines ql 
                        left join crm.Quote q on q.QuoteKey = ql.QuoteKey
                        left join acct.FranchiseVendors fv on fv.VendorId = ql.VendorId 
	                        and fv.FranchiseId = @franchiseId
                        left join crm.Orders ord on ord.QuoteKey = q.QuoteKey
                        left join crm.QuoteLineDetail qld on qld.QuoteLineId = ql.QuoteLineId
                        left join crm.OrderLines ol on ol.QuoteLineId = ql.QuoteLineId
                        left join crm.Opportunities opp on opp.OpportunityId = q.OpportunityId 
                        where ql.ProductTypeId in (1,2) 
                        and isnull(fv.BulkPurchase, 0) = 1
                        and ol.orderlineStatus in(7,8)
                        and ord.OrderStatus  IN (7)
                        and opp.FranchiseId = @franchiseId";

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                var tempResult = connection.QueryMultiple(query
                    , new { franchiseId = this.Franchise.FranchiseId });

                result = tempResult.Read<OrderBulkPurchaseDTO>().ToList();
                //var details = tempResult.Read<QuoteLines>().ToList();
                var details = tempResult.Read<QuoteLinesBulkPurchaseDTO>().ToList();

                foreach (var item in result)
                {
                    var temp = details.Where(x => x.QuoteKey == item.QuoteKey).ToList();
                    item.QuoteLines = temp;
                    item.TotalCost = temp.Sum(x => x.Cost);
                    item.TotalSales = temp.Sum(x => x.Sales);
                    item.TotalQuantity = temp.Sum(x => x.Quantity);
                }
                connection.Close();
            }

            var temp2 = result.Where(x => x.OrderNumber == 1100).FirstOrDefault();

            return result;
        }
    }
}