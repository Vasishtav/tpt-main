﻿using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using HFC.CRM.DTO.PIC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Managers
{
    public class PICHFCVendorManager : DataManager<HFCVendor>
    {
        public override string Add(HFCVendor data)
        {
            throw new NotImplementedException();
        }

        public override string Delete(int id)
        {
            throw new NotImplementedException();
        }

        public override HFCVendor Get(int id)
        {
            throw new NotImplementedException();
        }

        public override ICollection<HFCVendor> Get(List<int> idList)
        {
            throw new NotImplementedException();
        }

        public override string Update(HFCVendor data)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Insert or Update Vendor and vendor address
        /// </summary>        
        public bool ImportVendor(AllVendorRoot allVendorRoot)
        {
            try
            {
                foreach (var a in allVendorRoot.properties)
                {
                    string query = "select * from [Acct].[HFCVendors] where PICVendorId=@PICVendorId and VendorType=1";
                    var vendor = ExecuteIEnumerableObject<HFCVendor>(ContextFactory.CrmConnectionString, query, new { PICVendorId = a.vendor }).FirstOrDefault();
                    if (vendor != null)
                    {
                        Address ad = new Address();
                        ad.AddressId = vendor.AddressId != null ? Convert.ToInt32(vendor.AddressId) : 0;
                        ad.Address1 = a.address1;
                        ad.Address2 = a.address2;
                        ad.City = a.city;
                        ad.State = a.state;
                        ad.ZipCode = a.zip;
                        ad.CountryCode2Digits = a.country;

                        int AddressId = SaveAddress(ad);
                        if (vendor.AddressId == null || vendor.AddressId != AddressId)
                        {
                            vendor.AddressId = AddressId;
                            vendor.AccountRep = a.contact;
                            vendor.AccountRepPhone = a.phone;

                            if (vendor.IsActive == null)
                                vendor.IsActive = true;

                            Update<HFCVendor>(ContextFactory.CrmConnectionString, vendor);
                        }
                    }
                    else
                    {
                        HFCVendor hv = new HFCVendor();
                        Address ad = new Address();
                        ad.AddressId = 0;
                        ad.Address1 = a.address1;
                        ad.Address2 = a.address2;
                        ad.City = a.city;
                        ad.State = a.state;
                        ad.ZipCode = a.zip;
                        ad.CountryCode2Digits = a.country;

                        int AddressId = SaveAddress(ad);

                        hv.VendorId = ExecuteIEnumerableObject<int>(ContextFactory.CrmConnectionString, "exec [CRM].[spVendor_New]").FirstOrDefault();
                        hv.Name = a.name;
                        hv.PICVendorId = a.vendor;
                        hv.AddressId = AddressId;
                        hv.AccountRep = a.contact;
                        hv.AccountRepPhone = a.phone;
                        hv.VendorType = 1;
                        hv.IsActive = true;
                        Insert<HFCVendor>(ContextFactory.CrmConnectionString, hv);
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Common method for Address insert/update
        /// </summary>
        public int SaveAddress(Address a)
        {
            try
            {
                if (a.AddressId == 0)
                {
                    return Insert<int, Address>(ContextFactory.CrmConnectionString, a);
                }
                else
                {
                    var Ad = GetData<Address>(ContextFactory.CrmConnectionString, a.AddressId);

                    if (Ad == null)
                    {
                        a.AddressId = 0;
                        return Insert<int, Address>(ContextFactory.CrmConnectionString, a);
                    }
                    else if (Ad != null && Ad.Address1 == a.Address1 && Ad.Address2 == a.Address2 && Ad.City == a.City && Ad.State == a.State && Ad.ZipCode == a.ZipCode && Ad.CountryCode2Digits == a.CountryCode2Digits)
                    {
                        return a.AddressId;
                    }
                    else
                    {
                        a.CreatedBy = Ad.CreatedBy;
                        a.CreatedOn = Ad.CreatedOn;
                        Update<Address>(ContextFactory.CrmConnectionString, a);
                        return a.AddressId;
                    }
                }
            }
            catch (Exception ex)
            {
                return a.AddressId;
            }
        }


        /// <summary>
        /// Get List of Allience vendor
        /// </summary>
        public List<HFCVendor> GetListVendor()
        {
            return GetListData<HFCVendor>(ContextFactory.CrmConnectionString, "where VendorType=1 and IsActive=1 and IsAlliance=1 and PICVendorId is not null");
        }


        /// <summary>
        /// Update Particular vendor information
        /// </summary>
        public bool UpdateVendorData(PICHFCVendorData p)
        {
            try
            {
                p.h.AccountRep = p.v.properties.Vendor.contact;
                p.h.AccountRepPhone = p.v.properties.Vendor.phone;
                p.h.AccountRepEmail = p.v.properties.Vendor.email;
                if (p.v.properties.Vendor.suspended == "N")
                {
                    p.h.IsActive = true;
                }
                else
                {
                    p.h.IsActive = false;
                }

                Update<HFCVendor>(ContextFactory.CrmConnectionString, p.h);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}
