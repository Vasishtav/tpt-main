﻿using Dapper;
using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Core.Membership;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using HFC.CRM.Data.Geolocator;
using HFC.CRM.Managers.Exceptions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Web;
using System.Xml.Linq;

namespace HFC.CRM.Managers
{
    public class AccountsManager : DataManager<AccountTP>
    {
        private LeadsManager LeadsMgr = new LeadsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private NoteManager NoteMgr = new NoteManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        public AccountsManager(User user, Franchise franchise) : this(user, user, franchise)
        {
            this.User = user;
            this.Franchise = franchise;
        }
        FranchiseManager _franMgr = new FranchiseManager(SessionManager.CurrentUser);
        StatusChangesManager statusChangesMgr = new StatusChangesManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
        public string UpdateAccount(int accountId, int accountStatusId, int dispositionId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {

                connection.Open();
                var account = connection.Query<AccountTP>("select * from CRM.Accounts where AccountId = @accountId", new { accountId = accountId }).FirstOrDefault();
                account.AccountStatusId = Convert.ToInt16(accountStatusId);
                account.DispositionId = Convert.ToInt16(dispositionId);
                connection.Update(account);
                connection.Close();
                return Success;
            }

        }


        public AccountsManager(User user, User authorizingUser, Franchise franchise)
        {
            base.User = user;
            base.AuthorizingUser = authorizingUser;
            base.Franchise = franchise;
        }

        public override string Add(AccountTP data)
        {
            throw new NotImplementedException();
        }

        public string MergeLeadToAccount(int brandId, int LeadId, int AccountId, string newAccountStatus)
        {
            int accountId = 0;
            Lead leadmodel = this.LeadsMgr.Get(LeadId);
            AccountTP accountmodel = new AccountTP();

            // copy lead model to account model
            accountmodel = this.CopyLeadModelToAccount(leadmodel);
            if (accountmodel.SourcesTPIdFromCampaign > 0)
            {
                accountmodel.SourcesTPId = accountmodel.SourcesTPIdFromCampaign;
            }
            accountmodel.AccountStatusId = 1;//New Account after converting from lead
            //if(AccountId>0)
            //    accountmodel.AccountTypeId = 2;
            //this.CreateAccount(out accountId, accountmodel, LeadId);
            // Update the account
            this.UpdateAccountFromConverLead(accountmodel, leadmodel, LeadId, AccountId);
            using (SqlConnection connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                connection.Query("update crm.Leads set LeadStatusId = 31422 where LeadId=@LeadId", new { LeadId = LeadId });
                connection.Close();
            }

            return "Success";
        }
        public int ConvertLeadToAccount(out int accountId, int brandId, int LeadId, string newAccountStatus)
        {
            try
            {
                accountId = 0;
                Lead leadmodel = this.LeadsMgr.Get(LeadId);
                AccountTP accountmodel = new AccountTP();

                // Copy lead data in to account data
                accountmodel = this.CopyLeadModelToAccount(leadmodel);
                if (accountmodel.SourcesTPIdFromCampaign > 0)
                    accountmodel.SourcesTPId = accountmodel.SourcesTPIdFromCampaign;
                accountmodel.AccountStatusId = 1;
                accountmodel.AccountTypeId = 10004;

                // Call  CreateAccount method to create new account
                accountId = this.CreateAccountFromLead(out accountId, accountmodel, LeadId);

                return accountId;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public AccountTP CopyLeadModelToAccount(Lead LeadModel)
        {
            AccountTP accountTPmodel = new AccountTP()
            {
                //PrimCustomer = LeadModel.PrimCustomer,
                //Addresses = LeadModel.Addresses,
                CampaignId = LeadModel.CampaignId,
                CampaignName = LeadModel.CampaignName,
                AccountSourceId = LeadModel.leadSourceId,
                //AccountSources = this.GetAccountSources(LeadModel.LeadSources),
                SourcesList = LeadModel.SourcesList,
                SourcesTPId = LeadModel.SourcesTPId,
                SourcesTPIdFromCampaign = LeadModel.SourcesTPIdFromCampaign,
                //AccountNotes = LeadModel.LeadNotes,
                LeadType = LeadModel.LeadType,
                AllowFranchisetoReassign = LeadModel.AllowFranchisetoReassign,
                ReferralTypeId = LeadModel.ReferralTypeId,
                RelatedAccountId = LeadModel.RelatedAccountId,
                RelatedAccountName = LeadModel.RelatedAccountName,
                AccountName = LeadModel.AccountName,
                TerritoryId = LeadModel.TerritoryId,
                TerritoryDisplayId = LeadModel.TerritoryDisplayId,
                TerritoryType = LeadModel.TerritoryType,
                KeyAcct = LeadModel.KeyAcct,
                LegacyAccount = LeadModel.LegacyAccount,
                IsCommercial = LeadModel.IsCommercial,
                CommercialTypeId = LeadModel.CommercialTypeId,
                RelationshipTypeId = LeadModel.RelationshipTypeId,
                IndustryId = LeadModel.IndustryId,
                OtherIndustry = LeadModel.OtherIndustry,
                IsTaxExempt = LeadModel.IsTaxExempt,
                TaxExemptID = LeadModel.TaxExemptID,
                HFCUTMCampaignCode = LeadModel.HFCUTMCampaignCode,
                LMDBSourceID = LeadModel.LMDBSourceID,
                SourcesHFCId = LeadModel.SourcesHFCId,
                IsNotifyemails = LeadModel.IsNotifyemails,
                IsNotifyText = LeadModel.IsNotifyText
            };

            //Set Questions Answers
            if (LeadModel.LeadQuestionAns == null)
            {

                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    string Query = "SELECT ";
                    Query = Query + "TYQ.QuestionId";
                    Query = Query + ", TYQ.Question";
                    Query = Query + ", TYQ.SubQuestion";
                    Query = Query + ", TYQ.CreatedOnUtc";
                    Query = Query + ", TYQ.SelectionType";
                    Query = Query + ", TYQ.DisplayOrder";
                    Query = Query + ", TYQ.FranchiseId";
                    Query = Query + ", TYQ.BrandId";
                    Query = Query + ", ISNULL((CASE WHEN QA.Answer = 'True' THEN 'true' WHEN QA.Answer = 'False' THEN 'false' ELSE QA.Answer END), '') AS 'Answer'";
                    Query = Query + ", ISNULL(QA.AnswerId, '') AS 'AnswerId'";
                    Query = Query + ", ISNULL((CASE WHEN TYQ.SelectionType = 'date' THEN (CASE WHEN QA.Answer != '' THEN CONVERT(VARCHAR, CONVERT(DATE,QA.Answer), 101) ELSE QA.Answer END) ELSE '' END),'') AS 'Answers'";
                    Query = Query + ", (CASE SelectionType WHEN 'date' THEN (CASE WHEN QA.Answer<>'' THEN (CASE WHEN CONVERT(VARCHAR,CONVERT(datetime,QA.Answer),111)<CONVERT(VARCHAR,GETDATE(),111) THEN 'true' ELSE 'false' END) ELSE 'false' END)  ELSE 'false' END) AS 'PastDate'";
                    Query = Query + " FROM [CRM].[Type_Questions] TYQ ";
                    Query = Query + "LEFT OUTER JOIN ";
                    Query = Query + "(SELECT QuestionId, Answer, AnswerId FROM [CRM].[QuestionAnswers] WHERE LeadId = @LeadId) ";
                    Query = Query + "AS QA on TYQ.QuestionId = QA.QuestionId ";
                    Query = Query + "WHERE LeadQuestion = 1 AND BrandId = @BrandId";
                    var lstQa = connection.Query<Type_Question>(Query,
                     new
                     {
                         BrandId = SessionManager.BrandId,
                         LeadId = LeadModel.LeadId
                     }).OrderBy(ob => ob.DisplayOrder).ToList();
                    connection.Close();
                    if (lstQa != null)
                    {
                        accountTPmodel.AccountQuestionAns = lstQa;
                    }
                }

            }
            return accountTPmodel;
        }

        public int CreateAccount(out int accountId, AccountTP data, int LeadId)
        {
            DateTime? unsubscribedOn;
            DateTime value;
            bool kind;
            bool kind1;
            accountId = 0;
            if (data.AccountSources != null)
            {
                foreach (AccountSource source in data.AccountSources)
                {
                    if (source.SourceId == data.SourcesTPId)
                    {
                        return accountId;
                    }
                }
            }

            if (data != null)
            {
                //int franchiseId = Franchise.FranchiseId;
                data.FranchiseId = Franchise.FranchiseId;

                if (data.AccountStatusId <= 0)
                {
                    data.AccountStatusId = AppConfigManager.NewAccountStatusId;
                }

                if ((data.AccountNotes == null ? false : data.AccountNotes.Any<Note>()))
                {
                    if (!string.IsNullOrEmpty(data.AccountNotes.First<Note>().Notes))
                    {
                        data.AccountNotes.First<Note>().CreatedOn = base.GetNow();
                        data.AccountNotes.First<Note>().UpdatedBy = new int?(base.User.PersonId);
                    }
                    else
                    {
                        //  data.AccountNotes.Clear();
                    }
                }
                bool validEmail = true;
                bool validZip = true;
                if (data.PrimCustomer != null)
                {
                    data.PrimCustomer.HomePhone = RegexUtil.GetNumbersOnly(data.PrimCustomer.HomePhone);
                    data.PrimCustomer.WorkPhone = RegexUtil.GetNumbersOnly(data.PrimCustomer.WorkPhone);
                    data.PrimCustomer.CellPhone = RegexUtil.GetNumbersOnly(data.PrimCustomer.CellPhone);
                    data.PrimCustomer.FaxPhone = RegexUtil.GetNumbersOnly(data.PrimCustomer.FaxPhone);
                    if ((!validEmail ? false : !string.IsNullOrEmpty(data.PrimCustomer.PrimaryEmail)))
                    {
                        validEmail = RegexUtil.IsValidEmail(data.PrimCustomer.PrimaryEmail);
                    }
                    if ((!validEmail ? false : !string.IsNullOrEmpty(data.PrimCustomer.SecondaryEmail)))
                    {
                        validEmail = RegexUtil.IsValidEmail(data.PrimCustomer.SecondaryEmail);
                    }
                    if (!data.PrimCustomer.UnsubscribedOn.HasValue)
                    {
                        kind1 = false;
                    }
                    else
                    {
                        unsubscribedOn = data.PrimCustomer.UnsubscribedOn;
                        value = unsubscribedOn.Value;
                        kind1 = value.Kind == DateTimeKind.Unspecified;
                    }
                    if (kind1)
                    {
                        CustomerTP primCustomer = data.PrimCustomer;
                        unsubscribedOn = data.PrimCustomer.UnsubscribedOn;
                        value = unsubscribedOn.Value;
                        primCustomer.UnsubscribedOn = new DateTime?(value.ToUniversalTime());
                    }
                }
                if (data.SecCustomer != null)
                {
                    foreach (CustomerTP person in data.SecCustomer)
                    {
                        person.HomePhone = RegexUtil.GetNumbersOnly(person.HomePhone);
                        person.WorkPhone = RegexUtil.GetNumbersOnly(person.WorkPhone);
                        person.CellPhone = RegexUtil.GetNumbersOnly(person.CellPhone);
                        person.FaxPhone = RegexUtil.GetNumbersOnly(person.FaxPhone);
                        if ((!validEmail ? false : !string.IsNullOrEmpty(person.PrimaryEmail)))
                        {
                            validEmail = RegexUtil.IsValidEmail(person.PrimaryEmail);
                        }
                        if ((!validEmail ? false : !string.IsNullOrEmpty(person.SecondaryEmail)))
                        {
                            validEmail = RegexUtil.IsValidEmail(person.SecondaryEmail);
                        }
                        if (!person.UnsubscribedOn.HasValue)
                        {
                            kind = false;
                        }
                        else
                        {
                            value = person.UnsubscribedOn.Value;
                            kind = value.Kind == DateTimeKind.Unspecified;
                        }
                        if (kind)
                        {
                            value = person.UnsubscribedOn.Value;
                            person.UnsubscribedOn = new DateTime?(value.ToUniversalTime());
                        }
                    }
                }
                if ((data.Addresses == null ? false : data.Addresses.Count > 0))
                {
                    foreach (AddressTP addr in data.Addresses)
                    {
                        if (!string.IsNullOrEmpty(addr.ZipCode))
                        {
                            addr.ZipCode = addr.ZipCode.ToUpper();
                        }
                        if (string.IsNullOrEmpty(addr.CountryCode2Digits))
                        {
                            addr.CountryCode2Digits = base.Franchise.CountryCode;
                        }
                        if ((!validZip ? false : !string.IsNullOrEmpty(addr.ZipCode)))
                        {
                            validZip = RegexUtil.IsValidZipOrPostal(addr.ZipCode);
                        }
                    }
                }
                if ((!validEmail ? false : validZip))
                {

                    int? accountnumber = this.spAccountNumber_New(new int?(base.Franchise.FranchiseId));
                    if ((!accountnumber.HasValue ? true : accountnumber.Value <= 0))
                    {
                        throw new Exception("Unable to generate a account number");
                    }
                    else
                    {
                        data.AccountGuid = Guid.NewGuid();
                        data.CreatedOnUtc = DateTime.UtcNow;
                        data.CreatedByPersonId = User.PersonId;
                        data.AccountNumber = accountnumber.Value;
                        //string territoryCode = "";
                        string[] TerritoryDetails = new string[2];
                        if (data.Addresses != null)
                        {
                            TerritoryDetails = TerritoryManager.GetTerritoryDetails(data.Addresses.FirstOrDefault().ZipCode, data.Addresses.FirstOrDefault().CountryCode2Digits).Split(';');
                            //territoryCode = this.MatchTerritory(data.Addresses.FirstOrDefault<AddressTP>().ZipCode);
                        }
                        using (SqlConnection connection = new SqlConnection(ContextFactory.CrmConnectionString))
                        {
                            connection.Open();
                            var transaction = connection.BeginTransaction();
                            try
                            {
                                if (TerritoryDetails != null && TerritoryDetails[0] != null)
                                {
                                    if (TerritoryDetails[0] == "0")
                                    {
                                        data.TerritoryType = TerritoryDetails[1];
                                        data.TerritoryId = null;
                                    }
                                    else
                                    {
                                        data.TerritoryType = TerritoryDetails[1];
                                        data.TerritoryId = Convert.ToInt32(TerritoryDetails[0]);
                                    }
                                }

                                if (data.Addresses != null)
                                {
                                    foreach (AddressTP item in data.Addresses)
                                    {
                                        int? addressId = connection.Insert<AddressTP>(item, transaction);
                                        item.AddressId = addressId.Value;
                                    }
                                }
                                if (data.PrimCustomer != null)
                                {
                                    data.PrimCustomer.CreatedOn = DateTime.UtcNow;
                                    data.PrimCustomer.CreatedBy = User.PersonId;
                                    data.CustomerId = connection.Insert<CustomerTP>(data.PrimCustomer, transaction);
                                    data.PersonId = data.CustomerId;
                                }

                                //if (data.IsTaxExempt != true)
                                //{
                                //    data.IsTaxExempt = false;
                                //    data.TaxExemptID = null;
                                //}

                                //if (!(data.IsTaxExempt || data.IsPSTExempt
                                //    || data.IsGSTExempt || data.IsHSTExempt
                                //    || data.IsVATExempt))
                                //{
                                //    data.TaxExemptID = null;
                                //}

                                HandleTaxExemptId(data);


                                data.IndustryId = data.IndustryId > 0 ? data.IndustryId : (int?)null;
                                data.CommercialTypeId = data.CommercialTypeId > 0 ? data.CommercialTypeId : (int?)null;
                                data.RelationshipTypeId = data.RelationshipTypeId > 0 ? data.RelationshipTypeId : (int?)null;
                                data.LeadId = LeadId;

                                data.ImpersonatorPersonId = this.ImpersonatorPersonId;

                                if (data.TerritoryType.ToUpper().Contains("GRAY AREA"))
                                {
                                    data.TerritoryId = null;
                                }

                                accountId = (int)connection.Insert<AccountTP>(data, transaction);

                                int answerId = 0;
                                if (data.LeadId != 0 && data.AccountId == 0)
                                {
                                    if (data.AccountQuestionAns == null)
                                    {
                                        data.AccountQuestionAns = LeadsMgr.Get(data.LeadId).LeadQuestionAns;
                                    }
                                    if (data.AccountQuestionAns != null)
                                    {
                                        TypeQuestionManager QansMgr = new TypeQuestionManager(SessionManager.CurrentUser.PersonId, SessionManager.CurrentFranchise.FranchiseId);
                                        var status = QansMgr.SaveAccountQuestionAnswers(accountId, data.AccountQuestionAns, out answerId);
                                    }
                                }

                                if (data.CustomerId.HasValue)
                                {
                                    connection.Query("insert into crm.accountCustomers values(@accountid, @customerId,@isPrimaryCustomer)", new { accountid = accountId, customerId = data.CustomerId, isPrimaryCustomer = true }, transaction);
                                }
                                if (data.AccountSources != null)
                                {
                                    foreach (AccountSource item in data.AccountSources)
                                    {
                                        connection.Query("insert into crm.AccountSources (SourceId,accountId,CreatedOnUtc,IsManuallyAdded,IsPrimarySource) values(@sourceId,@accountId,@createdonutc,@isManuallyAdded,@isPrimarySource)", new { sourceId = item.SourceId, accountId = accountId, createdonutc = DateTime.UtcNow, isManuallyAdded = true, isPrimarySource = false }, transaction);
                                    }
                                }
                                if (data.SourcesTPId > 0)
                                {
                                    connection.Query("insert into crm.AccountSources (SourceId,accountId,CreatedOnUtc,IsManuallyAdded,IsPrimarySource) values(@sourceId,@accountId,@createdonutc,@isManuallyAdded,@isPrimarySource)", new { sourceId = data.SourcesTPId, accountId = accountId, createdonutc = DateTime.UtcNow, isManuallyAdded = true, isPrimarySource = true }, transaction);
                                }
                                List<int> secCustomerIDlst = new List<int>();
                                if (data.SecCustomer != null)
                                {
                                    foreach (CustomerTP item in data.SecCustomer)
                                    {
                                        item.CreatedOn = DateTime.UtcNow;
                                        int CustomerId = (int)connection.Insert<CustomerTP>(item, transaction);
                                        secCustomerIDlst.Add(CustomerId);
                                    }
                                    foreach (int id in secCustomerIDlst)
                                    {
                                        connection.Query("insert into crm.accountCustomers values(@accountid, @customerId,@isPrimaryCustomer)", new { accountid = accountId, customerId = id, isPrimaryCustomer = false }, transaction);
                                    }
                                }
                                if (data.AccountNotes != null)
                                {
                                    NoteManager noteMgr = new NoteManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                                    foreach (Note accountNote in data.AccountNotes)
                                    {
                                        var model = accountNote;
                                        model.LeadId = null;
                                        model.AccountId = accountId;
                                        model.OpportunityId = null;
                                        model.OrderId = null;
                                        model.IsNote = true;
                                        model.CreatedOn = DateTime.UtcNow;
                                        model.CreatedBy = SessionManager.CurrentUser.PersonId;
                                        var status = noteMgr.Add(model);
                                    }
                                }
                                if (data.Addresses != null)
                                {
                                    foreach (AddressTP item in data.Addresses)
                                    {
                                        connection.Query("insert into crm.accountAddresses values(@accountid, @addressid, @IsPrimaryAddress)", new { accountid = accountId, addressid = item.AddressId, IsPrimaryAddress = 1 }, transaction);
                                    }
                                }

                                // Not required as it was handled by notes and attachments components
                                // this.SaveNoteAttachment(accountId, data, connection);
                                transaction.Commit();
                                connection.Close();

                                // Audit trial
                                EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(data), AccountId: accountId);
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();
                                EventLogger.LogEvent(ex);
                                throw;
                            }

                        }
                    }
                }
                else
                {
                    //str = (validEmail ? string.Format("Invalid {0}", base.Franchise.ZipText) : "Invalid email address");
                }
            }
            else
            {
                //str = "Action cancelled, data can not be empty";
            }
            return accountId;
        }

        private void HandleTaxExemptId(AccountTP data)
        {
            if (!(data.IsTaxExempt || data.IsPSTExempt
                                    || data.IsGSTExempt || data.IsHSTExempt
                                    || data.IsVATExempt))
            {
                data.TaxExemptID = null;
            }
        }

        public int CreateAccountFromLead(out int accountId, AccountTP data, int LeadId)
        {
            accountId = 0;
            if (data == null)
                return accountId;

            int? accountnumber = this.spAccountNumber_New(SessionManager.CurrentFranchise.FranchiseId);
            if ((!accountnumber.HasValue ? true : accountnumber.Value <= 0))
                throw new Exception("Unable to generate a account number");

            using (SqlConnection connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    data.FranchiseId = data.FranchiseId != null && data.FranchiseId != 0 ? (int)data.FranchiseId : Franchise.FranchiseId;
                    data.CreatedByPersonId = base.User.PersonId;
                    data.CreatedOnUtc = DateTime.UtcNow;
                    data.LeadId = LeadId;
                    data.AccountGuid = Guid.NewGuid();
                    data.AccountNumber = (int)accountnumber;
                    data.ImpersonatorPersonId = this.ImpersonatorPersonId;

                    if (data.AccountStatusId <= 0)
                        data.AccountStatusId = AppConfigManager.NewAccountStatusId;

                    /// Create New Account
                    accountId = (int)connection.Insert<AccountTP>(data, transaction);

                    // Account Sources
                    if (data.SourcesTPId > 0)
                        connection.Query("insert into crm.AccountSources (SourceId,accountId,CreatedOnUtc,IsManuallyAdded,IsPrimarySource) values(@sourceId,@accountId,@createdonutc,@isManuallyAdded,@isPrimarySource)", new { sourceId = data.SourcesTPId, accountId = accountId, createdonutc = DateTime.UtcNow, isManuallyAdded = true, isPrimarySource = true }, transaction);

                    string copydataQ = @"
                    insert into CRM.AccountAddresses SELECT @AccountId,AddressId,IsPrimaryAddress FROM CRM.LeadAddresses where LeadId=@LeadId
                    insert into CRM.AccountCustomers select @AccountId,CustomerId,IsPrimaryCustomer FROM CRM.LeadCustomers where LeadId=@LeadId
                    update CRM.Note set AccountId=@AccountId where LeadId=@LeadId
                    update CRM.Calendar set AccountId=@AccountId where LeadId=@LeadId
                    update CRM.Tasks set AccountId=@AccountId where LeadId=@LeadId
                    update History.SentEmails set AccountId=@AccountId where LeadId=@LeadId
                   
                    UPDATE a SET a.PersonId = ac.CustomerId FROM CRM.Accounts a    
                     INNER JOIN CRM.AccountCustomers ac ON a.AccountId = ac.AccountId 
                     WHERE a.AccountId = @AccountId and ac.IsPrimaryCustomer=1
                    update crm.Leads set LeadStatusId = 31422 where LeadId=@LeadId";
                    //reoved texting update code from above query as we moved to both texting & email to  same table(History.SentEmails).
                    //update History.Texting set AccountId = @AccountId where LeadId = @LeadId

                    connection.Query(copydataQ, new { LeadId = LeadId, AccountId = accountId }, transaction);
                    transaction.Commit();
                    connection.Close();

                    // Audit trial
                    EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(data), AccountId: accountId);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    EventLogger.LogEvent(ex);
                    throw;
                }

            }

            return accountId;
        }

        public static void CreateMessage(XElement child, ref StringBuilder message, XAttribute operation = null)
        {
            bool flag;
            bool flag1;
            XAttribute childOperation = null;
            if (operation == null)
            {
                childOperation = child.Attribute("operation");
            }
            XElement current = child.Element("Current");
            XElement original = child.Element("Original");
            XAttribute nameAtr = child.Attribute("name");
            string name = (nameAtr != null ? nameAtr.Value : child.Name.LocalName);
            if (childOperation == null || !childOperation.Value.Equals("insert", StringComparison.OrdinalIgnoreCase))
            {
                flag = (operation == null ? false : operation.Value.Equals("insert", StringComparison.OrdinalIgnoreCase));
            }
            else
            {
                flag = true;
            }
            if (!flag)
            {
                if (childOperation == null || !childOperation.Value.Equals("delete", StringComparison.OrdinalIgnoreCase))
                {
                    flag1 = (operation == null ? false : operation.Value.Equals("delete", StringComparison.OrdinalIgnoreCase));
                }
                else
                {
                    flag1 = true;
                }
                if (flag1)
                {
                    if (original == null)
                    {
                        message.AppendFormat("{0} Deleted", name);
                    }
                    else if (operation == null)
                    {
                        message.AppendFormat("{0} Deleted: {1}", name, original.Value);
                    }
                    else
                    {
                        message.AppendFormat("{0}: {1}", name, original.Value);
                    }
                }
                else if ((current == null ? false : original != null))
                {
                    message.AppendFormat("{0} changed from '{1}' to '{2}'", name, original.Value, current.Value);
                }
                else if (current == null)
                {
                    message.AppendFormat("{0} Changed", name);
                }
                else
                {
                    message.AppendFormat("{0} changed to {1}", name, current.Value);
                }
            }
            else if (current == null)
            {
                message.AppendFormat("{0} Added", name);
            }
            else if (operation == null)
            {
                message.AppendFormat("{0} Added: {1}", name, current.Value);
            }
            else
            {
                message.AppendFormat("{0}: {1}", name, current.Value);
            }
        }

        public override string Delete(int id)
        {
            throw new NotImplementedException();
        }

        public override AccountTP Get(int id)
        {
            AccountTP accountTP;
            if (id < 0)
            {
                throw new ArgumentNullException("Action cancelled, account does not exist");
            }
            accountTP = this.GetAccount(id);
            //if (accountTP.AccountTypeId == 1)
            //    accountTP.AccountTypeName = "Prospect";
            //else if(accountTP.AccountTypeId == 2)
            //    accountTP.AccountTypeName = "Key/Commercial";
            //else accountTP.AccountTypeName = "Customer";
            if (accountTP.CreatedOnUtc != null)
                accountTP.CreatedOnUtc = TimeZoneManager.ToLocal(accountTP.CreatedOnUtc);
            if (accountTP.LastUpdatedOnUtc != null)
                accountTP.LastUpdatedOnUtc = TimeZoneManager.ToLocal((DateTime)accountTP.LastUpdatedOnUtc);

            //using (SqlConnection connection = new SqlConnection(ContextFactory.CrmConnectionString))
            //{
            //    connection.Open();
            //    int? nullable = null;
            //    CommandType? nullable1 = null;
            //    AccountTP account = connection.Query<AccountTP>("select * from CRM.Accounts Where AccountId = @accountId", new { accountId = id }, null, true, nullable, nullable1).FirstOrDefault<AccountTP>();
            //    if (account == null)
            //    {
            //        throw new ArgumentNullException("Action cancelled, account does not exist");
            //    }
            //    nullable = null;
            //    nullable1 = null;
            //    Type_AccountStatus accountStatus = connection.Query<Type_AccountStatus>("select * from CRM.Type_AccountStatus where AccountStatusId =@statusId", new { statusId = account.AccountStatusId }, null, true, nullable, nullable1).FirstOrDefault<Type_AccountStatus>();
            //    if (accountStatus != null)
            //    {
            //        account.Type_AccountStatus = accountStatus;
            //    }
            //    account = this.GetAccount(account.AccountId);
            //    accountTP = account;
            //    //Added for Qualification/Question Answers
            //    int BrandId = (int)SessionManager.BrandId;
            //    List<Type_Question> accountQuestion = HFC.CRM.Managers.TypeQuestionManager.GetTypeQuestionsByAccountId(BrandId, account.AccountId);
            //    account.AccountQuestionAns = accountQuestion;

            //}
            return accountTP;
        }

        public override ICollection<AccountTP> Get(List<int> idList)
        {
            return new List<AccountTP>();
        }

        public AccountTP GetAccount(int id)
        {
            AccountTP accountTP;
            bool flag;
            if (id < 0)
            {
                throw new ArgumentNullException("Action cancelled, account does not exist");
            }
            using (SqlConnection connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                string query = @"select ac.*,te.Name as DisplayTerritoryName,lv.Name as AccountTypeName from crm.Accounts ac
                                 left join CRM.Territories te on ac.TerritoryDisplayId = te.TerritoryId 
                                 left join CRM.Type_LookUpValues lv on ac.AccountTypeId=lv.Id
                                 where accountId = @accountId";
                int? franchiseId = null;
                CommandType? nullable = null;
                AccountTP account = connection.Query<AccountTP>(query, new { accountId = id }, null, true, franchiseId, nullable).FirstOrDefault<AccountTP>();
                if (account == null)
                {
                    //flag = false;
                    throw new ArgumentNullException("Action cancelled, account does not exist");
                }
                else
                {
                    franchiseId = account.FranchiseId;
                    int num = base.Franchise != null ? base.Franchise.FranchiseId : 0;
                    flag = (franchiseId.GetValueOrDefault() == num ? !franchiseId.HasValue : true);
                }
                if (flag)
                {
                    // throw new AccessViolationException("Sorry, you do not have permission");
                }
                //Add Account Name And Referral Type If it is available
                if (account.RelatedAccountId != null)
                {
                    query = @"select (select [CRM].[fnGetAccountNameByAccountId](a.RelatedAccountId)) as AccountName
                          from  crm.Accounts a
                          join crm.AccountCustomers ac on ac.AccountId = a.RelatedAccountId and ac.IsPrimaryCustomer =1
                          join crm.Customer cus on cus.CustomerId = ac.CustomerId                             
                          where a.RelatedAccountId = @relatedAccountId";
                    var acccountName = connection.Query<string>(query, new { relatedAccountId = account.RelatedAccountId }).FirstOrDefault();
                    account.RelatedAccountName = acccountName;
                }

                query = @"select a.AccountId,c.CompanyName,c.FirstName+' '+c.LastName as Name,a.AccountTypeId,acs.Name as AccountStatus,a.CreatedOnUtc,
                            lv.Name as AccountType
                            from  crm.Accounts a
                            join CRM.AccountCustomers ac on ac.AccountId = a.AccountId
                            join CRM.Customer c on c.CustomerId = ac.CustomerId
                            join CRM.Type_AccountStatus acs on acs.AccountStatusId = a.AccountStatusId
                            join CRM.Type_LookUpValues lv on a.AccountTypeId=lv.Id
                            where a.RelatedAccountId = @accountid";
                account.ChildAccountList = connection.Query<AccountChild>(query, new { accountid = id }).ToList();
                if (account.ChildAccountList.Count() > 0)
                {
                    query = "select * from crm.Addresses where AddressId in (\r\n select AddressId from crm.AccountAddresses where AccountId = @accountId)";
                    foreach (var childAccount in account.ChildAccountList)
                    {
                        childAccount.Address = connection.Query<AddressTP>(query, new { accountId = childAccount.AccountId }).FirstOrDefault();
                    }
                    //foreach(var item in account.ChildAccountList)
                    //{
                    //    if (item.AccountTypeId == 1)
                    //    {
                    //        item.AccountType = "Prospect";
                    //    }
                    //    else if (item.AccountTypeId == 2)
                    //    {
                    //        item.AccountType = "Key/Commercial";
                    //    }
                    //    else if(item.AccountTypeId == 3)
                    //    {
                    //        item.AccountType = "Customer";
                    //    }
                    //}
                }
                query = @" select rt.Name from crm.Accounts a 
                           join crm.Type_Referral rt on rt.Id = a.ReferralTypeId
                           where a.ReferralTypeId =@referralId";
                var referralName = connection.Query<string>(query, new { referralId = account.ReferralTypeId }).FirstOrDefault();
                account.ReferralTypeName = referralName;

                //if (account.TerritoryId != null && account.TerritoryId > 0)
                //{
                //    var terr = connection.Get<Territory>(account.TerritoryId);
                //    account.TerritoryType = terr.Name;
                //}
                if (account.IndustryId > 0)
                {
                    var queryIndustry = @"select * from [CRM].[Type_LookUpValues] where Id =@id";
                    var industry = connection.Query<Type_LookUpValues>(queryIndustry, new { id = account.IndustryId }).FirstOrDefault();
                    account.IndustryValue = industry.Name;
                }
                if (account.CommercialTypeId > 0)
                {
                    var queryCommercial = @"select * from [CRM].[Type_LookUpValues] where Id =@id";
                    var commercialType = connection.Query<Type_LookUpValues>(queryCommercial, new { id = account.CommercialTypeId }).FirstOrDefault();
                    account.CommercialValue = commercialType.Name;
                }
                if (account.RelationshipTypeId > 0)
                {
                    var queryRelationship = @"select * from [CRM].[Type_LookUpValues] where Id =@id";
                    var relationshipType = connection.Query<Type_LookUpValues>(queryRelationship, new { id = account.RelationshipTypeId }).FirstOrDefault();
                    account.RelationShipTypeValue = relationshipType.Name;
                }

                query = "select * from crm.Opportunities where accountId = @accountId";
                franchiseId = null;
                nullable = null;
                List<Opportunity> opportunities = connection.Query<Opportunity>(query, new { accountId = id }, null, true, franchiseId, nullable).ToList<Opportunity>();
                account.Opportunities = opportunities;
                query = "select * from crm.Customer where customerid in (\r\n                            select customerid from crm.AccountCustomers where accountId = @accountId and IsPrimaryCustomer = 0)";
                franchiseId = null;
                nullable = null;
                List<CustomerTP> secPerson = connection.Query<CustomerTP>(query, new { accountId = id }, null, true, franchiseId, nullable).ToList<CustomerTP>();
                account.SecCustomer = secPerson;
                query = "select * from crm.Customer where customerid in (\r\n                            select customerid from crm.AccountCustomers where accountId = @accountId and IsPrimaryCustomer = 1)";
                franchiseId = null;
                nullable = null;
                CustomerTP customer = connection.Query<CustomerTP>(query, new { accountId = id }, null, true, franchiseId, nullable).FirstOrDefault<CustomerTP>();
                account.PrimCustomer = customer;
                query = "select * from CRM.Campaign Where CampaignId = @campaignId";
                franchiseId = null;
                nullable = null;
                Campaign campaign = connection.Query<Campaign>(query, new { campaignId = account.CampaignId }, null, true, franchiseId, nullable).FirstOrDefault<Campaign>();
                if (campaign != null)
                {
                    account.CampaignName = campaign.Name;
                }
                query = "select * from crm.Addresses where AddressId in (\r\n                            select AddressId from crm.AccountAddresses where AccountId = @accountId)";
                franchiseId = null;
                nullable = null;
                List<AddressTP> addresses = connection.Query<AddressTP>(query, new { accountId = id }, null, true, franchiseId, nullable).ToList<AddressTP>();
                account.Addresses = addresses;

                query = @"select stp.SourcesTPId as SourceId,o.Name as OriginationName,s.Name as SourceName,c.Name as ChannelName,o.name +' - '+ c.Name + ' - ' + s.Name as name,
                    ls.IsPrimarySource from crm.SourcesTP stp                      
                    join crm.Type_Channel c on c.ChannelId = stp.channelid           
                    join crm.sources s on s.SourceId = stp.SourceId            
                    join crm.AccountSources ls on ls.SourceId = stp.SourcesTPId              
                    join crm.Accounts l on l.AccountId = ls.AccountId   
                    join CRM.Type_Owner o on o.OwnerId = stp.OwnerId 
                    where ls.AccountId = @accountId";
                franchiseId = null;
                nullable = null;
                List<SourceList> accountSources = connection.Query<SourceList>(query, new { accountId = id }, null, true, franchiseId, nullable).ToList<SourceList>();
                account.SourcesList = accountSources;
                franchiseId = null;
                nullable = null;
                account.Type_AccountStatus = connection.Query<Type_AccountStatus>("select * from CRM.Type_AccountStatus where AccountStatusId = @id", new { id = account.AccountStatusId }, null, true, franchiseId, nullable).FirstOrDefault<Type_AccountStatus>();
                franchiseId = null;
                nullable = null;
                account.Disposition = connection.Query<Disposition>("select * from CRM.Disposition where DispositionId = @dispositionid", new { dispositionid = account.DispositionId }, null, true, franchiseId, nullable).FirstOrDefault<Disposition>();

                franchiseId = null;
                nullable = null;

                // Not required as it was handled by notes and attachments components
                // query = "select * from crm.Note where accountId = @accountId";
                //List<Note> accountNotes = connection.Query<Note>(query, new { accountId = id }, null, true, franchiseId, nullable).ToList<Note>();
                //account.AccountNotes = accountNotes;
                //try
                //{
                //    query = string.Concat("SELECT AccountId, '' AS 'FileName', '' AS 'FullFileName' , Notes , ", " (CASE TypeEnum WHEN 1 THEN 'Internal' WHEN 2 THEN 'Customer' WHEN 3 THEN 'Direction' END) AS 'Category',");
                //    query = string.Concat(query, " Title, TypeEnum FROM [CRM].[Note] WHERE AccountId = @accountId ");
                //    query = string.Concat(query, " UNION ALL ");
                //    query = string.Concat(query, " SELECT LeadId, FileName, UrlPath,'',FileCategory,FileDescription,0 FROM [CRM].[PhysicalFiles] PHY ");
                //    query = string.Concat(query, " INNER JOIN [CRM].[Attachments] ATT ON PHY.PhysicalFileId = ATT.PhysicalFileId ");
                //    query = string.Concat(query, " WHERE ATT.AccountId = @accountId");
                //    franchiseId = null;
                //    nullable = null;
                //    List<NotesAttachmentslist> accountNoteAttachment = connection.Query<NotesAttachmentslist>(query, new { accountId = id }, null, true, franchiseId, nullable).ToList<NotesAttachmentslist>();
                //    account.NotesAttachmentslist = accountNoteAttachment;
                //}
                //catch (Exception exception)
                //{
                //}
                if (account != null)
                {
                    SourceList primarySource = (
                        from x in accountSources
                        where x.IsPrimarySource
                        select x).FirstOrDefault<SourceList>();
                    if (primarySource != null)
                    {
                        account.SourcesTPId = primarySource.SourceId;
                    }

                    franchiseId = account.CampaignId;
                    if (!franchiseId.HasValue)
                    {
                        account.CampaignId = new int?(0);
                    }
                }
                accountTP = account;
            }
            return accountTP;
        }

        public List<Address> GetAddress(List<int> accountIds)
        {
            //if (!BasePermission.CanRead)
            //    throw new UnauthorizedAccessException(Unauthorized);
            List<Address> addrlist = new List<Address>();
            try
            {
                if (accountIds == null || accountIds.Count == 0)
                    return null;
                int accountId = accountIds[0];
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    //var query = @"select PersonId as SalesAgentId,FirstName + ' ' + LastName  as Name FROM [CRM].[Person] ";
                    connection.Open();
                    var query = @"select AccountId from [CRM].[Accounts] where accountId=@accountId";
                    var account = connection.Query(query, new { accountId = accountId }).ToList();

                    accountId = account.First().AccountId;

                    query = @"select AddressId from [CRM].[AccountAddresses] where AccountId=@accountId";
                    var Address = connection.Query(query, new { AccountId = accountId }).ToList();
                    int AddressId = Address.First().AddressId;

                    query = @"select * from CRM.Addresses where AddressId=@AddressId";
                    var addresslist = connection.Query(query, new { AddressId = AddressId }).ToList();

                    connection.Close();
                    for (int i = 0; i < addresslist.Count; ++i)
                    {
                        addrlist.Add(new Address
                        {
                            AddressId = addresslist[i].AddressId,
                            Address1 = addresslist[i].Address1,
                            Address2 = addresslist[i].Address2,
                            City = addresslist[i].City,
                            State = addresslist[i].State,
                            ZipCode = addresslist[i].ZipCode,
                            CountryCode2Digits = addresslist[i].CountryCode2Digits,
                            CreatedOnUtc = addresslist[i].CreatedOnUtc,
                            IsDeleted = addresslist[i].IsDeleted,
                            IsResidential = addresslist[i].IsResidential,
                            CrossStreet = addresslist[i].CrossStreet,
                            Location = addresslist[i].Location,
                            AddressesGuid = addresslist[i].AddressesGuid
                        });

                    }
                    return addrlist;
                }

                //return (from l in CRMDBContext.Opportunities

            }
            catch (Exception ex)
            {
                //log error
            }
            return addrlist;


        }

        public AddressTP GetAddressTP(int accountId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var str = @"select a.* from crm.Accounts l
                        inner join crm.AccountAddresses  la on la.AccountId=l.AccountId
                        inner join crm.addresses a on a.AddressId=la.AddressId
                        where l.AccountId=@AccountId";
                var address = connection.Query<HFC.CRM.Data.AddressTP>(str
                    , new { AccountId = accountId }).FirstOrDefault();
                connection.Close();

                return address;
            }
        }

        public List<AccountTP> GetDuplicateAccount(SearchDuplicateAccountEnum searchType, string searchTerm, AccountTP dto = null)
        {
            //
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                //var linq = from l in CRMDBContext.Opportunities.AsNoTracking()
                //where l.FranchiseId == Franchise.FranchiseId && l.IsDeleted == false
                //select l;
                var linq = "select l.*, lc.customerid, c.FirstName, c.LastName from crm.Opportunities l join crm.Accountcustomers lc on lc.AccountId = l.AccountId join crm.Customer c on lc.customerid = c.CustomerId where l.FranchiseId = @franchiseId and lc.isprimarycustomer = 1";
                //var linq = connection.Query<Account>(query, new { franchiseId = Franchise.FranchiseId });
                string parsedPhone = RegexUtil.GetNumbersOnly(searchTerm);

                switch (searchType)
                {
                    case SearchDuplicateAccountEnum.CellPhone:
                        linq += @"and c.CellPhone = @parsedPhone";
                        break;
                    case SearchDuplicateAccountEnum.HomePhone:
                        linq += @" and c.HomePhone = @parsedPhone";
                        break;
                    case SearchDuplicateAccountEnum.PrimaryEmail:
                        linq += @" and c.HomePhone = @parsedPhone";
                        break;
                    case SearchDuplicateAccountEnum.FaxPhone:
                        linq += @" and c.FaxPhone = @parsedPhone";
                        break;
                    case SearchDuplicateAccountEnum.WorkPhone:
                        linq += @" and c.WorkPhone = @parsedPhone";
                        break;
                    case SearchDuplicateAccountEnum.SecondaryEmail:
                        linq += @" and c.SecondaryEmail = @parsedPhone";
                        break;
                    case SearchDuplicateAccountEnum.All:
                        linq += @" and c.CellPhone = @parsedPhone or";
                        linq += @" c.HomePhone = @parsedPhone or";
                        //linq += @" c.HomePhone = @parsedPhone or ";
                        linq += @" c.FaxPhone  = @parsedPhone or ";
                        linq += @" c.WorkPhone = @parsedPhone or ";
                        linq += @" c.SecondaryEmail = @parsedPhone ";
                        break;
                    default:
                        return new List<AccountTP>();
                }
                var list = connection.Query<AccountTP>(linq, new { franchiseId = Franchise.FranchiseId, parsedPhone = parsedPhone }).ToList();

                //var list = connection.GetList<Account>(linq, new { franchiseId = Franchise.FranchiseId,parsedPhone = parsedPhone }).ToList();

                foreach (var item in list)
                {
                    var query1 = "select a.* from crm.Addresses a inner join CRM.AccountAddresses la on a.AddressId = la.AddressId where la.AccountId = @AccountId";
                    item.Addresses = connection.Query<AddressTP>(query1, new { AccountId = item.AccountId }).ToList();
                    var query2 = "select c.* from crm.Customer c where c.CustomerId = @customerId";
                    item.PrimCustomer = connection.Query<CustomerTP>(query2, new { customerId = item.CustomerId }).FirstOrDefault();
                    var query3 = "select j.* from crm.Opportunities j where j.OpportunityId = @OpportunityId";
                    item.Opportunities = connection.Query<Opportunity>(query3, new { OpportunityId = item.OpportunityId }).ToList();
                }

                connection.Close();
                return list;
                //return linq.ToList();

            }
        }

        public int? spAccountNumber_New(int? franchiseId)
        {
            int? item;
            using (SqlConnection connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                int? nullable = null;
                CommandType? nullable1 = null;
                List<dynamic> dto = connection.Query("[CRM].[spAccountNumber_New] @FranchiseId", new { FranchiseId = franchiseId }, null, true, nullable, nullable1).ToList<object>();
                item = (int?)dto[0].AccountNumber;
            }
            return item;
        }
        /// <summary>
        ///
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="numberType"></param>
        /// numberType --> 1--Account Number
        /// numberType --> 2--QuoteNumber
        /// numberType --> 3--OrderNumber
        /// numberType --> 4--InvoiceNumber
        /// numberType --> 5--PONumber
        /// <returns></returns>
        public int GetNextNumber(int franchiseId, int numberType)
        {
            int item;
            using (SqlConnection connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                int? nullable = null;
                CommandType? nullable1 = null;
                List<dynamic> dto = connection.Query("[CRM].[spGetNextNumber]   @FranchiseId,@numberType", new { FranchiseId = franchiseId, numberType = numberType }, null, true, nullable, nullable1).ToList<object>();
                item = dto[0].nextNumber;
            }
            return item;
        }

        /// <summary>
        /// Gets the next temp number (It will not update the table).
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="numberType"></param>
        /// numberType --> 1--Account Number
        /// numberType --> 2--QuoteNumber
        /// numberType --> 3--OrderNumber
        /// numberType --> 4--InvoiceNumber
        /// numberType --> 5--PONumber
        /// <returns></returns>
        public int GetNextNumberReadOnly(int franchiseId, int numberType)
        {
            int item;
            using (SqlConnection connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                int? nullable = null;
                CommandType? nullable1 = null;
                List<dynamic> dto = connection.Query("[CRM].[spGetNextNumberReadOnly]   @FranchiseId,@numberType", new { FranchiseId = franchiseId, numberType = numberType }, null, true, nullable, nullable1).ToList<object>();
                item = dto[0].nextNumber;
            }
            return item;
        }

        public string UpdateAccountFromConverLead(AccountTP data, Lead leaddata, int leadId, int accountid)
        {
            //return "Update";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    var Account = connection.Query<AccountTP>("select * from CRM.Accounts where AccountId = @Accountid", new { Accountid = accountid }, transaction).FirstOrDefault();
                    Account.AccountStatusId = data.AccountStatusId;
                    Account.DispositionId = data.DispositionId;
                    Account.CampaignId = data.CampaignId;
                    Account.LeadId = leadId;

                    if (data != null && data.AccountSources != null)
                    {
                        var query = "delete from crm.AccountSources where AccountId = @AccountId ";
                        connection.Execute(query, new { AccountId = Account.AccountId }, transaction);

                        // Insert the primary source first....
                        connection.Query(@"insert into crm.AccountSources (SourceId,AccountId,CreatedOnUtc,IsManuallyAdded,IsPrimarySource) 
                        values(@sourceId,@AccountId,@createdonutc,@isManuallyAdded,@isPrimarySource)",
                            new
                            {
                                sourceId = data.SourcesTPId,
                                AccountId = Account.AccountId,
                                createdonutc = DateTime.UtcNow,
                                isManuallyAdded = true,
                                isPrimarySource = true
                            }, transaction);

                        // TODO: need to be hanlded better.
                        // Right now we are just removing the primary source from the
                        // additional source list.
                        //var additionalSources = data.AccountSources.Where(x => x.SourceId != data.SourcesTPId).ToList();

                        //foreach (var item in additionalSources)//data.AccountSources)
                        //{
                        //    connection.Query("insert into crm.AccountSources (SourceId,AccountId,CreatedOnUtc,IsManuallyAdded,IsPrimarySource) values(@sourceId,@AccountId,@createdonutc,@isManuallyAdded,@isPrimarySource)", new { sourceId = item.SourceId, AccountId = data.AccountId, createdonutc = DateTime.Now, isManuallyAdded = true, isPrimarySource = false }, transaction);
                        //}
                    }

                    //Update the Qualification section on the Account from the Lead Qualification section

                    //SELECT TYQ.QuestionId, TYQ.Question, TYQ.SubQuestion, TYQ.CreatedOnUtc, TYQ.SelectionType, TYQ.DisplayOrder, TYQ.FranchiseId, TYQ.BrandId, ISNULL((CASE WHEN QA.Answer = 'True' THEN 'true' WHEN QA.Answer = 'False' THEN 'false' ELSE QA.Answer END), '') AS 'Answer', ISNULL(QA.AnswerId, '') AS 'AnswerId'FROM [CRM].[Type_Questions] TYQ Left JOIN (SELECT QuestionId, Answer, AnswerId FROM [CRM].[QuestionAnswers] WHERE accountId = 31) AS QA on TYQ.QuestionId = QA.QuestionId WHERE AccountQuestion = 1 AND BrandId = 1 and AnswerId>0
                    var qualifyquery = " SELECT TYQ.QuestionId, TYQ.Question, TYQ.SubQuestion, TYQ.CreatedOnUtc, TYQ.SelectionType, TYQ.DisplayOrder, TYQ.FranchiseId, TYQ.BrandId, ISNULL((CASE WHEN QA.Answer = 'True' THEN 'true' WHEN QA.Answer = 'False' THEN 'false' ELSE QA.Answer END), '') AS 'Answer', ISNULL(QA.AnswerId, '') AS 'AnswerId'FROM [CRM].[Type_Questions] TYQ Left JOIN (SELECT QuestionId, Answer, AnswerId FROM [CRM].[QuestionAnswers] WHERE accountId = @accountId) AS QA on TYQ.QuestionId = QA.QuestionId WHERE AccountQuestion = 1 and AnswerId>0";
                    var AccountQualificationData = connection.Query(qualifyquery, new { accountId = accountid }, transaction).FirstOrDefault();
                    if (AccountQualificationData == null)
                    {
                        //Add Lead data into account
                    }
                    //if (data.Addresses != null)
                    //{
                    //    foreach (var addr in data.Addresses)
                    //    {
                    //        this.UpdateAddress(accountid, addr);
                    //    }
                    //}
                    //if (data.SecCustomer != null)
                    //{
                    //    // data.SecCustomer.Add(data.SecCustomer);
                    //    foreach (var secCustomer in data.SecCustomer)
                    //    {
                    //        this.UpdateCustomer(accountid, secCustomer);
                    //    }
                    //}
                    //if (data.PrimCustomer != null)
                    //{
                    //    this.UpdateCustomer(accountid, data.PrimCustomer);

                    //}
                    Account.LastUpdatedOnUtc = DateTime.UtcNow;
                    Account.LastUpdatedByPersonId = User.PersonId;

                    //Added for Notes and Attachment
                    //SaveNoteAttachment(Account.AccountId, data, connection, transaction);

                    // Copy Lead address to Account address
                    connection.Query("insert into CRM.AccountAddresses SELECT @AccountId,AddressId,IsPrimaryAddress FROM CRM.LeadAddresses where LeadId=@LeadId", new { LeadId = leadId, AccountId = Account.AccountId }, transaction);

                    // Copy Lead customer to Account Customer
                    connection.Query("insert into CRM.AccountCustomers select @AccountId,CustomerId,0 FROM CRM.LeadCustomers where LeadId=@LeadId", new { LeadId = leadId, AccountId = Account.AccountId }, transaction);

                    // Copy Lead Notes and attachment to Account
                    connection.Query("update CRM.Note set AccountId=@AccountId where LeadId=@LeadId", new { LeadId = leadId, AccountId = Account.AccountId }, transaction);

                    // Copy Lead Event to Account
                    connection.Query("update CRM.Calendar set AccountId=@AccountId where LeadId=@LeadId", new { LeadId = leadId, AccountId = Account.AccountId }, transaction);

                    // Copy Lead Task to Account
                    connection.Query("update CRM.Tasks set AccountId=@AccountId where LeadId=@LeadId", new { LeadId = leadId, AccountId = Account.AccountId }, transaction);


                    if (Account.CampaignId != null && Account.CampaignId == 0) Account.CampaignId = null;
                    if (data.AccountTypeId != null) Account.AccountTypeId = data.AccountTypeId;

                    Account.ImpersonatorPersonId = this.ImpersonatorPersonId;

                    connection.Update(Account, transaction);
                    transaction.Commit();
                    connection.Close();

                    // Audit trial
                    EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(data), AccountId: Account.AccountId);

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw;
                }
                return Success;
            }


        }
        public override string Update(AccountTP data)
        {
            try
            {
                //return "Update";
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();

                    if (data != null && data.AccountSources != null)
                    {
                        var query = "delete from crm.AccountSources where AccountId = @AccountId ";
                        connection.Execute(query, new { AccountId = data.AccountId });
                        //foreach(var source in data.AccountSources)
                        //{
                        //    connection.Delete(source);
                        //}

                        // Insert the primary source first....
                        if (data.SourcesTPId > 0)
                        {
                            connection.Query(@"insert into crm.AccountSources (SourceId,AccountId,CreatedOnUtc,IsManuallyAdded,IsPrimarySource) 
                        values(@sourceId,@AccountId,@createdonutc,@isManuallyAdded,@isPrimarySource)",
                        new
                        {
                            sourceId = data.SourcesTPId,
                            AccountId = data.AccountId,
                            createdonutc = DateTime.UtcNow,
                            isManuallyAdded = true,
                            isPrimarySource = true
                        });
                        }


                        // TODO: need to be hanlded better.
                        // Right now we are just removing the primary source from the
                        // additional source list.
                        var additionalSources = data.AccountSources.Where(x => x.SourceId != data.SourcesTPId).ToList();

                        foreach (var item in additionalSources)//data.AccountSources)
                        {
                            connection.Query("insert into crm.AccountSources (SourceId,AccountId,CreatedOnUtc,IsManuallyAdded,IsPrimarySource) values(@sourceId,@AccountId,@createdonutc,@isManuallyAdded,@isPrimarySource)", new { sourceId = item.SourceId, AccountId = data.AccountId, createdonutc = DateTime.UtcNow, isManuallyAdded = true, isPrimarySource = false });
                        }
                    }
                    if (data.Addresses != null)
                    {
                        foreach (var addr in data.Addresses)
                        {
                            this.UpdateAddress(data.AccountId, addr);
                            // Update Opportunity Installation territory info
                            if (addr.AddressId > 0)
                            {
                                string[] TerritoryDetails = TerritoryManager.GetTerritoryDetails(addr.ZipCode, addr.CountryCode2Digits).Split(';');
                                connection.Execute("update CRM.Opportunities set TerritoryId=@TerritoryId,TerritoryType=@TerritoryType where AccountId=@AccountId and InstallationAddressId=@InstallationAddressId", new
                                {
                                    TerritoryId = TerritoryDetails[0] != "0" ? Convert.ToInt32(TerritoryDetails[0]) : (int?)null,
                                    TerritoryType = TerritoryDetails[1],
                                    AccountId = data.AccountId,
                                    InstallationAddressId = addr.AddressId
                                });
                            }
                        }

                        string[] TerritoryData = new string[2];
                        TerritoryData = TerritoryManager.GetTerritoryDetails(data.Addresses.FirstOrDefault().ZipCode, data.Addresses.FirstOrDefault().CountryCode2Digits).Split(';');

                        if (TerritoryData != null && TerritoryData[0] != null)
                        {
                            if (TerritoryData[0] == "0")
                            {
                                data.TerritoryType = TerritoryData[1];
                                data.TerritoryId = null;
                            }
                            else
                            {
                                data.TerritoryType = TerritoryData[1];
                                data.TerritoryId = Convert.ToInt32(TerritoryData[0]);
                            }
                        }
                    }
                    if (data.SecCustomer != null || data.PrimCustomer != null)
                    {
                        data.SecCustomer.Add(data.PrimCustomer);
                        foreach (var secCustomer in data.SecCustomer)
                        {
                            this.UpdateCustomer(data.AccountId, secCustomer);
                        }
                    }
                    data.LastUpdatedOnUtc = DateTime.UtcNow;
                    data.LastUpdatedByPersonId = User.PersonId;

                    // Not required as it was handled by notes and attachments components
                    //Added for Notes and Attachment
                    //SaveNoteAttachment(data.AccountId, data, connection);

                    if (data.CampaignId != null && data.CampaignId == 0) data.CampaignId = null;
                    if (data.AccountTypeId != null) data.AccountTypeId = data.AccountTypeId;

                    data.IndustryId = data.IndustryId > 0 ? data.IndustryId : (int?)null;
                    data.CommercialTypeId = data.CommercialTypeId > 0 ? data.CommercialTypeId : (int?)null;
                    data.RelationshipTypeId = data.RelationshipTypeId > 0 ? data.RelationshipTypeId : (int?)null;
                    data.ImpersonatorPersonId = this.ImpersonatorPersonId;

                    HandleTaxExemptId(data);

                    var oppmgr = new OpportunitiesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                    var qmgr = new QuotesManager();
                    var opps = oppmgr.GetOpportunities(data.AccountId);

                    //"If the user changes the exemption on the account
                    //and there is only one Opportunity, and
                    //if no quotes exist on the this opportunity
                    //, then the opportunity flags for exemptions
                    //should be updated as well." - David
                    if (opps != null && opps.Count == 1)
                    {
                        var src = opps[0];
                        var quotes = qmgr.GetQuotes(src.OpportunityId);
                        if (quotes == null || quotes.Count == 0)
                        {
                            var opp = new Opportunity();
                            opp.OpportunityId = src.OpportunityId;
                            opp.IsTaxExempt = data.IsTaxExempt;
                            opp.TaxExemptID = data.TaxExemptID;
                            opp.IsGSTExempt = data.IsGSTExempt;
                            opp.IsHSTExempt = data.IsHSTExempt;
                            opp.IsPSTExempt = data.IsPSTExempt;
                            opp.IsVATExempt = data.IsVATExempt;
                            oppmgr.UpdateTaxExemption(opp);
                        }
                    }
                    if (data.TerritoryType.ToUpper().Contains("GRAY AREA"))
                    {
                        data.TerritoryId = null;
                    }

                    connection.Update(data);
                    connection.Close();

                    // Audit trial
                    EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(data), AccountId: data.AccountId);
                    return Success;
                }
            }
            catch (ValidationException ex)
            {
                return ex.Message;
            }
        }
        public string UpdateAddress(int AccountId, AddressTP data)
        {
            if (AccountId <= 0 || data == null || data.AddressId <= 0)
                return DataCannotBeNullOrEmpty;
            //if (!BasePermission.CanUpdate)
            //    return Unauthorized;

            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var AccountToCheck = connection.Query<AccountTP>("select * from CRM.Accounts where AccountId = @Accountid", new { Accountid = AccountId }).FirstOrDefault();
                    var address = connection.Query<AddressTP>("select * from CRM.Addresses where AddressId =@addressId", new { addressId = data.AddressId }).FirstOrDefault();
                    //var AccountToCheck = (from l in CRMDBContext.Accounts
                    //    where l.AccountId == AccountId && l.IsDeleted == false && l.FranchiseId == Franchise.FranchiseId
                    //    select new {Address = l.Addresses.FirstOrDefault(f => f.AddressId == data.AddressId)})
                    //    .FirstOrDefault();

                    if (AccountToCheck == null)
                        return AccountTPDoesNotExist;
                    if (address == null)
                    {
                        address.CreatedOnUtc = DateTime.UtcNow;
                        //TODO Created by has to be here.
                        var addressId = (int)connection.Insert(address);
                        connection.Query("insert into crm.AccountAddresses values(@Accountid, @addressid, @IsPrimaryAddress)", new { Accountid = AccountId, addressid = addressId, IsPrimaryAddress = 1 });
                        return "Success";
                    }



                    address.AttentionText = data.AttentionText;
                    address.Address1 = data.Address1;
                    address.Address2 = data.Address2;
                    address.City = data.City;
                    address.State = data.State;
                    address.Location = data.Location;
                    if (!string.IsNullOrEmpty(data.ZipCode))
                        address.ZipCode = data.ZipCode.ToUpper();
                    else
                        address.ZipCode = null;
                    address.CrossStreet = data.CrossStreet;
                    address.CountryCode2Digits = string.IsNullOrEmpty(data.CountryCode2Digits)
                        ? Franchise.CountryCode
                        : data.CountryCode2Digits;
                    address.IsResidential = data.IsResidential;
                    address.IsValidated = data.IsValidated;

                    //TODO Need to verify this code
                    //var changes = Util.GetEFChangedProperties(CRMDBContext.Entry(AccountToCheck.Address), AccountToCheck.Address);

                    //var Account = new Account {AccountId = AccountId};
                    //CRMDBContext.Accounts.Attach(Account);
                    //Account.LastUpdatedOnUtc = DateTime.Now;
                    //Account.LastUpdatedByPersonId = User.PersonId;

                    //if (changes != null)
                    //{
                    //    Account.EditHistories.Add(new EditHistory()
                    //    {
                    //        IPAddress = HttpContext.Current.Request.UserHostAddress,
                    //        LoggedByPersonId = User.PersonId,
                    //        HistoryValue = changes.ToString(System.Xml.Linq.SaveOptions.DisableFormatting)
                    //    });
                    //}

                    //CRMDBContext.SaveChanges();
                    connection.Update(address);
                    connection.Close();

                    return Success;
                }
            }
            catch (Exception ex)
            {

#if DEBUG
                return ex.Message;
#else
                return EventLogger.LogEvent(ex);
#endif
            }

        }
        public string UpdateCustomer(int AccountId, CustomerTP data)
        {
            if (AccountId <= 0 || data == null)
                return DataCannotBeNullOrEmpty;
            //if (!BasePermission.CanUpdate)
            //    return Unauthorized;

            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var Accountexist = connection.Query<AccountTP>("select * from CRM.Accounts where AccountId = @Accountid", new { Accountid = AccountId }).FirstOrDefault();
                    var customer = connection.Query<CustomerTP>("select * from CRM.Customer where CustomerId =@customerId", new { customerId = data.CustomerId }).FirstOrDefault();
                    if (Accountexist == null)
                        return AccountTPDoesNotExist;
                    if (customer == null)
                    {
                        data.CreatedOn = DateTime.UtcNow;
                        //TODO Created by has to be here.
                        var customerId = (int)connection.Insert(data);
                        connection.Query("insert into crm.AccountCustomers values(@Accountid, @customerId,@isPrimaryCustomer)", new { Accountid = AccountId, customerId = customerId, isPrimaryCustomer = false });
                        return "Success";
                    }
                    else if (customer != null)
                    {
                        //check for duplicate email
                        //TODO Check this email validation will check in other customer.
                        if ((customer.PrimaryEmail != data.PrimaryEmail && !string.IsNullOrEmpty(data.PrimaryEmail)) ||
                            (customer.SecondaryEmail != data.SecondaryEmail &&
                             !string.IsNullOrEmpty(data.SecondaryEmail)))
                        {
                            using (SqlConnection conn = new SqlConnection(ContextFactory.CrmConnectionString))
                            {
                                conn.OpenAsync();
                                //var exists = conn.Query<bool>(
                                //    "[CRM].[fnsHasDuplicateEmail](@FranchiseId, @PrimEmail, @SecEmail, @CustomerId)",
                                //    new
                                //    {
                                //        Franchise.FranchiseId,
                                //        data.PrimaryEmail,
                                //        data.SecondaryEmail,
                                //        data.CustomerId

                                var exists = CRMDBContext.Database.SqlQuery<bool>("SELECT [CRM].[fnsHasDuplicateEmail](@p0, @p1, @p2, @p3)",
                            Franchise.FranchiseId, data.PrimaryEmail, data.SecondaryEmail, data.CustomerId)
                            .SingleOrDefault();
                                //    }).FirstOrDefault();
                                if (exists)
                                {
                                    return "Duplicate email address found, unable to update person";
                                }
                            }
                        }

                        //var Account = new Account { AccountId = AccountId };
                        //CRMDBContext.Accounts.Attach(Account);

                        customer.FirstName = data.FirstName;
                        customer.LastName = data.LastName;
                        customer.PrimaryEmail = data.PrimaryEmail;
                        customer.MI = data.MI;
                        if (customer.PrimaryEmail != data.SecondaryEmail)
                            customer.SecondaryEmail = data.SecondaryEmail;
                        customer.IsReceiveEmails = data.IsReceiveEmails;
                        customer.HomePhone = RegexUtil.GetNumbersOnly(data.HomePhone);
                        customer.CellPhone = RegexUtil.GetNumbersOnly(data.CellPhone);
                        customer.WorkPhone = RegexUtil.GetNumbersOnly(data.WorkPhone);
                        customer.WorkPhoneExt = data.WorkPhoneExt;
                        customer.FaxPhone = RegexUtil.GetNumbersOnly(data.FaxPhone);
                        customer.CompanyName = data.CompanyName;
                        customer.WorkTitle = data.WorkTitle;
                        customer.PreferredTFN = data.PreferredTFN;
                        //for unsubscribe only change it if original has date and current doesnt
                        //OR
                        //original doesnt have date and current has date
                        //this is due to the fact that we're using a date instead of a boolean to enable/disable subscription
                        if ((customer.UnsubscribedOn.HasValue && !data.UnsubscribedOn.HasValue) ||
                            (!customer.UnsubscribedOn.HasValue && data.UnsubscribedOn.HasValue))
                            customer.UnsubscribedOn = data.UnsubscribedOn;

                        var CustomerAffected = connection.Update(customer);
                        return Success;
                        //add to history
                        //TODO Handle History Later
                        //var entry = CRMDBContext.Entry(customer);
                        //if (entry.State != EntityState.Unchanged)
                        //{
                        //    Account.LastUpdatedOnUtc = DateTime.Now;
                        //    Account.LastUpdatedByPersonId = User.PersonId;
                        //    var changes = Util.GetEFChangedProperties(entry, customer);

                        //    Account.EditHistories.Add(new EditHistory()
                        //    {
                        //        IPAddress = HttpContext.Current.Request.UserHostAddress,
                        //        LoggedByPersonId = User.PersonId,
                        //        HistoryValue = changes.ToString(SaveOptions.DisableFormatting)
                        //    });

                        //    CRMDBContext.SaveChanges();
                        //    return Success;

                        //}
                        //else //no changes detected
                        //    return Success;
                    }
                    else
                        return Success;
                }
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }

        }

        public List<NotesAttachmentslist> PrintAccountNotes(int id = 0)
        {

            string query = @"SELECT AccountId, '' AS 'FileName', '' AS 'FullFileName' , Notes AS 'Note',  
                            (CASE TypeEnum WHEN 0 THEN 'Internal' WHEN 2 THEN 'Direction' WHEN 5 THEN 'External' END) AS 'Category', 
                            Title, TypeEnum, NoteId AS 'Id' FROM [CRM].[Note] WHERE AccountId = @id and IsNote=1  and (TypeEnum=0 or TypeEnum=5) 
                            UNION ALL  SELECT ATT.LeadId, PHY.FileName, PHY.UrlPath,'',PHY.FileCategory,PHY.FileDescription, 0, 
                                PHY.PhysicalFileId FROM [CRM].[PhysicalFiles] PHY  INNER JOIN [CRM].[Attachments] ATT ON PHY.PhysicalFileId = ATT.PhysicalFileId  WHERE ATT.AccountId = @id
                                order by category desc
                           ";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                var InternalExternalNotes = connection.Query<NotesAttachmentslist>(query, new { id = id }).ToList();
                return InternalExternalNotes;
            }

        }

        public string[] GetAccQuoteNames(int id, int opportunityId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                if (id == 0)
                {
                    var account = connection.Query<QuoteVM>("select [CRM].[fnGetAccountName_forOpportunitySearch](OpportunityId) as AccountName from [CRM].[Opportunities] where OpportunityId = @OpportunityId", new { OpportunityId = opportunityId }).FirstOrDefault();
                    var quotee = connection.Query<Quote>("select * from CRM.Quote where OpportunityId=@OpportunityId", new { OpportunityId = opportunityId }).FirstOrDefault();
                    string[] AccQuoteNames = new string[2];
                    if (account != null)
                        AccQuoteNames[0] = account.AccountName;
                    if (quotee != null)
                        AccQuoteNames[1] = quotee.QuoteName;

                    connection.Close();
                    return AccQuoteNames;
                }
                else
                {
                    var quote = connection.Query<Quote>("select * from CRM.Quote where QuoteKey=@QuoteKey", new { QuoteKey = opportunityId }).FirstOrDefault();
                    int opportunityIdd = quote.OpportunityId;
                    var account = connection.Query<QuoteVM>("select [CRM].[fnGetAccountName_forOpportunitySearch](OpportunityId) as AccountName from [CRM].[Opportunities] where OpportunityId = @OpportunityId", new { OpportunityId = opportunityIdd }).FirstOrDefault();
                    var quotee = connection.Query<Quote>("select * from CRM.Quote where OpportunityId=@OpportunityId", new { OpportunityId = opportunityIdd }).FirstOrDefault();
                    string[] AccQuoteNames = new string[2];
                    if (account != null)
                        AccQuoteNames[0] = account.AccountName;
                    if (quotee != null)
                        AccQuoteNames[1] = quotee.QuoteName;

                    connection.Close();
                    return AccQuoteNames;

                }
            }

        }

        public List<LeadtoAcctDisp> GetMatchDetails(string Value)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                string Query = "";
                Query = @"select cus.*,acc.AccountTypeId,acc.AccountId, 
                          accadd.AddressId,addr.Address1,addr.Address2,addr.City,addr.State ,addr.CountryCode2Digits,
                          addr.ZipCode,addr.IsValidated,lv.Name as AccountType,
                          cus.FirstName + ' ' +cus.LastName+
                          case when cus.[CompanyName] is not null and cus.[CompanyName]!='' then ' / '+cus.[CompanyName] else '' end as FullName
                          from CRM.Customer cus
                          join CRM.AccountCustomers acccus on cus.CustomerId=acccus.CustomerId and IsPrimaryCustomer=1
                          join CRM.Accounts acc on acccus.AccountId=acc.AccountId
                          join CRM.AccountAddresses accadd on acc.AccountId=accadd.AccountId
                          join CRM.Addresses addr on accadd.AddressId=addr.AddressId
                          join CRM.Type_LookUpValues lv on acc.AccountTypeId=lv.Id
                          where acc.FranchiseId=@FranchiseId and cus.PrimaryEmail like @PrimaryEmail order by cus.FirstName asc";
                var result = connection.Query<LeadtoAcctDisp>(Query, new
                {
                    FranchiseId = SessionManager.CurrentFranchise.FranchiseId,
                    PrimaryEmail = "%" + Value + "%",
                }).ToList();
                if (result.Count > 0)
                {
                    foreach (var item in result)
                    {
                        if (item.PreferredTFN == " ")
                        {
                            if (item.HomePhone != null)
                            {
                                item.PreferedNumber = item.HomePhone;
                            }
                            else if (item.CellPhone != null)
                            {
                                item.PreferedNumber = item.CellPhone;
                            }
                            else if (item.WorkPhone != null)
                            {
                                item.PreferedNumber = item.WorkPhone;
                            }
                            else if (item.FaxPhone != null)
                            {
                                item.PreferedNumber = item.FaxPhone;
                            }
                        }
                        else
                        {
                            if (item.PreferredTFN == "H")
                            {
                                item.PreferedNumber = item.HomePhone;
                            }
                            else if (item.PreferredTFN == "C")
                            {
                                item.PreferedNumber = item.CellPhone;
                            }
                            else if (item.PreferredTFN == "W")
                            {
                                item.PreferedNumber = item.WorkPhone;
                            }
                        }

                        //if (item.AccountTypeId == 1)
                        //{

                        //    item.AccountType = "Prospect";
                        //}
                        //else if (item.AccountTypeId == 2)
                        //{
                        //    item.AccountType = "Key/Commercial";
                        //}
                        //else if (item.AccountTypeId == 3)
                        //{
                        //    item.AccountType = "Customer";
                        //}

                    }
                    fulladdress(result);
                }
                return result;
            }


        }

        public List<LeadtoAcctDisp> GetMatchPhneDetails(string Num_Val)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                string Query = "";
                Query = @"select cus.*,acc.AccountTypeId,acc.AccountId, 
                          accadd.AddressId,addr.Address1,addr.Address2,addr.City,addr.State ,addr.CountryCode2Digits,
                          addr.ZipCode,addr.IsValidated,lv.Name as AccountType,
                          cus.FirstName + ' ' +cus.LastName+
                          case when cus.[CompanyName] is not null and cus.[CompanyName]!='' then ' / '+cus.[CompanyName] else '' end as FullName
                          from CRM.Customer cus
                          join CRM.AccountCustomers acccus on cus.CustomerId=acccus.CustomerId and IsPrimaryCustomer=1
                          join CRM.Accounts acc on acccus.AccountId=acc.AccountId
                          join CRM.AccountAddresses accadd on acc.AccountId=accadd.AccountId
                          join CRM.Addresses addr on accadd.AddressId=addr.AddressId
                          join CRM.Type_LookUpValues lv on acc.AccountTypeId=lv.Id
                          where acc.FranchiseId=@FranchiseId and @Number in(cus.HomePhone,cus.CellPhone,cus.WorkPhone,cus.FaxPhone) order by cus.FirstName asc";
                var result = connection.Query<LeadtoAcctDisp>(Query, new
                {
                    FranchiseId = SessionManager.CurrentFranchise.FranchiseId,
                    Number = Num_Val,
                }).ToList();
                if (result.Count > 0)
                {
                    foreach (var item in result)
                    {
                        if (item.HomePhone != null && item.HomePhone == Num_Val)
                        {
                            item.PreferedNumber = item.HomePhone;
                        }
                        else if (item.CellPhone != null && item.CellPhone == Num_Val)
                        {
                            item.PreferedNumber = item.CellPhone;
                        }
                        else if (item.WorkPhone != null && item.WorkPhone == Num_Val)
                        {
                            item.PreferedNumber = item.WorkPhone;
                        }
                        else if (item.FaxPhone != null && item.FaxPhone == Num_Val)
                        {
                            item.PreferedNumber = item.FaxPhone;
                        }

                        //if (item.AccountTypeId == 1)
                        //{

                        //    item.AccountType = "Prospect";
                        //}
                        //else if (item.AccountTypeId == 2)
                        //{
                        //    item.AccountType = "Key/Commercial";
                        //}
                        //else if (item.AccountTypeId == 3)
                        //{
                        //    item.AccountType = "Customer";
                        //}
                    }

                    fulladdress(result);
                }
                return result;
            }
        }

        public List<LeadtoAcctDisp> GetMatchAddrDetails(string AddrValue)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                string Query = "";
                Query = @"select cus.*,acc.AccountTypeId,acc.AccountId, 
                          accadd.AddressId,addr.Address1,addr.Address2,addr.City,addr.State ,addr.CountryCode2Digits,
                          addr.ZipCode,addr.IsValidated,lv.Name as AccountType,
                          cus.FirstName + ' ' +cus.LastName+
                          case when cus.[CompanyName] is not null and cus.[CompanyName]!='' then ' / '+cus.[CompanyName] else '' end as FullName
                          from CRM.Customer cus
                          join CRM.AccountCustomers acccus on cus.CustomerId=acccus.CustomerId and IsPrimaryCustomer=1
                          join CRM.Accounts acc on acccus.AccountId=acc.AccountId
                          join CRM.AccountAddresses accadd on acc.AccountId=accadd.AccountId
                          join CRM.Addresses addr on accadd.AddressId=addr.AddressId
                          join CRM.Type_LookUpValues lv on Acc.AccountTypeId=lv.Id
                          where acc.FranchiseId=@FranchiseId and addr.Address1 like @Address order by cus.FirstName asc";
                var result = connection.Query<LeadtoAcctDisp>(Query, new
                {
                    FranchiseId = SessionManager.CurrentFranchise.FranchiseId,
                    Address = "%" + AddrValue + "%",
                }).ToList();
                if (result.Count > 0)
                {
                    foreach (var item in result)
                    {
                        if (item.PreferredTFN == " ")
                        {
                            if (item.HomePhone != null)
                            {
                                item.PreferedNumber = item.HomePhone;
                            }
                            else if (item.CellPhone != null)
                            {
                                item.PreferedNumber = item.CellPhone;
                            }
                            else if (item.WorkPhone != null)
                            {
                                item.PreferedNumber = item.WorkPhone;
                            }
                            else if (item.FaxPhone != null)
                            {
                                item.PreferedNumber = item.FaxPhone;
                            }
                        }
                        else
                        {
                            if (item.PreferredTFN == "H")
                            {
                                item.PreferedNumber = item.HomePhone;
                            }
                            else if (item.PreferredTFN == "C")
                            {
                                item.PreferedNumber = item.CellPhone;
                            }
                            else if (item.PreferredTFN == "W")
                            {
                                item.PreferedNumber = item.WorkPhone;
                            }
                        }
                    }

                    fulladdress(result);
                }
                return result;
            }
        }


        public List<LeadtoAcctDisp> fulladdress(List<LeadtoAcctDisp> model)
        {
            foreach (var item in model)
            {
                if (item.Address1 != null && item.Address1 != "")
                {
                    item.FullAddress = item.Address1;
                }
                if (item.Address2 != null && item.Address2 != "")
                {
                    if (item.FullAddress == null)
                    {
                        item.FullAddress = item.Address2;
                    }
                    else
                        item.FullAddress = item.FullAddress + "," + item.Address2;
                }
                if (item.City != null && item.City != "")
                {
                    if (item.FullAddress == null)
                    {
                        item.FullAddress = item.City;
                    }
                    else
                        item.FullAddress = item.FullAddress + "," + item.City;
                }
                if (item.State != null && item.State != "")
                {
                    if (item.FullAddress == null)
                    {
                        item.FullAddress = item.State;
                    }
                    else
                        item.FullAddress = item.FullAddress + "," + item.State;
                }
                if (item.ZipCode != null && item.ZipCode != "")
                {
                    if (item.FullAddress == null)
                    {
                        item.FullAddress = item.ZipCode;
                    }
                    else
                        item.FullAddress = item.FullAddress + "," + item.ZipCode;
                }
                if (item.CountryCode2Digits != null && item.CountryCode2Digits != "")
                {
                    if (item.FullAddress == null)
                    {
                        item.FullAddress = item.CountryCode2Digits;
                    }
                    else
                        item.FullAddress = item.FullAddress + "," + item.CountryCode2Digits;
                }

            }
            return model;
        }

        public dynamic getTerritoryForFranchise()
        {

            string query = @"select TerritoryId as Id, Name   FROM [CRM].[Territories] where FranchiseId=@FranchiseId ";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                var res = connection.Query(query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                return res;
            }

        }

        public dynamic GetAccountType()
        {
            string query = @"select * from CRM.Type_LookUpValues where TableId=11 order by SortOrder asc";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var res = connection.Query(query).ToList();
                return res;
            }
        }

        //texting
        public string UpdateNotifyText(int AccountId)
        {
            using (SqlConnection connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                connection.Query("UPDATE CRM.Accounts SET IsNotifyText = 0 WHERE AccountId = @AccountId", new { AccountId = AccountId });
                connection.Close();
            }

            return "Success";
        }
    }
}
