//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HFC.CRM.Managers.Maestro
{
    using System;
    using System.Collections.Generic;
    
    public partial class list_source_cat
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public Nullable<int> sort { get; set; }
        public string cat_default { get; set; }
        public int cat_locked { get; set; }
        public string synced_item { get; set; }
        public string @readonly { get; set; }
    }
}
