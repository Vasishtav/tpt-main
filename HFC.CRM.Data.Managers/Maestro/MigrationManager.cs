﻿using HFC.CRM.Core;
using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Web;
using System.Windows;
using System.ComponentModel;
using System.Diagnostics;


namespace HFC.CRM.Managers.Maestro
{
    public class MigrationManager : IDataManager
    {
        public MigrationManager() { }
        public MigrationManager(Franchise franchise, string sourceDBName)
        {
            this.Franchise = franchise;
            Initialize(sourceDBName);
        }
        #region Private classes
        public class StatusDTO
        {
            public int? job_status { get; set; }
            public int? job_status_pos { get; set; }
            public string status { get; set; }
            public string position { get; set; }
            public int? LeadStatusId { get; set; }
            public int? JobStatusId { get; set; }
        }

        public class SourceDTO
        {
            public int? CategoryID { get; set; }
            public int? list_sourceId { get; set; }
            public int? ParentId { get; set; }
        }
        public class SourceParentHelper
        {
            public int? CategoryID { get; set; }
            public int? ParentID { get; set; }
        }

        public class MigrationProduct
        {
            public job_product JobItem { get; set; }
            public product Category { get; set; }
            public list_materialtype Material { get; set; }
            public Job CreatedJob { get; set; }
        }
        public class lookupHelperJobs
        {
            public int? jobid { get; set; }
            public int? JobId { get; set; }
        }
        public enum PaymentMethod : int
        {
            Visa = 1,
            Mastercard = 2,
            AMEX = 3,
            Check = 4,
            MoneyOrder = 5,
            Cash = 6,
            Other = 7,
            GiftCertificate = 8
        }


        #endregion
        #region publics
        public LeadsManager LeadsManager { get; set; }
        public List<Data.User> Users { get; set; }
        public List<Data.Job> Jobs { get; set; }
        public InvoiceManager InvoiceManager { get; set; }
        public List<StatusDTO> MigrationStatuses { get; set; }
        public List<SourceDTO> MigrationSources { get; set; }
        public List<SourceParentHelper> MigrationaParentHelper { get; set; }
        MaestroEntities maestroDB { get; set; }
        CRMContext db { get; set; }
        public List<Type_JobStatus> JobStatus { get; set; }
        public List<Type_LeadStatus> LeadStatus { get; set; }
        public List<Source> Sources { get; set; }
        public List<job> MaeJobs { get; set; }
        public List<MigrationProduct> MaeJobProducts { get; set; }
        public List<customer> MaeCustomers { get; set; }
        private List<list_source> MaeSources { get; set; }
        private List<list_source_cat> MaeSourceParents { get; set; }
        public string RepositoryPath { get; set; }
        public string TargetPath { get; set; }
        #endregion
        private void Initialize(string sourceDBName)
        {
            try
            {
                InvoiceManager = new InvoiceManager(this.User, this.Franchise);
                maestroDB = new MaestroEntities();
                db = new CRMContext();
                maestroDB.Database.Connection.ConnectionString = maestroDB.Database.Connection.ConnectionString.Replace("source", sourceDBName);
                MigrationStatuses = db.Database.SqlQuery<StatusDTO>("SELECT job_status, job_status_pos, LeadStatusId, JobStatusId FROM TLMigrationTracker.dbo.list_jobstatus WHERE [LeadStatusId] IS NOT NULL or FranchiseId=" + this.Franchise.FranchiseId.ToString()  ).ToList();
                MigrationSources = db.Database.SqlQuery<SourceDTO>("SELECT CategoryID,List_sourceId,ParentId FROM TLMigrationTracker.dbo.SourceMap WHERE FranchiseId is null OR FranchiseId=" + this.Franchise.FranchiseId.ToString()).ToList().ToList();
                MigrationaParentHelper = db.Database.SqlQuery<SourceParentHelper>("SELECT CategoryID,ParentId FROM TLMigrationTracker.dbo.SourceCategoryToParentSource").ToList();
            }
            catch (Exception ex)
            {
                var err = ex.InnerException;
            }
            finally
            {
                Debug.WriteLine("Initialized, Time Complteted:" + DateTime.Now.ToString());
            }

        }

        public void migrateSources()
        {
            List<list_source> CustomSources = new List<list_source>();
            try
            {
                MaeSources = maestroDB.list_source.ToList(); //.Where(w=>w.src_activeflag=="TRUE")
                MaeSourceParents = maestroDB.list_source_cat.ToList();
                MigrationaParentHelper = new List<SourceParentHelper>();
                Sources = db.Sources.Where(w => w.FranchiseId == null).ToList();
                CustomSources = MaeSources.Where(p => !MigrationSources.Any(p2 => p2.list_sourceId == p.id)).ToList();

                CustomSources.ForEach(custSource => customSourceCreate(custSource));
            }
            catch (Exception ex)
            {
                var err = ex.InnerException;
            }
            finally
            {
                Debug.WriteLine("migrateSources " + CustomSources.Count().ToString() + " Time Complteted:" + DateTime.Now.ToString());
            }


        }
        public void migrateStatuses()
        {

            var _statuses = maestroDB.list_jobstatus.ToList();
            var CustomStatues = _statuses.Where(p => !MigrationStatuses.Any(p2 => p2.job_status_pos == p.id)).ToList();
            JobStatus = db.Type_JobStatus.Where(w => w.FranchiseId == null).ToList();
            LeadStatus = db.Type_LeadStatus.Where(w => w.FranchiseId == null).ToList();
            CustomStatues.ForEach(e => customStatusCreate(e));
        }
        private void customSourceCreate(list_source customSource)
        {
            Source Parent;
            if (!string.IsNullOrWhiteSpace(customSource.src_name))
            {
                try
                {
                    Parent = new Source();
                    SourceParentHelper CreatedParent = new SourceParentHelper();
                    CreatedParent = MigrationaParentHelper.FirstOrDefault(d => d.CategoryID == customSource.src_cat_link);

                    if (MaeSourceParents.Count() > 0 && MigrationaParentHelper.FirstOrDefault(d => d.CategoryID == customSource.src_cat_link) == null)
                    {
                        var _Parent = MaeSourceParents.FirstOrDefault(d => d.id == customSource.src_cat_link);
                        if (CreatedParent == null)
                        {
                            if (_Parent != null)
                            {
                                Parent.FranchiseId = this.Franchise.FranchiseId;
                                Parent.Name = _Parent.name;
                                Parent.Description = !string.IsNullOrWhiteSpace(_Parent.description) ? _Parent.description : null;
                                Parent.CreatedOn = DateTime.Now;

                                db.Sources.Add(Parent);
                                SaveChanges(db);

                                CreatedParent = new SourceParentHelper() { CategoryID = (short?)customSource.src_cat_link, ParentID = Parent.SourceId };
                                MigrationaParentHelper.Add(CreatedParent);
                            }
                            else
                            {
                                return;
                            }
                        }
                    }
                    var source = new Source()
                    {
                        FranchiseId = this.Franchise.FranchiseId,
                        Name = customSource.src_name,
                        ParentId = (MigrationaParentHelper.Count() > 0) ? (short?)CreatedParent.ParentID : Parent.SourceId,
                        Description = customSource.src_name,
                        CreatedOn = DateTime.Now
                    };
                    db.Sources.Add(source);
                    SaveChanges(db);
                    if (source.ParentId != null)
                    {
                        MigrationSources.Add(new SourceDTO() {CategoryID= (int?)customSource.src_cat_link,  list_sourceId = customSource.id, ParentId = source.ParentId });

                        string SQL = String.Format("INSERT INTO [TLMigrationTracker].[dbo].[SourceMap] ([CategoryID],[List_sourceId],[ParentId],[FranchiseId]) VALUES ({0},{1},{2},{3})",
                               customSource.src_cat_link, customSource.id, source.ParentId, this.Franchise.FranchiseId);
                        db.Database.ExecuteSqlCommand(SQL);
                    }
                    else
                    {
                        string unhandledDataScenarior = "Unhandled Data Scenarior Exists";
                    }
                }
                catch (Exception ex)
                {
                    var err = ex.InnerException;
                }
                finally
                {
                    Debug.WriteLine("customSourceCreate " + customSource.src_name + " Time Complteted:" + DateTime.Now.ToString());
                }
            }
        }
        private void customStatusCreate(list_jobstatus customStatus)
        {
            if (customStatus.job_status_fatherid > 0 && !string.IsNullOrWhiteSpace(customStatus.job_status))
            {
                var _status =
                    (MigrationStatuses.FirstOrDefault(w => w.job_status_pos == customStatus.id || w.job_status == customStatus.id) != null) ?
                        MigrationStatuses.FirstOrDefault(w => w.job_status_pos == customStatus.id || w.job_status == customStatus.id) :
                        MigrationStatuses.FirstOrDefault(w => w.job_status == customStatus.job_status_fatherid);

                if (_status != null && _status.JobStatusId.HasValue && _status.JobStatusId.Value > 0)
                {
                    try
                    {
                        var _jobStatus = JobStatus.FirstOrDefault(d => d.Id == _status.JobStatusId);
                        int? _JobParentID = null;
                        if (_jobStatus != null) _JobParentID= ( _jobStatus.ParentId == null) ? _jobStatus.Id : _jobStatus.ParentId;

                        var tJobStatus = new Type_JobStatus()
                            {
                                FranchiseId = this.Franchise.FranchiseId,
                                Name = customStatus.job_status,
                                ParentId = _JobParentID
                            };

                        var _leadStatus = LeadStatus.FirstOrDefault(d => d.Id == _status.LeadStatusId);
                        short? _LeadParentId =null;
                        if (_leadStatus != null) _LeadParentId = (_leadStatus.ParentId == null) ? _leadStatus.Id : _leadStatus.ParentId;
                        var tLeadStatus = new Type_LeadStatus()
                        {
                            FranchiseId = this.Franchise.FranchiseId,
                            Name = customStatus.job_status,
                            ParentId = _LeadParentId
                        };
                        persistCustomStatus(customStatus, tJobStatus, tLeadStatus);
                    }
                    catch (Exception ex)
                    {
                        var err = ex.InnerException;
                    }
                    finally
                    {
                        Debug.WriteLine("customStatusCreate " + customStatus.job_status + " Time Complteted:" + DateTime.Now.ToString());
                    }

                }
            }
        }
        private void persistCustomStatus(list_jobstatus sourceStatus, Type_JobStatus tJobStatus, Type_LeadStatus tLeadStatus)
        {
            db.Type_JobStatus.Add(tJobStatus);
            db.Type_LeadStatus.Add(tLeadStatus);
            try
            {
                SaveChanges(db);
                MigrationStatuses.Add(new StatusDTO() { job_status = sourceStatus.id, job_status_pos = sourceStatus.id, JobStatusId = tJobStatus.Id, LeadStatusId = tLeadStatus.Id });

                string SQL = String.Format("INSERT INTO [TLMigrationTracker].[dbo].[list_jobstatus] ([job_status],[job_status_pos],[status],[LeadStatusId] ,[JobStatusId],[FranchiseId]) VALUES ({0},{1},'{2}',{3},{4},{5})",
                    sourceStatus.id, sourceStatus.id, tJobStatus.Name.Replace("'",""), tLeadStatus.Id, tJobStatus.Id, this.Franchise.FranchiseId);
                db.Database.ExecuteSqlCommand(SQL);
            }
            catch (Exception ex)
            {
                var err = ex.InnerException;
            }
            finally
            {
                Debug.WriteLine("persistCustomStatus " + sourceStatus.job_status +" Time Complteted:" +DateTime.Now.ToString() );
            }

        }
        public void MigrateFranchise()
        {
            try
            {
                this.MigrateUsers();
                this.MigrateJobs();
                this.MigrateAppointments();
                this.MigrateInvoices();
                this.MigratePayments();
                this.MigrateFiles();
            }
            catch (Exception ex)
            {
                var err = ex.InnerException;
            }
            finally
            {
                Debug.WriteLine("MigrateFranchise: " + this.Franchise.FranchiseId.ToString()+ "Time Complteted:" + DateTime.Now.ToString());
            }
        }
        public void MigrateUsers()
        {
            List<HFC.CRM.Managers.Maestro.user> migratedUsers = maestroDB.users.Where(w => w.id != 113).Where(w2 => w2.usr_activeflag != "FALSE").ToList();
            int colorId = 1; //// THIS IS HUGE
            List<User> _Users = new List<Data.User>();
            try
            {
                migratedUsers.ForEach(u =>
                {
                    var _user = ToUser(u, ref colorId, this.Franchise.FranchiseId);
                    if (_user != null)
                    {
                        //_Users.Add(_user);
                        db.Users.Add(_user);
                        SaveChanges(db);
                    }

                });
            }
            catch (Exception ex)
            {
                var err = ex.InnerException;
            }
            finally
            {
                Debug.WriteLine("MigrateUsers " + _Users.Count() + " Time Complteted:" + DateTime.Now.ToString());
            }
        }
        public void MigrateAppointments()
        {
            List<appointment> appointments = maestroDB.appointments.ToList();
            getMigrated_Jobs();
            getMigrated_Users();
            try
            {
                List<Calendar> Calendars;
                 Calendars = (from a in appointments
                                            join j in Jobs on a.apt_jobid equals j.JobNumber
                                            join u in Users on a.apt_usrid.ToString() equals u.Person.Notes
                                            select new Calendar()
                                            {
                                                CalendarGuid = Guid.NewGuid(),
                                                FranchiseId = this.Franchise.FranchiseId,
                                                CreatedByPersonId = u.PersonId,
                                                LastUpdatedByPersonId = u.PersonId,
                                                AptTypeEnum = (typeof(AppointmentTypeEnum).GetType() == ((AppointmentTypeEnum)a.apt_atpid).GetType())
                                                   ? (AppointmentTypeEnum)a.apt_atpid
                                                   : AppointmentTypeEnum.Appointment,
                                                Subject = !string.IsNullOrEmpty(a.apt_note) ? a.apt_note.Substring(0, Math.Min(a.apt_note.Length, 255)) : "Appointment",
                                                JobId = j.JobId,
                                                JobNumber = j.JobNumber,
                                                LeadNumber = j.Lead.LeadNumber,
                                                LeadId = j.Lead.LeadId,
                                                Organizer = u.Person,
                                                StartDate = (DateTime)a.apt_time_start,
                                                EndDate = ((DateTime)a.apt_time_start).AddHours((double)a.apt_duration)
                                            }).ToList();
                //Calendars.ForEach(cal => helperEventToPeople(cal));
                db.Calendars.AddRange(Calendars);
                SaveChanges(db);
                List<Calendar> CalendarsCreated = db.Calendars.Where(w => w.FranchiseId == this.Franchise.FranchiseId).ToList();
                List<EventToPerson> EventsToPeople = CalendarsCreated.Select(s => new EventToPerson()
                {
                    CalendarId = s.CalendarId,
                    PersonId = s.CreatedByPersonId,
                    PersonEmail = s.CreatedByPerson.PrimaryEmail,
                    PersonName = s.CreatedByPerson.FirstName + " " + s.CreatedByPerson.LastName,
                    CreatedOnUtc = s.StartDate.DateTime
                }).ToList();

                db.EventToPeople.AddRange(EventsToPeople);
                SaveChanges(db);
            }
            catch (Exception e)
            {
                var err = e.InnerException;
            }
            finally
            {
                Debug.WriteLine("MigrateAppointments " + appointments.Count() + " Time Complteted:" + DateTime.Now.ToString());
            }
        }
        public void MigrateJobs()
        {
            try
            {
                migrateStatuses();
                migrateSources();
                MaeJobs = maestroDB.jobs.ToList();
                MaeJobProducts = getMigrationProducts();
                MaeCustomers = maestroDB.customers.ToList();
                getMigrated_Users();
                var _LeadsToBe = MaeJobs.GroupBy(g => g.job_cstid);
                foreach (var mjob in _LeadsToBe)
                {
                    processMigratedJob(mjob);
                }
                SaveChanges(db);
            }
            catch (Exception ex)
            {
                var err = ex.InnerException;
            }
            finally
            {
                Debug.WriteLine("MigrateJobs " + MaeJobs.Count().ToString() + " Time Complteted: " + DateTime.Now.ToString());
            }
        }
        public void MigrateInvoices()
        {
            List<invoice> Invoices;
            List<Invoice> InvoicesToMigrate= new List<Invoice>();
            try
            {
                getMigrated_Jobs();
                Invoices = maestroDB.invoices.ToList();
                Jobs.ForEach(job =>
                {
                    List<invoice> _JobsInvoices = Invoices.Where(w => w.inv_jobid == job.JobNumber).ToList();
                    if (_JobsInvoices != null)
                        _JobsInvoices.ForEach(_Invoice =>
                        {
                            InvoicesToMigrate.Add(ToInvoice(_Invoice, job));
                        });                        
                });
                db.Invoices.AddRange(InvoicesToMigrate.Where(i=>i.Amount>0).ToList() );
                SaveChanges(db);
            }
            catch (Exception ex)
            {
                var err = ex.InnerException;
            }
            finally
            {
                Debug.WriteLine("MigrateInvoices " + InvoicesToMigrate.Count() + " Time Complteted:" + DateTime.Now.ToString());
            }
        }
        public void MigratePayments()
        {
            List<Payment> payments = new List<Payment>();
            try
            {
                getMigrated_Jobs();
                List<payment> Payments = maestroDB.payments.ToList();
                List<Invoice> Invoices = db.Invoices.Where(w => w.Job.Lead.FranchiseId == this.Franchise.FranchiseId).ToList();
                List<Payment> paymentsToMigrateWithInvoices = new List<Payment>();
                List<Payment> paymentsToMigrateWithNoInvoices = new List<Payment>();

                paymentsToMigrateWithInvoices = processInvoices(Payments.Where(r => r.pay_invid > 0).ToList(), Invoices);
                paymentsToMigrateWithNoInvoices = processPaymentsWithNoInvoice(Payments.Where(r => r.pay_invid == 0).ToList());

                payments = paymentsToMigrateWithInvoices;
                payments.AddRange(paymentsToMigrateWithNoInvoices);

                payments = payments.Where(p => p.Amount > 0).ToList().ToList();

                if (payments.Count() > 0)
                    db.Payments.AddRange(payments); 

                SaveChanges(db);

                string sql = "exec [CRM].[migPostStep_FixInvoices] @FranchiseId =" + this.Franchise.FranchiseId.ToString()+" ; ";
                db.Database.ExecuteSqlCommand(sql);
                sql = "exec [CRM].[migPostStep_DedupPayments] @FranchiseId =" + this.Franchise.FranchiseId.ToString() + " ; ";
                db.Database.ExecuteSqlCommand(sql);

            }
            catch (Exception ex)
            {
                var err = ex.InnerException;
            }
            finally
            {
                Debug.WriteLine("MigratePayments " + payments.Count() + " Time Complteted:" + DateTime.Now.ToString());
            }
        }
        public void MigrateFiles()
        {
            List<PhysicalFile> physicalFiles = new List<PhysicalFile>();
            try
            {
                var SourceFiles = maestroDB.filemanagers.ToList();
                SourceFiles.ForEach(file =>
                {
                    PhysicalFile _PhysicalFile = new PhysicalFile();

                   string FileName= getFileName(file.file_name);

                    _PhysicalFile.PhysicalFileGuid = Guid.NewGuid();
                    _PhysicalFile.FileName = FileName;
                    _PhysicalFile.MimeType = string.IsNullOrEmpty(file.file_jobid.ToString()) ? string.Empty : file.file_jobid.ToString();
                    _PhysicalFile.FileCategory = string.IsNullOrEmpty(file.file_categoryid.ToString()) ? string.Empty : file.file_categoryid.ToString();
                    _PhysicalFile.FileDescription = string.IsNullOrEmpty(file.file_description) ? string.Empty : file.file_description;
                    _PhysicalFile.UrlPath = string.IsNullOrEmpty(file.file_jobid.ToString()) ? string.Empty : string.Format(@"files/{0}/{1}/{2}", this.Franchise.Code, file.file_jobid.ToString(), FileName);
                    _PhysicalFile.IsRelativeUrlPath = true;

                    _PhysicalFile.CreatedOnUtc = DateTime.Now;
                    _PhysicalFile.FileSize = (file.file_filesize == null) ? 0 : file.file_filesize;

                    _PhysicalFile.Width = 0;
                    _PhysicalFile.Height = 0;
                    _PhysicalFile.ThumbUrl = string.Empty;
                    _PhysicalFile.LastUpdatedUtc = DateTime.Now;
                    _PhysicalFile.LastUpdatedByPersonId = 1;
                    _PhysicalFile.AddedByPersonId = 1;
                    physicalFiles.Add(_PhysicalFile);
                });
                db.PhysicalFiles.AddRange(physicalFiles);
                if (SourceFiles.Count() > 0)
                {
                    SaveChanges(db);
                    string SQL = "[CRM].[migPostStep_FixJobFiles] @FranchiseId=" + this.Franchise.FranchiseId.ToString();
                    db.Database.ExecuteSqlCommand(SQL);

                    var DownloadMultiple = DownloadMultipleFilesAsync(SourceFiles);
                    DownloadMultiple.Wait();
                }
            }
            catch (Exception ex)
            {
                var err = ex.InnerException;
            }
            finally
            {
                Debug.WriteLine("MigrateFiles " + physicalFiles.Count() + " Time Complteted:" + DateTime.Now.ToString());
            }
        }
        private static Address validateAddress(user model, Address _Address)
        {
            try
            {
                if (!string.IsNullOrEmpty(model.usr_zipcode) && !string.IsNullOrEmpty(model.usr_address1))
                {

                    _Address = new Address()
                    {
                        Address1 = model.usr_address1,
                        Address2 = model.usr_address2,
                        City = model.usr_city,
                        State = model.usr_state,
                        ZipCode = model.usr_zipcode.ToUpper(),
                        CreatedOnUtc = DateTime.Now,
                        IsDeleted = false,
                        IsResidential = true,
                    };
                }
                else { return null; }
            }
            catch (Exception ex)
            {
                var err = ex.InnerException;
            }
            return _Address;
        }
        private static Person validatePerson(user model, Person _Person)
        {
            try
            {
                if (!string.IsNullOrEmpty(model.usr_username)
                    && !string.IsNullOrEmpty(model.usr_firstname)
                    && RegexUtil.IsValidEmail(model.usr_username)
                    )
                {
                    var _email = (!string.IsNullOrEmpty(model.usr_email) ? model.usr_email : model.usr_username).Trim();
                    _Person = new Person()
                    {
                        FirstName = model.usr_firstname,
                        LastName = model.usr_lastname,
                        PreferredTFN = "H",
                        PrimaryEmail = (RegexUtil.IsValidEmail(_email)) ? _email : null,
                        HomePhone = model.usr_homephone,
                        CellPhone = model.usr_cellphone,
                        FaxPhone = model.usr_fax,
                        WorkPhone = model.usr_workphone,
                        IsDeleted = false,
                        CreatedOnUtc = DateTime.UtcNow,
                        Notes = model.id.ToString(),
                    };
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                var err = ex.InnerException;
            }
            return _Person;
        }
        private List<MigrationProduct> getMigrationProducts()
        {
            List<MigrationProduct> maejob_products = (from jp in maestroDB.job_product
                                                      join p in maestroDB.products on jp.prdid equals p.id
                                                      join m in maestroDB.list_materialtype on jp.matid equals m.id into lftjoin
                                                      from mat in lftjoin.DefaultIfEmpty()
                                                      select new MigrationProduct()
                                                      {
                                                          JobItem = jp,
                                                          Category = p,
                                                          Material = mat
                                                      }).ToList();
            return maejob_products;
        }
        private void getMigrated_Users()
        {
            if (Users == null)
            {
                Users = db.Users.Where(w => w.FranchiseId == this.Franchise.FranchiseId)
                     .Include(i => i.Person)
                     .ToList();
            }
        }
        private void getMigrated_Jobs()
        {
            if (Jobs == null)
            {
                Jobs = db.Jobs.Where(w => w.Lead.FranchiseId == this.Franchise.FranchiseId)
                        .Include(i => i.Lead)
                        .Include(i => i.JobQuotes)
                        .ToList();

            }
        }
        private string getFileName(string fp)
        {
            if (string.IsNullOrEmpty(fp))
            {
                var _pAr = fp.Split('/');
                return _pAr[_pAr.Count() - 1];
            }
            else { return string.Empty; }
        }
        private static decimal getTaxRate(job maestrojob)
        {
            if ((maestrojob.job_total_sale_price ?? 0) > 0 && (maestrojob.job_tax1 ?? 0) > 0)
            {
                return (maestrojob.job_tax1 ?? 0) / (maestrojob.job_total_sale_price ?? 0);
            }
            else return 0;
        }
        private async System.Threading.Tasks.Task DownloadFileAsync(filemanager sourcefile)
        {
            try
            {
                string downloadToDir = string.Format(@"{0}\{1}\{2}\", TargetPath, this.Franchise.Code, sourcefile.file_jobid.ToString());
                if (!Directory.Exists(downloadToDir)) Directory.CreateDirectory(downloadToDir);

                using (WebClient webClient = new WebClient())
                {
                    var fileName = sourcefile.file_name.Split('/');
                    string downloadTo = downloadToDir + fileName[fileName.Count() - 1];
                    string url = RepositoryPath + sourcefile.file_name;

                    await webClient.DownloadFileTaskAsync(new Uri(url), downloadTo);
                }
            }
            catch (Exception ex)
            {
                var err = ex.InnerException;
            }
        }
        private async System.Threading.Tasks.Task DownloadMultipleFilesAsync(List<filemanager> doclist)
        {
            await System.Threading.Tasks.Task.WhenAll((doclist.Select(doc => DownloadFileAsync(doc))));
        }
        private List<Payment> processPaymentsWithNoInvoice(List<payment> Payments)
        {
            try
            {
                List<Payment> retval = new List<Payment>();
                List<payment> payments = null;
                Payments.ForEach(_Payment =>
                {
                    Invoice Dynamic_Invoice = processBatchInvoice(_Payment);
                    payments = Payments.Where(w => w.pay_jobid == _Payment.pay_jobid).ToList();
                    if (payments != null && Dynamic_Invoice != null)
                    {
                        processPayment(retval, Dynamic_Invoice, payments);
                    }
                    else
                    {
                        var INVESTIGATE = payments;
                    }
                });
                return retval;
            }
            catch (Exception ex)
            {
                var err = ex.InnerException;
                throw ex;
            }

        }
        private List<Payment> processInvoices(List<payment> Payments, List<Invoice> Invoices)
        {
            try
            {
                List<Payment> retval = new List<Payment>();

                List<payment> payments = null;
                Invoices.ForEach(invoice =>
                {
                    payments = Payments.Where(w => w.pay_invid.ToString() == invoice.InvoiceNumber).ToList();
                    if (payments != null)
                    {
                        processPayment(retval, invoice, payments);
                    }
                });
                return retval;
            }
            catch (Exception ex)
            {
                var err = ex.InnerException;
                throw ex;
            }
        }
        public void processMigratedJob(IGrouping<int?, job> mjob)
        {
            Lead _leadCreated = new Lead();
            try
            {
                var customer_people = MaeCustomers.FirstOrDefault(f => f.id == mjob.Key);
                if (customer_people == null)
                    return;

                var person = new Person
                {
                    FirstName = (customer_people.cst_firstname ?? "").Trim(),
                    LastName = (customer_people.cst_lastname ?? "").Trim(),
                    CompanyName = (customer_people.cst_company ?? "").Trim(),
                    CreatedOnUtc = (DateTime)customer_people.time_created,
                    WorkTitle = (customer_people.cst_title ?? "").Trim(),
                    PreferredTFN = "H"
                };

                var phone = (RegexUtil.GetNumbersOnly(customer_people.cst_homephone) ?? "").TrimStart('1');
                if (phone.Length >= 10)
                    person.HomePhone = phone.Substring(0, 10);

                phone = (RegexUtil.GetNumbersOnly(customer_people.cst_workphone) ?? "").TrimStart('1');
                if (phone.Length >= 10)
                    person.WorkPhone = phone.Substring(0, 10);

                phone = (RegexUtil.GetNumbersOnly(customer_people.cst_cellphone) ?? "").TrimStart('1');
                if (phone.Length >= 10)
                {
                    person.CellPhone = phone.Substring(0, 10);
                    person.PreferredTFN = "C";
                }

                phone = (RegexUtil.GetNumbersOnly(customer_people.cst_fax) ?? "").TrimStart('1');
                if (phone.Length >= 10)
                    person.FaxPhone = phone.Substring(0, 10);

                if (!string.IsNullOrEmpty(customer_people.cst_email))
                    person.PrimaryEmail = RegexUtil.IsValidEmail(customer_people.cst_email) ? customer_people.cst_email.Trim() : null;
                if (!string.IsNullOrEmpty(customer_people.cst_email2))
                    person.SecondaryEmail = RegexUtil.IsValidEmail(customer_people.cst_email2) ? customer_people.cst_email2.Trim() : null;

                if (string.Equals(customer_people.cst_mailinglist, "false", StringComparison.InvariantCultureIgnoreCase))
                    person.UnsubscribedOnUtc = customer_people.time_updated;

                var firstJob = mjob.FirstOrDefault();
                Guid leadGuid = Guid.Empty;
                if (!string.IsNullOrEmpty(customer_people.cst_globalid))
                    Guid.TryParse(customer_people.cst_globalid, out leadGuid);
                if (leadGuid == Guid.Empty)
                    leadGuid = Guid.NewGuid();

                /// This will work well if statuses persisted correctly;
                var maeleadStatus = MigrationStatuses.FirstOrDefault(f => f.job_status == ((firstJob.job_status != null) ? firstJob.job_status : firstJob.job_status_pos));

                int leadStatusId = AppConfigManager.NewLeadStatusId;
                if (maeleadStatus != null && maeleadStatus.LeadStatusId.HasValue && maeleadStatus.LeadStatusId.Value > 0)
                    leadStatusId = maeleadStatus.LeadStatusId.Value;

                _leadCreated = new Lead
                {
                    FranchiseId = Franchise.FranchiseId,
                    LeadNumber = customer_people.id,
                    CreatedOnUtc =(DateTime)firstJob.time_created,

                    LastUpdatedOnUtc = customer_people.time_updated,
                    LeadGuid = leadGuid,
                    LeadStatusId = (short)leadStatusId,
                    PrimPerson = person,
                    Addresses = new List<Address>(),
                    LeadSources = new List<LeadSource>(),
                    Jobs = new List<Job>()
                };

                var billingAddress = new Address
                {
                    Address1 = (customer_people.cst_address1 ?? "").Trim(),
                    Address2 = (customer_people.cst_address2 ?? "").Trim(),
                    City = (customer_people.cst_city ?? "").Trim(),
                    State = (customer_people.cst_state ?? "").Trim(),
                    ZipCode = (customer_people.cst_zip ?? "").Trim(),
                    CountryCode2Digits = Franchise.CountryCode ?? "US"
                };

                int product_creditId = 0, product_discountid = 0;
                foreach (var maestrojob in mjob)
                {
                    processJobLineItems(MaeJobProducts, person, _leadCreated, billingAddress, product_creditId, product_discountid, maestrojob);
                }
                db.Leads.Add(_leadCreated);
            }
            catch (Exception ex)
            {
                var err = ex.InnerException;
                Debug.WriteLine("Exception @ processMigratedJob " + ex.InnerException + " Time Complteted:" + DateTime.Now.ToString());
            }
            finally
            {
                Debug.WriteLine("processMigratedJob " + ((_leadCreated.PrimPerson == null) ? "NoLead" : _leadCreated.PrimPerson.FirstName) + " Time Complteted:" + DateTime.Now.ToString());
            }

        }
        private void processJobLineItems(List<MigrationProduct> maejob_products, Person person, Lead lead, Address billingAddress, int product_creditId, int product_discountid, job maestrojob)
        {
            Job job = new Job();
            try
            {
                // var ss=(maestrojob.job_sourceid2 ==0)? 
                var maesource = MigrationSources.FirstOrDefault(f => f.list_sourceId == maestrojob.job_sourceid1 || f.list_sourceId == maestrojob.job_sourceid2 );

                if (maesource != null)
                {

                    if (maesource.list_sourceId.HasValue && maesource.list_sourceId.Value > 0)
                        lead.LeadSources.Add(new LeadSource { SourceId = (short)maesource.ParentId.Value, IsManuallyAdded = true });
                }

                var jobAddress = new Address
                {
                    Address1 = (maestrojob.job_address1 ?? "").Trim(),
                    Address2 = (maestrojob.job_address2 ?? "").Trim(),
                    City = (maestrojob.job_city ?? "").Trim(),
                    State = (maestrojob.job_state ?? "").Trim(),
                    ZipCode = (maestrojob.job_zip ?? "").Trim(),
                    CountryCode2Digits = Franchise.CountryCode ?? "US"
                };
                var job_items = (from p in maejob_products
                                 where p.JobItem.jobid == maestrojob.id && p.JobItem.matid != product_creditId && p.JobItem.matid != product_discountid
                                 select new JobItem
                                 {
                                     CategoryName = (p.Category.prd_product ?? ""),
                                     ProductType = (p.Material != null ? p.Material.mat_material : null),
                                     Quantity = (short)(p.JobItem.quantity ?? 1),
                                     CreatedOnUtc = p.JobItem.time_created,
                                     LastUpdated = p.JobItem.time_updated,
                                     IsTaxable = (getTaxRate(maestrojob) > 0) ? true : false,
                                     UnitCost = p.JobItem.quantity > 0 ? ((p.JobItem.white ?? 0) / (p.JobItem.quantity ?? 1)) : 0,
                                     SalePrice = p.JobItem.quantity > 0 ? ((p.JobItem.price ?? 0) / (p.JobItem.quantity ?? 1)) : 0,
                                     ListPrice = p.JobItem.quantity > 0 ? ((p.JobItem.value ?? 0) / (p.JobItem.quantity ?? 1)) : 0,
                                     Subtotal = p.JobItem.price ?? 0
                                 }).ToList();

                var discount = maejob_products.Where(
                    w => 
                         w.JobItem.jobid == maestrojob.id 
                        && w.Category.id == product_discountid)
                    .Sum(s => s.JobItem.price ?? 0);

                var credit = maejob_products.Where(w => w.JobItem.jobid == maestrojob.id && w.Category.id == product_creditId).Sum(s => s.JobItem.price ?? 0);
                Guid jobGuid = Guid.Empty;
                Guid.TryParse(maestrojob.job_globalid, out jobGuid);
                if (jobGuid == Guid.Empty)
                    jobGuid = Guid.NewGuid();

                var jobstatus = MigrationStatuses.FirstOrDefault(f => f.job_status == maestrojob.job_status_pos);

                int jobStatusId = AppConfigManager.NewJobStatusId;
                if (jobstatus != null && jobstatus.JobStatusId.HasValue && jobstatus.JobStatusId.Value > 0)
                    jobStatusId = jobstatus.JobStatusId.Value;

                int? salesPersonId = null;
                if (maestrojob.job_usrid.HasValue)
                {
                    var salesPerson = Users.FirstOrDefault(f => f.Comment == maestrojob.job_usrid.ToString());
                    if (salesPerson != null)
                        salesPersonId = salesPerson.PersonId;
                }

                    job.JobConceptId = 1;
                    job.JobGuid = jobGuid;
                    job.SourceId = (lead.LeadSources.Count()>0)? (short?)lead.LeadSources.FirstOrDefault().SourceId:null ;
                    job.JobStatusId = jobStatusId;
                    job.ContractedOnUtc = maestrojob.job_contract_date;
                    job.CreatedOnUtc = (DateTime)maestrojob.time_created;
                    job.LastUpdated = maestrojob.time_updated;
                    job.BillingAddress = jobAddress.Equals(billingAddress) ? billingAddress : jobAddress;
                    job.InstallAddress = jobAddress;
                    job.SalesPersonId = salesPersonId;
                    job.CompletedOnUtc = string.Equals(maestrojob.job_complete, "true", StringComparison.InvariantCultureIgnoreCase) ? (DateTime?)maestrojob.time_updated : null;
                    job.JobNumber = maestrojob.id;
                    job.Customer = person;
                    job.JobQuotes = new List<JobQuote>();
                
                var quote = new JobQuote
                {
                    QuoteNumber = 1,
                    IsPrimary = true,
                    JobItems = job_items,
                    Subtotal = job_items.Sum(s => s.Subtotal),
                    TaxPercent = getTaxRate(maestrojob),
                    DiscountTotal = discount + credit,
                    Taxtotal = maestrojob.job_tax1 ?? 0,
                    NetTotal = maestrojob.job_total_sale_price ?? 0,
                    NetProfit = job_items.Sum(s => s.Quantity * s.UnitCost),
                    SaleAdjustments = new List<JobSaleAdjustment>()
                };
                if (discount > 0)
                {
                    var adj = new JobSaleAdjustment
                    {
                        Amount = discount,
                        TypeEnum = SaleAdjTypeEnum.Discount
                    };
                    quote.SaleAdjustments.Add(adj);
                }
                if (maestrojob.job_tax1.HasValue && maestrojob.job_tax1.Value > 0)
                {
                    var adj = new JobSaleAdjustment
                    {
                        TypeEnum = SaleAdjTypeEnum.Tax,
                        Percent = getTaxRate(maestrojob) * 100,
                        IsTaxable = (getTaxRate(maestrojob) > 0) ? true : false,
                    };
                    quote.SaleAdjustments.Add(adj);
                }
                if (credit > 0)
                {
                    var adj = new JobSaleAdjustment
                    {
                        Amount = credit,
                        TypeEnum = SaleAdjTypeEnum.Credit
                    };
                    quote.SaleAdjustments.Add(adj);
                }
                
                job.JobQuotes.Add(quote);
                if (billingAddress!=null ) lead.Addresses.Add(billingAddress);
                if (jobAddress != null) lead.Addresses.Add(jobAddress);
                lead.Jobs.Add(job);
            }
            catch (Exception ex)
            {
                var err = ex.InnerException;
                Debug.WriteLine("Exception @ processJobLineItems " + ex.InnerException + " Time Complteted:" + DateTime.Now.ToString());
            }
            finally
            {
                Debug.WriteLine("processJobLineItems JOB.CreatedOnUtc: " + ( (job.CreatedOnUtc.ToString() != null)?job.CreatedOnUtc.ToString():"noJobData" ) + " Time Complteted:" + DateTime.Now.ToString());
            }
        }
        private Invoice processBatchInvoice(payment payment)
        {
            var batchedInvoice = db.Invoices.Where(w => w.Job.Lead.FranchiseId == this.Franchise.FranchiseId).FirstOrDefault(w => w.Job.JobNumber == payment.pay_jobid);
            if (batchedInvoice != null)
            {
                return batchedInvoice;
            }
            else
            {
                var quote = db.JobQuotes.Where(w => w.Job.Lead.FranchiseId == this.Franchise.FranchiseId).Where(w => w.IsPrimary == true)
                    .FirstOrDefault(w => w.Job.JobNumber == payment.pay_jobid);
                if (quote != null)
                {
                    Invoice Dynamic_Invoice = InvoiceManager.BillQuote(quote.QuoteId, quote.JobId);
                    if (Dynamic_Invoice != null)
                    {
                        Dynamic_Invoice.InvoiceNumber = Dynamic_Invoice.InvoiceId.ToString();
                        return Dynamic_Invoice;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
        }
        private void processPayment(List<Payment> PaymentsToMigrate, Invoice invoice, List<payment> _payments)
        {
            var MigPayments = ToPayment(_payments, invoice);
            if (invoice == null) {
                string INVESTIGATE = "INVESTIGATE";
            }
            if (invoice!=null && MigPayments.Count > 0)
            {
                PaymentsToMigrate.AddRange(MigPayments);
            }
        }
        private User ToUser(user model, ref int colorId, int franchiseId)
        {
            try
            {
                Person _Person = new Person();
                Address _Address = new Address();

                _Person = validatePerson(model, _Person);
                _Address = validateAddress(model, _Address);
                if (_Person != null && !string.IsNullOrEmpty(_Person.PrimaryEmail))
                {
                    var _email = _Person.PrimaryEmail;
                    User _user = new User()
                    {
                        UserId = Guid.NewGuid(),
                        UserName = model.usr_username,
                        ColorId = colorId,
                        Email = RegexUtil.IsValidEmail(_email.Trim()) ? _email.Trim() : null
                                                ?? ((model.usr_username.Trim().Contains('@')) ? model.usr_username.Trim() : null),
                        FranchiseId = franchiseId,
                        IsApproved = true,
                        IsDeleted = false,
                        IsLockedOut = false,
                        CalendarFirstHour = 0,
                        FailedPasswordAttemptCount = 0,
                        CreationDateUtc = DateTime.Now,
                        Password = HFC.CRM.Core.Membership.LocalMembership.Encrypt(@"password"),
                        SyncStartsOnUtc = DateTime.Now,
                        Comment = model.id.ToString(),
                        Roles = new List<Role>() { 
                            //AppConfigManager.DefaultOperatorRole,
                            AppConfigManager.DefaultSalesRole, 
                            AppConfigManager.DefaultInstallRole },
                        Person = _Person,
                    };

                    if (!string.IsNullOrEmpty(model.usr_address1))
                        _user.Address = _Address;
                    foreach (var role in _user.Roles)
                    {
                        db.Roles.Attach(role);
                    }
                    colorId = colorId + 1;
                    return _user;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                var err = ex.InnerException;
                throw ex;
            }
        }
        private List<Payment> ToPayment(List<payment> payments, Invoice invoice)
        {
            List<Payment> retval = new List<Payment>();
            payments.ForEach(p =>
                {
                    if (invoice != null)
                    {
                        try
                        {
                            Payment payment = new Payment();
                                payment.PaymentGuid = Guid.NewGuid();
                                payment.InvoiceId = invoice.InvoiceId;
                                payment.Amount = (decimal)p.pay_amount;
                                payment.CreatedOn = (DateTime)p.time_created;
                                //string memo = !string.IsNullOrEmpty(p.pay_accountnum) ? (p.pay_accountnum.Trim()) : string.Empty;
                                //    memo =memo + (string.IsNullOrEmpty(p.pay_expiration) ? (p.pay_expiration.Trim()) : string.Empty);
                                //    memo = memo + (string.IsNullOrEmpty(p.pay_comment) ? (p.pay_comment.Trim()) : string.Empty);
                                //string.IsNullOrEmpty(memo) ? memo : string.Empty;
                                payment.Memo = (!string.IsNullOrEmpty(p.pay_accountnum) ? (p.pay_accountnum.Trim()) : string.Empty) +
                                (!string.IsNullOrEmpty(p.pay_expiration) ? (p.pay_expiration.Trim()) : string.Empty) +
                                (!string.IsNullOrEmpty(p.pay_comment) ? (p.pay_comment.Trim()) : string.Empty);
                                payment.ReferenceNum = p.pay_approvalnum;
                                payment.PaymentDate = (DateTime)p.time_created;
                                payment.PaymentMethod = ((PaymentMethod)p.pay_pmtid).ToString();
                            retval.Add(payment);
                        }
                        catch (Exception ex)
                        {
                            var err = ex.InnerException;
                        }
                        finally
                        {
                            Debug.WriteLine("ToPayment " + payments.Count().ToString() + "of invoice.JobId: " + invoice.JobId + " > Complteted:" + DateTime.Now.ToString());
                        }
                    }
                });
            return retval;
        }
        private Invoice ToInvoice(invoice invoice, Job job)
        {
            Invoice retval = new Invoice();
            try
            {
                retval = new Invoice()
                {
                    InvoiceGuid = Guid.NewGuid(),
                    JobId = job.JobId,
                    InvoiceNumber = invoice.id.ToString(),
                    QuoteId = job.JobQuotes.FirstOrDefault(f => f.IsPrimary == true).QuoteId,
                    LastUpdated = DateTime.Now,
                    CreatedOn = (DateTime)invoice.inv_date,
                    CreatorPersonId = job.CreatedByPersonId,
                    LastUpdatedPersonId = job.CreatedByPersonId,
                    IsDeleted = false,
                    Amount = (decimal)((invoice.inv_amount == null) ? 0 : invoice.inv_amount) + /// Only possible with Migrated data. New invoices will reffer to job for tax info. There are $0.0 total invoices, cs you can bill $0 job
                    + ((invoice.inv_taxamount1 == null) ? 0 : (decimal)invoice.inv_taxamount1)
                    + ((invoice.inv_taxamount2 == null) ? 0 : (decimal)invoice.inv_taxamount2)
                };
            }
            catch (Exception ex)
            {
                var err = ex.InnerException;
            }
            finally
            {
                Debug.WriteLine("ToInvoice InvoiceID#" + invoice.id.ToString()+"of Job.JobNumber: " +job.JobNumber+ " > Complteted:" + DateTime.Now.ToString());
            }
            return retval;
        }
        public DateTime ParseDate(string date)
        {
            DateTime dateObj = Franchise.CreatedOnUtc;
            if (!string.IsNullOrEmpty(date))
                DateTime.TryParse(date, out dateObj);
            return dateObj;
        }

        
    }
}

