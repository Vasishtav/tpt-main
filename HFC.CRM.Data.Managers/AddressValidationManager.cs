﻿using RestSharp;
using System.Configuration;
using Newtonsoft.Json;
using HFC.CRM.DTO;
using AddressValidationWebServiceClient;

namespace HFC.CRM.Managers
{
    public class AddressValidationManager
    {
        public FEDExResponse AddressValidation(FEDExRequest req)
        {
            return FEDExService.AddressValidation(req);
        }

        public SmartyStreetResponse SmartyStreetSuggession(string prefix, string prefer)
        {
            string url = ConfigurationManager.AppSettings["SmartystreetAPI"].ToString();
            string authid = ConfigurationManager.AppSettings["Smartystreetauthid"].ToString();
            string authtoken = ConfigurationManager.AppSettings["Smartystreetauthtoken"].ToString();
            var api = url + "?auth-id=" + authid + "&auth-token=" + authtoken + "&prefix=" + prefix + "&prefer_ratio=0.6&geolocate=false&prefer=" + prefer + "";
            var client = new RestClient(api);
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            return JsonConvert.DeserializeObject<SmartyStreetResponse>(response.Content);
        }

    }
}
