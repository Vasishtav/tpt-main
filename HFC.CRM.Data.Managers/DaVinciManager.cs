﻿using Dapper;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using HFC.CRM.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace HFC.CRM.Managers
{
    public class DaVinciManager : DataManager
    {
        AccountsManager actMgr;
        QuotesManager quoteMgr;
        public DaVinciManager(User user, Franchise franchise)
        {
            this.User = user;
            this.Franchise = franchise;
            actMgr = new AccountsManager(user, franchise);
            quoteMgr = new QuotesManager(user, franchise);
        }

        public List<DavinciOpportunityInfo> GetOpportunityList()
        {
            try
            {
                string query = @"select OpportunityId
	                            ,OpportunityName
	                            , isnull(ad.Address1,'')
	                            +''+case when ad.Address2!='' then ' '+ad.Address2 else '' end 
	                            +' '+ isnull(ad.City,'')+', '+isnull(ad.State,'')+' '+isnull(ad.CountryCode2Digits,'')+' '+isnull(ad.ZipCode,'') as InstallationAddress
	                            ,case when cus.PreferredTFN='H' then cus.HomePhone else case when cus.PreferredTFN='C' then cus.CellPhone else cus.CellPhone end end as Phone
	                            ,cus.PrimaryEmail as Email
	                            ,opps.Name as [Status]
	                            from CRM.Opportunities opp
	                            join CRM.Type_OpportunityStatus opps on opp.OpportunityStatusId=opps.OpportunityStatusId
	                            join CRM.Addresses ad on opp.InstallationAddressId=ad.AddressId
	                            join CRM.Accounts acc on opp.AccountId=acc.AccountId
	                            join CRM.AccountCustomers acccus on acc.AccountId=acccus.AccountId and acccus.IsPrimaryCustomer=1
	                            join CRM.Customer cus on acccus.CustomerId=cus.CustomerId
	                            where opp.FranchiseId=@FranchiseId";

                var data = ExecuteIEnumerableObject<DavinciOpportunityInfo>(ContextFactory.CrmConnectionString, query, new { FranchiseId = Franchise.FranchiseId }).ToList();

                return data;
            }
            catch (Exception)
            {
                throw;
            }


        }

        public List<Type_OpportunityStatus> GetOpportunityStatus()
        {
            string query = "select [OpportunityStatusId],[Name],[Description] from CRM.Type_OpportunityStatus";
            return ExecuteIEnumerableObject<Type_OpportunityStatus>(ContextFactory.CrmConnectionString, query).ToList();
        }

        public bool UpdateOpportunitiestatus(int OpportunityId, int OpportunityStatusId)
        {
            try
            {
                string query = @"update CRM.Opportunities set OpportunityStatusId=@OpportunityStatusId where OpportunityId=@OpportunityId";
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Execute(query, new { OpportunityId, OpportunityStatusId });
                }
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int CreateQuote(QuoteDavinci Quote)
        {
            try
            {
                int QuoteKey = 0;
                int QuoteLineId = 0;
                int QuoteId = actMgr.GetNextNumber(Franchise.FranchiseId, 2);//2 Quote Number
                string qQuote = @"insert into [CRM].[Quote]([QuoteID],[OpportunityId],[QuoteName],[PrimaryQuote],[QuoteStatusId],[CreatedOn],[CreatedBy],[BillofMaterial]) 
                                values (@QuoteID,@OpportunityId,@QuoteName,@PrimaryQuote,@QuoteStatusId,@CreatedOn,@CreatedBy,@BillofMaterial); SELECT CAST(SCOPE_IDENTITY() as int)";

                string qQuoteLine = @"INSERT INTO [CRM].[QuoteLines]
                                ([QuoteKey],[MeasurementsetId],[ProductId]
                                ,[CreatedOn],[CreatedBy],[IsActive]
                                ,[Width],[Height],[UnitPrice]
                                ,[Quantity],[Description],[PICJson]
                                ,[ProductTypeId],[Quntity],[ProductName]
                                ,[QuoteLineNumber],[ProductCategoryId]
                                ,[ProductSubCategoryId],[RoomName],[BillofMaterial])
                                VALUES
                                (@QuoteKey,@MeasurementsetId,@ProductId
                                ,@CreatedOn,@CreatedBy,@IsActive
                                ,@Width,@Height,@UnitPrice
                                ,@Quantity,@Description,@PIJson
                                ,@ProductTypeId,@Quntity,@ProductName
                                ,@QuoteLineNumber,@ProductCategoryId
                                ,@ProductSubCategoryId,@RoomName,@BillofMaterial); SELECT CAST(SCOPE_IDENTITY() as int)";

                string qQuoteFile = @"insert into [CRM].[QuoteFiles] values (@QuoteKey,@QuoteLineId,@stream_id)";

                var opportunity = GetOpportunity(Quote.OpportunityId);

                //using (var txscope = new TransactionScope(TransactionScopeOption.RequiresNew))
                //{
                using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    con.Open();
                    var transaction = con.BeginTransaction();
                    try
                    {
                        // Insert Quote
                        QuoteKey = con.Query<int>(qQuote, new
                        {
                            QuoteId,
                            OpportunityId = Quote.OpportunityId,
                            QuoteName = QuoteId + "-" + opportunity.OpportunityName,
                            PrimaryQuote = true,
                            QuoteStatusId = 1,
                            CreatedOn = DateTime.UtcNow,
                            CreatedBy = User.PersonId,
                            BillofMaterial = Quote.PartsDetail != null && Quote.PartsDetail.Length > 0 ? JsonConvert.SerializeObject(Quote.PartsDetail) : ""
                        }, transaction).Single();

                        // Insert D'vinci file
                        if (Quote.File != null && !string.IsNullOrEmpty(Quote.File.FileName) && Quote.File.File_Base64String != null && Quote.File.File_Base64String.Length > 0)
                        {
                            var file_stream_id = UploadFile(Quote.File);
                            con.Execute(qQuoteFile, new { QuoteKey, QuoteLineId = 0, stream_id = file_stream_id }, transaction);
                        }

                        // Quote Lines
                        if (Quote.QuoteLines != null)
                        {
                            foreach (var ql in Quote.QuoteLines)
                            {

                                PICJsonDavinci obj = new PICJsonDavinci()
                                {
                                    ProductName = ql.ProductName,
                                    PCategory = ql.ProductCategory,
                                    PicProduct = 10000,
                                    GGroup = 1000000,
                                    Quantity = ql.QTY
                                };

                                var ProductCategory = GetProductCategory(ql.ProductCategory);

                                // Insert Quote line
                                QuoteLineId = con.Query<int>(qQuoteLine, new
                                {
                                    QuoteKey,
                                    MeasurementsetId = 0,
                                    ProductId = 0,
                                    CreatedOn = DateTime.UtcNow,
                                    CreatedBy = User.PersonId,
                                    IsActive = true,
                                    Width = 0,
                                    Height = 0,
                                    UnitPrice = ql.UnitPrice,
                                    Quantity = ql.QTY,
                                    Description = ql.Description,
                                    PIJson = JsonConvert.SerializeObject(obj),
                                    ProductTypeId = 5,
                                    Quntity = 1,
                                    ProductName = ql.ProductName,
                                    QuoteLineNumber = 1,
                                    ProductCategoryId = ProductCategory != null ? ProductCategory.Parant : 0,
                                    ProductSubCategoryId = ProductCategory != null ? ProductCategory.ProductCategoryId : 0,
                                    RoomName = ql.RoomName,
                                    BillofMaterial = ql.PartsDetail != null && ql.PartsDetail.Length > 0 ? JsonConvert.SerializeObject(ql.PartsDetail) : ""
                                }, transaction).Single();

                                var qDetails = new QuoteLineDetail();
                                qDetails.QuoteLineId = QuoteLineId;
                                qDetails.CreatedBy = User.PersonId;
                                qDetails.CreatedOn = DateTime.UtcNow;
                                con.Insert<QuoteLineDetail>(qDetails, transaction);

                                if (ql.image != null && ql.image.Count > 0)
                                {
                                    // Insert list of images to the Quote line
                                    foreach (var file in ql.image)
                                    {
                                        if (!string.IsNullOrEmpty(file.FileName) && file.File_Base64String != null && file.File_Base64String.Length > 0)
                                        {
                                            var image_stream_id = UploadFile(file);
                                            con.Execute(qQuoteFile, new { QuoteKey, QuoteLineId = QuoteLineId, stream_id = image_stream_id }, transaction);
                                        }
                                    }
                                }
                            }
                        }
                        transaction.Commit();
                        con.Close();
                        quoteMgr.AddUpdateInsertQuoteLineNumber(QuoteKey);
                        quoteMgr.UpdatePrimaryQuote(Quote.OpportunityId, QuoteKey);
                        quoteMgr.RecalculateQuoteLines(QuoteKey);


                        //txscope.Complete();
                        //txscope.Dispose();

                        return QuoteKey;
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        //txscope.Dispose();
                        throw;
                    }
                    //}
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public bool UpdateQuote(int QuoteKey, QuoteDavinci Quote)
        {
            try
            {
                int QuoteLineId = 0;

                // Quote header level
                string qQuoteupdate = "update CRM.Quote set BillofMaterial=@BillofMaterial where QuoteKey=@QuoteKey";

                // Quote line level delete insert
                string qQuoteLineDel = @"delete crm.QuoteCoreProductDetails where QuoteLineId in (select QuoteLineId from [CRM].[QuoteLines] where QuoteKey=@QuoteKey and ProductTypeId=5);
                                         delete crm.DiscountsAndPromo where QuoteLineId in (select QuoteLineId from [CRM].[QuoteLines] where QuoteKey=@QuoteKey and ProductTypeId=5); 
                                         delete [CRM].[QuoteLineDetail] where QuoteLineId in (select QuoteLineId from [CRM].[QuoteLines] where QuoteKey=@QuoteKey and ProductTypeId=5); 
                                         delete [CRM].[QuoteLines] where QuoteLineId in (select QuoteLineId from [CRM].[QuoteLines] where QuoteKey=@QuoteKey and ProductTypeId=5);";

                string qQuoteLine = @"INSERT INTO [CRM].[QuoteLines]
                                ([QuoteKey],[MeasurementsetId],[ProductId]
                                ,[CreatedOn],[CreatedBy],[IsActive]
                                ,[Width],[Height],[UnitPrice]
                                ,[Quantity],[Description],[PICJson]
                                ,[ProductTypeId],[Quntity],[ProductName]
                                ,[QuoteLineNumber],[ProductCategoryId]
                                ,[ProductSubCategoryId],[RoomName],[BillofMaterial])
                                VALUES
                                (@QuoteKey,@MeasurementsetId,@ProductId
                                ,@CreatedOn,@CreatedBy,@IsActive
                                ,@Width,@Height,@UnitPrice
                                ,@Quantity,@Description,@PIJson
                                ,@ProductTypeId,@Quntity,@ProductName
                                ,@QuoteLineNumber,@ProductCategoryId
                                ,@ProductSubCategoryId,@RoomName,@BillofMaterial); SELECT CAST(SCOPE_IDENTITY() as int)";

                // Delete quote header and line level file and insert
                string qQuoteFileDel = "Delete from [CRM].[QuoteFiles] where QuoteKey=@QuoteKey";
                string qQuoteFile = @"insert into [CRM].[QuoteFiles] values (@QuoteKey,@QuoteLineId,@stream_id)";
                //using (var txscope = new TransactionScope(TransactionScopeOption.RequiresNew))
                //{
                using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    con.Open();
                    var transaction = con.BeginTransaction();
                    try
                    {
                        // Update header level parts detail
                        con.Execute(qQuoteupdate, new
                        {
                            QuoteKey,
                            BillofMaterial = Quote.PartsDetail != null && Quote.PartsDetail.Length > 0 ? JsonConvert.SerializeObject(Quote.PartsDetail) : ""
                        }, transaction);

                        // Delete QuoteLines
                        con.QueryMultiple(qQuoteLineDel, new { QuoteKey }, transaction);

                        // Delete D'vinci and Image file
                        con.Execute(qQuoteFileDel, new { QuoteKey }, transaction);

                        // Insert D'vinci file
                        if (Quote.File != null && !string.IsNullOrEmpty(Quote.File.FileName) && Quote.File.File_Base64String != null && Quote.File.File_Base64String.Length > 0)
                        {
                            var file_stream_id = UploadFile(Quote.File);
                            con.Execute(qQuoteFile, new { QuoteKey, QuoteLineId = 0, stream_id = file_stream_id }, transaction);
                        }

                        // Quote Lines
                        if (Quote.QuoteLines != null)
                        {
                            foreach (var ql in Quote.QuoteLines)
                            {
                                PICJsonDavinci obj = new PICJsonDavinci()
                                {
                                    ProductName = ql.ProductName,
                                    PCategory = ql.ProductCategory,
                                    PicProduct = 10000,
                                    GGroup = 1000000,
                                    Quantity = ql.QTY
                                };

                                var ProductCategory = GetProductCategory(ql.ProductCategory);

                                // Insert Quote line
                                QuoteLineId = con.Query<int>(qQuoteLine, new
                                {
                                    QuoteKey,
                                    MeasurementsetId = 0,
                                    ProductId = 0,
                                    CreatedOn = DateTime.UtcNow,
                                    CreatedBy = User.PersonId,
                                    IsActive = true,
                                    Width = 0,
                                    Height = 0,
                                    UnitPrice = ql.UnitPrice,
                                    Quantity = ql.QTY,
                                    Description = ql.Description,
                                    PIJson = JsonConvert.SerializeObject(obj),
                                    ProductTypeId = 5,
                                    Quntity = 1,
                                    ProductName = ql.ProductName,
                                    QuoteLineNumber = 1,
                                    ProductCategoryId = ProductCategory != null ? ProductCategory.Parant : 0,
                                    ProductSubCategoryId = ProductCategory != null ? ProductCategory.ProductCategoryId : 0,
                                    RoomName = ql.RoomName,
                                    BillofMaterial = ql.PartsDetail != null && ql.PartsDetail.Length > 0 ? JsonConvert.SerializeObject(ql.PartsDetail) : ""
                                }, transaction).Single();

                                var qDetails = new QuoteLineDetail();
                                qDetails.QuoteLineId = QuoteLineId;
                                qDetails.CreatedBy = User.PersonId;
                                qDetails.CreatedOn = DateTime.UtcNow;
                                con.Insert<QuoteLineDetail>(qDetails, transaction);

                                if (ql.image != null && ql.image.Count > 0)
                                {
                                    // Insert list of images to the Quote line
                                    foreach (var file in ql.image)
                                    {
                                        if (!string.IsNullOrEmpty(file.FileName) && file.File_Base64String != null && file.File_Base64String.Length > 0)
                                        {
                                            var image_stream_id = UploadFile(file);
                                            con.Execute(qQuoteFile, new { QuoteKey, QuoteLineId = QuoteLineId, stream_id = image_stream_id }, transaction);
                                        }
                                    }
                                }
                            }
                        }
                        transaction.Commit();
                        con.Close();

                        quoteMgr.AddUpdateInsertQuoteLineNumber(QuoteKey);
                        quoteMgr.UpdatePrimaryQuote(Quote.OpportunityId, QuoteKey);
                        quoteMgr.RecalculateQuoteLines(QuoteKey);

                        //txscope.Complete();
                        //txscope.Dispose();

                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        //txscope.Dispose();
                        throw;
                    }
                }
                //}
                return true;
            }
            catch (Exception e)
            {
                throw;
            }

        }

        public Opportunity GetOpportunity(int id)
        {
            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                return con.Get<Opportunity>(id);
            }
        }

        public string UploadFile(CRM.DTO.FileDataDavinci file)
        {
            Random rnd = new Random();
            string value = rnd.Next(1, 9999).ToString();
            string fileName = DateTime.UtcNow.ToString("MMddyyyy") + "_" + DateTime.UtcNow.ToString("HHMMssfff") + value + "_" + file.FileName;

            string Query = "INSERT INTO FileTableTb (name, file_stream) OUTPUT INSERTED.[stream_id] values(@FileName, @stream)";

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var dParams = new DynamicParameters();
                dParams.Add("@FileName", fileName, DbType.AnsiString);
                dParams.Add("@stream", file.File_Base64String, DbType.Binary);

                var streamid = connection.ExecuteScalar(Query, dParams);

                return streamid.ToString();
            }
        }

        public Type_ProductCategory GetProductCategory(string productcategory)
        {
            string query = @"select ProductCategoryId from [CRM].[Type_ProductCategory] where ProductCategory=@ProductCategory and BrandId=2 and ProductTypeId=1";
            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                return con.Query<Type_ProductCategory>(query, new { ProductCategory = productcategory }).FirstOrDefault();

            }
        }

        public bool checkOpportunityIdExist(int OpportunityId, int FranchiseId)
        {
            string query = @"select OpportunityId from CRM.Opportunities where OpportunityId=@OpportunityId and FranchiseId=@FranchiseId";
            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var res = con.Query<Opportunity>(query, new { OpportunityId, FranchiseId }).FirstOrDefault();
                if (res != null)
                    return true;
                else return false;

            }
        }

        //public bool checkProductCategoryExist(string ProductCategory)
        //{
        //    string query = @"select ProductCategoryId from CRM.Type_ProductCategory where BrandId=2 and ProductTypeId=1 and ProductCategory=@ProductCategory";
        //    using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
        //    {
        //        var res = con.Query<Type_ProductCategory>(query, new { ProductCategory }).FirstOrDefault();
        //        if (res != null)
        //            return true;
        //        else return false;

        //    }
        //}

        //public bool UpdatePOstatus()
        //{
        //    try
        //    {
        //        string query = @"select * from CRM.PurchaseOrderDetails where CreatedOn <='2018-09-19' and StatusId=11";
        //        using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
        //        {
        //            var data = connection.Query<PurchaseOrderDetails>(query).ToList();

        //            foreach (var d in data)
        //            {
        //                var poRes = JsonConvert.DeserializeObject<dynamic>(d.POResponse);

        //                if (poRes != null && poRes.valid == true && poRes.properties != null && poRes.properties.Items != null && poRes.properties.Items[0].dueDate != null)
        //                {
        //                    DateTime duedate = Convert.ToDateTime(poRes.properties.Items[0].dueDate);

        //                    string updateQ = "update CRM.PurchaseOrderDetails set ShipDate=@ShipDate, PromiseByDate=@PromiseByDate where PurchaseOrdersDetailId=@PurchaseOrdersDetailId";

        //                    connection.Execute(updateQ, new { ShipDate = duedate, PromiseByDate = duedate, PurchaseOrdersDetailId = d.PurchaseOrdersDetailId });
        //                }
        //            }


        //        }
        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}
    }
}
