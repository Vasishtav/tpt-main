﻿using Dapper;
using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HFC.CRM.Core.Logs;
using System.Data;
using Newtonsoft.Json;
using System.Dynamic;
using HFC.CRM.DTO.Order;
using HFC.CRM.DTO;
using Z.Dapper.Plus;
using HFC.CRM.Core.Extensions;
using HFC.CRM.DTO.Opportunity;

namespace HFC.CRM.Managers
{
    public class QuotesManager : DataManager<Quote>
    {
        private AccountsManager actMgr;
        private MeasurementManager MeasurementMgr;
        private OpportunitiesManager oppManager;
        private PICManager picManager = new PICManager();
        private FranchiseManager frnMgr;

        public QuotesManager()
        {
        }

        public QuotesManager(User user, Franchise franchise) : this(user, user, franchise)
        {
            this.User = user;
            this.Franchise = franchise;
        }

        public QuotesManager(User user, User authorizingUser, Franchise franchise)
        {
            this.User = user;
            this.AuthorizingUser = authorizingUser;
            this.Franchise = franchise;
            actMgr = new AccountsManager(user, franchise);
            MeasurementMgr = new MeasurementManager(user, franchise);
            oppManager = new OpportunitiesManager(user, franchise);
            frnMgr = new FranchiseManager(user);
        }

        //public override Permission BasePermission
        //{
        //    get
        //    {
        //        throw new NotImplementedException();
        //    }
        //}

        public bool CopyQuote(int quoteKey, bool copyTaxExempt)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                try
                {
                    var query = @"  INSERT INTO CRM.Quote (QuoteID ,OpportunityId ,QuoteName ,PrimaryQuote ,QuoteStatusId ,Discount ,DiscountType ,TotalDiscounts ,NetCharges ,CreatedOn ,CreatedBy ,LastUpdatedOn ,LastUpdatedBy ,ExpiryDate ,AdditionalCharges ,QuoteNumber ,TotalMargin ,ProductSubTotal ,QuoteSubTotal ,MeasurementId ,CoreProductMargin ,CoreProductMarginPercent ,MyProductMargin ,MyProductMarginPercent ,ServiceMargin ,ServiceMarginPercent ,TotalMarginPercent ,DiscountId ,NVR) VALUES (@QuoteID ,@OpportunityId ,@QuoteName ,@PrimaryQuote ,@QuoteStatusId ,@Discount ,@DiscountType ,@TotalDiscounts ,@NetCharges ,@CreatedOn ,@CreatedBy ,@LastUpdatedOn ,@LastUpdatedBy ,@ExpiryDate ,@AdditionalCharges ,@QuoteNumber ,@TotalMargin ,@ProductSubTotal ,@QuoteSubTotal ,@MeasurementId ,@CoreProductMargin ,@CoreProductMarginPercent ,@MyProductMargin ,@MyProductMarginPercent ,@ServiceMargin ,@ServiceMarginPercent ,@TotalMarginPercent ,@DiscountId ,@NVR);select cast(scope_identity() as int);";

                    var fQuery = @"select ql.* from crm.quotelines ql  where   ql.quotekey  = @quotekey;
                                   SELECT qd.* FROM [CRM].[quotelinedetail] qd inner join [CRM].[QuoteLines] ql on qd.[QuoteLineId] = ql.[QuoteLineId] WHERE quotekey = @quotekey;
                                   SELECT q.* FROM [CRM].[quote] AS q WHERE quotekey = @quotekey;select * from crm.discountsandpromo where quoteKey = @quotekey;
                                   SELECT o.* FROM crm.Opportunities o INNER JOIN crm.Quote q ON q.OpportunityId = o.OpportunityId WHERE q.QuoteKey=@quotekey;
                                   SELECT * FROM CRM.QuoteTax qt WHERE qt.quotekey=@quotekey;";

                    var insertquoteLinesQuery = @"INSERT INTO CRM.QuoteLines (QuoteKey ,MeasurementsetId ,VendorId ,ProductId ,SuggestedResale ,Discount ,CreatedOn ,CreatedBy ,LastUpdatedOn ,LastUpdatedBy ,IsActive ,Width ,Height ,UnitPrice ,Quantity ,Description ,MountType ,ExtendedPrice ,Memo ,PICJson ,ProductTypeId ,Quntity ,MarkupfactorType ,DiscountType ,PICPriceResponse ,ProductName ,VendorName ,CurrencyExchangeId ,ConversionRate ,VendorCurrency ,ConversionCalculationType) VALUES (@QuoteKey ,@MeasurementsetId ,@VendorId ,@ProductId ,@SuggestedResale ,@Discount ,@CreatedOn ,@CreatedBy ,@LastUpdatedOn ,@LastUpdatedBy ,@IsActive ,@Width ,@Height ,@UnitPrice ,@Quantity ,@Description ,@MountType ,@ExtendedPrice ,@Memo ,@PICJson ,@ProductTypeId ,@Quntity ,@MarkupfactorType ,@DiscountType ,@PICPriceResponse ,@ProductName ,@VendorName ,@CurrencyExchangeId ,@ConversionRate ,@VendorCurrency ,@ConversionCalculationType);select cast(scope_identity() as int)";

                    var qDetailsQuery = @"INSERT INTO CRM.QuoteLineDetail (QuoteLineId ,Quantity ,BaseCost ,BaseResalePrice ,BaseResalePriceWOPromos ,PricingCode ,PricingRule ,MarkupFactor ,Rounding ,SuggestedResale ,Discount ,unitPrice ,DiscountAmount ,ExtendedPrice ,MarginPercentage ,PricingId ,ProductOrOption ,SelectedPrice ,CreatedOn ,CreatedBy ,LastUpdatedOn ,LastUpdatedBy ,Cost ,NetCharge ,ManualUnitPrice) VALUES (@QuoteLineId ,@Quantity ,@BaseCost ,@BaseResalePrice ,@BaseResalePriceWOPromos ,@PricingCode ,@PricingRule ,@MarkupFactor ,@Rounding ,@SuggestedResale ,@Discount ,@unitPrice ,@DiscountAmount ,@ExtendedPrice ,@MarginPercentage ,@PricingId ,@ProductOrOption ,@SelectedPrice ,@CreatedOn ,@CreatedBy ,@LastUpdatedOn ,@LastUpdatedBy ,@Cost ,@NetCharge ,@ManualUnitPrice);select cast(scope_identity() as int); ";

                    var fDetails = connection.QueryMultiple(fQuery, new { quotekey = quoteKey });

                    var qLinesObj = fDetails.Read<QuoteLines>().ToList();//.OrderBy(x => x.QuoteLineDetail.QuoteLineId).ToList();
                    var quotelineDetaillist = fDetails.Read<QuoteLineDetail>().ToList();
                    var data = fDetails.Read<Quote>().FirstOrDefault();
                    var discountAndPromoList = fDetails.Read<DiscountsAndPromo>().ToList();
                    var opportunity = fDetails.Read<Opportunity>().FirstOrDefault();
                    var quoteTax = fDetails.Read<QuoteTax>().FirstOrDefault();

                    var user = User.PersonId;
                    //var data = connection.Query<Quote>(@"Select * from crm.quote where QuoteKey = @quotekey", new { quotekey = quoteKey }).FirstOrDefault();
                    //var quote = new Quote();

                    if (opportunity != null && copyTaxExempt)
                    {
                        data.IsTaxExempt = opportunity.IsTaxExempt;
                        data.TaxExemptID = opportunity.TaxExemptID;
                        data.IsPSTExempt = opportunity.IsPSTExempt;
                        data.IsGSTExempt = opportunity.IsGSTExempt;
                        data.IsHSTExempt = opportunity.IsHSTExempt;
                        data.IsVATExempt = opportunity.IsVATExempt;
                        data.IsNewConstruction = opportunity.IsNewConstruction;
                    }

                    data.CreatedBy = user;
                    data.LastUpdatedBy = user;

                    // TP-2497 : CLONE - Quote Name Not Updating
                    var newQuoteId = actMgr.GetNextNumber(Franchise.FranchiseId, 2);//2 Quote Number
                    data.QuoteName = data.QuoteName.Replace(data.QuoteID.ToString(), newQuoteId.ToString());

                    data.QuoteID = newQuoteId; //actMgr.GetNextNumber(Franchise.FranchiseId, 2);//2 Quote Number
                                               // data.ExpiryDate = GetExpiryDate(Franchise.FranchiseId);
                    data.ExpiryDate = null;
                    data.PrimaryQuote = false;
                    data.QuoteStatusId = 1;
                    data.MeasurementId = data.MeasurementId == 0 ? null : data.MeasurementId;
                    data.QuoteKey = 0;
                    //nulling sale date
                    data.SaleDate = null;
                    data.SaleDateCreatedBy = null;
                    data.SaleDateCreatedOn = null;
                    data.PreviousSaleDate = null;

                    data.QuoteKey = connection.Insert<int, Quote>(data);

                    foreach (var ql in qLinesObj)
                    {
                        var qlId = ql.QuoteLineId;
                        var quoteL = ql;
                        quoteL.QuoteLineId = 0;
                        quoteL.QuoteKey = data.QuoteKey;
                        quoteL.CreatedBy = user;
                        quoteL.LastUpdatedBy = user;

                        quoteL.QuoteLineId = connection.Insert<int, QuoteLines>(quoteL);

                        var qd = quotelineDetaillist.Where(x => x.QuoteLineId == qlId).First();
                        if (qd != null)
                        {
                            var qDetails = qd;
                            qDetails.QuoteLineDetailId = 0;
                            qDetails.QuoteLineId = quoteL.QuoteLineId;
                            qDetails.CreatedBy = user;
                            qDetails.LastUpdatedBy = user;

                            int detailid = connection.Insert<int, QuoteLineDetail>(qDetails);
                        }

                        var list = discountAndPromoList.Where(x => x.QuoteLineId == qlId).ToList();

                        foreach (var qDnP in list)
                        {
                            if (qDnP != null)
                            {
                                qDnP.QuoteLineId = quoteL.QuoteLineId;
                                qDnP.QuoteKey = data.QuoteKey;
                                qDnP.CreatedBy = user;
                                qDnP.LastUpdatedBy = user;
                                connection.Insert<DiscountsAndPromo>(qDnP);
                            }
                        }
                    }
                    CopyGroupDiscount(quoteKey, data.QuoteKey);
                    var response = RecalculateQuoteLines(data.QuoteKey, 0);

                    //Updating LastUpdatedOn from original Quote to copied Quote (TP-3771)
                    var UpdateQuery = @"Update CRM.Quote set LastUpdatedOn =@LastUpdatedOn
                                    where QuoteKey=@QuoteKey";
                    var ress = connection.Execute(UpdateQuery, new
                    {
                        QuoteKey = data.QuoteKey,
                        LastUpdatedOn = data.LastUpdatedOn
                    });

                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public int Save(Quote data)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                try
                {
                    var checkorderExist = CheckOrderExisitbyOpportunity(data.OpportunityId);
                    if (checkorderExist != null && checkorderExist.QuoteKey != data.QuoteKey)
                        data.PrimaryQuote = false;

                    if (data.SaleDate == null && data.QuoteStatusId == 3)
                        data.SaleDate = TimeZoneManager.ToLocal(DateTime.UtcNow).Date;

                    if (data.QuoteStatusId != 3)
                    {
                        data.SaleDate = null;
                        data.PreviousSaleDate = null;
                        data.SaleDateCreatedBy = null;
                        //data.SaleDateCreatedOn = null;
                    }

                    int res = 0;
                    var quoteList = connection.Query<Quote>(@"select * from crm.quote where OpportunityId = @OpportunityId", new { OpportunityId = data.OpportunityId }).ToList();
                    // var query = @"INSERT INTO [CRM].[Quote] ([QuoteID] ,[OpportunityId] ,[QuoteName] ,[PrimaryQuote] ,[QuoteStatusId] ,[Discount] ,[DiscountType] ,[TotalDiscounts] ,[NetCharges] ,[CreatedOn] ,[CreatedBy] ,[LastUpdatedOn] ,[LastUpdatedBy] ,[ExpiryDate] ,[AdditionalCharges] ,[QuoteNumber] ,[TotalMargin] ,[ProductSubTotal] ,[QuoteSubTotal] ,[MeasurementId],DiscountId) VALUES (@QuoteID ,@OpportunityId ,@QuoteName ,@PrimaryQuote ,@QuoteStatusId ,@Discount ,@DiscountType ,@TotalDiscounts ,@NetCharges ,@CreatedOn ,@CreatedBy ,@LastUpdatedOn ,@LastUpdatedBy ,@ExpiryDate ,@AdditionalCharges ,@QuoteNumber ,@TotalMargin ,@ProductSubTotal ,@QuoteSubTotal ,@MeasurementId,@DiscountId);select cast(scope_identity() as int)";
                    var query = @"INSERT INTO [CRM].[Quote] ([QuoteID] ,[OpportunityId] ,[QuoteName] ,[PrimaryQuote] ,[QuoteStatusId] ,[Discount] ,[DiscountType] ,[TotalDiscounts] ,[NetCharges] ,[CreatedOn] ,[CreatedBy] ,[LastUpdatedOn] ,[LastUpdatedBy] ,[ExpiryDate] ,[AdditionalCharges] ,[QuoteNumber] ,[TotalMargin] ,[ProductSubTotal] ,[QuoteSubTotal] ,[MeasurementId],DiscountId,IsTaxExempt ,TaxExemptID ,IsPSTExempt ,IsGSTExempt ,IsHSTExempt ,IsVATExempt ,IsNewConstruction,SideMark,SaleDate,PreviousSaleDate,SaleDateCreatedBy) VALUES (@QuoteID ,@OpportunityId ,@QuoteName ,@PrimaryQuote ,@QuoteStatusId ,@Discount ,@DiscountType ,@TotalDiscounts ,@NetCharges ,@CreatedOn ,@CreatedBy ,@LastUpdatedOn ,@LastUpdatedBy ,@ExpiryDate ,@AdditionalCharges ,@QuoteNumber ,@TotalMargin ,@ProductSubTotal ,@QuoteSubTotal ,@MeasurementId,@DiscountId,@IsTaxExempt,@TaxExemptID,@SideMark,@SaleDate,@PreviousSaleDate,@SaleDateCreatedBy);select cast(scope_identity() as int)";

                    var quote = quoteList.Where(x => x.QuoteKey == data.QuoteKey).FirstOrDefault();

                    if (quote != null && quote.QuoteStatusId != data.QuoteStatusId)
                    {
                        if (data.QuoteStatusId == 2)
                        {
                            if (data.ExpiryDate == null)
                                data.ExpiryDate = GetExpiryDate(Franchise.FranchiseId);

                            if (checkorderExist == null)
                                data.PrimaryQuote = UpdatePrimaryQuote(data.OpportunityId, data.QuoteKey);
                        }
                        else if (data.QuoteStatusId != 2 && data.ExpiryDate != null)
                        {
                            data.ExpiryDate = null;
                        }
                    }
                    if (data.QuoteKey == 0)
                    {
                        if (quote == null)
                        {
                            quote = new Quote();
                        }

                        if (data.QuoteStatusId == 3)
                        {
                            EventLogger.LogEvent("Empty Quote with accepted status.", "Quote", LogSeverity.Error, null,
                                "Error", User.UserName,
                                Franchise.FranchiseId);
                            throw new Exception(FailedByError);
                        }
                        data.CreatedOn = DateTime.UtcNow;
                        data.CreatedBy = User.PersonId;
                        data.QuoteID = actMgr.GetNextNumber(Franchise.FranchiseId, 2);//2 Quote Number
                        //data.ExpiryDate = GetExpiryDate(Franchise.FranchiseId);
                        //quote.QuoteKey = connection.Insert(query, data).First();

                        //TP-1715	CLONE - Quote Name Autofill with Opportunity Name
                        // IF the Quote is same as auto generated quote name, we need to update the
                        // name with the right quoteid, not the temp quoteid which we displyaed to
                        // the user
                        if (data.QuoteName == data.QuoteNameAutoGenerated)
                        {
                            data.QuoteName = data.QuoteID + "-" + data.OpportunityName;
                        }
                        data.MeasurementId = data.MeasurementId == 0 ? null : data.MeasurementId;
                        quote.QuoteKey = connection.Insert<int, Quote>(data);

                        if (data.PrimaryQuote == true)
                        {
                            UpdatePrimaryQuote(data.OpportunityId, quote.QuoteKey);
                        }
                    }
                    else
                    {
                        quote.QuoteName = data.QuoteName;
                        quote.QuoteStatusId = data.QuoteStatusId;
                        quote.PrimaryQuote = data.PrimaryQuote;
                        quote.Discount = data.Discount;
                        quote.DiscountType = data.DiscountType;
                        quote.DiscountId = data.DiscountId;
                        quote.IsTaxExempt = data.IsTaxExempt;
                        quote.TaxExemptID = data.TaxExemptID;
                        quote.IsPSTExempt = data.IsPSTExempt;
                        quote.IsGSTExempt = data.IsGSTExempt;
                        quote.IsHSTExempt = data.IsHSTExempt;
                        quote.IsVATExempt = data.IsVATExempt;
                        quote.IsNewConstruction = data.IsNewConstruction;
                        quote.LastUpdatedOn = DateTime.UtcNow;
                        quote.LastUpdatedBy = User.PersonId;
                        quote.SideMark = data.SideMark;
                        quote.QuoteLevelNotes = data.QuoteLevelNotes;
                        quote.OrderInvoiceLevelNotes = data.OrderInvoiceLevelNotes;
                        quote.InstallerInvoiceNotes = data.InstallerInvoiceNotes;
                        quote.ExpiryDate = TimeZoneManager.ToUTC(data.ExpiryDate);
                        quote.SaleDate = data.SaleDate;
                        quote.PreviousSaleDate = data.PreviousSaleDate;
                        quote.SaleDateCreatedBy = data.SaleDateCreatedBy;
                        //quote.SaleDateCreatedOn = data.SaleDateCreatedOn;

                        res = connection.Update(quote);
                        //if (quote.IsTaxExempt)
                        //{
                        //    DeleteTax(quote.QuoteKey);
                        //}
                    }
                    var response = RecalculateQuoteLines(quote.QuoteKey, 0);
                    UpdateQuote(quote.QuoteKey);
                    return quote.QuoteKey;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public int SaveMoveQuote(MoveOpportunityDTO model)
        {
            try
            {

                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    int QuoteKey = 0;

                    // Check the opportunity have already Quote, if there is no quoue set quote as primary Quote
                    int oppcount = connection.Query<int>("select count(1) from CRM.Quote where OpportunityId=@OpportunityId", new { OpportunityId = model.OpportunityId }).FirstOrDefault();

                    Quote quote = GetQuoteData(model.QuoteKey);
                    List<QuoteLines> quoteline = connection.Query<QuoteLines>("select * from CRM.QuoteLines where  QuoteKey = @QuoteKey", new { QuoteKey = model.QuoteKey }).ToList();

                    string qQuotelineDetails = @"select qld.* from CRM.QuoteLineDetail qld
                                            join CRM.QuoteLines ql on qld.QuoteLineId=ql.QuoteLineId
                                            where ql.QuoteKey=@QuoteKey";
                    List<QuoteLineDetail> quotelinedetail = connection.Query<QuoteLineDetail>(qQuotelineDetails, new { QuoteKey = model.QuoteKey }).ToList();

                    if (quote != null)
                    {
                        quote.QuoteKey = 0;
                        quote.OpportunityId = model.OpportunityId;
                        quote.QuoteStatusId = 1;
                        quote.PrimaryQuote = oppcount == 0 ? true : quote.PrimaryQuote;
                        quote.QuoteID = actMgr.GetNextNumber(Franchise.FranchiseId, 2);
                        //TP-3851 Copied Quotes - should default to date of original
                        quote.CreatedBy = User.PersonId;
                        quote.LastUpdatedBy = User.PersonId;
                        quote.ExpiryDate = null;
                        quote.SaleDate = null;
                        quote.SaleDateCreatedBy = null;
                        quote.SaleDateCreatedOn = null;
                        quote.PreviousSaleDate = null;
                        //TP-3485 Move Quote Side Mark does not show the right Side mark value in the newly moved quote.
                        quote.SideMark = model.SideMark;
                        QuoteKey = connection.Insert<int, Quote>(quote);
                    }

                    if (QuoteKey > 0)
                    {
                        foreach (var ql in quoteline)
                        {
                            var orgQuoteLineId = ql.QuoteLineId;
                            ql.QuoteLineId = 0;
                            ql.QuoteKey = QuoteKey;
                            ql.CreatedBy = User.PersonId;
                            ql.LastUpdatedBy = User.PersonId;
                            int QuoteLineId = connection.Insert<int, QuoteLines>(ql);

                            if (QuoteLineId > 0)
                            {
                                var qld = quotelinedetail.Where(x => x.QuoteLineId == orgQuoteLineId).FirstOrDefault();
                                if (qld != null)
                                {
                                    qld.QuoteLineDetailId = 0;
                                    qld.QuoteLineId = QuoteLineId;
                                    qld.CreatedBy = User.PersonId;
                                    qld.LastUpdatedBy = User.PersonId;
                                    int QuoteLineDetailId = connection.Insert<int, QuoteLineDetail>(qld);
                                }

                                var response = GetAndUpdateTax(QuoteKey, QuoteLineId);
                            }
                        }
                        CopyGroupDiscount(model.QuoteKey, QuoteKey);
                    }
                    return QuoteKey;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        //sale date
        public QuoteVM SaveSale(int QuoteKey, DateTime SaleDate)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var Query = @"update [CRM].[Quote] set
                                 SaleDateCreatedBy =@SaleDateCreatedBy ,SaleDateCreatedOn =@SaleDateCreatedOn,
                                 PreviousSaleDate=SaleDate,SaleDate=@SaleDate
                                    where QuoteKey=@QuoteKey";

                    connection.Open();
                    var ress = connection.Execute(Query, new
                    {
                        QuoteKey = QuoteKey,
                        SaleDate = SaleDate,
                        SaleDateCreatedBy = SessionManager.CurrentUser.PersonId,
                        SaleDateCreatedOn = DateTime.UtcNow
                    });

                    var SaleQuery = @"select *,p.FirstName +' '+ p.LastName as SaleDateCreatedByName  from CRM.quote q
							left join CRM.Person P on q.SaleDateCreatedBy = p.PersonId where QuoteKey=@QuoteKey";

                    var res = connection.Query<QuoteVM>(SaleQuery, new { QuoteKey = QuoteKey }).FirstOrDefault();
                    res.SaleDateCreatedOn = TimeZoneManager.ToLocal(res.SaleDateCreatedOn);
                    // res.PreviousSaleDate = TimeZoneManager.ToLocal(res.PreviousSaleDate);
                    //  res.SaleDate = TimeZoneManager.ToLocal(res.SaleDate);
                    return res;
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public DateTime GetExpiryDate(int franchiseId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var expiryDays = 30;
                connection.Open();
                var franchise = connection.Get<Franchise>(franchiseId);
                if (franchise.QuoteExpiryDays != 0)
                {
                    expiryDays = franchise.QuoteExpiryDays;
                }
                connection.Close();
                var todayDate = TimeZoneManager.GetLocal();
                return todayDate.AddDays(expiryDays);
                //return DateTime.UtcNow.AddDays(expiryDays);
            }
        }

        public int GetExpiry(int franchiseId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var expiryDays = 30;
                connection.Open();
                var franchise = connection.Get<Franchise>(franchiseId);
                if (franchise.QuoteExpiryDays != 0)
                {
                    expiryDays = franchise.QuoteExpiryDays;
                }
                connection.Close();
                return expiryDays;
            }
        }

        public override string Add(Quote data)
        {
            throw new NotImplementedException();
        }

        public override string Delete(int id)
        {
            throw new NotImplementedException();
        }

        public override Quote Get(int id)
        {
            throw new NotImplementedException();
        }

        public override ICollection<Quote> Get(List<int> idList)
        {
            throw new NotImplementedException();
        }

        public override string Update(Quote data)
        {
            throw new NotImplementedException();
        }

        public ICollection<Quote> GetQuote(int opportunityId)
        {
            var query = @"select case when o.TerritoryId is not null then tr.Name else o.TerritoryType end as Territory, q.Quotesubtotal, q.ExpiryDate, q.MeasurementId,o.InstallationAddressId,QuoteKey, q.QuoteStatusId, QuoteID,q.OpportunityId,PrimaryQuote,QuoteName,t.QuoteStatus,p.FirstName+' '+p.LastName as SalesAgent,NetCharges,CONVERT(datetime, q.CreatedOn) CreatedOn,CONVERT(datetime, q.LastUpdatedOn) LastUpdatedOn ,o.AccountId,
                            case when exists (select 1  from crm.orders where orderstatus not in (6,9) and opportunityId  =@oppotunityId) then 1 else 0 end as OrderExists,
							(select count(*) from crm.quotelines ql inner join crm.QuoteLineDetail details on details.quotelineid = ql.quotelineid where ql.producttypeid not in (0,4) and quoteKey = q.quotekey) as NumberOfQuotelines
                            ,q.QuoteLevelNotes,q.OrderInvoiceLevelNotes,q.InstallerInvoiceNotes,q.*
                            from [CRM].[Opportunities] o
                            join [CRM].[Quote] q on o.OpportunityId=q.OpportunityId
                            left join [CRM].[Type_QuoteStatus] t on q.QuoteStatusId=t.QuoteStatusId
                            left join [CRM].[Person] p on o.SalesAgentId=p.PersonId
                            LEFT JOIN CRM.Territories tr on tr.TerritoryId=o.TerritoryId
                            where o.OpportunityId=@oppotunityId and not t.QuoteStatusId=5 order by q.createdon Desc;";
            var result = ExecuteIEnumerableObject<Quote>(ContextFactory.CrmConnectionString, query, new
            {
                oppotunityId = opportunityId
            }).ToList();
            if (result != null)
            {
                foreach (var item in result)
                {
                    item.TaxDetails = GetTaxinfo(item.QuoteKey);
                    if (item.TaxDetails != null && item.TaxDetails.Count() > 0)
                    {
                        item.QuoteSubTotal = item.QuoteSubTotal + item.TaxDetails.Sum(x => x.Amount);
                    }

                    if (item.QuoteStatusId == 1 && item.QuoteStatusId == 4)
                        item.ExpiryDate = null;
                    if (item.QuoteStatusId == 2 && item.ExpiryDate == null)
                    {
                        int expiryday = GetExpiry(Franchise.FranchiseId);
                        var lastupdatedate = item.LastUpdatedOn;
                        var LastUpdatedOn = TimeZoneManager.ToLocal(((DateTimeOffset)lastupdatedate).DateTime);
                        //var value = Convert.ToDateTime(lastupdatedate);
                        item.ExpiryDate = LastUpdatedOn.AddDays(expiryday);
                    }
                    else
                        item.ExpiryDate = TimeZoneManager.ToLocal(item.ExpiryDate);

                    if (item.CreatedOn != null)
                        item.CreatedOn = TimeZoneManager.ToLocal(((DateTimeOffset)item.CreatedOn).DateTime);

                    if (item.LastUpdatedOn != null)
                        item.LastUpdatedOn = TimeZoneManager.ToLocal(((DateTimeOffset)item.LastUpdatedOn).DateTime);
                }

                return result;
            }
            else
                return null;
        }

        public ICollection<Quote> GetAllQuote(string QuoteStatusId, string dateRangeId, bool PrimaryQuote)
        {
            var result = ExecuteIEnumerableObject<Quote>(ContextFactory.CrmConnectionString,
                "EXEC [CRM].[GetAllQuote] @FranchiseId,@datefilter,@QuoteStatusId,@PrimaryQuote",
                new { FranchiseId = Franchise.FranchiseId, datefilter = dateRangeId ?? "", QuoteStatusId = QuoteStatusId ?? "", PrimaryQuote }).ToList();
            if (result != null)
            {
                foreach (var item in result)
                {
                    // To show lastupdatedon in FE timezone
                    if (item.LastUpdatedOn != null)
                        item.LastUpdatedOn = TimeZoneManager.ToLocal(((DateTimeOffset)item.LastUpdatedOn).DateTime);
                }
            }
            return result;
        }

        public QuoteVM GetDetails(int id)
        {
            var query = @"Select CASE WHEN EXISTS (SELECT * FROM crm.Orders o2 WHERE o2.QuoteKey=q.quotekey AND o2.OrderStatus NOT IN (9,6)) then 1 else 0 END AS QuoteOrder, q.MeasurementId,o.InstallationAddressId,o.BillingAddressId,
                      case when q.SideMark is not null then q.SideMark else o.SideMark end as SideMark,
                     ac.AccountId,QuoteKey,q.discountid, q.QuoteId, q.QuoteName,q.OpportunityId,OpportunityName,q.QuoteStatusId,
                     cs.FirstName+' '+cs.LastName as SalesAgent,
                     cs.FirstName+' '+cs.LastName as SalesAgent,
                    (select [CRM].[fnGetAccountNameByAccountId](ac.AccountId)) as AccountName,q.NVR,
                     ISNULL(PrimaryQuote,0) PrimaryQuote,Discount,DiscountType,TotalDiscounts,NetCharges,q.CreatedOn,q.LastUpdatedOn,q.ExpiryDate ,q.AdditionalCharges, q.ProductSubTotal,q.QuoteSubTotal,ISNULL(q.CoreProductMargin,0) as CoreProductMargin ,
                     ISNULL(q.CoreProductMarginPercent,0) as  CoreProductMarginPercent ,ISNULL(q.MyProductMargin,0) as MyProductMargin ,ISNULL(q.MyProductMarginPercent,0) as MyProductMarginPercent ,ISNULL(q.ServiceMargin,0) as ServiceMargin ,
                     ISNULL(q.ServiceMarginPercent,0) as ServiceMarginPercent,ISNULL(q.TotalMargin,0) as TotalMargin,ISNULL(q.TotalMarginPercent,0) as TotalMarginPercent,(select count(*) from crm.quotelines where quotekey = @quoteID) as NumberOfQuotelines
                     ,q.QuoteLevelNotes,q.OrderInvoiceLevelNotes,q.InstallerInvoiceNotes,
                      q.SaleDate, q.SaleDateCreatedBy, q.SaleDateCreatedOn,q.PreviousSaleDate,o.CreatedOnUtc as OpportunityCreatedOn
                       ,CASE WHEN ts.TaxType=3 THEN 1 ELSE 0 END AS UseTax,
                       ISNULL(
        (
            SELECT CASE
                       WHEN(ISNULL(ts.UseCustomerAddress, 0) = 0
                            AND ISNULL(ts.ShipToLocationId, 0) = 0
                            AND ts.TaxType = 2)
                           OR f.EnableTax = 0
                           OR (ts.TaxType IN(1))
                       THEN 1
                       ELSE 0
                   END
        ), 0) AS IsTaxDisabled,q.IsTaxExempt,q.TaxExemptID,q.IsVATExempt,q.IsPSTExempt,q.IsGSTExempt,q.IsVATExempt,q.IsNewConstruction,q.IsHSTExempt,p.FirstName +' '+ p.LastName as SaleDateCreatedByName
					 from [CRM].[Quote] q
                     join [CRM].[Opportunities] o on q.OpportunityId=o.OpportunityId
                     join [CRM].[Franchise] f on f.FranchiseId=o.FranchiseId
                     left join [CRM].[Accounts] ac on o.AccountId=ac.AccountId
                     left join [CRM].[Customer] c on ac.PersonId=c.CustomerId
                     left join [CRM].[Person] cs on o.SalesAgentId=cs.Personid
                    left join CRM.Person P on q.SaleDateCreatedBy = p.PersonId
                    INNER JOIN crm.TaxSettings ts ON  ts.FranchiseId = ac.FranchiseId AND ISNULL(ts.TerritoryId,0) = ISNULL(o.TerritoryId,0) 
                    where q.QuoteKey=@quoteID;";
            var result = ExecuteIEnumerableObject<QuoteVM>(ContextFactory.CrmConnectionString, query, new
            {
                quoteID = id
            }).FirstOrDefault();

            //     var SaleQuery = @"select p.FirstName +' '+ p.LastName as SaleDateCreatedByName  from CRM.quote q
            //left join CRM.Person P on q.SaleDateCreatedBy = p.PersonId where QuoteKey=@QuoteKey";

            //     var res = ExecuteIEnumerableObject<QuoteVM>(ContextFactory.CrmConnectionString, SaleQuery, new { QuoteKey = id }).FirstOrDefault();
            //     result.SaleDateCreatedByName = res.SaleDateCreatedByName;

            if (result != null)
            {
                query = @"select cus.FirstName+' '+cus.LastName as ContactName,* from CRM.Addresses addr
                      left join CRM.Customer cus on cus.CustomerId = addr.ContactId
                      where AddressId = @addressId";
                var insAddress = ExecuteIEnumerableObject<AddressTP>(ContextFactory.CrmConnectionString, query, new
                {
                    addressId = result.InstallationAddressId
                }).FirstOrDefault();
                if (insAddress != null)
                    result.InstallationAddress = insAddress;
                var bilAddress = ExecuteIEnumerableObject<AddressTP>(ContextFactory.CrmConnectionString, query, new
                {
                    addressId = result.BillingAddressId
                }).FirstOrDefault();
                if (bilAddress != null)
                    result.BillingAddress = bilAddress;
            }

            //var oppoquery = @"SELECT a.*,o.* FROM crm.Opportunities o
            //                INNER JOIN crm.Accounts a ON a.AccountId = o.AccountId
            //                INNER JOIN crm.Quote q ON q.OpportunityId = o.OpportunityId WHERE q.QuoteKey = @QuoteKey";

            // res.Opportunity = ExecuteIEnumerableObject<Opportunity>(ContextFactory.CrmConnectionString, oppoquery, new { QuoteKey = id }).FirstOrDefault();

            result.CreatedOn = TimeZoneManager.ToLocal(result.CreatedOn.DateTime);

            result.OpportunityCreatedOn = TimeZoneManager.ToLocal(((DateTimeOffset)result.OpportunityCreatedOn).DateTime);

            //if (result.SaleDate != null)
            //    result.SaleDate = TimeZoneManager.ToLocal(((DateTimeOffset)result.SaleDate).DateTime);

            if (result.SaleDateCreatedOn != null)
                result.SaleDateCreatedOn = TimeZoneManager.ToLocal(result.SaleDateCreatedOn);

            //if (result.PreviousSaleDate != null)
            //    result.PreviousSaleDate = TimeZoneManager.ToLocal(((DateTimeOffset)result.PreviousSaleDate).DateTime);

            if (result.LastUpdatedOn != null)
                result.LastUpdatedOn = TimeZoneManager.ToLocal(((DateTimeOffset)result.LastUpdatedOn).DateTime);
            ///Get all the orders for opportunity which is not in cancelled or void status
            var orders = ExecuteIEnumerableObject<Orders>(ContextFactory.CrmConnectionString, "select * from crm.orders where orderstatus not in (6,9) and OpportunityId= @OpportunityId;", new
            {
                OpportunityId = result.OpportunityId
            }).FirstOrDefault();
            result.OrderExists = orders != null ? true : false;
            var quotes = ExecuteIEnumerableObject<Quote>(ContextFactory.CrmConnectionString, "select * from crm.quote where OpportunityId= @OpportunityId  and QuoteStatusId not in(5)", new
            {
                OpportunityId = result.OpportunityId
            }).ToList();

            if (quotes.Where(x => x.PrimaryQuote == true).FirstOrDefault() != null)
            {
                result.PrimaryQuoteName = quotes.Where(x => x.PrimaryQuote == true).FirstOrDefault().QuoteName;
                result.PrimaryQuoteId = quotes.Where(x => x.PrimaryQuote == true).FirstOrDefault().QuoteKey;
            }
            else
            {
                result.PrimaryQuoteName = "";
                result.PrimaryQuoteId = 0;
            }

            var qvm = GetFranchiseServicesAndDiscount();
            result.ServicesList = qvm.ServicesList;
            result.DiscountsList = qvm.DiscountsList;
            if (result != null)
            {
                var quoteLineList = GetListData<QuoteLines>(ContextFactory.CrmConnectionString, "where QuoteKey = @quoteKey", new { quoteKey = id }).ToList();
                result.QuoteLinesList = quoteLineList;
                var mesurementHeader = MeasurementMgr.Get(result.InstallationAddressId);
                if (mesurementHeader != null)
                {
                    var measuremntSetBB = mesurementHeader.MeasurementBB;
                    if (measuremntSetBB != null)
                    {
                        quoteLineList = GetListData<QuoteLines>(ContextFactory.CrmConnectionString, "where QuoteKey = @quoteKey", new { quoteKey = id }).ToList();
                        foreach (var item in quoteLineList)
                        {
                            item.LineItemBB = measuremntSetBB.Where(x => x.id == item.MeasurementsetId).FirstOrDefault();
                        }
                        result.QuoteLinesList = quoteLineList;
                    }
                    // return result;
                }
                decimal hDisc = 0;
                if (result.DiscountType != null)
                {
                    if (result.DiscountType == "$")
                    {
                        result.HeaderDiscount = result.Discount.HasValue ? result.Discount.Value : 0;
                    }
                    else
                    {
                        decimal disclines = 0;
                        if (result.QuoteLinesList != null && result.QuoteLinesList.Count() > 0)
                        {
                            disclines = result.QuoteLinesList.Where(x => x.ProductTypeId == 4 && x.DiscountType == "$" && x.Discount.HasValue).Sum(x => x.Discount.Value);
                        }

                        //hDisc = ((result.ProductSubTotal.HasValue ? result.ProductSubTotal.Value : 0) / result.NVR);
                        result.HeaderDiscount = (result.ProductSubTotal.HasValue ? result.ProductSubTotal.Value - disclines : 0) * ((result.Discount.HasValue ? result.Discount.Value : 0) / 100);
                        //result.HeaderDiscount = hDisc * ((result.Discount.HasValue?result.Discount.Value:0 )/ 100);
                    }
                }

                result.QuoteLinesList = null;

                // Fill the tax exemption details from Opportunity
                //HandleSalesTaxExempt(result.OpportunityId, result);

                result.TaxDetails = GetTaxinfo(id);
                if (result.TaxDetails != null && result.TaxDetails.Count() > 0)
                {
                    result.QuoteSubTotal = result.QuoteSubTotal + result.TaxDetails.Sum(x => x.Amount);

                }
                if (result.QuoteStatusId == 1 && result.QuoteStatusId == 4)
                    result.ExpiryDate = null;
                if (result.QuoteStatusId == 2 && result.ExpiryDate == null)
                {
                    int expiryday = GetExpiry(Franchise.FranchiseId);
                    var lastupdatedate = result.LastUpdatedOn;
                    var LastUpdatedOn = TimeZoneManager.ToLocal(((DateTimeOffset)lastupdatedate).DateTime);
                    //var value = Convert.ToDateTime(lastupdatedate);
                    result.ExpiryDate = LastUpdatedOn.AddDays(expiryday);
                }
                else
                    result.ExpiryDate = TimeZoneManager.ToLocal(result.ExpiryDate);

                result.QuoteGroupDiscount = GetQuoteGroupDiscount(result.QuoteKey);

                return result;
            }
            else
                return null;
        }

        public List<TaxDetails> GetTaxinfo(int quoteKey)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();

                    var taxDetailsList = connection.Query<TaxDetails>(
                       @"SELECT  Jurisdiction, JurisType, Rate,TaxName,SUM(amount) AS Amount FROM (
                          SELECT distinct td.Jurisdiction, td.JurisType, td.Rate,td.TaxName, td.amount,t.QuoteLineId FROM[CRM].[tax] AS t 
                          INNER JOIN CRM.taxdetails td on t.taxid = td.taxid INNER JOIN crm.quotelines AS q ON t.quotelineId = q.quotelineId 
                          where q.quoteKey = @quoteKey) as tpt
                          GROUP BY Jurisdiction,JurisType,Rate,TaxName
                          order by JurisType ASC ",
                        new { quoteKey = quoteKey }).ToList();

                    if (taxDetailsList != null && taxDetailsList.Count() > 0)
                    {
                    }
                    else
                    {
                        taxDetailsList = ReturnTaxList(quoteKey);
                    }


                    return taxDetailsList;
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public List<TaxDetails> GetQuoteTaxinfo(int quoteKey)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();

                    var taxDetailsList = connection.Query<TaxDetails>(
                       @"SELECT  Jurisdiction, JurisType, Rate,TaxName,SUM(amount) AS Amount FROM (
                          SELECT distinct td.Jurisdiction, td.JurisType, td.Rate,td.TaxName, td.amount,t.QuoteLineId FROM[CRM].[tax] AS t 
                          INNER JOIN CRM.taxdetails td on t.taxid = td.taxid INNER JOIN crm.quotelines AS q ON t.quotelineId = q.quotelineId 
                          where q.quoteKey = @quoteKey) as tpt
                          GROUP BY Jurisdiction,JurisType,Rate,TaxName
                          order by JurisType ASC ",
                        new { quoteKey = quoteKey }).ToList();

                    return taxDetailsList;
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public QuoteVM NewQuote(int id, int oppurtunityId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var query = @"
							SELECT o.InstallationAddressId,o.BillingAddressId,o.SideMark,m.id AS MeasurementId,  o.OpportunityId, OpportunityName,
                            cs.FirstName+' '+cs.LastName AS SalesAgent, [CRM].[fnGetAccountName_forOpportunitySearch](o.OpportunityId) as AccountName,
                            ac.AccountId,CASE WHEN ts.TaxType=3 THEN 1 ELSE 0 END AS UseTax
                            FROM [CRM].[Opportunities] AS o
                            LEFT JOIN [CRM].[Accounts] AS ac ON o.AccountId = ac.AccountId
                            LEFT JOIN [CRM].[Customer] AS c ON ac.PersonId = c.CustomerId
                            LEFT JOIN [CRM].[person] AS cs ON o.SalesAgentId = cs.PersonId
                            LEFT JOIN  [CRM].[MeasurementHeader] m ON  m.InstallationAddressid = o.InstallationAddressid
                            INNER JOIN crm.TaxSettings ts ON  ts.FranchiseId = ac.FranchiseId AND ISNULL(ts.TerritoryId,0) = ISNULL(o.TerritoryId,0) 
                            where o.OpportunityId = @OpportunityId";
                    var result = ExecuteIEnumerableObject<QuoteVM>(ContextFactory.CrmConnectionString, query, new
                    {
                        OpportunityId = oppurtunityId
                    }).FirstOrDefault();
                    if (result != null)
                    {
                        query = @"select cus.FirstName+' '+cus.LastName as ContactName,* from CRM.Addresses addr
                      left join CRM.Customer cus on cus.CustomerId = addr.ContactId
                      where AddressId = @addressId";
                        var insAddress = ExecuteIEnumerableObject<AddressTP>(ContextFactory.CrmConnectionString, query, new
                        {
                            addressId = result.InstallationAddressId
                        }).FirstOrDefault();
                        if (insAddress != null)
                            result.InstallationAddress = insAddress;
                        var bilAddress = ExecuteIEnumerableObject<AddressTP>(ContextFactory.CrmConnectionString, query, new
                        {
                            addressId = result.BillingAddressId
                        }).FirstOrDefault();
                        if (bilAddress != null)
                            result.BillingAddress = bilAddress;
                    }
                    if (result != null)
                    {
                        result.PrimaryQuote = true;
                        var res = connection.Query<QuoteVM>("select *,[CRM].[fnGetAccountName_forOpportunitySearch] ( @OpportunityId) as AccountName from [CRM].[Quote] where [OpportunityId] = @OpportunityId  and QuoteStatusId not in(5)", new { OpportunityId = oppurtunityId }).ToList();
                        if (res != null && res.Count() > 0)
                        {
                            result.PrimaryQuote = false;
                        }

                        // tax exempt
                        var resAcc = connection.Query<AccountTP>("select * from [CRM].[Accounts] where [AccountId] = (Select AccountId from [CRM].[Opportunities] where OpportunityId=@OpportunityId)", new { OpportunityId = oppurtunityId }).FirstOrDefault();

                        // Fill the tax exemption details from Opportunity
                        //HandleSalesTaxExempt(oppurtunityId, result, connection);

                        var opp = connection.Query<Opportunity>(@"select * from [CRM].[Opportunities]
                                                                    where OpportunityId=@OpportunityId"
                            , new { OpportunityId = oppurtunityId }).FirstOrDefault();

                        ////TODO: Fill the tax exemption details from Opportunity
                        if (opp != null)
                        {
                            result.IsTaxExempt = opp.IsTaxExempt;
                            result.TaxExemptID = opp.TaxExemptID;
                            result.IsPSTExempt = opp.IsPSTExempt;
                            result.IsGSTExempt = opp.IsGSTExempt;
                            result.IsHSTExempt = opp.IsHSTExempt;
                            result.IsVATExempt = opp.IsVATExempt;
                            result.IsNewConstruction = opp.IsNewConstruction;
                        }

                        //if (resAcc != null)
                        //{
                        //    result.IsTaxExempt = resAcc.IsTaxExempt;
                        //    result.TaxExemptID = resAcc.TaxExemptID;
                        //}
                        //else
                        //{
                        //    result.IsTaxExempt = false;
                        //    result.TaxExemptID = null;
                        //}

                        if (res != null && res.Count > 0)
                        {
                            if (res.Where(x => x.PrimaryQuote == true).FirstOrDefault() != null)
                            {
                                result.PrimaryQuoteName = res.Where(x => x.PrimaryQuote == true).FirstOrDefault().QuoteName;
                                result.PrimaryQuoteId = res.Where(x => x.PrimaryQuote == true).FirstOrDefault().QuoteKey;
                            }
                            else { result.PrimaryQuoteName = ""; result.PrimaryQuoteId = 0; }
                        }

                        result.QuoteStatusId = 1;
                        var mesurementHeader = MeasurementMgr.Get(result.OpportunityId);
                        var qvm = GetFranchiseServicesAndDiscount();
                        result.ServicesList = qvm.ServicesList;
                        result.DiscountsList = qvm.DiscountsList;

                        //TP-1715	CLONE - Quote Name Autofill with Opportunity Name
                        var tempQuoteNumber = actMgr.GetNextNumberReadOnly(Franchise.FranchiseId, 2);
                        var rescus = connection.Query<CustomerTP>("select * from CRM.Customer cus join CRM.AccountCustomers accus on accus.CustomerId=cus.CustomerId and IsPrimaryCustomer=1 where accus.AccountId=@AccountId", new { resAcc.AccountId }).FirstOrDefault();
                        if (resAcc != null && resAcc.IsCommercial)
                        {
                            result.QuoteNameAutoGenerated = tempQuoteNumber + "-" + rescus.CompanyName + "-" + DateTime.Now.GlobalDateFormat();
                            result.QuoteName = result.QuoteNameAutoGenerated;
                        }
                        else
                        {
                            result.QuoteNameAutoGenerated = tempQuoteNumber + "-" + rescus.LastName + "-" + DateTime.Now.GlobalDateFormat();
                            result.QuoteName = result.QuoteNameAutoGenerated;
                        }
                        return result;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
            //var sql = @"select * from crm.quote;";
            //var query = @"SELECT m.id AS MeasurementId,  o.OpportunityId, OpportunityName, cs.FirstName+' '+cs.LastName AS SalesAgent, c.FirstName+' '+c.LastName AS AccountName, ad.Address1, ad.City, ad.State, ad.ZipCode FROM [CRM].[Opportunities] AS o LEFT JOIN [CRM].[Accounts] AS ac ON o.AccountId = ac.AccountId LEFT JOIN [CRM].[Customer] AS c ON ac.PersonId = c.CustomerId LEFT JOIN [CRM].[Customer] AS cs ON o.SalesAgentId = cs.CustomerId LEFT JOIN [CRM].[Addresses] AS ad ON o.InstallationAddressId = ad.AddressId LEFT JOIN  [CRM].[MeasurementHeader] m ON m.OpportunityId = o.OpportunityId";
            //var result = ExecuteIEnumerableObject<QuoteVM>(ContextFactory.CrmConnectionString, query, new
            //{
            //    quoteID = id
            //}).FirstOrDefault();
            //var res = ExecuteIEnumerableObject<QuoteVM>(ContextFactory.CrmConnectionString, sql, new
            //{
            //    OpportunityId = id
            //}).FirstOrDefault();

            //if (result != null)
            //{
            //    result.QuoteStatusId = 1;
            //    var mesurementHeader = MeasurementMgr.Get(result.OpportunityId);
            //    //if (mesurementHeader != null)
            //    //{
            //    //    var measuremntSetBB = mesurementHeader.MeasurementBB;
            //    //    if (measuremntSetBB != null)
            //    //    {
            //    //        var quoteLineList = GetListData<QuoteLines>(ContextFactory.CrmConnectionString, "WHERE o.OpportunityId = @OpportunityId", new { OpportunityId = id }).ToList();
            //    //        //foreach (var item in quoteLineList)
            //    //        //{
            //    //        //    item.LineItemBB = measuremntSetBB.Where(x => x.id == item.MeasurementsetId).FirstOrDefault();
            //    //        //}
            //    //        //result.QuoteLinesList = quoteLineList;
            //    //    }
            //    //    return result;
            //    //}
            //    //result.QuoteLinesList = null;
            //    return result;
            //}
            //else
            //    return null;
        }

        private void HandleSalesTaxExempt(int opportunityId, QuoteVM model)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    HandleSalesTaxExempt(opportunityId, model, connection);
                }
                catch (Exception ex)
                {
                    // TODO: do we really need this catch section???
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        private void HandleSalesTaxExempt(int opportunityId, QuoteVM model, SqlConnection connection)
        {
            var opp = connection.Query<Opportunity>(@"select * from [CRM].[Opportunities]
                                                        where OpportunityId=@OpportunityId"
                , new { OpportunityId = opportunityId }).FirstOrDefault();

            //TODO: Fill the tax exemption details from Opportunity
            if (opp != null)
            {
                model.IsTaxExempt = opp.IsTaxExempt;
                model.TaxExemptID = opp.TaxExemptID;
                model.IsPSTExempt = opp.IsPSTExempt;
                model.IsGSTExempt = opp.IsGSTExempt;
                model.IsHSTExempt = opp.IsHSTExempt;
                model.IsVATExempt = opp.IsVATExempt;

                // TODO: Is this related to tax????
                model.IsNewConstruction = opp.IsNewConstruction;
            }
        }

        public List<QuoteLineDetail> GetMargin(int id)
        {
            var query = @"select * from CRM.QuoteLineDetail
                        where QuoteLineId=@id";
            var result = ExecuteIEnumerableObject<QuoteLineDetail>(ContextFactory.CrmConnectionString, query, new
            {
                id = id
            }).ToList();
            if (result != null)
            {
                return result;
            }
            else
                return null;
        }

        public QuoteLines getQuoteLinebyId(int id)
        {
            var query = @"SELECT * FROM crm.QuoteLines ql WHERE  ql.quotelineid= @id";
            var result = ExecuteIEnumerableObject<QuoteLines>(ContextFactory.CrmConnectionString, query, new
            {
                id = id
            }).FirstOrDefault();
            if (result != null)
            {
                return result;
            }
            else
                return null;
        }

        public List<DiscountsAndPromo> GetDiscountAndProm(int quoteLineDetailId)
        {
            var query = @"select * from CRM.DiscountsAndPromo
                        where QuoteLineDetailId=@id";
            var result = ExecuteIEnumerableObject<DiscountsAndPromo>(ContextFactory.CrmConnectionString, query, new
            {
                id = quoteLineDetailId
            }).ToList();
            if (result != null)
            {
                return result;
            }
            else
                return null;
        }

        public bool UpdateAndInsertDiscountRow(QuoteLines quoteline, QuoteLineDetail qDetail)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var usr = User.PersonId;
                    var datetime = DateTime.UtcNow;

                    quoteline.ConversionCalculationType = null;
                    quoteline.ConversionRate = null;
                    quoteline.CurrencyExchangeId = null;
                    quoteline.ExtendedPrice = null;
                    quoteline.PICJson = null;
                    quoteline.SuggestedResale = null;
                    quoteline.UnitPrice = null;
                    quoteline.VendorCurrency = null;

                    quoteline.LastUpdatedBy = usr;
                    quoteline.LastUpdatedOn = datetime;

                    qDetail.BaseCost = null;
                    qDetail.BaseResalePrice = null;
                    qDetail.BaseResalePriceWOPromos = null;
                    qDetail.Cost = null;
                    qDetail.DiscountAmount = null;
                    qDetail.ExtendedPrice = null;
                    qDetail.ManualUnitPrice = false;
                    qDetail.MarginPercentage = null;
                    qDetail.MarkupFactor = null;
                    qDetail.NetCharge = 0;
                    qDetail.PricingCode = null;
                    qDetail.PricingId = null;
                    qDetail.Rounding = "";
                    qDetail.unitPrice = null;
                    qDetail.SuggestedResale = null;
                    qDetail.SelectedPrice = null;

                    qDetail.LastUpdatedBy = usr;
                    qDetail.LastUpdatedOn = datetime;

                    connection.Open();
                    connection.Update(quoteline);
                    connection.Update(qDetail);
                    RecalculateQuoteLines(quoteline.QuoteKey, quoteline.QuoteLineId);
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
            return true;
        }

        public string UpdateQuoteLine(int suggestedResale, List<QuoteLineDetail> qld)
        {
            var query = @"Update CRM.QuoteLines set SuggestedResale=@newsuggestvalue where QuoteLineId =@id";
            var result = ExecuteIEnumerableObject<int>(ContextFactory.CrmConnectionString, query, new
            {
                newsuggestvalue = suggestedResale,
                id = qld[0].QuoteLineId
            });
            foreach (var item in qld)
            {
                var query1 = @"Update CRM.QuoteLineDetail set SelectedPrice=@newSelectedPrice where QuoteLineDetailId =@id";
                var result1 = ExecuteIEnumerableObject<int>(ContextFactory.CrmConnectionString, query1, new
                {
                    newSelectedPrice = item.SelectedPrice,
                    id = item.QuoteLineDetailId
                });
            }

            return null;
        }

        public List<CurrencyExchange> GetCurrencyExchange()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var currencyExchange = connection.Query<CurrencyExchange>("select * from [CRM].[CurrencyExchange]").ToList();
                    return currencyExchange;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
            }
        }

        public CurrencyExchange GetCurrencyConversionPICVendor(int franchiseId, string Vendorcurrency)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var res = connection.Query<CurrencyExchange>("CRM.SPGetCurrencyConversionforCoreVendor", new { FranchiseId = franchiseId, fromCurrency = Vendorcurrency },
             commandType: CommandType.StoredProcedure).ToList();

                    return res.Where(x => x.StartDate < DateTime.Now && x.EndDate > DateTime.Now).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
            }
        }

        public CurrencyExchange GetCurrencyConversion(int franchiseId, int VendorId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var res = connection.Query<CurrencyExchange>("CRM.SPGetCurrencyConversion", new { FranchiseId = franchiseId, VendorId = VendorId },
             commandType: CommandType.StoredProcedure).ToList();

                    return res.Where(x => x.StartDate < DateTime.Now && x.EndDate > DateTime.Now).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
            }
        }

        /// <summary>
        /// Update the prices of Quote header values
        /// </summary>
        /// <param name="quoteKey"></param>
        /// <returns></returns>

        public bool UpdateQuote(int quoteKey, int quotelineId = 0, bool ignoreTaxUpdate = false)
        {
            // SessionManager.SetConfigstatus(SessionManager.CurrentUser.PersonId.ToString(), "");
            //EventLogger.LogEvent("Step 3 of 4: Updating Pricing");
            SessionManager.SetConfigstatus(SessionManager.CurrentUser.PersonId.ToString(), "Step 3 of 4: Updating Pricing");
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();

                    //var fQuery = "select * from [CRM].[Quote] where [QuoteKey] = @QuoteKey;select ql.* from crm.quotelines ql inner join crm.quotelinedetail  qd on ql.quotelineid = qd.quotelineid where ql.quotekey = @QuoteKey;select qd.* from crm.quotelines ql inner join crm.quotelinedetail  qd on ql.quotelineid = qd.quotelineid where ql.quotekey = @QuoteKey;";

                    ////connection.Open();

                    //var qDetails = connection.QueryMultiple(fQuery, new { QuoteKey = quoteKey });

                    //var quote = qDetails.Read<Quote>().FirstOrDefault();
                    ////var Franchise = fDetails.Read<Franchise>().FirstOrDefault();
                    ////var priceSettings = fDetails.Read<PricingSettings>().ToList();
                    //var qLinesObj = qDetails.Read<QuoteLines>().ToList();
                    ////var quotelineDetail = fDetails.Read<QuoteLineDetail>().FirstOrDefault();

                    //var quotelineDetail = qDetails.Read<QuoteLineDetail>().ToList();

                    ////var qLinesObj = connection.Query<QuoteLines>("select * from [CRM].[QuoteLines] where [QuoteKey] = @QuoteKey", new { QuoteKey = quoteKey }).ToList();
                    ////var quote = connection.Query<Quote>("select * from [CRM].[Quote] where [QuoteKey] = @QuoteKey", new { QuoteKey = quoteKey }).FirstOrDefault();

                    //decimal qSubTotal = 0;
                    //decimal coreCost = 0;
                    //decimal coreTotal = 0;
                    //decimal myProductTotal = 0;
                    //decimal myProductCost = 0;
                    //decimal serviceCost = 0;
                    //decimal netCharges = 0;
                    //// decimal productSubtotal = 0;
                    //decimal additionalCharges = 0;
                    //if (quote != null && qLinesObj != null && qLinesObj.Count() > 0)
                    //{
                    //    quote.NVR = CalculateNVR(quote.QuoteKey);
                    //}

                    //if (qLinesObj != null && qLinesObj.Count() > 0)
                    //    foreach (var qline in qLinesObj)
                    //    {
                    //        var details = quotelineDetail.Where(x => x.QuoteLineId == qline.QuoteLineId).FirstOrDefault();
                    //        if (qline.ProductTypeId == 1)//core products
                    //        {
                    //            coreTotal = coreTotal + (details.ExtendedPrice.HasValue ? details.ExtendedPrice.Value : 0);
                    //            coreCost = coreCost + ((details.BaseCost.HasValue ? Convert.ToDecimal(details.BaseCost.Value) : 0) * (qline.Quantity.HasValue ? qline.Quantity.Value : 0));
                    //        }
                    //        else if (qline.ProductTypeId == 2 || qline.ProductTypeId == 5)//My Products
                    //        {
                    //            myProductTotal = myProductTotal + (details.ExtendedPrice.HasValue ? details.ExtendedPrice.Value : 0);
                    //            myProductCost = myProductCost + ((details.BaseCost.HasValue ? Convert.ToDecimal(details.BaseCost.Value) : 0) * (qline.Quantity.HasValue ? qline.Quantity.Value : 0));
                    //        }
                    //        else if (qline.ProductTypeId == 3) //Services
                    //        {
                    //            additionalCharges = additionalCharges + (details.ExtendedPrice.HasValue ? details.ExtendedPrice.Value : 0);
                    //            serviceCost = serviceCost + ((details.BaseCost.HasValue ? Convert.ToDecimal(details.BaseCost.Value) : 0) * (qline.Quantity.HasValue ? qline.Quantity.Value : 0));
                    //        }
                    //    }
                    //quote.ProductSubTotal = coreTotal + myProductTotal;
                    //qSubTotal = coreTotal + additionalCharges + myProductTotal;
                    //quote.TotalMargin = (qSubTotal - (coreCost + myProductCost + serviceCost));
                    //quote.NetCharges = (quote.ProductSubTotal * quote.NVR) + additionalCharges;
                    //quote.AdditionalCharges = additionalCharges;
                    ////quote.ProductSubTotal = coreCost + myProductTotal;

                    //if (coreTotal != 0)
                    //{
                    //    quote.CoreProductMargin = (coreTotal * quote.NVR.Value) - coreCost;
                    //    if (quote.NVR.HasValue && quote.NVR.Value > 0)
                    //    {
                    //        quote.CoreProductMarginPercent = (quote.CoreProductMargin / (coreTotal * quote.NVR.Value)) * 100;
                    //    }
                    //    else
                    //    {
                    //        quote.CoreProductMarginPercent = 0;
                    //    }
                    //}
                    //if (myProductTotal != 0)
                    //{
                    //    quote.MyProductMargin = (myProductTotal * quote.NVR.Value) - myProductCost;
                    //    if (quote.NVR.HasValue && quote.NVR.Value > 0)
                    //    {
                    //        quote.MyProductMarginPercent = (quote.MyProductMargin / (myProductTotal * quote.NVR.Value)) * 100;
                    //    }
                    //    else
                    //    {
                    //        quote.MyProductMarginPercent = 0;
                    //    }
                    //}

                    //if (additionalCharges != 0)
                    //{
                    //    quote.ServiceMargin = additionalCharges - serviceCost;
                    //    quote.ServiceMarginPercent = (quote.ServiceMargin / additionalCharges) * 100;
                    //}

                    //quote.TotalMargin = quote.CoreProductMargin + quote.MyProductMargin + quote.ServiceMargin;
                    //if (quote.NetCharges > 0)
                    //    quote.TotalMarginPercent = (quote.TotalMargin / quote.NetCharges) * 100;

                    //var disc = (Math.Round(((coreTotal + myProductTotal) * (quote.NVR.HasValue ? quote.NVR.Value : 0)) * 100) / 100);

                    //quote.TotalDiscounts = (coreTotal + myProductTotal) - disc;

                    //quote.QuoteSubTotal = qSubTotal - quote.TotalDiscounts;

                    ////if (quote.DiscountType == "$")
                    ////{
                    ////    quote.QuoteSubTotal = qSubTotal - quote.Discount;
                    ////}
                    ////else if (quote.DiscountType == "%")
                    ////{
                    ////    quote.QuoteSubTotal = qSubTotal - (qSubTotal * (quote.Discount / 100));
                    ////}
                    ////else
                    ////{
                    ////    quote.QuoteSubTotal = qSubTotal;
                    ////}

                    ////quote.QuoteSubTotal = (coreTotal + myProductTotal) - quote.TotalDiscounts;

                    ////connection.Update(quote);
                    //// SessionManager.SetConfigstatus(SessionManager.CurrentUser.PersonId.ToString(), "");
                    ////EventLogger.LogEvent("Step 4 of 4: Calculating Taxes");

                    // Update quote
                    connection.Query(@"exec crm.UpdateQuoteData @quote ,@userId", new { quote = quoteKey, @userId = SessionManager.CurrentUser.PersonId });
                    SessionManager.SetConfigstatus(SessionManager.CurrentUser.PersonId.ToString(), "Step 4 of 4: Calculating taxes");
                    if (!ignoreTaxUpdate)
                    {
                        var response = GetAndUpdateTax(quoteKey, quotelineId);
                    }

                    // SessionManager.SetConfigstatus(SessionManager.CurrentUser.PersonId.ToString(), "");

                    return true;
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        //public bool SaveJobEstimator(JobEstimator model)
        //{

        //    var jobEstimatorSet = JsonConvert.SerializeObject(model);
        //    if (!model.SummariziedQuoteLine)
        //    {
        //        if (model.MaterialsList != null)
        //        {
        //            var quoteLine = InsertEmptyQuoteline(model.QuoteKey);
        //            var productName = "";
        //            var description = "";
        //            float cost = 0;
        //            float suggestedResale = 0;
        //            float quantity = 0;
        //            quoteLine.ProductTypeId = 2;// my product
        //            quoteLine.ProductCategoryId = GetProductCategory("Building Materials");
        //            foreach (var item in model.MaterialsList)
        //            {
        //                quantity = quantity +item.QuantityUsed;

        //                if (productName == "")
        //                    productName = "MATERIALS: " + item.ProductName.Split('-')[1];
        //                else if (!productName.Contains(item.ProductName.Split('-')[1]))
        //                    productName = productName + " ," + item.ProductName.Split('-')[1];

        //                if (description == "")
        //                    description = item.Product;
        //                else
        //                    description = description + " ," + item.Product;

        //                cost = cost +item.UsedCost;

        //                suggestedResale = suggestedResale + item.QuotePrice;
        //            }
        //            //quoteLine.Cost = cost;
        //            quoteLine.ProductName = productName;
        //            quoteLine.Description = description;
        //            quoteLine.SuggestedResale =Convert.ToDecimal(suggestedResale);
        //            quoteLine.UnitPrice = Convert.ToDecimal(suggestedResale);
        //            quoteLine.ExtendedPrice = Convert.ToInt16(quantity) * quoteLine.UnitPrice;
        //            quoteLine.Quantity =Convert.ToInt16(quantity);
        //            quoteLine.JobEstimatorSet = jobEstimatorSet;

        //            var result = UpdateMySurfaceJobEstimatorConfiguration(model, quoteLine,cost);
        //        }

        //        if (model.LaborsList != null)
        //        {
        //            var quoteLine = InsertEmptyQuoteline(model.QuoteKey);
        //            var productName = "";
        //            var description = "";
        //            float cost = 0;
        //            float suggestedResale = 0;
        //            float quantity = 0;
        //            quoteLine.ProductTypeId = 3;// service
        //            quoteLine.ProductCategoryId = GetProductCategory("Labor");
        //            foreach (var item in model.LaborsList)
        //            {
        //                quantity = quantity + item.QuoteHours;

        //                if (productName == "")
        //                    productName = "LABORS: " + item.ProductName;
        //                else if (!productName.Contains(item.ProductName))
        //                    productName = productName + " ," + item.ProductName;

        //                if (description == "")
        //                    description = item.Process;
        //                else
        //                    description = description + " ," + item.Process;

        //                cost = cost + item.LaborCost;

        //                suggestedResale = suggestedResale + item.QuotePrice;
        //            }
        //            quoteLine.ProductName = productName;
        //            quoteLine.Description = description;
        //            quoteLine.SuggestedResale =Convert.ToDecimal( suggestedResale);
        //            quoteLine.UnitPrice = Convert.ToDecimal(suggestedResale);
        //            quoteLine.ExtendedPrice = Convert.ToInt16(quantity) * quoteLine.UnitPrice;
        //            quoteLine.Quantity =Convert.ToInt16(quantity);
        //            quoteLine.JobEstimatorSet = jobEstimatorSet;
        //            var result = UpdateMySurfaceJobEstimatorConfiguration(model, quoteLine,cost);
        //        }
        //    }
        //    return true;
        //}

        public int GetProductCategory(string categoryName)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var query = "select ProductCategoryId from crm.Type_ProductCategory where ProductCategory=@ProductCategory and BrandId =@BrandId";
                    return connection.Query<int>(query, new { ProductCategory = categoryName, BrandId = SessionManager.CurrentFranchise.BrandId }).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public string UpdateMySurfaceJobEstimatorConfiguration(JobEstimator item, QuoteLines quoteLine, float cost)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();

                    var dateTime = DateTime.UtcNow;
                    var user = User.PersonId;

                    var details = quoteLine.QuoteLineDetail;
                    var ql = quoteLine;
                    var orgcost = details.BaseCost;
                    details.BaseCost = Convert.ToDecimal(cost);
                    details.unitPrice = Convert.ToDecimal(quoteLine.UnitPrice);

                    if (orgcost != null && orgcost != Convert.ToDecimal(cost))
                    {
                        var qlcc = new QuoteLineCostChange();
                        qlcc.Cost = Convert.ToDecimal(cost);
                        qlcc.OldCost = orgcost;
                        qlcc.QuoteKey = quoteLine.QuoteKey;
                        qlcc.QuoteLineId = quoteLine.QuoteLineId;
                        qlcc.QuoteLineNumber = quoteLine.QuoteLineNumber;
                        qlcc.EditedBy = User.PersonId;
                        qlcc.EditedOn = DateTime.UtcNow;
                        connection.Insert<QuoteLineCostChange>(qlcc);
                    }
                    //if (orgcost == null && Fproduct.Cost != quotelineobj.Cost)
                    //{
                    //    var qlcc = new QuoteLineCostChange();
                    //    qlcc.Cost = quotelineobj.Cost;
                    //    qlcc.OldCost = Fproduct.Cost;
                    //    qlcc.QuoteKey = quoteline.QuoteKey;
                    //    qlcc.QuoteLineId = quoteline.QuoteLineId;
                    //    qlcc.QuoteLineNumber = quoteline.QuoteLineNumber;
                    //    qlcc.EditedBy = User.PersonId;
                    //    qlcc.EditedOn = DateTime.UtcNow;
                    //    connection.Insert<QuoteLineCostChange>(qlcc);
                    //}

                    // UpdatePricingMgmtDiscount(quotelineobj.PICGroupId, quotelineobj.VendorId, quoteline.QuoteLineId);
                    //if (Franchise.BrandId != 1)
                    //{
                    //    ql.ProductCategoryId = quotelineobj.ProductCategoryId;
                    //    ql.ProductSubCategoryId = quotelineobj.ProductSubcategoryId;
                    //    ql.Discount = quotelineobj.Discount;
                    //}
                    connection.Update(ql);
                    connection.Update(details);
                    return Success;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        /// <summary>
        /// Quote lines validation based on when the product was updated on hfcproducts
        /// </summary>
        /// <param name="QuoteKey"></param>
        /// <returns></returns>
        public List<QuoteLinesVM> QuoteLinesValidation(int QuoteKey)
        {
            var quoteLines = this.GetQuoteLine(QuoteKey);
            foreach (var ql in quoteLines)
            {
                if (ql.ProductTypeId == 1 && ql.ProductUpdatedOn.HasValue && ql.ValidatedOn.HasValue && ql.ValidatedOn.Value < ql.ProductUpdatedOn.Value)
                {
                    ql.Valid = false;
                }
                else
                {
                    ql.Valid = true;
                }
            }

            return quoteLines;
        }

        public bool UpdateQuoteTax(int quoteKey)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();

                    var fQuery = @"SELECT q.* FROM [CRM].[quote] AS q WHERE quotekey = @quotekey;
                                   SELECT o.* FROM crm.Opportunities o INNER JOIN crm.Quote q ON q.OpportunityId = o.OpportunityId WHERE q.QuoteKey=@quotekey;";
                    var fDetails = connection.QueryMultiple(fQuery, new { quotekey = quoteKey });

                    var data = fDetails.Read<Quote>().FirstOrDefault();
                    var opp = fDetails.Read<Opportunity>().FirstOrDefault();

                    //if (opp != null)
                    //{
                    //    data.IsTaxExempt = opp.IsTaxExempt;
                    //    data.TaxExemptID = opp.TaxExemptID;
                    //    data.IsPSTExempt = opp.IsPSTExempt;
                    //    data.IsGSTExempt = opp.IsGSTExempt;
                    //    data.IsHSTExempt = opp.IsHSTExempt;
                    //    data.IsVATExempt = opp.IsVATExempt;
                    //    data.IsNewConstruction = opp.IsNewConstruction;
                    //}

                    connection.Update(data);
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
                return true;
            }
        }

        public bool MarkQuotelinesTaxExempt(List<int> quoteLineIds, bool isTaxExempt)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var datetime = DateTime.UtcNow;
                    var user = User.PersonId;
                    connection.Open();
                    var quotelines = connection.Query<QuoteLines>("select ql.* from crm.quotelines ql inner join crm.quotelinedetail  qd on ql.quotelineid = qd.quotelineid where ql.QuoteLineId in @QuoteLineId", new { QuoteLineId = quoteLineIds }).ToList();
                    foreach (var ql in quotelines)
                    {
                        ql.TaxExempt = isTaxExempt;
                        ql.LastUpdatedOn = datetime;
                        ql.LastUpdatedBy = user;
                    }
                    connection.BulkUpdate(quotelines);
                    return true;
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public bool CopyDeleteQuoteline(List<int> quoteLineId, string copyOrDelete)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var sqlQuery = @"DELETE td from crm.TaxDetails td INNER JOIN crm.Tax t ON t.TaxId = td.TaxId WHERE t.QuoteLineId in @QuoteLineId;
                                     DELETE  FROM crm.Tax WHERE QuoteLineId in @QuoteLineId;
                                     delete crm.QuoteCoreProductDetails where QuoteLineId in @QuoteLineId;
                                     delete crm.DiscountsAndPromo where QuoteLineId in @QuoteLineId;
                                     delete [CRM].[QuoteLineDetail] where QuoteLineId in @QuoteLineId;
                                     delete [CRM].[QuoteLines] where QuoteLineId in @QuoteLineId;
                                     DELETE History.QuoteLineCostChange WHERE QuoteLineId in @QuoteLineId";
                    var insertquoteLinesQuery = @"INSERT INTO CRM.QuoteLines (QuoteKey ,MeasurementsetId ,VendorId ,ProductId ,SuggestedResale ,Discount ,CreatedOn ,CreatedBy ,LastUpdatedOn ,LastUpdatedBy ,IsActive ,Width ,Height ,UnitPrice ,Description ,MountType ,ExtendedPrice ,Memo ,PICJson ,ProductTypeId ,Quntity ,MarkupfactorType ,DiscountType ,PICPriceResponse ,ProductName ,VendorName ,CurrencyExchangeId ,ConversionRate ,VendorCurrency ,ConversionCalculationType) VALUES (@QuoteKey ,@MeasurementsetId ,@VendorId ,@ProductId ,@SuggestedResale ,@Discount ,@CreatedOn ,@CreatedBy ,@LastUpdatedOn ,@LastUpdatedBy ,@IsActive ,@Width ,@Height ,@UnitPrice ,@Quantity ,@Description ,@MountType ,@ExtendedPrice ,@Memo ,@PICJson ,@ProductTypeId ,@MarkupfactorType ,@DiscountType ,@PICPriceResponse ,@ProductName ,@VendorName ,@CurrencyExchangeId ,@ConversionRate ,@VendorCurrency ,@ConversionCalculationType);select cast(scope_identity() as int)";
                    var datetime = DateTime.UtcNow;
                    var user = User.PersonId;
                    var qDetailsQuery = @"INSERT INTO CRM.QuoteLineDetail (QuoteLineId ,Quantity ,BaseCost ,BaseResalePrice ,BaseResalePriceWOPromos ,PricingCode ,PricingRule ,MarkupFactor ,Rounding ,SuggestedResale ,Discount ,unitPrice ,DiscountAmount ,ExtendedPrice ,MarginPercentage ,PricingId ,ProductOrOption ,SelectedPrice ,CreatedOn ,CreatedBy ,LastUpdatedOn ,LastUpdatedBy ,Cost ,NetCharge ,ManualUnitPrice) VALUES (@QuoteLineId ,@Quantity ,@BaseCost ,@BaseResalePrice ,@BaseResalePriceWOPromos ,@PricingCode ,@PricingRule ,@MarkupFactor ,@Rounding ,@SuggestedResale ,@Discount ,@unitPrice ,@DiscountAmount ,@ExtendedPrice ,@MarginPercentage ,@PricingId ,@ProductOrOption ,@SelectedPrice ,@CreatedOn ,@CreatedBy ,@LastUpdatedOn ,@LastUpdatedBy ,@Cost ,@NetCharge ,@ManualUnitPrice);select cast(scope_identity() as int); ";

                    var ql = connection.Query<QuoteLines>("select ql.* from crm.quotelines ql inner join crm.quotelinedetail  qd on ql.quotelineid = qd.quotelineid where ql.QuoteLineId in @QuoteLineId", new { QuoteLineId = quoteLineId }).FirstOrDefault();
                    var qd = connection.Query<QuoteLineDetail>("select qd.* from crm.quotelines ql inner join crm.quotelinedetail  qd on ql.quotelineid = qd.quotelineid where ql.QuoteLineId  in @QuoteLineId", new { QuoteLineId = quoteLineId }).FirstOrDefault();
                    var dnpList = connection.Query<DiscountsAndPromo>("select * from [CRM].[DiscountsAndPromo] where [QuoteLineId] in @QuoteLineId", new { QuoteLineId = quoteLineId }).ToList();
                    var qcpdList = connection.Query<QuoteCoreProductDetails>("select * from [CRM].[QuoteCoreProductDetails] where [QuoteLineId] in @QuoteLineId", new { QuoteLineId = quoteLineId }).ToList();

                    connection.Open();
                    if (copyOrDelete == "COPY")
                    {
                        var quotelines = new QuoteLines();
                        quotelines = ql;
                        quotelines.QuoteLineId = 0;
                        quotelines.CreatedBy = user;
                        quotelines.CreatedOn = datetime;
                        quotelines.LastUpdatedBy = user;
                        quotelines.LastUpdatedOn = datetime;

                        quotelines.QuoteLineId = connection.Insert<int, QuoteLines>(quotelines);

                        if (qd != null)
                        {
                            qd.QuoteLineId = quotelines.QuoteLineId;
                            qd.CreatedBy = user;
                            qd.CreatedOn = datetime;
                            qd.LastUpdatedBy = user;
                            qd.LastUpdatedOn = datetime;
                            int detailid = connection.Insert<int, QuoteLineDetail>(qd);
                        }

                        if (dnpList != null && dnpList.Count > 0)
                        {
                            foreach (var qDnP in dnpList)
                            {
                                if (qDnP != null)
                                {
                                    qDnP.QuoteLineId = quotelines.QuoteLineId;
                                    qDnP.QuoteKey = ql.QuoteKey;
                                    qDnP.CreatedBy = user;
                                    qDnP.CreatedOn = datetime;
                                    qDnP.LastUpdatedBy = user;
                                    qDnP.LastUpdatedOn = datetime;
                                    connection.Insert<DiscountsAndPromo>(qDnP);
                                }
                            }
                        }
                        if (qcpdList != null && qcpdList.Count > 0)
                        {
                            foreach (var qcpd in qcpdList)
                            {
                                if (qcpd != null)
                                {
                                    connection.Insert<QuoteCoreProductDetails>(qcpd);
                                }
                            }
                        }
                    }
                    else
                    {
                        var res = connection.QueryMultiple(sqlQuery, new { QuoteLineId = quoteLineId });
                        //var isSuccess1 = connection.Delete(new QuoteLineDetail { QuoteLineId = quoteLineId });
                    }
                    var response = AddUpdateInsertQuoteLineNumber(ql.QuoteKey);
                    return true;
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    throw;
                    //return false;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool DeleteDiscountAndPromos(int quotelineId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var query =
                        @"DELETE crm.DiscountsAndPromo WHERE QuoteLineId = @QuoteLineId";
                    var fDetails = connection.Query(query, new { quotelineId = quotelineId });
                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool UpdatePricingMgmtDiscount(int prdGrp, int VendorId, int quotelineId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var query =
                        @"select * from [CRM].[PricingSettings] where franchiseid  = @FranchiseId and isActive = 1;select * from crm.quotelines where quotelineId = @quotelineId;select * from crm.quotelinedetail where quotelineId = @quotelineId;";
                    var fDetails = connection.QueryMultiple(query, new { FranchiseId = Franchise.FranchiseId, quotelineId = quotelineId });

                    var priceSettingsList = fDetails.Read<PricingSettings>().ToList();
                    var ql = fDetails.Read<QuoteLines>().FirstOrDefault();
                    var quotelineDetail = fDetails.Read<QuoteLineDetail>().FirstOrDefault();

                    var vendorProductMU = priceSettingsList.Where(
                        x => x.ProductCategoryId.HasValue && x.ProductCategoryId == prdGrp && x.VendorId == VendorId && x.IsActive).FirstOrDefault();
                    var productMU = priceSettingsList.Where(x => x.ProductCategoryId.HasValue && x.ProductCategoryId == prdGrp && x.VendorId == null && x.IsActive).FirstOrDefault();

                    var vendorMU = priceSettingsList.Where(x => x.VendorId == VendorId && x.ProductCategoryId == null && x.IsActive).FirstOrDefault();
                    var franchiseMU = priceSettingsList.Where(x => x.VendorId == null && x.ProductCategoryId == null && x.IsActive).FirstOrDefault();

                    if (vendorProductMU != null)
                    {
                        quotelineDetail.Discount =
                            vendorProductMU.Discount.HasValue ? vendorProductMU.Discount.Value : 0;
                        ql.Discount =
                            vendorProductMU.Discount.HasValue ? vendorProductMU.Discount.Value : 0;
                    }
                    else if (productMU != null)
                    {
                        quotelineDetail.Discount =
                            productMU.Discount.HasValue ? productMU.Discount.Value : 0;
                        ql.Discount =
                            productMU.Discount.HasValue ? productMU.Discount.Value : 0;
                    }
                    else if (vendorMU != null)
                    {
                        quotelineDetail.Discount =
                            vendorMU.Discount.HasValue ? vendorMU.Discount.Value : 0;
                        ql.Discount =
                            vendorMU.Discount.HasValue ? vendorMU.Discount.Value : 0;
                    }
                    else if (franchiseMU != null)
                    {
                        quotelineDetail.Discount =
                            franchiseMU.Discount.HasValue ? franchiseMU.Discount.Value : 0;
                        ql.Discount =
                            franchiseMU.Discount.HasValue ? franchiseMU.Discount.Value : 0;
                    }

                    connection.Update(ql);
                    connection.Update(quotelineDetail);
                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        /// <summary>
        /// To get Core product Descriptions based on the selections and the values in PIC Returns in Visible prompts values when PIC Validates promptAnswers
        /// Do not display the values like "CUSTOMER SELECTED", "SELECT COLOR", "NOT SELECTED", "0 - NONE"
        /// </summary>
        /// <param name="qcpdList"></param>
        /// <returns></returns>
        public string getCoreProductDesciption(List<QuoteCoreProductDetails> qcpdList)
        {
            var list = new List<string>
            {
                "CUSTOMER SELECTED", "SELECT COLOR", "NOT SELECTED", "0 - NONE"
            };
            var desclist = new List<string>
            {
                "WIDTH", "HEIGHT", "ROOM"
            };

            string desc = string.Empty;
            if (qcpdList != null && qcpdList.Count > 0)
            {
                foreach (var it in qcpdList)
                {
                    //if (it.Description.IndexOf("width", StringComparison.OrdinalIgnoreCase) < 0 && it.Description.IndexOf("height", StringComparison.OrdinalIgnoreCase) < 0)
                    //{
                    var d = string.Empty;
                    ///For Measurements there is no Value description So only show Value
                    ///
                    if (it.Description.ToUpper().Trim() != "WIDTH" && it.Description.ToUpper().Trim() != "HEIGHT" && it.Description.ToUpper().Trim() != "ROOM")
                    {
                        if (string.IsNullOrEmpty(it.ValueDescription) && string.IsNullOrEmpty(it.ColorValueDescription))
                        {
                            if (!list.Contains(it.Value.ToUpper()))
                            {
                                d = it.Description + ": <b>" + it.Value + "</b>";
                            }
                        }
                        else if (string.IsNullOrEmpty(it.ValueDescription) && !string.IsNullOrEmpty(it.ColorValueDescription))
                        {
                            if (!list.Contains(it.ColorValueDescription.ToUpper()))
                            {
                                d = it.Description + ": <b>" + it.ColorValueDescription + "</b>";
                            }
                        }
                        else if (!string.IsNullOrEmpty(it.ValueDescription) && string.IsNullOrEmpty(it.ColorValueDescription))
                        {
                            if (!list.Contains(it.ValueDescription.ToUpper()))
                            {
                                d = it.Description + ": <b>" + it.ValueDescription + "</b>";
                            }
                        }
                        else
                        {
                            if (!list.Contains(it.ValueDescription.ToUpper()))
                            {
                                d = "<b>" + it.ValueDescription + "</b>";
                            }
                            if (!list.Contains(it.ColorValueDescription.ToUpper()))
                            {
                                if (string.IsNullOrEmpty(d))
                                {
                                    d = " <b>" + it.ColorValueDescription + "</b>";
                                }
                                else
                                {
                                    d = d + " <b>" + it.ColorValueDescription + "</b>";
                                }
                            }
                            if (d != string.Empty)
                            {
                                d = it.Description + ":" + d;
                            }
                        }
                        if (d != string.Empty)
                        {
                            desc = desc == string.Empty ? d + "; " : desc + d + "; ";
                        }
                    }
                    //}
                }
            }
            return desc;
        }

        public bool updateProductPricing(int QuoteLineId, decimal cost, decimal price, CurrencyExchange currencyExchange, int vendorId, int productId, string selectedPrice, dynamic picResponse, dynamic input, int productTypeId, CoreProductVM coreProductObj = null, bool ignoreTaxUpdate = false)
        {
            //EventLogger.LogEvent("Step 2 of 4: Applying Pricing Rules");
            SessionManager.SetConfigstatus(SessionManager.CurrentUser.PersonId.ToString(), "Step 2 of 4: Applying Pricing Rules");
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var fQuery = @"select * from [CRM].[PricingSettings] where franchiseid  = @FranchiseId and isActive = 1;select ql.* from crm.quotelines ql inner join crm.quotelinedetail  qd on ql.quotelineid = qd.quotelineid where ql.quotelineid = @QuoteLineId;select * from [CRM].[quotelinedetail] where [QuoteLineId] = @QuoteLineId;select q.* from [CRM].[quote] q inner join [CRM].[QuoteLines] ql on ql.QuoteKey = q.quoteKey where ql.QuoteLineId = @QuoteLineId;select * from crm.QuoteCoreProductDetails where QuotelineId = @QuotelineId;";
                    //var oneTimeprdQuery = @"INSERT INTO CRM.FranchiseOneTimeProduct (FranchiseId ,VendorName ,ProductName ,Description ,Cost ,UnitPrice ,CreatedOn ,CreatedBy ,UpdatedOn ,UpdatedBy) VALUES (@FranchiseId ,@VendorName ,@ProductName ,@Description ,@Cost ,@UnitPrice ,@CreatedOn ,@CreatedBy ,@UpdatedOn ,@UpdatedBy);SELECT CAST(SCOPE_IDENTITY() as int);";
                    connection.Open();

                    var fDetails = connection.QueryMultiple(fQuery, new { FranchiseId = this.Franchise.FranchiseId, QuoteLineId = QuoteLineId });

                    var priceSettings = fDetails.Read<PricingSettings>().ToList();
                    var qLinesObj = fDetails.Read<QuoteLines>().FirstOrDefault();
                    var quotelineDetail = fDetails.Read<QuoteLineDetail>().FirstOrDefault();
                    var quote = fDetails.Read<Quote>().FirstOrDefault();
                    var qcpdList = fDetails.Read<QuoteCoreProductDetails>().ToList();
                    var rounding = "N/A";
                    var dateTime = DateTime.UtcNow;
                    var user = User.PersonId;
                    int prdGrp = 0;

                    if (quotelineDetail == null)
                    {
                        quotelineDetail = new QuoteLineDetail();
                        quotelineDetail.QuoteLineId = QuoteLineId;
                        quotelineDetail.Quantity = Convert.ToInt32(input.Quantity);
                    }

                    decimal baseCost = Convert.ToDecimal(cost);
                    decimal BaseResalePrice = Convert.ToDecimal(price);

                    quotelineDetail.BaseCost = baseCost;
                    quotelineDetail.BaseResalePrice = BaseResalePrice;
                    quotelineDetail.BaseResalePriceWOPromos = BaseResalePrice;
                    quotelineDetail.SelectedPrice = selectedPrice == null ? "SuggestedResale" : selectedPrice;
                    if (picResponse != null && ((IDictionary<string, object>)picResponse).ContainsKey("counter"))
                    {
                        quotelineDetail.CounterId = (int)Convert.ToInt64(picResponse.counter);
                        quotelineDetail.LastValidatedon = dateTime;
                        quotelineDetail.ValidConfiguration = true;
                    }

                    qLinesObj.ProductTypeId = productTypeId;
                    ///only specific to Core products of BB
                    if (coreProductObj != null)
                    {
                        qLinesObj.Width = coreProductObj.Width;
                        qLinesObj.FranctionalValueWidth = coreProductObj.FranctionalValueWidth;
                        qLinesObj.Height = coreProductObj.Height;
                        qLinesObj.FranctionalValueHeight = coreProductObj.FranctionalValueHeight;
                        qLinesObj.MountType = coreProductObj.MountType;
                        qLinesObj.RoomName = coreProductObj.RoomName;
                        qLinesObj.MeasurementsetId = coreProductObj.MeasurementSetId;
                        qLinesObj.InternalNotes = coreProductObj.InternalNotes;
                        qLinesObj.ModelDescription = coreProductObj.ModelDescription;

                        if (((IDictionary<string, object>)picResponse).ContainsKey("color"))
                        {
                            if (string.IsNullOrEmpty(picResponse.color.Description))
                            {
                                qLinesObj.Color = coreProductObj.Color;
                            }
                            else
                            {
                                qLinesObj.Color = picResponse.color.Description;
                            }
                        }
                        if (((IDictionary<string, object>)picResponse).ContainsKey("fabric"))
                        {
                            if (string.IsNullOrEmpty(picResponse.fabric.Description))
                            {
                                qLinesObj.Fabric = coreProductObj.Fabric;
                            }
                            else
                            {
                                qLinesObj.Fabric = picResponse.fabric.Description;
                            }
                        }
                        if (productTypeId == 1)
                        {
                            qLinesObj.ProductCategoryId = coreProductObj.ProductCategoryId;
                        }
                    }
                    prdGrp = Convert.ToInt32(input.GGroup);

                    if (productTypeId == 2 || productTypeId == 3)
                    {
                        int prdKey = Convert.ToInt32(input.ProductId);

                        var hp = connection.Query<FranchiseProducts>(" select * from crm.franchiseproducts where productKey =@productKey ", new { productKey = prdKey }).FirstOrDefault();
                        prdGrp = hp.ProductCategory.HasValue ? hp.ProductCategory.Value : prdGrp;
                    }

                    qLinesObj.VendorId = Convert.ToInt32(input.VendorId);
                    var vendorProductMU = priceSettings.Where(
                    x => x.ProductCategoryId.HasValue && x.ProductCategoryId == prdGrp && x.VendorId == qLinesObj.VendorId && x.IsActive).FirstOrDefault();
                    var productMU = priceSettings.Where(x => x.ProductCategoryId.HasValue && x.ProductCategoryId == prdGrp && x.VendorId == null && x.IsActive).FirstOrDefault();
                    var vendorMU = priceSettings.Where(x => x.VendorId == qLinesObj.VendorId && x.ProductCategoryId == null && x.IsActive).FirstOrDefault();
                    var franchiseMU = priceSettings.Where(x => x.VendorId == null && x.ProductCategoryId == null && x.IsActive).FirstOrDefault();
                    rounding = franchiseMU != null ? franchiseMU.PriceRoundingOpt : "N/A";
                    if (qLinesObj.ProductTypeId == 1 || qLinesObj.ProductTypeId == 2 || qLinesObj.ProductTypeId == 5)
                    {
                        if (vendorProductMU != null)
                        {
                            quotelineDetail.Discount =
                                vendorProductMU.Discount.HasValue ? vendorProductMU.Discount.Value : 0;
                            qLinesObj.Discount =
                                vendorProductMU.Discount.HasValue ? vendorProductMU.Discount.Value : 0;
                        }
                        else if (productMU != null)
                        {
                            quotelineDetail.Discount =
                                productMU.Discount.HasValue ? productMU.Discount.Value : 0;
                            qLinesObj.Discount =
                                productMU.Discount.HasValue ? productMU.Discount.Value : 0;
                        }
                        else if (vendorMU != null)
                        {
                            quotelineDetail.Discount =
                                vendorMU.Discount.HasValue ? vendorMU.Discount.Value : 0;
                            qLinesObj.Discount =
                                vendorMU.Discount.HasValue ? vendorMU.Discount.Value : 0;
                        }
                        else if (franchiseMU != null)
                        {
                            quotelineDetail.Discount =
                                franchiseMU.Discount.HasValue ? franchiseMU.Discount.Value : 0;
                            qLinesObj.Discount =
                                franchiseMU.Discount.HasValue ? franchiseMU.Discount.Value : 0;
                        }
                    }

                    //5--> one time product
                    //4--> Discount
                    if (productTypeId != 5 && productTypeId != 4)
                    {
                        //Apply Currency exchange to the base price and cost
                        if (currencyExchange != null)
                        {
                            //1-Multiply
                            //2 - Divide
                            qLinesObj.CurrencyExchangeId = currencyExchange.CurrencyExchangeId;
                            qLinesObj.ConversionRate = currencyExchange.Rate;
                            qLinesObj.VendorCurrency = currencyExchange.CountryFrom;
                            qLinesObj.ConversionCalculationType = currencyExchange.CalculationType;

                            if (currencyExchange.CalculationType == 1)
                            {
                                quotelineDetail.BaseResalePrice = Convert.ToDecimal(BaseResalePrice * currencyExchange.Rate);
                                quotelineDetail.BaseCost = Convert.ToDecimal(baseCost * currencyExchange.Rate);
                                quotelineDetail.BaseResalePriceWOPromos = Convert.ToDecimal(BaseResalePrice * currencyExchange.Rate);
                            }
                            else if (currencyExchange.CalculationType == 2)
                            {
                                quotelineDetail.BaseResalePrice = Convert.ToDecimal(BaseResalePrice / currencyExchange.Rate);
                                quotelineDetail.BaseCost = Convert.ToDecimal(baseCost / currencyExchange.Rate);
                                quotelineDetail.BaseResalePriceWOPromos = Convert.ToDecimal(BaseResalePrice / currencyExchange.Rate);
                            }
                        }

                        //Get all the discounts and promos from PIC
                        if (picResponse != null && picResponse.Promos != null && picResponse.Promos.Count > 0)
                        {
                            var dnpList = new List<DiscountsAndPromo>();
                            foreach (var item in picResponse.Promos)
                            {
                                var dnp = new DiscountsAndPromo();
                                dnp.Discount = Convert.ToDecimal(item.retail) + Convert.ToDecimal(item.surch);
                                //apply currency conversion to Discounts
                                if (currencyExchange != null)
                                {
                                    //1-Multiply
                                    //2 - Divide
                                    if (currencyExchange.CalculationType == 1)
                                    {
                                        dnp.Discount = Convert.ToDecimal(dnp.Discount * currencyExchange.Rate);
                                    }
                                    else if (currencyExchange.CalculationType == 2)
                                    {
                                        dnp.Discount = Convert.ToDecimal(dnp.Discount / currencyExchange.Rate);
                                    }
                                }
                                dnp.Description = item.description;
                                dnp.QuoteLineId = qLinesObj.QuoteLineId;
                                dnp.QuoteKey = qLinesObj.QuoteKey;
                                dnp.CreatedOn = dateTime;
                                dnp.CreatedBy = user;
                                dnp.Cost = Convert.ToDecimal(picResponse.cost);
                                dnpList.Add(dnp);
                                //connection.Insert(dnp);
                            }
                            if (dnpList != null && dnpList.Count > 0)
                            {
                                connection.BulkInsert(dnpList);
                            }
                        }

                        if (productTypeId == 1)
                        {
                            //var newobj = JsonConvert.DeserializeObject<dynamic>(input.PICJson);
                            qLinesObj.ModelDescription = input.ModelDescription;
                        }
                        else if (productTypeId == 2 || productTypeId == 3)
                        {
                            /*var newobj = JsonConvert.DeserializeObject<dynamic>(input.PICJson)*/
                            int prdKey = Convert.ToInt32(input.ProductId);

                            var hp = connection.Query<FranchiseProducts>(" select * from crm.franchiseproducts where productKey =@productKey ", new { productKey = prdKey }).FirstOrDefault();
                            prdGrp = hp.ProductCategory.HasValue ? hp.ProductCategory.Value : 0;
                        }

                        qLinesObj.VendorName = input.VendorName;

                        qLinesObj.ProductName = input.PCategory;
                        if (qLinesObj.ProductTypeId == 1)
                        {
                            string desc = string.Empty;
                            if (!string.IsNullOrEmpty(input.ProductName))
                            {
                                desc = "Product: <b>" + input.ProductName + "</b>; ";
                            }
                            if (!string.IsNullOrEmpty(qLinesObj.ModelDescription) && !string.Equals(qLinesObj.ModelDescription, "ALL", StringComparison.OrdinalIgnoreCase))
                            {
                                desc = desc + "Model: <b>" + qLinesObj.ModelDescription + "</b>; ";
                            }
                            desc = desc + getCoreProductDesciption(qcpdList);

                            if (desc == string.Empty)
                            {
                                qLinesObj.Description = input.Description;
                            }
                            else
                            {
                                qLinesObj.Description = desc;
                            }
                            qLinesObj.ValidatedOn = DateTime.UtcNow;
                        }

                        qLinesObj.ProductId = Convert.ToInt32(input.ProductId);

                        if (vendorProductMU != null)
                        {
                            quotelineDetail.PricingId = vendorProductMU.Id;
                            quotelineDetail.PricingCode = "PD";
                            quotelineDetail.MarkupFactor = vendorProductMU.MarkUp;
                        }
                        else if (productMU != null)
                        {
                            quotelineDetail.PricingId = productMU.Id;
                            quotelineDetail.PricingCode = input.ProductName != null ? input.ProductName.Substring(0, 5).ToUpper() : "";
                            quotelineDetail.MarkupFactor = productMU.MarkUp;
                        }
                        else if (vendorMU != null)
                        {
                            quotelineDetail.PricingCode = "VN";
                            quotelineDetail.PricingId = vendorMU.Id;
                            quotelineDetail.MarkupFactor = vendorMU.MarkUp;
                        }
                        else if (franchiseMU != null)
                        {
                            quotelineDetail.PricingCode = "FE";
                            quotelineDetail.PricingId = franchiseMU.Id;
                            quotelineDetail.MarkupFactor = franchiseMU.MarkUp;
                            quotelineDetail.PricingRule = "Corp";
                        }
                        qLinesObj.UnitPrice = quotelineDetail.unitPrice;
                        qLinesObj.SuggestedResale = quotelineDetail.SuggestedResale;
                    }
                    else
                    {
                        qLinesObj.ProductTypeId = 5;

                        if (franchiseMU != null)
                        {
                            quotelineDetail.PricingCode = "FE";
                            quotelineDetail.PricingId = franchiseMU.Id;
                            quotelineDetail.MarkupFactor = franchiseMU.MarkUp;
                            quotelineDetail.PricingRule = "Corp";
                        }

                        //qLinesObj.ProductId = connection.Query<int>(oneTimeprdQuery, new
                        //{
                        //    FranchiseId = Franchise.FranchiseId,
                        //    VendorName = input.VendorName,
                        //    ProductName = input.ProductName,
                        //    Description = input.Description,
                        //    Cost = cost,
                        //    UnitPrice = price,
                        //    CreatedOn = dateTime,
                        //    CreatedBy = user,
                        //    UpdatedOn = dateTime,
                        //    UpdatedBy = user

                        //}).Single();
                    }

                    quotelineDetail.QuoteLineId = Convert.ToInt32(QuoteLineId);
                    quotelineDetail.Quantity = Convert.ToInt32(input.Quantity);
                    quotelineDetail.Rounding = rounding;
                    qLinesObj.Quantity = quotelineDetail.Quantity;
                    if (picResponse != null)
                    {
                        qLinesObj.PICPriceResponse = JsonConvert.SerializeObject(picResponse);
                    }
                    //set it to false and reset unit price once they come back to Configuration.
                    quotelineDetail.ManualUnitPrice = false;
                    if (quotelineDetail.QuoteLineDetailId == 0)
                    {
                        quotelineDetail.CreatedOn = dateTime;
                        quotelineDetail.CreatedBy = user;
                        quotelineDetail.LastUpdatedOn = dateTime;
                        quotelineDetail.LastUpdatedBy = user;

                        connection.Insert(quotelineDetail);
                    }
                    else
                    {
                        quotelineDetail.LastUpdatedOn = dateTime;
                        quotelineDetail.LastUpdatedBy = user;
                        connection.Update(quotelineDetail);
                    }

                    qLinesObj.LastUpdatedOn = DateTime.UtcNow;
                    // input parameters
                    qLinesObj.Quantity = Convert.ToInt32(input.Quantity);
                    qLinesObj.ExtendedPrice = quotelineDetail.unitPrice * quotelineDetail.Quantity;

                    qLinesObj.ProductTypeId = productTypeId;
                    qLinesObj.PICJson = JsonConvert.SerializeObject(input);
                    qLinesObj.LastUpdatedOn = dateTime;
                    qLinesObj.LastUpdatedBy = user;

                    connection.Update(qLinesObj);
                    //UpdateQuote(qLinesObj.QuoteKey);
                    // SessionManager.SetConfigstatus(SessionManager.CurrentUser.PersonId.ToString(), "");
                    var response = RecalculateQuoteLines(quote.QuoteKey, 0, ignoreTaxUpdate);
                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        // TODO: The following method extracted from the previous one to address
        // specific to TL quote. The above method is called from multiple
        // place which need special attention.
        public bool updateProductPricingTL(int QuoteLineId, decimal cost, decimal price
            , CurrencyExchange currencyExchange, int vendorId, int productId, string selectedPrice
            , dynamic picResponse, dynamic input, int productTypeId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var fQuery = @"select * from [CRM].[Franchise] where [FranchiseId] = @FranchiseId;
                                    select * from [CRM].[PricingSettings]
                                        where franchiseid  = @FranchiseId and isActive = 1;
                                    select ql.* from crm.quotelines ql inner join crm.quotelinedetail  qd on ql.quotelineid = qd.quotelineid where ql.quotelineid = @QuoteLineId;
                                    select qd.* from crm.quotelines ql inner join crm.quotelinedetail  qd on ql.quotelineid = qd.quotelineid where ql.quotelineid = @QuoteLineId;
                                    select q.* from [CRM].[quote] q inner join [CRM].[QuoteLines] ql
                                        on ql.QuoteKey = q.quoteKey
                                        where ql.QuoteLineId = @QuoteLineId;
                                    select * from crm.QuoteCoreProductDetails where QuotelineId = @QuotelineId;";
                    //var oneTimeprdQuery = @"INSERT INTO CRM.FranchiseOneTimeProduct (FranchiseId ,VendorName ,ProductName ,Description ,Cost ,UnitPrice ,CreatedOn ,CreatedBy ,UpdatedOn ,UpdatedBy) VALUES (@FranchiseId ,@VendorName ,@ProductName ,@Description ,@Cost ,@UnitPrice ,@CreatedOn ,@CreatedBy ,@UpdatedOn ,@UpdatedBy);SELECT CAST(SCOPE_IDENTITY() as int);";
                    connection.Open();

                    var fDetails = connection.QueryMultiple(fQuery, new { FranchiseId = this.Franchise.FranchiseId, QuoteLineId = QuoteLineId });

                    var Franchise = fDetails.Read<Franchise>().FirstOrDefault();
                    var priceSettings = fDetails.Read<PricingSettings>().ToList();
                    var qLinesObj = fDetails.Read<QuoteLines>().FirstOrDefault();
                    var quotelineDetail = fDetails.Read<QuoteLineDetail>().FirstOrDefault();
                    var quote = fDetails.Read<Quote>().FirstOrDefault();
                    var qcpdList = fDetails.Read<QuoteCoreProductDetails>().ToList();
                    var rounding = "N/A";
                    var dateTime = DateTime.UtcNow;
                    var user = User.PersonId;
                    int prdGrp = 0;

                    if (quotelineDetail == null)
                    {
                        quotelineDetail = new QuoteLineDetail();
                        quotelineDetail.QuoteLineId = QuoteLineId;
                        quotelineDetail.Quantity = Convert.ToInt32(input.Quantity);
                    }

                    decimal baseCost = Convert.ToDecimal(cost);
                    decimal BaseResalePrice = Convert.ToDecimal(price);

                    quotelineDetail.BaseCost = baseCost;
                    quotelineDetail.BaseResalePrice = BaseResalePrice;
                    quotelineDetail.BaseResalePriceWOPromos = BaseResalePrice;
                    quotelineDetail.SelectedPrice = selectedPrice == null ? "SuggestedResale" : selectedPrice;
                    qLinesObj.ProductTypeId = productTypeId;
                    //CurrencyExchange currencyExchange = GetCurrencyConversionPICVendor(Franchise.FranchiseId, vendorCurrency);

                    //5--> one time product
                    //4--> Discount
                    if (productTypeId != 5 && productTypeId != 4)
                    {
                        //Apply Currency exchange to the base price and cost
                        if (currencyExchange != null)
                        {
                            //1-Multiply
                            //2 - Divide
                            qLinesObj.CurrencyExchangeId = currencyExchange.CurrencyExchangeId;
                            qLinesObj.ConversionRate = currencyExchange.Rate;
                            qLinesObj.VendorCurrency = currencyExchange.CountryFrom;
                            qLinesObj.ConversionCalculationType = currencyExchange.CalculationType;

                            if (currencyExchange.CalculationType == 1)
                            {
                                quotelineDetail.BaseResalePrice = Convert.ToDecimal(quotelineDetail.BaseResalePrice * currencyExchange.Rate);
                                quotelineDetail.BaseCost = Convert.ToDecimal(quotelineDetail.BaseCost * currencyExchange.Rate);
                            }
                            else if (currencyExchange.CalculationType == 2)
                            {
                                quotelineDetail.BaseResalePrice = Convert.ToDecimal(quotelineDetail.BaseResalePrice / currencyExchange.Rate);
                                quotelineDetail.BaseCost = Convert.ToDecimal(quotelineDetail.BaseCost / currencyExchange.Rate);
                            }
                        }

                        //Get all the discounts and promos from PIC
                        if (picResponse != null && picResponse.Promos != null && picResponse.Promos.Count > 0)
                        {
                            foreach (var item in picResponse.Promos)
                            {
                                var dnp = new DiscountsAndPromo();
                                dnp.Discount = Convert.ToDecimal(picResponse.Promos.retail) + Convert.ToDecimal(picResponse.Promos.surch);
                                //apply currency conversion to Discounts
                                if (currencyExchange != null)
                                {
                                    //1-Multiply
                                    //2 - Divide
                                    if (currencyExchange.CalculationType == 1)
                                    {
                                        dnp.Discount = Convert.ToDecimal(dnp.Discount * currencyExchange.Rate);
                                    }
                                    else if (currencyExchange.CalculationType == 2)
                                    {
                                        dnp.Discount = Convert.ToDecimal(dnp.Discount / currencyExchange.Rate);
                                    }
                                }
                                dnp.Description = item.description;
                                dnp.QuoteLineId = qLinesObj.QuoteLineId;
                                dnp.QuoteKey = qLinesObj.QuoteKey;
                                connection.Insert(dnp);
                            }
                        }

                        if (productTypeId == 1)
                        {
                            //var newobj = JsonConvert.DeserializeObject<dynamic>(input.PICJson);
                            prdGrp = Convert.ToInt32(input.GGroup);
                        }
                        else if (productTypeId == 2 || productTypeId == 3)
                        {
                            /*var newobj = JsonConvert.DeserializeObject<dynamic>(input.PICJson)*/
                            ;
                            int prdKey = Convert.ToInt32(input.ProductId);
                            // select * from crm.franchiseproducts where productKey =@productKey
                            string query = @"select * from crm.franchiseproducts where  productKey =@productKey ";

                            var hp = connection.Query<FranchiseProducts>(query, new { productKey = prdKey }).FirstOrDefault();
                            prdGrp = hp.ProductCategory.HasValue ? hp.ProductCategory.Value : 0;
                        }

                        qLinesObj.VendorId = Convert.ToInt32(input.VendorId);
                        qLinesObj.VendorName = input.VendorName;

                        // TODO: No Need to update the Product Category and ProductName
                        //qLinesObj.ProductName = input.PCategory;
                        qLinesObj.ProductName = input.ProductName;

                        if (qLinesObj.ProductTypeId == 1)
                        {
                            string desc = string.Empty;
                            if (!string.IsNullOrEmpty(input.ProductName))
                            {
                                desc = "Product - " + input.ProductName + "; ";
                            }
                            if (!string.IsNullOrEmpty(qLinesObj.ModelDescription) && !string.Equals(qLinesObj.ModelDescription, "ALL", StringComparison.OrdinalIgnoreCase))
                            {
                                desc = desc + "Model - " + qLinesObj.ModelDescription + "; ";
                            }
                            desc = desc + getCoreProductDesciption(qcpdList);

                            if (desc == string.Empty)
                            {
                                qLinesObj.Description = input.Description;
                            }
                            else
                            {
                                qLinesObj.Description = desc;
                            }
                        }

                        qLinesObj.ProductId = Convert.ToInt32(input.ProductId);

                        var vendorProductMU = priceSettings.Where(
                            x => x.ProductCategoryId.HasValue && x.ProductCategoryId == prdGrp && x.VendorId == qLinesObj.VendorId && x.IsActive).FirstOrDefault();
                        var productMU = priceSettings.Where(x => x.ProductCategoryId.HasValue && x.ProductCategoryId == prdGrp && x.IsActive).FirstOrDefault();
                        var vendorMU = priceSettings.Where(x => x.VendorId == qLinesObj.VendorId && x.ProductCategoryId == null && x.IsActive).FirstOrDefault();
                        var franchiseMU = priceSettings.Where(x => x.VendorId == null && x.ProductCategoryId == null && x.IsActive).FirstOrDefault();
                        rounding = franchiseMU != null ? franchiseMU.PriceRoundingOpt : "N/A";
                        if (vendorProductMU != null)
                        {
                            quotelineDetail.PricingId = vendorProductMU.Id;
                            quotelineDetail.PricingCode = "PD";
                            quotelineDetail.MarkupFactor = vendorProductMU.MarkUp;
                        }
                        else if (productMU != null)
                        {
                            quotelineDetail.PricingId = productMU.Id;
                            quotelineDetail.PricingCode = input.ProductName != null ? input.ProductName.Substring(0, 5).ToUpper() : "";
                            quotelineDetail.MarkupFactor = productMU.MarkUp;
                        }
                        else if (vendorMU != null)
                        {
                            quotelineDetail.PricingCode = "VN";
                            quotelineDetail.PricingId = vendorMU.Id;
                            quotelineDetail.MarkupFactor = vendorMU.MarkUp;
                        }
                        else if (franchiseMU != null)
                        {
                            quotelineDetail.PricingCode = "FE";
                            quotelineDetail.PricingId = franchiseMU.Id;
                            quotelineDetail.MarkupFactor = franchiseMU.MarkUp;
                            quotelineDetail.PricingRule = "Corp";
                        }
                        qLinesObj.UnitPrice = quotelineDetail.unitPrice;
                        qLinesObj.SuggestedResale = quotelineDetail.SuggestedResale;
                    }
                    else
                    {
                        qLinesObj.ProductTypeId = 5;

                        var franchiseMU = priceSettings.Where(x => x.VendorId == null && x.ProductCategoryId == null).FirstOrDefault();
                        if (franchiseMU != null)
                        {
                            quotelineDetail.PricingCode = "FE";
                            quotelineDetail.PricingId = franchiseMU.Id;
                            quotelineDetail.MarkupFactor = franchiseMU.MarkUp;
                            quotelineDetail.PricingRule = "Corp";
                        }

                        //qLinesObj.ProductId = connection.Query<int>(oneTimeprdQuery, new
                        //{
                        //    FranchiseId = Franchise.FranchiseId,
                        //    VendorName = input.VendorName,
                        //    ProductName = input.ProductName,
                        //    Description = input.Description,
                        //    Cost = cost,
                        //    UnitPrice = price,
                        //    CreatedOn = dateTime,
                        //    CreatedBy = user,
                        //    UpdatedOn = dateTime,
                        //    UpdatedBy = user

                        //}).Single();
                    }

                    quotelineDetail.QuoteLineId = Convert.ToInt32(QuoteLineId);
                    quotelineDetail.Quantity = Convert.ToInt32(input.Quantity);
                    quotelineDetail.Rounding = rounding;
                    qLinesObj.Quantity = quotelineDetail.Quantity;
                    if (picResponse != null)
                    {
                        qLinesObj.PICPriceResponse = JsonConvert.SerializeObject(picResponse);
                    }
                    //set it to false and reset unit price once they come back to Configuration.
                    //quotelineDetail.ManualUnitPrice = false;
                    if (quotelineDetail.QuoteLineDetailId == 0)
                    {
                        quotelineDetail.CreatedOn = dateTime;
                        quotelineDetail.CreatedBy = user;
                        quotelineDetail.LastUpdatedOn = dateTime;
                        quotelineDetail.LastUpdatedBy = user;

                        connection.Insert(quotelineDetail);
                    }
                    else
                    {
                        quotelineDetail.LastUpdatedOn = dateTime;
                        quotelineDetail.LastUpdatedBy = user;
                        connection.Update(quotelineDetail);
                    }

                    qLinesObj.LastUpdatedOn = DateTime.UtcNow;
                    // input parameters
                    qLinesObj.Quantity = Convert.ToInt32(input.Quantity);
                    qLinesObj.ExtendedPrice = quotelineDetail.unitPrice * quotelineDetail.Quantity;

                    qLinesObj.ProductTypeId = productTypeId;
                    qLinesObj.PICJson = JsonConvert.SerializeObject(input);
                    qLinesObj.LastUpdatedOn = dateTime;
                    qLinesObj.LastUpdatedBy = user;

                    connection.Update(qLinesObj);

                    //TODO: Do we need a special method for TL???
                    var response = RecalculateQuoteLines(quote.QuoteKey, QuoteLineId);
                    UpdateQuote(qLinesObj.QuoteKey);

                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void SaveDataInTables(DataTable dataTable, string tablename)
        {
            if (dataTable.Rows.Count > 0)
            {
                using (SqlConnection con = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                    {
                        sqlBulkCopy.DestinationTableName = tablename;
                        con.Open();
                        sqlBulkCopy.WriteToServer(dataTable);
                        con.Close();
                    }
                }
            }
        }

        public bool UpdateCoreProductQuoteconfiguration(CoreProductVM input, dynamic picResponse)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();

                    //var qcpdList = connection
                    //    .Query<QuoteCoreProductDetails>("select* from crm.QuoteCoreProductDetails where QuotelineId = @QuotelineId",
                    //        new { QuoteLineId = input.QuotelineId }).ToList();
                    var str = JsonConvert.SerializeObject(picResponse);

                    CurrencyExchange currencyExchange =
                       GetCurrencyConversion(Franchise.FranchiseId,
                           input.VendorId);
                    var vsiblepmtsdynamic = JsonConvert.DeserializeObject<dynamic>(str);

                    if (vsiblepmtsdynamic.visiblePrompts != null)
                    {
                        var query =
                            @"DELETE crm.QuoteCoreProductDetails WHERE QuoteLineId = @QuoteLineId";
                        var fDetails = connection.Query(query, new { quotelineId = input.QuotelineId });

                        var obj = JsonConvert.DeserializeObject<Dictionary<string, VisiblePrompt>>(
                            JsonConvert.SerializeObject(vsiblepmtsdynamic.visiblePrompts));
                        var qcpdlist = new List<QuoteCoreProductDetails>();
                        foreach (var key in obj.Keys)
                        {
                            var newobj = obj[key];

                            var qcpd = new QuoteCoreProductDetails();

                            //var exqcpd = qcpdList.Where(x => x.Description == newobj.Description).FirstOrDefault();

                            //if (exqcpd != null)
                            //{
                            //    exqcpd.Value = newobj.Value;
                            //    exqcpd.ColorValue = newobj.ColorValue;
                            //    exqcpd.Description = newobj.Description;
                            //    exqcpd.ValueDescription = newobj.ValueDescription;
                            //    exqcpd.ColorValueDescription = newobj.ColorValueDescription;
                            //    connection.Update(exqcpd);
                            //}
                            //else
                            //{
                            qcpd.Value = newobj.Value;
                            qcpd.ColorValue = newobj.ColorValue;
                            qcpd.Description = newobj.Description;
                            qcpd.ValueDescription = newobj.ValueDescription;
                            qcpd.ColorValueDescription = newobj.ColorValueDescription;
                            qcpd.QuotelineId = input.QuotelineId;
                            qcpd.QuoteKey = input.QuoteKey;
                            qcpdlist.Add(qcpd);
                            //connection.Insert(qcpd);
                            //}
                        }
                        if (qcpdlist != null && qcpdlist.Count > 0)
                        {
                            connection.BulkInsert(qcpdlist);
                        }
                    }

                    DeleteDiscountAndPromos(input.QuotelineId);
                    //UpdatePricingMgmtDiscount(input.GGroup, input.VendorId, input.QuotelineId);

                    var result = updateProductPricing(Convert.ToInt32(input.QuotelineId),
                        // Convert.ToDecimal(picResponse.cost),
                        picResponse.GetType().GetProperty("CostAfterPromo") != null ?
                        (Convert.ToDecimal(picResponse.CostAfterPromo) > 0 ? Convert.ToDecimal(picResponse.CostAfterPromo) : Convert.ToDecimal(picResponse.cost)) :
                        Convert.ToDecimal(picResponse.cost),
                        Convert.ToDecimal(picResponse.list), currencyExchange,
                        Convert.ToInt32(input.VendorId), Convert.ToInt32(input.ProductId), "SuggestedResale",
                        picResponse, input, 1, input);

                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public string UpdateQuoteConfiguration(MyVendorProductVM quotelineobj)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var oneTimeprdQuery = @"INSERT INTO CRM.FranchiseOneTimeProduct (FranchiseId ,VendorName ,ProductName ,Description ,Cost ,UnitPrice ,CreatedOn ,CreatedBy ,UpdatedOn ,UpdatedBy) VALUES (@FranchiseId ,@VendorName ,@ProductName ,@Description ,@Cost ,@UnitPrice ,@CreatedOn ,@CreatedBy ,@UpdatedOn ,@UpdatedBy);SELECT CAST(SCOPE_IDENTITY() as int);";

                    var fQuery = "select * from crm.quotelinedetail where quotelineid = @quotelineid;select * from crm.quotelines where quotelineid = @quotelineid";

                    connection.Open();
                    //var obj = JsonConvert.DeserializeObject(myProductConfiguration);
                    //var test = myProductConfiguration.Replace("'", "O\u0027");
                    //var obj =  JsonConvert.DeserializeAnonymousType(myProductConfiguration, objgjhg);
                    //dynamic quotelineobj =  JsonConvert.DeserializeObject<MyVendorVM>(myProductConfiguration);
                    //dynamic quotelineobj = JsonConvert.DeserializeObject<ExpandoObject>(myProductConfiguration);

                    dynamic inputobj = new ExpandoObject();
                    var dateTime = DateTime.UtcNow;
                    var user = User.PersonId;
                    var Description = "";
                    decimal cost = 0, price = 0;
                    Int32 productId = 0, VendorID = 0;

                    var fDetails = connection.QueryMultiple(fQuery, new { quotelineid = Convert.ToInt32(quotelineobj.QuotelineId) });

                    var details = fDetails.Read<QuoteLineDetail>().First();
                    var ql = fDetails.Read<QuoteLines>().First();

                    if (Convert.ToInt32(quotelineobj.ProductTypeId) == 5 && quotelineobj.Vendor != null && quotelineobj.Vendor != "")
                    {
                        var ress = connection.Query(@"
                                            select fv.VendorId,hfc.Name from Acct.FranchiseVendors fv
                                            join Acct.HFCVendors hfc on fv.VendorId=hfc.VendorId
                                            where fv.VendorType=1 and fv.FranchiseId=2 and fv.VendorStatus=1 and  hfc.Name =@VendorName",
                                             new { VendorName = quotelineobj.Vendor }).FirstOrDefault();
                        if (ress != null) ql.VendorId = quotelineobj.VendorId = ress.VendorId;
                        else ql.VendorId = quotelineobj.VendorId = 0;
                    }

                    ql.VendorName = quotelineobj.Vendor;
                    ql.ProductName = quotelineobj.ProductName;
                    ql.Description = quotelineobj.Description;
                    ql.UnitPrice = Convert.ToDecimal(quotelineobj.UnitPrice);

                    details.BaseCost = Convert.ToDecimal(quotelineobj.Cost);
                    details.unitPrice = Convert.ToDecimal(quotelineobj.UnitPrice);

                    inputobj.VendorName = quotelineobj.Vendor;
                    inputobj.ProductName = quotelineobj.ProductName;
                    inputobj.PCategory = quotelineobj.ProductCategory;
                    inputobj.PicVendor = "";
                    inputobj.PicProduct = quotelineobj.PICProductId;
                    inputobj.ProductId = quotelineobj.ProductId;
                    inputobj.ModelId = "";
                    inputobj.GGroup = quotelineobj.PICGroupId;
                    inputobj.QuoteLineId = quotelineobj.QuotelineId;
                    inputobj.Quantity = quotelineobj.Quantity;
                    inputobj.VendorId = quotelineobj.VendorId;

                    if (Convert.ToInt32(quotelineobj.ProductTypeId) == 5)
                    {
                        cost = Convert.ToDecimal(quotelineobj.Cost);
                        if (Franchise.BrandId == 2)
                        {
                            if (quotelineobj.BaseResalePriceWOPromos.HasValue)
                            {
                                price = Convert.ToDecimal(quotelineobj.BaseResalePriceWOPromos.Value);
                            }
                            else
                            {
                                price = Convert.ToDecimal(quotelineobj.UnitPrice);
                            }
                        }
                        else
                        {
                            price = Convert.ToDecimal(quotelineobj.UnitPrice);
                        }

                        productId = Convert.ToInt32(inputobj.ProductId);
                        VendorID = Convert.ToInt32(inputobj.VendorId);
                        Description = quotelineobj.Description;
                        details.BaseResalePriceWOPromos = quotelineobj.BaseResalePriceWOPromos;
                        details.BaseResalePrice = quotelineobj.BaseResalePriceWOPromos;

                        if (quotelineobj.ProductId > 0)
                        {
                            productId = connection.Query<int>(oneTimeprdQuery, new
                            {
                                FranchiseId = Franchise.FranchiseId,
                                VendorName = inputobj.VendorName,
                                ProductName = inputobj.ProductName,
                                Description = Description,
                                Cost = cost,
                                UnitPrice = price,
                                CreatedOn = dateTime,
                                CreatedBy = Convert.ToInt32(user),
                                UpdatedOn = dateTime,
                                UpdatedBy = Convert.ToInt32(user)
                            }).Single();

                            inputobj.ProductId = productId;
                            ql.ProductId = productId;
                        }
                    }
                    else
                    {
                        var Fproduct = connection.Query<FranchiseProducts>("select * from [CRM].[franchiseproducts] where [ProductKey] = @ProductId", new { ProductId = quotelineobj.ProductId }).FirstOrDefault();

                        cost = Convert.ToDecimal(Fproduct.Cost);
                        price = Convert.ToDecimal(Fproduct.SalePrice);
                        productId = Convert.ToInt32(quotelineobj.ProductKey);
                        VendorID = Convert.ToInt32(quotelineobj.VendorId);
                    }

                    ql.ProductTypeId = Convert.ToInt32(quotelineobj.ProductTypeId);

                    //UpdatePricingMgmtDiscount(quotelineobj.PICGroupId, quotelineobj.VendorId, quotelineobj.QuotelineId);
                    if (Franchise.BrandId != 1)
                    {
                        ql.ProductCategoryId = quotelineobj.ProductCategoryId;
                        ql.ProductSubCategoryId = quotelineobj.ProductSubcategoryId;
                        ql.Discount = quotelineobj.Discount;
                    }
                    connection.Update(ql);
                    connection.Update(details);
                    CurrencyExchange currencyExchange = GetCurrencyConversion(Franchise.FranchiseId, Convert.ToInt32(quotelineobj.VendorId));

                    var result = updateProductPricing(Convert.ToInt32(quotelineobj.QuotelineId), cost, price, currencyExchange, VendorID, productId, "SuggestedResale", null, inputobj, Convert.ToInt32(quotelineobj.ProductTypeId));
                    if (result)
                    {
                        return Success;
                    }
                    else
                    {
                        return FailedSaveChanges;
                    }
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        // TODO: I branched this from the previous because it was consumed by
        // both TL and others and i am not sure how other works at this
        // point of time.
        public string UpdateQuoteConfigurationTL(MyVendorProductVM quotelineobj)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var oneTimeprdQuery = @"INSERT INTO CRM.FranchiseOneTimeProduct (FranchiseId ,VendorName ,ProductName ,Description ,Cost ,UnitPrice ,CreatedOn ,CreatedBy ,UpdatedOn ,UpdatedBy) VALUES (@FranchiseId ,@VendorName ,@ProductName ,@Description ,@Cost ,@UnitPrice ,@CreatedOn ,@CreatedBy ,@UpdatedOn ,@UpdatedBy);SELECT CAST(SCOPE_IDENTITY() as int);";
                    var fQuery = "select * from crm.quotelinedetail where quotelineid = @quotelineid;select * from crm.quotelines where quotelineid = @quotelineid";
                    connection.Open();

                    dynamic inputobj = new ExpandoObject();
                    var dateTime = DateTime.UtcNow;
                    var user = User.PersonId;
                    var Description = "";
                    decimal cost = 0, price = 0;
                    Int32 productId = 0, VendorID = 0;

                    var fDetails = connection.QueryMultiple(fQuery, new { quotelineid = Convert.ToInt32(quotelineobj.QuotelineId) });

                    var details = fDetails.Read<QuoteLineDetail>().First();
                    var ql = fDetails.Read<QuoteLines>().First();
                    details.ManualUnitPrice = quotelineobj.ManualUnitPrice;
                    if (ql.ProductTypeId != quotelineobj.ProductTypeId)
                    {
                        details.ManualUnitPrice = false;
                    }
                    //if (quotelineobj.ManualUnitPrice)
                    //{
                    //    details.ManualUnitPrice = true;
                    //}
                    //else
                    //{
                    //        details.ManualUnitPrice = false;
                    //    }
                    //    else
                    //    {
                    //        if (details.unitPrice == quotelineobj.UnitPrice)
                    //            details.ManualUnitPrice = true;
                    //        }
                    //    }
                    //}
                    ql.VendorName = quotelineobj.Vendor;
                    ql.ProductName = quotelineobj.ProductName;
                    ql.Description = quotelineobj.Description;
                    //if (details.ManualUnitPrice)
                    ql.UnitPrice = Convert.ToDecimal(quotelineobj.UnitPrice);

                    details.BaseCost = Convert.ToDecimal(quotelineobj.Cost);
                    //if (details.ManualUnitPrice)
                    details.unitPrice = Convert.ToDecimal(quotelineobj.UnitPrice);
                    if (quotelineobj.SuggestedResale.HasValue)
                    {
                        details.SuggestedResale = Convert.ToDecimal(quotelineobj.SuggestedResale.Value);
                    }

                    inputobj.VendorName = quotelineobj.Vendor;
                    inputobj.ProductName = quotelineobj.ProductName;
                    inputobj.PCategory = quotelineobj.ProductCategory;
                    inputobj.PicVendor = "";
                    inputobj.PicProduct = quotelineobj.PICProductId;
                    inputobj.ProductId = quotelineobj.ProductId;
                    inputobj.ModelId = "";
                    inputobj.GGroup = quotelineobj.PICGroupId;
                    inputobj.QuoteLineId = quotelineobj.QuotelineId;
                    inputobj.Quantity = quotelineobj.Quantity;
                    inputobj.VendorId = quotelineobj.VendorId;

                    if (Convert.ToInt32(quotelineobj.ProductTypeId) == 5)
                    {
                        cost = Convert.ToDecimal(quotelineobj.Cost);
                        if (Franchise.BrandId != 1)
                        {
                            if (quotelineobj.BaseResalePriceWOPromos.HasValue)
                            {
                                price = Convert.ToDecimal(quotelineobj.BaseResalePriceWOPromos.Value);
                            }
                            else
                            {
                                price = Convert.ToDecimal(quotelineobj.UnitPrice);
                            }
                        }
                        else
                        {
                            price = Convert.ToDecimal(quotelineobj.UnitPrice);
                        }

                        productId = Convert.ToInt32(inputobj.ProductId);
                        VendorID = Convert.ToInt32(inputobj.VendorId);
                        Description = quotelineobj.Description;
                        details.BaseResalePriceWOPromos = quotelineobj.BaseResalePriceWOPromos;
                        details.BaseResalePrice = quotelineobj.BaseResalePriceWOPromos;

                        if (quotelineobj.ProductId > 0)
                        {
                            productId = connection.Query<int>(oneTimeprdQuery, new
                            {
                                FranchiseId = Franchise.FranchiseId,
                                VendorName = inputobj.VendorName,
                                ProductName = inputobj.ProductName,
                                Description = Description,
                                Cost = cost,
                                UnitPrice = price,
                                CreatedOn = dateTime,
                                CreatedBy = Convert.ToInt32(user),
                                UpdatedOn = dateTime,
                                UpdatedBy = Convert.ToInt32(user)
                            }).Single();

                            inputobj.ProductId = productId;
                            ql.ProductId = productId;
                        }
                    }
                    else
                    {
                        var querey = @"select * from [CRM].[franchiseproducts]
                                        where [ProductKey] = @ProductId";
                        var Fproduct = connection.Query<FranchiseProducts>(querey, new { ProductId = quotelineobj.ProductKey }).FirstOrDefault();

                        cost = Convert.ToDecimal(Fproduct.Cost);
                        price = Convert.ToDecimal(Fproduct.SalePrice);
                        productId = Convert.ToInt32(quotelineobj.ProductKey);
                        VendorID = Convert.ToInt32(quotelineobj.VendorId);
                    }

                    ql.ProductTypeId = Convert.ToInt32(quotelineobj.ProductTypeId);
                    //UpdatePricingMgmtDiscount(quotelineobj.PICGroupId, quotelineobj.VendorId, quotelineobj.QuotelineId);
                    if (Franchise.BrandId != 1)
                    {
                        ql.ProductCategoryId = quotelineobj.ProductCategoryId;
                        ql.ProductSubCategoryId = quotelineobj.ProductSubcategoryId;
                        ql.Discount = quotelineobj.Discount;

                        // TODO: don't we need this.
                        ql.ProductId = quotelineobj.ProductId;
                    }
                    connection.Update(ql);
                    connection.Update(details);
                    CurrencyExchange currencyExchange = GetCurrencyConversion(Franchise.FranchiseId, Convert.ToInt32(quotelineobj.VendorId));

                    var result = updateProductPricingTL(Convert.ToInt32(quotelineobj.QuotelineId), cost, price
                        , currencyExchange, VendorID, productId, "SuggestedResale", null
                        , inputobj, Convert.ToInt32(quotelineobj.ProductTypeId));
                    if (result)
                    {
                        return Success;
                    }
                    else
                    {
                        return FailedSaveChanges;
                    }
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool updateQuotelineFromConfig(QuoteLinesVM quoteline)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();

                    var ql = connection.Get<QuoteLines>(quoteline.QuoteLineId);

                    ql.Width = quoteline.Width;
                    ql.FranctionalValueWidth = quoteline.FranctionalValueWidth;
                    ql.Height = quoteline.Height;
                    ql.FranctionalValueHeight = quoteline.FranctionalValueHeight;
                    ql.MountType = quoteline.MountType;
                    ql.RoomName = quoteline.RoomName;
                    ql.InternalNotes = quoteline.InternalNotes;
                    ql.Color = quoteline.Color;
                    ql.Fabric = quoteline.Fabric;

                    if (ql.MeasurementsetId == null && quoteline.MeasurementsetId != null)
                    {
                        ql.MeasurementsetId = quoteline.MeasurementsetId;
                    }

                    var res = connection.Update(ql);

                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool updatequoteLineNumber(List<QuoteLinesVM> QuoteLines)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var fQuery = "select * from crm.quotelinedetail where quotelineid = @quotelineid;select * from crm.quotelines where quotelineid = @quotelineid";

                    connection.Open();

                    for (var i = 0; i < QuoteLines.Count; i++)
                    {
                        var fDetails = connection.QueryMultiple(fQuery, new { quotelineid = QuoteLines[i].QuoteLineId });

                        var details = fDetails.Read<QuoteLineDetail>().First();
                        var ql = fDetails.Read<QuoteLines>().First();

                        ql.QuoteLineNumber = QuoteLines[i].QuoteLineNumber;
                        connection.Update(ql);
                    }
                    connection.Close();
                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
            }
        }

        public bool updatequoteinlineEdit(QuoteLinesVM quoteline, bool fromGroupDiscount = false)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var fQuery = "select * from crm.quotelinedetail where quotelineid = @quotelineid;select * from crm.quotelines where quotelineid = @quotelineid";

                    connection.Open();

                    #region quoteDetails

                    var fDetails = connection.QueryMultiple(fQuery, new { quotelineid = quoteline.QuoteLineId });

                    var details = fDetails.Read<QuoteLineDetail>().First();
                    var ql = fDetails.Read<QuoteLines>().First();

                    #endregion quoteDetails

                    ql.Description = quoteline.Description;
                    ql.RoomName = quoteline.RoomName;
                    ql.WindowLocation = quoteline.WindowLocation;
                    ql.InternalNotes = quoteline.InternalNotes;
                    ql.VendorNotes = quoteline.VendorNotes;
                    ql.CustomerNotes = quoteline.CustomerNotes;
                    details.ManualUnitPrice = quoteline.ManualUnitPrice;
                    if (quoteline.ProductTypeId == 4)
                    {
                        details.Discount = quoteline.Discount;
                        ql.Discount = quoteline.Discount;
                        ql.ProductId = quoteline.ProductId;
                        ql.DiscountType = quoteline.DiscountType;
                        ql.ProductTypeId = quoteline.ProductTypeId;
                        ql.ProductName = quoteline.ProductName;
                    }
                    else
                    {
                        ql.IsGroupDiscountApplied = quoteline.IsGroupDiscountApplied;

                        if (ql.UnitPrice != quoteline.UnitPrice)
                        {
                            //details.ManualUnitPrice = true;
                            ql.UnitPrice = quoteline.UnitPrice;
                            details.unitPrice = quoteline.UnitPrice;
                            if (ql.IsGroupDiscountApplied)
                                ql.IsGroupDiscountApplied = false;
                        }

                        if (quoteline.Width != ql.Width)
                        {
                            ql.Width = quoteline.Width;
                        }
                        if (quoteline.Height != ql.Height)
                        {
                            ql.Height = quoteline.Height;
                        }
                        if (ql.Discount != quoteline.Discount)
                        {
                            ql.Discount = quoteline.Discount;
                            if (ql.IsGroupDiscountApplied)
                            {
                                var AppliedGroupDiscountAmount = GetAppliedDiscountAmount(quoteline.QuoteLineId);

                                if (AppliedGroupDiscountAmount != ql.Discount)
                                    ql.IsGroupDiscountApplied = false;
                            }

                        }
                        if (ql.FranctionalValueWidth != quoteline.FranctionalValueWidth)
                        {
                            ql.FranctionalValueWidth = quoteline.FranctionalValueWidth;
                        }
                        if (ql.FranctionalValueHeight != quoteline.FranctionalValueHeight)
                        {
                            ql.FranctionalValueHeight = quoteline.FranctionalValueHeight;
                        }

                        if (quoteline.Quantity != ql.Quantity)
                        {
                            ql.Quantity = quoteline.Quantity;
                            details.Quantity = quoteline.Quantity.HasValue ? quoteline.Quantity.Value : 0;

                            if (!string.IsNullOrEmpty(quoteline.PICJson))
                            {
                                dynamic picJson = JsonConvert.DeserializeObject<dynamic>(quoteline.PICJson);
                                picJson.Quantity = quoteline.Quantity;
                                ql.PICJson = JsonConvert.SerializeObject(picJson);
                            }
                        }
                    }
                    var usr = User.PersonId;
                    var datetime = DateTime.UtcNow;

                    ql.LastUpdatedBy = usr;
                    ql.LastUpdatedOn = datetime;

                    details.LastUpdatedBy = usr;
                    details.LastUpdatedOn = datetime;

                    // TL specific
                    //ql.Cost = quoteline.Cost;
                    details.BaseCost = quoteline.Cost;
                    ql.ProductCategoryId = quoteline.ProductCategoryId;
                    ql.ProductSubCategoryId = quoteline.ProductSubCategoryId;

                    connection.Update(ql);
                    connection.Update(details);
                    UpdateQuote(quoteline.QuoteKey, 0, fromGroupDiscount);

                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public List<QuoteLinesVM> GetQuoteLine(int quoteId)
        {
            var sql = "CRM.SPGetQuoteLines";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();

                    var res = connection.Query<QuoteLinesVM>(sql, new { quoteId = quoteId, franchiseId = Franchise.FranchiseId },
              commandType: CommandType.StoredProcedure).ToList();
                    var myprdservice = GetMyProductCategories();
                    var query = "select * from [CRM].[MeasurementHeader] where id = @measurmentId";
                    var result = new MeasurementHeader();
                    if (res.FirstOrDefault() != null && res.FirstOrDefault().MeasurementId != null && res.FirstOrDefault().MeasurementId != 0)
                    {
                        result = connection.Query<MeasurementHeader>(query, new { measurmentId = res.FirstOrDefault().MeasurementId }).FirstOrDefault();
                    }

                    for (int i = 0; i < res.Count(); i++)
                    {
                        res[i].FranctionalValueHeight = string.IsNullOrEmpty(res[i].FranctionalValueHeight) || res[i].FranctionalValueHeight == "0"
                                ? ""
                                : res[i].FranctionalValueHeight;
                        res[i].FranctionalValueWidth = string.IsNullOrEmpty(res[i].FranctionalValueWidth) || res[i].FranctionalValueWidth == "0"
                            ? ""
                            : res[i].FranctionalValueWidth;
                        //if (res[i].MeasurementsetId != 0 && res[i].MeasurementsetId != null)
                        //{

                        //    if (result.Id != 0)
                        //    {
                        //        var mesu = JsonConvert.DeserializeObject<List<MeasurementLineItemBB>>(result.MeasurementSet).ToList();
                        //        var rnSet = mesu.Where(x => x.id == res[i].MeasurementsetId).FirstOrDefault();
                        //        //   res[i].RoomName = mesu.Where(x => x.id == res[i].MeasurementsetId).FirstOrDefault().RoomName;
                        //        res[i].RoomLocation = rnSet != null ? rnSet.RoomLocation : "";
                        //        //   res[i].WindowLocation = mesu.Where(x => x.id == res[i].MeasurementsetId).FirstOrDefault().WindowLocation;
                        //    }
                        //}else
                        //{
                        if (result.Id != 0)
                        {
                            var mesu = JsonConvert.DeserializeObject<List<MeasurementLineItemBB>>(result.MeasurementSet).ToList();
                            for (int z = 0; z < mesu.Count; z++)
                            {
                                mesu[z].FranctionalValueWidth = string.IsNullOrEmpty(mesu[z].FranctionalValueWidth) || mesu[z].FranctionalValueWidth == "0"
                                ? ""
                                : mesu[z].FranctionalValueWidth;
                                mesu[z].FranctionalValueHeight = string.IsNullOrEmpty(mesu[z].FranctionalValueHeight) || mesu[z].FranctionalValueHeight == "0"
                               ? ""
                               : mesu[z].FranctionalValueHeight;
                            }
                            if (Franchise.BrandId == 1)
                            {
                                var currsetId = res[i].MeasurementsetId;
                                res[i].MeasurementsetId = 0;
                                var secMesu = mesu.Where(
                                    x => x.RoomName?.Trim() == res[i].RoomName?.Trim()
                                    && Convert.ToDouble(x.Width?.Trim()) == res[i].Width.Value
                                    && Convert.ToDouble(x.Height?.Trim()) == res[i].Height.Value
                                    && x.MountTypeName?.Trim() == res[i].MountType?.Trim()
                                    && x.FranctionalValueHeight?.Trim() == res[i].FranctionalValueHeight?.Trim()
                                    && x.FranctionalValueWidth?.Trim() == res[i].FranctionalValueWidth?.Trim()).ToList();
                                if (secMesu != null)
                                {
                                    if (secMesu.Count() == 1)
                                    {
                                        res[i].MeasurementsetId = secMesu[0].id;
                                        res[i].RoomLocation = secMesu != null ? secMesu[0].RoomLocation : "";
                                    }
                                    else if (secMesu.Count() > 1)
                                    {
                                        var ext_mesu = secMesu.Where(x => x.id == currsetId).FirstOrDefault();
                                        if (ext_mesu != null && ext_mesu.id != 0)
                                        {
                                            res[i].MeasurementsetId = currsetId;
                                            res[i].RoomLocation = ext_mesu != null ? ext_mesu.RoomLocation : "";
                                        }
                                        else
                                        {
                                            res[i].MeasurementsetId = secMesu.FirstOrDefault() != null ? secMesu.FirstOrDefault().id : 0;
                                            res[i].RoomLocation = secMesu.FirstOrDefault() != null ? secMesu.FirstOrDefault().RoomLocation : "";
                                        }
                                    }
                                }
                            }
                        }


                        //}

                        if (Franchise.BrandId != 1)
                        {
                            if (res[i].ProductCategoryId.HasValue)
                            {
                                if (myprdservice
                                        .Where(x => x.ProductCategoryId == res[i].ProductCategoryId.Value)
                                        .FirstOrDefault() != null)
                                {
                                    res[i].ProductCategory = myprdservice
                                        .Where(x => x.ProductCategoryId == res[i].ProductCategoryId.Value).FirstOrDefault()
                                        .ProductCategory;
                                }
                            }

                            if (res[i].ProductSubCategoryId.HasValue)
                            {
                                if (myprdservice
                                        .Where(x => x.ProductCategoryId == res[i].ProductSubCategoryId.Value &&
                                                    x.Parant == res[i].ProductCategoryId.Value).FirstOrDefault() !=
                                    null)
                                {
                                    res[i].ProductSubCategory = myprdservice
                                        .Where(x => x.ProductCategoryId == res[i].ProductSubCategoryId.Value && x.Parant == res[i].ProductCategoryId.Value).FirstOrDefault()
                                        .ProductCategory;
                                }
                            }
                        }
                    }

                    //Group Discount - Budget Blinds only.
                    if (Franchise.BrandId == 1)
                    {
                        //updating product category id and product category for my product and service
                        string sQuery = @"select QL.QuoteLineId,PC.ProductCategoryId,PC.ProductCategory from crm.FranchiseProducts FP
                                    inner join crm.Type_ProductCategory PC on FP.ProductCategory = PC.ProductCategoryId
                                    inner join crm.QuoteLines QL on QL.ProductId = FP.ProductKey
                                    where FP.ProductKey in(select ProductId from crm.QuoteLines where ProductTypeId in (2,3) and QL.QuoteKey = @QuoteKey)";

                        var resultProductCategory = connection.Query<dynamic>(sQuery, new { QuoteKey = quoteId }).ToList();

                        var lstQuoteGroupDiscount = GetQuoteGroupDiscount(quoteId);
                        foreach (QuoteLinesVM QL in res)
                        {
                            if ((QL.ProductTypeId == 1 || QL.ProductTypeId == 2))
                            {
                                if (QL.ProductTypeId == 1 && !string.IsNullOrEmpty(QL.PICJson))
                                {
                                    var picInput = JsonConvert.DeserializeObject<dynamic>(QL.PICJson);
                                    if (!string.IsNullOrEmpty(picInput.GGroup.ToString()))
                                    {
                                        QL.PSProductCategoryId = Int32.Parse(picInput.GGroup.ToString());
                                        QL.PSProductCategory = picInput.PCategory.ToString();
                                    }
                                }
                                else if (QL.ProductTypeId == 2)
                                {
                                    var ProductWithCategory = resultProductCategory.Where(x => x.QuoteLineId == QL.QuoteLineId).FirstOrDefault();
                                    if (ProductWithCategory != null)
                                    {
                                        QL.PSProductCategoryId = ProductWithCategory.ProductCategoryId;
                                        QL.PSProductCategory = ProductWithCategory.ProductCategory;
                                    }
                                }
                            }
                        }
                    }

                    return res.OrderBy(x => x.QuoteLineNumber).ToList();
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public List<Type_ProductCategory> GetMyProductCategories()
        {
            var NameList = ExecuteIEnumerableObject<Type_ProductCategory>(ContextFactory.CrmConnectionString,
                @" SELECT tpc.ProductCategoryId, tpc.ProductCategory, tpc.Parant
                    FROM crm.Type_ProductCategory tpc
                    WHERE tpc.producttypeid= 2 AND tpc.brandid = @brandid
                    and IsActive = 1 and IsDeleted = 0 ",
                new { brandid = Franchise.BrandId }).ToList();
            var myprd = new Type_ProductCategory
            {
                ProductCategoryId = 99997,
                ProductCategory = "My Product",
                Parant = 0
            };
            NameList.Add(myprd);
            var services = new Type_ProductCategory
            {
                ProductCategoryId = 99998,
                ProductCategory = "Service",
                Parant = 0
            };
            NameList.Add(services);
            var Discount = new Type_ProductCategory
            {
                ProductCategoryId = 99999,
                ProductCategory = "Discount",
                Parant = 0
            };
            NameList.Add(Discount);

            var prdList = ExecuteIEnumerableObject<FranchiseProducts>(ContextFactory.CrmConnectionString,
                @"SELECT * FROM crm.FranchiseProducts fp WHERE fp.ProductStatus =1 AND  fp.FranchiseId =@FranchiseId",
                new { FranchiseId = Franchise.FranchiseId }).ToList();

            var discList = ExecuteIEnumerableObject<FranchiseDiscount>(ContextFactory.CrmConnectionString,
                @"SELECT * FROM crm.FranchiseDiscount fp WHERE fp.Status =1 AND  fp.FranchiseId =@FranchiseId",
                new { FranchiseId = Franchise.FranchiseId }).ToList();

            ////Generating the dropdown for My products services and Discount
            if (prdList != null && prdList.Count > 0)
            {
                foreach (var item in prdList)
                {
                    if (item.ProductType == 2)
                    {
                        NameList.Add(new Type_ProductCategory
                        {
                            ProductCategoryId = item.ProductKey,
                            ProductCategory = item.ProductName,
                            Parant = 99997
                        });
                    }
                    if (item.ProductType == 3)
                    {
                        NameList.Add(new Type_ProductCategory
                        {
                            ProductCategoryId = item.ProductKey,
                            ProductCategory = item.ProductName,
                            Parant = 99998
                        });
                    }
                }
            }

            //making a discount list
            if (discList != null && discList.Count > 0)
            {
                foreach (var item in discList)
                {
                    NameList.Add(new Type_ProductCategory
                    {
                        ProductCategoryId = item.DiscountId,
                        ProductCategory = item.Name,
                        Parant = 99999
                    });
                }
            }

            return NameList;
        }

        public FranchiseDiscount GetMyDiscountById(int discountId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    int franchiseId = Franchise.FranchiseId;
                    var query =
                        @"SELECT * FROM [CRM].[FranchiseDiscount] fd WHERE fd.Status = 1 AND fd.DiscountId  =@DiscountId AND fd.FranchiseId=@FranchiseId;";
                    connection.Open();
                    var myProduct = connection
                        .Query<FranchiseDiscount>(query, new { DiscountId = discountId, FranchiseId = franchiseId })
                        .FirstOrDefault();

                    return myProduct;
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public FranchiseProducts GetMyProductById(int productKey)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    int franchiseId = Franchise.FranchiseId;
                    var query =
                        @"SELECT fp.* FROM crm.FranchiseProducts fp
                          INNER JOIN crm.Type_ProductCategory tpc ON tpc.ProductCategoryId = fp.ProductCategory
                          WHERE fp.ProductStatus =1 AND fp.ProductKey=@ProductKey AND fp.FranchiseId =@FranchiseId;
                          SELECT tpc.* FROM crm.FranchiseProducts fp
                          INNER JOIN crm.Type_ProductCategory tpc ON tpc.ProductCategoryId = fp.ProductCategory
                          WHERE fp.ProductStatus =1 AND fp.ProductKey=@ProductKey AND fp.FranchiseId =@FranchiseId;";
                    connection.Open();
                    var fDetails = connection.QueryMultiple(query, new { ProductKey = productKey, franchiseid = franchiseId });
                    var myProduct = fDetails.Read<FranchiseProducts>().FirstOrDefault();
                    var pcategory = fDetails.Read<Type_ProductCategory>().FirstOrDefault();
                    myProduct.TypeProductCategory = pcategory;
                    return myProduct;
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public string GetMyProducts()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    int franchiseId = Franchise.FranchiseId;
                    var query = @"SELECT CASE
                                	   WHEN EXISTS
                                (
                                	SELECT VendorId
                                	FROM [Acct].[HFCVendors]
                                	WHERE VendorId = fp.VendorID
                                ) THEN
                                (
                                	SELECT Name
                                	FROM [Acct].[HFCVendors]
                                	WHERE VendorId = fp.VendorID
                                )
                                	   ELSE CASE
                                			WHEN EXISTS
                                (
                                	SELECT VendorId
                                	FROM [Acct].[FranchiseVendors]
                                	WHERE vendorstatus = 1
                                ) THEN
                                (
                                	SELECT Name
                                	FROM [Acct].[FranchiseVendors]
                                	WHERE VendorId = fp.VendorID AND
                                		  vendorstatus = 1
                                )
                                			END
                                	   END AS Vendor, fp.ProductKey, fp.VendorID, fp.ProductName, fp.VendorProductSKU, pc.[ProductCategory],
                                	    fp.ProductType AS ProductTypeId, fp.Description, fp.Cost, fp.SalePrice AS UnitPrice, pc.PICGroupId, pc.PICProductId
                                FROM [CRM].[FranchiseProducts] AS fp
                                	 LEFT JOIN
                                	 [CRM].[Type_ProductCategory] AS pc
                                	 ON fp.ProductCategory = pc.ProductCategoryId
                                WHERE fp.ProductStatus = 1 AND
                                	  FranchiseId = @FranchiseId order by pc.[ProductCategory] ASC;";
                    connection.Open();
                    var myProducts = connection.Query(query, new { franchiseid = franchiseId }).ToList();
                    return JsonConvert.SerializeObject(myProducts).ToString();
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public string[] getVendorNames()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    int franchiseId = Franchise.FranchiseId;
                    var query = @"select hfc.Name from Acct.FranchiseVendors fv
                                        join Acct.HFCVendors hfc on fv.VendorId=hfc.VendorId
                                        where fv.VendorType=1 and fv.FranchiseId=2 and fv.VendorStatus=1
										and hfc.IsActive=1
										order by hfc.Name ASC;";
                    connection.Open();
                    var myProducts = connection.Query<string>(query, new { franchiseid = franchiseId }).ToList();
                    //  string[] dfdf= myProducts.Select(i => i.ToString()).ToArray();
                    return myProducts.Select(i => i.ToString()).ToArray();
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public string GetMyProductsOrServiceDiscount(List<int> productTypeid)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    int franchiseId = Franchise.FranchiseId;
                    var query = "";
                    if (productTypeid.IndexOf(2) != -1)
                    {
                        query = @"SELECT CASE
                                	   WHEN EXISTS
                                (
                                	SELECT VendorId
                                	FROM [Acct].[HFCVendors]
                                	WHERE VendorId = fp.VendorID
                                ) THEN
                                (
                                	SELECT Name
                                	FROM [Acct].[HFCVendors]
                                	WHERE VendorId = fp.VendorID
                                )
                                	   ELSE CASE
                                			WHEN EXISTS
                                (
                                	SELECT VendorId
                                	FROM [Acct].[FranchiseVendors]
                                	WHERE  VendorId = fp.VendorID
                                ) THEN
                                (
                                	SELECT Name
                                	FROM [Acct].[FranchiseVendors]
                                	WHERE VendorId = fp.VendorID
                                )
                                			END
                                	   END AS Vendor, fp.ProductKey, fp.VendorID, fp.ProductName, fp.VendorProductSKU, pc.[ProductCategory],
                                	    fp.ProductType AS ProductTypeId,fp.ProductCategory as ProductCategoryId, fp.Description, fp.Cost, fp.SalePrice AS UnitPrice, pc.PICGroupId, pc.PICProductId,
										sc.ProductCategory AS ProductSubCategory ,fp.ProductSubCategory AS ProductSubCategoryid,fp.VendorID AS VendorId
                                FROM [CRM].[FranchiseProducts] AS fp
								 Inner JOIN [Acct].[FranchiseVendors] afv on afv.VendorId = fp.VendorID AND afv.vendorstatus = 1 AND afv.FranchiseId =@FranchiseId
                                	 LEFT JOIN
                                	 [CRM].[Type_ProductCategory] AS pc
                                	 ON fp.ProductCategory = pc.ProductCategoryId
									  LEFT JOIN crm.[Type_ProductCategory] AS sc ON sc.ProductCategoryId  = fp.ProductSubCategory
                                      LEFT JOIN  [Acct].[HFCVendors] ahv ON ahv.VendorId = fp.VendorID AND ahv.isActive = 1
                                	  where fp.ProductStatus=1 AND fp.FranchiseId = @FranchiseId and fp.ProductType in @productType   order by pc.[ProductCategory],Vendor, fp.ProductName  ASC;";
                    }
                    else
                    {
                        query = @"SELECT distinct CASE
                                	   WHEN EXISTS
                                (
                                	SELECT VendorId
                                	FROM [Acct].[HFCVendors]
                                	WHERE VendorId = fp.VendorID
                                ) THEN
                                (
                                	SELECT Name
                                	FROM [Acct].[HFCVendors]
                                	WHERE VendorId = fp.VendorID
                                )
                                	   ELSE CASE
                                			WHEN EXISTS
                                (
                                	SELECT VendorId
                                	FROM [Acct].[FranchiseVendors]
                                	WHERE  VendorId = fp.VendorID
                                ) THEN
                                (
                                	SELECT Name
                                	FROM [Acct].[FranchiseVendors]
                                	WHERE VendorId = fp.VendorID
                                )
                                			END
                                	   END AS Vendor, fp.ProductKey, fp.VendorID, fp.ProductName, fp.VendorProductSKU, pc.[ProductCategory],
                                	    fp.ProductType AS ProductTypeId,fp.ProductCategory as ProductCategoryId, fp.Description, fp.Cost, fp.SalePrice AS UnitPrice, pc.PICGroupId, pc.PICProductId,
										sc.ProductCategory AS ProductSubCategory ,fp.ProductSubCategory AS ProductSubCategoryid,fp.VendorID AS VendorId
                                FROM [CRM].[FranchiseProducts] AS fp
                                	 LEFT JOIN
                                	 [CRM].[Type_ProductCategory] AS pc
                                	 ON fp.ProductCategory = pc.ProductCategoryId
									  LEFT JOIN crm.[Type_ProductCategory] AS sc ON sc.ProductCategoryId  = fp.ProductSubCategory
                                      LEFT JOIN [Acct].[FranchiseVendors] afv on afv.VendorId = fp.VendorID AND afv.vendorstatus = 1 and afv.FranchiseId = @FranchiseId
                                      LEFT JOIN  [Acct].[HFCVendors] ahv ON ahv.VendorId = fp.VendorID AND ahv.isActive = 1
                                	 where fp.ProductStatus=1 AND fp.FranchiseId = @FranchiseId and fp.ProductType in @productType   order by pc.[ProductCategory],Vendor, fp.ProductName  ASC;";
                    }

                    connection.Open();
                    var myProducts = connection.Query(query, new { franchiseid = franchiseId, productType = productTypeid }).ToList();
                    return JsonConvert.SerializeObject(myProducts).ToString();
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public string GetQuoteLineByQuoteLineId(int quoteLineId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    int franchiseId = Franchise.FranchiseId;
                    var query = @"select ql.*,q.OpportunityId,det.basecost as Cost from crm.quotelines ql inner join crm.quote q on ql.quotekey = q.quotekey inner join crm.quotelinedetail det on det.quotelineid = ql.quotelineid  where ql.quotelineid =";
                    query = query + quoteLineId;
                    connection.Open();
                    var myProducts = connection.Query(query).ToList();
                    return JsonConvert.SerializeObject(myProducts).ToString();
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool DeleteTax(int quoteKey, int quotelineId = 0)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();

                    int retryCount = 0;

                    while (retryCount < 2)
                    {
                        try
                        {
                            var fQuery = string.Empty;
                            if (quotelineId == 0)
                            {
                                fQuery = @"BEGIN TRAN
                                   DELETE td FROM crm.TaxDetails td
                                   INNER JOIN crm.Tax t ON td.TaxId = t.TaxId
                                   INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = t.QuoteLineId
                                   WHERE ql.QuoteKey = @QuoteKey
                                   DELETE t FROM crm.tax t
                                   INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = t.QuoteLineId
                                   WHERE ql.QuoteKey = @QuoteKey
                                   COMMIT TRAN";
                                var fDetails = connection.Query(fQuery, new { QuoteKey = quoteKey });
                            }
                            else
                            {
                                fQuery = @"BEGIN TRAN
                                   DELETE td FROM crm.TaxDetails td
                                   INNER JOIN crm.Tax t ON td.TaxId = t.TaxId
                                   INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = t.QuoteLineId
                                   WHERE ql.QuoteLineId = @quotelineId
                                   DELETE t FROM crm.tax t
                                   INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = t.QuoteLineId
                                   WHERE ql.QuoteLineId = @quotelineId
                                   COMMIT TRAN";
                                var fDetails = connection.Query(fQuery, new { quotelineId = quotelineId });
                            }
                            break;
                        }
                        catch (SqlException e)
                        {
                            if (e.Number == 1205)
                            {
                                retryCount++;
                            }
                            else
                            {
                                throw;
                            }
                        }
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool saveQuoteResponse(int quoteKey, string taxResponse)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    int franchiseId = Franchise.FranchiseId;
                    connection.Open();
                    var query = @"SELECT * FROM crm.QuoteTax qt WHERE qt.QuoteKey=@QuoteKey;";

                    var qt = connection.Query<QuoteTax>(query, new { QuoteKey = quoteKey }).FirstOrDefault();

                    if (qt != null && qt.Id != 0)
                    {
                        qt.TaxReponseObject = taxResponse;
                        qt.LastUpdatedBy = SessionManager.CurrentUser.PersonId;
                        qt.LastUpdatedOn = DateTime.UtcNow;
                        connection.Update(qt);
                    }
                    else
                    {
                        var nqt = new QuoteTax();
                        nqt.QuoteKey = quoteKey;
                        nqt.TaxReponseObject = taxResponse;
                        nqt.CreatedBy = SessionManager.CurrentUser.PersonId;
                        nqt.LastUpdatedBy = SessionManager.CurrentUser.PersonId;
                        nqt.CreatedOn = DateTime.UtcNow;
                        nqt.LastUpdatedOn = DateTime.UtcNow;
                        connection.Insert(nqt);
                    }
                    return true;
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public string GetAndUpdateTax(int id, int quotelineId = 0)
        {
            try
            {
                //return "{'valid':true}";
                DeleteTax(id, 0);
                var res = GetOrderObject(id, 0);
                if (res != null)
                {
                    var response = picManager.GetAndUpdateTax(res);
                    dynamic responseObj = JsonConvert.DeserializeObject(response);

                    if (Convert.ToBoolean(responseObj.valid.Value))
                    {
                        saveQuoteResponse(id, response);
                    }
                    else
                    {
                        saveQuoteResponse(id, "");
                        return response;
                    }
                    //return "Success";
                    return response;
                }
                else
                {
                    //removing the response if the quote is tax exempt
                    saveQuoteResponse(id, "");
                    return "{'valid':true}";
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Create and update tax table on sales order
        /// </summary>
        /// <param name="id"></param>
        /// <param name="quotelineId"></param>
        /// <returns></returns>
        public string CreateTaxOnSalesOrder(int id, int quotelineId = 0)
        {
            try
            {
                DeleteTax(id, quotelineId);
                var res = GetOrderObject(id, quotelineId);
                if (res != null)
                {
                    var response = picManager.GetAndUpdateTax(res);
                    dynamic responseObj = JsonConvert.DeserializeObject(response);

                    if (Convert.ToBoolean(responseObj.valid.Value))
                    {
                        saveQuoteResponse(id, response);
                        AddorUpdateTax(id, responseObj, quotelineId);
                    }
                    else
                    {
                        saveQuoteResponse(id, "");
                        return response;
                    }
                    //return "Success";
                    return response;
                }
                else
                {
                    return "{'valid':true}";
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public dynamic GetOrderObject(int quoteKey, int quotelineId = 0)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var query = @"select * from [CRM].[Quote] where [QuoteKey] = @quoteKey
                                  select ql.* from crm.quotelines ql inner join crm.quotelinedetail  qd on ql.quotelineid = qd.quotelineid where ql.quotekey = @quoteKey
                                  select det.* from crm.quotelines ql inner join crm.quotelinedetail det on ql.quotelineId = det.quotelineid where ql.quoteKey = @quoteKey
                                  select * from [CRM].[Type_ProductCategory]
                                  select op.* from crm.Opportunities op INNER JOIN crm.Quote q ON q.OpportunityId = op.OpportunityId where q.QuoteKey=@quoteKey
                                  SELECT fp.* FROM crm.Quote q INNER JOIN crm.QuoteLines ql ON ql.QuoteKey = q.QuoteKey
                                  LEFT JOIN crm.FranchiseProducts fp ON fp.ProductKey=ql.ProductId WHERE q.QuoteKey = @quoteKey;";

                    var fData = connection.QueryMultiple(query, new { quoteKey = quoteKey });

                    var quote = fData.Read<Quote>().FirstOrDefault();
                    var quoteLines = fData.Read<QuoteLines>().ToList();
                    var qLDetails = fData.Read<QuoteLineDetail>().ToList();
                    var pCategogories = fData.Read<dynamic>().ToList();
                    var Opportunity = fData.Read<Opportunity>().FirstOrDefault();
                    var fProductList = fData.Read<FranchiseProducts>().ToList();

                    var franchise = Franchise;

                    dynamic salesOrder = new ExpandoObject();
                    dynamic order = new ExpandoObject();
                    var _orderitems = new List<ExpandoObject>();

                    var taxSettingslist = frnMgr.GetTaxSettingsByFranchise(SessionManager.CurrentFranchise.FranchiseId);
                    var taxsettings = new TaxSettings();
                    taxsettings = taxSettingslist.Where(x => x.TerritoryId == Opportunity.TerritoryId).FirstOrDefault();

                    var addressid = Opportunity.InstallationAddressId;
                    var address = new AddressTP();

                    if (quote != null && quote.IsTaxExempt)
                    {
                        return null;
                    }
                    if (taxsettings!=null && (taxsettings.TaxType==3 || taxsettings.TaxType==1))
                    {
                        return null;
                    }
                    if (franchise!=null && !franchise.EnableTax)
                    {
                        return null;
                    }
                    //This section is specifically for the situation where all quote lines are services and non taxable
                    if (quote != null && quoteLines != null && quoteLines.Count > 0 && quoteLines.Where(x => x.ProductTypeId == 3).ToList().Count > 0)
                    {
                        if (quoteLines.Count == quoteLines.Where(x => x.ProductTypeId == 3).ToList().Count)
                        {
                            var listq = quoteLines.Where(x => x.ProductTypeId == 3).ToList();
                            var taxable = false;
                            foreach (var it in listq)
                            {
                                var fProduct = fProductList.Where(x => x.ProductKey == it.ProductId).FirstOrDefault();
                                if (fProduct.Taxable)
                                {
                                    taxable = true;
                                }
                            }
                            if (!taxable)
                            {
                                return null;
                            }
                        }
                    }
                    ///Check the tax setting for the Territory
                    if (taxsettings != null)
                    {
                        if (!taxsettings.UseCustomerAddress && !taxsettings.ShipToLocationId.HasValue)
                        {
                            return null;
                        }
                        else if (!taxsettings.UseCustomerAddress && taxsettings.ShipToLocationId.HasValue)
                        {
                            addressid = taxsettings.AddressId;
                        }
                    }

                    address = connection.Query<AddressTP>("select * from crm.Addresses where addressid = @addressid ", new { addressid = addressid }).FirstOrDefault();
                    order.Order = quote.QuoteID;
                    order.Date = DateTime.Now.ToString("yyyy-MM-dd");
                    order.Customer = Franchise.Code;
                    if (address.Address1 != null && address.Address1 != "")
                        order.DestinationAddress1 = address.Address1;
                    else
                        order.DestinationAddress1 = quote.QuoteID + " " + quote.QuoteName + " " + Opportunity.OpportunityName;
                    order.DestinationAddress2 = address.Address2;
                    order.DestinationCity = address.City;
                    order.DestinationState = address.State;
                    order.DestinationZip = address.ZipCode;
                    order.DestinationCountry = address.CountryCode2Digits;

                    if (quoteLines != null && quoteLines.Count > 0)
                    {
                        int i = 1;
                        foreach (var item in quoteLines)
                        {
                            if (quotelineId == 0 || quotelineId == item.QuoteLineId)
                            {
                                var itemDetails = qLDetails.Where(x => x.QuoteLineId == item.QuoteLineId).FirstOrDefault();
                                if (item.ProductTypeId == 1 && item.Quantity.HasValue && item.Quantity.Value > 0)
                                {
                                    dynamic orderItems = new ExpandoObject();

                                    dynamic picJson = JsonConvert.DeserializeObject<dynamic>(item.PICJson);

                                    orderItems.Item = item.QuoteLineId;
                                    orderItems.Product = picJson.PicProduct;
                                    orderItems.Quantity = item.Quantity;
                                    orderItems.Amount = itemDetails.NetCharge / item.Quantity;
                                    orderItems.ediCustomerItem = item.QuoteLineId;
                                    _orderitems.Add(orderItems);
                                }
                                else if ((item.ProductTypeId == 2 || item.ProductTypeId == 5) && item.Quantity.HasValue && item.Quantity.Value > 0)
                                {
                                    dynamic orderItems = new ExpandoObject();

                                    dynamic picJson = JsonConvert.DeserializeObject<dynamic>(item.PICJson);
                                    orderItems.Item = item.QuoteLineId;
                                    orderItems.Product = picJson.PicProduct;
                                    orderItems.Quantity = item.Quantity;
                                    orderItems.Amount = itemDetails.NetCharge / item.Quantity;
                                    orderItems.ediCustomerItem = item.QuoteLineId;
                                    _orderitems.Add(orderItems);
                                }
                                else if ((item.ProductTypeId == 3) && item.Quantity.HasValue && item.Quantity.Value > 0)
                                {
                                    var fProduct = fProductList.Where(x => x.ProductKey == item.ProductId).FirstOrDefault();
                                    if (fProduct != null && fProduct.Taxable)

                                    {
                                        dynamic orderItems = new ExpandoObject();
                                        dynamic picJson = JsonConvert.DeserializeObject<dynamic>(item.PICJson);
                                        orderItems.Item = item.QuoteLineId;
                                        orderItems.Product = picJson.PicProduct;
                                        orderItems.Quantity = item.Quantity;
                                        orderItems.Amount = itemDetails.NetCharge / item.Quantity;
                                        orderItems.ediCustomerItem = item.QuoteLineId;
                                        _orderitems.Add(orderItems);
                                    }
                                }

                                i++;
                            }
                        }
                    }
                    //salesOrder.Order = order;
                    order.Items = _orderitems;

                    return order;
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public string updateQuotelineMemo(int QuotelineId, string memo)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var query = @"Update CRM.QuoteLines set Memo=@memo where QuoteLineId =@id";
                    var result = ExecuteIEnumerableObject<int>(ContextFactory.CrmConnectionString, query, new
                    {
                        memo = memo,
                        id = QuotelineId
                    });
                    return "Sucessful";
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        /// <summary>
        /// Add and insert quote line numbers
        /// </summary>
        /// <param name="quoteKey"></param>
        /// <returns></returns>
        public bool AddUpdateInsertQuoteLineNumber(int quoteKey)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var qLinesObj = connection.Query<QuoteLines>("select ql.* from crm.quotelines ql inner join crm.quotelinedetail  qd on ql.quotelineid = qd.quotelineid where ql.quotekey = @QuoteKey", new { QuoteKey = quoteKey }).ToList();

                    var quotelines = qLinesObj.Where(x => x.QuoteLineNumber > 0).OrderBy(x => x.QuoteLineNumber).ToList();

                    var newQuote = qLinesObj.Where(x => x.QuoteLineNumber == 0).ToList();
                    if (newQuote != null && newQuote.Count > 0)
                    {
                        foreach (var item in newQuote)
                        {
                            quotelines.Add(item);
                        }
                    }
                    for (int i = 0; i < quotelines.Count(); i++)
                    {
                        quotelines[i].QuoteLineNumber = i + 1;
                        connection.Update(quotelines[i]);
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public string insertQuoteLines(string JsonQuoteLines)
        {
            var quotelineobj = new
            {
                QuoteID = 0,
                QuoteLineList = new[] { new { measurmentSetId = 0, selected = "", RoomLocation = "", RoomName = "", MountTypeName = "", AddemptyLine = "", WindowLocation = "", InternalNotes = "" } }
            };

            var obj = JsonConvert.DeserializeAnonymousType(JsonQuoteLines, quotelineobj);

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var datetime = DateTime.UtcNow;
                    var personid = User.PersonId;
                    var mesu = new List<MeasurementLineItemBB>();

                    var fQuery = "select * from crm.quote where  QuoteKey = @QuoteKey;select * from crm.quotelines where QuoteKey= @QuoteKey;select m.* from crm.quote q inner join [CRM].[Opportunities] o on q.OpportunityId = o.OpportunityId inner join crm.MeasurementHeader m on m.InstallationAddressId = o.InstallationAddressId where q.QuoteKey= @QuoteKey;";

                    connection.Open();

                    var fDetails = connection.QueryMultiple(fQuery, new { QuoteKey = obj.QuoteID });
                    var quoteObj = fDetails.Read<Quote>().FirstOrDefault();
                    var qLinesObj = fDetails.Read<QuoteLines>().ToList();
                    //var quotelineDetail = fDetails.Read<QuoteLineDetail>().FirstOrDefault();
                    var mHeader = fDetails.Read<MeasurementHeader>().FirstOrDefault();
                    if (quoteObj.MeasurementId == null)
                    {
                        if (mHeader != null && mHeader.Id > 0)
                        {
                            quoteObj.MeasurementId = mHeader.Id;
                            connection.Update(quoteObj);
                        }
                    }

                    //var quoteObj = connection.Query<Quote>("select * from [CRM].[Quote] where QuoteKey = @QuoteKey", new { QuoteKey = obj.QuoteID }).FirstOrDefault();
                    //var quoteLines = connection.Query<QuoteLines>("select * from [CRM].[Quotelines] where QuoteKey = @QuoteKey", new { QuoteKey = obj.QuoteID }).ToList();
                    var sqlInsertQuery = @"INSERT INTO CRM.QuoteLines (QuoteKey ,MeasurementsetId ,CreatedOn ,CreatedBy ,IsActive ,Width ,Height,ProductTypeId,FranctionalValueWidth,FranctionalValueHeight,MountType,RoomName) VALUES (@QuoteKey ,@MeasurementsetId ,@CreatedOn ,@CreatedBy ,@IsActive ,@Width ,@Height,@ProductTypeId,@FranctionalValueWidth,@FranctionalValueHeight,@MountType,@RoomName);SELECT CAST(SCOPE_IDENTITY() as int);";
                    //var mHeader = connection.Query<MeasurementHeader>("select * from [CRM].[MeasurementHeader] where id = @measurmentId", new { measurmentId = quoteObj.MeasurementId }).FirstOrDefault();
                    if (mHeader != null && mHeader.MeasurementSet != null)
                    {
                        mesu = JsonConvert.DeserializeObject<List<MeasurementLineItemBB>>(mHeader.MeasurementSet).ToList();
                    }

                    //var qLinesObj = connection.Query<QuoteLines>("select * from [CRM].[QuoteLines] where QuoteKey = @QuoteKey", new { QuoteKey = obj.QuoteID }).ToList();

                    foreach (var item in obj.QuoteLineList)
                    {
                        bool exists = qLinesObj.Exists(x => x.MeasurementsetId == item.measurmentSetId);

                        if (!exists || item.AddemptyLine == "Y")
                        {
                            if (item.selected.Trim().ToLower() == "true")
                            {
                                var ql = new QuoteLines();
                                ql.ProductTypeId = 0;

                                ql.QuoteKey = obj.QuoteID;
                                if (item.AddemptyLine == "Y")
                                {
                                    ql.MeasurementsetId = 0;
                                }
                                else
                                {
                                    ql.MeasurementsetId = item.measurmentSetId;
                                }
                                ql.MountType = item.MountTypeName;
                                ql.IsActive = true;
                                ql.ProductTypeId = 0;
                                ql.CreatedBy = personid;
                                ql.CreatedOn = datetime;
                                if (mesu != null && mesu.Count() > 0)
                                {
                                    if (item.measurmentSetId != 0)
                                    {
                                        ql.Width = Double.Parse(mesu.Where(x => x.id == item.measurmentSetId).FirstOrDefault().Width);
                                        ql.Height = Double.Parse(mesu.Where(x => x.id == item.measurmentSetId).FirstOrDefault().Height);
                                        ql.FranctionalValueHeight = mesu.Where(x => x.id == item.measurmentSetId)
                                            .FirstOrDefault().FranctionalValueHeight;
                                        ql.FranctionalValueWidth = mesu.Where(x => x.id == item.measurmentSetId)
                                            .FirstOrDefault().FranctionalValueWidth;
                                    }
                                }

                                ql.RoomName = item.RoomName;
                                ql.WindowLocation = item.WindowLocation;
                                ql.InternalNotes = item.InternalNotes;
                                ql.QuoteLineId = connection.Insert<int, QuoteLines>(ql);
                                //ql.QuoteLineId = connection.Query<int>(sqlInsertQuery, new
                                //{
                                //    ProductTypeId = ql.ProductTypeId,
                                //    QuoteKey = ql.QuoteKey,
                                //    MeasurementsetId = ql.MeasurementsetId,
                                //    CreatedOn = ql.CreatedOn,
                                //    CreatedBy = ql.CreatedBy,
                                //    IsActive = ql.IsActive,
                                //    Width = ql.Width,
                                //    Height = ql.Height,
                                //    FranctionalValueWidth = ql.FranctionalValueWidth,
                                //    FranctionalValueHeight = ql.FranctionalValueHeight,
                                //    MountType = ql.MountType,
                                //    RoomName = ql.RoomName

                                //}).Single();

                                var qDetails = new QuoteLineDetail();
                                qDetails.QuoteLineId = ql.QuoteLineId;
                                qDetails.CreatedBy = ql.CreatedBy;
                                qDetails.CreatedOn = ql.CreatedOn;
                                connection.Insert<QuoteLineDetail>(qDetails);
                                //connection.Insert<QuoteLines>(ql);
                            }
                        }
                        else
                        {
                            bool isSelected;
                            if (item.selected.Trim().ToLower() == "false")
                            {
                                isSelected = false;
                            }
                            else
                            {
                                isSelected = true;
                            }

                            var ql = qLinesObj.Where(x => x.MeasurementsetId == item.measurmentSetId).FirstOrDefault();
                            if (isSelected != ql.IsActive)
                            {
                                ql.LastUpdatedBy = personid;
                                ql.LastUpdatedOn = datetime;
                                //ql.IsActive = isSelected;
                                connection.Update<QuoteLines>(ql);
                            }
                            //else
                            //{
                            //}
                        }
                    }
                    var response = AddUpdateInsertQuoteLineNumber(obj.QuoteID);
                    return "Sucessful";
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public string InsertQuoteLines(List<QuoteLines> quoteLines)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var datetime = DateTime.UtcNow;
                    var personid = User.PersonId;
                    foreach (var ql in quoteLines)
                    {
                        ql.CreatedBy = personid;
                        ql.CreatedOn = datetime;
                        var res = connection.Insert<QuoteLines>(ql);
                    }
                    return "Sucessful";
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        /// <summary>
        /// Method to get Tax from PIC to Tpt
        /// </summary>
        /// <param name="quoteKey"> Quoteline Id fr</param>
        /// <param name="taxInfo">Tax details object from PIC</param>
        /// <returns></returns>
        public List<TaxDetails> ReturnTaxList(int quoteKey)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var tdlist = new List<TaxDetails>();
                    connection.Open();
                    var query = @"select ql.* from crm.quotelines ql inner join crm.quotelinedetail  qd on ql.quotelineid = qd.quotelineid where ql.quotekey = @QuoteKey
                                    SELECT * FROM crm.Quote q WHERE q.quotekey=@QuoteKey;
                                    SELECT * FROM crm.QuoteTax qt WHERE qt.TaxReponseObject<>'' AND  qt.QuoteKey=@QuoteKey;";
                    var fData = connection.QueryMultiple(query, new { QuoteKey = quoteKey });

                    var quoteLines = fData.Read<QuoteLines>().ToList();
                    var quote = fData.Read<Quote>().FirstOrDefault();
                    var qtax = fData.Read<QuoteTax>().FirstOrDefault();

                    if (qtax != null && qtax.Id != 0)
                    {
                        dynamic taxInfo = JsonConvert.DeserializeObject(qtax.TaxReponseObject);
                        //if (string.IsNullOrEmpty(qtax.TaxReponseObject))
                        //{
                        //    taxInfo = JsonConvert.DeserializeObject(qtax.TaxReponseObject);

                        //}
                        if (taxInfo.valid.Value)
                        {
                            foreach (var ql in quoteLines)
                            {
                                if (!ql.TaxExempt)
                                {
                                    if (ql.ProductTypeId != 0 && ql.ProductTypeId != 4)
                                    {
                                        //dynamic picJson = JsonConvert.DeserializeObject(ql.PICJson);
                                        var info = taxInfo.Order.Items;
                                        foreach (dynamic item in info)
                                        {
                                            if (Convert.ToInt32(item.Item) == ql.QuoteLineId)
                                            {

                                                decimal txamtbyline = 0;

                                                foreach (dynamic itemDetail in item.tax.taxDetail)
                                                {
                                                    if (quote.IsHSTExempt && ((string)itemDetail.taxName).IndexOf("HST", StringComparison.OrdinalIgnoreCase) >= 0)
                                                    {
                                                        txamtbyline = txamtbyline + Convert.ToDecimal(itemDetail.amount);
                                                    }
                                                    else if (quote.IsGSTExempt && ((string)itemDetail.taxName).IndexOf("GST", StringComparison.OrdinalIgnoreCase) >= 0)
                                                    {
                                                        txamtbyline = txamtbyline + Convert.ToDecimal(itemDetail.amount);
                                                    }
                                                    else if (quote.IsPSTExempt && ((string)itemDetail.taxName).IndexOf("PST", StringComparison.OrdinalIgnoreCase) >= 0)
                                                    {
                                                        txamtbyline = txamtbyline + Convert.ToDecimal(itemDetail.amount);
                                                    }
                                                    else if (quote.IsVATExempt && ((string)itemDetail.taxName).IndexOf("VAT", StringComparison.OrdinalIgnoreCase) >= 0)
                                                    {
                                                        txamtbyline = txamtbyline + Convert.ToDecimal(itemDetail.amount);
                                                    }
                                                    else
                                                    {
                                                        var td = tdlist.Where(x => x.Jurisdiction == (string)itemDetail.jurisdiction && x.JurisType == (string)itemDetail.jurisType && x.TaxName == (string)itemDetail.taxName && x.Rate == Convert.ToDecimal(itemDetail.rate)).FirstOrDefault();
                                                        if (td != null)
                                                        {
                                                            td.Amount = td.Amount + Convert.ToDecimal(itemDetail.amount);
                                                            //tdlist.Add(td);
                                                        }
                                                        else
                                                        {
                                                            var taxDetail = new TaxDetails();
                                                            taxDetail.Jurisdiction = (string)itemDetail.jurisdiction;
                                                            taxDetail.JurisType = (string)itemDetail.jurisType;
                                                            taxDetail.TaxName = (string)itemDetail.taxName;
                                                            taxDetail.Rate = Convert.ToDecimal(itemDetail.rate);
                                                            taxDetail.Amount = Convert.ToDecimal(itemDetail.amount);
                                                            tdlist.Add(taxDetail);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }


                    }



                    //       group TaxDetails by   TaxDetails.Jurisdiction, TaxDetails.JurisType,TaxDetails.Rate,TaxDetails.TaxName into grpd
                    //select new TaxDetails {Jurisdiction= grpd.jur };




                    //      GROUP BY Jurisdiction,JurisType,Rate,TaxName
                    //order by JurisType ASC ",


                    return tdlist;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        /// <summary>
        /// Method to get Tax from PIC to Tpt
        /// </summary>
        /// <param name="quoteKey"> Quoteline Id fr</param>
        /// <param name="taxInfo">Tax details object from PIC</param>
        /// <returns></returns>
        public bool AddorUpdateTax(int quoteKey, dynamic taxInfo, int quoteLineId = 0)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var user = User.PersonId;
                    var dateTime = DateTime.UtcNow;

                    var listTaxDetails = new List<TaxDetails>();
                    connection.Open();
                    var query = @"select ql.* from crm.quotelines ql inner join crm.quotelinedetail  qd on ql.quotelineid = qd.quotelineid where ql.quotekey = @QuoteKey
                                    SELECT * FROM crm.Quote q INNER JOIN crm.Opportunities o ON o.OpportunityId = q.OpportunityId WHERE q.quotekey=@QuoteKey
                                    SELECT * FROM crm.Quote q WHERE q.quotekey=@QuoteKey;";
                    var fData = connection.QueryMultiple(query, new { QuoteKey = quoteKey });

                    var quoteLines = fData.Read<QuoteLines>().ToList();
                    var opportunity = fData.Read<Opportunity>().FirstOrDefault();
                    var quote = fData.Read<Quote>().FirstOrDefault();
                    //Existing Tax information

                    var taxList = new List<Tax>();

                    foreach (var ql in quoteLines)
                    {
                        if ((quoteLineId == 0 || ql.QuoteLineId == quoteLineId) && !ql.TaxExempt)
                        {
                            if (ql.ProductTypeId != 0 && ql.ProductTypeId != 4)
                            {
                                var tax = new Tax();

                                //dynamic picJson = JsonConvert.DeserializeObject(ql.PICJson);

                                var info = taxInfo.Order.Items;
                                foreach (dynamic item in info)
                                {
                                    if (Convert.ToInt32(item.Item) == ql.QuoteLineId)
                                    {
                                        tax = new Tax();
                                        tax.QuoteLineId = ql.QuoteLineId;
                                        tax.Code = (string)item.tax.code;
                                        tax.Rate = Convert.ToDecimal(item.tax.rate);
                                        tax.Amount = Convert.ToDecimal(item.tax.amount);
                                        tax.FranchiseId = opportunity != null && opportunity.FranchiseId.HasValue ? opportunity.FranchiseId.Value : 0;
                                        tax.QuoteKey = ql.QuoteKey;
                                        tax.Zipcode = taxInfo.Order.DestinationZip;
                                        tax.CreatedBy = user;
                                        tax.CreatedOn = dateTime;
                                        tax.LastUpdatedBy = user;
                                        tax.LastUpdatedOn = dateTime;
                                        //tax.TaxId = connection.Insert<int, Tax>(tax);

                                        decimal txamtbyline = 0;
                                        var tdlist = new List<TaxDetails>();
                                        foreach (dynamic itemDetail in item.tax.taxDetail)
                                        {
                                            if (quote.IsHSTExempt && ((string)itemDetail.taxName).IndexOf("HST", StringComparison.OrdinalIgnoreCase) >= 0)
                                            {
                                                txamtbyline = txamtbyline + Convert.ToDecimal(itemDetail.amount);
                                            }
                                            else if (quote.IsGSTExempt && ((string)itemDetail.taxName).IndexOf("GST", StringComparison.OrdinalIgnoreCase) >= 0)
                                            {
                                                txamtbyline = txamtbyline + Convert.ToDecimal(itemDetail.amount);
                                            }
                                            else if (quote.IsPSTExempt && ((string)itemDetail.taxName).IndexOf("PST", StringComparison.OrdinalIgnoreCase) >= 0)
                                            {
                                                txamtbyline = txamtbyline + Convert.ToDecimal(itemDetail.amount);
                                            }
                                            else if (quote.IsVATExempt && ((string)itemDetail.taxName).IndexOf("VAT", StringComparison.OrdinalIgnoreCase) >= 0)
                                            {
                                                txamtbyline = txamtbyline + Convert.ToDecimal(itemDetail.amount);
                                            }
                                            else
                                            {
                                                var taxDetail = new TaxDetails();
                                                taxDetail.TaxId = tax.TaxId;
                                                taxDetail.Jurisdiction = (string)itemDetail.jurisdiction;
                                                taxDetail.JurisType = (string)itemDetail.jurisType;
                                                taxDetail.TaxName = (string)itemDetail.taxName;
                                                taxDetail.Rate = Convert.ToDecimal(itemDetail.rate);
                                                taxDetail.TaxableAmount = Convert.ToDecimal(itemDetail.taxableAmount);
                                                taxDetail.Amount = Convert.ToDecimal(itemDetail.amount);
                                                taxDetail.CreatedOn = dateTime;
                                                taxDetail.CreatedBy = user;
                                                taxDetail.LastUpdatedBy = user;
                                                taxDetail.LastUpdatedOn = dateTime;
                                                //connection.Insert<int, TaxDetails>(taxDetail);
                                                tdlist.Add(taxDetail);
                                            }
                                        }
                                        tax.TaxDetails = tdlist;
                                        tax.Amount = tax.Amount - txamtbyline;
                                        //connection.Update(tax);
                                        taxList.Add(tax);
                                    }
                                }
                            }
                        }
                    }
                    if (taxList != null && taxList.Count > 0)
                    {
                        DapperPlusManager.Entity<Tax>().Table("CRM.Tax").Identity(x => x.TaxId);
                        DapperPlusManager.Entity<TaxDetails>().Table("CRM.TaxDetails").Identity(x => x.TaxDetailsId);

                        connection.BulkInsert(taxList);
                        taxList.ForEach(x => x.TaxDetails.ForEach(y => y.TaxId = x.TaxId));
                        connection.BulkInsert(taxList.SelectMany(x => x.TaxDetails));
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool QuoteOrderExists(int quoteKey)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();

                    var order = ExecuteIEnumerableObject<Orders>(ContextFactory.CrmConnectionString,
                        "select * from crm.orders o inner join crm.quote q  on o.OpportunityId = q.OpportunityId where q.quotekey = @quotekey",
                        new
                        {
                            quotekey = quoteKey
                        }).ToList();

                    if (order != null && order.Count > 0)
                    {
                        var finalstatus = false;
                        foreach (var ord in order)
                        {
                            if (ord.OrderStatus == 9 || ord.OrderStatus == 6)
                                finalstatus = false;
                            else
                                finalstatus = true;
                        }
                        return finalstatus;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public int CreateOrder(int quoteKey)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var datetime = DateTime.UtcNow;
                    var personid = User.PersonId;
                    var quote = connection.Query<Quote>("select * from [CRM].[Quote] where QuoteKey = @QuoteKey", new { QuoteKey = quoteKey }).FirstOrDefault();
                    //var quoteLines = connection.Query<QuoteLines>("select * from [CRM].[QuoteLines] where QuoteKey = @QuoteKey", new { QuoteKey = quoteKey }).ToList();
                    //var tax = connection.Query<Tax>("SELECT t.* FROM [CRM].[tax] AS t INNER JOIN crm.quotelines AS q ON t.quotelineId = q.quotelineId where q.quoteKey = @quoteKey;", new { quoteKey = quoteKey }).ToList();
                    var taxDetailsList = connection.Query<TaxDetails>("SELECT distinct t.QuoteLineId,td.Jurisdiction,td.JurisType,td.Rate,td.TaxableAmount,td.Amount,td.TaxName FROM [CRM].[tax] AS t INNER JOIN CRM.taxdetails td on t.taxid = td.taxid INNER JOIN crm.quotelines AS q ON t.quotelineId = q.quotelineId where q.quoteKey = @quoteKey;", new { quoteKey = quoteKey }).ToList();
                    var order = new Orders();
                    order.OpportunityId = quote.OpportunityId;

                    //order.IsTaxExempt = quote.IsTaxExempt;
                    //order.TaxExemptID = quote.TaxExemptID;
                    order.QuoteKey = quoteKey;
                    order.OrderName = quote.QuoteName;
                    order.OrderNumber = actMgr.GetNextNumber(Franchise.FranchiseId, 3);//Get the next order number by Franchise
                    //order.OrderStatus = 1;//Procurement Pending
                    order.OrderStatus = 8;
                    order.ProductSubtotal = quote.ProductSubTotal;

                    order.AdditionalCharges = quote.AdditionalCharges;
                    //order.Tax = tax.Sum(x => x.Amount);
                    order.OrderDiscount = quote.TotalDiscounts;
                    order.OrderSubtotal = quote.QuoteSubTotal;
                    //order.ContractedDate = DateTime.UtcNow;
                    if (quote.PreviousSaleDate != null)
                    {
                        order.ContractedDate = quote.SaleDate;
                        order.PreviousContractedDate = quote.PreviousSaleDate;
                        order.ContractedDateUpdatedBy = quote.SaleDateCreatedBy;
                        order.ContractedDateUpdatedOn = quote.SaleDateCreatedOn;
                    }
                    else
                    {
                        order.ContractedDate = (TimeZoneManager.ToLocal(DateTime.UtcNow)).Date;
                    }
                    order.CreatedBy = personid;
                    order.CreatedOn = datetime;
                    order.LastUpdatedBy = personid;
                    order.LastUpdatedOn = datetime;
                    if (taxDetailsList != null && taxDetailsList.Count > 0)
                    {
                        order.Tax = taxDetailsList.Sum(x => x.Amount);
                        order.TaxCalculated = true;
                    }
                    else
                    {
                        order.Tax = 0;
                        order.TaxCalculated = false;
                    }

                    order.OrderTotal = order.OrderSubtotal + order.Tax;
                    order.BalanceDue = order.OrderTotal;

                    order.ImpersonatorPersonId = this.ImpersonatorPersonId;

                    int orderId = connection.Insert<int, Orders>(order);
                    //int orderId = (int)connection.Insert(order);

                    var acc = connection.Query<AccountTP>(" select * from CRM.Accounts where AccountId = (select top 1 AccountId from CRM.Opportunities where OpportunityId = @OpportunityId)", new { OpportunityId = order.OpportunityId }).FirstOrDefault();

                    //string queryAccouttype = "update CRM.Accounts set AccountType='Customers' where AccountId = (select top 1 AccountId from CRM.Opportunities where OpportunityId=@OpportunityId)";
                    //string queryAccouttype = "update CRM.Accounts set AccountTypeId = 3 where AccountId = (select top 1 AccountId from CRM.Opportunities where OpportunityId=@OpportunityId)"; old id is replaced with new id below
                    string queryAccouttype = "update CRM.Accounts set AccountTypeId = 10005 where AccountId = (select top 1 AccountId from CRM.Opportunities where OpportunityId=@OpportunityId)";
                    connection.Execute(queryAccouttype, new { OpportunityId = order.OpportunityId });

                    ///Set the status of opportunity to Ordering when a order is created
                    //oppManager.UpdateStatus(quote.OpportunityId, 4);
                    var value = oppManager.update(quote.OpportunityId, 4);
                    UpdatePrimaryQuote(quote.OpportunityId, quoteKey);
                    UpdateOrderStatustoOpenById(orderId);

                    // Need to update the Orderline Status same as Order status to faciliate
                    // bulk purchasing.
                    var queryol = @"insert into crm.OrderLines(QuoteKey, QuoteLineId, CreatedOn, CreatedBy, OrderID, OrderLineStatus)
                                    select ql.QuoteKey, ql.QuoteLineId, ql.CreatedOn, ql.CreatedBy, o.OrderID, o.OrderStatus
                                    from crm.QuoteLines ql
                                    left join crm.orders o on o.QuoteKey = ql.QuoteKey
                                    where ql.QuoteKey = @quotekey";
                    connection.Execute(queryol, new { quotekey = quoteKey });

                    EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(order), OrderId: orderId);
                    return orderId;
                    //return true;
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void UpdateOrderStatustoOpenById(int orderId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var sqlQuery = @"select o.OrderID,o.CreatedOn,f.BuyerremorseDay from CRM.Orders o
                            join CRM.Opportunities op on o.OpportunityId=op.OpportunityId
                            join CRM.Franchise f on op.FranchiseId=f.FranchiseId
                            where o.OrderID=@OrderID;";
                    connection.Open();

                    var qDetails = connection.QueryMultiple(sqlQuery, new { OrderID = orderId, franchiseid = Franchise.FranchiseId });

                    var orderdata = qDetails.Read<OrderStatusOpen>().FirstOrDefault();

                    DateTime today = DateTime.UtcNow.Date;
                    DateTime createdt = orderdata.CreatedOn.AddDays(orderdata.BuyerremorseDay).Date;
                    if (today >= createdt)
                    {
                        connection.Query("update CRM.Orders set OrderStatus=7 where OrderID=@OrderID;", new { OrderID = orderId });
                    }

                    //return qvm;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }

            //string query = @"select o.OrderID,o.CreatedOn,f.BuyerremorseDay from CRM.Orders o
            //                join CRM.Opportunities op on o.OpportunityId=op.OpportunityId
            //                join CRM.Franchise f on op.FranchiseId=f.FranchiseId
            //                where o.OrderID=@OrderID;";

            //var orderdata =
            //    ExecuteIEnumerableObject<OrderStatusOpen>(ContextFactory.CrmConnectionString, query,
            //        new { OrderID = orderId }).FirstOrDefault();

            ////foreach (var d in orderdata)
            ////{
            //DateTime today = DateTime.UtcNow.Date;
            //DateTime createdt = orderdata.CreatedOn.AddDays(orderdata.BuyerremorseDay).Date;
            //if (today >= createdt)
            //{
            //    string qUpdate = @"update CRM.Orders set OrderStatus=7 where OrderID=@OrderID";
            //    ExecuteObject(ContextFactory.CrmConnectionString, qUpdate, new { orderdata.OrderID });
            //}
            ////}
        }

        public QuoteVM GetFranchiseServicesAndDiscount()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var qvm = new QuoteVM();
                    var sqlQuery = "SELECT fp.ProductKey, fp.ProductID, fp.ProductName,  fp.Description, fp.ProductCategory, fp.Cost, fp.SalePrice, fp.ProductStatus, fp.ProductSubCategory,  fp.ProductType, tpg.ProductCategory as ProductCategoryName, tpg.PICGroupId, tpg.PICProductId, sub.ProductCategory as SubCategoryName FROM crm.franchiseproducts AS fp 	 INNER JOIN 	 [CRM].[Type_ProductCategory] AS tpg 	 ON tpg.productCategoryId = fp.ProductCategory 	 LEFT JOIN 	 [CRM].[Type_ProductCategory] AS sub 	 ON sub.productCategoryId = fp.productSubCategory WHERE fp.producttype IN(3) AND productstatus = 1 and 	  fp.franchiseid = @franchiseid; select * from [CRM].[FranchiseDiscount] where status = 1 and franchiseid = @franchiseid;";
                    connection.Open();

                    var qDetails = connection.QueryMultiple(sqlQuery, new { franchiseid = Franchise.FranchiseId });

                    qvm.ServicesList = qDetails.Read<FranchiseProducts>().ToList();
                    qvm.DiscountsList = qDetails.Read<FranchiseDiscount>().ToList();

                    return qvm;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool UpdatePrimaryQuote(int oppurtunityId, int quoteKey)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var quotes = connection.Query<Quote>("select * from [CRM].[quote] where OpportunityId = @OpportunityId", new { OpportunityId = oppurtunityId }).ToList();
                    if (quotes != null)
                    {
                        foreach (var quote in quotes)
                        {
                            if (quote.QuoteKey == quoteKey)
                            {
                                quote.PrimaryQuote = true;
                            }
                            else
                            {
                                quote.PrimaryQuote = false;
                            }
                            connection.Update<Quote>(quote);
                        }
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public Quote CheckOrderExisitbyOpportunity(int oppurtunityId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    string qopp = @"select q.QuoteKey,[QuoteName] from CRM.Opportunities op
                                    join CRM.Quote q on op.OpportunityId=q.OpportunityId
                                    join CRM.Orders o on q.QuoteKey=o.QuoteKey
                                    where o.OrderStatus NOT IN (6,9) and  op.OpportunityId=@oppurtunityId";
                    return connection.Query<Quote>(qopp, new { oppurtunityId }).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
            }
        }

        public List<DiscountsAndPromo> GetDiscountAndPromo(int quotekey)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var sqlQuery = "select * from [CRM].[DiscountsAndPromo] where QuoteKey = @quotekey;";
                    connection.Open();
                    var discount = connection.Query<DiscountsAndPromo>(sqlQuery, new { quotekey = quotekey }).ToList();

                    return discount;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        #region Quote Margin Review

        public List<QuoteMarginReviewVM> GetQuoteMarginDetails(int quoteKey)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var sqlQuery = @"select ql.ProductTypeId,ql.quotelinenumber, ql.quotelineid,ql.quantity,ql.ProductName as product, ql.VendorName as vendor,qd.PricingId as PricingStrategyId,qd.PricingId as RuleNumber, concat(ps.markup, '', ps.MarkUpFactor) as markup,
                                     pse.PriceRoundingOpt as RoundingRule,qd.BaseCost,qd.BaseResalePrice,qd.PricingCode,qd.PricingRule, qd.BaseResalePriceWOPromos,qd.SelectedPrice,qd.SuggestedResale,qd.Discount,ql.DiscountType,ql.IsGroupDiscountApplied,qd.unitPrice ,qd.DiscountAmount , ps.Markup as PricingMarkup,ps.markupfactor as MarkupFactor,
                                     qd.ExtendedPrice,qd.DiscountAmount, qd.MarginPercentage, case when  exists (select * from crm.DiscountsAndPromo where  ql.quotelineid = quotelineid) then 'Y' ELSE 'N' END as VendorPromo,QL.PICJson
                                     from crm.quotelines ql inner join crm.QuoteLineDetail qd on ql.QuoteLineId = qd.QuoteLineId left join [CRM].[PricingSettings] ps  on ps.id = qd.PricingId
                                     left join [CRM].[PricingSettings] as pse on pse.FranchiseId = @FranchiseId and pse.PricingStrategyTypeID = 1 and pse.isactive = 1
                                     where ql.QuoteKey = @QuoteKey  order by ql.QuoteLineNumber asc;";
                    connection.Open();
                    var quotesMarginReview = connection.Query<QuoteMarginReviewVM>(sqlQuery, new { FranchiseId = Franchise.FranchiseId, quotekey = quoteKey }).ToList();

                    for (int i = 0; quotesMarginReview.Count > i; i++)
                    {
                        if (quotesMarginReview[i].SelectedPrice == "SuggestedResale")
                        {
                            quotesMarginReview[i].SuggestedResaleCheck = true;
                        }
                        else if (quotesMarginReview[i].SelectedPrice == "BaseResalePriceWOPromos")
                        {
                            quotesMarginReview[i].BaseResalePriceWoPromoscheck = true;
                        }
                        else
                        {
                            quotesMarginReview[i].BaseResalePriceCheck = true;
                        }
                    }

                    //Group Discount - Budget Blinds only.
                    if (Franchise.BrandId == 1)
                    {
                        //updating product category id and product category for my product and service
                        string sQuery = @"select QL.QuoteLineId,PC.ProductCategoryId,PC.ProductCategory from crm.FranchiseProducts FP
                                    inner join crm.Type_ProductCategory PC on FP.ProductCategory = PC.ProductCategoryId
                                    inner join crm.QuoteLines QL on QL.ProductId = FP.ProductKey
                                    where FP.ProductKey in(select ProductId from crm.QuoteLines where ProductTypeId in (2,3) and QL.QuoteKey = @QuoteKey)";

                        var resultProductCategory = connection.Query<dynamic>(sQuery, new { QuoteKey = quoteKey }).ToList();

                        var lstQuoteGroupDiscount = GetQuoteGroupDiscount(quoteKey);
                        foreach (QuoteMarginReviewVM QL in quotesMarginReview)
                        {
                            if ((QL.ProductTypeId == 1 || QL.ProductTypeId == 2))
                            {
                                if (QL.ProductTypeId == 1 && !string.IsNullOrEmpty(QL.PICJson))
                                {
                                    var picInput = JsonConvert.DeserializeObject<dynamic>(QL.PICJson);
                                    if (!string.IsNullOrEmpty(picInput.GGroup.ToString()))
                                    {
                                        QL.PSProductCategoryId = Int32.Parse(picInput.GGroup.ToString());
                                        QL.PSProductCategory = picInput.PCategory.ToString();
                                    }
                                }
                                else if (QL.ProductTypeId == 2)
                                {
                                    var ProductWithCategory = resultProductCategory.Where(x => x.QuoteLineId == QL.QuoteLineId).FirstOrDefault();
                                    if (ProductWithCategory != null)
                                    {
                                        QL.PSProductCategoryId = ProductWithCategory.ProductCategoryId;
                                        QL.PSProductCategory = ProductWithCategory.ProductCategory;
                                    }
                                }
                            }
                        }
                    }

                    return quotesMarginReview;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        #endregion Quote Margin Review

        //public bool UpdatePricing(int quoteLineId)
        //{
        //    using(var connection = new SqlConnection(ContextFactory.CrmConnectionString))
        //    {
        //        try
        //        {
        //            var fQuery = "select * from [CRM].[QuoteLines] where [QuoteLineId] = @QuoteLineId;select * from [CRM].[quotelinedetail] where [QuoteLineId] = @QuoteLineId;";

        //            connection.Open();
        //            var fDetails = connection.QueryMultiple(fQuery, new { FranchiseId = Franchise.FranchiseId, QuoteLineId = quoteLineId });
        //            var qLinesObj = fDetails.Read<QuoteLines>().FirstOrDefault();
        //            var quotelineDetail = fDetails.Read<QuoteLineDetail>().FirstOrDefault();
        //            //var discountandPromos = fDetails.Read<DiscountsAndPromo>().ToList();

        //            return true;

        //        }
        //        catch(Exception ex)
        //        {
        //            EventLogger.LogEvent(ex);
        //            throw ;
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }
        //    }
        //}

        public bool UpdateDiscountAndPromo(DiscountsAndPromo model)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();

                    var fQuery = "select ql.* from crm.quotelines ql inner join crm.quotelinedetail  qd on ql.quotelineid = qd.quotelineid where ql.quotelineid = @QuoteLineId;select qd.* from crm.quotelines ql inner join crm.quotelinedetail  qd on ql.quotelineid = qd.quotelineid where ql.quotelineid = @QuoteLineId;select * from crm.discountsandPromo where quotelineid = QuoteLineId;";

                    model.DiscountAmountPassed = model.Discount * model.DiscountPercentPassed / 100;
                    model.LastUpdatedBy = User.PersonId;
                    model.LastUpdatedOn = DateTime.UtcNow;
                    model.QuoteLineDetailId = model.QuoteLineDetailId == 0 ? null : model.QuoteLineDetailId;
                    connection.Update<DiscountsAndPromo>(model);

                    var fDetails = connection.QueryMultiple(fQuery, new { FranchiseId = Franchise.FranchiseId, QuoteLineId = model.QuoteLineId });

                    var qLinesObj = fDetails.Read<QuoteLines>().FirstOrDefault();
                    var quotelineDetail = fDetails.Read<QuoteLineDetail>().FirstOrDefault();
                    var discountandPromos = fDetails.Read<DiscountsAndPromo>().ToList();

                    var discountapplied = discountandPromos.Sum(x => x.DiscountAmountPassed);
                    if (discountapplied < 0)
                    {
                        discountapplied = discountapplied * -1;
                    }
                    quotelineDetail.BaseResalePrice = quotelineDetail.BaseResalePriceWOPromos - discountapplied;
                    //quotelineDetail.unitPrice = quotelineDetail.unitPrice - discountapplied;
                    //quotelineDetail.ManualUnitPrice = true;
                    connection.Update<QuoteLineDetail>(quotelineDetail);

                    //if (quotelineDetail.SelectedPrice == "BaseResalePrice")
                    //{
                    //}
                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool RecalculateQuoteLines(int quoteKey, int qLid = 0, bool ignoreTaxUpdate = false)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var fQuery = "SELECT * FROM [CRM].[Franchise] WHERE [FranchiseId] = @FranchiseId; SELECT * FROM [CRM].[PricingSettings] WHERE franchiseid = @FranchiseId; select ql.* from crm.quotelines ql inner join crm.quotelinedetail  qd on ql.quotelineid = qd.quotelineid where ql.quotekey = @quotekey; SELECT qd.* FROM [CRM].[quotelinedetail] qd inner join [CRM].[QuoteLines] ql on qd.[QuoteLineId] = ql.[QuoteLineId] WHERE quotekey = @quotekey; SELECT q.* FROM [CRM].[quote] AS q WHERE quotekey = @quotekey;SELECT ps.* FROM [CRM].[PricingSettings] ps inner join [CRM].[QuoteLineDetail] qd on ps.id = qd.PricingId inner join crm.quotelines ql on ql.quotelineId = qd.quotelineid where ql.quotekey = @quotekey;";

                    connection.Open();

                    var fDetails = connection.QueryMultiple(fQuery, new { FranchiseId = this.Franchise.FranchiseId, quotekey = quoteKey });

                    var Franchise = fDetails.Read<Franchise>().FirstOrDefault();
                    var priceSettings = fDetails.Read<PricingSettings>().ToList();
                    var qLinesObj = fDetails.Read<QuoteLines>().ToList();
                    var quotelineDetaillist = fDetails.Read<QuoteLineDetail>().ToList();
                    var quote = fDetails.Read<Quote>().FirstOrDefault();
                    var PricingUsedList = fDetails.Read<PricingSettings>().ToList();//ist();.FirstOrDefault();

                    var user = User.PersonId;
                    var datetime = DateTime.UtcNow;



                    foreach (var ql in qLinesObj)
                    {
                        if (ql.QuoteLineNumber == 0)
                        {
                            ql.QuoteLineNumber = qLinesObj.Max(x => x.QuoteLineNumber) + 1;
                        }

                        var quotelineDetail = quotelineDetaillist.Where(x => x.QuoteLineId == ql.QuoteLineId).FirstOrDefault();
                        quotelineDetail.Discount = ql.Discount;

                        var vendorGroupMU = priceSettings.Where(
                            x => x.ProductCategoryId.HasValue && x.ProductCategoryId == ql.ProductId
                                                              && x.VendorId == ql.VendorId && x.IsActive).FirstOrDefault();
                        var productMU = priceSettings.Where(x => x.ProductCategoryId.HasValue && x.ProductCategoryId == ql.ProductId && x.IsActive).FirstOrDefault();
                        var vendorMU = priceSettings.Where(x => x.VendorId == ql.VendorId && x.ProductCategoryId == null && x.IsActive).FirstOrDefault();
                        var franchiseMU = priceSettings.Where(x => x.VendorId == null && x.ProductCategoryId == null && x.IsActive).FirstOrDefault();
                        var rounding = "N/A";

                        var PricingUsed = PricingUsedList.Where((x => x.Id == quotelineDetail.PricingId))
                            .FirstOrDefault();

                        if (ql.ProductTypeId == 1 || ql.ProductTypeId == 2 || ql.ProductTypeId == 3 || ql.ProductTypeId == 5)
                        {
                            if (franchiseMU != null)
                            {
                                if (franchiseMU.PriceRoundingOpt != null && franchiseMU.PriceRoundingOpt != "")
                                {
                                    quotelineDetail.Rounding = franchiseMU.PriceRoundingOpt;
                                }
                                else
                                {
                                    quotelineDetail.Rounding = "";
                                }
                            }

                            rounding = quotelineDetail.Rounding;

                            #region Markup

                            //Apply Mark up only when suggested Resale
                            //If Unit price is manually edited by FE do not apply any markup on it
                            if (PricingUsed != null)
                            {
                                /// No Pricing should be applied to One time products
                                if ((ql.ProductTypeId != 5 && ql.ProductTypeId != 3))
                                {
                                    if (PricingUsed.MarkUpFactor == "$")
                                    {
                                        if (quotelineDetail.BaseResalePrice.HasValue &&
                                            quotelineDetail.BaseResalePrice.Value > 0)
                                        {
                                            quotelineDetail.SuggestedResale = quotelineDetail.BaseResalePrice + PricingUsed.MarkUp;
                                        }
                                        else
                                        {
                                            quotelineDetail.SuggestedResale = quotelineDetail.BaseResalePriceWOPromos + PricingUsed.MarkUp;
                                        }

                                        if (!quotelineDetail.ManualUnitPrice)
                                            quotelineDetail.unitPrice = quotelineDetail.SuggestedResale;
                                    }
                                    else if (PricingUsed.MarkUpFactor == "%")
                                    {
                                        if (quotelineDetail.BaseResalePrice.HasValue &&
                                            quotelineDetail.BaseResalePrice.Value > 0)
                                        {
                                            quotelineDetail.SuggestedResale = quotelineDetail.BaseResalePrice * (1 + (PricingUsed.MarkUp / 100));
                                        }
                                        else
                                        {
                                            quotelineDetail.SuggestedResale = quotelineDetail.BaseResalePriceWOPromos * (1 + (PricingUsed.MarkUp / 100));
                                        }

                                        if (!quotelineDetail.ManualUnitPrice)
                                            quotelineDetail.unitPrice = quotelineDetail.SuggestedResale;
                                    }
                                    else
                                    {
                                        if (quotelineDetail.BaseResalePrice.HasValue &&
                                            quotelineDetail.BaseResalePrice.Value > 0)
                                        {
                                            quotelineDetail.SuggestedResale = quotelineDetail.BaseResalePrice;
                                        }
                                        else
                                        {
                                            quotelineDetail.SuggestedResale = quotelineDetail.BaseResalePriceWOPromos;
                                        }
                                        if (!quotelineDetail.ManualUnitPrice)
                                            quotelineDetail.unitPrice = quotelineDetail.SuggestedResale;
                                    }
                                }
                                else if (ql.ProductTypeId == 5 && Franchise.BrandId == 2)
                                {
                                    if (PricingUsed.MarkUpFactor == "$")
                                    {
                                        quotelineDetail.SuggestedResale = quotelineDetail.BaseResalePriceWOPromos + PricingUsed.MarkUp;
                                        if (!quotelineDetail.ManualUnitPrice)
                                            quotelineDetail.unitPrice = quotelineDetail.SuggestedResale;
                                    }
                                    if (PricingUsed.MarkUpFactor == "%")
                                    {
                                        quotelineDetail.SuggestedResale = quotelineDetail.BaseResalePriceWOPromos * (1 + (PricingUsed.MarkUp / 100));
                                        if (!quotelineDetail.ManualUnitPrice)
                                            quotelineDetail.unitPrice = quotelineDetail.SuggestedResale;
                                    }
                                }
                                else if (ql.ProductTypeId == 5 || ql.ProductTypeId == 3)
                                {
                                    quotelineDetail.SuggestedResale = quotelineDetail.BaseResalePriceWOPromos;
                                }

                                //Discount is set up as inline discount
                                //if (PricingUsed.Discount.HasValue)
                                //{
                                //    //Apply Pricing rule discount only if the unit price is not edited manually
                                //    if (!quotelineDetail.ManualUnitPrice)
                                //        quotelineDetail.unitPrice =
                                //            quotelineDetail.SuggestedResale -
                                //            (quotelineDetail.SuggestedResale * (PricingUsed.Discount.Value / 100));
                                //}
                            }
                            else
                            {
                                if (quotelineDetail.BaseResalePrice.HasValue &&
                                    quotelineDetail.BaseResalePrice.Value > 0)
                                {
                                    quotelineDetail.SuggestedResale = quotelineDetail.BaseResalePrice;
                                }
                                else
                                {
                                    quotelineDetail.SuggestedResale = quotelineDetail.BaseResalePriceWOPromos;
                                }

                                //Update unit price only if unitprice is not edited
                                if (!quotelineDetail.ManualUnitPrice)
                                {
                                    quotelineDetail.unitPrice = quotelineDetail.SuggestedResale;
                                }
                            }

                            #endregion Markup

                            #region Rounding

                            int round = 0;
                            if (ql.ProductTypeId != 5 && ql.ProductTypeId != 3)
                            {
                                if (rounding == "WD")
                                {
                                    if (!quotelineDetail.ManualUnitPrice)
                                    {
                                        quotelineDetail.unitPrice = quotelineDetail.unitPrice.HasValue
                                            ? (decimal?)Math.Ceiling(quotelineDetail.unitPrice.Value)
                                            : null;
                                    }

                                    quotelineDetail.SuggestedResale = quotelineDetail.SuggestedResale.HasValue
                                        ? (decimal?)Math.Ceiling(quotelineDetail.SuggestedResale.Value)
                                        : null;
                                }
                                else if (int.TryParse(rounding, out round) && round == 5)
                                {
                                    if (!quotelineDetail.ManualUnitPrice)
                                    {
                                        quotelineDetail.unitPrice = quotelineDetail.unitPrice.HasValue
                                            ? (decimal?)Math.Ceiling(quotelineDetail.unitPrice.Value / 5) * 5
                                            : null;
                                    }

                                    quotelineDetail.SuggestedResale = quotelineDetail.SuggestedResale.HasValue
                                        ? (decimal?)Math.Ceiling(quotelineDetail.SuggestedResale.Value / 5) * 5
                                        : null;
                                }
                                else if (int.TryParse(rounding, out round) && round == 10)
                                {
                                    if (!quotelineDetail.ManualUnitPrice)
                                    {
                                        quotelineDetail.unitPrice = quotelineDetail.unitPrice.HasValue
                                            ? (decimal?)Math.Ceiling(quotelineDetail.unitPrice.Value / 10) * 10
                                            : null;
                                    }

                                    quotelineDetail.SuggestedResale = quotelineDetail.SuggestedResale.HasValue
                                        ? (decimal?)Math.Ceiling(quotelineDetail.SuggestedResale.Value / 10) * 10
                                        : null;
                                }
                            }

                            #endregion Rounding

                            #region inline Discount

                            if (quotelineDetail.Discount != null)
                            {
                                if (quotelineDetail.SelectedPrice == "BaseResalePrice")
                                {
                                    if (!quotelineDetail.ManualUnitPrice)
                                    {
                                        quotelineDetail.DiscountAmount = (quotelineDetail.BaseResalePrice * (quotelineDetail.Discount / 100));
                                        quotelineDetail.unitPrice = quotelineDetail.BaseResalePrice - (quotelineDetail.BaseResalePrice * (quotelineDetail.Discount / 100));
                                    }
                                    else
                                    {
                                        quotelineDetail.DiscountAmount = 0;
                                        //quotelineDetail.DiscountAmount = (quotelineDetail.unitPrice * (quotelineDetail.Discount / 100));
                                        //if (qLid != 0 && qLid == ql.QuoteLineId)
                                        //    quotelineDetail.unitPrice = quotelineDetail.unitPrice - (quotelineDetail.unitPrice * (quotelineDetail.Discount / 100));
                                    }
                                }
                                else if (quotelineDetail.SelectedPrice == "BaseResalePriceWOPromos")
                                {
                                    if (!quotelineDetail.ManualUnitPrice)
                                    {
                                        quotelineDetail.DiscountAmount = (quotelineDetail.BaseResalePriceWOPromos * (quotelineDetail.Discount / 100));
                                        quotelineDetail.unitPrice = quotelineDetail.BaseResalePriceWOPromos - (quotelineDetail.BaseResalePriceWOPromos * (quotelineDetail.Discount / 100));
                                    }
                                    else
                                    {
                                        quotelineDetail.DiscountAmount = 0;
                                        //quotelineDetail.DiscountAmount = (quotelineDetail.unitPrice * (quotelineDetail.Discount / 100));
                                        //if (qLid != 0 && qLid == ql.QuoteLineId)
                                        //    quotelineDetail.unitPrice = quotelineDetail.unitPrice - (quotelineDetail.unitPrice * (quotelineDetail.Discount / 100));
                                    }
                                }
                                else
                                {
                                    if (!quotelineDetail.ManualUnitPrice)
                                    {
                                        quotelineDetail.DiscountAmount = (quotelineDetail.SuggestedResale * (quotelineDetail.Discount / 100));
                                        quotelineDetail.unitPrice = quotelineDetail.SuggestedResale - (quotelineDetail.SuggestedResale * (quotelineDetail.Discount / 100));
                                    }
                                    else
                                    {
                                        quotelineDetail.DiscountAmount = 0;
                                        //quotelineDetail.DiscountAmount = (quotelineDetail.unitPrice * (quotelineDetail.Discount / 100));
                                        //if (qLid != 0 && qLid == ql.QuoteLineId)
                                        //    quotelineDetail.unitPrice = quotelineDetail.unitPrice - (quotelineDetail.unitPrice * (quotelineDetail.Discount / 100));
                                    }
                                }
                            }
                            else
                            {
                                if (quotelineDetail.SelectedPrice == "BaseResalePrice")
                                {
                                    if (!quotelineDetail.ManualUnitPrice)
                                        quotelineDetail.unitPrice = quotelineDetail.BaseResalePrice;
                                }
                                else if (quotelineDetail.SelectedPrice == "BaseResalePriceWOPromos")
                                {
                                    if (!quotelineDetail.ManualUnitPrice)
                                        quotelineDetail.unitPrice = quotelineDetail.BaseResalePriceWOPromos;
                                }
                                else
                                {
                                    if (!quotelineDetail.ManualUnitPrice)
                                        quotelineDetail.unitPrice = quotelineDetail.SuggestedResale;
                                }
                            }

                            #endregion inline Discount

                            #region Final prices by line

                            quotelineDetail.ExtendedPrice = quotelineDetail.unitPrice * quotelineDetail.Quantity;

                            ql.SuggestedResale = quotelineDetail.SuggestedResale;
                            ql.Discount = quotelineDetail.Discount;

                            ql.Quantity = quotelineDetail.Quantity;
                            ql.UnitPrice = quotelineDetail.unitPrice;

                            ql.ExtendedPrice = quotelineDetail.ExtendedPrice;

                            if (ql.ProductTypeId == 1 || ql.ProductTypeId == 2 || ql.ProductTypeId == 5)
                            {
                                //quotelineDetail.NetCharge = (quotelineDetail.ExtendedPrice.HasValue ? quotelineDetail.ExtendedPrice.Value : 0) * nvr;
                            }
                            else if (ql.ProductTypeId == 3)
                            {
                                quotelineDetail.NetCharge = quotelineDetail.ExtendedPrice.HasValue
                                    ? quotelineDetail.ExtendedPrice.Value
                                    : 0;
                            }
                            if (quotelineDetail.unitPrice > 0)
                            {
                                if (ql.ProductTypeId == 1 || ql.ProductTypeId == 2 || ql.ProductTypeId == 5)
                                {
                                    //if (nvr > 0)
                                    //{
                                    //    quotelineDetail.MarginPercentage = ((quotelineDetail.unitPrice * nvr - quotelineDetail.BaseCost) / (quotelineDetail.unitPrice * nvr)) * 100;
                                    //}
                                    //else
                                    //{
                                    //    quotelineDetail.MarginPercentage = 0;
                                    //}
                                }
                                else if (ql.ProductTypeId == 3)
                                {
                                    quotelineDetail.MarginPercentage = ((quotelineDetail.unitPrice - quotelineDetail.BaseCost) / quotelineDetail.unitPrice) * 100;
                                }
                            }
                            else
                            {
                                quotelineDetail.MarginPercentage = 0;
                            }

                            #endregion Final prices by line

                            #region Save

                            quotelineDetail.LastUpdatedBy = user;
                            quotelineDetail.LastUpdatedOn = datetime;

                            ql.LastUpdatedBy = user;
                            ql.LastUpdatedOn = datetime;

                            //connection.Update<QuoteLineDetail>(quotelineDetail);
                            //connection.Update<QuoteLines>(ql);

                            #endregion Save
                        }
                    }

                    connection.BulkUpdate<QuoteLineDetail>(quotelineDetaillist);
                    connection.BulkUpdate<QuoteLines>(qLinesObj);

                    decimal nvr = CalculateNVR(quote.QuoteKey);
                    foreach (var ql in qLinesObj)
                    {
                        var quotelineDetail = quotelineDetaillist.Where(x => x.QuoteLineId == ql.QuoteLineId).FirstOrDefault();

                        if (ql.ProductTypeId == 1 || ql.ProductTypeId == 2 || ql.ProductTypeId == 5)
                        {
                            quotelineDetail.NetCharge = (quotelineDetail.ExtendedPrice.HasValue ? quotelineDetail.ExtendedPrice.Value : 0) * nvr;
                        }
                        else if (ql.ProductTypeId == 3)
                        {
                            quotelineDetail.NetCharge = quotelineDetail.ExtendedPrice.HasValue
                                ? quotelineDetail.ExtendedPrice.Value
                                : 0;
                        }
                        if (quotelineDetail.unitPrice > 0)
                        {
                            if (ql.ProductTypeId == 1 || ql.ProductTypeId == 2 || ql.ProductTypeId == 5)
                            {
                                if (nvr > 0)
                                {
                                    quotelineDetail.MarginPercentage = ((quotelineDetail.unitPrice * nvr - quotelineDetail.BaseCost) / (quotelineDetail.unitPrice * nvr)) * 100;
                                }
                                else
                                {
                                    quotelineDetail.MarginPercentage = 0;
                                }
                            }
                            else if (ql.ProductTypeId == 3)
                            {
                                quotelineDetail.MarginPercentage = ((quotelineDetail.unitPrice - quotelineDetail.BaseCost) / quotelineDetail.unitPrice) * 100;
                            }
                        }
                        else
                        {
                            quotelineDetail.MarginPercentage = 0;
                        }
                        //connection.Update<QuoteLineDetail>(quotelineDetail);
                        //connection.Update<QuoteLines>(ql);
                    }

                    connection.BulkUpdate<QuoteLineDetail>(quotelineDetaillist);
                    connection.BulkUpdate<QuoteLines>(qLinesObj);
                    UpdateQuote(quoteKey, qLid, ignoreTaxUpdate);
                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }


        public decimal CalculateNVR(int quoteKey)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();

                    var fQuery = "SELECT  CRM.CalculateNVR (@quoteKey)";


                    var fDetails = connection.QueryMultiple(fQuery, new { quoteKey = quoteKey });

                    var nvr = fDetails.Read<decimal>().FirstOrDefault();

                    return nvr;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public decimal CalculateNVR(Quote quote, List<QuoteLines> quotelines)
        {
            var arr = new List<decimal>();
            decimal quoteDiscount = 1;
            decimal productTotal = quotelines.Where(x => x.ExtendedPrice.HasValue && (x.ProductTypeId == 1 || x.ProductTypeId == 2 || x.ProductTypeId == 5)).ToList().Sum(y => y.ExtendedPrice).Value;
            decimal discountTotal = 0;
            foreach (var ql in quotelines)
            {
                if (ql.ProductTypeId == 4)
                {
                    if (!string.IsNullOrEmpty(ql.DiscountType))
                    {
                        if (ql.DiscountType == "%")
                        {
                            decimal val = (1 - ((ql.Discount.HasValue ? ql.Discount.Value : 0)) / 100);
                            if (val != 1)
                            {
                                arr.Add(val);
                            }
                        }
                        else
                        {
                            discountTotal = discountTotal + (ql.Discount.HasValue ? ql.Discount.Value : 0);
                        }
                    }
                }
                //if (ql.ProductTypeId == 1 || ql.ProductTypeId == 2 || ql.ProductTypeId == 5)
                //{
                //    productTotal = productTotal + (ql.ExtendedPrice.HasValue ? ql.ExtendedPrice.Value : 0);
                //}
            }
            if (quote.DiscountType != null)
            {
                if (quote.DiscountType == "$")
                {
                    discountTotal = discountTotal + (quote.Discount.HasValue ? quote.Discount.Value : 0);
                    //if (productTotal != 0)
                    //{
                    //    quoteDiscount = 1 - (discountTotal / productTotal);
                    //}
                    //arr.Add(quoteDiscount);
                }
                else if (quote.DiscountType == "%")
                {
                    quoteDiscount = (100 - (quote.Discount.HasValue ? quote.Discount.Value : 0)) / 100;
                    arr.Add(quoteDiscount);
                }
            }
            //else
            //{
            if (productTotal != 0)
            {
                quoteDiscount = 1 - (discountTotal / productTotal);
                arr.Add(quoteDiscount);
            }

            if (arr.Count > 0)
            {
                return arr.Aggregate((a, x) => a * x);
            }
            else
            {
                return 1;
            }
        }

        public bool UpdateMargins(int quoteKey, QuoteLineDetail quotelineDetail)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var fQuery = @"select * from [CRM].[Quote] where [QuoteKey] = @QuoteKey;
                                   select ql.* from crm.quotelines ql inner join crm.quotelinedetail  qd on ql.quotelineid = qd.quotelineid where ql.quotekey = @QuoteKey;
                                   select qd.* from crm.quotelines ql inner join crm.quotelinedetail  qd on ql.quotelineid = qd.quotelineid where ql.quotekey = @QuoteKey;";
                    connection.Open();

                    var qDetails = connection.QueryMultiple(fQuery, new { QuoteKey = quoteKey });
                    var quote = qDetails.Read<Quote>().FirstOrDefault();
                    var qLinesObj = qDetails.Read<QuoteLines>().ToList();
                    var qldlist = qDetails.Read<QuoteLineDetail>().ToList();

                    var ql = qLinesObj.Where(x => x.QuoteLineId == quotelineDetail.QuoteLineId).FirstOrDefault();

                    var user = User.PersonId;
                    var datetime = DateTime.UtcNow;

                    var nvr = quote.NVR;
                    for (int i = 0; i < qldlist.Count; i++)
                    {
                        if (qldlist[i].QuoteLineId == quotelineDetail.QuoteLineId)
                        {
                            qldlist[i] = quotelineDetail;

                            if (qldlist[i].unitPrice > 0)
                            {
                                if (ql.ProductTypeId == 1 || ql.ProductTypeId == 2 || ql.ProductTypeId == 5)
                                {
                                    if (nvr > 0)
                                    {
                                        qldlist[i].MarginPercentage = ((qldlist[i].unitPrice * nvr - qldlist[i].BaseCost) / (qldlist[i].unitPrice * nvr)) * 100;
                                    }
                                    else
                                    {
                                        qldlist[i].MarginPercentage = 0;
                                    }
                                }
                                else if (ql.ProductTypeId == 3)
                                {
                                    qldlist[i].MarginPercentage = ((qldlist[i].unitPrice - qldlist[i].BaseCost) / qldlist[i].unitPrice) * 100;
                                }
                            }
                            else
                            {
                                qldlist[i].MarginPercentage = 0;
                            }

                            connection.Update(qldlist[i]);
                        }
                    }

                    decimal qSubTotal = 0;
                    decimal coreCost = 0;
                    decimal coreTotal = 0;
                    decimal myProductTotal = 0;
                    decimal myProductCost = 0;
                    decimal serviceCost = 0;
                    decimal netCharges = 0;
                    // decimal productSubtotal = 0;
                    decimal additionalCharges = 0;

                    if (qLinesObj != null && qLinesObj.Count > 0)
                        foreach (var qline in qLinesObj)
                        {
                            var details = qldlist.Where(x => x.QuoteLineId == qline.QuoteLineId).FirstOrDefault();
                            if (qline.ProductTypeId == 1)//core products
                            {
                                coreTotal = coreTotal + (details.ExtendedPrice.HasValue ? details.ExtendedPrice.Value : 0);
                                coreCost = coreCost + ((details.BaseCost.HasValue ? Convert.ToDecimal(details.BaseCost.Value) : 0) * (qline.Quantity.HasValue ? qline.Quantity.Value : 0));
                            }
                            else if (qline.ProductTypeId == 2 || qline.ProductTypeId == 5)//My Products
                            {
                                myProductTotal = myProductTotal + (details.ExtendedPrice.HasValue ? details.ExtendedPrice.Value : 0);
                                myProductCost = myProductCost + ((details.BaseCost.HasValue ? Convert.ToDecimal(details.BaseCost.Value) : 0) * (qline.Quantity.HasValue ? qline.Quantity.Value : 0));
                            }
                            else if (qline.ProductTypeId == 3) //Services
                            {
                                additionalCharges = additionalCharges + (details.ExtendedPrice.HasValue ? details.ExtendedPrice.Value : 0);
                                serviceCost = serviceCost + ((details.BaseCost.HasValue ? Convert.ToDecimal(details.BaseCost.Value) : 0) * (qline.Quantity.HasValue ? qline.Quantity.Value : 0));
                            }
                        }
                    quote.ProductSubTotal = coreTotal + myProductTotal;
                    qSubTotal = coreTotal + additionalCharges + myProductTotal;
                    quote.TotalMargin = (qSubTotal - (coreCost + myProductCost + serviceCost));
                    quote.NetCharges = (quote.ProductSubTotal * quote.NVR) + additionalCharges;
                    quote.AdditionalCharges = additionalCharges;
                    //quote.ProductSubTotal = coreCost + myProductTotal;

                    if (coreTotal != 0)
                    {
                        quote.CoreProductMargin = (coreTotal * quote.NVR.Value) - coreCost;
                        if (quote.NVR.HasValue && quote.NVR.Value > 0)
                        {
                            quote.CoreProductMarginPercent = (quote.CoreProductMargin / (coreTotal * quote.NVR.Value)) * 100;
                        }
                        else
                        {
                            quote.CoreProductMarginPercent = 0;
                        }
                    }
                    if (myProductTotal != 0)
                    {
                        quote.MyProductMargin = (myProductTotal * quote.NVR.Value) - myProductCost;
                        if (quote.NVR.HasValue && quote.NVR.Value > 0)
                        {
                            quote.MyProductMarginPercent = (quote.MyProductMargin / (myProductTotal * quote.NVR.Value)) * 100;
                        }
                        else
                        {
                            quote.MyProductMarginPercent = 0;
                        }
                    }

                    if (additionalCharges != 0)
                    {
                        quote.ServiceMargin = additionalCharges - serviceCost;
                        quote.ServiceMarginPercent = (quote.ServiceMargin / additionalCharges) * 100;
                    }

                    quote.TotalMargin = quote.CoreProductMargin + quote.MyProductMargin + quote.ServiceMargin;
                    if (quote.NetCharges > 0)
                        quote.TotalMarginPercent = (quote.TotalMargin / quote.NetCharges) * 100;

                    quote.TotalDiscounts = (coreTotal + myProductTotal) - ((coreTotal + myProductTotal) * quote.NVR);

                    quote.QuoteSubTotal = qSubTotal - quote.TotalDiscounts;

                    connection.Update(quote);
                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool UpdatequotMargin(int quoteKey, QuoteMarginReviewVM model)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();

                    var fQuery = @"select qd.* from crm.quotelines ql inner join crm.quotelinedetail  qd on ql.quotelineid = qd.quotelineid where ql.quotelineid = @QuoteLineId;";

                    var fDetails = connection.QueryMultiple(fQuery, new { FranchiseId = Franchise.FranchiseId, QuoteLineId = model.QuoteLineId });

                    var quotelineDetail = fDetails.Read<QuoteLineDetail>().FirstOrDefault();

                    if (quotelineDetail.BaseCost != model.BaseCost && quotelineDetail.SelectedPrice == model.SelectedPrice)
                    {
                        quotelineDetail.SelectedPrice = model.SelectedPrice;
                        if (quotelineDetail.BaseCost != model.BaseCost)
                        {
                            var qlcc = new QuoteLineCostChange();
                            qlcc.Cost = model.BaseCost;
                            qlcc.OldCost = quotelineDetail.BaseCost;
                            qlcc.QuoteKey = quoteKey;
                            qlcc.QuoteLineId = model.QuoteLineId;
                            qlcc.QuoteLineNumber = model.QuoteLineNumber;
                            qlcc.EditedBy = User.PersonId;
                            qlcc.EditedOn = DateTime.UtcNow;
                            connection.Insert<QuoteLineCostChange>(qlcc);
                        }
                        quotelineDetail.BaseCost = model.BaseCost;
                        UpdateMargins(quoteKey, quotelineDetail);
                    }
                    else
                    {
                        quotelineDetail.SelectedPrice = model.SelectedPrice;
                        quotelineDetail.BaseCost = model.BaseCost;
                        if (quotelineDetail.BaseCost != model.BaseCost)
                        {
                            var qlcc = new QuoteLineCostChange();
                            qlcc.Cost = model.BaseCost;
                            qlcc.OldCost = quotelineDetail.BaseCost;
                            qlcc.QuoteKey = quoteKey;
                            qlcc.QuoteLineId = model.QuoteLineId;
                            qlcc.QuoteLineNumber = model.QuoteLineNumber;
                            qlcc.EditedBy = User.PersonId;
                            qlcc.EditedOn = DateTime.UtcNow;
                            connection.Insert<QuoteLineCostChange>(qlcc);
                        }
                        quotelineDetail.LastUpdatedBy = User.PersonId;
                        quotelineDetail.LastUpdatedOn = DateTime.UtcNow;

                        connection.Update<QuoteLineDetail>(quotelineDetail);

                        var res = RecalculateQuoteLines(quoteKey, 0);
                    }

                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public List<QuoteLineCostChange> GetCostChangeAudit(int quoteKey)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();

                    var qlcc = ExecuteIEnumerableObject<QuoteLineCostChange>(ContextFactory.CrmConnectionString,
                        @"SELECT p.FirstName+', '+p.LastName AS PersonName,        qcc.Id,        qcc.QuoteLineId,        qcc.QuoteKey,        qcc.OldCost,        qcc.Cost,        ql.QuoteLineNumber,        qcc.EditedBy,        qcc.EditedOn FROM History.QuoteLineCostChange qcc INNER JOIN crm.QuoteLines ql  ON ql.QuoteLineId=qcc.QuoteLineId LEFT JOIN crm.Person p ON p.PersonId=qcc.EditedBy WHERE qcc.QuoteKey = @quotekey",
                        new
                        {
                            quotekey = quoteKey
                        }).ToList();
                    foreach (var item in qlcc)
                    {
                        item.EditedOn = TimeZoneManager.ToLocal(item.EditedOn);
                    }
                    return qlcc;
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public double ConvertMeasurmentFractionToDecimal(string fraction)
        {
            double response = 0;
            if (!string.IsNullOrEmpty(fraction))
            {
                switch (fraction)
                {
                    case "1/8":
                        response = .125;
                        break;

                    case "2/8":
                        response = .25;
                        break;

                    case "3/8":
                        response = .375;
                        break;

                    case "4/8":
                        response = .5;
                        break;

                    case "5/8":
                        response = .625;
                        break;

                    case "6/8":
                        response = .75;
                        break;

                    case "7/8":
                        response = .875;
                        break;
                }
            }
            return response;
        }

        public decimal FractionToDecimal(string fraction)
        {
            decimal result;

            if (decimal.TryParse(fraction, out result))
            {
                return result;
            }

            string[] split = fraction.Split(new char[] { ' ', '/' });

            if (split.Length == 2 || split.Length == 3)
            {
                int a, b;

                if (int.TryParse(split[0], out a) && int.TryParse(split[1], out b))
                {
                    if (split.Length == 2)
                    {
                        return (decimal)a / b;
                    }

                    int c;

                    if (int.TryParse(split[2], out c))
                    {
                        return a + (decimal)b / c;
                    }
                }
            }

            throw new FormatException("Not a valid fraction.");
        }

        public Person GetSalesAgentByQuoteKey(int QuoteKey)
        {
            string query = @"select * from CRM.Person p
                            join CRM.Opportunities op on p.PersonId=op.SalesAgentId
                            join CRM.Quote q on op.OpportunityId=q.OpportunityId
                            where QuoteKey=@QuoteKey";
            return ExecuteIEnumerableObject<Person>(ContextFactory.CrmConnectionString, query, new { QuoteKey }).FirstOrDefault();
        }

        public CustomerTP GetCustomerByQuoteKey(int QuoteKey)
        {
            string query = @"select * from CRM.Customer c
                            join CRM.AccountCustomers ac on c.CustomerId=ac.CustomerId and ac.IsPrimaryCustomer=1
                            join CRM.Accounts a on ac.AccountId=a.AccountId
                            join CRM.Opportunities op on a.AccountId=op.AccountId
                            join CRM.Quote q on op.OpportunityId=q.OpportunityId
                            where QuoteKey=@QuoteKey";
            return ExecuteIEnumerableObject<CustomerTP>(ContextFactory.CrmConnectionString, query, new { QuoteKey }).FirstOrDefault();
        }

        public AddressTP GetBillingAddressByQuoteKey(int QuoteKey)
        {
            string query = @"select * from CRM.Addresses ad
                            join CRM.Opportunities op on ad.AddressId=op.BillingAddressId
                            join CRM.Quote q on op.OpportunityId=q.OpportunityId
                            where QuoteKey=@QuoteKey";
            return ExecuteIEnumerableObject<AddressTP>(ContextFactory.CrmConnectionString, query, new { QuoteKey }).FirstOrDefault();
        }

        public AddressTP GetInstallationAddressByQuoteKey(int QuoteKey)
        {
            string query = @"select * from CRM.Addresses ad
                            join CRM.Opportunities op on ad.AddressId=op.InstallationAddressId
                            join CRM.Quote q on op.OpportunityId=q.OpportunityId
                            where QuoteKey=@QuoteKey";
            return ExecuteIEnumerableObject<AddressTP>(ContextFactory.CrmConnectionString, query, new { QuoteKey }).FirstOrDefault();
        }

        public Opportunity GetOpportunityByQuoteKey(int QuoteKey)
        {
            string query = @"select * from CRM.Opportunities op
                            join CRM.Quote q on op.OpportunityId=q.OpportunityId
                            where QuoteKey=@QuoteKey";
            return ExecuteIEnumerableObject<Opportunity>(ContextFactory.CrmConnectionString, query, new { QuoteKey }).FirstOrDefault();
        }

        public QuoteVM GetQuoteInfo(int QuoteKey)
        {
            QuoteVM result = new QuoteVM();

            //string quoteQ = "select q.*,[CRM].[fnGetAccountName_forOpportunitySearch] (q.OpportunityId) as AccountName from CRM.Quote q where QuoteKey=@QuoteKey";
            string quoteQ = @"select q.*,(select [CRM].[fnGetAccountNameByAccountId](ac.AccountId)) as AccountName
					 from CRM.Quote q
					 join [CRM].[Opportunities] o on q.OpportunityId=o.OpportunityId
                     left join [CRM].[Accounts] ac on o.AccountId=ac.AccountId
					 where q.QuoteKey=@QuoteKey";
            result = ExecuteIEnumerableObject<QuoteVM>(ContextFactory.CrmConnectionString, quoteQ, new { QuoteKey }).FirstOrDefault();

            if (result == null)
                return null;

            result.QuoteLinesVM = GetQuoteLine(result.QuoteKey);

            string taxinfoQ = @"select t.* from CRM.Tax t
                                    join CRM.QuoteLines ql on t.QuoteLineId=ql.QuoteLineId
                                    where ql.QuoteKey=@QuoteKey";
            result.Taxinfo = ExecuteIEnumerableObject<Tax>(ContextFactory.CrmConnectionString, taxinfoQ, new { QuoteKey }).ToList();

            string DiscountsAndPromoQ = @"select dp.* from CRM.DiscountsAndPromo dp
                                    join CRM.QuoteLines ql on dp.QuoteLineId=ql.QuoteLineId
                                     where ql.QuoteKey=@QuoteKey";
            result.DiscountsAndPromo = ExecuteIEnumerableObject<DiscountsAndPromo>(ContextFactory.CrmConnectionString, DiscountsAndPromoQ, new { QuoteKey }).ToList();
            //if(result.QuoteStatusId==2 && result.ExpiryDate == null)
            //{
            //    int expiryday = GetExpiry(Franchise.FranchiseId);
            //    var createddate = result.CreatedOn;
            //    result.ExpiryDate = createddate.AddDays(expiryday).DateTime;
            //}
            return result;
        }

        public Quote GetQuoteData(int QuoteKey)
        {
            string quoteQ = "select q.* from CRM.Quote q where QuoteKey=@QuoteKey";
            return ExecuteIEnumerableObject<Quote>(ContextFactory.CrmConnectionString, quoteQ, new { QuoteKey }).FirstOrDefault();
        }

        public Quote GetQuoteDataforPrimary(int OpportunityId)
        {
            string quoteQ = "select q.* from CRM.Quote q where OpportunityId=@OpportunityId and q.PrimaryQuote=1;";
            return ExecuteIEnumerableObject<Quote>(ContextFactory.CrmConnectionString, quoteQ, new { OpportunityId = OpportunityId }).FirstOrDefault();
        }

        public AccountTP getTaxExempt(int QuoteStatusId, int OpportunityId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var resAcc = connection.Query<AccountTP>("select * from [CRM].[Accounts] where [AccountId] = (Select AccountId from [CRM].[Opportunities] where OpportunityId=@OpportunityId)", new { OpportunityId = OpportunityId }).FirstOrDefault();

                    return resAcc;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool changeIsTaxExempt(int Id, int QuoteId, int OpportunityId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    if (Id == 0)
                    {
                        var resAcc = connection.Query<AccountTP>("select * from [CRM].[Accounts] where [AccountId] = (Select AccountId from [CRM].[Opportunities] where OpportunityId=@OpportunityId)", new { OpportunityId = OpportunityId }).FirstOrDefault();

                        connection.Query("Update [CRM].[Quote] set IsTaxExempt=@IsTaxExempt, TaxExemptID=@TaxExemptID where QuoteID=@QuoteID", new { IsTaxExempt = resAcc.IsTaxExempt, TaxExemptID = resAcc.TaxExemptID, QuoteID = QuoteId });

                        return true;
                    }
                    else
                    {
                        connection.Query("Update [CRM].[Quote] set IsTaxExempt = @IsTaxExempt where QuoteID=@QuoteID", new { IsTaxExempt = false, QuoteID = QuoteId });
                        return false;
                    }
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool ClearQuoteslines(int QuoteStatusId, int QuoteKey)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var res_quote = connection.Query<Quote>("select * from CRM.Quote where QuoteStatusId=1 and QuoteKey = @QuoteKey", new { QuoteKey = QuoteKey }).FirstOrDefault();

                    connection.Open();
                    if (res_quote != null)
                    {
                        var res_QuoteLines = connection.Query<QuoteLines>("select * from CRM.QuoteLines where  QuoteKey = @QuoteKey", new { QuoteKey = QuoteKey }).ToList();

                        if (res_QuoteLines.Count > 0)
                        {
                            var qIds = new List<int>();
                            foreach (var item in res_QuoteLines)
                            {
                                qIds.Add(item.QuoteLineId);
                            }
                            CopyDeleteQuoteline(qIds, "DELETE");
                            //connection.Query("Delete from CRM.QuoteLineDetail where QuoteLineId	in (select QuoteLineId from CRM.QuoteLines where QuoteKey = @QuoteKey)", new { QuoteKey = QuoteKey });
                            //connection.Query("Delete From CRM.QuoteLines where QuoteKey = @QuoteKey", new { QuoteKey = QuoteKey });
                            UpdateQuote(QuoteKey);
                            return true;
                        }
                        else return true;
                    }
                    else return false;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public string UpdateQuotesStatus(int quoteKey, int quoteStatus)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    //var quote = connection.Query<Quote>("select * from CRM.Quote where  QuoteKey = @QuoteKey", new { QuoteKey = quoteKey }).FirstOrDefault();
                    var query = @"Update CRM.Quote set QuoteStatusId=@statusId where QuoteKey =@QuoteKey";
                    var result = ExecuteIEnumerableObject<int>(ContextFactory.CrmConnectionString, query, new
                    {
                        statusId = quoteStatus,
                        QuoteKey = quoteKey
                    });
                    return "Sucessful";
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        #region Batch Product Insert

        public bool InsertBatchMyProductConfiguration(int quoteKey, List<MyVendorProductVM> myProductVmList)
        {
            try
            {
                foreach (var item in myProductVmList)
                {
                    var quoteLine = InsertEmptyQuoteline(quoteKey);
                    var res = UpdateMyProductConfigurationConfiguration(item, quoteLine);
                    var response = AddUpdateInsertQuoteLineNumber(quoteKey);
                }
                UpdateQuote(quoteKey);
                var groupDiscountResponse = ApplyGroupDiscount(quoteKey);
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string UpdateMyProductConfigurationConfiguration(MyVendorProductVM quotelineobj, QuoteLines quoteline)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var oneTimeprdQuery = @"INSERT INTO CRM.FranchiseOneTimeProduct (FranchiseId ,VendorName ,ProductName ,Description ,Cost ,UnitPrice ,CreatedOn ,CreatedBy ,UpdatedOn ,UpdatedBy) VALUES (@FranchiseId ,@VendorName ,@ProductName ,@Description ,@Cost ,@UnitPrice ,@CreatedOn ,@CreatedBy ,@UpdatedOn ,@UpdatedBy);SELECT CAST(SCOPE_IDENTITY() as int);";

                    connection.Open();

                    dynamic inputobj = new ExpandoObject();
                    var dateTime = DateTime.UtcNow;
                    var user = User.PersonId;
                    var Description = "";
                    decimal cost = 0, price = 0;
                    Int32 productId = 0, VendorID = 0;

                    var details = quoteline.QuoteLineDetail;
                    var ql = quoteline;

                    if (Convert.ToInt32(quotelineobj.ProductTypeId) == 5 && quotelineobj.Vendor != null && quotelineobj.Vendor != "")
                    {
                        var ress = connection.Query(@"
                                            select fv.VendorId,hfc.Name from Acct.FranchiseVendors fv
                                            join Acct.HFCVendors hfc on fv.VendorId=hfc.VendorId
                                            where fv.VendorType=1 and fv.FranchiseId=2 and fv.VendorStatus=1 and  hfc.Name =@VendorName",
                                             new { VendorName = quotelineobj.Vendor }).FirstOrDefault();
                        if (ress != null) ql.VendorId = quotelineobj.VendorId = ress.VendorId;
                    }

                    ql.VendorName = quotelineobj.Vendor;
                    ql.ProductName = quotelineobj.ProductName;
                    ql.Description = quotelineobj.Description;
                    ql.UnitPrice = Convert.ToDecimal(quotelineobj.UnitPrice);

                    ql.RoomName = quotelineobj.RoomName;
                    ql.MeasurementsetId = quotelineobj.MeasurementsetId;
                    ql.MountType = quotelineobj.MountType;
                    ql.Width = quotelineobj.Width;
                    ql.FranctionalValueWidth = quotelineobj.FranctionalValueWidth;
                    ql.Height = quotelineobj.Height;
                    ql.FranctionalValueHeight = quotelineobj.FranctionalValueHeight;

                    var orgcost = details.BaseCost;
                    details.BaseCost = Convert.ToDecimal(quotelineobj.Cost);
                    details.unitPrice = Convert.ToDecimal(quotelineobj.UnitPrice);

                    inputobj.VendorName = quotelineobj.Vendor;
                    inputobj.ProductName = quotelineobj.ProductName;
                    inputobj.PCategory = quotelineobj.ProductCategory;
                    inputobj.PicVendor = "";
                    inputobj.PicProduct = quotelineobj.PICProductId;
                    inputobj.ProductId = quotelineobj.ProductId;
                    inputobj.ModelId = "";
                    inputobj.GGroup = quotelineobj.PICGroupId;
                    inputobj.QuoteLineId = quotelineobj.QuotelineId;
                    inputobj.Quantity = quotelineobj.Quantity;
                    inputobj.VendorId = quotelineobj.VendorId;
                    cost = Convert.ToDecimal(quotelineobj.Cost);

                    if (Convert.ToInt32(quotelineobj.ProductTypeId) == 5)
                    {
                        cost = Convert.ToDecimal(quotelineobj.Cost);
                        if (Franchise.BrandId == 2)
                        {
                            if (quotelineobj.BaseResalePriceWOPromos.HasValue)
                            {
                                price = Convert.ToDecimal(quotelineobj.BaseResalePriceWOPromos.Value);
                            }
                            else
                            {
                                price = Convert.ToDecimal(quotelineobj.UnitPrice);
                            }
                        }
                        else
                        {
                            price = Convert.ToDecimal(quotelineobj.UnitPrice);
                        }

                        productId = Convert.ToInt32(inputobj.ProductId);
                        VendorID = Convert.ToInt32(inputobj.VendorId);
                        Description = quotelineobj.Description;
                        details.BaseResalePriceWOPromos = quotelineobj.BaseResalePriceWOPromos;
                        details.BaseResalePrice = quotelineobj.BaseResalePriceWOPromos;

                        if (quotelineobj.ProductId > 0)
                        {
                            productId = connection.Query<int>(oneTimeprdQuery, new
                            {
                                FranchiseId = Franchise.FranchiseId,
                                VendorName = inputobj.VendorName,
                                ProductName = inputobj.ProductName,
                                Description = Description,
                                Cost = cost,
                                UnitPrice = price,
                                CreatedOn = dateTime,
                                CreatedBy = Convert.ToInt32(user),
                                UpdatedOn = dateTime,
                                UpdatedBy = Convert.ToInt32(user)
                            }).Single();

                            inputobj.ProductId = productId;
                            ql.ProductId = productId;
                        }
                    }
                    else
                    {
                        var Fproduct = connection.Query<FranchiseProducts>("select * from [CRM].[franchiseproducts] where [ProductKey] = @ProductId", new { ProductId = quotelineobj.ProductId }).FirstOrDefault();

                        //cost = Convert.ToDecimal(Fproduct.Cost);
                        price = Convert.ToDecimal(Fproduct.SalePrice);
                        productId = Convert.ToInt32(quotelineobj.ProductKey);
                        VendorID = Convert.ToInt32(quotelineobj.VendorId);

                        if (orgcost != null && orgcost != quotelineobj.Cost)
                        {
                            var qlcc = new QuoteLineCostChange();
                            qlcc.Cost = quotelineobj.Cost;
                            qlcc.OldCost = orgcost;
                            qlcc.QuoteKey = quoteline.QuoteKey;
                            qlcc.QuoteLineId = quoteline.QuoteLineId;
                            qlcc.QuoteLineNumber = quoteline.QuoteLineNumber;
                            qlcc.EditedBy = User.PersonId;
                            qlcc.EditedOn = DateTime.UtcNow;
                            connection.Insert<QuoteLineCostChange>(qlcc);
                        }
                        if (orgcost == null && Fproduct.Cost != quotelineobj.Cost)
                        {
                            var qlcc = new QuoteLineCostChange();
                            qlcc.Cost = quotelineobj.Cost;
                            qlcc.OldCost = Fproduct.Cost;
                            qlcc.QuoteKey = quoteline.QuoteKey;
                            qlcc.QuoteLineId = quoteline.QuoteLineId;
                            qlcc.QuoteLineNumber = quoteline.QuoteLineNumber;
                            qlcc.EditedBy = User.PersonId;
                            qlcc.EditedOn = DateTime.UtcNow;
                            connection.Insert<QuoteLineCostChange>(qlcc);
                        }
                    }

                    ql.ProductTypeId = Convert.ToInt32(quotelineobj.ProductTypeId);
                    //UpdatePricingMgmtDiscount(quotelineobj.PICGroupId, quotelineobj.VendorId, quoteline.QuoteLineId);
                    if (Franchise.BrandId != 1)
                    {
                        ql.ProductCategoryId = quotelineobj.ProductCategoryId;
                        ql.ProductSubCategoryId = quotelineobj.ProductSubcategoryId;
                        ql.Discount = quotelineobj.Discount;
                    }
                    connection.Update(ql);
                    connection.Update(details);
                    CurrencyExchange currencyExchange = GetCurrencyConversion(Franchise.FranchiseId, Convert.ToInt32(quotelineobj.VendorId));

                    var result = updateProductPricing(quoteline.QuoteLineId, cost, price, currencyExchange, VendorID, quotelineobj.ProductId, "SuggestedResale", null, inputobj, Convert.ToInt32(quotelineobj.ProductTypeId),null, true);
                    if (result)
                    {
                        return Success;
                    }
                    else
                    {
                        return FailedSaveChanges;
                    }
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public QuoteLines InsertEmptyQuoteline(int quoteKey)
        {
            var personid = SessionManager.CurrentUser.PersonId;
            var datetime = DateTime.UtcNow;
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();

                    var ql = new QuoteLines();
                    ql.ProductTypeId = 0;

                    ql.QuoteKey = quoteKey;

                    ql.IsActive = true;
                    ql.ProductTypeId = 0;
                    ql.CreatedBy = personid;
                    ql.CreatedOn = datetime;

                    ql.QuoteLineId = connection.Insert<int, QuoteLines>(ql);
                    var qDetails = new QuoteLineDetail();
                    qDetails.QuoteLineId = ql.QuoteLineId;
                    qDetails.CreatedBy = ql.CreatedBy;
                    qDetails.CreatedOn = ql.CreatedOn;
                    //connection.Insert<QuoteLineDetail>(qDetails);
                    qDetails.QuoteLineDetailId = connection.Insert<int, QuoteLineDetail>(qDetails);
                    ql.QuoteLineDetail = qDetails;

                    return ql;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        #endregion Batch Product Insert

        #region BatchCoreProductInsert

        public List<QuoteLines> GetCoreConfigQuoteLines(int QuoteKey)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var query = @"SELECT ql.QuoteLineNumber,ql.MeasurementsetId,ql.QuoteLineId,ql.ProductTypeId FROM crm.quotelines ql WHERE ql.ProductTypeId =1 AND ql.QuoteKey = @QuoteKey";
                    connection.Open();
                    var resAcc = connection.Query<QuoteLines>(query, new { QuoteKey = QuoteKey }).ToList();
                    return resAcc;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public QuoteVM GetQuoteDetailsforCoreConfiguration(int QuoteKey)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var query = @"Select q.MeasurementId,o.InstallationAddressId,q.SideMark,ac.AccountId,QuoteKey,q.discountid, q.QuoteId, q.QuoteName,q.OpportunityId,OpportunityName,q.QuoteStatusId,cs.FirstName+' '+cs.LastName as SalesAgent,
                            c.FirstName+' '+c.LastName + case when c.CompanyName is not null and c.CompanyName!='' then ' / '+ c.CompanyName else '' end  as AccountName,
                            q.IsTaxExempt,q.TaxExemptID,q.NVR,
                            ISNULL(PrimaryQuote,0) PrimaryQuote,Discount,DiscountType,TotalDiscounts,NetCharges,q.CreatedOn,q.LastUpdatedOn,q.ExpiryDate ,q.AdditionalCharges, q.ProductSubTotal,q.QuoteSubTotal,ISNULL(q.CoreProductMargin,0) as CoreProductMargin ,
                            ISNULL(q.CoreProductMarginPercent,0) as  CoreProductMarginPercent ,ISNULL(q.MyProductMargin,0) as MyProductMargin ,ISNULL(q.MyProductMarginPercent,0) as MyProductMarginPercent ,ISNULL(q.ServiceMargin,0) as ServiceMargin ,
                            ISNULL(q.ServiceMarginPercent,0) as ServiceMarginPercent,ISNULL(q.TotalMargin,0) as TotalMargin,ISNULL(q.TotalMarginPercent,0) as TotalMarginPercent,(select count(*) from crm.quotelines where quotekey = @QuoteKey) as NumberOfQuotelines,
							m.MeasurementSet as Measurements
							from [CRM].[Quote] q
                            join [CRM].[Opportunities] o on q.OpportunityId=o.OpportunityId
							left join crm.measurementheader m on  m.InstallationAddressId = o.InstallationAddressId
                            left join [CRM].[Accounts] ac on o.AccountId=ac.AccountId
                            left join [CRM].[Customer] c on ac.PersonId=c.CustomerId
                            left join [CRM].[Person] cs on o.SalesAgentId=cs.Personid
                            where q.QuoteKey=@QuoteKey";
                    connection.Open();
                    var resAcc = connection.Query<QuoteVM>(query, new { QuoteKey = QuoteKey }).FirstOrDefault();
                    if (resAcc != null)
                    {
                        query = @"select cus.FirstName+' '+cus.LastName as ContactName,* from CRM.Addresses addr
                      left join CRM.Customer cus on cus.CustomerId = addr.ContactId
                      where AddressId = @addressId";
                        var insAddress = ExecuteIEnumerableObject<AddressTP>(ContextFactory.CrmConnectionString, query, new
                        {
                            addressId = resAcc.InstallationAddressId
                        }).FirstOrDefault();
                        if (insAddress != null)
                            resAcc.InstallationAddress = insAddress;
                        var bilAddress = ExecuteIEnumerableObject<AddressTP>(ContextFactory.CrmConnectionString, query, new
                        {
                            addressId = resAcc.BillingAddressId
                        }).FirstOrDefault();
                        if (bilAddress != null)
                            resAcc.BillingAddress = bilAddress;
                    }
                    return resAcc;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        #endregion BatchCoreProductInsert

        public bool UpdateBulkQuotelineDescription()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var selQuery = @"select ql.* from crm.quotelines ql
                                     INNER JOIN crm.Quote q ON q.QuoteKey = ql.QuoteKey
                                     INNER JOIN crm.Opportunities o ON o.OpportunityId = q.OpportunityId
                                     INNER JOIN crm.Franchise f ON f.FranchiseId = o.FranchiseId
                                     WHERE LEN(ql.Description)<100
                                     AND ql.ProductTypeId = 1 AND ql.QuoteLineId NOT IN (SELECT quotelineid FROM crm.QuoteCoreProductDetails qcpd)
                                     ORDER BY  ql.quotekey";

                    connection.Open();

                    var quotelist = ExecuteIEnumerableObject<QuoteLines>(ContextFactory.CrmConnectionString, selQuery);

                    if (quotelist != null && quotelist.Count() > 0)
                    {
                        foreach (var qlItem in quotelist)
                        {
                            var qcpdList = new List<QuoteCoreProductDetails>();
                            var vsiblepmtsdynamic = JsonConvert.DeserializeObject<dynamic>(qlItem.PICPriceResponse);

                            if (vsiblepmtsdynamic.visiblePrompts != null)
                            {
                                var query =
                                    @"DELETE crm.QuoteCoreProductDetails WHERE QuoteLineId = @QuoteLineId";
                                var fDetails = connection.Query(query, new { quotelineId = qlItem.QuoteLineId });

                                var obj = JsonConvert.DeserializeObject<Dictionary<string, VisiblePrompt>>(
                                    JsonConvert.SerializeObject(vsiblepmtsdynamic.visiblePrompts));

                                foreach (var key in obj.Keys)
                                {
                                    var newobj = obj[key];

                                    var qcpd = new QuoteCoreProductDetails();

                                    qcpd.Value = newobj.Value;
                                    qcpd.ColorValue = newobj.ColorValue;
                                    qcpd.Description = newobj.Description;
                                    qcpd.ValueDescription = newobj.ValueDescription;
                                    qcpd.ColorValueDescription = newobj.ColorValueDescription;
                                    qcpd.QuotelineId = qlItem.QuoteLineId;
                                    qcpd.QuoteKey = qlItem.QuoteKey;
                                    qcpdList.Add(qcpd);
                                    connection.Insert(qcpd);
                                }

                                var Description = getCoreProductDesciption(qcpdList);
                                if (!string.IsNullOrEmpty(Description))
                                {
                                    qlItem.Description = Description;
                                    connection.Update(qlItem);
                                }
                            }
                        }
                    }

                    return true;
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool UpdateQuoteLineNotes(QuoteLinesVM qlm)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var selQuery = @"select ql.* from crm.quotelines ql
                                     where ql.QuoteLineId=@quoteLineId";

                    connection.Open();

                    var quoteLineList = connection.Query<QuoteLines>(selQuery, new { quoteLineId = qlm.QuoteLineId }).FirstOrDefault();

                    if (quoteLineList != null)
                    {
                        quoteLineList.InternalNotes = qlm.InternalNotes;
                        quoteLineList.CustomerNotes = qlm.CustomerNotes;
                        quoteLineList.VendorNotes = qlm.VendorNotes;

                        connection.Update(quoteLineList);
                    }

                    return true;
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool SaveQuoteNotes(int QuoteKey, string QuoteLevelNotes, string OrderInvoiceLevelNotes, string InstallerInvoiceNotes)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var Query = @"update [CRM].[Quote] set QuoteLevelNotes=@QuoteLevelNotes,
                                 OrderInvoiceLevelNotes=@OrderInvoiceLevelNotes,InstallerInvoiceNotes=@InstallerInvoiceNotes
                                    where QuoteKey=@QuoteKey";

                    connection.Open();

                    //  var quoteLineList = connection.Query<QuoteLines>(selQuery, new { quoteLineId = qlm.QuoteLineId }).FirstOrDefault();
                    var ress = connection.Execute(Query, new { QuoteKey = QuoteKey, QuoteLevelNotes = QuoteLevelNotes, OrderInvoiceLevelNotes = OrderInvoiceLevelNotes, InstallerInvoiceNotes = InstallerInvoiceNotes });
                    return true;
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        #region Cancel Quoteline

        /// <summary>
        /// Set the quantity unitprice and extended price to zero so that the line will be cancelled
        /// </summary>
        /// <param name="quoteLineId"></param>
        /// <returns></returns>
        public bool CancelLinebyQuoteLineId(int quoteLineId, string cancelReason)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var fQuery = "select * from crm.quotelinedetail where quotelineid = @quotelineid;select * from crm.quotelines where quotelineid = @quotelineid";
                    var fDetails = connection.QueryMultiple(fQuery, new { quotelineid = quoteLineId });

                    var details = fDetails.Read<QuoteLineDetail>().First();
                    var ql = fDetails.Read<QuoteLines>().First();
                    ql.Quantity = 0;
                    ql.UnitPrice = 0;
                    ql.ExtendedPrice = 0;
                    ql.CancelReason = cancelReason;

                    details.Quantity = 0;
                    details.unitPrice = 0;
                    details.ExtendedPrice = 0;

                    connection.Update(ql);
                    connection.Update(details);

                    RecalculateQuoteLines(ql.QuoteKey, quoteLineId);

                    return true;
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        #endregion Cancel Quoteline

        public TerritoryDisplay GetTerritoryDisplayData(int quoteKey)
        {
            string query = @"select isnull(tes.UseFranchiseName,1) UseFranchiseName,tes.LicenseNumber,t.WebSiteURL,(select [CRM].[fnGetTerritoryDisplayName](t.BrandId,t.TerritoryId)) as Displayname, stl.PhoneNumber as Phone, ad.Address1,ad.Address2,ad.City,ad.State,ad.ZipCode from CRM.Quote q
                             join CRM.Opportunities op on q.OpportunityId=op.OpportunityId
                             left join CRM.Territories t on op.TerritoryId=t.TerritoryId
                             left join CRM.TerritoryEmailSettings tes on t.TerritoryId=tes.TerritoryId
                             left join CRM.ShipToLocations stl on tes.ShipToLocationId=stl.Id
                             left join CRM.Addresses ad on stl.AddressId=ad.AddressId
                             where op.FranchiseId=@FranchiseId and q.QuoteKey=@quoteKey";

            string query2 = @"select isnull(tes.UseFranchiseName,1) UseFranchiseName,tes.LicenseNumber,t.WebSiteURL,(select [CRM].[fnGetTerritoryDisplayName](t.BrandId,t.TerritoryId)) as Displayname, stl.PhoneNumber as Phone, ad.Address1,ad.Address2,ad.City,ad.State,ad.ZipCode from CRM.Quote q
                             join CRM.Opportunities op on q.OpportunityId=op.OpportunityId
                             left join CRM.Territories t on op.TerritoryDisplayId=t.TerritoryId
                             left join CRM.TerritoryEmailSettings tes on t.TerritoryId=tes.TerritoryId
                             left join CRM.ShipToLocations stl on tes.ShipToLocationId=stl.Id
                             left join CRM.Addresses ad on stl.AddressId=ad.AddressId
                             where op.FranchiseId=@FranchiseId and q.QuoteKey=@quoteKey";

            var checkres = ExecuteIEnumerableObject<Opportunity>(ContextFactory.CrmConnectionString, @"select * from CRM.Opportunities Op
join  CRM.Quote Ord on ord.OpportunityId = Op.OpportunityId
where  Op.FranchiseId= @FranchiseId  and Ord.QuoteKey=@QuoteKey", new { Franchise.FranchiseId, QuoteKey = quoteKey }).FirstOrDefault();

            if (!(checkres.TerritoryType.ToUpper().Contains("GRAY AREA")))
                return ExecuteIEnumerableObject<TerritoryDisplay>(ContextFactory.CrmConnectionString, query, new { Franchise.FranchiseId, quoteKey }).FirstOrDefault();
            else return ExecuteIEnumerableObject<TerritoryDisplay>(ContextFactory.CrmConnectionString, query2, new { Franchise.FranchiseId, quoteKey }).FirstOrDefault();

            //var checkres = connection.Query<Opportunity>("select * from CRM.Opportunities where FranchiseId=@FranchiseId and OpportunityId = @OpportunityId", new { Franchise.FranchiseId, OpportunityId }).FirstOrDefault();

            //if (!(checkres.TerritoryType.ToUpper().Contains("GRAY AREA")))
        }

        //public Territory GetTerritoryDisplay(int quoteKey)
        //{
        //    string query = @" select t.* from CRM.Quote q
        //join CRM.Opportunities o on o.OpportunityId= q.OpportunityId
        //join CRM.Territories t on t.TerritoryId= o.TerritoryDisplayId
        //where q.QuoteKey=@QuoteKey";
        //    return ExecuteIEnumerableObject<Territory>(ContextFactory.CrmConnectionString, query, new { QuoteKey=quoteKey }).FirstOrDefault();
        //}

        public List<Quote> GetQuotes(int opportunityId)
        {
            var quotes = new List<Quote>();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                try
                {
                    var query = @"select * from crm.quote
                                    where OpportunityId = @opportunityid";
                    quotes = connection.Query<Quote>(query, new { opportunityid = opportunityId }).ToList();
                }
                finally
                {
                    connection.Close();
                }
            }

            return quotes;
        }
        public bool changeExpiryToDraft(int quoteKey)
        {
            var query = @"update CRM.Quote set ExpiryDate=null,QuoteStatusId=1 where QuoteKey=@QuoteKey";
            var result = ExecuteIEnumerableObject<int>(ContextFactory.CrmConnectionString, query, new
            {
                QuoteKey = quoteKey
            });
            return true;
        }

        public string GetJobCrewSize()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    if (SessionManager.CurrentFranchise != null)
                    {
                        int BrandID = SessionManager.CurrentFranchise.BrandId;
                        var query = @"Select Id,BrandId,HTML,CreatedOn,CreatedBy,LastUpdatedOn,LastUpdatedBy from CRM.JobCrewSize where BrandID=@BrandID";
                        var data = connection.Query<JobCrewSize>(query, new { BrandID }).FirstOrDefault();
                        if (data != null)
                            return data.HTML;
                        else
                            return "";
                    }
                    else
                        return "";
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
            }
        }
        #region ValidateQuote


        public bool ValidateProduct(int quoteKey)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var query1 = "SELECT  qld.* FROM crm.QuoteLineDetail qld INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = qld.QuoteLineId WHERE ql.QuoteKey=@QuoteKey";
                    var counterIdsList = connection.Query<QuoteLineDetail>(query1, new { QuoteKey = quoteKey }).ToList();
                    //var qldList =picManager.ReValidateByCounterIDs(counterIdsList);

                    var response = picManager.ReValidateByCounterIDs(counterIdsList);
                    dynamic responseObj = JsonConvert.DeserializeObject<ExpandoObject>(response);

                    if (Convert.ToBoolean(responseObj.valid) == true && responseObj.properties.Count > 0)
                    {
                        for (var i = 0; i < responseObj.properties.Count; i++)
                        {
                            var cntrId = (int)Convert.ToInt64(responseObj.properties[i].counter);
                            if (cntrId > 0)
                            {
                                var valid = (bool)responseObj.properties[i].valid;
                                counterIdsList.Where(x => x.CounterId == cntrId).FirstOrDefault().ValidConfiguration = valid;
                                counterIdsList.Where(x => x.CounterId == cntrId).FirstOrDefault().LastValidatedon = DateTime.UtcNow;
                                connection.BulkUpdate(counterIdsList);
                            }
                        }
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    //throw;
                    return false;
                }
            }
        }
        public string ValidateQuoteforCore(int quoteKey)
        {
            if (SessionManager.CurrentFranchise.BrandId == 1)
            {
                ValidateProduct(quoteKey);
            }

            var response = string.Empty;
            var validlines = string.Empty;
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var query = "CRM.SPValidatetQuoteLines";

                    var qld = connection.Query<dynamic>(query, new { quoteId = quoteKey, franchiseId = SessionManager.CurrentFranchise.FranchiseId }, commandType: CommandType.StoredProcedure).ToList();

                    foreach (var dyn in qld)
                    {
                        if (!string.IsNullOrEmpty(dyn.ValidConfiguration) && (bool)Convert.ToBoolean(dyn.ValidConfiguration))
                        {
                            if (!string.IsNullOrEmpty(validlines))
                            {
                                validlines = dyn.quotelinenumber;
                            }
                            else
                            {
                                validlines = validlines + ", " + dyn.quotelinenumber;
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                if (!string.IsNullOrEmpty(validlines))
                {
                    response = "<p> Quote lines " + validlines + " have invalid product configuration. Please re-validate quote lines..</p>";
                }


                return response;
            }
        }
        public string ValidateQuote(int quoteKey, bool isOrder)
        {
            if (SessionManager.CurrentFranchise.BrandId == 1)
            {
                ValidateProduct(quoteKey);
            }

            var returnValue = new List<string>();
            var response = string.Empty;
            var validlines = string.Empty;
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var query = "CRM.SPValidatetQuoteLines";

                    var qld = connection.Query<dynamic>(query, new { quoteId = quoteKey, franchiseId = SessionManager.CurrentFranchise.FranchiseId }, commandType: CommandType.StoredProcedure).ToList();

                    foreach (var dyn in qld)
                    {
                        var str = string.Empty;
                        if (!string.IsNullOrEmpty(dyn.vend))
                        {
                            str = dyn.quotelinenumber + ": " + dyn.vend;
                            returnValue.Add(str);
                        }


                        if (dyn.ValidConfiguration != null && !(bool)Convert.ToBoolean(dyn.ValidConfiguration))
                        {
                            if (string.IsNullOrEmpty(validlines))
                            {
                                validlines = Convert.ToString(dyn.quotelinenumber);
                            }
                            else
                            {
                                validlines = validlines + ", " + dyn.quotelinenumber;
                            }

                        }
                    }

                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                if (returnValue != null && returnValue.Count() > 0)
                {
                    response = "<ul>";
                    foreach (var item in returnValue)
                    {
                        response = response + "<li>Line No" + item + "</li>";
                    }
                    var text = "Quote";
                    if (isOrder)
                    {
                        text = "Order";
                    }
                    response = response + "</ul>" + "<p> Please update the " + text + " with active Vendor/Products.</p>";

                }
                if (!string.IsNullOrEmpty(validlines))
                {
                    response = response + "<p> Quote lines " + validlines + " have invalid product configuration. Please re-validate quote lines..</p>";
                }
                return response;
            }
        }
        /// <summary>
        /// Validate all Quote lines in a nightly job that were created or updated in last 30 days
        /// </summary>
        /// <returns></returns>
        public bool ValidateAllQuotes()
        {

            var returnValue = new List<int>();
            var response = string.Empty;
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var query = "CRM.GetQuotesForBatchJob";

                    var qld = connection.Query<int>(query, commandType: CommandType.StoredProcedure).ToList();

                    if (qld != null && qld.Count > 0)
                    {
                        foreach (var item in qld)
                        {
                            ValidateProduct(item);
                        }

                    }

                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }

                return true;
            }
        }
        #endregion

        #region Group Discount
        public List<QuoteGroupDiscount> GetQuoteGroupDiscount(int QuoteKey)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var sqlQuery = "Select * from CRM.QuoteGroupDiscount where QuoteKey=@QuoteKey";
                    connection.Open();

                    var qDetails = connection.Query<QuoteGroupDiscount>(sqlQuery, new { QuoteKey }).ToList();

                    return qDetails;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public List<QuoteGroupDiscount> SaveGroupDiscount(int QuoteKey, List<QuoteGroupDiscount> lstGroupDiscount)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    foreach (QuoteGroupDiscount QGD in lstGroupDiscount)
                    {
                        if (QGD.QuoteGroupDiscountId == 0)
                        {
                            QGD.CreatedBy = SessionManager.CurrentUser.PersonId;
                            QGD.CreatedOn = DateTime.UtcNow;
                            connection.Insert<QuoteGroupDiscount>(QGD);
                        }
                        else
                        {
                            QGD.LastUpdatedBy = SessionManager.CurrentUser.PersonId;
                            QGD.LastUpdatedOn = DateTime.UtcNow;
                            connection.Update<QuoteGroupDiscount>(QGD);
                        }
                    }
                    return GetQuoteGroupDiscount(QuoteKey);
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool ApplyGroupDiscount(int QuoteKey)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                //TODO: This should be part of a transaction. as it involves more than one transaction.
                try
                {
                    var lstGroupDiscount = GetQuoteGroupDiscount(QuoteKey);

                    var QuoteLines = GetQuoteLine(QuoteKey);

                    //updating product category id and product category for my product and service
                    string sQueryProductCategory = @"select QL.QuoteLineId,PC.ProductCategoryId,PC.ProductCategory from crm.FranchiseProducts FP
                                    inner join crm.Type_ProductCategory PC on FP.ProductCategory = PC.ProductCategoryId
                                    inner join crm.QuoteLines QL on QL.ProductId = FP.ProductKey
                                    where FP.ProductKey in(select ProductId from crm.QuoteLines where ProductTypeId in (2,3) and QL.QuoteKey = @QuoteKey)";

                    var resultProductCategory = connection.Query<dynamic>(sQueryProductCategory, new { QuoteKey }).ToList();

                    bool requiredRecalculate = false;

                    foreach (QuoteLinesVM QL in QuoteLines)
                    {
                        if ((QL.ProductTypeId == 1 || QL.ProductTypeId == 2))
                        {
                            if (QL.ProductTypeId == 1 && !string.IsNullOrEmpty(QL.PICJson))
                            {
                                var picInput = JsonConvert.DeserializeObject<dynamic>(QL.PICJson);
                                if (!string.IsNullOrEmpty(picInput.GGroup.ToString()))
                                {
                                    QL.PSProductCategoryId = Int32.Parse(picInput.GGroup.ToString());
                                    QL.PSProductCategory = picInput.PCategory.ToString();
                                }
                            }
                            else if (QL.ProductTypeId == 2)
                            {
                                var ProductWithCategory = resultProductCategory.Where(x => x.QuoteLineId == QL.QuoteLineId).FirstOrDefault();
                                if (ProductWithCategory != null)
                                {
                                    QL.PSProductCategoryId = ProductWithCategory.ProductCategoryId;
                                    QL.PSProductCategory = ProductWithCategory.ProductCategory;
                                }
                            }

                            var GroupDiscount = lstGroupDiscount.Where(x => x.ProductGroupId == QL.PSProductCategoryId && x.ProductTypeId == QL.ProductTypeId).FirstOrDefault();
                            if (GroupDiscount != null)
                            {
                                if (GroupDiscount.Discount == null || GroupDiscount.Discount == 0)
                                {
                                    if (QL.Discount != GroupDiscount.Discount && QL.IsGroupDiscountApplied == true)
                                    {
                                        QL.Discount = null;
                                        QL.DiscountType = "";
                                        QL.IsGroupDiscountApplied = false;
                                        updatequoteinlineEdit(QL, true);
                                        UpdatePricingMgmtDiscount(QL.PSProductCategoryId, (int)QL.VendorId, QL.QuoteLineId);
                                        //RecalculateQuoteLines(QL.QuoteKey, QL.QuoteLineId);
                                        requiredRecalculate = true;
                                    }
                                }
                                else
                                {
                                    if (QL.Discount != GroupDiscount.Discount)
                                    {
                                        QL.Discount = GroupDiscount.Discount;
                                        QL.DiscountType = "%";
                                        QL.IsGroupDiscountApplied = true;
                                        updatequoteinlineEdit(QL, true);
                                        //RecalculateQuoteLines(QL.QuoteKey, QL.QuoteLineId);
                                        requiredRecalculate = true;
                                    }
                                    else
                                    {
                                        QL.IsGroupDiscountApplied = true;
                                        updatequoteinlineEdit(QL);
                                    }
                                }
                            }
                            else if (QL.IsGroupDiscountApplied)
                            {
                                QL.Discount = null;
                                QL.DiscountType = "";
                                QL.IsGroupDiscountApplied = false;
                                updatequoteinlineEdit(QL, true);
                                UpdatePricingMgmtDiscount(QL.PSProductCategoryId, (int)QL.VendorId, QL.QuoteLineId);
                                //RecalculateQuoteLines(QL.QuoteKey, QL.QuoteLineId);
                                requiredRecalculate = true;
                            }
                        }
                    }

                    if (requiredRecalculate)
                        RecalculateQuoteLines(QuoteKey);
                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool CopyGroupDiscount(int FromQuoteKey, int ToQuoteKey)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var sQuery = @"delete from CRM.QuoteGroupDiscount where QuoteKey=@ToQuoteKey;
                                        insert into CRM.QuoteGroupDiscount(QuoteKey,ProductTypeId,ProductGroupId,ProductGroupDesc,Discount,CreatedOn,CreatedBy,LastUpdatedOn,LastUpdatedBy)
                                        select @ToQuoteKey,ProductTypeId,ProductGroupId,ProductGroupDesc,Discount,CreatedOn,CreatedBy,LastUpdatedOn,LastUpdatedBy 
                                        from CRM.QuoteGroupDiscount where QuoteKey=@FromQuoteKey";

                    var result = connection.Query(sQuery, new { FromQuoteKey, ToQuoteKey });
                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool RefreshGroupDiscountTable(int QuoteKey)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var lstGroupDiscount = GetQuoteGroupDiscount(QuoteKey);

                    var QuoteLines = GetQuoteLine(QuoteKey);

                    //updating product category id and product category for my product and service
                    string sQueryProductCategory = @"select QL.QuoteLineId,PC.ProductCategoryId,PC.ProductCategory from crm.FranchiseProducts FP
                                    inner join crm.Type_ProductCategory PC on FP.ProductCategory = PC.ProductCategoryId
                                    inner join crm.QuoteLines QL on QL.ProductId = FP.ProductKey
                                    where FP.ProductKey in(select ProductId from crm.QuoteLines where ProductTypeId in (2,3) and QL.QuoteKey = @QuoteKey)";

                    var resultProductCategory = connection.Query<dynamic>(sQueryProductCategory, new { QuoteKey }).ToList();

                    //Below loop is to update PSProductCategoryId && PSProductCategory
                    foreach (QuoteLinesVM QL in QuoteLines)
                    {
                        if ((QL.ProductTypeId == 1 || QL.ProductTypeId == 2))
                        {
                            if (QL.ProductTypeId == 1 && !string.IsNullOrEmpty(QL.PICJson))
                            {
                                var picInput = JsonConvert.DeserializeObject<dynamic>(QL.PICJson);
                                if (!string.IsNullOrEmpty(picInput.GGroup.ToString()))
                                {
                                    QL.PSProductCategoryId = Int32.Parse(picInput.GGroup.ToString());
                                    QL.PSProductCategory = picInput.PCategory.ToString();
                                }
                            }
                            else if (QL.ProductTypeId == 2)
                            {
                                var ProductWithCategory = resultProductCategory.Where(x => x.QuoteLineId == QL.QuoteLineId).FirstOrDefault();
                                if (ProductWithCategory != null)
                                {
                                    QL.PSProductCategoryId = ProductWithCategory.ProductCategoryId;
                                    QL.PSProductCategory = ProductWithCategory.ProductCategory;
                                }
                            }
                        }
                    }

                    string DeleteIds = "";
                    foreach (QuoteGroupDiscount GD in lstGroupDiscount)
                    {
                        var count = QuoteLines.Where(x => x.PSProductCategoryId == GD.ProductGroupId).Count();
                        if (count == 0)
                        {
                            DeleteIds += (DeleteIds == "" ? GD.QuoteGroupDiscountId.ToString() : "," + GD.QuoteGroupDiscountId);
                        }
                    }

                    if (DeleteIds != "")
                    {
                        var sQueryDelete = "delete from CRM.QuoteGroupDiscount where QuoteGroupDiscountId in @QuoteGroupDiscountIds";
                        int[] ia = DeleteIds.Split(',').Select(n => Convert.ToInt32(n)).ToArray();
                        var deleteResult = connection.Query(sQueryDelete, new { QuoteGroupDiscountIds = ia });
                    }

                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public decimal GetAppliedDiscountAmount(int QuoteLineId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {


                    string sQuery = @"Select* from crm.QuoteLines where QuoteLineId = @QuoteLineId";
                    var QuoteLine = connection.Query<QuoteLinesVM>(sQuery, new { QuoteLineId }).FirstOrDefault();

                    //updating product category id and product category for my product and service
                    string sQueryProductCategory = @"select QL.QuoteLineId,PC.ProductCategoryId,PC.ProductCategory from crm.FranchiseProducts FP
                                    inner join crm.Type_ProductCategory PC on FP.ProductCategory = PC.ProductCategoryId
                                    inner join crm.QuoteLines QL on QL.ProductId = FP.ProductKey
                                    where FP.ProductKey in(select ProductId from crm.QuoteLines where ProductTypeId in (2,3) and QL.QuoteKey = @QuoteKey)";

                    var resultProductCategory = connection.Query<dynamic>(sQueryProductCategory, new { QuoteLine.QuoteKey }).ToList();

                    if (QuoteLine != null)
                    {
                        var lstGroupDiscount = GetQuoteGroupDiscount(QuoteLine.QuoteKey);
                        if ((QuoteLine.ProductTypeId == 1 || QuoteLine.ProductTypeId == 2))
                        {
                            if (QuoteLine.ProductTypeId == 1 && !string.IsNullOrEmpty(QuoteLine.PICJson))
                            {
                                var picInput = JsonConvert.DeserializeObject<dynamic>(QuoteLine.PICJson);
                                if (!string.IsNullOrEmpty(picInput.GGroup.ToString()))
                                {
                                    QuoteLine.PSProductCategoryId = Int32.Parse(picInput.GGroup.ToString());
                                    QuoteLine.PSProductCategory = picInput.PCategory.ToString();
                                }
                            }
                            else if (QuoteLine.ProductTypeId == 2)
                            {
                                var ProductWithCategory = resultProductCategory.Where(x => x.QuoteLineId == QuoteLine.QuoteLineId).FirstOrDefault();
                                if (ProductWithCategory != null)
                                {
                                    QuoteLine.PSProductCategoryId = ProductWithCategory.ProductCategoryId;
                                    QuoteLine.PSProductCategory = ProductWithCategory.ProductCategory;
                                }
                            }
                        }

                        var GroupDiscount = lstGroupDiscount.Where(x => x.ProductGroupId == QuoteLine.PSProductCategoryId && x.ProductTypeId == QuoteLine.ProductTypeId).FirstOrDefault();
                        if (GroupDiscount != null)
                        {
                            if (GroupDiscount.Discount != null && GroupDiscount.Discount != 0)
                            {
                                return (decimal)GroupDiscount.Discount;
                            }
                        }
                    }
                    return 0;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool UpdateQuoteLineGroupDiscount(int QuoteKey, QuoteLinesVM oldQuoteLine)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var lstGroupDiscount = GetQuoteGroupDiscount(QuoteKey);

                    var newQuoteLine = GetQuoteLine(QuoteKey).Where(x => x.QuoteLineId == oldQuoteLine.QuoteLineId).FirstOrDefault();

                    if (newQuoteLine != null)
                    {
                        //updating product category id and product category for my product and service
                        string sQueryProductCategory = @"select QL.QuoteLineId,PC.ProductCategoryId,PC.ProductCategory from crm.FranchiseProducts FP
                                    inner join crm.Type_ProductCategory PC on FP.ProductCategory = PC.ProductCategoryId
                                    inner join crm.QuoteLines QL on QL.ProductId = FP.ProductKey
                                    where FP.ProductKey in(select ProductId from crm.QuoteLines where ProductTypeId in (2,3) and QL.QuoteKey = @QuoteKey)";

                        var resultProductCategory = connection.Query<dynamic>(sQueryProductCategory, new { QuoteKey }).ToList();

                        if ((newQuoteLine.ProductTypeId == 1 || newQuoteLine.ProductTypeId == 2))
                        {
                            if (newQuoteLine.ProductTypeId == 1 && !string.IsNullOrEmpty(newQuoteLine.PICJson))
                            {
                                var picInput = JsonConvert.DeserializeObject<dynamic>(newQuoteLine.PICJson);
                                if (!string.IsNullOrEmpty(picInput.GGroup.ToString()))
                                {
                                    newQuoteLine.PSProductCategoryId = Int32.Parse(picInput.GGroup.ToString());
                                    newQuoteLine.PSProductCategory = picInput.PCategory.ToString();
                                }
                            }
                            else if (newQuoteLine.ProductTypeId == 2)
                            {
                                var ProductWithCategory = resultProductCategory.Where(x => x.QuoteLineId == newQuoteLine.QuoteLineId).FirstOrDefault();
                                if (ProductWithCategory != null)
                                {
                                    newQuoteLine.PSProductCategoryId = ProductWithCategory.ProductCategoryId;
                                    newQuoteLine.PSProductCategory = ProductWithCategory.ProductCategory;
                                }
                            }
                        }

                        var GroupDiscount = lstGroupDiscount.Where(x => x.ProductGroupId == oldQuoteLine.PSProductCategoryId && x.ProductTypeId == oldQuoteLine.ProductTypeId).FirstOrDefault();
                        if (GroupDiscount != null)
                        {
                            if (newQuoteLine.ProductTypeId != oldQuoteLine.ProductTypeId || oldQuoteLine.PSProductCategoryId != newQuoteLine.PSProductCategoryId)
                            {
                                newQuoteLine.Discount = null;
                                newQuoteLine.DiscountType = "";
                                newQuoteLine.IsGroupDiscountApplied = false;
                                updatequoteinlineEdit(newQuoteLine);
                                UpdatePricingMgmtDiscount(newQuoteLine.PSProductCategoryId, (int)newQuoteLine.VendorId, newQuoteLine.QuoteLineId);
                                RecalculateQuoteLines(newQuoteLine.QuoteKey, newQuoteLine.QuoteLineId);
                            }
                        }

                        GroupDiscount = lstGroupDiscount.Where(x => x.ProductGroupId == newQuoteLine.PSProductCategoryId && x.ProductTypeId == newQuoteLine.ProductTypeId).FirstOrDefault();
                        if (GroupDiscount != null)
                        {
                            if (GroupDiscount.Discount == null || GroupDiscount.Discount == 0)
                            {
                                newQuoteLine.Discount = null;
                                newQuoteLine.DiscountType = "";
                                newQuoteLine.IsGroupDiscountApplied = false;
                                updatequoteinlineEdit(newQuoteLine);
                                UpdatePricingMgmtDiscount(newQuoteLine.PSProductCategoryId, (int)newQuoteLine.VendorId, newQuoteLine.QuoteLineId);
                                RecalculateQuoteLines(newQuoteLine.QuoteKey, newQuoteLine.QuoteLineId);
                            }
                        }
                    }

                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public QuoteLinesVM getQuoteLineWithGroupDiscount(int QuoteLineId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    string sQuery = @"Select* from crm.QuoteLines where QuoteLineId = @QuoteLineId";
                    var QuoteLine = connection.Query<QuoteLinesVM>(sQuery, new { QuoteLineId }).FirstOrDefault();

                    //updating product category id and product category for my product and service
                    string sQueryProductCategory = @"select QL.QuoteLineId,PC.ProductCategoryId,PC.ProductCategory from crm.FranchiseProducts FP
                                    inner join crm.Type_ProductCategory PC on FP.ProductCategory = PC.ProductCategoryId
                                    inner join crm.QuoteLines QL on QL.ProductId = FP.ProductKey
                                    where FP.ProductKey in(select ProductId from crm.QuoteLines where ProductTypeId in (2,3) and QL.QuoteKey = @QuoteKey)";

                    var resultProductCategory = connection.Query<dynamic>(sQueryProductCategory, new { QuoteLine.QuoteKey }).ToList();

                    if (QuoteLine != null)
                    {
                        if ((QuoteLine.ProductTypeId == 1 || QuoteLine.ProductTypeId == 2))
                        {
                            if (QuoteLine.ProductTypeId == 1 && !string.IsNullOrEmpty(QuoteLine.PICJson))
                            {
                                var picInput = JsonConvert.DeserializeObject<dynamic>(QuoteLine.PICJson);
                                if (!string.IsNullOrEmpty(picInput.GGroup.ToString()))
                                {
                                    QuoteLine.PSProductCategoryId = Int32.Parse(picInput.GGroup.ToString());
                                    QuoteLine.PSProductCategory = picInput.PCategory.ToString();
                                }
                            }
                            else if (QuoteLine.ProductTypeId == 2)
                            {
                                var ProductWithCategory = resultProductCategory.Where(x => x.QuoteLineId == QuoteLine.QuoteLineId).FirstOrDefault();
                                if (ProductWithCategory != null)
                                {
                                    QuoteLine.PSProductCategoryId = ProductWithCategory.ProductCategoryId;
                                    QuoteLine.PSProductCategory = ProductWithCategory.ProductCategory;
                                }
                            }
                        }
                    }

                    return QuoteLine;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        //for getting sidemark and opportunity number using quotekey
        public Quote GetSidemarkOppId(int QuoteKey)
        {
            using (SqlConnection conn = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                conn.Open();
                string cmdStr = "select q.SideMark, opp.OpportunityNumber as OpportunityId from crm.Quote q left join crm.Opportunities opp on opp.OpportunityId = q.OpportunityId where QuoteKey = @QuoteKey;";
                var result = conn.Query<Quote>(cmdStr, new { QuoteKey = QuoteKey }).FirstOrDefault();
                conn.Close();
                return result;
            }
        }
        //getting quote detail using quotekey for pdf naming
        public string GetQuotebyQuoteKey(int QuoteKey)
        {
            string fileDownloadName = string.Empty;
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var query = @"select q.QuoteName, q.SideMark, opp.OpportunityNumber as OpportunityId from CRM.Quote q
                                  left join crm.Opportunities opp on opp.OpportunityId = q.OpportunityId
                                  where QuoteKey = @QuoteKey";
                    var data = connection.Query<Quote>(query, new { QuoteKey = QuoteKey }).FirstOrDefault();
                    fileDownloadName = string.Format("OPP {0} - {1} - {2}.pdf", data.OpportunityId, data.QuoteName.Replace("/", "-"), data.SideMark.Replace("/", "-"));
                    return fileDownloadName;
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    return fileDownloadName;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        #endregion



        #region Description Datafix
        public bool UpdateQuoteLineDescription()
        {

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var sqlQuery = @"SELECT ql.* FROM crm.Quote q INNER JOIN CRM.QuoteLines ql ON q.QuoteKey = ql.QuoteKey AND ql.ProductTypeId=1 WHERE ql.LastUpdatedOn>DATEADD(Month, -1, getutcdate())";


                    var quotelines = connection.Query<QuoteLines>(sqlQuery).ToList();
                    foreach (var qLinesObj in quotelines)
                    {
                        try
                        {
                            if (qLinesObj.ProductTypeId == 1)
                            {
                                var qcpdList = connection.Query<QuoteCoreProductDetails>("select* from crm.QuoteCoreProductDetails where QuotelineId = @QuotelineId; ", new { QuotelineId = qLinesObj.QuoteLineId }).ToList();
                                dynamic input = JsonConvert.DeserializeObject<ExpandoObject>(qLinesObj.PICJson);
                                string desc = string.Empty;
                                if (!string.IsNullOrEmpty(input.ProductName))
                                {
                                    desc = "Product: <b>" + input.ProductName + "</b>; ";
                                }
                                if (!string.IsNullOrEmpty(qLinesObj.ModelDescription) && !string.Equals(qLinesObj.ModelDescription, "ALL", StringComparison.OrdinalIgnoreCase))
                                {
                                    desc = desc + "Model: <b>" + qLinesObj.ModelDescription + "</b>; ";
                                }
                                desc = desc + getCoreProductDesciption(qcpdList);

                                if (desc == string.Empty)
                                {
                                    qLinesObj.Description = input.Description;
                                }
                                else
                                {
                                    qLinesObj.Description = desc;
                                }
                                connection.Update(qLinesObj);
                            }
                        }
                        catch (Exception e)
                        {
                            EventLogger.LogEvent(e);
                        }

                    }

                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    //throw;
                }
                finally
                {
                    connection.Close();
                }
            }


            return true;
        }
        #endregion
    }
}