﻿using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Core.Membership;
using HFC.CRM.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using HFC.CRM.DTO.Source;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Data.Entity.Infrastructure;
using HFC.CRM.Data.Context;
using System.Data.Entity.Core.EntityClient;
using System.Dynamic;
using Newtonsoft.Json;
using HFC.CRM.Managers.Exceptions;
using System.Web.Http;
using System.Net;
using System.Net.Http;
using HFC.CRM.DTO.PIC;
using HFC.CRM.DTOs;
using System.Data;
using HFC.CRM.DTO.Lookup;
using System.Globalization;

namespace HFC.CRM.Managers
{
    /// <summary>
    /// Class FranchiseManager.
    /// </summary>
    public class FranchiseManager : DataManager<Franchise>
    {
        private EmailManager emailMgr;

        //TODO: Temp only to be removed in production.
        public FranchiseManager()
        {
        }

        /// <summary>
        /// Create a new instance of calendar manager to handle CRUD operations
        /// </summary>
        /// <param name="user">The user that is performing the CRUD operation and used to check authorization permission</param>
        public FranchiseManager(User user)
            : this(user, user, null)
        {
            //TODO: This is a temp fix
            //DeleteAllFranchise();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FranchiseManager"/> class.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="franchise">The franchise.</param>
        public FranchiseManager(User user, Franchise franchise)
            : this(user, user, franchise)
        {
            this.User = user;
            this.AuthorizingUser = user;
            this.Franchise = franchise;
            emailMgr = new EmailManager(user, franchise);
        }

        /// <summary>
        /// Create a new instance of calendar manager to handle CRUD operations
        /// </summary>
        /// <param name="authorizingUser">Used to check authorization permission</param>
        /// <param name="user">The user that is performing the CRUD operation</param>
        /// <param name="franchise">Franchise to assign calendar</param>
        public FranchiseManager(User authorizingUser, User user, Franchise franchise)
        {
            this.User = user;
            this.AuthorizingUser = authorizingUser;
            this.Franchise = franchise;
            emailMgr = new EmailManager(user, franchise);
        }

        /// <summary>
        /// Adds the lead status.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <returns>System.String.</returns>
        public string AddOpportunityStatus(Type_OpportunityStatus status)
        {
            if (status == null)
                return DataCannotBeNullOrEmpty;
            if (string.IsNullOrEmpty(status.Name))
                return "Action cancelled, status must have a name";

            // TODO: is this required in TP???
            //if (!Account_BasePermission.CanCreate)
            //    return Unauthorized;

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var original =
                        db.Type_LeadStatus.SingleOrDefault(
                            f =>
                                (f.FranchiseId == status.FranchiseId || !f.FranchiseId.HasValue) &&
                                f.ParentId == status.OpportunityStatusId && f.Name == status.Name);
                    if (original != null)
                        return "Action cancelled, duplicate status found.";

                    status.Createdon = DateTime.Now;
                    db.Type_OpportunityStatus.Add(status);
                    db.SaveChanges();

                    //clear cache so it can be reloaded
                    CacheManager.OpportunityStatusTypeCollection = null;

                    return Success;
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }
        }

        public string UpdateOpportunityStatus(Type_OpportunityStatus status)
        {
            return "";
        }

        public string DeleteOpportunityStatus(int franchiseId, int statusId)
        {
            return "";
        }

        /// <summary>
        /// Creates a new franchise
        /// </summary>
        /// <param name="model">First Property level info will be saved</param>
        private User helperCreateOwnerAdminUser(User model)
        {
            var user = new User()
            {
                UserId = Guid.NewGuid(),
                UserName = model.UserName,//model.Person.PrimaryEmail,
                ColorId = 1, // This causes problems, we need to generate 300 more colors just in case?
                Email = model.Person.PrimaryEmail,
                FranchiseId = model.FranchiseId,
                IsApproved = true,
                IsDeleted = false,
                IsLockedOut = false,
                CalendarFirstHour = 0,
                FailedPasswordAttemptCount = 0,
                CreationDateUtc = DateTime.Now,
                Password = @"password",
                SyncStartsOnUtc = DateTime.Now,
                Comment = "Comment",
                Roles = new List<Role>() {
                    AppConfigManager.DefaultOwnerRole,
                    AppConfigManager.FranchiseAdminRole
                },
                Person = new Person
                {
                    FirstName = model.Person.FirstName,
                    LastName = model.Person.LastName,
                    PreferredTFN = "H",
                    PrimaryEmail = model.Person.PrimaryEmail,
                    SecondaryEmail = model.Person.SecondaryEmail,
                    HomePhone = model.Person.HomePhone,
                    CellPhone = model.Person.CellPhone,
                    FaxPhone = model.Person.FaxPhone,
                    WorkPhone = model.Person.WorkPhone,
                    IsDeleted = false,
                    CreatedOnUtc = DateTime.Now
                }
            };

            //TODO : do we really need this code???
            //foreach (var role in user.Roles)
            //{
            //    db.Roles.Attach(role);
            //}
            return user;
        }

        public string Add(out int franchiseId, Franchise model, CRMContextEx db)
        {
            franchiseId = 0;
            if (model.Address != null && model.FranchiseOwners != null)
            {
                // TODO: check the validation.
                model = ValidateFranchise(model);
                var userGuids = model.FranchiseOwners.Select(x => x.User.UserId).ToArray();

                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var transaction = connection.BeginTransaction();
                    try
                    {
                        // Insert Address into CRM.Addresses table
                        var addressId = connection.Insert(model.Address, transaction);
                        model.QuoteExpiryDays = 30;

                        model.AddressId = addressId;

                        // model.BrandId = SessionManager.BrandIdOld; //SessionManager.BrandId;
                        model.LastUpdatedUtc = DateTime.Now;
                        model.Currency = model.CountryCode;

                        // Insert model data into CRM.Franchise table
                        franchiseId = (int)connection.Insert(model, transaction);

                        var user = model.Users.FirstOrDefault();
                        //Getting First name and last name based on Vendor name
                        var vendorName = model.Name.Split(' ');
                        string firstName = vendorName[0];
                        string lastName = "";
                        if (vendorName.Length > 1)
                        {
                            lastName = vendorName[1];
                        }
                        // Insert Person info into CRM.Person table
                        var PersonId = connection.Insert(new Person
                        {
                            FirstName = firstName,
                            LastName = lastName,
                            PrimaryEmail = model.OwnerEmail, //User.Email,
                            CreatedOnUtc = model.CreatedOnUtc,
                            PreferredTFN = User.Person.PreferredTFN,
                            FranchiseID = franchiseId,
                        }, transaction);
                        user.AddressId = model.AddressId;
                        user.PersonId = (int)PersonId;
                        user.FranchiseId = franchiseId;

                        //});
                        user.AddressId = model.AddressId;
                        user.PersonId = (int)PersonId;
                        user.FranchiseId = franchiseId;

                        // Insert Authendication info into Auth.Users table
                        connection.Execute(@"insert auth.users(UserId, UserName, PersonId,AddressId, CreationDateUtc
                        ,Password,IsDeleted,IsApproved,IsLockedOut,FailedPasswordAttemptCount,FranchiseId
                        ,Email,ColorId,SyncStartsOnUtc,CalendarFirstHour)
                        values(@UserId, @UserName, @PersonId,@AddressId,@CreationDateUtc,@Password,@IsDeleted
                        ,@IsApproved,@IsLockedOut,@FailedPasswordAttemptCount, @FranchiseId,@Email,@ColorId
                        ,@SyncStartsOnUtc,@CalendarFirstHour)", user, transaction);

                        // TODO: we need to add rolles also.
                        foreach (var role in user.Roles)
                        {
                            connection.Execute(@"insert into Auth.UsersInRoles values(@userId, @roleId)"
                                , new { userId = user.UserId, roleId = role.RoleId }, transaction);
                        }

                        var franchiseOwner = new FranchiseOwner
                        {
                            UserId = new Guid(userGuids[0].ToString()),
                            FranchiseId = (int)franchiseId,
                            OwnershipPercent = model.FranchiseOwners.Select(x => x.OwnershipPercent).FirstOrDefault(),
                            CreatedOnUtc = model.FranchiseOwners.Select(x => x.CreatedOnUtc).FirstOrDefault()
                        };

                        // After creating franchise and user Insert UserId, FranchiseId into CRM.FranchiseOwners table
                        connection.Execute(@"insert CRM.FranchiseOwners(UserId, FranchiseId, OwnershipPercent,CreatedOnUtc)
                        values(@UserId, @FranchiseId,@OwnershipPercent,@CreatedOnUtc)", franchiseOwner, transaction);

                        var templateColors = connection.GetList<Type_NamedColor>("where FranchiseId is null", null, transaction);
                        foreach (var color in templateColors)
                        {
                            color.FranchiseId = model.FranchiseId;
                            connection.Update(color);
                        }

                        transaction.Commit();
                        connection.Close();

                        PICManager pic = new PICManager();
                        pic.CreateOrUpdatePICCustomer(CreatePICCustomerObject(model));
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        EventLogger.LogEvent(ex);
                        return ex.Message.ToString();
                    }
                }
            }
            else
            {
                return DataCannotBeNullOrEmpty;
            }

            return Success;
        }

        private PICCustomer CreatePICCustomerObject(Franchise model)
        {
            var cus = new PICCustomer();
            string ownerName = "";

            try
            {
                if (!string.IsNullOrEmpty(model.OwnerName))
                {
                    dynamic owners = JsonConvert.DeserializeObject(model.OwnerName);
                    if (owners[0].FirstName != null)
                        ownerName += Convert.ToString(owners[0].FirstName);// : "" + ", " + owners[0].LastName != null ? Convert.ToString(owners[0].LastName) : "";

                    if (owners[0].LastName != null)
                        ownerName += ", " + Convert.ToString(owners[0].LastName);
                }
            }
            catch (Exception)
            {
            }

            cus.Customer = model.Code;
            cus.Name = string.IsNullOrEmpty(model.DBAName) ? model.Name : model.DBAName;
            cus.FirstName = model.Name;
            cus.AccountType = model.Address.CountryCode2Digits.ToUpper() == "CA" ? "CAN" : "USA";
            cus.Contact = ownerName;
            cus.Address1 = model.Address.Address1;
            cus.Address2 = model.Address.Address2;
            cus.City = model.Address.City;
            cus.State = model.Address.State;
            cus.Country = model.Address.CountryCode2Digits;
            cus.Zip = model.Address.ZipCode;
            cus.Phone = model.LocalPhoneNumber;
            cus.Fax = model.FaxNumber;
            cus.Email = model.OwnerEmail;
            cus.BillName = model.Name;
            cus.InvAttn = model.Name;
            cus.BillAddress1 = model.Address.Address1;
            cus.BillAddress2 = model.Address.Address2;
            cus.BillCity = model.Address.City;
            cus.BillState = model.Address.State;
            cus.BillCountry = model.Address.CountryCode2Digits;
            cus.BillZip = model.Address.ZipCode;
            cus.BillPhone = model.BusinessPhone;
            cus.DateOpened = model.CreatedOnUtc.ToString();
            cus.ShipEmail = model.OwnerEmail;
            cus.OrderEmail = model.OwnerEmail;
            cus.InternalXMLID = model.Code;
            cus.Site = "1";
            cus.AddressType = "C";
            cus.Source = "";
            cus.DeActivate = model.IsSuspended != null && model.IsSuspended == true ? "Y" : "N";
            cus.Salesperson1 = 1;
            cus.Territory = "ALL";

            return cus;
        }

        private void ValidateAddress(Address model)
        {
            if (string.IsNullOrEmpty(model.ZipCode))
                throw new Exception("Zip Code required");

            model.CreatedOnUtc = DateTime.Now;
            model.IsDeleted = false;
            model.IsResidential = false;
        }

        private Franchise ValidateFranchise(Franchise model)
        {
            var franchiseOwners = model.FranchiseOwners.Select(s => s).ToList();
            ValidateAddress(model.Address);
            var brand = string.Empty;
            //switch (AppConfigManager.HostDomain)
            //{
            //    case "tailoredliving":
            //        brand = "tl";
            //        break;
            //    case "budgetblinds":
            //        brand = "bb";
            //        break;
            //    case "concretecraft":
            //        brand = "cc";
            //        break;
            //    default:
            //        break;
            //}
            //switch (SessionManager.BrandId)
            switch (model.BrandId)
            {
                case 1:
                    //brand = "bb";
                    brand = "budgetblinds";
                    break;

                case 2:
                    brand = "tailoredliving";
                    break;

                case 3:
                    brand = "concretecraft";
                    break;
            }

            //var userName = string.Format("{0}@{1}.com", model.Code, brand);
            var userName = model.Code;

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                var franchiseCode = connection.GetList<Franchise>("where Code = @code", new { code = model.Code }).FirstOrDefault();
                if (franchiseCode != null)
                    throw new ValidationException("Account Code already exists");

                //if (db.Users.FirstOrDefault(v => v.UserName == userName) != null)
                //    throw new Exception("Account Code already exists");

                //

                connection.Close();
            }

            foreach (var o in franchiseOwners)
            {
                o.User.Person.PrimaryEmail = string.Format("{0}@{1}.com", model.Code, brand);
                o.User.UserName = model.Code;// userName;
            }

            franchiseOwners.ForEach(e =>
            {
                e.User = LocalMembership.CreateUser(helperCreateOwnerAdminUser(e.User));
                e.User.Address = model.Address;
                e.OwnershipPercent = 100.00m / model.FranchiseOwners.Count();
            });

            model.FranchiseOwners = franchiseOwners;
            model.CreatedOnUtc = DateTime.Now;
            model.IsDeleted = false;
            model.FranchiseGuid = Guid.NewGuid();

            model.Users.Add(model.FranchiseOwners.FirstOrDefault().User);

            return model;
        }

        private void AddFranchiseOwner(FranchiseOwner model, CRMContextEx db)
        {
            try
            {
                if (model != null && model.User != null)
                {
                    var user = LocalMembership.CreateUser(helperCreateOwnerAdminUser(model.User));
                    model.User = user;
                    model.UserId = user.UserId;
                    db.Entry(model).State = EntityState.Added;

                    db.SaveChanges();
                }
                CacheManager.UserCollection = null; //clear so it can refresh
            }
            catch (Exception ex)
            {
                throw new ValidationException("username already is in use");
            }
        }

        public string UpdateAddress(Address model)
        {
            using (var db = ContextFactory.Create())
            {
                var entry = db.Addresses.Find(model.AddressId);
                if (entry != null)
                {
                    db.Entry(entry).CurrentValues.SetValues(model);
                    db.SaveChanges();
                }
                else
                {
                    db.Addresses.Add(model);
                    db.SaveChanges();
                }
                return Success;
            }
        }

        public string AssignTerritory(Territory model, bool isInUsed, out int territoryId)
        {
            territoryId = 0;
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    if (model.TerritoryId > 0)
                    {
                        Territory terrexist = connection.GetList<Territory>("where BrandId=@BrandId and Minion=@Minion ", new { model.BrandId, model.Minion }).FirstOrDefault();
                        if (terrexist != null)
                        {
                            if (terrexist.FranchiseId != model.FranchiseId)
                                return "Territory Code already used some other Franchise!";

                            if (terrexist.FranchiseId == model.FranchiseId && terrexist.TerritoryId != model.TerritoryId)
                                return "Minion Id already in current Franchise!";
                        }

                        Territory ter = connection.Get<Territory>(model.TerritoryId);
                        ter.Code = model.Code.Trim();
                        ter.Name = model.Name.Trim();
                        ter.isArrears = model.isArrears;
                        ter.Minion = model.Minion.Trim();
                        ter.WebSiteURL = model.WebSiteURL.Trim();
                        connection.Update(ter);
                    }
                    else
                    {
                        Territory terrexist = connection.GetList<Territory>("where BrandId=@BrandId and Minion=@Minion ", new { model.BrandId, model.Minion }).FirstOrDefault();
                        if (terrexist != null)
                            return "Territory Code already assigned to another Franchise!";
                        else
                        {
                            Territory terr = new Territory
                            {
                                FranchiseId = model.FranchiseId,
                                Code = model.Code.Trim(),
                                Name = model.Name.Trim(),
                                isArrears = model.isArrears,
                                Minion = model.Minion.Trim(),
                                BrandId = model.BrandId,
                                WebSiteURL = model.WebSiteURL.Trim(),
                                CreatedOnUtc = DateTime.Now
                            };
                            var terrId = connection.Insert(terr);

                            //Insert tax settings as soon as new territory is created
                            CreateTaxSettings(model.FranchiseId.Value, terrId.Value);
                        }
                    }

                    territoryId = model.TerritoryId;
                }
            }
            catch (DbEntityValidationException e)
            {
                var err = e.EntityValidationErrors;
                return FailedByError;
            }
            catch (DbUpdateException e)
            {
                var errData = e.Data;
                var errEntries = e.Entries;
                return FailedByError;
            }
            return Success;
        }
        /// <summary>
        /// Insert record into Taxsettings table when a territory is created
        /// </summary>
        /// <param name="FranchiseId"></param>
        /// <param name="territoryId"></param>
        /// <returns></returns>
        public bool CreateTaxSettings(int FranchiseId, int territoryId)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {

                    var tSettings = connection.Query<TaxSettings>("SELECT * FROM crm.TaxSettings ts WHERE ts.franchiseid=@franchiseid AND ts.territoryid is null", new
                    {
                        franchiseid = FranchiseId
                    }).FirstOrDefault();


                    if (tSettings == null || tSettings.Id==0)
                    {
                        //Inserting Grey area record if they do not exist
                        var tsGrey = new TaxSettings();
                        tsGrey.FranchiseId = FranchiseId;
                        tsGrey.ShipToLocationId = null;
                        tsGrey.UseCustomerAddress = true;
                        tsGrey.TaxType = 2;//Sales Tax
                        tsGrey.UseTaxPercentage = 0;
                        tsGrey.CreatedBy = SessionManager.CurrentUser.PersonId;
                        tsGrey.CreatedOn = DateTime.UtcNow;
                        tsGrey.UpdatedBy = SessionManager.CurrentUser.PersonId;
                        tsGrey.UpdatedOn = DateTime.UtcNow;
                        connection.Insert<TaxSettings>(tsGrey);
                    }
                    //Insert tax settings as soon as new territory is created
                    var ts = new TaxSettings();
                    ts.TerritoryId = territoryId;
                    ts.FranchiseId = FranchiseId;
                    ts.ShipToLocationId = null;
                    ts.UseCustomerAddress = true;
                    ts.TaxType = 2;//Sales Tax
                    ts.UseTaxPercentage = 0;
                    ts.CreatedBy = SessionManager.CurrentUser.PersonId;
                    ts.CreatedOn = DateTime.UtcNow;
                    ts.UpdatedBy = SessionManager.CurrentUser.PersonId;
                    ts.UpdatedOn = DateTime.UtcNow;
                    connection.Insert<TaxSettings>(ts);


                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return false;
            }
            return true;
        }
        /// <summary>
        /// Creates a new franchise
        /// </summary>
        /// <param name="model">First Property level info will be saved</param>
        public Franchise InitFranchise(Franchise model)
        {
            try
            {
                model.FranchiseOwners = new List<FranchiseOwner>
            {
                    new FranchiseOwner
                    {
                         User = new User
                         {
                              Person = new Person()
                         }
                    }
            };
                using (var db = ContextFactory.Create())
                {
                    if (model.FranchiseGuid == Guid.Empty)
                        model.FranchiseGuid = Guid.NewGuid();
                    if (model.Users == null)
                        model.Users = new List<User>();

                    model.TollFreeNumber = RegexUtil.GetNumbersOnly(model.TollFreeNumber);
                    model.LocalPhoneNumber = RegexUtil.GetNumbersOnly(model.LocalPhoneNumber);
                    model.FaxNumber = RegexUtil.GetNumbersOnly(model.FaxNumber);

                    //attach these two roles so EF does a foreign key insert into UsersInRoles instead of inserting new role record
                    db.Roles.Attach(AppConfigManager.DefaultRole);
                    db.Roles.Attach(AppConfigManager.DefaultOwnerRole);

                    //validate owners and their user record
                    foreach (var owner in model.FranchiseOwners)
                    {
                        if (owner.User.Roles == null || owner.User.Roles.Count == 0)
                        {
                            if (owner.User.Roles == null)
                                owner.User.Roles = new List<Role>();

                            owner.User.Roles.Add(AppConfigManager.DefaultRole);
                            owner.User.Roles.Add(AppConfigManager.DefaultOwnerRole);
                        }

                        if (owner.User.UserId == Guid.Empty)
                            owner.User.UserId = Guid.NewGuid();

                        //if (string.IsNullOrEmpty(owner.User.Address.CountryCode2Digits))
                        //    owner.User.Address.CountryCode2Digits = "US";

                        //strip special char
                        owner.User.Person.HomePhone = RegexUtil.GetNumbersOnly(owner.User.Person.HomePhone);
                        owner.User.Person.WorkPhone = RegexUtil.GetNumbersOnly(owner.User.Person.WorkPhone);
                        owner.User.Person.CellPhone = RegexUtil.GetNumbersOnly(owner.User.Person.CellPhone);
                        owner.User.Person.FaxPhone = RegexUtil.GetNumbersOnly(owner.User.Person.FaxPhone);
                    }

                    //TODO: we need to check whether it works???
                    model.MailingAddress = model.Address;

                    return model;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Gets the color of the or insert.
        /// </summary>
        /// <param name="colorRGB">The color RGB.</param>
        /// <param name="foreColorRGB">The fore color RGB.</param>
        /// <returns>System.Int32.</returns>
        public int GetOrInsertColor(string colorRGB, string foreColorRGB)
        {
            byte r = 0, g = 0, b = 0, foregroundRed = 0, foregroundGreen = 0, foregroundBlue = 0;
            decimal alpha = 1, fgAlpha = 1;
            int colorId = 0;

            if (!string.IsNullOrEmpty(colorRGB) && RegexUtil.ExtractRGB(colorRGB, out r, out g, out b, out alpha))
            {
                if (!string.IsNullOrEmpty(foreColorRGB))
                    RegexUtil.ExtractRGB(foreColorRGB, out foregroundRed, out foregroundGreen, out foregroundBlue, out fgAlpha);

                var color = CacheManager.ColorPalettes.FirstOrDefault(f => f.Red == r && f.Green == g && f.Blue == b && f.Alpha == alpha &&
                    f.FGRed == foregroundRed && f.FGGreen == foregroundGreen && f.FGBlue == foregroundBlue && f.FGAlpha == fgAlpha);

                if (color != null)
                    colorId = color.ColorId;
                else
                {
                    if (r <= 255 && g <= 255 && b <= 255 &&
                        alpha >= 0 && alpha <= 1 && foregroundRed <= 255 && foregroundGreen <= 255 && foregroundBlue <= 255 &&
                        fgAlpha >= 0 && fgAlpha <= 1)
                    {
                        try
                        {
                            using (var db = ContextFactory.Create())
                            {
                                color = new Type_Color
                                {
                                    Red = r,
                                    Green = g,
                                    Blue = b,
                                    Alpha = alpha,
                                    FGRed = foregroundRed,
                                    FGGreen = foregroundGreen,
                                    FGBlue = foregroundBlue,
                                    FGAlpha = fgAlpha,
                                    IsCustom = true
                                };

                                db.Type_Colors.Add(color);
                                db.SaveChanges();

                                if (color.ColorId > 0)
                                {
                                    colorId = color.ColorId;
                                    CacheManager.ColorPalettes.Add(color);
                                }
                            }
                        }
                        catch (Exception ex) { throw; }
                    }
                }
            }

            return colorId;
        }

        /// <summary>
        /// Gets the franchises.
        /// </summary>
        /// <param name="totalRecord">The total record.</param>
        /// <param name="includeInactive">if set to <c>true</c> [include inactive].</param>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <returns>List&lt;Franchise&gt;.</returns>
        public List<Franchise> Get(out int totalRecord
            , bool includeInactive = false
            , int? pageIndex = null
            , int pageSize = 30
            , string searchTerm = null
            , bool showInActive = false
            )
        {
            totalRecord = 0;

            var result = new List<Franchise>();
            try
            {
                var query = "select f.*, totalRecords = count(*) over() from crm.Franchise f ";

                // Apply grid filter
                if (!string.IsNullOrEmpty(searchTerm))
                {
                    query += @"join crm.Addresses a on a.AddressId = f.AddressId
                                and ( f.Name like @searchTerm or f.Code like @searchTerm or f.LocalPhoneNumber like @searchTerm
		                            or f.FaxNumber like @searchTerm or a.Address1 like @searchTerm
		                            or a.Address2 like @searchTerm  or a.City like @searchTerm
		                            or a.ZipCode like @searchTerm )";//where f.BrandId = @brandId
                }
                else
                {
                    query += " where 1=1";
                }

                // Get InActive data also
                if (!includeInactive)
                    query += " and f.IsDeleted = 0";

                if (!showInActive)
                    query += " and ( IsSuspended = 0 or IsSuspended is null)";

                // query += @"ORDER BY f.CreatedOnUtc desc, f.LastUpdatedUtc desc";
                query += @"ORDER BY f.LastUpdatedUtc desc";
                // Apply Pagination here
                //There is no need of passing the pagesize and page index if you are using Kendo grid.it will take care by default.
                //if (pageIndex.HasValue)
                //{
                //    query += @"ORDER BY f.CreatedOnUtc desc, f.LastUpdatedUtc desc OFFSET @pageIndexSize ROWS FETCH NEXT @pagesize ROWS ONLY";
                //}

                // Execute the query
                result = ExecuteIEnumerableObject<Franchise>(ContextFactory.CrmConnectionString, query, new
                {
                    // brandId = SessionManager.BrandIdOld, //SessionManager.BrandId,

                    searchTerm = string.Format($"%{searchTerm}%"),
                    //pageIndexSize = pageIndex.Value * pageSize,
                    //pagesize = pageSize
                }).ToList();

                // Get Total records
                //if (pageIndex.HasValue)
                //{
                //    totalRecord = result[0].totalRecords;
                //}

                // Get Address, user data and person info
                var multipleQuery =
                    @"SELECT a.* FROM crm.Addresses a INNER JOIN crm.Franchise f ON f.AddressId = a.AddressId ;
                      SELECT u.* FROM auth.Users u
                      INNER JOIN crm.Franchise f ON f.franchiseid = u.FranchiseId and u.IsDeleted = 0 AND ISNULL(u.IsDisabled,0)=0;
                      SELECT p.* FROM crm.Person p
                      INNER JOIN auth.Users u ON u.PersonId = p.PersonId;
                      SELECT * FROM crm.FranchiseOwners fo;";
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    var fDetails = connection.QueryMultiple(multipleQuery);

                    var ListAddresses = fDetails.Read<Address>().ToList();
                    var Users = fDetails.Read<User>().ToList();
                    var Persons = fDetails.Read<Person>().ToList();
                    var Franchiseowners = fDetails.Read<FranchiseOwner>().ToList();

                    foreach (var item in result)
                    {
                        item.Address =
                            ListAddresses.Where(x => x.AddressId == item.AddressId).FirstOrDefault();
                        item.Users = Users.Where(x => x.FranchiseId == item.FranchiseId).ToList();
                        item.FranchiseOwners = Franchiseowners.Where(x => x.FranchiseId == item.FranchiseId).ToList();
                        foreach (var subItem in item.FranchiseOwners)
                        {
                            subItem.User = Users.Where(x => x.UserId == subItem.UserId).FirstOrDefault();
                            subItem.User.Person =
                                Persons.Where(x => x.PersonId == subItem.User.PersonId).FirstOrDefault();
                        }
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string UpdateOwnerInfoIntoJson()
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var franchiseList = connection.GetList<Franchise>();

                    foreach (var franchise in franchiseList)
                    {
                        bool isJsonData = false;
                        if (franchise != null && franchise.OwnerName != null)
                        {
                            var ownerName = franchise.OwnerName.Trim();
                            if ((ownerName.StartsWith("{") && ownerName.EndsWith("}")) || //For object
                                (ownerName.StartsWith("[") && ownerName.EndsWith("]"))) //For array
                            {
                                isJsonData = true;
                            }
                            else if (ownerName.Contains("&"))
                            {
                                var OwnerNamelst = ownerName.Split('&');
                                foreach (var item in OwnerNamelst)
                                {
                                    OwnerVM owner = new OwnerVM();
                                    var trimItem = item.Trim();
                                    var ownerFullName = trimItem.Split(' ');
                                    owner.FirstName = ownerFullName[0];
                                    if (ownerFullName.Length == 2)
                                        owner.LastName = ownerFullName[1];

                                    franchise.lstOwnerName.Add(owner);
                                }
                            }
                            else if (ownerName.Contains("and"))
                            {
                                var OwnerNamelst = ownerName.Split(new string[] { "and" }, StringSplitOptions.None);
                                foreach (var item in OwnerNamelst)
                                {
                                    OwnerVM owner = new OwnerVM();
                                    var trimItem = item.Trim();
                                    var ownerFullName = trimItem.Split(' ');
                                    owner.FirstName = ownerFullName[0];
                                    if (ownerFullName.Length == 2)
                                        owner.LastName = ownerFullName[1];
                                    franchise.lstOwnerName.Add(owner);
                                }
                            }
                            else
                            {
                                OwnerVM owner = new OwnerVM();
                                var ownerFullName = franchise.OwnerName.Split(' ');
                                owner.FirstName = ownerFullName[0];
                                if (ownerFullName.Length == 2)
                                    owner.LastName = ownerFullName[1];
                                franchise.lstOwnerName.Add(owner);
                            }

                            if (isJsonData == false)
                            {
                                if (franchise.lstOwnerName != null)
                                {
                                    if (franchise.lstOwnerName.Count != 0)
                                        franchise.OwnerName = JsonConvert.SerializeObject(franchise.lstOwnerName);
                                }

                                connection.Update(franchise);
                            }
                        }
                    }

                    connection.Close();
                    //db.SaveChanges();
                }
            }
            catch (DbEntityValidationException e)
            {
                var err = e.EntityValidationErrors;
                return FailedByError;
            }
            catch (DbUpdateException e)
            {
                var errData = e.Data;
                var errEntries = e.Entries;
                return FailedByError;
            }
            return Success;
        }

        /// <summary>
        /// QB Enabled by HFC user - Rajkumar
        /// </summary>
        /// <param name="id"></param>
        public void AddQBApp(int id)
        {
            try
            {
                var QBCheck = ExecuteIEnumerableObjectSingle<QBApps>(ContextFactory.CrmConnectionString, "where FranchiseId=@FranchiseId", new { FranchiseId = id }).FirstOrDefault();

                if (QBCheck != null)
                    throw new Exception("QB app is already added");

                var qbApp = new QBApps()
                {
                    AppGuid = Guid.NewGuid(),
                    IsActive = true,
                    FranchiseId = id,
                    OwnerID = Guid.NewGuid().ToString()
                };

                var res = Insert<QBApps>(ContextFactory.CrmConnectionString, qbApp);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override Franchise Get(int id)
        {
            if (id <= 0)
                throw new ArgumentOutOfRangeException("Invalid franchise id");

            string Result;

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                // Get Franchise data, Address, MailingAddress and Territories
                var franchise = connection.Get<Franchise>(id);
                franchise.Address = connection.Get<Address>(franchise.AddressId);
                franchise.MailingAddress = connection.Get<Address>(franchise.MailingAddressId);
                franchise.Territories = connection.GetList<Territory>("where franchiseid = @franchiseId", new { franchiseId = franchise.FranchiseId }).ToList();
                foreach (var territory in franchise.Territories)
                {
                    territory.FranchiseRoyalties = connection.GetList<FranchiseRoyalty>(
                        "where TerritoryId = @territoryId", new { territoryId = territory.TerritoryId }).ToList();
                }

                franchise.FranchiseOwners = connection.GetList<FranchiseOwner>("where franchiseid = @franchiseId", new { franchiseId = franchise.FranchiseId }).ToList();
                foreach (var fowner in franchise.FranchiseOwners)
                {
                    fowner.User = connection.Get<User>(fowner.UserId);
                    fowner.User.Address = connection.Get<Address>(fowner.User.AddressId);

                    fowner.User.Person = connection.Get<Person>(fowner.User.PersonId);
                }

                franchise.Users = connection.GetList<User>("where franchiseid = @franchiseId  and IsDeleted = 0 and (IsDisabled=0 or IsDisabled is null)", new { franchiseId = franchise.FranchiseId }).ToList();
                foreach (var user in franchise.Users)
                {
                    user.Person = connection.Get<Person>(user.PersonId);
                    user.Address = connection.Get<Address>(user.AddressId);
                }
                franchise.Users = franchise.Users.Where(v => (franchise.FranchiseOwners.Any() && v.UserId != franchise.FranchiseOwners.First().UserId) || !franchise.FranchiseOwners.Any()).OrderBy(x => x.UserName).ToList();
                var QBApp = connection.GetList<QBApps>("where FranchiseId=@FranchiseId", new { FranchiseId = id }).ToList();

                if (QBApp != null && QBApp.Count > 0)
                    franchise.IsQBEnabled = true;
                else
                    franchise.IsQBEnabled = false;

                connection.Close();
                if (franchise != null && franchise.OwnerName != null)
                {
                    var ownerName = franchise.OwnerName.Trim();
                    if ((ownerName.StartsWith("{") && ownerName.EndsWith("}")) || //For object
                        (ownerName.StartsWith("[") && ownerName.EndsWith("]"))) //For array
                    {
                        franchise.lstOwnerName = JsonConvert.DeserializeObject<List<OwnerVM>>(franchise.OwnerName);
                    }
                }
                // TODO: the following is for only for debugging.
                Result = JsonConvert.SerializeObject(franchise, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    PreserveReferencesHandling = PreserveReferencesHandling.None
                });

                return franchise;
            }
        }

        public string Add(out int franchiseId, Franchise data)
        {
            var db = ContextFactory.Create();
            this.Add(out franchiseId, data, db);
            return Success;
        }

        public string Deactivated(int id, int deactivationReasonId, string additionalReason = null)
        {
            if (id <= 0)
            {
                return DataCannotBeNullOrEmpty;
            }
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();

                    //TODO: Will it be good to just issue an update query directly using dapper instead?
                    var franchise = connection.Get<Franchise>(id);
                    franchise.IsSuspended = true;
                    franchise.DeactivationReasonId = deactivationReasonId;
                    franchise.DeactivationAdditionalReason = additionalReason;
                    connection.Update(franchise);
                    franchise.Address = connection.Get<Address>(franchise.AddressId);

                    //TODO: When I deactivate a franchise, I also want to deactivate associated user IDs.

                    connection.Close();

                    PICCustomer cus = new PICCustomer();
                    cus.Customer = franchise.Code;
                    cus.DeActivate = "Y";
                    cus.Salesperson1 = 1;
                    PICManager pic = new PICManager();

                    pic.CreateOrUpdatePICCustomer(CreatePICCustomerObject(franchise));
                }

                //using (var db = ContextFactory.Create())
                //{
                //    var frn = db.Franchises.SingleOrDefault(x => x.FranchiseId == id);

                //    if (frn != null)
                //    {
                //        frn.IsSuspended = true;
                //        db.SaveChanges();
                //    }
                //}
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }
            return Success;
        }

        public bool IsSuspended(int id)
        {
            if (id <= 0)
                throw new ArgumentOutOfRangeException("Invalid franchise id");
            using (var db = ContextFactory.Create())
            {
                var fran = db.Franchises.SingleOrDefault(v => v.FranchiseId == id);
                if (fran != null)
                {
                    return fran.IsSuspended.HasValue && fran.IsSuspended.Value;
                }
            }
            throw new ArgumentOutOfRangeException("Invalid franchise id");
        }

        public string RemoveSuspend(List<Territory> territorylst)
        {
            if (territorylst.Count() <= 0)
            {
                return DataCannotBeNullOrEmpty;
            }
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    //var territory = connection.GetList<Territory>("where FranchiseId = @Id", new { Id = territorylst[0].FranchiseId }).ToList();
                    //TODO: Will it be good to just issue an update query directly using dapper instead?
                    foreach (var item in territorylst)
                    {
                        foreach (var subItem in item.FranchiseRoyalties)
                        {
                            connection.Update(subItem);
                        }
                    }
                    var franchise = connection.Get<Franchise>(territorylst[0].FranchiseId);
                    franchise.Address = connection.Get<Address>(franchise.AddressId);
                    franchise.IsSuspended = false;
                    PICManager pic = new PICManager();
                    pic.CreateOrUpdatePICCustomer(CreatePICCustomerObject(franchise));
                    connection.Update(franchise);

                    connection.Close();
                }

                //using (var db = ContextFactory.Create())
                //{
                //    var frn = db.Franchises.SingleOrDefault(x => x.FranchiseId == id);

                //    if (frn != null)
                //    {
                //        frn.IsSuspended = false;
                //        db.SaveChanges();
                //    }
                //}
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }
            return Success;
        }

        public string ReactivateFranchise(int franchiseId)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();

                    var franchise = connection.Get<Franchise>(franchiseId);
                    var address = connection.Get<Address>(franchise.AddressId);
                    franchise.Address = address;
                    franchise.IsSuspended = false;
                    connection.Update(franchise);

                    connection.Close();

                    PICCustomer cus = new PICCustomer();
                    cus.Customer = franchise.Code;
                    cus.DeActivate = "N";
                    cus.Salesperson1 = 1;
                    PICManager pic = new PICManager();
                    pic.CreateOrUpdatePICCustomer(CreatePICCustomerObject(franchise));
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }
            return Success;
        }

        #region Lead and Job Status methods

        /// <summary>
        /// Adds the lead status.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <returns>System.String.</returns>
        public string AddLeadStatus(Type_LeadStatus status)
        {
            if (status == null)
                return DataCannotBeNullOrEmpty;
            if (string.IsNullOrEmpty(status.Name))
                return "Action cancelled, status must have a name";

            // TODO: is this required in TP???
            //if (!Account_BasePermission.CanCreate)
            //    return Unauthorized;

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var original =
                        db.Type_LeadStatus.SingleOrDefault(
                            f =>
                                (f.FranchiseId == status.FranchiseId || !f.FranchiseId.HasValue) &&
                                f.ParentId == status.ParentId && f.Name == status.Name);
                    if (original != null)
                        return "Action cancelled, duplicate status found.";

                    status.CreatedAt = DateTime.Now;
                    db.Type_LeadStatus.Add(status);
                    db.SaveChanges();

                    //clear cache so it can be reloaded
                    CacheManager.LeadStatusTypeCollection = null;

                    return Success;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Updates the lead status.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <returns>System.String.</returns>
        public string UpdateLeadStatus(Type_LeadStatus status)
        {
            if (status == null || status.Id <= 0)
                return DataCannotBeNullOrEmpty;
            if (string.IsNullOrEmpty(status.Name))
                return "Action cancelled, status must have a name";

            // TODO: is this required in TP???
            //if (!Account_BasePermission.CanUpdate)
            //    return Unauthorized;

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var original =
                        db.Type_LeadStatus.SingleOrDefault(
                            f => f.FranchiseId == status.FranchiseId && f.Id == status.Id);
                    if (original == null)
                        return "Action cancelled, lead status does not exist.";

                    original.Name = status.Name;
                    db.SaveChanges();

                    //clear cache so it can be reloaded
                    CacheManager.LeadStatusTypeCollection = null;
                    return Success;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Deletes the lead status.
        /// </summary>
        /// <param name="franchiseId">The franchise identifier.</param>
        /// <param name="statusId">The status identifier.</param>
        /// <returns>System.String.</returns>
        public string DeleteLeadStatus(int franchiseId, int statusId)
        {
            if (franchiseId <= 0 || statusId <= 0)
                return DataCannotBeNullOrEmpty;

            // TODO: is this required in TP???
            //if (!Account_BasePermission.CanDelete)
            //    return Unauthorized;

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var original =
                        db.Type_LeadStatus.SingleOrDefault(
                            f => f.FranchiseId == franchiseId && f.Id == statusId);
                    if (original != null)
                    {
                        int count = db.Leads.Count(a => a.LeadStatusId == statusId);
                        if (count > 0)
                            return string.Format("Unable to delete status, there are {0} lead(s) using this status",
                                count);

                        db.Type_LeadStatus.Remove(original);
                        db.SaveChanges();
                        CacheManager.LeadStatusTypeCollection = null;
                    }
                    return Success;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Adds the lead status.
        /// </summary>
        /// <param name="question">The status.</param>
        /// <returns>System.String.</returns>
        public string AddQuestion(Type_Question question)
        {
            if (question == null)
                return DataCannotBeNullOrEmpty;
            if (string.IsNullOrEmpty(question.SubQuestion))
                return "Action cancelled, question field is required";

            // TODO: is this required in TP???
            //if (!Account_BasePermission.CanCreate)
            //    return Unauthorized;

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var original =
                        db.Type_Questions.SingleOrDefault(
                            f =>
                                (f.FranchiseId == question.FranchiseId || !f.FranchiseId.HasValue) &&
                                f.SubQuestion == question.SubQuestion && f.Question == question.Question);
                    if (original != null)
                        return "Action cancelled, duplicate question found.";

                    question.SelectionType = question.SelectionType == "textbox" ? null : question.SelectionType;

                    db.Type_Questions.Add(question);
                    db.SaveChanges();

                    //clear cache so it can be reloaded
                    CacheManager.QuestionTypeCollection = null;

                    return Success;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Updates the lead status.
        /// </summary>
        /// <param name="question">The status.</param>
        /// <returns>System.String.</returns>
        public string UpdateQuestion(Type_Question question)
        {
            if (question == null || question.QuestionId <= 0)
                return DataCannotBeNullOrEmpty;
            if (string.IsNullOrEmpty(question.SubQuestion))
                return "Action cancelled, question is required";

            //if (!Account_BasePermission.CanUpdate)
            //    return Unauthorized;

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var original =
                        db.Type_Questions.SingleOrDefault(
                            f => f.QuestionId == question.QuestionId);
                    if (original == null)
                        return "Action cancelled, question does not exist.";

                    original.SubQuestion = question.SubQuestion;
                    original.Question = question.Question;
                    original.SelectionType = question.SelectionType == "textbox" ? null : question.SelectionType;
                    db.SaveChanges();

                    //clear cache so it can be reloaded
                    CacheManager.QuestionTypeCollection = null;

                    return Success;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Deletes the lead status.
        /// </summary>
        /// <param name="franchiseId">The franchise identifier.</param>
        /// <param name="questionId">The status identifier.</param>
        /// <returns>System.String.</returns>
        public string DeleteQuestion(int franchiseId, int questionId)
        {
            if (franchiseId <= 0 || questionId <= 0)
                return DataCannotBeNullOrEmpty;

            // TODO: is this required in TP???
            //if (!Account_BasePermission.CanDelete)
            //    return Unauthorized;

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var original =
                        db.Type_Questions.SingleOrDefault(
                            f => f.FranchiseId == franchiseId && f.QuestionId == questionId);
                    if (original != null)
                    {
                        if (
                            db.JobQuestionAnswers.Any(
                                v => string.IsNullOrEmpty(v.Answer) && v.QuestionId == questionId))
                        {
                            db.JobQuestionAnswers.RemoveRange(
                                db.JobQuestionAnswers.Where(
                                    v => string.IsNullOrEmpty(v.Answer) && v.QuestionId == questionId));
                            db.SaveChanges();
                        }

                        int count =
                            db.Jobs.Sum(
                                a => a.JobQuestionAnswers.Any(qa => qa.QuestionId == questionId) ? 1 : 0);
                        if (count > 0)
                            return string.Format("Unable to delete question, there are {0} job(s) using this question",
                                count);

                        db.Type_Questions.Remove(original);
                        db.SaveChanges();
                        CacheManager.QuestionTypeCollection = null;
                    }
                    return Success;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Adds the job status.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <returns>System.String.</returns>
        public string AddJobStatus(Type_JobStatus status)
        {
            if (status == null)
                return DataCannotBeNullOrEmpty;
            if (string.IsNullOrEmpty(status.Name))
                return "Action cancelled, status must have a name";

            // TODO: is this required in TP???
            //if (!Account_BasePermission.CanCreate)
            //    return Unauthorized;

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var original =
                        db.Type_JobStatus.SingleOrDefault(
                            f =>
                                (f.FranchiseId == status.FranchiseId || !f.FranchiseId.HasValue) &&
                                f.ParentId == status.ParentId && f.Name == status.Name);
                    if (original != null)
                        return "Action cancelled, duplicate status found.";

                    status.CreatedAt = DateTime.Now;
                    db.Type_JobStatus.Add(status);
                    db.SaveChanges();

                    //clear cache so it can be reloaded
                    CacheManager.JobStatusTypeCollection = null;

                    return Success;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Updates the job status.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <returns>System.String.</returns>
        public string UpdateJobStatus(Type_JobStatus status)
        {
            if (status == null || status.Id <= 0)
                return DataCannotBeNullOrEmpty;
            if (string.IsNullOrEmpty(status.Name))
                return "Action cancelled, status must have a name";

            // TODO: is this required in TP???
            //if (!Account_BasePermission.CanUpdate)
            //    return Unauthorized;

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var original =
                        db.Type_JobStatus.SingleOrDefault(
                            f => f.FranchiseId == status.FranchiseId && f.Id == status.Id);
                    if (original == null)
                        return "Action cancelled, job status does not exist.";

                    original.Name = status.Name;
                    db.SaveChanges();

                    //clear cache so it can be reloaded
                    CacheManager.JobStatusTypeCollection = null;

                    return Success;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Deletes the job status.
        /// </summary>
        /// <param name="franchiseId">The franchise identifier.</param>
        /// <param name="statusId">The status identifier.</param>
        /// <returns>System.String.</returns>
        public string DeleteJobStatus(int franchiseId, int statusId)
        {
            if (franchiseId <= 0 || statusId <= 0)
                return DataCannotBeNullOrEmpty;

            // TODO: is this required in TP???
            //if (!Account_BasePermission.CanDelete)
            //    return Unauthorized;

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var original = db.Type_JobStatus.SingleOrDefault(f => f.FranchiseId == franchiseId && f.Id == statusId);
                    if (original != null)
                    {
                        int count = db.Jobs.Count(a => a.JobStatusId == statusId);
                        if (count > 0)
                            return string.Format("Unable to delete status, there are {0} job(s) using this status",
                                count);

                        db.Type_JobStatus.Remove(original);
                        db.SaveChanges();

                        CacheManager.JobStatusTypeCollection = null;
                    }
                }
                return Success;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion Lead and Job Status methods

        /// <summary>
        /// Deletes the source.
        /// </summary>
        /// <param name="franchiseId">The franchise identifier.</param>
        /// <param name="sourceId">The source identifier.</param>
        /// <returns>System.String.</returns>
        public string DeleteSource(int franchiseId, int sourceId)
        {
            if (franchiseId <= 0 && sourceId <= 0)
                return "Invalid source id";

            // TODO: is this required in TP???
            //if (!Account_BasePermission.CanDelete)
            //    return Unauthorized;

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var original = db.Sources.SingleOrDefault(f => f.SourceId == sourceId && f.FranchiseId == franchiseId);
                    if (original != null)
                    {
                        db.Sources.Remove(original);
                        db.SaveChanges();
                    }
                }

                return Success;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }
        }

        [Obsolete]
        public string UpdateCampaign(Campaign model)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var campaign = connection.Get<Campaign>(model.CampaignId);
                    campaign.SourcesTPId = model.SourcesTPId;
                    campaign.StartDate = model.StartDate;
                    campaign.Amount = model.Amount;
                    campaign.Memo = model.Memo;
                    campaign.Name = model.Name;
                    campaign.EndDate = model.EndDate;
                    campaign.IsActive = model.IsActive;
                    campaign.LastUpdatedOn = DateTime.Now;
                    campaign.LastUpdatedBy = model.LastUpdatedBy;
                    //DateTime todatDtLocal = TimeZoneManager.ToLocal(DateTime.UtcNow);
                    //if (campaign.EndDate != null)
                    //{
                    //    if (campaign.EndDate <= todatDtLocal.Date)
                    //    {
                    //        campaign.IsActive = false;
                    //    }
                    //}

                    var response = connection.Update(campaign);
                    connection.Close();
                    return Success;
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }
        }

        [Obsolete]
        public string AddNewCampaign(Campaign model)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    //DateTime todatDtLocal = TimeZoneManager.ToLocal(DateTime.UtcNow);
                    //if (model.EndDate != null)
                    //{
                    //    if (model.EndDate <= todatDtLocal.Date)
                    //    {
                    //        model.IsActive = false;
                    //    }
                    //}

                    var campaignId = (int)connection.Insert(model);
                    connection.Close();
                    return Success;
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }
        }

        /// <summary>
        /// Adds the source.
        /// </summary>
        /// <param name="src">The source.</param>
        /// <returns>System.String.</returns>
        public string AddSource(Source src)
        {
            if (src == null || src.FranchiseId <= 0)
                return DataCannotBeNullOrEmpty;
            if (string.IsNullOrEmpty(src.Name))
                return "Action cancelled, source name required";

            // TODO: is this required in TP???
            //if (!Account_BasePermission.CanCreate)
            //    return Unauthorized;

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var originalTax =
                        db.Sources.SingleOrDefault(
                            f =>
                                (f.FranchiseId == src.FranchiseId || !f.FranchiseId.HasValue) &&
                                f.ParentId == src.ParentId && f.Name == src.Name);
                    if (originalTax != null)
                        return "Action cancelled, duplicate source found.";

                    db.Sources.Add(src);
                    db.SaveChanges();

                    CacheManager.SourceCollection = null;

                    return Success;
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }
        }

        /// <summary>
        /// Updates the source.
        /// </summary>
        /// <param name="src">The source.</param>
        /// <returns>System.String.</returns>
        public string UpdateSource(Source src)
        {
            if (src == null || src.FranchiseId <= 0 || src.SourceId <= 0)
                return DataCannotBeNullOrEmpty;
            if (string.IsNullOrEmpty(src.Name))
                return "Action cancelled, source name is required";

            // TODO: is this required in TP???
            //if (!Account_BasePermission.CanUpdate)
            //    return Unauthorized;

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var original =
                        db.Sources.SingleOrDefault(
                            f => f.FranchiseId == src.FranchiseId && f.SourceId == src.SourceId);
                    if (original == null)
                        return "Action cancelled, source does not exist";

                    var isDuplicated =
                        db.Sources.FirstOrDefault(
                            f =>
                                (f.FranchiseId == src.FranchiseId || !f.FranchiseId.HasValue) &&
                                f.ParentId == src.ParentId && f.Name == src.Name);
                    if (isDuplicated != null)
                        return "Action cancelled, duplicate source found.";

                    original.Name = src.Name;
                    original.Amount = src.Amount;
                    original.Memo = src.Memo;

                    original.StartDate = src.StartDate;
                    original.EndDate = src.EndDate;

                    db.SaveChanges();

                    CacheManager.SourceCollection = null;

                    return Success;
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }
        }

        private static readonly EntityConnectionStringBuilder EntityCnxStringBuilder = new EntityConnectionStringBuilder(System.Configuration.ConfigurationManager.ConnectionStrings["CRMContext"].ConnectionString);

        public IEnumerable<SourceDTO> GetSourcesTwo()
        {
            try
            {
                if (SessionManager.CurrentFranchise != null)
                {
                    using (var conn = new SqlConnection(ContextFactory.CrmConnectionString))
                    {
                        //conn.Open();
                        var dto = conn.Query<SourceDTO>(
                            "[crm].[GetSourcesByFranchise] @FranchiseId",
                            new
                            {
                                franchiseId = SessionManager.CurrentFranchise.FranchiseId,
                            });
                        return dto;
                    }
                }
                return Enumerable.Empty<SourceDTO>();
            }
            catch (Exception e)
            {
                EventLogger.LogEvent(e);
                return Enumerable.Empty<SourceDTO>();
                //throw;
            }
        }

        public static List<SourceDTO> GetSourcesThree()
        {
            using (var conn = new SqlConnection(EntityCnxStringBuilder.ProviderConnectionString))
            {
                conn.Open();
                var dto = conn.Query<SourceDTO>(
                    "[crm].[GetSources_LeadByFranchise] @FranchiseId",
                    new
                    {
                        franchiseId = SessionManager.CurrentFranchise.FranchiseId,
                    }).ToList();
                return dto;
            }
        }

        #region Marketing Campaign Methods

        /// <summary>
        /// Updates the campaign.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>System.String.</returns>
        public string UpdateCampaign(MarketingCampaign model)
        {
            if (model == null)
                return DataCannotBeNullOrEmpty;

            // TODO: is this required in TP???
            //if (!Sources_BasePermission.CanUpdate)
            //    return Unauthorized;

            if (model.FranchiseId <= 0 && Franchise != null)
                model.FranchiseId = Franchise.FranchiseId;

            using (var db = ContextFactory.Create())
            {
                var entry = db.Entry(model);

                var validation = entry.GetValidationResult();
                if (!validation.IsValid)
                    return validation.ToFlattenString();

                try
                {
                    var original =
                        db.MarketingCampaigns.SingleOrDefault(s => s.MktCampaignId == model.MktCampaignId);
                    if (original == null)
                        return "Campaign does not exist";

                    original.Amount = model.Amount;
                    original.EndDate = model.EndDate;
                    original.StartDate = model.StartDate;
                    original.Label = model.Label;
                    original.IsActive = model.IsActive;
                    original.Memo = model.Memo;

                    db.SaveChanges();
                    return Success;
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Deletes the campaign.
        /// </summary>
        /// <param name="mktCampaignId">The MKT campaign identifier.</param>
        /// <returns>System.String.</returns>
        public string DeleteCampaign(int mktCampaignId)
        {
            if (mktCampaignId <= 0)
                return DataCannotBeNullOrEmpty;

            // TODO: is this required in TP???
            //if (!Sources_BasePermission.CanDelete)
            //    return Unauthorized;

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var original =
                        db.MarketingCampaigns.Any(
                            s => s.MktCampaignId == mktCampaignId && s.FranchiseId == Franchise.FranchiseId);
                    if (!original)
                        return "Campaign does not exist";

                    var mkt = new MarketingCampaign { MktCampaignId = mktCampaignId };
                    db.MarketingCampaigns.Attach(mkt);
                    db.MarketingCampaigns.Remove(mkt);

                    db.SaveChanges();
                    return Success;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion Marketing Campaign Methods

        #region Royalties

        public IEnumerable<Territory> GetRoyalties(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                var territory = connection.GetList<Territory>("where FranchiseId = @Id", new { Id = id }).ToList();
                if (territory != null)
                {
                    foreach (var item in territory)
                    {
                        item.FranchiseRoyalties = connection.GetList<FranchiseRoyalty>("where territoryId = @territoryId", new { territoryId = item.TerritoryId }).ToList();
                        foreach (var subItem in item.FranchiseRoyalties)
                        {
                            subItem.Type_Royalty = connection.Get<Type_Royalty>(subItem.Type_RoyaltyId);
                        }
                    }
                }
                connection.Close();
                return territory;
            }
            //using (var db = ContextFactory.Create())
            //{
            //    return
            //        db.Territories.AsNoTracking().Include(i => i.FranchiseRoyalties.Select(r => r.Type_Royalty))
            //            .Where(t => t.FranchiseId == id)
            //            .ToList();
            //}
        }

        public string AppyRoyaltyTemplate(int territoryId, int profileId, DateTime? startDate)
        {
            if (!startDate.HasValue)
            {
                return "Start Date is required";
            }
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                //var profile =
                //    db.Type_RoyaltyProfile.Include(i => i.RoyaltyTemplates.Select(rt => rt.Type_Royalty))
                //        .FirstOrDefault(t => t.Id == profileId);
                var profile = connection.Get<Type_RoyaltyProfile>(profileId);
                if (profile != null)
                {
                    profile.RoyaltyTemplates = connection.GetList<RoyaltyTemplate>("where RoyaltyProfileId = @profileid and BrandId = @brandid", new { profileid = profileId, brandid = SessionManager.BrandId }).ToList();
                    foreach (var item in profile.RoyaltyTemplates)
                    {
                        item.Type_Royalty = connection.Get<Type_Royalty>(item.Type_RoyaltyId);
                    }
                }
                else
                    return "RoyaltyProfile does not exist";

                //var territory =
                //    db.Territories.Include(i => i.FranchiseRoyalties)
                //        .FirstOrDefault(t => t.TerritoryId == territoryId);
                var territory = connection.Get<Territory>(territoryId);
                if (territory != null)
                {
                    territory.FranchiseRoyalties = connection.GetList<FranchiseRoyalty>("where territoryId = @territoryId", new { territoryId = territory.TerritoryId }).ToList();
                }
                if (territory == null)
                {
                    return "Territory does not exist";
                }

                var toDelete = territory.FranchiseRoyalties.ToList();
                foreach (var franchiseRoyalty in toDelete)
                {
                    // db.FranchiseRoyalties.Remove(franchiseRoyalty);
                    connection.Delete(franchiseRoyalty);
                }
                var royaltyStructure = connection.Query<fntRoyaltyStructure_Result>("SELECT * FROM [CRM].[fntRoyaltyStructure]( @startDate, @profileId)", new { startDate = startDate, profileId = profileId }).OrderBy(x => x.Order).ToList();
                foreach (var t in royaltyStructure)
                {
                    var franRoyalties = new FranchiseRoyalty()
                    {
                        TerritoryId = territoryId,
                        Amount = t.Amount,
                        Name = t.Royalty,
                        StartOnUtc = t.StartDate,
                        EndOnUtc = t.EndDate,
                        Type_RoyaltyId = t.RoyaltyId
                    };
                    connection.Insert(franRoyalties);
                }

                //db.SaveChanges();

                return Success;
            }
        }

        public string GetNextRoyaltyLevelName(int territoryId, out string nextRoyaltyName)
        {
            nextRoyaltyName = null;

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var territory = connection.Get<Territory>(territoryId);
                if (territory != null)
                {
                    territory.FranchiseRoyalties = connection.GetList<FranchiseRoyalty>("where territoryId = @territoryId", new { territoryId = territory.TerritoryId }).ToList();
                    foreach (var item in territory.FranchiseRoyalties)
                    {
                        item.Type_Royalty = connection.Get<Type_Royalty>(item.Type_RoyaltyId);
                    }
                }
                //var territory =
                //    db.Territories.Include(i => i.FranchiseRoyalties.Select(r => r.Type_Royalty))
                //        .FirstOrDefault(t => t.TerritoryId == territoryId);
                if (territory == null)
                {
                    return "Territory does not exist";
                }

                var lastUsedRoyaltyType =
                    territory.FranchiseRoyalties.OrderByDescending(o => o.Type_Royalty.Order).FirstOrDefault();

                var lastUsedRoyaltyTypeOder = lastUsedRoyaltyType != null ? lastUsedRoyaltyType.Type_Royalty.Order : -1;

                //var nextRoyaltyType = db.Type_Royalty.FirstOrDefault(r => r.Order > lastUsedRoyaltyTypeOder);
                var nextRoyaltyType = connection.GetList<Type_Royalty>("where [CRM].[Type_Royalty].[Order] > @lastUsedRoyaltyTypeOderValue", new { lastUsedRoyaltyTypeOderValue = lastUsedRoyaltyTypeOder }).FirstOrDefault();
                if (nextRoyaltyType == null)
                {
                    return "No available royalty levels";
                }

                nextRoyaltyName = nextRoyaltyType.Name;

                return Success;
            }
        }

        public string AddNextRoyaltyLevel(int territoryId, int monthes, decimal amount)
        {
            if (monthes == 0)
            {
                return "Amount of months should be greater than 0";
            }

            if (amount == 0)
            {
                return "Amount should be greater than 0";
            }
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var territory = connection.Get<Territory>(territoryId);
                if (territory != null)
                {
                    territory.FranchiseRoyalties = connection.GetList<FranchiseRoyalty>("where TerritoryId = @territoryId", new { territoryId = territory.TerritoryId }).ToList();
                    foreach (var item in territory.FranchiseRoyalties)
                    {
                        item.Type_Royalty = connection.Get<Type_Royalty>(item.Type_RoyaltyId);
                    }
                }
                //var territory =
                //    db.Territories.Include(i => i.FranchiseRoyalties.Select(r => r.Type_Royalty))
                //        .FirstOrDefault(t => t.TerritoryId == territoryId);
                if (territory == null)
                {
                    return "Territory does not exist";
                }

                var lastUsedRoyaltyType =
                    territory.FranchiseRoyalties.OrderByDescending(o => o.Type_Royalty.Order).FirstOrDefault();

                var lastUsedRoyaltyTypeOder = lastUsedRoyaltyType != null ? lastUsedRoyaltyType.Type_Royalty.Order : -1;

                // var nextRoyaltyType = db.Type_Royalty.FirstOrDefault(r => r.Order > lastUsedRoyaltyTypeOder);
                var nextRoyaltyType = connection.GetList<Type_Royalty>("where [CRM].[Type_Royalty].[Order] > @lastUsedRoyaltyTypeOderValue", new { lastUsedRoyaltyTypeOderValue = lastUsedRoyaltyTypeOder }).FirstOrDefault();
                if (nextRoyaltyType == null)
                {
                    return "No available royalty levels";
                }
                var franchiseRoyalties = new FranchiseRoyalty()
                {
                    TerritoryId = territoryId,
                    Amount = amount,
                    Name = nextRoyaltyType.Name,
                    StartOnUtc = lastUsedRoyaltyType != null ? lastUsedRoyaltyType.EndOnUtc : null,
                    EndOnUtc =
                        lastUsedRoyaltyType != null && lastUsedRoyaltyType.EndOnUtc.HasValue
                            ? lastUsedRoyaltyType.EndOnUtc.Value.AddMonths(monthes)
                            : (DateTime?)null,
                    Type_RoyaltyId = nextRoyaltyType.Id
                };
                var royaltId = connection.Insert(franchiseRoyalties);
                // db.SaveChanges();

                return Success;
            }
        }

        public string DeleteRoyalty(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var royalty = connection.GetList<FranchiseRoyalty>("where RoyaltyId = @id", new { id = id }).FirstOrDefault();
                //var royalty = db.FranchiseRoyalties.FirstOrDefault(r => r.RoyaltyId == id);
                if (royalty == null)
                {
                    return "Royalty does not exist";
                }
                connection.Delete(royalty);
                //db.FranchiseRoyalties.Remove(royalty);
                //db.SaveChanges();
                connection.Close();
                return Success;
            }
        }

        public string UpdateRoyalty(int id, FranchiseRoyalty model)
        {
            if (string.IsNullOrEmpty(model.Name))
            {
                return "Royalty Name is required";
            }

            if (model.Amount == 0)
            {
                return "Royalty Amount is required";
            }
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                //var royalty = db.FranchiseRoyalties.FirstOrDefault(r => r.RoyaltyId == id);
                var royalty = connection.GetList<FranchiseRoyalty>("where RoyaltyId = @id", new { id = id }).FirstOrDefault();
                if (royalty == null)
                {
                    return "Royalty does not exist";
                }

                royalty.Name = model.Name;
                royalty.Amount = model.Amount;
                royalty.StartOnUtc = model.StartOnUtc;
                royalty.EndOnUtc = model.EndOnUtc;
                connection.Update(royalty);
                //db.SaveChanges();
                connection.Close();
                return Success;
            }
        }

        public List<Territory> GetUnexpiredRoyalty(int franchiseId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var terrorities = connection.GetList<Territory>("where FranchiseId = @id ", new { id = franchiseId }).ToList();
                foreach (var item in terrorities)
                {
                    item.FranchiseRoyalties = connection.GetList<FranchiseRoyalty>("where TerritoryId = @id and EndOnUtc >= @todayDate ", new { id = item.TerritoryId, todayDate = DateTime.Now.ToString("MM-dd-yyyy") }).ToList();
                }
                connection.Close();

                return terrorities;
            }
        }

        #endregion Royalties


        /// <summary>
        /// Not Implemented
        /// </summary>
        /// <param name="idList"></param>
        /// <returns></returns>
        public override ICollection<Franchise> Get(List<int> idList)
        {
            throw new NotImplementedException();
        }

        public override string Update(Franchise model)
        {
            try
            {
                var db = ContextFactory.Create();
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var franchise = connection.Get<Franchise>(model.FranchiseId);
                    if (franchise != null)
                    {
                        if (string.IsNullOrEmpty(model.Currency))
                        {
                            model.Currency = model.CountryCode;
                        }
                        if (franchise.Code != model.Code)
                            ValidateFranchise(model);

                        //if (model.IsAddressSame == true)
                        //{
                        //    model.MailingAddressId = model.AddressId;
                        //}
                        //else
                        //{
                        //    if (model.MailingAddressId == model.AddressId)
                        //    {
                        //        model.MailingAddress.CreatedOnUtc = model.MailingAddress.CreatedOn;
                        //        model.MailingAddressId = connection.Insert(model.MailingAddress);
                        //    }
                        //    else
                        //    {
                        //        if (model.MailingAddress != null)
                        //        {
                        //            model.MailingAddress.LastUpdatedOn = DateTime.Now;
                        //            model.MailingAddress.CreatedOnUtc = model.MailingAddress.CreatedOn;
                        //            connection.Update(model.MailingAddress);
                        //        }
                        //    }
                        //}
                        if (model.QuoteExpiryDays == 0)
                        {
                            model.QuoteExpiryDays = 30;
                        }
                        connection.Update(model);

                        // TODO: the dapper extension is nullifying the lastupdate
                        // Need to be fixed.
                        connection.Execute(@"Update crm.franchise set LastUpdatedUtc = @lastUpdatedUtc
                                    where FranchiseId = @franchiseId"
                            , new { lastUpdatedUtc = DateTime.Now, franchiseId = franchise.FranchiseId });

                        franchise.Address = connection.Get<Address>(franchise.AddressId);

                        franchise.FranchiseOwners = connection.GetList<FranchiseOwner>("where franchiseid = @franchiseId", new { franchiseId = franchise.FranchiseId }).ToList();
                        foreach (var fowner in franchise.FranchiseOwners)
                        {
                            fowner.User = connection.Get<User>(fowner.UserId);
                            fowner.User.Address = connection.Get<Address>(fowner.User.AddressId);
                            fowner.User.Person = connection.Get<Person>(fowner.User.PersonId);
                        }

                        franchise.Users = connection.GetList<User>("where franchiseid = @franchiseId", new { franchiseId = franchise.FranchiseId }).ToList();
                        foreach (var user in franchise.Users)
                        {
                            user.Person = connection.Get<Person>(user.PersonId);
                            user.Address = connection.Get<Address>(user.AddressId);
                        }

                        if (franchise != null && model.FranchiseId != 0)
                        {
                            if (model.Address != null && model.FranchiseOwners != null)
                            {
                                // db.Entry(franchise).CurrentValues.SetValues(model); //for basic info

                                var address = franchise.Address;
                                address.Address1 = model.Address.Address1;
                                address.Address2 = model.Address.Address2;
                                address.City = model.Address.City;
                                address.State = model.Address.State;
                                address.ZipCode = model.Address.ZipCode;
                                address.CountryCode2Digits = model.Address.CountryCode2Digits;
                                address.IsValidated = model.Address.IsValidated;

                                connection.Update(address);
                                var franchiseOwners = franchise.FranchiseOwners.ToList();
                                model.FranchiseOwners = franchiseOwners;

                                foreach (var owner in model.FranchiseOwners)
                                {
                                    if (string.IsNullOrEmpty(User.UserName)) continue;
                                    var _owner = franchise.FranchiseOwners.SingleOrDefault(x => x.OwnerId == owner.OwnerId);
                                    if (_owner != null)
                                    {
                                        var person = _owner.User.Person;
                                        var noOfrowsAffected = connection.Update(person);
                                    }
                                    else
                                    {
                                        //TODO: Do we really need this???
                                        AddFranchiseOwner(owner, db);
                                    }
                                }
                            }
                            else
                            {
                                return DataCannotBeNullOrEmpty;
                            }
                            PICManager pic = new PICManager();
                            pic.CreateOrUpdatePICCustomer(CreatePICCustomerObject(model));
                            return Success;
                        }
                    }
                    connection.Close();

                    return Success;
                }
            }
            catch (ValidationException ex)
            {
                return ex.Message;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }
        }

        public override string Delete(int id)
        {
            if (id <= 0)
            {
                return DataCannotBeNullOrEmpty;
            }
            try
            {
                using (var db = ContextFactory.Create())
                {
                    var frn = db.Franchises.SingleOrDefault(x => x.FranchiseId == id);

                    if (frn != null)
                    {
                        frn.IsDeleted = true;
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }
            return Success;
        }

        public string DeleteTerritory(Territory model)
        {
            try
            {
                using (var db = ContextFactory.Create())
                {
                    var terr = db.Territories.SingleOrDefault(x => x.TerritoryId == model.TerritoryId && x.FranchiseId == model.FranchiseId);
                    if (terr != null)
                    {
                        db.Entry(terr).State = EntityState.Modified;
                        terr.FranchiseId = null;
                        terr.Name = null;
                        db.SaveChanges();
                    }
                }
            }
            catch (DbEntityValidationException e)
            {
                var err = e.EntityValidationErrors;
                return FailedByError;
            }
            catch (DbUpdateException e)
            {
                var errData = e.Data;
                var errEntries = e.Entries;
                return FailedByError;
            }
            return Success;
        }

        public List<DeactivationReason> GetDeactivationReasons()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var result = connection.GetList<DeactivationReason>().ToList();

                connection.Close();

                return result;
            }
        }

        public string GetTerritoryName(string Zipcode, string Country)
        {
            if (Zipcode.Contains('-'))
            {
                var str = Zipcode.Trim().Split('-');
                if (str.Count() > 0)
                {
                    Zipcode = str[0];
                }
            }

            if (Country.ToLower() == "us")
                Zipcode = Zipcode.Substring(0, 5);

            var BrandId = SessionManager.BrandId;

            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var ret = (string)con.ExecuteScalar("select CRM.fnGetTerritory(@BrandId,@Zipcode,@Country,@FranchiseId)", new { BrandId, Zipcode, Country, SessionManager.CurrentFranchise?.FranchiseId });
                return ret;
            }
        }

        public QBApp GetQbinfo()
        {
            if (SessionManager.CurrentFranchise == null)
                return null;

            string query = "select * from QB.Apps where FranchiseId= @FranchiseId";
            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                return con.Query<QBApp>(query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).FirstOrDefault();
            }
        }

        /// <summary>
        /// Get Franchise data
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Franchise GetFEinfo(int id)
        {
            if (id <= 0)
                throw new ArgumentOutOfRangeException("Invalid franchise id");

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var franchise = connection.Get<Franchise>(id);
                return franchise;
            }
        }

        public List<Territory> GetTerritoryByFranchiseId(int franchiseId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var result = connection.GetList<Territory>("where franchiseid = @franchiseId", new { franchiseId = franchiseId }).ToList();
                return result;
            }
        }

        public override string Add(Franchise data)
        {
            throw new NotImplementedException();
        }

        public void DisableCampaign()
        {
            string query = @"select CampaignId,EndDate,c.FranchiseId,isnull(f.TimezoneCode,1) TimezoneCode
                                , C.DeactivateOnEndDate 
                                from CRM.Campaign c
                                left join CRM.Franchise f on c.FranchiseId = f.FranchiseId
                                where c.IsActive=1 and c.IsDeleted=0 and c.EndDate is not null and c.EndDate<GETDATE()+2";
            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var camlist = con.Query<DisableCampaign>(query).ToList();

                DateTime todatDt = DateTime.UtcNow;
                foreach (var c in camlist)
                {
                    DateTime todatDtLocal = TimeZoneManager.ToLocal(todatDt, c.TimezoneCode);

                    if (todatDtLocal > c.EndDate && c.DeactivateOnEndDate)
                    {
                        string Qupdate = "update CRM.Campaign set IsActive=0 where CampaignId=@CampaignId";
                        con.Execute(Qupdate, new { c.CampaignId });
                    }
                }
            }
        }

        public Franchise GetRemorseData()
        {
            string query = "select [BuyerremorseDay],[QuoteExpiryDays],FranchiseId,Name,EnableElectronicSignFranchise from [CRM].[Franchise] where FranchiseId=@FranchiseId";
            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                return con.Query<Franchise>(query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).FirstOrDefault();
            }
        }

        public string SaveRemorse(int FranchiseId, int BuyerremorseDay, int QuoteExpiryDays, bool EnableElectSign = false)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    string query = "update [CRM].[Franchise] set BuyerremorseDay=@BuyerremorseDay, QuoteExpiryDays=@QuoteExpiryDays, EnableElectronicSignFranchise=@EnableElectSign where FranchiseId=@FranchiseId";
                    connection.Execute(query, new { BuyerremorseDay, QuoteExpiryDays, EnableElectSign, FranchiseId });
                }
                return "Success";
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }
        }

        public bool ResendEmail(int SentEmailId)
        {
            string query = "select * from history.SentEmails where SentEmailId=@SentEmailId";
            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var data = con.Query<SentEmail>(query, new { SentEmailId }).FirstOrDefault();

                //int templateEmailId = getemailtypeid(data.Subject);
                EmailType templateEmailId = (EmailType)getemailtypeid(data.Subject);

                var fromEmailAddress = emailMgr.GetFranchiseEmail(this.Franchise.FranchiseId, templateEmailId);

                List<string> toRecipients = new List<string>();
                List<string> ccAddress = new List<string>(); List<string> bccAddress = new List<string>();

                if (!string.IsNullOrEmpty(data.Recipients))
                    toRecipients = data.Recipients.Split(',').ToList();

                if (!string.IsNullOrEmpty(data.CCAddress))
                    ccAddress = data.CCAddress.Split(',').ToList();

                if (!string.IsNullOrEmpty(data.BCCAddress))
                    bccAddress = data.BCCAddress.Split(',').ToList();

                if (toRecipients.Count == 0)
                    return true;

                ExchangeManager mgr = new ExchangeManager();
                mgr.SendEmail(fromEmailAddress, data.Subject, data.Body, toRecipients, ccAddress, bccAddress);

                emailMgr.AddSentEmail(string.Join(",", toRecipients), data.Subject, data.Body, true, "", ccAddress, bccAddress, fromEmailAddress, data.LeadId, data.AccountId, data.OpportunityId, data.OrderId, data.TemplateId, null);
            }

            return true;
        }

        public int getemailtypeid(string subject)
        {
            switch (subject)
            {
                case "Thank You for Your Inquiry!":
                    return 1;

                case "Confirmation of Your Upcoming Appointment":
                    return 2;

                case "Reminder! You have an upcoming appointment":
                    return 3;

                case "Order Summary":
                    return 4;

                case "Install Schedule - What to Expect":
                    return 5;

                case "Thank You - Review":
                    return 6;

                default: return 1;
            }
        }

        public bool updateminion()
        {
            string query = "select * from CRM.Territories;";
            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var terris = con.Query<Territory>(query).ToList();
                foreach (var item in terris)
                {
                    item.Minion = item.Minion.Trim();
                    con.Update(item);
                }
            }

            return true;
        }

        public List<TaxSettings> GetTaxSettingsByFranchise(int franchiseId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
            var res = connection.Query<TaxSettings>("CRM.GetTaxSettingsByFranchiseId", new { FranchiseId = franchiseId, PersonId = User.PersonId},
             commandType: CommandType.StoredProcedure).ToList();

                    return res;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
            }
        }

        public List<Dictionary<string, int>> GetTaxTypes()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var query = @"
                                SELECT lv.Name, 
                                       lv.SortOrder AS TaxTypeId
                                FROM crm.Type_LookUpValues lv
                                     INNER JOIN CRM.Type_LookUpTable tlut ON lv.TableId = tlut.Id
                                WHERE tlut.TableName = 'TaxType'
                                ORDER BY lv.SortOrder ASC;";

                    var dictionary = new Dictionary<string, int>();

                    var res = connection.Query<Dictionary<string, int>>(query).ToList();

                    return res;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
            }
        }
        //enable or disable tax at FE Level
        public bool EnableOrDisableTax(bool taxable)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var feData = GetFEinfo(SessionManager.CurrentFranchise.FranchiseId);
                    feData.EnableTax = taxable;

                    connection.Update(feData);

                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
            }
        }

        public bool AddorUpdateTaxSettings(TaxSettings tSettings)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    tSettings.UpdatedOn = DateTime.UtcNow;
                    tSettings.UpdatedBy = SessionManager.CurrentUser.PersonId;

                    //Audit for Tax settings
                    AddTaxSettingsAudit(tSettings);

                    if (tSettings.Id > 0)
                    {
                        connection.Update<TaxSettings>(tSettings);
                    }
                    else
                    {
                        tSettings.CreatedOn = DateTime.UtcNow;
                        tSettings.CreatedBy = SessionManager.CurrentUser.PersonId;
                        connection.Insert<TaxSettings>(tSettings);
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
            }
        }
        public List<TaxSettingsAudit> GetTaxSettingsAudit()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var query = @"SELECT tsa.Id, 
                               tsa.TaxSettingsId, 
                               tsa.OldUseTaxValue, 
                               tsa.NewUseTaxValue, 
                               tsa.OldTaxSettingsJson, 
                               tsa.NewTaxSettingsJson, 
                               tsa.UseTaxChanged, 
                               tsa.UpdatedBy, 
                               tsa.UpdatedOn, 
                               (p.FirstName + ' ' + p.LastName) AS UpdatedByPerson,
                               CASE
                                   WHEN t.name IS NULL
                                   THEN 'Grey Area'
                                   ELSE t.Name
                               END AS TerritoryName
                        FROM crm.TaxSettingsAudit tsa
                             INNER JOIN crm.TaxSettings ts ON tsa.TaxSettingsId = ts.Id
                             LEFT JOIN crm.Territories t ON t.FranchiseId = ts.FranchiseId
                                                            AND ISNULL(ts.TerritoryId, 0) = ISNULL(t.TerritoryId, 0)
                             INNER JOIN crm.Person p ON p.PersonId = tsa.UpdatedBy
                        WHERE tsa.UseTaxChanged = 1
                              AND ts.FranchiseId = @FranchiseId;";



                    var res = connection.Query<TaxSettingsAudit>(query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId}).ToList();
                    connection.Close();
                    return res;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
            }
        }
        public bool AddTaxSettingsAudit(TaxSettings tSettings)
        {
            var org = GetTaxSettingsByFranchise(SessionManager.CurrentFranchise.FranchiseId).Where(x=>x.Id==tSettings.Id).FirstOrDefault();
            var audit = new TaxSettingsAudit();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    audit.TaxSettingsId = tSettings.Id;
                    audit.NewTaxSettingsJson = JsonConvert.SerializeObject(tSettings);
                    if(org.UseTaxPercentage != tSettings.UseTaxPercentage)
                    {
                        audit.NewUseTaxValue = tSettings.UseTaxPercentage;
                        audit.OldUseTaxValue = org.UseTaxPercentage;
                        audit.UseTaxChanged = true;
                    }
                    else
                    {
                        audit.NewUseTaxValue = 0;
                        audit.OldUseTaxValue = 0;
                        audit.UseTaxChanged = false;
                    }
                    audit.OldTaxSettingsJson = JsonConvert.SerializeObject(org);
                    audit.UpdatedBy = tSettings.UpdatedBy.Value;
                    audit.UpdatedOn = DateTime.UtcNow;

                    connection.Insert<TaxSettingsAudit>(audit);

                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
            }
        }

        #region Batch Job to update All FE's to PIC
        public bool SyncAllFranchiseCodeToPIC()
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    var feList = connection.GetList<Franchise>().ToList();
                    foreach (var fe in feList)
                    {
                        try
                        {
                            var franchise = connection.Get<Franchise>(fe.FranchiseId);
                            franchise.Address = connection.Get<Address>(fe.AddressId);
                            franchise.IsSuspended = false;
                            PICManager pic = new PICManager();
                            pic.CreateOrUpdatePICCustomer(CreatePICCustomerObject(franchise));
                        }
                        catch (Exception ex)
                        {
                            EventLogger.LogEvent(ex);
                            continue;
                        }

                    }
                }

            }
            catch (Exception e)
            {
                EventLogger.LogEvent(e);
                throw;
            }

            return true;

        }
        #endregion

        #region
        public List<Territory> GetTerritorydatas(int BrandId,int FranchiseId)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    List<Territory> terrexist = connection.GetList<Territory>("where BrandId=@BrandId and FranchiseId=@FranchiseId ", new { BrandId= BrandId, FranchiseId = FranchiseId }).ToList();
                    connection.Close();
                    return terrexist;
                }
            }
            catch (Exception e)
            {
                EventLogger.LogEvent(e);
                throw;
            }
        }


        public List<MonthValues> GetMonthSets()
        {
            try
            {
                string[] month = DateTimeFormatInfo.CurrentInfo.MonthNames;
                int Ids = 1; List<MonthValues> data = new List<MonthValues>();
                for (int i = 0; i < month.Length; i++)
                {
                    if (month[i] != "")
                    {
                        data.Add(new MonthValues
                        {
                            Id = Ids,
                            MonthName = month[i] + " " + "1st"
                        });
                        Ids++;
                    }
                }
                var results = data;
                return results;
            }
            catch (Exception e)
            {
                EventLogger.LogEvent(e);
                throw;
            }
        }

        public string SaveorUpdateTerritory(Territory values)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    if (values.TerritoryId != 0)
                    {
                        var query = "update CRM.Territories set MsrReportDate=@MsrReportDate where TerritoryId = @TerritoryId and FranchiseId = @FranchiseId and BrandId = @BrandId";
                        var result = connection.Query<Territory>(query,new{
                                       MsrReportDate = values.MsrReportDate,
                                       TerritoryId = values.TerritoryId,
                                       FranchiseId = values.FranchiseId,
                                       BrandId=values.BrandId
                                   });
                        connection.Close();

                    }
                }
                return "Success";
            }
            catch (Exception e)
            {
                EventLogger.LogEvent(e);
                throw;
            }
        }
        #endregion
    }
}