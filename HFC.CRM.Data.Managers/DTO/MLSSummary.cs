﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace HFC.CRM.Managers.DTO
{
    public class MLSSummary
    {
        public IList<MLSSummaryItem> MLSVendorSummaryItems { get; set; }
        public IList<MLSSummaryItem> MLSTypeSummaryItems { get; set; }
        public IList<MLSSummaryItem> MLSTaxSummaryItems { get; set; }
        public IList<MLSSummaryItem> MLSSurchargeSummaryItems { get; set; }
    }
}
