﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Managers.DTO
{
    public class MlsData
    {
        public string TerrCode { get; set; }
        public string TerrName { get; set; }
        public string CustomerName { get; set; }
        public int JobNumber { get; set; }
        public string ItemName { get; set; }
        public string ItemDetail { get; set; }
        public decimal? Subtotal { get; set; }
        public short? Quantity { get; set; }
        public string ContractedDate { get; set; }
        public decimal? Taxtotal { get; set; }
        public string ItemType { get; set; }
        public bool IsTaxable { get; set; }
        public decimal? SalePrice { get; set; }
        public decimal? UnitCost { get; set; }
        public decimal? ItemCostSubtotal { get; set; }
        public string ZipCode { get; set; }
        public decimal? ItemTaxPercent { get; set; }
        public decimal? ItemTaxAmount { get; set; }
        public decimal? TaxRateTotal { get; set; }
        public decimal? ItemSubtotal { get; set; }
    }
}
