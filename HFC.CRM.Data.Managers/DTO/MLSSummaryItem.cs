﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Managers.DTO
{
    public class MLSSummaryItem
    {
        public string ItemName { get; set; }
        public decimal Cost { get; set; }
        public decimal Retail { get; set; }
        public decimal GP { get; set; }
    }
}
