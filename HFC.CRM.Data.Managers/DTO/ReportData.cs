﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Managers.DTO
{
    public class ReportData
    {
        /// <summary>
        /// Gets or sets identifier
        /// </summary>
        public int? Id { get; set; }

        /// <summary>
        /// get or set uniquie identifer
        /// </summary>
        public Guid? GuidId { get; set; }

        /// <summary>
        /// Gets or sets unique key, such as Category Name, Employee name, etc.
        /// </summary>
        public string Key { get; set; }
        /// <summary>
        /// Gets or sets month
        /// </summary>
        public int Month { get; set; }
        /// <summary>
        /// Gets or sets year
        /// </summary>
        public int Year { get; set; }
        /// <summary>
        /// Gets or sets day
        /// </summary>
        public int Day { get; set; }
        /// <summary>
        /// Gets or sets aggregated sum total
        /// </summary>
        public decimal Sum { get; set; }
    }
}
