﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Managers.DTO
{
    public class SourceChartData
    {
        public string LeadSource { get; set; }
        public decimal Count { get; set; }
    }

}
