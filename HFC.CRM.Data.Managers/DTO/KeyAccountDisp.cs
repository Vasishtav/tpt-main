﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Managers.DTO
{
    public class KeyAccountDisp
    {
        public string CustName { get; set; }

        public string KeyAccountName { get; set; }

        public string TerrName { get; set; }

        public string TerrCode { get; set; }

        public string PaymentName { get; set; }

        public string PaymentAddr { get; set; }

        public string PaymentZip { get; set; }

        public decimal GrandTotal { get; set; }

        public decimal ApptTotal { get; set; }

        public decimal SalesTotal { get; set; }

        public decimal TotalAmount { get; set; }
    }
}
