﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Managers.DTO
{
    public class SaleAgentData
    {
        //public int? id { get; set; }
        //public string KeyName { get; set; }
        //public decimal  Count { get; set; }
        //public decimal Total { get; set; }
        //public string Percent { get; set; }


        public Nullable<int> SalesPersonid { get; set; }
        public string SalesPerson { get; set; }
        //public int Count { get; set; }
        public decimal Total { get; set; }
        //public decimal Percent { get; set; }
        public string Color { get; set; }
        public int? ColorId { get; set; }

    }
}
