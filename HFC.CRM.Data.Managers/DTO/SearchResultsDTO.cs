﻿using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Managers.DTO
{
    public  class LeadAccountDTO
    {
        public int Id { get; set; }
        public string Territory { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }

    public class VendorsDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string VendorType { get; set; }
        public string Location { get; set; }
        public string AccountRep { get; set; }
        public string RepPhone { get; set; }
        public string Status { get; set; }
        public string  OrderingMethod { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }

    }

    public class ContactsDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LeadAccountName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public bool IsLead { get; set; }
        public bool IsPrimaryCustomer { get; set; }
        public int CustomerId { get; set; }
    }

    public class OpportunityDTO
    {
        public string Territory { get;set; }
        public int OpportunityId { get; set; }
        public int OpportunityNumber { get; set; }
        public int InstallationAddressId { get; set; }
        public int AccountId { get; set; }
        public string OpportunityName { get; set; }
        public string AccountName { get; set; }
        public string Status { get; set; }
        public string SalesAgent { get; set; }
        public decimal OrderAmount { get; set; }
        public DateTime? ContractDate { get; set; }
        public int SalesAgentId { get; set; }
        public int InstallerId { get; set; }

    }

    public class AppointmentDTO
    {
        public int CalendarId { get; set; }
        public int LeadId { get; set; }
        public int AccountId { get; set; }
        public int OpportunityId { get; set; }
        public int OrderId { get; set; }
        public string Subject { get; set; }
        public string AttendeesName { get; set; }

        public string AttendeesId { get; set; }

        //public DateTime StartDate { get; set; }
        //public DateTime EndDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public string Related { get; set; }
        public string AppointmentType { get; set; }
        public bool? IsCancelled { get; set; }
        public DateTimeOffset StartDateUTC { get; set; }
        public DateTimeOffset EndDateUTC { get; set; }
    }

    public class TaskDTO
    {
        public int TaskId { get; set; }
        public int LeadId { get; set; }
        public int AccountId { get; set; }
        public int OpportunityId { get; set; }
        public int OrderId { get; set; }
        public string Subject { get; set; }
        public string AttendeesName { get; set; }
        public DateTimeOffset DueDate { get; set; }

        public string Related { get; set; }
        public string Note { get; set; }
        public DateTimeOffset? completed { get; set; }
    }

    public class QuoteDTO
    {
        public int QuoteId { get; set; }
        public int AccountId { get; set; }
        public int OpportunityId { get; set; }
        public int QuoteKey { get; set; }
        public string QuoteName { get; set; }
        public string Opportunity { get; set; }
        public string Account { get; set; }
        public string  Territory { get; set; }
        public string Sidemark { get; set; }
        public string QuoteStatus { get; set; }
        public decimal? TotalAmount { get; set; } = 0;
        public DateTime ContractedDate { get; set; }
        public DateTime? SaleDate { get; set; }
        public int SalesAgentId { get; set; }
        public int InstallerId { get; set; }

        public bool OrderExists { get; set; }
        public int QuoteStatusId { get; set; }
        public DateTime? ExpiryDate { get; set; }
    }

    public class OrderDTO
    {
        public int OrderId { get; set; }
        public int OrderNumber { get; set; }
        public int AccountId { get; set; }
        public int OpportunityId { get; set; }
        public int QuoteId { get; set; }
        public string OrderName { get; set; }
        public DateTime ContractedDate { get; set; }
        public int QuoteKey { get; set; }
        public string Account { get; set; }
        public string Opportunity { get; set; }
        public string Territory { get; set; }
        public string Sidemark { get; set; }
        public string Status { get; set; }
        public decimal? TotalAmount { get; set; } = 0;
        public int SalesAgentId { get; set; }
        public int InstallerId { get; set; }
    }

    public class PaymentDTO
    {
        public int PaymentId { get; set; }
        public int OrderId { get; set; }

        public int OrderNumber { get; set; }
        public string PaymentMethod { get; set; }
        public DateTime PaymentDate { get; set; }
        public decimal PaymentAmount { get; set; }
        public string Memo { get; set; }
        public string ReverseReason { get; set; }
        public string Authorisation { get; set; }
        public string Account { get; set; }
        public int AccountId { get; set; }
    }

    public class NoteDTO
    {
        public int NoteId { get; set; }
        public int FranchiseId { get; set; }
        public String Type { get; set; }
        public string FileName { get; set; }
        public int LeadId { get; set; }
        public int AccountId { get; set; }
        public int OpportunityId { get; set; }
        public int QuoteId { get; set; }
        public int OrderId { get; set; }
        public int OrderNumber { get; set; }
        public string Title { get; set; }
        public string  Account { get; set; }
        public string Opportunity { get; set; }
        public string Order { get; set; }
        public DateTime CreatedOn { get; set; }
        public System.Guid? AttachmentStreamId { get; set; }
        public string Categories { get; set; }
        public string CategoriesName
        {
            get
            {
                string temp = null;
                if (!string.IsNullOrEmpty(Categories))
                {
                    var typeenums = Categories.Split(',').Select(int.Parse).ToArray();
                    foreach (var item in typeenums)
                    {
                        var x = (NoteTypeEnum)item;
                        temp += temp == null ? x.GetDisplayName() : ", " + x.GetDisplayName();
                    }
                }

                return temp;
            }
        }

    }

    public class MpoVpoDTO
    {
        public int Mpo { get; set; }
        public int Vpo { get; set; }
        public string VendorName { get; set; }
        public string AccountName { get; set; }
        public int OrderNumber { get; set; }
        public DateTime MpoDate { get; set; }
        public DateTime VpoDate { get; set; }
        public DateTime PromiseDate { get; set; }
        public string VpoStatus { get; set; }
        public string Territory { get; set; }
        public int VendorId { get; set; }
        public int PurchaseOrderId { get; set; }
        public int StatusId { get; set; }
        public int AccountId { get; set; }
        public int OrderId { get; set; }
        public int PICPO { get; set; }
        public string OpportunityName { get; set; }
        public int OpportunityId { get; set; }
        public int SalesAgentId { get; set; }
        public int InstallerId { get; set; }
    }

    public class ShipmentDTO
    {
        public int ShipNoticeId { get; set; }
        public int ShipmentId { get; set; }
        public DateTime ShippedDate { get; set; }
        public string VendorName { get; set; }
        public int OrderId { get; set; }
        public string SideMark { get; set; }
        public int Mpo { get; set; }
        public int Vpo { get; set; }
        public int VendorId { get; set; }
        public int POLines { get; set; }
        public int POQty { get; set; }
        public int QtyShipped { get; set; }
        public int QtyReceived { get; set; }
        public int PurchaseOrderId { get; set; }
        public int OrderNumber { get; set; }

    }

    public class CaseDTO
    {
        public int CaseId { get; set; }
        public int CaseNumber { get; set; }
        public int CaseLineNumber { get; set; }
        public int Mpo { get; set; }
        public int Vpo { get; set; }
        public int VendorId { get; set; }
        public string VendorName { get; set; }
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public DateTime DateOpened { get; set; }
        public int PICPO { get; set; }
        public int PurchaseOrderId { get; set; }
        public int VendorCaseNumber { get; set; }
    }

    public class AddressDTO
    {
        public int AddressId { get; set; }
        public string AttentionText { get; set; }
        public string Contact { get; set; }
        public int ContactId { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string CrossStreet { get; set; }

        public int? LeadId { get; set; }
        public int? AccountId { get; set; }
        public bool IsPrimaryAddress { get; set; }
        
    }

}
