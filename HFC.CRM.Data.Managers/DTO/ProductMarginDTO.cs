﻿namespace HFC.CRM.Managers.DTO
{
    using System;

    public class ProductMarginDTO
    {
        public int ManufacturerId { get; set; }

        public Guid ProductGuid { get; set; }

        public string ProductName { get; set; }

        public double Multiplier { get; set; }
    }
}