﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Managers.DTO
{
    public class SalesBetaMonthlyStatisticsData
    {
        public string Month { get; set; }

        public int NumberOfLeads { get; set; }

        public int ValidLeads { get; set; }

        public int LostSale { get; set; }

        public int NumberOfSales { get; set; }

        public decimal TotalSales { get; set; }

        public decimal ConversionRate { get; set; }

        public decimal ClosingRate { get; set; }

        public decimal AvgSale { get; set; }

        public decimal RevenuePerLead { get; set; }

        public int TotalNumQuotes { get; set; }

        public decimal TotalQuotes { get; set; }

        public decimal AvgQuote { get; set; }

        public decimal QuoteCloseRate { get; set; }


        public string StartDate { get; set; }

        public string EndDate { get; set; }
    }
}
