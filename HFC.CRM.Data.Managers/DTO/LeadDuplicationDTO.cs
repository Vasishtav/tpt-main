﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Managers.DTO
{
    public  class LeadDuplicationDTO
    {
        /// <summary>
        /// If contains value the lead is called for validation while editing an existing
        /// Lead, otherwise it is a new lead
        /// </summary>
        public int LeadId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Email { get; set; }
        public string CellPhone { get; set; }
        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public string PreferredTFN { get; set; }
    }

    public class LeadDuplicationResultDTO
    {
        public string Result { get; set; }
        public string ErrorMessage { get; set; }
    }
}
