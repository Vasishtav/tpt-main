﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Managers.DTO
{
    public class MSRData
    {
        /// <summary>
        /// Gets or sets category id
        /// </summary>
        public int? Category_Type_Id { get; set; }
        /// <summary>
        /// Gets or sets category name
        /// </summary>
        public string Category_Type { get; set; }
        /// <summary>
        /// Gets or sets territory id
        /// </summary>
        public int TerritoryId { get; set; }
        /// <summary>
        /// Gets or sets cost
        /// </summary>
        public decimal Cost { get; set; }
        /// <summary>
        /// Gets or sets sales
        /// </summary>
        public decimal Sales { get; set; }
        /// <summary>
        /// Gets or sets aggregated sum total
        /// </summary>
        public decimal? Sum { get; set; }
    }
}
