﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Managers.DTO
{
    public class MarketingData
    {
        /// <summary>
        /// Gets or sets source id
        /// </summary>
        public int? SourceId { get; set; }
        /// <summary>
        /// Gets or sets source name
        /// </summary>
        public string SourceName { get; set; }
        /// <summary>
        /// Gets or sets sales total amount
        /// </summary>
        public decimal? TotalSales { get; set; }
        /// <summary>
        /// Gets or sets total sales count
        /// </summary>
        public int TotalSalesCount { get; set; }
        /// <summary>
        /// Gets or sets total item count
        /// </summary>
        public int TotalCount { get; set; }
        /// <summary>
        /// Gets or sets total expense amount
        /// </summary>
        public decimal? TotalExpense { get; set; }
        /// <summary>
        /// Gets or sets ranking
        /// </summary>
        public int Ranking { get; set; }
    }
}
