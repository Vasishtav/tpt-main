﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Managers.DTO
{
    public class SalesData
    {
        /// <summary>
        /// Gets or sets _leadCreated id
        /// </summary>
        public int LeadId { get; set; }
        /// <summary>
        /// Gets or sets job id
        /// </summary>
        public int JobId { get; set; }
        /// <summary>
        /// Gets or sets job number
        /// </summary>
        public int JobNumber { get; set; }
        /// <summary>
        /// Gets or sets job status id
        /// </summary>
        public int JobStatusId { get; set; }
        /// <summary>
        /// Gets or sets unique key id, such as categoryId, product type id, sales person id, etc.
        /// </summary>
        /// 
        public string JobStatus { get; set; }
        public int? KeyId { get; set; }
        /// <summary>
        /// Gets or sets unique guid key id for products
        /// </summary>
        public Guid? GuidKeyId { get; set; }
        /// <summary>
        /// Gets or sets unique key name, such as category name, product type, sales person name
        /// </summary>
        public string KeyName { get; set; }
        /// <summary>
        /// Gets or sets zip code
        /// </summary>
        public string ZipCode { get; set; }
        /// <summary>
        /// Gets or sets city
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// Gets or sets state
        /// </summary>
        public string State { get; set; }
        /// <summary>
        /// Gets or sets customer first name
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Gets or sets customer last name
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// get Job Count as fraction for allocate all
        /// </summary>
        public decimal JobCount { get; set; }
        /// <summary>
        /// Gets or sets subtotal
        /// </summary>
        public decimal Subtotal { get; set; }
        /// <summary>
        /// Gets or sets discount total
        /// </summary>
        public decimal DiscountTotal { get; set; }
        /// <summary>
        /// Gets or sets surcharge total
        /// </summary>
        public decimal SurchargeTotal { get; set; }

        private DateTime? _contractDate;
        /// <summary>
        /// Gets or sets contracted date aka 'sales date'
        /// </summary>
        public DateTime? ContractedOnUtc
        {
            get { return _contractDate; }
            set
            {
                if (value.HasValue)
                    _contractDate = DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified);
                else
                    _contractDate = null;
            }
        }


        private DateTime? _createdDate;
        /// <summary>
        /// Gets or sets created date
        /// </summary>
        public DateTime? CreatedOnUtc
        {
            get { return _createdDate; }
            set
            {
                if (value.HasValue)
                    _createdDate = DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified);
                else
                    _createdDate = null;
            }
        }

        /// <summary>
        /// Gets or sets first appointment date
        /// </summary>
        public DateTimeOffset? FirstAppointment { get; set; }
    }
}
