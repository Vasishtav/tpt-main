﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Managers.DTO
{
    public class SalesBetaMarketingPerformanceData
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int SourceId { get; set; }
        public int ParentSourceId { get; set; }
        public string Source { get; set; }
        public string Campaign { get; set; }
        public decimal TotalLeads { get; set; }
        public decimal TotalValidLeads { get; set; }
        public decimal NumberOfSales { get; set; }
        public decimal Sales { get; set; }
        public decimal ConversionRate { get; set; }
        public decimal ClosingRate { get; set; }
        public decimal AveSale { get; set; }
        public decimal RevenuePerLead { get; set; }
        public decimal TotalNumQuotes { get; set; }
        public decimal TotalQuotes { get; set; }
        public decimal AvgQuote
        {
            get
            {

                if (TotalNumQuotes != 0)
                {
                    return TotalQuotes / TotalNumQuotes;
                }
                else
                {
                    return TotalQuotes;
                }

            }
        }

        public decimal QuoteToClose { get; set; }
        public decimal CampaignCost { get; set; }
        public decimal CostPerLead { get; set; }
        public decimal CostPerSale { get; set; }
        public decimal ROI { get; set; }
        public decimal TotalValidLeadsNoFactor { get; set; }

    }
}
