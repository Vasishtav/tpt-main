﻿namespace HFC.CRM.Managers.DTO
{
    public class AppTypeColorModel
    {
        public string Appointment1 { get; set; }
        public string Appointment2 { get; set; }
        public string Appointment3 { get; set; }
        public string Appointment4 { get; set; }
        public string Sales_Design { get; set; }
        public string Installation { get; set; }
        public string DayOff { get; set; }
        public string Other { get; set; }
        public string Personal { get; set; }
        public string Meeting_Training { get; set; }
        public string Service { get; set; }
        public string Vacation { get; set; }
        public string Holiday { get; set; }
        public string Followup { get; set; }
        public string TimeBlock { get; set; }
        public bool EnableColor { get; set; }

        public bool EnableBorders { get; set; }
        public bool Enable { get; set; }
    }
}