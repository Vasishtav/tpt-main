﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Managers.DTO
{
  public  class PrintInputDTO
    {
        public int NoteId { get; set; }
       
        public string Title { get; set; }

        public string PrintItemId {
            get
            {
                return "mydocuments_" + NoteId;
            }
        }

        public int Quantity { get; set; }
        public bool Selected { get; set; }

        public string stream_id { get; set; }
    }
}
