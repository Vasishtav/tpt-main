﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Managers.DTO
{
    public class SalesBetaSalesAgentData
    {
        public string SalesAgent { get; set; }

        public int? SalesPersonId { get; set; }
        public int? NumValidLeads { get; set; }
        public int? NumOpenJobs { get; set; }
        public int? NumLostJobs { get; set; }
        public int? NumSales { get; set; }
        public decimal? Sales { get; set; }
        public decimal? ClosingRate { get; set; }
        public decimal? AvgSale { get; set; }

        public string StartData { get; set; }

        public string EndData { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }
    }
}
