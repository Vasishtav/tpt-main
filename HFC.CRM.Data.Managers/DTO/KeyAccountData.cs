﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Managers.DTO
{
    public class KeyAccountData
    {
        /// <summary>
        /// Gets or sets CustName
        /// </summary>
        public string CustName { get; set; }
        /// <summary>
        /// Gets or sets Key Account name
        /// </summary>
        public string KAName { get; set; }
        /// <summary>
        /// Gets or sets PayByAppointment
        /// </summary>
        public decimal PayByAppointment { get; set; }
        /// <summary>
        /// Gets or sets ApptCount
        /// </summary>
        public int ApptCount { get; set; }
        /// <summary>
        /// Gets or sets CorporateAmount
        /// </summary>
        public decimal CorporateAmount { get; set; }
        /// <summary>
        /// Gets or sets CustPrice
        /// </summary>
        public decimal CustPrice { get; set; }
        /// <summary>
        /// Gets or sets AddChgs
        /// </summary>
        public decimal AddChgs { get; set; }
        /// <summary>
        /// Gets or sets PayByPercentOfSale
        /// </summary>
        public decimal PayByPercentOfSale { get; set; }
        /// <summary>
        /// Gets or sets Key TerrName
        /// </summary>
        public string TerrName { get; set; }
        /// <summary>
        /// Gets or sets Key TerrCode
        /// </summary>
        public string TerrCode { get; set; }
        /// <summary>
        /// Gets or sets PayMethodType
        /// </summary>
        public int PayMethodType { get; set; }
        /// <summary>
        /// Gets or sets Key KAAddress
        /// </summary>
        public string KAAddress { get; set; }
        /// <summary>
        /// Gets or sets Key KACSZ
        /// </summary>
        public string KACSZ { get; set; }
        /// <summary>
        /// Gets or sets Key Storename
        /// </summary>
        public string Storename { get; set; }
        /// <summary>
        /// Gets or sets Key KALocAddress
        /// </summary>
        public string KALocAddress { get; set; }
        /// <summary>
        /// Gets or sets Key KALocCSZ
        /// </summary>
        public string KALocCSZ { get; set; }
        /// <summary>
        /// Gets or sets LeadCount
        /// </summary>
        public int LeadCount { get; set; }
        /// <summary>
        /// Gets or sets PayByLead
        /// </summary>
        public decimal PayByLead { get; set; }
        /// <summary>
        /// Gets or sets SaleCount
        /// </summary>
        public int SaleCount { get; set; }
        /// <summary>
        /// Gets or sets PayBySale
        /// </summary>
        public decimal PayBySale { get; set; }

        public decimal ApptTotal { get; set; }

        /// <summary>
        /// Gets and Calculate SalesTotal
        /// </summary>
        public decimal SalesTotal { get; set; }

        public decimal Total { get; set; }
    }
}
