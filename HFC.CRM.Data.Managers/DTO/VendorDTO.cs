﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Managers.DTO
{
  public  class VendorDTO
    {
        public int VendorId { get; set; }
        public System.Guid VendorGuid { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Phone { get; set; }
        public string AltPhone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public Nullable<int> ReferenceId { get; set; }
        public string ReferenceNum { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }
}
