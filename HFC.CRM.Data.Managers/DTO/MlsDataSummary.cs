﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Managers.DTO
{

    public class MlsDataSummary : MlsData
    {
        public string Vendor { get; set; }
    }
}
