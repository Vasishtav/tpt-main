﻿using System;
using System.Collections.Generic;
using System.Linq;
using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Data.Constants;
using Dapper;
using HFC.CRM.Data.Context;
using System.Data.SqlClient;
using HFC.CRM.Managers.DTO;

/// <summary>
/// The Managers namespace.
/// </summary>
namespace HFC.CRM.Managers
{
    /// <summary>
    /// Class CalendarManager.
    /// </summary>
    public class PrintManager : IMessageConstants
    {
        /// <summary>
        /// The CRMDB context
        /// </summary>
        private CRMContext CRMDBContext = ContextFactory.Current.Context;

        /// <summary>
        /// Gets the user.
        /// </summary>
        /// <value>The user.</value>
        public User User { get; private set; }
        /// <summary>
        /// Gets the authorizing user.
        /// </summary>
        /// <value>The authorizing user.</value>
        public User AuthorizingUser { get; private set; }

        /// <summary>
        /// Create a new instance of calendar manager to handle CRUD operations
        /// </summary>
        /// <param name="user">The user that is performing the CRUD operation and used to check authorization permission</param>
        public PrintManager(User user) : this(user, user)
        {
            
        }

        /// <summary>
        /// Create a new instance of calendar manager to handle CRUD operations
        /// </summary>
        /// <param name="authorizingUser">Used to check authorization permission</param>
        /// <param name="user">The user that is performing the CRUD operation</param>
        /// <exception cref="System.ArgumentOutOfRangeException">User must belong to a franchise</exception>
        public PrintManager(User authorizingUser, User user)
        {
            this.User = user;
            this.AuthorizingUser = authorizingUser;
            if (!this.User.FranchiseId.HasValue || this.User.FranchiseId.Value <= 0)
                throw new ArgumentOutOfRangeException("User must belong to a franchise");
        }

        public List<PrintInputDTO> GetMydocumentsForSales()
        {
            var query = @"select n.NoteId, n.Title from crm.note n 
                         join FileTableTb ft on n.AttachmentStreamId = ft.stream_id
                         join crm.notescategories ng on ng.noteid = n.NoteId and ng.typeenum = 10
                         where 
                            --n.TypeEnum = 10 and
                             n.IsEnabled = 1
	                        and ft.file_type = 'pdf'
	                        and n.FranchiseId = @franchiseId";

            var result = new List<PrintInputDTO>();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                 result = connection.Query<PrintInputDTO>(query,
                    new {franchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();

                connection.Close();
            }
            return result;
        }

        public List<PrintInputDTO> GetMydocumentsForInstall()
        {
            var query = @"select n.NoteId, n.Title from crm.note n 
                         join FileTableTb ft on n.AttachmentStreamId = ft.stream_id
                        join crm.notescategories ng on ng.noteid = n.NoteId and ng.typeenum = 12
                         where 
                            --n.TypeEnum = 12 and 
                            n.IsEnabled = 1
	                        and ft.file_type = 'pdf'
	                        and n.FranchiseId = @franchiseId";

            var result = new List<PrintInputDTO>();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<PrintInputDTO>(query,
                   new { franchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();

                connection.Close();
            }
            return result;
        }
    }
}

