﻿using Dapper;
using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Membership;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using HFC.CRM.Data.Geolocator;
using HFC.CRM.DTO.Product;
using HFC.CRM.DTO.Vendors;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Managers
{
    public class LookupManager : DataManager
    {
        QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        /// <summary>
        /// Fetches list of Product of type Myproduct and Service.
        /// </summary>
        /// <returns></returns>
        public List<FranchiseProducts> GetProdcustTL()
        {
            var prdList = ExecuteIEnumerableObject<FranchiseProducts>(ContextFactory.CrmConnectionString,
               @"SELECT * FROM crm.FranchiseProducts fp WHERE fp.ProductStatus =1 AND  fp.FranchiseId =@FranchiseId",
               new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();

            return prdList;
        }
        public List<FranchiseDiscount> GetDiscountTL()
        {
            var discList = ExecuteIEnumerableObject<FranchiseDiscount>(ContextFactory.CrmConnectionString,
                @"SELECT * FROM crm.FranchiseDiscount fp WHERE fp.Status =1 AND   fp.FranchiseId =@FranchiseId",
                new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();

            return discList;
        }

        public List<Type_ProductCategory> GetMyProductCategories()
        {
            string query = "";
            if (SessionManager.BrandId == 2 || SessionManager.BrandId == 3)
            {
                query = @" SELECT tpc.ProductCategoryId, tpc.ProductCategory
                        , tpc.Parant , tpc.ProductTypeId
                        FROM crm.Type_ProductCategory tpc
                        WHERE tpc.producttypeid = 1 AND tpc.brandid = @brandid 
                        and IsActive = 1 and IsDeleted = 0 
                        union
                        SELECT tpc.ProductCategoryId, tpc.ProductCategory
                        , tpc.Parant, tpc.ProductTypeId
	                    FROM crm.Type_ProductCategory tpc
	                    WHERE (producttypeid = 2 or producttypeid = 3)  
                        AND tpc.brandid = @brandid and tpc.Parant = 0";
            }
            else
            {
                query = @" SELECT tpc.ProductCategoryId, tpc.ProductCategory, tpc.Parant 
                    FROM crm.Type_ProductCategory tpc
                    WHERE tpc.producttypeid= 2 AND tpc.brandid = @brandid 
                    and IsActive = 1 and IsDeleted = 0 ";
            }

            var NameList = ExecuteIEnumerableObject<Type_ProductCategory>(ContextFactory.CrmConnectionString,
                query,
                new { brandid = SessionManager.BrandId }).ToList();


            var prdList = ExecuteIEnumerableObject<FranchiseProducts>(ContextFactory.CrmConnectionString,
                @"SELECT * FROM crm.FranchiseProducts fp WHERE fp.ProductStatus =1 AND  fp.FranchiseId =@FranchiseId",
                new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();

            var discList = ExecuteIEnumerableObject<FranchiseDiscount>(ContextFactory.CrmConnectionString,
                @"SELECT * FROM crm.FranchiseDiscount fp WHERE fp.Status =1 AND  fp.FranchiseId =@FranchiseId",
                new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();

            //if (prdList != null && prdList.Where(x => x.ProductType == 2).Count() > 0)
            //{
            //    var myprd = new Type_ProductCategory
            //    {
            //        ProductCategoryId = 99997,
            //        ProductCategory = "My Product",
            //        Parant = 0
            //    };
            //    NameList.Add(myprd);
            //}
            //if (discList != null)
            //{
            //    foreach (var item in discList)
            //    {
            //        var disc = new FranchiseProducts();

            //        disc.Description = item.Description;
            //        disc.Discount = item.DiscountValue;
            //        disc.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
            //        disc.ProductCategory = 99990;
            //        disc.ProductID = item.DiscountId;
            //        disc.ProductKey = item.DiscountIdPk;
            //        disc.ProductName = item.Name;
            //        disc.

            //    }
            //}

            var myprd = new Type_ProductCategory
            {
                ProductCategoryId = 99997,
                ProductCategory = "My Product",
                Parant = 0,
                ProductTypeId = 2
            };
            NameList.Add(myprd);


            //if (prdList != null && prdList.Where(x => x.ProductType == 3).Count() > 0)
            //{
            //    var services = new Type_ProductCategory
            //    {
            //        ProductCategoryId = 99998,
            //        ProductCategory = "Service",
            //        Parant = 0
            //    };
            //    NameList.Add(services);
            //}
            var services = new Type_ProductCategory
            {
                ProductCategoryId = 99998,
                ProductCategory = "Service",
                Parant = 0,
                ProductTypeId = 3
            };
            NameList.Add(services);


            //if (discList != null && discList.Count() > 0)
            //{
            //    var Discount = new Type_ProductCategory
            //    {
            //        ProductCategoryId = 99999,
            //        ProductCategory = "Discount",
            //        Parant = 0
            //    };
            //    NameList.Add(Discount);
            //}
            var discount = new Type_ProductCategory
            {
                ProductCategoryId = 99999,
                ProductCategory = "Discount",
                Parant = 0,
                ProductTypeId = 4
            };
            NameList.Add(discount);

            ////////Generating the dropdown for My products services and Discount
            ////if (prdList != null && prdList.Count>0)
            ////{

            ////    foreach (var item in prdList)
            ////    {
            ////        if (item.ProductType == 2)
            ////        {
            ////            NameList.Add(new Type_ProductCategory
            ////            {
            ////                ProductCategoryId = item.ProductKey,
            ////                ProductCategory = item.ProductName,
            ////                Parant = 99997
            ////            });
            ////        }
            ////        if (item.ProductType == 3)
            ////        {
            ////            NameList.Add(new Type_ProductCategory
            ////            {
            ////                ProductCategoryId = item.ProductKey,
            ////                ProductCategory = item.ProductName,
            ////                Parant = 99998
            ////            });
            ////        }
            ////    }
            ////}


            //////making a discount list
            ////if (discList != null && discList.Count>0)
            ////{
            ////    foreach (var item in discList)
            ////    {
            ////        NameList.Add(new Type_ProductCategory
            ////        {
            ////            ProductCategoryId = item.DiscountId,
            ////            ProductCategory = item.Name,
            ////            Parant = 99999
            ////        });
            ////    }
            ////}

            return NameList;
        }

        public List<Type_ProductCategory> GetProductList(int id, int drpvalue)
        {
            var BrandId = SessionManager.BrandId;
            var PoductList = GetListData<Type_ProductCategory>(ContextFactory.CrmConnectionString, "where Parant = @Parant and ProductTypeId = @ProductTypeId and BrandId=@BrandId order by ProductCategory asc", new { Parant = 0, ProductTypeId = drpvalue, BrandId = BrandId }).ToList();
            return PoductList;
            //if (drpvalue == 3)
            //{
            //    var PoductList = GetListData<Type_ProductCategory>(ContextFactory.CrmConnectionString, "where Parant = @Parant and ProductTypeId = @ProductTypeId and BrandId=@BrandId", new { Parant = 0, ProductTypeId = drpvalue, BrandId= BrandId }).ToList();
            //    return PoductList;
            //}
            //else
            //{
            //    var PoductList = GetListData<Type_ProductCategory>(ContextFactory.CrmConnectionString, "where Parant = @Parant and BrandId=@BrandId", new { Parant = 0, BrandId= BrandId }).ToList();
            //    return PoductList;
            //}

        }

        public List<Type_ProductCategory> GetProductListforAdmin(int id, int drpvalue)
        {
            var BrandId = id;
            var PoductList = GetListData<Type_ProductCategory>(ContextFactory.CrmConnectionString, "where Parant = @Parant and ProductTypeId = @ProductTypeId and BrandId=@BrandId order by ProductCategory asc", new { Parant = 0, ProductTypeId = drpvalue, BrandId = BrandId }).ToList();
            return PoductList;
        }

        public List<Type_ProductCategory> GetSubCategoryList(int id)
        {
            var BrandId = SessionManager.BrandId;
            var SubList = GetListData<Type_ProductCategory>(ContextFactory.CrmConnectionString, "where Parant = @Parant and BrandId=@BrandId", new { BrandId = BrandId, Parant = id }).ToList();
            return SubList;
        }

        public List<Type_ProductCategory> GetSubCategoryListforAdmin(int id, int drpvalue)
        {
            var BrandId = id;
            var SubList = GetListData<Type_ProductCategory>(ContextFactory.CrmConnectionString, "where Parant = @Parant and BrandId=@BrandId", new { BrandId = BrandId, Parant = drpvalue }).ToList();
            return SubList;
        }
        public List<Type_ProductStatus> GetStatus()
        {
            var Status = ExecuteIEnumerableObject<Type_ProductStatus>(ContextFactory.CrmConnectionString, "select * from [CRM].[Type_ProductStatus]").ToList();
            return Status;
        }

        public List<ShipToLocationsMap> GetLOVShip(int id, int TerritoryValue)
        {
            //var NameList = ExecuteIEnumerableObject<ShipToLocationsMap>(ContextFactory.CrmConnectionString,
            //               @"select *,ad.Address1+','+ad.Address2+','+ad.City+','+ad.State+','+ad.ZipCode as Addressvalue 
            //                from [CRM].[ShipToLocationsMap] slm
            //                join [CRM].[ShipToLocations] sl on slm.ShipToLocation=sl.Id
            //                left join CRM.Addresses ad on ad.AddressId= sl.AddressId  
            //                where TerritoryId=@TerritoryId and sl.Active=1", 
            var NameList = ExecuteIEnumerableObject<ShipToLocationsMap>(ContextFactory.CrmConnectionString,
                        @"select *,ad.Address1,ad.Address2,ad.City,ad.State,ad.ZipCode 
                        from [CRM].[ShipToLocationsMap] slm
                        join [CRM].[ShipToLocations] sl on slm.ShipToLocation=sl.Id
                        left join CRM.Addresses ad on ad.AddressId= sl.AddressId  
                        where TerritoryId=@TerritoryId and sl.Active=1",
                       new
                       {
                           FranchiseId = SessionManager.CurrentFranchise.FranchiseId,
                           TerritoryId = TerritoryValue,
                       }).ToList();
            return NameList;
        }

        public List<ProductListNameModel> GetProductVendorname(int id)
        {
            if (SessionManager.CurrentFranchise != null)
            {
                if (id == 2)
                {
                    var NameList = ExecuteIEnumerableObject<ProductListNameModel>(ContextFactory.CrmConnectionString,
                    @" select hv.VendorIdPk,hv.VendorId, hv.Name,hv.VendorType from Acct.HFCVendors hv
                   join Acct.FranchiseVendors fv on hv.VendorId=fv.VendorId
                   where FranchiseId=@FranchiseId and fv.VendorStatus=1 and (hv.IsAlliance=0 or hv.IsAlliance is null)
                   union
                   select VendorIdPk,VendorId,Name,VendorType from Acct.FranchiseVendors where FranchiseId=@FranchiseId and 
                   VendorType=3 and VendorStatus=1
                   Order by Name asc", new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                    return NameList;
                }
                else if (id == 1)
                {
                    var NameLists = ExecuteIEnumerableObject<ProductListNameModel>(ContextFactory.CrmConnectionString,
                        @"select hv.VendorIdPk,hv.VendorId, hv.Name from Acct.HFCVendors hv
                      join Acct.FranchiseVendors fv on hv.VendorId=fv.VendorId
                      where FranchiseId=@FranchiseId and fv.VendorStatus=1 and fv.VendorType=1
                      Order by Name asc", new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                    return NameLists;
                }
            }
            else
            {
                List<Role> roles = SessionManager.CurrentUser.Roles.ToList();
                if (roles.FindAll(delegate (Role role) { return role.RoleName == "Global Admin"; }).Count != 0)
                {
                    if (id == 2)
                    {
                        var NameList = ExecuteIEnumerableObject<ProductListNameModel>(ContextFactory.CrmConnectionString,
                        @" select hv.VendorIdPk,hv.VendorId, hv.Name,hv.VendorType from Acct.HFCVendors hv
                   join Acct.FranchiseVendors fv on hv.VendorId=fv.VendorId
                   where fv.VendorStatus=1 and (hv.IsAlliance=0 or hv.IsAlliance is null)
                   union
                   select VendorIdPk,VendorId,Name,VendorType from Acct.FranchiseVendors where 
                   VendorType=3 and VendorStatus=1
                   Order by Name asc").ToList();
                        return NameList;
                    }
                    else if (id == 1)
                    {
                        var NameLists = ExecuteIEnumerableObject<ProductListNameModel>(ContextFactory.CrmConnectionString,
                            @"select hv.VendorIdPk,hv.VendorId, hv.Name from Acct.HFCVendors hv
                      join Acct.FranchiseVendors fv on hv.VendorId=fv.VendorId
                      where fv.VendorStatus=1 and fv.VendorType=1
                      Order by Name asc").ToList();
                        return NameLists;
                    }
                }
            }
            return null;
            //var NameLists= ExecuteIEnumerableObject<ProductListNameModel>(ContextFactory.CrmConnectionString,
            //    @" select hv.VendorIdPk,hv.VendorId, hv.Name from Acct.HFCVendors hv
            //       join Acct.FranchiseVendors fv on hv.VendorId=fv.VendorId
            //       where FranchiseId=@FranchiseId and fv.VendorStatus=1
            //       union
            //       select VendorIdPk,VendorId,Name from Acct.FranchiseVendors where FranchiseId=@FranchiseId and 
            //       VendorType=3 and VendorStatus=1
            //       Order by Name asc", new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
            //return NameLists;
        }
        public VendorNameListModel GetVendName(string value)
        {
            var VendorNameList = ExecuteIEnumerableObject<VendorNameListModel>(ContextFactory.CrmConnectionString, @"select VendorIdPk,VendorId,Name,VendorType from [Acct].[HFCVendors] where Name=@Name",
                                                                               new { Name = value }).FirstOrDefault();
            if (VendorNameList != null)
            {
                var list = ExecuteIEnumerableObject<VendorNameListModel>(ContextFactory.CrmConnectionString, @"select * from Acct.FranchiseVendors where VendorId=@VendorId and FranchiseId=@FranchiseId",
                                                                            new { VendorId = VendorNameList.VendorID, FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).FirstOrDefault();
                if (list != null)
                {
                    return list;
                }
                else
                {
                    return VendorNameList;
                }
            }


            return null;
        }

        public ProductListNameModel GetVendor(int id)
        {
            ProductListNameModel VendorType = null;
            if (SessionManager.CurrentFranchise != null)
            {
                VendorType = ExecuteIEnumerableObject<ProductListNameModel>(ContextFactory.CrmConnectionString, @"select VendorType from [Acct].[FranchiseVendors] where VendorId=@Id and FranchiseId=@FranchiseId",
                                                                               new { Id = id, FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).FirstOrDefault();
            }
            else
            {
                List<Role> roles = SessionManager.CurrentUser.Roles.ToList();
                if (roles.FindAll(delegate (Role role) { return role.RoleName == "Global Admin"; }).Count != 0)
                {
                    VendorType = ExecuteIEnumerableObject<ProductListNameModel>(ContextFactory.CrmConnectionString, @"select distinct VendorType from [Acct].[FranchiseVendors] where VendorId=@Id",
                                                                                  new { Id = id }).FirstOrDefault();
                }
            }

            return VendorType;
        }

        public VendorNameListModel GetEditVendName(string value)
        {
            var VendorNameList = ExecuteIEnumerableObject<VendorNameListModel>(ContextFactory.CrmConnectionString, @"select VendorIdPk,VendorId,Name,VendorType from [Acct].[HFCVendors] where Name=@Name",
                                                                               new { Name = value }).FirstOrDefault();
            return VendorNameList;
        }
        public VendorNameListModel GetLocalvalue(string value)
        {
            var VendorLocalNameList = ExecuteIEnumerableObject<VendorNameListModel>(ContextFactory.CrmConnectionString, @"select * from Acct.FranchiseVendors where Name=@Name and FranchiseId=@FranchiseId",
                                                                              new { Name = value, FranchiseId = SessionManager.CurrentUser.FranchiseId }).FirstOrDefault();
            return VendorLocalNameList;
        }

        public List<VendorStatusModel> GetVendorStatus()
        {

            var VendorStatusList = ExecuteIEnumerableObject<VendorStatusModel>(ContextFactory.CrmConnectionString, @"select * from CRM.Type_VendorStatus").ToList();
            return VendorStatusList;
        }
        public VendorCurrencyModel GetCurrency(string value)
        {
            var Currencydata = ExecuteIEnumerableObject<VendorCurrencyModel>(ContextFactory.CrmConnectionString, @"select CountryCodeId,ISOCode2Digits,Country,CurrencyCode from CRM.Type_CountryCodes where CurrencyCode is not null and ISOCode2Digits=@value",
                                                                               new { value = value }).FirstOrDefault();
            return Currencydata;
        }

        public List<ShipToLocationsMap> GetLOVShipBP(int id)
        {
            var NameList = ExecuteIEnumerableObject<ShipToLocationsMap>(ContextFactory.CrmConnectionString,
                        @"Select sl.Id,sl.ShipToLocation, sl.AddressId,sl.ShipToName, ad.Address1,ad.Address2,ad.City,ad.State,ad.ZipCode 
                        from [CRM].[ShipToLocations] sl
                        left join CRM.Addresses ad on ad.AddressId= sl.AddressId  
                        where sl.FranchiseId=@FranchiseId and sl.Active=1",
                       new
                       {
                           FranchiseId = SessionManager.CurrentFranchise.FranchiseId
                       }).ToList();
            return NameList;
        }

        public dynamic Type_LookUpValues(int tableId)
        {
            LookupManager Lookup_manager = new LookupManager();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = @"select * from [CRM].[Type_LookUpValues] where TableId =@tableId ORDER BY  CASE WHEN Name = 'Other' THEN    1 ELSE null END,name ASC;";
                var result = connection.Query(query, new { tableId = tableId }).ToList();
                return result;
            }
        }

        public dynamic Type_DateTime(int Tableid)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = @"select * from [CRM].[Type_LookUpValues] where TableId =@tableId";
                var result = connection.Query(query, new { tableId = Tableid }).Skip(5).ToList();
                return result;
            }
        }


        public dynamic GetDispositionsTP(int dispositionid)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = "select * from crm.disposition ";
                if (dispositionid > 0) query += "where dispositionid = @dpid";
                var result = connection.Query<Disposition>(query, new { dpid = dispositionid }).ToList();
                return result;

            }
        }

        public dynamic Campaigns()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = @"select CampaignId, name from crm.campaign 
                                where ISNULL(isActive,0) = 1 and FranchiseId = @FranchiseId
                                order by name ";
                var result = connection.Query<Campaign>(query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                return result;
            }
        }


        public dynamic SourceTp(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = @"select stp.SourcesTPId as SourceId, s.Name as SourceName
                            ,c.Name as ChannelName, c.Name + ' - ' + s.Name as name
                            , o.name as ownerName
                            from crm.SourcesTP stp
                            join crm.Type_Channel c on c.ChannelId = stp.channelid AND ISNULL(stp.IsActive,0)=1 AND ISNULL(stp.IsDeleted,0)=0 AND ISNULL(c.IsActive,0)=1 AND ISNULL(c.IsDeleted,0)=0
                            join crm.sources s on s.SourceId = stp.SourceId AND ISNULL(s.IsActive,0)=1 AND ISNULL(s.IsDeleted,0)=0
                            join crm.type_owner o on o.OwnerId = stp.OwnerId AND ISNULL(o.IsActive,0)=1 AND ISNULL(o.IsDeleted,0)=0";

                if (id > 0)
                {
                    query += @" join crm.Campaign ca on ca.SourcesTPId = stp.SourcesTPId AND ISNULL(ca.IsActive,0)=1 AND ISNULL(ca.IsDeleted,0)=0
                            where ca.CampaignId =@campaignId order by name asc";
                    var result1 = connection.Query(query, new { campaignId = id }).ToList();
                    return result1;
                }
                else
                {
                    query += @" order by name asc";
                    var result = connection.Query(query).ToList();
                    connection.Close();
                    return result;
                }
            }
        }

        public dynamic ReferralType(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                if (id > 0)
                {
                    var query = @"select * from [CRM].[Type_Referral]
                                where Id = @id";
                    var result1 = connection.Query(query, new { Id = id }).ToList();
                    return result1;
                }
                else
                {
                    var query = @"select * from [CRM].[Type_Referral]";
                    var result1 = connection.Query(query).ToList();
                    return result1;
                }
            }
        }

        public dynamic ChannelBySource(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = @"select distinct stp.SourcesTPId as SourceId,
                            s.Name as SourceName,o.Name as OriginationName ,
                            c.Name as ChannelName    , c.Name + ' - ' + s.Name as name from crm.SourcesTP stp
                            join crm.Type_Channel c on c.ChannelId = stp.channelid
                            join crm.sources s on s.SourceId = stp.SourceId
                            join CRM.Type_Owner o on o.OwnerId = stp.OwnerId

                            where stp.SourcesTPId=@sourceId order by 3 asc";

                if (id > 0)
                {
                    var result = connection.Query(query, new { sourceId = id }).ToList();
                    return result;
                }
                else
                    return null;
            }
        }

        public dynamic ZipCodes(string q)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmGeoLocatorConnectionString))
            {
                var list = connection.Query<ZipPostalCode>("Select z.* from CRM.ZipCodes z where z.ZipCode like @q", new { q = string.Format($"{q}%") }).
                        Select(s => new { id = s.ZipCode, text = s.ZipCode, City = s.PrimaryCity, State = s.State, s.CountryCode }).
                        Distinct().
                        Take(10).
                        ToList();
                return list;
            }

        }

        public dynamic GetRelatedAccounts(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = @"SELECT
                                  [AccountId]
                                  ,(select [CRM].[fnGetAccountNameByAccountId](AccountId)) as FullName
                                  FROM CRM.[Accounts]
                                  WHERE FranchiseId= @FranchiseId AND AccountId!=@accountid AND ISNULL(IsDeleted,0)=0 order by isnull(LastUpdatedOnUtc,CreatedOnUtc) desc";
                int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                connection.Open();
                var response = connection.Query(query, new { FranchiseId = FranchiseId, accountid = id }).ToList();

                connection.Close();
                return response;
            }
        }

        public dynamic AccountStatus()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = "select * from CRM.Type_AccountStatus";
                var result = connection.Query(query).ToList();
                connection.Close();
                return result;
            }
        }

        public List<RoomLocationViewModel> GetRoomLocation()
        {
            List<RoomLocationViewModel> response = new List<RoomLocationViewModel>();
            response.Add(new RoomLocationViewModel
            {
                Key = "Basement",
                Value = "Basement",
                AbbrValue = "BSMT"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Bathroom",
                Value = "Bathroom",
                AbbrValue = "BTH"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Bedroom",
                Value = "Bedroom",
                AbbrValue = "BR"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Bedroom 1",
                Value = "Bedroom 1",
                AbbrValue = "BR1"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Bedroom 2",
                Value = "Bedroom 2",
                AbbrValue = "BR2"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Bedroom 3",
                Value = "Bedroom 3",
                AbbrValue = "BR3"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Bedroom 4",
                Value = "Bedroom 4",
                AbbrValue = "BR4"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Den",
                Value = "Den",
                AbbrValue = "DEN"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Dining Room",
                Value = "Dining Room",
                AbbrValue = "DR"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Family  Room",
                Value = "Family  Room",
                AbbrValue = "FAM"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Formal Dining Room",
                Value = "Formal Dining Room",
                AbbrValue = "FDR"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Informal Dining Room",
                Value = "Informal Dining Room",
                AbbrValue = "IDR"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Garage",
                Value = "Garage",
                AbbrValue = "GAR"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Game Room",
                Value = "Game Room",
                AbbrValue = "GAME"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Kitchen",
                Value = "Kitchen",
                AbbrValue = "KIT"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Laundry",
                Value = "Laundry",
                AbbrValue = "LAU"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Living Room",
                Value = "Living Room",
                AbbrValue = "LIV"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Master Bathroom",
                Value = "Master Bathroom",
                AbbrValue = "MBTH"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Master Bedroom",
                Value = "Master Bedroom",
                AbbrValue = "MBR"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Media Room",
                Value = "Media Room",
                AbbrValue = "MED"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Office",
                Value = "Office",
                AbbrValue = "OFF"
            });

            response.Add(new RoomLocationViewModel
            {
                Key = "Play Room",
                Value = "Play Room",
                AbbrValue = "PLAY"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Studio",
                Value = "Studio",
                AbbrValue = "STD"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Sunroom",
                Value = "Sunroom",
                AbbrValue = "SUN"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Theater Room",
                Value = "Theater Room",
                AbbrValue = "THTR"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Common Area",
                Value = "Common Area",
                AbbrValue = "COM"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Conference  Room",
                Value = "Conference  Room",
                AbbrValue = "CONF"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "File Room",
                Value = "File Room",
                AbbrValue = "FILE"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Fitness Room",
                Value = "Fitness Room",
                AbbrValue = "FIT"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "IT Room",
                Value = "IT Room",
                AbbrValue = "IT"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Laboratory",
                Value = "Laboratory",
                AbbrValue = "LAB"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Lobby",
                Value = "Lobby",
                AbbrValue = "LBY"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Reception",
                Value = "Reception",
                AbbrValue = "REC"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Supply Room",
                Value = "Supply Room",
                AbbrValue = "SUPP"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Warehouse",
                Value = "Warehouse",
                AbbrValue = "WHSE"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Entry",
                Value = "Entry",
                AbbrValue = "ENT"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Stair",
                Value = "Stair",
                AbbrValue = "STR"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Bonus",
                Value = "Bonus",
                AbbrValue = "BON"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Loft",
                Value = "Loft",
                AbbrValue = "LFT"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Great Room",
                Value = "Great Room",
                AbbrValue = "GRT"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Powder Room",
                Value = "Powder Room",
                AbbrValue = "POW"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Slider",
                Value = "Slider",
                AbbrValue = "SLD"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Nook",
                Value = "Nook",
                AbbrValue = "NK"
            });
            response.Add(new RoomLocationViewModel
            {
                Key = "Other",
                Value = "Other",
                AbbrValue = "OTR"
            });

            return response;
        }

        public List<KeyValuePair<string, string>> GetWindowLocation()
        {
            List<KeyValuePair<string, string>> data = new List<KeyValuePair<string, string>>();
            var value = "";
            for (var i = 1; i < 51; i++)
            {
                value = "Window " + i + " / " + "W" + i;
                data.Add(new KeyValuePair<string, string>(value, value));
            }
            for (var j = 1; j < 5; j++)
            {

                value = "Door " + j + " / " + "D" + j;
                data.Add(new KeyValuePair<string, string>(value, value));
            }
            return data;
        }

        public List<MountTypeViewModel> GetMountType()
        {
            List<MountTypeViewModel> response = new List<MountTypeViewModel>();
            response.Add(new MountTypeViewModel
            {
                Key = "IB",
                Value = "IB"
            });
            response.Add(new MountTypeViewModel
            {
                Key = "OB",
                Value = "OB"
            });
            response.Add(new MountTypeViewModel
            {
                Key = "Partial IB",
                Value = "Partial IB"
            });
            response.Add(new MountTypeViewModel
            {
                Key = "Partial OB",
                Value = "Partial OB"
            });
            response.Add(new MountTypeViewModel
            {
                Key = "Wall Mounted",
                Value = "Wall Mounted"
            });
            response.Add(new MountTypeViewModel
            {
                Key = "Ceiling Mounted",
                Value = "Ceiling Mounted"
            });
            return response;
        }

        public dynamic GetAttendees()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = @"select distinct per.personid as SalesAgentId,FirstName + ' ' + LastName  as FullName ,usr.Email
                                    from [Auth].[Users] usr
                                    join [CRM].[Person] per on per.personid = usr.personid
                                    join [Auth].[UsersInRoles] uir on usr.userid = uir.userid
                                    where usr.franchiseid = @FranchiseId and usr.Domain is not null and usr.addressid is not null and ISNULL(usr.IsDeleted,0)=0 and ISNULL(usr.IsDisabled,0)=0 
                                    order by FullName";
                connection.Open();
                var result = connection.Query(query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                connection.Close();
                return result;
            }
        }

        public dynamic GetAllDropdownValues()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = @"Select SourcesHFCId,typo.Name+' -> '+typc.Name+' -> '+stp.Name as HFCSourceName
                                  from CRM.SourcesHFC SHF
                                  inner join CRM.Type_Owner typo on typo.OwnerId=SHF.OwnerId
                                  inner join CRM.Type_Channel typc on typc.ChannelId=SHF.ChannelId
                                  inner join CRM.Sources stp on stp.SourceId=SHF.SourceId
                                  where SHF.IsActive=1 order by SourcesHFCId";
                connection.Open();
                var response = connection.Query(query).ToList();
                connection.Close();
                return response;
            }
        }

        public dynamic GetEmails()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select p.PersonId, p.PrimaryEmail from Auth.Users u
                             join CRM.Person p on u.PersonId=p.PersonId
                             WHERE u.Domain IS NOT NULL
                             AND isnull(u.IsDisabled,0)=0 AND u.IsDeleted=0 AND u.FranchiseId = @franchiseId";
                var response = connection.Query(query, new { franchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                connection.Close();
                return response;
            }
        }

        public dynamic Type_ShipToLocation()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select * from [CRM].[Type_ShipToLocations]";
                var result = connection.Query(query).ToList();
                connection.Close();
                return result;
            }
        }

        public dynamic GetCampaignByFranchise()
        {
            int franchiseid = 0;

            if (SessionManager.CurrentFranchise != null)
            {
                franchiseid = SessionManager.CurrentFranchise.FranchiseId;
            }
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = @"SELECT ow.Name AS Owner, tc.Name AS Channel, s.Name AS Source, c.Name AS Campaign,
                                CONVERT(date, c.StartDate ) AS CampaignStartDate,CONVERT(date, c.EndDate) AS CampaignEndDate,
                                c.IsDeleted AS CampaignIsDeleted, c.IsActive AS CampaignIsActive, c.SourcesTPId,
                                c.CampaignId,c.Memo,c.Amount FROM CRM.SourcesTP st left JOIN
                                crm.Campaign c  ON st.SourcesTpId = c.SourcesTpId INNER JOIN CRM.Sources s ON
                                s.sourceId = st.sourceId INNER JOIN CRM.Type_Channel tc ON tc.ChannelId = st.ChannelId INNER JOIN
                                CRM.Type_Owner ow ON ow.ownerid = st.ownerid where c.Franchiseid = @franchiseid and c.IsActive !=0 and c.IsActive is not null;";
                connection.Open();
                var response = connection.Query(query, new { franchiseid = franchiseid }).ToList();
                connection.Close();
                return response;
            }
        }

        public dynamic GetAllSources()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = @" SELECT st.SourcesTPId,   s.Name as Source, tc.Name AS Channel, ow.Name as Owner FROM crm.SourcesTP st INNER JOIN CRM.Sources s ON s.sourceId = st.sourceId INNER JOIN CRM.Type_Channel tc ON tc.ChannelId = st.ChannelId INNER JOIN CRM.Type_Owner ow ON ow.ownerid = st.ownerid;";
                connection.Open();
                var response = connection.Query(query).ToList();
                connection.Close();
                return response;
            }
        }

        public dynamic GetQuoteStatus(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                if (id > 0)
                {
                    var query = @"select q.QuoteStatusId,q.QuoteStatus from [CRM].[Type_QuoteStatus] q
                                where q.QuoteStatusId = @id";
                    var result = connection.Query(query, new { id = id }).ToList();
                    connection.Close();
                    return result;
                }
                else
                {
                    var query = @"select * from [CRM].[Type_QuoteStatus]";
                    var result = connection.Query(query, new { id = id }).ToList();
                    connection.Close();
                    return result;
                }
            }
        }

        public dynamic GetStorageType()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select a.StorageType as Id , a.StorageType as Name, a.System as Value from CRM.[MeasurementConfiguration] a";
                var result = connection.Query<dynamic>(query).ToList();
                connection.Close();
                return result;
            }
        }

        public dynamic GetFESalesPerson()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                var user = LocalMembership.GetUser(SessionManager.CurrentUser.PersonId, "Person", "Roles", "OAuthUsers", "UserWidgets");
                if (user.IsInRole(AppConfigManager.FranchiseAdminRole.RoleId) || user.IsInRole(AppConfigManager.DefaultOwnerRole.RoleId))
                {
                    var query = @"select distinct p.PersonId, p.FirstName+' '+p.LastName as PersonName from Auth.Users u
                              join Auth.UsersInRoles ur on u.UserId=ur.UserId
                              join CRM.Person p on u.PersonId=p.PersonId
                              where u.FranchiseId=@FranchiseId and RoleId in( '9CFE899F-A036-4D69-A58B-A1BFBDD8DCA0','06547EFF-24AA-454B-A399-9017009B3FBD')";
                    var result = connection.Query(query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                    connection.Close();
                    return result;
                }
                else if (user.IsInRole(AppConfigManager.DefaultSalesRole.RoleId, AppConfigManager.DefaultSalesNoMargineRole.RoleId))
                {
                    var query = @"select distinct p.PersonId, p.FirstName+' '+p.LastName as PersonName from Auth.Users u
                                  join Auth.UsersInRoles ur on u.UserId=ur.UserId
                                  join CRM.Person p on u.PersonId=p.PersonId
                                  where u.FranchiseId=@FranchiseId and u.PersonId=@PersonId and RoleId in( '9CFE899F-A036-4D69-A58B-A1BFBDD8DCA0','06547EFF-24AA-454B-A399-9017009B3FBD')";
                    var result = connection.Query(query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId, PersonId = SessionManager.CurrentUser.PersonId }).ToList();
                    connection.Close();
                    return result;
                }
                return "";
            }
        }

        public dynamic GetFESource()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select s.SourceId,s.Name from(
                                select distinct SourceId from CRM.LeadSources ls
                                join CRM.Leads l on l.LeadId=ls.LeadId
                                where l.FranchiseId=@FranchiseId) as lsource
                                join CRM.SourcesTP stp on lsource.SourceId=stp.SourcesTPId
                                join CRM.Sources s on stp.SourceId=s.SourceId
                                union
                                select s.SourceId,s.Name from(
                                select distinct SourceId from CRM.AccountSources accs
                                join CRM.Accounts acc on accs.AccountId=acc.AccountId
                                where acc.FranchiseId=@FranchiseId) as Accsource
                                join CRM.SourcesTP stp on Accsource.SourceId=stp.SourcesTPId
                                join CRM.Sources s on stp.SourceId=s.SourceId
                                order by Name asc";
                var result = connection.Query(query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                connection.Close();
                return result;
            }
        }
    }
}
