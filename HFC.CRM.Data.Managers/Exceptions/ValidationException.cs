﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Managers.Exceptions
{
    class ValidationException : ApplicationException
    {
        public ValidationException(string message) : base(message) { }
    }
}
