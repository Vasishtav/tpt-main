﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.IO;
using HFC.CRM.DTO;

namespace HFC.CRM.Managers
{
    public class AdditionalContactManager : DataManager<CustomerTP>
    {
        public AdditionalContactManager(User user, Franchise franchise) : this(user, user, franchise)
        {
        }

        public AdditionalContactManager(User user, User authorizingUser, Franchise franchise)
        {
            base.User = user;
            base.AuthorizingUser = authorizingUser;
            base.Franchise = franchise;
        }

        public bool CanbeDeleted(int customerId)
        {
            var result = false;

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select count(1) from crm.Addresses where 
                        ISNULL(IsDeleted, 0) = 0 and ContactId = @contactId";
                result = connection.ExecuteScalar<bool>(query, new { contactid = customerId });
                connection.Close();
            }

            return !result;
        }
        public List<CustomerTP> GetGlobalContacts(int CustomerId)
        {
            List<CustomerTP> contacts = new List<CustomerTP>();

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select * from crm.customer c join crm.leadcustomers lc 
                            on c.CustomerId = lc.CustomerId
							where c.CustomerId = @CustomerId and lc.IsPrimaryCustomer = 0
                            union
	                        select * from crm.customer c join crm.AccountCustomers ac 
                            on c.CustomerId = ac.CustomerId
                            where c.CustomerId = @CustomerId and ac.IsPrimaryCustomer = 0";
                contacts = connection.Query<CustomerTP>(query, new { CustomerId = CustomerId }).ToList();
                connection.Close();
            }

            return contacts;
        }
        public List<CustomerTP> GetLeadContacts(int leadId, bool includeDeleted)
        {
            List<CustomerTP> contacts = new List<CustomerTP>();

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select * from crm.customer c join crm.leadcustomers lc 
                            on c.CustomerId = lc.CustomerId
                            where lc.LeadId = @leadId and lc.IsPrimaryCustomer = 0";

                if (!includeDeleted) query+= " and IsDeleted <> 1"; 

                    contacts = connection.Query<CustomerTP>(query, new { leadId = leadId }).ToList();
                connection.Close();
            }

            return contacts;
        }

        public List<CustomerTP> GetAccountContacts(int accountId, bool includeDeleted)
        {
            List<CustomerTP> contacts = new List<CustomerTP>();

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select * from crm.customer c join crm.accountcustomers ac 
                            on c.CustomerId = ac.CustomerId
                            where ac.AccountId = @accountId and ac.IsPrimaryCustomer = 0";
                            //and IsDeleted <> 1";
                if (!includeDeleted) query += " and IsDeleted <> 1";
                contacts = connection.Query<CustomerTP>(query, new { accountId = accountId }).ToList();
                connection.Close();
            }

            return contacts;
        }

        public override string Add(CustomerTP model )
        {
            using (SqlConnection connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var customerId = (int)connection.Insert(model);

                switch (model.AssociatedSource)
                {
                    case CustomerSource.Lead:
                        connection.Query(@"insert into crm.LeadCustomers values(
                                        @leadid, @customerId, @isPrimaryCustomer)"
                        , new { leadid = model.LeadId, customerId = customerId, isPrimaryCustomer = false });
                        break;
                    case CustomerSource.Account:
                        connection.Query(@"insert into crm.accountcustomers values(
                                        @accountid, @customerId, @isPrimaryCustomer)"
                        , new { accountid = model.AccountId, customerId = customerId, isPrimaryCustomer = false });
                        break;
                    case CustomerSource.Opportunity:
                        break;
                    default:
                        break;
                }
            }

            return Success;
        }

        public override string Update(CustomerTP model)
        {
            using (SqlConnection connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {

                var query = @"update crm.customer set FirstName=@firstName, LastName=@lastName
                            , HomePhone=@homePhone, CellPhone=@cellPhone, WorkPhone=@workPhone
                            , WorkPhoneExt = @workPhoneExt
                            , CompanyName=@companyName, WorkTitle=@workTitle
                            , PrimaryEmail=@primaryEmail, SecondaryEmail=@secondaryEmail
                            , PreferredTFN=@preferredTFN, IsReceiveEmails=@receiveEmail
                            , Updatedon=@updatedOn, UpdatedBy=@updatedBy
                            where CustomerId=@customerId";

                connection.Execute(query, new
                {
                    firstName = model.FirstName,
                    lastName = model.LastName,
                    homePhone = model.HomePhone,
                    cellPhone = model.CellPhone,
                    workPhone = model.WorkPhone,
                    companyName = model.CompanyName,
                    workTitle = model.WorkTitle,
                    workPhoneExt = model.WorkPhoneExt,
                    primaryEmail = model.PrimaryEmail,
                    secondaryEmail = model.SecondaryEmail,
                    preferredTFN = model.PreferredTFN,
                    receiveEmail = model.IsReceiveEmails,
                    customerId = model.CustomerId,
                    updatedOn = model.UpdatedOn,
                    updatedBy = model.UpdatedBy
                });
                   
                //var customerId = (int)connection.Update(model);
            }

            return Success;
        }

        public override string Delete(int id)
        {
            using (SqlConnection connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {

                var query = @"update crm.customer set IsDeleted = 1
                            where CustomerId=@customerId";

                connection.Execute(query, new
                {
                    customerId = id,
                });

                //var customerId = (int)connection.Update(model);
            }

            return Success;
        }

        public  string Recover(int id)
        {
            using (SqlConnection connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = @"update crm.customer set IsDeleted = 0
                            where CustomerId=@customerId";

                connection.Execute(query, new
                {
                    customerId = id,
                });
            }

            return Success;
        }

        public override ICollection<CustomerTP> Get(List<int> idList)
        {
            throw new NotImplementedException();
        }

        public override CustomerTP Get(int id)
        {
            throw new NotImplementedException();
        }
    }
}


