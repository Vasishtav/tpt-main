﻿using Dapper;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using HFC.CRM.DTO;
using HFC.CRM.DTO.Lookup;
using HFC.CRM.DTO.Source;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace HFC.CRM.Managers
{
    public class MyCRMSourceMappingManager : DataManager
    {
        public MyCRMSourceMappingManager(User user, Franchise franchise)
        {
            this.User = user;
            this.Franchise = franchise;
        }

        public List<MyCRMMapData> GetmyCRMSource()
        {
            List<LookupBaseDTO> TPTSource = new List<LookupBaseDTO>();
            List<MyCRMMapData> myCRMSourceMap = new List<MyCRMMapData>();

            string queryTPTSource = @"SELECT [SourceId] as Id, ChannelName+' -> '+ SourceName  AS Name FROM [dbo].[vwTPTSourceLookup] order by ChannelName,SourceName ASC";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                //TPTSource = connection.Query<LookupBaseDTO>(queryTPTSource).ToList();
            }

            string query = @"select  sl.[SourceId] as Id,sl.Source [Name],ms.[TPTSourceID] from [dbo].[vwMyCRMSourceLookup] sl
                             left join [MyCRM_TPT_Migration].[Migration].[Source] ms on ms.[MyCRMSourceID]=sl.SourceId and ms.[FranchiseID] =sl.FranchiseCode
                             where FranchiseCode=@Code and BrandID=@BrandId";
            using (var connection = new SqlConnection(ContextFactory.MigrationConstr))
            {
                myCRMSourceMap = connection.Query<MyCRMMapData>(query, new { Code = Franchise.Code, Franchise.BrandId }).ToList();
                TPTSource = connection.Query<LookupBaseDTO>(queryTPTSource).ToList();
                foreach (var map in myCRMSourceMap)
                {
                    if (map.TPTSourceID != null && map.TPTSourceID > 0)
                        map.TPTSourceName = TPTSource.Where(x => x.Id == map.TPTSourceID).Select(x => x.Name).FirstOrDefault();
                }

                return myCRMSourceMap;
            }
        }

        public List<LookupBaseDTO> GetTPTSource()
        {
            string query = @"SELECT [SourceId] as Id, ChannelName+' -> '+ SourceName  AS Name FROM [dbo].[vwTPTSourceLookup] order by ChannelName,SourceName ASC";
            using (var connection = new SqlConnection(ContextFactory.MigrationConstr))
            {
                return connection.Query<LookupBaseDTO>(query).ToList();
            }
        }

        //MyCRMSourceMap

        public void SavemyCRMSourcemapping(List<MyCRMSourceMap> MyCRMSourceMap)
        {
            string insQ = @"insert into [Migration].[Source] values (@Brand,@FranchiseId,@MyCRMSourceID,@TPTSourceID,@DateAdd)";

            string delQ = @"delete from [Migration].[Source] where FranchiseID =@FranchiseId";

            using (var connection = new SqlConnection(ContextFactory.MigrationConstr))
            {
                connection.Execute(delQ, new { FranchiseId = Franchise.Code });
            }

            foreach (var map in MyCRMSourceMap)
            {
                using (var connection = new SqlConnection(ContextFactory.MigrationConstr))
                {
                    connection.Execute(insQ, new
                    {
                        Brand = Franchise.Brand,
                        FranchiseId = Franchise.Code,
                        map.MyCRMSourceID,
                        map.TPTSourceID,
                        DateAdd = DateTime.UtcNow
                    });
                }
            }
        }

        public bool ConfirmMyCRM_TPT_SourceMap()
        {
            try
            {
                string insQ = @"insert into dbo.FranchiseSourceMapConfirmed values (@FranchiseCode,@IsConfirmed)";

                using (var connection = new SqlConnection(ContextFactory.MigrationConstr))
                {
                    connection.Execute(insQ, new { FranchiseCode = Franchise.Code, IsConfirmed = true });
                }
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetConfirmMyCRM_TPT_SourceMap()
        {
            try
            {
                string selectQ = @"select IsConfirmed from dbo.FranchiseSourceMapConfirmed where FranchiseCode=@FranchiseCode";

                using (var connection = new SqlConnection(ContextFactory.MigrationConstr))
                {
                    var res = connection.ExecuteScalar<bool?>(selectQ, new { FranchiseCode = Franchise.Code });

                    if (res != null && res == true)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}