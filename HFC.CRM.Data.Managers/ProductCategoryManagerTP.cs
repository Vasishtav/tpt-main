﻿using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HFC.CRM.Managers
{
    public class ProductCategoryManagerTP : DataManager<Type_ProductCategory>
    {
        /// <summary>
        /// Get the Logged in user info
        /// </summary>
        /// <param name="user"></param>
        public ProductCategoryManagerTP(User user)
        {
            User = user;
        }

        /// <summary>
        /// Insert the product category
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public override string Add(Type_ProductCategory data)
        {
            var result = Insert<int, Type_ProductCategory>(ContextFactory.CrmConnectionString, data);
            data.ProductCategoryId = result;
            return result.ToString();
        }

        /// <summary>
        /// Soft delete the product category
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override string Delete(int id)
        {
            var result = GetData<Type_ProductCategory>(ContextFactory.CrmConnectionString, id);
            if (result != null)
            {
                result.IsActive = false;
                result.IsDeleted = true;
                Update<Type_ProductCategory>(ContextFactory.CrmConnectionString, result);
            }
            return "ProductCategory deleted";
        }

        /// <summary>
        /// Get the Particular product category based on product category id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override Type_ProductCategory Get(int id)
        {
            return GetData<Type_ProductCategory>(ContextFactory.CrmConnectionString, id);
        }

        public override ICollection<Type_ProductCategory> Get(List<int> idList)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Update the Product category information
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public override string Update(Type_ProductCategory data)
        {
            var ProductCategory = GetData<Type_ProductCategory>(ContextFactory.CrmConnectionString, data.ProductCategoryId);
            if (ProductCategory != null)
            {
                ProductCategory.ProductCategory = data.ProductCategory;
                ProductCategory.Parant = data.Parant;
                ProductCategory.IsActive = data.IsActive;
                ProductCategory.PICGroupId = data.PICGroupId;
                ProductCategory.PICProductId = data.PICProductId;
                ProductCategory.BrandId = data.BrandId;
                ProductCategory.PICGroupId = data.PICGroupId;
                ProductCategory.PICProductId = data.PICProductId;
                //ProductCategory.SalesTaxCode = data.SalesTaxCode;
                ProductCategory.ProductTypeId = data.ProductTypeId;
                Update<Type_ProductCategory>(ContextFactory.CrmConnectionString, ProductCategory);
            }
            return "Updated Successfully";
        }

        /// <summary>
        /// Get all the product category include active and deleted
        /// </summary>
        /// <returns></returns>
        public List<Type_ProductCategory> GetAllProductCategory(int BrandId)
        {
            string selectQ = @"SELECT [ProductCategoryId]
                              ,[ProductCategory]
                              ,[Parant]
                              ,[IsActive]
                              ,[IsDeleted]
                              ,[CreatedOn]
                              ,[CreatedBy]
                              ,[LastUpdatedOn]
                              ,[LastUpdatedBy]
                              ,[PICGroupId]
                              ,[PICProductId]
                              ,[BrandId]
                              ,[SalesTaxCode]
                              ,[ProductTypeId]
                              FROM [CRM].[Type_ProductCategory] where BrandId=@BrandId";

            var res = ExecuteIEnumerableObject<Type_ProductCategory>(ContextFactory.CrmConnectionString, selectQ, new { BrandId = BrandId }).ToList();
            return res;
        }

        /// <summary>
        /// Get all the all the products only active
        /// </summary>
        /// <returns></returns>
        public List<Type_ProductCategory> GetAllActiveProductCategory(int BrandId)
        {
            string selectQ = @"SELECT [ProductCategoryId]
                              ,[ProductCategory]
                              ,[Parant]
                              ,[IsActive]
                              ,[IsDeleted]
                              ,[CreatedOn]
                              ,[CreatedBy]
                              ,[LastUpdatedOn]
                              ,[LastUpdatedBy]
                              ,[PICGroupId]
                              ,[PICProductId]
                              ,[BrandId]
                              ,[SalesTaxCode]
                              ,[ProductTypeId]
                              FROM [CRM].[Type_ProductCategory] where IsActive=1 and IsDeleted=0 and BrandId=@BrandId";

            var res = ExecuteIEnumerableObject<Type_ProductCategory>(ContextFactory.CrmConnectionString, selectQ, new { BrandId = BrandId }).ToList();
            return res;
        }
    }
}
