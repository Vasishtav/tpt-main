﻿using Dapper;
using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Managers
{
    public class ShippingManager : DataManager<ShipToLocations>
    {
        public ShippingManager(User user, Franchise franchise) : this(user, user, franchise)
        {
        }
        public ShippingManager(User user, User authorizingUser, Franchise franchise)
        {
            this.User = user;
            this.AuthorizingUser = authorizingUser;
            this.Franchise = franchise;
        }
        public override string Add(ShipToLocations data)
        {

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {


                connection.Open();
                data.Addresses.CreatedOn = DateTime.Now;
                data.Addresses.CreatedBy = User.PersonId;
                var addressId = connection.Insert(data.Addresses);
                if (addressId != null)
                {
                    data.CreatedBy = User.PersonId;
                    data.CreatedOn = DateTime.Now;
                    data.Active = true;
                    data.AddressId = addressId;
                    data.FranchiseId = Franchise.FranchiseId;
                    var shipId = connection.Insert(data);

                    foreach (var terriotoryId in data.SelectedRelatedTerritoriesId)
                    {
                        var mappingShip = new ShipToLocationsMap
                        {
                            FranchiseId = Franchise.FranchiseId,
                            ShipToLocation = shipId,
                            TerritoryId = terriotoryId
                        };
                        connection.Insert(mappingShip);
                    }

                }
                connection.Close();
                return Success;
            }

        }

        public override string Delete(int id)
        {
            throw new NotImplementedException();
        }

        public override ShipToLocations Get(int id)
        {
            throw new NotImplementedException();
        }

        public ICollection<ShippingViewModel> Get()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = @"select * from [CRM].[ShipToLocations] s 
                           left join CRM.Addresses a on s.AddressId = a.AddressId where FranchiseId=@franchiseId
                           order by s.CreatedOn desc";
                connection.Open();
                // Later Implementation
                //var shipToLocations = connection.Query<ShipToLocations, AddressTP, ShipToLocations>(query, (ship, address) => { ship.Addresses = address; return ship; }).ToList();
                var shipToLocations = connection.Query<ShipToLocations>(query, new { franchiseId = Franchise.FranchiseId }).ToList();

                foreach (var address in shipToLocations)
                {
                    query = @"select * from CRM.Addresses where AddressId=@Id";
                    address.Addresses = connection.Query<AddressTP>(query, new { Id = address.AddressId }).FirstOrDefault();
                }
                foreach (var shiptype in shipToLocations)
                {
                    query = @"select s.ShipToLocation from CRM.Type_ShipToLocations s where Id=@Id";
                    shiptype.ShipToLocationName = connection.Query<string>(query, new { Id = shiptype.ShipToLocation }).FirstOrDefault();
                }

                List<ShippingViewModel> svm = new List<ShippingViewModel>();
                foreach (var item in shipToLocations)
                {
                    var ship = new ShippingViewModel
                    {
                        Id = item.Id,
                        ShipToLocation = item.ShipToLocation,
                        ShipToLocationName = item.ShipToLocationName,
                        ShipToName = item.ShipToName,
                        CreatedOn = item.CreatedOn,
                        CreatedBy = item.CreatedBy,
                        Active = item.Active,
                        LastUpdatedOn = item.LastUpdatedOn,
                        LastUpdatedBy = item.LastUpdatedBy,
                        AddressId = item.Addresses.AddressId,
                        Address1 = item.Addresses.Address1,
                        Address2 = item.Addresses.Address2,
                        City = item.Addresses.City,
                        State = item.Addresses.State,
                        ZipCode = item.Addresses.ZipCode,
                        Country = item.Addresses.CountryCode2Digits,
                        IsValidated = item.Addresses.IsValidated,
                        PhoneNumber = item.PhoneNumber,
                    };
                    query = @"select t.TerritoryId from [CRM].[ShipToLocationsMap] t
                        where ShipToLocation =@id";
                    ship.TerritoriesId = connection.Query<int>(query, new { id = item.Id }).ToList();
                    ship.Territory = SessionManager.CurrentFranchise.Territories.Where(x => ship.TerritoriesId.Contains(x.TerritoryId)).Select(x => x.Name).ToList();
                    ship.TerritoriesName = String.Join(",", ship.Territory);

                    svm.Add(ship);
                }


                connection.Close();
                return svm;
            }
        }

        public List<ShipToLocationsMap> GetVendor(int id, int VendorId, string Status)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                //var Query = @"select t.TerritoryId,vt.AccountNumber,vt.FranchiseId,vt.Id,vt.Id as PreviousId,vt.ShipToLocation,vt.VendorId,
                //              sl.ShipToName ,sl.AddressId,(ad.Address1+','+ad.Address2+','+ad.City+','+ad.State+','+ad.ZipCode)
                //              as Addressvalue from CRM.Territories t
                //              left join CRM.VendorTerritoryAccount vt on t.TerritoryId=vt.TerritoryId and vt.VendorId=@VendorId
                //              left join Crm.[ShipToLocations] sl on vt.ShipToLocation=sl.Id
                //              left join CRM.Addresses ad on ad.AddressId=sl.AddressId
                //              where t.FranchiseId=@FranchiseId ";
                var Query = @" select t.TerritoryId,t.Name as TerritoryName,vt.AccountNumber,vt.FranchiseId,vt.Id,vt.Id as PreviousId,vt.ShipToLocation,vt.VendorId,ISNULL(h.IsAlliance,0) As IsAlliance,
                              sl.ShipToName ,sl.AddressId,ad.Address1,ad.Address2,ad.City,ad.State,ad.ZipCode
                              from CRM.Territories t
                              left join CRM.VendorTerritoryAccount vt on t.TerritoryId=vt.TerritoryId and vt.VendorId=@VendorId
							  LEFT JOIN acct.HFCVendors h ON h.VendorId = @VendorId
                              left join Crm.[ShipToLocations] sl on vt.ShipToLocation=sl.Id
                              left join CRM.Addresses ad on ad.AddressId=sl.AddressId
                              where t.FranchiseId=@FranchiseId; ";

                var result = connection.Query<ShipToLocationsMap>(Query, new { VendorId = VendorId, FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                if (result != null)
                {
                    foreach (var item in result)
                    {
                        if (item.ShipToName == null || item.ShipToName == "")
                        {
                            item.ShipToName = "Select";
                        }
                        if (item.ShipToName == "Select" && Status == "False")
                        {
                            item.ShipToName = "";
                        }
                    }
                    return result;
                }
            }
            return null;
        }


        public List<ShipToLocationsMap> GetVendor(int id, int VendorId, int FranchiseID, string Status)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var Query = @" select t.TerritoryId,t.Name as TerritoryName,vt.AccountNumber,vt.FranchiseId,vt.Id,vt.Id as PreviousId,vt.ShipToLocation,vt.VendorId,ISNULL(h.IsAlliance,0) As IsAlliance,
                              sl.ShipToName ,sl.AddressId,ad.Address1,ad.Address2,ad.City,ad.State,ad.ZipCode
                              from CRM.Territories t
                              left join CRM.VendorTerritoryAccount vt on t.TerritoryId=vt.TerritoryId and vt.VendorId=@VendorId
							  LEFT JOIN acct.HFCVendors h ON h.VendorId = @VendorId
                              left join Crm.[ShipToLocations] sl on vt.ShipToLocation=sl.Id
                              left join CRM.Addresses ad on ad.AddressId=sl.AddressId
                              where t.FranchiseId=@FranchiseId; ";

                var result = connection.Query<ShipToLocationsMap>(Query, new { VendorId = VendorId, FranchiseId = FranchiseID }).ToList();
                if (result != null)
                {
                    foreach (var item in result)
                    {
                        if (item.ShipToName == null || item.ShipToName == "")
                        {
                            item.ShipToName = "Select";
                        }
                        if (item.ShipToName == "Select" && Status == "False")
                        {
                            item.ShipToName = "";
                        }
                    }
                    return result;
                }
            }
            return null;
        }


        public override string Update(ShipToLocations data)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {

                connection.Open();
                data.Addresses.LastUpdatedBy = User.PersonId;
                data.Addresses.LastUpdatedOn = DateTime.Now;
                var addressId = connection.Update(data.Addresses);
                if (addressId > 0)
                {
                    data.LastUpdatedBy = User.PersonId;
                    data.LastUpdatedOn = DateTime.Now;
                    data.FranchiseId = User.FranchiseId;
                    //var personid = connection.Update(data)
                    var shipId = connection.Update(data);
                }
                var query = @"delete from [CRM].[ShipToLocationsMap] where ShipToLocation =@id";
                var deleteStatus = connection.Execute(query, new { id = data.Id });
                foreach (var terriotoryId in data.SelectedRelatedTerritoriesId)
                {
                    var mappingShip = new ShipToLocationsMap
                    {
                        FranchiseId = Franchise.FranchiseId,
                        ShipToLocation = data.Id,
                        TerritoryId = terriotoryId
                    };
                    connection.Insert(mappingShip);
                }
                connection.Close();
                return Success;
            }
        }
        public string ActivateDeactivateShipLocation(ShipToLocations data)
        {

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = @"update [CRM].[ShipToLocations]
                            set Active = @value where Id =@id ";

                connection.Open();
                var status = connection.Query<string>(query, new { value = data.Active, id = data.Id }).FirstOrDefault();
                connection.Close();
                return Success;
            }
        }

        public bool DuplicateShippingName(int id, string Name)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = @"select s.ShipToName from [CRM].[ShipToLocations] s
                              where s.ShipToName =@name and s.Id != @id AND s.FranchiseId = @FranchiseId";

                connection.Open();
                var status = connection.Query<string>(query, new { name = @Name, id = @id, FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).FirstOrDefault();
                connection.Close();
                if (status != null)
                    return true;
                else
                    return false;

            }
        }
        public override ICollection<ShipToLocations> Get(List<int> idList)
        {
            throw new NotImplementedException();
        }
        public List<ShipToLocationsMap> GetBP(int id, int VendorId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var Query = @"Select fv.BPAccountNo AccountNumber, fv.BPShipLocation ShipToLocation,
                              sl.ShipToName ,sl.AddressId,ad.Address1,ad.Address2,ad.City,ad.State,ad.ZipCode
                              from Acct.FranchiseVendors fv
							  LEFT JOIN acct.HFCVendors h ON h.VendorId = @VendorId
                              left join Crm.[ShipToLocations] sl on fv.BPShipLocation=sl.Id
                              left join CRM.Addresses ad on ad.AddressId=sl.AddressId
                              where fv.FranchiseId= @FranchiseId and fv.VendorId= @VendorId; ";

                var result = connection.Query<ShipToLocationsMap>(Query, new { VendorId = VendorId, FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                if (result != null)
                {
                    foreach (var item in result)
                    {
                        if (item.ShipToName == null || item.ShipToName == "")
                        {
                            item.ShipToName = "Select";
                        }
                    }
                    return result;
                }
            }
            return null;
        }
    }
}
