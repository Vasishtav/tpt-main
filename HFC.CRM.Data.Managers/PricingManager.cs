﻿using HFC.CRM.Core.Membership;
using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Dapper;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HFC.CRM.Core.Logs;
using HFC.CRM.DTO.Pricing;
using Newtonsoft.Json;

namespace HFC.CRM.Managers
{
    public class PricingManager : DataManager<PricingSettings>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PricingManager"/> class.
        /// </summary>
        /// <param name="franchise">The franchise.</param>
        /// <param name="user"></param>
        /// 

        public PricingManager(User user, Franchise franchise) : this(user, user, franchise)
        {
        }
        public PricingManager(User user, User authorizingUser, Franchise franchise)
        {
            User = user;
            AuthorizingUser = authorizingUser;
            Franchise = franchise;
        }

        public PricingSettings Get()
        {
            try
            {
                PricingSettings pricing = new PricingSettings();

                var query = @"select * from CRM.[PricingSettings] where PricingStrategyTypeId=1 and IsActive= 1 and FranchiseId = @FranchiseId";

                pricing = ExecuteIEnumerableObject<PricingSettings>(ContextFactory.CrmConnectionString, query, new
                {
                    FranchiseId = Franchise.FranchiseId
                }).FirstOrDefault();
                return pricing;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return null;
            }
        }

        public IList<AdvancedPricingList> GetAdvancedPricing()
        {
            try
            {
                IList<AdvancedPricingList> pricing = new List<AdvancedPricingList>();

                var query = @"select P.Id,P.FranchiseId,P.PricingStrategyTypeId,P.VendorId,P.ProductCategoryId,P.RetailBasis ,
                            P.MarkUp,P.MarkUpFactor,P.Discount,P.DiscountFactor, P.PriceRoundingOpt, ISNULL(H.Name,F.Name) as Vendor ,
                            TPS.PricingStrategyType,
                            case when ProductCategoryId is not null and ProductCategoryId>=1000 then (select top 1 ProductGroupDesc from CRM.HFCProducts where ProductGroup=p.ProductCategoryId) 
                            else case when ProductCategoryId is not null and ProductCategoryId>0 and ProductCategoryId<1000  then (select ProductCategory from CRM.Type_ProductCategory where ProductCategoryId=p.ProductCategoryId) end end ProductCategory
                            , P.IsActive from CRM.PricingSettings P 
                            left join CRM.Type_PricingStrategyType TPS on P.PricingStrategyTypeId = TPS.Id 
                            left join Acct.HFCVendors H on H.VendorId = P.VendorId 
                            left join Acct.FranchiseVendors F on P.VendorId = F.VendorId and f.FranchiseId = p.FranchiseId
                            where P.IsActive = 1 and P.FranchiseId = @FranchiseId";
                pricing = ExecuteIEnumerableObject<AdvancedPricingList>(ContextFactory.CrmConnectionString, query, new
                {
                    FranchiseId = Franchise.FranchiseId
                }).ToList();

                return pricing;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return null;
            }
        }

        public string FranchiseLevelSave(PricingSettings data)
        {
            PricingSettings result = new PricingSettings();

            data.FranchiseId = Franchise.FranchiseId;
            data.PricingStrategyTypeId = data.PricingStrategyTypeId > 0 ? data.PricingStrategyTypeId : 1;

            try
            {

                //if (!BasePermission.CanCreate)
                //    return Unauthorized;
                if (data.Id > 0)
                {
                    return FranchiseLevelUpdate(data);

                }
                else
                {
                    var query = "";
                    if (data.PricingStrategyTypeId == 1)
                    {
                        query = @"Select * from CRM.PricingSettings where FranchiseId = @FranchiseId and PricingStrategyTypeId = 1 and IsActive = 1";

                        result = ExecuteIEnumerableObject<PricingSettings>(ContextFactory.CrmConnectionString, query, new
                        {
                            FranchiseId = Franchise.FranchiseId
                        }).FirstOrDefault();

                    }

                    if (result != null)
                    {
                        return FranchiseLevelUpdate(data);
                    }
                    data.ImpersonatorPersonId = this.ImpersonatorPersonId;
                    var save = Insert<int, PricingSettings>(ContextFactory.CrmConnectionString, data);
                    if (save < 0)
                    {
                        return FailedByError;
                    }
                    EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(data), PricingId: save);
                    return Success;
                }

            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return FailedByError;
            }
        }
        public string FranchiseLevelUpdate(PricingSettings data)
        {
            PricingSettings check = new PricingSettings();

            try
            {
                //if (!BasePermission.CanCreate)
                //    return Unauthorized;

                if (data.Id != 0)
                {
                    var updquery = @"Select * from CRM.PricingSettings where Id=@Id";
                    var result = ExecuteIEnumerableObject<PricingSettings>(ContextFactory.CrmConnectionString, updquery, new
                    {
                        Id = data.Id
                    }).FirstOrDefault();
                    if (result != null)
                    {
                        data.CreatedOn = result.CreatedOn;
                        data.CreatedBy = result.CreatedBy;
                        data.ImpersonatorPersonId = this.ImpersonatorPersonId;
                        var datas = Update<PricingSettings>(ContextFactory.CrmConnectionString, data);
                        EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(data), PricingId: data.Id);
                        return Success;
                    }
                    else
                    {
                        return FailedByError;
                    }
                }
                else
                {
                    return FailedByError;
                }
                //var result = Update(ContextFactory.CrmConnectionString, data);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return FailedByError;
            }
        }
        public string Save(PricingSettings data)
        {
            PricingSettings result = new PricingSettings();
            data.FranchiseId = Franchise.FranchiseId;
            data.PricingStrategyTypeId = data.PricingStrategyTypeId > 0 ? data.PricingStrategyTypeId : 1;

            try
            {
                //if (!BasePermission.CanCreate)
                //    return Unauthorized;
                if (data.Id > 0)
                {
                    return PricingUpdate(data);

                }
                else
                {
                    var query = "";
                    if (data.PricingStrategyTypeId == 1)
                    {
                        query = @"Select * from CRM.PricingSettings where FranchiseId = @FranchiseId and PricingStrategyTypeId = 1 and IsActive=1";

                        result = ExecuteIEnumerableObject<PricingSettings>(ContextFactory.CrmConnectionString, query, new
                        {
                            FranchiseId = Franchise.FranchiseId
                        }).FirstOrDefault();

                    }
                    else if (data.PricingStrategyTypeId == 2)
                    {
                        query = @"Select * from CRM.PricingSettings where FranchiseId = @FranchiseId and VendorId = @VendorId and PricingStrategyTypeId = 2 and IsActive=1";

                        result = ExecuteIEnumerableObject<PricingSettings>(ContextFactory.CrmConnectionString, query, new
                        {
                            FranchiseId = Franchise.FranchiseId,
                            VendorId = data.VendorId
                        }).FirstOrDefault();

                    }
                    else if (data.PricingStrategyTypeId == 3)
                    {
                        query = @"Select * from CRM.PricingSettings where FranchiseId = @FranchiseId and VendorId = @VendorId and
                                ProductCategoryId = @ProductCategoryId and PricingStrategyTypeId = 3 and IsActive=1";

                        result = ExecuteIEnumerableObject<PricingSettings>(ContextFactory.CrmConnectionString, query, new
                        {
                            FranchiseId = Franchise.FranchiseId,
                            VendorId = data.VendorId,
                            ProductCategoryId = data.ProductCategoryId
                        }).FirstOrDefault();

                    }



                    if (result != null)
                    {
                        if (data.PricingStrategyTypeId == 1)
                        {
                            return FePricing;
                        }
                        else if (data.PricingStrategyTypeId == 2)
                        {
                            return FePricingVendor;
                        }
                        else
                        {
                            return FePricingProduct;
                        }

                    }

                    data.ImpersonatorPersonId = this.ImpersonatorPersonId;

                    var save = Insert<int, PricingSettings>(ContextFactory.CrmConnectionString, data);
                    if (save < 0)
                    {
                        return FailedByError;
                    }

                    // Log History Data Audit trial
                    EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(data), PricingId: save);

                    return Success;
                }

            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return FailedByError;
            }
        }
        public string PricingUpdate(PricingSettings data)
        {
            PricingSettings check = new PricingSettings();
            try
            {
                //if (!BasePermission.CanCreate)
                //    return Unauthorized;

                if (data.Id != 0)
                {
                    var query = "";
                    if (data.PricingStrategyTypeId == 1)
                    {
                        query = @"Select * from CRM.PricingSettings where FranchiseId = @FranchiseId and PricingStrategyTypeId = 1 and IsActive=1";

                        check = ExecuteIEnumerableObject<PricingSettings>(ContextFactory.CrmConnectionString, query, new
                        {
                            FranchiseId = Franchise.FranchiseId
                        }).FirstOrDefault();

                    }
                    else if (data.PricingStrategyTypeId == 2)
                    {
                        query = @"Select * from CRM.PricingSettings where FranchiseId = @FranchiseId and VendorId = @VendorId and PricingStrategyTypeId = 2 and IsActive=1";

                        check = ExecuteIEnumerableObject<PricingSettings>(ContextFactory.CrmConnectionString, query, new
                        {
                            FranchiseId = Franchise.FranchiseId,
                            VendorId = data.VendorId
                        }).FirstOrDefault();

                    }
                    else if (data.PricingStrategyTypeId == 3)
                    {
                        query = @"Select * from CRM.PricingSettings where FranchiseId = @FranchiseId and VendorId = @VendorId and
                                ProductCategoryId = @ProductCategoryId and PricingStrategyTypeId = 3 and IsActive=1";

                        check = ExecuteIEnumerableObject<PricingSettings>(ContextFactory.CrmConnectionString, query, new
                        {
                            FranchiseId = Franchise.FranchiseId,
                            VendorId = data.VendorId,
                            ProductCategoryId = data.ProductCategoryId
                        }).FirstOrDefault();

                    }



                    if (check != null && check.Id != data.Id)
                    {
                        return PricingExist;
                    }

                    var updquery = @"Select * from CRM.PricingSettings where Id=@Id";
                    var result = ExecuteIEnumerableObject<PricingSettings>(ContextFactory.CrmConnectionString, updquery, new
                    {
                        Id = data.Id
                    }).FirstOrDefault();
                    if (result != null)
                    {
                        data.CreatedOn = result.CreatedOn;
                        data.CreatedBy = result.CreatedBy;
                        data.ImpersonatorPersonId = this.ImpersonatorPersonId;
                        var datas = Update<PricingSettings>(ContextFactory.CrmConnectionString, data);

                        // Log History Data Audit trial
                        EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(data), PricingId: data.Id);

                        return Success;
                    }
                    else
                    {
                        return FailedByError;
                    }
                }
                else
                {
                    return FailedByError;
                }
                //var result = Update(ContextFactory.CrmConnectionString, data);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return FailedByError;
            }
        }
        public string PricingDelete(int id)
        {
            try
            {
                //if (!BasePermission.CanCreate)
                //    return Unauthorized;

                if (id != 0)
                {
                    var query = @"Select * from CRM.PricingSettings where Id = @Id";
                    var result = ExecuteIEnumerableObject<PricingSettings>(ContextFactory.CrmConnectionString, query, new
                    {
                        Id = id
                    }).FirstOrDefault();
                    if (result != null)
                    {
                        result.IsActive = false;
                        result.ImpersonatorPersonId = this.ImpersonatorPersonId;
                        var datas = Update<PricingSettings>(ContextFactory.CrmConnectionString, result);
                        EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(result), PricingId: result.Id);
                        return Success;
                    }
                    return FailedByError;
                }
                else
                {
                    return FailedByError;
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return FailedByError;
            }
        }
        public List<Type_PricingStrategyType> GetPriceStrategyTypeDropDown()
        {
            try
            {
                List<Type_PricingStrategyType> ddl = new List<Type_PricingStrategyType>();
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();

                    var query = @"select * from CRM.[Type_PricingStrategyType]";
                    ddl = connection.Query<Type_PricingStrategyType>(query).ToList();

                    connection.Close();
                }
                return ddl;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return null;
            }
        }

        public override PricingSettings Get(int id)
        {
            throw new NotImplementedException();
        }
        public override ICollection<PricingSettings> Get(List<int> idList)
        {
            throw new NotImplementedException();
        }

        public override string Update(PricingSettings data)
        {
            throw new NotImplementedException();
        }

        public override string Delete(int id)
        {
            throw new NotImplementedException();
        }

        public override string Add(PricingSettings data)
        {
            throw new NotImplementedException();
        }
    }
}