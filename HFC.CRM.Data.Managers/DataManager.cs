﻿using HFC.CRM.Data;
using HFC.CRM.Core.Common;
using System;
using System.Collections.Generic;
using HFC.CRM.Data.Context;

namespace HFC.CRM.Managers
{
    public class DataManager : ContantStrings, IDisposable
    {

        /// <summary>
        /// This is the user that leads manager uses to check all permission
        /// </summary>
        /// <value>The user.</value>
        public User User { get; protected set; }

        /// <summary>
        /// Gets or sets the authorizing user.
        /// </summary>
        /// <value>The authorizing user.</value>
        public User AuthorizingUser { get; protected set; }

        /// <summary>
        /// Gets or sets the franchise.
        /// </summary>
        /// <value>The franchise.</value>
        public Franchise Franchise { get; protected set; }

        #region SQLOperation

        #region Insert Data
        public static int? Insert<TEntity>(string Constr, TEntity value) where TEntity : BaseEntity
        {
            // If logged in as admin
            if (SessionManager.CurrentUser != null && SessionManager.CurrentUser.PersonId == 0)
            {
                value.CreatedBy = AdminPersonId;
                value.LastUpdatedBy = AdminPersonId;
            }
            else if (SessionManager.CurrentUser != null && SessionManager.CurrentUser.PersonId > 0)
            {
                value.CreatedBy = SessionManager.CurrentUser.PersonId;
                value.LastUpdatedBy = SessionManager.CurrentUser.PersonId;
            }
            else
            {
                value.CreatedBy = 0;
                value.LastUpdatedBy = 0;
            }
            return SQLOperationManager.Insert<TEntity>(Constr, value);
        }

        public static T Insert<T, TEntity>(string Constr, TEntity value) where TEntity : BaseEntity
        {
            // If logged in as admin
            if (SessionManager.CurrentUser != null && SessionManager.CurrentUser.PersonId == 0)
            {
                value.CreatedBy = AdminPersonId;
                value.LastUpdatedBy = AdminPersonId;
            }
            else if (SessionManager.CurrentUser != null && SessionManager.CurrentUser.PersonId > 0)
            {
                value.CreatedBy = SessionManager.CurrentUser.PersonId;
                value.LastUpdatedBy = SessionManager.CurrentUser.PersonId;
            }
            else
            {
                value.CreatedBy = 0;
                value.LastUpdatedBy = 0;
            }
            return SQLOperationManager.Insert<T, TEntity>(Constr, value);
        }
        #endregion

        #region Update Data
        public static int Update<T>(string Constr, T value) where T : BaseEntity
        {
            // If logged in as admin
            if (SessionManager.CurrentUser != null && SessionManager.CurrentUser.PersonId == 0)
                value.LastUpdatedBy = AdminPersonId;
            else if (SessionManager.CurrentUser != null && SessionManager.CurrentUser.PersonId > 0)
                value.LastUpdatedBy = SessionManager.CurrentUser.PersonId;
            else
                value.LastUpdatedBy = 0;
            value.LastUpdatedOn = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc);
            return SQLOperationManager.Update<T>(Constr, value);
        }
        #endregion

        #region Delete Data
        public static int Delete<T>(string Constr, object value)
        {
            return SQLOperationManager.Delete<T>(Constr, value);
        }
        public static int Delete(string Constr, object value)
        {
            return SQLOperationManager.Delete(Constr, value);
        }
        #endregion

        #region GetData

        public T GetData<T>(string Constr, Guid? value) where T : class
        {
            var data = SQLOperationManager.ExecuteObject<T>(Constr, value);
            if (data != null)
                TimeZoneManager.ConvertToLocal(data);
            return data;
        }
        public T GetData<T>(string Constr, int? value) where T : class
        {
            var data = SQLOperationManager.ExecuteObject<T>(Constr, value);
            if (data != null)
                TimeZoneManager.ConvertToLocal(data);
            return data;
        }

        #endregion

        #region GetListData
        public List<T> GetListData<T>(string Constr, string Query, object value = null) where T : class
        {
            var data = SQLOperationManager.ExecuteListObject<T>(Constr, Query, value);
            if (data != null && data.Count > 0)
                TimeZoneManager.ConvertToLocalList(data);
            return data;

        }

        public IEnumerable<T> ExecuteIEnumerableObjectSingle<T>(string Constr, string Query, object value = null) where T : class
        {
            return SQLOperationManager.ExecuteIEnumerableObjectSingle<T>(Constr, Query, value);
        }
        #endregion

        // Execute custom sql query and return data
        public static IEnumerable<T> ExecuteIEnumerableObject<T>(string Constr, string Query, object value = null)
        {
            return SQLOperationManager.ExecuteIEnumerableObject<T>(Constr, Query, value);
        }

        // Execute custom sql query for Insert and Update
        public static void ExecuteObject(string Constr, string Query, object value = null)
        {
            SQLOperationManager.ExecuteObject(Constr, Query, value);
        }

        // Execute scalar value
        public static T ExecuteScalar<T>(string Constr, string Query, object value = null)
        {
            return SQLOperationManager.ExecuteScalar<T>(Constr, Query, value);
        }

        #endregion

        /// <summary>
        /// DB db for this manager
        /// </summary>
        protected CRMContext CRMDBContext = ContextFactory.Current.Context;

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            //CRMDBContext.Dispose();
        }

        protected DateTime GetNow()
        {
            var now = DateTime.Now;
            return new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second);
        }

        public int? ImpersonatorPersonId
        {
            get
            {
                if (CookieManager.ImpersonateUserId != null && SessionManager.SecurityUser != null && SessionManager.SecurityUser.Person != null)
                {
                    if (Guid.Empty != CookieManager.ImpersonateUserId)
                        return SessionManager.SecurityUser.Person.PersonId;
                    else
                        return null;
                }
                else
                    return null;
            }
        }
    }

    /// <summary>
    /// Class DataManager.
    /// </summary>
    public abstract class DataManager<T> : DataManager
    {
        //public abstract Permission BasePermission { get; }

        public abstract ICollection<T> Get(List<int> idList);

        public abstract T Get(int id);

        public abstract string Add(T data);

        public abstract string Update(T data);

        public abstract string Delete(int id);
    }
}
