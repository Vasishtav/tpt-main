﻿using HFC.CRM.Data;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using HFC.CRM.Data.Context;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using Dapper;
using HFC.CRM.Managers.DTO;
using System.Data.SqlClient;
using System.Runtime.Caching;

namespace HFC.CRM.Managers
{


    /// <summary>
    /// Class ProductsManager.
    /// </summary>
    public class ProductsManager : DataManager
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProductsManager"/> class.
        /// </summary>
        /// <param name="franchise">The franchise.</param>
        /// <param name="user"></param>
        public ProductsManager(Franchise franchise, User user)
        {
            Franchise = franchise;
            AuthorizingUser = user;
        }

        /// <summary>
        /// Gets the products.
        /// </summary>
        /// <param name="categoryId">The category identifier.</param>
        /// <returns>List&lt;Product&gt;.</returns>
        public List<Product> GetProducts(int? categoryId)
        {
            var allProducts = this.CRMDBContext.Products.AsNoTracking()
                    .Include(i => i.ProductType)
                    .Include(i => i.ProductStyles)
                    .Where(w => w.IsActive == true
                        && (w.ProductType.FranchiseId == null || w.ProductType.FranchiseId == Franchise.FranchiseId) && (categoryId == null || w.CategoryId == categoryId))
                .ToList();
            return allProducts.Cast<Product>().ToList();
        }

        public List<ProductMarginDTO> GetProductWithMargins(int manufacturerId)
        {
            var products = CRMDBContext.Products.AsNoTracking().Where(m => m.ManufacturerId == manufacturerId && !m.isCustom.Value);
            var multipliers = CRMDBContext.ProductMutipliers.AsNoTracking().Where(m => m.FranchiseId == this.Franchise.FranchiseId);

            var result = (from product in products
                          join multiplier in multipliers on product.ProductGuid equals multiplier.ProductGuid into j
                          from j1 in j.DefaultIfEmpty()
                          select
                              new ProductMarginDTO()
                              {
                                  ManufacturerId = product.ManufacturerId.Value,
                                  ProductGuid = product.ProductGuid,
                                  ProductName = product.Name,
                                  Multiplier = j1 != null ? j1.Multiplier.Value : 0.3d
                              }).ToList();

            return result;

        }

        public string AddProduct(int categoryId, string typeName, out int productId)
        {
            productId = 0;

            if (string.IsNullOrEmpty(typeName))
                return "Action cancelled, product name required";

            if (categoryId == 0)
                return "Action cancelled, category required";

            // TODO: is this required in TP???
            //if (!Account_BasePermission.CanCreate)
            //    return Unauthorized;

            try
            {
                using (var db = ContextFactory.Create())
                {

                    var typeProduct = db.Type_Products.SingleOrDefault(tp => tp.TypeName == typeName);
                    // TODO may we need filter by FranchiseId here and allow create duplicates globlally??
                    if (typeProduct != null)
                        return "Action cancelled, duplicate product name found.";

                    // add productType and assign it with current Franchise
                    var productTypeEntity = db.Type_Products.Add(new Type_Product()
                    {
                        FranchiseId = this.Franchise.FranchiseId,
                        TypeName = typeName
                    });
                    db.SaveChanges();

                    // add product and set up just created type for it
                    // we are using default values as DB now, need to make default values configurable
                    var productEntity = db.Products.Add(
                        new Product()
                        {
                            CategoryId = categoryId,
                            ProductTypeId = productTypeEntity.ProductTypeId,
                            ProductGuid = Guid.NewGuid(),
                            CreatedOn = DateTimeOffset.Now,
                            ListPrice = 0m,
                            MSRP = 0m,
                            JobberPrice = 0m,
                            JobberDiscountPercent = 60m,
                            IsActive = true,
                            IsTaxable = true,
                        });

                    db.SaveChanges();
                    productId = productEntity.ProductId;

                    // invalidate product cache
                    CacheManager.ProductTypeCollection = null;
                    CacheManager.ProductCategoryCollection = null;

                    return Success;
                }
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        public string UpdateProduct(int productId, string productType)
        {
            if (productId <= 0)
                return DataCannotBeNullOrEmpty;

            if (string.IsNullOrEmpty(productType))
                return "Action cancelled, product must have a name";

            // TODO: is this required in TP???
            //if (!Account_BasePermission.CanUpdate)
            //    return Unauthorized;

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var product = db.Products.Include(i => i.ProductType)
                        .Single(f => f.ProductId == productId);
                    if (product.ProductType.FranchiseId == null ||
                        product.ProductType.FranchiseId != this.Franchise.FranchiseId)
                    {
                        return string.Format("You can update your own products only!");
                    }

                    var productTypeEntity = db.Type_Products.Single(t => t.ProductTypeId == product.ProductTypeId);
                    productTypeEntity.TypeName = productType;

                    db.SaveChanges();

                    // invalidate product cache
                    CacheManager.ProductTypeCollection = null;
                    CacheManager.ProductCategoryCollection = null;

                    return Success;
                }
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        public string UpdateProductMargin(IEnumerable<ProductMarginDTO> dto)
        {
            foreach (var productMarginDTO in dto)
            {
                var status = this.UpdateProductMarginCore(productMarginDTO);
                if (status != Success)
                {
                    return status;
                }
            }

            return Success;
        }

        private string UpdateProductMarginCore(ProductMarginDTO dto)
        {
            if (dto.ProductGuid == Guid.Empty)
                return DataCannotBeNullOrEmpty;

            // TODO: is this required in TP???
            //if (!Account_BasePermission.CanUpdate)
            //    return Unauthorized;

            using (var db = ContextFactory.Create())
            {
                var product = db.Products.FirstOrDefault(p => p.ProductGuid == dto.ProductGuid);
                if (product == null)
                {
                    return string.Format("Product not found");
                }

                var productMultiplier = db.ProductMutipliers.FirstOrDefault(
                        m => m.FranchiseId == this.Franchise.FranchiseId && m.ProductGuid == product.ProductGuid);
                if (productMultiplier == null)
                {
                    productMultiplier = new ProductMutiplier()
                    {
                        FranchiseId = this.Franchise.FranchiseId,
                        ProductGuid = product.ProductGuid,
                    };

                    db.ProductMutipliers.Add(productMultiplier);
                }

                productMultiplier.Multiplier = dto.Multiplier;

                db.SaveChanges();

                return Success;
            }
        }

        public string DeleteProduct(int productId)
        {
            if (productId <= 0)
                return DataCannotBeNullOrEmpty;

            // TODO: is this required in TP???
            //if (!Account_BasePermission.CanDelete)
            //    return Unauthorized;

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var product = db.Products.Include(i => i.ProductType).SingleOrDefault(f => f.ProductId == productId);
                    if (product != null)
                    {
                        if (product.ProductType.FranchiseId == null ||
                            product.ProductType.FranchiseId != this.Franchise.FranchiseId)
                        {
                            return string.Format("You can delete your own products only!");
                        }

                        int count = db.JobItems.Count(i => i.ProductTypeId == product.ProductType.ProductTypeId);
                        if (count > 0)
                            return
                                string.Format("Unable to delete product, there are {0} job item(s) using this product",
                                    count);

                        var productType = db.Type_Products.First(t => t.ProductTypeId == product.ProductTypeId);

                        // delete product
                        db.Products.Remove(product);
                        db.SaveChanges();

                        // delete type
                        db.Type_Products.Remove(productType);
                        db.SaveChanges();

                        // invalidate product cache
                        CacheManager.ProductTypeCollection = null;
                        CacheManager.ProductCategoryCollection = null;
                    }
                    return Success;
                }
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        public List<Type_ProductCategory> GetProductCategoryDropDown()
        {
            try
            {
                List<Type_ProductCategory> ddl = new List<Type_ProductCategory>();
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();

                    var query = @"select Distinct pc.ProductCategoryId,pc.ProductCategory from CRM.[Type_ProductCategory] pc
                                join CRM.FranchiseProducts fp on pc.ProductCategoryId=fp.ProductCategory
                                join Acct.FranchiseVendors fv on fp.VendorID=fv.VendorId 
                                where pc.Parant = 0 and pc.IsActive = 1 and fp.ProductStatus=1 and fv.VendorStatus=1 and fp.ProductType=2 and fp.FranchiseId=@FranchiseId and fv.FranchiseId=@FranchiseId
                                union 
                                select Distinct ProductGroup as ProductCategoryId, ProductGroupDesc as ProductCategory from [CRM].HFCProducts p
                                
                                join Acct.FranchiseVendors fv on p.VendorID=fv.VendorId and fv.VendorStatus=1
                                join Acct.HFCVendors hv on fv.VendorId=hv.VendorId and hv.IsActive=1
                                where p.ProductGroup is not null and fv.FranchiseId=@FranchiseId  AND p.ProductKey NOT IN ( SELECT tps.ProductKey FROM crm.HFCProductStatus tps WHERE tps.FranchiseId =@FranchiseId AND tps.ProductStatusId IN (2,3)) 
                                order by ProductCategory asc";
                    ddl = connection.Query<Type_ProductCategory>(query,new { FranchiseId = this.Franchise.FranchiseId }).ToList();

                    connection.Close();
                }
                return ddl;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return null;
            }
        }



        public List<HFCModels> GetCoreProductModels()
        {
            try
            {
                List<HFCModels> ddl = new List<HFCModels>();
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();

                    var query = @"exec [CRM].[GetCoreProductModels]";
                    ddl = connection.Query<HFCModels>(query).ToList();

                    connection.Close();
                }
                return ddl;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return null;
            }
        }



        public bool UpdateCoreProductModel(HFCModels model)
        {
            try
            {

                List<HFCModels> ddl = new List<HFCModels>();
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();


                    var orgModel = connection.Get<HFCModels>(model.ModelKey);


                    if(orgModel.Active!=model.Active || orgModel.CAActive != model.CAActive||orgModel.USActive != model.USActive)
                    {

                        if(orgModel.CAActive != model.CAActive)
                        {
                            var motra = new HFCModelFlags();
                            motra.ModelKey = model.ModelKey;
                            motra.FieldName = "CAActive";
                            motra.Oldvalue = orgModel.CAActive;
                            motra.NewValue = model.CAActive;
                            motra.ChangedBy = SessionManager.CurrentUser.PersonId;
                            motra.ChangedOn = DateTime.UtcNow;
                            connection.Insert<HFCModelFlags>(motra);

                            orgModel.CAActive = model.CAActive;

                        }
                        if (orgModel.USActive != model.USActive)
                        {
                            var motra = new HFCModelFlags();
                            motra.ModelKey = model.ModelKey;
                            motra.FieldName = "USActive";
                            motra.Oldvalue = orgModel.USActive;
                            motra.NewValue = model.USActive;
                            motra.ChangedBy = SessionManager.CurrentUser.PersonId;
                            motra.ChangedOn = DateTime.UtcNow;
                            connection.Insert<HFCModelFlags>(motra);

                            orgModel.USActive = model.USActive;

                        }

                        if (model.Active != orgModel.Active)
                        {
                            var motra = new HFCModelFlags();
                            motra.ModelKey = model.ModelKey;
                            motra.FieldName = "Active";
                            motra.Oldvalue = orgModel.Active;
                            motra.NewValue = model.Active;
                            motra.ChangedBy = SessionManager.CurrentUser.PersonId;
                            motra.ChangedOn = DateTime.UtcNow;
                            connection.Insert<HFCModelFlags>(motra);

                            if (model.Active)
                            {
                                orgModel.IsManualFLag = false;
                            }

                            orgModel.Active = model.Active;
                        }

                        orgModel.LastUpdatedBy = SessionManager.CurrentUser.PersonId;
                        orgModel.LastUpdatedOn = DateTime.UtcNow;

                        connection.Update(orgModel);
                    }

                    List<string> cacheKeys = MemoryCache.Default.Select(x => x.Key).ToList().Where(x => x.Contains("GetPICProductGroup_")).ToList();
                    foreach (string cacheKey in cacheKeys)
                    {
                        MemoryCache.Default.Remove(cacheKey);
                    }

                    //var query = @"SELECT hv.Name AS Vendor,hv.PICVendorId,hp.ProductName,hp.ProductGroup, hp.ProductGroupDesc,
                    //                CONCAT(hp.WidthPrompt,', ',hp.HeightPrompt,', ',hp.MountPrompt,', ',hp.ColorPrompt,', ',hp.FabricPrompt) AS Prompts,h.* 
                    //                FROM crm.HFCModels h
                    //                INNER JOIN crm.HFCProducts hp ON hp.ProductKey = h.ProductId
                    //                INNER JOIN Acct.HFCVendors hv ON hv.VendorId = h.VendorId
                    //                ORDER BY hv.name,hp.ProductName,h.Model ASC";
                    //ddl = connection.Query<HFCModels>(query).ToList();

                    connection.Close();
                }
                return true;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return false;
            }
        }




    }
}
