﻿// TODO: is this required in TP???

//using HFC.CRM.Core.Common;
//using HFC.CRM.Core.Logs;
//using HFC.CRM.Data;
//using HFC.CRM.Core;
//using HFC.CRM.Managers.DTO;

//using System;
//using System.Collections.Generic;
//using System.Data.Entity;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using HFC.CRM.Core.Membership;
//using HFC.CRM.Data.Constants;
//using HFC.CRM.Core.Extensions;
//using System.Data.SqlClient;
//using Dapper;
//using System.Data.Entity;
//using System.Data.SqlClient;
//using System.Data.Entity.Core.EntityClient;

//namespace HFC.CRM.Managers
//{
//    /// <summary>
//    /// Manager to handle charts and reports
//    /// </summary>
//    /// 
//    public class ChartsManager : DataManager
//    {

//        public class MSR_KeyAccountData
//        {
//            public List<MSRData> MSRData { get; set; }

//            public List<KeyAccountData> KAData { get; set; }

//            public List<KeyAccountDisp> KADataDisp
//            {
//                get
//                {
//                    var list = new List<KeyAccountDisp>();
//                    if (KAData == null || KAData.Count == 0) return list;

//                    var distinctKAName = new List<string>();
//                    foreach (var x in KAData)
//                    {
//                        if (!distinctKAName.Contains(x.KAName))
//                        {
//                            distinctKAName.Add(x.KAName);
//                        }
//                    }

//                    foreach (var obj in distinctKAName)
//                    {
//                        var kaGroup = KAData.Where(x => x.KAName == obj);

//                        //Define Payment Method Type
//                        string paymentName = string.Empty;
//                        string paymentAddr = string.Empty;
//                        string paymentZip = string.Empty;

//                        if (kaGroup.First().PayMethodType == 1)
//                        {
//                            paymentName = kaGroup.First().KAName;
//                            paymentAddr = kaGroup.First().KAAddress;
//                            paymentZip = kaGroup.First().KACSZ;
//                        }
//                        else if (kaGroup.First().PayMethodType == 2)
//                        {
//                            paymentName = kaGroup.First().Storename;
//                            paymentAddr = kaGroup.First().KALocAddress;
//                            paymentZip = kaGroup.First().KALocCSZ;
//                        }
//                        else if (kaGroup.First().PayMethodType > 2)
//                        {
//                            paymentName = "Budget Blinds";
//                            paymentAddr = "1927 N. Glassell Street";
//                            paymentZip = "Orange, CA 92865";
//                        }


//                        //General Calculation 
//                        var sumCustPrice = kaGroup.Sum(x => x.CustPrice);
//                        var sumAddChargs = kaGroup.Sum(x => x.AddChgs);
//                        var sumLeadCount = kaGroup.Sum(x => x.LeadCount);
//                        var sumSaleCount = kaGroup.Sum(x => x.SaleCount);

//                        //Calculating Grand Total, Appt Total and Sales Total
//                        decimal apptTotal = 0;
//                        decimal saleTotal = 0;

//                        if (kaGroup.First().PayByAppointment > 0)
//                            apptTotal = kaGroup.First().ApptCount * kaGroup.First().PayByAppointment;

//                        if (kaGroup.First().CorporateAmount > 0)
//                            saleTotal = (sumCustPrice + sumAddChargs) * kaGroup.First().CorporateAmount;
//                        else
//                            saleTotal = (sumCustPrice + sumAddChargs) * kaGroup.First().PayByPercentOfSale;
//                        //End-------------------


//                        //Calculating Grand Total, Appt Total and Sales Total
//                        decimal total = 0;

//                        if (kaGroup.First().CorporateAmount > 0)
//                            total = (kaGroup.First().ApptCount * kaGroup.First().PayByAppointment) +
//                                (sumLeadCount * kaGroup.First().PayByLead) +
//                                (sumSaleCount * kaGroup.First().PayBySale) +
//                                ((sumCustPrice + sumAddChargs) * kaGroup.First().CorporateAmount);
//                        else
//                            total = (kaGroup.First().ApptCount * kaGroup.First().PayByAppointment) +
//                                (sumLeadCount * kaGroup.First().PayByLead) +
//                                (sumSaleCount * kaGroup.First().PayBySale) +
//                                ((sumCustPrice + sumAddChargs) * kaGroup.First().PayByPercentOfSale);
//                        //End-------------------

//                        var KeyAccountDisp = new KeyAccountDisp
//                        {
//                            CustName = kaGroup.First().CustName,
//                            TerrName = kaGroup.First().TerrName,
//                            TerrCode = kaGroup.First().TerrCode,
//                            KeyAccountName = obj,
//                            PaymentName = paymentName,
//                            PaymentAddr = paymentAddr,
//                            PaymentZip = paymentZip,
//                            GrandTotal = apptTotal + saleTotal,
//                            ApptTotal = apptTotal,
//                            SalesTotal = saleTotal,
//                            TotalAmount = total
//                        };

//                        list.Add(KeyAccountDisp);
//                    }

//                    return list;
//                }
//            }
//        }
//        public class MLSSummary_Data
//        {
//            public List<MlsDataSummary> MLSData { get; set; }
//            public MLSSummary MLSSummary { get { return GenerateMLSSummary(); } }
//            private MLSSummary GenerateMLSSummary()
//            {
//                IList<MLSSummaryItem> mLSTypeSummaryItems = new List<MLSSummaryItem>();
//                IList<MLSSummaryItem> mLSVendorSummaryItems = new List<MLSSummaryItem>();
//                IList<MLSSummaryItem> mLSSurchargeSummaryItems = new List<MLSSummaryItem>();
//                IList<MLSSummaryItem> mLSTaxSummaryItems = new List<MLSSummaryItem>();

//                var itemTypeGroup = MLSData.GroupBy(x => x.ItemType).ToList();
//                foreach (var g in itemTypeGroup)
//                {
//                    var key = g.Key;
//                    if (key == "product")
//                    {
//                        mLSTypeSummaryItems = GetMLSSummaryItemsFor(key, g.GroupBy(x => x.ItemName));
//                        mLSVendorSummaryItems = GetMLSSummaryItemsFor(key, g.GroupBy(x => x.Vendor));
//                    }

//                    if (key == "surcharge")
//                    {
//                        mLSSurchargeSummaryItems = GetMLSSummaryItemsFor(key, g.GroupBy(x => x.ItemName));
//                    }
//                    if (key == "tax")
//                    {
//                        mLSTaxSummaryItems = GetMLSSummaryItemsFor(key, g.GroupBy(x => x.ItemName));
//                    }
//                }
//                return new MLSSummary()
//                {
//                    MLSTypeSummaryItems = mLSTypeSummaryItems,
//                    MLSVendorSummaryItems = mLSVendorSummaryItems,
//                    MLSSurchargeSummaryItems = mLSSurchargeSummaryItems,
//                    MLSTaxSummaryItems = mLSTaxSummaryItems,
//                };
//            }
//            private List<MLSSummaryItem> GetMLSSummaryItemsFor(string key, IEnumerable<IGrouping<string, MlsDataSummary>> innergroup)
//            {
//                List<MLSSummaryItem> ainnerList = new List<MLSSummaryItem>();

//                foreach (var item in innergroup)
//                {
//                    var cost = Math.Round((decimal)item.Sum(x => x.ItemCostSubtotal), 2);
//                    decimal retail = 0m;
//                    if (key == "tax")
//                        retail = Math.Round((decimal)item.Sum(x => x.TaxRateTotal), 2);
//                    else
//                        retail = Math.Round((decimal)item.Sum(x => x.ItemSubtotal), 2);
//                    decimal gp = 0;
//                    if (retail != 0)
//                        gp = Math.Round((retail - cost) / retail * 100, 2);
//                    var mlsTypeDisp = new MLSSummaryItem
//                    {
//                        ItemName = item.Key,
//                        Cost = cost,
//                        Retail = retail,
//                        GP = gp
//                    };

//                    ainnerList.Add(mlsTypeDisp);
//                }
//                return ainnerList.OrderBy(x => x.ItemName).ToList();
//            }
//        }


//        private EntityConnectionStringBuilder EntityCnxStringBuilder = new EntityConnectionStringBuilder (System.Configuration.ConfigurationManager.ConnectionStrings["CRMContext"].ConnectionString);

//        /// <summary>
//        /// Gets the authorized user
//        /// </summary>
//        public User AuthorizedUser { get; private set; }

//        /// <summary>
//        /// Gets the charts permission.
//        /// </summary>
//        public Permission ChartsPermission
//        {
//            get
//            {
//                if (AuthorizedUser.FranchiseId.HasValue)
//                    return PermissionProvider.GetPermission(AuthorizedUser, "Leads", "Charts");
//                else
//                    return SessionManager.ControlPanelPermission;
//            }
//        }

//        /// <summary>
//        /// Initializes a new instance of the <see cref="ChartsManager"/> class.
//        /// </summary>
//        /// <param name="authorizedUser">The authorized user</param>
//        public ChartsManager(User authorizedUser)
//        {
//            AuthorizedUser = authorizedUser;
//        }

//        /// <summary>
//        /// Initializes a new instance of the <see cref="ChartsManager"/> class.
//        /// </summary>
//        /// <param name="authorizedUser">The authorized user</param>
//        /// <param name="franchise"></param>
//        public ChartsManager(User authorizedUser, Franchise franchise)
//        {
//            AuthorizedUser = authorizedUser;
//            Franchise = franchise;
//        }

//        /// <summary>
//        /// Gets the sales data for all franchise. TODO: Recode to use SQL Stored Procedure
//        /// </summary>
//        /// <param name="startDate">Contract start date</param>
//        /// <param name="endDate">Contract end date</param>
//        /// <param name="topNRecord">Number of record to return</param>        
//        /// <exception cref="System.UnauthorizedAccessException"></exception>
//        public List<ChartData> GetFranchiseSales(DateTime startDate, DateTime endDate, int topNRecord = 5)
//        {
//            if (!ChartsPermission.CanRead)
//                throw new UnauthorizedAccessException(Unauthorized);

//            Util.SortDate(ref startDate, ref endDate);
//            try
//            {
//                List<ChartData> result = new List<ChartData>();

//                var data = (from j in CRMDBContext.Jobs
//                            join l in CRMDBContext.Leads on j.LeadId equals l.LeadId
//                            join f in CRMDBContext.Franchises on l.FranchiseId equals f.FranchiseId
//                            let q = j.JobQuotes.FirstOrDefault(x => x.IsPrimary)
//                            where j.IsDeleted == false && l.IsDeleted == false && j.ContractedOnUtc.HasValue &&
//                                    DbFunctions.TruncateTime(j.ContractedOnUtc) >= startDate.Date && DbFunctions.TruncateTime(j.ContractedOnUtc) <= endDate.Date
//                            group q by new { f.FranchiseId, f.Name } into grouped
//                            orderby grouped.Sum(s => s.NetTotal) descending
//                            select new { Key = grouped.Key.Name, Value = grouped.Sum(s => s.NetTotal) }).Take(topNRecord).ToList();
//                foreach (var item in data)
//                {
//                    result.Add(new ChartData { Key = item.Key, Value = item.Value });
//                }
//                return result;
//            }
//            catch (Exception ex)
//            {
//                EventLogger.LogEvent(ex);
//                throw;
//            }
//        }

//        /// <summary>
//        /// Gets sales report by Source.  TODO: Recode to use SQL Stored Procedure
//        /// </summary>
//        /// <param name="startDate">Contract start date</param>
//        /// <param name="endDate">Contract end date</param>
//        /// <param name="franchiseId">Optionally filter report by Franchise id</param>
//        /// <param name="topNRecord">Number of record to return</param>        
//        /// <exception cref="System.UnauthorizedAccessException"></exception>
//        public List<ChartData> GetSourceSales(DateTime startDate, DateTime endDate, int? franchiseId = null, int topNRecord = 5)
//        {
//            if (!ChartsPermission.CanRead)
//                throw new UnauthorizedAccessException(Unauthorized);

//            Util.SortDate(ref startDate, ref endDate);
//            try
//            {

//                if (AuthorizedUser.FranchiseId.HasValue)
//                {
//                    franchiseId = AuthorizedUser.FranchiseId;
//                }

//                List<ChartData> result = new List<ChartData>();

//                var datagroup = GetSourceSales(startDate, endDate, franchiseId, null).Where(x => x.JobStatus.ToLower() == "sale made").GroupBy(x => x.KeyName).ToList();

//                foreach(var g in datagroup)
//                {
//                    var key = g.Key;
//                    var total = Math.Round((decimal)g.Sum(x => x.Subtotal + x.SurchargeTotal - x.DiscountTotal), 2);

//                    result.Add(new ChartData
//                    {
//                        Key = g.Key,
//                        Value = total
//                    });
//                }

//                return result.OrderByDescending(x => x.Value).Take(topNRecord).ToList();

//            }
//            catch (Exception ex)
//            {
//                EventLogger.LogEvent(ex);
//                throw;
//            }
//        }

//        public List<ChartData> GetSourceSalesDashBoard(DateTime startDate, DateTime endDate, int? franchiseId = null)
//        {
//            Util.SortDate(ref startDate, ref endDate);
//            try
//            {
//                if (AuthorizedUser.FranchiseId.HasValue)
//                {
//                    franchiseId = AuthorizedUser.FranchiseId;
//                }

//                List<ChartData> result = new List<ChartData>();
//                result = dw_GetSourceSales(startDate, endDate, franchiseId, null).ToList();
//                return result;
//            }
//            catch (Exception ex)
//            {
//                EventLogger.LogEvent(ex);
//                throw;
//            }
//        }
//        /// <summary>
//        /// Gets job count by Source. TODO: Recode to use SQL stored procedure
//        /// </summary>
//        /// <param name="startDate">Contract start date</param>
//        /// <param name="endDate">Contract end date</param>
//        /// <param name="franchiseId">Optionally filter report by franchise id</param>
//        /// <param name="topNRecord">Number of record to return</param>
//        /// <exception cref="System.UnauthorizedAccessException"></exception>
//        public List<ChartData> GetJobSourceCount(DateTime startDate, DateTime endDate, int? franchiseId = null, int topNRecord = 5)
//        {
//            if (!ChartsPermission.CanRead)
//                throw new UnauthorizedAccessException(Unauthorized);
//            Util.SortDate(ref startDate, ref endDate);
//            try
//            {
//                if (AuthorizedUser.FranchiseId.HasValue)
//                {
//                    franchiseId = AuthorizedUser.FranchiseId;
//                }
//                List<ChartData> result = new List<ChartData>();
//                result = dw_GetSourceSales(startDate, endDate, franchiseId, null);
//                return result;
//            }
//            catch (Exception ex)
//            {
//                EventLogger.LogEvent(ex);
//                throw;
//            }
//        }
//        /// <summary>
//        /// Gets report on job status
//        /// </summary>
//        /// <param name="startDate">Created start date</param>
//        /// <param name="endDate">Created end date</param>        
//        /// <exception cref="System.UnauthorizedAccessException"></exception>
//        public List<ChartData> GetJobStatus(DateTime startDate, DateTime endDate)
//        {
//            int? franchiseId = 0;
//            if (!ChartsPermission.CanRead)
//                throw new UnauthorizedAccessException(Unauthorized);

//            franchiseId = SessionManager.CurrentFranchise.FranchiseId;

//            Util.SortDate(ref startDate, ref endDate);
//            try
//            {
//                var jobstatusList = GetJobStatusForDashBoard(startDate, endDate, franchiseId, null);
//                var total = jobstatusList.Count();
//                var jobstatusgroup = jobstatusList.GroupBy(x => x.JobStatus);

//                List<ChartData> result = new List<ChartData>();

//                foreach (var jobstatus in jobstatusgroup)
//                {

//                    var item = new ChartData
//                    {
//                        Color = GetJobStatusColorforChart(jobstatus.Key.ToLower()),
//                        Key = jobstatus.Key,
//                        Value = jobstatus.Count(),                    
//                    };
//                    result.Add(item);

//                }
//                return result;
















//                //List<ChartData> result = new List<ChartData>();

//                //var data = (from j in CRMDBContext.Jobs
//                //            where j.IsDeleted == false && j.Lead.IsDeleted == false
//                //            && (j.Lead.FranchiseId == franchiseId) &&
//                //                    DbFunctions.TruncateTime(j.CreatedOnUtc) >= startDate.Date && DbFunctions.TruncateTime(j.CreatedOnUtc) <= endDate.Date
//                //            group j by j.JobStatusId into grouped
//                //            select new
//                //            {
//                //                grouped.Key,
//                //                Value = grouped.Count(),
//                //                items = grouped
//                //            }).ToList();



//                //var newJobsStatusIds = CacheManager.JobStatusTypeCollection.Where(w => w.Id == 27 || w.ParentId == 27).Select(s => s.Id).ToList();
//                //var lostJobsStatusIds = CacheManager.JobStatusTypeCollection.Where(w => w.Id == 486 || w.ParentId == 486).Select(s => s.Id).ToList();
//                //var quoteJobsStatusIds = CacheManager.JobStatusTypeCollection.Where(w => w.Id == 31 || w.ParentId == 31).Select(s => s.Id).ToList();
//                //var lostSaleStatusIds = CacheManager.JobStatusTypeCollection.Where(w => w.Id == 28 || w.ParentId == 28).Select(s => s.Id).ToList();
//                //var saleMadeStatusIds = CacheManager.JobStatusTypeCollection.Where(w => w.Id == 32 || w.ParentId == 32).Select(s => s.Id).ToList();
//                //var completeStatusIds = CacheManager.JobStatusTypeCollection.Where(w => w.Id == 29 || w.ParentId == 29).Select(s => s.Id).ToList();
//                //var serviceStatusIds = CacheManager.JobStatusTypeCollection.Where(w => w.Id == 30 || w.ParentId == 30).Select(s => s.Id).ToList();

//                //int totals = data.Sum(s => s.Value),
//                //    newJobs = data.Where(w => newJobsStatusIds.Contains(w.Key)).Sum(s => s.Value),
//                //    lostJobs = data.Where(w => lostJobsStatusIds.Contains(w.Key)).Sum(s => s.Value),
//                //    quoteJobs = data.Where(w => quoteJobsStatusIds.Contains(w.Key)).Sum(s => s.Value),
//                //    lostSales = data.Where(w => lostSaleStatusIds.Contains(w.Key)).Sum(s => s.Value),
//                //    saleMades = data.Where(w => saleMadeStatusIds.Contains(w.Key)).Sum(s => s.Value),
//                //    completes = data.Where(w => completeStatusIds.Contains(w.Key)).Sum(s => s.Value),
//                //    services = data.Where(w => serviceStatusIds.Contains(w.Key)).Sum(s => s.Value);

//                //if (totals > 0)
//                //{
//                //    result.Add(new ChartData { Key = "New Jobs", Value = newJobs, Color = "#53a8fb"});//"#53a8fb"));      // new job
//                //    result.Add(new ChartData { Key = "lost Jobs", Value = lostJobs, Color = "#109618" });//"#109618"));  // lost job
//                //    result.Add(new ChartData { Key = "Quote Give", Value = quoteJobs, Color = "#3366cc" });//#3366cc"));   // quote give
//                //    result.Add(new ChartData { Key = "Lost Sale", Value = lostSales, Color = "#f1ca3a" });//"#f1ca3a")); // lost sale
//                //    result.Add(new ChartData { Key = "Sale Made", Value = saleMades, Color = "#dc3912" });//"#dc3912"));  // sale made  
//                //    result.Add(new ChartData { Key = "Complete", Value = completes, Color = "#5D225A" });//"#5D225A"));  // complete ?
//                //    result.Add(new ChartData { Key = "Service", Value = services, Color = "#cd853f" }); // service
//                //}

//               // return result;
//            }
//            catch (Exception ex)
//            {
//                EventLogger.LogEvent(ex);
//                throw;
//            }
//        }
//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="jobstatus"></param>
//        /// <returns></returns>
//        private string GetJobStatusColorforChart(string jobstatus)
//        {
//            string result = string.Empty;
//            switch (jobstatus.ToLower())
//            {
//                case "new jobs":
//                    result = "#53a8fb";
//                    break;
//                case "lost jobs":
//                    result = "#109618";
//                    break;
//                case "quote given":
//                    result = "#3366cc";
//                    break;
//                case "lost sale":
//                    result = "#f1ca3a";
//                    break;
//                case "sale made":
//                    result = "#dc3912";
//                    break;
//                case "complete":
//                    result = "#5D225A";
//                    break;
//                case "service":
//                    result = "#cd853f";
//                    break;
//                default:
//                    result = "#53a8fb";
//                    break;

//            }
//            return result;
//        }
//        /// <summary>
//        /// Gets report on _leadCreated status
//        /// </summary>
//        /// <param name="startDate">Created start date</param>
//        /// <param name="endDate">Created end date</param>
//        /// <param name="franchiseId">Optionally filter by franchise id</param>        
//        /// <exception cref="System.UnauthorizedAccessException"></exception>
//        public List<ChartData> GetLeadStatus(DateTime startDate, DateTime endDate, int? franchiseId = null)
//        {
//            if (!ChartsPermission.CanRead)
//                throw new UnauthorizedAccessException(Unauthorized);

//            Util.SortDate(ref startDate, ref endDate);
//            try
//            {
//                //set the franchise id to match the user if its not provided or in case they pass in a different franchise from the current user
//                //we only allow franchise id to be null if it is for control panel.  null franchise id will retrieve data from all franchise.
//                if (AuthorizedUser.FranchiseId.HasValue)
//                {
//                    franchiseId = AuthorizedUser.FranchiseId;
//                }

//                List<ChartData> result = new List<ChartData>();

//                var statuses = (from j in CRMDBContext.Leads
//                                where j.IsDeleted == false && (j.FranchiseId == franchiseId) &&
//                                        DbFunctions.TruncateTime(j.CreatedOnUtc) >= startDate.Date && DbFunctions.TruncateTime(j.CreatedOnUtc) <= endDate.Date
//                                group j by j.LeadStatusId into grouped
//                                select new { grouped.Key, Count = grouped.Count() }).ToList();

//                var lostStatusIds = CacheManager.LeadStatusTypeCollection.Where(w => w.Id == 2 || w.ParentId == 2).Select(s => s.Id).ToList();
//                var salesStatusIds = CacheManager.LeadStatusTypeCollection.Where(w => w.Id == 5).Select(s => s.Id).ToList();
//                var hotStatusIds = CacheManager.LeadStatusTypeCollection.Where(w => w.Id == 4 || w.ParentId == 4).Select(s => s.Id).ToList();
//                var warmStatusIds = CacheManager.LeadStatusTypeCollection.Where(w => w.Id == 3 || w.ParentId == 3).Select(s => s.Id).ToList();

//                int currentTotal = statuses != null && statuses.Count > 0 ? statuses.Sum(s => s.Count) : 0,
//                    currentNew = 0,
//                    currentSales = 0,
//                    currentLost = 0,
//                    currentWarm = 0,
//                    currentHot = 0,
//                    currentOther = 0;

//                if (currentTotal > 0)
//                {
//                    currentNew = statuses.Where(w => w.Key == 1).Sum(s => s.Count);
//                    currentHot = statuses.Where(w => hotStatusIds.Contains(w.Key)).Sum(s => s.Count);
//                    currentWarm = statuses.Where(w => warmStatusIds.Contains(w.Key)).Sum(s => s.Count);
//                    currentSales = statuses.Where(w => salesStatusIds.Contains(w.Key)).Sum(s => s.Count);
//                    currentLost = statuses.Where(w => lostStatusIds.Contains(w.Key)).Sum(s => s.Count);
//                    currentOther = currentTotal - currentNew - currentSales - currentLost - currentHot - currentWarm;

//                    result.Add(new ChartData { Key = "New Leads", Value = currentNew, Color = "#3366cc" });//"#53a8fb")); // new job
//                    result.Add(new ChartData { Key = "Sales Made", Value = currentSales, Color = "#5cb85c" });//"#109618")); // lost job
//                    result.Add(new ChartData { Key = "Lost Sales", Value = currentLost, Color = "#5bc0de" });//#3366cc")); // quote give
//                    result.Add(new ChartData { Key = "Warm Leads", Value = currentWarm, Color = "#eea236" });//"#f1ca3a")); // lost sale
//                    result.Add(new ChartData { Key = "Hot Leads", Value = currentHot, Color = "#d43f3a" });//"#dc3912"));  // sale made  // complete ?
//                    result.Add(new ChartData { Key = "Others", Value = currentOther, Color = "#cd853f" }); // service
//                }

//                return result;
//            }
//            catch (Exception ex)
//            {
//                EventLogger.LogEvent(ex);
//                throw;
//            }
//        }

//        /// <summary>
//        /// Gets report on sales representative. TODO: Recode in SQL Stored Procedure
//        /// </summary>
//        /// <param name="startDate">Contracted start date</param>
//        /// <param name="endDate">Contracted end date</param>
//        /// <param name="franchiseId">Optionally filter by franchise id</param>
//        /// <param name="topNRecord">Number of records to return</param>
//        /// <exception cref="System.UnauthorizedAccessException"></exception>
//        public List<ChartData> GetSalesRep(DateTime startDate, DateTime endDate, int? franchiseId = null, int topNRecord = 5)
//        {
//            if (!ChartsPermission.CanRead || franchiseId <= 0)
//                throw new UnauthorizedAccessException(Unauthorized);

//            List<ChartData> result = new List<ChartData>();

//            //swap the dates if they are reverse
//            Util.SortDate(ref startDate, ref endDate);

//            try
//            {
//                //set the franchise id to match the user if its not provided or in case they pass in a different franchise from the current user
//                //we only allow franchise id to be null if it is for control panel.  null franchise id will retrieve data from all franchise.
//                if (AuthorizedUser.FranchiseId.HasValue)
//                {
//                    franchiseId = AuthorizedUser.FranchiseId;
//                }

//                var List = GetSalesAgentForDashBoard(startDate, endDate, franchiseId, null).OrderByDescending(x => x.Total).Take(5).ToList();

//                foreach (var item in List)
//                {

//                    if (item.ColorId == null) item.ColorId = 1;
//                    var color = CacheManager.ColorPalettes.FirstOrDefault(f => f.ColorId == item.ColorId);

//                    item.Color = color.BGColorToHex();


//                    var newItem = new ChartData
//                    {
//                        Key = item.SalesPerson,
//                        Value = item.Total,
//                        Color = item.Color

//                    };
//                    result.Add(newItem);
//                }

//                return result; /// result;

//            }
//            catch (Exception ex) { EventLogger.LogEvent(ex); throw; }


//        }

//        public List<KeyValuePair<string, decimal>> GetLeadBySource(DateTime startDate, DateTime endDate)
//        {
//            //

//            int? franchiseId = 0;
//            if (!ChartsPermission.CanRead)
//                throw new UnauthorizedAccessException(Unauthorized);
//            franchiseId = SessionManager.CurrentFranchise.FranchiseId;
//            Dictionary<string, decimal> myDictionary = new Dictionary<string, decimal>(StringComparer.OrdinalIgnoreCase);
//            Util.SortDate(ref startDate, ref endDate);
//            try
//            {
//                var List = GetLeadBySourceForDashBoard(startDate, endDate, franchiseId, null);
//                var result = ConertResultToList(myDictionary, List);
//                return result;
//            }
//            catch (Exception ex)
//            {
//                EventLogger.LogEvent(ex);
//                throw;
//            }
//        }

//        public List<ChartData> GetSalesByVendor(DateTime startDate, DateTime endDate)
//        {
//            int? franchiseId = 0;
//            if (!ChartsPermission.CanRead)
//                throw new UnauthorizedAccessException(Unauthorized);
//            franchiseId = SessionManager.CurrentFranchise.FranchiseId;
//            Util.SortDate(ref startDate, ref endDate);
//            try
//            {
//                var datagroup = GetVendorSalesForDashBoard(startDate, endDate, franchiseId, null).GroupBy(x => x.KeyName).ToList();
//                return ConvertChartDataGroups(datagroup);
//            }
//            catch (Exception ex)
//            {
//                EventLogger.LogEvent(ex);
//                throw;
//            }
//        }

//        public List<ChartData> GetSalesByProduct(DateTime startDate, DateTime endDate)
//        {
//            int? franchiseId = 0;
//            if (!ChartsPermission.CanRead)
//                throw new UnauthorizedAccessException(Unauthorized);
//            franchiseId = SessionManager.CurrentFranchise.FranchiseId;
//            Util.SortDate(ref startDate, ref endDate);
//            try
//            {
//                var datagroup = GetProductSalesForDashBoard(startDate, endDate, franchiseId, null).GroupBy(x => x.KeyName).ToList();
//                return ConvertChartDataGroups(datagroup);
//            }
//            catch (Exception ex)
//            {
//                EventLogger.LogEvent(ex);
//                throw;
//            }
//        }

//        private List<ChartData> ConvertChartDataGroups(List<IGrouping<string, SalesData>> datagroup)
//        {
//            List<ChartData> list = new List<ChartData>();
//            List<ChartData> result = new List<ChartData>();
//            foreach (var g in datagroup)
//            {
//                var key = g.Key;
//                // var total = Math.Round((decimal)g.Sum(x => x.Subtotal + x.SurchargeTotal - x.DiscountTotal), 2);
//                var total = Math.Round((decimal)g.Sum(x => x.Subtotal), 2);
//                list.Add(new ChartData
//                {
//                    Key = g.Key,
//                    Value = total
//                });
//            }


//            var orderedlist = list.OrderByDescending(x => x.Value).ToList();

//            int counter = 0;
//            decimal othercount = 0m;
//            foreach (var one in orderedlist)
//            {
//                if (counter < 4)
//                    result.Add(one);
//                else
//                {
//                    othercount += (decimal)one.Value;
//                    if (counter == orderedlist.Count() - 1)
//                        result.Add(new ChartData() { Key = "Others", Value = othercount });
//                }
//                counter++;
//            }

//            return result;
//        }

//        private static List<KeyValuePair<string, decimal>> ConertResultToList(Dictionary<string, decimal> myDictionary, List<LeadSourceChartData> List)
//        {
//            var sourcegroup = List.GroupBy(x => x.name);
//            foreach (var i in sourcegroup)
//            {
//                var test = i.Key;
//                if (!myDictionary.ContainsKey(i.Key))
//                    myDictionary.Add(i.Key.ToString(), 0.00M);
//            }

//            var innergroup = List.GroupBy(x => x.leadid);
//            foreach (var item in innergroup)
//            {
//                var totalsource = item.Count();
//                foreach (var one in item)
//                {
//                    if (totalsource == 1) //for lead with one source
//                    {
//                        myDictionary[one.name] = myDictionary[one.name] + 1;
//                    }
//                    else //for lead with multiple sources
//                    {
//                        decimal number = (decimal)1 / totalsource;
//                        myDictionary[one.name] = myDictionary[one.name] + number;
//                    }

//                }
//            }

//            var result = new List<KeyValuePair<string, decimal>>();
//            var orderedlist = myDictionary.ToList().OrderByDescending(x => x.Value);
//            int counter = 0;
//            decimal othercount = 0m;
//            foreach (var one in orderedlist)
//            {
//                if (counter < 4)
//                    result.Add(one);
//                else
//                {
//                    othercount += one.Value;
//                    if (counter == orderedlist.Count() - 1)
//                        result.Add(new KeyValuePair<string, decimal>("Others", othercount));
//                }
//                counter++;
//            }
//            return result;
//        }

//        public List<ChartData> GetSaleAgent(DateTime startDate, DateTime endDate)
//        {
//            List<ChartData> result = new List<ChartData>();
//            int? franchiseId = 0;
//            if (!ChartsPermission.CanRead)
//                throw new UnauthorizedAccessException(Unauthorized);

//            franchiseId = SessionManager.CurrentFranchise.FranchiseId;

//            Util.SortDate(ref startDate, ref endDate);
//            try
//            {
//                var List = GetSalesAgentForDashBoard(startDate, endDate, franchiseId, null).OrderByDescending(x => x.Total).Take(5).ToList();



//                foreach (var item in List)
//                {

//                    if (item.ColorId == null) item.ColorId = 1;
//                    var color = CacheManager.ColorPalettes.FirstOrDefault(f => f.ColorId == item.ColorId);

//                    item.Color = color.BGColorToHex();


//                    var newItem = new ChartData
//                    {
//                         Key = item.SalesPerson,
//                         Value = item.Total,
//                         Color = item.Color

//                    };
//                    result.Add(newItem);
//                }

//                return result; /// result;
//            }
//            catch (Exception ex)
//            {
//                EventLogger.LogEvent(ex);
//                throw;
//            }
//        }

//        private List<SaleAgentData> GetSalesAgentForDashBoard(DateTime startDate, DateTime endDate, int? franchiseId, int? salesAgentId)
//        {

//            return CRMDBContext.Database.SqlQuery<SaleAgentData>("SELECT SalesPersonid , SalesPerson, ColorId, Count, Total FROM CRM.fntChart_SalesAgent(@p0, @p1, @p2, @p3)",
//                startDate, endDate, franchiseId, salesAgentId).ToList();
//        }

//        private List<SalesData> GetJobStatusForDashBoard(DateTime startDate, DateTime endDate, int? franchiseId, int? statusId)
//        {
//            return CRMDBContext.Database.SqlQuery<SalesData>("SELECT LeadId, JobId, JobNumber, JobStatusId, JobStatus, KeyId, [KeyName] FROM CRM.fntCharts_JobStatus(@p0, @p1, @p2, @p3)",
//                startDate, endDate, franchiseId, statusId).ToList();
//        }

//        private List<LeadSourceChartData> GetLeadBySourceForDashBoard(DateTime startDate, DateTime endDate, int? franchiseId, int? statusId)
//        {
//            var test = CRMDBContext.Database.SqlQuery<LeadSourceChartData>("SELECT leadid, name FROM CRM.fntChart_LeadBySource(@p0, @p1, @p2, @p3)",
//                startDate, endDate, franchiseId, statusId).ToList();
//            return test;
//        }

//        private List<SalesData> GetVendorSalesForDashBoard(DateTime startDate, DateTime endDate, int? franchiseId, int? mfgId)
//        {
//            return CRMDBContext.Database.SqlQuery<SalesData>("SELECT LeadId, JobId, JobNumber, JobStatusId, JobStatus, KeyId, [KeyName], FirstName, LastName, Subtotal, SurchargeTotal, DiscountTotal, CreatedOnUtc, ContractedOnUtc, FirstAppointment FROM CRM.fntChart_VendorSales(@p0, @p1, @p2, @p3)",
//                startDate, endDate, franchiseId, mfgId).ToList();
//        }

//        private List<SalesData> GetProductSalesForDashBoard(DateTime startDate, DateTime endDate, int? franchiseId, int? mfgId)
//        {
//            return CRMDBContext.Database.SqlQuery<SalesData>("SELECT LeadId, JobId, JobNumber, JobStatusId, JobStatus, GuidKeyId, [KeyName], FirstName, LastName, Subtotal, SurchargeTotal, DiscountTotal, CreatedOnUtc, ContractedOnUtc, FirstAppointment FROM CRM.fntChart_ProductSales(@p0, @p1, @p2, @p3)",
//                startDate, endDate, franchiseId, mfgId).ToList();
//        }

//        #region Summary Report

//        /// <summary>
//        /// Gets summary report by report type
//        /// </summary>
//        /// <param name="reportType">Type of report, options are: Category, Territory, SalesAgent, or Source</param>
//        /// <param name="startDate">Contract start date</param>
//        /// <param name="endDate">Contract end date</param>
//        /// <param name="franchiseId">Optionally filter by franchise id</param>        
//        /// <exception cref="System.UnauthorizedAccessException"></exception>
//        public List<ReportData> GetSummary(string reportType, DateTime startDate, DateTime endDate, int? franchiseId = null)
//        {
//            if (!ChartsPermission.CanRead || (franchiseId.HasValue && franchiseId <= 0))
//                throw new UnauthorizedAccessException(Unauthorized);

//            //swap the dates if they are reverse
//            Util.SortDate(ref startDate, ref endDate);

//            try
//            {
//                //set the franchise id to match the user if its not provided or in case they pass in a different franchise from the current user
//                //we only allow franchise id to be null if it is for control panel.  a null franchise id will retrieve data from all franchise.
//                if (AuthorizedUser.FranchiseId.HasValue)
//                {
//                    franchiseId = AuthorizedUser.FranchiseId;
//                }

//                switch (reportType.ToLower())
//                {
//                    case "category": return GetCategorySummary(startDate, endDate, franchiseId);
//                    case "type": return GetTypeSummary(startDate, endDate, franchiseId);
//                    case "product": return GetProductSummary(startDate, endDate, franchiseId);
//                    case "vendor": return GetVendorSummary(startDate, endDate, franchiseId);
//                    case "territory": return GetTerritorySummary(startDate, endDate, franchiseId);
//                    case "salesagent": return GetSalesAgentSummary(startDate, endDate, franchiseId);
//                    case "source": return GetSourceSummary(startDate, endDate, franchiseId);
//                    default: return null;
//                }
//            }
//            catch (Exception ex) { EventLogger.LogEvent(ex); throw; }
//        }

//        /// <summary>
//        /// Helper method for GetSummary
//        /// </summary>
//        private List<ReportData> GetCategorySummary(DateTime startDate, DateTime endDate, int? franchiseId = null)
//        {
//            return CRMDBContext.Database.SqlQuery<ReportData>("select [Key], [Month], [Day], [Year], [Sum] from crm.fntReports_CategorySummary(@p0, @p1, @p2)",
//                    startDate.Date, endDate.Date, franchiseId).ToList();
//        }

//        /// <summary>
//        /// Helper method for GetSummary
//        /// </summary>
//        private List<ReportData> GetTypeSummary(DateTime startDate, DateTime endDate, int? franchiseId = null)
//        {
//            return CRMDBContext.Database.SqlQuery<ReportData>("select Id, [Key], [Month], [Day], [Year], [Sum] from crm.fntReports_TypeSummary(@p0, @p1, @p2)",
//                    startDate.Date, endDate.Date, franchiseId).ToList();
//        }

//        /// <summary>
//        /// Helper method for GetSummary
//        /// </summary>
//        private List<ReportData> GetProductSummary(DateTime startDate, DateTime endDate, int? franchiseId = null)
//        {
//            return CRMDBContext.Database.SqlQuery<ReportData>("select GuidId, [Key], [Month], [Day], [Year], [Sum] from crm.fntReports_ProductSummary(@p0, @p1, @p2)",
//                    startDate.Date, endDate.Date, franchiseId).ToList();
//        }

//        /// <summary>
//        /// Helper method for GetSummary
//        /// </summary>
//        private List<ReportData> GetVendorSummary(DateTime startDate, DateTime endDate, int? franchiseId = null)
//        {
//            return CRMDBContext.Database.SqlQuery<ReportData>("select Id, [Key], [Month], [Day], [Year], [Sum] from crm.fntReports_VendorSummary(@p0, @p1, @p2)",
//                    startDate.Date, endDate.Date, franchiseId).ToList();
//        }

//        /// <summary>
//        /// Helper method for GetSummary
//        /// </summary>
//        private List<ReportData> GetTerritorySummary(DateTime startDate, DateTime endDate, int? franchiseId = null)
//        {
//            return CRMDBContext.Database.SqlQuery<ReportData>("select Id, [Key], [Month], [Day], [Year], [Sum] from crm.fntReports_TerritorySummary(@p0, @p1, @p2)",
//                    startDate.Date, endDate.Date, franchiseId).ToList();
//        }

//        /// <summary>
//        /// Helper method for GetSummary
//        /// </summary>
//        private List<ReportData> GetSalesAgentSummary(DateTime startDate, DateTime endDate, int? franchiseId = null)
//        {
//            return CRMDBContext.Database.SqlQuery<ReportData>("select Id, [Key], [Month], [Day], [Year], [Sum] from crm.fntReports_SalesAgentSummary(@p0, @p1, @p2)",
//                    startDate.Date, endDate.Date, franchiseId).ToList();
//        }

//        /// <summary>
//        /// Helper method for GetSummary
//        /// </summary>
//        public List<ReportData> GetSourceSummary(DateTime startDate, DateTime endDate, int? franchiseId = null)
//        {
//            return CRMDBContext.Database.SqlQuery<ReportData>("select [Id], [Key], [Month], [Day], [Year], [Sum] from crm.fntReports_SourceSummary(@p0, @p1, @p2)",
//                    startDate.Date, endDate.Date, franchiseId).ToList();
//        }

//        #endregion

//        #region Sales Report

//        /// <summary>
//        /// Gets sales report by report type
//        /// </summary>
//        /// <param name="reportType">Type of report, options are: Category, SalesAgent, City, Zipcode, Source, Territory, or JobStatus</param>
//        /// <param name="startDate">Either Contract or Created start date, depending on report type</param>
//        /// <param name="endDate">Either Contract or Created end date, depending on report type</param>
//        /// <param name="franchiseId">Optionally filter by franchise id</param>
//        /// <param name="additionalFilterId">Additional id paramter to filter, such as categoryId, JobStatusId</param>        
//        /// <exception cref="System.UnauthorizedAccessException"></exception>
//        public List<SalesData> GetSales(string reportType, DateTime startDate, DateTime endDate, int? franchiseId = null, int? additionalFilterId = null, Guid? productGuidfilterId = null)
//        {
//            if (!ChartsPermission.CanRead || (franchiseId.HasValue && franchiseId <= 0))
//                throw new UnauthorizedAccessException(Unauthorized);

//            //swap the dates if they are reverse
//            Util.SortDate(ref startDate, ref endDate);

//            try
//            {
//                //set the franchise id to match the user if its not provided or in case they pass in a different franchise from the current user
//                //we only allow franchise id to be null if it is for control panel.  a null franchise id will retrieve data from all franchise.
//                if (AuthorizedUser.FranchiseId.HasValue)
//                {
//                    franchiseId = AuthorizedUser.FranchiseId;
//                }

//                switch (reportType.ToLower())
//                {
//                    case "category": return GetCategorySales(startDate, endDate, franchiseId, additionalFilterId);
//                    case "type": return GetTypeSales(startDate, endDate, franchiseId, additionalFilterId);
//                    case "product": return GetProductSales(startDate, endDate, franchiseId, productGuidfilterId);
//                    case "vendor": return GetVendorSales(startDate, endDate, franchiseId, additionalFilterId);
//                    case "salesagent": return GetSalesAgentSales(startDate, endDate, franchiseId, additionalFilterId);
//                    case "city":
//                    case "zipcode": return GetZipCodeSales(startDate, endDate, franchiseId);
//                    case "source": return GetSourceSales(startDate, endDate, franchiseId, additionalFilterId);
//                    case "territory": return GetTerritorySales(startDate, endDate, franchiseId, additionalFilterId);
//                    case "jobstatus": return GetJobStatusSales(startDate, endDate, franchiseId, additionalFilterId);
//                    default: return null;
//                }
//            }
//            catch (Exception ex) { EventLogger.LogEvent(ex); throw; }
//        }

//        ///<summary>
//        /// Helper method for GetSales
//        /// </summary>
//        private List<SalesData> GetCategorySales(DateTime startDate, DateTime endDate, int? franchiseId, int? categoryId)
//        {
//            return CRMDBContext.Database.SqlQuery<SalesData>("SELECT LeadId, JobId, JobNumber, JobStatusId, JobStatus, KeyId, [KeyName], FirstName, LastName, Subtotal, SurchargeTotal, DiscountTotal, CreatedOnUtc, ContractedOnUtc, FirstAppointment FROM CRM.fntReports_CategorySales(@p0, @p1, @p2, @p3)",
//                startDate, endDate, franchiseId, categoryId).ToList();
//        }

//        /// <summary>
//        /// Helper method for GetSales
//        /// </summary>
//        private List<SalesData> GetTypeSales(DateTime startDate, DateTime endDate, int? franchiseId, int? typeId)
//        {
//            return CRMDBContext.Database.SqlQuery<SalesData>("SELECT LeadId, JobId, JobNumber, JobStatusId, JobStatus, KeyId, [KeyName], FirstName, LastName, Subtotal, SurchargeTotal, DiscountTotal, CreatedOnUtc, ContractedOnUtc, FirstAppointment FROM CRM.fntReports_TypeSales(@p0, @p1, @p2, @p3)",
//                startDate, endDate, franchiseId, typeId).ToList();
//        }

//        /// <summary>
//        /// Helper method for GetSales
//        /// </summary>
//        private List<SalesData> GetProductSales(DateTime startDate, DateTime endDate, int? franchiseId, Guid? productGuid)
//        {
//            return CRMDBContext.Database.SqlQuery<SalesData>("SELECT LeadId, JobId, JobNumber, JobStatusId, JobStatus, GuidKeyId, [KeyName], FirstName, LastName, Subtotal, SurchargeTotal, DiscountTotal, CreatedOnUtc, ContractedOnUtc, FirstAppointment FROM CRM.fntReports_ProductSales(@p0, @p1, @p2, @p3)",
//                  startDate, endDate, franchiseId, productGuid).ToList();
//        }

//        /// <summary>
//        /// Helper method for GetSales
//        /// </summary>
//        private List<SalesData> GetVendorSales(DateTime startDate, DateTime endDate, int? franchiseId, int? mfgId)
//        {
//            return CRMDBContext.Database.SqlQuery<SalesData>("SELECT LeadId, JobId, JobNumber, JobStatusId, JobStatus, KeyId, [KeyName], FirstName, LastName, Subtotal, SurchargeTotal, DiscountTotal, CreatedOnUtc, ContractedOnUtc, FirstAppointment FROM CRM.fntReports_VendorSales(@p0, @p1, @p2, @p3)",
//                startDate, endDate, franchiseId, mfgId).ToList();
//        }

//        /// <summary>
//        /// Helper method for GetSales
//        /// </summary>
//        private List<SalesData> GetSalesAgentSales(DateTime startDate, DateTime endDate, int? franchiseId, int? personId)
//        {
//            return CRMDBContext.Database.SqlQuery<SalesData>("SELECT LeadId, JobId, JobNumber, JobStatusId, JobStatus, KeyId, [KeyName], FirstName, LastName, Subtotal, SurchargeTotal, DiscountTotal, CreatedOnUtc, ContractedOnUtc, FirstAppointment FROM CRM.fntReports_SalesAgentSales(@p0, @p1, @p2, @p3)",
//                startDate, endDate, franchiseId, personId).ToList();
//        }

//        /// <summary>
//        /// Helper method for GetSales
//        /// </summary>
//        private List<SalesData> GetTerritorySales(DateTime startDate, DateTime endDate, int? franchiseId, int? territoryId)
//        {
//            return CRMDBContext.Database.SqlQuery<SalesData>("SELECT LeadId, JobId, JobNumber, JobStatusId, JobStatus, KeyId, [KeyName], FirstName, LastName, Subtotal, SurchargeTotal, DiscountTotal, CreatedOnUtc, ContractedOnUtc, FirstAppointment FROM CRM.fntReports_TerritorySales(@p0, @p1, @p2, @p3)",
//                startDate, endDate, franchiseId, territoryId).ToList();
//        }

//        /// <summary>
//        /// Helper method for GetSales
//        /// </summary>
//        private List<SalesData> GetZipCodeSales(DateTime startDate, DateTime endDate, int? franchiseId)
//        {
//            return CRMDBContext.Database.SqlQuery<SalesData>("SELECT LeadId, JobId, JobNumber, JobStatusId, JobStatus, ZipCode, City, [State], FirstName, LastName, Subtotal, SurchargeTotal, DiscountTotal, CreatedOnUtc, ContractedOnUtc, FirstAppointment FROM CRM.fntReports_ZipCodeSales(@p0, @p1, @p2)",
//                startDate, endDate, franchiseId).ToList();
//        }

//        /// <summary>
//        /// Used by the Sales summary report. 
//        /// </summary>
//        /// <param name="startDate"></param>
//        /// <param name="endDate"></param>
//        /// <param name="franchiseId"></param>
//        /// <param name="sourceId"></param>
//        /// <returns></returns>
//        private List<SalesData> GetSourceSales(DateTime startDate, DateTime endDate, int? franchiseId, int? sourceId)
//        {
//            /// 
//            //var cacheKey = string.Format("GetSourceSales_{0}_{1}_{2}_{3}", startDate.Ticks, endDate.Ticks, franchiseId, sourceId);

//            //var result = (CacheManager.GetCache<List<SalesData>>(cacheKey));
//            //if (result != null)
//            //{
//            //    return result;
//            //}

//            List<SalesData> result = CRMDBContext.Database.SqlQuery<SalesData>("SELECT LeadId, JobId, JobNumber, JobStatusId, JobStatus, KeyId, KeyName, FirstName, LastName, JobCount, Subtotal, SurchargeTotal, DiscountTotal, CreatedOnUtc, ContractedOnUtc, FirstAppointment FROM CRM.fntReports_SourceSales(@p0, @p1, @p2, @p3)",
//                startDate, endDate, franchiseId, sourceId).ToList();

//            //CacheManager.SetCache(cacheKey, result, TimeSpan.FromSeconds(600));

//            return result;
//        }
//        /// <summary>
//        /// Get pre-aggregated data just as GetSourceSales but faster.
//        /// </summary>
//        /// <param name="startDate"></param>
//        /// <param name="endDate"></param>
//        /// <param name="franchiseId"></param>
//        /// <param name="sourceId"></param>
//        /// <returns></returns>
//        private List<ChartData> dw_GetSourceSales(DateTime startDate, DateTime endDate, int? franchiseId, int? sourceId)
//        {
//            var cacheKey = string.Format("{0}_{1}_{2}_{3}", startDate.Ticks, endDate.Ticks, franchiseId, sourceId);
//            var result = (CacheManager.GetCache<List<ChartData>>(cacheKey));
//            var dto = new List<ChartData>();

//            if (result != null)
//            {
//                return result;
//            }

//            using (SqlConnection conn = new SqlConnection(EntityCnxStringBuilder.ProviderConnectionString))
//            {
//                conn.Open();
//                result = conn.Query<ChartData>("select [Key],[Value],[Color] from [DW].[fnt_SourceSales_Monthly] (@startDate, @endDate, @franchiseId, @sourceId)",
//                    new {
//                        startDate = startDate,
//                        endDate = endDate,
//                        franchiseId = franchiseId,
//                        sourceId = sourceId,
//                    }).ToList();
//            }
//            CacheManager.SetCache(cacheKey, result, TimeSpan.FromSeconds(600));

//            return result;
//        }

//        /// <summary>
//        /// Helper method for GetSales
//        /// </summary>
//        private List<SalesData> GetJobStatusSales(DateTime startDate, DateTime endDate, int? franchiseId, int? statusId)
//        {
//            return CRMDBContext.Database.SqlQuery<SalesData>("SELECT LeadId, JobId, JobNumber, JobStatusId, JobStatus, KeyId, [KeyName], FirstName, LastName, Subtotal, SurchargeTotal, DiscountTotal, CreatedOnUtc, ContractedOnUtc, FirstAppointment FROM CRM.fntReports_JobStatusSales(@p0, @p1, @p2, @p3)",
//                startDate, endDate, franchiseId, statusId).ToList();
//        }


//        //private List<SalesData> GetJobStatusForDashBoard(DateTime startDate, DateTime endDate, int? franchiseId, int? statusId)
//        //{
//        //    return CRMDBContext.Database.SqlQuery<SalesData>("SELECT LeadId, JobId, JobNumber, JobStatusId, JobStatus, KeyId, [KeyName] FROM CRM.fntCharts_JobStatus(@p0, @p1, @p2, @p3)",
//        //        startDate, endDate, franchiseId, statusId).ToList();
//        //}

//        public List<SalesBetaSalesAgentData> GetSalesBetaSalesAgent(DateTime startDate, DateTime endDate, int franchiseId, CommercialTypes commercial)
//        {
//            if (!ChartsPermission.CanRead || (franchiseId <= 0))
//                throw new UnauthorizedAccessException(Unauthorized);

//            //swap the dates if they are reverse
//            Util.SortDate(ref startDate, ref endDate);

//            try
//            {
//                //set the franchise id to match the user if its not provided or in case they pass in a different franchise from the current user
//                //we only allow franchise id to be null if it is for control panel.  a null franchise id will retrieve data from all franchise.
//                if (AuthorizedUser.FranchiseId.HasValue)
//                {
//                    franchiseId = AuthorizedUser.FranchiseId.Value;
//                }
//                var data = CRMDBContext.Database.SqlQuery<SalesBetaSalesAgentData>(
//                            "exec [CRM].[spReports_SalesAgent] @startDate, @endDate, @franchiseId, @commercialType",
//                            new SqlParameter("@startDate", startDate),
//                            new SqlParameter("@endDate", endDate),
//                            new SqlParameter("@franchiseId", franchiseId),
//                            new SqlParameter("@commercialType", (int)commercial)).ToList();

//                foreach (var item in data)
//                {
//                    item.StartDate = startDate.ToShortDateString();
//                    item.EndDate = endDate.ToShortDateString();
//                }

//                return data;
//            }
//            catch (Exception ex) { EventLogger.LogEvent(ex); throw; }
//        }

//        public List<SalesBetaMarketingPerformanceData> GetSalesBetaMarketingPerformanceData(DateTime startDate,
//            DateTime endDate, int franchiseId, CommercialTypes commercial)
//        {
//            if (!ChartsPermission.CanRead || (franchiseId <= 0))
//                throw new UnauthorizedAccessException(Unauthorized);

//            //swap the dates if they are reverse
//            Util.SortDate(ref startDate, ref endDate);

//            try
//            {
//                //set the franchise id to match the user if its not provided or in case they pass in a different franchise from the current user
//                //we only allow franchise id to be null if it is for control panel.  a null franchise id will retrieve data from all franchise.
//                if (AuthorizedUser.FranchiseId.HasValue)
//                {
//                    franchiseId = AuthorizedUser.FranchiseId.Value;
//                }
//                //var data = new List<SalesBetaMarketingPerformanceData>();
//                //var f1 = new SalesBetaMarketingPerformanceData();
//                //f1.Source = "Database";
//                //f1.Campaign = "New Home Owner";
//                //f1.TotalLeads = 7;
//                //f1.TotalValidLeads = 5;
//                //f1.NumberOfSales = 3;
//                //f1.Sales = 23212.00M;
//                //f1.ConversionRate = 71;
//                //f1.ClosingRate = 60;
//                //f1.AvgSales = 7737.33M;
//                //f1.RevenuePerLead = 4642.40M;
//                //f1.TotalNumQuotes = 6;
//                //f1.TotalQuotes = 25000M;
//                ////f1.AvgQuote = 5000M;
//                //f1.QuoteToClose = 93;
//                //f1.CampaignCost = 700;
//                //f1.CostPerLead = 140;
//                //f1.CostPerSale = 233.33M;
//                //f1.ROI = 33.16M;

//                //data.Add(f1);
//                //var data = CRMDBContext.Database.SqlQuery<SalesBetaMarketingPerformanceData>(
//                //          "select * from _SalesReportSourceTable").ToList();
//                //return data;

//                var data = CRMDBContext.Database.SqlQuery<SalesBetaMarketingPerformanceData>(
//                    "exec [CRM].[spReport_Sales_Source] @startDate, @endDate, @franchiseId, @commercialType",
//                    new SqlParameter("@startDate", startDate),
//                    new SqlParameter("@endDate", endDate),
//                    new SqlParameter("@franchiseId", franchiseId), new SqlParameter("@commercialType", (int) commercial))
//                    .ToList();

//                var sources =
//                    CRMDBContext.Sources.Where(v => v.FranchiseId == franchiseId || v.FranchiseId == null).ToList();
//                foreach (var item in data)
//                {

//                    item.StartDate = startDate.ToShortDateString();
//                    item.EndDate = endDate.ToShortDateString();

//                    var source = sources.FirstOrDefault(v => v.Name == item.Campaign);
//                    if (source != null)
//                        item.SourceId = source.SourceId;
//                    source = sources.FirstOrDefault(v => v.Name == item.Source);

//                    if (source != null)
//                        item.ParentSourceId = source.SourceId;
//                }
//                return data;

//            }
//            catch (Exception ex)
//            {
//                EventLogger.LogEvent(ex);
//                throw;
//            }
//        }

//        public List<SalesBetaMonthlyStatisticsData> GetSalesBetaMonthlyStatisticsData(DateTime startDate, DateTime endDate, int franchiseId, CommercialTypes commercial)
//        {
//            if (!ChartsPermission.CanRead || (franchiseId <= 0))
//                throw new UnauthorizedAccessException(Unauthorized);

//            //swap the dates if they are reverse
//            Util.SortDate(ref startDate, ref endDate);

//            try
//            {
//                //set the franchise id to match the user if its not provided or in case they pass in a different franchise from the current user
//                //we only allow franchise id to be null if it is for control panel.  a null franchise id will retrieve data from all franchise.
//                if (AuthorizedUser.FranchiseId.HasValue)
//                {
//                    franchiseId = AuthorizedUser.FranchiseId.Value;
//                }

//                var data = CRMDBContext.Database.SqlQuery<SalesBetaMonthlyStatisticsData>(
//                          "exec [CRM].[spReports_MonthlyStatistics] @startDate, @endDate, @franchiseId, @commercialType",
//                          new SqlParameter("@startDate", startDate),
//                          new SqlParameter("@endDate", endDate),
//                          new SqlParameter("@franchiseId", franchiseId), new SqlParameter("@commercialType", (int)commercial)).ToList();

//                foreach (var item in data)
//                {
//                    item.StartDate = startDate.ToShortDateString();
//                    item.EndDate = endDate.ToShortDateString();
//                }

//                return data;
//            }
//            catch (Exception ex) { EventLogger.LogEvent(ex); throw; }
//        }


//        #endregion

//        /// <summary>
//        /// Gets Monthly Sales Report
//        /// </summary>
//        /// <param name="territoryId">Territory id</param>
//        /// <param name="startDate">Contract start date</param>
//        /// <param name="endDate">Contract end date</param>
//        /// <exception cref="System.ArgumentException">
//        /// Invalid territory
//        /// or
//        /// Invalid year
//        /// </exception>
//        /// <exception cref="System.UnauthorizedAccessException"></exception>
//        public List<MSRData> GetMSR(int territoryId, DateTime startDate, DateTime endDate)
//        {
//            //swap the dates if they are reverse
//            Util.SortDate(ref startDate, ref endDate);

//            if (territoryId < 0)
//                throw new ArgumentException("Invalid territory");
//            //if (startDate.Year < Franchise.CreatedOnUtc.Year)
//            //    throw new ArgumentException("Invalid year");

//            if (!ChartsPermission.CanRead)
//                throw new UnauthorizedAccessException(Unauthorized);

//            try
//            {
//                //var result = CRMDBContext.Database.SqlQuery<MSRData>("select TerritoryId, Category_Type_Id, Category_Type, Cost, Sales, [Sum] from crm.fntReports_MSR(@p0, @p1, @p2)", startDate.Date, endDate.Date, territoryId).ToList();
//                return CRMDBContext.Database.SqlQuery<MSRData>("select TerritoryId, Category_Type_Id, Category_Type, Cost, Sales, [Sum] from crm.fntReports_MSR(@p0, @p1, @p2)",
//                    startDate.Date, endDate.Date, territoryId).OrderBy(x=>x.Category_Type).ToList();
//            }
//            catch (Exception ex) { EventLogger.LogEvent(ex); throw; }
//        }

//        public List<KeyAccountData> GetKeyAccount(DateTime startDate, DateTime endDate, int territoryId)
//        {
//            //swap the dates if they are reverse
//            Util.SortDate(ref startDate, ref endDate);

//            if (territoryId < 0)
//                throw new ArgumentException("Invalid territory");
//            //if (startDate.Year < Franchise.CreatedOnUtc.Year)
//            //    throw new ArgumentException("Invalid year");

//            if (!ChartsPermission.CanRead)
//                throw new UnauthorizedAccessException(Unauthorized);

//            try
//            {

//                var rawData = CRMDBContext.fntReports_KeyAccountPaystub(startDate, endDate, territoryId);

//                var results = rawData.Select(x =>
//                    new KeyAccountData
//                    {
//                        CustName = x.CustName,
//                        KAName = x.KAName,
//                        PayByAppointment = x.PayByAppointment.Value,
//                        ApptCount = x.ApptCount.Value,
//                        CorporateAmount = x.CorporateAmount.Value,
//                        CustPrice = x.CustPrice,
//                        AddChgs = x.AddChgs,
//                        PayByPercentOfSale = x.PayByPercentOfSale.Value,
//                        TerrName = x.TerrName,
//                        TerrCode = x.TerrCode,
//                        PayMethodType = x.PayMethodType.Value,
//                        KAAddress = x.KAAddress,
//                        KACSZ = x.KACSZ,
//                        KALocAddress = x.KALocAddress,
//                        KALocCSZ = x.KALocCSZ,
//                        Storename = x.Storename,
//                        LeadCount = x.LeadCount,
//                        PayByLead = x.PayByLead.Value,
//                        SaleCount = x.SaleCount,
//                        PayBySale = x.PayBySale.Value
//                    });

//                return results.ToList();
//            }
//            catch (Exception ex)
//            {
//                EventLogger.LogEvent(ex); throw;
//            }
//        }

//        /// <summary>
//        /// Gets Marketing report based on Source and known campaigns
//        /// </summary>
//        /// <param name="startDate">Contract start date</param>
//        /// <param name="endDate">Contract end date</param>
//        /// <param name="franchiseId">Optionally filter by franchise id</param>
//        /// <exception cref="System.UnauthorizedAccessException"></exception>
//        public List<MarketingData> GetMarketing(DateTime startDate, DateTime endDate, int? franchiseId = null)
//        {
//            //swap the dates if they are reverse
//            Util.SortDate(ref startDate, ref endDate);

//            if (!ChartsPermission.CanRead)
//                throw new UnauthorizedAccessException(Unauthorized);

//            try
//            {
//                return CRMDBContext.Database.SqlQuery<MarketingData>("select SourceId, SourceName, TotalSales, TotalSalesCount, TotalCount, TotalExpense, CAST((ROW_NUMBER() OVER(ORDER BY TotalSales DESC)) AS INT) Ranking FROM [CRM].[fntReports_SourceMarketing](@p0, @p1, @p2, null)",
//                    startDate.Date, endDate.Date, franchiseId).ToList();
//            }
//            catch (Exception ex) { EventLogger.LogEvent(ex); throw; }
//        }

//        public DateTime GetReportStartDate(int franchiseId)
//        {
//            try
//            {
//                return CRMDBContext.Database.SqlQuery<DateTime>("SELECT [CRM].[fnsReportStartDate](@p0)",
//                       franchiseId).FirstOrDefault();
//            }
//            catch (Exception ex) { EventLogger.LogEvent(ex); throw; }
//        }

//        public List<MlsData> GetMLS(string terrlist, DateTime startDate, DateTime endDate)
//        {
//            if (string.IsNullOrEmpty(terrlist))
//                throw new ArgumentException("Invalid territory");

//            if (!ChartsPermission.CanRead)
//                return new List<MlsData>();

//            try
//            {
//                var rawData = CRMDBContext.spMLSReportForExcel(startDate, endDate, terrlist);

//                var results = rawData.Select(x =>
//                    new MlsData
//                    {
//                        TerrCode = x.TerrCode,
//                        TerrName = x.TerrName,
//                        CustomerName = x.CustomerName,
//                        JobNumber = x.JobNumber,
//                        ItemName = x.ItemName,
//                        ItemDetail = x.ItemDetail,
//                        Quantity = x.Quantity,
//                        ContractedDate = DateTime.Parse(x.ContractedDate.ToString()).ToShortDateString(),
//                        Taxtotal = x.Taxtotal,
//                        ItemType = x.ItemType,
//                        IsTaxable = x.IsTaxable,
//                        SalePrice = x.SalePrice,
//                        UnitCost = x.UnitCost,
//                        ItemCostSubtotal = x.ItemCostSubtotal,
//                        ZipCode = x.ZipCode,
//                        ItemTaxPercent = x.ItemTaxPercent,
//                        ItemTaxAmount = x.ItemTaxAmount,
//                        TaxRateTotal = x.TaxRateTotal,
//                        ItemSubtotal = x.ItemSubtotal
//                    });

//                return results.ToList();
//            }
//            catch (Exception ex)
//            {
//                EventLogger.LogEvent(ex); throw;
//            }
//        }

//        public List<MlsDataSummary> GetMLSSummary(string terrlist, DateTime startDate, DateTime endDate)
//        {
//            if (string.IsNullOrEmpty(terrlist))
//                throw new ArgumentException("Invalid territory");

//            if (!ChartsPermission.CanRead)
//                return new List<MlsDataSummary>();

//            try
//            {
//                var rawData = CRMDBContext.spMLSReportForSummary(startDate, endDate, terrlist);

//                var results = rawData.Select(x =>
//                    new MlsDataSummary
//                    {
//                        TerrCode = x.TerrCode,
//                        TerrName = x.TerrName,
//                        CustomerName = x.CustomerName,
//                        JobNumber = x.JobNumber,
//                        ItemName = string.IsNullOrEmpty(x.ItemName) ? "" : x.ItemName.CapitalFirstLetterOfEachWord(),
//                        ItemDetail = x.ItemDetail,
//                        Quantity = x.Quantity,
//                        ContractedDate = DateTime.Parse(x.ContractedDate.ToString()).ToShortDateString(),
//                        Taxtotal = x.Taxtotal,
//                        ItemType = x.ItemType,
//                        IsTaxable = x.IsTaxable,
//                        SalePrice = x.SalePrice,
//                        UnitCost = x.UnitCost,
//                        ItemCostSubtotal = x.ItemCostSubtotal,
//                        ZipCode = x.ZipCode,
//                        ItemTaxPercent = x.ItemTaxPercent,
//                        ItemTaxAmount = x.ItemTaxAmount,
//                        TaxRateTotal = x.TaxRateTotal,
//                        ItemSubtotal = x.ItemSubtotal,
//                        //Vendor = string.IsNullOrEmpty(x.Vendor) ? "" : x.Vendor.CapitalFirstLetterOfEachWord()
//                    });

//                return results.ToList();
//            }
//            catch (Exception ex)
//            {
//                EventLogger.LogEvent(ex); throw;
//            }
//        }

//        public class spReports_Category
//        {
//            public string Category { get; set; }
//            public string SubCategory { get; set; }
//            public decimal? TotalSales { get; set; }
//            public decimal? TotalQuotes { get; set; }
//            public int? LineItmesQuoted { get; set; }
//            public int? LinesItemsSold { get; set; }
//            public decimal? ClosingRate { get; set; }
//            public decimal? AvgSalesPerLineItem { get; set; }
//            public decimal? PercentOf_TotalSalesPerCategory { get; set; }

//            public string StartDate { get; set; }

//            public string EndDate { get; set; }
//        }


//        public List<spReports_Category> GetSalesBetaCategoryData(DateTime startDate, DateTime endDate, int franchiseId, CommercialTypes commercial)
//        {
//            if (!ChartsPermission.CanRead || (franchiseId <= 0))
//                return new List<spReports_Category>();

//            //swap the dates if they are reverse
//            Util.SortDate(ref startDate, ref endDate);

//            try
//            {
//                if (AuthorizedUser.FranchiseId.HasValue)
//                {
//                    franchiseId = AuthorizedUser.FranchiseId.Value;
//                }

//                var data = CRMDBContext.Database.SqlQuery<spReports_Category>(
//                          "exec [CRM].[spReports_Category] @startDate, @endDate, @franchiseId, @commercialType",
//                          new SqlParameter("@startDate", startDate),
//                          new SqlParameter("@endDate", endDate),
//                          new SqlParameter("@franchiseId", franchiseId), new SqlParameter("@commercialType", (int)commercial)).ToList();

//                foreach (var item in data)
//                {
//                    item.StartDate = startDate.ToShortDateString();
//                    item.EndDate = endDate.ToShortDateString();
//                }

//                return data;
//            }
//            catch (Exception ex) { EventLogger.LogEvent(ex); throw; }
//        }

//        public class spReports_City
//        {
//            public string City { get; set; }
//            public int? GTotalLeads { get; set; }
//            public int? GValidLeads { get; set; }
//            public int? GLostSales { get; set; }
//            public int? GNumOfSales { get; set; }
//            public decimal? GTotalSales { get; set; }
//            public decimal? GConversionRate { get; set; }
//            public decimal? GClosingRate { get; set; }
//            public decimal? GAvgSales { get; set; }
//            public decimal? GRevenuePerLead { get; set; }
//            public int? GNumberQuotes { get; set; }
//            public decimal? GTotalQuotes { get; set; }
//            public decimal? GAvgQuote { get; set; }
//            //public int? JobId { get; set; }
//            //public int? LeadId { get; set; }
//            //public int? JobNumber { get; set; }
//            //public DateTime? ContractedOnUtc { get; set; }
//            //public decimal? Subtotal { get; set; }
//            //public decimal? SurchargeTotal { get; set; }
//            //public decimal? DiscountTotal { get; set; }
//            //public decimal? TotalSales { get; set; }
//        }

//        public class CityReportModel
//        {
//            public string City { get; set; }
//            public int? GTotalLeads { get; set; }
//            public int? GValidLeads { get; set; }
//            public int? GLostSales { get; set; }
//            public int? GNumOfSales { get; set; }
//            public decimal? GTotalSales { get; set; }
//            public decimal? GConversionRate { get; set; }
//            public decimal? GClosingRate { get; set; }
//            public decimal? GAvgSales { get; set; }
//            public decimal? GRevenuePerLead { get; set; }
//            public int? GNumberQuotes { get; set; }
//            public decimal? GTotalQuotes { get; set; }
//            public decimal? GAvgQuote { get; set; }

//           // public List<CityReportSubModel>  SubModels{ get; set; }

//            public string StartDate { get; set; }

//            public string EndDate { get; set; }
//        }
//        //public class CityReportSubModel
//        //{
//        //    public int? JobId { get; set; }
//        //    public int? LeadId { get; set; }
//        //    public int? JobNumber { get; set; }
//        //    public DateTime? ContractedOnUtc { get; set; }
//        //    public decimal? Subtotal { get; set; }
//        //    public decimal? SurchargeTotal { get; set; }
//        //    public decimal? DiscountTotal { get; set; }
//        //    public decimal? TotalSales { get; set; }
//        //}

//        public List<CityReportModel> GetSalesBetaCityData(DateTime startDate, DateTime endDate, int franchiseId, CommercialTypes commercial)
//        {
//            if (!ChartsPermission.CanRead || (franchiseId <= 0))
//                return new List<CityReportModel>();

//            //swap the dates if they are reverse
//            Util.SortDate(ref startDate, ref endDate);

//            try
//            {
//                if (AuthorizedUser.FranchiseId.HasValue)
//                {
//                    franchiseId = AuthorizedUser.FranchiseId.Value;
//                }

//                var data = CRMDBContext.Database.SqlQuery<spReports_City>(
//                          "exec [CRM].[spReports_City] @startDate, @endDate, @franchiseId, @commercialType",
//                          new SqlParameter("@startDate", startDate),
//                          new SqlParameter("@endDate", endDate),
//                          new SqlParameter("@franchiseId", franchiseId), new SqlParameter("@commercialType", (int)commercial)).ToList();

//                var result = new List<CityReportModel>();
//                foreach (var group in data.GroupBy(v => v.City))
//                {

//                    var groupKey = group.Key;
//                    var model = new CityReportModel();
//                    model.City = group.FirstOrDefault().City;
//                    model.GTotalLeads = group.FirstOrDefault().GTotalLeads;
//                    model.GValidLeads = group.FirstOrDefault().GValidLeads;
//                    model.GLostSales = group.FirstOrDefault().GLostSales;
//                    model.GNumOfSales = group.FirstOrDefault().GNumOfSales;
//                    model.GTotalSales = group.FirstOrDefault().GTotalSales;
//                    model.GConversionRate = group.FirstOrDefault().GConversionRate;
//                    model.GClosingRate = group.FirstOrDefault().GClosingRate;
//                    model.GAvgSales = group.FirstOrDefault().GAvgSales;
//                    model.GRevenuePerLead = group.FirstOrDefault().GRevenuePerLead;
//                    model.GNumberQuotes = group.FirstOrDefault().GNumberQuotes;
//                    model.GTotalQuotes = group.FirstOrDefault().GTotalQuotes;
//                    model.GAvgQuote = group.FirstOrDefault().GAvgQuote;

//                    model.StartDate = startDate.ToShortDateString();
//                    model.EndDate = endDate.ToShortDateString();


//                    //var subModels = new List<CityReportSubModel>();
//                    //foreach (var groupedItem in group)
//                    //{
//                    //    var subModel = new CityReportSubModel();
//                    //    subModel.JobId = groupedItem.JobId;
//                    //    subModel.LeadId = groupedItem.LeadId;
//                    //    subModel.JobNumber = groupedItem.JobNumber;
//                    //    subModel.ContractedOnUtc = groupedItem.ContractedOnUtc;
//                    //    subModel.Subtotal = groupedItem.Subtotal;
//                    //    subModel.SurchargeTotal = groupedItem.SurchargeTotal;
//                    //    subModel.DiscountTotal = groupedItem.DiscountTotal;
//                    //    subModel.TotalSales = groupedItem.TotalSales;
//                    //    subModels.Add(subModel);
//                    //}
//                    //model.SubModels = subModels;
//                    result.Add(model);
//                }

//                return result;
//            }
//            catch (Exception ex) { EventLogger.LogEvent(ex); throw; }
//        }

//        public class spReports_ZipCode
//        {
//            public string ZipCode { get; set; }
//            public int? GTotalLeads { get; set; }
//            public int? GValidLeads { get; set; }
//            public int? GLostSales { get; set; }
//            public int? GNumOfSales { get; set; }
//            public decimal? GTotalSales { get; set; }
//            public decimal? GConversionRate { get; set; }
//            public decimal? GClosingRate { get; set; }
//            public decimal? GAvgSales { get; set; }
//            public decimal? GRevenuePerLead { get; set; }
//            public int? GNumberQuotes { get; set; }
//            public decimal? GTotalQuotes { get; set; }
//            public decimal? GAvgQuote { get; set; }
//            //public int? JobId { get; set; }
//            //public int? LeadId { get; set; }
//            //public int? JobNumber { get; set; }
//            //public DateTime? ContractedOnUtc { get; set; }
//            //public decimal? Subtotal { get; set; }
//            //public decimal? SurchargeTotal { get; set; }
//            //public decimal? DiscountTotal { get; set; }
//            //public decimal? TotalSales { get; set; }
//        }

//        public class ZipReportModel
//        {
//            public string ZipCode { get; set; }
//            public int? GTotalLeads { get; set; }
//            public int? GValidLeads { get; set; }
//            public int? GLostSales { get; set; }
//            public int? GNumOfSales { get; set; }
//            public decimal? GTotalSales { get; set; }
//            public decimal? GConversionRate { get; set; }
//            public decimal? GClosingRate { get; set; }
//            public decimal? GAvgSales { get; set; }
//            public decimal? GRevenuePerLead { get; set; }
//            public int? GNumberQuotes { get; set; }
//            public decimal? GTotalQuotes { get; set; }
//            public decimal? GAvgQuote { get; set; }

//           // public List<ZipReportSubModel> SubModels { get; set; }

//            public string StartDate { get; set; }

//            public string EndDate { get; set; }
//        }
//        //public class ZipReportSubModel
//        //{
//        //    public int? JobId { get; set; }
//        //    public int? LeadId { get; set; }
//        //    public int? JobNumber { get; set; }
//        //    public DateTime? ContractedOnUtc { get; set; }
//        //    public decimal? Subtotal { get; set; }
//        //    public decimal? SurchargeTotal { get; set; }
//        //    public decimal? DiscountTotal { get; set; }
//        //    public decimal? TotalSales { get; set; }
//        //}
//        public List<ZipReportModel> GetSalesBetaZipData(DateTime startDate, DateTime endDate, int franchiseId, CommercialTypes commercial)
//        {
//            if (!ChartsPermission.CanRead || (franchiseId <= 0))
//                throw new UnauthorizedAccessException(Unauthorized);

//            //swap the dates if they are reverse
//            Util.SortDate(ref startDate, ref endDate);

//            try
//            {
//                if (AuthorizedUser.FranchiseId.HasValue)
//                {
//                    franchiseId = AuthorizedUser.FranchiseId.Value;
//                }

//                var data = CRMDBContext.Database.SqlQuery<spReports_ZipCode>(
//                          "exec [CRM].[spReports_ZipCode] @startDate, @endDate, @franchiseId , @commercialType",
//                          new SqlParameter("@startDate", startDate),
//                          new SqlParameter("@endDate", endDate),
//                          new SqlParameter("@franchiseId", franchiseId), new SqlParameter("@commercialType", (int)commercial)).ToList();


//                var result = new List<ZipReportModel>();
//                foreach (var group in data.GroupBy(v => v.ZipCode))
//                {
//                    var groupKey = group.Key;
//                    var model = new ZipReportModel();
//                    model.ZipCode = group.FirstOrDefault().ZipCode;
//                    model.GTotalLeads = group.FirstOrDefault().GTotalLeads;
//                    model.GValidLeads = group.FirstOrDefault().GValidLeads;
//                    model.GLostSales = group.FirstOrDefault().GLostSales;
//                    model.GNumOfSales = group.FirstOrDefault().GNumOfSales;
//                    model.GTotalSales = group.FirstOrDefault().GTotalSales;
//                    model.GConversionRate = group.FirstOrDefault().GConversionRate;
//                    model.GClosingRate = group.FirstOrDefault().GClosingRate;
//                    model.GAvgSales = group.FirstOrDefault().GAvgSales;
//                    model.GRevenuePerLead = group.FirstOrDefault().GRevenuePerLead;
//                    model.GNumberQuotes = group.FirstOrDefault().GNumberQuotes;
//                    model.GTotalQuotes = group.FirstOrDefault().GTotalQuotes;
//                    model.GAvgQuote = group.FirstOrDefault().GAvgQuote;
//                    model.StartDate = startDate.ToShortDateString();
//                    model.EndDate = endDate.ToShortDateString();

//                    model.StartDate = startDate.ToShortDateString();
//                    model.EndDate = endDate.ToShortDateString();


//                    //var subModels = new List<ZipReportSubModel>();
//                    //foreach (var groupedItem in group)
//                    //{
//                    //    var subModel = new ZipReportSubModel();
//                    //    subModel.JobId = groupedItem.JobId;
//                    //    subModel.LeadId = groupedItem.LeadId;
//                    //    subModel.JobNumber = groupedItem.JobNumber;
//                    //    subModel.ContractedOnUtc = groupedItem.ContractedOnUtc;
//                    //    subModel.Subtotal = groupedItem.Subtotal;
//                    //    subModel.SurchargeTotal = groupedItem.SurchargeTotal;
//                    //    subModel.DiscountTotal = groupedItem.DiscountTotal;
//                    //    subModel.TotalSales = groupedItem.TotalSales;
//                    //    subModels.Add(subModel);
//                    //}
//                    //model.SubModels = subModels;
//                    result.Add(model);
//                }

//                return result;
//            }
//            catch (Exception ex) { EventLogger.LogEvent(ex); throw; }
//        }

//        public class spReports_JobStatus
//        {
//            public string Status { get; set; }
//            public string SubStatus { get; set; }
//            public int? NumOfJobs { get; set; }
//            public decimal? Totals { get; set; }
//            public int? ParentStatusId { get; set; }
//            public string ParentStatusIds { get; set; }
//            public int? SubStatusId { get; set; }

//            public string StartDate { get; set; }

//            public string EndDate { get; set; }
//        }


//        public List<spReports_JobStatus> GetSalesBetaJobStatusData(DateTime startDate, DateTime endDate, int franchiseId, CommercialTypes commercial)
//        {
//            if (!ChartsPermission.CanRead || (franchiseId <= 0))
//                throw new UnauthorizedAccessException(Unauthorized);

//            //swap the dates if they are reverse
//            Util.SortDate(ref startDate, ref endDate);

//            try
//            {
//                if (AuthorizedUser.FranchiseId.HasValue)
//                {
//                    franchiseId = AuthorizedUser.FranchiseId.Value;
//                }

//                var data = CRMDBContext.Database.SqlQuery<spReports_JobStatus>(
//                          "exec [CRM].[spReports_JobStatus] @startDate, @endDate, @franchiseId, @commercialType",
//                          new SqlParameter("@startDate", startDate),
//                          new SqlParameter("@endDate", endDate),
//                          new SqlParameter("@franchiseId", franchiseId), new SqlParameter("@commercialType", (int)commercial)).ToList();
//                foreach (var item in data)
//                {
//                    item.StartDate = startDate.ToShortDateString();
//                    item.EndDate = endDate.ToShortDateString();
//                }
//                foreach (var item in data.GroupBy(v=>v.ParentStatusId))
//                {
//                    var p = new List<string>();
//                    foreach (var i in item)
//                    {
//                        p.Add(i.SubStatusId.ToString());
//                    }
//                    foreach (var i in item)
//                    {
//                        i.ParentStatusIds = string.Join("-", p);
//                    }
//                }

//                foreach (var item in data)
//                {
//                    item.StartDate = startDate.ToShortDateString();
//                    item.EndDate = endDate.ToShortDateString();
//                }

//                return data;
//            }
//            catch (Exception ex) { EventLogger.LogEvent(ex); throw; }
//        }

//        public class spReports_Territory
//        {
//            public string TerrCode { get; set; }
//            public int? GTotalLeads { get; set; }
//            public int? GValidLeads { get; set; }
//            public int? GLostSales { get; set; }
//            public int? GNumOfSales { get; set; }
//            public decimal? GTotalSales { get; set; }
//            public decimal? GConversionRate { get; set; }
//            public decimal? GClosingRate { get; set; }
//            public decimal? GAvgSales { get; set; }
//            public decimal? GRevenuePerLead { get; set; }
//            public int? GNumberQuotes { get; set; }
//            public decimal? GTotalQuotes { get; set; }
//            public decimal? GAvgQuote { get; set; }
//            //public int? JobId { get; set; }
//            //public int? LeadId { get; set; }
//            //public int? JobNumber { get; set; }
//            //public DateTime? ContractedOnUtc { get; set; }
//            //public decimal? Subtotal { get; set; }
//            //public decimal? SurchargeTotal { get; set; }
//            //public decimal? DiscountTotal { get; set; }
//            //public decimal? TotalSales { get; set; }
//        }
//        public class TerritoryReportModel
//        {
//            public string TerrCode { get; set; }
//            public int? GTotalLeads { get; set; }
//            public int? GValidLeads { get; set; }
//            public int? GLostSales { get; set; }
//            public int? GNumOfSales { get; set; }
//            public decimal? GTotalSales { get; set; }
//            public decimal? GConversionRate { get; set; }
//            public decimal? GClosingRate { get; set; }
//            public decimal? GAvgSales { get; set; }
//            public decimal? GRevenuePerLead { get; set; }
//            public int? GNumberQuotes { get; set; }
//            public decimal? GTotalQuotes { get; set; }
//            public decimal? GAvgQuote { get; set; }

//           // public List<TerritoryReportSubModel> SubModels { get; set; }

//            public string StartDate { get; set; }

//            public string EndDate { get; set; }
//        }
//        //public class TerritoryReportSubModel
//        //{
//        //    public int? JobId { get; set; }
//        //    public int? LeadId { get; set; }
//        //    public int? JobNumber { get; set; }
//        //    public DateTime? ContractedOnUtc { get; set; }
//        //    public decimal? Subtotal { get; set; }
//        //    public decimal? SurchargeTotal { get; set; }
//        //    public decimal? DiscountTotal { get; set; }
//        //    public decimal? TotalSales { get; set; }
//        //}
//        public List<TerritoryReportModel> GetSalesBetaTerritoryData(DateTime startDate, DateTime endDate, int franchiseId, CommercialTypes commercial)
//        {
//            if (!ChartsPermission.CanRead || (franchiseId <= 0))
//                return new List<TerritoryReportModel>();

//            //swap the dates if they are reverse
//            Util.SortDate(ref startDate, ref endDate);

//            try
//            {
//                if (AuthorizedUser.FranchiseId.HasValue)
//                {
//                    franchiseId = AuthorizedUser.FranchiseId.Value;
//                }

//                var data = CRMDBContext.Database.SqlQuery<spReports_Territory>(
//                          "exec [CRM].[spReports_Territory] @startDate, @endDate, @franchiseId, @commercialType",
//                          new SqlParameter("@startDate", startDate),
//                          new SqlParameter("@endDate", endDate),
//                          new SqlParameter("@franchiseId", franchiseId), new SqlParameter("@commercialType", (int)commercial)).ToList();


//                var result = new List<TerritoryReportModel>();
//                foreach (var group in data.GroupBy(v => v.TerrCode))
//                {
//                    var groupKey = group.Key;
//                    var model = new TerritoryReportModel();
//                    model.TerrCode = group.FirstOrDefault().TerrCode;
//                    model.GTotalLeads = group.FirstOrDefault().GTotalLeads;
//                    model.GValidLeads = group.FirstOrDefault().GValidLeads;
//                    model.GLostSales = group.FirstOrDefault().GLostSales;
//                    model.GNumOfSales = group.FirstOrDefault().GNumOfSales;
//                    model.GTotalSales = group.FirstOrDefault().GTotalSales;
//                    model.GConversionRate = group.FirstOrDefault().GConversionRate;
//                    model.GClosingRate = group.FirstOrDefault().GClosingRate;
//                    model.GAvgSales = group.FirstOrDefault().GAvgSales;
//                    model.GRevenuePerLead = group.FirstOrDefault().GRevenuePerLead;
//                    model.GNumberQuotes = group.FirstOrDefault().GNumberQuotes;
//                    model.GTotalQuotes = group.FirstOrDefault().GTotalQuotes;
//                    model.GAvgQuote = group.FirstOrDefault().GAvgQuote;
//                    model.StartDate = startDate.ToShortDateString();
//                    model.EndDate = endDate.ToShortDateString();

//                    model.StartDate = startDate.ToShortDateString();
//                    model.EndDate = endDate.ToShortDateString();

//                    //var subModels = new List<TerritoryReportSubModel>();
//                    //foreach (var groupedItem in group)
//                    //{
//                    //    var subModel = new TerritoryReportSubModel();
//                    //    subModel.JobId = groupedItem.JobId;
//                    //    subModel.LeadId = groupedItem.LeadId;
//                    //    subModel.JobNumber = groupedItem.JobNumber;
//                    //    subModel.ContractedOnUtc = groupedItem.ContractedOnUtc;
//                    //    subModel.Subtotal = groupedItem.Subtotal;
//                    //    subModel.SurchargeTotal = groupedItem.SurchargeTotal;
//                    //    subModel.DiscountTotal = groupedItem.DiscountTotal;
//                    //    subModel.TotalSales = groupedItem.TotalSales;
//                    //    subModels.Add(subModel);
//                    //}
//                    //model.SubModels = subModels;
//                    result.Add(model);
//                }

//                return result;
//            }
//            catch (Exception ex) { EventLogger.LogEvent(ex); throw; }
//        }

//        public class spReports_LeadStatus
//        {
//            public string LeadStatus { get; set; }
//            public string SubStatus { get; set; }
//            public int? NumOfLeads { get; set; }
//            public Int16? ParentStatusId { get; set; }
//            public Int16? SubStatusId { get; set; }
//            public string StartDate { get; set; }
//            public string EndDate { get; set; }

//            public string ParentStatusIds { get; set; }
//        }


//        public List<spReports_LeadStatus> GetSalesBetaLeadReportData(DateTime startDate, DateTime endDate, int franchiseId, CommercialTypes commercial)
//        {
//            if (!ChartsPermission.CanRead || (franchiseId <= 0))
//                throw new UnauthorizedAccessException(Unauthorized);

//            //swap the dates if they are reverse
//            Util.SortDate(ref startDate, ref endDate);

//            try
//            {
//                if (AuthorizedUser.FranchiseId.HasValue)
//                {
//                    franchiseId = AuthorizedUser.FranchiseId.Value;
//                }

//                var data = CRMDBContext.Database.SqlQuery<spReports_LeadStatus>(
//                          "exec [CRM].[spReports_LeadStatus] @startDate, @endDate, @franchiseId, @commercialType",
//                          new SqlParameter("@startDate", startDate),
//                          new SqlParameter("@endDate", endDate),
//                          new SqlParameter("@franchiseId", franchiseId), new SqlParameter("@commercialType", (int)commercial)).ToList();

//                foreach (var item in data)
//                {
//                    item.StartDate = startDate.ToShortDateString();
//                    item.EndDate = endDate.ToShortDateString();
//                }

//                foreach (var item in data.GroupBy(v => v.ParentStatusId))
//                {
//                    var p = item.Select(i => i.SubStatusId.ToString()).ToList();
//                    foreach (var i in item)
//                    {
//                        i.ParentStatusIds = string.Join("-", p);
//                    }
//                }

//                foreach (var item in data)
//                {
//                    item.StartDate = startDate.ToShortDateString();
//                    item.EndDate = endDate.ToShortDateString();
//                }

//                return data;
//            }
//            catch (Exception ex) { EventLogger.LogEvent(ex); throw; }
//        }
//    }
//}
