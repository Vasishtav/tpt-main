﻿using DotNetOpenAuth.OAuth;
using DotNetOpenAuth.OAuth.ChannelElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// The OAuth namespace.
/// </summary>
namespace HFC.CRM.Managers.OAuth
{
    /// <summary>
    /// Class OAuthConsumerDescription.
    /// </summary>
    public class OAuthConsumerDescription : HFC.CRM.Data.OAuthConsumer, IConsumerDescription
    {
        #region Private IConsumerDescription 

        /// <summary>
        /// Gets the key.
        /// </summary>
        /// <value>The key.</value>
        string IConsumerDescription.Key
        {
            get { return Key.ToString(); }
        }

        /// <summary>
        /// Gets the secret.
        /// </summary>
        /// <value>The secret.</value>
        string IConsumerDescription.Secret
        {
            get { return Secret.ToString(); }
        }

        /// <summary>
        /// Gets the callback URI that this consumer has pre-registered with the service provider, if any.
        /// </summary>
        /// <value>A URI that user authorization responses should be directed to; or <c>null</c> if no preregistered callback was arranged.</value>
        Uri IConsumerDescription.Callback
        {
            get { return new Uri(CallbackUrl); }
        }

        /// <summary>
        /// The _certificate
        /// </summary>
        private X509Certificate2 _certificate;
        /// <summary>
        /// Gets the certificate that can be used to verify the signature of an incoming
        ///             message from a Consumer.
        /// </summary>
        /// <value>The certificate.</value>
        /// <returns>The public key from the Consumer's X.509 Certificate, if one can be found; otherwise <c>null</c>.</returns>
        /// <remarks>This property must be implemented only if the RSA-SHA1 algorithm is supported by the Service Provider.</remarks>
        X509Certificate2 IConsumerDescription.Certificate
        {
            get { return _certificate; }
        }

        /// <summary>
        /// The _ver code format
        /// </summary>
        private VerificationCodeFormat _verCodeFormat;
        /// <summary>
        /// Gets the verification code format that is most appropriate for this consumer
        ///             when a callback URI is not available.
        /// </summary>
        /// <value>A set of characters that can be easily keyed in by the user given the Consumer's
        ///             application type and form factor.</value>
        /// <remarks>The value 
        /// <see cref="F:DotNetOpenAuth.OAuth.VerificationCodeFormat.IncludedInCallback" /> should NEVER be returned
        ///             since this property is only used in no callback scenarios anyway.</remarks>
        VerificationCodeFormat IConsumerDescription.VerificationCodeFormat
        {
            get { return _verCodeFormat; }
        }

        /// <summary>
        /// The _ver code length
        /// </summary>
        private int _verCodeLength;
        /// <summary>
        /// Gets the length of the verification code to issue for this Consumer.
        /// </summary>
        /// <value>A positive number, generally at least 4.</value>
        int IConsumerDescription.VerificationCodeLength
        {
            get { return _verCodeLength; }
        }

        #endregion

        /// <summary>
        /// Validate the callback url to match the stored callback in API.OAuthConsumers table, if CallbackUrl is empty then it will allow all callback url, otherwise it will only allow url in the (comma or semi-colon) separated CallbackUrl property
        /// </summary>
        /// <param name="callback">Request callback uri</param>
        /// <returns><c>true</c> if [is valid callback] [the specified callback]; otherwise, <c>false</c>.</returns>
        public bool IsValidCallback(Uri callback)
        {
            bool valid = true;
            if(!string.IsNullOrEmpty(this.CallbackUrl)) 
            {
                var callbackUrlList = this.CallbackUrl.Split(',', ';');
                var uri = new UriBuilder(callback);
                uri.Query = null;
                valid = callbackUrlList.Any(a => a.TrimEnd('/').Equals(uri.Uri.AbsoluteUri.TrimEnd('/'), StringComparison.InvariantCultureIgnoreCase));
            }

            return valid;
        }

        /// <summary>
        /// Check request IP address against list of white listed addresses, if AllowIPAddresses is null/empty then all addresses are allowed
        /// </summary>
        /// <param name="requestIP">Request IP Address</param>
        /// <returns><c>true</c> if [is ip allowed] [the specified request ip]; otherwise, <c>false</c>.</returns>
        public bool IsIPAllowed(string requestIP)
        {
            bool allow = true;

            if (!string.IsNullOrEmpty(AllowIPAddresses))
            {
                var addresses = AllowIPAddresses.Split(',', ';');

                allow = addresses.Any(a => a.Equals(requestIP, StringComparison.InvariantCultureIgnoreCase));
            }

            return allow;
        }
    }
}
