﻿using DotNetOpenAuth.OAuth.ChannelElements;
using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// The OAuth namespace.
/// </summary>
namespace HFC.CRM.Managers.OAuth
{
    /// <summary>
    /// Class OAuthToken.
    /// </summary>
    public class OAuthToken : IServiceProviderRequestToken, IServiceProviderAccessToken
    {

        #region IServiceProviderRequestToken

        /// <summary>
        /// The _SP requesttoken
        /// </summary>
        private String _spRequesttoken;
        /// <summary>
        /// Gets the token itself.
        /// </summary>
        /// <value>The token.</value>
        String IServiceProviderRequestToken.Token
        {
            get { return _spRequesttoken; }
        }

        /// <summary>
        /// The _callback
        /// </summary>
        private Uri _callback;

        /// <summary>
        /// Gets or sets the callback associated specifically with this token, if any.
        /// </summary>
        /// <value>The callback URI; or <c>null</c> if no callback was specifically assigned to this token.</value>
        public Uri Callback
        {
            get { return _callback; }
            set { _callback = value; }
        }

        /// <summary>
        /// Gets or sets the callback associated specifically with this token, if any.
        /// </summary>
        /// <value>The callback URI; or <c>null</c> if no callback was specifically assigned to this token.</value>
        Uri IServiceProviderRequestToken.Callback
        {
            get
            {
                return _callback;
            }
            set
            {
                _callback = value;
            }
        }

        /// <summary>
        /// Gets the consumer key that requested this token.
        /// </summary>
        /// <value>The consumer key.</value>
        string IServiceProviderRequestToken.ConsumerKey
        {
            get { return Consumer.Key.ToString(); }
        }

        /// <summary>
        /// The _version
        /// </summary>
        private Version _version;

        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        /// <value>The version.</value>
        public Version Version
        {
            get { return _version; }
            set { _version = value; }
        }
        /// <summary>
        /// Gets or sets the version of the Consumer that requested this token.
        /// </summary>
        /// <value>The consumer version.</value>
        /// <remarks>This property is used to determine whether a 
        /// <see cref="P:DotNetOpenAuth.OAuth.ChannelElements.IServiceProviderRequestToken.VerificationCode" /> must be
        ///             generated when the user authorizes the Consumer or not.</remarks>
        Version IServiceProviderRequestToken.ConsumerVersion
        {
            get
            {
                return _version;
            }
            set
            {
                _version = value;
            }
        }

        //private DateTime _createdOn;

        //public DateTime CreatedOn
        //{
        //    get { return _createdOn; }
        //    set { _createdOn = value; }
        //}

        /// <summary>
        /// Gets the (local) date that this request token was first created on.
        /// </summary>
        /// <value>The created on.</value>
        DateTime IServiceProviderRequestToken.CreatedOn
        {
            get { return IssueDate; }
        }


        /// <summary>
        /// The _verification code
        /// </summary>
        private string _verificationCode;

        /// <summary>
        /// Gets or sets the verifier that the consumer must include in the 
        /// <see cref="T:DotNetOpenAuth.OAuth.Messages.AuthorizedTokenRequest" />
        ///             message to exchange this request token for an access token.
        /// </summary>
        /// <value>The verifier code, or <c>null</c> if none has been assigned (yet).</value>
        public string VerificationCode
        {
            get { return _verificationCode; }
            set { _verificationCode = value; }
        }
        /// <summary>
        /// Gets or sets the verifier that the consumer must include in the 
        /// <see cref="T:DotNetOpenAuth.OAuth.Messages.AuthorizedTokenRequest" />
        ///             message to exchange this request token for an access token.
        /// </summary>
        /// <value>The verifier code, or <c>null</c> if none has been assigned (yet).</value>
        string IServiceProviderRequestToken.VerificationCode
        {
            get
            {
                return _verificationCode;
            }
            set
            {
                _verificationCode = value;
            }
        }

        #endregion

        #region IServiceProviderAccessToken

        /// <summary>
        /// The _expiration date
        /// </summary>
        private DateTime? _expirationDate;
        /// <summary>
        /// Gets the expiration date (local time) for the access token.
        /// </summary>
        /// <value>The expiration date, or <c>null</c> if there is no expiration date.</value>
        DateTime? IServiceProviderAccessToken.ExpirationDate
        {
            get { return _expirationDate; }
        }

        /// <summary>
        /// The _roles
        /// </summary>
        private string[] _roles;
        /// <summary>
        /// Gets the roles that the OAuth principal should belong to.
        /// </summary>
        /// <value>The roles that the user belongs to, or a subset of these according to the rights
        ///             granted when the user authorized the request token.</value>
        string[] IServiceProviderAccessToken.Roles
        {
            get { return _roles; }
        }

        /// <summary>
        /// Gets the username of the principal that will be impersonated by this access token.
        /// </summary>
        /// <value>The name of the user who authorized the OAuth request token originally.</value>
        string IServiceProviderAccessToken.Username
        {
            get { return User.UserName; }
        }

        /// <summary>
        /// The _spaccess token
        /// </summary>
        private String _spaccessToken;
        /// <summary>
        /// Gets the token itself.
        /// </summary>
        /// <value>The token.</value>
        String IServiceProviderAccessToken.Token
        {
            get { return _spaccessToken; }
        }

        #endregion

        /// <summary>
        /// The _consumer
        /// </summary>
        private OAuthConsumer _consumer;

        /// <summary>
        /// Gets or sets the consumer.
        /// </summary>
        /// <value>The consumer.</value>
        public OAuthConsumer Consumer
        {
            get { return _consumer; }
            set { _consumer = value; }
        }

        /// <summary>
        /// The _state
        /// </summary>
        private TokenAuthorizationState _state;

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>The state.</value>
        public TokenAuthorizationState State
        {
            get { return _state; }
            set { _state = value; }
        }

        /// <summary>
        /// The _issue date
        /// </summary>
        private DateTime _issueDate;

        /// <summary>
        /// Gets or sets the issue date.
        /// </summary>
        /// <value>The issue date.</value>
        public DateTime IssueDate
        {
            get { return _issueDate; }
            set { _issueDate = value; }
        }

        /// <summary>
        /// The _user
        /// </summary>
        private User _user;

        /// <summary>
        /// Gets or sets the user.
        /// </summary>
        /// <value>The user.</value>
        public User User
        {
            get { return _user; }
            set { _user = value; }
        }

        /// <summary>
        /// The _token secret
        /// </summary>
        private String _tokenSecret;

        /// <summary>
        /// Gets or sets the token secret.
        /// </summary>
        /// <value>The token secret.</value>
        public String TokenSecret
        {
            get { return _tokenSecret; }
            set { _tokenSecret = value; }
        }

        /// <summary>
        /// The _scope
        /// </summary>
        private String _scope;

        /// <summary>
        /// Gets or sets the scope.
        /// </summary>
        /// <value>The scope.</value>
        public String Scope
        {
            get { return _scope; }
            set { _scope = value; }
        }

        /// <summary>
        /// Gets the token itself.
        /// </summary>
        /// <value>The token.</value>
        public String Token
        {
            set
            {
                _spaccessToken = value;
                _spRequesttoken = value;
            }

            get
            {
                return _spaccessToken;
            }
        }
    }
}
