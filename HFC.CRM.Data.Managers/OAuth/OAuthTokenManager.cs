﻿using DotNetOpenAuth.Messaging;
using DotNetOpenAuth.OAuth;
using DotNetOpenAuth.OAuth.ChannelElements;
using DotNetOpenAuth.OAuth.Messages;
using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

/// <summary>
/// The OAuth namespace.
/// </summary>
namespace HFC.CRM.Managers.OAuth
{
    /// <summary>
    /// Class OAuthTokenManager.
    /// </summary>
    public class OAuthTokenManager : IServiceProviderTokenManager
    {
        /// <summary>
        /// Gets the access token endpoint.
        /// </summary>
        /// <value>The access token endpoint.</value>
        public string AccessTokenEndpoint { get; private set; }
        /// <summary>
        /// Gets the request token endpoint.
        /// </summary>
        /// <value>The request token endpoint.</value>
        public string RequestTokenEndpoint { get; private set; }
        /// <summary>
        /// Gets the user authorization endpoint.
        /// </summary>
        /// <value>The user authorization endpoint.</value>
        public string UserAuthorizationEndpoint { get; private set; }

        /// <summary>
        /// Gets the authentication tokens.
        /// </summary>
        /// <value>The authentication tokens.</value>
        public List<OAuthToken> AuthTokens { get; private set; }
        /// <summary>
        /// The _consumers
        /// </summary>
        private List<OAuthConsumerDescription> _consumers;
        /// <summary>
        /// Gets the consumers.
        /// </summary>
        /// <value>The consumers.</value>
        public List<OAuthConsumerDescription> Consumers {
            get
            {
                if (_consumers == null)
                {
                    using (var db = new CRMContextEx())
                    {
                        _consumers = db.Database.SqlQuery<OAuthConsumerDescription>("SELECT [OAConsumerId],[Key],[Secret],[CallbackUrl],[AllowIPAddresses] FROM [API].[OAuthConsumers] WHERE [IsDeleted] = 0").ToList();
                    }
                }
                return _consumers;
            }
            private set
            {
                _consumers = value;
            }
        }

        /// <summary>
        /// Gets the description.
        /// </summary>
        /// <value>The description.</value>
        public ServiceProviderDescription Description
        {
            get
            {
                Uri baseUrl = new Uri(string.Format("https://{0}", HttpContext.Current.Request.Url.Authority.ToLower()));
                return new ServiceProviderDescription
                {
                    AccessTokenEndpoint = new MessageReceivingEndpoint(new Uri(baseUrl, AccessTokenEndpoint), HttpDeliveryMethods.PostRequest),
                    RequestTokenEndpoint = new MessageReceivingEndpoint(new Uri(baseUrl, RequestTokenEndpoint), HttpDeliveryMethods.PostRequest),
                    UserAuthorizationEndpoint = new MessageReceivingEndpoint(new Uri(baseUrl, UserAuthorizationEndpoint), HttpDeliveryMethods.PostRequest),
                    TamperProtectionElements = new ITamperProtectionChannelBindingElement[] {
					    new HmacSha1SigningBindingElement(),
				    }
                };
            }
        }

        /// <summary>
        /// Instantiate new oauth token manager
        /// </summary>
        /// <param name="accessTokenEndpoint">Required, url endpoint to receive message</param>
        /// <param name="requestTokenEndpoint">Optional, will point to same endpoint as accessTokenEndpoint if this is null/empty</param>
        /// <param name="userAuthorizationEndpoint">Optional, will point to same endpoint as accessTokenEndpoint if this is null/empty</param>
        /// <exception cref="System.ArgumentNullException">Invalid accessTokenEndpoint</exception>
        public OAuthTokenManager(string accessTokenEndpoint, string requestTokenEndpoint = null, string userAuthorizationEndpoint = null)
        {
            if (string.IsNullOrEmpty(accessTokenEndpoint))
                throw new ArgumentNullException("Invalid accessTokenEndpoint");

            AccessTokenEndpoint = accessTokenEndpoint.ToLower();
            if (string.IsNullOrEmpty(requestTokenEndpoint))
                RequestTokenEndpoint = AccessTokenEndpoint;
            else
                RequestTokenEndpoint = requestTokenEndpoint.ToLower();
            if (string.IsNullOrEmpty(userAuthorizationEndpoint))
                UserAuthorizationEndpoint = AccessTokenEndpoint;
            else
                UserAuthorizationEndpoint = userAuthorizationEndpoint.ToLower();

            AuthTokens = new List<OAuthToken>();
        }

        #region IServiceProviderTokenManager


        /// <summary>
        /// Gets the Consumer description for a given a Consumer Key.
        /// </summary>
        /// <param name="consumerKey">The Consumer Key.</param>
        /// <returns>A description of the consumer.  Never null.</returns>
        /// <exception cref="System.Collections.Generic.KeyNotFoundException"></exception>
        public IConsumerDescription GetConsumer(string consumerKey)
        {
            var consumerRow = Consumers.SingleOrDefault(
                consumerCandidate => consumerCandidate.Key.ToString().Equals(consumerKey, StringComparison.InvariantCultureIgnoreCase));
            if (consumerRow == null)
            {
                throw new KeyNotFoundException();
            }

            return consumerRow;
        }

        /// <summary>
        /// Gets details on the named request token.
        /// </summary>
        /// <param name="token">The request token.</param>
        /// <returns>A description of the token.  Never null.</returns>
        /// <exception cref="System.Collections.Generic.KeyNotFoundException">Unrecognized token</exception>
        /// <remarks>It is acceptable for implementations to find the token, see that it has expired,
        ///             delete it from the database and then throw 
        /// <see cref="T:System.Collections.Generic.KeyNotFoundException" />,
        ///             or alternatively it can return the expired token anyway and the OAuth channel will
        ///             log and throw the appropriate error.</remarks>
        public IServiceProviderRequestToken GetRequestToken(string token)
        {

            var foundToken = AuthTokens.FirstOrDefault(t => t.Token == token && t.State != TokenAuthorizationState.AccessToken);

            if (foundToken == null)
            {
                throw new KeyNotFoundException("Unrecognized token");
            }
            return foundToken;
        }

        /// <summary>
        /// Gets details on the named access token.
        /// </summary>
        /// <param name="token">The access token.</param>
        /// <returns>A description of the token.  Never null.</returns>
        /// <exception cref="System.Collections.Generic.KeyNotFoundException">Unrecognized token</exception>
        /// <remarks>It is acceptable for implementations to find the token, see that it has expired,
        ///             delete it from the database and then throw 
        /// <see cref="T:System.Collections.Generic.KeyNotFoundException" />,
        ///             or alternatively it can return the expired token anyway and the OAuth channel will
        ///             log and throw the appropriate error.</remarks>
        public IServiceProviderAccessToken GetAccessToken(string token)
        {
            try
            {
                return AuthTokens.First(t => t.Token == token && t.State == TokenAuthorizationState.AccessToken);
            }
            catch (InvalidOperationException ex)
            {
                throw new KeyNotFoundException("Unrecognized token", ex);
            }
        }

        /// <summary>
        /// Persists any changes made to the token.
        /// </summary>
        /// <param name="token">The token whose properties have been changed.</param>
        /// <remarks>This library will invoke this method after making a set
        ///             of changes to the token as part of a web request to give the host
        ///             the opportunity to persist those changes to a database.
        ///             Depending on the object persistence framework the host site uses,
        ///             this method MAY not need to do anything (if changes made to the token
        ///             will automatically be saved without any extra handling).</remarks>
        public void UpdateToken(IServiceProviderRequestToken token)
        {
            var tokenInDb = AuthTokens.SingleOrDefault(x => x.Token == token.Token);
            if (tokenInDb != null)
            {
                tokenInDb.VerificationCode = token.VerificationCode;
                tokenInDb.Callback = token.Callback;
                //tokenInDb.ConsumerKey = token.ConsumerKey;
                tokenInDb.Version = token.ConsumerVersion;
                tokenInDb.Token = token.Token;
                tokenInDb.VerificationCode = token.VerificationCode;
            }
        }

        #endregion

        #region ITokenManager Members

        /// <summary>
        /// Gets the Token Secret given a request or access token.
        /// </summary>
        /// <param name="token">The request or access token.</param>
        /// <returns>The secret associated with the given token.</returns>
        /// <exception cref="System.ArgumentException"></exception>
        public string GetTokenSecret(string token)
        {
            var tokenRow = AuthTokens.SingleOrDefault(
                tokenCandidate => tokenCandidate.Token == token);
            if (tokenRow == null)
            {
                throw new ArgumentException();
            }

            return tokenRow.TokenSecret;
        }

        /// <summary>
        /// Stores a newly generated unauthorized request token, secret, and optional
        ///             application-specific parameters for later recall.
        /// </summary>
        /// <param name="request">The request message that resulted in the generation of a new unauthorized request token.</param>
        /// <param name="response">The response message that includes the unauthorized request token.</param>
        /// <remarks>Request tokens stored by this method SHOULD NOT associate any user account with this token.
        ///             It usually opens up security holes in your application to do so.  Instead, you associate a user
        ///             account with access tokens (not request tokens) in the 
        /// <see cref="M:DotNetOpenAuth.OAuth.ChannelElements.ITokenManager.ExpireRequestTokenAndStoreNewAccessToken(System.String,System.String,System.String,System.String)" />
        ///             method.</remarks>
        public void StoreNewRequestToken(UnauthorizedTokenRequest request, ITokenSecretContainingMessage response)
        {
            RequestScopedTokenMessage scopedRequest = (RequestScopedTokenMessage)request;
            var consumer = Consumers.Single(consumerRow => consumerRow.Key.ToString().Equals(request.ConsumerKey, StringComparison.InvariantCultureIgnoreCase));
            string scope = scopedRequest.Scope;
            OAuthToken newToken = new OAuthToken
            {
                Consumer = consumer,
                Token = response.Token,
                TokenSecret = response.TokenSecret,
                IssueDate = DateTime.Now,
                Scope = scope,
            };

            AuthTokens.Add(newToken);
        }

        /// <summary>
        /// Checks whether a given request token has already been authorized
        /// by some user for use by the Consumer that requested it.
        /// </summary>
        /// <param name="requestToken">The Consumer's request token.</param>
        /// <returns>True if the request token has already been fully authorized by the user
        /// who owns the relevant protected resources.  False if the token has not yet
        /// been authorized, has expired or does not exist.</returns>
        public bool IsRequestTokenAuthorized(string requestToken)
        {
            var tokenFound = AuthTokens.SingleOrDefault(
                token => token.Token == requestToken &&
                token.State == TokenAuthorizationState.AuthorizedRequestToken);
            return tokenFound != null;
        }

        /// <summary>
        /// Deletes a request token and its associated secret and stores a new access token and secret.
        /// </summary>
        /// <param name="consumerKey">The Consumer that is exchanging its request token for an access token.</param>
        /// <param name="requestToken">The Consumer's request token that should be deleted/expired.</param>
        /// <param name="accessToken">The new access token that is being issued to the Consumer.</param>
        /// <param name="accessTokenSecret">The secret associated with the newly issued access token.</param>
        /// <remarks><para>
        ///             Any scope of granted privileges associated with the request token from the
        ///             original call to <see cref="M:DotNetOpenAuth.OAuth.ChannelElements.ITokenManager.StoreNewRequestToken(DotNetOpenAuth.OAuth.Messages.UnauthorizedTokenRequest,DotNetOpenAuth.OAuth.Messages.ITokenSecretContainingMessage)" /> should be carried over
        ///             to the new Access Token.
        ///             </para>
        /// <para>
        ///             To associate a user account with the new access token, 
        ///             <see cref="P:System.Web.HttpContext.User">HttpContext.Current.User</see> may be
        ///             useful in an ASP.NET web application within the implementation of this method.
        ///             Alternatively you may store the access token here without associating with a user account,
        ///             and wait until WebConsumer.ProcessUserAuthorization or
        ///             DesktopConsumer.ProcessUserAuthorization return the access
        ///             token to associate the access token with a user account at that point.
        ///             </para></remarks>
        public void ExpireRequestTokenAndStoreNewAccessToken(string consumerKey, string requestToken, string accessToken, string accessTokenSecret)
        {

            var consumerRow = Consumers.Single(consumer => consumer.Key.ToString().Equals(consumerKey, StringComparison.InvariantCultureIgnoreCase));
            var tokenRow = AuthTokens.Single(token => token.Token == requestToken && token.Consumer == consumerRow);
            Debug.Assert(tokenRow.State == TokenAuthorizationState.AuthorizedRequestToken, "The token should be authorized already!");

            // Update the existing row to be an access token.
            tokenRow.IssueDate = DateTime.Now;
            tokenRow.State = TokenAuthorizationState.AccessToken;
            tokenRow.Token = accessToken;
            tokenRow.TokenSecret = accessTokenSecret;
        }

        /// <summary>
        /// Classifies a token as a request token or an access token.
        /// </summary>
        /// <param name="token">The token to classify.</param>
        /// <returns>Request or Access token, or invalid if the token is not recognized.</returns>
        public TokenType GetTokenType(string token)
        {
            var tokenRow = AuthTokens.SingleOrDefault(tokenCandidate => tokenCandidate.Token == token);
            if (tokenRow == null)
            {
                return TokenType.InvalidToken;
            }
            else if (tokenRow.State == TokenAuthorizationState.AccessToken)
            {
                return TokenType.AccessToken;
            }
            else
            {
                return TokenType.RequestToken;
            }
        }

        #endregion

        /// <summary>
        /// Authorizes the request token.
        /// </summary>
        /// <param name="requestToken">The request token.</param>
        /// <param name="user">The user.</param>
        /// <exception cref="System.ArgumentNullException">
        /// requestToken
        /// or
        /// user
        /// </exception>
        /// <exception cref="System.ArgumentException"></exception>
        public void AuthorizeRequestToken(string requestToken, User user)
        {
            if (requestToken == null)
            {
                throw new ArgumentNullException("requestToken");
            }
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            var tokenRow = AuthTokens.SingleOrDefault(
                tokenCandidate => tokenCandidate.Token == requestToken &&
                tokenCandidate.State == TokenAuthorizationState.UnauthorizedRequestToken);
            if (tokenRow == null)
            {
                throw new ArgumentException();
            }

            tokenRow.State = TokenAuthorizationState.AuthorizedRequestToken;
            tokenRow.User = user;
        }

        /// <summary>
        /// Gets the consumer for token.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>OAuthConsumer.</returns>
        /// <exception cref="System.ArgumentNullException">requestToken</exception>
        /// <exception cref="System.ArgumentException"></exception>
        public OAuthConsumer GetConsumerForToken(string token)
        {
            if (String.IsNullOrEmpty(token))
            {
                throw new ArgumentNullException("requestToken");
            }

            var tokenRow = AuthTokens.SingleOrDefault(
                tokenCandidate => tokenCandidate.Token == token);
            if (tokenRow == null)
            {
                throw new ArgumentException();
            }

            return tokenRow.Consumer;
        }

    }
}
