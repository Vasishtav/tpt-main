﻿using DotNetOpenAuth.Messaging;
using DotNetOpenAuth.OAuth.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// The OAuth namespace.
/// </summary>
namespace HFC.CRM.Managers.OAuth
{
    /// <summary>
    /// Class RequestScopedTokenMessage.
    /// </summary>
    public class RequestScopedTokenMessage : UnauthorizedTokenRequest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RequestScopedTokenMessage" /> class.
        /// </summary>
        /// <param name="endpoint">The endpoint that will receive the message.</param>
        /// <param name="version">The OAuth version.</param>
        public RequestScopedTokenMessage(MessageReceivingEndpoint endpoint, Version version)
            : base(endpoint, version)
        {
        }

        /// <summary>
        /// Gets or sets the scope of the access being requested.
        /// </summary>
        /// <value>The scope.</value>
        [MessagePart("scope", IsRequired = true)]
        public string Scope { get; set; }

    }
}
