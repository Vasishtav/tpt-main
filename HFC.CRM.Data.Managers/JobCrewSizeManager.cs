﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Core.Membership;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using HFC.CRM.Data.Geolocator;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Web;
using System.IO;
using HFC.CRM.DTO;

namespace HFC.CRM.Managers
{
    public class JobCrewSizeManager
    {
        public JobCrewSize GetCrewSize(int BrandId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = @"select * from Crm.JobCrewSize where 
                            BrandId = @BrandId";
                var model = connection.Query<JobCrewSize>(query, new { BrandId }).FirstOrDefault();
                return model;
            }
        }

        public JobCrewSize AddUpdateCrewSize(JobCrewSize data)
        {
            string UpdateQuery = "", InsertQuery = "";

            UpdateQuery = @"UPDATE CRM.JobCrewSize SET 
                    HTML=@HTML
                    , lastUpdatedOn=@updatedOn, lastUpdatedBy=@updatedBy 
                    where Id=@Id";

            InsertQuery = @"Insert into CRM.JobCrewSize(BrandId,HTML,CreatedOn,CreatedBy) Values(@BrandId,@HTML,@CreatedOn,@CreatedBy)";

            using (SqlConnection connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                if(data.Id!=0)
                {
                    connection.Execute(UpdateQuery, new
                    {
                        HTML = data.HTML,
                        Id = data.Id,
                        updatedon=DateTime.Now,
                        updatedby = SessionManager.CurrentUser.PersonId
                    });
                    return data;
                }
                else
                {
                    connection.Execute(InsertQuery, new
                    {
                        HTML = data.HTML,
                        BrandId = data.BrandId,
                        CreatedOn = DateTime.Now,
                        CreatedBy = SessionManager.CurrentUser.PersonId
                    });

                    var query = @"select * from Crm.JobCrewSize where 
                            BrandId = @BrandId";
                    var model = connection.Query<JobCrewSize>(query, new { data.BrandId }).FirstOrDefault();
                    return model;
                }
            }
        }
    }
}


