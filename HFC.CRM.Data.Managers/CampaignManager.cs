﻿using Dapper;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using HFC.CRM.DTO.Source;
using System;
using System.Data.SqlClient;
using System.Linq;
using HFC.CRM.Core.Common;
using System.Collections.Generic;
using HFC.CRM.Core.Logs;
using HFC.CRM.DTOs;

namespace HFC.CRM.Managers
{
    public class CampaignManager : DataManagerFE<Campaign>
    {
        public CampaignManager(User user, Franchise franchise) : base(user, franchise)
        {
        }

        public void DeleteCampaigs(List<int> campaingIds)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    var query = @"update crm.Campaign set IsDeleted = 1 
                                , LastUpdatedOn = @updatedon, LastUpdatedBy = @updatedby 
                                where CampaignId = @campaignid";

                    foreach (var item in campaingIds)
                    {
                        connection.Execute(query, new
                        {
                            campaignid = item
                            , updatedon = DateTime.UtcNow
                            , updatedby = this.User.PersonId
                        }
                        , transaction);
                    }

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    EventLogger.LogEvent(ex);
                    throw;
                } finally
                {
                    connection.Close();
                }
            }        
        }

        public List<ChannelDTO> GetCamapaigns(int franchiseId, bool includeInactive = false)
        {
            using(var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var condition = includeInactive
                        ? ""
                        : " and ISNULL(c.IsActive, 0) <> 0";
                    string query = @"SELECT Distinct tc.Name, tc.ChannelId 
	                                FROM CRM.SourcesTP st 
	                                left JOIN crm.Campaign c  ON st.SourcesTpId = c.SourcesTpId 
	                                INNER JOIN CRM.Type_Channel tc ON tc.ChannelId = st.ChannelId 
	                                where c.Franchiseid = @franchiseid and isnull(c.IsDeleted, 0) = 0 "
                                + condition
                                + " order by tc.Name; "
                                + @"SELECT ow.Name AS Owner, tc.Name AS Channel, s.Name AS Source, c.Name AS Name,
	                                CONVERT(date, c.StartDate ) AS StartDate,CONVERT(date, c.EndDate) AS EndDate,
	                                c.IsDeleted AS CampaignIsDeleted, c.IsActive AS CampaignIsActive, c.SourcesTPId,
	                                c.CampaignId,c.Memo,c.Amount
	                                ,c.isdeleted, st.ChannelId
                                    , c.Duration, c.RecurringType, c.DeactivateOnEndDate 
                                    , c.IsActive, c.FranchiseId
	                                FROM CRM.SourcesTP st left JOIN
	                                crm.Campaign c ON st.SourcesTpId = c.SourcesTpId INNER JOIN CRM.Sources s ON
	                                s.sourceId = st.sourceId INNER JOIN CRM.Type_Channel tc ON tc.ChannelId = st.ChannelId INNER JOIN
	                                CRM.Type_Owner ow ON ow.ownerid = st.ownerid 
	                                where c.Franchiseid = @franchiseid and isnull(c.IsDeleted, 0) = 0 "
                                + condition    
                                + " order by  StartDate desc;";

                    var result = connection.QueryMultiple(query
                        , new { franchiseid = franchiseId });
                    var channels = result.Read<ChannelDTO>().ToList();
                    var campaigns = result.Read<Campaign>().ToList();


                    foreach (var item in campaigns)
                    {
                        var recurringType = (RecurringType)item.RecurringType;

                        var totalMonths = 0;
                        if( recurringType == RecurringType.OneTime)
                        {
                            totalMonths = 1;
                        }
                        else if (recurringType == RecurringType.Monthly)
                        {
                            totalMonths = 1 * item.Duration;
                        }
                        else if (recurringType == RecurringType.Quarterly)
                        {
                            totalMonths = 3 * item.Duration;
                        }
                        else if (recurringType == RecurringType.Yearly)
                        {
                            totalMonths = 12 * item.Duration;
                        }

                        // The old campaings may have a invlid recurringType, which results 
                        // totalMonths = 0..
                        if (totalMonths > 0)
                            item.MonthlyBudget = item.Amount.Value / totalMonths;
                    }

                    foreach (var item in channels)
                    {
                        var list = campaigns
                            .Where(x => x.ChannelId == item.ChannelId).ToList();
                        item.Campaigns = list;

                        item.TotalCampaign = list.Count;
                        item.MonthlyCost = list.Sum(x => x.MonthlyBudget);
                    }

                    return channels;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
            }
        }

        public string UpdateCampaign(Campaign model)
        {
            try
            {
                var recurringType = (RecurringType)model.RecurringType;
                if (recurringType == RecurringType.OneTime &&
                    !model.EndDate.HasValue)
                {
                    throw new Exception("End date is requried for this Campaign.");
                }

                if (recurringType == RecurringType.Invalid)
                    throw new Exception("Vaid Recurring Type required for this Campaign.");

                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {

                    var campaign = connection.Get<Campaign>(model.CampaignId);

                    // The created on and created by should be un modified....
                    model.CreatedOn = campaign.CreatedOn;
                    model.CreatedBy = campaign.CreatedBy;
                    model.FranchiseId = campaign.FranchiseId;
                    model.StartDate = TimeZoneManager.ToUTC(model.StartDate);
                    
                    var edate = GetEndDate(model.StartDate, model.Duration, recurringType);
                    if (edate != null)
                        model.EndDate = edate;

                    model.LastUpdatedOn = DateTime.UtcNow;
                    model.LastUpdatedBy = this.User.PersonId;

                    var response = connection.Update(model);
                    return Success;
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }
        }

        private DateTime? GetEndDate(DateTime? startDate, int duration, RecurringType recurringType)
        {
            // For OneTime the user need to give the EndDate.
            // For Invlid The system can't calculate the EndDate.
            if (recurringType == RecurringType.OneTime ||
                recurringType == RecurringType.Invalid) return null;

            if (!startDate.HasValue) return null;

            var sdate = startDate.Value;
            DateTime? result = null;

            switch (recurringType)
            {
                case RecurringType.Monthly:
                    result = sdate.AddMonths(duration);
                    break;
                case RecurringType.Quarterly:
                    result = sdate.AddMonths(duration * 3);
                    break;
                case RecurringType.Yearly:
                    result = sdate.AddMonths(duration * 12);
                    break;
                default:
                    break;
            }

            return result;
        }

        public int AddCampaign(Campaign model)
        {
            try
            {
                var recurringType = (RecurringType)model.RecurringType;
                if (recurringType == RecurringType.OneTime && 
                    !model.EndDate.HasValue)
                {
                    throw new Exception("End date is requried for this Campaign.");
                }

                if (recurringType == RecurringType.Invalid)
                    throw new Exception("Vaid Recurring Type required for this Campaign.");


                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {

                    // TODO: end date to be updated based on recurrence and...
                    model.CreatedOn = DateTime.UtcNow;
                    model.CreatedBy = this.User.PersonId;
                    model.FranchiseId = this.Franchise.FranchiseId;
                    model.StartDate = TimeZoneManager.ToUTC(model.StartDate);
                    var edate = GetEndDate(model.StartDate, model.Duration, recurringType);
                    if (edate != null)
                        model.EndDate = edate;

                    var campaignId = (int)connection.Insert(model);
                    return campaignId;
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }
        }

       
    }
}
