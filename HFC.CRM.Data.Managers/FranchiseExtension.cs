﻿using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using HFC.CRM.Data.Geolocator;
using HFC.CRM.DTO.Source;
using HFC.CRM.Data.Context;
using HFC.CRM.DTO.Search;

namespace HFC.CRM.Managers
{


    /// <summary>
    /// This will add additional methods for retrieving franchise specific sets of data
    /// </summary>
    public static class FranchiseExtension
    {

        /// <summary>
        /// MigrationSources the type collection.
        /// </summary>
        /// <param name="fran">The fran.</param>
        /// <param name="sourceId">The source identifier.</param>
        /// <returns>Type_Source.</returns>
        public static Source SourceCollection(this Franchise fran, int sourceId)
        {
            return CacheManager.SourceCollection.SingleOrDefault(w => (!w.FranchiseId.HasValue || w.FranchiseId == fran.FranchiseId) && w.SourceId == sourceId);
        }

        public static List<Source> SourceCollection(this Franchise fran)
        {
            var linq = CacheManager.SourceCollection.Where(w => !w.FranchiseId.HasValue || w.FranchiseId == fran.FranchiseId && w.isDeleted == null);
            return linq.ToList();
        }

        /// <summary>
        /// Leads the status type collection.
        /// </summary>
        /// <param name="fran">The fran.</param>
        /// <param name="statusId">The status identifier.</param>
        /// <returns>Type_LeadStatus.</returns>
        public static Type_LeadStatus LeadStatusTypeCollection(this Franchise fran, int statusId)
        {
            var linq = CacheManager.LeadStatusTypeCollection.Where(w => (!w.FranchiseId.HasValue || w.FranchiseId == fran.FranchiseId) && w.Id == statusId);

            return linq.SingleOrDefault();
        }

        /// <summary>
        /// Orders the status type collection.
        /// </summary>
        /// <param name="fran">The fran.</param>
        /// <param name="statusId">The status identifier.</param>
        /// <returns>Type_OrderStatus.</returns>
        public static Type_OrderStatus OrderStatusTypeCollection(this Franchise fran, int statusId)
        {
            var linq = CacheManager.OrderStatusTypeCollection.Where(w => (!w.FranchiseId.HasValue || w.FranchiseId == fran.FranchiseId) && w.Id == statusId);

            return linq.SingleOrDefault();
        }

        /// <summary>
        /// Leads the status type collection.
        /// </summary>
        /// <param name="fran">The fran.</param>
        /// <returns>List&lt;Type_LeadStatus&gt;.</returns>
        public static List<Type_LeadStatus> LeadStatusTypeCollection(this Franchise fran)
        {
            var linq = CacheManager.LeadStatusTypeCollection.Where(w => !w.FranchiseId.HasValue || w.FranchiseId == fran.FranchiseId);

            var notSorted = linq.ToList();

            var result = new List<Type_LeadStatus>();
            var parents = notSorted.Where(ls => ls.ParentId == null).ToList();

            foreach (var parent in parents)
            {
                result.Add(parent);
                result.AddRange(notSorted.Where(l => l.ParentId == parent.Id));
            }

            return result;
        }
        /// <summary>
        /// Jobs the status type collection.
        /// </summary>
        /// <param name="fran">The fran.</param>
        /// <param name="statusId">The status identifier.</param>
        /// <returns>Type_JobStatus.</returns>
        public static Type_JobStatus JobStatusTypeCollection(this Franchise fran, int statusId)
        {
            return CacheManager.JobStatusTypeCollection.SingleOrDefault(w => (!w.FranchiseId.HasValue || w.FranchiseId == fran.FranchiseId) && w.Id == statusId);
        }
        /// <summary>
        /// Jobs the status type collection.
        /// </summary>
        /// <param name="fran">The fran.</param>
        /// <param name="statusId">The status identifier.</param>
        /// <param name="parentId">The parent identifier.</param>
        /// <returns>List&lt;Type_JobStatus&gt;.</returns>
        public static List<Type_JobStatus> JobStatusTypeCollection(this Franchise fran, int? statusId = null, int? parentId = null)
        {
            var linq = CacheManager.JobStatusTypeCollection.Where(w => !w.FranchiseId.HasValue || w.FranchiseId == fran.FranchiseId);
            if (statusId.HasValue)
                linq = linq.Where(w => w.Id == statusId);
            if (parentId.HasValue)
                linq = linq.Where(w => w.ParentId == parentId);
            var notSorted = linq.ToList();

            var parents = notSorted.Where(s => s.ParentId == null);

            var result = new List<Type_JobStatus>();

            foreach (var parent in parents)
            {
                result.Add(parent);
                result.AddRange(notSorted.Where(s => s.ParentId == parent.Id));
            }

            return result;
        }

        public static List<JobStatusDTO> JobStatusTypeCollectionDTO(this Franchise fran, int? statusId = null,
            int? parentId = null)
        {
            var linq =
                CacheManager.JobStatusTypeCollection.Where(
                    w => !w.FranchiseId.HasValue || w.FranchiseId == fran.FranchiseId);
            if (statusId.HasValue)
                linq = linq.Where(w => w.Id == statusId);
            if (parentId.HasValue)
                linq = linq.Where(w => w.ParentId == parentId);
            var result = new List<JobStatusDTO>();
            foreach (var status in linq.ToList().Where(s => s.isDeleted != true && s.ParentId == null))
            {
                var model = new JobStatusDTO { Id = status.Id, Name = status.Name, IsChiled = false };
                result.Add(model);
                var child =
                    status.Children.Where(
                        c => c.FranchiseId == null || c.FranchiseId == SessionManager.CurrentFranchise.FranchiseId)
                        .ToList();
                if (child.Any())
                {
                    foreach (var sub in child)
                    {
                        if (sub.isDeleted != true)
                        {
                            model = new JobStatusDTO { Id = sub.Id, Name = sub.Name, IsChiled = true };
                            result.Add(model);
                        }
                    }
                }
            }

            return result;
        }


        public static List<SourceDTO> SourceStatusTypeCollectionDTO(this Franchise fran, int? statusId = null,
            int? parentId = null)
        {

            FranchiseManager FranMgr = new FranchiseManager(SessionManager.CurrentUser);

            //  var sour = FranMgr.GetSources();
            var sourses = FranMgr.GetSourcesTwo();
            //var sourses = sour.Result;
            //var sourses = fran.GetSources();
            var list = new List<SourceDTO>();


            foreach (var parent in sourses.Where(v => v.ParentId == null))
            {
                list.Add(parent);
                list.AddRange(sourses.Where(v => v.ParentId == parent.SourceId && v.IsCampaign == true));

                foreach (var chield in sourses.Where(v => v.ParentId == parent.SourceId && v.IsCampaign != true))
                {
                    list.Add(chield);
                    list.AddRange(sourses.Where(v => v.ParentId == chield.SourceId && v.ParentId != parent.SourceId && v.IsCampaign == true));
                }
            }


            return list;

        }


        public static List<JobStatusDTO> LeadStatusTypeCollectionDTO(this Franchise fran, int? statusId = null,
          int? parentId = null)
        {
            var linq =
                CacheManager.LeadStatusTypeCollection.Where(
                    w => !w.FranchiseId.HasValue || w.FranchiseId == fran.FranchiseId);
            if (statusId.HasValue)
                linq = linq.Where(w => w.Id == statusId);
            if (parentId.HasValue)
                linq = linq.Where(w => w.ParentId == parentId);
            var result = new List<JobStatusDTO>();
            foreach (var status in linq.ToList().Where(s => s.isDeleted != true && s.ParentId == null))
            {
                var model = new JobStatusDTO { Id = status.Id, Name = status.Name, IsChiled = false };
                result.Add(model);
                var child =
                    status.Children.Where(
                        c => c.FranchiseId == null || c.FranchiseId == SessionManager.CurrentFranchise.FranchiseId)
                        .ToList();
                if (child.Any())
                {
                    foreach (var sub in child)
                    {
                        if (sub.isDeleted != true)
                        {
                            model = new JobStatusDTO { Id = sub.Id, Name = sub.Name, IsChiled = true };
                            result.Add(model);
                        }
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Products the type collection.
        /// </summary>
        /// <param name="fran">The fran.</param>
        /// <returns>List&lt;Type_Product&gt;.</returns>
        public static List<Type_Product> ProductTypeCollection(this Franchise fran)
        {
            var linq = CacheManager.ProductTypeCollection.Where(w => !w.FranchiseId.HasValue || w.FranchiseId == fran.FranchiseId);

            return linq.ToList();
        }
        /// <summary>
        /// Products the type collection.
        /// </summary>
        /// <param name="fran">The fran.</param>
        /// <param name="typeid">The typeid.</param>
        /// <returns>Type_Product.</returns>
        public static Type_Product ProductTypeCollection(this Franchise fran, int typeid)
        {
            return CacheManager.ProductTypeCollection.SingleOrDefault(w => (!w.FranchiseId.HasValue || w.FranchiseId == fran.FranchiseId) && w.ProductTypeId == typeid);
        }

        /// <summary>
        /// Products the category collection.
        /// </summary>
        /// <param name="fran">The fran.</param>
        /// <returns>List&lt;Type_Category&gt;.</returns>
        public static List<Type_Category> ProductCategoryCollection(this Franchise fran)
        {
            var linq = CacheManager.ProductCategoryCollection.Where(w => !w.FranchiseId.HasValue || w.FranchiseId == fran.FranchiseId);

            var categories = linq.ToList();
            foreach (var category in categories)
            {
                category.Category_Color = category.Category_Color.Where(cc => cc.FranchiseId == fran.FranchiseId).ToList();
            }

            return categories;
        }

        public static List<Type_Appointments> AppointmentTypesCollection(this Franchise fran)
        {
            var linq = CacheManager.AppointmentTypes.Where(w => !w.FranchiseId.HasValue || w.FranchiseId == fran.FranchiseId);

            return linq.ToList();
        }
        /// <summary>
        /// Products the category collection.
        /// </summary>
        /// <param name="fran">The fran.</param>
        /// <param name="cateogryId">The cateogry identifier.</param>
        /// <returns>Type_Category.</returns>
        public static Type_Category ProductCategoryCollection(this Franchise fran, int cateogryId)
        {
            return CacheManager.ProductCategoryCollection.SingleOrDefault(w => (!w.FranchiseId.HasValue || w.FranchiseId == fran.FranchiseId) && w.Id == cateogryId);
        }
        /// <summary>
        /// Products the category collection.
        /// </summary>
        /// <param name="fran">The fran.</param>
        /// <param name="categoryName">Name of the category.</param>
        /// <returns>Type_Category.</returns>
        public static Type_Category ProductCategoryCollection(this Franchise fran, string categoryName)
        {
            return CacheManager.ProductCategoryCollection.SingleOrDefault(w => (!w.FranchiseId.HasValue || w.FranchiseId == fran.FranchiseId) && string.Equals(w.Category, categoryName, StringComparison.InvariantCultureIgnoreCase));
        }
        /// <summary>
        /// Questions the type collection.
        /// </summary>
        /// <param name="fran">The fran.</param>
        /// <returns>List&lt;Type_Question&gt;.</returns>
        public static List<Type_Question> QuestionTypeCollection(this Franchise fran)
        {
            var linq = CacheManager.QuestionTypeCollection.Where(w => !w.FranchiseId.HasValue || w.FranchiseId == fran.FranchiseId);

            return linq.OrderBy(o => o.Question).ThenBy(o => o.DisplayOrder).ToList();
        }

        /// <summary>
        /// Users the collection.
        /// </summary>
        /// <param name="fran">The fran.</param>
        /// <returns>List&lt;User&gt;.</returns>
        public static List<User> UserCollection(this Franchise fran)
        {
            if (fran == null)
            {
                return new List<User>();
            }

            var users = CacheManager.UserCollection.Where(w => w.FranchiseId == fran.FranchiseId).ToList();

            try
            {
                foreach (User usr in users)
                {
                    var lstUserColorPalette = CacheManager.ColorPalettes.Where(x => x.ColorId == usr.ColorId).FirstOrDefault();
                    if (lstUserColorPalette == null)
                    {
                        CacheManager.RefreshColorPalettes();
                        lstUserColorPalette = CacheManager.ColorPalettes.Where(x => x.ColorId == usr.ColorId).FirstOrDefault();
                        if(lstUserColorPalette == null)
                        {
                            System.Text.StringBuilder sb = new System.Text.StringBuilder();
                            sb.Append("Environment: " + System.Environment.MachineName + System.Environment.NewLine);
                            sb.Append("FranchiseId: " + fran.FranchiseId + System.Environment.NewLine);
                            sb.Append("CurrentUser: " + (SessionManager.CurrentUser != null ? SessionManager.CurrentUser.UserName : "") + System.Environment.NewLine);
                            sb.Append("Issue Username: " + usr.UserName + System.Environment.NewLine);
                            sb.Append("Issue UserColorId: " + usr.ColorId);

                            Core.Logs.EventLogger.LogEvent(sb.ToString(), "UserCollection Login Issue");

                            //Below code to update the default colorid
                            usr.ColorId = CacheManager.ColorPalettes.First().ColorId;
                        }
                    }
                }
            }
            catch (Exception ex) { Core.Logs.EventLogger.LogEvent(ex); }

            return users;
        }

        /// <summary>
        /// Users the collection.
        /// </summary>
        /// <param name="fran">The fran.</param>
        /// <param name="personId">The person identifier.</param>
        /// <returns>User.</returns>
        public static User UserCollection(this Franchise fran, int personId)
        {
            return CacheManager.UserCollection.FirstOrDefault(w => w.FranchiseId == fran.FranchiseId && w.PersonId == personId);
        }

        /// <summary>
        /// Users the collection.
        /// </summary>
        /// <param name="fran">The fran.</param>
        /// <param name="roleGuids">The role guids.</param>
        /// <returns>List&lt;User&gt;.</returns>
        public static List<User> UserCollection(this Franchise fran, params Guid[] roleGuids)
        {
            var users = CacheManager.UserCollection.Where(w => w.FranchiseId == fran.FranchiseId && w.IsInRole(roleGuids)).ToList();
            return users;
        }

        /// <summary>
        /// Users the collection.
        /// </summary>
        /// <param name="fran">The fran.</param>
        /// <param name="roles">The roles.</param>
        /// <returns>List&lt;User&gt;.</returns>
        public static List<User> UserCollection(this Franchise fran, params Role[] roles)
        {
            var users = CacheManager.UserCollection.Where(w => w.FranchiseId == fran.FranchiseId && w.IsInRole(roles)).ToList();
            return users;
        }

        /// <summary>
        /// Gets the franchise tax rule from DB and saves to Franchise.FranchiseTaxRules property as well as returning a list of the tax rule
        /// </summary>
        public static List<FranchiseTaxRule> GetTaxRules(this Franchise fran)
        {
            fran.FranchiseTaxRules = ContextFactory.Current.Context.FranchiseTaxRules.Where(w => w.FranchiseId == fran.FranchiseId).ToList();
            return fran.FranchiseTaxRules.ToList();
        }

        public static FranchiseTaxRule GetTaxRule(this Franchise fran, Address address)
        {
            if (address == null || (string.IsNullOrEmpty(address.ZipCode) && string.IsNullOrEmpty(address.City)))
            {
                //return any base tax rule
                return fran.FranchiseTaxRules.FirstOrDefault(w => string.IsNullOrEmpty(w.County)
                    && string.IsNullOrEmpty(w.ZipCode) && string.IsNullOrEmpty(w.City) && string.IsNullOrEmpty(w.State));
            }

            string trimmedZipPostal = (address.ZipCode ?? "").Replace(" ", "");

            using (var geo = new GeoLocatorEntities())
            {
                if (fran.FranchiseTaxRules == null)
                    fran.GetTaxRules();

                if (fran.CountryCode == "US")
                {
                    var tax = fran.FranchiseTaxRules.FirstOrDefault(w => w.ZipCode == trimmedZipPostal);
                    if (tax != null)
                        return tax;

                    var county = geo.ZipPostalCodes.FirstOrDefault(s => s.CountryCode == "US" && s.ZipCode == trimmedZipPostal);
                    if (county != null && !string.IsNullOrEmpty(county.County))
                    {
                        tax = fran.FranchiseTaxRules.FirstOrDefault(s => string.Equals(s.County, county.County, StringComparison.InvariantCultureIgnoreCase));
                        if (tax != null)
                            return tax;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(trimmedZipPostal) && trimmedZipPostal.Length >= 3)
                    {
                        //match all 6 postalcode digits, no spaces
                        var tax = fran.FranchiseTaxRules.FirstOrDefault(w => w.ZipCode == trimmedZipPostal);
                        if (tax != null)
                            return tax;

                        //get province from first 3 digits
                        var first3 = trimmedZipPostal.Substring(0, 3);
                        var province = geo.ZipPostalCodes.FirstOrDefault(s => s.CountryCode == "CA" && DbFunctions.Left(s.ZipCode, 3) == first3);
                        if (province != null && !string.IsNullOrEmpty(province.State))
                        {
                            tax = fran.FranchiseTaxRules.SingleOrDefault(s => string.Equals(s.County, province.State, StringComparison.InvariantCultureIgnoreCase));
                            if (tax != null)
                                return tax;
                        }
                    }
                }
            }

            //lastly if nothing else matches, then return any base tax rule
            return fran.FranchiseTaxRules.FirstOrDefault(w => string.IsNullOrEmpty(w.County) && string.IsNullOrEmpty(w.ZipCode) && string.IsNullOrEmpty(w.City) && string.IsNullOrEmpty(w.State) && w.Default == true);
        }

        public static List<FranchiseRoyalty> GetRoyalty(this Franchise fran, int territoryId, DateTime startDate, DateTime endDate)
        {
            return ContextFactory.Current.Context.FranchiseRoyalties.AsNoTracking().Where(w => w.TerritoryId == territoryId && w.StartOnUtc <= startDate && endDate <= w.EndOnUtc).ToList();
        }

        public static List<Territory> GetTerritories(this Franchise franchise)
        {
            return ContextFactory.Current.Context.Territories.AsNoTracking().Where(w => w.FranchiseId == franchise.FranchiseId).ToList();
        }

        /// <summary>
        /// Gets the sources.
        /// </summary>
        /// <param name="fran">The fran.</param>
        /// <returns>List&lt;Type_Source&gt;.</returns>
        public static List<Type_Source> GetTypeSources(this Franchise fran)
        {
            return ContextFactory.Current.Context.Type_Sources.AsNoTracking().Where(w => w.FranchiseId == fran.FranchiseId || !w.FranchiseId.HasValue).Include(i => i.MarketingCampaigns).ToList();
        }

        /// <summary>
        /// Gets the sources.
        /// </summary>
        /// <param name="fran">The fran.</param>
        public static IQueryable<Source> GetSources(this Franchise fran)
        {
            return
                ContextFactory.Current.Context.Sources.AsNoTracking().Include(i => i.Parent)
                    .Include(i => i.Children)
                    .Where(w => w.FranchiseId == fran.FranchiseId || !w.FranchiseId.HasValue)
                    .Where(w => w.isDeleted != true)
                    .OrderBy(o => o.Name);
        }
    }
}
