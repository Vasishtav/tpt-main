﻿using Dapper;
using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Core.Membership;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using HFC.CRM.DTO;
using HFC.CRM.DTO.Calandar;
using HFC.CRM.DTO.SentEmail;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Twilio;
using Twilio.Exceptions;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace HFC.CRM.Managers
{

    public enum EmailType
    {
        OptinOptout = 0,
        ThankyouInquiry = 1, //Type1 = 1,
        AppointmentConfirmation = 2, //Type2 = 2,
        Reminder = 3, // Type3 = 3,
        OrderSummary = 4, //Type4 = 4,
        Installation = 5,  //Type5 = 5,
        ThankyouReview = 6,  //Type6 = 6,
        //Type7 = 7,
        ProcurementAlert = 8, //Type8 = 8,
        ProcurementProcessingIssues, //Type9 = 9,
        GlobalEmail = 10, //Type10 = 10
        CaseFeedback=11,
        CaseChange = 12,

    }

    public class CommunicationManager : DataManager
    {
        private LeadsManager LeadMgr;
        private OrderManager ordermgr;
        private FranchiseManager _franMgr;
        private PaymentManager PayMgr;
        private CalendarManager CalendarMgr;
        private EmailManager emailMgr;
        private OpportunitiesManager OpportunityMgr;
        private AccountsManager AccountMgr;
        private TextingHistoryManager TxtHismgr;
        private TextTemplateManager texttemp;

        public CommunicationManager(User user, Franchise franchise)
        {
            this.User = user;
            this.Franchise = franchise;
            if (user != null && franchise != null)
            {
                LeadMgr = new LeadsManager(user, franchise);
                _franMgr = new FranchiseManager(user, franchise);
                PayMgr = new PaymentManager(user, franchise);
                CalendarMgr = new CalendarManager(user);
                emailMgr = new EmailManager(user, franchise);
                OpportunityMgr = new OpportunitiesManager(user, franchise);
                AccountMgr = new AccountsManager(user, franchise);
                ordermgr = new OrderManager(user, franchise);
                TxtHismgr = new TextingHistoryManager(user, franchise);
                texttemp = new TextTemplateManager(user, franchise);
            }
        }

        public string ResendEmailNew(int SentEmailId)
        {
            string ResendMail = "ResendEmail";
            string query = "select * from history.SentEmails where SentEmailId=@SentEmailId";
            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var data = con.Query<SentEmail>(query, new { SentEmailId }).FirstOrDefault();

                //int templateEmailId = getemailtypeid(Convert.ToInt32(data.TemplateId));
                EmailType templateEmailId = getemailtypeid(Convert.ToInt32(data.TemplateId));

                if (templateEmailId == EmailType.ThankyouInquiry) // == 1)
                {
                    var result = LeadMgr.SendThankYouEmail((int)data.LeadId, EmailType.ThankyouInquiry);  //, 1);
                    return "Success";
                }
                else if (templateEmailId == EmailType.AppointmentConfirmation)  //== 2)
                {
                    if (data.CalendarId == null)
                    {
                        var result = _franMgr.ResendEmail(SentEmailId);
                        return "Success";
                    }
                    else
                    {
                        var Query = @"select * from CRM.Calendar cal where cal.CalendarId=@calendarid";
                        var calid = data.CalendarId;
                        var caloutput = con.Query<Calendar>(Query, new { calendarid = calid }).FirstOrDefault();
                        if (caloutput != null)
                        {
                            var EQuey = "select * from CRM.EventToPeople where CalendarId =@calendarid";
                            var Eoutput = con.Query<EventToPerson>(EQuey, new { calendarid = calid }).ToList();
                            if (Eoutput != null)
                            {
                                caloutput.Attendees = Eoutput;
                            }
                            CalendarVM newdata = new CalendarVM();
                            newdata.CalendarId = caloutput.CalendarId;
                            newdata.Subject = caloutput.Subject;
                            newdata.LeadId = caloutput.LeadId;
                            newdata.AccountId = caloutput.AccountId;
                            newdata.OpportunityId = caloutput.OpportunityId;
                            newdata.OrderId = caloutput.OrderId;
                            newdata.CaseId = caloutput.CaseId;
                            newdata.VendorCaseId = caloutput.VendorCaseId;
                            newdata.AssignedName = caloutput.AssignedName;
                            newdata.StartDate = TimeZoneManager.ToLocal(caloutput.StartDate.DateTime);
                            newdata.EndDate = TimeZoneManager.ToLocal(caloutput.EndDate.DateTime);
                            newdata.AptTypeEnum = caloutput.AptTypeEnum;
                            newdata.CreatedByPersonId = caloutput.CreatedByPersonId;
                            newdata.Attendees = caloutput.Attendees;
                            newdata.OrganizerEmail = caloutput.OrganizerEmail;
                            newdata.OrganizerName = caloutput.OrganizerName;
                            newdata.OrganizerPersonId = caloutput.OrganizerPersonId;
                            newdata.RecurringEventId = caloutput.RecurringEventId;
                            newdata.Location = caloutput.Location;
                            newdata.AssignedPersonId = caloutput.AssignedPersonId;
                            newdata.PhoneNumber = caloutput.PhoneNumber;
                            newdata.Message = caloutput.Message;
                            newdata.FranchiseId = caloutput.FranchiseId;
                            var result = CalendarMgr.SendEmail(newdata, EmailType.AppointmentConfirmation); //, 2);
                            return "Success";
                        }
                    }
                }
                else if (templateEmailId == EmailType.Reminder) //== 3)
                {
                    if (data.CalendarId == null)
                    {
                        var result = _franMgr.ResendEmail(SentEmailId);
                        return "Success";
                    }
                    else
                    {
                        var result = emailMgr.SendRemaindarEmail();
                        return "Success";
                    }
                }
                else if (templateEmailId == EmailType.OrderSummary)  //== 4)
                {
                    var orderresult = ordermgr.GetOrder((int)data.OrderId, Franchise.FranchiseId);
                    if (orderresult != null)
                    {
                        var result = PayMgr.SendOrderSummaryEmail(orderresult.OrderID, EmailType.OrderSummary, ""); //, 4, "");
                        return "Success";
                    }
                    //var result = ordermgr.GetOrder(id, FranchiseId);
                }
                else if (templateEmailId == EmailType.Installation) //== 5)
                {
                    if (data.CalendarId == null)
                    {
                        var result = _franMgr.ResendEmail(SentEmailId);
                        return "Success";
                    }
                    else
                    {
                        var Query = @"select * from CRM.Calendar cal where cal.CalendarId=@calendarid";
                        var caloutput = con.Query<Calendar>(Query, new { calendarid = data.CalendarId }).FirstOrDefault();
                        if (caloutput != null)
                        {
                            var EQuey = "select * from CRM.EventToPeople where CalendarId =@calendarid";
                            var Eoutput = con.Query<EventToPerson>(EQuey, new { calendarid = data.CalendarId }).ToList();
                            if (Eoutput != null)
                            {
                                caloutput.Attendees = Eoutput;
                            }
                            CalendarVM newdata = new CalendarVM();
                            newdata.CalendarId = caloutput.CalendarId;
                            newdata.Subject = caloutput.Subject;
                            newdata.LeadId = caloutput.LeadId;
                            newdata.AccountId = caloutput.AccountId;
                            newdata.OpportunityId = caloutput.OpportunityId;
                            newdata.OrderId = caloutput.OrderId;
                            newdata.CaseId = caloutput.CaseId;
                            newdata.VendorCaseId = caloutput.VendorCaseId;
                            newdata.AssignedName = caloutput.AssignedName;
                            newdata.StartDate = TimeZoneManager.ToLocal(caloutput.StartDate.DateTime);
                            newdata.EndDate = TimeZoneManager.ToLocal(caloutput.EndDate.DateTime);
                            newdata.AptTypeEnum = caloutput.AptTypeEnum;
                            newdata.CreatedByPersonId = caloutput.CreatedByPersonId;
                            newdata.Attendees = caloutput.Attendees;
                            newdata.OrganizerEmail = caloutput.OrganizerEmail;
                            newdata.OrganizerName = caloutput.OrganizerName;
                            newdata.OrganizerPersonId = caloutput.OrganizerPersonId;
                            newdata.RecurringEventId = caloutput.RecurringEventId;
                            newdata.Location = caloutput.Location;
                            newdata.AssignedPersonId = caloutput.AssignedPersonId;
                            newdata.PhoneNumber = caloutput.PhoneNumber;
                            newdata.Message = caloutput.Message;
                            newdata.FranchiseId = caloutput.FranchiseId;
                            var result = CalendarMgr.SendEmail(newdata, EmailType.Installation); //, 5);
                            return "Success";
                        }
                    }
                }
                else if (templateEmailId == EmailType.ThankyouReview) //== 6)
                {

                    var OrderDetaisl = ordermgr.GetOrder((int)data.OrderId, Franchise.FranchiseId);
                    if (OrderDetaisl != null)
                    {
                        if (OrderDetaisl.OrderStatus == 5)
                        {
                            var InvoiceHistory = ordermgr.GetInvoiceHistory(OrderDetaisl.OrderID);
                            if (InvoiceHistory != null)
                            {
                                // 6, InvoiceHistory.Streamid.ToString(), true, ResendMail);
                                var result = ordermgr.SendOrdermail(OrderDetaisl.OrderID, EmailType.ThankyouReview, InvoiceHistory.Streamid.ToString(), true, ResendMail);
                                return "Success";
                            }

                            else
                            {
                                var result = ordermgr.SendOrdermail(OrderDetaisl.OrderID, EmailType.ThankyouReview, "", true, ResendMail);
                                return "Success";
                            }

                        }
                    }
                }
                else if (templateEmailId == EmailType.GlobalEmail) //== 10)
                {
                    // TODO: Don't we just need to call RresetEmail method???
                    if (data.LeadId != null)
                    {
                        var result = _franMgr.ResendEmail(SentEmailId);
                        return "Success";
                    }
                    else if (data.OpportunityId != null)
                    {
                        var result = _franMgr.ResendEmail(SentEmailId);
                        return "Success";
                    }
                    else if (data.OrderId != null)
                    {
                        var result = _franMgr.ResendEmail(SentEmailId);
                        return Success;
                    }
                }
            }
            return null;
        }

        public string ResendTexting(int textingId)
        {
            //var query = "select * from History.Texting where TextingId = @id";
            var query = "select * from History.SentEmails where SentEmailId= @id";
            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                //var data = con.Query<TextingHistory>(query, new { id = textingId }).FirstOrDefault();
                var data = con.Query<SentEmail>(query, new { id = textingId }).FirstOrDefault();
                if (data == null) return "Invalid Texting Id";

                EmailType templateType = (EmailType)data.TextingTemplateTypeId;
                if (data.CalendarId.HasValue && data.CalendarId > 0)
                {
                    var calendarvm = CalendarMgr.GetEvent(data.CalendarId.Value);
                    var modal = CalendarMgr.GetTextingModal(calendarvm);

                    SendTextSms(modal, templateType);
                }
                else if (data.TextingTemplateTypeId == (int)EmailType.OptinOptout)
                {
                    ForceOptinMessage(data.CellPhone, Franchise.BrandId, Franchise.CountryCode, null, null);
                }
                else if (data.OrderId.HasValue && data.OrderId.Value > 0)
                {
                    SendOrderTextSms(data.OrderId.Value, templateType);
                }
                else if (data.TextingTemplateTypeId == (int)EmailType.Reminder)
                {
                    SendRemainderTextSms();
                }
                else
                {
                    return "Invalid option : textingId: " + textingId;
                }
            }

            return Success;
        }

        public EmailType getemailtypeid(int templateid)
        {
            switch (templateid)
            {
                case 501:
                case 502:
                case 503:
                    //return 1;
                    return EmailType.ThankyouInquiry;
                case 504:
                case 505:
                case 506:
                    //return 2;
                    return EmailType.AppointmentConfirmation;
                case 507:
                case 508:
                case 509:
                    //return 3;
                    return EmailType.Reminder;
                case 510:
                case 511:
                case 512:
                    //return 4;
                    return EmailType.OrderSummary;
                case 513:
                case 514:
                case 515:
                    //return 5;
                    return EmailType.Installation;
                case 516:
                case 517:
                case 518:
                    //return 6;
                    return EmailType.ThankyouReview;
                case 521:
                case 522:
                case 523:
                    //return 10;
                    return EmailType.GlobalEmail;
                case 524:                
                    return EmailType.CaseFeedback;
                case 525:
                    return EmailType.CaseChange;
                default: return EmailType.ThankyouInquiry; //return 1;
            }
        }

        public SentEmail GetHistory(int id)
        {
            string query = @"select *,lv.Name as Type from history.SentEmails s 
                            left join CRM.Type_LookUpValues lv on lv.Id = s.LogType
                            where SentEmailId = @id";
            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var data = con.Query<SentEmail>(query, new { id }).FirstOrDefault();
                data.SentAt = TimeZoneManager.ToLocal(data.SentAt);
                return data;
            }
        }

        public SentEmail GetEmaildetails(int id)
        {
            string query = "select * from history.SentEmails where SentEmailId=@id";
            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var data = con.Query<SentEmail>(query, new { id }).FirstOrDefault();
                //int templateEmailId =  getemailtypeid(Convert.ToInt32(data.TemplateId));
                EmailType templateEmailId = getemailtypeid(Convert.ToInt32(data.TemplateId));

                if (templateEmailId == EmailType.ThankyouInquiry) //== 1)
                {
                    var result = LeadMgr.Get((int)data.LeadId);
                    if (result != null)
                    {
                        if (!string.IsNullOrEmpty(result.PrimCustomer.PrimaryEmail) && result.PrimCustomer.PrimaryEmail != "")
                        {
                            if (result.PrimCustomer.PrimaryEmail.Trim() != "")
                            {

                                data.Recipients = result.PrimCustomer.PrimaryEmail.Trim();
                            }
                        }
                        else if (!string.IsNullOrEmpty(result.PrimCustomer.SecondaryEmail) && result.PrimCustomer.SecondaryEmail != "")
                        {
                            if (result.PrimCustomer.SecondaryEmail.Trim() != "")
                            {
                                data.Recipients = (result.PrimCustomer.SecondaryEmail.Trim());
                            }
                        }
                    }
                    return data;

                }
                else if (templateEmailId == EmailType.AppointmentConfirmation) //== 2)
                {
                    if (data.CalendarId == null)
                    {
                        var result = data;
                        return result;
                    }
                    else
                    {

                        var Query = @"select * from CRM.Calendar cal where cal.CalendarId=@calendarid";
                        var caloutput = con.Query<Calendar>(Query, new { calendarid = data.CalendarId }).FirstOrDefault();
                        if (caloutput.AccountId != null && caloutput.AccountId > 0)
                        {
                            var results = emailMgr.GetAccountEmail((int)caloutput.AccountId);
                            if (results != null)
                            {
                                if (results.PrimaryEmail != null && results.PrimaryEmail.Trim() != "")
                                {
                                    data.Recipients = (results.PrimaryEmail.Trim());
                                }
                                else if (results.SecondaryEmail != null && results.SecondaryEmail.Trim() != "")
                                {
                                    data.Recipients = (results.SecondaryEmail.Trim());
                                }
                                return data;
                            }


                        }
                        else if (caloutput.LeadId != null && caloutput.LeadId > 0)
                        {
                            var results = emailMgr.GetLeadEmail((int)caloutput.LeadId);
                            if (results != null)
                            {
                                if (results.PrimaryEmail != null && results.PrimaryEmail.Trim() != "")
                                {
                                    data.Recipients = (results.PrimaryEmail.Trim());
                                }
                                else if (results.SecondaryEmail != null && results.SecondaryEmail.Trim() != "")
                                {
                                    data.Recipients = (results.SecondaryEmail.Trim());
                                }
                                return data;
                            }
                        }
                    }
                }
                else if (templateEmailId == EmailType.Reminder) //== 3)
                {
                    if (data.CalendarId == null)
                    {
                        var result = data;
                        return result;
                    }
                    else
                    {
                        var Query = @"select * from CRM.Calendar cal where cal.CalendarId=@calendarid";
                        var caloutput = con.Query<Calendar>(Query, new { calendarid = data.CalendarId }).FirstOrDefault();
                        CustomerTP LeadCustomer = new CustomerTP();
                        if (caloutput.AccountId > 0)
                        {
                            LeadCustomer = emailMgr.GetAccountEmail((int)caloutput.AccountId);
                        }
                        else if (caloutput.LeadId > 0)
                        {
                            LeadCustomer = emailMgr.GetLeadEmail((int)caloutput.LeadId);
                        }

                        //If Valid email 
                        if (LeadCustomer != null)
                        {
                            if (LeadCustomer != null)
                            {
                                if (LeadCustomer.PrimaryEmail != null && LeadCustomer.PrimaryEmail.Trim() != "")
                                {
                                    data.Recipients = (LeadCustomer.PrimaryEmail.Trim());
                                }
                                else if (LeadCustomer.SecondaryEmail != null && LeadCustomer.SecondaryEmail.Trim() != "")
                                {
                                    data.Recipients = (LeadCustomer.SecondaryEmail.Trim());
                                }
                            }
                        }
                        return data;
                    }
                }
                else if (templateEmailId == EmailType.OrderSummary) //== 4)
                {
                    var tocustomerAddress = emailMgr.GetOrderCustomerEmail((int)data.OrderId);

                    if (tocustomerAddress != null)
                    {
                        if (tocustomerAddress.PrimaryEmail != null && tocustomerAddress.PrimaryEmail.Trim() != "")
                        {
                            data.Recipients = (tocustomerAddress.PrimaryEmail.Trim());
                        }
                        else if (tocustomerAddress.SecondaryEmail != null && tocustomerAddress.SecondaryEmail.Trim() != "")
                        {
                            data.Recipients = (tocustomerAddress.SecondaryEmail.Trim());
                        }
                        return data;
                    }
                }
                else if (templateEmailId == EmailType.Installation) //== 5)
                {
                    if (data.CalendarId == null)
                    {
                        var result = data;
                        return result;
                    }
                    else
                    {

                        var Query = @"select * from CRM.Calendar cal where cal.CalendarId=@calendarid";
                        var caloutput = con.Query<Calendar>(Query, new { calendarid = data.CalendarId }).FirstOrDefault();
                        if (caloutput.AccountId != null && caloutput.AccountId > 0)
                        {
                            var results = emailMgr.GetAccountEmail((int)caloutput.AccountId);
                            if (results != null)
                            {
                                if (results.PrimaryEmail != null && results.PrimaryEmail.Trim() != "")
                                {
                                    data.Recipients = (results.PrimaryEmail.Trim());
                                }
                                else if (results.SecondaryEmail != null && results.SecondaryEmail.Trim() != "")
                                {
                                    data.Recipients = (results.SecondaryEmail.Trim());
                                }
                                return data;
                            }


                        }
                        else if (caloutput.LeadId != null && caloutput.LeadId > 0)
                        {
                            var results = emailMgr.GetLeadEmail((int)caloutput.LeadId);
                            if (results != null)
                            {
                                if (results.PrimaryEmail != null && results.PrimaryEmail.Trim() != "")
                                {
                                    data.Recipients = (results.PrimaryEmail.Trim());
                                }
                                else if (results.SecondaryEmail != null && results.SecondaryEmail.Trim() != "")
                                {
                                    data.Recipients = (results.SecondaryEmail.Trim());
                                }
                                return data;
                            }
                        }
                    }
                }
                else if (templateEmailId == EmailType.ThankyouReview) //== 6)
                {
                    var tocustomerAddress = emailMgr.GetOrderCustomerEmail((int)data.OrderId);

                    if (tocustomerAddress != null)
                    {
                        if (tocustomerAddress.PrimaryEmail != null && tocustomerAddress.PrimaryEmail.Trim() != "")
                        {
                            data.Recipients = (tocustomerAddress.PrimaryEmail.Trim());
                        }
                        else if (tocustomerAddress.SecondaryEmail != null && tocustomerAddress.SecondaryEmail.Trim() != "")
                        {
                            data.Recipients = (tocustomerAddress.SecondaryEmail.Trim());
                        }
                    }
                    return data;
                }
                else if (templateEmailId == EmailType.GlobalEmail) //== 10)
                {
                    return data;
                }
            }
            return null;
        }

        public Response SendSms(string toNumber, string body, int BrandId, string Country)
        {
            // Defualt we are sending only US and canada
            toNumber = "+1" + toNumber;
            string fromNumber = "";

            var accountSid = AppConfigManager.GetConfig<string>("TwilioAccountSid", "Twilio", "TwilioMessage");
            var authToken = AppConfigManager.GetConfig<string>("TwilioAuthToken", "Twilio", "TwilioMessage");

            if (BrandId == 1)
            {
                if (Country.ToUpper() == "US")
                    fromNumber = AppConfigManager.GetConfig<string>("TwiliofromNumberBBUS", "Twilio", "TwilioMessage");
                if (Country.ToUpper() == "CA")
                    fromNumber = AppConfigManager.GetConfig<string>("TwiliofromNumberBBCA", "Twilio", "TwilioMessage");
            }
            else if (BrandId == 2)
            {
                if (Country.ToUpper() == "US")
                    fromNumber = AppConfigManager.GetConfig<string>("TwiliofromNumberTLUS", "Twilio", "TwilioMessage");
                if (Country.ToUpper() == "CA")
                    fromNumber = AppConfigManager.GetConfig<string>("TwiliofromNumberTLCA", "Twilio", "TwilioMessage");
            }
            else if (BrandId == 3)
            {
                if (Country.ToUpper() == "US")
                    fromNumber = AppConfigManager.GetConfig<string>("TwiliofromNumberCCUS", "Twilio", "TwilioMessage");
                if (Country.ToUpper() == "CA")
                    fromNumber = AppConfigManager.GetConfig<string>("TwiliofromNumberCCCA", "Twilio", "TwilioMessage");
            }

            TwilioClient.Init(accountSid, authToken);

            try
            {
                var res = MessageResource.Create(
                      body: body,
                      from: new PhoneNumber(fromNumber),
                      to: new PhoneNumber(toNumber)
                  );
                EventLogger.LogEvent("from: " + fromNumber + ", to: " + toNumber + ", body: " + body, "SendSms");
                return new Response(true, "");
            }
            catch (ApiException e)
            {
                if (e.Code == 21614)
                {
                    EventLogger.LogEvent("from: " + fromNumber + ", to: " + toNumber + ", body: " + body + " Error: looks like this caller can't receive SMS messages.", "SendSms", severity: LogSeverity.Error);
                    EventLogger.LogEvent(e);
                    return new Response(false, "looks like this caller can't receive SMS messages.");
                }
                EventLogger.LogEvent("from: " + fromNumber + ", to: " + toNumber + ", body: " + body + " Error: " + e.Message.ToString(), "SendSms", severity: LogSeverity.Error);
                EventLogger.LogEvent(e);
                return new Response(false, e.Message.ToString());
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return new Response(false, ex.Message.ToString());
            }
        }

        /// <summary>
        /// Send the Optin message to the customer and Insert the Phone number into smsOptinginfo table
        /// </summary>
        /// <param name="Phonenumber"></param>
        /// <param name="BrandId"></param>
        /// <returns></returns>
        public Response SendOptinMessage(string Phonenumber, int BrandId, string Country, int? LeadId = null, int? AccountId = null)
        {
            try
            {
                using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    string qoptinfo = "select * from CRM.smsOptinginfo where Phonenumber = @Phonenumber";
                    var optinfo = con.Query<smsOptinginfo>(qoptinfo, new { Phonenumber, BrandId }).FirstOrDefault();
                    if (optinfo == null)
                    {
                        string body = "";

                        if (BrandId == 1)
                            body = "Text YES/CONFIRM to receive Budget Blinds Account related alerts. Msg&data rates may apply. Reply HELP/INFO for help, STOP/ARRET to cancel";
                        else if (BrandId == 2)
                            body = "Text YES/CONFIRM to receive Tailored Living Account related alerts. Msg&data rates may apply. Reply HELP/INFO for help, STOP/ARRET to cancel";
                        else if (BrandId == 3)
                            body = "Text YES/CONFIRM to receive Concrete Craft Account related alerts. Msg&data rates may apply. Reply HELP/INFO for help, STOP/ARRET to cancel";

                        // Send Optin sms
                        var res = SendSms(Phonenumber, body, BrandId, Country);

                        // Save Optin Optout information
                        smsOptinginfo s = new smsOptinginfo();
                        s.Phonenumber = Phonenumber;
                        s.Optin = null;
                        s.Optout = null;
                        s.BrandId = BrandId;
                        s.IsOptinmessagesent = res.status;
                        s.Errorinfo = res.message;
                        s.CreatedOn = DateTime.UtcNow;
                        s.LastUpdatedOn = DateTime.UtcNow;
                        s.Id = (int)con.Insert(s);

                        // Texting History
                        //TextingHistory m = new TextingHistory();
                        //m.CellPhone = Phonenumber;
                        //m.Subject = "Text Opt In Send";
                        //m.Body = body;
                        //m.SentByPersonId = User.PersonId;
                        //m.SentAt = DateTime.UtcNow;
                        //m.Status = res.status == true ? 1 : 3;
                        //m.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                        //m.TemplateTypeId = 0;
                        //TxtHismgr.Add(m);
                        var Modelvalue = GetModeldata(Phonenumber, null, null, null, null, body, res, 0, BrandId, null, null);
                        var result = SaveNewLogTexting(Modelvalue, ContantStrings.AddType);

                        if (res.status)
                            return new Response(true, "", s);
                        else
                            return new Response(false, res.message, s);
                    }
                    else return new Response(false, "Already Exist", optinfo);
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return null;
            }

        }

        /// <summary>
        /// Force Optin message to the Customer already Optin sent
        /// </summary>
        /// <param name="Phonenumber"></param>
        /// <param name="BrandId"></param>
        /// <returns></returns>
        public Response ForceOptinMessage(string Phonenumber, int BrandId, string Country, int? LeadId = null, int? AccountId = null)
        {
            try
            {
                string body = "";

                if (BrandId == 1)
                    body = "Text YES/CONFIRM to receive Budget Blinds Account related alerts. Msg&data rates may apply. Reply HELP/INFO for help, STOP/ARRET to cancel";
                else if (BrandId == 2)
                    body = "Text YES/CONFIRM to receive Tailored Living Account related alerts. Msg&data rates may apply. Reply HELP/INFO for help, STOP/ARRET to cancel";
                else if (BrandId == 3)
                    body = "Text YES/CONFIRM to receive Concrete Craft Account related alerts. Msg&data rates may apply. Reply HELP/INFO for help, STOP/ARRET to cancel";

                var res = SendSms(Phonenumber, body, BrandId, Country);

                // Update smsOptinginfo table
                var smsinfo = GetOptingInfo(Phonenumber);
                if (smsinfo != null)
                {
                    using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
                    {
                        smsinfo.BrandId = BrandId;
                        smsinfo.Optin = null;
                        smsinfo.Optout = null;
                        smsinfo.IsOptinmessagesent = res.status;
                        smsinfo.Errorinfo = res.message;
                        smsinfo.LastUpdatedOn = DateTime.UtcNow;
                        con.Update(smsinfo);
                    }
                }

                // Texting History
                //TextingHistory m = new TextingHistory();
                //m.CellPhone = Phonenumber;
                //m.Subject = "Text Opt In Send";
                //m.Body = body;
                //m.SentByPersonId = User.PersonId;
                //m.SentAt = DateTime.UtcNow;
                //m.Status = res.status == true ? 1 : 3;
                //m.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                //m.TemplateTypeId = 0;
                //TxtHismgr.Add(m);
                var Modelvalue = GetModeldata(Phonenumber, null, null, null, null, body, res, 0, BrandId, null,null);
                var result = SaveNewLogTexting(Modelvalue, ContantStrings.AddType);

                return new Response(res.status, res.message);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return new Response(false, ex.Message.ToString());
            }

        }

        /// <summary>
        /// Get Opting Information
        /// </summary>
        /// <param name="Phonenumber"></param>
        /// <returns></returns>
        public smsOptinginfo GetOptingInfo(string Phonenumber)
        {
            try
            {
                using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    string qoptinfo = "select * from CRM.smsOptinginfo where Phonenumber = @Phonenumber";
                    var optinfo = con.Query<smsOptinginfo>(qoptinfo, new { Phonenumber }).FirstOrDefault();
                    return optinfo;
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return null;
            }

        }

        public Response UpdateOptin(string Phonenumber)
        {
            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                string qupdate = "update [CRM].smsOptinginfo set Optin=1,Optout=0,LastUpdatedOn=getutcdate() where Phonenumber=@Phonenumber";
                con.Execute(qupdate, new { Phonenumber });
            }
            return new Response(true, "");
        }

        public Response UpdateOptout(string Phonenumber)
        {
            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                string qupdate = "update [CRM].smsOptinginfo set Optin=0,Optout=1,LastUpdatedOn=getutcdate() where Phonenumber=@Phonenumber";
                con.Execute(qupdate, new { Phonenumber });
            }
            return new Response(true, "");
        }


        public TextingHistory GetTextingDetails(int id)
        {
            var result = TxtHismgr.Get(id);

            return result;
        }

        public string SaveHistroryText(TextingHistory Values)
        {
            try
            {
                if (Values != null)
                {
                    var value = TxtHismgr.Add(Values);
                    return value;
                }
                else
                {
                    return "Un-Success Save";
                }

            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return null;
            }
        }

        

        private string GetSalesperson(ICollection<EventToPerson> attendees)
        {
            string salesperson = "";

            foreach (var personemail in attendees)
            {
                var person = CacheManager.UserCollection.FirstOrDefault(
                    f => string.Equals(f.Email, personemail.PersonEmail
                    , StringComparison.InvariantCultureIgnoreCase)
                    && f.FranchiseId == Franchise.FranchiseId);

                if (person != null)
                {
                    var user = LocalMembership.GetUser(person.PersonId, "Person", "Roles", "OAuthUsers", "UserWidgets");
                    var res = user.IsInRole(AppConfigManager.DefaultSalesRole.RoleId, AppConfigManager.DefaultSalesNoMargineRole.RoleId);
                    if (user.Person != null)
                    {
                        salesperson = user.Person.FullName;
                    }
                    if (res == true) break;
                }
            }

            return salesperson;
        }

        private string GetFromEmailAddress(int? leadId, EmailType type, int? AccountId, int? OpportunityId)
        {
            TerritoryDisplay datas = new TerritoryDisplay();
            if (OpportunityId.HasValue && OpportunityId > 0)
                emailMgr.GetTerritoryDisplayByOpp((int)OpportunityId);
            else if (AccountId.HasValue && AccountId > 0)
                datas = emailMgr.GetTerritoryDisplayByAccountId((int)AccountId);
            else if (leadId.HasValue && leadId > 0)
                datas = emailMgr.GetTerritoryDisplayByLeadId((int)leadId);


            string fromEmailAddress = "";
            if (datas != null && datas.Id == null)
                fromEmailAddress = emailMgr.GetFranchiseEmail(this.Franchise.FranchiseId, type); //franchise.FranchiseId, type);
            else
            {
                if (this.Franchise.AdminEmail != "") //franchise.AdminEmail != "")
                    fromEmailAddress = this.Franchise.AdminEmail; //franchise.AdminEmail;
                else if (this.Franchise.OwnerEmail != "") //franchise.OwnerEmail != "")
                    fromEmailAddress = this.Franchise.OwnerEmail; //franchise.OwnerEmail;
            }

            return fromEmailAddress;
        }

        public string SendTextSms(TextingSMS model, EmailType type)
        {
            try
            {
                FranchiseManager _franMgr = new FranchiseManager(null);
                var franchise = _franMgr.GetFEinfo(model.FranchiseId);

                var salesperson = GetSalesperson(model.Attendees);



                string fromEmailAddress = GetFromEmailAddress(model.LeadId, type, model.AccountId, model.OpportunityId);

                if (model.LeadId > 0)
                {

                    var statusvalue = SendSmsText(model.LeadId, model.AccountId, model.OpportunityId,
                            model.OrderId, model.StartDate, type, fromEmailAddress, salesperson, model.CalendarId, model.IsAllDay);
                    return statusvalue;
                }
                else if (model.AccountId > 0)
                {

                    var statusvalue = SendSmsText(model.LeadId, model.AccountId, model.OpportunityId,
                           model.OrderId, model.StartDate, type, fromEmailAddress, salesperson, model.CalendarId, model.IsAllDay);
                    return statusvalue;
                }


                return "Success";
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
            }
            return "Success";
        }


        public SentEmail GetModeldata(string CellPhone, int? LeadId, int? AccountId, int? OpportunityId,
            int? OrderId, string GetHtmlBody, Response SMS, EmailType type, int brandid, int? Calendarid,int? userid)
        {
            SentEmail Modelvalues = new SentEmail();
            Modelvalues.CellPhone = CellPhone;
            Modelvalues.Body = GetHtmlBody;
            Modelvalues.SentAt = DateTime.UtcNow;
            Modelvalues.FranchiseId = Franchise.FranchiseId;
            if(userid !=null )
            {
                Modelvalues.SentByPersonId = userid;
            } else
            Modelvalues.SentByPersonId = User?.PersonId;
            Modelvalues.LeadId = LeadId;
            Modelvalues.AccountId = AccountId;
            Modelvalues.OrderId = OrderId;
            Modelvalues.OpportunityId = OpportunityId;
            Modelvalues.Status = SMS.status == true ? 1 : 3;
            Modelvalues.Errorinfo = SMS.message;
            Modelvalues.CalendarId = Calendarid;
            Modelvalues.LogType = ContantStrings.LogTypeIdTwo;
            if (type == EmailType.AppointmentConfirmation) //== 2)
            {
                Modelvalues.TextingTemplateTypeId = (int)EmailType.AppointmentConfirmation;
                if (brandid == 1)
                    Modelvalues.Subject = "Budget Blinds - Appointment Confirmation";
                else if (brandid == 2)
                    Modelvalues.Subject = "Tailored Living - Appointment Confirmation";
                else
                    Modelvalues.Subject = "Concrete Craft - Appointment Confirmation";
            }
            else if (type == EmailType.Installation) // Type5
            {
                Modelvalues.TextingTemplateTypeId = (int)EmailType.Installation;
                if (brandid == 1)
                    Modelvalues.Subject = "Budget Blinds - Install Schedule";
                else if (brandid == 2)
                    Modelvalues.Subject = "Tailored Living - Install Schedule";
                else
                    Modelvalues.Subject = "Concrete Craft - Install Schedule";
            }
            else if (type == EmailType.ThankyouReview)//Type6
            {
                Modelvalues.TextingTemplateTypeId = (int)EmailType.ThankyouReview;
                Modelvalues.Subject = "Thank You!";
            }
            else if(type == EmailType.Reminder) //Type 3
            {
                Modelvalues.TextingTemplateTypeId = (int)EmailType.Reminder;
                Modelvalues.Subject = "Reminder! You have an upcoming appointment";
            }
            else //Type 0
            {
                Modelvalues.TextingTemplateTypeId = (int)EmailType.OptinOptout;
                Modelvalues.Subject = "Text Opt In Send";
            }

            return Modelvalues;
        }

        public string SendOrderTextSms(int orderid, EmailType type)
        {
            try
            {
                if (orderid > 0)
                {
                    string fromEmailAddress = "";

                    var orderInfo = emailMgr.GetOrderInfo(orderid);

                    fromEmailAddress = GetFromEmailAddress(null, type, orderInfo.AccountId, orderInfo.OpportunityId);

                    var OrderValue = ordermgr.GetOrder(orderid, Franchise.FranchiseId);

                    var Account = AccountMgr.Get(OrderValue.AccountId);
                    var Phonedata = GetOptingInfo(Account.PrimCustomer.CellPhone);
                    if (Franchise.EnableTexting == true && Phonedata != null && Phonedata.Optin == true && Account.IsNotifyText == true)
                    {
                        var GetSmsTempalate = texttemp.GetOrderTextTemplate(Franchise.BrandId, fromEmailAddress);
                        var SMS = SendSms(Account.PrimCustomer.CellPhone, GetSmsTempalate, Franchise.BrandId, Franchise.CountryCode);
                        if (SMS != null)
                        {
                            var OrderModelvaluesar = GetModeldata(Account.PrimCustomer.CellPhone, null, OrderValue.AccountId, OrderValue.OpportunityId,
                                OrderValue.OrderID, GetSmsTempalate, SMS, type, Franchise.BrandId, null,null);
                            //var StoreHistoryTable = SaveHistroryText(OrderModelvaluesar);
                            var StoreHistoryTable = SaveNewLogTexting(OrderModelvaluesar, ContantStrings.AddType);

                            return StoreHistoryTable;
                        }
                    }
                    else
                        return "Success";


                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
            }
            return "Success";
        }

        public string SendRemainderTextSms()
        {
            try
            {
                _franMgr = new FranchiseManager(null);
                EmailType type = EmailType.Reminder;
                var CalendarList = new EmailManager(null, null).GetEMailReminder();

                foreach (var cal in CalendarList)
                {
                    var franchise = _franMgr.GetFEinfo(cal.FranchiseId);
                    this.Franchise = franchise;
                    emailMgr = new EmailManager(null, franchise);
                    LeadMgr = new LeadsManager(null, franchise);
                    AccountMgr = new AccountsManager(null, franchise);
                    texttemp = new TextTemplateManager(null, franchise);
                    TxtHismgr = new TextingHistoryManager(null, franchise);

                    string fromEmailAddress = "";
                    fromEmailAddress = GetFromEmailAddress(cal.LeadId, type, cal.AccountId, cal.OpportunityId);

                    var salesperson = GetSalesperson(cal.Attendees);

                    if (cal.LeadId > 0)
                    {
                        var Date = (DateTimeOffset)cal.StartDate;
                        Date = Date.DateTime;
                        DateTime ApptDate = TimeZoneManager.ToLocal(Date, Franchise.TimezoneCode);
                        var statusvalue = SendSmsText(cal.LeadId, cal.AccountId, cal.OpportunityId, cal.OrderId,
                            ApptDate, type, fromEmailAddress, salesperson, cal.CalendarId, cal.IsAllDay);
                    }
                    else if (cal.AccountId > 0)
                    {
                        var Date = (DateTimeOffset)cal.StartDate;
                        Date = Date.DateTime;
                        DateTime ApptDate = TimeZoneManager.ToLocal(Date, Franchise.TimezoneCode);
                        var statusvalue = SendSmsText(cal.LeadId, cal.AccountId, cal.OpportunityId, cal.OrderId,
                           ApptDate, type, fromEmailAddress, salesperson, cal.CalendarId, cal.IsAllDay);
                    }

                }

            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
            }
            return "Success";
        }


        public string SendSmsText(int? LeadId, int? AccountId, int? OpportunityId, int? OrderId,
            DateTime StartDate, EmailType type, string fromEmailAddress,
            string salesperson, int? CalendarId, bool IsAllDay)
        {
            try
            {
                if (LeadId > 0)
                {
                    var lead = LeadMgr.Get((int)LeadId);
                    var data = GetOptingInfo(lead.PrimCustomer.CellPhone);
                    if (Franchise.EnableTexting == true && data != null && data.Optin == true && lead.IsNotifyText == true)
                    {

                        var Date = StartDate;
                        var GetHtmlBody = texttemp.GetTextTemplate(Franchise.BrandId, type, salesperson, Date, fromEmailAddress, Franchise.Name, IsAllDay);
                        var SMS = SendSms(lead.PrimCustomer.CellPhone, GetHtmlBody, Franchise.BrandId, Franchise.CountryCode);
                        if (SMS != null)
                        {
                            var Modelvalues = GetModeldata(lead.PrimCustomer.CellPhone, LeadId, AccountId, OpportunityId,
                               OrderId, GetHtmlBody, SMS, type, Franchise.BrandId, CalendarId,null);
                            //var StoreHistoryTable = SaveHistroryText(Modelvalues);
                            var StoreHistoryTable = SaveNewLogTexting(Modelvalues, ContantStrings.AddType);
                            return StoreHistoryTable;
                        }
                    }
                    else return "Success";
                }
                else if (AccountId > 0)
                {                   var Account = AccountMgr.Get((int)AccountId);
                    var data = GetOptingInfo(Account.PrimCustomer.CellPhone);
                    if (Franchise.EnableTexting == true && data != null && data.Optin == true && Account.IsNotifyText == true)
                    {
                        var Date = StartDate;
                        var GetHtmlBody = texttemp.GetTextTemplate(Franchise.BrandId, type, salesperson, Date, fromEmailAddress, Franchise.Name, IsAllDay);
                        var SMS = SendSms(Account.PrimCustomer.CellPhone, GetHtmlBody, Franchise.BrandId, Franchise.CountryCode);
                        if (SMS != null)
                        {

                            var Modelvalues = GetModeldata(Account.PrimCustomer.CellPhone, LeadId, AccountId, OpportunityId,
                                OrderId, GetHtmlBody, SMS, type, Franchise.BrandId, CalendarId,null);
                            //var StoreHistoryTable = SaveHistroryText(Modelvalues);
                            var StoreHistoryTable = SaveNewLogTexting(Modelvalues, ContantStrings.AddType);
                            return StoreHistoryTable;
                        }
                    }
                    else return "Success";
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
            }
            return "Success";
        }

        //only for Lead Import
        public Response SendOptinMessageforLeadImport(string Phonenumber, int BrandId, string Country, int? FranchiseId = null)
        {
            try
            {
                using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    string qoptinfo = "select * from CRM.smsOptinginfo where Phonenumber = @Phonenumber";
                    var optinfo = con.Query<smsOptinginfo>(qoptinfo, new { Phonenumber, BrandId }).FirstOrDefault();
                    if (optinfo == null)
                    {
                        string body = "";

                        if (BrandId == 1)
                            body = "Text YES/CONFIRM to receive Budget Blinds Account related alerts. Msg&data rates may apply. Reply HELP/INFO for help, STOP/ARRET to cancel";
                        else if (BrandId == 2)
                            body = "Text YES/CONFIRM to receive Tailored Living Account related alerts. Msg&data rates may apply. Reply HELP/INFO for help, STOP/ARRET to cancel";
                        else if (BrandId == 3)
                            body = "Text YES/CONFIRM to receive Concrete Craft Account related alerts. Msg&data rates may apply. Reply HELP/INFO for help, STOP/ARRET to cancel";

                        // Send Optin sms
                        var res = SendSms(Phonenumber, body, BrandId, Country);

                        // Save Optin Optout information
                        smsOptinginfo s = new smsOptinginfo();
                        s.Phonenumber = Phonenumber;
                        s.Optin = null;
                        s.Optout = null;
                        s.BrandId = BrandId;
                        s.IsOptinmessagesent = res.status;
                        s.Errorinfo = res.message;
                        s.CreatedOn = DateTime.UtcNow;
                        s.LastUpdatedOn = DateTime.UtcNow;
                        s.Id = (int)con.Insert(s);

                        // Texting History
                        //TextingHistory m = new TextingHistory();
                        //m.CellPhone = Phonenumber;
                        //m.Subject = "Text Opt In Send";
                        //m.Body = body;
                        //m.SentByPersonId = 1107554;
                        //m.SentAt = DateTime.UtcNow;
                        //m.Status = res.status == true ? 1 : 3;
                        //m.FranchiseId = FranchiseId;
                        //m.TemplateTypeId = 0;
                        //TextingHistoryManager TxtHismngr = new TextingHistoryManager(null, null);
                        //TxtHismngr.Add(m);
                        var Modelvalue = GetModeldata(Phonenumber, null,null,null,null, body,res, 0, BrandId,null, ContantStrings.AdminPersonId);
                        var result = SaveNewLogTexting(Modelvalue, ContantStrings.AddType);

                        if (res.status)
                            return new Response(true, "", s);
                        else
                            return new Response(false, res.message, s);
                    }
                    else return new Response(false, "Already Exist", optinfo);
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return null;
            }

        }
    
        public List<Type_LookUpValues> GetCommunicationLogType()
        {
            try
            {
                using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    string logType = "select * from CRM.Type_LookUpValues where TableId = @TableId";
                    var logData = con.Query<Type_LookUpValues>(logType, new { TableId=18 }).ToList();
                    return logData;
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return null;
            }
        }

        

            //save the texting & new log addded from ui.
        public string SaveNewLogTexting(SentEmail data,String AddType)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    if (AddType == "AddLog")
                    {
                        data.SentAt = null;
                    }
                    else
                        data.SentAt = data.SentAt == null?(DateTime?)null: DateTime.UtcNow;
                    connection.Open();
                    string Query = "";
                    Query = "INSERT INTO History.SentEmails ";
                    Query = Query + "(LeadId,CellPhone,Subject,Body,SentAt";
                    Query = Query + ",FranchiseId,SentByPersonId,AccountId,OpportunityId,OrderId,CalendarId,LogType";
                    Query = Query + ",Status,Errorinfo,TextingTemplateTypeId,SentDate,UserName,Description)";
                    Query = Query + "VALUES (@LeadId,@CellPhone,@Subject,@Body,@SentAt,";
                    Query = Query + "@FranchiseId,@SentByPersonId,@AccountId,@OpportunityId,@OrderId,@CalendarId,@LogType";
                    Query = Query + ",@Status,@Errorinfo,@TextingTemplateTypeId,@SentDate,@UserName,@Description)";
                    var SentEmail = connection.Query<string>(Query,
                    new
                    {

                        LeadId = data.LeadId,
                        CellPhone=data.CellPhone,
                        Subject=data.Subject,
                        Body = data.Body,
                        SentAt = data.SentAt,
                        FranchiseId = Franchise == null ? (int?)null : this.Franchise.FranchiseId,
                        SentByPersonId = User != null && User.PersonId > 0 ? User.PersonId : (int?)null,
                        AccountId = data.AccountId,
                        OpportunityId = data.OpportunityId,
                        OrderId = data.OrderId,
                        CalendarId = data.CalendarId,
                        LogType = data.LogType,
                        Status = data.Status,
                        Errorinfo=data.Errorinfo,
                        TextingTemplateTypeId = data.TextingTemplateTypeId,
                        SentDate = TimeZoneManager.ToUTC(data.SentDate),
                        UserName = data.UserName,
                        Description = data.Description

                    });
                    connection.Close();
                }
                return "Success";
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return null;
            }

        }

        
    }
}
