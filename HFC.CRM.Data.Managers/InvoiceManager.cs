﻿using HFC.CRM.Core.Membership;
using HFC.CRM.Data;
using HFC.CRM.Core.Common;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using HFC.CRM.Data.Context;
using System.Data.Entity.SqlServer;
using System.Data.Linq.SqlClient;
using System.Text.RegularExpressions;

using HFC.CRM.Core;
using System.Data.SqlClient;
using Dapper;
using HFC.CRM.Core.Logs;

namespace HFC.CRM.Managers
{
    public class InvoiceManager : DataManager<Invoice>
    {
        
        public InvoiceManager(User user, Franchise franchise) : this(user, user, franchise) { }

        public InvoiceManager(User user, User authorizingUser, Franchise franchise)
        {
            this.User = user;
            this.AuthorizingUser = authorizingUser;
            this.Franchise = franchise;
        }

        public List<Invoice> Get(
            out int totalRecords,
            InvoiceSearchFilterEnum searchFilter,
            string searchTerm,
            InvoiceStatusEnum[] statusFilter = null,
            DateTimeOffset? createdOnStart = null,
            DateTimeOffset? createdOnEnd = null,
            int pageNum = 1,
            int pageSize = 25,
            string orderBy = null,
            OrderByEnum orderDirection = OrderByEnum.Desc)
        {
            totalRecords = 0;
            //if (!BasePermission.CanRead)
            //    throw new UnauthorizedAccessException(Unauthorized);
            if (pageNum <= 0)
                throw new ArgumentOutOfRangeException("Page number must be greater than 0");
            if (pageSize <= 0)
                throw new ArgumentOutOfRangeException("Page size must be greater than 0");

            var linq = CRMDBContext.Invoices.AsNoTracking().Include(i => i.Job)
                .Where(w => w.IsDeleted == false
                    && w.Job.Lead.FranchiseId == this.Franchise.FranchiseId
                );

            if (statusFilter != null && statusFilter.Count() > 0)
                linq = linq.Where(w => statusFilter.Contains(w.StatusEnum));
            if (createdOnStart.HasValue)
                linq = linq.Where(w => w.CreatedOn >= createdOnStart.Value);
            if (createdOnEnd.HasValue)
                linq = linq.Where(w => w.CreatedOn <= createdOnEnd.Value);

            if (!string.IsNullOrEmpty(searchTerm))
            {
                InvoiceSearchFilterEnum realSearchFilter = searchFilter;
                int anumber = 0;
                if (searchFilter == InvoiceSearchFilterEnum.Auto_Detect)
                {
                    var isNumber = int.TryParse(searchTerm, out anumber);

                    linq = from l in linq
                           let job = l.Job
                           let p1 = l.Job.Customer
                           where
                           p1.PrimaryEmail.Contains(searchTerm) ||
                           p1.HomePhone.Contains(searchTerm) ||
                               p1.CellPhone.Contains(searchTerm) ||
                               p1.WorkPhone.Contains(searchTerm) ||
                               p1.FaxPhone.Contains(searchTerm) ||
                               p1.FirstName.StartsWith(searchTerm) ||
                               p1.LastName.StartsWith(searchTerm) ||
                               (p1.FirstName + " " + p1.LastName).Contains(searchTerm) ||
                                l.InvoiceNumber == searchTerm ||
                               (isNumber ? job.JobNumber == anumber : false)
                           select l;
                }

                if (realSearchFilter == InvoiceSearchFilterEnum.Number)
                {
                    if (int.TryParse(searchTerm, out anumber))
                    {
                        linq = linq.Where(o => o.Job.JobNumber == anumber);
                    }
                }
                else if (realSearchFilter == InvoiceSearchFilterEnum.Invoice_Number)
                {
                    if (int.TryParse(searchTerm, out anumber))
                    {
                        linq = linq.Where(o => o.InvoiceNumber == searchTerm);
                    }
                }

                else if (realSearchFilter == InvoiceSearchFilterEnum.Email)
                {
                    linq = linq.Where(o => o.Job.Customer.PrimaryEmail.Contains(searchTerm));
                }
                else if (realSearchFilter == InvoiceSearchFilterEnum.Phone_Number)
                {
                    string parsedPhone = RegexUtil.GetNumbersOnly(searchTerm);
                    linq =
                        linq.Where(
                            w => w.Job.Customer.HomePhone == parsedPhone || w.Job.Customer.CellPhone == parsedPhone || w.Job.Customer.WorkPhone == parsedPhone || w.Job.Customer.FaxPhone == parsedPhone);
                }
                else if (realSearchFilter == InvoiceSearchFilterEnum.Customer_Name)
                {
                    linq = from l in linq
                           let p1 = l.Job.Customer
                           where p1.FirstName.StartsWith(searchTerm) || p1.LastName.StartsWith(searchTerm) || (p1.FirstName + " " + p1.LastName).Contains(searchTerm)
                           select l;
                }
            }

            var result = new List<Invoice>();
            totalRecords = linq.Count();
            if (totalRecords > 0)
            {
                if (!string.IsNullOrEmpty(orderBy)) orderBy = orderBy.ToLower();

                switch (orderBy)
                {
                    case "jobnumber":
                        linq = orderDirection == OrderByEnum.Asc ? linq.OrderBy(i => i.Job.JobNumber) : linq.OrderByDescending(i => i.Job.JobNumber);
                        break;
                    case "balance":
                        linq = orderDirection == OrderByEnum.Asc
                            ? linq.OrderBy(i => i.JobQuote.NetTotal - i.Payments.Sum(s => s.Amount))
                            : linq.OrderByDescending(i => i.JobQuote.NetTotal - i.Payments.Sum(s => s.Amount));
                        break;
                    case "total":
                        linq = orderDirection == OrderByEnum.Asc ? linq.OrderBy(i => i.JobQuote.NetTotal) : linq.OrderByDescending(i => i.JobQuote.NetTotal);
                        break;
                    case "contact":
                        linq = orderDirection == OrderByEnum.Asc ? linq.OrderBy(i => i.Job.Customer.PrimaryEmail) : linq.OrderByDescending(i => i.Job.Customer.PrimaryEmail);
                        break;
                    case "customer":
                        linq = orderDirection == OrderByEnum.Asc
                            ? linq.OrderBy(i => i.Job.Customer.FirstName).ThenBy(i => i.Job.Customer.LastName)
                            : linq.OrderByDescending(i => i.Job.Customer.FirstName).ThenByDescending(i => i.Job.Customer.LastName);
                        break;
                    case "status":
                        linq = orderDirection == OrderByEnum.Asc ? linq.OrderBy(i => i.StatusEnum) : linq.OrderByDescending(i => i.StatusEnum);
                        break;
                    default:
                        linq = orderDirection == OrderByEnum.Asc ? linq.OrderBy(i => i.CreatedOn) : linq.OrderByDescending(i => i.CreatedOn);
                        break;
                }

                var ids = linq.Skip((pageNum - 1) * pageSize).Take(pageSize).Select(s => s.InvoiceId).ToList();
                var invoices = Get(ids);
                if (invoices != null && invoices.Count > 0)
                {
                    switch (orderBy)
                    {
                        case "invoicenumber":
                            result = orderDirection == OrderByEnum.Asc
                                ? invoices.ToList().OrderBy(c => c.InvoiceNumber, new SemiNumericComparer()).ToList()
                                : invoices.ToList().OrderByDescending(c => c.InvoiceNumber, new SemiNumericComparer()).ToList();
                            break;
                        case "jobnumber":
                            result = orderDirection == OrderByEnum.Asc ? invoices.OrderBy(i => i.Job.JobNumber).ToList() : invoices.OrderByDescending(i => i.Job.JobNumber).ToList();
                            break;
                        case "balance":
                            result = orderDirection == OrderByEnum.Asc ? invoices.OrderBy(i => i.JobQuote.NetTotal - i.Payments.Sum(s => s.Amount)).ToList() : invoices.OrderByDescending(i => i.JobQuote.NetTotal - i.Payments.Sum(s => s.Amount)).ToList();
                            break;
                        case "total":
                            result = orderDirection == OrderByEnum.Asc ? invoices.OrderBy(i => i.JobQuote.NetTotal).ToList() : invoices.OrderByDescending(i => i.JobQuote.NetTotal).ToList();
                            break;
                        case "contact":
                            result = orderDirection == OrderByEnum.Asc ? invoices.OrderBy(i => i.Job.Customer.PrimaryEmail).ToList() : invoices.OrderByDescending(i => i.Job.Customer.PrimaryEmail).ToList();
                            break;
                        case "customer":
                            result = orderDirection == OrderByEnum.Asc ? invoices.OrderBy(i => i.Job.Customer.FirstName).ThenBy(i => i.Job.Customer.LastName).ToList() : invoices.OrderByDescending(i => i.Job.Customer.FirstName).ThenByDescending(i => i.Job.Customer.LastName).ToList();
                            break;
                        case "status":
                            result = orderDirection == OrderByEnum.Asc ? invoices.OrderBy(i => i.StatusEnum).ToList() : invoices.OrderByDescending(i => i.StatusEnum).ToList();
                            break;
                        default:
                            result = orderDirection == OrderByEnum.Asc ? invoices.OrderBy(i => i.CreatedOn).ToList() : invoices.OrderByDescending(i => i.CreatedOn).ToList();
                            break;
                    }
                }
            }

            return result;
        }

        public override ICollection<Invoice> Get(List<int> idList)
        {
            //if (!BasePermission.CanRead)
            //    throw new UnauthorizedAccessException(Unauthorized);
            if (idList == null)
                throw new ArgumentNullException("idList parameter can not be null");
            if (idList.Count == 0)
                return new List<Invoice>();

            return CRMDBContext.Invoices.AsNoTracking()
                            .Include(i => i.Job)
                            .Include(i => i.Job.Customer)
                            .Include(i => i.Payments)
                            .Include(i => i.JobQuote)
                            .Where(w => idList.Contains(w.InvoiceId)).ToList();
        }

        public Invoice BillQuote(int quoteId, int jobId)
        {
            /// This is to be used to apply payment to a job from the job qoute directly, without creating Dynamic_Invoice explisitly. 
            /// Whereas creating the Dynamic_Invoice will let user to select qoute line items.

            using (var db = ContextFactory.Create())
            {
                var invoice = db.Invoices.Where(x => x.IsDeleted == false && x.JobId == jobId).ToList();
                if (!invoice.Any())
                {
                    var newInvoiceNumber = CRMDBContext.spInvoiceNumber_New(Franchise.FranchiseId).FirstOrDefault();

                    var newInvoice = new Invoice()
                    {
                        InvoiceGuid = Guid.NewGuid(),
                        InvoiceNumber = newInvoiceNumber.ToString(),
                        JobId = jobId,
                        QuoteId = quoteId,
                        CreatorPersonId = (int?)SessionManager.CurrentUser.PersonId,
                        LastUpdatedPersonId = (int?)SessionManager.CurrentUser.PersonId,
                        CreatedOn = DateTime.Now,
                        LastUpdated = DateTime.Now,
                        StatusEnum = InvoiceStatusEnum.Open
                    };
                    db.Invoices.Add(newInvoice);
                    db.SaveChanges();

                    return newInvoice;
                }

                return invoice.FirstOrDefault();
            }
            //    return "Can't generate Dynamic_Invoice for this Job, it has been billed before";
        }

        public override Invoice Get(int id)
        {
            //if (!BasePermission.CanRead)
            //    throw new UnauthorizedAccessException(Unauthorized);
            if (id <= 0)
                return null;

            var invoice = CRMDBContext.Invoices
                            .Include(i => i.JobQuote)
                            .Where(x => x.IsDeleted == false)
                            .Include(i => i.JobQuote.JobItems)
                            .Include(i => i.JobQuote.JobItems.Select(s => s.Options))
                            .Include(i => i.JobQuote.SaleAdjustments)
                            .Include(i => i.Job.Lead)
                            .Include(i => i.Job.Lead.PrimCustomer)
                            .SingleOrDefault(w => w.InvoiceId == id);

            if (invoice != null)
            {
                //var JobMgr = new JobsManager(AuthorizingUser, User, Franchise);

                ////invoice.BasePermission = BasePermission;
                //invoice.Job = JobMgr.Get(invoice.JobId);
                //invoice.JobQuote.JobItems = invoice.JobQuote.JobItems.OrderBy(o => o.SortOrder).ToList();
            }

            return invoice;
        }
        public List<InvoicesTP> GetInvoicesByFranchiseId(int franchiseId, QBApps app)
        {
            try
            {
                DateTime? startDate = app.CreatedOn == null || app.CreatedOn < DateTime.Now.AddDays(-160) ? DateTime.Now.AddDays(-160) : app.CreatedOn;

                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    // get invoice data
                    string invoiceQuery = @"select iv.* from [CRM].[Invoices] iv
                                            Inner join [CRM].[Orders] ord on iv.OrderID=ord.OrderID
                                            Inner join [CRM].[Opportunities] op on ord.OpportunityId=op.OpportunityId
                                            where ord.OrderStatus NOT IN (6,9) AND FranchiseId=@franchiseId and ord.ContractedDate>=@startDate";

                    var invoicelstdata = connection.Query<InvoicesTP>(invoiceQuery, new { franchiseId, startDate }).ToList();

                    foreach (var invoicedata in invoicelstdata)
                    {
                        // Get Order data
                        string orderQ = @"select * from [CRM].[Orders] where OrderID=@OrderID ";
                        invoicedata.Orders = connection.Query<Orders>(orderQ, new { OrderID = invoicedata.OrderID }).FirstOrDefault();

                        // Get Payment data
                        string paymentQ = @"select * from [CRM].[Payments] where OrderID=@OrderID and Reversal=0";
                        invoicedata.Payments = connection.Query<Payments>(paymentQ, new { OrderID = invoicedata.OrderID }).ToList();

                        if (invoicedata.Orders != null)
                        {

                            // Get Order lines data
                            string orderlinesQ = @"select * from [CRM].[OrderLines] where orderID=@orderID";
                            invoicedata.Orders.OrderLines = connection.Query<OrderLines>(orderlinesQ, new { orderID = invoicedata.Orders.OrderID }).ToList();

                            // Get Quote data
                            string quoteQ = @"select * from [CRM].[Quote] where QuoteKey=@QuoteKey";
                            invoicedata.Quote = connection.Query<Quote>(quoteQ, new { QuoteKey = invoicedata.Orders.QuoteKey }).FirstOrDefault();

                            if (invoicedata.Quote != null)
                            {
                                // Get Quote Line data
                                string quotelineQ = @"select ql.* from CRM.quotelines ql inner join CRM.quotelinedetail  qd on ql.quotelineid = qd.quotelineid where ql.quotekey=@QuoteKey";
                                invoicedata.Quote.QuoteLines = connection.Query<QuoteLines>(quotelineQ, new { QuoteKey = invoicedata.Quote.QuoteKey }).ToList();

                                foreach (var QuoteLine in invoicedata.Quote.QuoteLines)
                                {
                                    // Get QuoteLine detail
                                    string quoteLineDetailQ = "select * from [CRM].QuoteLineDetail where QuoteLineId=@QuoteLineId order by 1 desc";
                                    QuoteLine.QuoteLineDetail = connection.Query<QuoteLineDetail>(quoteLineDetailQ, new { QuoteLineId = QuoteLine.QuoteLineId }).FirstOrDefault();

                                    // Get Service info
                                    if (QuoteLine.ProductTypeId == 3)
                                    {
                                        string serviceinfo = "SELECT * FROM crm.FranchiseProducts fp WHERE fp.ProductKey = @ProductKey";
                                        QuoteLine.FranchiseProducts = connection.Query<FranchiseProducts>(serviceinfo, new { ProductKey = QuoteLine.ProductId }).FirstOrDefault();
                                    }

                                    // Get sales tax amount data
                                    string taxQ = @"select * from [CRM].[Tax] where QuoteLineId=@QuoteLineId order by 1 desc";
                                    QuoteLine.SalesTax = connection.Query<Tax>(taxQ, new { QuoteLineId = QuoteLine.QuoteLineId }).FirstOrDefault();

                                    if (QuoteLine.SalesTax != null)
                                    {
                                        // Get sales tax amount detailed data
                                        string taxdetailQ = @"select * from [CRM].[TaxDetails] where TaxId=@TaxId";
                                        QuoteLine.SalesTax.TaxDetails = connection.Query<TaxDetails>(taxdetailQ, new { TaxId = QuoteLine.SalesTax.TaxId }).ToList();

                                    }
                                }

                            }

                            // Get Opportunity data
                            string opportunityQ = @"select * from [CRM].[Opportunities] where OpportunityId=@OpportunityId ";
                            invoicedata.Opportunity = connection.Query<Opportunity>(opportunityQ, new { OpportunityId = invoicedata.Orders.OpportunityId }).FirstOrDefault();

                            if (invoicedata.Opportunity != null)
                            {
                                // Get Opportunity Account data
                                string accountQ = @"select * from [CRM].[Accounts] where AccountId=@AccountId ";
                                invoicedata.Opportunity.AccountTP = connection.Query<AccountTP>(accountQ, new { AccountId = invoicedata.Opportunity.AccountId }).FirstOrDefault();

                                // Get Opportunity Primary customer data
                                string accCustomerQ = @"select * from [CRM].[Customer] c
                                                    join [CRM].[AccountCustomers] ac on c.CustomerId=ac.CustomerId
                                                    where ac.IsPrimaryCustomer=1 and ac.AccountId=@AccountId";
                                invoicedata.Opportunity.PrimCustomer = connection.Query<CustomerTP>(accCustomerQ, new { AccountId = invoicedata.Opportunity.AccountId }).FirstOrDefault();

                                // Get Opportunity Primary customer address data
                                string accAddressQ = @"select * from [CRM].[Addresses] where AddressId=@AddressId";
                                invoicedata.Opportunity.AccountBillingAddressTP = connection.Query<AddressTP>(accAddressQ, new { AddressId = invoicedata.Opportunity.BillingAddressId }).FirstOrDefault();

                                // Get Opportunity account installation address data
                                string accInstAddressQ = @"select * from [CRM].[Addresses] where AddressId=@AddressId";
                                invoicedata.Opportunity.InstallationAddressTP = connection.Query<AddressTP>(accInstAddressQ, new { AddressId = invoicedata.Opportunity.InstallationAddressId }).FirstOrDefault();

                            }
                        }
                    }
                    return invoicelstdata;
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }

            //IEnumerable<Invoice> invoices = CRMDBContext.Invoices
            //                .Where(x => x.IsDeleted == false &&
            //                        x.Job.ContractedOnUtc >= startDate &&
            //                        x.Job.Lead.FranchiseId == franchiseId)
            //                .Include(i => i.JobQuote)
            //                .Include(i => i.JobQuote.JobItems)
            //                .Include(i => i.JobQuote.JobItems.Select(s => s.Options))
            //                .Include(i => i.JobQuote.SaleAdjustments)
            //                .Include(i => i.Job)
            //                .Include(i => i.Job.Lead)
            //                .Include(i => i.Job.Lead.Addresses)
            //                .Include(i => i.Job.Lead.PrimCustomer)
            //                .Include(i => i.Payments);

            //return invoices.ToList();
        }

        public override string Add(Invoice data)
        {
            throw new NotImplementedException();
        }

        public override string Update(Invoice data)
        {
            throw new NotImplementedException();
        }

        public override string Delete(int id)
        {
            // throw new NotImplementedException();
            if (id <= 0)
                return "Invalid invoice id";

            var paymentCheck = (from l in CRMDBContext.Payments
                                where l.InvoiceId == id
                                select new { l.PaymentId }).SingleOrDefault();
            if (paymentCheck != null)
                return PaymentExist;

            //Base permission of can delete (like admin and managers) or assigned permission AND assigned person matches current user
            var invoice = new Invoice { InvoiceId = id, IsDeleted = false };
            CRMDBContext.Invoices.Attach(invoice);

            invoice.IsDeleted = true;
            invoice.LastUpdated = DateTime.Now;

            CRMDBContext.SaveChanges();

            return Success;

            //if (BasePermission.CanDelete)
            //{

            //}
            //else
            //    return Unauthorized;
        }

    }
}
