﻿using Dapper;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
//using HFC.CRM.DTO.Permission;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HFC.CRM.Core;
using HFC.CRM.Core.Common;

namespace HFC.CRM.Managers
{
    public class PermissionManager : DataManager<Permission>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Permission"/> class.
        /// </summary>
        /// <param name="franchise">The franchise.</param>
        /// <param name="user"></param>
        /// 

        public PermissionManager(User user, Franchise franchise)
        {
            User = user;
            Franchise = franchise;
        }
        public List<PermissionSetTP> GetPermissionSetList()
        {

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = "";
                if (Franchise != null)
                {
                    query = @"SELECT Id
                            ,PermissionSetName
                            ,Description
                            ,FranchiseId
                            ,CONVERT(date, CreatedOn) CreatedOn
                            ,CreatedBy
                            ,CONVERT(date, LastUpdatedOn) LastUpdatedOn
                            ,LastUpdatedBy
                            ,IsActive
                            FROM Auth.PermissionSet where FranchiseId =@franchiseId or FranchiseId is null ";
                    return ExecuteIEnumerableObject<PermissionSetTP>(ContextFactory.CrmConnectionString, query, new { franchiseId = Franchise.FranchiseId }).ToList();
                }
                else
                {
                    query = @"SELECT Id
                            ,PermissionSetName
                            ,Description
                            ,FranchiseId
                            ,CONVERT(date, CreatedOn) CreatedOn
                            ,CreatedBy
                            ,CONVERT(date, LastUpdatedOn) LastUpdatedOn
                            ,LastUpdatedBy
                            ,IsActive
                            FROM Auth.PermissionSet where FranchiseId is null";
                    return ExecuteIEnumerableObject<PermissionSetTP>(ContextFactory.CrmConnectionString, query).ToList();
                }

            }
        }
        #region Permission set
        public Permissionset_VM GetPermissionSet(int id, int RoleType)
        {
            Permissionset_VM PSVM = new Permissionset_VM();
            List<PermissionTPT> lstPermTPT = new List<PermissionTPT>();
            var permSet = new List<Permission_PS>();
            var query = "";


            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                if (id > 0)
                {
                    query = queryPermissionSetsById;
                    permSet = ExecuteIEnumerableObject<Permission_PS>(ContextFactory.CrmConnectionString, query, new { PermissionSetId = id, RoleType }).ToList();
                    query = @"Select * from Auth.PermissionSet where Id = @permissionSetId";
                    var permissionSet = ExecuteIEnumerableObject<PermissionSetTP>(ContextFactory.CrmConnectionString, query, new { permissionSetId = id }).FirstOrDefault();
                    PSVM.PermissionSetId = permissionSet.Id;
                    PSVM.PermissionSetName = permissionSet.PermissionSetName;
                    PSVM.FranchiseId = permissionSet.FranchiseId;
                    PSVM.Description = permissionSet.Description;
                    PSVM.CreatedOnUtc = permissionSet.CreatedOn;
                    PSVM.CreatedOnUtc = TimeZoneManager.ToLocal(PSVM.CreatedOnUtc);
                    PSVM.LastUpdatedOnUtc = permissionSet.LastUpdatedOn;
                    PSVM.LastUpdatedOnUtc = TimeZoneManager.ToLocal(PSVM.LastUpdatedOnUtc);
                    PSVM.IsActive = permissionSet.IsActive;
                }
                else
                {
                    query = queryEmptyPermissionSets;
                    permSet = ExecuteIEnumerableObject<Permission_PS>(ContextFactory.CrmConnectionString, query, new { RoleType }).ToList();
                }

                var splitdata = permSet.GroupBy(x => x.ModuleId, (key, elements) =>
                 new
                 {
                     ModuleId = key,
                     Items = elements.ToList()
                 }).ToList();
                foreach (var set in splitdata)
                {
                    var baseper = set.Items.Where(x => x.IsSpecialPermission == false).ToList();
                    if (baseper.Count > 0)
                    {
                        if (set.Items[0].ModuleGroupId == null)
                        {
                            PermissionTPT pb = new PermissionTPT();
                            pb.id = set.Items[0].ModuleId;
                            pb.ModuleId = set.Items[0].ModuleId;
                            pb.PermissionTypeId = null;
                            pb.Name = set.Items[0].ModuleName;
                            pb.Module = set.Items[0].ModuleName;
                            pb.ModuleGroupId = null;
                            pb.parentId = null;
                            pb.IsSpecialPermission = false;
                            pb.Create = set.Items.Where(x => x.PermissionTypeId == 1).Select(x => x.IsAccessible).FirstOrDefault();
                            pb.Read = set.Items.Where(x => x.PermissionTypeId == 2).Select(x => x.IsAccessible).FirstOrDefault();
                            pb.Update = set.Items.Where(x => x.PermissionTypeId == 3).Select(x => x.IsAccessible).FirstOrDefault();
                            pb.Delete = set.Items.Where(x => x.PermissionTypeId == 4).Select(x => x.IsAccessible).FirstOrDefault();
                            lstPermTPT.Add(pb);
                        }
                        else
                        {
                            var checkGroupExist = lstPermTPT.Select(x => x.ModuleGroupId).Contains(set.Items[0].ModuleGroupId);
                            var parentGroupForModule = lstPermTPT.Where(pt => pt.ParentGroupId == set.Items[0].ModuleGroupId).FirstOrDefault();
                            if (parentGroupForModule == null && !checkGroupExist)
                            {
                                var parentGroup = GetCheckedParentvalue(lstPermTPT, set.Items[0]);

                                PermissionTPT pbg = new PermissionTPT();
                                pbg.id = (int)set.Items[0].ModuleGroupId;
                                pbg.ModuleId = null;
                                pbg.PermissionTypeId = null;
                                pbg.Name = set.Items[0].GroupName;
                                pbg.Module = set.Items[0].ModuleName;
                                pbg.ModuleGroupId = set.Items[0].ModuleGroupId;
                                if (parentGroup != null)
                                {
                                    pbg.parentId = parentGroup.id;
                                }
                                //pbg.parentId = null;
                                pbg.IsSpecialPermission = false;
                                lstPermTPT.Add(pbg);
                            }

                            PermissionTPT pb = new PermissionTPT();
                            pb.id = set.Items[0].ModuleId;
                            pb.ModuleId = set.Items[0].ModuleId;
                            pb.PermissionTypeId = null;
                            pb.Name = set.Items[0].ModuleName;
                            pb.Module = set.Items[0].ModuleName;
                            pb.ModuleGroupId = null;
                            if (parentGroupForModule != null)
                            {
                                pb.parentId = parentGroupForModule.id;
                            }
                            else
                            {
                                pb.parentId = (int)set.Items[0].ModuleGroupId;
                            }

                            pb.IsSpecialPermission = false;
                            pb.Create = set.Items.Where(x => x.PermissionTypeId == 1).Select(x => x.IsAccessible).FirstOrDefault();
                            pb.Read = set.Items.Where(x => x.PermissionTypeId == 2).Select(x => x.IsAccessible).FirstOrDefault();
                            pb.Update = set.Items.Where(x => x.PermissionTypeId == 3).Select(x => x.IsAccessible).FirstOrDefault();
                            pb.Delete = set.Items.Where(x => x.PermissionTypeId == 4).Select(x => x.IsAccessible).FirstOrDefault();
                            lstPermTPT.Add(pb);
                        }
                    }

                    var specialper = set.Items.Where(x => x.IsSpecialPermission == true).ToList();
                    if (specialper.Count > 0)
                    {
                        foreach (var sp in specialper)
                        {
                            PermissionTPT ps = new PermissionTPT();
                            ps.id = sp.ModuleId + 1000;
                            ps.ModuleId = null;
                            ps.PermissionTypeId = sp.PermissionTypeId;
                            ps.Name = sp.PermissionName;
                            ps.Module = sp.ModuleName;
                            ps.ModuleGroupId = null;
                            ps.parentId = sp.ModuleId;
                            ps.IsSpecialPermission = true;
                            ps.SpecialPermission = sp.IsAccessible;
                            lstPermTPT.Add(ps);
                        }

                    }

                }
                connection.Close();
                PSVM.PermissionSets = lstPermTPT;
                return PSVM;
            }

        }

        public int UpdatePermissionSet(Permissionset_VM permissionSets)
        {
            try
            {
                int PermissionSetId = permissionSets.PermissionSetId;
                string qInsertps = @"insert into [Auth].[Permission_PS] values(@PermissionTypeId,@TypeModuleId,@PermissionSetId,@IsAccessible)";
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    if (PermissionSetId > 0)
                    {
                        var PermissionSet = connection.Get<PermissionSetTP>(PermissionSetId);
                        PermissionSet.PermissionSetName = permissionSets.PermissionSetName;
                        PermissionSet.Description = permissionSets.Description;
                        if (Franchise != null)
                            PermissionSet.FranchiseId = Franchise.FranchiseId;
                        else
                            PermissionSet.FranchiseId = null;
                        PermissionSet.LastUpdatedBy = User.PersonId;
                        PermissionSet.LastUpdatedOn = DateTime.UtcNow;
                        PermissionSet.IsActive = true;
                        connection.Update(PermissionSet);

                        connection.Execute("delete from [Auth].[Permission_PS] where PermissionSetId=@PermissionSetId", new { PermissionSetId });
                    }
                    else
                    {
                        PermissionSetTP ps = new PermissionSetTP();
                        ps.PermissionSetName = permissionSets.PermissionSetName;
                        ps.Description = permissionSets.Description;
                        if (Franchise != null)
                            ps.FranchiseId = Franchise.FranchiseId;
                        else
                            ps.FranchiseId = null;
                        ps.CreatedBy = User.PersonId;
                        ps.CreatedOn = DateTime.UtcNow;
                        ps.IsActive = true;
                        PermissionSetId = (int)connection.Insert(ps);
                    }

                    var baseset = permissionSets.PermissionSets.Where(x => x.ModuleId != null).ToList();
                    var specialset = permissionSets.PermissionSets.Where(x => x.PermissionTypeId != null).ToList();

                    foreach (var bs in baseset)
                    {
                        if (bs.Create == true)
                        {
                            connection.Execute(qInsertps, new { PermissionTypeId = 1, TypeModuleId = bs.ModuleId, PermissionSetId = PermissionSetId, IsAccessible = true });
                        }
                        if (bs.Read == true)
                        {
                            connection.Execute(qInsertps, new { PermissionTypeId = 2, TypeModuleId = bs.ModuleId, PermissionSetId = PermissionSetId, IsAccessible = true });
                        }
                        if (bs.Update == true)
                        {
                            connection.Execute(qInsertps, new { PermissionTypeId = 3, TypeModuleId = bs.ModuleId, PermissionSetId = PermissionSetId, IsAccessible = true });
                        }
                        if (bs.Delete == true)
                        {
                            connection.Execute(qInsertps, new { PermissionTypeId = 4, TypeModuleId = bs.ModuleId, PermissionSetId = PermissionSetId, IsAccessible = true });
                        }
                    }
                    foreach (var ss in specialset)
                    {
                        if (ss.SpecialPermission == true)
                        {
                            connection.Execute(qInsertps, new { PermissionTypeId = ss.PermissionTypeId, TypeModuleId = (int?)null, PermissionSetId = PermissionSetId, IsAccessible = true });
                        }
                    }


                }
                return PermissionSetId;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public bool UpdatePermissionSetRole(Permissionset_VM permissionSets)
        {
            try
            {

                string qInsertps = @"insert into [Auth].[Permission_Role](PermissionTypeId,RoleId,TypeModuleId,IsAccessible) values(@PermissionTypeId,@RoleId,@TypeModuleId,@IsAccessible)";
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Execute("delete from [Auth].[Permission_Role] where RoleId=@RoleId", new { RoleId = permissionSets.RoleId });
                    var baseset = permissionSets.PermissionSets.Where(x => x.ModuleId != null).ToList();
                    var specialset = permissionSets.PermissionSets.Where(x => x.PermissionTypeId != null).ToList();

                    foreach (var bs in baseset)
                    {
                        if (bs.Create == true)
                        {
                            connection.Execute(qInsertps, new { PermissionTypeId = 1, TypeModuleId = bs.ModuleId, RoleId = permissionSets.RoleId, IsAccessible = true });
                        }
                        if (bs.Read == true)
                        {
                            connection.Execute(qInsertps, new { PermissionTypeId = 2, TypeModuleId = bs.ModuleId, RoleId = permissionSets.RoleId, IsAccessible = true });
                        }
                        if (bs.Update == true)
                        {
                            connection.Execute(qInsertps, new { PermissionTypeId = 3, TypeModuleId = bs.ModuleId, RoleId = permissionSets.RoleId, IsAccessible = true });
                        }
                        if (bs.Delete == true)
                        {
                            connection.Execute(qInsertps, new { PermissionTypeId = 4, TypeModuleId = bs.ModuleId, RoleId = permissionSets.RoleId, IsAccessible = true });
                        }
                    }
                    foreach (var ss in specialset)
                    {
                        if (ss.SpecialPermission == true)
                        {
                            connection.Execute(qInsertps, new { PermissionTypeId = ss.PermissionTypeId, TypeModuleId = (int?)null, RoleId = permissionSets.RoleId, IsAccessible = true });
                        }
                    }


                }
                return true;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        #endregion
        public List<UserRolesPermissionTPT> GetUserRolesPermissions()
        {
            try
            {
                List<UserRolesPermissionTPT> lstRolePermTPT = new List<UserRolesPermissionTPT>();
                var query = "";
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    if (CookieManager.ImpersonateUserId.HasValue && CookieManager.ImpersonateUserId != Guid.Empty)
                    {
                        foreach (var objj in SessionManager.SecurityUser.Roles)
                        {
                            if(objj.RoleId.ToString().ToUpper() == "D6BAAB7B-D94F-41D3-88E9-601FD1C27398")
                            {
                                query = qUserRolesPermissions; break;
                            }
                            var readView = connection.Query<dynamic>(@"SELECT PR.*  FROM [Auth].[Permission_Role] PR
                                                        inner join [Auth].[Type_PermissionTP] TP on PR.PermissionTypeId = TP.Id 
                                                        where PermissionCode like 'Read Only View' and PR.RoleId=@RoleId", new { RoleId = objj.RoleId.ToString() }).ToList();
                           if (readView.Count > 0)
                            { query = readUserRolesPermissions; }
                        }
                        if (query == "") query = qUserRolesPermissions;
                    }
                    else query = qUserRolesPermissions;



                    var permissions = ExecuteIEnumerableObject<Permission_PS>(ContextFactory.CrmConnectionString, query, new { UserId = User.UserId, RoleType = RoleType() }).ToList();

                    var splitdata = permissions.GroupBy(x => x.ModuleId, (key, elements) =>
                     new
                     {
                         ModuleId = key,
                         Items = elements.ToList()
                     }).ToList();

                    foreach (var set in splitdata)
                    {
                        var baseper = set.Items.Where(x => x.IsSpecialPermission == false).ToList();
                        var specialper = set.Items.Where(x => x.IsSpecialPermission == true).ToList();
                        if (baseper.Count > 0)
                        {
                            UserRolesPermissionTPT rp = new UserRolesPermissionTPT();
                            rp.ModuleId = set.Items[0].ModuleId;
                            rp.Module = set.Items[0].ModuleName;
                            rp.ModuleCode = set.Items[0].ModuleCode;
                            rp.IsSpecialPermission = false;
                            rp.CanCreate = set.Items.Where(x => x.PermissionTypeId == 1).Select(x => x.IsAccessible).FirstOrDefault();
                            rp.CanRead = set.Items.Where(x => x.PermissionTypeId == 2).Select(x => x.IsAccessible).FirstOrDefault();
                            rp.CanUpdate = set.Items.Where(x => x.PermissionTypeId == 3).Select(x => x.IsAccessible).FirstOrDefault();
                            rp.CanDelete = set.Items.Where(x => x.PermissionTypeId == 4).Select(x => x.IsAccessible).FirstOrDefault();

                            List<UserRolesSpecialPermissionTPT> ursp = new List<UserRolesSpecialPermissionTPT>();
                            if (specialper.Count > 0)
                            {
                                foreach (var sp in specialper)
                                {
                                    UserRolesSpecialPermissionTPT rsp = new UserRolesSpecialPermissionTPT();
                                    rsp.PermissionTypeId = sp.PermissionTypeId;
                                    rsp.PermissionName = sp.PermissionName;
                                    rsp.PermissionCode = sp.PermissionCode;
                                    rsp.CanAccess = sp.IsAccessible;
                                    ursp.Add(rsp);
                                }
                                rp.SpecialPermission = ursp;
                            }
                            lstRolePermTPT.Add(rp);
                        }
                    }
                    return lstRolePermTPT;
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public bool CheckDuplicatePermissionSets(int id, string name)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                PermissionSetTP permissionSet = new PermissionSetTP();
                var query = "";
                if (Franchise != null)
                {
                    query = @"Select * from [Auth].[PermissionSet]
                               where PermissionSetName =@permissionSetName and FranchiseId=@franchiseId";
                    permissionSet = ExecuteIEnumerableObject<PermissionSetTP>(ContextFactory.CrmConnectionString, query, new { permissionSetName = name, franchiseId = Franchise.FranchiseId }).FirstOrDefault();
                }
                else
                {
                    query = @"Select * from [Auth].[PermissionSet]
                               where PermissionSetName =@permissionSetName and FranchiseId is null";
                    permissionSet = ExecuteIEnumerableObject<PermissionSetTP>(ContextFactory.CrmConnectionString, query, new { permissionSetName = name }).FirstOrDefault();
                }
                if (permissionSet == null)
                    return false;
                if (id > 0)
                {
                    if (permissionSet.Id == id)
                        return false;
                    else
                        return true;
                }
                return true;
            }
        }

        public List<RolesTP> GetRolesList()
        {
            var query = @"SELECT [RoleId]
                        ,[RoleName]
                        ,[Description]
                        ,CONVERT(date, [CreatedOnUtc]) CreatedOnUtc
                        ,[IsDeletable]
                        ,[IsInheritable]
                        ,[FranchiseId]
                        ,[IsActive]
                        ,[DisplayName]
                        ,[SortOrder]
                        ,[RoleType]
                        FROM [Auth].[Roles] where IsActive=1 
                        order by sortorder";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var result = ExecuteIEnumerableObject<RolesTP>(ContextFactory.CrmConnectionString, query).ToList();
                return result;
            }
        }

        public Permissionset_VM GetPermissionSetbyRole(System.Guid roleid)
        {
            Permissionset_VM PSVM = new Permissionset_VM();
            List<PermissionTPT> lstPermTPT = new List<PermissionTPT>();
            var permSet = new List<Permission_PS>();
            var query = "";


            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                if (roleid != null)
                {
                    query = GetPermissionSetByRoleId;
                    permSet = ExecuteIEnumerableObject<Permission_PS>(ContextFactory.CrmConnectionString, query, new { roleId = roleid, RoleType = RoleTypebyRole(roleid) }).ToList();

                }

                var splitdata = permSet.GroupBy(x => x.ModuleId, (key, elements) =>
                 new
                 {
                     ModuleId = key,
                     Items = elements.ToList()
                 }).ToList();
                foreach (var set in splitdata)
                {
                    var baseper = set.Items.Where(x => x.IsSpecialPermission == false).ToList();
                    if (baseper.Count > 0)
                    {
                        if (set.Items[0].ModuleGroupId == null)
                        {
                            PermissionTPT pb = new PermissionTPT();
                            pb.id = set.Items[0].ModuleId;
                            pb.ModuleId = set.Items[0].ModuleId;
                            pb.PermissionTypeId = null;
                            pb.Name = set.Items[0].ModuleName;
                            pb.Module = set.Items[0].ModuleName;
                            pb.ModuleGroupId = null;
                            pb.parentId = null;
                            pb.IsSpecialPermission = false;
                            pb.Create = set.Items.Where(x => x.PermissionTypeId == 1).Select(x => x.IsAccessible).FirstOrDefault();
                            pb.Read = set.Items.Where(x => x.PermissionTypeId == 2).Select(x => x.IsAccessible).FirstOrDefault();
                            pb.Update = set.Items.Where(x => x.PermissionTypeId == 3).Select(x => x.IsAccessible).FirstOrDefault();
                            pb.Delete = set.Items.Where(x => x.PermissionTypeId == 4).Select(x => x.IsAccessible).FirstOrDefault();
                            lstPermTPT.Add(pb);
                        }
                        else
                        {
                            var checkGroupExist = lstPermTPT.Select(x => x.ModuleGroupId).Contains(set.Items[0].ModuleGroupId);
                            var parentGroupForModule = lstPermTPT.Where(pt => pt.ParentGroupId == set.Items[0].ModuleGroupId).FirstOrDefault();

                            if (parentGroupForModule == null && !checkGroupExist)
                            {

                                //PermissionTPT lstTPT = new PermissionTPT();
                                var parentGroup = GetCheckedParentvalue(lstPermTPT, set.Items[0]);


                                PermissionTPT pbg = new PermissionTPT();
                                pbg.id = (int)set.Items[0].ModuleGroupId;
                                pbg.ModuleId = null;
                                pbg.PermissionTypeId = null;
                                pbg.Name = set.Items[0].GroupName;
                                pbg.Module = set.Items[0].ModuleName;
                                pbg.ModuleGroupId = set.Items[0].ModuleGroupId;
                                //pbg.parentId = null;
                                if (parentGroup != null)
                                {
                                    pbg.parentId = parentGroup.id;
                                }
                                //.parentId = lstTPT
                                pbg.IsSpecialPermission = false;

                                //lstPermTPT = lstTPT;
                                lstPermTPT.Add(pbg);
                            }

                            PermissionTPT pb = new PermissionTPT();
                            pb.id = set.Items[0].ModuleId;
                            pb.ModuleId = set.Items[0].ModuleId;
                            pb.PermissionTypeId = null;
                            pb.Name = set.Items[0].ModuleName;
                            pb.Module = set.Items[0].ModuleName;
                            pb.ModuleGroupId = null;

                            if (parentGroupForModule != null)
                            {
                                pb.parentId = parentGroupForModule.id;
                            }
                            else
                            {
                                pb.parentId = (int)set.Items[0].ModuleGroupId;
                            }


                            pb.IsSpecialPermission = false;
                            pb.Create = set.Items.Where(x => x.PermissionTypeId == 1).Select(x => x.IsAccessible).FirstOrDefault();
                            pb.Read = set.Items.Where(x => x.PermissionTypeId == 2).Select(x => x.IsAccessible).FirstOrDefault();
                            pb.Update = set.Items.Where(x => x.PermissionTypeId == 3).Select(x => x.IsAccessible).FirstOrDefault();
                            pb.Delete = set.Items.Where(x => x.PermissionTypeId == 4).Select(x => x.IsAccessible).FirstOrDefault();
                            lstPermTPT.Add(pb);
                        }
                    }

                    var specialper = set.Items.Where(x => x.IsSpecialPermission == true).ToList();
                    if (specialper.Count > 0)
                    {
                        foreach (var sp in specialper)
                        {
                            PermissionTPT ps = new PermissionTPT();
                            ps.id = sp.ModuleId + 1000;
                            ps.ModuleId = null;
                            ps.PermissionTypeId = sp.PermissionTypeId;
                            ps.Name = sp.PermissionName;
                            ps.Module = sp.ModuleName;
                            ps.ModuleGroupId = null;
                            ps.parentId = sp.ModuleId;
                            ps.IsSpecialPermission = true;
                            ps.SpecialPermission = sp.IsAccessible;
                            lstPermTPT.Add(ps);
                        }

                    }

                }
                connection.Close();
                PSVM.PermissionSets = lstPermTPT;
                return PSVM;
            }
        }

        private PermissionTPT GetCheckedParentvalue(List<PermissionTPT> PermissionTP, Permission_PS Permission_PS)
        {
            if (Permission_PS.ParentGroupId == null)
            {
                return null;
            }
            PermissionTPT pbg = new PermissionTPT();
            //List<PermissionTPT> Permission_TP = new List<PermissionTPT>();
            //var checkGroupExist = PermissionTP.Select(x => x.ParentGroupId).Contains(Permission_PS.ParentGroupId);
            // var rootparentgroup = PermissionTP.Select(x => x.ParentGroupId).FirstOrDefault();
            var parentGroup = PermissionTP.Where(pt => pt.ParentGroupId == Permission_PS.ParentGroupId).FirstOrDefault();
            if (parentGroup == null)
            {
                pbg.id = 9000 + (int)Permission_PS.ModuleGroupId;
                //pbg.ModuleId = null;
                //pbg.PermissionTypeId = null;
                pbg.Name = Permission_PS.ParentName;
                //pbg.Module = Permission_PS.ModuleName;
                //pbg.ModuleGroupId = Permission_PS.ModuleGroupId;
                //pbg.parentId = Permission_PS.ParentGroupId;
                //pbg.IsSpecialPermission = false;
                pbg.ParentGroupId = Permission_PS.ParentGroupId;
                PermissionTP.Add(pbg);
                // return Permission_TP;
                return pbg;
            }

            // return Permission_TP;
            return parentGroup;
        }
        public List<UserInPermissionVM> GetAssignedUser(int permissionSetId)
        {
            var query = @"select p.FirstName +'-'+p.LastName as UserName, uip.PermissionSetId,uip.CreatedOn,uip.UserId from Auth.UserInPermission uip
                          join Auth.Users u on u.UserId = uip.UserId
                          join CRM.Person p on p.PersonId = u.PersonId
                          where PermissionSetId=@permissionSetId";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                return ExecuteIEnumerableObject<UserInPermissionVM>(ContextFactory.CrmConnectionString, query, new { permissionSetId = permissionSetId }).ToList();
            }
        }

        public dynamic GetRoleName(System.Guid id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var Query = @"select * from Auth.Roles where RoleId =@roleid";
                var data = ExecuteIEnumerableObject<dynamic>(ContextFactory.CrmConnectionString, Query, new
                {
                    roleid = id
                }).FirstOrDefault();
                return data;
            }
        }

        public string DisablePermissionSets(int id, string type)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                if (type == "Disable")
                {
                    var Query = @"select * from Auth.PermissionSet where Id=@Id";
                    var data = ExecuteIEnumerableObject<PermissionSetTP>(ContextFactory.CrmConnectionString, Query, new
                    {
                        Id = id
                    }).FirstOrDefault();
                    if (data != null)
                    {
                        data.IsActive = false;
                        connection.Update(data);
                        return "Success";
                    }
                }
                else
                {
                    var Query = @"select * from Auth.PermissionSet where Id=@Id";
                    var data = ExecuteIEnumerableObject<PermissionSetTP>(ContextFactory.CrmConnectionString, Query, new
                    {
                        Id = id
                    }).FirstOrDefault();
                    if (data != null)
                    {
                        data.IsActive = true;
                        connection.Update(data);
                        return "Success";
                    }
                }

            }
            return null;
        }

        public int RoleType()
        {
            if (User == null)
                return 0;
            int RoleType = 0;
            if (User.IsAdminInRole())
                RoleType = 1;
            else if (User.IsInRole(AppConfigManager.AppVendorRole.RoleId))
                RoleType = 3;
            else
                RoleType = 2;
            return RoleType;
        }

        public int RoleTypebyRole(Guid roleid)
        {
            if (roleid == null || roleid == Guid.Empty)
                return 0;

            var roletype = GetRolesList().Where(x => x.RoleId == roleid).Select(x => x.RoleType).FirstOrDefault();
            return roletype;
        }

        public override string Add(Permission data)
        {
            throw new NotImplementedException();
        }

        public override string Delete(int id)
        {
            throw new NotImplementedException();
        }

        public override Permission Get(int id)
        {
            throw new NotImplementedException();
        }

        public override ICollection<Permission> Get(List<int> idList)
        {
            throw new NotImplementedException();
        }

        public override string Update(Permission data)
        {
            throw new NotImplementedException();
        }

        public string queryEmptyPermissionSets = @"SELECT '' PermissionSetName, tm.Id AS ModuleId,tm.ModuleName,tm.ModuleCode,tm.ModuleGroupId,
                                                   tlv.Name AS GroupName, tm.ParentGroupId,tlp.Name as ParentName,
                                                   ptp.Id as PermissionTypeId,ptp.Name AS PermissionName,
                                                   ptp.Description AS PermissionDesc,ptp.IsSpecialPermission,0 IsAccessible
                                                   FROM [Auth].[Type_Module] tm 
                                                   LEFT JOIN CRM.Type_LookUpValues tlv ON tm.ModuleGroupId=tlv.Id
                                                   left join CRM.Type_LookUpValues tlp on tm.ParentGroupId=tlp.Id
                                                   CROSS JOIN (SELECT * FROM Auth.Type_PermissionTP WHERE TypeModuleId IS null) ptp
                                                   where tm.RoleType=@RoleType
                                                   UNION
                                                   SELECT '' PermissionSetName, tm.Id AS ModuleId,tm.ModuleName,tm.ModuleCode,tm.ModuleGroupId,
                                                   tlv.Name AS GroupName,tm.ParentGroupId,tlp.Name as ParentName,
                                                   ptp.Id as PermissionTypeId,ptp.Name AS PermissionName,
                                                   ptp.Description AS PermissionDesc,ptp.IsSpecialPermission,0 IsAccessible
                                                   FROM [Auth].[Type_Module] tm 
                                                   LEFT JOIN CRM.Type_LookUpValues tlv ON tm.ModuleGroupId=tlv.Id
                                                   left join CRM.Type_LookUpValues tlp on tm.ParentGroupId=tlp.Id
                                                   JOIN Auth.Type_PermissionTP ptp ON tm.Id=ptp.TypeModuleId
                                                   where tm.RoleType=@RoleType
                                                   ORDER BY tm.Id ASC";

        public string queryPermissionSetsById = @"select ModuleId,ModuleName,ModuleCode,ModuleGroupId,GroupName,
                                                  ParentGroupId,ParentName,
                                                  mp.PermissionTypeId,PermissionName,
                                                  PermissionDesc,IsSpecialPermission,coalesce(IsAccessible,0) IsAccessible from
                                                  (
                                                  SELECT tm.Id AS ModuleId,tm.ModuleName,tm.ModuleCode,tm.ModuleGroupId,
                                                  tlv.Name AS GroupName,tm.ParentGroupId,tlp.Name as ParentName,
                                                  ptp.Id as PermissionTypeId,ptp.Name AS PermissionName,
                                                  ptp.Description AS PermissionDesc,ptp.IsSpecialPermission
                                                  FROM [Auth].[Type_Module] tm 
                                                  LEFT JOIN CRM.Type_LookUpValues tlv ON tm.ModuleGroupId=tlv.Id
                                                  left join CRM.Type_LookUpValues tlp on tm.ParentGroupId=tlp.Id
                                                  CROSS JOIN (SELECT * FROM Auth.Type_PermissionTP WHERE TypeModuleId IS null) ptp
                                                  where tm.RoleType=@RoleType
                                                  UNION
                                                  SELECT tm.Id AS ModuleId,tm.ModuleName,tm.ModuleCode,tm.ModuleGroupId,
                                                  tlv.Name AS GroupName,tm.ParentGroupId,tlp.Name as ParentName,
                                                  ptp.Id as PermissionTypeId,ptp.Name AS PermissionName,
                                                  ptp.Description AS PermissionDesc,ptp.IsSpecialPermission
                                                  FROM [Auth].[Type_Module] tm 
                                                  LEFT JOIN CRM.Type_LookUpValues tlv ON tm.ModuleGroupId=tlv.Id
                                                  left join CRM.Type_LookUpValues tlp on tm.ParentGroupId=tlp.Id
                                                  JOIN Auth.Type_PermissionTP ptp ON tm.Id=ptp.TypeModuleId
                                                  where tm.RoleType=@RoleType
                                                  ) as mp
                                                  left join
                                                  (select 
                                                  case when pps.TypeModuleId is null then (SELECT ptp.TypeModuleId
                                                  FROM Auth.Type_PermissionTP ptp
                                                  JOIN  [Auth].[Type_Module] tm  ON tm.Id=ptp.TypeModuleId
                                                  where ptp.Id=pps.PermissionTypeId) else pps.TypeModuleId end as TypeModuleId
                                                  ,pps.PermissionTypeId,pps.IsAccessible from [Auth].PermissionSet ps
                                                  join Auth.Permission_PS pps on ps.Id=pps.PermissionSetId
                                                  where ps.Id=@PermissionSetId) as pset on mp.ModuleId=pset.TypeModuleId and mp.PermissionTypeId=pset.PermissionTypeId
                                                  order by 1 asc ";


        public string qUserRolesPermissions = @"select ModuleId,ModuleName,ModuleCode,mp.PermissionTypeId,PermissionName,PermissionCode,IsSpecialPermission,isnull(IsAccessible,0) IsAccessible from 
                              (
                              SELECT tm.Id AS ModuleId,tm.ModuleName,ModuleCode,ptp.Id as PermissionTypeId,ptp.Name AS PermissionName,PermissionCode,IsSpecialPermission
                              FROM [Auth].[Type_Module] tm 
                              CROSS JOIN (SELECT * FROM Auth.Type_PermissionTP WHERE TypeModuleId IS null) ptp
                              where tm.RoleType=@RoleType
                              UNION
                              SELECT tm.Id AS ModuleId,tm.ModuleName,ModuleCode,ptp.Id as PermissionTypeId,ptp.Name AS PermissionName,PermissionCode,IsSpecialPermission
                              FROM [Auth].[Type_Module] tm 
                              JOIN Auth.Type_PermissionTP ptp ON tm.Id=ptp.TypeModuleId
                              where tm.RoleType=@RoleType
                              ) as mp  --- master data
                              left join
                              (select 
                              -- select TypeModuleId for special permission
                              case when psr.TypeModuleId is null then (SELECT ptp.TypeModuleId
                              FROM Auth.Type_PermissionTP ptp
                              JOIN  [Auth].[Type_Module] tm  ON tm.Id=ptp.TypeModuleId
                              where ptp.Id=psr.PermissionTypeId) else psr.TypeModuleId end as TypeModuleId
                              ,psr.PermissionTypeId,psr.IsAccessible
                               from  ( 
                               ---- Select list of permission
                                select max(cast(IsAccessible as tinyint)) IsAccessible, TypeModuleId,PermissionTypeId
                               from
                               (
                               select pr.TypeModuleId,pr.PermissionTypeId,pr.IsAccessible from Auth.Users u
                               join Auth.UsersInRoles ur on u.UserId=ur.UserId
                               join Auth.Roles r on ur.RoleId=r.RoleId
                               join [Auth].[Permission_Role] pr on r.RoleId=pr.RoleId
                               where u.UserId=@UserId
                               union all
                              select pps.TypeModuleId,pps.PermissionTypeId,pps.IsAccessible from [Auth].[UserInPermission] uip
                              join Auth.Permission_PS pps on uip.PermissionSetId=pps.PermissionSetId
                               where UserId=@UserId
                               ) as perm
                                group by TypeModuleId,PermissionTypeId
                               ) as psr) as psr1 on mp.ModuleId=psr1.TypeModuleId and mp.PermissionTypeId=psr1.PermissionTypeId
                               order by 1 asc";

        public string GetPermissionSetByRoleId = @"select ModuleId,ModuleName,ModuleCode,ModuleGroupId,
                                                   GroupName,ParentGroupId,ParentName,
                                                   mp.PermissionTypeId,PermissionName,PermissionDesc,
                                                   IsSpecialPermission,coalesce(IsAccessible,0) IsAccessible 
                                                   from
                                                   (SELECT tm.Id AS ModuleId,tm.ModuleName,tm.ModuleCode,tm.ModuleGroupId,tlv.Name AS GroupName,
                                                   tm.ParentGroupId,tlp.Name as ParentName,
                                                   ptp.Id as PermissionTypeId,ptp.Name AS PermissionName,
                                                   ptp.Description AS PermissionDesc,ptp.IsSpecialPermission
                                                   FROM [Auth].[Type_Module] tm 
                                                   LEFT JOIN CRM.Type_LookUpValues tlv ON tm.ModuleGroupId=tlv.Id
                                                   left join CRM.Type_LookUpValues tlp on tm.ParentGroupId=tlp.Id
                                                   CROSS JOIN (SELECT * FROM Auth.Type_PermissionTP WHERE TypeModuleId IS null) ptp
                                                   where tm.RoleType=@RoleType
                                                   UNION
                                                   SELECT tm.Id AS ModuleId,tm.ModuleName,tm.ModuleCode,tm.ModuleGroupId,
                                                   tlv.Name AS GroupName,tm.ParentGroupId,tlp.Name as ParentName,
                                                   ptp.Id as PermissionTypeId,ptp.Name AS PermissionName,
                                                   ptp.Description AS PermissionDesc,ptp.IsSpecialPermission
                                                   FROM [Auth].[Type_Module] tm 
                                                   LEFT JOIN CRM.Type_LookUpValues tlv ON tm.ModuleGroupId=tlv.Id
                                                   left join CRM.Type_LookUpValues tlp on tm.ParentGroupId=tlp.Id
                                                   JOIN Auth.Type_PermissionTP ptp ON tm.Id=ptp.TypeModuleId
                                                   where tm.RoleType=@RoleType
                                                   ) as mp
                                                   left join
                                                   (select 
                                                   case when pr.TypeModuleId is null then (SELECT ptp.TypeModuleId
                                                   FROM Auth.Type_PermissionTP ptp
                                                   JOIN  [Auth].[Type_Module] tm  ON tm.Id=ptp.TypeModuleId
                                                   where ptp.Id=pr.PermissionTypeId) else pr.TypeModuleId end as TypeModuleId
                                                   ,pr.PermissionTypeId,pr.IsAccessible from [Auth].[Permission_Role] pr
                                                    where pr.RoleId=@roleId) 
                                                    as pset on mp.ModuleId=pset.TypeModuleId and mp.PermissionTypeId=pset.PermissionTypeId
                                                    order by 1 asc";

        public string readUserRolesPermissions = @"select ModuleId,ModuleName,ModuleCode,mp.PermissionTypeId,PermissionName,
                              PermissionCode,IsSpecialPermission,case when PermissionCode in ('Read','OpportunityAllRecordAccess',
                              'QuotesAllRecordAccess','SalesOrderAllRecordAccess','ProcurementDashboardAllRecordAccess','OtherCalendars ',
                               'CaseManagementAllRecordAccess') then 1 else 0 end IsAccessible from 
                              (SELECT tm.Id AS ModuleId,tm.ModuleName,ModuleCode,ptp.Id as PermissionTypeId,ptp.Name AS PermissionName,PermissionCode,IsSpecialPermission
                              FROM [Auth].[Type_Module] tm 
                              CROSS JOIN (SELECT * FROM Auth.Type_PermissionTP WHERE TypeModuleId IS null) ptp
                              where tm.RoleType=@RoleType
                              UNION
                              SELECT tm.Id AS ModuleId,tm.ModuleName,ModuleCode,ptp.Id as PermissionTypeId,ptp.Name AS PermissionName,PermissionCode,IsSpecialPermission
                              FROM [Auth].[Type_Module] tm 
                              JOIN Auth.Type_PermissionTP ptp ON tm.Id=ptp.TypeModuleId
                              where tm.RoleType=@RoleType
                              ) as mp  --- master data
                              left join
                              (select 
                              -- select TypeModuleId for special permission
                              case when psr.TypeModuleId is null then (SELECT ptp.TypeModuleId
                              FROM Auth.Type_PermissionTP ptp
                              JOIN  [Auth].[Type_Module] tm  ON tm.Id=ptp.TypeModuleId
                              where ptp.Id=psr.PermissionTypeId) else psr.TypeModuleId end as TypeModuleId
                              ,psr.PermissionTypeId,psr.IsAccessible
                               from  ( 
                               ---- Select list of permission
                                select max(cast(IsAccessible as tinyint)) IsAccessible, TypeModuleId,PermissionTypeId
                               from
                               (
                               select pr.TypeModuleId,pr.PermissionTypeId,pr.IsAccessible from Auth.Users u
                               join Auth.UsersInRoles ur on u.UserId=ur.UserId
                               join Auth.Roles r on ur.RoleId=r.RoleId
                               join [Auth].[Permission_Role] pr on r.RoleId=pr.RoleId
                               where u.UserId=@UserId
                               union all
                              select pps.TypeModuleId,pps.PermissionTypeId,pps.IsAccessible from [Auth].[UserInPermission] uip
                              join Auth.Permission_PS pps on uip.PermissionSetId=pps.PermissionSetId
                               where UserId=@UserId
                               ) as perm
                                group by TypeModuleId,PermissionTypeId
                               ) as psr) as psr1 on mp.ModuleId=psr1.TypeModuleId and mp.PermissionTypeId=psr1.PermissionTypeId
                               order by 1 asc";

    }
}
