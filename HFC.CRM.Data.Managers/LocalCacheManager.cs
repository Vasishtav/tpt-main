﻿using System.Collections.Generic;
using HFC.CRM.Data;
namespace HFC.CRM.Operations
{
    internal class LocalCacheManager : HFC.CRM.Core.Common.CacheManager
    {
        new public List<Type_Appointment> AppointmentTypeCollection
        {
            get { return base.AppointmentTypeCollection; }
            set { base.AppointmentTypeCollection = value; }
        }

        new public List<Type_LeadStatus> LeadStatusTypeCollection
        {
            get { return base.LeadStatusTypeCollection; }
            set { base.LeadStatusTypeCollection = value; }
        }
    }
}
