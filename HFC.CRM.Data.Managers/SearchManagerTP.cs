﻿using System.Data.Entity.Core.EntityClient;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.DTO.Search;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Dapper;
using HFC.CRM.Managers.DTO;
using HFC.CRM.Data.Context;

namespace HFC.CRM.Managers
{

    public class SearchManagerTP : DataManagerFE
    {
        private PermissionManager permissionManager;// = new PermissionManager(SessionManager.CurrentFranchise);
        public SearchManagerTP(User user, Franchise franchise) : base(user, franchise)
        {
            permissionManager = new PermissionManager(this.User, this.Franchise);
        }

        public List<LeadAccountDTO> GetLeads(string searchType, string value)
        {
            if (string.IsNullOrEmpty(value)) return null;

            List<LeadAccountDTO> result = null;
            var query = "exec CRM.GetLeadsGlobalSearch @FranchiseId, @SearchType, @Value";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<LeadAccountDTO>(query
                    , new
                    {
                        FranchiseId = this.Franchise.FranchiseId
                        ,
                        SearchType = searchType
                        ,
                        Value = value
                    }).ToList();

                connection.Close();
            }

            return result;

        }
        public List<LeadAccountDTO> GetAccounts(string searchType, string value)
        {
            if (string.IsNullOrEmpty(value)) return null;

            List<LeadAccountDTO> result = null;
            var query = "exec CRM.GetAccountsGlobalSearch @FranchiseId, @SearchType, @Value";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<LeadAccountDTO>(query
                    , new
                    {
                        FranchiseId = this.Franchise.FranchiseId
                        ,
                        SearchType = searchType
                        ,
                        Value = value
                    }).ToList();

                connection.Close();
            }

            return result;

        }
        public List<VendorsDTO> GetVendors(string searchType, string value)
        {
            if (string.IsNullOrEmpty(value)) return null;

            List<VendorsDTO> result = null;
            var query = "exec CRM.GetVendorsGlobalSearch @FranchiseId, @SearchType, @Value";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<VendorsDTO>(query
                    , new
                    {
                        FranchiseId = this.Franchise.FranchiseId
                        , SearchType = searchType
                        , Value = value
                    }).ToList();

                connection.Close();
            }

            return result;

        }
        public List<ContactsDTO> GetContacts(string searchType, string value)
        {
            if (string.IsNullOrEmpty(value)) return null;

            List<ContactsDTO> result = null;
            var query = "exec CRM.GetContactsGlobalSearch @FranchiseId, @SearchType, @Value";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<ContactsDTO>(query
                    , new
                    {
                        FranchiseId = this.Franchise.FranchiseId
                        , SearchType = searchType
                        , Value = value
                    }).ToList();

                connection.Close();
            }

            result = ApplyPermissionContact(result);
            return result;

        }
        public List<OpportunityDTO> GetOpportunities(string searchType, string value)
        {
            if (string.IsNullOrEmpty(value)) return null;

            List<OpportunityDTO> result = null;
            var query = "exec CRM.GetOpportunitiesGlobalSearch @FranchiseId, @SearchType, @Value";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<OpportunityDTO>(query
                    , new
                    {
                        FranchiseId = this.Franchise.FranchiseId
                        , SearchType = searchType
                        , Value = value
                    }).ToList();

                connection.Close();
            }

            result = ApplyPermissionOpportunity(result);
            return result;

        }
        public List<AppointmentDTO> GetAppointments(string searchType, string value)
        {
            if (string.IsNullOrEmpty(value)) return null;

            List<AppointmentDTO> result = null;
            var query = "exec CRM.GetAppointmentsGlobalSearch @FranchiseId, @SearchType, @Value";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<AppointmentDTO>(query
                    , new
                    {
                        FranchiseId = this.Franchise.FranchiseId
                        ,
                        SearchType = searchType
                        ,
                        Value = value
                    }).ToList();

                connection.Close();
            }

            foreach (var item in result)
            {
                item.StartDate = TimeZoneManager.ToLocal(item.StartDateUTC);
                item.EndDate = TimeZoneManager.ToLocal(item.EndDateUTC);
            }

            result = ApplyPermissionAppointment(result);
            return result;

        }
        public List<TaskDTO> GetTasks(string searchType, string value)
        {
            if (string.IsNullOrEmpty(value)) return null;

            List<TaskDTO> result = null;
            var query = "exec CRM.GetTasksGlobalSearch @FranchiseId, @SearchType, @Value";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<TaskDTO>(query
                    , new
                    {
                        FranchiseId = this.Franchise.FranchiseId
                        ,
                        SearchType = searchType
                        ,
                        Value = value
                    }).ToList();

                connection.Close();
            }

            //foreach (var item in result)
            //{
            //    item.StartDate = TimeZoneManager.ToLocal(item.StartDateUTC);
            //    item.EndDate = TimeZoneManager.ToLocal(item.EndDateUTC);
            //}

            //result = ApplyPermissionAppointment(result);
            return result;

        }
        public List<QuoteDTO> GetQuotes(string searchType, string value)
        {
            if (string.IsNullOrEmpty(value)) return null;

            List<QuoteDTO> result = null;
            var query = "exec CRM.GetQuotesGlobalSearch @FranchiseId, @SearchType, @Value";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<QuoteDTO>(query
                    , new
                    {
                        FranchiseId = this.Franchise.FranchiseId
                        , SearchType = searchType
                        , Value = value
                    }).ToList();

                connection.Close();
            }

            result = TimeZoneManager.ConvertToLocalList<QuoteDTO>(result);

            result = ApplyPermissionQuote(result);
            return result;

        }
        public List<OrderDTO> GetOrders(string searchType, string value)
        {
            if (string.IsNullOrEmpty(value)) return null;

            List<OrderDTO> result = null;
            var query = "exec CRM.GetOrdersGlobalSearch @FranchiseId, @SearchType, @Value";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<OrderDTO>(query
                    , new
                    {
                        FranchiseId = this.Franchise.FranchiseId
                        ,
                        SearchType = searchType
                        ,
                        Value = value
                    }).ToList();

                connection.Close();
            }

            result = TimeZoneManager.ConvertToLocalList<OrderDTO>(result);

            result = ApplyPermissionOrder(result);
            return result;

        }
        public List<NoteDTO> GetNotes(string searchType, string value)
        {
            if (string.IsNullOrEmpty(value)) return null;

            List<NoteDTO> result = null;
            var query = "exec CRM.GetNotesGlobalSearch @FranchiseId, @SearchType, @Value";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<NoteDTO>(query
                    , new
                    {
                        FranchiseId = this.Franchise.FranchiseId
                        , SearchType = searchType
                        , Value = value
                    }).ToList();

                connection.Close();
            }

            HandleFileName(ref result);
            result = TimeZoneManager.ConvertToLocalList<NoteDTO>(result);
            return result;
        }
        public List<PaymentDTO> GetPayments(string searchType, string value)
        {
            if (string.IsNullOrEmpty(value)) return null;

            List<PaymentDTO> result = null;
            var query = "exec CRM.GetPaymentsGlobalSearch @FranchiseId, @SearchType, @Value";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<PaymentDTO>(query
                    , new
                    {
                        FranchiseId = this.Franchise.FranchiseId
                        , SearchType = searchType
                        , Value = value
                    }).ToList();

                connection.Close();
            }

            result = TimeZoneManager.ConvertToLocalList<PaymentDTO>(result);
            return result;
        }
        public List<MpoVpoDTO> GetMposVpos(string searchType, string value)
        {
            if (string.IsNullOrEmpty(value)) return null;

            List<MpoVpoDTO> result = null;
            var query = "exec CRM.GetMposVposGlobalSearch @FranchiseId, @SearchType, @Value";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<MpoVpoDTO>(query
                    , new
                    {
                        FranchiseId = this.Franchise.FranchiseId
                        ,
                        SearchType = searchType
                        ,
                        Value = value
                    }).ToList();

                connection.Close();
            }

            result = TimeZoneManager.ConvertToLocalList<MpoVpoDTO>(result);

            // need to filter out all the unwanted records.
            List<MpoVpoDTO> ulist = new List<MpoVpoDTO>();
            List<MpoVpoDTO> dupLlist = new List<MpoVpoDTO>();

            foreach (var item in result)
            {
                var temp = ulist.Where(x => x.Vpo == item.Vpo &&
                    x.Mpo == item.Mpo && x.VendorId == item.VendorId).ToList();
                if (temp.Count == 0) // the item is not available in the uList.
                {
                    ulist.Add(item);
                }
                else
                {
                    dupLlist.Add(item);
                }
            }

            foreach (var item in ulist)
            {
                var temp = dupLlist.Where(x => x.PurchaseOrderId == item.PurchaseOrderId
                                && x.Vpo == item.Vpo && x.VendorId == item.VendorId).ToList();
                temp.Add(item);

                if (temp.Count > 1)
                {
                    var error = false;
                    var backordered = false;
                    var partialshipped = false;
                    var shipped = false;

                    foreach (var dup in temp)
                    {
                        if (dup.StatusId == 10)
                        {
                            error = true;
                        }
                        else if (dup.StatusId == 4)
                        {
                            backordered = true;
                        }
                        else if (dup.StatusId == 3)
                        {
                            shipped = true;
                        }
                    }

                    if (error)
                    {
                        item.VpoStatus = "Error";
                        item.StatusId = 10;
                    }
                    else if (partialshipped)
                    {
                        item.VpoStatus = "Partial Shipped";
                        item.StatusId = 4;
                    }
                    else if (backordered)
                    {
                        item.VpoStatus = "Back Ordered";
                        item.StatusId = 4;
                    }
                }

            }

            //return result;

            result = ApplyPermissionMpoVpo(result);

            return ulist;
        }
        public List<ShipmentDTO> GetShipments(string searchType, string value)
        {
            if (string.IsNullOrEmpty(value)) return null;

            List<ShipmentDTO> result = null;
            var query = "exec CRM.GetShipmentsGlobalSearch @FranchiseId, @SearchType, @Value";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<ShipmentDTO>(query
                    , new
                    {
                        FranchiseId = this.Franchise.FranchiseId
                        , SearchType = searchType
                        , Value = value
                    }).ToList();

                connection.Close();
            }

            result = TimeZoneManager.ConvertToLocalList<ShipmentDTO>(result);
            return result;
        }
        public List<CaseDTO> GetCases(string searchType, string value)
        {
            if (string.IsNullOrEmpty(value)) return null;

            List<CaseDTO> result = null;
            var query = "exec CRM.GetCasesGlobalSearch @FranchiseId, @SearchType, @Value";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<CaseDTO>(query
                    , new
                    {
                        FranchiseId = this.Franchise.FranchiseId
                        , SearchType = searchType
                        , Value = value
                    }).ToList();

                connection.Close();
            }

            result = TimeZoneManager.ConvertToLocalList<CaseDTO>(result);
            return result;
        }
        public List<AddressDTO> GetAddresses(string searchType, string value)
        {
            if (string.IsNullOrEmpty(value)) return null;

            List<AddressDTO> result = null;
            var query = "exec CRM.GetAddressesGlobalSearch @FranchiseId, @SearchType, @Value";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                result = connection.Query<AddressDTO>(query
                    , new
                    {
                        FranchiseId = this.Franchise.FranchiseId
                        , SearchType = searchType
                        , Value = value
                    }).ToList();

                connection.Close();
            }

            result = ApplyPermissionAddress(result);
            return result;
        }

        #region Helper methods
       
        private List<AppointmentDTO> ApplyPermissionAppointment(List<AppointmentDTO> data)
        {
            List<AppointmentDTO> result = data;
            if (data != null)
            {
                var GetPermission = permissionManager.GetUserRolesPermissions();
                var ModuleData = GetPermission.Find(x => x.ModuleCode == "Calendar");
                var specialPermission = ModuleData.SpecialPermission.Find(x => x.PermissionCode == "OtherCalendars ").CanAccess;
                if (specialPermission == false)
                {
                    var filtered = data.Where(x => (x.AttendeesId.Contains(this.User.PersonId.ToString()))
                                       ).ToList();
                    result = filtered;

                    //data.Where(y => y.AttendeesName.Contains(this.User.per))
                }
            }

            return result;
        }
        private List<OpportunityDTO> ApplyPermissionOpportunity(List<OpportunityDTO> data)
        {
            List<OpportunityDTO> result = data;
            if (data != null)
            {
                var GetPermission = permissionManager.GetUserRolesPermissions();
                var ModuleData = GetPermission.Find(x => x.ModuleCode == "Opportunity");
                var specialPermission = ModuleData.SpecialPermission.Find(x => x.PermissionCode == "OpportunityAllRecordAccess").CanAccess;
                if (specialPermission == false)
                {
                    var filtered = data.Where(x => (x.SalesAgentId == this.User.PersonId ||
                                       x.InstallerId == this.User.PersonId)
                                       ).ToList();
                    result = filtered;
                }
            }

            return result;
        }
        private List<QuoteDTO> ApplyPermissionQuote(List<QuoteDTO> data)
        {
            List<QuoteDTO> result = data;
            if (data != null)
            {
                var GetPermission = permissionManager.GetUserRolesPermissions();
                var ModuleData = GetPermission.Find(x => x.ModuleCode == "Quote");
                var specialPermission = ModuleData.SpecialPermission.Find(x => x.PermissionCode == "QuotesAllRecordAccess").CanAccess;
                if (specialPermission == false)
                {
                    var filtered = data.Where(x => (x.SalesAgentId == this.User.PersonId ||
                                       x.InstallerId == this.User.PersonId)
                                       ).ToList();
                    result = filtered;
                }
            }

            return result;
        }
        private List<OrderDTO> ApplyPermissionOrder(List<OrderDTO> data)
        {
            List<OrderDTO> result = data;
            if (data != null)
            {
                var GetPermission = permissionManager.GetUserRolesPermissions();
                var ModuleData = GetPermission.Find(x => x.ModuleCode == "SalesOrder");
                var specialPermission = ModuleData.SpecialPermission.Find(x => x.PermissionCode == "SalesOrderAllRecordAccess").CanAccess;
                if (specialPermission == false)
                {
                    var filtered = data.Where(x => (x.SalesAgentId == this.User.PersonId ||
                                       x.InstallerId == this.User.PersonId)
                                       ).ToList();
                    result = filtered;
                }
            }

            return result;
        }
        private List<MpoVpoDTO> ApplyPermissionMpoVpo(List<MpoVpoDTO> data)
        {
            List<MpoVpoDTO> result = data;
            if (data != null)
            {
                var GetPermission = permissionManager.GetUserRolesPermissions();
                var ModuleData = GetPermission.Find(x => x.ModuleCode == "ProcurementDashboard");
                var specialPermission = ModuleData.SpecialPermission.Find(x => x.PermissionCode == "ProcurementDashboardAllRecordAccess").CanAccess;
                if (specialPermission == false)
                {
                    var filtered = data.Where(x => (x.SalesAgentId == this.User.PersonId ||
                                       x.InstallerId == this.User.PersonId)
                                       ).ToList();
                    result = filtered;
                }
            }

            return result;
        }

        private List<ContactsDTO> ApplyPermissionContact(List<ContactsDTO> data)
        {
            List<ContactsDTO> result = data;
            var GetPermission = permissionManager.GetUserRolesPermissions();
            var Leadpermission = GetPermission.Find(x => x.ModuleCode == "Lead");
            var ListLead = Leadpermission.CanRead;
            var Accountpermission = GetPermission.Find(x => x.ModuleCode == "Account");
            var ListAccount = Accountpermission.CanRead;
            List<ContactsDTO> newresult = new List<ContactsDTO>();
            foreach (var item in result)
            {
                if (item.IsLead && ListLead)
                {
                    newresult.Add(item);
                }
                if (!item.IsLead && ListAccount)
                {
                    newresult.Add(item);
                }
            }
            return newresult;
        }

        private List<AddressDTO> ApplyPermissionAddress(List<AddressDTO> data)
        {
            List<AddressDTO> result = data;
            var GetPermission = permissionManager.GetUserRolesPermissions();
            var Leadpermission = GetPermission.Find(x => x.ModuleCode == "Lead");
            var ListLead = Leadpermission.CanRead;
            var Accountpermission = GetPermission.Find(x => x.ModuleCode == "Account");
            var ListAccount = Accountpermission.CanRead;
            List<AddressDTO> newresult = new List<AddressDTO>();
            foreach (var item in result)
            {
                if ((item.LeadId != null || item.LeadId != 0) && ListLead)
                {
                    newresult.Add(item);
                }
                else if ((item.AccountId != null || item.LeadId != 0) && ListAccount)
                {
                    newresult.Add(item);
                }
            }
            return newresult;
        }

        private void HandleFileName(ref List<NoteDTO> notes)
        {
            foreach (var noteresult in notes)
            {
                //var person = CacheManager.UserCollection.FirstOrDefault(f => f.PersonId == noteresult.CreatedBy);

                if (!string.IsNullOrEmpty(noteresult.FileName))
                {
                    var result = noteresult.FileName.Split('_');
                    string filename = string.Join("_", result.Skip(2).ToArray());
                    if (filename == "")
                        filename = string.Join("_", result.ToArray());
                    noteresult.FileName = filename;
                }
            }
        }
        #endregion

    }
}
