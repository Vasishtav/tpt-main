﻿using Dapper;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.DTO;
using HFC.CRM.Data.Context;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HFC.CRM.DTO.ShipNotice;
using HFC.CRM.Core.Membership;
using HFC.CRM.Core;
using static HFC.CRM.Data.EnumExtension;

namespace HFC.CRM.Managers
{
    public class ShipNoticeManager : DataManager<ShipNotice>
    {
        public ShipNoticeManager(User user, Franchise franchise) : this(user, user, franchise)
        {
        }

        public ShipNoticeManager(User user, User authorizingUser, Franchise franchise)
        {
            this.User = user;
            this.AuthorizingUser = authorizingUser;
            this.Franchise = franchise;
        }

        private PICManager picMgr = new PICManager();
        private VendorInvoiceManager _vendorInvoiceManager = new VendorInvoiceManager();
        private PurchaseOrderManager _poMgr = new PurchaseOrderManager();

        public List<Type_ShipmentStatus> GeTypeShipmentStatus()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    // ShipNotice sNotice = new ShipNotice();

                    var query = @"SELECT * FROM crm.Type_ShipmentStatus;";
                    connection.Open();

                    var qDetails = connection.QueryMultiple(query);

                    var shipmentstatus = qDetails.Read<Type_ShipmentStatus>().ToList();

                    return shipmentstatus;
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    throw e;
                }
                finally
                {
                    connection.Close();
                }
            }
        }


        public List<dynamic> GetRelatedShipNoticeIds(int shipNoticeId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var query = @"select sn.ShipNoticeId,sn.ShipmentId from crm.ShipNotice sn
                                inner join crm.ShipNotice sn2 on sn2.PurchaseOrderId = sn.PurchaseOrderId
                                where sn2.ShipNoticeId = @snId";
                    var result = connection.Query<dynamic>(query
                        , new { snId = shipNoticeId }).ToList();

                    return result;
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    throw;
                }
                finally
                {
                    connection.Close();
                }

            }
        }

        public bool UpdateShipNoticeOriginal(int ShipNoticeId, ShipNotice model)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    // ShipNotice sNotice = new ShipNotice();

                    connection.Open();

                    var query = @"SELECT * FROM crm.ShipmentDetail sd where sd.ShipNoticeId=@ShipNoticeId;";

                    var qDetails = connection.QueryMultiple(query, new { ShipNoticeId = ShipNoticeId });

                    var orgSN = connection.Get<ShipNotice>(ShipNoticeId);
                    if (!orgSN.ReceivedComplete && model.ReceivedComplete)
                    {
                        var orgSDetails = qDetails.Read<ShipmentDetail>().ToList();
                        foreach (var item in orgSDetails)
                        {
                            if (item.Status == 2 || item.Status == 3 || item.Status == 4)
                            {
                            }
                            else
                            {
                                item.Status = 1;
                                item.QuantityReceived = item.QuantityShipped;
                            }
                            item.UpdatedOn = DateTime.UtcNow;
                            item.UpdatedBy = SessionManager.CurrentUser.PersonId;
                            connection.Update<ShipmentDetail>(item);
                        }
                        model.ReceivedCompleteDate = DateTime.UtcNow;
                    }
                    model.UpdatedBy = SessionManager.CurrentUser.PersonId;
                    model.UpdatedOn = DateTime.UtcNow;
                    connection.Update<ShipNotice>(model);
                    if (model.ReceivedComplete)
                    {
                        UpdateSOStatus(model.PurchaseOrderId);
                    }
                    return true;
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    throw e;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool UpdateShipNotice(int ShipNoticeId, ShipNotice model)
        {
            //if (model.ReceivedComplete == true)
            //    throw new Exception("Shipment is fully received");

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                // Get the existing ShipNotice details to move to the change history...

                var oldShipNotice = this.GetValue(model.ShipNoticeId, false);

                var transaction = connection.BeginTransaction();

                try
                {

                    HandleChangeTracking(oldShipNotice, model, connection, transaction);

                    var quantityShipped = model.ListShipmentDetails.Sum(x => x.QuantityShipped);
                    var quantityReceived = model.ListShipmentDetails.Sum(y => y.QuantityReceived);
                    if (quantityShipped == quantityReceived)
                    {
                        model.ReceivedComplete = true;
                        model.ReceivedCompleteDate = DateTime.UtcNow;
                    }
                    else
                    {
                        model.ReceivedComplete = false;
                        model.ReceivedCompleteDate = null;
                    }

                    // Update the header (shipnotice) first.
                    var query = @"Update crm.ShipNotice 
                                set RemakeReference = @remakereference
                                , CheckedinDate = @cdate
                                , UpdatedOn = @updatedOn
                                , UpdatedBy = @updatedBy ";

                    //if (model.ReceivedComplete)
                    query += @", ReceivedComplete = @receivedComplete 
                                   , ReceivedCompleteDate = @receivedCompleteDate ";

                    query += " where ShipNoticeId = @shipnoticeId";
                    connection.Execute(query, new
                    {
                        remakereference = model.RemakeReference
                        ,
                        cdate = TimeZoneManager.ToUTC(model.CheckedinDate)
                        ,
                        receivedComplete = model.ReceivedComplete
                        ,
                        receivedCompleteDate = model.ReceivedCompleteDate
                        ,
                        updatedon = DateTime.UtcNow
                        ,
                        updatedBy = this.User.PersonId
                        ,
                        shipnoticeId = model.ShipNoticeId
                    }
                    , transaction); ;

                    // update the line items (shipment details)
                    var queryShipmentDetail = @"update crm.ShipmentDetail set PackingSlip = @packingSlip
	                                , QuantityReceived = @quantityReceived
	                                , QuantityDamaged = @qantityDamaged
	                                , QuantityMisShipped = @quantityMisShipped
	                                , QuantityShortShipped = @quantityShortShipped
	                                , Notes = @notes
                                    , UpdatedOn = @updatedOn
                                    , UpdatedBy = @updatedBy
                                where ShipmentDetailId = @sdid;";
                    foreach (var item in model.ListShipmentDetails)
                    {
                        connection.Execute(queryShipmentDetail,
                            new
                            {
                                packingSlip = item.PackingSlip
                            ,
                                quantityReceived = item.QuantityReceived
                            ,
                                qantityDamaged = item.QuantityDamaged
                            ,
                                quantityMisShipped = item.QuantityMisShipped
                            ,
                                quantityShortShipped = item.QuantityShortShipped
                            ,
                                notes = item.Notes
                            ,
                                updatedon = DateTime.UtcNow
                            ,
                                updatedBy = this.User.PersonId
                            ,
                                sdid = item.ShipmentDetailId
                            }
                            , transaction);
                    }

                    // delete the existing status for shipment details.
                    var queryDelete = @"delete from crm.ShipmentStatus
                                        where ShipmentDetailID in (select ShipmentDetailID 
                                            from crm.ShipmentDetail 
	                                        where ShipNoticeId = @shipnoticeId)";
                    connection.Execute(queryDelete, new { shipnoticeId = model.ShipNoticeId }
                    , transaction);

                    var queryStausInsert = @"insert into crm.ShipmentStatus 
                                    values( @shipmentDetailid, @shipmentStatusId)";
                    //insert the newly selected status for each shipment details.
                    foreach (var item in model.ListShipmentDetails)
                    {
                        foreach (var status in item.Statuses)
                        {
                            connection.Execute(queryStausInsert
                                , new
                                {
                                    shipmentDetailid = item.ShipmentDetailId
                                    ,
                                    shipmentStatusId = status
                                }, transaction);
                        }
                    }

                    transaction.Commit();

                    if (model.ReceivedComplete && oldShipNotice.ReceivedComplete == false)
                    {
                        UpdateSOStatus(model.PurchaseOrderId);
                    }
                    // Old is recceived completed and new is not, so we need to update the status..
                    else if (oldShipNotice.ReceivedComplete && model.ReceivedComplete == false)
                    {
                        UpdateSOStatus(model.PurchaseOrderId);
                    }
                    return true;
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    EventLogger.LogEvent(e);
                    throw e;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        private void HandleChangeTracking(ShipNotice modelOld, ShipNotice model
            , SqlConnection connection, SqlTransaction transaction)
        {
            var tableCode = "ShipNotice";
            if (modelOld.RemakeReference != model.RemakeReference)
            {
                var ch = new CommonChangeHistory(tableCode, DateTime.UtcNow
                    , this.User.PersonId, model.ShipNoticeId)
                {
                    ChangedFieldName = "Remake Reference",
                    ChangedValue = model.RemakeReference
                };

                connection.Insert<CommonChangeHistory>(ch, transaction);
            }

            if (modelOld.CheckedinDate != model.CheckedinDate
                && model.CheckedinDate.HasValue)
            {
                var ch = new CommonChangeHistory(tableCode, DateTime.UtcNow
                    , this.User.PersonId, model.ShipNoticeId)
                {
                    ChangedFieldName = "Checkedin Date",
                    ChangedValue = model.CheckedinDate.ToString()
                };

                connection.Insert<CommonChangeHistory>(ch, transaction);
            }

            foreach (var item in model.ListShipmentDetails)
            {
                var itemOld = modelOld.ListShipmentDetails
                    .Where(x => x.ShipmentDetailId == item.ShipmentDetailId)
                    .FirstOrDefault();
                if (item == null) continue;

                if (itemOld.PackingSlip != item.PackingSlip)
                {
                    var ch = new CommonChangeHistory(tableCode, DateTime.UtcNow
                    , this.User.PersonId, model.ShipNoticeId
                    , item.ShipmentDetailId)
                    {
                        ChangedFieldName = "Packing Slip",
                        ChangedValue = item.PackingSlip.ToString()
                    };

                    connection.Insert<CommonChangeHistory>(ch, transaction);
                }

                if (itemOld.QuantityReceived != item.QuantityReceived)
                {
                    var ch = new CommonChangeHistory(tableCode, DateTime.UtcNow
                    , this.User.PersonId, model.ShipNoticeId
                    , item.ShipmentDetailId)
                    {
                        ChangedFieldName = "Quantity Received",
                        ChangedValue = item.QuantityReceived.ToString()
                    };

                    connection.Insert<CommonChangeHistory>(ch, transaction);
                }

                if (itemOld.QuantityDamaged != item.QuantityDamaged)
                {
                    var ch = new CommonChangeHistory(tableCode, DateTime.UtcNow
                    , this.User.PersonId, model.ShipNoticeId
                    , item.ShipmentDetailId)
                    {
                        ChangedFieldName = "Quantity Damaged",
                        ChangedValue = item.QuantityDamaged.ToString()
                    };

                    connection.Insert<CommonChangeHistory>(ch, transaction);
                }

                if (itemOld.QuantityShortShipped != item.QuantityShortShipped)
                {
                    var ch = new CommonChangeHistory(tableCode, DateTime.UtcNow
                    , this.User.PersonId, model.ShipNoticeId
                    , item.ShipmentDetailId)
                    {
                        ChangedFieldName = "Quantity Short Shipped",
                        ChangedValue = item.QuantityShortShipped.ToString()
                    };

                    connection.Insert<CommonChangeHistory>(ch, transaction);
                }

                if (itemOld.QuantityMisShipped != item.QuantityMisShipped)
                {
                    var ch = new CommonChangeHistory(tableCode, DateTime.UtcNow
                    , this.User.PersonId, model.ShipNoticeId
                    , item.ShipmentDetailId)
                    {
                        ChangedFieldName = "Quantity Mis-Shipped",
                        ChangedValue = item.QuantityMisShipped.ToString()
                    };

                    connection.Insert<CommonChangeHistory>(ch, transaction);
                }

                // TODO : not sure we need tracking for notess...

                // TODO : Need to do tracking for Status..
                var oldStatus = string.Join(",", itemOld.Statuses.OrderBy(x => x));
                var newStatus = string.Join(",", item.Statuses.OrderBy(x => x));
                if (oldStatus != newStatus)
                {
                    var ch = new CommonChangeHistory(tableCode, DateTime.UtcNow
                    , this.User.PersonId, model.ShipNoticeId
                    , item.ShipmentDetailId)
                    {
                        ChangedFieldName = "Status",
                        ChangedValue = newStatus
                    };

                    connection.Insert<CommonChangeHistory>(ch, transaction);
                }
            }
        }

        public bool UpdateShipNoticeDetail(int ShipNoticeId, ShipmentDetail model)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    // ShipNotice sNotice = new ShipNotice();

                    connection.Open();
                    model.UpdatedBy = SessionManager.CurrentUser.PersonId;
                    model.UpdatedOn = DateTime.UtcNow;
                    connection.Update<ShipmentDetail>(model);

                    return true;
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    throw e;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool UpdateSOStatus(int purchaseOrderId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    // ShipNotice sNotice = new ShipNotice();

                    var query = @"SELECT pod.PurchaseOrdersDetailId,pod.PurchaseOrderId,pod.QuoteLineId,pod.Status,pod.PICPO
		                            ,sd.Status ShipNoticeStatus
                                    , sd.QuantityReceived, sd.QuantityShipped, ss.ShipmentStatusId
		                            FROM crm.MasterPurchaseOrder mpo
		                            INNER JOIN crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
		                            INNER JOIN crm.Accounts a ON a.AccountId = mpox.AccountId
		                            INNER JOIN crm.PurchaseOrderDetails pod ON pod.PurchaseOrderId = mpo.PurchaseOrderId
		                            INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
		                            LEFT JOIN  crm.ShipmentDetail sd on pod.quotelineid=sd.quotelineid
                                    left join crm.ShipmentStatus ss on ss.ShipmentDetailID=sd.ShipmentDetailId and ss.ShipmentStatusId=1
		                            WHERE ql.ProductTypeId=1 and a.FranchiseId = @FranchiseId AND mpo.PurchaseOrderId = @PurchaseOrderId order by QuoteLineId asc;

		                            select o.* from crm.MasterPurchaseOrder mpo
                                    left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
		                            inner join crm.Orders o on o.OrderId = mpox.OrderID
		                            where mpo.PurchaseOrderId =@PurchaseOrderId;";
                    connection.Open();

                    var qDetails = connection.QueryMultiple(query, new { PurchaseOrderId = purchaseOrderId, FranchiseId = SessionManager.CurrentFranchise.FranchiseId });

                    var sNoticeStatus = qDetails.Read<dynamic>().ToList();
                    var order = qDetails.Read<Orders>().FirstOrDefault();

                    var allReceived = true;
                    var oneReceived = false;

                    foreach (var sn in sNoticeStatus)
                    {
                        if (sn.ShipmentStatusId == 1 &&
                            (sn.QuantityReceived == sn.QuantityShipped))
                        {
                            oneReceived = true;
                        }
                        else
                        {
                            allReceived = false;
                        }
                    }

                    if (order.OrderStatus.HasValue && (order.OrderStatus == 1 || order.OrderStatus == 2 ||
                                                         order.OrderStatus == 7 || order.OrderStatus == 8 || order.OrderStatus == 10 || order.OrderStatus == 3))
                    {
                        if (allReceived)
                        {
                            order.OrderStatus = 3;
                        }
                        else if (oneReceived)
                        {
                            order.OrderStatus = 10;
                        }

                        if (allReceived || oneReceived)
                            connection.Update<Orders>(order);
                    }

                    return true;
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    throw e;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public ShipNotice GetValue(int id, bool includeChangeTracking = true)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    // ShipNotice sNotice = new ShipNotice();

                    var query = @"	SELECT distinct ql.VendorName AS VendorName --(use shipnotice.vendorname itself),
                        , CASE when (ISNULL(q.SideMark,''))<>'' then q.sidemark  
	                        WHEN (isnull(o.sidemark,''))<>'' then o.sidemark 
	                        else q.quotename 
	                        End as SideMark
                        , (SELECT sum(sd.QuantityShipped) FROM crm.ShipmentDetail sd 
	                        WHERE sd.ShipNoticeId = @ShipNoticeId) AS TotalQuantity,
                        a.Address1++','+a.Address2+','+a.City+','+a.[State]+','+a.ZipCode+','+a.CountryCode2Digits AS ShippedTo
                        , mpo.ShipToLocation, mpo.MasterPONumber,f.Name AS ShiptoAccount,mpo.ShipAddressId
                        , (select count( distinct boxid) from crm.ShipmentDetail where ShipNoticeId = @ShipNoticeId ) as TotalBoxesTP
                        , (SELECT sum(sd.QuantityReceived) FROM crm.ShipmentDetail sd 
	                        WHERE sd.ShipNoticeId = @ShipNoticeId) AS TotalQuantityReceived
                        , (SELECT sum(sd.QuantityShipped) FROM crm.ShipmentDetail sd 
	                        WHERE sd.ShipNoticeId = @ShipNoticeId) AS TotalQuantityShipped
                        , (SELECT sum(sd.QuantityShortShipped) FROM crm.ShipmentDetail sd 
	                        WHERE sd.ShipNoticeId = @ShipNoticeId) AS TotalQuantityShortShipped
                        , (SELECT sum(sd.QuantityMisShipped) FROM crm.ShipmentDetail sd 
	                        WHERE sd.ShipNoticeId = @ShipNoticeId) AS TotalQuantityMisShipped
                        , (SELECT sum(sd.QuantityDamaged) FROM crm.ShipmentDetail sd 
	                        WHERE sd.ShipNoticeId = @ShipNoticeId) AS TotalQuantityDamaged
                        , sr.TouchpointShippedVia ShippedViaTP, sr.TouchPointRouting Routing, sn.ShipNoticeId
                        , sn.ShipmentId, sn.ShippedDate, sn.EstDeliveryDate
                        , sn.ShippedVia, sn.BOL, sn.Weight
                        , sn.TotalBoxes, sn.PurchaseOrderId, sn.PurchaseOrdersDetailId
                        , sn.VendorName, sn.ReceivedComplete, sn.ReceivedCompleteDate
                        , sn.ShipNoticeJson, sn.CreatedBy, sn.CreatedOn
                        , sn.UpdatedBy, sn.UpdatedOn, sn.PICVpo
                        , sn.TrackingURL, sn.TrackingNumber, sn.CheckedinDate
                        , sn.RemakeReference 
                        , u.UserName UpdatedByUserName
                        , sn.ShippedViaText
                        FROM crm.ShipNotice sn
                        INNER JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = sn.PurchaseOrderId
                        left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                        INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
                        INNER JOIN crm.Quote q ON q.QuoteKey = mpox.QuoteId
                      INNER JOIN crm.PurchaseOrderDetails pod ON pod.PICPO = sn.PICVpo
                      INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
                        INNER JOIN crm.Franchise f ON f.FranchiseId = o.FranchiseId
                        LEFT JOIN crm.Addresses a ON mpo.ShipAddressId = a.AddressId
                        LEFT JOIN crm.Type_ShipmentShippedViaRouting sr on sr.JsonShippedVia = sn.ShippedVia
                        INNER JOIN auth.Users u on u.PersonId = sn.UpdatedBy
                        WHERE o.FranchiseId = @FranchiseId AND sn.ShipNoticeId = @ShipNoticeId;

                        --https://blog.sqlauthority.com/2015/05/05/sql-server-generating-row-number-without-ordering-any-columns/
                        SELECT ROW_NUMBER() OVER (Order by (select 'a')) as SNO
                        ,  sn.PICVpo as PICVpo, ql.ProductName +' - '+ql.[Description] AS ItemDescription 
                        , case when ISNULL(q.sidemark, '') <> '' then q.SideMark
	                        when ISNULL(o.SideMark, '') <> '' then o.SideMark
	                        else q.QuoteName 
	                        end as SideMark
                        , ord.OrderNumber -- For sidemark manipulation....
                        , mpox1.OrderId, ql.QuoteLineNumber LineNumber
                        , sd.ShipmentDetailId, sd.ShipNoticeId, sd.ShipmentId
                        , sd.POItem, sd.ProductNumber, sd.QuantityShipped
                        , sd.BoxId, sd.TrackingId, sd.Status
                        , sd.TrackingUrl
                        , sd.CreatedOn, sd.CreatedBy, sd.UpdatedOn
                        , sd.UpdatedBy, sd.QuoteLineId, sd.QuantityReceived
                        , sd.Notes, sd.QuantityShortShipped, sd.QuantityMisShipped
                        , sd.QuantityDamaged, sd.UomCode, sd.PackingSlip
                        , uom.UomDefinition
                        FROM crm.ShipmentDetail sd
	                        INNER JOIN crm.ShipNotice sn ON sn.ShipNoticeId = sd.ShipNoticeId
	                        INNER JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = sn.PurchaseOrderId
	                        left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
	                        INNER JOIN crm.Accounts a ON a.AccountId = mpox.AccountId
	                        left JOIN crm.QuoteLines ql ON ql.QuoteLineId = sd.QuoteLineId
	                        inner join crm.PurchaseOrderDetails pod on pod.QuoteLineId = sd.QuoteLineId and pod.StatusId <> 8
	                        inner join crm.MasterPurchaseOrderExtn mpox1 on mpox1.PurchaseOrderId = pod.PurchaseOrderId
	                        inner join crm.Quote q on q.QuoteKey = mpox1.QuoteId
	                        inner join crm.Opportunities o on o.OpportunityId = mpox1.OpportunityId
	                        inner join crm.Orders ord on ord.OrderID = mpox1.OrderId
	                        left join crm.Type_ShipmentUom  uom on uom.uomcode = sd.uomcode
                        WHERE a.FranchiseId = @FranchiseId AND sn.ShipNoticeId = @ShipNoticeId;

                        select ss.ShipmentDetailId, ss.ShipmentStatusId  
                        from crm.shipmentstatus ss
                        inner join crm.ShipmentDetail sd on sd.ShipmentDetailId = ss.ShipmentDetailId
                        where sd.ShipNoticeId = @ShipNoticeId;

                        select ShipmentStatusId [Key] , name Value from crm.Type_ShipmentStatus
                        where IsActive = 1;
                        ";
                    connection.Open();

                    var qDetails = connection.QueryMultiple(query, new { ShipNoticeId = id, FranchiseId = SessionManager.CurrentFranchise.FranchiseId });

                    var sNotice = qDetails.Read<ShipNotice>().FirstOrDefault();
                    var listSD = qDetails.Read<ShipmentDetail>().ToList();
                    var listStatus = qDetails.Read<ShipmentStatus>().ToList();
                    var shipmentStatuses = qDetails.Read<KeyValuePair<int, string>>().ToList();

                    foreach (var item in listSD)
                    {
                        var temp = listStatus.Where(x => x.ShipmentDetailId == item.ShipmentDetailId)
                                             .ToList();

                        item.Statuses = temp.Select(x => x.ShipmentStatusId).ToArray();
                    }

                    var dCount = listStatus.Where(x => x.ShipmentStatusId == 3).ToList().Count();
                    var ssCount = listStatus.Where(x => x.ShipmentStatusId == 5).ToList().Count();
                    var msCount = listStatus.Where(x => x.ShipmentStatusId == 4).ToList().Count();
                    if (dCount >= 1)
                    {
                        sNotice.Status = shipmentStatuses.Where(x => x.Key == 3)
                            .FirstOrDefault().Value;   //"Damaged";
                    }
                    else if (ssCount >= 1)
                    {
                        sNotice.Status = shipmentStatuses.Where(x => x.Key == 5)
                            .FirstOrDefault().Value; //"Short Shipped";
                    }
                    else if (msCount >= 1)
                    {
                        sNotice.Status = shipmentStatuses.Where(x => x.Key == 4)
                            .FirstOrDefault().Value; //"Mis-Shipped";
                    }
                    else
                    {
                        if (sNotice.TotalQuantityShipped == sNotice.TotalQuantityReceived)
                        {
                            sNotice.Status = shipmentStatuses.Where(x => x.Key == 7)
                            .FirstOrDefault().Value; //"Received in Full";
                        }
                        else
                        {
                            sNotice.Status = shipmentStatuses.Where(x => x.Key == 8)
                                    .FirstOrDefault().Value; //"Open";
                        }
                    }

                    sNotice.ListShipmentDetails = listSD;

                    //Append the change tracking for autit trial
                    if (includeChangeTracking)
                    {
                        sNotice.AuditTrial = new List<string>();
                        var changes = GetChangeTracking(id);
                        foreach (var item in changes)
                        {
                            var line = "";
                            if (item.SecondaryTablePrimaryKey != null)
                            {
                                var sno = listSD.Where(x => x.ShipmentDetailId == item.SecondaryTablePrimaryKey)
                                    .Select(y => y.SNO).FirstOrDefault();
                                line = "Line " + sno + ": ";
                            }

                            if (item.ChangedFieldName.ToLower() == "status")
                            {
                                var statusValue = "";
                                // Status stored as comma separated values for simplicity...
                                // we need to get all the status description from the master.

                                if (!string.IsNullOrEmpty(item.ChangedValue))
                                {
                                    var itemStatuses = item.ChangedValue.Split(',');
                                    foreach (var itemStatus in itemStatuses)
                                    {
                                        var statusid = int.Parse(itemStatus);
                                        var typess = shipmentStatuses
                                            .Where(x => x.Key == statusid)
                                            .FirstOrDefault();

                                        statusValue += string.IsNullOrEmpty(statusValue) ?
                                            typess.Value
                                            : ", " + typess.Value;
                                    }
                                }

                                var text = "Updated: " + item.CreatedOn.ToString("")
                                + " By: " + item.PersonName + " - "
                                + line + " "
                                + item.ChangedFieldName + " " + statusValue;
                                sNotice.AuditTrial.Add(text);
                            }
                            else
                            {
                                var text = "Updated: " + item.CreatedOn.ToString("")
                                + " By: " + item.PersonName + " - "
                                + line + " "
                                + item.ChangedFieldName + " " + item.ChangedValue;
                                sNotice.AuditTrial.Add(text);
                            }


                        }

                    }



                    sNotice.CheckedinDate = TimeZoneManager.ToLocal(sNotice.CheckedinDate);

                    var sdTracking = listSD.Select(x => new KeyValuePair<string, string>(x.TrackingId, x.TrackingUrl))
                        .Distinct().Where(x =>  ! (string.IsNullOrEmpty(x.Key)
                                    && string.IsNullOrEmpty(x.Value) )).ToList();

                    if (sdTracking.Where(x => x.Key == sNotice.TrackingNumber).Count() == 0)
                    {
                        if (! (string.IsNullOrEmpty(sNotice.TrackingNumber)
                               && string.IsNullOrEmpty(sNotice.TrackingURL)))
                            sdTracking.Add(new KeyValuePair<string, string>(sNotice.TrackingNumber, sNotice.TrackingURL));
                    }

                    sNotice.TrackingList = sdTracking;

                    if (string.IsNullOrEmpty(sNotice.ShippedViaTP))
                        sNotice.ShippedViaTP = sNotice.ShippedViaText;


                    return sNotice;
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        private List<CommonChangeHistory> GetChangeTracking(int shipNoticeId)
        {
            try
            {

                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    var query = @"Select TableCode, PrimaryTablePrimaryKey, SecondaryTablePrimaryKey
                                        , changedFieldName, ChangedValue, CreatedOn, CreatedBy
                                    	, p.FirstName + ' ' + p.LastName PersonName
                                    From crm.CommonChangeHistory ch
                                    Join crm.Person p on p.PersonId = ch.CreatedBy 
                                    where TableCode = @tableCode 
                                    and PrimaryTablePrimaryKey = @shipNoticeId
                                    order by CreatedOn desc";
                    var result = connection.Query<CommonChangeHistory>(query
                        , new
                        {
                            tableCode = "ShipNotice",
                            shipNoticeId = shipNoticeId
                        }).ToList();

                    return result;
                }
            }
            catch (Exception e)
            {
                EventLogger.LogEvent(e);
                throw;
            }
        }

        public bool UpdateShipNotice()
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    var usr = AdminPersonId; // Admin account
                    if (SessionManager.CurrentFranchise != null)
                    {
                        usr = SessionManager.CurrentUser.PersonId;
                    }

                    var dateTime = DateTime.UtcNow;

                    var podListQuery = @" select distinct * from (SELECT pod.PICPO FROM crm.PurchaseOrderDetails pod
                                         WHERE pod.PICPO IS NOT NULL  AND pod.StatusId IN (3,4,5,11) AND pod.picpo NOT IN (select PICVpo from crm.shipnotice)
                                         UNION ALL
                                         SELECT pod.PICPO  FROM crm.PurchaseOrderDetails pod
                                         WHERE pod.PICPO IS NOT NULL AND pod.StatusId IN (3,5)
                                         AND pod.PICPO not in (select PICVpo from crm.shipnotice)
                                         UNION ALL
                                         SELECT pod.PICPO  FROM crm.PurchaseOrderDetails pod
                                         WHERE pod.StatusId not IN (3,4,5) AND pod.PICPO IN (select PICVpo from crm.shipnotice)
										 UNION ALL
										 SELECT DISTINCT pod.PICPO FROM crm.PurchaseOrderDetails pod
                                         WHERE pod.PICPO IS NOT NULL  AND pod.StatusId IN (4)
										 ) AS X";

                    var podQuery = @"SELECT * FROM crm.PurchaseOrderDetails pod
                                     INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
                                     INNER JOIN crm.QuoteLineDetail qld ON qld.QuoteLineId = ql.QuoteLineId
                                     WHERE pod.PICPO=@PICPO";

                    //                    var query = @"SELECT DISTINCT top 2000 * FROM (
                    //SELECT distinct pod.* FROM crm.PurchaseOrderDetails pod
                    //LEFT JOIN crm.ShipNotice sn ON sn.PICVpo=pod.PICPO
                    //WHERE pod.PICPO IS NOT NULL  AND pod.StatusId IN (3,4,5,11)
                    //AND sn.ShipNoticeId IS NULL
                    //                              UNION ALL
                    //                              SELECT distinct pod1.* FROM crm.PurchaseOrderDetails pod1
                    //LEFT JOIN crm.shipnotice sn1 ON sn1.PICVpo=pod1.PICPO
                    //WHERE pod1.StatusId IN (1,2,6,9,11) AND sn1.ShipNoticeId IS NOT null) AS x
                    //ORDER BY x.PurchaseOrdersDetailId DESC";
                    //var query = @"	SELECT * FROM crm.PurchaseOrderDetails pod
                    //                     INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
                    //                     INNER JOIN crm.QuoteLineDetail qld ON ql.QuoteLineId = qld.QuoteLineId
                    //                     WHERE pod.PICPO IS NOT NULL  AND pod.StatusId IN (3,4,5,11) AND pod.picpo NOT IN (select PICVpo from crm.shipnotice)
                    //                     UNION ALL
                    //                     SELECT *FROM crm.PurchaseOrderDetails pod
                    //                     INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
                    //                     INNER JOIN crm.QuoteLineDetail qld ON ql.QuoteLineId = qld.QuoteLineId
                    //                     WHERE pod.PICPO IS NOT NULL AND pod.StatusId IN (3,5)
                    //                     AND pod.PICPO not in (select PICVpo from crm.shipnotice)
                    //                     UNION ALL
                    //                     SELECT * FROM crm.PurchaseOrderDetails pod
                    //                     INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
                    //                     INNER JOIN crm.QuoteLineDetail qld ON ql.QuoteLineId = qld.QuoteLineId
                    //                     WHERE pod.StatusId not IN (3,4,5) AND pod.PICPO IN (select PICVpo from crm.shipnotice);";

                    var fQuery = @"SELECT * FROM crm.ShipNotice sn
                                   WHERE sn.PurchaseOrdersDetailId =@PurchaseOrdersDetailId;
                                   SELECT * FROM crm.ShipmentDetail sd
                                   INNER JOIN crm.ShipNotice sn ON sn.ShipNoticeId = sd.ShipNoticeId
                                   WHERE sn.PurchaseOrdersDetailId =@PurchaseOrdersDetailId;";

                    var sDetailsQuery = @"SELECT sn.* FROM crm.ShipNotice sn
                                   WHERE sn.PICVpo =@PICVpo;
                                   SELECT sd.* FROM crm.ShipmentDetail sd
                                   INNER JOIN crm.ShipNotice sn ON sn.ShipNoticeId = sd.ShipNoticeId
                                   WHERE sn.PICVpo =@PICVpo;";

                    var listsnQuery = @"SELECT * FROM crm.ShipNotice sn
                                   WHERE sn.PICVpo  in @PICVpo;
                                   SELECT * FROM crm.ShipmentDetail sd
                                   INNER JOIN crm.ShipNotice sn ON sn.ShipNoticeId = sd.ShipNoticeId
                                   WHERE sn.PICVpo  in @PICVpo;";

                    connection.Open();

                    //SqlDataAdapter adapter = new SqlDataAdapter(query, connection);

                    //DataSet customers = new DataSet();
                    //adapter.Fill(customers, "crm.PurchaseOrderDetails");

                    //EventLogger.LogEvent("Start UpdateShipNotice for vpo", "ShipNoticeManager", LogSeverity.Information);

                    //var list = new List<PurchaseOrderDetails>();
                    //foreach (DataRow pRow in customers.Tables["crm.PurchaseOrderDetails"].Rows)
                    //{
                    //    PurchaseOrderDetails pod = new PurchaseOrderDetails();

                    //    //pod.PurchaseOrderId = pRow["PurchaseOrderId"];
                    //    //Console.WriteLine(pRow["CustomerID"]);
                    //    //foreach (DataRow cRow in pRow.GetChildRows(custOrderRel))
                    //    //    Console.WriteLine("\t" + cRow["OrderID"]);
                    //}

                    var list = connection.Query<int>(podListQuery).ToList();
                    //var listPurchaseOrderDetails = connection.Query<PurchaseOrderDetails>(query).Distinct().ToList();

                    //var listPurchaseOrderDetails = connection
                    //    .Query<PurchaseOrderDetails, QuoteLines, QuoteLineDetail, PurchaseOrderDetails>(
                    //        query,
                    //        (PurchaseOrderDetails, QuoteLines, QuoteLineDetail) =>
                    //        {
                    //            PurchaseOrderDetails.QuoteLines = QuoteLines;
                    //            PurchaseOrderDetails.QuoteLineDetail = QuoteLineDetail;
                    //            return PurchaseOrderDetails;
                    //        },
                    //        splitOn: "QuoteLineId")
                    //    .Distinct()
                    //    .ToList();

                    //var listPicPO = listPurchaseOrderDetails.Where(x => x.PICPO != null && x.PICPO != 0)
                    //    .Select(x => x.PICPO).Distinct().ToList();

                    //var listPicPOstr = listPurchaseOrderDetails.Where(x => x.PICPO != null && x.PICPO != 0)
                    //   .Select(x => x.PICPO.ToString()).Distinct().ToArray();

                    //EventLogger.LogEvent("before for loop UpdateShipNotice for vpo", "ShipNoticeManager", LogSeverity.Information);
                    //go through all the items of VPO with status Fully shipped or partially shipped
                    foreach (var vpo in list)
                    {
                        //EventLogger.LogEvent("Start UpdateShipNotice for vpo" + vpo, "ShipNoticeManager", LogSeverity.Information);
                        try
                        {
                            var itemList = connection
                                .Query<PurchaseOrderDetails, QuoteLines, QuoteLineDetail, PurchaseOrderDetails>(
                                    podQuery,
                                    (PurchaseOrderDetails, QuoteLines, QuoteLineDetail) =>
                                    {
                                        PurchaseOrderDetails.QuoteLines = QuoteLines;
                                        PurchaseOrderDetails.QuoteLineDetail = QuoteLineDetail;
                                        return PurchaseOrderDetails;
                                    }, new { PICPO = vpo },
                                    splitOn: "QuoteLineId")
                                .Distinct()
                                .ToList();

                            var sndetails = connection.QueryMultiple(sDetailsQuery,
                                 new { PICVpo = vpo });

                            var listSN = sndetails.Read<ShipNotice>().ToList();
                            var listSD = sndetails.Read<ShipmentDetail>().ToList();

                            //var itemList = listPurchaseOrderDetails.Where(x => x.PICPO == vpo).ToList();
                            //go only if it has PIC VPO
                            //if (item.PICPO.HasValue)
                            //{
                            //call PIc to get Ship notice details based on VPO
                            var snjson = listSN.Where(x => x.PICVpo == vpo.ToString()).FirstOrDefault();

                            var snrespJson = snjson != null ? snjson.ShipNoticeJson : null;// alllistSN.Where(x => x.PICVpo == vpo.Value.ToString()).FirstOrDefault().ShipNoticeJson;

                            //var listSN = alllistSN.Where(x => x.PICVpo == vpo.ToString()).ToList();
                            //var listSD = alllistSD.Where(x => x.PICVpo == vpo).ToList();

                            var response = picMgr.GetShippingNotice(vpo, snrespJson);
                            var result = JsonConvert.DeserializeObject<SNRootObject>(response);

                            if (result.valid)
                            {
                                AddUpdateShipNotice(response, itemList, listSN, dateTime, usr, vpo);
                                if (itemList.FirstOrDefault().StatusId != (int)PurchaseOrderStatus.Shipped &&  itemList.FirstOrDefault().StatusId != (int)PurchaseOrderStatus.Invoiced)
                                {
                                    CheckASNStatus(vpo);
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            EventLogger.LogEvent(e, "ShipNotice");
                            continue;
                        }
                    }

                    connection.Close();
                    return true;
                }
            }
            catch (Exception ex1)
            {
                EventLogger.LogEvent(ex1,"ShipNotice");
                return true;
            }
        }

        /// <summary>
        /// Check if ASN is fully shipped or partial Shipped
        /// </summary>
        /// <returns></returns>
        public bool CheckASNStatus(int vpo)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"SELECT * FROM crm.PurchaseOrderDetails pod WHERE pod.PICPO=@PICVpo;
                              SELECT sn.* FROM crm.shipnotice sn WHERE sn.PICVpo=@PICVpo;
                              SELECT sd.* FROM crm.shipnotice sn inner join crm.ShipmentDetail sd on sd.ShipmentId = sn.ShipmentId WHERE sn.PICVpo=@PICVpo;
                              SELECT vi.* FROM crm.VendorInvoice vi WHERE vi.PICVpo=@PICVpo;
                              SELECT vid.* FROM crm.VendorInvoiceDetail vid INNER JOIN crm.VendorInvoice vi ON vi.VendorInvoiceId = vid.VendorInvoiceId  WHERE vi.PICVpo=@PICVpo;";

                var data = connection.QueryMultiple(query,
                            new { PICVpo = vpo });

                try
                {
                    if (vpo > 0)
                    {
                        var poDetails = data.Read<PurchaseOrderDetails>().ToList();
                        var alllistSN = data.Read<ShipNotice>().ToList();
                        var alllistSD = data.Read<ShipmentDetail>().ToList();
                        var vInvoice = data.Read<VendorInvoice>().ToList();
                        var vIDetail = data.Read<VendorInvoiceDetail>().ToList();

                        var status = 0;
                        var vpoQTY = poDetails.Sum(x => x.QuantityPurchased);
                        var vpoCount = poDetails.Count;
                        if (alllistSN != null && alllistSD != null && alllistSN.Count > 0 && alllistSD.Count > 0)
                        {
                            var sdCount = alllistSD.Select(x => x.QuoteLineId).Distinct().Count();
                            var sdQTY = alllistSD.Sum(x => x.QuantityShipped);
                            if (vpoCount == sdCount && vpoQTY <= sdQTY)
                            {
                                status = (int)PurchaseOrderStatus.Shipped;
                            }
                            else
                            {
                                status = (int)PurchaseOrderStatus.Partial_Shipped_Back_Ordered;
                            }
                        }
                        if (vInvoice != null && vIDetail != null && vInvoice.Count > 0 && vIDetail.Count > 0)
                        {
                            if (status == (int)PurchaseOrderStatus.Partial_Shipped_Back_Ordered)
                            {
                                status = (int)PurchaseOrderStatus.Invoiced;
                            }
                        }
                        if (status != 0)
                        {
                            if (poDetails.FirstOrDefault().StatusId != status)
                            {
                                foreach (var item in poDetails)
                                {
                                    item.Status = Enum.GetName(typeof(PurchaseOrderStatus), status);
                                    item.StatusId = status;
                                    connection.Update(item);
                                }
                                _poMgr.SetMasterPurchaseOrderStatus(poDetails.FirstOrDefault().PurchaseOrderId);
                            }
                        }
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    return false;
                }
                finally
                {
                    connection.Close();
                }
                return true;
            }
        }

        /// <summary>
        /// Adding the code in Purchase order manager as well
        /// </summary>
        /// <param name="response"></param>
        /// <param name="itemList"></param>
        /// <param name="listSN"></param>
        /// <param name="dateTime"></param>
        /// <param name="usr"></param>
        /// <param name="vpo"></param>
        /// <returns></returns>
        public bool AddUpdateShipNotice(string response, List<PurchaseOrderDetails> itemList, List<ShipNotice> listSN, DateTime dateTime, int usr, int vpo = 0)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                try
                {
                    var result = JsonConvert.DeserializeObject<SNRootObject>(response);
                    foreach (var picSNItem in result.properties)
                    {
                        if (listSN != null && listSN.Count > 0 && listSN.Where(x => x.ShipmentId == picSNItem.ShipNotice.shipmentNum).FirstOrDefault() != null)
                        {
                            var sn_Item = listSN.Where(x => x.ShipmentId == picSNItem.ShipNotice.shipmentNum).FirstOrDefault();

                            foreach (var item in itemList)
                            {
                                if (picSNItem.ShipNotice.shippedDate != null &&
                                    picSNItem.ShipNotice.shippedDate != "0000-00-00")
                                {
                                    item.ShipDate =
                                        Convert.ToDateTime(picSNItem.ShipNotice.shippedDate);
                                }

                                item.UpdatedOn = dateTime;
                                item.UpdatedBy = usr;
                                connection.Update(item);
                            }
                        }
                        else
                        {
                            var PICSN = new ShipNotice();
                            var poItem = itemList.FirstOrDefault();
                            if (poItem != null)
                            {
                                PICSN.ShipmentId = String.IsNullOrEmpty(picSNItem.ShipNotice.shipmentNum) ? poItem.VendorReference : picSNItem.ShipNotice.shipmentNum;
                                PICSN.PurchaseOrderId = poItem.PurchaseOrderId;
                            }
                            PICSN.PICVpo = vpo.ToString();
                            //PICSN.PurchaseOrdersDetailId = item.PurchaseOrdersDetailId;
                            PICSN.ShipNoticeJson = response.ToString();
                            PICSN.BOL = picSNItem.ShipNotice.bOL;
                            if (picSNItem.ShipNotice.shippedDate != null &&
                                picSNItem.ShipNotice.shippedDate != "0000-00-00")
                            {
                                PICSN.ShippedDate =
                                    Convert.ToDateTime(picSNItem.ShipNotice.shippedDate);
                            }

                            PICSN.Weight = picSNItem.ShipNotice.weight.ToString();
                            PICSN.ShippedVia = picSNItem.ShipNotice.shipVia;
                            PICSN.ShippedViaText = picSNItem.ShipNotice.shipViaName;

                            PICSN.VendorName = itemList.Where(x => x.PICPO == vpo).FirstOrDefault().QuoteLines.VendorName;// picSNItem.ShipNotice.vendor;
                            if (picSNItem.ShipNotice.deliveryDate != null &&
                                picSNItem.ShipNotice.deliveryDate != "0000-00-00")
                            {
                                PICSN.EstDeliveryDate =
                                    Convert.ToDateTime(picSNItem.ShipNotice.deliveryDate);
                            }

                            PICSN.CreatedOn = dateTime;
                            PICSN.CreatedBy = usr;
                            PICSN.UpdatedOn = dateTime;
                            PICSN.UpdatedBy = usr;

                            PICSN.TrackingNumber = picSNItem.ShipNotice.trackingNumber;
                            PICSN.TrackingURL = picSNItem.ShipNotice.trackingUrl;
                            PICSN.ShipNoticeId = connection.Insert<int, ShipNotice>(PICSN);

                            for (int i = 0; i < picSNItem.Items.Count(); i++)
                            {
                                var sditem = new ShipmentDetail();
                                sditem.ShipNoticeId = PICSN.ShipNoticeId;
                                sditem.ShipmentId = PICSN.ShipmentId;
                                sditem.POItem = picSNItem.Items[i].poItem.ToString();
                                sditem.QuantityShipped = Convert.ToInt32(picSNItem.Items[i].qty);
                                sditem.BoxId = picSNItem.Items[i].boxId;

                                sditem.UomCode = string.IsNullOrEmpty(picSNItem.Items[i].uom)
                                    ? "EA"
                                    : picSNItem.Items[i].uom;

                                sditem.TrackingId = picSNItem.Items[i].tracking;
                                sditem.TrackingUrl = picSNItem.Items[i].trackingUrl;

                                if (picSNItem.Items[i].ediCustomerItem.HasValue &&
                                    picSNItem.Items[i].ediCustomerItem != 0)
                                {
                                    sditem.QuoteLineId = picSNItem.Items[i].ediCustomerItem.Value;

                                    var po = itemList.Where(x => x.QuoteLineId == sditem.QuoteLineId)
                                        .FirstOrDefault();
                                    sditem.ProductNumber = po.PartNumber;
                                    sditem.ItemDescription = po.QuoteLines.ProductName;
                                }
                                else
                                {
                                    sditem.QuoteLineId = itemList[i].QuoteLineId;
                                    sditem.ProductNumber = itemList[i].PartNumber;
                                    sditem.ItemDescription = itemList[i].QuoteLines.ProductName;
                                }

                                sditem.CreatedOn = PICSN.CreatedOn;
                                sditem.CreatedBy = PICSN.CreatedBy;
                                sditem.UpdatedOn = PICSN.UpdatedOn;
                                sditem.UpdatedBy = PICSN.UpdatedBy;
                                var id = connection.Insert<int, ShipmentDetail>(sditem);
                            }

                            foreach (var item in itemList)
                            {
                                item.ShipDate = PICSN.ShippedDate;
                                item.UpdatedOn = dateTime;
                                item.UpdatedBy = usr;
                                connection.Update(item);
                            }
                        }

                        _vendorInvoiceManager.InsertOrUpdateVendorInvoiceByVPO(vpo);
                    }
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                }
                finally
                {
                    connection.Close();
                }
                return true;
            }
        }

        public bool OneTimeUpdateShipNotice()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                //var query = @"SELECT * FROM crm.ShipmentDetail sd WHERE sd.shipnoticeid=@shipnoticeid;";
                var poQuery = @"SELECT * FROM crm.PurchaseOrderDetails pod
                               INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
                               INNER JOIN crm.QuoteLineDetail qld ON ql.QuoteLineId = qld.QuoteLineId
                               WHERE  pod.PICPO=";
                connection.Open();
                var listSNotice = connection.GetList<ShipNotice>().ToList();
                try
                {
                    foreach (var PICSN in listSNotice)
                    {
                        //var listPoDetails = connection.Query<PurchaseOrderDetails>(poQuery, new { PICPO = PICSN.PICVpo })
                        //    .ToList().OrderBy(x => x.PurchaseOrdersDetailId).ToList();
                        var upQuery = poQuery + "'" + PICSN.PICVpo + "'";
                        //poQuery = poQuery + "'" + PICSN.PICVpo + "'";
                        var listPoDetails = connection
                            .Query<PurchaseOrderDetails, QuoteLines, QuoteLineDetail, PurchaseOrderDetails>(
                                upQuery,
                                (PurchaseOrderDetails, QuoteLines, QuoteLineDetail) =>
                                {
                                    PurchaseOrderDetails.QuoteLines = QuoteLines;
                                    PurchaseOrderDetails.QuoteLineDetail = QuoteLineDetail;
                                    return PurchaseOrderDetails;
                                },
                                splitOn: "QuoteLineId")
                            .Distinct()
                            .ToList();

                        var SNResObj = JsonConvert.DeserializeObject<SNRootObject>(PICSN.ShipNoticeJson);

                        var obj = SNResObj.properties[0];

                        PICSN.TrackingURL = obj.ShipNotice.trackingUrl;
                        PICSN.TrackingNumber = obj.ShipNotice.trackingNumber;
                        connection.Update(PICSN);

                        for (int i = 0; i < obj.Items.Count; i++)
                        {
                            var sditem = new ShipmentDetail();
                            sditem.ShipNoticeId = PICSN.ShipNoticeId;
                            sditem.ShipmentId = PICSN.ShipmentId;
                            sditem.POItem = obj.Items[i].poItem.ToString();
                            sditem.QuantityShipped = Convert.ToInt32(obj.Items[i].qty);
                            sditem.BoxId = obj.Items[i].boxId;
                            sditem.TrackingId = obj.Items[i].tracking;

                            sditem.UomCode = string.IsNullOrEmpty(obj.Items[i].uom)
                                ? "EA"
                                : obj.Items[i].uom;
                            sditem.TrackingUrl = obj.Items[i].trackingUrl;


                            if (obj.Items[i].ediCustomerItem.HasValue && obj.Items[i].ediCustomerItem != 0)
                            {
                                sditem.QuoteLineId = obj.Items[i].ediCustomerItem.Value;

                                var po = listPoDetails.Where(x => x.QuoteLineId == sditem.QuoteLineId).FirstOrDefault();
                                sditem.ProductNumber = po.PartNumber;
                                sditem.ItemDescription = po.QuoteLines.ProductName;
                            }
                            else
                            {
                                sditem.QuoteLineId = listPoDetails[i].QuoteLineId;
                                sditem.ProductNumber = listPoDetails[i].PartNumber;
                                sditem.ItemDescription = listPoDetails[i].QuoteLines.ProductName;
                            }

                            sditem.CreatedOn = PICSN.CreatedOn;
                            sditem.CreatedBy = PICSN.CreatedBy;
                            sditem.UpdatedOn = PICSN.UpdatedOn;
                            sditem.UpdatedBy = PICSN.UpdatedBy;
                            var id = connection.Insert<int, ShipmentDetail>(sditem);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                try
                {
                    var listVendorInvoice = connection.GetList<VendorInvoice>().ToList();

                    foreach (var PICVi in listVendorInvoice)
                    {
                        //var listPoDetails = connection.Query<PurchaseOrderDetails>(poQuery, new { PICPO = PICSN.PICVpo })
                        //    .ToList().OrderBy(x => x.PurchaseOrdersDetailId).ToList();
                        var upQuery = poQuery + "'" + PICVi.PICVpo + "'";
                        //poQuery = poQuery + "'" + PICSN.PICVpo + "'";
                        var listPoDetails = connection
                            .Query<PurchaseOrderDetails, QuoteLines, QuoteLineDetail, PurchaseOrderDetails>(
                                upQuery,
                                (PurchaseOrderDetails, QuoteLines, QuoteLineDetail) =>
                                {
                                    PurchaseOrderDetails.QuoteLines = QuoteLines;
                                    PurchaseOrderDetails.QuoteLineDetail = QuoteLineDetail;
                                    return PurchaseOrderDetails;
                                },
                                splitOn: "QuoteLineId")
                            .Distinct()
                            .ToList();

                        var SNResObj = JsonConvert.DeserializeObject<RootObjectAP>(PICVi.PICInvoiceResponse);

                        var obj = SNResObj.properties[0];

                        for (int i = 0; i < obj.Items.Count; i++)
                        {
                            var vDItem = new VendorInvoiceDetail();

                            vDItem.VendorInvoiceId = PICVi.VendorInvoiceId;

                            vDItem.Quantity = Convert.ToInt32(obj.Items[i].quantity);
                            vDItem.Cost = obj.Items[i].unitPrice;
                            vDItem.Comment = obj.Items[i].comment;

                            vDItem.CreatedOn = PICVi.CreatedOn;
                            vDItem.CreatedBy = PICVi.CreatedBy;
                            vDItem.UpdatedBy = PICVi.UpdatedBy;
                            vDItem.UpdatedOn = PICVi.UpdatedOn;

                            if (obj.Items[i].ediCustomerItem.HasValue && obj.Items[i].ediCustomerItem != 0)
                            {
                                vDItem.QuoteLineId = obj.Items[i].ediCustomerItem.Value;

                                var po = listPoDetails.Where(x => x.QuoteLineId == vDItem.QuoteLineId).FirstOrDefault();
                                vDItem.ProductNumber = po.PartNumber;
                                vDItem.Name = po.QuoteLines.ProductName;
                                vDItem.POLineNumber = po.QuoteLines.QuoteLineNumber;
                            }
                            else
                            {
                                if (i <= listPoDetails.Count() - 1)
                                {
                                    vDItem.QuoteLineId = listPoDetails[i].QuoteLineId;
                                    vDItem.ProductNumber = listPoDetails[i].PartNumber;
                                    vDItem.Name = listPoDetails[i].QuoteLines.ProductName;
                                    vDItem.POLineNumber = listPoDetails[i].QuoteLines.QuoteLineNumber;
                                }
                                else
                                {
                                    vDItem.QuoteLineId = 0;
                                    vDItem.ProductNumber = "";
                                    vDItem.Name = "";
                                    vDItem.POLineNumber = null;
                                }
                            }

                            var id = connection.Insert<int, VendorInvoiceDetail>(vDItem);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            return true;
        }

        public override string Add(ShipNotice data)
        {
            throw new NotImplementedException();
        }

        public override string Delete(int id)
        {
            throw new NotImplementedException();
        }

        public override ShipNotice Get(int id)
        {
            throw new NotImplementedException();
        }

        public List<ShipNoticeDTO> Get(int vendorId, List<int> status, string date)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var query =
                        @"SELECT DISTINCT  sn.ShipNoticeId,mpo.PurchaseOrderId,o2.OrderID,ql.VendorId, ql.VendorName AS FullVendorName,o.SalesAgentId,o.InstallerId , CASE when (ISNULL(q.SideMark,''))<>'' then q.sidemark  WHEN (isnull(o.sidemark,''))<>'' then o.sidemark else q.quotename End as SideMark,
                          (SELECT sum(sd.QuantityShipped) FROM crm.ShipmentDetail sd WHERE sd.ShipNoticeId = sn.ShipNoticeId) AS TotalQuantityShipped,
                          pod.PICPO AS VPO, mpo.MasterPONumber,o2.OrderNumber,sn.ShipmentId,CONVERT(date, sn.ShippedDate) ShippedDate,
                          (SELECT sum(pod2.QuantityPurchased) FROM crm.PurchaseOrderDetails pod2 WHERE pod2.PICPO = sn.PICVpo ) AS POQuantity,
                          sn.ReceivedComplete,CONVERT(date, sn.ReceivedCompleteDate) ReceivedCompleteDate,(SELECT count(*) FROM crm.PurchaseOrderDetails pod3 WHERE pod3.PICPO = sn.PICVpo )  POLines,
                          CASE WHEN sn.ReceivedComplete=1 THEN 'Received Complete'
                          WHEN  EXISTS(SELECT * FROM crm.ShipmentDetail sd WHERE sd.Status<>0 AND sd.ShipNoticeId = sn.ShipNoticeId ) then 'Received w/ Exceptions' else 'Open' END as Status,
                          CASE WHEN sn.ReceivedComplete=1 THEN 2
                          WHEN  EXISTS(SELECT * FROM crm.ShipmentDetail sd WHERE sd.Status<>0 AND sd.ShipNoticeId = sn.ShipNoticeId) then 3 else 1 END as StatusId,
						  (SELECT SUM(sd1.QuantityReceived) FROM crm.ShipmentDetail sd1 WHERE sd1.ShipNoticeId = sn.ShipNoticeId) as QuantityReceived
                          FROM crm.ShipNotice sn
	                      INNER JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = sn.PurchaseOrderId
                            left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
						  INNER JOIN crm.Orders o2 ON o2.OrderID = mpox.OrderId
	                      INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
						  INNER JOIN crm.Quote q ON q.QuoteKey = mpox.QuoteId
						  INNER JOIN crm.PurchaseOrderDetails pod ON pod.PICPO = sn.PICVpo
						  INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
						  INNER JOIN crm.Franchise f ON f.FranchiseId = o.FranchiseId
						  LEFT JOIN crm.Addresses a ON mpo.ShipAddressId = a.AddressId
                          WHERE 1=1 ";
                    if (vendorId != 0)
                    {
                        query = query + "AND ql.VendorId = " + vendorId;
                    }
                    if (SessionManager.CurrentFranchise.FranchiseId != 0)
                    {
                        query = query + " " + "AND f.FranchiseId = " + SessionManager.CurrentFranchise.FranchiseId;
                    }

                    //var user = LocalMembership.GetUser(User.PersonId, "Person", "Roles", "OAuthUsers", "UserWidgets");
                    //if (user.IsInRole(AppConfigManager.DefaultSalesRole.RoleId) || user.IsInRole(AppConfigManager.DefaultSalesNoMargineRole.RoleId))
                    //{
                    //    query = query + " and o.SalesAgentId = " + SessionManager.CurrentUser.PersonId;
                    //}
                    //else if (user.IsInRole(AppConfigManager.DefaultInstallRole.RoleId))
                    //{
                    //    query = query + " and o.InstallerId = " + SessionManager.CurrentUser.PersonId;
                    //}

                    switch (date)
                    {
                        case "4000": //Today
                            query = query + " " + "AND sn.ShippedDate >= GETDATE()";
                            break;

                        case "4001": //Lastday
                            query = query + " " + "AND sn.ShippedDate BETWEEN GETDATE()-1 AND GETDATE()";
                            break;

                        case "4002": //Week to Date
                            query = query + " " + "AND sn.ShippedDate BETWEEN (SELECT DATEADD(WEEKDAY, DATEDIFF(WEEKDAY, 0, GETDATE()), -1)) AND GETDATE() ";
                            break;

                        case "4003": //Month to Date
                            query = query + " " + "AND sn.ShippedDate >= (SELECT DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0))";
                            break;

                        case "4004": //Year to Date
                            query = query + " " + "AND sn.ShippedDate >= (SELECT DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0))";
                            break;

                        case "4005": //Last 7 days
                            query = query + " " + "AND sn.ShippedDate > GETDATE()-7";
                            break;

                        case "4006": //Last 14 days
                            query = query + " " + "AND sn.ShippedDate > GETDATE()-14";
                            break;

                        case "4007": //Last 30 days
                            query = query + " " + "AND sn.ShippedDate > GETDATE()-30";
                            break;

                        case "4008": //Last 90 days
                            query = query + " " + "AND sn.ShippedDate > GETDATE()-90";
                            break;

                        case "4009": //Last 180 day
                            query = query + " " + "AND sn.ShippedDate > GETDATE()-180";
                            break;

                        default:
                            break;
                    }

                    int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                    connection.Open();
                    var res = connection.Query<ShipNoticeDTO>(query,
                        commandType: CommandType.Text).ToList();
                    if (status.Count > 0)
                    {
                        var res1 = res.Where(x => status.Contains(x.StatusId)).ToList();
                        return res1.OrderByDescending(x => x.ShippedDate).ThenBy(x => x.StatusId).ToList();
                    }
                    return res.OrderByDescending(x => x.ShippedDate).ThenBy(x => x.StatusId).ToList();
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public override ICollection<ShipNotice> Get(List<int> idList)
        {
            throw new NotImplementedException();
        }

        public override string Update(ShipNotice data)
        {
            throw new NotImplementedException();
        }

        public void UpdateReceivedComplete(int ShipNoticeId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    string sQueryChangeShipNoticeReceivedStatus = @"update crm.ShipNotice set ReceivedComplete=(SELECT 
										case when Count(case when sd.status=1 then sn.ShipNoticeId end) = count(sn.ShipNoticeId)
										then 1 else 0 end as Status
										FROM crm.ShipmentDetail sd
											INNER JOIN crm.ShipNotice sn ON sn.ShipNoticeId = sd.ShipNoticeId
											INNER JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = sn.PurchaseOrderId
											left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
											INNER JOIN crm.Accounts a ON a.AccountId = mpox.AccountId
											left JOIN crm.QuoteLines ql ON ql.QuoteLineId = sd.QuoteLineId
											WHERE a.FranchiseId = @FranchiseId AND sn.ShipNoticeId = @ShipNoticeId
											group by mpo.PurchaseOrderId
									) where ShipNoticeId=@ShipNoticeId";
                    connection.Execute(sQueryChangeShipNoticeReceivedStatus, new { ShipNoticeId, FranchiseId = SessionManager.CurrentFranchise.FranchiseId });

                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                }
            }

        }
    }
}