﻿using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HFC.CRM.Managers
{
    public static class TimeZoneManager
    {
        // Don't use
        public static DateTime GetFranchiseDateTime()
        {
            string FrianchiseTimeZone = SessionManager.CurrentFranchise != null ? SessionManager.CurrentFranchise.TimezoneCode.ToString() : TimeZoneEnum.UTC.ToString();
            decimal franchiseOffSetTime = CacheManager.TimeZoneCollection.Where(t => t.TimeZone.Contains(FrianchiseTimeZone)).SingleOrDefault().OffSetTime;
            DateTime val = DateTime.UtcNow.AddSeconds(double.Parse(franchiseOffSetTime.ToString()));
            return val;
        }

        /// <summary>
        /// Convert FE datetime to UTC Datetime based FE timezone
        /// </summary>
        /// <param name="date"></param>
        /// <param name="tze"></param>
        /// <returns></returns>
        public static DateTime ToUTC(DateTime date, TimeZoneEnum? tze = null)
        {
            TimeZoneEnum feTimeZone;
            if (SessionManager.CurrentFranchise != null)
                feTimeZone = (TimeZoneEnum)Enum.Parse(typeof(TimeZoneEnum), SessionManager.CurrentFranchise.TimezoneCode.ToString());
            else if (tze != null) feTimeZone = (TimeZoneEnum)tze;
            else feTimeZone = TimeZoneEnum.UTC;

            DateTime utc = Util.ConvertDate(date, TimeZoneEnum.UTC, feTimeZone);
            return utc;
        }

        public static DateTime? ToUTC(DateTime? date, TimeZoneEnum? tze = null)
        {
            if (date == null)
                return date;

            TimeZoneEnum feTimeZone;
            if (SessionManager.CurrentFranchise != null)
                feTimeZone = (TimeZoneEnum)Enum.Parse(typeof(TimeZoneEnum), SessionManager.CurrentFranchise.TimezoneCode.ToString());
            else if (tze != null) feTimeZone = (TimeZoneEnum)tze;
            else feTimeZone = TimeZoneEnum.UTC;

            DateTime? utc = Util.ConvertDate(date, TimeZoneEnum.UTC, feTimeZone);
            return utc;
        }

        public static DateTime ToUTC(DateTimeOffset date, TimeZoneEnum? tze = null)
        {
            TimeZoneEnum feTimeZone;
            if (SessionManager.CurrentFranchise != null)
                feTimeZone = (TimeZoneEnum)Enum.Parse(typeof(TimeZoneEnum), SessionManager.CurrentFranchise.TimezoneCode.ToString());
            else if (tze != null) feTimeZone = (TimeZoneEnum)tze;
            else feTimeZone = TimeZoneEnum.UTC;

            DateTime utc = Util.ConvertDate(date.DateTime, TimeZoneEnum.UTC, feTimeZone);
            return utc;
        }

        public static DateTime? ToUTC(DateTimeOffset? date, TimeZoneEnum? tze = null)
        {
            if (date == null)
                return null;

            TimeZoneEnum feTimeZone;
            if (SessionManager.CurrentFranchise != null)
                feTimeZone = (TimeZoneEnum)Enum.Parse(typeof(TimeZoneEnum), SessionManager.CurrentFranchise.TimezoneCode.ToString());
            else if (tze != null) feTimeZone = (TimeZoneEnum)tze;
            else feTimeZone = TimeZoneEnum.UTC;

            DateTime? utc = Util.ConvertDate(((DateTimeOffset)date).DateTime, TimeZoneEnum.UTC, feTimeZone);
            return utc;
        }

        /// <summary>
        /// Convert UTC datetime to FE Datetime based FE timezone
        /// </summary>
        /// <param name="date"></param>
        /// <param name="tze"></param>
        /// <returns></returns>
        public static DateTime ToLocal(DateTime date, TimeZoneEnum? tze = null)
        {
            TimeZoneEnum feTimeZone;
            if (SessionManager.CurrentFranchise != null)
                feTimeZone = (TimeZoneEnum)Enum.Parse(typeof(TimeZoneEnum), SessionManager.CurrentFranchise.TimezoneCode.ToString());
            else if (tze != null) feTimeZone = (TimeZoneEnum)tze;
            else if (SessionManager.CurrentUser !=null && SessionManager.CurrentUser.Person != null 
                && SessionManager.CurrentUser.Person.TimezoneCode != null && SessionManager.CurrentUser.Person.TimezoneCode != TimeZoneEnum.NA)
                feTimeZone = (TimeZoneEnum)Enum.Parse(typeof(TimeZoneEnum), SessionManager.CurrentUser.Person.TimezoneCode.ToString());
            else feTimeZone = TimeZoneEnum.UTC;

            if (feTimeZone != TimeZoneEnum.UTC)
                return Util.ConvertDate(date, feTimeZone, TimeZoneEnum.UTC);
            return date;
        }

        public static DateTime? ToLocal(DateTime? date, TimeZoneEnum? tze = null)
        {
            if (date == null)
                return null;

            TimeZoneEnum feTimeZone;
            if (SessionManager.CurrentFranchise != null)
                feTimeZone = (TimeZoneEnum)Enum.Parse(typeof(TimeZoneEnum), SessionManager.CurrentFranchise.TimezoneCode.ToString());
            else if (tze != null) feTimeZone = (TimeZoneEnum)tze;
            else if (SessionManager.CurrentUser != null && SessionManager.CurrentUser.Person != null 
                && SessionManager.CurrentUser.Person.TimezoneCode != null && SessionManager.CurrentUser.Person.TimezoneCode != TimeZoneEnum.NA)
                feTimeZone = (TimeZoneEnum)Enum.Parse(typeof(TimeZoneEnum), SessionManager.CurrentUser.Person.TimezoneCode.ToString());
            else feTimeZone = TimeZoneEnum.UTC;

            if (feTimeZone != TimeZoneEnum.UTC)
                return Util.ConvertDate(date, feTimeZone, TimeZoneEnum.UTC);
            return date;
        }

        public static DateTime ToLocal(DateTimeOffset date, TimeZoneEnum? tze = null)
        {
            TimeZoneEnum feTimeZone;
            if (SessionManager.CurrentFranchise != null)
                feTimeZone = (TimeZoneEnum)Enum.Parse(typeof(TimeZoneEnum), SessionManager.CurrentFranchise.TimezoneCode.ToString());
            else if (tze != null) feTimeZone = (TimeZoneEnum)tze;
            else if (SessionManager.CurrentUser != null && SessionManager.CurrentUser.Person != null 
                && SessionManager.CurrentUser.Person.TimezoneCode != null && SessionManager.CurrentUser.Person.TimezoneCode != TimeZoneEnum.NA)
                feTimeZone = (TimeZoneEnum)Enum.Parse(typeof(TimeZoneEnum), SessionManager.CurrentUser.Person.TimezoneCode.ToString());
            else feTimeZone = TimeZoneEnum.UTC;

            var fetime = Util.ConvertDate(date.DateTime, feTimeZone, TimeZoneEnum.UTC);
            return fetime;
        }

        public static DateTime? ToLocal(DateTimeOffset? date, TimeZoneEnum? tze = null)
        {
            if (date == null)
                return null;

            TimeZoneEnum feTimeZone;
            if (SessionManager.CurrentFranchise != null)
                feTimeZone = (TimeZoneEnum)Enum.Parse(typeof(TimeZoneEnum), SessionManager.CurrentFranchise.TimezoneCode.ToString());
            else if (tze != null) feTimeZone = (TimeZoneEnum)tze;
            else if (SessionManager.CurrentUser != null && SessionManager.CurrentUser.Person != null 
                && SessionManager.CurrentUser.Person.TimezoneCode != null && SessionManager.CurrentUser.Person.TimezoneCode != TimeZoneEnum.NA)
                feTimeZone = (TimeZoneEnum)Enum.Parse(typeof(TimeZoneEnum), SessionManager.CurrentUser.Person.TimezoneCode.ToString());
            else feTimeZone = TimeZoneEnum.UTC;

            var fetime = Util.ConvertDate(((DateTimeOffset)date).DateTime, feTimeZone, TimeZoneEnum.UTC);
            return fetime;
        }

        /// <summary>
        /// Get Current FE datetime
        /// </summary>
        /// <returns></returns>
        public static DateTime GetLocal()
        {
            TimeZoneEnum feTimeZone = SessionManager.CurrentFranchise != null ? (TimeZoneEnum)Enum.Parse(typeof(TimeZoneEnum), SessionManager.CurrentFranchise.TimezoneCode.ToString()) : TimeZoneEnum.UTC;
            var fetime = Util.ConvertDate(DateTime.UtcNow, feTimeZone, TimeZoneEnum.UTC);
            return fetime;
        }

        /// <summary>
        /// Convet UTC to FE time whole object
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static object ConvertToLocal(object obj)
        {
            if (obj.GetType().GetProperty("CreatedOn") != null && obj.GetType().GetProperty("CreatedOn").GetValue(obj) != null)
            {
                if (obj.GetType().GetProperty("CreatedOn").PropertyType == typeof(DateTime))
                {
                    var CreatedOn = ToLocal((DateTime)obj.GetType().GetProperty("CreatedOn").GetValue(obj));
                    obj.GetType().GetProperty("CreatedOn").SetValue(obj, CreatedOn);
                }
                if (obj.GetType().GetProperty("CreatedOn").PropertyType == typeof(DateTimeOffset))
                {
                    var CreatedOn = ToLocal(((DateTimeOffset)obj.GetType().GetProperty("CreatedOn").GetValue(obj)).DateTime);
                    obj.GetType().GetProperty("CreatedOn").SetValue(obj, (DateTimeOffset)CreatedOn);
                }
                if (obj.GetType().GetProperty("CreatedOn").PropertyType == typeof(DateTime?))
                {
                    var CreatedOn = ToLocal((DateTime?)obj.GetType().GetProperty("CreatedOn").GetValue(obj));
                    obj.GetType().GetProperty("CreatedOn").SetValue(obj, CreatedOn);
                }
                if (obj.GetType().GetProperty("CreatedOn").PropertyType == typeof(DateTimeOffset?))
                {
                    var CreatedOn = ToLocal(((DateTimeOffset?)obj.GetType().GetProperty("CreatedOn").GetValue(obj))?.DateTime);
                    obj.GetType().GetProperty("CreatedOn").SetValue(obj, (DateTimeOffset?)CreatedOn);
                }

            }
            if (obj.GetType().GetProperty("LastUpdatedOn") != null && obj.GetType().GetProperty("LastUpdatedOn").GetValue(obj) != null)
            {
                if (obj.GetType().GetProperty("LastUpdatedOn").PropertyType == typeof(DateTime))
                {
                    var LastUpdatedOn = ToLocal((DateTime)obj.GetType().GetProperty("LastUpdatedOn").GetValue(obj));
                    obj.GetType().GetProperty("LastUpdatedOn").SetValue(obj, LastUpdatedOn);
                }
                if (obj.GetType().GetProperty("LastUpdatedOn").PropertyType == typeof(DateTime?))
                {
                    var LastUpdatedOn = ToLocal((DateTime?)obj.GetType().GetProperty("LastUpdatedOn").GetValue(obj));
                    obj.GetType().GetProperty("LastUpdatedOn").SetValue(obj, LastUpdatedOn);
                }
                if (obj.GetType().GetProperty("LastUpdatedOn").PropertyType == typeof(DateTimeOffset))
                {
                    var LastUpdatedOn = ToLocal(((DateTimeOffset)obj.GetType().GetProperty("LastUpdatedOn").GetValue(obj)).DateTime);
                    obj.GetType().GetProperty("LastUpdatedOn").SetValue(obj, (DateTimeOffset)LastUpdatedOn);
                }
                if (obj.GetType().GetProperty("LastUpdatedOn").PropertyType == typeof(DateTimeOffset?))
                {
                    var LastUpdatedOn = ToLocal(((DateTimeOffset?)obj.GetType().GetProperty("LastUpdatedOn").GetValue(obj))?.DateTime);
                    obj.GetType().GetProperty("LastUpdatedOn").SetValue(obj, (DateTimeOffset?)LastUpdatedOn);
                }

            }
            if (obj.GetType().GetProperty("CreatedOnUtc") != null && obj.GetType().GetProperty("CreatedOnUtc").GetValue(obj) != null)
            {
                if (obj.GetType().GetProperty("CreatedOnUtc").PropertyType == typeof(DateTime))
                {
                    var CreatedOnUtc = ToLocal((DateTime)obj.GetType().GetProperty("CreatedOnUtc").GetValue(obj));
                    obj.GetType().GetProperty("CreatedOnUtc").SetValue(obj, CreatedOnUtc);
                }
                if (obj.GetType().GetProperty("CreatedOnUtc").PropertyType == typeof(DateTimeOffset))
                {
                    var CreatedOnUtc = ToLocal(((DateTimeOffset)obj.GetType().GetProperty("CreatedOnUtc").GetValue(obj)).DateTime);
                    obj.GetType().GetProperty("CreatedOnUtc").SetValue(obj, (DateTimeOffset)CreatedOnUtc);
                }
                if (obj.GetType().GetProperty("CreatedOnUtc").PropertyType == typeof(DateTime?))
                {
                    var CreatedOnUtc = ToLocal((DateTime?)obj.GetType().GetProperty("CreatedOnUtc").GetValue(obj));
                    obj.GetType().GetProperty("CreatedOnUtc").SetValue(obj, CreatedOnUtc);
                }
                if (obj.GetType().GetProperty("CreatedOnUtc").PropertyType == typeof(DateTimeOffset?))
                {
                    var CreatedOnUtc = ToLocal(((DateTimeOffset?)obj.GetType().GetProperty("CreatedOnUtc").GetValue(obj))?.DateTime);
                    obj.GetType().GetProperty("CreatedOnUtc").SetValue(obj, (DateTimeOffset?)CreatedOnUtc);
                }

            }
            if (obj.GetType().GetProperty("LastUpdatedOnUtc") != null && obj.GetType().GetProperty("LastUpdatedOnUtc").GetValue(obj) != null)
            {
                if (obj.GetType().GetProperty("LastUpdatedOnUtc").PropertyType == typeof(DateTime))
                {
                    var LastUpdatedOnUtc = ToLocal((DateTime)obj.GetType().GetProperty("LastUpdatedOnUtc").GetValue(obj));
                    obj.GetType().GetProperty("LastUpdatedOnUtc").SetValue(obj, LastUpdatedOnUtc);
                }
                if (obj.GetType().GetProperty("LastUpdatedOnUtc").PropertyType == typeof(DateTimeOffset))
                {
                    var LastUpdatedOnUtc = ToLocal(((DateTimeOffset)obj.GetType().GetProperty("LastUpdatedOnUtc").GetValue(obj)).DateTime);
                    obj.GetType().GetProperty("LastUpdatedOnUtc").SetValue(obj, (DateTimeOffset)LastUpdatedOnUtc);
                }
                if (obj.GetType().GetProperty("LastUpdatedOnUtc").PropertyType == typeof(DateTime?))
                {
                    var LastUpdatedOnUtc = ToLocal((DateTime?)obj.GetType().GetProperty("LastUpdatedOnUtc").GetValue(obj));
                    obj.GetType().GetProperty("LastUpdatedOnUtc").SetValue(obj, LastUpdatedOnUtc);
                }
                if (obj.GetType().GetProperty("LastUpdatedOnUtc").PropertyType == typeof(DateTimeOffset?))
                {
                    var LastUpdatedOnUtc = ToLocal(((DateTimeOffset?)obj.GetType().GetProperty("LastUpdatedOnUtc").GetValue(obj))?.DateTime);
                    obj.GetType().GetProperty("LastUpdatedOnUtc").SetValue(obj, (DateTimeOffset?)LastUpdatedOnUtc);
                }
            }
            if (obj.GetType().GetProperty("UpdatedOn") != null && obj.GetType().GetProperty("UpdatedOn").GetValue(obj) != null)
            {
                if (obj.GetType().GetProperty("UpdatedOn").PropertyType == typeof(DateTime))
                {
                    var UpdatedOn = ToLocal((DateTime)obj.GetType().GetProperty("UpdatedOn").GetValue(obj));
                    obj.GetType().GetProperty("UpdatedOn").SetValue(obj, UpdatedOn);
                }
                if (obj.GetType().GetProperty("UpdatedOn").PropertyType == typeof(DateTimeOffset))
                {
                    var UpdatedOn = ToLocal(((DateTimeOffset)obj.GetType().GetProperty("UpdatedOn").GetValue(obj)).DateTime);
                    obj.GetType().GetProperty("UpdatedOn").SetValue(obj, (DateTimeOffset)UpdatedOn);
                }
                if (obj.GetType().GetProperty("UpdatedOn").PropertyType == typeof(DateTime?))
                {
                    var UpdatedOn = ToLocal((DateTime?)obj.GetType().GetProperty("UpdatedOn").GetValue(obj));
                    obj.GetType().GetProperty("UpdatedOn").SetValue(obj, UpdatedOn);
                }
                if (obj.GetType().GetProperty("UpdatedOn").PropertyType == typeof(DateTimeOffset?))
                {
                    var UpdatedOn = ToLocal(((DateTimeOffset?)obj.GetType().GetProperty("UpdatedOn").GetValue(obj))?.DateTime);
                    obj.GetType().GetProperty("UpdatedOn").SetValue(obj, (DateTimeOffset?)UpdatedOn);
                }
            }
            if (obj.GetType().GetProperty("StartDate") != null && obj.GetType().GetProperty("StartDate").GetValue(obj) != null)
            {
                if (obj.GetType().GetProperty("StartDate").PropertyType == typeof(DateTime))
                {
                    var StartDate = ToLocal((DateTime)obj.GetType().GetProperty("StartDate").GetValue(obj));
                    obj.GetType().GetProperty("StartDate").SetValue(obj, StartDate);
                }
                if (obj.GetType().GetProperty("StartDate").PropertyType == typeof(DateTimeOffset))
                {
                    var StartDate = ToLocal(((DateTimeOffset)obj.GetType().GetProperty("StartDate").GetValue(obj)).DateTime);
                    obj.GetType().GetProperty("StartDate").SetValue(obj, (DateTimeOffset)StartDate);
                }
                if (obj.GetType().GetProperty("StartDate").PropertyType == typeof(DateTime?))
                {
                    var StartDate = ToLocal((DateTime?)obj.GetType().GetProperty("StartDate").GetValue(obj));
                    obj.GetType().GetProperty("StartDate").SetValue(obj, StartDate);
                }
                if (obj.GetType().GetProperty("StartDate").PropertyType == typeof(DateTimeOffset?))
                {
                    var StartDate = ToLocal(((DateTimeOffset?)obj.GetType().GetProperty("StartDate").GetValue(obj))?.DateTime);
                    obj.GetType().GetProperty("StartDate").SetValue(obj, (DateTimeOffset?)StartDate);
                }
            }
            if (obj.GetType().GetProperty("EndDate") != null && obj.GetType().GetProperty("EndDate").GetValue(obj) != null)
            {
                if (obj.GetType().GetProperty("EndDate").PropertyType == typeof(DateTime))
                {
                    var EndDate = ToLocal((DateTime)obj.GetType().GetProperty("EndDate").GetValue(obj));
                    obj.GetType().GetProperty("EndDate").SetValue(obj, EndDate);
                }
                if (obj.GetType().GetProperty("EndDate").PropertyType == typeof(DateTimeOffset))
                {
                    var EndDate = ToLocal(((DateTimeOffset)obj.GetType().GetProperty("EndDate").GetValue(obj)).DateTime);
                    obj.GetType().GetProperty("EndDate").SetValue(obj, (DateTimeOffset)EndDate);
                }
                if (obj.GetType().GetProperty("EndDate").PropertyType == typeof(DateTime?))
                {
                    var EndDate = ToLocal((DateTime?)obj.GetType().GetProperty("EndDate").GetValue(obj));
                    obj.GetType().GetProperty("EndDate").SetValue(obj, EndDate);
                }
                if (obj.GetType().GetProperty("EndDate").PropertyType == typeof(DateTimeOffset?))
                {
                    var EndDate = ToLocal(((DateTimeOffset?)obj.GetType().GetProperty("EndDate").GetValue(obj))?.DateTime);
                    obj.GetType().GetProperty("EndDate").SetValue(obj, (DateTimeOffset?)EndDate);
                }
            }
            if (obj.GetType().GetProperty("DueDate") != null && obj.GetType().GetProperty("DueDate").GetValue(obj) != null)
            {
                if (obj.GetType().GetProperty("DueDate").PropertyType == typeof(DateTime))
                {
                    var DueDate = ToLocal((DateTime)obj.GetType().GetProperty("DueDate").GetValue(obj));
                    obj.GetType().GetProperty("DueDate").SetValue(obj, DueDate);
                }
                if (obj.GetType().GetProperty("DueDate").PropertyType == typeof(DateTimeOffset))
                {
                    var DueDate = ToLocal(((DateTimeOffset)obj.GetType().GetProperty("DueDate").GetValue(obj)).DateTime);
                    obj.GetType().GetProperty("DueDate").SetValue(obj, (DateTimeOffset)DueDate);
                }
                if (obj.GetType().GetProperty("DueDate").PropertyType == typeof(DateTime?))
                {
                    var DueDate = ToLocal((DateTime?)obj.GetType().GetProperty("DueDate").GetValue(obj));
                    obj.GetType().GetProperty("DueDate").SetValue(obj, DueDate);
                }
                if (obj.GetType().GetProperty("DueDate").PropertyType == typeof(DateTimeOffset?))
                {
                    var DueDate = ToLocal(((DateTimeOffset?)obj.GetType().GetProperty("DueDate").GetValue(obj))?.DateTime);
                    obj.GetType().GetProperty("DueDate").SetValue(obj, (DateTimeOffset?)DueDate);
                }
            }
            if (obj.GetType().GetProperty("CreationDateUtc") != null && obj.GetType().GetProperty("CreationDateUtc").GetValue(obj) != null)
            {
                if (obj.GetType().GetProperty("CreationDateUtc").PropertyType == typeof(DateTime))
                {
                    var CreationDateUtc = ToLocal((DateTime)obj.GetType().GetProperty("CreationDateUtc").GetValue(obj));
                    obj.GetType().GetProperty("CreationDateUtc").SetValue(obj, CreationDateUtc);
                }
                if (obj.GetType().GetProperty("CreationDateUtc").PropertyType == typeof(DateTime?))
                {
                    var CreationDateUtc = ToLocal((DateTime)obj.GetType().GetProperty("CreationDateUtc").GetValue(obj));
                    obj.GetType().GetProperty("CreationDateUtc").SetValue(obj, CreationDateUtc);
                }
                if (obj.GetType().GetProperty("CreationDateUtc").PropertyType == typeof(DateTimeOffset))
                {
                    var CreationDateUtc = ToLocal(((DateTimeOffset)obj.GetType().GetProperty("CreationDateUtc").GetValue(obj)).DateTime);
                    obj.GetType().GetProperty("CreationDateUtc").SetValue(obj, (DateTimeOffset)CreationDateUtc);
                }
                if (obj.GetType().GetProperty("CreationDateUtc").PropertyType == typeof(DateTimeOffset?))
                {
                    var CreationDateUtc = ToLocal(((DateTimeOffset?)obj.GetType().GetProperty("CreationDateUtc").GetValue(obj))?.DateTime);
                    obj.GetType().GetProperty("CreationDateUtc").SetValue(obj, (DateTimeOffset?)CreationDateUtc);
                }
            }
            if (obj.GetType().GetProperty("LastLoginDateUtc") != null && obj.GetType().GetProperty("LastLoginDateUtc").GetValue(obj) != null)
            {
                if (obj.GetType().GetProperty("LastLoginDateUtc").PropertyType == typeof(DateTime))
                {
                    var LastLoginDateUtc = ToLocal((DateTime)obj.GetType().GetProperty("LastLoginDateUtc").GetValue(obj));
                    obj.GetType().GetProperty("LastLoginDateUtc").SetValue(obj, LastLoginDateUtc);
                }
                if (obj.GetType().GetProperty("LastLoginDateUtc").PropertyType == typeof(DateTime?))
                {
                    var LastLoginDateUtc = ToLocal((DateTime?)obj.GetType().GetProperty("LastLoginDateUtc").GetValue(obj));
                    obj.GetType().GetProperty("LastLoginDateUtc").SetValue(obj, LastLoginDateUtc);
                }
                if (obj.GetType().GetProperty("LastLoginDateUtc").PropertyType == typeof(DateTimeOffset))
                {
                    var LastLoginDateUtc = ToLocal(((DateTimeOffset)obj.GetType().GetProperty("LastLoginDateUtc").GetValue(obj)).DateTime);
                    obj.GetType().GetProperty("LastLoginDateUtc").SetValue(obj, (DateTimeOffset)LastLoginDateUtc);
                }
                if (obj.GetType().GetProperty("LastLoginDateUtc").PropertyType == typeof(DateTimeOffset?))
                {
                    var LastLoginDateUtc = ToLocal(((DateTimeOffset?)obj.GetType().GetProperty("LastLoginDateUtc").GetValue(obj))?.DateTime);
                    obj.GetType().GetProperty("LastLoginDateUtc").SetValue(obj, (DateTimeOffset?)LastLoginDateUtc);
                }
            }
            if (obj.GetType().GetProperty("SentAt") != null && obj.GetType().GetProperty("SentAt").GetValue(obj) != null)
            {
                if (obj.GetType().GetProperty("SentAt").PropertyType == typeof(DateTime?))
                {
                    if (obj.GetType().GetProperty("SentAt").GetValue(obj) != null)
                    {
                        var SentAt = ToLocal((DateTime)obj.GetType().GetProperty("SentAt").GetValue(obj));
                        obj.GetType().GetProperty("SentAt").SetValue(obj, SentAt);
                    }
                }
                if (obj.GetType().GetProperty("SentAt").PropertyType == typeof(DateTime))
                {
                    var SentAt = ToLocal((DateTime)obj.GetType().GetProperty("SentAt").GetValue(obj));
                    obj.GetType().GetProperty("SentAt").SetValue(obj, SentAt);
                }
                if (obj.GetType().GetProperty("SentAt").PropertyType == typeof(DateTimeOffset))
                {
                    var SentAt = ToLocal(((DateTimeOffset)obj.GetType().GetProperty("SentAt").GetValue(obj)).DateTime);
                    obj.GetType().GetProperty("SentAt").SetValue(obj, (DateTimeOffset)SentAt);
                }
            }

            return obj;
        }

        /// <summary>
        /// Convet UTC to FE time whole list object
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="objEntity"></param>
        /// <returns></returns>
        public static List<TEntity> ConvertToLocalList<TEntity>(List<TEntity> objEntity) where TEntity : class
        {
            foreach (var obj in objEntity)
            {
                if (obj.GetType().GetProperty("CreatedOn") != null && obj.GetType().GetProperty("CreatedOn").GetValue(obj) != null)
                {
                    if (obj.GetType().GetProperty("CreatedOn").PropertyType == typeof(DateTime))
                    {
                        var CreatedOn = ToLocal((DateTime)obj.GetType().GetProperty("CreatedOn").GetValue(obj));
                        obj.GetType().GetProperty("CreatedOn").SetValue(obj, CreatedOn);
                    }
                    if (obj.GetType().GetProperty("CreatedOn").PropertyType == typeof(DateTimeOffset))
                    {
                        var CreatedOn = ToLocal(((DateTimeOffset)obj.GetType().GetProperty("CreatedOn").GetValue(obj)).DateTime);
                        obj.GetType().GetProperty("CreatedOn").SetValue(obj, (DateTimeOffset)CreatedOn);
                    }

                }
                if (obj.GetType().GetProperty("LastUpdatedOn") != null && obj.GetType().GetProperty("LastUpdatedOn").GetValue(obj) != null)
                {
                    if (obj.GetType().GetProperty("LastUpdatedOn").PropertyType == typeof(DateTime))
                    {
                        var LastUpdatedOn = ToLocal((DateTime)obj.GetType().GetProperty("LastUpdatedOn").GetValue(obj));
                        obj.GetType().GetProperty("LastUpdatedOn").SetValue(obj, LastUpdatedOn);
                    }
                    if (obj.GetType().GetProperty("LastUpdatedOn").PropertyType == typeof(DateTimeOffset))
                    {
                        var LastUpdatedOn = ToLocal(((DateTimeOffset)obj.GetType().GetProperty("LastUpdatedOn").GetValue(obj)).DateTime);
                        obj.GetType().GetProperty("LastUpdatedOn").SetValue(obj, (DateTimeOffset)LastUpdatedOn);
                    }


                }
                if (obj.GetType().GetProperty("CreatedOnUtc") != null && obj.GetType().GetProperty("CreatedOnUtc").GetValue(obj) != null)
                {
                    if (obj.GetType().GetProperty("CreatedOnUtc").PropertyType == typeof(DateTime))
                    {
                        var CreatedOnUtc = ToLocal((DateTime)obj.GetType().GetProperty("CreatedOnUtc").GetValue(obj));
                        obj.GetType().GetProperty("CreatedOnUtc").SetValue(obj, CreatedOnUtc);
                    }
                    if (obj.GetType().GetProperty("CreatedOnUtc").PropertyType == typeof(DateTimeOffset))
                    {
                        var CreatedOnUtc = ToLocal(((DateTimeOffset)obj.GetType().GetProperty("CreatedOnUtc").GetValue(obj)).DateTime);
                        obj.GetType().GetProperty("CreatedOnUtc").SetValue(obj, (DateTimeOffset)CreatedOnUtc);
                    }

                }
                if (obj.GetType().GetProperty("LastUpdatedOnUtc") != null && obj.GetType().GetProperty("LastUpdatedOnUtc").GetValue(obj) != null)
                {
                    if (obj.GetType().GetProperty("LastUpdatedOnUtc").PropertyType == typeof(DateTime))
                    {
                        var LastUpdatedOnUtc = ToLocal((DateTime)obj.GetType().GetProperty("LastUpdatedOnUtc").GetValue(obj));
                        obj.GetType().GetProperty("LastUpdatedOnUtc").SetValue(obj, LastUpdatedOnUtc);
                    }
                    if (obj.GetType().GetProperty("LastUpdatedOnUtc").PropertyType == typeof(DateTimeOffset))
                    {
                        var LastUpdatedOnUtc = ToLocal(((DateTimeOffset)obj.GetType().GetProperty("LastUpdatedOnUtc").GetValue(obj)).DateTime);
                        obj.GetType().GetProperty("LastUpdatedOnUtc").SetValue(obj, (DateTimeOffset)LastUpdatedOnUtc);
                    }
                }
                if (obj.GetType().GetProperty("UpdatedOn") != null && obj.GetType().GetProperty("UpdatedOn").GetValue(obj) != null)
                {
                    if (obj.GetType().GetProperty("UpdatedOn").PropertyType == typeof(DateTime))
                    {
                        var UpdatedOn = ToLocal((DateTime)obj.GetType().GetProperty("UpdatedOn").GetValue(obj));
                        obj.GetType().GetProperty("UpdatedOn").SetValue(obj, UpdatedOn);
                    }
                    if (obj.GetType().GetProperty("UpdatedOn").PropertyType == typeof(DateTimeOffset))
                    {
                        var UpdatedOn = ToLocal(((DateTimeOffset)obj.GetType().GetProperty("UpdatedOn").GetValue(obj)).DateTime);
                        obj.GetType().GetProperty("UpdatedOn").SetValue(obj, (DateTimeOffset)UpdatedOn);
                    }
                }
                if (obj.GetType().GetProperty("StartDate") != null && obj.GetType().GetProperty("StartDate").GetValue(obj) != null)
                {
                    if (obj.GetType().GetProperty("StartDate").PropertyType == typeof(DateTime))
                    {
                        var StartDate = ToLocal((DateTime)obj.GetType().GetProperty("StartDate").GetValue(obj));
                        obj.GetType().GetProperty("StartDate").SetValue(obj, StartDate);
                    }
                    if (obj.GetType().GetProperty("StartDate").PropertyType == typeof(DateTimeOffset))
                    {
                        var StartDate = ToLocal(((DateTimeOffset)obj.GetType().GetProperty("StartDate").GetValue(obj)).DateTime);
                        obj.GetType().GetProperty("StartDate").SetValue(obj, (DateTimeOffset)StartDate);
                    }
                }
                if (obj.GetType().GetProperty("EndDate") != null && obj.GetType().GetProperty("EndDate").GetValue(obj) != null)
                {
                    if (obj.GetType().GetProperty("EndDate").PropertyType == typeof(DateTime))
                    {
                        var EndDate = ToLocal((DateTime)obj.GetType().GetProperty("EndDate").GetValue(obj));
                        obj.GetType().GetProperty("EndDate").SetValue(obj, EndDate);
                    }
                    if (obj.GetType().GetProperty("EndDate").PropertyType == typeof(DateTimeOffset))
                    {
                        var EndDate = ToLocal(((DateTimeOffset)obj.GetType().GetProperty("EndDate").GetValue(obj)).DateTime);
                        obj.GetType().GetProperty("EndDate").SetValue(obj, (DateTimeOffset)EndDate);
                    }
                }
                if (obj.GetType().GetProperty("DueDate") != null && obj.GetType().GetProperty("DueDate").GetValue(obj) != null)
                {
                    if (obj.GetType().GetProperty("DueDate").PropertyType == typeof(DateTime))
                    {
                        var DueDate = ToLocal((DateTime)obj.GetType().GetProperty("DueDate").GetValue(obj));
                        obj.GetType().GetProperty("DueDate").SetValue(obj, DueDate);
                    }
                    if (obj.GetType().GetProperty("DueDate").PropertyType == typeof(DateTimeOffset))
                    {
                        var DueDate = ToLocal(((DateTimeOffset)obj.GetType().GetProperty("DueDate").GetValue(obj)).DateTime);
                        obj.GetType().GetProperty("DueDate").SetValue(obj, (DateTimeOffset)DueDate);
                    }
                }
                if (obj.GetType().GetProperty("SentAt") != null && obj.GetType().GetProperty("SentAt").GetValue(obj) != null)
                {
                    if (obj.GetType().GetProperty("SentAt").PropertyType == typeof(DateTime?))
                    {
                        if (obj.GetType().GetProperty("SentAt").GetValue(obj) != null)
                        {
                            var SentAt = ToLocal((DateTime)obj.GetType().GetProperty("SentAt").GetValue(obj));
                            obj.GetType().GetProperty("SentAt").SetValue(obj, SentAt);
                        }
                    }
                    if (obj.GetType().GetProperty("SentAt").PropertyType == typeof(DateTime))
                    {
                        var SentAt = ToLocal((DateTime)obj.GetType().GetProperty("SentAt").GetValue(obj));
                        obj.GetType().GetProperty("SentAt").SetValue(obj, SentAt);
                    }
                    if (obj.GetType().GetProperty("SentAt").PropertyType == typeof(DateTimeOffset))
                    {
                        var SentAt = ToLocal(((DateTimeOffset)obj.GetType().GetProperty("SentAt").GetValue(obj)).DateTime);
                        obj.GetType().GetProperty("SentAt").SetValue(obj, (DateTimeOffset)SentAt);
                    }
                }
                if (obj.GetType().GetProperty("DateValue") != null && obj.GetType().GetProperty("DateValue").GetValue(obj) != null)
                {
                    if (obj.GetType().GetProperty("DateValue").PropertyType == typeof(DateTime?))
                    {
                        if (obj.GetType().GetProperty("DateValue").GetValue(obj) != null)
                        {
                            var DateValue = ToLocal((DateTime)obj.GetType().GetProperty("DateValue").GetValue(obj));
                            obj.GetType().GetProperty("DateValue").SetValue(obj, DateValue);
                        }
                    }
                    if (obj.GetType().GetProperty("DateValue").PropertyType == typeof(DateTime))
                    {
                        var DateValue = ToLocal((DateTime)obj.GetType().GetProperty("DateValue").GetValue(obj));
                        obj.GetType().GetProperty("DateValue").SetValue(obj, DateValue);
                    }
                    if (obj.GetType().GetProperty("DateValue").PropertyType == typeof(DateTimeOffset))
                    {
                        var DateValue = ToLocal(((DateTimeOffset)obj.GetType().GetProperty("DateValue").GetValue(obj)).DateTime);
                        obj.GetType().GetProperty("DateValue").SetValue(obj, (DateTimeOffset)DateValue);
                    }
                }
            }
            return objEntity;
        }

        /// <summary>
        /// Get UTC Start date time based on FE timezone
        /// </summary>
        /// <param name="date"></param>
        /// <param name="feTimeZone"></param>
        /// <returns></returns>
        public static DateTime GetUTCstartdatetime(DateTime date, TimeZoneEnum? feTimeZone = null)
        {
            List<string> dt = new List<string>();

            DateTime utctimenow = DateTime.UtcNow;
            DateTime fetimenow = TimeZoneManager.ToLocal(utctimenow, feTimeZone);

            // find the utc time varience
            TimeSpan timediff = utctimenow - fetimenow;

            DateTime startdate = date.Date.Add(timediff);
            return startdate;
        }

        /// <summary>
        /// Get UTC End date time based on FE timezone
        /// </summary>
        /// <param name="date"></param>
        /// <param name="feTimeZone"></param>
        /// <returns></returns>
        public static DateTime GetUTCenddatetime(DateTime date, TimeZoneEnum? feTimeZone = null)
        {
            List<string> dt = new List<string>();

            DateTime utctimenow = DateTime.UtcNow;
            DateTime fetimenow = TimeZoneManager.ToLocal(utctimenow, feTimeZone);

            // find the utc time varience
            TimeSpan timediff = utctimenow - fetimenow;

            DateTime startdate = date.Date.Add(timediff);
            DateTime endDate = startdate.AddHours(24);

            return endDate;
        }

        /// <summary>
        /// Get UTC Today Start date time based on FE timezone
        /// </summary>
        /// <param name="date"></param>
        /// <param name="feTimeZone"></param>
        /// <returns></returns>
        public static DateTime GetUTCTodaystartdatetime(TimeZoneEnum? feTimeZone = null)
        {
            List<string> dt = new List<string>();

            DateTime utctimenow = DateTime.UtcNow;
            DateTime fetimenow = TimeZoneManager.ToLocal(utctimenow, feTimeZone);

            // find the utc time varience
            TimeSpan timediff = utctimenow - fetimenow;

            DateTime startdate = fetimenow.Date.Add(timediff);
            return startdate;
        }

        /// <summary>
        /// Get UTC Today End date time based on FE timezone
        /// </summary>
        /// <param name="date"></param>
        /// <param name="feTimeZone"></param>
        /// <returns></returns>
        public static DateTime GetUTCTodayenddatetime(TimeZoneEnum? feTimeZone = null)
        {
            List<string> dt = new List<string>();

            DateTime utctimenow = DateTime.UtcNow;
            DateTime fetimenow = TimeZoneManager.ToLocal(utctimenow, feTimeZone);

            // find the utc time varience
            TimeSpan timediff = utctimenow - fetimenow;

            DateTime startdate = fetimenow.Date.Add(timediff);
            DateTime endDate = startdate.AddHours(24);

            return endDate;
        }

        //public static DateTime ConvertDate(DateTime dateTime, int sourceTimeZone, int destinationTimeZone)
        //{
        //    if (sourceTimeZone == destinationTimeZone)
        //        return dateTime;

        //    TimeZoneInfo frominfo = sourceTimeZone == 1 ? TimeZoneInfo.Utc : TimeZoneInfo.FindSystemTimeZoneById(CacheManager.TimeZoneCollection.Where(t => t.TimeZoneId == sourceTimeZone).Select(x => x.Description).FirstOrDefault() ?? "");
        //    TimeZoneInfo toinfo = destinationTimeZone == 1 ? TimeZoneInfo.Utc : TimeZoneInfo.FindSystemTimeZoneById(CacheManager.TimeZoneCollection.Where(t => t.TimeZoneId == destinationTimeZone).Select(x => x.Description).FirstOrDefault() ?? "");

        //    //necessary to reset the DateTimeKind to unspecified
        //    dateTime = new DateTime(dateTime.Ticks);
        //    return TimeZoneInfo.ConvertTime(dateTime, frominfo, toinfo);
        //}

    }
}
