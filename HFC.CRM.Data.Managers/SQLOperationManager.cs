﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Managers
{
    public class SQLOperationManager
    {
        // Execute custom sql query and return data
        public static IEnumerable<T> ExecuteIEnumerableObject<T>(string Constr, string Query, object value = null)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(Constr))
                {
                    var data = con.Query<T>(Query, value);
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Execute custom sql query for Insert and Update
        public static void ExecuteObject(string Constr, string Query, object value = null)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(Constr))
                {
                    var data = con.Execute(Query, value);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Insert
        public static int? Insert<TEntity>(string Constr, TEntity value)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(Constr))
                {
                    var data = con.Insert<TEntity>(value);
                    return data;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static T Insert<T, TEntity>(string Constr, TEntity value)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(Constr))
                {
                    var data = con.Insert<T, TEntity>(value);
                    return data;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Update
        public static int Update<T>(string Constr, T value)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(Constr))
                {
                    var data = con.Update<T>(value);
                    return data;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Delete
        public static int Delete(string Constr, object value)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(Constr))
                {
                    var data = con.Delete(value);
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static int Delete<T>(string Constr, object value)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(Constr))
                {
                    var data = con.Delete<T>(value);
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region ExecuteObject

        public static T ExecuteObject<T>(string Constr, Guid? value) where T : class
        {
            try
            {
                using (SqlConnection con = new SqlConnection(Constr))
                {
                    var data = con.Get<T>(value);
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static T ExecuteObject<T>(string Constr, int? value) where T : class
        {
            try
            {
                using (SqlConnection con = new SqlConnection(Constr))
                {
                    var data = con.Get<T>(value);
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region ExecuteListObject
        public static List<T> ExecuteListObject<T>(string Constr, string Query, object value = null) where T : class
        {
            try
            {
                using (SqlConnection con = new SqlConnection(Constr))
                {
                    var data = con.GetList<T>(Query, value).ToList();
                    return data;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static IEnumerable<T> ExecuteIEnumerableObjectSingle<T>(string Constr, string Query, object value = null) where T : class
        {
            try
            {
                using (SqlConnection con = new SqlConnection(Constr))
                {
                    var data = con.GetList<T>(Query, value);
                    return data;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static T ExecuteScalar<T>(string Constr, string Query, object value = null)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(Constr))
                {
                    var data = con.ExecuteScalar<T>(Query, value);
                    return data;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}
