﻿using HFC.CRM.Core.Membership;
using HFC.CRM.Data;

using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Web;
using HFC.CRM.Data.Context;
using HFC.CRM.Core.Common;
using HFC.CRM.Data.webOE;
using HFC.CRM.Core.Logs;
using System.Data.SqlClient;
using Dapper;
using System.Dynamic;
using Newtonsoft.Json;
using System.Collections;
using System.Data;
using static HFC.CRM.Data.EnumExtension;
using HFC.CRM.DTO.PurchaseOrder;
using HFC.CRM.DTO.BulkPurchase;
using HFC.CRM.Core.Extensions;

//using static HFC.CRM.Managers.EnumExtension;
//using HFC.CRM.System.Net.Mail;

namespace HFC.CRM.Managers
{
    public class PurchaseOrderManager : DataManager<Order>
    {
        public PurchaseOrderManager()
        {
        }

        public PurchaseOrderManager(User user, Franchise franchise) : this(user, user, franchise)
        {
        }

        private QuotesManager QuotesMgr = new QuotesManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private EmailManager eManager = new EmailManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        private OrderManager ordMgr = new OrderManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        public PurchaseOrderManager(User user, User authorizingUser, Franchise franchise)
        {
            this.User = user;
            this.AuthorizingUser = authorizingUser;
            this.Franchise = franchise;
        }

        private PICManager _picMgr = new PICManager();

        #region Purchase Order Management for Touchpoint

        public dynamic GetOrderObjectXMpo(int xmpoId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var queryAddress = @"select a.* from crm.Addresses a
                                        left join crm.MasterPurchaseOrder mpo on mpo.ShipAddressId = a.AddressId
                                        where mpo.PurchaseOrderId = @xmpoid";
                    var address = connection.Query<AddressTP>(queryAddress, new { xmpoid = xmpoId }).FirstOrDefault();

                    if (address == null)
                        throw new Exception("Please select shipping address.");
                    var fequery = @"
								   select distinct f.* from crm.MasterPurchaseOrder mpo
								   INNER JOIN crm.MasterPurchaseOrderExtn mpoe ON mpoe.PurchaseOrderId = mpo.PurchaseOrderId
								   INNER JOIN crm.Opportunities o ON o.OpportunityId = mpoe.OpportunityId
								   INNER JOIN crm.Franchise f ON f.FranchiseId = o.FranchiseId
                                    where mpo.PurchaseOrderId = @xmpoid";
                    var franchise = connection.Query<Franchise>(fequery, new { xmpoid = xmpoId }).FirstOrDefault();

                    var query = @"select mpo.* from crm.MasterPurchaseOrder mpo
                                    where mpo.PurchaseOrderId = @xmpoid;

                                    select mpox.* from crm.MasterPurchaseOrderExtn mpox
                                    where mpox.PurchaseOrderId = @xmpoid; 

                                    select fv.VendorId, fv.BPAccountNo, hv.IsAlliance, case when fv.VendorType = 3 then fv.name
                                    else hv.name end VendorName
                                    from acct.FranchiseVendors fv
                                    left join crm.MasterPurchaseOrder mpo on mpo.VendorId = fv.VendorId
                                    left join acct.HFCVendors hv on hv.VendorId = fv.VendorId
                                    where mpo.PurchaseOrderId = @xmpoid and fv.FranchiseId = @franchiseid

                                    select* from crm.QuoteLines ql
                                    left join crm.MasterPurchaseOrderExtn mpox on mpox.QuoteId = ql.QuoteKey
                                    left join crm.MasterPurchaseOrder mpo on mpo.VendorId = ql.VendorId
                                    where mpox.PurchaseOrderId = @xmpoid
                                    and mpo.PurchaseOrderId = @xmpoid
                                    order by ql.VendorId 

                                    select pod.* from crm.PurchaseOrderDetails pod
                                    where pod.PurchaseOrderId = @xmpoid";

                    var tresult = connection.QueryMultiple(query
                        , new { xmpoid = xmpoId, franchiseid = franchise.FranchiseId });
                    var mpo = tresult.Read<MasterPurchaseOrder>().FirstOrDefault();
                    var mpox = tresult.Read<MasterPurchaseOrderExtn>().ToList();
                    var fvendor = tresult.Read<FranchiseVendor>().FirstOrDefault();
                    var quotelines = tresult.Read<QuoteLines>().ToList();
                    var pods = tresult.Read<PurchaseOrderDetails>().ToList();

                    if (!fvendor.IsAlliance) throw new Exception("The vendor should be a PIC enabled Alliance Vendor: " + fvendor.VendorName);
                    if (fvendor.BPAccountNo == null) throw new Exception("There is no account information available for Vendor: " + fvendor.VendorName);

                    dynamic order = new ExpandoObject();
                    order.Date = DateTime.Now.ToString("yyyy-MM-dd");
                    if (SessionManager.CurrentFranchise != null)
                    {
                        order.Customer = SessionManager.CurrentFranchise.Code;
                        order.shName = SessionManager.CurrentFranchise.Name;
                        order.inName = order.shName;
                        order.shPhone = SessionManager.CurrentFranchise.LocalPhoneNumber;
                        order.shFax = SessionManager.CurrentFranchise.FaxNumber;
                        order.contact = SessionManager.CurrentFranchise.Name;
                        order.addressType = SessionManager.CurrentFranchise.Address.IsBusiness ? "C" : "R";
                    }

                    // TODO: what happens when the current franchise is null?

                    order.po = mpo.MasterPONumber;
                    order.shAddress1 = address.Address1;
                    order.shAddress2 = address.Address2 != null ? address.Address2 : "";
                    order.shAddress3 = "";
                    order.shAddress4 = "";
                    order.shCity = address.City;
                    order.shState = address.State;
                    order.shZip = address.ZipCode;
                    order.shCountry = address.CountryCode2Digits;

                    order.inAddress1 = address.Address1;
                    order.inAddress2 = address.Address2 != null ? address.Address2 : "";
                    order.inAddress3 = "";
                    order.inAddress4 = "";
                    order.inCity = address.City;
                    order.inState = address.State;
                    order.inZip = address.ZipCode;
                    order.inCountry = address.CountryCode2Digits;

                    order.shipVia = "";
                    order.site = "1";

                    order.notes = "";
                    order.comm = "";
                    order.source = "";
                    // TODO sidemark
                    order.sideMark = "x - " + mpo.MasterPONumber;

                    var orderItems = new List<ExpandoObject>();

                    foreach (var qline in quotelines)
                    {
                        // The product type should be a Core Product.
                        if (qline.ProductTypeId == 1)
                        {
                            dynamic orderItem = new ExpandoObject();
                            dynamic picJson = JsonConvert.DeserializeObject<dynamic>(qline.PICJson);

                            orderItem.item = qline.QuoteLineId;
                            var pod = pods.Where(x => x.QuoteLineId == qline.QuoteLineId).FirstOrDefault();
                            if (pod != null)
                                orderItem.description = pod.NotesVendor;
                            orderItem.gGroup = picJson.GGroup;
                            orderItem.product = picJson.PicProduct;
                            orderItem.model = picJson.ModelId;
                            orderItem.stock = 0;
                            orderItem.color = "";
                            orderItem.qty = qline.Quantity;
                            orderItem.ediInfo1 = fvendor.BPAccountNo;
                            orderItem.promptAnswers = JsonConvert.DeserializeObject(picJson.PromptAnswers.ToString());
                            orderItem.reliefPurchase = "";
                            ///passing ediCustomerItem so that the value can be retrieved for down the line in SHipping notice and vendor Invoice
                            orderItem.ediCustomerItem = qline.QuoteLineId;

                            orderItems.Add(orderItem);
                        }
                    }

                    dynamic salesOrder = new ExpandoObject();
                    salesOrder.Order = order;
                    salesOrder.Items = orderItems;

                    return salesOrder;

                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    throw e;
                }
                finally
                {
                    connection.Close();
                }
            }
        }


        /// <summary>
        /// Create a sales object to convert sales order to MPO
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public dynamic GetOrderObject(int orderId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    int? franchiseId;
                    var orderobj = connection.Query<Orders>("select * from crm.orders where OrderId = @orderId", new { orderId = orderId }).FirstOrDefault();
                    var quoteKey = orderobj.QuoteKey;
                    var quote = connection.Query<Quote>("select * from [CRM].[Quote] where [QuoteKey] = @quoteKey", new { quoteKey = quoteKey }).FirstOrDefault();
                    var quoteLines = connection.Query<QuoteLines>("select ql.* from crm.quotelines ql where ql.quotekey = @quoteKey ORDER BY ql.QuoteLineNumber ASC", new { quoteKey = quoteKey }).ToList();
                    var pCategogories = connection.Query<dynamic>("select * from [CRM].[Type_ProductCategory]").ToList();
                    var Opportunity = connection.Query<Opportunity>("select * from crm.Opportunities where OpportunityId = @OpportunityId;", new { OpportunityId = quote.OpportunityId }).FirstOrDefault();
                    if (SessionManager.CurrentFranchise == null)
                    {
                        franchiseId = Opportunity.FranchiseId;
                    }
                    else
                    {
                        franchiseId = SessionManager.CurrentFranchise.FranchiseId;
                    }
                    var vendors = connection.Query<FranchiseVendor>(@"SELECT fv.*,h.Name AS VendorName,h.IsAlliance FROM acct.FranchiseVendors fv INNER JOIN acct.HFCVendors h ON h.vendorid = fv.VendorId AND fv.VendorType = 1 WHERE fv.FranchiseId = @FranchiseId; ", new { FranchiseId = franchiseId }).ToList();

                    var vta = connection
                        .Query<VendorTerritoryAccount>(
                            "SELECT* FROM crm.VendorTerritoryAccount vta WHERE vta.FranchiseId = @FranchiseId;",
                            new { FranchiseId = franchiseId }).ToList();
                    var address = connection.Query<AddressTP>(@"SELECT a.* FROM crm.MasterPurchaseOrder mpo
                                                                left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                                                INNER JOIN crm.Addresses a ON a.AddressId = mpo.ShipAddressId
                                                                INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
                                                                WHERE mpox.OrderId = @OrderId  AND o.FranchiseId = @FranchiseId; ", new { OrderId = orderId, FranchiseId = franchiseId }).FirstOrDefault();

                    var mpo = connection.Query<MasterPurchaseOrder>(@"SELECT mpox.AccountId, mpox.OpportunityId, mpox.QuoteId, mpox.OrderId, mpo.* FROM crm.MasterPurchaseOrder mpo
                                                                      left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                                                      INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
                                                                      left JOIN crm.Addresses a ON a.AddressId = mpo.ShipAddressId
                                                                      WHERE mpox.OrderId = @OrderId  AND o.FranchiseId = @FranchiseId; ", new { OrderId = orderId, FranchiseId = franchiseId }).FirstOrDefault();

                    var podDetails = connection.Query<PurchaseOrderDetails>(@"SELECT ql.QuoteLineNumber as LineNumber, mpox.AccountId, mpox.OpportunityId, mpox.QuoteId, mpox.OrderId, * FROM crm.MasterPurchaseOrder mpo
                                                                            left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                                                            INNER JOIN crm.PurchaseOrderDetails pod ON pod.PurchaseOrderId = mpo.PurchaseOrderId
                                                                            INNER JOIN crm.Addresses a ON a.AddressId = mpo.ShipAddressId
                                                                            INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
																			INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
                                                                              WHERE mpox.OrderId = @OrderId  AND o.FranchiseId = @FranchiseId;  ",
                        new { OrderId = orderId, FranchiseId = franchiseId }).ToList();

                    var stlm = connection
                        .Query<ShipToLocationsMap>(
                            "SELECT * FROM crm.ShipToLocationsMap stlm WHERE stlm.FranchiseId = @FranchiseId;",
                            new { FranchiseId = franchiseId }).ToList();

                    //var franchise = SessionManager.CurrentFranchise;

                    if (address == null)
                    {
                        throw new Exception("Please select shipping address.");
                    }

                    dynamic salesOrder = new ExpandoObject();
                    dynamic order = new ExpandoObject();
                    var _orderitems = new List<ExpandoObject>();

                    order.Date = DateTime.Now.ToString("yyyy-MM-dd");
                    if (SessionManager.CurrentFranchise != null)
                    {
                        order.Customer = SessionManager.CurrentFranchise.Code;
                        order.shName = SessionManager.CurrentFranchise.Name;
                        order.inName = SessionManager.CurrentFranchise.Name;
                        order.shPhone = SessionManager.CurrentFranchise.LocalPhoneNumber;
                        order.shFax = SessionManager.CurrentFranchise.FaxNumber;
                        order.contact = SessionManager.CurrentFranchise.Name;
                        order.addressType = SessionManager.CurrentFranchise.Address.IsBusiness ? "C" : "R";
                    }

                    if (quote != null && !string.IsNullOrEmpty(quote.SideMark))
                    {
                        order.sideMark = quote.SideMark;
                    }
                    else if (!string.IsNullOrEmpty(Opportunity.SideMark))
                    {
                        order.sideMark = Opportunity.SideMark;
                    }
                    else
                    {
                        order.sideMark = quote.QuoteName;
                    }

                    if (mpo != null)
                    {
                        order.po = mpo.MasterPONumber;
                        if (mpo.DropShip)
                        {
                            order.dropShip = "Y";
                        }
                        else
                        {
                            order.dropShip = "N";
                        }
                    }
                    else
                    {
                        order.dropShip = "N";
                        order.po = "";
                    }

                    order.shAddress1 = address.Address1;
                    order.shAddress2 = address.Address2 != null ? address.Address2 : "";
                    order.shAddress3 = "";
                    order.shAddress4 = "";
                    order.shCity = address.City;
                    order.shState = address.State;
                    order.shZip = address.ZipCode;
                    order.shCountry = address.CountryCode2Digits;

                    order.inAddress1 = address.Address1;
                    order.inAddress2 = address.Address2 != null ? address.Address2 : "";
                    order.inAddress3 = "";
                    order.inAddress4 = "";
                    order.inCity = address.City;
                    order.inState = address.State;
                    order.inZip = address.ZipCode;
                    order.inCountry = address.CountryCode2Digits;

                    order.shipVia = "";
                    order.site = "1";

                    order.notes = "";
                    order.comm = "";
                    order.source = "";

                    if (quoteLines != null && quoteLines.Count() > 0)
                    {
                        foreach (var item in quoteLines)
                        {
                            var ven = vendors.Where(x => x.VendorId == item.VendorId).FirstOrDefault();
                            var pod = podDetails.Where(x => x.QuoteLineId == item.QuoteLineId).FirstOrDefault();
                            if (item.ProductTypeId == 1 && ven.IsAlliance && pod.StatusId != 8)
                            {
                                //TODO: Change with the new changes
                                var terrid = Opportunity.TerritoryId;
                                if (terrid == null)
                                {
                                    if (!mpo.DropShip)
                                    {
                                        if (stlm.Where(x => x.Id == mpo.ShipToLocation).FirstOrDefault() != null)
                                        {
                                            terrid = stlm.Where(x => x.Id == mpo.ShipToLocation).FirstOrDefault()
                                                .TerritoryId;
                                        }
                                        else
                                        {
                                            terrid = stlm.FirstOrDefault().TerritoryId;
                                        }
                                    }
                                    else
                                    {
                                        terrid = stlm.FirstOrDefault().TerritoryId;
                                    }
                                }

                                var actven = vta.Where(x =>
                                        x.VendorId == item.VendorId && x.TerritoryId == terrid)
                                    .FirstOrDefault();

                                if (actven == null)
                                {
                                    throw new Exception("There is no account information available for Vendor: " + ven.VendorName);
                                }
                                dynamic orderItems = new ExpandoObject();

                                dynamic picJson = JsonConvert.DeserializeObject<dynamic>(item.PICJson);

                                orderItems.item = item.QuoteLineId;
                                if (pod != null)
                                {
                                    orderItems.description = pod.NotesVendor;
                                }

                                orderItems.gGroup = picJson.GGroup;
                                orderItems.product = picJson.PicProduct;
                                orderItems.model = picJson.ModelId;
                                orderItems.stock = 0;
                                orderItems.color = "";
                                orderItems.qty = item.Quantity;
                                orderItems.ediInfo1 = actven.AccountNumber.Trim();
                                //orderItems.ediInfo1 = vendors
                                //    .Where(x => x.VendorId == item.VendorId && x.VendorStatus == 1).FirstOrDefault()
                                //    .AccountNo.ToString();

                                orderItems.promptAnswers = JsonConvert.DeserializeObject(picJson.PromptAnswers.ToString());
                                orderItems.reliefPurchase = "";
                                ///passing ediCustomerItem so that the value can be retrieved for down the line in SHipping notice and vendor Invoice
                                orderItems.ediCustomerItem = item.QuoteLineId;

                                _orderitems.Add(orderItems);
                            }
                        }
                    }
                    salesOrder.Order = order;
                    salesOrder.Items = _orderitems;

                    return salesOrder;
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    throw e;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool SyncVPOStatusByMPO(int mpoId, int? FranchiseID)
        {
            var dateTime = DateTime.UtcNow;
            var usr = 1107554;
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var fQuery = @"SELECT ql.QuoteLineNumber as LineNumber,pod.*,ql.* FROM crm.PurchaseOrderDetails pod
                               INNER JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = pod.PurchaseOrderId
                               left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
                               INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
                               WHERE ql.ProductTypeId = 1 AND pod.StatusId NOT IN (7,8)  AND o.FranchiseId = @FranchiseId AND pod.PurchaseOrderId = @PurchaseOrderId;";
                    connection.Open();
                    var ids = new[] { 5, 7, 8 };
                    var purchaseOrderDetials = connection.Query<PurchaseOrderDetails, QuoteLines, PurchaseOrderDetails>(
                            fQuery,
                            (po, qlines) =>
                            {
                                po.QuoteLines = qlines;
                                return po;
                            }, new { PurchaseOrderId = mpoId, FranchiseId = FranchiseID },
                            splitOn: "QuoteLineId")
                        .Distinct()
                        .ToList();

                    if (purchaseOrderDetials != null && purchaseOrderDetials.Count > 0)
                    {
                        var pogroup = purchaseOrderDetials.Where(a => a.PICPO.HasValue && !ids.Contains(a.StatusId)).GroupBy(x => x.PICPO).ToList();

                        foreach (var pogItem in pogroup)
                        {
                            if (pogItem.Key.HasValue && pogItem.Key.Value != 0)
                            {
                                var podjson = purchaseOrderDetials.Where(x => x.PICPO == pogItem.Key.Value).FirstOrDefault();

                                var porespJson = podjson != null ? podjson.POResponse : null;

                                var PICPOresponse = _picMgr.GetCurrentVPOStatus(pogItem.Key.Value, porespJson);
                                if (JsonConvert.DeserializeObject<dynamic>(PICPOresponse).valid.Value)
                                {
                                    var PICPODetails = JsonConvert.DeserializeObject<PICVPOObject>(PICPOresponse);
                                    if (PICPODetails.valid)
                                    {
                                        int po = 0;
                                        foreach (var item in PICPODetails.properties.Items)
                                        {
                                            var podetails = new PurchaseOrderDetails();

                                            if (item.ediCustomerItem.HasValue && item.ediCustomerItem.Value != 0)
                                            {
                                                podetails = purchaseOrderDetials.Where(x =>
                                                    x.PICPO == Convert.ToInt32(item.po) &&
                                                    x.QuoteLineId == item.ediCustomerItem.Value).FirstOrDefault();
                                            }
                                            else
                                            {
                                                podetails = purchaseOrderDetials.Where(x =>
                                                    x.PICPO == Convert.ToInt32(item.po) && JsonConvert.DeserializeObject<dynamic>(x.QuoteLines.PICJson).ModelId == item.model).FirstOrDefault();
                                            }
                                            if (podetails != null)
                                            {
                                                var current_poSTatus1 = ReturnPICPOStatusId(podetails.StatusId);
                                                var api_PoStatus1 = Convert.ToInt32(item.status);
                                                if (api_PoStatus1 >= current_poSTatus1)
                                                {
                                                    if (podetails != null)
                                                    {
                                                        podetails.POResponse = PICPOresponse;
                                                        podetails.PartNumber = item.product;
                                                        if (Convert.ToInt32(item.status) >= 15)
                                                        {
                                                            if (item.dueDate != null && item.dueDate != "0000-00-00" &&
                                                                item.dueDate != "")
                                                            {
                                                                podetails.ShipDate = Convert.ToDateTime(item.dueDate);
                                                                if (podetails.PromiseByDate == null)
                                                                {
                                                                    podetails.PromiseByDate = Convert.ToDateTime(item.dueDate);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                podetails.ShipDate = null;
                                                            }
                                                        }

                                                        podetails.Status = item.statusDescription;
                                                        podetails.VendorReference = PICPODetails.properties.PurchaseOrder.vendorReference;
                                                        podetails.StatusId = ReturnPOStatusId(Convert.ToInt32(item.status));
                                                        //item.PromiseByDate = Convert.ToDateTime(details.dateReturned);
                                                        podetails.VendorNotes = item.comment;
                                                        podetails.QuantityPurchased = Convert.ToInt32(Convert.ToDecimal(item.qtyOrdered));
                                                        podetails.ImpersonatorPersonId = this.ImpersonatorPersonId;
                                                        if (item.estShipDate != null && item.estShipDate != "0000-00-00" &&
                                                                item.estShipDate != "")
                                                        {
                                                            podetails.estShipDate = Convert.ToDateTime(item.estShipDate);
                                                        }

                                                        podetails.QTY = Convert.ToInt32(Convert.ToDecimal(item.qty));
                                                        podetails.AckCost = Convert.ToDecimal(item.AckCost);
                                                        podetails.Discount = Convert.ToDecimal(item.discGoods);
                                                        if (!string.IsNullOrEmpty(item.item))
                                                        {
                                                            podetails.item = Convert.ToInt32(item.item);
                                                        }
                                                        if (!string.IsNullOrEmpty(item.woItem))
                                                        {
                                                            podetails.woItem = Convert.ToInt32(item.woItem);
                                                        }
                                                        podetails.UoM = (string.IsNullOrEmpty(item.UoM) ? "EA" : item.UoM);

                                                        connection.Update(podetails);
                                                        //EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(podetails), PurchaseOrdersDetailId: podetails.PurchaseOrdersDetailId);
                                                        po = podetails.PurchaseOrderId;
                                                    }
                                                }
                                            }
                                        }

                                        if (po != 0)
                                        {
                                            AddUpdateVPO(po, PICPODetails.properties.PurchaseOrder, dateTime, usr);
                                            SetMasterPurchaseOrderStatus(po);
                                        }
                                    }
                                    else
                                    {
                                        EventLogger.LogEvent(JsonConvert.SerializeObject(PICPODetails.error));
                                    }
                                }
                            }
                        }
                    }

                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool SyncVPOStatus()
        {
            var dateTime = DateTime.UtcNow;
            var usr = 1107554;
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var fQuery = @"SELECT ql.QuoteLineNumber as LineNumber, pod.PurchaseOrdersDetailId,
                                   pod.PurchaseOrderId, pod.QuoteLineId, pod.PartNumber, pod.ShipDate, pod.Status, pod.PromiseByDate, pod.VendorNotes, pod.QuantityPurchased, pod.PickedBy, pod.PONumber,  pod.CreatedOn, pod.CreatedBy, pod.UpdatedOn, pod.UpdatedBy, pod.NotesVendor, pod.PICPO, pod.StatusId, pod.Errors, pod.vendorReference, pod.Information, pod.Warning, pod.CancelReason, pod.ImpersonatorPersonId,
                                   ql.QuoteLineNumber,ql.QuoteKey,ql.VendorId FROM crm.PurchaseOrderDetails pod
                                   INNER JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = pod.PurchaseOrderId
                                   INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
                                    WHERE ql.ProductTypeId = 1 AND (pod.statusid in (0,2,3,4,6,9,11) OR pod.STATUS like 'entered')";

                    var sDetailsQuery = @"SELECT * FROM crm.ShipNotice sn
                                   WHERE sn.PICVpo =@PICVpo;
                                   SELECT * FROM crm.ShipmentDetail sd
                                   INNER JOIN crm.ShipNotice sn ON sn.ShipNoticeId = sd.ShipNoticeId
                                   WHERE sn.PICVpo =@PICVpo;";

                    connection.Open();

                    var purchaseOrderDetials = connection.Query<PurchaseOrderDetails>(
                         fQuery)
                     .Distinct()
                     .ToList();

                    var quotelineList = connection.Query<QuoteLines>(
                           fQuery).ToList();
                    foreach (var item in purchaseOrderDetials)
                    {
                        item.QuoteLines = quotelineList.Where(x => x.QuoteLineId == item.QuoteLineId).FirstOrDefault();
                    }

                    if (purchaseOrderDetials != null && purchaseOrderDetials.Count > 0)
                    {
                        var pogroup = purchaseOrderDetials.Where(a => a.PICPO.HasValue).GroupBy(x => x.PICPO).ToList();

                        foreach (var pogItem in pogroup)
                        {
                            try
                            {
                                if (pogItem.Key.HasValue && pogItem.Key.Value != 0)
                                {
                                    var podjson = purchaseOrderDetials.Where(x => x.PICPO == pogItem.Key.Value).FirstOrDefault();

                                    var porespJson = podjson != null ? podjson.POResponse : null;

                                    var PICPOresponse = _picMgr.GetCurrentVPOStatus(pogItem.Key.Value, porespJson);
                                    var PICPODetails = JsonConvert.DeserializeObject<PICVPOObject>(PICPOresponse);

                                    if (PICPODetails.valid)
                                    {
                                        int po = 0;
                                        foreach (var item in PICPODetails.properties.Items)
                                        {
                                            var podetails = new PurchaseOrderDetails();

                                            if (item.ediCustomerItem.HasValue && item.ediCustomerItem.Value != 0)
                                            {
                                                podetails = purchaseOrderDetials.Where(x =>
                                                    x.PICPO == Convert.ToInt32(item.po) &&
                                                    x.QuoteLineId == item.ediCustomerItem.Value).FirstOrDefault();

                                                //Get current status
                                                var current_poSTatus = ReturnPICPOStatusId(podetails.StatusId);
                                                var api_PoStatus = Convert.ToInt32(item.status);
                                                if (api_PoStatus >= current_poSTatus)
                                                {
                                                    if (podetails != null)
                                                    {
                                                        podetails.POResponse = PICPOresponse;
                                                        podetails.PartNumber = item.product;
                                                        if (Convert.ToInt32(item.status) >= 15)
                                                        {
                                                            if (item.dueDate != null && item.dueDate != "0000-00-00" &&
                                                                item.dueDate != "")
                                                            {
                                                                podetails.ShipDate = Convert.ToDateTime(item.dueDate);
                                                                if (podetails.PromiseByDate == null)
                                                                {
                                                                    podetails.PromiseByDate = Convert.ToDateTime(item.dueDate);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                podetails.ShipDate = null;
                                                            }
                                                        }
                                                        var status = (item.statusDescription != podetails.Status && item.statusDescription == "FULL SHIPPED");
                                                        podetails.Status = item.statusDescription;
                                                        podetails.VendorReference = PICPODetails.properties.PurchaseOrder.vendorReference;
                                                        podetails.StatusId = ReturnPOStatusId(Convert.ToInt32(item.status));
                                                        //item.PromiseByDate = Convert.ToDateTime(details.dateReturned);
                                                        podetails.VendorNotes = item.comment;
                                                        podetails.QuantityPurchased = Convert.ToInt32(Convert.ToDecimal(item.qtyOrdered));
                                                        podetails.UpdatedOn = dateTime;
                                                        podetails.ImpersonatorPersonId = this.ImpersonatorPersonId;
                                                        if (item.estShipDate != null && item.estShipDate != "0000-00-00" &&
                                                                item.estShipDate != "")
                                                        {
                                                            podetails.estShipDate = Convert.ToDateTime(item.estShipDate);
                                                        }
                                                        podetails.QTY = Convert.ToInt32(Convert.ToDecimal(item.qty));
                                                        podetails.AckCost = Convert.ToDecimal(item.AckCost);
                                                        podetails.Discount = Convert.ToDecimal(item.discGoods);
                                                        if (!string.IsNullOrEmpty(item.item))
                                                        {
                                                            podetails.item = Convert.ToInt32(item.item);
                                                        }
                                                        if (!string.IsNullOrEmpty(item.woItem))
                                                        {
                                                            podetails.woItem = Convert.ToInt32(item.woItem);
                                                        }
                                                        podetails.UoM = (string.IsNullOrEmpty(item.UoM) ? "EA" : item.UoM);

                                                        connection.Update(podetails);

                                                        if (status)
                                                        {
                                                            var sndetails = connection.QueryMultiple(sDetailsQuery, new { PICVpo = pogItem.Key.Value });

                                                            var alllistSN = sndetails.Read<ShipNotice>().ToList();
                                                            var alllistSD = sndetails.Read<ShipmentDetail>().ToList();

                                                            var itemList = purchaseOrderDetials.Where(x => x.PICPO == pogItem.Key.Value).ToList();
                                                            var snjson = alllistSN.Where(x => x.PICVpo == pogItem.Key.Value.ToString()).FirstOrDefault();

                                                            var snrespJson = snjson != null ? snjson.ShipNoticeJson : null;

                                                            var listSN = alllistSN.Where(x => x.PICVpo == pogItem.Key.Value.ToString()).ToList();
                                                            var listSD = alllistSD.Where(x => x.PICVpo == pogItem.Key.Value).ToList();

                                                            var response = _picMgr.GetShippingNotice(pogItem.Key.Value, snrespJson);
                                                            var result = JsonConvert.DeserializeObject<SNRootObject>(response);

                                                            if (result.valid)
                                                            {
                                                                AddUpdateShipNotice(response, itemList, listSN, dateTime, usr, pogItem.Key.Value);
                                                            }
                                                        }
                                                        //EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(podetails), PurchaseOrdersDetailId: podetails.PurchaseOrdersDetailId);
                                                        po = podetails.PurchaseOrderId;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                //podetails = purchaseOrderDetials.Where(x =>
                                                //    x.PICPO == Convert.ToInt32(item.po) && JsonConvert.DeserializeObject<dynamic>(x.QuoteLines.PICJson).ModelId == item.model).FirstOrDefault();

                                                string query = "SELECT * FROM crm.QuoteLines ql WHERE ql.QuoteLineId IN @QuoteLineIds;";
                                                var quotelineids = purchaseOrderDetials.Where(x => x.PICPO == Convert.ToInt32(item.po)).ToList().Select(x => x.QuoteLineId).ToList();

                                                var poquotelineList = connection.Query<QuoteLines>(
                                                       query, new { QuoteLineIds = quotelineids }).ToList();

                                                var lispodetails = purchaseOrderDetials.Where(x =>
                                                    x.PICPO == Convert.ToInt32(item.po) &&
                                                    JsonConvert.DeserializeObject<dynamic>((poquotelineList.Where(a => a.QuoteLineId == x.QuoteLineId).FirstOrDefault()).PICJson).ModelId ==
                                                    item.model).ToList();

                                                if (lispodetails != null && lispodetails.Count > 0)
                                                {
                                                    foreach (var poItem in lispodetails)
                                                    {
                                                        var current_poSTatus1 = ReturnPICPOStatusId(poItem.StatusId);
                                                        var api_PoStatus1 = Convert.ToInt32(item.status);
                                                        if (api_PoStatus1 >= current_poSTatus1)
                                                        {
                                                            poItem.POResponse = PICPOresponse;
                                                            poItem.PartNumber = item.product;
                                                            if (Convert.ToInt32(item.status) >= 15)
                                                            {
                                                                if (item.dueDate != null && item.dueDate != "0000-00-00" &&
                                                                    item.dueDate != "")
                                                                {
                                                                    poItem.ShipDate = Convert.ToDateTime(item.dueDate);
                                                                    if (poItem.PromiseByDate == null)
                                                                    {
                                                                        poItem.PromiseByDate = Convert.ToDateTime(item.dueDate);
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    poItem.ShipDate = null;
                                                                }
                                                            }
                                                            var status = (item.statusDescription != poItem.Status && item.statusDescription == "FULL SHIPPED");
                                                            poItem.Status = item.statusDescription;
                                                            poItem.VendorReference = PICPODetails.properties.PurchaseOrder.vendorReference;
                                                            poItem.StatusId = ReturnPOStatusId(Convert.ToInt32(item.status));
                                                            //item.PromiseByDate = Convert.ToDateTime(details.dateReturned);
                                                            poItem.VendorNotes = item.comment;
                                                            poItem.QuantityPurchased = Convert.ToInt32(Convert.ToDecimal(item.qtyOrdered));
                                                            poItem.UpdatedOn = dateTime;
                                                            poItem.ImpersonatorPersonId = this.ImpersonatorPersonId;
                                                            if (item.estShipDate != null && item.estShipDate != "0000-00-00" &&
                                                                item.estShipDate != "")
                                                            {
                                                                podetails.estShipDate = Convert.ToDateTime(item.estShipDate);
                                                            }
                                                            podetails.QTY = Convert.ToInt32(Convert.ToDecimal(item.qty));
                                                            podetails.AckCost = Convert.ToDecimal(item.AckCost);
                                                            podetails.Discount = Convert.ToDecimal(item.discGoods);
                                                            if (!string.IsNullOrEmpty(item.item))
                                                            {
                                                                podetails.item = Convert.ToInt32(item.item);
                                                            }
                                                            if (!string.IsNullOrEmpty(item.woItem))
                                                            {
                                                                podetails.woItem = Convert.ToInt32(item.woItem);
                                                            }
                                                            podetails.UoM = (string.IsNullOrEmpty(item.UoM) ? "EA" : item.UoM);

                                                            connection.Update(poItem);
                                                            if (status)
                                                            {
                                                                var sndetails = connection.QueryMultiple(sDetailsQuery, new { PICVpo = pogItem.Key.Value });

                                                                var alllistSN = sndetails.Read<ShipNotice>().ToList();
                                                                var alllistSD = sndetails.Read<ShipmentDetail>().ToList();

                                                                var itemList = purchaseOrderDetials.Where(x => x.PICPO == pogItem.Key.Value).ToList();
                                                                var snjson = alllistSN.Where(x => x.PICVpo == pogItem.Key.Value.ToString()).FirstOrDefault();

                                                                var snrespJson = snjson != null ? snjson.ShipNoticeJson : null;

                                                                var listSN = alllistSN.Where(x => x.PICVpo == pogItem.Key.Value.ToString()).ToList();
                                                                var listSD = alllistSD.Where(x => x.PICVpo == pogItem.Key.Value).ToList();

                                                                var response = _picMgr.GetShippingNotice(pogItem.Key.Value, snrespJson);
                                                                var result = JsonConvert.DeserializeObject<SNRootObject>(response);

                                                                if (result.valid)
                                                                {
                                                                    AddUpdateShipNotice(response, itemList, listSN, dateTime, usr, pogItem.Key.Value);
                                                                }
                                                            }
                                                            //EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(poItem), PurchaseOrdersDetailId: poItem.PurchaseOrdersDetailId);
                                                            po = poItem.PurchaseOrderId;
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        if (po != 0)
                                        {
                                            AddUpdateVPO(po, PICPODetails.properties.PurchaseOrder, dateTime, usr);
                                            SetMasterPurchaseOrderStatus(po);
                                        }
                                    }
                                    else
                                    {
                                        EventLogger.LogEvent(JsonConvert.SerializeObject(PICPODetails.error));
                                    }
                                }
                            }
                            catch (Exception exin)
                            {
                                EventLogger.LogEvent(exin);
                            }
                        }
                    }

                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool SetMasterPurchaseOrderStatus(int mPOId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var fQuery = @"  SELECT mpox.AccountId, mpox.OpportunityId, mpox.QuoteId, mpox.OrderId, mpo.* FROM crm.MasterPurchaseOrder mpo
                                    left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                     INNER JOIN crm.Opportunities o ON mpox.OpportunityId = o.OpportunityId
                                     WHERE mpo.PurchaseOrderId = @PurchaseOrderId ;

                                     SELECT ql.QuoteLineNumber as LineNumber,pod.*,ql.ProductTypeId FROM crm.PurchaseOrderDetails pod
                                     inner join crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = pod.PurchaseOrderId
                                     left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                     INNER JOIN crm.Opportunities o ON mpox.OpportunityId = o.OpportunityId
                                     INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
                                     WHERE  mpo.PurchaseOrderId = @PurchaseOrderId ";
                    connection.Open();

                    var fDetails = connection.QueryMultiple(fQuery, new { PurchaseOrderId = mPOId });

                    var mpoDetails = fDetails.Read<MasterPurchaseOrder>().FirstOrDefault();

                    var purchaseOrderDetials = fDetails.Read<PurchaseOrderDetails>().ToList();

                    var allStatus = new List<string>();
                    var allStatusId = new List<int>();
                    bool allCoreVPOsCanceled = false;
                    foreach (var item in purchaseOrderDetials)
                    {
                        allStatus.Add(item.Status.ToLower().Trim());
                        allStatusId.Add(item.StatusId);
                        if (item.ProductTypeId == 1 || item.ProductTypeId == 2 || item.ProductTypeId == 5)
                        {
                            if (item.StatusId == (int)PurchaseOrderStatus.Canceled)
                            {
                                allCoreVPOsCanceled = true;
                            }
                            else
                            {
                                allCoreVPOsCanceled = false;
                            }
                        }
                    }

                    ///This is the order of status MPO needs to be in
                    //10  Error
                    //1   Open
                    //2   Submitted
                    //9   Processing
                    //11  ACKNOWLEDGED
                    //4   Partial Shipped Back Ordered
                    //3   Shipped
                    //5   Invoiced
                    //7   Closed
                    //8   Canceled
                    if (allStatusId.Contains(10))
                    {
                        mpoDetails.POStatusId = 10;
                    }
                    else if (allStatusId.Contains(1))
                    {
                        mpoDetails.POStatusId = 1;
                    }
                    else if (allStatusId.Contains(2))
                    {
                        mpoDetails.POStatusId = 2;
                    }
                    else if (allStatusId.Contains(9))
                    {
                        mpoDetails.POStatusId = 9;
                    }
                    else if (allStatusId.Contains(11))
                    {
                        mpoDetails.POStatusId = 11;
                    }
                    else if (allStatusId.Contains(4))
                    {
                        mpoDetails.POStatusId = 4;
                    }
                    else if (allStatusId.Contains(3))
                    {
                        mpoDetails.POStatusId = 3;
                    }
                    else if (allStatusId.Contains(5))
                    {
                        mpoDetails.POStatusId = 5;
                    }
                    else if (allStatusId.Contains(7))
                    {
                        mpoDetails.POStatusId = 7;
                    }
                    else if (allStatusId.Contains(8))
                    {
                        mpoDetails.POStatusId = 8;
                    }

                    ////If any Vendor PO status = Acknowledged
                    //if (allStatus.Contains(VPOPICStatus.Acknowledged.ToLower().Trim()))
                    //{
                    //    //Acknowledged
                    //    mpoDetails.POStatusId = 11;
                    //}

                    //else if (allStatusId.Contains(8)) ///Set the cancelled Status if all the items are set to cancel status
                    //{
                    //    mpoDetails.POStatusId = (int)PurchaseOrderStatus.Canceled;
                    //    mpoDetails.Status = Enum.GetName(typeof(PurchaseOrderStatus), 8);
                    //}
                    ////If any Vendor PO status = “Partial Shipped/Backordered”, MPO status is “Partial Shipped Back Ordered.”
                    //else if (allStatus.Contains(VPOPICStatus.PartialShipped.ToLower().Trim()))
                    //{
                    //    //Partial Shipped
                    //    mpoDetails.POStatusId = 4;
                    //}
                    ////If Vendor PO status is not Partial Shipped/Backordered, and any Vendor PO status is “Submitted” MPO status is “Submitted”
                    //else if (!allStatus.Contains(VPOPICStatus.PartialShipped.ToLower().Trim()) && allStatus.Contains(VPOPICStatus.Entered))
                    //{
                    //    //Submitted
                    //    mpoDetails.POStatusId = 2;
                    //}
                    ////If Vendor PO status is not Partial Shipped/Backordered or Submitted, and any Vendor PO status is “open,” MPO status is “Open”
                    //else if (allStatus.Contains(VPOPICStatus.Entered) && !allStatus.Contains(VPOPICStatus.PartialShipped.ToLower().Trim()))
                    //{
                    //    //Open
                    //    mpoDetails.POStatusId = 1;
                    //}//If Vendor PO status is not Partial Shipped/Backordered or Submitted or Open and any Vendor PO status is “Shipped,” MPO status is “Shipped”
                    //else if (!allStatus.Contains("open") && !allStatus.Contains(VPOPICStatus.PartialShipped.ToLower().Trim()) && allStatus.Contains(VPOPICStatus.FullShipped.ToLower().Trim()))
                    //{
                    //    //Shipped
                    //    mpoDetails.POStatusId = 3;
                    //}
                    ////If Vendor PO status is not Partial Shipped/Backordered or Submitted or Open or Shipped, and any Vendor PO status is “invoiced, “ MPO status is “Invoiced”
                    //else if (allStatus.Contains(VPOPICStatus.Invoiced) && !allStatus.Contains(VPOPICStatus.PartialShipped.ToLower().Trim()) && allStatus.Contains(VPOPICStatus.FullShipped.ToLower().Trim()))
                    //{
                    //    //Invoiced
                    //    mpoDetails.POStatusId = 5;
                    //}

                    /////Cancelled Status
                    //if (allStatusId.Contains(8))
                    //{
                    //    mpoDetails.POStatusId = (int)PurchaseOrderStatus.Canceled;
                    //    mpoDetails.Status = Enum.GetName(typeof(PurchaseOrderStatus), 8);
                    //}
                    mpoDetails.ImpersonatorPersonId = this.ImpersonatorPersonId;
                    connection.Update(mpoDetails);
                    //EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(mpoDetails), PurchaseOrderId: mpoDetails.PurchaseOrderId);
                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public string GetMPOStatus(int statusId)
        {
            string status = "";

            switch (statusId)
            {
                case 40:
                    status = "POs Sent to Vendors";
                    break;

                case 70:
                    status = "PARTIAL SHIPPED";
                    break;

                case 71:
                    status = "FULL SHIPPED";
                    break;
                case 0:
                    status = "Entered";
                    break;
            }
            return status;
        }

        //[Description("Open")]
        //Open=1,
        //    [Description("Submitted")]
        //Submitted=2,
        //    [Description("Shipped")]
        //Shipped=3,
        //    [Description("Partial Shipped Back Ordered")]
        //Partial_Shipped_Back_Ordered=4,
        //    [Description("Invoiced")]
        //Invoiced=5,
        //    [Description("Printed")]
        //Printed=6,
        //    [Description("Closed")]
        //Closed=7,
        //    [Description("Canceled")]
        //Canceled=8,
        //    [Description("Processing")]
        //Processing = 9,
        //    [Description("Error")]
        //Error = 10,
        //    [Description("ACKNOWLEDGED")]
        //ACKNOWLEDGED = 11
        public int ReturnPICPOStatusId(int status)
        {
            int statusid = 0;
            //string status = "ENTERED";

            switch (status)
            {
                //FULL Shipped
                case 3:
                    statusid = 30;
                    break;
                // Partial Shipped
                case 4:
                    statusid = 20;
                    break;
                //Invoiced
                case 5:
                    statusid = 40;
                    break;
                //ACKNOWLEDGED
                case 11:
                    statusid = 15;
                    break;
            }
            return statusid;
        }

        public string ReturnPOStatus(int statusId)
        {
            string status = "ENTERED";

            switch (statusId)
            {
                case 0:
                    status = "ENTERED";
                    break;

                case 10:
                    status = "SENT TO VENDOR";
                    break;

                case 20:
                    status = "PARTIAL SHIPPED/ BACKORDERED";
                    break;

                case 30:
                    status = "FULL SHIPPED";
                    break;

                case 40:
                    status = "INVOICED";
                    break;

                case 15:
                    status = "ACKNOWLEDGED";
                    break;
            }
            return status;
        }

        public int ReturnPOStatusId(int statusId)
        {
            int status = 1;

            switch (statusId)
            {
                case 0:
                    status = 1;
                    break;

                case 10:
                    status = 2;
                    break;

                case 20:
                    status = 4;
                    break;

                case 30:
                    status = 3;
                    break;

                case 40:
                    status = 5;
                    break;

                case 15:
                    status = 11;
                    break;
            }
            return status;
        }

        public bool SaveNotesToVendor(VPODetail model)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var fQuery = @"SELECT ql.QuoteLineNumber as LineNumber,mpox.AccountId, mpox.OpportunityId, mpox.QuoteId, mpox.OrderId, * FROM crm.PurchaseOrderDetails pod
                                   INNER JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = pod.PurchaseOrderId
                                    left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId                                   
                                    INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
                                   INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
                                   WHERE pod.PurchaseOrdersDetailId = @PurchaseOrdersDetailId AND o.FranchiseId = @FranchiseId;";
                    connection.Open();

                    var fDetails = connection.QueryMultiple(fQuery, new { PurchaseOrdersDetailId = model.PurchaseOrdersDetailId, FranchiseId = SessionManager.CurrentFranchise.FranchiseId });

                    var purchaseOrderDetials = fDetails.Read<PurchaseOrderDetails>().FirstOrDefault();

                    purchaseOrderDetials.NotesVendor = model.NotesVendor;

                    purchaseOrderDetials.UpdatedBy = SessionManager.CurrentUser.PersonId;
                    purchaseOrderDetials.UpdatedOn = DateTime.UtcNow;
                    purchaseOrderDetials.ImpersonatorPersonId = this.ImpersonatorPersonId;
                    connection.Update(purchaseOrderDetials);
                    //EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(purchaseOrderDetials), PurchaseOrdersDetailId: purchaseOrderDetials.PurchaseOrdersDetailId);
                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool SaveMyProductInlineVPO(VPODetail model)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var fQuery = @"SELECT ql.QuoteLineNumber as LineNumber, mpox.AccountId, mpox.OpportunityId, mpox.QuoteId, mpox.OrderId,* FROM crm.PurchaseOrderDetails pod
                                   INNER JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = pod.PurchaseOrderId
                                    left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId          
                                    INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
                                   INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
                                   WHERE pod.PurchaseOrdersDetailId = @PurchaseOrdersDetailId AND o.FranchiseId = @FranchiseId;";
                    connection.Open();

                    var fDetails = connection.QueryMultiple(fQuery, new { PurchaseOrdersDetailId = model.PurchaseOrdersDetailId, FranchiseId = SessionManager.CurrentFranchise.FranchiseId });

                    var purchaseOrderDetials = fDetails.Read<PurchaseOrderDetails>().FirstOrDefault();

                    purchaseOrderDetials.Status = model.Status;

                    if (model.Status.ToLower() == "canceled")
                    {
                        purchaseOrderDetials.StatusId = 8;
                    }
                    else if (model.Status.ToLower() == "closed")
                    {
                        purchaseOrderDetials.StatusId = 7;
                    }
                    else
                    {
                        purchaseOrderDetials.StatusId = 1;
                    }

                    purchaseOrderDetials.QuantityPurchased = model.purchased;
                    purchaseOrderDetials.PickedBy = model.PickedBy;
                    purchaseOrderDetials.ShipDate = model.ShipDate;
                    purchaseOrderDetials.UpdatedBy = SessionManager.CurrentUser.PersonId;
                    purchaseOrderDetials.UpdatedOn = DateTime.UtcNow;
                    purchaseOrderDetials.ImpersonatorPersonId = this.ImpersonatorPersonId;
                    connection.Update(purchaseOrderDetials);
                    //EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(purchaseOrderDetials), PurchaseOrdersDetailId: purchaseOrderDetials.PurchaseOrdersDetailId);
                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool AddUpdateShipAddress(MasterPurchaseOrder model)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var usr = SessionManager.CurrentUser.PersonId;
                    var datetime = DateTime.UtcNow;
                    var fQuery = @"SELECT mpox.AccountId, mpox.OpportunityId, mpox.QuoteId, mpox.OrderId, mpo.*  from crm.MasterPurchaseOrder mpo
                                    left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                   INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
                                   WHERE mpo.PurchaseOrderId = @PurchaseOrderId AND o.FranchiseId = @FranchiseId;";
                    connection.Open();

                    //var fDetails = connection.QueryMultiple(fQuery, new { PurchaseOrderId = model.PurchaseOrderId, FranchiseId = SessionManager.CurrentFranchise.FranchiseId });

                    //var mpoObj = fDetails.Read<MasterPurchaseOrder>().FirstOrDefault();

                    if (model.DropShip)
                    {
                        if (model.ShipAddressId == null)
                        {
                            int addressId = connection.Insert<int, AddressTP>(model.DropShipAddress);
                            model.ShipAddressId = addressId;
                        }
                        else
                        {
                            model.DropShipAddress.LastUpdatedOn = datetime;
                            model.DropShipAddress.LastUpdatedBy = usr;
                            connection.Update(model.DropShipAddress);
                        }
                        model.Address = ((model.DropShipAddress.Address1 != null &&
                                          model.DropShipAddress.Address1 != string.Empty)
                                            ? model.DropShipAddress.Address1 + ", "
                                            : "") + ((model.DropShipAddress.Address2 != null &&
                                                      model.DropShipAddress.Address2 != string.Empty)
                                            ? model.DropShipAddress.Address2 + ", "
                                            : "") + ((model.DropShipAddress.City != null &&
                                                      model.DropShipAddress.City != string.Empty)
                                            ? model.DropShipAddress.City + ", "
                                            : "") + ((model.DropShipAddress.State != null &&
                                                      model.DropShipAddress.State != string.Empty)
                                            ? model.DropShipAddress.State + ", "
                                            : "") + ((model.DropShipAddress.ZipCode != null &&
                                                      model.DropShipAddress.ZipCode != string.Empty)
                                            ? model.DropShipAddress.ZipCode + ", "
                                            : "") + ((model.DropShipAddress.CountryCode2Digits != null &&
                                                      model.DropShipAddress.CountryCode2Digits != string.Empty)
                                            ? model.DropShipAddress.CountryCode2Digits
                                            : "");
                        //int addressId = connection.Insert<int, AddressTP>(model.DropShipAddress);
                        //model.ShipAddressId = addressId;
                        //mpoObj.DropShip = true;
                        model.ShipToLocation = null;
                    }
                    model.UpdateOn = datetime;
                    model.UpdatedBy = usr;
                    model.ConfirmAddress = true;
                    model.ImpersonatorPersonId = this.ImpersonatorPersonId;
                    connection.Update(model);
                    //EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(model), PurchaseOrderId: model.PurchaseOrderId);
                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool AddUpdateXmpoShipAddress(XMpoDTO value)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    MasterPurchaseOrder model = new MasterPurchaseOrder();
                    var fQuery = @"select * from CRM.MasterPurchaseOrder where PurchaseOrderId=@PurchaseOrderId";
                    connection.Open();

                    var fDetails = connection.Query<MasterPurchaseOrder>(fQuery, new { PurchaseOrderId = value.PurchaseOrderId, FranchiseId = Franchise.FranchiseId }).FirstOrDefault();
                    if (fDetails != null)
                    {
                        fDetails.DropShip = value.DropShip;
                        fDetails.ShipAddressId = value.ShipAddressId;
                        fDetails.DropShipAddress = value.DropShipAddress;
                        bool result = AddUpdateShipAddress(fDetails);
                        return result;
                    }
                    return false;

                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
            }

        }

        public bool CheckMPOCancelled(int PurchaseOrderId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var res = connection.Query<PurchaseOrderDetails>("select * from CRM.PurchaseOrderDetails where PurchaseOrderId=@id and StatusId not in (8,9,10)", new { id = PurchaseOrderId }).ToList();
                    if (res.Count > 0) return true; else return false;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally { connection.Close(); }
            }
        }

        public MasterPurchaseOrder GetMPOHeader(int purchaseOrderId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var fQuery = @"SELECT mpox.AccountId, mpox.OpportunityId, mpox.QuoteId, mpox.OrderId, mpo.* FROM crm.MasterPurchaseOrder mpo 
                                    left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                    where mpox.purchaseOrderId=@PurchaseOrderId";
                    var mpo = connection.QueryMultiple(fQuery, new { PurchaseOrderId = purchaseOrderId }).Read<MasterPurchaseOrder>().FirstOrDefault();
                    return mpo;
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }


        public IList<OrderBulkPurchaseDTO> GetSalesOrdersByXMpo(int xmpoId)
        {
            var result = new List<OrderBulkPurchaseDTO>();
            var query = @"select o.OrderID, o.OrderNumber, o.OrderName 
                    , o.ProductSubtotal TotalCost
                    , o.OrderTotal TotalSales
                    , (select sum(isnull(Quantity, 0)) from crm.QuoteLines where QuoteKey = o.QuoteKey) TotalQuantity 
                    , o.QuoteKey
                    from crm.Orders o
                    left join crm.MasterPurchaseOrderExtn mpox on mpox.OrderId = o.OrderId
                    where mpox.PurchaseOrderId = @xmpoid;

                    select ql.QuoteLineId, ql.QuoteKey, ql.Quantity, ql.RoomName
                    , ql.VendorId
                    , [CRM].[fnGetVendorNameByVendorId](ql.VendorId, @franchiseId) VendorName  --ql.VendorName
                    , ql.ProductName, ql.Width, ql.Height, ql.MountType
                    , ql.Color, ql.Fabric, ql.ExtendedPrice--, ql.UnitPrice Cost
                    , q.QuoteID 
                    , isnull(ql.FranctionalValueWidth, '') FranctionalValueWidth
                    , isnull(ql.FranctionalValueHeight, '') FranctionalValueHeight
                    , (qld.BaseCost * qld.Quantity) Cost, (qld.NetCharge) Sales
                    from crm.QuoteLines ql 
                    left join crm.Quote q on q.QuoteKey = ql.QuoteKey
                    left join crm.QuoteLineDetail qld on qld.QuoteLineId = ql.QuoteLineId
                    where ql.QuoteKey in (
                    select o.QuoteKey
                    from crm.Orders o
                    left join crm.MasterPurchaseOrderExtn mpox on mpox.OrderId = o.OrderId
                    where mpox.PurchaseOrderId = @xmpoid
                    )

                    Order by q.QuoteKey";

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                var tempResult = connection.QueryMultiple(query
                    , new
                    {
                        franchiseId = this.Franchise.FranchiseId
                        ,
                        xmpoid = xmpoId
                    });

                result = tempResult.Read<OrderBulkPurchaseDTO>().ToList();
                //var details = tempResult.Read<QuoteLines>().ToList();
                var details = tempResult.Read<QuoteLinesBulkPurchaseDTO>().ToList();

                foreach (var item in result)
                {
                    var temp = details.Where(x => x.QuoteKey == item.QuoteKey).ToList();
                    item.QuoteLines = temp;

                    item.TotalCost = temp.Sum(x => x.Cost);
                    item.TotalSales = temp.Sum(x => x.Sales);
                    item.TotalQuantity = temp.Sum(x => x.Quantity);
                }
                connection.Close();
            }

            return result;
        }


        public XMpoDTO GetXmpo(int xmpoId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var query = @"select mpo.PurchaseOrderId, mpo.CreateDate, mpo.SubmittedDate, mpo.POStatusId,mpo.MasterPONumber
                            , 111111 VendorPromoId, 'FranciseName' ShipToAccount, CancelReason
                            , ShipToLocation, mpo.VendorId,mpo.CancelReason as CancelReasonText
                            , case when  fv.VendorType = 1 then hv.Name
	                            else fv.Name 
	                            end VendorName
                            , CONCAT(fv.Carrier, ' - ' , fv.ShipViaAccountNo) ShipVia
                            , pos.POStatusName Status
                            , mpo.SubmittedTOPIC
                            , mpo.ShipAddressId,mpo.DropShip
                            ,  STUFF(
                                COALESCE(', ' + NULLIF(a.Address1, ''), '')  +
                                COALESCE(', ' + NULLIF(a.Address2, ''), '')  +
                                COALESCE(', ' + NULLIF(a.City, ''), '') +
                                COALESCE(', ' + NULLIF(a.[State], ''), '') +
                                COALESCE(', ' + NULLIF(a.ZipCode, ''), '') +
                                COALESCE(', ' + NULLIF(a.CountryCode2Digits , ''), ''),
                                1, 1, '') AS Address
                            , a.IsValidated 
                            from crm.MasterPurchaseOrder mpo
                            Left join acct.HFCVendors hv on hv.VendorId = mpo.VendorId 
                            left join acct.FranchiseVendors fv on fv.VendorId = mpo.VendorId and fv.FranchiseId = @franchiseId
                            left join crm.Type_POStatus pos on pos.POStatusId = mpo.POStatusId
                            left join crm.Addresses a on a.AddressId = mpo.ShipAddressId
                            where PurchaseOrderId = @xmpoId and ISNULL(IsXMpo, 0) = 1; 
                            
                            select ROW_NUMBER() OVER(order by ql.QuoteLineId) LineNumber
                            ,  ql.QuoteLineId, ql.ProductName, pod.PartNumber ProductNumber
                            , ql.Description , ql.Quantity, qld.BaseCost * ql.Quantity cost
                            , pod.PromiseByDate, pod.ShipDate EstimatedShipDate, pod.Status 
                            , pod.NotesVendor, pod.PurchaseOrdersDetailId, pod.Errors
                            , pod.PurchaseOrderId, ql.QuoteKey, mpox.OpportunityId, mpox.OrderId
                            , o.OrderNumber,ql.RoomName as WindowName
                            from crm.QuoteLines ql
                            left join crm.QuoteLineDetail qld on qld.QuoteLineId = ql.QuoteLineId
                            left join crm.PurchaseOrderDetails pod on pod.QuoteLineId = ql.QuoteLineId
                            left join crm.HFCProducts hp on hp.ProductID = ql.ProductId
                            left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = pod.PurchaseOrderId and mpox.QuoteId = ql.QuoteKey
                            join crm.Orders o on o.OrderID = mpox.OrderId
                            where pod.PurchaseOrderId = @xmpoId  

                            SELECT a.* FROM crm.MasterPurchaseOrder mpo
                            left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                            INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
                            INNER JOIN crm.Addresses a ON a.AddressId = mpo.ShipAddressId
                            WHERE mpo.PurchaseOrderId = @xmpoId  AND o.FranchiseId =  @franchiseId;
                            
                            SELECT stlm.Id, stl.ShipToLocation, t.Name + ' - '+stl.ShipToName AS ShipToName , stl.AddressId, stl.Active, stl.Deleted, stl.CreatedOn, stl.CreatedBy,
                            stl.LastUpdatedOn, stl.LastUpdatedBy, stl.FranchiseId, stlm.TerritoryId, STUFF(
                            COALESCE(', ' + NULLIF(Address1, ''), '')  +
                            COALESCE(', ' + NULLIF(Address2, ''), '')  +
                            COALESCE(', ' + NULLIF(City, ''), '') +
                            COALESCE(', ' + NULLIF([State], ''), '') +
                            COALESCE(', ' + NULLIF(ZipCode, ''), '') +
                            COALESCE(', ' + NULLIF(CountryCode2Digits , ''), ''),
                            1, 1, '') AS Address,a.IsValidated FROM crm.ShipToLocations stl
                             INNER JOIN crm.ShipToLocationsMap stlm ON stl.Id = stlm.ShipToLocation
                             INNER JOIN crm.Addresses a ON a.AddressId = stl.AddressId
                            INNER JOIN crm.Territories t ON t.TerritoryId = stlm.TerritoryId
                             WHERE ISNULL(stl.Active,0)=1 AND ISNULL(stl.Deleted,0)=0  AND   stlm.FranchiseId = @franchiseId;";

                    var tResult = connection.QueryMultiple(query
                        , new { xmpoId = xmpoId, franchiseId = this.Franchise.FranchiseId });
                    var xmpo = tResult.Read<XMpoDTO>().FirstOrDefault();
                    var lineItems = tResult.Read<XMpoLineDetailDTO>().ToList();
                    var dropShipadd = tResult.Read<AddressTP>().FirstOrDefault();
                    var shiptoLocationList = tResult.Read<ShipToLocations>().ToList();

                    if (xmpo == null) throw new Exception("The given Id: " + xmpoId + " does not exist, or not a xmpo id");

                    xmpo.TotalCost = lineItems.Sum(x => x.Cost);
                    xmpo.TotalQuantity = lineItems.Sum(x => x.Quantity);
                    xmpo.LineItems = lineItems;
                    xmpo.ShipToLocationList = shiptoLocationList;
                    xmpo.DropShipAddress = dropShipadd;

                    return xmpo;

                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public MasterPurchaseOrder GetMPO(int purchaseOrderId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var oppFranchiseIDQuery = @"SELECT o.FranchiseId  from crm.MasterPurchaseOrder mpo
                                    left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                   INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
                                   WHERE mpo.PurchaseOrderId = @PurchaseOrderId;";
                    var FranchiseIDData = connection.QueryMultiple(oppFranchiseIDQuery, new { PurchaseOrderId = purchaseOrderId }).Read<int>().FirstOrDefault();

                    int franchiseId = 0;
                    if (SessionManager.CurrentFranchise == null)
                    {
                        franchiseId = FranchiseIDData;
                    }
                    else
                    {
                        franchiseId = SessionManager.CurrentFranchise.FranchiseId;
                    }

                    SyncVPOStatusByMPO(purchaseOrderId, franchiseId);

                    //SetMasterPurchaseOrderStatus(purchaseOrderId);

                    var oppoquery = @"SELECT o.*  from crm.MasterPurchaseOrder mpo
                                    left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                   INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
                                   WHERE mpo.PurchaseOrderId = @PurchaseOrderId;";
                    var opDetails = connection.QueryMultiple(oppoquery, new { PurchaseOrderId = purchaseOrderId }).Read<Opportunity>().FirstOrDefault();

                    var fQuery = @"CRM.SPGetMPO";

                    connection.Open();

                    var sql = "CRM.SPGetQuoteLines";

                    var fDetails = connection.QueryMultiple(fQuery, new { FranchiseId = franchiseId, PurchaseOrderId = purchaseOrderId }, commandType: CommandType.StoredProcedure);
                    //var shippingdetails = connection.QueryMultiple(sdDetails, new { PurchaseOrderId = purchaseOrderId, FranchiseId = franchiseId });
                    //var vendordetails = connection.QueryMultiple(VIDetails, new { PurchaseOrderId = purchaseOrderId, FranchiseId = franchiseId });

                    var podList = fDetails.Read<PurchaseOrderDetails>().ToList();
                    var qlList = fDetails.Read<QuoteLines>().ToList();
                    var qlDetailsList = fDetails.Read<QuoteLineDetail>().ToList();
                    var Mpo = fDetails.Read<MasterPurchaseOrder>().FirstOrDefault();
                    var MpoHeader = fDetails.Read<MPOHeader>().ToList();
                    var vpocoreDetails = fDetails.Read<VPODetail>().ToList();
                    var vpomyVendorDetails = fDetails.Read<VPODetail>().ToList();
                    var VendorTerritoryAccountList = fDetails.Read<VendorTerritoryAccount>().ToList();
                    var shiptoLocationList = fDetails.Read<ShipToLocations>().ToList();
                    var dropShipadd = fDetails.Read<AddressTP>().FirstOrDefault();

                    var sNotice = fDetails.Read<ShipNotice>().ToList();
                    var sdetails = fDetails.Read<ShipmentDetail>().ToList();

                    var vi = fDetails.Read<VendorInvoice>().ToList();
                    var viDetails = fDetails.Read<VendorInvoiceDetail>().ToList();
                    Mpo.PICResponse = "";
                    for (int i = 0; i < sNotice.Count(); i++)
                    {
                        sNotice[i].ListShipmentDetails = sdetails.Where(x => x.ShipNoticeId == sNotice[i].ShipNoticeId).ToList();
                    }

                    if (Mpo.DropShip)
                    {
                        Mpo.DropShipAddress = dropShipadd;
                    }

                    var qlVM = connection.Query<QuoteLinesVM>(sql, new { quoteId = Mpo.QuoteId, franchiseId = franchiseId },
                        commandType: CommandType.StoredProcedure).ToList();
                    qlVM.ForEach(x => x.PICJson = "");
                    Mpo.MPOHeaderTable = MpoHeader;
                    int qty = 0;
                    decimal totalValue = 0;

                    for (int i = 0; i < vpocoreDetails.Count(); i++)
                    {
                        var sd = sNotice.Where(x => x.PICVpo == vpocoreDetails[i].PICPO.ToString()).ToList();
                        vpocoreDetails[i].ShipNotices = sd;
                    }
                    vpocoreDetails.ForEach(x => x.POResponse = "");
                    Mpo.CoreProductVPOs = vpocoreDetails;
                    Mpo.MyProductVPOs = vpomyVendorDetails;
                    var coreProdVPO = new List<VPO>();
                    var myProdVPO = new List<VPO>();

                    coreProdVPO = vpocoreDetails.GroupBy(x => new { x.VendorName, x.PICPO, x.vendorReference }).Select(y => new VPO() { VendorName = y.Key.VendorName, PICPO = y.Key.PICPO, VendorOrderNumber = y.Key.vendorReference }).ToList();
                    myProdVPO = vpomyVendorDetails.GroupBy(x => new { x.VendorName, x.PICPO }).Select(y => new VPO() { VendorName = y.Key.VendorName, PICPO = y.Key.PICPO }).ToList();

                    if (coreProdVPO != null && coreProdVPO.Count > 0)
                    {
                        foreach (var item in coreProdVPO)
                        {
                            item.VPODetails = new List<VPODetail>();
                            item.VPODetails = vpocoreDetails.Where(x => x.VendorName == item.VendorName && x.PICPO == item.PICPO).ToList();
                            item.VPODetails.ForEach(x => x.POResponse = "");
                            var sd = sNotice.Where(x => x.PICVpo == item.PICPO.ToString()).ToList();
                            item.Shipment = new List<Details>();
                            if (sd != null && sd.Count > 0)
                            {
                                foreach (var sditem in sd)
                                {
                                    var sdetail = new Details();
                                    sdetail.Key = sditem.ShipNoticeId.ToString();
                                    sdetail.Value = sditem.ShipmentId;
                                    item.Shipment.Add(sdetail);
                                }
                            }

                            var vilist = vi.Where(x => x.PICVpo == item.PICPO.ToString()).ToList();
                            item.VendorInvoice = new List<Details>();
                            if (vilist != null && vilist.Count > 0)
                            {
                                foreach (var sditem in vilist)
                                {
                                    var sdetail = new Details();
                                    sdetail.Key = sditem.InvoiceNumber;
                                    sdetail.Value = sditem.InvoiceNumber;
                                    item.VendorInvoice.Add(sdetail);
                                }
                            }
                        }
                    }
                    if (myProdVPO != null && myProdVPO.Count > 0)
                    {
                        foreach (var item in myProdVPO)
                        {
                            item.VPODetails = new List<VPODetail>();
                            item.VPODetails = vpomyVendorDetails.Where(x => x.VendorName == item.VendorName && x.PICPO == item.PICPO).ToList();
                            item.VPODetails.ForEach(x => x.POResponse = "");
                        }
                    }
                    Mpo.CoreProductGroupVpos = coreProdVPO;
                    Mpo.MyProductGroupVpos = myProdVPO;

                    var listpod = qlList.GroupBy(x => x.VendorId).Select(x => x.First()).ToList();
                    Mpo.VendorTerritoryAccountList = VendorTerritoryAccountList;

                    //if the territory is Grey area then show all the territory addresses
                    if (Mpo.TerritoryId.HasValue)
                    {
                        Mpo.ShipToLocationList = shiptoLocationList.Where(x => x.TerritoryId == Mpo.TerritoryId.Value)
                            .ToList();
                    }
                    else
                    {
                        Mpo.ShipToLocationList = shiptoLocationList.Where(x => x.TerritoryId.HasValue).OrderBy(x => x.Address).ToList();
                    }

                    if (Mpo.TerritoryId.HasValue)
                    {
                        if (Mpo.ShipToLocationList != null && Mpo.ShipToLocationList.Count > 0)
                        {
                            if (!Mpo.ShipToLocation.HasValue && !Mpo.DropShip)
                            {
                                Mpo.ShipToLocation = Mpo.ShipToLocationList.FirstOrDefault().Id;
                                Mpo.Address = Mpo.ShipToLocationList.FirstOrDefault().Address;
                            }
                        }
                    }

                    for (int i = 0; i < podList.Count; i++)
                    {
                        podList[i].QuoteLines =
                            qlList.Where(x => x.QuoteLineId == podList[i].QuoteLineId).FirstOrDefault();
                        podList[i].QuoteLineDetail =
                            qlDetailsList.Where(x => x.QuoteLineId == podList[i].QuoteLineId).FirstOrDefault();
                        qty = qty + (podList[i].QuoteLines.Quantity.HasValue
                                  ? podList[i].QuoteLines.Quantity.Value
                                  : 0);
                        podList[i].QuoteLinesVm =
                            qlVM.Where(x => x.QuoteLineId == podList[i].QuoteLineId).FirstOrDefault();

                        totalValue = totalValue + podList[i].QuoteLineDetail.NetCharge;

                        podList[i].cost = podList[i].QuoteLineDetail.Cost.HasValue
                            ? podList[i].QuoteLineDetail.Cost.Value
                            : 0;

                        podList[i].TotalQuantity = qlList.Where(x => x.VendorId == podList[i].QuoteLines.VendorId).GroupBy(x => x.VendorId).Select(x => x.Sum(a => a.Quantity.Value)).Sum();
                        podList[i].TotalLines = qlList.Where(x => x.VendorId == podList[i].QuoteLines.VendorId).Count();

                        podList[i].Vendor = podList[i].QuoteLinesVm.VendorName;

                        podList[i].ProductName = podList[i].QuoteLinesVm.ProductName;

                        podList[i].ShipNotices = sNotice;

                        //podList[i].ProductNumber = podList[i].QuoteLineDetail.;
                    }

                    //Mpo.Total = totalValue;

                    Mpo.Quantity = qty;
                    Mpo.PurchaseOrderDetails = podList;
                    Mpo.CreateDate = TimeZoneManager.ToLocal(Mpo.CreateDate);
                    Mpo.CreatedOn = TimeZoneManager.ToLocal(Mpo.CreatedOn);
                    Mpo.UpdateOn = TimeZoneManager.ToLocal(Mpo.UpdateOn);
                    Mpo.SubmittedDate = Mpo.SubmittedDate.HasValue
                        ? TimeZoneManager.ToLocal(Mpo.SubmittedDate.Value)
                        : Mpo.SubmittedDate;

                    return Mpo;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public MasterPurchaseOrder GetMasterPurchaseOrder(int purchaseOrderId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    SyncVPOStatusByMPO(purchaseOrderId, SessionManager.CurrentFranchise.FranchiseId);

                    SetMasterPurchaseOrderStatus(purchaseOrderId);

                    var oppoquery = @"SELECT o.*  from crm.MasterPurchaseOrder mpo
                                    left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                   INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
                                   WHERE mpo.PurchaseOrderId = @PurchaseOrderId;";
                    var opDetails = connection.QueryMultiple(oppoquery, new { PurchaseOrderId = purchaseOrderId }).Read<Opportunity>().FirstOrDefault();

                    var fQuery = @"SELECT pod.PurchaseOrdersDetailId, pod.PurchaseOrderId, pod.QuoteLineId, pod.PartNumber, pod.ShipDate, pod.Status, pod.PromiseByDate, pod.VendorNotes, pod.QuantityPurchased, pod.PickedBy, pod.PONumber, pod.CreatedOn, pod.CreatedBy, pod.UpdatedOn, pod.UpdatedBy, pod.NotesVendor, pod.PICPO, pod.StatusId, pod.Errors, pod.vendorReference, pod.Information, pod.Warning
                                    ,case when isnull(ql.productname,'')!='' then ql.productname
							            else
                                            case when ql.ProductId=0 then '' 
							            else case when ql.ProductTypeId = 1 and exists(select ProductId from CRM.HFCProducts where ProductId=ql.ProductId)
							                then (select ProductGroupDesc from CRM.HFCProducts where ProductId=ql.ProductId)
                                        else case when ql.ProductTypeId = 2 and exists(select ProductId from CRM.FranchiseProducts where ProductKey=ql.ProductId) 
                                            then (select ProductName from CRM.FranchiseProducts where ProductKey=ql.ProductId) else '' end end end 
                                    end as ProductName
                                    ,case when ql.VendorId is null and ql.VendorId=0 then '' else case when exists(select VendorId from Acct.HFCVendors where VendorId=ql.VendorId) 
                                    then (select VendorName from Acct.HFCVendors where VendorId=ql.VendorId) 
                                    else case when exists(select VendorId from Acct.FranchiseVendors where VendorId=ql.VendorId) 
                                    then (select VendorName from Acct.FranchiseVendors where VendorId=ql.VendorId) else '' end end end  as Vendor                                    
                                    FROM CRM.MasterPurchaseOrder mpo
                                    join CRM.PurchaseOrderDetails pod on mpo.PurchaseOrderId=pod.PurchaseOrderId
                                    join CRM.MasterPurchaseOrderExtn mpox on mpo.PurchaseOrderId=mpox.PurchaseOrderId
                                    join CRM.Orders o on mpox.OrderId=o.OrderID
                                    join CRM.Opportunities opp on o.OpportunityId=opp.OpportunityId
                                    join CRM.Quote q on o.QuoteKey=q.QuoteKey
                                    join CRM.QuoteLines ql on q.QuoteKey=ql.QuoteKey and pod.QuoteLineId=ql.QuoteLineId
                                    WHERE pod.PurchaseOrderId = @PurchaseOrderId AND opp.FranchiseId = @FranchiseId;

                                    SELECT ql.QuoteLineId, ql.QuoteKey, ql.MeasurementsetId, ql.VendorId, ql.ProductId, ql.SuggestedResale, ql.Discount, ql.CreatedOn, ql.CreatedBy,
                                    ql.LastUpdatedOn, ql.LastUpdatedBy, ql.IsActive, ql.Width, ql.Height, ql.UnitPrice, ql.Quantity, ql.Description, ql.MountType, ql.ExtendedPrice,
                                    ql.Memo, ql.ProductTypeId, ql.Quntity, ql.MarkupfactorType, ql.DiscountType,       ql.ProductName, ql.VendorName, ql.CurrencyExchangeId, ql.ConversionRate,
                                    ql.VendorCurrency, ql.ConversionCalculationType, ql.QuoteLineNumber, ql.FranctionalValueWidth, ql.FranctionalValueHeight, ql.RoomName, ql.WindowLocation,
                                    ql.ProductCategoryId, ql.ProductSubCategoryId, ql.BillofMaterial
                                    FROM CRM.MasterPurchaseOrder mpo
                                    join CRM.PurchaseOrderDetails pod on mpo.PurchaseOrderId=pod.PurchaseOrderId
                                    join CRM.MasterPurchaseOrderExtn mpox on mpo.PurchaseOrderId=mpox.PurchaseOrderId
                                    join CRM.Orders o on mpox.OrderId=o.OrderID
                                    join CRM.Opportunities opp on o.OpportunityId=opp.OpportunityId
                                    join CRM.Quote q on o.QuoteKey=q.QuoteKey
                                    join CRM.QuoteLines ql on q.QuoteKey=ql.QuoteKey and pod.QuoteLineId=ql.QuoteLineId
                                    WHERE mpo.PurchaseOrderId = @PurchaseOrderId AND opp.FranchiseId = @FranchiseId;

                                    SELECT qld.* FROM CRM.MasterPurchaseOrder mpo
                                    join CRM.PurchaseOrderDetails pod on mpo.PurchaseOrderId=pod.PurchaseOrderId
                                    join CRM.MasterPurchaseOrderExtn mpox on mpo.PurchaseOrderId=mpox.PurchaseOrderId
                                    join CRM.Orders o on mpox.OrderId=o.OrderID
                                    join CRM.Opportunities opp on o.OpportunityId=opp.OpportunityId
                                    join CRM.Quote q on o.QuoteKey=q.QuoteKey
                                    join CRM.QuoteLines ql on q.QuoteKey=ql.QuoteKey and pod.QuoteLineId=ql.QuoteLineId
									join CRM.QuoteLineDetail qld on qld.QuoteLineId = pod.QuoteLineId
                                    WHERE pod.PurchaseOrderId = @PurchaseOrderId AND opp.FranchiseId = @FranchiseId;

                                    SELECT ord.OrderNumber, mpox.AccountId, mpox.OpportunityId, mpox.QuoteId, mpox.OrderId,  mpo.*,o.OpportunityName,q.SideMark,[CRM].[fnGetAccountName_forOpportunitySearch](mpox.OpportunityId) AS AccountName, tp.POStatusName AS Status, ord.OrderTotal AS Total,
		                            STUFF(
                                    COALESCE(', ' + NULLIF(Address1, ''), '')  +
		                            COALESCE(', ' + NULLIF(Address2, ''), '')  +
                                    COALESCE(', ' + NULLIF(City, ''), '') +
		                            COALESCE(', ' + NULLIF([State], ''), '') +
                                    COALESCE(' ' + NULLIF(ZipCode, ''), '') +
                                    COALESCE(', ' + NULLIF(CountryCode2Digits , ''), ''),
                                    1, 1, '') AS Address,a.IsValidated
                                    FROM crm.MasterPurchaseOrder mpo
                                    left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                    INNER JOIN crm.Type_POStatus tp ON tp.POStatusId = mpo.POStatusId
                                    INNER JOIN crm.orders ord ON ord.OrderId = mpox.OrderID
                                    INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
								    LEFT JOIN crm.Addresses a ON a.AddressId = mpo.ShipAddressId
                                    left join CRM.Quote q on o.OpportunityId =q.OpportunityId
                                    WHERE mpo.PurchaseOrderId = @PurchaseOrderId AND o.FranchiseId = @FranchiseId;

                                    SELECT hven.Name as VendorName,ql.vendorId, SUM(ql.Quantity) AS TotalQuantity, count(*) TotalLines,pod.Status AS Status,pod.ShipDate,ROW_NUMBER() OVER(ORDER BY ql.VendorId, hven.Name ASC) AS LineNumber,pod.PICPO FROM crm.PurchaseOrderDetails pod
                                    INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
                                    inner join crm.QuoteLineDetail qld on ql.QuoteLineId = qld.QuoteLineId
                                    INNER JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = pod.PurchaseOrderId
                                    left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                    INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
									LEFT JOIN Acct.FranchiseVendors AS ven ON ven.VendorId = ql.VendorId AND ven.FranchiseId = o.FranchiseId
		                            LEFT JOIN Acct.HFCVendors AS hven ON ven.VendorId = hven.VendorId
									WHERE pod.PurchaseOrderId = @PurchaseOrderId AND o.FranchiseId = @FranchiseId
                                    GROUP BY pod.PICPO,ql.VendorId, hven.Name,pod.Status,pod.ShipDate;

                                    SELECT ql.QuoteLineNumber as LineNumber,pod.PurchaseOrdersDetailId,pod.PurchaseOrderId,pod.QuoteLineId,pod.PartNumber,ql.ProductName,ql.ProductTypeId,pod.QuantityPurchased AS purchased,
                                    ql.VendorName,ql.Description,ql.Quantity,pod.PartNumber,pod.ShipDate,pod.PromiseByDate,pod.Status,pod.NotesVendor,pod.PICPO,pod.POResponse,pod.StatusId,
                                    pod.warning,pod.information,pod.Errors,
                                    pod.PONumber,  pod.PickedBy,pod.vendorReference,ql.ExtendedPrice,ql.RoomName as WindowName  
									FROM CRM.MasterPurchaseOrder mpo
                                    join CRM.PurchaseOrderDetails pod on mpo.PurchaseOrderId=pod.PurchaseOrderId
                                    join CRM.MasterPurchaseOrderExtn mpox on mpo.PurchaseOrderId=mpox.PurchaseOrderId
                                    join CRM.Orders o on mpox.OrderId=o.OrderID
                                    join CRM.Opportunities opp on o.OpportunityId=opp.OpportunityId
                                    join CRM.Quote q on o.QuoteKey=q.QuoteKey
                                    join CRM.QuoteLines ql on q.QuoteKey=ql.QuoteKey and pod.QuoteLineId=ql.QuoteLineId
                                    WHERE ql.ProductTypeId = 1 AND  pod.PurchaseOrderId = @PurchaseOrderId AND opp.FranchiseId = @FranchiseId;

                                    SELECT ql.QuoteLineNumber as LineNumber,pod.PurchaseOrdersDetailId,pod.PurchaseOrderId,pod.QuoteLineId,pod.PartNumber,ql.ProductName,ql.ProductTypeId,pod.QuantityPurchased AS purchased,
                                    hven.name as VendorName,ql.Description,ql.Quantity,pod.PartNumber,pod.ShipDate,pod.PromiseByDate,pod.Status,pod.NotesVendor,pod.PICPO,pod.StatusId,
                                    pod.PONumber,  pod.PickedBy ,ql.ExtendedPrice,ql.RoomName as WindowName  FROM crm.PurchaseOrderDetails pod
                                    INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
                                    inner join crm.QuoteLineDetail qld on ql.QuoteLineId = qld.QuoteLineId
                                    INNER JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = pod.PurchaseOrderId
                                    left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                    INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
									LEFT JOIN Acct.FranchiseVendors AS ven ON ven.VendorId = ql.VendorId AND ven.FranchiseId = o.FranchiseId
		                            LEFT JOIN Acct.HFCVendors AS hven ON ven.VendorId = hven.VendorId
                                    WHERE ql.ProductTypeId IN (2,5) AND  pod.PurchaseOrderId = @PurchaseOrderId AND o.FranchiseId = @FranchiseId;

                                    select vta.*,stl.ShipToName from [CRM].[VendorTerritoryAccount] vta
                                    INNER JOIN crm.ShipToLocations stl ON stl.ShipToLocation = vta.ShipToLocation
                                    left JOIN crm.Opportunities o ON o.TerritoryId = vta.TerritoryId
                                    left join crm.MasterPurchaseOrderExtn mpox on mpox.OpportunityId = o.OpportunityId  
                                    INNER JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = mpox.PurchaseOrderId  
                                    where o.FranchiseId=@FranchiseId AND mpo.PurchaseOrderId = @PurchaseOrderId;

                                   SELECT stlm.Id, stl.ShipToLocation, t.Name + ' - '+stl.ShipToName AS ShipToName , stl.AddressId, stl.Active, stl.Deleted, stl.CreatedOn, stl.CreatedBy,
                                   stl.LastUpdatedOn, stl.LastUpdatedBy, stl.FranchiseId, stlm.TerritoryId, STUFF(
                                                                        COALESCE(', ' + NULLIF(Address1, ''), '')  +
                                    		                            COALESCE(', ' + NULLIF(Address2, ''), '')  +
                                                                        COALESCE(', ' + NULLIF(City, ''), '') +
                                    		                            COALESCE(', ' + NULLIF([State], ''), '') +
                                                                        COALESCE(', ' + NULLIF(ZipCode, ''), '') +
                                                                        COALESCE(', ' + NULLIF(CountryCode2Digits , ''), ''),
                                                                        1, 1, '') AS Address,a.IsValidated FROM crm.ShipToLocations stl
                                    INNER JOIN crm.ShipToLocationsMap stlm ON stl.Id = stlm.ShipToLocation
                                    INNER JOIN crm.Addresses a ON a.AddressId = stl.AddressId
									INNER JOIN crm.Territories t ON t.TerritoryId = stlm.TerritoryId
                                    WHERE ISNULL(stl.Active,0)=1 AND ISNULL(stl.Deleted,0)=0  AND   stlm.FranchiseId = @FranchiseId;

                                   SELECT a.* FROM crm.MasterPurchaseOrder mpo
                                    left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                   INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
                                   INNER JOIN crm.Addresses a ON a.AddressId = mpo.ShipAddressId
                                   WHERE mpo.PurchaseOrderId = @PurchaseOrderId  AND o.FranchiseId =  @FranchiseId;";

                    connection.Open();

                    var sql = "CRM.SPGetQuoteLines";

                    var sdDetails = @"SELECT distinct sn.* FROM crm.ShipNotice sn
                                      INNER JOIN crm.ShipmentDetail sd ON sd.ShipmentId = sn.ShipmentId
                                      INNER JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = sn.PurchaseOrderId
                                        left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                      INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
                                      WHERE mpo.PurchaseOrderId = @PurchaseOrderId AND o.FranchiseId = @FranchiseId;
                                      
                                        SELECT distinct sd.* FROM crm.ShipNotice sn
                                      INNER JOIN crm.ShipmentDetail sd ON sd.ShipmentId = sn.ShipmentId
                                      INNER JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = sn.PurchaseOrderId
                                        left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                      INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
                                      WHERE mpo.PurchaseOrderId = @PurchaseOrderId AND o.FranchiseId = @FranchiseId";

                    int franchiseId = 0;
                    if (SessionManager.CurrentFranchise == null)
                    {
                        franchiseId = opDetails.FranchiseId.Value;
                    }
                    else
                    {
                        franchiseId = SessionManager.CurrentFranchise.FranchiseId;
                    }

                    var fDetails = connection.QueryMultiple(fQuery, new { FranchiseId = franchiseId, PurchaseOrderId = purchaseOrderId });
                    var shippingdetails = connection.QueryMultiple(sdDetails, new { PurchaseOrderId = purchaseOrderId, FranchiseId = franchiseId });

                    var podList = fDetails.Read<PurchaseOrderDetails>().ToList();
                    var qlList = fDetails.Read<QuoteLines>().ToList();
                    var qlDetailsList = fDetails.Read<QuoteLineDetail>().ToList();
                    var Mpo = fDetails.Read<MasterPurchaseOrder>().FirstOrDefault();
                    var MpoHeader = fDetails.Read<MPOHeader>().ToList();
                    var vpocoreDetails = fDetails.Read<VPODetail>().ToList();
                    var vpomyVendorDetails = fDetails.Read<VPODetail>().ToList();
                    var VendorTerritoryAccountList = fDetails.Read<VendorTerritoryAccount>().ToList();
                    var shiptoLocationList = fDetails.Read<ShipToLocations>().ToList();
                    var dropShipadd = fDetails.Read<AddressTP>().FirstOrDefault();

                    var sNotice = shippingdetails.Read<ShipNotice>().ToList();
                    var sdetails = shippingdetails.Read<ShipmentDetail>().ToList();

                    for (int i = 0; i < sNotice.Count(); i++)
                    {
                        sNotice[i].ListShipmentDetails = sdetails.Where(x => x.ShipNoticeId == sNotice[i].ShipNoticeId).ToList();
                    }

                    if (Mpo.DropShip)
                    {
                        Mpo.DropShipAddress = dropShipadd;
                    }

                    //var qlVM = connection.Query<QuoteLinesVM>(sql, new { quoteId = Mpo.QuoteId, franchiseId = franchiseId },
                    //    commandType: CommandType.StoredProcedure).ToList();

                    Mpo.MPOHeaderTable = MpoHeader;
                    int qty = 0;
                    decimal totalValue = 0;

                    for (int i = 0; i < vpocoreDetails.Count(); i++)
                    {
                        vpocoreDetails[i].ShipNotices = sNotice.Where(x =>
                            x.PurchaseOrdersDetailId == vpocoreDetails[i].PurchaseOrdersDetailId).ToList();
                    }

                    Mpo.CoreProductVPOs = vpocoreDetails;
                    Mpo.MyProductVPOs = vpomyVendorDetails;
                    var coreProdVPO = new List<VPO>();
                    var myProdVPO = new List<VPO>();
                    var listpod = qlList.GroupBy(x => x.VendorId).Select(x => x.First()).ToList();
                    Mpo.VendorTerritoryAccountList = VendorTerritoryAccountList;

                    //if the territory is Grey area then show all the territory addresses
                    if (Mpo.TerritoryId.HasValue)
                    {
                        Mpo.ShipToLocationList = shiptoLocationList.Where(x => x.TerritoryId == Mpo.TerritoryId.Value)
                            .ToList();
                    }
                    else
                    {
                        Mpo.ShipToLocationList = shiptoLocationList.Where(x => x.TerritoryId.HasValue).OrderBy(x => x.Address).ToList();
                    }

                    if (Mpo.TerritoryId.HasValue)
                    {
                        if (Mpo.ShipToLocationList != null && Mpo.ShipToLocationList.Count > 0)
                        {
                            if (!Mpo.ShipToLocation.HasValue && !Mpo.DropShip)
                            {
                                Mpo.ShipToLocation = Mpo.ShipToLocationList.FirstOrDefault().Id;
                                Mpo.Address = Mpo.ShipToLocationList.FirstOrDefault().Address;
                            }
                        }
                    }

                    for (int i = 0; i < podList.Count; i++)
                    {
                        podList[i].QuoteLines =
                            qlList.Where(x => x.QuoteLineId == podList[i].QuoteLineId).FirstOrDefault();
                        podList[i].QuoteLineDetail =
                            qlDetailsList.Where(x => x.QuoteLineId == podList[i].QuoteLineId).FirstOrDefault();
                        qty = qty + (podList[i].QuoteLines.Quantity.HasValue
                                  ? podList[i].QuoteLines.Quantity.Value
                                  : 0);
                        //podList[i].QuoteLinesVm =
                        //    qlVM.Where(x => x.QuoteLineId == podList[i].QuoteLineId).FirstOrDefault();

                        totalValue = totalValue + podList[i].QuoteLineDetail.NetCharge;

                        podList[i].cost = podList[i].QuoteLineDetail.Cost.HasValue
                            ? podList[i].QuoteLineDetail.Cost.Value
                            : 0;

                        podList[i].TotalQuantity = qlList.Where(x => x.VendorId == podList[i].QuoteLines.VendorId).GroupBy(x => x.VendorId).Select(x => x.Sum(a => a.Quantity.Value)).Sum();
                        podList[i].TotalLines = qlList.Where(x => x.VendorId == podList[i].QuoteLines.VendorId).Count();

                        //if(podList[i].QuoteLinesVm!=null)
                        //{
                        //    podList[i].Vendor = podList[i].QuoteLinesVm.VendorName;

                        //    podList[i].ProductName = podList[i].QuoteLinesVm.ProductName;
                        //}


                        podList[i].ShipNotices = sNotice;

                        //podList[i].ProductNumber = podList[i].QuoteLineDetail.;
                    }

                    Mpo.Total = totalValue;

                    Mpo.Quantity = qty;
                    Mpo.PurchaseOrderDetails = podList;
                    Mpo.CreateDate = TimeZoneManager.ToLocal(Mpo.CreateDate);
                    Mpo.CreatedOn = TimeZoneManager.ToLocal(Mpo.CreatedOn);
                    Mpo.UpdateOn = TimeZoneManager.ToLocal(Mpo.UpdateOn);
                    Mpo.SubmittedDate = Mpo.SubmittedDate.HasValue
                        ? TimeZoneManager.ToLocal(Mpo.SubmittedDate.Value)
                        : Mpo.SubmittedDate;

                    return Mpo;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        /// <summary>
        ///  Users clicks on confirm addres MPO Page
        /// </summary>
        /// <param name="mpoId"></param>
        /// <returns></returns>
        public bool ConfirmAddress(int mpoId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var fQuery = @"SELECT mpox.AccountId, mpox.OpportunityId, mpox.QuoteId, mpox.OrderId, mpo.* FROM crm.MasterPurchaseOrder mpo 
                                    left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                    INNER JOIN crm.Opportunities co ON co.OpportunityId = mpox.OpportunityId 
                                    WHERE mpo.PurchaseOrderId = @PurchaseOrderId AND co.FranchiseId = @FranchiseId;";
                    connection.Open();

                    var fDetails = connection.QueryMultiple(fQuery, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId, PurchaseOrderId = mpoId });

                    var mpo = fDetails.Read<MasterPurchaseOrder>().FirstOrDefault();

                    mpo.ConfirmAddress = true;
                    mpo.ImpersonatorPersonId = this.ImpersonatorPersonId;
                    connection.Update(mpo);
                    EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(mpo), PurchaseOrderId: mpo.PurchaseOrderId);
                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        // not used anywhere.... - murugan
        //public List<MasterPurchaseOrder> GetTerrioryAddressMap()
        //{
        //    using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
        //    {
        //        try
        //        {
        //            var fQuery =
        //                @"SELECT mpo.*,co.opportunityName, [CRM].[fnGetAccountName_forOpportunitySearch](mpo.OpportunityId) AS AccountName,tp.POStatusName AS Status,o.OrderNumber AS OrderNumber
        //                           FROM crm.MasterPurchaseOrder mpo
        //                           Left join crm.MasterPurchaseOrderExtn mpox on mpox.PurhcaseOrderId = mpo.PurchaseOrderId
        //                           INNER JOIN crm.Orders o ON o.OrderID = mpox.OrderId
        //                           INNER JOIN crm.Type_POStatus tp ON tp.POStatusId = mpo.POStatusId
        //                           INNER JOIN crm.Opportunities co ON co.OpportunityId = mpox.OpportunityId
        //                           WHERE co.FranchiseId = @franchiseId";
        //            connection.Open();

        //            var fDetails = connection.QueryMultiple(fQuery,
        //                new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId });

        //            var MPOList = fDetails.Read<MasterPurchaseOrder>().ToList();

        //            return MPOList;
        //        }
        //        catch (Exception ex)
        //        {
        //            EventLogger.LogEvent(ex);
        //            throw ex;
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }
        //    }
        //}

        public List<MasterPurchaseOrder> GetMPOList()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var fQuery = @"SELECT case when co.TerritoryId is not null then tr.Name else co.TerritoryType end as Territory
                                    ,mpox.AccountId, mpox.OpportunityId, mpox.QuoteId, mpox.OrderId, mpo.*,co.opportunityName
                                    , [CRM].[fnGetAccountName_forOpportunitySearch](mpox.OpportunityId) AS AccountName
                                    ,tp.POStatusName AS Status
                                    ,o.OrderNumber AS OrderNumber
                                    , co.InstallerId
                                    , co.SalesAgentId
                                   FROM crm.MasterPurchaseOrder mpo
                                    left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                   INNER JOIN crm.Orders o ON o.OrderID = mpox.OrderId
                                   INNER JOIN crm.Type_POStatus tp ON tp.POStatusId = mpo.POStatusId
                                   INNER JOIN crm.Opportunities co ON co.OpportunityId = mpox.OpportunityId
                                   LEFT JOIN CRM.Territories tr on tr.TerritoryId=co.TerritoryId
                                   WHERE co.FranchiseId = @franchiseId order by mpo.UpdateOn desc";
                    connection.Open();

                    var fDetails = connection.QueryMultiple(fQuery, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId });

                    var MPOList = fDetails.Read<MasterPurchaseOrder>().ToList();

                    return MPOList;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        // Not used anywhere... -murugan.
        //public List<MasterPurchaseOrder> GetShippingAddressList()
        //{
        //    using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
        //    {
        //        try
        //        {
        //            var fQuery = @"SELECT mpo.*,co.opportunityName, [CRM].[fnGetAccountName_forOpportunitySearch](mpo.OpportunityId) AS AccountName,tp.POStatusName AS Status,o.OrderNumber AS OrderNumber
        //                           FROM crm.MasterPurchaseOrder mpo
        //                           INNER JOIN crm.Orders o ON o.OrderID = mpo.OrderId
        //                           INNER JOIN crm.Type_POStatus tp ON tp.POStatusId = mpo.POStatusId
        //                           INNER JOIN crm.Opportunities co ON co.OpportunityId = mpo.OpportunityId
        //                           WHERE co.FranchiseId = @franchiseId";
        //            connection.Open();

        //            var fDetails = connection.QueryMultiple(fQuery, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId });

        //            var MPOList = fDetails.Read<MasterPurchaseOrder>().ToList();

        //            return MPOList;
        //        }
        //        catch (Exception ex)
        //        {
        //            EventLogger.LogEvent(ex);
        //            throw ex;
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }
        //    }
        //}

        // not used anywhere... -murugan
        //public bool GetVPOUpdate(int purchaseOrderId)
        //{
        //    using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
        //    {
        //        try
        //        {
        //            var fQuery = @"SELECT mpo.* FROM crm.MasterPurchaseOrder mpo INNER JOIN crm.PurchaseOrderDetails pod ON pod.PurchaseOrderId = mpo.PurchaseOrderId
        //                           INNER JOIN crm.Opportunities o ON mpo.OpportunityId = o.OpportunityId WHERE mpo.PurchaseOrderId = @PurchaseOrderId AND o.FranchiseId = @franchiseId;
        //                           SELECT ql.QuoteLineNumber as LineNumber,pod.* FROM crm.MasterPurchaseOrder mpo
        //                           INNER JOIN crm.PurchaseOrderDetails pod ON pod.PurchaseOrderId = mpo.PurchaseOrderId
        //                           INNER JOIN crm.Opportunities o ON mpo.OpportunityId = o.OpportunityId
        //                           INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId WHERE mpo.PurchaseOrderId = @PurchaseOrderId AND o.FranchiseId = @franchiseId;";
        //            connection.Open();

        //            var fDetails = connection.QueryMultiple(fQuery, new { PurchaseOrderId = purchaseOrderId, FranchiseId = SessionManager.CurrentFranchise.FranchiseId });

        //            var MPOList = fDetails.Read<MasterPurchaseOrder>().ToList();
        //            var podList = fDetails.Read<PurchaseOrderDetails>().ToList();

        //            return true;
        //        }
        //        catch (Exception ex)
        //        {
        //            EventLogger.LogEvent(ex);
        //            throw ex;
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }
        //    }
        //}

        public string InsertOrUpdatePurchaseOrder(int purchaseOrderId, string picResponse)
        {
            var connection = new SqlConnection(ContextFactory.CrmConnectionString);
            try
            {
                int user = 0;
                if (SessionManager.CurrentFranchise != null)
                {
                    user = SessionManager.CurrentUser.PersonId;
                }
                else
                {
                    ///Admin user
                    user = AdminPersonId;
                }
                var dateTime = DateTime.UtcNow;
                PICSalesOrderVM poResponse = JsonConvert.DeserializeObject<PICSalesOrderVM>(picResponse);
                connection.Open();
                //var query = @"SELECT a.AccountId, o.* FROM crm.orders AS o INNER JOIN [CRM].[Opportunities] AS a ON a.OpportunityId = o.OpportunityId WHERE o.orderId = @OrderId;";
                //var mPOQuery = @" INSERT INTO CRM.MasterPurchaseOrder (MasterPONumber ,AccountId ,OpportunityId ,CreateDate ,SubmittedDate ,OrderId ,QuoteId ,Status ,CreatedBy ,CreatedOn ,UpdatedBy ,UpdateOn) VALUES (@MasterPONumber ,@AccountId ,@OpportunityId ,@CreateDate ,@SubmittedDate ,@OrderId ,@QuoteId ,@Status ,@CreatedBy ,@CreatedOn ,@UpdatedBy ,@UpdateOn); SELECT CAST(SCOPE_IDENTITY() as int)";

                //var mPOExQuery = @" INSERT INTO CRM.MasterPurchaseOrderExtn (PurchaseOrderId,AccountId ,OpportunityId, QuoteId, OrderId, CreatedBy, CreatedOn ,UpdatedBy ,UpdateOn) VALUES (@purhcaseOrderId, @AccountId, @OpportunityId, @QuoteId, @OrderId, @CreatedBy, @CreatedOn, @UpdatedBy ,@UpdateOn);";

                //var poDetails = @"INSERT INTO CRM.PurchaseOrderDetails (PurchaseOrderId ,QuoteLineId ,PartNumber ,ShipDate ,Status ,PromiseByDate ,QuantityPurchased ,PickedBy ,PONumber ,CreateOn ,CreatedBy ,UpdatedOn ,UpdatedBy) VALUES (@PurchaseOrderId ,@QuoteLineId ,@PartNumber ,@ShipDate ,@Status ,@PromiseByDate ,@QuantityPurchased ,@PickedBy ,@PONumber ,@CreateOn ,@CreatedBy ,@UpdatedOn ,@UpdatedBy); SELECT CAST(SCOPE_IDENTITY() as int)";
                var quoteLineQuery = @"SELECT ql.* FROM crm.QuoteLines ql INNER JOIN crm.PurchaseOrderDetails pod ON pod.QuoteLineId = ql.QuoteLineId WHERE pod.PurchaseOrderId=@PurchaseOrderId";
                //var fProductQuery = @"select * from crm.franchiseproducts where productId = @productId";

                //var mpo = connection.Query<MasterPurchaseOrder>(@"SELECT mpox.AccountId, mpox.OpportunityId, mpox.QuoteId, mpox.OrderId,  mpo.* FROM crm.MasterPurchaseOrder mpo 
                //            left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                //            WHERE mpox.OrderId = @OrderId;", new { orderId = orderId }).FirstOrDefault();

                var pdetails = @"SELECT ql.QuoteLineNumber as LineNumber,pod.* FROM crm.PurchaseOrderDetails pod
                                      INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
                                      WHERE pod.PurchaseOrderId = @PurchaseOrderId;";
                var mpoquery = @"SELECT mpox.AccountId, mpox.OpportunityId, mpox.QuoteId, mpox.OrderId, mpo.* from  crm.MasterPurchaseOrder mpo 
                                left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                WHERE mpox.PurchaseOrderId = @PurchaseOrderId ;";

                var podDetails = connection.Query<PurchaseOrderDetails>(pdetails, new { PurchaseOrderId = purchaseOrderId }).ToList();
                var mpoobj = connection.Query<MasterPurchaseOrder>(mpoquery, new { PurchaseOrderId = purchaseOrderId }).FirstOrDefault();

                //var order = connection.Query(query, new { OrderId = orderId }).FirstOrDefault();
                var quotelines = connection.Query(quoteLineQuery, new { PurchaseOrderId = purchaseOrderId }).ToList();

                if (Convert.ToBoolean(poResponse.valid))
                {
                    var po = poResponse.properties;
                    if (Convert.ToInt32(po.Order.status) == 40 || Convert.ToInt32(po.Order.status) == 70 || Convert.ToInt32(po.Order.status) == 71 || Convert.ToInt32(po.Order.status) == 20 || Convert.ToInt32(po.Order.status) == 0)
                    {
                        mpoobj.PICPO = Convert.ToInt32(po.Order.wo);
                        mpoobj.SubmittedDate = Convert.ToDateTime(po.Order.date);
                        mpoobj.Status = GetMPOStatus(Convert.ToInt32(po.Order.status));
                        mpoobj.POStatusId = 2;
                        mpoobj.UpdatedBy = user;
                        mpoobj.UpdateOn = dateTime;
                        mpoobj.PICResponse = picResponse;
                        mpoobj.SubmittedDate = dateTime;
                        mpoobj.SubmittedTOPIC = true;
                        mpoobj.Message = "";
                        mpoobj.ImpersonatorPersonId = this.ImpersonatorPersonId;
                        connection.Update(mpoobj);
                        EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(mpoobj), PurchaseOrderId: mpoobj.PurchaseOrderId);

                        foreach (var ql in quotelines)
                        {
                            if (ql.ProductTypeId == 1)
                            {
                                dynamic jsonObj = JsonConvert.DeserializeObject(ql.PICJson);

                                var poItemDetail = po.PurchaseOrders.Where(x => x.vendor == jsonObj.PicVendor.Value).FirstOrDefault();

                                //((IEnumerable)po.PurchaseOrders).Cast<dynamic>()
                                //.Where(p => p.vendor == jsonObj.picVendor);

                                var details = podDetails.Where(x => x.QuoteLineId == Convert.ToInt32(ql.QuoteLineId))
                                    .FirstOrDefault();

                                //details.PartNumber = Convert.ToString(jsonObj.picProduct);
                                //details.ShipDate = Convert.ToDateTime(poItemDetail.date);
                                details.Status = ReturnPOStatus(Convert.ToInt32(poItemDetail.status));
                                details.StatusId = ReturnPOStatusId(Convert.ToInt32(poItemDetail.status));
                                //details.PromiseByDate = Convert.ToDateTime(poItemDetail.date);
                                details.QuantityPurchased = Convert.ToInt32(Convert.ToDecimal(poItemDetail.ordered));

                                details.PICPO = Convert.ToInt32(poItemDetail.po);
                                details.UpdatedBy = user;
                                details.UpdatedOn = dateTime;
                                details.ImpersonatorPersonId = this.ImpersonatorPersonId;
                                connection.Update(details);
                                //EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(details), PurchaseOrdersDetailId: details.PurchaseOrdersDetailId);
                            }
                        }
                    }
                }
                //else
                //{
                //    //var err = JsonConvert.SerializeObject(poResponse.error);
                //    EventLogger.LogEvent(err, "Purchase Order", LogSeverity.Error, null, null, SessionManager.CurrentUser.UserName, SessionManager.CurrentFranchise.FranchiseId, "", null);
                //    return err;
                //}
                return Success;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ex.Message;
            }
            finally
            {
                connection.Close();
            }
        }

        //public string CancelVPOLineItem(int poLineId, mpoId)
        //{
        //    using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
        //    {
        //        try
        //        {
        //            var usr = SessionManager.CurrentUser.PersonId;
        //            var datetime = DateTime.UtcNow;

        //            var fQuery = @"SELECT mpo.* FROM crm.MasterPurchaseOrder mpo INNER JOIN crm.PurchaseOrderDetails pod ON pod.PurchaseOrderId = mpo.PurchaseOrderId
        //                           INNER JOIN crm.Opportunities o ON mpo.OpportunityId = o.OpportunityId WHERE mpo.PurchaseOrderId = @PurchaseOrderId AND o.FranchiseId = @franchiseId;
        //                           SELECT ql.QuoteLineNumber as LineNumber,pod.* FROM crm.MasterPurchaseOrder mpo INNER JOIN crm.PurchaseOrderDetails pod ON pod.PurchaseOrderId = mpo.PurchaseOrderId
        //                           INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
        //                           INNER JOIN crm.Opportunities o ON mpo.OpportunityId = o.OpportunityId WHERE mpo.PurchaseOrderId = @PurchaseOrderId AND o.FranchiseId = @franchiseId;";
        //            connection.Open();

        //            var fDetails = connection.QueryMultiple(fQuery, new { PurchaseOrderId = MPOId, FranchiseId = SessionManager.CurrentFranchise.FranchiseId });

        //            var MPOList = fDetails.Read<MasterPurchaseOrder>().ToList();
        //            var podList = fDetails.Read<PurchaseOrderDetails>().ToList();

        //            foreach (var pod in podList)
        //            {
        //                if (pod.PICPO == VPOId)
        //                {
        //                    pod.StatusId = (int)PurchaseOrderStatus.Canceled;
        //                    pod.Status = Enum.GetName(typeof(PurchaseOrderStatus), 8);
        //                    pod.UpdatedBy = usr;
        //                    pod.UpdatedOn = datetime;
        //                    connection.Update(pod);

        //                }
        //            }

        //            SetMasterPurchaseOrderStatus(MPOId);

        //            return Success;
        //        }
        //        catch (Exception ex)
        //        {
        //            EventLogger.LogEvent(ex);
        //            throw ex;
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }
        //    }

        //}
        public string CancelVPO(int VPOId, int MPOId, string cancelReason)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var usr = SessionManager.CurrentUser.PersonId;
                    var datetime = DateTime.UtcNow;

                    var fQuery = @"SELECT mpox.AccountId, mpox.OpportunityId, mpox.QuoteId, mpox.OrderId, mpo.* FROM crm.MasterPurchaseOrder mpo 
                                    left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                    INNER JOIN crm.PurchaseOrderDetails pod ON pod.PurchaseOrderId = mpo.PurchaseOrderId
                                   INNER JOIN crm.Opportunities o ON mpox.OpportunityId = o.OpportunityId 
                                    WHERE mpo.PurchaseOrderId = @PurchaseOrderId AND o.FranchiseId = @franchiseId;

                                   SELECT ql.QuoteLineNumber as LineNumber,pod.* FROM crm.MasterPurchaseOrder mpo 
                                    left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                    INNER JOIN crm.PurchaseOrderDetails pod ON pod.PurchaseOrderId = mpo.PurchaseOrderId
                                   INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
                                   INNER JOIN crm.Opportunities o ON mpox.OpportunityId = o.OpportunityId 
                                    WHERE mpo.PurchaseOrderId = @PurchaseOrderId AND o.FranchiseId = @franchiseId;";
                    connection.Open();

                    var fDetails = connection.QueryMultiple(fQuery, new { PurchaseOrderId = MPOId, FranchiseId = SessionManager.CurrentFranchise.FranchiseId });

                    var Mpo = fDetails.Read<MasterPurchaseOrder>().FirstOrDefault();
                    var podList = fDetails.Read<PurchaseOrderDetails>().ToList();

                    foreach (var pod in podList)
                    {
                        if (pod.PurchaseOrdersDetailId == VPOId)
                        {
                            pod.StatusId = (int)PurchaseOrderStatus.Canceled;
                            pod.Status = Enum.GetName(typeof(PurchaseOrderStatus), 8);
                            pod.CancelReason = cancelReason;
                            pod.UpdatedBy = usr;
                            pod.UpdatedOn = datetime;
                            pod.ImpersonatorPersonId = this.ImpersonatorPersonId;
                            connection.Update(pod);
                            EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(pod), PurchaseOrdersDetailId: pod.PurchaseOrdersDetailId);
                            //Cancel Quote lines

                            // This should not be called if we cancel at line level at mpo.
                            // QuotesMgr.CancelLinebyQuoteLineId(pod.QuoteLineId, cancelReason);


                            // xmpo canclled quoteline items should be reset to Open (Not Cancel)..
                            var queryOL = @"Update Crm.OrderLines set OrderLineStatus = 7 
                                where QuoteLineId = @qlid";
                            connection.Execute(queryOL, new { qlid = pod.QuoteLineId });
                        }
                    }

                    ///Update order Total
                    // This is no more need to be called as cancelling a row will not affect the sales
                    // order in anyway.
                    //ordMgr.UpdateOrderTotal(Mpo.OrderId);

                    SetMasterPurchaseOrderStatus(MPOId);


                    // Need to update the crm.orders ...
                    string queryorderid = @"select top 1 orderid from crm.MasterPurchaseOrderExtn 
                                                where PurchaseOrderId = @mpoid";
                    var orderid = connection.ExecuteScalar<int>(queryorderid, new { mpoid = MPOId });

                    var orderManager = new OrderManager(this.User, this.Franchise);
                    orderManager.UpdateOrderStatusToOpen(orderid);

                    return Success;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public string CancelXMpo(int xmpoId, string cancelReason)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var user = SessionManager.CurrentUser.PersonId;
                var datetime = DateTime.UtcNow;
                connection.Open();
                var transaction = connection.BeginTransaction();

                try
                {
                    var queryPod = @"select pod.* from crm.PurchaseOrderDetails pod
                        where pod.PurchaseOrderId = @xmpoid";

                    var pods = connection.Query<PurchaseOrderDetails>(queryPod
                        , new { xmpoid = xmpoId }
                        , transaction).ToList();

                    foreach (var pod in pods)
                    {
                        pod.StatusId = (int)PurchaseOrderStatus.Canceled;
                        pod.Status = PurchaseOrderStatus.Canceled.GetDisplayName();
                        pod.UpdatedBy = user;
                        pod.UpdatedOn = datetime;
                        pod.ImpersonatorPersonId = this.ImpersonatorPersonId;
                        connection.Update(pod, transaction);

                        // Also we need to update the Crm.Orderlines to mpo so that it
                        // represent the same status for each quote lines.
                        // xmpo canclled quoteline items should be reset to Open (Not Cancel)..
                        var queryOL = @"Update Crm.OrderLines set OrderLineStatus = 7 
                                where QuoteLineId = @qlid";
                        connection.Execute(queryOL, new { qlid = pod.QuoteLineId }, transaction);

                        EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(pod)
                            , PurchaseOrdersDetailId: pod.PurchaseOrdersDetailId);
                    }

                    var queryMPo = @"Update [CRM].[MasterPurchaseOrder] set POStatusId=@statusId, CancelReason=@cancelReason where PurchaseOrderId = @xmpoid";
                    var result = connection.Execute(queryMPo, new
                    {
                        statusId = PurchaseOrderStatus.Canceled
                        ,
                        xmpoid = xmpoId
                        ,
                        cancelReason = cancelReason
                    }, transaction);

                    transaction.Commit();

                    var query = @"select orderid from crm.MasterPurchaseOrderExtn
                                    where PurchaseOrderId = @mpoid";
                    var orderids = connection.Query<int>(query, new { mpoid = xmpoId }).ToList();

                    // An xmpo can have multiple orders, and each need to be updated.
                    foreach (var item in orderids)
                    {
                        var orderManager = new OrderManager(this.User, this.Franchise);
                        orderManager.UpdateOrderStatusToOpen(item);
                    }



                    return Success;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public string CancelMPO(int MPOId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var usr = SessionManager.CurrentUser.PersonId;
                    var datetime = DateTime.UtcNow;

                    var fQuery = @"SELECT mpox.AccountId, mpox.OpportunityId, mpox.QuoteId, mpox.OrderId, mpo.* FROM crm.MasterPurchaseOrder mpo 
                                    left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                    INNER JOIN crm.PurchaseOrderDetails pod ON pod.PurchaseOrderId = mpo.PurchaseOrderId
                                    INNER JOIN crm.Opportunities o ON mpox.OpportunityId = o.OpportunityId 
                                    WHERE mpo.PurchaseOrderId = @PurchaseOrderId AND o.FranchiseId = @franchiseId;

                                   SELECT ql.QuoteLineNumber as LineNumber, pod.* 
                                    FROM crm.MasterPurchaseOrder mpo 
                                    left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                    INNER JOIN crm.PurchaseOrderDetails pod ON pod.PurchaseOrderId = mpo.PurchaseOrderId
                                   INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
                                   INNER JOIN crm.Opportunities o ON mpox.OpportunityId = o.OpportunityId 
                                    WHERE mpo.PurchaseOrderId = @PurchaseOrderId AND o.FranchiseId = @franchiseId;";
                    connection.Open();

                    var fDetails = connection.QueryMultiple(fQuery, new { PurchaseOrderId = MPOId, FranchiseId = SessionManager.CurrentFranchise.FranchiseId });

                    //var MPOList = fDetails.Read<MasterPurchaseOrder>().ToList();
                    var mpo = fDetails.Read<MasterPurchaseOrder>().FirstOrDefault();
                    var podList = fDetails.Read<PurchaseOrderDetails>().ToList();

                    foreach (var pod in podList)
                    {
                        pod.StatusId = (int)PurchaseOrderStatus.Canceled;
                        pod.Status = Enum.GetName(typeof(PurchaseOrderStatus), 8);
                        pod.UpdatedBy = usr;
                        pod.UpdatedOn = datetime;
                        pod.ImpersonatorPersonId = this.ImpersonatorPersonId;
                        connection.Update(pod);

                        // Also we need to update the Crm.Orderlines to mpo so that it
                        // represent the same status for each quote lines.
                        // xmpo canclled quoteline items should be reset to Open (Not Cancel)..
                        var queryOL = @"Update Crm.OrderLines set OrderLineStatus = 7 
                                where QuoteLineId = @qlid";
                        connection.Execute(queryOL, new { qlid = pod.QuoteLineId });

                        EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(pod), PurchaseOrdersDetailId: pod.PurchaseOrdersDetailId);
                    }
                    if (podList.Count == 0)
                    {
                        var query = @"SELECT mpox.AccountId, mpox.OpportunityId, mpox.QuoteId, mpox.OrderId, mpo.* FROM crm.MasterPurchaseOrder 
                                    left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                    where mpo.PurchaseOrderId =@purchaseOrderId ";
                        var MPO = connection.Query<MasterPurchaseOrder>(query, new { purchaseOrderId = MPOId }).FirstOrDefault();
                        var Updatequery = @"Update [CRM].[MasterPurchaseOrder] set POStatusId=@statusId where PurchaseOrderId =@purchaseOrderId";
                        var result = ExecuteIEnumerableObject<int>(ContextFactory.CrmConnectionString, Updatequery, new
                        {
                            statusId = (int)PurchaseOrderStatus.Canceled,
                            purchaseOrderId = MPOId
                        });
                    }
                    else
                    {
                        SetMasterPurchaseOrderStatus(MPOId);
                    }

                    var orderManager = new OrderManager(this.User, this.Franchise);

                    // TODO: This also should be part of a transaction..
                    orderManager.UpdateOrderStatusToOpen(mpo.OrderId);
                    return Success;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool UpdateProcessingXMpo(int xmpoId, PoAsync poa)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var user = SessionManager.CurrentUser.PersonId;
                    var datetime = DateTime.UtcNow;

                    var query = @"select * from crm.MasterPurchaseOrder where PurchaseOrderId = @xmpoid;

                                select ql.ProductTypeId, pod.* from crm.PurchaseOrderDetails pod
                                left join crm.QuoteLines ql on ql.QuoteLineId = pod.QuoteLineId 
                                where PurchaseOrderId = @xmpoid";
                    var tresult = connection.QueryMultiple(query, new { xmpoid = xmpoId });
                    var xmpo = tresult.Read<MasterPurchaseOrder>().FirstOrDefault();
                    var pods = tresult.Read<PurchaseOrderDetails>().ToList();

                    xmpo.POStatusId = (int)PurchaseOrderStatus.Processing;
                    xmpo.Status = Enum.GetName(typeof(PurchaseOrderStatus), 9);
                    xmpo.Message = null;
                    xmpo.CounterId = poa.Counter;
                    xmpo.SubmittedDate = datetime;
                    xmpo.SubmittedTOPIC = true;
                    xmpo.UpdatedBy = user;
                    xmpo.UpdateOn = datetime;
                    xmpo.SubmittedBy = user;
                    xmpo.ImpersonatorPersonId = this.ImpersonatorPersonId;
                    connection.Update(xmpo);

                    foreach (var pod in pods)
                    {
                        // Only core product type..
                        if (pod.ProductTypeId == 1)
                        {
                            pod.StatusId = (int)PurchaseOrderStatus.Processing;
                            pod.Status = Enum.GetName(typeof(PurchaseOrderStatus), 9);
                            pod.Errors = null;
                            pod.Information = null;
                            pod.Warning = null;
                            pod.UpdatedBy = user;
                            pod.UpdatedOn = datetime;
                            pod.ImpersonatorPersonId = this.ImpersonatorPersonId;
                            connection.Update(pod);
                        }
                    }

                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool UpdateProcessingMPO(int OrderId, int purchaseOrderId, PoAsync poa)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    var usr = SessionManager.CurrentUser.PersonId;
                    var datetime = DateTime.UtcNow;

                    var fQuery = @"SELECT mpox.AccountId, mpox.OpportunityId, mpox.QuoteId, mpox.OrderId, mpo.* FROM crm.MasterPurchaseOrder mpo 
                                   left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                    INNER JOIN crm.PurchaseOrderDetails pod ON pod.PurchaseOrderId = mpo.PurchaseOrderId
                                   INNER JOIN crm.Opportunities o ON mpox.OpportunityId = o.OpportunityId 
                                    WHERE mpox.PurchaseOrderId = @PurchaseOrderId AND o.FranchiseId = @franchiseId;
                                   
                                    SELECT ql.QuoteLineNumber as LineNumber,pod.*,ql.ProductTypeId FROM crm.MasterPurchaseOrder mpo
                                    left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                   INNER JOIN crm.PurchaseOrderDetails pod ON pod.PurchaseOrderId = mpo.PurchaseOrderId
                                   INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
                                   INNER JOIN crm.Opportunities o ON mpox.OpportunityId = o.OpportunityId 
                                    WHERE mpox.PurchaseOrderId = @PurchaseOrderId AND o.FranchiseId = @franchiseId;";
                    //connection.Open();

                    var fDetails = connection.QueryMultiple(fQuery, new
                    {
                        PurchaseOrderId = purchaseOrderId
                            ,
                        FranchiseId = SessionManager.CurrentFranchise.FranchiseId
                    }
                        , transaction);


                    var MPO = fDetails.Read<MasterPurchaseOrder>().FirstOrDefault();
                    var podList = fDetails.Read<PurchaseOrderDetails>().ToList();

                    MPO.POStatusId = (int)PurchaseOrderStatus.Processing;
                    MPO.Status = Enum.GetName(typeof(PurchaseOrderStatus), 9);
                    MPO.Message = null;
                    MPO.CounterId = poa.Counter;
                    MPO.SubmittedDate = datetime;
                    MPO.SubmittedTOPIC = true;
                    MPO.UpdatedBy = usr;
                    MPO.UpdateOn = datetime;
                    MPO.SubmittedBy = usr;
                    MPO.ImpersonatorPersonId = this.ImpersonatorPersonId;
                    connection.Update(MPO, transaction);
                    //EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(MPO), PurchaseOrderId: MPO.PurchaseOrderId);

                    foreach (var pod in podList)
                    {
                        if (pod.ProductTypeId == 1 && pod.StatusId != 8)
                        {
                            pod.StatusId = (int)PurchaseOrderStatus.Processing;
                            pod.Status = Enum.GetName(typeof(PurchaseOrderStatus), 9);
                            pod.Errors = null;
                            pod.Information = null;
                            pod.Warning = null;
                            pod.UpdatedBy = usr;
                            pod.UpdatedOn = datetime;
                            pod.ImpersonatorPersonId = this.ImpersonatorPersonId;
                            connection.Update(pod, transaction);
                            //EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(pod), PurchaseOrdersDetailId: pod.PurchaseOrdersDetailId);
                        }
                    }

                    transaction.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool UpdatePODetailsMessage(int PurchaseOrderId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var sql = @"SELECT pod.* FROM crm.PurchaseOrderDetails pod
                                INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId AND ql.ProductTypeId=1
                               WHERE  pod.PurchaseOrderId= @PurchaseOrderId; ";
                    var purchaseOrderDetails = connection
                        .Query<PurchaseOrderDetails>(sql, new { PurchaseOrderId = PurchaseOrderId }).ToList();
                    foreach (var poDetails in purchaseOrderDetails)
                    {
                        poDetails.Information = "";
                        poDetails.Warning = "";
                        poDetails.Errors = "";
                        poDetails.Status = "";
                        poDetails.StatusId = 0;
                        poDetails.UpdatedBy = AdminPersonId;
                        poDetails.UpdatedOn = DateTime.UtcNow;
                        poDetails.ImpersonatorPersonId = this.ImpersonatorPersonId;
                        connection.Update(poDetails);
                        //EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(poDetails), PurchaseOrdersDetailId: poDetails.PurchaseOrdersDetailId);
                    }
                    return true;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool UpdateMasterPOError(MasterPurchaseOrder mpo)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    mpo.POStatusId = (int)PurchaseOrderStatus.Error;
                    mpo.Status = Enum.GetName(typeof(PurchaseOrderStatus), 10);
                    mpo.UpdatedBy = AdminPersonId;
                    mpo.UpdateOn = DateTime.UtcNow;
                    mpo.ImpersonatorPersonId = this.ImpersonatorPersonId;
                    connection.Update(mpo);
                    //EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(mpo), PurchaseOrderId: mpo.PurchaseOrderId);

                    var sql = @"SELECT pod.* FROM crm.PurchaseOrderDetails pod
                               INNER JOIN crm.QuoteLines ql ON pod.QuoteLineId = ql.QuoteLineId
                               WHERE ql.ProductTypeId = 1 AND pod.PurchaseOrderId= @PurchaseOrderId; ";
                    var purchaseOrderDetails = connection
                        .Query<PurchaseOrderDetails>(sql, new { PurchaseOrderId = mpo.PurchaseOrderId }).ToList();
                    foreach (var poDetails in purchaseOrderDetails)
                    {
                        poDetails.Status = "";
                        poDetails.StatusId = 0;
                        poDetails.UpdatedBy = AdminPersonId;
                        poDetails.UpdatedOn = DateTime.UtcNow;
                        poDetails.ImpersonatorPersonId = this.ImpersonatorPersonId;
                        connection.Update(poDetails);
                        //EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(poDetails), PurchaseOrdersDetailId: poDetails.PurchaseOrdersDetailId);
                    }
                    return true;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void UpdateMasterPOCancelReason(MasterPurchaseOrder mpo)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    mpo.UpdatedBy = AdminPersonId;
                    mpo.UpdateOn = DateTime.UtcNow;
                    mpo.ImpersonatorPersonId = this.ImpersonatorPersonId;
                    connection.Update(mpo);
                    EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(mpo), PurchaseOrderId: mpo.PurchaseOrderId);
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool UpdateVPOError(int quotelineid, List<string> message, MasterPurchaseOrder mpo, int typeMsg)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var msg = "";
                    var fQuery = @"select ql.QuoteLineNumber as LineNumber,pod.* from crm.purchaseorderDetails pod INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
                                   where pod.quotelineid = @quotelineid AND pod.PurchaseOrderId=@PurchaseOrderId;";
                    connection.Open();

                    var purchaseOrderDetails = connection
                        .Query<PurchaseOrderDetails>(fQuery, new { quotelineid = quotelineid, PurchaseOrderId = mpo.PurchaseOrderId }).FirstOrDefault();

                    if (message != null && message.Count > 0)
                    {
                        msg = "<ul>";
                        foreach (var error in message)
                        {
                            msg = msg + "<li>" + error + "</li>";
                        }

                        msg = msg + "</ul>";
                    }
                    //error
                    if (typeMsg == 1)
                    {
                        purchaseOrderDetails.Errors = msg;
                        purchaseOrderDetails.StatusId = (int)PurchaseOrderStatus.Error;
                        purchaseOrderDetails.Status = Enum.GetName(typeof(PurchaseOrderStatus), 10);
                        mpo.POStatusId = (int)PurchaseOrderStatus.Error;
                        mpo.Status = Enum.GetName(typeof(PurchaseOrderStatus), 10);
                    }
                    //Information
                    if (typeMsg == 2)
                    {
                        purchaseOrderDetails.Information = msg;
                    }
                    //warning
                    if (typeMsg == 3)
                    {
                        purchaseOrderDetails.Warning = msg;
                    }

                    purchaseOrderDetails.UpdatedOn = DateTime.UtcNow;
                    purchaseOrderDetails.ImpersonatorPersonId = this.ImpersonatorPersonId;
                    connection.Update(purchaseOrderDetails);
                    //EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(purchaseOrderDetails), PurchaseOrdersDetailId: purchaseOrderDetails.PurchaseOrdersDetailId);


                    mpo.UpdateOn = DateTime.UtcNow;
                    mpo.ImpersonatorPersonId = this.ImpersonatorPersonId;
                    connection.Update(mpo);
                    //EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(mpo), PurchaseOrderId: mpo.PurchaseOrderId);

                    return true;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        /// <summary>
        /// Get all the MPOs with status of Processing and make a call to PIC to get MPO Details
        /// </summary>
        /// <returns></returns>
        public bool CheckAsyncMPO()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var fQuery = @"SELECT distinct mpox.AccountId, mpox.OpportunityId, mpox.QuoteId, mpox.OrderId, mpo.* FROM crm.MasterPurchaseOrder mpo
                                    left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                   INNER JOIN crm.PurchaseOrderDetails pod ON pod.PurchaseOrderId = mpo.PurchaseOrderId AND pod.StatusId NOT IN (10) AND mpo.POStatusId NOT IN (10)
                                   INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId AND ql.ProductTypeId=1
                                   where (pod.StatusId in (9,0) or mpo.POStatusId in (0,9)) AND mpo.SubmittedTOPIC =1";
                    connection.Open();

                    var listMasterPurchaseOrders = connection.Query<MasterPurchaseOrder>(fQuery).ToList();

                    foreach (var item in listMasterPurchaseOrders)
                    {
                        try
                        {
                            if (item.CounterId.HasValue)
                            {
                                var res = _picMgr.CheckMPOAsyncStatus(item.CounterId.Value);
                                var respo = JsonConvert.DeserializeObject<PoAsyncCounterforError>(res);

                                //dynamic pores = JsonConvert.DeserializeObject<ExpandoObject>(res);
                                UpdatePODetailsMessage(item.PurchaseOrderId);

                                dynamic orderobj = new ExpandoObject();
                                //var orderobj = GetOrderObject(item.OrderId);
                                if (item.IsXMpo)
                                {
                                    orderobj = GetOrderObjectXMpo(item.PurchaseOrderId);
                                }
                                else
                                {
                                    orderobj = GetOrderObject(item.OrderId);
                                }
                                try
                                {
                                    if (respo.information != null && respo.information.Items != null &&
                                respo.information.Items.Count > 0)
                                {
                                    ///information
                                    if (orderobj.Items != null && orderobj.Items.Count > 0)
                                    {
                                            foreach (var errItem in respo.information.Items)
                                        {
                                            for (var i = 0; i < orderobj.Items.Count; i++)
                                            {
                                                if (i + 1 == errItem.Item)
                                                {
                                                    UpdateVPOError(orderobj.Items[i].item, errItem.message, item, 2);//information
                                                }
                                            }
                                        }
                                    }
                                }
                                }
                                catch(Exception e)
                                {
                                    EventLogger.LogEvent(e);
                                }

                                try
                                {
                                    if (respo.warning != null && respo.warning.Items != null &&
                                        respo.warning.Items.Count > 0)
                                {
                                    ///warning
                                    if (orderobj.Items != null && orderobj.Items.Count > 0)
                                    {
                                            foreach (var errItem in respo.warning.Items)
                                        {
                                            for (var i = 0; i < orderobj.Items.Count; i++)
                                            {
                                                if (i + 1 == errItem.Item)
                                                {
                                                    UpdateVPOError(orderobj.Items[i].item, errItem.message, item, 3);//warning
                                                }
                                            }
                                        }
                                    }
                                }
                                }
                                catch (Exception e)
                                {
                                    EventLogger.LogEvent(e);
                                }
                                if (!respo.valid)
                                {

                                    try
                                {
                                        if (respo.error.Order != null && respo.error.Order.Count > 0)
                                    {
                                        item.Message = "<ul>";
                                        var msglst = new List<string>();
                                            foreach (var errItem in respo.error.Order)
                                        {
                                            if (errItem.ToUpper().Contains("DUPLICATE"))
                                            {
                                                var msg =
                                                       "This MPO has already been submitted for Processing. Please contact your site administrator.";
                                                item.Message = item.Message + "<li>" + msg + " </li>";
                                                msglst.Add(msg);
                                            }
                                            else
                                            {
                                                item.Message = item.Message + "<li>" + errItem + " </li>";
                                                msglst.Add(errItem);
                                            }
                                        }

                                        item.Message = item.Message + "</ul>";

                                        UpdateMasterPOError(item);

                                        for (var i = 0; i < orderobj.Items.Count; i++)
                                        {
                                            UpdateVPOError(orderobj.Items[i].item, msglst, item, 1);//error
                                        }
                                        SendEmail(item.PurchaseOrderId, "");
                                    }
                                    }
                                    catch (Exception e)
                                    {
                                        EventLogger.LogEvent(e);
                                    }

                                    try
                                    {
                                    ///Errors
                                    if (orderobj.Items != null && orderobj.Items.Count > 0)
                                    {
                                            foreach (var errItem in respo.error.Items)
                                        {
                                            for (var i = 0; i < orderobj.Items.Count; i++)
                                            {
                                                if (i + 1 == errItem.Item)
                                                {
                                                    UpdateVPOError(orderobj.Items[i].item, errItem.message, item, 1);//error
                                                }
                                            }
                                        }
                                    }
                                    SendEmail(item.PurchaseOrderId, "");
                                    }
                                    catch (Exception e)
                                    {
                                        EventLogger.LogEvent(e);
                                    }
                                }
                                else
                                {
                                    var pores = JsonConvert.DeserializeObject<PoAsyncCounter>(res);
                                    var poOrder = InsertOrUpdatePurchaseOrder(item.PurchaseOrderId, res);//SendEmail(item.OrderId, 6);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            EventLogger.LogEvent(ex);
                            continue;
                        }
                    }

                    return true;
                    //string response = _picMgr.SubmitPICOrder_Async(orderobj);

                    //var pores = JsonConvert.DeserializeObject<PoAsync>(response);
                    //if (pores.Valid == true)
                    //{
                    //    var poOrder = UpdateProcessingMPO(orderId, pores);
                    //    return Success;
                    //}
                    //else
                    //{
                    //    string err = "";
                    //    if (pores.Error.Count > 0)
                    //    {
                    //        foreach (var item in pores.Error)
                    //        {
                    //            err = err + item;
                    //        }
                    //    }
                    //    return err;
                    //}
                }
                //catch (Exception ex)
                //{
                //    //EventLogger.LogEvent(ex);
                //    throw ex;
                //}
                finally
                {
                    connection.Close();
                }
            }
        }

        public MasterPurchaseOrder GetMasterPurchaseOrderNoFranchise(int purchaseOrderId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var fQuery = @"SELECT ord.OrderNumber, mpox.AccountId, mpox.OpportunityId, mpox.QuoteId, mpox.OrderId
                                    ,  mpo.*, o.OpportunityName,o.SideMark
                                    ,[CRM].[fnGetAccountName_forOpportunitySearch](mpox.OpportunityId) AS AccountName
                                    , tp.POStatusName AS Status, ord.OrderTotal AS Total,
		                            STUFF(
                                    COALESCE(', ' + NULLIF(Address1, ''), '')  +
		                            COALESCE(', ' + NULLIF(Address2, ''), '')  +
                                    COALESCE(', ' + NULLIF(City, ''), '') +
		                            COALESCE(', ' + NULLIF([State], ''), '') +
                                    COALESCE(' ' + NULLIF(ZipCode, ''), '') +
                                    COALESCE(', ' + NULLIF(CountryCode2Digits , ''), ''),
                                    1, 1, '') AS Address
                                    FROM crm.MasterPurchaseOrder mpo
                                    left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                    INNER JOIN crm.Type_POStatus tp ON tp.POStatusId = mpo.POStatusId
                                    INNER JOIN crm.orders ord ON ord.OrderId = mpox.OrderID
                                    INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
								    LEFT JOIN crm.Addresses a ON a.AddressId = mpo.ShipAddressId
                                    WHERE mpo.PurchaseOrderId = @PurchaseOrderId;

                                    SELECT pod.PurchaseOrdersDetailId,pod.PurchaseOrderId,pod.QuoteLineId,pod.PartNumber,ql.ProductName,ql.ProductTypeId,pod.QuantityPurchased AS purchased,
                                    ql.VendorName,ql.Description,ql.Quantity,pod.PartNumber,pod.ShipDate,pod.PromiseByDate,pod.Status,pod.NotesVendor,pod.PICPO,pod.POResponse,pod.StatusId,pod.QuantityPurchased AS Quantity,
                                    pod.PONumber,pod.Errors,  pod.PickedBy  FROM crm.PurchaseOrderDetails pod
                                    INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
                                    INNER JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = pod.PurchaseOrderId
                                    left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                    INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
                                    WHERE ql.ProductTypeId = 1 AND  pod.PurchaseOrderId = @PurchaseOrderId;

                                    SELECT pod.PurchaseOrdersDetailId,pod.PurchaseOrderId,pod.QuoteLineId,pod.PartNumber,ql.ProductName,ql.ProductTypeId,pod.QuantityPurchased AS purchased,
                                    ql.VendorName,ql.Description,ql.Quantity,pod.PartNumber,pod.ShipDate,pod.PromiseByDate,pod.Status,pod.NotesVendor,pod.PICPO,pod.StatusId,pod.QuantityPurchased AS Quantity,
                                    pod.PONumber,  pod.PickedBy  FROM crm.PurchaseOrderDetails pod
                                    INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
                                    INNER JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = pod.PurchaseOrderId
                                    left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                    INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
                                    WHERE ql.ProductTypeId IN (2,5) AND  pod.PurchaseOrderId = @PurchaseOrderId;";

                    connection.Open();

                    var fDetails = connection.QueryMultiple(fQuery, new { PurchaseOrderId = purchaseOrderId });

                    var Mpo = fDetails.Read<MasterPurchaseOrder>().FirstOrDefault();
                    var vpocoreDetails = fDetails.Read<VPODetail>().ToList();
                    var vpomyVendorDetails = fDetails.Read<VPODetail>().ToList();
                    Mpo.CoreProductVPOs = vpocoreDetails;
                    Mpo.MyProductVPOs = vpomyVendorDetails;
                    Mpo.CreateDate = TimeZoneManager.ToLocal(Mpo.CreateDate);
                    Mpo.CreatedOn = TimeZoneManager.ToLocal(Mpo.CreatedOn);
                    Mpo.UpdateOn = TimeZoneManager.ToLocal(Mpo.UpdateOn);
                    Mpo.SubmittedDate = Mpo.SubmittedDate.HasValue
                        ? TimeZoneManager.ToLocal(Mpo.SubmittedDate.Value)
                        : Mpo.SubmittedDate;

                    return Mpo;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool EmailAlreadySent(int purchaseOrderId, string Recipients, string Sidemark)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var dup_query = @"IF EXISTS(SELECT TOP 1 * FROM History.SentEmails se 
                                    INNER JOIN crm.Accounts a ON a.FranchiseId = se.FranchiseId
                                    INNER JOIN CRM.MasterPurchaseOrderExtn mpoe ON a.AccountId = mpoe.AccountId AND se.OrderId = mpoe.OrderId
                                    WHERE mpoe.PurchaseOrderId=@purchaseOrderId and se.TemplateId=519 AND se.Recipients =@Recipients AND se.SentAt <   getutcdate()-1 AND se.Body LIKE  @sidemark
									)
                                    BEGIN SELECT 1 END
                                    ELSE 
                                    BEGIN SELECT 0 END";



                    //duplicate check query

                    var dupExists = ExecuteIEnumerableObject<bool>(
                               ContextFactory.CrmConnectionString, dup_query, new { purchaseOrderId = purchaseOrderId, Recipients = @Recipients, sidemark = "%" + Sidemark + "%" }).FirstOrDefault();
                    return dupExists;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public string SendEmail(int purchaseOrderId, string Streamid = "")
        {
            List<string> toAddresses = new List<string>();
            List<string> ccAddresses = null;
            List<string> bccAddresses = null;
            CustomerTP tocustomerAddress = new CustomerTP();
            var mpo = GetMasterPurchaseOrderNoFranchise(purchaseOrderId);
            //BUILD - From email address
            //var fromEmailAddress = eManager.GetFranchiseEmail(SessionManager.CurrentFranchise.FranchiseId, emailType);// SessionManager.CurrentUser.Person.PrimaryEmail;
            //if (string.IsNullOrEmpty(fromEmailAddress))
            //{
            //    fromEmailAddress = SessionManager.CurrentUser.Email;
            //}
            string fromEmailAddress = "noreply@budgetblinds.com";/*new MailAddress(fromEmailAddress, SessionManager.CurrentUser.Person.FullName);*/

            tocustomerAddress = eManager.GetVPOSubmitEmail(mpo.SubmittedBy.Value);

            if (tocustomerAddress != null)
            {
                if (tocustomerAddress.PrimaryEmail.Trim() != "")
                {
                    toAddresses.Add(tocustomerAddress.PrimaryEmail.Trim());
                }
                //if (tocustomerAddress.SecondaryEmail.Trim() != "")
                //{
                //    toAddresses.Add(tocustomerAddress.SecondaryEmail.Trim());
                //}
            }

            //If no from or to emails return
            if (toAddresses == null || toAddresses.Count == 0)
                return "Success";

            //Preparing email Body with formating
            string TemplateSubject = "", bodyHtml = "";
            int FranchiseId = 0;

            //Take Body Html from EmailTemplate Table
            var emailtemplate = eManager.GetHtmlBody(EmailType.ProcurementAlert, 1); //8, 1);
            TemplateSubject = emailtemplate.TemplateSubject;
            bodyHtml = emailtemplate.TemplateLayout;
            short TemplateEmailId = (short)emailtemplate.EmailTemplateId;
            var mailSent = EmailAlreadySent(mpo.PurchaseOrderId, string.Join(",", toAddresses), mpo.SideMark);
            if (!mailSent)
            {
                var mailMsg = eManager.BuildMailMessageError(mpo,
                purchaseOrderId, bodyHtml, TemplateSubject, TemplateEmailId, 1, FranchiseId,
                fromEmailAddress, toAddresses, ccAddresses, bccAddresses, Streamid);
            }
            return "Success";
        }

        //public string ConvertOrdertoMPO(int orderId)
        //{
        //    using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
        //    {
        //        try
        //        {
        //            var fQuery = @"SELECT * FROM crm.MasterPurchaseOrder mpo INNER JOIN crm.Opportunities o ON o.OpportunityId = mpo.OpportunityId WHERE OrderId = @orderId AND o.FranchiseId = FranchiseId;";
        //            connection.Open();

        //            var fDetails = connection.Query<MasterPurchaseOrder>(fQuery, new { orderId = orderId, FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).FirstOrDefault();

        //            var orderobj = GetOrderObject(orderId);
        //            string response = _picMgr.AddOrder(orderobj);
        //            dynamic poResponse = JsonConvert.DeserializeObject<ExpandoObject>(response);

        //            var pores = JsonConvert.DeserializeObject<PICSalesOrderVM>(response);
        //            if (pores.valid == true)
        //            {
        //                var poOrder = InsertOrUpdatePurchaseOrder(orderId, response);

        //                return Success;
        //            }
        //            else
        //            {
        //                string err = "";
        //                if (pores.error.Count > 0)
        //                {
        //                    foreach (var item in pores.error)
        //                    {
        //                        err = err + item;
        //                    }
        //                }
        //                return err;
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            //EventLogger.LogEvent(ex);
        //            throw ex;
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }
        //    }
        //}

        public string SubmitXMpo(int xmpoId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    // TODO: we need to add the FranchiseId in crm.MasterPurchaseOrder so that
                    // when multiple order involved in a xMpo, it will be easy to handle otherwise
                    // we need to apply disticnt. Is there any otehr way?
                    var query = @"select pod.* From crm.PurchaseOrderDetails pod
                                left join crm.QuoteLines ql on ql.QuoteLineId = pod.QuoteLineId
                                --left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = pod.PurchaseOrderId
                                where pod.PurchaseOrderId = @xmpoid
                                and pod.StatusId in (1, 10, 0) and ql.ProductTypeId = 1
                                order by pod.CreatedOn desc";
                    var pods = connection.Query<PurchaseOrderDetails>(query, new { xmpoid = xmpoId }).ToList();

                    if (pods == null || pods.Count == 0)
                        return "At least one core product should be in open status.";

                    // We have at least one core product in the xmpo with open status.
                    var orderobj = GetOrderObjectXMpo(xmpoId);
                    string response = _picMgr.SubmitPICOrder_Async(orderobj);

                    var pores = JsonConvert.DeserializeObject<PoAsync>(response);
                    if (pores.Valid)
                    {
                        var poOrder = UpdateProcessingXMpo(xmpoId, pores);
                        return Success;
                    }
                    else
                    {
                        string err = "";
                        if (pores.Error.Count > 0)
                        {
                            foreach (var item in pores.Error)
                            {
                                err = err + item;
                            }
                        }
                        return err;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }


        // The method name is misleading/confusing, so changed to SubmitMPO -murugan
        //public string ConvertOrdertoMPO_(int orderId)
        public string SubmitMPO(int orderId, int purchaseOrderId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var fQuery = @"SELECT mpox.AccountId, mpox.OpportunityId, mpox.QuoteId, mpox.OrderId, mpo.* FROM crm.MasterPurchaseOrder mpo 
                                left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId 
                                WHERE OrderId = @orderId AND o.FranchiseId = FranchiseId;";
                    connection.Open();

                    var fDetails = connection.Query<MasterPurchaseOrder>(fQuery, new { orderId = orderId, FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).FirstOrDefault();

                    var query = @"SELECT pod.* FROM crm.PurchaseOrderDetails pod
                                  INNER JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = pod.PurchaseOrderId
                                    left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                  INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
                                  INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
                                  WHERE ql.ProductTypeId=1 and pod.StatusId in (1,10,0) AND mpox.OrderId = @OrderId 
                                  AND o.FranchiseId = @FranchiseId";

                    var podlist = connection.Query<MasterPurchaseOrder>(query, new { OrderId = orderId, FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();

                    if (podlist != null && podlist.Count > 0)
                    {
                        var orderobj = GetOrderObject(orderId);
                        string response = _picMgr.SubmitPICOrder_Async(orderobj);

                        var pores = JsonConvert.DeserializeObject<PoAsync>(response);
                        if (pores.Valid == true)
                        {
                            var poOrder = UpdateProcessingMPO(orderId, purchaseOrderId, pores);
                            return Success;
                        }
                        else
                        {
                            string err = "";
                            if (pores.Error.Count > 0)
                            {
                                foreach (var item in pores.Error)
                                {
                                    err = err + item;
                                }
                            }
                            return err;
                        }
                    }
                    else
                    {
                        return "At least one core product should be in open status.";
                    }
                }
                catch (Exception ex)
                {
                    //EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void ConvertOrderToXMasterPurchaseOrders(List<int> orderIds)
        {
            if (orderIds == null || orderIds.Count == 0)
                throw new Exception("No OrderIds to convert to Xmpo");

            // comma separated orderids to be converted into xmpo.
            var _orderIds = string.Join(",", orderIds);

            var query = @"-- Quote lines related to BulkPurchase optout vendors, to creat xMPO
                        select o.OrderID, ql.* from crm.QuoteLines ql 
                        left join crm.Quote q on q.QuoteKey = ql.QuoteKey
                        left join crm.Orders o on o.QuoteKey = q.QuoteKey
                        left join acct.FranchiseVendors fv on fv.VendorId = ql.VendorId and fv.FranchiseId = @franchiseId
                        where o.OrderID in @orderids
                            and isnull(ql.VendorId, 0) > 0 and isnull(fv.BulkPurchase, 0) = 1
                        order by ql.VendorId
                        
                        -- This is no mroe needed as we are showing only the bulk purchase enabled
                        -- vendor related items in the list.
                        -- Quote lines related to BulkPurchase optout vendors, which need to follow
                        -- the previous MPO process
                        -- select o.OrderID, ql.* from crm.QuoteLines ql 
                        -- left join crm.Quote q on q.QuoteKey = ql.QuoteKey
                        -- left join crm.Orders o on o.QuoteKey = q.QuoteKey
                        -- left join acct.FranchiseVendors fv on fv.VendorId = ql.VendorId and fv.FranchiseId = @franchiseId
                        -- where o.OrderID in @orderids
                            -- and isnull(ql.VendorId, 0) > 0 and isnull(fv.BulkPurchase, 0) = 0
                        -- order by o.OrderID";


            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                //var transaction = connection.BeginTransaction();
                //connection.Open();

                connection.Open();
                var transaction = connection.BeginTransaction();

                try
                {
                    var tResult = connection.QueryMultiple(query
                    , new
                    {
                        franchiseId = this.Franchise.FranchiseId
                        ,
                        orderids = orderIds
                    }, transaction);

                    var quoteLinesXmpo = tResult.Read<QuoteLines>().ToList();

                    // no more mpo
                    // var quoteLinesMpo = tResult.Read<QuoteLines>().ToList();

                    var vendors = quoteLinesXmpo.GroupBy(g => g.VendorId)
                                                .Select(v => v.First())
                                                .Select(o => new
                                                {
                                                    VendorId = o.VendorId
                                                })
                                                .ToList();

                    var user = this.User.PersonId;

                    var _qeuryMpoPoNumber = @" SELECT TOP 1 ISNULL(MasterPONumber,0) FROM crm.MasterPurchaseOrder mpo
                                        left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                        INNER JOIN crm.Opportunities co ON co.OpportunityId = mpox.OpportunityId
                                        WHERE co.FranchiseId = @franchiseId ORDER BY mpo.PurchaseOrderId desc;

                                        SELECT TOP 1 ISNULL(PONumber,0) FROM crm.PurchaseOrderDetails pod
                                        INNER JOIN  crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = pod.PurchaseOrderId
                                        left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                        INNER JOIN crm.Opportunities co ON co.OpportunityId = mpox.OpportunityId
                                        WHERE co.FranchiseId = @franchiseId ORDER BY pod.PONumber desc;

                                ";

                    // Create xMPO for each vendors from the selected orders, which are enabled
                    // who are enabled the BulkPurchase options..
                    foreach (var vendor in vendors)
                    {
                        // Get list of quotelines matching with the current vendors from differnt orders
                        // to make a single Vendor Purhase Order (xMPO)
                        var qlines = quoteLinesXmpo.Where(v => v.VendorId == vendor.VendorId)
                                                   .ToList();

                        var pods = new List<PurchaseOrderDetails>();

                        var queryMpoPoNumber = _qeuryMpoPoNumber + @"
                                        select fv.VendorId, fv.VendorIdPk, fv.BPShipLocation, fv.ShipViaAccountNo
                                        , fv.BPAccountNo, fv.Carrier, stl.AddressId ShipAddressId
                                         from acct.FranchiseVendors fv
                                         left join crm.ShipToLocations stl on stl.Id = fv.BPShipLocation and stl.FranchiseId = @franchiseId
                                         where fv.FranchiseId = @franchiseId and fv.VendorId = @vendorid ";


                        int mpoNumber = 1000, poNumber = 1000;
                        var dateTime = DateTime.UtcNow;

                        var xResult = connection.QueryMultiple(queryMpoPoNumber
                                            , new
                                            {
                                                franchiseId = this.Franchise.FranchiseId
                                                ,
                                                vendorid = vendor.VendorId
                                            }, transaction);
                        var xMpoNumber = xResult.Read<int>().FirstOrDefault();
                        var xPONumber = xResult.Read<int>().FirstOrDefault();
                        var fVendor = xResult.Read<FranchiseVendor>().FirstOrDefault();

                        if (xMpoNumber > 0) mpoNumber = xMpoNumber + 1;
                        if (xPONumber > 0) poNumber = xPONumber + 1;

                        //Cretate the xMpo for this Vendor Purchase Order.
                        var xmpo = new MasterPurchaseOrder();
                        xmpo.ShipToLocation = fVendor.BPShipLocation;
                        xmpo.ShipAddressId = fVendor.ShipAddressId;
                        xmpo.MasterPONumber = mpoNumber.ToString();
                        xmpo.CreateDate = dateTime;
                        xmpo.SubmittedTOPIC = false;
                        xmpo.POStatusId = 1;
                        // TODO : xmpo.TerritoryId
                        xmpo.DropShip = false;
                        xmpo.ConfirmAddress = false;
                        xmpo.CreatedBy = user;
                        xmpo.CreatedOn = dateTime;
                        // TODO: The following should not be required at the creation time however, in the it
                        // designed as not null
                        xmpo.UpdateOn = dateTime;
                        xmpo.UpdatedBy = user;
                        xmpo.ImpersonatorPersonId = this.ImpersonatorPersonId;
                        xmpo.IsXMpo = true;
                        xmpo.VendorId = vendor.VendorId;
                        var xMpoId = connection.Insert<int, MasterPurchaseOrder>(xmpo, transaction);


                        // One Xmpo spans multiple orders and we need all the orders participating
                        // in one xmpo. Not sure how it will be helpful though.
                        var orders = qlines.GroupBy(g => g.OrderId)
                                                .Select(o => o.First())
                                                .Select(x => new { OrderId = x.OrderId })
                                                .ToList();
                        foreach (var item in orders)
                        {
                            var order = ordMgr.GetOrderByOrderId(item.OrderId);
                            var xmpox = new MasterPurchaseOrderExtn();
                            xmpox.PurchaseOrderId = xMpoId;
                            xmpox.AccountId = order.AccountId;
                            xmpox.OpportunityId = order.OpportunityId;
                            xmpox.QuoteId = order.QuoteKey;
                            xmpox.OrderId = order.OrderID;
                            xmpox.CreatedBy = user;
                            xmpox.CreatedOn = dateTime;
                            xmpox.UpdatedBy = user;
                            xmpox.UpdateOn = dateTime;
                            connection.Insert<int, MasterPurchaseOrderExtn>(xmpox, transaction);


                        }

                        foreach (var ql in qlines)
                        {
                            // we are not dealing product type 3:Service and 4:Discount,
                            if (!(ql.ProductTypeId == 1 ||
                                ql.ProductTypeId == 2 ||
                                ql.ProductTypeId == 5)) continue;

                            var myProductQuery = @"select * from crm.franchiseproducts where productKey = @productId";
                            var myProduct = ExecuteIEnumerableObject<FranchiseProducts>(
                                ContextFactory.CrmConnectionString, myProductQuery, new
                                {
                                    productId = Convert.ToInt32(ql.ProductId)
                                }).FirstOrDefault();

                            // The products are either 1:Core Product, 2: My Product, or 3: One Time Product
                            var pd = new PurchaseOrderDetails();
                            pd.Status = "Open";
                            pd.StatusId = 1;
                            if (ql.ProductTypeId == 1)
                            {
                                pd.PartNumber = ql.ProductId.ToString();
                            }
                            else if (ql.ProductTypeId == 2)
                            {
                                pd.PartNumber = myProduct != null ? myProduct.VendorProductSKU : "";
                                pd.PONumber = poNumber;
                            }
                            else
                            {
                                pd.PONumber = poNumber;
                            }
                            pd.PurchaseOrderId = xMpoId;
                            pd.QuoteLineId = ql.QuoteLineId;
                            pd.VendorId = ql.VendorId.Value;
                            pd.QuantityPurchased = ql.Quantity.Value;
                            pd.NotesVendor = ql.VendorNotes;
                            pd.CreatedBy = user;
                            pd.CreatedOn = dateTime;
                            pd.UpdatedOn = dateTime;
                            pd.UpdatedBy = user;
                            // Updateon and updateby is not required while creating an item.
                            pd.ImpersonatorPersonId = this.ImpersonatorPersonId;
                            //pods.Add(pd);

                            // Also we need to update the Crm.Orderlines to mpo so that it
                            // represent the same status for each quote lines.
                            var queryOL = @"Update Crm.OrderLines set OrderLineStatus = 1 
                                where QuoteLineId = @qlid";
                            connection.Execute(queryOL, new { qlid = ql.QuoteLineId }, transaction);

                            int podId = connection.Insert<int, PurchaseOrderDetails>(pd, transaction);
                            EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(pd), PurchaseOrdersDetailId: podId);
                        }
                    }

                    var ordersForUpdate = quoteLinesXmpo.GroupBy(g => g.OrderId)
                                                .Select(o => o.First())
                                                .Select(x => new { OrderId = x.OrderId })
                                                .ToList();
                    foreach (var item in ordersForUpdate)
                    {
                        var queryOrder = "update crm.Orders set OrderStatus = 1 where OrderID = @orderid";
                        connection.Execute(queryOrder, new { orderid = item.OrderId }, transaction);
                    }


                    // TODO : need to update the Order Status for all the affected sales orders.


                    // No more Mpo creation while bulk purchasing
                    ////var _orders = quoteLinesMpo.GroupBy(g => g.OrderId)
                    ////                            .Select(o => o.First())
                    ////                            .Select(x => new { OrderId = x.OrderId })
                    ////                            .ToList();
                    ////foreach (var oItem in _orders)
                    ////{
                    ////    var _query = _qeuryMpoPoNumber + @"SELECT op.AccountId, o.* FROM crm.orders o 
                    ////            INNER JOIN [CRM].[Opportunities] op ON op.OpportunityId = o.OpportunityId 
                    ////            WHERE o.orderId = @OrderId; 

                    ////            SELECT Opp.* FROM crm.QuoteLines ql 
                    ////            INNER JOIN crm.Orders o ON o.QuoteKey = ql.QuoteKey 
                    ////            --INNER JOIN crm.QuoteLineDetail qld ON qld.QuoteLineId = ql.QuoteLineId 
                    ////            INNER JOIN crm.Opportunities Opp ON opp.OpportunityId = o.OpportunityId 
                    ////            WHERE o.OrderID = @orderId AND Opp.FranchiseId = @franchiseId;

                    ////            SELECT stlm.id, a.Address1, a.Address2, a.City, a.State, a.ZipCode, a.AddressId   
                    ////            FROM CRM.ShipToLocationsMap stlm 
                    ////            INNER JOIN crm.ShipToLocations stl ON stlm.ShipToLocation = stl.id 
                    ////            INNER JOIN crm.Addresses a ON a.AddressId = stl.AddressId 
                    ////            WHERE ISNULL(stl.Active,0) = 1 AND ISNULL(stl.Deleted,0) = 0 
                    ////            AND stlm.FranchiseId = @franchiseId;
                    ////            ";

                    ////    int mpoNumber = 1000, poNumber = 1000;
                    ////    var dateTime = DateTime.UtcNow;

                    ////    var xResult = connection.QueryMultiple(_query
                    ////                        , new
                    ////                        {
                    ////                            franchiseId = this.Franchise.FranchiseId
                    ////                            , orderId = oItem.OrderId
                    ////                        }, transaction);
                    ////    var MpoNumber = xResult.Read<int>().FirstOrDefault();
                    ////    var PONumber = xResult.Read<int>().FirstOrDefault();
                    ////    var order = xResult.Read<Orders>().FirstOrDefault();
                    ////    var opportunity = xResult.Read<Opportunity>().FirstOrDefault();
                    ////    var shiptoLocationMaps = xResult.Read<ShipToLocationsMap>().ToList();

                    ////    if (MpoNumber > 0) mpoNumber = MpoNumber + 1;
                    ////    if (PONumber > 0) poNumber = PONumber + 1;

                    ////    var qlines = quoteLinesMpo.Where(o => o.OrderId == oItem.OrderId)
                    ////                              .ToList();

                    ////    var mpo = new MasterPurchaseOrder();
                    ////    mpo.ShipToLocation = null;

                    ////    if (opportunity.TerritoryId != null)
                    ////    {
                    ////        if (shiptoLocationMaps != null && shiptoLocationMaps.Count > 0)
                    ////        {
                    ////            var stl = shiptoLocationMaps.Where(x => x.TerritoryId == opportunity.TerritoryId).FirstOrDefault();
                    ////            if (stl != null)
                    ////            {
                    ////                mpo.ShipToLocation = stl.Id;
                    ////                mpo.Address = stl.Addressvalue;
                    ////                mpo.ShipAddressId = Convert.ToInt32(stl.AddressId);
                    ////            }
                    ////        }
                    ////    }
                    ////    mpo.MasterPONumber = mpoNumber.ToString();
                    ////    //mpo.AccountId
                    ////    //mpo.OpportunityId
                    ////    //mpo.QuoteId
                    ////    //mpo.OrderId
                    ////    mpo.CreateDate = dateTime;
                    ////    mpo.SubmittedTOPIC = false;
                    ////    mpo.POStatusId = 1;
                    ////    mpo.TerritoryId = opportunity.TerritoryId;
                    ////    mpo.DropShip = false;
                    ////    mpo.ConfirmAddress = false;
                    ////    mpo.CreatedBy = user;
                    ////    mpo.CreatedOn = dateTime;
                    ////    mpo.UpdatedBy = user;
                    ////    mpo.UpdateOn = dateTime;
                    ////    mpo.ImpersonatorPersonId = this.ImpersonatorPersonId;
                    ////    int mpoid = connection.Insert<int, MasterPurchaseOrder>(mpo, transaction);

                    ////    var mpox = new MasterPurchaseOrderExtn();
                    ////    mpox.PurchaseOrderId = mpoid;
                    ////    mpox.AccountId = order.AccountId;
                    ////    mpox.OpportunityId = order.OpportunityId;
                    ////    mpox.QuoteId = order.QuoteKey;
                    ////    mpox.OrderId = order.OrderID;
                    ////    mpox.CreatedBy = user;
                    ////    mpox.CreatedOn = dateTime;
                    ////    mpox.UpdatedBy = user;
                    ////    mpox.UpdateOn = dateTime;
                    ////    connection.Insert<int, MasterPurchaseOrderExtn>(mpox, transaction);

                    ////    var pods = new List<PurchaseOrderDetails>();
                    ////    foreach (var ql in quoteLinesMpo)
                    ////    {
                    ////        var existingPOD = pods.Where(x => x.VendorId == ql.VendorId).FirstOrDefault();
                    ////        if (ql.ProductTypeId == 1
                    ////            || ql.ProductTypeId == 2
                    ////            || ql.ProductTypeId == 5)
                    ////        {
                    ////            var pd = new PurchaseOrderDetails();
                    ////            pd.Status = "Open";
                    ////            pd.StatusId = 1;
                    ////            if (ql.ProductTypeId == 1)
                    ////            {
                    ////                pd.PartNumber = ql.ProductId.ToString();
                    ////            }
                    ////            else
                    ////            {
                    ////                if (ql.ProductTypeId == 2)
                    ////                {
                    ////                    var fProductQuery = @"select * from crm.franchiseproducts where productKey = @productId";
                    ////                    var fProduct = ExecuteIEnumerableObject<FranchiseProducts>(
                    ////                        ContextFactory.CrmConnectionString, fProductQuery, new
                    ////                        {
                    ////                            productId = Convert.ToInt32(ql.ProductId)
                    ////                        }).FirstOrDefault();
                    ////                    pd.PartNumber = fProduct != null ? Convert.ToString(fProduct.VendorProductSKU) : "";
                    ////                }

                    ////                if (existingPOD != null)
                    ////                {
                    ////                    pd.PONumber = existingPOD.PONumber;
                    ////                }
                    ////                else
                    ////                {
                    ////                    poNumber = poNumber + 1;
                    ////                    pd.PONumber = poNumber;
                    ////                }
                    ////            }

                    ////            pd.PurchaseOrderId = mpoid;
                    ////            pd.QuoteLineId = ql.QuoteLineId;
                    ////            pd.VendorId = ql.VendorId.HasValue ? ql.VendorId.Value : 0;
                    ////            pd.QuantityPurchased = ql.Quantity.Value;
                    ////            pd.NotesVendor = ql.VendorNotes;
                    ////            pd.CreatedBy = user;
                    ////            pd.CreatedOn = dateTime;
                    ////            pd.UpdatedBy = user;
                    ////            pd.UpdatedOn = dateTime;
                    ////            pd.ImpersonatorPersonId = this.ImpersonatorPersonId;
                    ////            pods.Add(pd);

                    ////            var podId = connection.Insert<int, PurchaseOrderDetails>(pd, transaction);
                    ////            EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(pd), PurchaseOrdersDetailId: podId);
                    ////        }
                    ////    }

                    ////    order.OrderStatus = 1;
                    ////    order.LastUpdatedBy = user;
                    ////    order.LastUpdatedOn = dateTime;
                    ////    order.ImpersonatorPersonId = this.ImpersonatorPersonId;
                    ////    connection.Update(order, transaction);
                    ////    EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(order), OrderId: order.OrderID);
                    ////}

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    EventLogger.LogEvent(ex);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }


        /// <summary>
        /// Check if MPO exist for a salesOrder
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public bool CheckIfMPOExists(int orderId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var dup_query = "exec [CRM].[DuplicateMPOCheck]  @orderId";

                    connection.Open();

                    //duplicate check query

                    var dupExists = ExecuteIEnumerableObject<bool>(
                               ContextFactory.CrmConnectionString, dup_query, new { OrderId = orderId }).FirstOrDefault();
                    return dupExists;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public int ConvertOrderToMasterPurchaseOrder(int orderId)
        {
            var connection = new SqlConnection(ContextFactory.CrmConnectionString);
            try
            {
                int mpoNumber = 1000, poNumber = 1000;
                var user = SessionManager.CurrentUser.PersonId;
                var dateTime = DateTime.UtcNow;
                connection.Open();
                var query = @"SELECT a.AccountId, o.* FROM crm.orders AS o 
                                INNER JOIN [CRM].[Opportunities] AS a ON a.OpportunityId = o.OpportunityId 
                                WHERE o.orderId = @OrderId;

	                            select ql.* from crm.QuoteLines ql
	                            left join crm.OrderLines ol on ol.QuoteLineId = ql.QuoteLineId
	                            where ol.OrderID = @OrderId and (ol.OrderLineStatus = 7 or ol.OrderLineStatus = 8);

/*
                              select * from crm.QuoteLines ql
	                            left join crm.Orders o on o.QuoteKey = ql.QuoteKey
	                            where o.OrderID = @OrderId 
	                            and not exists(
		                         select 1 From crm.PurchaseOrderDetails pod 
		                         left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = pod.PurchaseOrderId
		                         where mpox.OrderId = @OrderId
		                         and ql.QuoteLineId = pod.QuoteLineId
	                            )
*/

                              --SELECT qld.* FROM crm.QuoteLines ql 
                              --  INNER JOIN crm.Orders o ON o.QuoteKey = ql.QuoteKey 
                              --  INNER JOIN crm.QuoteLineDetail qld ON qld.QuoteLineId = ql.QuoteLineId 
                              --  INNER JOIN crm.Opportunities o2 ON o2.OpportunityId = o.OpportunityId 
                              --  WHERE o.OrderID = @OrderId AND o2.FranchiseId = @FranchiseId;
                              
                                SELECT max(cast(mpo.MasterPONumber as int))  AS MasterPONumber FROM crm.MasterPurchaseOrder mpo 
                                inner join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                INNER JOIN crm.Opportunities co ON co.OpportunityId = mpox.OpportunityId 
                                WHERE co.FranchiseId = @FranchiseId
								GROUP BY co.FranchiseId;
                              
                               SELECT max(pod.PONumber) AS PONumber FROM crm.PurchaseOrderDetails pod 
                                INNER JOIN  crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = pod.PurchaseOrderId 
                                INNER join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                INNER JOIN crm.Opportunities co ON co.OpportunityId = mpox.OpportunityId 
                                WHERE co.FranchiseId = @FranchiseId
								GROUP BY co.FranchiseId;

                              SELECT o2.* FROM crm.QuoteLines ql 
                                INNER JOIN crm.Orders o ON o.QuoteKey = ql.QuoteKey 
                                INNER JOIN crm.QuoteLineDetail qld ON qld.QuoteLineId = ql.QuoteLineId 
                                INNER JOIN crm.Opportunities o2 ON o2.OpportunityId = o.OpportunityId 
                                WHERE o.OrderID = @OrderId AND o2.FranchiseId = @FranchiseId;

                              SELECT stlm.*, a.*   FROM CRM.ShipToLocationsMap stlm 
                                INNER JOIN crm.ShipToLocations stl ON stlm.ShipToLocation = stl.id 
                                INNER JOIN crm.Addresses a ON a.AddressId = stl.AddressId 
                                WHERE ISNULL(stl.Active,0) = 1 AND ISNULL(stl.Deleted,0) = 0 
                                    AND stlm.FranchiseId = @FranchiseId;
                              
                                SELECT stl.Id, stl.ShipToLocation, t.Name + ' - '+stl.ShipToName AS ShipToName , stl.AddressId, stl.Active, stl.Deleted, stl.CreatedOn, stl.CreatedBy, stl.LastUpdatedOn, stl.LastUpdatedBy, stl.FranchiseId,
                                             stlm.TerritoryId, STUFF(
                                                                        COALESCE(', ' + NULLIF(Address1, ''), '')  +
                                    		                            COALESCE(', ' + NULLIF(Address2, ''), '')  +
                                                                        COALESCE(', ' + NULLIF(City, ''), '') +
                                    		                            COALESCE(', ' + NULLIF([State], ''), '') +
                                                                        COALESCE(', ' + NULLIF(ZipCode, ''), '') +
                                                                        COALESCE(', ' + NULLIF(CountryCode2Digits , ''), ''),
                                                                        1, 1, '') AS Address FROM crm.ShipToLocations stl
                                    INNER JOIN crm.ShipToLocationsMap stlm ON stl.Id = stlm.ShipToLocation
                                    INNER JOIN crm.Addresses a ON a.AddressId = stl.AddressId
									INNER JOIN crm.Territories t ON t.TerritoryId = stlm.TerritoryId
                                    WHERE ISNULL(stl.Active,0)=1 AND ISNULL(stl.Deleted,0)=0  AND   stlm.FranchiseId = @FranchiseId;
                              
                                    SELECT * FROM crm.VendorTerritoryAccount vta WHERE vta.FranchiseId = @FranchiseId";

                //var mPOQuery = @" INSERT INTO CRM.MasterPurchaseOrder (MasterPONumber ,AccountId ,OpportunityId ,CreateDate ,SubmittedDate ,OrderId ,QuoteId ,Status ,CreatedBy ,CreatedOn ,UpdatedBy ,UpdateOn) VALUES (@MasterPONumber ,@AccountId ,@OpportunityId ,@CreateDate ,@SubmittedDate ,@OrderId ,@QuoteId ,@Status ,@CreatedBy ,@CreatedOn ,@UpdatedBy ,@UpdateOn); SELECT CAST(SCOPE_IDENTITY() as int)";

                //var mPOExQuery = @" INSERT INTO CRM.MasterPurchaseOrderExtn (PurchaseOrderId,AccountId ,OpportunityId, QuoteId, OrderId, CreatedBy, CreatedOn ,UpdatedBy ,UpdateOn) VALUES (@purhcaseOrderId, @AccountId, @OpportunityId, @QuoteId, @OrderId, @CreatedBy, @CreatedOn, @UpdatedBy ,@UpdateOn);";

                // not required
                ////var poDetails = @"INSERT INTO CRM.PurchaseOrderDetails (PurchaseOrderId ,QuoteLineId ,PartNumber ,ShipDate ,Status ,PromiseByDate ,QuantityPurchased ,PickedBy ,PONumber ,CreateOn ,CreatedBy ,UpdatedOn ,UpdatedBy) VALUES (@PurchaseOrderId ,@QuoteLineId ,@PartNumber ,@ShipDate ,@Status ,@PromiseByDate ,@QuantityPurchased ,@PickedBy ,@PONumber ,@CreateOn ,@CreatedBy ,@UpdatedOn ,@UpdatedBy); SELECT CAST(SCOPE_IDENTITY() as int)";
                var fProductQuery = @"select * from crm.franchiseproducts where productKey = @productId";

                var fDetails = connection.QueryMultiple(query, new { OrderId = orderId, franchiseId = SessionManager.CurrentFranchise.FranchiseId });


                var order = fDetails.Read<Orders>().First();
                var quotelines = fDetails.Read<QuoteLines>().ToList();
                //not required
                ////var quoteLineDetails = fDetails.Read<QuoteLineDetail>().ToList();
                var MPO = fDetails.Read<MasterPurchaseOrder>().FirstOrDefault();
                var pod = fDetails.Read<PurchaseOrderDetails>().FirstOrDefault();
                var Oppor = fDetails.Read<Opportunity>().FirstOrDefault();
                var stlmList = fDetails.Read<ShipToLocationsMap>().ToList();

                // not required
                ////var shiptolocationsList = fDetails.Read<ShipToLocations>().ToList();
                var vtaList = fDetails.Read<VendorTerritoryAccount>().ToList();

                var mpoObj = new MasterPurchaseOrder();
                mpoObj.ShipToLocation = null;
                if (pod != null)
                {
                    poNumber = Convert.ToInt32(pod.PONumber);
                }

                if (MPO != null)
                {
                    mpoNumber = Convert.ToInt32(MPO.MasterPONumber) + 1;
                }

                if (Oppor.TerritoryId != null)
                {
                    if (stlmList != null && stlmList.Count > 0)
                    {
                        if (!mpoObj.ShipToLocation.HasValue && !mpoObj.DropShip)
                        {
                            var stl = stlmList.Where(x => x.TerritoryId == Oppor.TerritoryId).FirstOrDefault();
                            if (stl != null)
                            {
                                mpoObj.ShipToLocation = stl.Id;
                                mpoObj.Address = stl.Addressvalue;
                                mpoObj.ShipAddressId = Convert.ToInt32(stl.AddressId);
                            }
                        }
                    }
                }

                mpoObj.MasterPONumber = mpoNumber.ToString();
                mpoObj.AccountId = order.AccountId;
                mpoObj.OpportunityId = Convert.ToInt32(order.OpportunityId);
                mpoObj.CreateDate = dateTime;
                mpoObj.OrderId = Convert.ToInt32(order.OrderID);
                mpoObj.QuoteId = Convert.ToInt32(order.QuoteKey);
                mpoObj.SubmittedTOPIC = false;
                mpoObj.POStatusId = 1;
                mpoObj.TerritoryId = Oppor.TerritoryId;

                mpoObj.DropShip = false;
                mpoObj.ConfirmAddress = false;

                mpoObj.CreatedBy = user;
                mpoObj.CreatedOn = dateTime;
                mpoObj.UpdatedBy = user;
                mpoObj.UpdateOn = dateTime;

                //if (vtaList != null && vtaList.Count() > 0 && Oppor != null && Oppor.TerritoryId != null)
                //{
                //}
                //if (stlmList != null && stlmList.Count > 0 && Oppor != null && Oppor.TerritoryId != null)
                //{
                //    var stlm = stlmList.Where(x => x.TerritoryId == Oppor.TerritoryId).ToList();

                //    mpoObj.DropShip = false;
                //    if (stlm != null && stlm.Count>0 && stlm.Count==1)
                //    {
                //        mpoObj.ShipToLocation = shiptolocation.Where(x => x.ShipToLocation == stlm[0].ShipToLocation)
                //            .FirstOrDefault().ShipToLocation;
                //    }

                //    mpoObj.ConfirmAddress = false;
                //}
                //else
                //{
                //    //mpoObj.TerritoryId = null;
                //    mpoObj.DropShip = false;
                //    mpoObj.ShipToLocation = null;
                //    mpoObj.ConfirmAddress = false;
                //}

                mpoObj.ImpersonatorPersonId = this.ImpersonatorPersonId;

                int mPOId = connection.Insert<int, MasterPurchaseOrder>(mpoObj);

                // TODO: need to insert into crm.MasterPurchaseOrderExtn
                var mpox = new MasterPurchaseOrderExtn();
                mpox.PurchaseOrderId = mPOId;
                mpox.AccountId = order.AccountId;
                mpox.OpportunityId = order.OpportunityId;
                mpox.QuoteId = order.QuoteKey;
                mpox.OrderId = order.OrderID;
                mpox.CreatedBy = user;
                mpox.CreatedOn = dateTime;
                mpox.UpdatedBy = user;
                mpox.UpdateOn = dateTime;
                connection.Insert<int, MasterPurchaseOrderExtn>(mpox);

                //EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(mpoObj), PurchaseOrderId: mPOId);

                var pdlist = new List<PurchaseOrderDetails>();

                foreach (var ql in quotelines)
                {
                    // not required
                    ////var details = quoteLineDetails.Where(x => x.QuoteLineId == ql.QuoteLineId).FirstOrDefault();
                    var pdetais = pdlist.Where(x => x.VendorId == ql.VendorId).FirstOrDefault();
                    if (ql.ProductTypeId == 1 || ql.ProductTypeId == 2 || ql.ProductTypeId == 5)
                    {
                        var fProduct = ExecuteIEnumerableObject<FranchiseProducts>(
                            ContextFactory.CrmConnectionString, fProductQuery, new
                            {
                                productId = Convert.ToInt32(ql.ProductId)
                            }).FirstOrDefault();

                        var pd = new PurchaseOrderDetails();
                        if (ql.ProductTypeId == 1)
                        {
                            pd.PartNumber = ql.ProductId.ToString();
                            pd.Status = "Open";
                            pd.StatusId = 1;
                        }
                        else if (ql.ProductTypeId == 2)
                        {
                            pd.PartNumber = fProduct != null ? Convert.ToString(fProduct.VendorProductSKU) : "";
                            pd.Status = "Open";
                            pd.StatusId = 1;
                            if (pdetais != null)
                            {
                                pd.PONumber = pdetais.PONumber;
                            }
                            else
                            {
                                poNumber = poNumber + 1;
                                pd.PONumber = poNumber;
                            }
                        }
                        else
                        {
                            pd.Status = "Open";
                            pd.StatusId = 1;
                            if (pdetais != null)
                            {
                                pd.PONumber = pdetais.PONumber;
                            }
                            else
                            {
                                poNumber = poNumber + 1;
                                pd.PONumber = poNumber;
                            }
                        }
                        pd.PurchaseOrderId = mPOId;
                        pd.QuoteLineId = Convert.ToInt32(ql.QuoteLineId);
                        pd.VendorId = ql.VendorId.HasValue ? ql.VendorId.Value : 0;
                        pd.QuantityPurchased = Convert.ToInt32(ql.Quantity);
                        pd.NotesVendor = ql.VendorNotes;
                        pd.CreatedBy = user;
                        pd.CreatedOn = dateTime;
                        pd.UpdatedBy = user;
                        pd.UpdatedOn = dateTime;
                        //pd.Status = ReturnPOStatus(0);

                        pd.ImpersonatorPersonId = this.ImpersonatorPersonId;
                        pdlist.Add(pd);

                        int poDetailsId = connection.Insert<int, PurchaseOrderDetails>(pd);

                        // Also we need to update the Crm.Orderlines to mpo so that it
                        // represent the same status for each quote lines.
                        var queryOL = @"Update Crm.OrderLines set OrderLineStatus = 1
                                where QuoteLineId = @qlid";
                        connection.Execute(queryOL, new { qlid = ql.QuoteLineId });

                        EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(pd), PurchaseOrdersDetailId: poDetailsId);
                    }
                }
                //Set the order status to Procurement Pending once order is converted to MPO
                //order.OrderStatus = 1;
                order.OrderStatus = 1;
                order.LastUpdatedBy = user;
                order.LastUpdatedOn = dateTime;
                order.ImpersonatorPersonId = this.ImpersonatorPersonId;
                connection.Update(order);

                EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(order), OrderId: order.OrderID);
                return mPOId;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        #endregion Purchase Order Management for Touchpoint

        public object Get(
            out int totalRecords,
            int? statusFilter = null,
            DateTimeOffset? createdOnStart = null,
            DateTimeOffset? createdOnEnd = null,
            int pageNum = 1,
            int pageSize = 25,
            string orderBy = null,
            int? leadId = null,
            OrderByEnum orderDirection = OrderByEnum.Desc)
        {
            totalRecords = 0;

            //if (!BasePermission.CanRead) throw new UnauthorizedAccessException(Unauthorized);

            if (pageNum <= 0) throw new ArgumentOutOfRangeException("Page number must be greater than 0");
            if (pageSize <= 0) throw new ArgumentOutOfRangeException("Page size must be greater than 0");

            using (var db = ContextFactory.Create())
            {
                var linq =
                    db.Orders.AsNoTracking()
                        .Include(i => i.OrderItems)
                        .Include(i => i.JobQuote.Job.Lead)
                        .Include(i => i.OrderItems)
                        .Where(
                            w => w.IsDeleted == false && w.JobQuote.Job.Lead.FranchiseId == this.Franchise.FranchiseId);

                if (leadId.HasValue)
                {
                    linq = linq.Where(o => o.JobQuote.Job.LeadId == leadId);
                }

                if (statusFilter.HasValue)
                {
                    linq = linq.Where(w => w.OrderStatusId == statusFilter);
                }

                if (createdOnStart.HasValue)
                    linq = linq.Where(w => w.CreatedOnUtc >= createdOnStart.Value);
                if (createdOnEnd.HasValue) linq = linq.Where(w => w.CreatedOnUtc <= createdOnEnd.Value);

                Func<int?, string> lookupOrderStatus = s =>
                {
                    var orderStatus = CacheManager.OrderStatusTypeCollection.FirstOrDefault(e => e.Id == s);
                    if (orderStatus != null)
                    {
                        return orderStatus.Name;
                    }

                    return null;
                };

                totalRecords = linq.Count();
                if (totalRecords > 0)
                {
                    if (!string.IsNullOrEmpty(orderBy)) orderBy = orderBy.ToLower();

                    switch (orderBy)
                    {
                        case "jobnumber":
                            linq = orderDirection == OrderByEnum.Asc
                                ? linq.OrderBy(i => i.JobQuote.Job.JobNumber)
                                : linq.OrderByDescending(i => i.JobQuote.Job.JobNumber);
                            break;

                        case "sidemark":
                            linq = orderDirection == OrderByEnum.Asc
                                ? linq.OrderBy(i => i.JobQuote.Job.SideMark)
                                : linq.OrderByDescending(i => i.JobQuote.Job.SideMark);
                            break;

                        case "ordernumber":
                            linq = orderDirection == OrderByEnum.Asc
                                ? linq.OrderBy(i => i.OrderId)
                                : linq.OrderByDescending(i => i.OrderId);
                            break;

                        case "lastname":
                            linq = orderDirection == OrderByEnum.Asc
                                ? linq.OrderBy(i => i.JobQuote.Job.Lead.PrimCustomer.FirstName)
                                : linq.OrderByDescending(i => i.JobQuote.Job.Lead.PrimCustomer.FirstName);
                            break;

                        case "total":
                            linq = orderDirection == OrderByEnum.Asc
                                ? linq.OrderBy(i => i.JobQuote.NetTotal)
                                : linq.OrderByDescending(i => i.JobQuote.NetTotal);
                            break;

                        case "orderstatus":
                            linq = orderDirection == OrderByEnum.Asc
                                ? linq.OrderBy(i => i.OrderStatusId)
                                : linq.OrderByDescending(i => i.OrderStatusId);
                            break;

                        case "manufacturer":
                            linq = orderDirection == OrderByEnum.Asc
                                ? linq.OrderBy(i => i.OrderItems.FirstOrDefault().Manufacturer)
                                : linq.OrderByDescending(i => i.OrderItems.FirstOrDefault().Manufacturer);
                            break;

                        default:
                            linq = orderDirection == OrderByEnum.Asc
                                ? linq.OrderBy(i => i.CreatedOnUtc)
                                : linq.OrderByDescending(i => i.CreatedOnUtc);
                            break;
                    }

                    List<int> ids;
                    if (orderBy == null || string.Equals(orderBy, "ordernumber") ||
                        string.Equals(orderBy, "ordernumber") || string.Equals(orderBy, "createddate"))
                        ids = linq.Skip((pageNum - 1) * pageSize).Take(pageSize).Select(s => s.OrderId).ToList();
                    else
                        ids = linq.Select(s => s.OrderId).ToList();
                    var result = Get(ids)
                        .ToList()
                        .Select(
                            o =>
                                new
                                {
                                    OrderId = o.OrderId,
                                    OrderStatusId = o.OrderStatusId,
                                    OrderStatus = lookupOrderStatus(o.OrderStatusId),
                                    JobNumber = o.Job.JobNumber,
                                    OrderNumber = o.OrderNumber,
                                    LeadId = o.Job.LeadId,
                                    LeadLastName = o.Job.Lead.PrimCustomer.LastName,
                                    CreatedDate = o.CreatedOnUtc,
                                    ZipCode = o.Job.InstallAddress != null ? o.Job.InstallAddress.ZipCode : null,
                                    OrderTotal = o.OrderTotal,
                                    Manufacturer = o.OrderItems.First().Manufacturer,
                                    SideMark = o.Job.SideMark,
                                    IsOrdered = o.OrderStatusId != 6 ? true : false,
                                    HasItemsSavedWithErrors = o.HasItemsSavedWithErrors
                                }).ToList();

                    switch (orderBy)
                    {
                        case "jobnumber":
                            result = orderDirection == OrderByEnum.Asc
                                ? result.OrderBy(x => x.JobNumber).ToList()
                                : result.OrderByDescending(x => x.JobNumber).ToList();
                            break;

                        case "sidemark":
                            result = orderDirection == OrderByEnum.Asc
                                ? result.OrderBy(x => x.SideMark).ToList()
                                : result.OrderByDescending(x => x.SideMark).ToList();
                            break;

                        case "ordernumber":
                            result = orderDirection == OrderByEnum.Asc
                                ? result.OrderBy(x => x.OrderNumber).ToList()
                                : result.OrderByDescending(x => x.OrderNumber).ToList();
                            break;

                        case "lastname":
                            result = orderDirection == OrderByEnum.Asc
                                ? result.OrderBy(x => x.LeadLastName).ToList()
                                : result.OrderByDescending(x => x.LeadLastName).ToList();
                            break;

                        case "total":
                            result = orderDirection == OrderByEnum.Asc
                                ? result.OrderBy(x => x.OrderTotal).ToList()
                                : result.OrderByDescending(x => x.OrderTotal).ToList();
                            break;

                        case "zipcode":
                            result = orderDirection == OrderByEnum.Asc
                                ? result.OrderBy(x => x.ZipCode).ToList()
                                : result.OrderByDescending(x => x.ZipCode).ToList();
                            break;

                        case "orderstatus":
                            result = orderDirection == OrderByEnum.Asc
                                ? result.OrderBy(i => i.OrderStatusId).ToList()
                                : result.OrderByDescending(i => i.OrderStatusId).ToList();
                            break;

                        case "manufacturer":
                            result = orderDirection == OrderByEnum.Asc
                                ? result.OrderBy(i => i.Manufacturer).ToList()
                                : result.OrderByDescending(i => i.Manufacturer).ToList();
                            break;

                        default:
                            result = orderDirection == OrderByEnum.Asc
                                ? result.OrderBy(x => x.CreatedDate).ToList()
                                : result.OrderByDescending(x => x.CreatedDate).ToList();
                            break;
                    }

                    if (orderBy == null || string.Equals(orderBy, "ordernumber") ||
                        string.Equals(orderBy, "ordernumber") || string.Equals(orderBy, "createddate"))
                        return result;
                    else
                        return result.Skip((pageNum - 1) * pageSize).Take(pageSize).ToList();
                }
            }

            return new List<object>();
        }

        public override ICollection<Order> Get(List<int> idList)
        {
            //if (!BasePermission.CanRead)
            //    throw new UnauthorizedAccessException(Unauthorized);

            if (idList == null)
                throw new ArgumentNullException("idList parameter can not be null");
            if (idList.Count == 0)
                return new List<Order>();

            var orders = CRMDBContext.Orders.AsNoTracking()
                        .Include(i => i.OrderItems)
                        .Include(i => i.Job)
                        .Include(i => i.Job.InstallAddress)
                        .Include(i => i.Job.Lead)
                        .Include(i => i.Job.Lead.PrimCustomer)
                        .Where(w => idList.Contains(w.OrderId)).ToList();
            return orders;
        }

        public ICollection<int> GetSentJobItemIds(List<int> jobs)
        {
            return CRMDBContext.Orders.AsNoTracking()
                        .Include(i => i.OrderItems)
                        .Where(w => w.IsDeleted != true && jobs.Contains(w.JobId) && w.OrderStatusId == 2).SelectMany(w => w.OrderItems).Select(oi => oi.JobItemId).ToList();
        }

        public ICollection<int> GetOrderedJobItemIds(List<int> jobs)
        {
            return CRMDBContext.Orders.AsNoTracking()
                        .Include(i => i.OrderItems)
                        .Where(w => w.IsDeleted != true && jobs.Contains(w.JobId)).SelectMany(w => w.OrderItems).Select(oi => oi.JobItemId).ToList();
        }

        public IEnumerable<OrderItem> GetOrderItems(int id)
        {
            //if (!BasePermission.CanRead)
            //    throw new UnauthorizedAccessException(Unauthorized);

            if (id <= 0)
                return null;

            var orders = CRMDBContext.OrderItems.AsNoTracking().Where(w => w.OrderId == id).ToList();

            return orders;
        }

        public List<PurchaseOrderDetails> GetPurchaseOrderDetailsLines(int purchaseOrderId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var query = @"SELECT * FROM crm.PurchaseOrderDetails pod WHERE pod.PurchaseOrderId=@PurchaseOrderId;";
                    var fDetails = connection.QueryMultiple(query, new { PurchaseOrderId = purchaseOrderId });
                    var POList = fDetails.Read<PurchaseOrderDetails>().ToList();
                    return POList;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    return null;
                }
            }
        }
        public bool UpdatePurchaseOrderDetails(PurchaseOrderDetails poDetails)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    poDetails.UpdatedBy = SessionManager.CurrentUser.PersonId;
                    poDetails.UpdatedOn = DateTime.UtcNow;
                    connection.Update(poDetails);
                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    return false;
                }
            }
        }
        public override Order Get(int id)
        {
            //if (!BasePermission.CanRead)
            //    throw new UnauthorizedAccessException(Unauthorized);

            if (id <= 0)
                return null;

            var order = CRMDBContext.Orders.AsNoTracking()
                            .Include(i => i.JobQuote)
                            .Include(i => i.JobQuote.JobItems)
                            .Include(i => i.JobQuote.Job.Lead.PrimCustomer)
                            .Include(i => i.JobQuote.Job.Customer)
                            .Include(i => i.JobQuote.JobItems.Select(s => s.Options))
                            .Include(i => i.JobQuote.SaleAdjustments)
                            .SingleOrDefault(w => w.OrderId == id);

            return order;
        }

        public string Cancel(int id)
        {
            if (id <= 0)
                return "Invalid order id";

            //Base permission of can delete (like admin and managers) or assigned permission AND assigned person matches current user
            //if (BasePermission.CanDelete)
            //{
            using (var db = ContextFactory.Create())
            {
                var order = db.Orders.Include(i => i.Job).FirstOrDefault(o => o.OrderId == id);

                if (order != null)
                {
                    order.IsDeleted = true;
                    db.SaveChanges();

                    order.Job.EditHistories.Add(new EditHistory
                    {
                        LoggedByPersonId = User.PersonId,
                        IPAddress = HttpContext.Current.Request.UserHostAddress,
                        HistoryValue =
                            string.Format(
                                "<Quote operation=\"information\">Order #{0}-{1} has been cancelled</Quote>",
                                order.OrderId, order.OrderNumber),
                        CreatedOnUtc = DateTime.Now
                    });

                    return Success;
                }

                return "Order # not valid.";
            }
            //}
            //return Unauthorized;
        }

        public Order GetOrderReview(int orderId)
        {
            //if (!BasePermission.CanRead)
            //    throw new UnauthorizedAccessException(Unauthorized);

            if (orderId <= 0)
                return null;

            var order = CRMDBContext.Orders.AsNoTracking()
                                    .Include(i => i.OrderItems)
                                    .Include(i => i.OrderItems.Select(s => s.JobItem.Options))
                                    .Include(i => i.Vendor)
                                    .Include(i => i.Job)
                                    .Include(i => i.Job.Customer)
                                    .SingleOrDefault(w => w.OrderId == orderId);
            return order;
        }

        /// <summary>
        /// Add order and order items
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns>status</returns>
        public string AddOrder(int quoteId)
        {
            if (quoteId <= 0)
                return "Quote Id is required";

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var quote = db.JobQuotes.AsNoTracking()
                        .Where(w => w.QuoteId == quoteId)
                        .Include(i => i.Job)
                        .Include(i => i.JobItems)
                        .SingleOrDefault();

                    if (quote != null)
                    {
                        var jobItems = quote.JobItems.Where(w => w.isCustom != null && !w.isCustom.Value).ToList();

                        var quoteGroup = jobItems.GroupBy(g => g.ManufacturerId);

                        foreach (var grpItems in quoteGroup)
                        {
                            var mfg =
                                CacheManager.ManufacturerCollection.FirstOrDefault(w => w.ManufacturerId == grpItems.Key);
                            var vendor =
                                CacheManager.VendorCollection.FirstOrDefault(
                                    w => mfg != null && w.VendorGuid == mfg.ManufacturerGuid);

                            var order =
                                db.Orders.AsNoTracking()
                                    .SingleOrDefault(
                                        w =>
                                            w.QuoteId == quoteId && w.IsDeleted != true && w.VendorId == vendor.VendorId);
                            if (order != null)
                            {
                                continue;
                            }

                            var orderNum = db.spOrderNumber_New(Franchise.FranchiseId).FirstOrDefault();

                            if (vendor != null)
                            {
                                var newOrder = new Order
                                {
                                    QuoteId = quote.QuoteId,
                                    JobId = quote.JobId,
                                    OrderStatusId = 6,
                                    OrderNumber = orderNum,
                                    OrderTotal = quote.NetTotal,
                                    CreatedByPersonId = User.PersonId,
                                    IsDeleted = false,
                                    CreatedOnUtc = DateTime.Now,
                                    OrderGuid = Guid.NewGuid(),
                                    VendorId = vendor.VendorId,
                                    OrderItems = new List<OrderItem>(),
                                    HasItemsSavedWithErrors = jobItems.Any(ji => ji.IsSavedWithErrors == true)
                                };

                                newOrder.OrderItems = (from item in grpItems
                                                       select new OrderItem
                                                       {
                                                           Quantity = item.Quantity,
                                                           ManufacturerId = item.ManufacturerId,
                                                           Manufacturer = item.Manufacturer,
                                                           ProductTypeId = item.ProductTypeId,
                                                           ProductType = item.ProductType,
                                                           ProductGuid = item.ProductGuid,
                                                           ProductName = item.ProductName,
                                                           CreatedByPersonId = User.PersonId,
                                                           Cost = item.UnitCost,
                                                           Order = newOrder,
                                                           JobItemId = item.JobItemId,
                                                           Style = item.Style,
                                                           StyleId = item.StyleId
                                                       }).ToList();

                                var sum = newOrder.OrderItems.Sum(s => s.Quantity * s.Cost);
                                if (sum != null) newOrder.OrderTotal = sum.Value;

                                db.Orders.Add(newOrder);

                                db.SaveChanges();

                                var job = db.Jobs.Find(quote.JobId);
                                job.EditHistories.Add(new EditHistory
                                {
                                    LoggedByPersonId = User.PersonId,
                                    IPAddress = HttpContext.Current.Request.UserHostAddress,
                                    HistoryValue =
                                        string.Format(
                                            "<Quote operation=\"information\">Order #{0}-{1} has been added</Quote>",
                                            newOrder.OrderId, newOrder.OrderNumber),
                                    CreatedOnUtc = DateTime.UtcNow
                                });
                            }
                        }
                    }

                    db.SaveChanges();
                    return Success;
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return ex.Message;
            }
        }

        /// <summary>
        /// Add order item to Order
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="orderItem"></param>
        /// <returns></returns>
        public string AddOrderItem(int orderId, OrderItem orderItem)
        {
            return string.Empty;
        }

        /// <summary>
        /// Update order item to order
        /// </summary>
        /// <returns>status string</returns>
        public string UpdateOrderItem(int orderId, OrderItem orderItem)
        {
            if (orderId <= 0 && orderItem != null)
                throw new ArgumentOutOfRangeException(QuoteDoesNotExist);

            //if (!BasePermission.CanRead)
            //    throw new UnauthorizedAccessException(Unauthorized);

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var originalOrderItem =
                        db.OrderItems.SingleOrDefault(
                            w => w.OrderId == orderId && w.OrderItemId == orderItem.OrderItemId);

                    if (originalOrderItem != null)
                    {
                        db.Entry(originalOrderItem).CurrentValues.SetValues(orderItem);
                        db.SaveChanges();

                        return Success;
                    }
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }

            return FailedSaveChanges;
        }

        /// <summary>
        /// update order status
        /// </summary>
        /// <param name="id">order id</param>
        /// <param name="orderStatus"></param>
        /// <returns>status string</returns>
        public string UpdateOrderStatus(int orderId, Type_OrderStatus orderStatus)
        {
            if (orderId <= 0)
                throw new ArgumentOutOfRangeException(QuoteDoesNotExist);

            //if (!BasePermission.CanRead)
            //    throw new UnauthorizedAccessException(Unauthorized);

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var order = db.Orders.Include(i => i.Job).SingleOrDefault(w => w.OrderId == orderId);

                    if (order != null)
                    {
                        order.OrderStatusId = (int?)orderStatus.Id;

                        db.Orders.Attach(order);

                        db.Entry(order).Property(i => i.OrderStatusId).IsModified = true;

                        order.Job.EditHistories.Add(new EditHistory
                        {
                            LoggedByPersonId = User.PersonId,
                            IPAddress = HttpContext.Current.Request.UserHostAddress,
                            HistoryValue =
                                string.Format(
                                    "<Quote operation=\"information\">Order #{0}-{1} status been updated to: {2}</Quote>",
                                    order.OrderId, order.OrderNumber,
                                    CacheManager.OrderStatusTypeCollection.First(w => w.Id == orderStatus.Id).Name),
                            CreatedOnUtc = DateTime.UtcNow
                        });

                        db.SaveChanges();

                        return Success;
                    }
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }

            return FailedSaveChanges;
        }

        public List<Order> GetUnsentPurchaseOrders()
        {
            List<Order> retVal = new List<Order>();
            return CRMDBContext.Orders.Where(w =>
                w.Job.Lead.Franchise.isSolaTechEnabled == true &&
                w.OrderStatusId == CacheManager.OrderStatusTypeCollection.FirstOrDefault(s => s.Name == "Pending").Id &&
                (DateTime.Now - w.CreatedOnUtc).TotalHours > 72
                ).ToList();
        }

        public List<string> CheckVendorsAcoountNumLess(int orderId)
        {
            var results = new List<string>();

            //if (!BasePermission.CanRead)
            //    throw new UnauthorizedAccessException(Unauthorized);

            var order = CRMDBContext.Orders.Where(w => w.OrderId == orderId)
                            .Include(i => i.Vendor)
                            .FirstOrDefault();
            if (order != null)
            {
                var mfgs = this.CRMDBContext.Manufacturers.Where(v => v.isCustom == false && v.ManufacturerGuid == order.Vendor.VendorGuid).ToList();

                foreach (var man in mfgs)
                {
                    var manContract = CRMDBContext.ManufacturerContacts.FirstOrDefault(w => w.FranchiseId == Franchise.FranchiseId && w.ManufacturerId == man.ManufacturerId);
                    if (manContract != null)
                    {
                        if (string.IsNullOrEmpty(manContract.AccountNumber))
                        {
                            results.Add(man.Name);
                        }
                    }
                    else
                    {
                        results.Add(man.Name);
                    }
                }
            }
            return results;
        }

        public string GetWebOEPurchaseOrder(int orderId, out WebOE webOE)
        {
            string status = Success;

            //if (!BasePermission.CanRead)
            //    throw new UnauthorizedAccessException(Unauthorized);

            if (orderId <= 0)
                throw new ArgumentOutOfRangeException(OrderDoesNotExist);

            webOE = null;

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var order = db.Orders.Where(w => w.OrderId == orderId)
                        .Include(i => i.OrderItems)
                        .Include(i => i.Vendor)
                        .Include(i => i.Vendor.BillAddress)
                        .Include(i => i.Vendor.ShipAddress)
                        .Include(i => i.Job)
                        .Include(i => i.Job.BillingAddress)
                        .Include(i => i.Job.InstallAddress)
                        .Include(i => i.Job.Customer)
                        .Include(i => i.Job.SalesPerson)
                        .Include(i => i.JobQuote.JobItems)
                        .Include(i => i.OrderItems.Select(c => c.JobItem.Options))
                        .SingleOrDefault();

                    if (order != null)
                    {
                        webOE = new WebOE
                        {
                            FranchiseGuid = Franchise.FranchiseGuid.ToString(),
                            Company = new WebOECompany()
                            {
                                Name = Franchise.Name,
                                Phone1 = Franchise.LocalPhoneNumber,
                                Fax1 = Franchise.FaxNumber,
                                Email = Franchise.AdminEmail,
                                WebSite = Franchise.Website,

                                BillToAddress1 = Franchise.Address.Address1,
                                BillToCity = Franchise.Address.City,
                                BillToState = Franchise.Address.State,
                                BillToZip = Franchise.Address.ZipCode,
                                BillToCountry = Franchise.Address.CountryCode2Digits,

                                ShipToAddress1 = Franchise.Address.Address1,
                                ShipToCity = Franchise.Address.City,
                                ShipToState = Franchise.Address.State,
                                ShipToZip = Franchise.Address.ZipCode,
                                ShipToCountry = Franchise.Address.CountryCode2Digits,

                                LicenseProductId = string.Empty,
                                UniqueId = Franchise.FranchiseGuid.ToString()
                            },
                            Order = new WebOEOrder()
                            {
                                POrderNoBB = order.OrderId.ToString() + "-" + order.OrderNumber.ToString(),
                                POrderDateBB = order.CreatedOnUtc,
                                AltPOrderNo = string.Empty,
                                CustomerPOrderNo = string.Empty,
                                CustomerName = order.Job.Customer.FullName,
                                SaleRep = string.Empty,
                                SideMark = order.Job.SideMark,

                                BillToName = Franchise.Name,
                                BillToAddress1 = Franchise.Address.Address1,
                                BillToCity = Franchise.Address.City,
                                BillToState = Franchise.Address.State,
                                BillToZip = Franchise.Address.ZipCode,
                                BillToCountry = Franchise.Address.CountryCode2Digits,

                                IsDropShip = 0,

                                ShipToName = Franchise.Name,
                                ShipToAddress1 = Franchise.Address.Address1,
                                ShipToCity = Franchise.Address.City,
                                ShipToState = Franchise.Address.State,
                                ShipToZip = Franchise.Address.ZipCode,
                                ShipToCountry = Franchise.Address.CountryCode2Digits,

                                Cost = Convert.ToSingle(order.OrderTotal),
                                Origin = 0,
                                UniqueId = order.OrderGuid.ToString(),
                                OrderUniqueId = string.Empty,
                                InternalRefNumber = order.ReferenceNumber
                            }
                        };

                        //update franchise information.

                        var mfg =
                            CacheManager.ManufacturerCollection.SingleOrDefault(
                                w => w.ManufacturerGuid == order.Vendor.VendorGuid);

                        var accountNumber =
                            db.ManufacturerContacts.FirstOrDefault(
                                w => w.FranchiseId == Franchise.FranchiseId && w.ManufacturerId == mfg.ManufacturerId);

                        if (mfg != null && accountNumber != null)
                        {
                            webOE.Vendor = new WebOEVendor()
                            {
                                Name = order.Vendor.Name,
                                AccoutNumber = accountNumber.AccountNumber,
                                BillToName = order.Vendor.Name,
                                BillToAddress1 = order.Vendor.BillAddress.Address1,
                                BillToCity = order.Vendor.BillAddress.City,
                                BillToState = order.Vendor.BillAddress.State,
                                BillToZip = order.Vendor.BillAddress.ZipCode,
                                BillToCountry = order.Vendor.BillAddress.CountryCode2Digits,

                                ShipToName = order.Vendor.Name,
                                ShipToAddress1 = order.Vendor.BillAddress.Address1,
                                ShipToCity = order.Vendor.BillAddress.City,
                                ShipToState = order.Vendor.BillAddress.State,
                                ShipToZip = order.Vendor.BillAddress.ZipCode,
                                ShipToCountry = order.Vendor.BillAddress.CountryCode2Digits,
                                ShipVia = string.Empty,

                                Terms = string.Empty,
                                OOPProcess = 1,
                                OOPPrintPO = 0,
                                OOPVendorID = order.Vendor.ReferenceNum,
                                UniqueId = order.OrderGuid.ToString(),
                            };

                            webOE.OrderItems = new List<WebOEOrderItem>();

                            foreach (var orderItem in order.OrderItems)
                            {
                                var webOEOrderItem = new WebOEOrderItem()
                                {
                                    Quantity = (int)orderItem.Quantity,
                                    ProductName = orderItem.ProductName,
                                    ProductType = orderItem.ProductType,
                                    Manufacturer = orderItem.Manufacturer,
                                    Style = orderItem.Style,
                                    LineItem = Convert.ToInt16(webOE.OrderItems.Count),
                                    Description = string.Empty,
                                    Notes = string.Empty,
                                    BeforeDiscountRetailPrice = 0,
                                    AfterDiscountRetailPrice = 0,
                                    CustomerDiscount = 0,
                                    VendorDiscount = 0,
                                    RetailPrice = 0,
                                    DiscountedPrice = 0,
                                    Discount = string.Empty,
                                    Price = 0,
                                    Cost = Convert.ToSingle(orderItem.Quantity * orderItem.Cost),
                                    OptionsData = orderItem.JobItem.Options.Select(w =>
                                        new KeyValuePair<string, string>(w.Name, w.Value)
                                        ).ToList()
                                };

                                webOE.OrderItems.Add(webOEOrderItem);
                            }
                        }
                        else
                        {
                            webOE = null;
                            status = "Vendor Account # missing.";
                        }
                    }
                    else
                    {
                        status = "Order doesnt exist.";
                    }
                }
            }
            catch (Exception ex)
            {
                webOE = null;
                EventLogger.LogEvent(ex);
            }

            return status;
        }

        public string UpdatePurchaseOrderResult(int orderId, WebOEResult oeResult)
        {
            //if (!BasePermission.CanRead)
            //    throw new UnauthorizedAccessException();

            if (orderId <= 0)
                throw new ArgumentOutOfRangeException(OrderDoesNotExist);

            try
            {
                using (var db = ContextFactory.Create())
                {
                    var order = db.Orders
                        .Where(w => w.OrderId == orderId)
                        .Include(i => i.Job)
                        .Include(i => i.Job.Lead)
                        .SingleOrDefault();

                    if (order != null)
                    {
                        order.SessionId = oeResult.SessionId;
                        order.ReferenceNumber = oeResult.VendorOrderId;
                        order.PONumber = oeResult.OrderId;

                        var typeOrderStatus =
                            CacheManager.OrderStatusTypeCollection.SingleOrDefault(w => w.Name == "Order Sent");
                        if (typeOrderStatus != null)
                            order.OrderStatusId =
                                typeOrderStatus
                                    .Id;

                        var typeJobStatus =
                            CacheManager.JobStatusTypeCollection.SingleOrDefault(w => w.Name == "To Order");
                        if (typeJobStatus != null)
                            order.Job.JobStatusId =
                                typeJobStatus
                                    .Id;

                        var typeLeadStatus =
                            CacheManager.LeadStatusTypeCollection.SingleOrDefault(w => w.Name == "Sale Made");
                        if (typeLeadStatus != null)
                            order.Job.Lead.LeadStatusId =
                                typeLeadStatus
                                    .Id;

                        db.Orders.Attach(order);
                        db.Jobs.Attach(order.Job);
                        db.Leads.Attach(order.Job.Lead);

                        db.Entry(order).Property(i => i.SessionId).IsModified = true;
                        db.Entry(order).Property(i => i.ReferenceNumber).IsModified = true;
                        db.Entry(order).Property(i => i.PONumber).IsModified = true;
                        db.Entry(order).Property(i => i.OrderStatusId).IsModified = true;
                        db.Entry(order.Job).Property(i => i.JobStatusId).IsModified = true;
                        db.Entry(order.Job.Lead).Property(i => i.LeadStatusId).IsModified = true;

                        order.Job.EditHistories.Add(new EditHistory
                        {
                            LoggedByPersonId = User.PersonId,
                            IPAddress = HttpContext.Current.Request.UserHostAddress,
                            HistoryValue =
                                string.Format(
                                    "<Quote operation=\"information\">Order #{0}-{1} has been sent, sessionId: {2}, referenceNumber: {3}</Quote>",
                                    order.OrderId, order.OrderNumber, order.SessionId, order.ReferenceNumber),
                            CreatedOnUtc = DateTime.UtcNow
                        });

                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
            }

            return Success;
        }

        /// <summary>
        /// Duplicate code in Ship notice manager
        /// </summary>
        /// <param name="response"></param>
        /// <param name="itemList"></param>
        /// <param name="listSN"></param>
        /// <param name="dateTime"></param>
        /// <param name="usr"></param>
        /// <param name="vpo"></param>
        /// <returns></returns>
        public bool AddUpdateShipNotice(string response, List<PurchaseOrderDetails> itemList, List<ShipNotice> listSN, DateTime dateTime, int usr, int vpo = 0)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                try
                {
                    var result = JsonConvert.DeserializeObject<SNRootObject>(response);
                    foreach (var picSNItem in result.properties)
                    {
                        if (listSN != null && listSN.Count > 0 && listSN.Where(x => x.ShipmentId == picSNItem.ShipNotice.shipmentNum).FirstOrDefault() != null)
                        {
                            var sn_Item = listSN.Where(x => x.ShipmentId == picSNItem.ShipNotice.shipmentNum).FirstOrDefault();

                            foreach (var item in itemList)
                            {
                                if (picSNItem.ShipNotice.shippedDate != null &&
                                    picSNItem.ShipNotice.shippedDate != "0000-00-00")
                                {
                                    item.ShipDate =
                                        Convert.ToDateTime(picSNItem.ShipNotice.shippedDate);
                                }

                                item.UpdatedOn = dateTime;
                                item.UpdatedBy = usr;
                                connection.Update(item);
                            }
                        }
                        else
                        {
                            var PICSN = new ShipNotice();
                            PICSN.ShipmentId = picSNItem.ShipNotice.shipmentNum;
                            PICSN.PurchaseOrderId = itemList.FirstOrDefault().PurchaseOrderId;
                            PICSN.PICVpo = vpo.ToString();
                            //PICSN.PurchaseOrdersDetailId = item.PurchaseOrdersDetailId;
                            PICSN.ShipNoticeJson = response.ToString();
                            PICSN.BOL = picSNItem.ShipNotice.bOL;
                            if (picSNItem.ShipNotice.shippedDate != null &&
                                picSNItem.ShipNotice.shippedDate != "0000-00-00")
                            {
                                PICSN.ShippedDate =
                                    Convert.ToDateTime(picSNItem.ShipNotice.shippedDate);
                            }

                            PICSN.Weight = picSNItem.ShipNotice.weight.ToString();
                            PICSN.ShippedVia = picSNItem.ShipNotice.shipVia;
                            PICSN.ShippedViaText = picSNItem.ShipNotice.shipViaName;
                            PICSN.VendorName = itemList.Where(x => x.PICPO == vpo).FirstOrDefault().QuoteLines.VendorName;// picSNItem.ShipNotice.vendor;
                            if (picSNItem.ShipNotice.deliveryDate != null &&
                                picSNItem.ShipNotice.deliveryDate != "0000-00-00")
                            {
                                PICSN.EstDeliveryDate =
                                    Convert.ToDateTime(picSNItem.ShipNotice.deliveryDate);
                            }

                            PICSN.CreatedOn = dateTime;
                            PICSN.CreatedBy = usr;
                            PICSN.UpdatedOn = dateTime;
                            PICSN.UpdatedBy = usr;

                            PICSN.TrackingNumber = picSNItem.ShipNotice.trackingNumber;
                            PICSN.TrackingURL = picSNItem.ShipNotice.trackingUrl;
                            PICSN.ShipNoticeId = connection.Insert<int, ShipNotice>(PICSN);

                            for (int i = 0; i < picSNItem.Items.Count(); i++)
                            {
                                var sditem = new ShipmentDetail();
                                sditem.ShipNoticeId = PICSN.ShipNoticeId;
                                sditem.ShipmentId = PICSN.ShipmentId;
                                sditem.POItem = picSNItem.Items[i].poItem.ToString();
                                sditem.QuantityShipped = Convert.ToInt32(picSNItem.Items[i].qty);
                                sditem.BoxId = picSNItem.Items[i].boxId;
                                // UoM code alone will be sent from PIC
                                sditem.UomCode = string.IsNullOrEmpty(picSNItem.Items[i].uom) ?
                                    "EA" :
                                    picSNItem.Items[i].uom;

                                sditem.TrackingId = picSNItem.Items[i].tracking;
                                sditem.TrackingUrl = picSNItem.Items[i].trackingUrl;

                                if (picSNItem.Items[i].ediCustomerItem.HasValue &&
                                    picSNItem.Items[i].ediCustomerItem != 0)
                                {
                                    sditem.QuoteLineId = picSNItem.Items[i].ediCustomerItem.Value;

                                    var po = itemList.Where(x => x.QuoteLineId == sditem.QuoteLineId)
                                        .FirstOrDefault();
                                    sditem.ProductNumber = po.PartNumber;
                                    sditem.ItemDescription = po.QuoteLines.ProductName;
                                }
                                else
                                {
                                    sditem.QuoteLineId = itemList[i].QuoteLineId;
                                    sditem.ProductNumber = itemList[i].PartNumber;
                                    sditem.ItemDescription = itemList[i].QuoteLines.ProductName;
                                }

                                sditem.CreatedOn = PICSN.CreatedOn;
                                sditem.CreatedBy = PICSN.CreatedBy;
                                sditem.UpdatedOn = PICSN.UpdatedOn;
                                sditem.UpdatedBy = PICSN.UpdatedBy;
                                var id = connection.Insert<int, ShipmentDetail>(sditem);
                            }

                            foreach (var item in itemList)
                            {
                                item.ShipDate = PICSN.ShippedDate;
                                item.UpdatedOn = dateTime;
                                item.UpdatedBy = usr;
                                connection.Update(item);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                }
                finally
                {
                    connection.Close();
                }
                return true;
            }
        }

        public object GetOrderStatus()
        {
            try
            {
                var orderStatus = CRMDBContext.Type_OrderStatus.AsNoTracking().Select(s => new { Id = s.Id, Name = s.Name }).ToList();
                return orderStatus;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
            }

            return null;
        }

        //public override Permission BasePermission
        //{
        //    get { return PermissionProvider.GetPermission(this.AuthorizingUser, "Orders"); }
        //}

        public override string Add(Order data)
        {
            throw new NotImplementedException();
        }

        public override string Update(Order data)
        {
            throw new NotImplementedException();
        }

        public override string Delete(int id)
        {
            throw new NotImplementedException();
        }

        public List<QuoteCoreProductDetails> UpdateCoreProductQuoteconfiguration(int QuoteLineId, int QuoteKey, dynamic picResponse)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var newqcpdList = new List<QuoteCoreProductDetails>();
                    var str = JsonConvert.SerializeObject(picResponse);

                    var vsiblepmtsdynamic = JsonConvert.DeserializeObject<dynamic>(str);

                    if (vsiblepmtsdynamic.visiblePrompts != null)
                    {
                        var query =
                            @"DELETE crm.QuoteCoreProductDetails WHERE QuoteLineId = @QuoteLineId";
                        var fDetails = connection.Query(query, new { quotelineId = QuoteLineId });

                        var obj = JsonConvert.DeserializeObject<Dictionary<string, VisiblePrompt>>(
                            JsonConvert.SerializeObject(vsiblepmtsdynamic.visiblePrompts));

                        foreach (var key in obj.Keys)
                        {
                            var newobj = obj[key];

                            var qcpd = new QuoteCoreProductDetails();
                            qcpd.Value = newobj.Value;
                            qcpd.ColorValue = newobj.ColorValue;
                            qcpd.Description = newobj.Description;
                            qcpd.ValueDescription = newobj.ValueDescription;
                            qcpd.ColorValueDescription = newobj.ColorValueDescription;
                            qcpd.QuotelineId = QuoteLineId;
                            qcpd.QuoteKey = QuoteKey;
                            qcpd.QuoteCoreProductDetailsId = connection.Insert<int, QuoteCoreProductDetails>(qcpd);
                            newqcpdList.Add(qcpd);
                        }
                    }

                    return newqcpdList;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool SavequoteData(QuoteLines qline, int mpoId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var usr = User.PersonId;
                    var datetime = DateTime.UtcNow;
                    qline.LastUpdatedBy = usr;
                    qline.LastUpdatedOn = datetime;

                    qline.QuoteLineDetail.LastUpdatedBy = usr;
                    qline.QuoteLineDetail.LastUpdatedOn = datetime;

                    connection.Update(qline);
                    connection.Update(qline.QuoteLineDetail);

                    //RecalculateQuoteLines(qline.QuoteKey, qline.QuoteLineId);
                    QuotesMgr.UpdateMargins(qline.QuoteKey, qline.QuoteLineDetail);

                    ResetErrorStatus(mpoId, qline.QuoteLineId);
                    var po = GetPurchaseOrderDetailsLines(mpoId);
                    if (po != null && po.Count > 0)
                    {
                        var current_Po = po.Where(x => x.QuoteLineId == qline.QuoteLineId).FirstOrDefault();
                        current_Po.Errors = "";
                        current_Po.UpdatedBy = SessionManager.CurrentUser.PersonId;
                        current_Po.UpdatedOn = DateTime.UtcNow;
                        connection.Update(current_Po);
                    }
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return true;
        }

        public bool AuditMPOEditErrorConfig(QuoteLines qline, int mpoId, QuoteLines Old_quoteLine)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var audit = new MPOErrorEditConfigAudit();
                    audit.PurchaseOrderId = mpoId;
                    audit.QuoteLineId = qline.QuoteLineId;
                    audit.PICResponseOLD = Old_quoteLine.PICPriceResponse;
                    audit.PromptAnswerOLD = Old_quoteLine.PICJson;

                    audit.PICResponse = qline.PICPriceResponse;
                    audit.PromptAnswers = qline.PICJson;
                    audit.PromptAnswers = JsonConvert.SerializeObject(Old_quoteLine);

                    audit.UpdatedOn = DateTime.UtcNow;
                    audit.UpdatedBy = SessionManager.CurrentUser.PersonId;
                    audit.Impersonatedby = this.ImpersonatorPersonId;

                    connection.Update(audit);
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return true;
        }

        public bool ResetErrorStatus(int mpoId, int quotelineId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var fQuery = @"  SELECT mpox.AccountId, mpox.OpportunityId, mpox.QuoteId, mpox.OrderId, mpo.* FROM crm.MasterPurchaseOrder mpo
                                    left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                     INNER JOIN crm.Opportunities o ON mpox.OpportunityId = o.OpportunityId
                                     WHERE mpo.PurchaseOrderId = @PurchaseOrderId ;

                                     SELECT ql.QuoteLineNumber as LineNumber,pod.*,ql.ProductTypeId FROM crm.PurchaseOrderDetails pod
                                     inner join crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = pod.PurchaseOrderId
                                        left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                     INNER JOIN crm.Opportunities o ON mpox.OpportunityId = o.OpportunityId
                                     INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
                                     WHERE  mpo.PurchaseOrderId = @PurchaseOrderId ";
                    connection.Open();

                    var fDetails = connection.QueryMultiple(fQuery, new { PurchaseOrderId = mpoId });

                    var mpoDetails = fDetails.Read<MasterPurchaseOrder>().FirstOrDefault();

                    var purchaseOrderDetials = fDetails.Read<PurchaseOrderDetails>().ToList();

                    var podet = purchaseOrderDetials.Where(x => x.QuoteLineId == quotelineId).FirstOrDefault();
                    podet.Status = "Open";
                    podet.StatusId = 1;
                    podet.UpdatedBy = SessionManager.CurrentUser.PersonId;
                    podet.UpdatedOn = DateTime.UtcNow;
                    connection.Update(podet);

                    var allStatus = new List<string>();
                    var allStatusId = new List<int>();
                    var allCorVPOStatusId = new List<int>();
                    bool allCoreVPOsCanceled = false;
                    foreach (var item in purchaseOrderDetials)
                    {
                        item.Status = "Open";
                        item.StatusId = 1;

                        allStatus.Add(item.Status.ToLower().Trim());
                        allStatusId.Add(item.StatusId);
                        if (item.ProductTypeId == 1)
                        {
                            allCorVPOStatusId.Add(item.StatusId);
                        }
                        if (item.ProductTypeId == 1 || item.ProductTypeId == 2 || item.ProductTypeId == 5)
                        {
                            if (item.StatusId == (int)PurchaseOrderStatus.Canceled)
                            {
                                allCoreVPOsCanceled = true;
                            }
                            else
                            {
                                allCoreVPOsCanceled = false;
                            }
                        }
                    }

                    ///This is the order of status MPO needs to be in
                    //10  Error
                    //1   Open
                    //2   Submitted
                    //9   Processing
                    //11  ACKNOWLEDGED
                    //4   Partial Shipped Back Ordered
                    //3   Shipped
                    //5   Invoiced
                    //7   Closed
                    //8   Canceled

                    if (!allCorVPOStatusId.Contains(10) && !allCorVPOStatusId.Contains(7))
                    {
                        mpoDetails.Status = "Open";
                        mpoDetails.POStatusId = 1;
                        mpoDetails.UpdatedBy = SessionManager.CurrentUser.PersonId;
                        mpoDetails.UpdateOn = DateTime.UtcNow;

                        mpoDetails.SubmittedTOPIC = false;

                        connection.Update(mpoDetails);
                    }

                    EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(mpoDetails), PurchaseOrderId: mpoDetails.PurchaseOrderId);
                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        //getting mpo detail for naming pdf
        public string GetMpobyId(int Id)
        {
            string fileDownloadName = string.Empty;
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var Query = @"SELECT ord.OrderNumber, mpox.OrderId,
                                  mpo.MasterPONumber, [CRM].[fnGetAccountName_forOpportunitySearch](mpox.OpportunityId) AS AccountName
                                  FROM crm.MasterPurchaseOrder mpo
                                  left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                  INNER JOIN crm.orders ord ON ord.OrderId = mpox.OrderID
                                  INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
                                  left join CRM.Quote q on o.OpportunityId =q.OpportunityId
                                  WHERE mpo.PurchaseOrderId = @PurchaseOrderId AND o.FranchiseId = @FranchiseId AND q.PrimaryQuote = 1;";
                    var Mpo = connection.Query<MasterPurchaseOrder>(Query, new { FranchiseId = this.Franchise.FranchiseId, PurchaseOrderId = Id }).FirstOrDefault();
                    //var fDetails = connection.QueryMultiple(fQuery, new { FranchiseId = this.Franchise.FranchiseId, PurchaseOrderId = Id }, commandType: CommandType.StoredProcedure);
                    //var Mpo = fDetails.Read<MasterPurchaseOrder>().FirstOrDefault();
                    fileDownloadName = string.Format("SO {0} - MPO {1} - {2}.pdf", Mpo.OrderNumber, Mpo.MasterPONumber, Mpo.AccountName.Replace("/", "-"));
                    return fileDownloadName;
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    return fileDownloadName;
                }
                finally
                {
                    connection.Close();
                }
            }
        }


        //getting mpo related all Ids for showing all the menus related to vendor acknowledgement, ship notice & invoice
        public dynamic GetMPORelatedIds(int PurchaseOrderId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var Query = @" SELECT distinct pod.PurchaseOrderId,pod.PICPO,pod.vendorReference,
		                             sn.ShipmentId,sn.ShipNoticeId,
		                             vi.VendorInvoiceId,vi.InvoiceNumber
		                             FROM crm.PurchaseOrderDetails pod
		                             INNER JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = pod.PurchaseOrderId
		                             INNER JOIN crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
		                             INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
		                             LEFT JOIN crm.ShipNotice sn ON sn.PICVpo = pod.PICPO
		                             LEFT JOIN crm.VendorInvoice vi ON vi.PICVpo = pod.PICPO
		                             WHERE pod.PurchaseOrderId = @PurchaseOrderId AND o.FranchiseId = @FranchiseId";
                    var MPORelatedIds = connection.Query<dynamic>(Query, new { PurchaseOrderId = PurchaseOrderId, FranchiseId = this.Franchise.FranchiseId, }).ToList();
                    return MPORelatedIds;
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    throw e;
                }
            }
        }

        //This method will add or update vendor purchase order table
        public bool AddUpdateVPO(int PurchaseOrderId, VPurchaseorder vpo, DateTime dateTime, int usr)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    if (vpo != null)
                    {
                        var sVPOQuery = @"SELECT * FROM crm.VendorPurchaseOrder where PICPO = @PICPO and PurchaseOrderId = @PurchaseOrderId;";

                        var VPOData = connection.Query<VendorPurchaseOrder>(sVPOQuery, new { PICPO = Convert.ToInt32(vpo.po), PurchaseOrderId }).FirstOrDefault();

                        if (VPOData == null)
                        {
                            VPOData = new VendorPurchaseOrder();
                            VPOData.PurchaseOrderId = PurchaseOrderId;
                            VPOData.PICPO = Convert.ToInt32(vpo.po);

                            VPOData.CreatedBy = usr;
                            VPOData.CreatedOn = dateTime;
                            VPOData.UpdatedBy = usr;
                            VPOData.UpdatedOn = dateTime;
                        }
                        else
                        {
                            VPOData.UpdatedBy = usr;
                            VPOData.UpdatedOn = dateTime;
                        }

                        if (vpo.dateAcknowledged != null && vpo.dateAcknowledged != "0000-00-00" && vpo.dateAcknowledged != "")
                        {
                            VPOData.DateAcknowledged = Convert.ToDateTime(vpo.dateAcknowledged);
                        }

                        VPOData.VendorReference = vpo.vendorReference;
                        VPOData.Status = Convert.ToInt32(vpo.status);
                        VPOData.StatusDescription = vpo.statusDescription;
                        VPOData.Currency = vpo.currency;
                        VPOData.Terms = vpo.terms;
                        VPOData.Comment = vpo.comment;
                        VPOData.VendorMSG = vpo.VendorMsg;
                        VPOData.ShipName = vpo.shipName;
                        VPOData.ShipAddress1 = vpo.shipAddress1;
                        VPOData.ShipAddress2 = vpo.shipAddress2;
                        VPOData.ShipCity = vpo.shipCity;
                        VPOData.ShipState = vpo.shipState;
                        VPOData.ShipZip = vpo.shipZip;
                        VPOData.ShipCountry = vpo.shipCountry;
                        VPOData.ShipPhone = vpo.shipPhone;
                        VPOData.ShipFax = vpo.shipFax;
                        VPOData.ShipFax = vpo.shipFax;

                        if (VPOData.VendorPurchaseOrderId == 0)
                            connection.Insert<int, VendorPurchaseOrder>(VPOData);
                        else
                            connection.Update(VPOData);
                    }
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                }
                return true;
            }
        }

        //this method is to update the VendorPurchaseOrder table and new pic fields in the purchaseorderdetails from the existing POResponse field
        public bool UpdateVPOPICResponse(int PurchaseOrderId, string vendorReference)
        {
            var dateTime = DateTime.UtcNow;
            var usr = 1107554;
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var sVPOQuery = @"select pod.* FROM crm.PurchaseOrderDetails pod
                                    inner join crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = pod.PurchaseOrderId
                                    inner join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                    inner join crm.Opportunities opp ON opp.OpportunityId = mpox.OpportunityId
                                    inner join crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
                                    WHERE ql.ProductTypeId = 1 AND pod.StatusId NOT IN (7,8) --7 is closed and 8 is cancelled
		                            AND opp.FranchiseId = @FranchiseId AND pod.PurchaseOrderId = @PurchaseOrderId 
                                    AND pod.vendorReference=@vendorReference";

                    var podDetails = connection.Query<PurchaseOrderDetails>(sVPOQuery, new
                    {
                        FranchiseId = SessionManager.CurrentFranchise.FranchiseId,
                        PurchaseOrderId,
                        vendorReference
                    }).ToList();

                    if (podDetails != null && podDetails.Count > 0)
                    {
                        var podjson = podDetails.Where(a => a.PICPO.HasValue).FirstOrDefault();

                        var porespJson = podjson != null ? podjson.POResponse : null;

                        try
                        {
                            if (!string.IsNullOrEmpty(porespJson))
                                if (JsonConvert.DeserializeObject<dynamic>(porespJson).valid.Value)
                                {
                                    var PICPODetails = JsonConvert.DeserializeObject<PICVPOObject>(porespJson);
                                    if (PICPODetails.valid)
                                    {
                                        int po = 0;
                                        foreach (var item in PICPODetails.properties.Items)
                                        {
                                            var podetails = new PurchaseOrderDetails();

                                            if (item.ediCustomerItem.HasValue && item.ediCustomerItem.Value != 0)
                                            {
                                                podetails = podDetails.Where(x =>
                                                    x.PICPO == Convert.ToInt32(item.po) &&
                                                    x.QuoteLineId == item.ediCustomerItem.Value).FirstOrDefault();

                                                if (podetails != null)
                                                {
                                                    if (podetails != null)
                                                    {
                                                        if (item.estShipDate != null && item.estShipDate != "0000-00-00" &&
                                                                item.estShipDate != "")
                                                        {
                                                            podetails.estShipDate = Convert.ToDateTime(item.estShipDate);
                                                        }

                                                        podetails.QTY = Convert.ToInt32(Convert.ToDecimal(item.qty));
                                                        podetails.AckCost = Convert.ToDecimal(item.AckCost);
                                                        podetails.Discount = Convert.ToDecimal(item.discGoods);
                                                        if (!string.IsNullOrEmpty(item.item))
                                                        {
                                                            podetails.item = Convert.ToInt32(item.item);
                                                        }
                                                        if (!string.IsNullOrEmpty(item.woItem))
                                                        {
                                                            podetails.woItem = Convert.ToInt32(item.woItem);
                                                        }
                                                        podetails.UoM = (string.IsNullOrEmpty(item.UoM) ? "EA" : item.UoM);
                                                        connection.Update(podetails);
                                                        po = podetails.PurchaseOrderId;
                                                    }
                                                }
                                            }
                                        }

                                        if (po != 0)
                                        {
                                            AddUpdateVPO(po, PICPODetails.properties.PurchaseOrder, dateTime, usr);
                                        }
                                    }
                                }
                        }
                        catch (Exception ex) { EventLogger.LogEvent(ex); }
                    }
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                }
                return true;
            }
        }

        //this method is to update the VendorPurchaseOrder table and new pic fields in the purchaseorderdetails from the existing POResponse field
        public bool SyncDBVPOPICResponse()
        {
            var dateTime = DateTime.UtcNow;
            var usr = 1107554;
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var sVPOQuery = @"select pod.* FROM crm.PurchaseOrderDetails pod
                                    inner join crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = pod.PurchaseOrderId
                                    inner join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                    inner join crm.Opportunities opp ON opp.OpportunityId = mpox.OpportunityId
                                    inner join crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
                                    WHERE ql.ProductTypeId = 1 AND pod.StatusId NOT IN (7,8) --7 is closed and 8 is cancelled
                                    order by pod.PurchaseOrdersDetailId desc";

                    var podDetails = connection.Query<PurchaseOrderDetails>(sVPOQuery).ToList();

                    if (podDetails != null && podDetails.Count > 0)
                    {
                        var pogroup = podDetails.Where(a => a.PICPO.HasValue).GroupBy(x => x.PICPO).ToList();

                        foreach (var pogItem in pogroup)
                        {
                            if (pogItem.Key.HasValue && pogItem.Key.Value != 0)
                            {
                                var podjson = podDetails.Where(x => x.PICPO == pogItem.Key.Value).FirstOrDefault();

                                var porespJson = podjson != null ? podjson.POResponse : null;

                                try
                                {
                                    if (!string.IsNullOrEmpty(porespJson))
                                        if (JsonConvert.DeserializeObject<dynamic>(porespJson).valid.Value)
                                        {
                                            var PICPODetails = JsonConvert.DeserializeObject<PICVPOObject>(porespJson);
                                            if (PICPODetails.valid)
                                            {
                                                int po = 0;
                                                foreach (var item in PICPODetails.properties.Items)
                                                {
                                                    var podetails = new PurchaseOrderDetails();

                                                    if (item.ediCustomerItem.HasValue && item.ediCustomerItem.Value != 0)
                                                    {
                                                        podetails = podDetails.Where(x =>
                                                            x.PICPO == Convert.ToInt32(item.po) &&
                                                            x.QuoteLineId == item.ediCustomerItem.Value).FirstOrDefault();

                                                        if (podetails != null)
                                                        {
                                                            if (podetails != null)
                                                            {
                                                                if (item.estShipDate != null && item.estShipDate != "0000-00-00" &&
                                                                        item.estShipDate != "")
                                                                {
                                                                    podetails.estShipDate = Convert.ToDateTime(item.estShipDate);
                                                                }

                                                                podetails.QTY = Convert.ToInt32(Convert.ToDecimal(item.qty));
                                                                podetails.AckCost = Convert.ToDecimal(item.AckCost);
                                                                podetails.Discount = Convert.ToDecimal(item.discGoods);
                                                                if (!string.IsNullOrEmpty(item.item))
                                                                {
                                                                    podetails.item = Convert.ToInt32(item.item);
                                                                }
                                                                if (!string.IsNullOrEmpty(item.woItem))
                                                                {
                                                                    podetails.woItem = Convert.ToInt32(item.woItem);
                                                                }
                                                                podetails.UoM = (string.IsNullOrEmpty(item.UoM) ? "EA" : item.UoM);
                                                                connection.Update(podetails);
                                                                po = podetails.PurchaseOrderId;
                                                            }
                                                        }
                                                    }
                                                }

                                                if (po != 0)
                                                {
                                                    AddUpdateVPO(po, PICPODetails.properties.PurchaseOrder, dateTime, usr);
                                                }
                                            }
                                        }
                                }
                                catch (Exception ex) { EventLogger.LogEvent(ex); }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                }
                return true;
            }
        }
    }
}