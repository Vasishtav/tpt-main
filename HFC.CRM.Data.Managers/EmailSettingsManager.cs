﻿using Dapper;
using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Managers
{
    public class EmailSettingsManager : DataManager
    {
        public EmailSettingsManager(Franchise franchise, User user)
        {
            Franchise = franchise;
            AuthorizingUser = user;
        }

        public List<TerritoryEmailSettings> GetDetails()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {

                {
                    var Query = "";
                    Query = @"select tes.Id,t.TerritoryId,t.Name as TerritoryName,T.Code,T.Minion,T.BrandId,isnull(tes.UseFranchiseName,1) UseFranchiseName,
                              (select [CRM].[fnGetTerritoryDisplayName](t.BrandId,t.TerritoryId)) as Displayname
                              ,LicenseNumber,FromEmailPersonId,ShipToLocationId,ShipToName,stl.AddressId,p.PrimaryEmail
                              ,ad.Address1,ad.Address2,ad.City,ad.State,ad.CountryCode2Digits,ad.ZipCode
                              from CRM.Territories t
                              left join CRM.TerritoryEmailSettings tes on t.TerritoryId=tes.TerritoryId
                              left join CRM.Person p on tes.FromEmailPersonId=p.PersonId
                              left join CRM.ShipToLocations stl on tes.ShipToLocationId=stl.Id
                              left join CRM.Addresses ad on stl.AddressId=ad.AddressId
                              where t.FranchiseId=@FranchiseId";
                    var Value = connection.Query<TerritoryEmailSettings>(Query, new { FranchiseId = Franchise.FranchiseId }).ToList();
                    return Value;
                }
            }
        }

        public List<TerritoryEmailSettings> GetLoaction()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {

                {
                    var Query = "";
                    Query = @"select Id as ShipToLocationId,ShipToName as ShipToName,AddressId from CRM.ShipToLocations where FranchiseId=@FranchiseId";
                    var Value = connection.Query<TerritoryEmailSettings>(Query, new { FranchiseId = Franchise.FranchiseId }).ToList();
                    return Value;
                }
            }
        }

        public string GetLocAddress(int addressid)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {

                {
                    var Query = "";
                    Query = @"select * from CRM.Addresses where AddressId=@AddressId";
                    var Value = connection.Query<Address>(Query, new { AddressId = addressid }).FirstOrDefault();
                    if (Value != null)
                    {
                        var Result = "";
                        if (Value.Address1 != null && Value.Address1 != "")
                        {
                            Result = Value.Address1;
                        }
                        if (Value.Address2 != null && Value.Address2 != "")
                        {
                            if (Result != null && Result != "")
                                Result = Result + "," + Value.Address2;
                            else
                                Result = Value.Address2;
                        }
                        if (Value.City != null && Value.City != "")
                        {
                            if (Result != null && Result != "")
                                Result = Result + "," + Value.City;
                            else
                                Result = Value.City;
                        }
                        if (Value.State != null && Value.State != "")
                        {
                            if (Result != null && Result != "")
                                Result = Result + "," + Value.State;
                            else
                                Result = Value.State;
                        }
                        if (Value.CountryCode2Digits != null && Value.CountryCode2Digits != "")
                        {
                            if (Result != null && Result != "")
                                Result = Result + "," + Value.CountryCode2Digits;
                            else
                                Result = Value.CountryCode2Digits;
                        }
                        if (Value.ZipCode != null && Value.ZipCode != "")
                        {
                            if (Result != null && Result != "")
                                Result = Result + "," + Value.ZipCode;
                            else
                                Result = Value.ZipCode;
                        }

                        return Result;
                    }
                    return null;
                }
            }
        }

        public string SaveData(List<TerritoryEmailSettings> data)
        {
            var Updatevalue = 0;
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                foreach (var item in data)
                {
                    if (item.Id == 0)
                    {
                        item.CreatedBy = AuthorizingUser.Person.PersonId;
                        item.LastUpdatedBy = AuthorizingUser.Person.PersonId;
                        var TerritoryEmailSettingsId = connection.Insert(item);
                    }
                    else
                    {
                        item.CreatedBy = AuthorizingUser.Person.PersonId;
                        item.LastUpdatedBy = AuthorizingUser.Person.PersonId;
                        Updatevalue = connection.Update(item);
                    }

                }
            }
            return "Success";
        }
                
    }
}
