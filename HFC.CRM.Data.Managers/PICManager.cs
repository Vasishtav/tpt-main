﻿using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.DTO.PIC;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;
using HFC.CRM.Data.Context;
using System.Data.SqlClient;
using HFC.CRM.DTO.Address;
using System.Net;
using HFC.CRM.Core.Extensions;

namespace HFC.CRM.Managers
{
    public class PICManager : DataManager<HFCVendor>
    {
        public override string Add(HFCVendor data)
        {
            throw new NotImplementedException();
        }

        public override string Delete(int id)
        {
            throw new NotImplementedException();
        }

        public override HFCVendor Get(int id)
        {
            throw new NotImplementedException();
        }

        public override ICollection<HFCVendor> Get(List<int> idList)
        {
            throw new NotImplementedException();
        }

        public override string Update(HFCVendor data)
        {
            throw new NotImplementedException();
        }

        public static string PICToken()
        {
            string CacheKey = "PICToken";
            ObjectCache cache = MemoryCache.Default;

            if (cache.Contains(CacheKey))
                return cache.Get(CacheKey).ToString();
            else
            {
                var authMgr = new OAuthManager();
                var picToken = authMgr.GetAccessToken();
                CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
                cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddMinutes(55);
                cache.Add(CacheKey, picToken.token.access_token, cacheItemPolicy);

                return picToken.token.access_token;
            }
        }

        private Log_Error _errorLog = new Log_Error();

        /// <summary>
        /// /SalesOrder/add_order_and_create_po_async
        /// </summary>
        /// <param name="orderObj"></param>
        /// <returns></returns>
        public string SubmitPICOrder_Async(dynamic orderObj)
        {
            try
            {
                var start = new Stopwatch();
                string url = AppConfigManager.PICUrl;

                string FranchiseCode = SessionManager.CurrentFranchise.Code;
                var api = url + "/SalesOrder/add_order_and_create_po_async?access_token=" + PICToken();

                var client = new RestClient(api);
                var request = new RestRequest(Method.PUT);
                request.AddHeader("cache-control", "no-cache");
                request.AddParameter("undefined", JsonConvert.SerializeObject(orderObj), ParameterType.RequestBody);
                //client.Timeout = 300000;/// For now set the Timeout to be at 5 mins Until the PIC Async is setup
                start.Start();
                IRestResponse response = client.Execute(request);
                start.Stop();
                if (response.ErrorException != null)
                {
                    throw response.ErrorException;
                }

                EventLogger.LogRequest(request, response, start.ElapsedMilliseconds, "PIC", "/SalesOrder/add_order_and_create_po_async");
                return response.Content;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw ex;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string GetAndUpdateTax(dynamic orderObj)
        {
            try
            {
                var start = new Stopwatch();
                string url = AppConfigManager.PICUrl;
                var api = url + "/SalesOrder/get_tax?access_token=" + PICToken();
                var client = new RestClient(api);
                var request = new RestRequest(Method.PUT);
                request.AddHeader("cache-control", "no-cache");
                request.AddParameter("undefined", JsonConvert.SerializeObject(orderObj), ParameterType.RequestBody);
                start.Start();
                IRestResponse response = client.Execute(request);
                start.Stop();
                EventLogger.LogRequest(request, response, start.ElapsedMilliseconds, "PIC", "/get_tax");
                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Check Async
        /// </summary>
        /// <param name="counter"></param>
        /// <returns></returns>
        public string CheckMPOAsyncStatus(int counter)
        {
            try
            {
                var start = new Stopwatch();
                string url = AppConfigManager.PICUrl;

                var api = url + "/SalesOrder/" + counter + "/check_async_add?access_token=" + PICToken();
                var client = new RestClient(api);
                var request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "no-cache");
                start.Start();
                IRestResponse response = client.Execute(request);
                start.Stop();
                EventLogger.LogRequest(request, response, start.ElapsedMilliseconds, "PIC", "/SalesOrder/check_async_add");
                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get VPO Status
        /// </summary>
        /// <param name="VPOID"></param>
        /// <returns></returns>
        public string GetCurrentVPOStatus(int vpoId, string porespo = null)
        {
            try
            {
                string url = AppConfigManager.PICUrl;
                var start = new Stopwatch();
                var api = url + "/PurchaseOrder/" + vpoId + "?access_token=" + PICToken();
                var client = new RestClient(api);
                var request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "no-cache");
                start.Start();
                IRestResponse response = client.Execute(request);
                start.Stop();
                if (response.Content != porespo)
                {
                    //EventLogger.LogRequest(request, response, start.ElapsedMilliseconds, "PIC", "/PurchaseOrder");
                }
                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// There are three steps in SYncing PIC to Touchpoint
        /// Step 1: Get all the Vendors from PIC and check the status and add or update in Touchpoint
        /// Step 2: Get All products by Vendor and Sync with PIC to Touchpoint and add or update in Touchpoint
        /// Step 3: Take all products from Touchpoint and check with pic Where they are Discontinued or phased out and update Touchpoint
        ///
        /// 04/25/2018 Step 3 Use always use only active products Do not consider inactive products, If any scenario a product needs to be reactivated that needs to be done through DB Script agreed by Devon
        /// </summary>
        /// <returns></returns>
        public bool PICVendorProduct()
        {
            string pToken = PICToken();
            string url = AppConfigManager.PICUrl;
            ImportVendor(url, pToken);
            UpdateVendor(url, pToken);
            UpdateProduct(url, pToken);

            return true;
        }

        /// <summary>
        /// Insert or Update Vendors Information
        /// </summary>
        private bool ImportVendor(string url, string pToken)
        {
            try
            {
                var start = new Stopwatch();
                var avapi = url + "/Vendor?access_token=" + pToken;
                var avclient = new RestClient(avapi);
                var avrequest = new RestRequest(Method.GET);
                avrequest.AddHeader("cache-control", "no-cache");
                start.Start();
                var avresponse = avclient.Execute(avrequest);
                start.Stop();
                EventLogger.LogRequest(avrequest, avresponse, start.ElapsedMilliseconds, "PIC", "/Vendor");
                var avobj = JsonConvert.DeserializeObject<AllVendorRoot>(avresponse.Content); // Convert Json to Object
                if (avobj.valid == true && avobj.properties != null)
                {
                    PICHFCVendorManager _rep = new PICHFCVendorManager();
                    _rep.ImportVendor(avobj);
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
            }

            return true;
        }

        /// <summary>
        /// Update Particular vendor Information
        /// </summary>
        private bool UpdateVendor(string url, string pToken)
        {
            try
            {
                var start = new Stopwatch();
                PICHFCVendorManager _rep = new PICHFCVendorManager();
                var VendorList = _rep.GetListVendor();
                foreach (var vlist in VendorList)
                {
                    var vapi = url + "/Vendor/" + vlist.PICVendorId + "?access_token=" + pToken;
                    var vclient = new RestClient(vapi);
                    var vrequest = new RestRequest(Method.GET);
                    vrequest.AddHeader("cache-control", "no-cache");
                    start.Start();
                    var vresponse = vclient.Execute(vrequest);
                    start.Stop();
                    EventLogger.LogRequest(vrequest, vresponse, start.ElapsedMilliseconds, "PIC", "Vendor");
                    var vobj = JsonConvert.DeserializeObject<VendorRoot>(vresponse.Content);  // Convert Json to Object
                    if (vobj.valid == true && vobj.properties != null && vobj.properties.Vendor != null)
                    {
                        PICHFCVendorData pvd = new PICHFCVendorData();
                        pvd.h = vlist;
                        pvd.v = vobj;
                        _rep.UpdateVendorData(pvd);
                    }
                    ImportProduct(vlist, url, pToken);
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
            }
            return true;
        }

        /// <summary>
        /// Insert or Update Product Information
        /// </summary>
        private bool ImportProduct(HFCVendor hv, string url, string pToken)
        {
            try
            {
                var start = new Stopwatch();
                var papi = url + "/Vendor/" + hv.PICVendorId + "/Product?access_token=" + pToken;
                var pclient = new RestClient(papi);
                var prequest = new RestRequest(Method.GET);
                prequest.AddHeader("cache-control", "no-cache");
                start.Start();
                var presponse = pclient.Execute(prequest);
                start.Stop();
                EventLogger.LogRequest(prequest, presponse, start.ElapsedMilliseconds, "PIC", "/Vendor/Product");
                var pobj = JsonConvert.DeserializeObject<ProductRoot>(presponse.Content);  // Convert Jason to Object
                if (pobj.valid == true && pobj.properties != null && pobj.properties.Groups != null)
                {
                    PICHFCProductManager _rep = new PICHFCProductManager();
                    _rep.ImportProducts(hv.VendorId, pobj);
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
            }

            return true;
        }

        public static dynamic GetProductInfo(string productId)
        {
            string CacheKey = "GetProductInfo" + productId.ToString();
            ObjectCache cache = MemoryCache.Default;

            if (cache.Contains(CacheKey))
            {
                return cache.Get(CacheKey);
            }
            else
            {
                var start = new Stopwatch();
                string url = AppConfigManager.PICUrl;
                var api = url + "/Product/" + productId + "?access_token=" + PICToken();
                var client = new RestClient(api);
                var request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "no-cache");
                start.Start();
                IRestResponse response = client.Execute(request);
                start.Stop();
                EventLogger.LogRequest(request, response, start.ElapsedMilliseconds, "PIC", "Product");
                var result = JsonConvert.DeserializeObject<dynamic>(response.Content);

                CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
                cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddMinutes(1); cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddMinutes(1);
                //cache.Add(CacheKey, result.properties.Product, cacheItemPolicy);

                return result.properties.Product;
            }
        }

        private bool UpdateProduct(string url, string pToken)
        {
            try
            {
                var start = new Stopwatch();
                PICHFCProductManager _rep = new PICHFCProductManager();
                var ProductList = _rep.GetListCoreProduct();
                foreach (var plist in ProductList)
                {
                    var pvapi = url + "/Product/" + plist.PICProductID + "?access_token=" + pToken;
                    var pclient = new RestClient(pvapi);
                    var prequest = new RestRequest(Method.GET);
                    prequest.AddHeader("cache-control", "no-cache");
                    start.Start();
                    var presponse = pclient.Execute(prequest);
                    start.Stop();
                    EventLogger.LogRequest(prequest, presponse, start.ElapsedMilliseconds, "PIC", "Product");
                    var pobj = JsonConvert.DeserializeObject<sProductRoot>(presponse.Content);  // Convert Json to Object
                    if (pobj.valid == true && pobj.properties != null && pobj.properties.Product != null)
                    {
                        PICHFCProductManager _urep = new PICHFCProductManager();
                        _urep.UpdateProducts(plist, pobj.properties.Product);
                    }
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
            }
            return true;
        }

        /// <summary>
        /// If Customer exists in PIC the update the PIC API
        /// </summary>
        /// <param name="cus"></param>
        /// <returns></returns>
        public bool CreateOrUpdatePICCustomer(PICCustomer cus)
        {
            try
            {
                var start = new Stopwatch();
                RootPICCustomer root = new RootPICCustomer();
                root.Customer = cus;
                string jCustomer = JsonConvert.SerializeObject(root);

                var url = AppConfigManager.PICUrl + "/Customer?access_token=" + PICToken();
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("content-type", "application/json");
                request.AddParameter("application/json", jCustomer, ParameterType.RequestBody);
                start.Start();
                IRestResponse response = client.Execute(request);
                start.Stop();
                EventLogger.LogRequest(request, response, start.ElapsedMilliseconds, "PIC", "Customer");
                var respo = JsonConvert.DeserializeObject<RootPICCustomer>(response.Content);

                if (!respo.valid)
                {
                    if (respo.message.ToUpper().Contains("ALREADY EXISTS"))
                    {
                        UpdatePICCustomer(cus);
                    }
                    EventLogger.LogEvent(respo.message, "PIC Customer Create", LogSeverity.Information, null, null, SessionManager.CurrentUser.UserName, SessionManager.CurrentFranchise.FranchiseId, "", null);
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return false;
            }

            return true;
        }

        public bool UpdatePICCustomer(PICCustomer cus)
        {
            try
            {
                var start = new Stopwatch();
                RootPICCustomer root = new RootPICCustomer();
                root.Customer = cus;
                string jCustomer = JsonConvert.SerializeObject(root);

                var url = AppConfigManager.PICUrl + "/Customer/" + cus.Customer + "?access_token=" + PICToken();
                var client = new RestClient(url);
                var request = new RestRequest(Method.PUT);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("content-type", "application/json");
                request.AddParameter("application/json", jCustomer, ParameterType.RequestBody);
                start.Start();
                IRestResponse response = client.Execute(request);
                start.Stop();
                EventLogger.LogRequest(request, response, start.ElapsedMilliseconds, "PIC", "Customer");
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return false;
            }

            return true;
        }

        public string GetShippingNotice(int vpoId, string shipJson = null)
        {
            try
            {
                var start = new Stopwatch();
                string url = AppConfigManager.PICUrl;

                url = url + "/PurchaseOrder/" + vpoId + "/ShipNotice?access_token=" + PICToken();
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "no-cache");
                start.Start();
                var response = client.Execute(request);
                start.Stop();
                if (shipJson != response.Content)
                {
                    EventLogger.LogRequest(request, response, start.ElapsedMilliseconds, "PIC", "/ShipNotice");
                }

                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetVendorInvoice(int vpoId)
        {
            try
            {
                var start = new Stopwatch();
                string url = AppConfigManager.PICUrl;

                url = url + "/AccountsPayable/by_purchase_order/" + vpoId + "?access_token=" + PICToken();
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "no-cache");
                start.Start();
                var response = client.Execute(request);
                start.Stop();
                EventLogger.LogRequest(request, response, start.ElapsedMilliseconds, "PIC", "/AccountsPayable/by_purchase_order/");

                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Configurator

        public string GetDatasourceURL(string productid, string modelId)
        {
            string url = AppConfigManager.PICUrl;
            var api = url + "/Configurator/Datasource/" + productid + "/" + modelId + "?access_token=" + PICToken();
            return api;
        }

        public static string GetDataSource(string productid, string modelId)
        {
            string CacheKey = "GetDataSource" + productid.ToString();
            ObjectCache cache = MemoryCache.Default;

            if (cache.Contains(CacheKey))
            {
                return cache.Get(CacheKey).ToString();
            }
            else
            {
                var start = new Stopwatch();
                string url = AppConfigManager.PICUrl;
                var api = url + "/Configurator/Datasource/" + productid + "/" + modelId + "?access_token=" + PICToken();
                var client = new RestClient(api);
                var request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "max-age=3600");
                start.Start();
                IRestResponse response = client.Execute(request);
                start.Stop();
                EventLogger.LogRequest(request, response, start.ElapsedMilliseconds, "PIC", "Datasource");

                ///Cache the datasource for a day
                CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
                cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddMinutes(-1);
                //cache.Add(CacheKey, response.Content, cacheItemPolicy);
                return response.Content;
            }
        }

        public static dynamic GetModels(string productId)
        {
            try
            {
                string CacheKey = "GetModels" + productId;
                ObjectCache cache = MemoryCache.Default;

                if (cache.Contains(CacheKey))
                {
                    return cache.Get(CacheKey);
                }
                else
                {
                    var start = new Stopwatch();

                    string url = AppConfigManager.PICUrl;
                    var api = url + "/Product/" + productId + "/Model?access_token=" + PICToken();
                    var client = new RestClient(api);
                    var request = new RestRequest(Method.GET);
                    request.AddHeader("cache-control", "max-age=3600");
                    start.Start();
                    IRestResponse response = client.Execute(request);
                    start.Stop();
                    EventLogger.LogRequest(request, response, start.ElapsedMilliseconds, "PIC", "Model");
                    dynamic responseObj = JsonConvert.DeserializeObject<ExpandoObject>(response.Content);
                    dynamic PICModels = new List<ExpandoObject>();

                    // https://www.developerfusion.com/community/blog-entry/8389108/c-40-why-dynamic-binding-and-extension-methods-dont-mix/
                    var temp = DateTime.Now;

                    if (responseObj.valid)
                    {
                        foreach (var item in responseObj.properties)
                        {
                            dynamic dyno = new ExpandoObject();
                            dyno.Model = item.Model;
                            dyno.Description = item.Description;
                            dyno.PhaseOutDate = item.PhaseOutDate;
                            dyno.DiscontinuedDate = item.DiscontinuedDate;

                            //Set if Model is active or not based on the discontinued date. if there is a discountinue date show warning message on screen for FE
                            if (item.DiscontinuedDate == "0000-00-00" || item.DiscontinuedDate == "9999-12-31")
                            {
                                dyno.IsActive = true;
                            }
                            else if ((item.DiscontinuedDate != "0000-00-00" && Convert.ToDateTime(item.DiscontinuedDate) > DateTime.Now))
                            {
                                dyno.IsActive = true;
                                temp = Convert.ToDateTime(item.DiscontinuedDate);
                                dyno.Discontinued = "Model will be Discontinued from " + temp.GlobalDateFormat();
                            }
                            else if ((item.DiscontinuedDate != "0000-00-00" && Convert.ToDateTime(item.DiscontinuedDate) < DateTime.Now))
                            {
                                dyno.IsActive = false;
                                temp = Convert.ToDateTime(item.DiscontinuedDate);
                                dyno.Discontinued = "Model is Discontinued on " + temp.GlobalDateFormat();
                            }
                            if (item.PhaseOutDate != "0000-00-00" && item.PhaseOutDate != "9999-12-31")
                            {
                                //var samp = Convert.ToDateTime(item.PhaseOutDate);                             
                                temp = Convert.ToDateTime(item.PhaseOutDate);
                                dyno.WarningPhasingOut = "Model is Phasing out from" + temp.GlobalDateFormat();
                            }
                            else
                            {
                                dyno.WarningPhasingOut = "";
                            }
                            PICModels.Add(dyno);
                        }
                        CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
                        cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddDays(-1);
                        //cache.Add(CacheKey, PICModels, cacheItemPolicy);
                    }
                    return PICModels;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string PICModelsByProduct(int productId)
        {
            var start = new Stopwatch();
            string url = AppConfigManager.PICUrl;
            var api = url + "/Product/" + productId + "/Model?access_token=" + PICToken();
            var client = new RestClient(api);
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            start.Start();
            IRestResponse response = client.Execute(request);
            start.Stop();
            EventLogger.LogRequest(request, response, start.ElapsedMilliseconds, "PIC", "PICModelsByProduct");
            return response.Content;

        }

        public static string PICDatasourceByProduct(string productId)
        {
            var start = new Stopwatch();
            string CacheKey = "PICDatasourceByProduct" + productId;
            ObjectCache cache = MemoryCache.Default;

            if (cache.Contains(CacheKey))
            {
                return cache.Get(CacheKey).ToString();
            }
            else
            {
                string url = AppConfigManager.PICUrl;
                var api = url + "/Configurator/Datasource/" + productId + "?access_token=" + PICToken();
                var client = new RestClient(api);
                var request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "max-age=3600");
                start.Start();
                IRestResponse response = client.Execute(request);
                start.Stop();
                EventLogger.LogRequest(request, response, start.ElapsedMilliseconds, "PIC", "Datasource");

                CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
                cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddDays(-1);
                //cache.Add(CacheKey, response.Content, cacheItemPolicy);
                return response.Content;
            }
        }

        public static string PICProductPromptAnswers(string productId, string modelId, string FranchiseCode)
        {
            var start = new Stopwatch();

            string CacheKey = "PICProductPromptAnswers" + productId + modelId;
            ObjectCache cache = MemoryCache.Default;

            if (cache.Contains(CacheKey))
            {
                return cache.Get(CacheKey).ToString();
            }
            else
            {
                string url = AppConfigManager.PICUrl;

                var api = url + "/Configurator/PromptAnswers/Customer/" + FranchiseCode + "/" + productId + "/" + modelId + "?access_token=" + PICToken();
                var client = new RestClient(api);
                var request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "no-cache");
                start.Start();
                IRestResponse response = client.Execute(request);
                start.Stop();
                EventLogger.LogRequest(request, response, start.ElapsedMilliseconds, "PIC", "PromptAnswers");

                CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
                cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddDays(-1);
                //cache.Add(CacheKey, response.Content, cacheItemPolicy);

                return response.Content;
            }
        }

        public static string GetPICVendor(string PICVendorId)
        {
            var start = new Stopwatch();

            string CacheKey = "GetPICVendor" + PICVendorId;
            ObjectCache cache = MemoryCache.Default;

            if (cache.Contains(CacheKey))
            {
                return cache.Get(CacheKey).ToString();
            }
            else
            {
                string url = AppConfigManager.PICUrl;
                var api = url + "/Vendor/" + PICVendorId + "?access_token=" + PICToken();
                var client = new RestClient(api);
                var request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "no-cache");
                start.Start();
                IRestResponse response = client.Execute(request);
                start.Stop();
                EventLogger.LogRequest(request, response, start.ElapsedMilliseconds, "PIC", "Vendor");

                CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
                cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddDays(-1);
                //cache.Add(CacheKey, response.Content, cacheItemPolicy);

                return response.Content;
            }
        }

        public string ValidatePromptAnswersforPICConfigurator(CoreProductVM obj)
        {
            string content = "";
            try
            {
                Stopwatch start = new Stopwatch();
                string url = AppConfigManager.PICUrl;
                string FranchiseCode = SessionManager.CurrentFranchise.Code;
                var api = url + "/Configurator/PromptAnswers/Validate/" + FranchiseCode + "/" + obj.PicProduct + "/" +
                          obj.ModelId + "?access_token=" + PICManager.PICToken();
                var client = new RestClient(api);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");

                request.AddHeader("content-type", "application/x-www-form-urlencoded");
                request.AddParameter("application/x-www-form-urlencoded", "promptAnswers=" + obj.PromptAnswers,
                    ParameterType.RequestBody);
                start.Start();
                IRestResponse response = client.Execute(request);
                start.Stop();
                EventLogger.LogRequest(request, response, start.ElapsedMilliseconds, "PIC", "Validate");
                content = response.Content;
                return response.Content;
            }
            catch (Exception ex)
            {
                string msg = "";
                if (ex.Message ==
                    "Unexpected character encountered while parsing value: <. Path '', line 0, position 0.")
                {
                    msg = content;
                }
                else
                {
                    msg = ex.Message;
                }

                EventLogger.LogEvent(ex);
                throw ex;
            }
        }


        public string ReValidateByCounterIDs(List<QuoteLineDetail> obj)
        {
            string content = "";
            try
            {
                Stopwatch start = new Stopwatch();
                string url = AppConfigManager.PICUrl;
                string FranchiseCode = SessionManager.CurrentFranchise.Code;
                var api = url + "/Configurator/PromptAnswers/Validate/Counter?access_token=" + PICManager.PICToken();
                var client = new RestClient(api);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("content-type", "application/x-www-form-urlencoded");
                request.AddParameter("application/x-www-form-urlencoded", "counter=" + String.Join(",", obj.Select(x => x.CounterId).ToList()), ParameterType.RequestBody);
                start.Start();
                IRestResponse response = client.Execute(request);
                start.Stop();
                EventLogger.LogRequest(request, response, start.ElapsedMilliseconds, "PIC", "Validate");
                content = response.Content;
                return response.Content;
            }
            catch (Exception ex)
            {
                string msg = "";
                if (ex.Message ==
                    "Unexpected character encountered while parsing value: <. Path '', line 0, position 0.")
                {
                    msg = content;
                }
                else
                {
                    msg = ex.Message;
                }

                EventLogger.LogEvent(ex);
                throw ex;
            }
        }

        #endregion Configurator

        #region Vendor Customer

        public string PUTVendorCustomer(string vendorId, string vendorCustomerId)
        {
            try
            {
                string url = AppConfigManager.PICUrl;
                var feCode = SessionManager.CurrentFranchise.Code;
                var start = new Stopwatch();
                url = url + "/VendorCustomer/" + vendorId + "/" + feCode + "/" + vendorCustomerId + "?access_token=" + PICToken();
                var client = new RestClient(url);
                var request = new RestRequest(Method.PUT);
                request.AddHeader("cache-control", "no-cache,no-cache");
                request.AddHeader("Content-Type", "application/json");
                start.Start();
                IRestResponse response = client.Execute(request);
                start.Stop();
                EventLogger.LogRequest(request, response, start.ElapsedMilliseconds, "PIC", "/VendorCustomer");
                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GETAllVendorCustomer(string franchisecode)
        {
            try
            {
                string url = AppConfigManager.PICUrl;
                var start = new Stopwatch();
                url = url + "/Customer/" + franchisecode + "/VendorCustomer?access_token=" + PICToken();
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "no-cache,no-cache");
                request.AddHeader("Content-Type", "application/json");
                start.Start();
                IRestResponse response = client.Execute(request);
                start.Stop();
                EventLogger.LogRequest(request, response, start.ElapsedMilliseconds, "PIC", "/VendorCustomer");
                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string DeleteVendorCustomer(string vendorId, string vendorCustomerId)
        {
            try
            {
                string url = AppConfigManager.PICUrl;
                var feCode = SessionManager.CurrentFranchise.Code;
                var start = new Stopwatch();
                url = url + "/VendorCustomer/" + vendorId + "/" + feCode + "/" + vendorCustomerId + "?access_token=" + PICToken();
                var client = new RestClient(url);
                var request = new RestRequest(Method.DELETE);
                request.AddHeader("cache-control", "no-cache");
                start.Start();
                var response = client.Execute(request);
                start.Stop();
                EventLogger.LogRequest(request, response, start.ElapsedMilliseconds, "PIC", "/VendorCustomer");
                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GETVendorCustomer(string vendorId, string vendorCustomerId)
        {
            try
            {
                string url = AppConfigManager.PICUrl;
                var feCode = SessionManager.CurrentFranchise.Code;
                var start = new Stopwatch();
                url = url + "/VendorCustomer/" + vendorId + "/" + feCode + "?access_token=" + PICToken();
                var client = new RestClient(url);
                var request = new RestRequest(Method.DELETE);
                request.AddHeader("cache-control", "no-cache");
                start.Start();
                var response = client.Execute(request);
                start.Stop();
                EventLogger.LogRequest(request, response, start.ElapsedMilliseconds, "PIC", "/VendorCustomer");
                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool BatchVendorCustomer()
        {
            //dynamic obj = new ExpandoObject();
            //obj.FranchiseId = 0;
            //obj.VendorId = 0;
            //obj.PICVendorId = "";
            //obj.AccountNumber = "";
            //obj.code = "";

            var query = @"SELECT  DISTINCT vta.FranchiseId,f.code,vta.VendorId,h.PICVendorId,vta.AccountNumber  FROM crm.VendorTerritoryAccount vta
                          INNER JOIN acct.HFCVendors h ON h.VendorId = vta.VendorId
                          INNER JOIN crm.Franchise f ON f.FranchiseId = vta.FranchiseId
                          WHERE h.IsAlliance =1;";

            var result = ExecuteIEnumerableObject<VendorAccountBatch>(ContextFactory.CrmConnectionString, query).ToList();
            if (result != null)
            {
                foreach (var item in result)
                {
                    var te = PUTBatchVendorCustomer(item.PICVendorId, item.AccountNumber.Trim(), item.code);
                }
                //var str =
            }

            return true;
        }

        public string PUTBatchVendorCustomer(string vendorId, string vendorCustomerId, string fecode)
        {
            try
            {
                string url = AppConfigManager.PICUrl;
                var start = new Stopwatch();
                url = url + "/VendorCustomer/" + vendorId + "/" + fecode + "/" + vendorCustomerId + "?access_token=" + PICToken();
                var client = new RestClient(url);
                var request = new RestRequest(Method.PUT);
                request.AddHeader("cache-control", "no-cache,no-cache");
                request.AddHeader("Content-Type", "application/json");
                start.Start();
                IRestResponse response = client.Execute(request);
                start.Stop();
                EventLogger.LogRequest(request, response, start.ElapsedMilliseconds, "PIC", "/VendorCustomer");
                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Vendor Customer

        public VendorRoot GetVendorInfo(string PICVendorId)
        {
            try
            {
                string pToken = PICToken();
                string url = AppConfigManager.PICUrl;

                PICHFCVendorManager _rep = new PICHFCVendorManager();

                var vapi = url + "/Vendor/" + PICVendorId + "?access_token=" + pToken;
                var vclient = new RestClient(vapi);
                var vrequest = new RestRequest(Method.GET);
                vrequest.AddHeader("cache-control", "no-cache");
                var vresponse = vclient.Execute(vrequest);
                var vobj = JsonConvert.DeserializeObject<VendorRoot>(vresponse.Content);  // Convert Json to Object
                return vobj;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
            }
            return null;
        }

        public ValidateCustomerAddress_Res ValidateCustomerAddress(ValidateCustomerAddress req)
        {
            try
            {
                var start = new Stopwatch();

                var url = AppConfigManager.PICUrl + "/Customer/validateAddress?access_token=" + PICToken();
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("content-type", "multipart/form-data");
                request.AddParameter("Address1", req.Address1, ParameterType.GetOrPost);
                request.AddParameter("Address2", req.Address2, ParameterType.GetOrPost);
                request.AddParameter("City", req.City, ParameterType.GetOrPost);
                request.AddParameter("State", req.State, ParameterType.GetOrPost);
                request.AddParameter("Zip", req.Zip, ParameterType.GetOrPost);
                request.AddParameter("Country", req.Country, ParameterType.GetOrPost);
                start.Start();
                IRestResponse response = client.Execute(request);
                start.Stop();
                EventLogger.LogRequest(request, response, start.ElapsedMilliseconds, "PIC", "ValidateCustomerAddress");

                if (response.StatusCode == HttpStatusCode.OK)
                    return JsonConvert.DeserializeObject<ValidateCustomerAddress_Res>(response.Content);
                else
                {
                    ValidateCustomerAddress_Res res = new ValidateCustomerAddress_Res() { valid = false, message = response.Content.ToString() };
                    return res;
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                ValidateCustomerAddress_Res res = new ValidateCustomerAddress_Res() { valid = false, message = ex.Message.ToString() };
                return res;
            }
        }
    }

    public class VendorAccountBatch
    {
        public int FranchiseId { get; set; }
        public int VendorId { get; set; }
        public string error { get; set; }
        public string PICVendorId { get; set; }
        public string AccountNumber { get; set; }
        public string code { get; set; }
    }
}