﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Google.Apis.Calendar.v3.Data;
using HFC.CRM.Core;
using HFC.CRM.Data;
using System.Net;
using System.IO;
using System.Data.SqlClient;
using Newtonsoft.Json;
using HFC.CRM.Data.Constants;
using HFC.CRM.Core.Logs;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Data.Entity;
using HFC.CRM.Core.Common;
using System.Data.Entity.Core.Objects;

using System.Data.Entity.Core.EntityClient;

using HFC.CRM.DTO;
using Dapper;

using System.Collections;
using System.ComponentModel;
using System.Data.Entity.Internal;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using HFC.CRM.DTO.Calandar;

namespace HFC.CRM.Managers
{
    /// <summary>
    /// Google Manager contains business logic for handling google calendar sync
    /// </summary>
    public class GoogleManager : IMessageConstants
    {
        #region Members & Properties
        /// <summary>
        /// Binding object for Google Http Responses
        /// </summary>
        public class GoogleHttpResponse
        {
            /// <summary>
            /// Gets or sets the message.
            /// </summary>
            /// <value>The message.</value>
            public string message { get; set; }
            /// <summary>
            /// Gets or sets the code.
            /// </summary>
            /// <value>The code.</value>
            public int code { get; set; }
        }

        /// <summary>
        /// The provider name
        /// </summary>
        public const string ProviderName = "Google";

        private enum HttpMethod { GET, POST, DELETE, PUT };
        private Uri CalendarBaseUri = new Uri("https://www.googleapis.com/calendar/v3/calendars/");
        private CRMContext CRMDBContext = new CRMContextEx();
        private CalendarManager CalendarMgr;
        private Regex DateRegex = new Regex(@"(\d{4})(\d{2})(\d{2})T(\d{2})(\d{2})(\d{2})", RegexOptions.IgnoreCase);
        private List<string> AppointmentTypeEnumStrings = new List<string>();
        private Sync_Tracker Tracker;

        /// <summary>
        /// Gets the Google API key
        /// </summary>
        /// <value>The API key.</value>
        public string ApiKey { get { return AppConfigManager.GetConfig<string>("CalendarApiKey", "GoogleCalendar", "Authentication"); } }
        //public string ApiKey { get { return "AIzaSyA8VMtwwdkth8l8MbJH7QKFBEsMwh9C_vw"; } }

        /// <summary>
        /// Gets the access token expires on UTC.
        /// </summary>
        /// <value>The access token expires on UTC.</value>
        public DateTime AccessTokenExpiresOnUtc { get; private set; }

        /// <summary>
        /// Gets the OAuth user information, such as google id, refresh token, etc
        /// </summary>
        public OAuthUser ProviderUserInfo
        {
            get;
            private set;
        }

        /// <summary>
        /// Returns true if Google Push Notification Sync is enabled, this can be toggled per Franchise
        /// </summary>
        /// <value><c>true</c> if this instance is enabled; otherwise, <c>false</c>.</value>
        public static bool IsEnabled
        {
            get
            {
                return AppConfigManager.GetConfig<bool>("Enabled", "Google", "Synchronize");
            }
        }

        /// <summary>
        /// Gets number of max retry count before stopping
        /// </summary>
        public static int ErrorRetryCount
        {
            get
            {
                int retryCount = AppConfigManager.GetConfig<int>("ErrorRetryCount", "Google", "Synchronize");
                if (retryCount < 0)
                    retryCount = 0;
                return retryCount;
            }
        }

        /// <summary>
        /// Gets the reconnect timeout.
        /// </summary>
        public static int ReconnectTimeout
        {
            get
            {
                return AppConfigManager.GetConfig<int>("ReconnectTimeout", "Google", "Synchronize");
            }
        }

        /// <summary>
        /// Gets the push Time to live time in seconds
        /// </summary>
        public static int Push_TTL
        {
            get
            {
                return AppConfigManager.GetConfig<int>("Push_TTL", "Google", "Synchronize");
            }
        }

        /// <summary>
        /// Gets the notification service URL for google to use for pushing notifications
        /// </summary>
        public static string NotificationServiceUrl
        {
            get
            {
                return AppConfigManager.GetConfig<string>("NotificationServiceUrl", "Google", "Synchronize");
            }
        }

        /// <summary>
        /// Gets the latest access token using a refresh token if any
        /// </summary>
        public string AccessToken
        {
            get
            {
                if (ProviderUserInfo == null || string.IsNullOrEmpty(ProviderUserInfo.ProviderUserId))
                    return null;

                AccessTokenExpiresOnUtc = ProviderUserInfo.TokenLastUpdatedOnUtc.HasValue ? ProviderUserInfo.TokenLastUpdatedOnUtc.Value : (DateTime)ProviderUserInfo.CreatedOnUtc;
                if (ProviderUserInfo.TokenExpiresIn.HasValue)
                    AccessTokenExpiresOnUtc = AccessTokenExpiresOnUtc.AddSeconds(ProviderUserInfo.TokenExpiresIn.Value);

                if (AccessTokenExpiresOnUtc.CompareTo(DateTime.Now) > 0)
                {
                    return ProviderUserInfo.AccessToken;
                }
                else if (!string.IsNullOrEmpty(ProviderUserInfo.RefreshToken))
                {
                    GoogleOAuthClient client = new GoogleOAuthClient(
                     AppConfigManager.GetConfig<string>("ClientId", "GoogleCalendar", "Authentication"),
                     AppConfigManager.GetConfig<string>("ClientSecret", "GoogleCalendar", "Authentication")
                     );

                    //    GoogleOAuthClient client = new GoogleOAuthClient(
                    //        "900569903913-qmdrq2bt4a5th1jbsj1lvhvdkuk6bh52.apps.googleusercontent.com",
                    //"gvVfhk5cf9y-hVTZRFiwUeRQ"
                    //        );

                    short expire = 0;
                    var newToken = client.RefreshAccessToken(ProviderUserInfo.RefreshToken, out expire);

                    if (!string.IsNullOrEmpty(newToken) && expire > 0)
                    {
                        try
                        {
                            using (var db = new CRMContextEx())
                            {
                                var oauth = new OAuthUser
                                {
                                    ProviderLinkId = ProviderUserInfo.ProviderLinkId,
                                    Provider = GoogleManager.ProviderName,
                                    ProviderUserId = ProviderUserInfo.ProviderUserId,
                                    UserId = ProviderUserInfo.UserId
                                };
                                db.OAuthUsers.Attach(oauth);
                                oauth.AccessToken = ProviderUserInfo.AccessToken = newToken;
                                oauth.TokenExpiresIn = ProviderUserInfo.TokenExpiresIn = expire;
                                oauth.TokenLastUpdatedOnUtc = ProviderUserInfo.TokenLastUpdatedOnUtc = DateTime.Now;

                                db.SaveChanges();
                            }
                        }
                        catch { }
                    }
                    return newToken;
                }
                else
                    return null;
            }
        }

        #endregion

        #region ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="GoogleManager"/> class.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        public GoogleManager(string userId)
        {
            var UserId = Util.CastObject<Guid>(userId);
            if (UserId != Guid.Empty)
            {
                ProviderUserInfo = CRMDBContext.OAuthUsers.FirstOrDefault(f => f.UserId == UserId && f.Provider == GoogleManager.ProviderName && f.IsActive == true);
                CRMDBContext.Entry(ProviderUserInfo).State = EntityState.Detached;
            }

            Initialize();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GoogleManager"/> class.
        /// </summary>
        public GoogleManager(OAuthUser userInfo)
        {
            ProviderUserInfo = userInfo;

            Initialize();
        }

        #endregion

        #region Private Methods

        private void Initialize()
        {
            if (ProviderUserInfo != null && !string.IsNullOrEmpty(ProviderUserInfo.ProviderUserId))
            {
                //need to run as admin for elevated permission
                var admin = CacheManager.UserCollection.FirstOrDefault(f => f.UserName.Equals("admin", StringComparison.InvariantCultureIgnoreCase));
                var curUser = CacheManager.UserCollection.FirstOrDefault(f => f.UserId == ProviderUserInfo.UserId);

                ProviderUserInfo.User = curUser;
                CalendarMgr = new CalendarManager(admin, curUser);

                Tracker = CRMDBContext.Sync_Tracker.FirstOrDefault(f => f.ProviderName == GoogleManager.ProviderName && f.UserId == curUser.UserId);

                if (Tracker == null)
                {
                    Tracker = new Sync_Tracker { ProviderName = GoogleManager.ProviderName, UserId = ProviderUserInfo.UserId };
                    CRMDBContext.Sync_Tracker.Add(Tracker);
                    CRMDBContext.SaveChanges();
                }
            }

            foreach (var apt in Enum.GetValues(typeof(AppointmentTypeEnumTP)))
            {
                AppointmentTypeEnumStrings.Add(apt.ToString().ToLower());
            }
        }

        private static EntityConnectionStringBuilder EntityCnxStringBuilder = new EntityConnectionStringBuilder(System.Configuration.ConfigurationManager.ConnectionStrings["CRMContext"].ConnectionString);

        /// <summary>
        /// Helper method to perform http posts
        /// </summary>
        /// <param name="response">Raw response from the post</param>
        /// <param name="url">Post URL</param>
        /// <param name="postData">Post data</param>
        /// <param name="contentType">Http content type to use for post</param>
        /// <param name="additionalHeaders">Additional headers to append to post request</param>
        private GoogleHttpResponse PostRequest(out string response,
            string url,
            string postData = null,
            ResponseTypeEnum contentType = ResponseTypeEnum.Form,
            Dictionary<string, string> additionalHeaders = null)
        {
            return SendRequest(out response, url, HttpMethod.POST, postData, contentType, additionalHeaders);
        }

        /// <summary>
        /// Helper method to perform http requests.
        /// </summary>
        /// <param name="response">Raw response from the post</param>
        /// <param name="url">Post URL</param>
        /// <param name="postData">Post data</param>
        /// <param name="requestMethod">Request method, GET, POST, etc.</param>
        /// <param name="contentType">Http content type to use for post</param>
        /// <param name="additionalHeaders">Additional headers to append to post request</param>
        private GoogleHttpResponse SendRequest(out string response,
            string url,
            HttpMethod requestMethod = HttpMethod.GET,
            string postData = null,
            ResponseTypeEnum contentType = ResponseTypeEnum.Form,
            Dictionary<string, string> additionalHeaders = null)
        {
            response = "";

            var status = new GoogleHttpResponse();

            WebRequest webRequest = WebRequest.Create(url);
            webRequest.Method = requestMethod.ToString();

            if (!string.IsNullOrEmpty(AccessToken))
                webRequest.Headers.Add("Authorization", string.Format("Bearer {0}", AccessToken));
            else
            {
                status.message = Unauthorized;
                return status;
            }


            if (additionalHeaders != null)
            {
                foreach (var item in additionalHeaders)
                {
                    webRequest.Headers.Add(item.Key, item.Value);
                }
            }

            if (requestMethod == HttpMethod.POST || requestMethod == HttpMethod.PUT)
            {
                webRequest.ContentType = contentType.Description();

                if (!string.IsNullOrEmpty(postData))
                {
                    webRequest.ContentLength = postData.Length;

                    using (Stream requestStream = webRequest.GetRequestStream())
                    {
                        StreamWriter sw = new StreamWriter(requestStream);
                        sw.Write(postData);
                        sw.Flush();
                        sw.Close();
                    }
                }
            }

            try
            {
                // Process the response
                using (WebResponse webResponse = webRequest.GetResponse())
                {
                    using (Stream responseStream = webResponse.GetResponseStream())
                    {
                        StreamReader streamReader = new StreamReader(responseStream);
                        response = streamReader.ReadToEnd();
                    }
                    status.message = Success;
                }
            }
            catch (WebException web)
            {
                //parse the response from google for the real error message
                if (web.Response != null)
                {
                    using (var errRes = (HttpWebResponse)web.Response)
                    {
                        using (var reader = new StreamReader(errRes.GetResponseStream()))
                        {
                            var body = reader.ReadToEnd();
                            var errObj = JsonConvert.DeserializeObject<dynamic>(body);
                            if (errObj != null)
                            {
                                status.code = (int)errObj.error.code.Value;
                                status.message = errObj.error.message.Value;
                            }
                            else
                                status.message = errRes.StatusCode.ToString();
                        }
                    }
                }
                else
                    status.message = web.Message;
            }

            return status;
        }

        /// <summary>
        /// Converts a google event to CRM calendar
        /// </summary>
        /// <param name="evtData">Source event</param>
        /// <param name="defReminders">Default reminders to apply if event doesn't contain one</param>        
        private CalendarVM ConvertToCalendar(Event evtData, IList<Google.Apis.Calendar.v3.Data.EventReminder> defReminders)
        {
            DateTimeOffset startOffset, endOffset, createdOnOffset, lastUpdatedOffset;
            bool isAllDay = false;
            if (!string.IsNullOrEmpty(evtData.Start.Date))
            {
                isAllDay = true;
                startOffset = DateTimeOffset.Parse(evtData.Start.Date, null, DateTimeStyles.RoundtripKind);
                endOffset = DateTimeOffset.Parse(evtData.End.Date, null, DateTimeStyles.RoundtripKind);
            }
            else
            {
                startOffset = DateTimeOffset.Parse(evtData.Start.DateTimeRaw, null, DateTimeStyles.RoundtripKind);
                endOffset = DateTimeOffset.Parse(evtData.End.DateTimeRaw, null, DateTimeStyles.RoundtripKind);
            }
            createdOnOffset = DateTimeOffset.Parse(evtData.CreatedRaw, null, DateTimeStyles.RoundtripKind);
            lastUpdatedOffset = DateTimeOffset.Parse(evtData.UpdatedRaw, null, DateTimeStyles.RoundtripKind);
            lastUpdatedOffset.AddMilliseconds(-lastUpdatedOffset.Millisecond); //clear millisecond so we can compare date up to second

            AppointmentTypeEnumTP apttype = AppointmentTypeEnumTP.Appointment;
            if (evtData.ExtendedProperties != null && evtData.ExtendedProperties.Shared != null)
            {
                try
                {
                    var type = evtData.ExtendedProperties.Shared.FirstOrDefault(f => f.Key.Equals("AppointmentTypeEnum", StringComparison.InvariantCultureIgnoreCase));
                    Enum.TryParse(type.Value, out apttype);
                }
                catch { }
            }

            var convertedCalendar = new CalendarVM
            {
                Subject = evtData.Summary,
                Message = evtData.Description,
                StartDate = startOffset.LocalDateTime,
                EndDate = endOffset.LocalDateTime,
                AptTypeEnum = apttype,
                CreatedOnUtc = createdOnOffset.UtcDateTime,
                IsAllDay = isAllDay,
                LastUpdatedUtc = lastUpdatedOffset.UtcDateTime,
                //LastUpdatedByPersonId = _userToSync.PersonId, //hard to tell no info is given                
                FranchiseId = CalendarMgr.User.FranchiseId.Value,
                IsPrivate = evtData.Visibility == "private",
                IsDeleted = evtData.Status == "cancelled"
            };
            if (evtData.Organizer != null)
            {
                if (evtData.Organizer.Email.Equals(CalendarMgr.User.Email, StringComparison.InvariantCultureIgnoreCase))
                {
                    convertedCalendar.CreatedByPersonId = CalendarMgr.User.PersonId;
                    convertedCalendar.OrganizerPersonId = CalendarMgr.User.PersonId;
                }
                else
                {
                    convertedCalendar.OrganizerEmail = evtData.Organizer.Email;
                    convertedCalendar.OrganizerName = evtData.Organizer.DisplayName;
                }
            }

            if (evtData.Reminders != null)
            {
                bool useDefaultReminders = false;
                if (evtData.Reminders.UseDefault.HasValue)
                    useDefaultReminders = evtData.Reminders.UseDefault.Value;

                short minute = 0;
                Google.Apis.Calendar.v3.Data.EventReminder rem = null;
                if (useDefaultReminders)
                {
                    rem = defReminders.FirstOrDefault(f => f.Method == "popup");
                }
                else if (evtData.Reminders.Overrides != null)
                {
                    rem = evtData.Reminders.Overrides.FirstOrDefault(f => f.Method == "popup");
                }

                if (rem != null && rem.Minutes.HasValue)
                    minute = (short)rem.Minutes.Value;

                if (minute > 0)
                {
                    convertedCalendar.ReminderMinute = minute;
                    convertedCalendar.FirstRemindDate = startOffset.AddMinutes(-minute);
                    //default to all? //TODO
                    convertedCalendar.RemindMethodEnum = RemindMethodEnum.Popup | RemindMethodEnum.Email | RemindMethodEnum.SMS;
                }
            }

            convertedCalendar.Attendees = new List<EventToPerson>();
            convertedCalendar.Attendees.Add(new EventToPerson
            {
                PersonEmail = evtData.Organizer.Email,
                PersonName = evtData.Organizer.DisplayName,
                PersonId = convertedCalendar.OrganizerPersonId,
                RemindDate = convertedCalendar.FirstRemindDate,
                RemindMethodEnum = RemindMethodEnum.Popup | RemindMethodEnum.Email | RemindMethodEnum.SMS
            });

            if (evtData.Attendees != null && evtData.Attendees.Count > 0)
            {
                //get get list of attendees matched to our list of user collection and exclude the organizer
                var attendeeToAdd = (from p in evtData.Attendees
                                     join lj in CacheManager.UserCollection.Where(w => w.FranchiseId == CalendarMgr.User.FranchiseId) on p.Email equals lj.Email into lftjoin
                                     from u in lftjoin.DefaultIfEmpty()
                                     where evtData.Organizer == null || p.Email != evtData.Organizer.Email
                                     select new
                                     {
                                         PersonId = u != null ? (int?)u.PersonId : null,
                                         PersonEmail = u != null ? (u.Email ?? p.Email) : p.Email,
                                         PersonName = u != null ? (u.Person.FullName ?? p.DisplayName) : p.DisplayName
                                     }).ToList();

                foreach (var atndee in attendeeToAdd)
                {
                    convertedCalendar.Attendees.Add(new EventToPerson
                    {
                        PersonEmail = atndee.PersonEmail,
                        PersonName = atndee.PersonName,
                        RemindMethodEnum = RemindMethodEnum.Popup | RemindMethodEnum.Email | RemindMethodEnum.SMS,
                        RemindDate = convertedCalendar.FirstRemindDate,
                        PersonId = atndee.PersonId
                    });
                }
            }

            //recurring master refer to Page #40 of http://www.ietf.org/rfc/rfc2445 for more rules
            if (evtData.Recurrence != null && evtData.Recurrence.Count > 0)
            {
                var ruleStr = evtData.Recurrence.FirstOrDefault(f => f.StartsWith("RRULE"));
                var recurRulesArray = ruleStr.Replace("RRULE:", "").Split(';');

                var pattern = RecurringPatternEnum.Daily;
                int interval = 1;
                int? endsAfterXOcc = null;
                short? dayOfMonth = null;
                DateTimeOffset? endsOn = null;
                DayOfWeekEnum? dowCollection = null;
                DayOfWeekIndexEnum? dowIndex = null;
                foreach (var rule in recurRulesArray)
                {
                    var keyvalue = rule.Split('=');
                    if (keyvalue[0] == "FREQ")
                    {
                        switch (keyvalue[1])
                        {
                            case "WEEKLY": pattern = RecurringPatternEnum.Weekly; break;
                            case "MONTHLY":
                                dayOfMonth = (short)startOffset.Day;
                                pattern = RecurringPatternEnum.Monthly; break;
                            case "YEARLY":
                                dayOfMonth = (short)startOffset.Day;
                                pattern = RecurringPatternEnum.Yearly; break;
                            default: break;
                        }
                    }
                    else if (keyvalue[0] == "COUNT")
                    {
                        endsAfterXOcc = Util.CastObject<int?>(keyvalue[1]);
                    }
                    else if (keyvalue[0] == "UNTIL")
                    {
                        var toIsoStr = DateRegex.Replace(keyvalue[1], "$1-$2-$3T$4:$5:$6"); //Z is included in google recurring time 
                        endsOn = DateTimeOffset.Parse(toIsoStr, null, DateTimeStyles.RoundtripKind);
                        //convert ends on to use the end time of the recurring event and with offset hours
                        endsOn = new DateTimeOffset(endsOn.Value.UtcDateTime.Date.Add(endOffset.TimeOfDay).Ticks, endOffset.Offset);
                    }
                    else if (keyvalue[0] == "INTERVAL")
                    {
                        int.TryParse(keyvalue[1], out interval);
                    }
                    else if (keyvalue[0] == "BYDAY")
                    {
                        var dows = keyvalue[1].Split(',');
                        var firstDow = dows.FirstOrDefault();
                        Regex ByDayRegex = new Regex(@"(?<index>[-|+]?\d{0,2})(?<dow>[a-z]{2})", RegexOptions.IgnoreCase);
                        var match = ByDayRegex.Match(firstDow);
                        if (match.Success)
                        {
                            int indexNum = 0;
                            if (match.Groups["index"] != null && int.TryParse(match.Groups["index"].Value, out indexNum))
                            {
                                if (indexNum < 0) //-1 is last day of week
                                    dowIndex = DayOfWeekIndexEnum.Last;
                                else if (indexNum > 0)
                                    dowIndex = (DayOfWeekIndexEnum)indexNum;

                                //clear day of month since we're using relative day of week instead of day
                                dayOfMonth = null;
                            }
                            if (match.Groups["dow"] != null && !string.IsNullOrEmpty(match.Groups["dow"].Value))
                                dowCollection = ConvertDayOfWeek(match.Groups["dow"].Value);

                            if (dows.Length > 1)
                            {
                                //only weekly will have more than 1 dows to loop and they don't have numbers in front of the day text so dont need to strip them
                                for (int i = 1; i < dows.Length; i++)
                                {
                                    dowCollection = dowCollection | ConvertDayOfWeek(dows[i]);
                                }
                            }
                        }
                    }
                }
                convertedCalendar.EventRecurring = new EventRecurring
                {
                    CreatedByPersonId = convertedCalendar.CreatedByPersonId.HasValue ? convertedCalendar.CreatedByPersonId.Value : CalendarMgr.User.PersonId,
                    CreatedOnUtc = createdOnOffset.UtcDateTime,
                    EndsAfterXOccurrences = endsAfterXOcc,
                    EndsOn = endsOn,
                    StartDate = startOffset,
                    EndDate = endOffset,
                    PatternEnum = pattern,
                    DayOfWeekIndex = dowIndex,
                    DayOfWeekEnum = dowCollection,
                    RecursEvery = interval,
                    DayOfMonth = dayOfMonth
                };
            }

            return convertedCalendar;
        }

        /// <summary>
        /// Converts google event day of week string to DayOfWeekEnum
        /// </summary>
        /// <param name="googDoW">google day of week string</param>        
        private DayOfWeekEnum ConvertDayOfWeek(string googDoW)
        {
            switch (googDoW)
            {
                case "MO": return DayOfWeekEnum.Monday;
                case "TU": return DayOfWeekEnum.Tuesday;
                case "WE": return DayOfWeekEnum.Wednesday;
                case "TH": return DayOfWeekEnum.Thursday;
                case "FR": return DayOfWeekEnum.Friday;
                case "SA": return DayOfWeekEnum.Saturday;
                case "SU": return DayOfWeekEnum.Sunday;
                default: return DayOfWeekEnum.Sunday;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Gets the event list for current google user
        /// </summary>
        /// <param name="lastModDate">The last mod date.</param>
        /// <param name="alwaysIncludeEmail">if set to <c>true</c> [always include email].</param>
        /// <param name="showDeleted">if set to <c>true</c> [show deleted].</param>
        /// <returns>Events.</returns>
        public Events GetEventList(DateTime? lastModDate = null, bool alwaysIncludeEmail = true, bool showDeleted = false)
        {
            if (ProviderUserInfo == null || string.IsNullOrEmpty(ProviderUserInfo.ProviderUserId))
                return null;

            Dictionary<string, string> paramDict = new Dictionary<string, string>();
            paramDict.Add("alwaysIncludeEmail", alwaysIncludeEmail.ToString().ToLower());
            paramDict.Add("showDeleted", showDeleted.ToString().ToLower());
            if (lastModDate.HasValue)
                paramDict.Add("updatedMin", lastModDate.ToISOString());
            paramDict.Add("key", ApiKey);

            string response = null;
            var url = new Uri(CalendarBaseUri, string.Format("{0}/events", ProviderUserInfo.User.Email));
            var Query = string.Join("&", paramDict.Select(s => string.Format("{0}={1}", Uri.EscapeDataString(s.Key), Uri.EscapeDataString(s.Value))));

            var status = SendRequest(out response, url.ToString() + "?" + Query);
            if (status.message == Success && !string.IsNullOrEmpty(response))
            {
                return Util.JSONDeserialize<Events>(response);
            }
            else
                return null;
        }

        /// <summary>
        /// Sends a watch notification channel to google
        /// </summary>
        /// <param name="receivingUrl">The Url google sends notifications to</param>
        /// <returns>Status</returns>
        public string RequestNotificationChannel(string receivingUrl)
        {
            if (ProviderUserInfo == null || string.IsNullOrEmpty(ProviderUserInfo.ProviderUserId) || string.IsNullOrEmpty(receivingUrl))
                return DataCannotBeNullOrEmpty;

            if (!ProviderUserInfo.SyncStartsOnUtc.HasValue || ProviderUserInfo.SyncStartsOnUtc.Value > DateTime.Now)
                return SyncNotEnabled;

            if (Tracker.IsConnected && Tracker.ExpiresOnUtc.HasValue && Tracker.ExpiresOnUtc.Value > DateTime.Now)
                return "Action cancelled, already connected and expiration date is still valid";

            var url = new Uri(CalendarBaseUri, string.Format("{0}/events/watch?key={1}", ProviderUserInfo.User.Email, ApiKey));

            Channel channelParams = new Channel();
            channelParams.Id = Guid.NewGuid().ToString().ToLower();
            channelParams.Token = ProviderUserInfo.UserId.ToString().ToLower();
            channelParams.Type = "web_hook";
            channelParams.Address = receivingUrl;
            channelParams.Params.Add("ttl", Push_TTL.ToString());

            string response = null;

            var httpstatus = PostRequest(out response, url.ToString(), Util.JSONSerialize(channelParams), ResponseTypeEnum.Json);

            if (httpstatus.message == Success && !string.IsNullOrEmpty(response))
            {
                var channel = JsonConvert.DeserializeObject<Channel>(response);

                Tracker.IsConnected = true;
                Tracker.ChannelId = channel.Id;
                Tracker.ResourceId = channel.ResourceId;
                Tracker.ResourceExtraInfo = channel.ResourceUri;
                Tracker.LastSyncDateUtc = DateTime.Now;
                Tracker.LastErrorMessage = null;
                Tracker.LastErrorDateUtc = null;
                Tracker.ErrorCount = 0;

                long timestamp = Util.CastObject<long>(channel.Expiration);
                if (timestamp > 0)
                {
                    Tracker.ExpiresOnUtc = Util.ConvertUnixTimestamp(timestamp);
                }

            }
            else if (!string.IsNullOrEmpty(httpstatus.message) && httpstatus.message.ToLower().Contains("not unique")) //already subscribed
            {
                Tracker.IsConnected = true;
                httpstatus.message = Success;
            }
            else
            {
                Tracker.LastErrorDateUtc = DateTime.Now;
                Tracker.LastErrorMessage = httpstatus.message;
                Tracker.ErrorCount++;
            }

            try
            {
                CRMDBContext.SaveChanges();

                return httpstatus.message;
            }
            catch (Exception ex)
            {
                ApiEventLogger.LogEvent(ex);
                return ex.Message;
            }
        }

        /// <summary>
        /// This will try to sync push noficiation requests, based on CRM settings
        /// </summary>
        public static string SyncPushNotificationRequests()
        {
            if (!GoogleManager.IsEnabled)
                return "Google Sync is not enabled";

            try
            {
                using (var db = new Data.CRMContextEx())
                {
                    //get any user that is active, with sync start date less than now, is either not already connected or the connection has already expired
                    var linq = (from u in db.Users
                                from oauth in u.OAuthUsers.Where(w => w.Provider == GoogleManager.ProviderName)
                                from t in u.Sync_Trackers.Where(w => w.ProviderName == GoogleManager.ProviderName).DefaultIfEmpty()
                                where oauth != null && oauth.IsActive && !u.IsDeleted && oauth.SyncStartsOnUtc.HasValue && oauth.SyncStartsOnUtc.Value < DateTime.Now &&
                                    (t == null || (t.ErrorCount < GoogleManager.ErrorRetryCount && (t.IsConnected == false || t.ExpiresOnUtc < DateTime.Now)))
                                select oauth);

                    foreach (var oauth in linq.ToList())
                    {
                        try
                        {
                            var mgr = new GoogleManager(oauth);
                            mgr.RequestNotificationChannel(NotificationServiceUrl);
                        }
                        catch (Exception ex)
                        {
                            ApiEventLogger.LogEvent(ex);
                        }
                    }

                    return Success;
                }
            }
            catch (Exception ex)
            {
                return ApiEventLogger.LogEvent(ex);
            }
        }

        public static string PushUp()
        {
            using (var db = new CRMContextEx())
            {
                try
                {
                    IEnumerable<HFC.CRM.DTO.Calandar.CalendarToSynch> _calToPush = Enumerable.Empty<CRM.DTO.Calandar.CalendarToSynch>();
                    IEnumerable<HFC.CRM.DTO.Calandar.CalendarToSynch> _acu = Enumerable.Empty<CRM.DTO.Calandar.CalendarToSynch>();

                    using (SqlConnection conn = new SqlConnection(EntityCnxStringBuilder.ProviderConnectionString))
                    {
                        conn.Open();
                        try
                        {
                            _calToPush = conn.Query<HFC.CRM.DTO.Calandar.CalendarToSynch>("[dbo].[getGoogleCalendarToPush]").ToList();
                            _acu = conn.Query<HFC.CRM.DTO.Calandar.CalendarToSynch>("[dbo].[getGoogleAssignedChangedUsers]").ToList();
                        }
                        catch (Exception ex)
                        {
                            return ApiEventLogger.LogEvent(ex);
                        }
                        finally
                        {
                            conn.Close();
                        }

                    }

                    var calToPush = _calToPush.ToList();
                    var assignedChangedUsers = _acu.ToList();

                    if (calToPush != null && calToPush.Count > 0)
                    {
                        foreach (var cal in calToPush.GroupBy(g => g.UserID))
                        {
                            GoogleManager mgr = new GoogleManager(cal.Key.ToString());
                            foreach (var user in cal)
                            {
                                Thread.Sleep(200);
                                mgr.SyncChannel_Up(user.CalendarId);
                            }
                        }
                    }

                    if (assignedChangedUsers != null && assignedChangedUsers.Count > 0)
                    {
                        foreach (var cal in assignedChangedUsers.GroupBy(g => g.UserID))
                        {
                            GoogleManager mgr = new GoogleManager(cal.Key.ToString());
                            foreach (var user in cal)
                            {
                                Thread.Sleep(200);
                                mgr.SyncChannel_UpAssignedChanged(user.CalendarId, user.EventToPeople_HistoryID);
                            }
                        }
                    }
                    return Success;
                }
                catch (Exception ex)
                {
                    return ApiEventLogger.LogEvent(ex);
                }
            }
        }

        /// <summary>
        /// Synchronizes CRM calendar up to google
        /// </summary>
        /// <param name="calendarId">CRM Calendar Id</param>
        public string SyncChannel_Up(int calendarId)
        {
            if (calendarId <= 0)
                return DataCannotBeNullOrEmpty;

            try
            {
                string status = "";

                var calendar = CRMDBContext.Calendars
                                .Include(i => i.Attendees)
                                .Include(i => i.Franchise)
                                .Include(i => i.CalendarSyncs)
                                .Include(i => i.EventRecurring)
                                .FirstOrDefault(f => f.CalendarId == calendarId);

                if (calendar == null)
                    return CalendarDoesNotExist;

                var syncRecord = calendar.CalendarSyncs.FirstOrDefault(f => f.ProviderName == GoogleManager.ProviderName);
                if (syncRecord == null)
                {
                    syncRecord = new CalendarSync
                    {
                        CalendarId = calendarId,
                        ProviderName = GoogleManager.ProviderName
                    };
                    CRMDBContext.CalendarSyncs.Add(syncRecord);
                }

                if (string.IsNullOrEmpty(syncRecord.ProviderUniqueId)) //doesn't have record in exchange
                {
                    //if crm calendar isn't already deleted and its not an occurrence type
                    //google calendar api doesnt support creating new occurrence in a series
                    if (!calendar.IsDeleted && calendar.EventTypeEnum != EventTypeEnum.Occurrence)
                    {
                        string uniqueId = "";
                        status = Create(calendar, out uniqueId);
                        if (!string.IsNullOrEmpty(uniqueId))
                            syncRecord.ProviderUniqueId = uniqueId;
                    }
                }
                else //this is an update or delete
                {
                    if (calendar.IsDeleted || (calendar.IsCancelled.HasValue && calendar.IsCancelled.Value == true))
                    {
                        //this will delete single event, master recurring, and modified occurrences
                        status = Delete(syncRecord.ProviderUniqueId);
                    }
                    else
                    {
                        //check for deleted single occurrences in CRM, but do nothing since google api doesnt have any method to create new instances to delete 
                        //if (calendar.EventRecurring != null && calendar.EventRecurring.SingleOccurrences != null && calendar.EventRecurring.SingleOccurrences.Any(a => a.DeletedOnUtc.HasValue))
                        //{
                        //    
                        //}
                        int revisionSequence = syncRecord.ProviderSequence;
                        status = Update(syncRecord.ProviderUniqueId, calendar, ref revisionSequence);

                        //if update was successful then the revision number will have incremented so we can update
                        syncRecord.ProviderSequence = revisionSequence;
                    }
                }

                if (status == ExchangeManager.Success)
                {
                    syncRecord.ProviderLastModifiedUtc = calendar.LastUpdatedUtc;
                    syncRecord.LastSyncDateUtc = DateTime.Now;
                    syncRecord.LastErrorDateUtc = null;
                    syncRecord.LastErrorMessage = null;
                    syncRecord.ErrorCount = 0;
                }
                else
                {
                    syncRecord.LastErrorDateUtc = DateTime.Now;
                    syncRecord.LastErrorMessage = status;
                    syncRecord.ErrorCount++;
                }

                CRMDBContext.SaveChanges();

                return status;
            }
            catch (Exception ex)
            {
                ApiEventLogger.LogEvent(ex);
                return ex.Message;
            }
        }

        public string SyncChannel_UpAssignedChanged(int calendarId, int historyId)
        {
            if (calendarId <= 0)
                return DataCannotBeNullOrEmpty;

            try
            {
                string status = "";

                var calendar = CRMDBContext.Calendars
                                .Include(i => i.CalendarSyncs)
                                .FirstOrDefault(f => f.CalendarId == calendarId);

                if (calendar == null)
                    return CalendarDoesNotExist;

                var syncRecord = calendar.CalendarSyncs.FirstOrDefault(f => f.ProviderName == GoogleManager.ProviderName);

                if (syncRecord != null && !string.IsNullOrEmpty(syncRecord.ProviderUniqueId))
                {
                    try
                    {
                        status = Delete(syncRecord.ProviderUniqueId);
                    }
                    catch (Exception ex)
                    {
                        status = ExchangeManager.AppointmentDoesNotExist;
                    }

                    if (status == ExchangeManager.Success)
                    {
                        checkForRemoveFromHistory(historyId);
                        syncRecord.ProviderLastModifiedUtc = calendar.LastUpdatedUtc;
                        syncRecord.LastSyncDateUtc = DateTime.Now;
                        syncRecord.LastErrorDateUtc = null;
                        syncRecord.LastErrorMessage = null;
                        syncRecord.ErrorCount = 0;
                    }
                    else
                    {
                        syncRecord.LastErrorDateUtc = DateTime.Now;
                        syncRecord.LastErrorMessage = status;
                        syncRecord.ErrorCount++;
                    }

                    CRMDBContext.SaveChanges();

                }

                return status;
            }
            catch (Exception ex)
            {
                ApiEventLogger.LogEvent(ex);
                return ex.Message;
            }
        }

        private void checkForRemoveFromHistory(int historyId)
        {
            var entity = CRMDBContext.EventToPeople_History.FirstOrDefault(v => v.EventToPeople_HistoryID == historyId);
            if (entity.IsExchangeSynced.HasValue && entity.IsExchangeSynced.Value == true)
            {
                CRMDBContext.EventToPeople_History.Remove(entity);
            }
            else
            {
                entity.IsGoogleSynced = true;
            }
        }

        /// <summary>
        /// Sync google events down to CRM
        /// </summary>
        /// <param name="messageNumber">The message number is like an attempt number but may not be sequential</param>
        public string SyncChannel_Down(int? messageNumber = null)
        {
            string warning;

            if (ProviderUserInfo == null || string.IsNullOrEmpty(ProviderUserInfo.ProviderUserId))
                return DataCannotBeNullOrEmpty;

            if (!ProviderUserInfo.SyncStartsOnUtc.HasValue ||
                ProviderUserInfo.SyncStartsOnUtc.Value > DateTime.Now ||
                !GoogleManager.IsEnabled)
                return SyncNotEnabled;

            if (Tracker == null)
                return PushSubscriptionNotFound;

            bool showDeleted = true;
            DateTime? lastHighestModUtc = Tracker.ProviderLastUpdatedUtc;

            //first time ever to run sync so we can turn off showDeleted, with showDeleted set to true, google will apply a 20 day limit on last modified date.
            //setting it to false will remove that limit for first sync
            if (!lastHighestModUtc.HasValue)
            {
                showDeleted = false;
                lastHighestModUtc = ProviderUserInfo.SyncStartsOnUtc; //set it to the datetime of sync start time 
            }
            else if (DateTime.Now.Subtract(lastHighestModUtc.Value).TotalDays > 20) //make sure date is within 20 days
            {
                lastHighestModUtc = DateTime.Now.AddDays(-19); //go back 19 days so we are not playing with the limit
            }

            var eventList = GetEventList(lastHighestModUtc, showDeleted);
            if (eventList != null && eventList.Items.Count > 0)
            {
                var googIds = eventList.Items.Select(s => s.Id).ToList();
                var calSyncItems = CRMDBContext.CalendarSyncs.Where(w => googIds.Contains(w.ProviderUniqueId)).Include("Calendar").ToList();

                foreach (var evt in eventList.Items)
                {
                    try
                    {
                        string syncstatus = "";

                        var lastUpdatedOffset = DateTimeOffset.Parse(evt.UpdatedRaw, null, DateTimeStyles.RoundtripKind);

                        DateTimeOffset? originalStartDate = null;
                        if (evt.OriginalStartTime != null)
                            originalStartDate = DateTimeOffset.Parse(string.IsNullOrEmpty(evt.OriginalStartTime.Date) ? evt.OriginalStartTime.DateTimeRaw : evt.OriginalStartTime.Date, null, DateTimeStyles.RoundtripKind);

                        CalendarSync syncRecord = null;
                        if (calSyncItems != null)
                            syncRecord = calSyncItems.FirstOrDefault(f => f.ProviderUniqueId == evt.Id);

                        if (evt.Status == "cancelled") //deleted 
                        {
                            //if evt.RecurringEventId is empty then it is a single event or a master recurring since google event will not give a recurring event id for master and single events
                            //must also have a record in crm calendar for us to delete
                            if (syncRecord != null && syncRecord.CalendarId.HasValue && string.IsNullOrEmpty(evt.RecurringEventId))
                            {
                                if (syncRecord.Calendar.IsDeleted) //already deleted so return success
                                    syncstatus = Success;
                                else
                                {
                                    syncstatus = CalendarMgr.DeleteEvent(syncRecord.CalendarId.Value, lastUpdatedOffset.UtcDateTime);
                                    if (syncstatus == CalendarManager.Success)
                                    {
                                        syncRecord.ProviderLastModifiedUtc = lastUpdatedOffset.UtcDateTime;
                                        syncRecord.LastSyncDateUtc = DateTime.Now;
                                        syncRecord.LastErrorDateUtc = null;
                                        syncRecord.LastErrorMessage = null;
                                        syncRecord.ErrorCount = 0;

                                        if (lastUpdatedOffset.UtcDateTime > lastHighestModUtc)
                                            lastHighestModUtc = lastUpdatedOffset.UtcDateTime;
                                    }
                                    else
                                    {
                                        syncRecord.LastErrorDateUtc = DateTime.Now;
                                        syncRecord.ErrorCount++;
                                        syncRecord.LastErrorMessage = syncstatus;
                                    }
                                }
                            }
                            //else it is an occurrence event so delete just this occurrence in series and recurringeventid is the id of the master event
                            else if (originalStartDate.HasValue && !string.IsNullOrEmpty(evt.RecurringEventId))
                            {
                                //get the master recurring calendar id so we can delete just the occurrence
                                var masterCalSync = CRMDBContext.CalendarSyncs.FirstOrDefault(f => f.ProviderUniqueId == evt.RecurringEventId);
                                if (masterCalSync != null && masterCalSync.CalendarId.HasValue)
                                {
                                    syncstatus = CalendarMgr.DeleteEvent(masterCalSync.CalendarId.Value, lastUpdatedOffset.UtcDateTime, originalStartDate.Value);

                                    //update this so we dont push it back up to google
                                    if (syncstatus == CalendarManager.Success)
                                    {
                                        masterCalSync.ProviderLastModifiedUtc = lastUpdatedOffset.UtcDateTime;
                                        masterCalSync.LastSyncDateUtc = DateTime.Now;
                                        masterCalSync.LastErrorDateUtc = null;
                                        masterCalSync.LastErrorMessage = null;
                                        masterCalSync.ErrorCount = 0;

                                        if (lastUpdatedOffset.UtcDateTime > lastHighestModUtc)
                                            lastHighestModUtc = lastUpdatedOffset.UtcDateTime;
                                    }
                                    else
                                    {
                                        masterCalSync.LastErrorDateUtc = DateTime.Now;
                                        masterCalSync.ErrorCount++;
                                        masterCalSync.LastErrorMessage = syncstatus;
                                    }
                                }
                                //else nothing to delete so do nothing                                
                            }
                        }
                        else if (evt.Status == "confirmed" || evt.Status == "tentative") //either an update or new record
                        {
                            if (syncRecord == null)
                            {
                                syncRecord = new CalendarSync
                                {
                                    ProviderUniqueId = evt.Id,
                                    ProviderName = GoogleManager.ProviderName
                                };
                                CRMDBContext.CalendarSyncs.Add(syncRecord);
                            }

                            var converted = ConvertToCalendar(evt, eventList.DefaultReminders);
                            if (!syncRecord.CalendarId.HasValue) //doesn't exist in our system so we can create it
                            {
                                //calendar doesnt exist in crm so we can use whatever current sequence google has
                                if (evt.Sequence.HasValue)
                                {
                                    converted.RevisionSequence = evt.Sequence.Value;
                                }

                                int calId = 0;
                                // this is a single occurrence so delete any event recurring setup and just use same recurringeventid
                                if (!string.IsNullOrEmpty(evt.RecurringEventId))
                                {
                                    converted.EventRecurring = null;

                                    var masterCalRecurId = CRMDBContext.CalendarSyncs.Where(f => f.ProviderUniqueId == evt.RecurringEventId).Select(s => s.Calendar.RecurringEventId).FirstOrDefault();
                                    if (masterCalRecurId.HasValue)
                                    {
                                        converted.RecurringEventId = masterCalRecurId.Value;
                                        syncstatus = CalendarMgr.InsertEvent(out calId, converted, out warning);
                                    }
                                    else
                                        syncstatus = "Unable to find master calendar recurring Id";
                                }
                                else
                                    syncstatus = CalendarMgr.InsertEvent(out calId, converted, out warning);

                                if (syncstatus == CalendarManager.Success && calId > 0)
                                    syncRecord.CalendarId = calId;
                            }
                            else if (((DateTime)converted.LastUpdatedUtc).Subtract((DateTime)syncRecord.Calendar.LastUpdatedUtc).TotalSeconds > 1) //google calendar last updated is greater than CRM so update CRM
                            {
                                converted.CalendarId = syncRecord.CalendarId.Value;
                                //since we're not storing lead id and number information in exchange we need to set it here so the Calendar Manager doesnt clear out the lead id
                                converted.LeadId = syncRecord.Calendar.LeadId;
                                converted.LeadNumber = syncRecord.Calendar.LeadNumber;
                                converted.RecurringEventId = syncRecord.Calendar.RecurringEventId;
                                converted.RevisionSequence = syncRecord.Calendar.RevisionSequence; //set it to the current revision sequence

                                syncstatus = CalendarMgr.UpdateEvent(converted, out warning, true);
                            }
                            //else do nothing, the other CRM push to Google schedule will handle when CRM last mod is greater than google                        

                            if (syncstatus == CalendarManager.Success)
                            {
                                syncRecord.LastSyncDateUtc = DateTime.Now;
                                syncRecord.ProviderLastModifiedUtc = converted.LastUpdatedUtc;
                                syncRecord.LastErrorDateUtc = null;
                                syncRecord.LastErrorMessage = null;
                                syncRecord.ErrorCount = 0;
                                if (evt.Sequence.HasValue)
                                    syncRecord.ProviderSequence = evt.Sequence.Value;

                                if (converted.LastUpdatedUtc > lastHighestModUtc)
                                    lastHighestModUtc = converted.LastUpdatedUtc;
                            }
                            else if (!string.IsNullOrEmpty(syncstatus))
                            {
                                syncRecord.LastErrorDateUtc = DateTime.Now;
                                syncRecord.LastErrorMessage = syncstatus;
                                syncRecord.ErrorCount++;
                            }
                        }

                        CRMDBContext.SaveChanges();

                    }
                    catch (Exception ex)
                    {
                        ApiEventLogger.LogEvent(ex);
                    }
                }

                Tracker.ProviderLastUpdatedUtc = lastHighestModUtc;
                CRMDBContext.SaveChanges();

                return Success;
            }
            else
                return Success;
        }

        #endregion

        #region CRUD Methods

        /// <summary>
        /// Converts a CRM calendar to google event
        /// </summary>
        /// <param name="model">CRM source calendar</param>
        private Event ConvertToEvent(Data.Calendar model)
        {
            var convertedEvt = new Event
            {
                Description = model.Message,
                Summary = model.Subject,
                Location = model.Location,
                GuestsCanInviteOthers = false
            };

            var timezone = TimeZoneInfo.FindSystemTimeZoneById(model.Franchise.TimezoneCode.Description()).StandardName;

            if (model.IsPrivate.HasValue && model.IsPrivate.Value)
            {
                convertedEvt.Visibility = "private";
            }

            var start = new DateTimeWithZone(model.StartDate.DateTime, TimeZoneInfo.FindSystemTimeZoneById(model.Franchise.TimezoneCode.Description()));
            var end = new DateTimeWithZone(model.EndDate.DateTime, TimeZoneInfo.FindSystemTimeZoneById(model.Franchise.TimezoneCode.Description()));

            if (model.IsAllDay)
            {
                convertedEvt.Start = new EventDateTime { Date = model.StartDate.Date.ToString("yyyy-MM-dd") };
                convertedEvt.End = new EventDateTime { Date = model.EndDate.Date.ToString("yyyy-MM-dd") };
            }
            else
            {
                convertedEvt.Start = new EventDateTime { DateTime = start.UniversalTime, TimeZone = TimeZoneUtils.WindowsToIana(timezone) };
                convertedEvt.End = new EventDateTime { DateTime = end.UniversalTime, TimeZone = TimeZoneUtils.WindowsToIana(timezone) };
            }


            //we're using shared extendedproperties to set appointment type
            convertedEvt.ExtendedProperties = new Event.ExtendedPropertiesData
            {
                Shared = new Dictionary<string, string>()
            };
            convertedEvt.ExtendedProperties.Shared.Add("AppointmentTypeEnum", model.AptTypeEnum.ToString());

            //TODO: set all 3 reminder methods or just popup?
            if (model.ReminderMinute > 0 && model.RemindMethodEnum.HasValue)
            {
                convertedEvt.Reminders = new Event.RemindersData();
                convertedEvt.Reminders.Overrides = new List<EventReminder>();
                convertedEvt.Reminders.UseDefault = false;
                convertedEvt.Reminders.Overrides.Add(new EventReminder
                {
                    Method = RemindMethodEnum.Popup.ToString().ToLower(),
                    Minutes = model.ReminderMinute
                });
            }

            if (model.Attendees != null && model.Attendees.Count > 0)
            {
                //get get list of ppl who has ids
                var attendeeToCheck = (from p in model.Attendees
                                       from u in CacheManager.UserCollection
                                       where p.PersonId.HasValue && u.PersonId == p.PersonId.Value && u.FranchiseId == model.FranchiseId
                                       select new { PersonEmail = u.Email, PersonName = u.Person.FullName }).ToList();

                //now list of ppl who has email instead of Id
                attendeeToCheck = attendeeToCheck.Union(
                    model.Attendees.Where(w => !w.PersonId.HasValue && !string.IsNullOrEmpty(w.PersonEmail))
                        .Select(s => new { s.PersonEmail, s.PersonName })).ToList();

                //exclude organizer in the attendee list so this appointment doesnt get turned into a meeting
                if (model.OrganizerPersonId.HasValue)
                {
                    var organizer = CacheManager.UserCollection.FirstOrDefault(f => f.PersonId == model.OrganizerPersonId);
                    if (organizer != null)
                        attendeeToCheck.Remove(attendeeToCheck.FirstOrDefault(f => f.PersonEmail == organizer.Email));
                }
                if (attendeeToCheck.Count > 0)
                {
                    convertedEvt.Attendees = new List<EventAttendee>();
                    foreach (var attee in attendeeToCheck)
                    {
                        convertedEvt.Attendees.Add(new EventAttendee
                        {
                            Email = attee.PersonEmail,
                            DisplayName = attee.PersonName
                        });
                    }
                }
            }

            if (model.EventRecurring != null && model.EventTypeEnum == EventTypeEnum.Series)
            {
                List<string> ruleList = new List<string>();
                ruleList.Add(string.Format("FREQ={0}", model.EventRecurring.PatternEnum.ToString().ToUpper()));
                if (model.EventRecurring.RecursEvery > 1)
                    ruleList.Add(string.Format("INTERVAL={0}", model.EventRecurring.RecursEvery));
                if (model.EventRecurring.EndsAfterXOccurrences.HasValue && model.EventRecurring.EndsAfterXOccurrences.Value > 0)
                    ruleList.Add(string.Format("COUNT={0}", model.EventRecurring.EndsAfterXOccurrences.Value));
                if (model.EventRecurring.EndsOn.HasValue)
                    ruleList.Add(string.Format("UNTIL={0}", model.EventRecurring.EndsOn.Value.ToString("yyyyMMddThhmmss")));
                if (model.EventRecurring.PatternEnum == RecurringPatternEnum.Weekly && model.EventRecurring.DayOfWeekEnum.HasValue)
                {
                    var str = model.EventRecurring.DayOfWeekEnum.Value.ToList().Select(s => s.ToString().ToUpper().Substring(0, 2));

                    ruleList.Add(string.Format("BYDAY={0}", string.Join(",", str)));
                }
                if ((model.EventRecurring.PatternEnum == RecurringPatternEnum.Monthly ||
                     model.EventRecurring.PatternEnum == RecurringPatternEnum.Yearly) &&
                    model.EventRecurring.DayOfWeekEnum.HasValue && model.EventRecurring.DayOfWeekIndex.HasValue)
                {
                    int num = (int)model.EventRecurring.DayOfWeekIndex.Value;
                    if (model.EventRecurring.DayOfWeekIndex.Value == DayOfWeekIndexEnum.Last)
                        num = -1;

                    ruleList.Add(string.Format("BYDAY={0}{1}", num, model.EventRecurring.DayOfWeekEnum.Value.ToString().ToUpper().Substring(0, 2)));
                }
                convertedEvt.Recurrence = new List<string>();
                convertedEvt.Recurrence.Add("RRULE:" + string.Join(";", ruleList));
            }

            return convertedEvt;
        }

        /// <summary>
        /// Creates a new google event using a CRM calendar object
        /// </summary>
        /// <param name="calendar">CRM Source calendar.</param>
        /// <param name="uniqueId">Google unique event identifier.</param>        
        public string Create(Data.Calendar calendar, out string uniqueId)
        {
            return Create(ConvertToEvent(calendar), out uniqueId);
        }

        /// <summary>
        /// Creates a new google event using a google event object
        /// </summary>
        /// <param name="evt">Google source event</param>
        /// <param name="uniqueId">Google unique event identifier</param>
        /// <returns>System.String.</returns>
        public string Create(Google.Apis.Calendar.v3.Data.Event evt, out string uniqueId)
        {
            uniqueId = "";
            try
            {
                /*//start and end timeZone are required for a recurring event
                if (evt.Recurrence != null && evt.Recurrence.Count > 0 && (string.IsNullOrEmpty(evt.Start.TimeZone) || string.IsNullOrEmpty(evt.End.TimeZone)))
                {
                    var timezone = GetSettings("timezone");
                    if(string.IsNullOrEmpty(evt.Start.TimeZone))
                        evt.Start.TimeZone = timezone;
                    if (string.IsNullOrEmpty(evt.End.TimeZone))
                        evt.End.TimeZone = timezone;
                }*/

                //sendNotification default is false, this is only for alerting attendees of new or updated events, its not for push notification.
                Uri url = new Uri(CalendarBaseUri, string.Format("{0}/events?sendNotifications=true&supportsAttachments=true&key={1}", ProviderUserInfo.User.Email, ApiKey));

                string response;
                var status = PostRequest(out response, url.ToString(), Util.JSONSerialize(evt), ResponseTypeEnum.Json);

                if (status.message == Success && !string.IsNullOrEmpty(response))
                {
                    var newEvt = Util.JSONDeserialize<Event>(response);
                    uniqueId = newEvt.Id;
                    return Success;
                }
                else
                    return status.ToString();
            }
            catch (Exception Ex)
            {
                EventLogger.LogEvent(Ex);
                return Ex.Message;
            }
        }

        /// <summary>
        /// Updates a google event using a CRM calendar object
        /// </summary>
        /// <param name="uniqueId">Unique google event identifier.</param>
        /// <param name="calendar">CRM source calendar</param>
        /// <param name="revisionSequence">Revision sequence</param>
        public string Update(string uniqueId, Data.Calendar calendar, ref int revisionSequence)
        {
            var evt = ConvertToEvent(calendar);
            evt.Id = uniqueId;
            evt.Sequence = revisionSequence;
            return Update(evt, ref revisionSequence);
        }

        /// <summary>
        /// Updates a google event using a google event object
        /// </summary>
        /// <param name="evt">Source event</param>
        /// <param name="revisionSequence">Revision sequence</param>        
        public string Update(Google.Apis.Calendar.v3.Data.Event evt, ref int revisionSequence)
        {
            if (evt == null || string.IsNullOrEmpty(evt.Id))
                return DataCannotBeNullOrEmpty;

            try
            {
                /*//start and end timeZone are required for a recurring event
                if (evt.Recurrence != null && evt.Recurrence.Count > 0 && (string.IsNullOrEmpty(evt.Start.TimeZone) || string.IsNullOrEmpty(evt.End.TimeZone)))
                {
                    var timezone = GetSettings("timezone");
                    if (string.IsNullOrEmpty(evt.Start.TimeZone))
                        evt.Start.TimeZone = timezone;
                    if (string.IsNullOrEmpty(evt.End.TimeZone))
                        evt.End.TimeZone = timezone;
                }*/

                //sendNotification default is false, this is only for alerting attendees of new or updated events, its not for push notification.
                Uri url = new Uri(CalendarBaseUri, string.Format("{0}/events/{1}?key={2}", ProviderUserInfo.User.Email, evt.Id, ApiKey));

                string response;
                var status = SendRequest(out response, url.ToString(), HttpMethod.PUT, Util.JSONSerialize(evt), ResponseTypeEnum.Json);
                if (status.message == Success && !string.IsNullOrEmpty(response))
                {
                    var updatedEvt = JsonConvert.DeserializeObject<Event>(response);
                    if (updatedEvt.Sequence.HasValue)
                        revisionSequence = updatedEvt.Sequence.Value;
                }

                return status.message;
            }
            catch (Exception Ex)
            {
                EventLogger.LogEvent(Ex);
                return Ex.Message;
            }
        }

        /// <summary>
        /// Deletes a google event.  TODO: Delete single occurrence in a recurring series without knowing the event Id first. For example: delete based on occurrence number like in Exchange
        /// </summary>
        /// <param name="googEventId">The goog event identifier.</param>
        /// <returns>System.String.</returns>
        public string Delete(string googEventId)
        {
            if (string.IsNullOrEmpty(googEventId))
                return "Invalid Google event Id";

            Uri url = new Uri(CalendarBaseUri, string.Format("{0}/events/{1}?key={2}", ProviderUserInfo.User.Email, googEventId, ApiKey));

            string response = "";
            var status = SendRequest(out response, url.ToString(), HttpMethod.DELETE);
            return status.message;
        }

        #endregion
    }
}
