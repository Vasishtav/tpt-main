﻿using Dapper;
using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Core.Membership;
using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml.Linq;
using HFC.CRM.DTO;
using HFC.CRM.Managers.DTO;
using HFC.CRM.Managers.Exceptions;
using HFC.CRM.Data.Context;
using Newtonsoft.Json;
using System.Data;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Drawing;
using System.Web.Helpers;
using SaveOptions = System.Xml.Linq.SaveOptions;

namespace HFC.CRM.Managers
{
    /// <summary>
    /// Class LeadsManager.
    /// </summary>
    public class LeadsManager : DataManager<Lead>
    {
        private EmailManager emailMgr;

        public LeadsManager(User user, Franchise franchise) : this(user, user, franchise)
        {
            this.User = user;
            this.Franchise = franchise;
            emailMgr = new EmailManager(user, franchise);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadsManager"/> class.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="franchise">The franchise.</param>
        public LeadsManager(User user, User authorizingUser, Franchise franchise)
        {
            this.User = user;
            this.AuthorizingUser = authorizingUser;
            this.Franchise = franchise;
        }

        private FranchiseManager _franMgr = new FranchiseManager(SessionManager.CurrentUser);
        private StatusChangesManager statusChangesMgr = new StatusChangesManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
        private NoteManager noteMgr = new NoteManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        private EntityConnectionStringBuilder EntityCnxStringBuilder = new EntityConnectionStringBuilder
                (System.Configuration.ConfigurationManager.ConnectionStrings["CRMContext"].ConnectionString);

        public static string ImportLead(Lead model)
        {
            EventLogger.LogEvent(JsonConvert.SerializeObject(model), "Import Lead");

            #region Validations

            if (model == null) return DataCannotBeNullOrEmpty;

            //required fields
            var firstAddress = model.Addresses.FirstOrDefault();
            if (firstAddress == null) return "At least one address record is required";

            if (string.IsNullOrEmpty(firstAddress.ZipCode) || !RegexUtil.IsValidZipOrPostal(firstAddress.ZipCode))
                return "A valid zip code is required";

            if (model.PrimCustomer == null) return "Primary person information is required";
            if (string.IsNullOrEmpty(model.PrimCustomer.FirstName)) return "First name is required";
            if (model.IsNotifyemails && string.IsNullOrEmpty(model.PrimCustomer.PrimaryEmail)) return "Email address is required";

            if (!string.IsNullOrEmpty(model.PrimCustomer.PrimaryEmail) &&
                !RegexUtil.IsValidEmail(model.PrimCustomer.PrimaryEmail)) return "Email address is invalid";

            //optional fields, but must be valid entries
            if (!string.IsNullOrEmpty(model.PrimCustomer.SecondaryEmail) &&
                !RegexUtil.IsValidEmail(model.PrimCustomer.SecondaryEmail)) return "Invalid secondary email address";
            if (!string.IsNullOrEmpty(model.PrimCustomer.HomePhone) &&
                !RegexUtil.IsValidUSPhone(model.PrimCustomer.HomePhone)) return "Invalid home phone number";
            if (!string.IsNullOrEmpty(model.PrimCustomer.CellPhone) &&
                !RegexUtil.IsValidUSPhone(model.PrimCustomer.CellPhone)) return "Invalid cell phone number";
            if (!string.IsNullOrEmpty(model.PrimCustomer.WorkPhone) &&
                !RegexUtil.IsValidUSPhone(model.PrimCustomer.WorkPhone)) return "Invalid work phone number";
            if (!string.IsNullOrEmpty(model.PrimCustomer.FaxPhone) && !RegexUtil.IsValidUSPhone(model.PrimCustomer.FaxPhone))
                return "Invalid fax number";

            if (model.SecCustomer != null && model.SecCustomer.Count != 0)
            {
                foreach (var secPerson in model.SecCustomer)
                {
                    if (string.IsNullOrEmpty(secPerson.FirstName) || string.IsNullOrEmpty(secPerson.LastName))
                        return "First and last name is required for secondary person";
                    if (!string.IsNullOrEmpty(secPerson.HomePhone) &&
                        !RegexUtil.IsValidUSPhone(secPerson.HomePhone))
                        return "Invalid home phone number for secondary person";
                    if (!string.IsNullOrEmpty(secPerson.CellPhone) &&
                        !RegexUtil.IsValidUSPhone(secPerson.CellPhone))
                        return "Invalid cell phone number for secondary person";
                    if (!string.IsNullOrEmpty(secPerson.WorkPhone) &&
                        !RegexUtil.IsValidUSPhone(secPerson.WorkPhone))
                        return "Invalid work phone number for secondary person";
                    if (!string.IsNullOrEmpty(secPerson.FaxPhone) &&
                        !RegexUtil.IsValidUSPhone(secPerson.FaxPhone))
                        return "Invalid fax number for secondary person";
                    if (!string.IsNullOrEmpty(secPerson.PrimaryEmail) &&
                        !RegexUtil.IsValidEmail(secPerson.PrimaryEmail))
                        return "Invalid primary email address for secondary person";
                    if (!string.IsNullOrEmpty(secPerson.SecondaryEmail) &&
                        !RegexUtil.IsValidEmail(secPerson.SecondaryEmail))
                        return "Invalid secondary email address for secondary person";
                }
            }

            #endregion Validations

            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    int leadNumber = 0;
                    model.BrandId = model.Territory.BrandId;
                    //var db = ContextFactory.Current.Context;
                    string CountryCode2Digits = "";
                    if (model.Territory != null && !string.IsNullOrEmpty(model.Territory.Code))
                    {
                        connection.Open();

                        var territory = connection.GetList<Territory>("where  brandId = @brandid and code = @code", new { brandid = model.Territory.BrandId, code = model.Territory.Code }).FirstOrDefault();

                        if (territory != null)
                        {

                            if (model.Addresses.ToList() != null && model.Addresses.ToList().Count > 0 && !string.IsNullOrEmpty(model.Addresses.ToList()[0].CountryCode2Digits) && model.Addresses.ToList()[0].CountryCode2Digits != "")
                                CountryCode2Digits = model.Addresses.ToList()[0].CountryCode2Digits;
                            else
                                CountryCode2Digits = connection.GetList<Address>("WHERE addressid IN (SELECT f.AddressId FROM  CRM.Franchise f INNER JOIN crm.Territories t ON t.FranchiseId = f.FranchiseId WHERE t.code = @code)", new { code = model.Territory.Code }).FirstOrDefault().CountryCode2Digits;

                            string TERRNUM = "", CONTRAC = "";
                            if (model.Addresses != null && model.Addresses.ToList().Count > 0 && CountryCode2Digits.ToLower() == "us")
                            {
                                using (var con = new SqlConnection(ContextFactory.CrmGeoLocatorConnectionString))
                                {
                                    string query = "select * from [gisadmin].[HFCUS_ALL] where ZIP=@ZIP";
                                    model.Addresses.ToList()[0].ZipCode = model.Addresses.ToList()[0].ZipCode.Trim().Substring(0, 5);

                                    var USdata = con.Query<HFCUS_ALL>(query, new { ZIP = model.Addresses.ToList()[0].ZipCode.Trim().Substring(0, 5) }).FirstOrDefault();

                                    if (USdata == null)
                                    {
                                        model.TerritoryId = null;
                                        model.TerritoryType = "Gray Area / Unassigned";
                                    }
                                    else
                                    {
                                        if (model.Territory.BrandId == 1) // Budget Blinds
                                        {
                                            TERRNUM = USdata.BBTERRNUM;
                                            CONTRAC = USdata.BBICONTRAC;
                                            if (CONTRAC != "C")
                                            {
                                                model.TerritoryId = null;
                                                model.TerritoryType = "Gray Area / Unassigned";
                                            }
                                            else
                                            {
                                                model.TerritoryId = territory.TerritoryId;
                                                model.TerritoryType = "Own";
                                            }
                                        }
                                        if (model.Territory.BrandId == 2)  // Tailor Living
                                        {
                                            TERRNUM = USdata.CTTERRNUM;
                                            CONTRAC = USdata.CTCONTRAC;
                                            if (CONTRAC != "C")
                                            {
                                                model.TerritoryId = null;
                                                model.TerritoryType = "Gray Area / Unassigned";
                                            }
                                            else
                                            {
                                                model.TerritoryId = territory.TerritoryId;
                                                model.TerritoryType = "Own";
                                            }
                                        }
                                        if (model.Territory.BrandId == 3)  // Concrete Craft
                                        {
                                            model.TerritoryId = null;
                                            model.TerritoryType = "Not applicable";
                                        }
                                    }
                                }
                            }
                            if (model.Addresses != null && model.Addresses.ToList().Count > 0 && CountryCode2Digits.ToLower() == "ca")
                            {
                                string Zipcode = model.Addresses.ToList()[0].ZipCode;
                                string postalcode = Zipcode;
                                string query = "select * from [gisadmin].[HFCCanada] where ZIP=@ZIP";
                                using (var con = new SqlConnection(ContextFactory.CrmGeoLocatorConnectionString))
                                {
                                    HFCCanada Canadadata = new HFCCanada();
                                    if (postalcode.Length == 6)
                                    {
                                        Canadadata = con.Query<HFCCanada>(query, new { ZIP = Zipcode }).FirstOrDefault();
                                        if (Canadadata == null)
                                        {
                                            postalcode = Zipcode.Substring(0, 3);
                                            Canadadata = con.Query<HFCCanada>(query, new { ZIP = postalcode }).FirstOrDefault();
                                        }
                                    }
                                    else
                                    {
                                        postalcode = Zipcode.Substring(0, 3);
                                        Canadadata = con.Query<HFCCanada>(query, new { ZIP = postalcode }).FirstOrDefault();
                                    }

                                    if (Canadadata == null)
                                    {
                                        model.TerritoryId = null;
                                        model.TerritoryType = "Gray Area / Unassigned";
                                    }
                                    else
                                    {
                                        if (model.Territory.BrandId == 1) // Budget Blinds
                                        {
                                            TERRNUM = Canadadata.BBTERRNUM;
                                            CONTRAC = Canadadata.BBICONTRAC;
                                            if (CONTRAC != "C")
                                            {
                                                model.TerritoryId = null;
                                                model.TerritoryType = "Gray Area / Unassigned";
                                            }
                                            else
                                            {
                                                model.TerritoryId = territory.TerritoryId;
                                                model.TerritoryType = "Own";
                                            }
                                        }
                                        if (model.Territory.BrandId == 2) // Tailor Living
                                        {
                                            TERRNUM = Canadadata.CTTERRNUM;
                                            CONTRAC = Canadadata.CTCONTRAC;
                                            if (CONTRAC != "C")
                                            {
                                                model.TerritoryId = null;
                                                model.TerritoryType = "Gray Area / Unassigned";
                                            }
                                            else
                                            {
                                                model.TerritoryId = territory.TerritoryId;
                                                model.TerritoryType = "Own";
                                            }
                                        }
                                        if (model.Territory.BrandId == 3)  // Concrete Craft
                                        {
                                            model.TerritoryId = null;
                                            model.TerritoryType = "Not applicable";
                                        }
                                    }
                                }
                            }

                            model.FranchiseId = territory.FranchiseId;
                            //model.TerritoryId = territory.TerritoryId;
                            //model.TerritoryType = "Own";

                            if (model.FranchiseId.HasValue)
                            {
                                var lNumber = connection.Query<int>("[CRM].[spLeadNumber_New]", new { FranchiseId = model.FranchiseId.Value }, commandType: CommandType.StoredProcedure).First();

                                if (lNumber != 0)
                                {
                                    leadNumber = lNumber;
                                }
                                if (model.Addresses != null && model.Addresses.ToList()[0] != null && !string.IsNullOrEmpty(model.Addresses.ToList()[0].ZipCode) && !string.IsNullOrEmpty(model.Addresses.ToList()[0].CountryCode2Digits))
                                {
                                    model.TerritoryType = GetTerritoryName(model.Addresses.ToList()[0].ZipCode, model.Addresses.ToList()[0].CountryCode2Digits, model.Territory.BrandId, model.FranchiseId.Value);

                                    var terrid = GetTerritoryId(model.Addresses.ToList()[0].ZipCode, model.Addresses.ToList()[0].CountryCode2Digits, model.Territory.BrandId, model.FranchiseId.Value);
                                    if (!string.IsNullOrEmpty(terrid))
                                    {
                                        model.TerritoryId = Convert.ToInt32(terrid);
                                    }
                                }
                            }
                            foreach (var addr in model.Addresses)
                            {
                                if (string.IsNullOrEmpty(addr.CountryCode2Digits))
                                    addr.CountryCode2Digits = connection.GetList<Address>("WHERE addressid IN (SELECT f.AddressId FROM  CRM.Franchise f INNER JOIN crm.Territories t ON t.FranchiseId = f.FranchiseId WHERE t.code = @code)", new { code = model.Territory.Code }).FirstOrDefault().CountryCode2Digits;
                                if (addr.ZipCode.Length > 5 && addr.CountryCode2Digits == "US")
                                {
                                    addr.ZipCode = addr.ZipCode.Replace(@"-", "");
                                    addr.ZipCode = addr.ZipCode.Insert(5, "-");
                                }
                                addr.AddressId = (int)connection.Insert(addr);
                            }
                        }
                        else
                        {
                            model.FranchiseId = null;
                            model.TerritoryId = null;
                            model.TerritoryType = "Gray Area / Unassigned";
                            foreach (var addr in model.Addresses)
                            {
                                addr.AddressId = (int)connection.Insert(addr);
                            }
                        }
                    }
                    else
                    {
                        model.FranchiseId = null;
                        model.TerritoryId = null;
                        model.TerritoryType = "Gray Area / Unassigned";
                    }

                    //check for duplicate lead with just first, last name and prim email for the assigned franchise
                    //var exists = (from l in db.Leads
                    //             where l.FranchiseId == model.FranchiseId && l.PrimCustomer.FirstName == model.PrimCustomer.FirstName &&
                    //                    l.PrimCustomer.LastName == model.PrimCustomer.LastName && l.PrimCustomer.PrimaryEmail == model.PrimCustomer.PrimaryEmail
                    //             select l.PersonId).Any();
                    //if (exists)
                    //    return "Import failed, possible duplicate lead";

                    model.PrimCustomer.CellPhone = !string.IsNullOrEmpty(model.PrimCustomer.CellPhone) && model.PrimCustomer.CellPhone != "" ? RegexUtil.GetNumbersOnly(model.PrimCustomer.CellPhone) : RegexUtil.GetNumbersOnly(model.PrimCustomer.HomePhone);
                    model.PrimCustomer.HomePhone = RegexUtil.GetNumbersOnly(model.PrimCustomer.HomePhone);
                    model.PrimCustomer.WorkPhone = RegexUtil.GetNumbersOnly(model.PrimCustomer.WorkPhone);
                    model.PrimCustomer.FaxPhone = RegexUtil.GetNumbersOnly(model.PrimCustomer.FaxPhone);
                    if (string.IsNullOrEmpty(model.PrimCustomer.PreferredTFN)) model.PrimCustomer.PreferredTFN = "C";
                    model.PrimCustomer.CreatedOn = DateTime.UtcNow;

                    // Default Receive Promotion Emails is true
                    model.PrimCustomer.IsReceiveEmails = true;
                    var custid = connection.Insert(model.PrimCustomer);
                    model.PersonId = custid;
                    model.CustomerId = custid;

                    model.LeadStatusId = AppConfigManager.NewLeadStatusId;
                    model.LeadGuid = Guid.NewGuid();
                    model.LeadNumber = leadNumber;

                    //prevent user from adding their own info for EF to create on save
                    //model.Franchise = null;
                    //model.Territory = null;
                    //model.EditHistories = null;
                    //model.Calendars = null;
                    //model.Jobs = null;
                    //model.LastUpdatedByPersonId = null;
                    //model.LeadStatus = null;
                    //model.CustomerId = null;
                    //model.ReferringCustomerId = null;
                    //model.SecCustomerId = null;
                    //model.CreatedByPersonId = null;
                    //model.Tasks = null;
                    model.CreatedOnUtc = DateTime.UtcNow;
                    model.LeadId = (int)connection.Insert(model);
                    EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(model), LeadId: model.LeadId);

                    var leadcustQuery = @"INSERT INTO [CRM].[LeadCustomers] ([LeadId] ,[CustomerId] ,[IsPrimaryCustomer]) VALUES (@LeadId ,@CustomerId ,@IsPrimaryCustomer);";
                    if (custid.HasValue && custid != 0)
                    {
                        connection.Query(leadcustQuery, new { LeadId = model.LeadId, CustomerId = custid, IsPrimaryCustomer = 1 });
                    }

                    foreach (var adr in model.Addresses)
                    {
                        var query = @"INSERT INTO CRM.LeadAddresses (LeadId,AddressId,IsPrimaryAddress)  VALUES(@leadid,@addressid,1);";
                        connection.Query(query, new { leadid = model.LeadId, addressid = adr.AddressId });
                    }

                    var src = model.LeadSources.FirstOrDefault();
                    string shquery = "Select * from CRM.SourcesHFC where LMDBSourceID=@LMDBSourceID";
                    var lsMapping = connection.Query<SourcesHFC>(shquery, new { LMDBSourceID = src.SourceId }).FirstOrDefault();

                    bool campaignValid = false;  // maintain flag value default UTM Campaign Code not exist in HFC Campaign Table

                    // Insert Lead Source info based on LMDB Campaing
                    if (model.LeadAdditionalInfo != null && model.LeadAdditionalInfo.Count > 0)
                    {
                        var LeadadditionalInfo = model.LeadAdditionalInfo.Where(x => x.Name == "Campaign").FirstOrDefault();
                        if (LeadadditionalInfo != null)
                        {
                            string queryUTM = @"select sHFC.* from [CRM].[UTMCampaign] utm
                                            join CRM.SourcesHFC sHFC on utm.SourcesHFCId = sHFC.SourcesHFCId
                                            where @UTMCode  like RTRIM(utm.UTMCode)+'%' and utm.IsActive=1";
                            var UTMMapping = connection.Query<SourcesHFC>(queryUTM, new { UTMCode = LeadadditionalInfo.Value }).FirstOrDefault();
                            if (UTMMapping != null)
                            {
                                string LeadType = "";
                                int? LMDBSourceID = null;
                                // Get Lead Type based on LM DB SourceId
                                if (model.LeadSources != null && model.LeadSources.Count > 0)
                                {
                                    if (lsMapping != null)
                                    {
                                        LeadType = lsMapping.LeadType;
                                    }
                                    LMDBSourceID = src.SourceId;
                                }

                                var ls = new LeadSource();
                                ls.LeadId = model.LeadId;
                                ls.IsPrimarySource = true;
                                ls.SourceId = UTMMapping.SourcesTPId;
                                connection.Insert<LeadSource>(ls);

                                model.AllowFranchisetoReassign = UTMMapping.AllowFranchisetoReassign;
                                model.LeadType = LeadType;
                                model.HFCUTMCampaignCode = LeadadditionalInfo.Value;
                                model.LMDBSourceID = LMDBSourceID;
                                model.SourcesHFCId = UTMMapping.SourcesHFCId;
                                connection.Update<Lead>(model);

                                campaignValid = true; // UTM Campaign Code valid in HFC Campaign Table
                            }
                        }
                    }

                    // Insert Lead Source info based on LMDB SourceId
                    if (campaignValid == false && model.LeadSources != null && model.LeadSources.Count > 0)
                    {
                        //var src = model.LeadSources.FirstOrDefault();

                        //string query = "Select * from CRM.SourcesHFC where LMDBSourceID=@LMDBSourceID";
                        //var lsMapping = connection.Query<SourcesHFC>(query, new { LMDBSourceID = src.SourceId }).FirstOrDefault();

                        if (lsMapping == null)
                        {
                            var ls = new LeadSource();
                            ls.LeadId = model.LeadId;
                            ls.IsPrimarySource = true;
                            // TOD: for now, if there is no LMDB source Id in TPT then default it to use Website and allow it to re-assign but still save LMDB sourceid
                            ls.SourceId = 49;
                            connection.Insert<LeadSource>(ls);

                            model.AllowFranchisetoReassign = true;
                            //model.LeadType = lsMapping.LeadType;
                            model.LMDBSourceID = src.SourceId;
                            //model.SourcesHFCId = lsMapping.SourcesHFCId;
                        }
                        else
                        {
                            var ls = new LeadSource();
                            ls.LeadId = model.LeadId;
                            ls.IsPrimarySource = true;
                            ls.SourceId = lsMapping.SourcesTPId;
                            connection.Insert<LeadSource>(ls);

                            model.AllowFranchisetoReassign = lsMapping.AllowFranchisetoReassign;
                            model.LeadType = lsMapping.LeadType;
                            model.LMDBSourceID = lsMapping.LMDBSourceID;
                            model.SourcesHFCId = lsMapping.SourcesHFCId;
                        }
                        //return "Invalid LM DB SourceId";

                        connection.Update<Lead>(model);
                    }

                    if (model.LeadAdditionalInfo != null && model.LeadAdditionalInfo.Count > 0)
                    {
                        var LeadadditionalInfo = model.LeadAdditionalInfo.ToList();
                        for (var i = 0; i < LeadadditionalInfo.Count; i++)
                        {
                            if (LeadadditionalInfo[i].Value != null && !string.IsNullOrEmpty(LeadadditionalInfo[i].Value.Trim()))
                            {
                                connection.Query("INSERT INTO [CRM].[LeadAdditionalInfo] ([LeadId] ,[Name] ,[Value] ,[CreatedOnUtc] ) VALUES (@leadId,@name,@value,@createdon)", new { leadId = model.LeadId, name = LeadadditionalInfo[i].Name, value = LeadadditionalInfo[i].Value, createdon = model.CreatedOnUtc });
                            }
                        }
                    }

                    if (model.LeadAdditionalInfo != null && model.LeadAdditionalInfo.ToList().Count > 0)
                    {
                        var response = AddAdditionalInfo(model.LeadGuid, model.LeadAdditionalInfo.ToList());
                    }
                    // connection.Query("insert into crm.LeadCustomers values(@leadid, @customerId,@isPrimaryCustomer)", new { leadid = model.LeadId, customerId = custid, isPrimaryCustomer = true });
                    if (model.SecCustomer != null && model.SecCustomer.Count() != 0)
                    {
                        foreach (var secPerson in model.SecCustomer)
                        {
                            secPerson.CellPhone = RegexUtil.GetNumbersOnly(secPerson.CellPhone);
                            secPerson.HomePhone = RegexUtil.GetNumbersOnly(secPerson.HomePhone);
                            secPerson.WorkPhone = RegexUtil.GetNumbersOnly(secPerson.WorkPhone);
                            secPerson.FaxPhone = RegexUtil.GetNumbersOnly(secPerson.FaxPhone);
                            if (string.IsNullOrEmpty(secPerson.PreferredTFN)) secPerson.PreferredTFN = "H";
                            secPerson.CreatedOn = DateTime.UtcNow;
                            var secPersonId = (int)connection.Insert(secPerson);
                            connection.Query("insert into crm.LeadCustomers values(@leadid, @customerId,@isPrimaryCustomer)", new { leadid = model.LeadId, customerId = secPersonId, isPrimaryCustomer = false });
                        }
                    }

                    //Commercial and Key account changes
                    ///If source is BBCS then Lead is commercial
                    if (lsMapping != null && lsMapping.ChannelId == 28)
                    {
                        model.IsCommercial = true;
                    }
                    else
                    {
                        model.IsCommercial = false;
                    }

                    if (model.LeadAdditionalInfo != null && model.LeadAdditionalInfo.Count > 0)
                    {
                        var LeadadditionalInfo = model.LeadAdditionalInfo.ToList();
                        for (var i = 0; i < LeadadditionalInfo.Count; i++)
                        {
                            if (!string.IsNullOrEmpty(LeadadditionalInfo[i].Name.Trim()) && LeadadditionalInfo[i].Name.Trim().ToUpper() == "COMPANYNAME")
                            {
                                if (LeadadditionalInfo[i].Value != null &&
                                    !string.IsNullOrEmpty(LeadadditionalInfo[i].Value.Trim()))
                                {
                                    model.KeyAcct = LeadadditionalInfo[i].Value;
                                    model.CommercialTypeId = 2000;
                                    string ppquery = "SELECT * FROM crm.Customer c WHERE c.CustomerId = @CustomerId;";
                                    var cust = connection.Query<CustomerTP>(ppquery, new { CustomerId = model.PersonId }).FirstOrDefault();
                                    cust.CompanyName = LeadadditionalInfo[i].Value;
                                    connection.Update<CustomerTP>(cust);
                                }
                            }
                        }
                    }
                    //to update opt option in CRM.smsOptinginfo && need to check franchise enabling the texting
                    if (!string.IsNullOrEmpty(model.PrimCustomer.TextOptin))
                    {
                        if (model.PrimCustomer.TextOptin == "optin")
                        {
                            model.IsNotifyText = true;
                            string qry = "SELECT EnableTexting FROM crm.Franchise where FranchiseId= @FranchiseId";
                            var enabletext = connection.Query<int>(qry, new { FranchiseId = model.FranchiseId}).FirstOrDefault();
                            if (model.PrimCustomer.CellPhone != null && !string.IsNullOrEmpty(CountryCode2Digits) && enabletext==1)
                            {
                                var CountryCode = CountryCode2Digits;
                                if (model.Territory != null)
                                {
                                    var franchiseID = model.FranchiseId;
                                    var BrandId = model.Territory.BrandId;
                                    CommunicationManager communication_Mgr = new CommunicationManager(null, null);
                                    communication_Mgr.SendOptinMessageforLeadImport(model.PrimCustomer.CellPhone, BrandId, CountryCode, franchiseID);
                                }
                            }
                        }
                    }
                    connection.Update<Lead>(model);
                    //InsertLeadImportHistory(model, Success);
                    //db.SaveChanges();
                    connection.Close();
                    return Success;
                }
            }
            catch (Exception ex)
            {
                InsertLeadImportHistory(model, ex.Message);
                return EventLogger.LogEvent(ex);
            }
        }

        public static string GetTerritoryName(string Zipcode, string Country, int brandId, int franchiseId)
        {
            if (Zipcode.Contains('-'))
            {
                var str = Zipcode.Trim().Split('-');
                if (str.Count() > 0)
                {
                    Zipcode = str[0];
                }
            }
            if (Country.ToLower() == "us")
                Zipcode = Zipcode.Substring(0, 5);
            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var ret = (string)con.ExecuteScalar("select CRM.fnGetTerritory(@BrandId,@Zipcode,@Country,@FranchiseId)", new { brandId, Zipcode, Country, franchiseId });
                return ret;
            }
        }

        public static string GetTerritoryId(string Zipcode, string Country, int BrandId, int franchiseId)
        {
            if (Zipcode.Contains('-'))
            {
                var str = Zipcode.Trim().Split('-');
                if (str.Count() > 0)
                {
                    Zipcode = str[0];
                }
            }

            if (string.Equals(Country, "us", StringComparison.OrdinalIgnoreCase))
                Zipcode = Zipcode.Substring(0, 5);

            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var retId = (string)con.ExecuteScalar("select CRM.fnGetTerritory_Id(@BrandId,@Zipcode,@Country,@FranchiseId)", new { BrandId, Zipcode, Country, franchiseId });
                return retId;
            }
        }

        public static bool InsertLeadImportHistory(Lead model, string message)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var Historyquery = @"INSERT INTO History.LeadImport(LeadId ,LeadGuid ,LeadImportValue,Message)  VALUES(@LeadId ,@LeadGuid ,@LeadImportValue,@Message) ;";
                connection.Query(Historyquery, new { leadid = model.LeadId, LeadGuid = model.LeadGuid, LeadImportValue = JsonConvert.SerializeObject(model), Message = message });
            }

            return true;
        }

        /// <summary>
        /// Adds additional lead information.
        /// </summary>
        /// <param name="leadGuid">The lead unique identifier.</param>
        public static string AddAdditionalInfo(Guid leadGuid, List<LeadAdditionalInfo> infos)
        {
            if (leadGuid == Guid.Empty)
                return "Invalid lead guid";
            if (infos == null || infos.Count == 0)
                return "AdditionalLeadInfo array can not be empty";
            if (infos != null && infos.Count > 25)
                return "AdditionalLeadInfo array can not exceed 25 records";

            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    var lead = connection.GetList<Lead>("where LeadGuid = @leadGuid", new { leadGuid = leadGuid }).FirstOrDefault();

                    if (lead == null) return LeadDoesNotExist;

                    var additionalInfo = connection.GetList<LeadAdditionalInfo>("where LeadId = @leadId", new { leadId = lead.LeadId }).ToList();
                    var matched =
                    (from li in additionalInfo
                     join i in infos on li.Name.ToLower() equals i.Name.ToLower()
                     where !string.IsNullOrEmpty(i.Name)
                     select new { Current = i, Original = li }).
                            ToList();
                    var toAdd = infos.Except(matched.Select(s => s.Current));
                    foreach (var item in matched)
                    {
                        item.Original.Value = item.Current.Value;
                    }
                    foreach (var item in toAdd)
                    {
                        item.CreatedOnUtc = DateTime.UtcNow;
                        item.LeadId = lead.LeadId;
                        connection.Insert(item);
                    }

                    connection.Close();

                    return Success;
                }
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        /// <summary>
        /// Gets a list of Lead data by status Ids and created on date
        /// </summary>
        /// <param name="totalRecords">The total records.</param>
        /// <param name="leadStatusIds">List of status Ids to match</param>
        /// <param name="createdOnUtcStart">The created on UTC start.</param>
        /// <param name="createdOnUtcEnd">The created on UTC end.</param>
        /// <param name="searchFilter">The search filter.</param>
        /// <param name="searchTerm">The search term.</param>
        /// <param name="orderBy">The order by.</param>
        /// <param name="orderByDirection">The order by direction.</param>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="excludeLeadStatusNames"></param>
        /// <returns>List&lt;Lead&gt;.</returns>
        /// <exception cref="System.AccessViolationException">
        /// </exception>

        public List<Lead> GetDuplicateLeads(SearchDuplicateLeadEnum searchType, string searchTerm, Lead dto = null)
        {
            //
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                //var linq = from l in CRMDBContext.Leads.AsNoTracking()
                //where l.FranchiseId == Franchise.FranchiseId && l.IsDeleted == false
                //select l;
                var linq = "select l.*, lc.customerid, c.FirstName, c.LastName from crm.Leads l join crm.leadcustomers lc on lc.LeadId = l.LeadId join crm.Customer c on lc.customerid = c.CustomerId where l.FranchiseId = @franchiseId and lc.isprimarycustomer = 1";
                //var linq = connection.Query<Lead>(query, new { franchiseId = Franchise.FranchiseId });
                string parsedPhone = RegexUtil.GetNumbersOnly(searchTerm);

                switch (searchType)
                {
                    case SearchDuplicateLeadEnum.CellPhone:
                        linq += @"and c.CellPhone = @parsedPhone";
                        break;

                    case SearchDuplicateLeadEnum.HomePhone:
                        linq += @" and c.HomePhone = @parsedPhone";
                        break;

                    case SearchDuplicateLeadEnum.PrimaryEmail:
                        linq += @" and c.HomePhone = @parsedPhone";
                        break;

                    case SearchDuplicateLeadEnum.FaxPhone:
                        linq += @" and c.FaxPhone = @parsedPhone";
                        break;

                    case SearchDuplicateLeadEnum.WorkPhone:
                        linq += @" and c.WorkPhone = @parsedPhone";
                        break;

                    case SearchDuplicateLeadEnum.SecondaryEmail:
                        linq += @" and c.SecondaryEmail = @parsedPhone";
                        break;

                    case SearchDuplicateLeadEnum.All:
                        linq += @" and c.CellPhone = @parsedPhone or";
                        linq += @" c.HomePhone = @parsedPhone or";
                        //linq += @" c.HomePhone = @parsedPhone or ";
                        linq += @" c.FaxPhone  = @parsedPhone or ";
                        linq += @" c.WorkPhone = @parsedPhone or ";
                        linq += @" c.SecondaryEmail = @parsedPhone ";
                        break;

                    default:
                        return new List<Lead>();
                }
                var list = connection.Query<Lead>(linq, new { franchiseId = Franchise.FranchiseId, parsedPhone = parsedPhone }).ToList();

                //var list = connection.GetList<Lead>(linq, new { franchiseId = Franchise.FranchiseId,parsedPhone = parsedPhone }).ToList();

                foreach (var item in list)
                {
                    var query1 = "select a.* from crm.Addresses a inner join CRM.LeadAddresses la on a.AddressId = la.AddressId where la.LeadId = @leadId";
                    item.Addresses = connection.Query<AddressTP>(query1, new { leadId = item.LeadId }).ToList();
                    var query2 = "select c.* from crm.Customer c where c.CustomerId = @customerId";
                    item.PrimCustomer = connection.Query<CustomerTP>(query2, new { customerId = item.CustomerId }).FirstOrDefault();
                    var query3 = "select j.* from crm.Jobs j where j.JobId = @jobid";
                    item.Jobs = connection.Query<Job>(query3, new { jobid = item.LeadId }).ToList();
                }

                connection.Close();
                return list;
                //return linq.ToList();
            }
        }

        public LeadDuplicationResultDTO ValidateLeadDuplication(LeadDuplicationDTO model)
        {
            var status = new LeadDuplicationResultDTO { Result = Success };

            if (IsDuplicateName(model))
            {
                status.ErrorMessage = "Matching Name already exists!" + Environment.NewLine;
            }

            if (IsDuplicateEmail(model))
            {
                status.ErrorMessage += "Matching Email already exists!" + Environment.NewLine;
            }

            if (IsDuplicatePhone(model))
            {
                status.ErrorMessage += "Matching Preferred Phone already exists!" + Environment.NewLine;
            }

            if (IsDuplicateAddress(model))
            {
                status.ErrorMessage += "Matching Address already exists!" + Environment.NewLine;
            }

            if (!string.IsNullOrEmpty(status.ErrorMessage))
            {
                status.ErrorMessage += "Do you wish to Save this Duplicate?" + Environment.NewLine;
                status.Result = "Error";
            }

            return status;
        }

        private bool IsDuplicateName(LeadDuplicationDTO model)
        {
            var result = false;
            // Name match check (bot will be definitely available.
            //c.CustomerId, c.FirstName, c.LastName, l.LeadId
            var query = @"select count(1)
                            from crm.customer c
                            join crm.leadcustomers lc on lc.CustomerId = c.CustomerId
                            join crm.leads l on l.LeadId = lc.LeadId
                            where c.FirstName = @firstName and c.LastName = @lastName
                            and l.FranchiseId = " + SessionManager.CurrentFranchise.FranchiseId;
            if (model.LeadId > 0)
            {
                query += " and l.LeadId <> " + model.LeadId;
            }
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var count = connection.ExecuteScalar(query,
                    new { firstName = model.FirstName, lastName = model.LastName });

                if ((int)count > 0) result = true;
            }

            return result;
        }

        private bool IsDuplicateEmail(LeadDuplicationDTO model)
        {
            var result = false;
            if (string.IsNullOrEmpty(model.Email)) return result;

            var query = @"select count(1)
                            from crm.customer c
                            join crm.leadcustomers lc on lc.CustomerId = c.CustomerId
                            join crm.leads l on l.LeadId = lc.LeadId
                            where c.PrimaryEmail = @email
                            and l.FranchiseId = " + SessionManager.CurrentFranchise.FranchiseId;
            if (model.LeadId > 0)
            {
                query += " and l.LeadId <> " + model.LeadId;
            }
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var count = connection.ExecuteScalar(query,
                    new { email = model.Email });

                if ((int)count > 0) result = true;
            }

            return result;
        }

        private bool IsDuplicatePhone(LeadDuplicationDTO model)
        {
            var result = false;

            var query = @"select count(1)
                            from crm.customer c
                            join crm.leadcustomers lc on lc.CustomerId = c.CustomerId
                            join crm.leads l on l.LeadId = lc.LeadId
                            where c.PreferredTFN = @preferredPhone
                            and l.FranchiseId = " + SessionManager.CurrentFranchise.FranchiseId;

            var pphone = "";
            switch (model.PreferredTFN)
            {
                case "C":
                    pphone = model.CellPhone;
                    query += " and c.CellPhone = @phone";
                    break;

                case "H":
                    pphone = model.HomePhone;
                    query += " and c.HomePhone = @phone";
                    break;

                default:
                    pphone = model.WorkPhone;
                    query += " and c.WorkPhone = @phone";
                    break;
            }

            if (model.LeadId > 0)
            {
                query += " and l.LeadId <> " + model.LeadId;
            }
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var count = connection.ExecuteScalar(query,
                    new { preferredPhone = model.PreferredTFN, phone = pphone });

                if ((int)count > 0) result = true;
            }

            return result;
        }

        private bool IsDuplicateAddress(LeadDuplicationDTO model)
        {
            var result = false;

            // 1)	We should not report duplicate address if no address 1 or address 2 have been entered - Kannan
            // while UAT testing.
            if (string.IsNullOrEmpty(model.Address1))
                return result;

            var query = @"select count(1)
                            from crm.addresses a
                            join crm.leadaddresses la on la.AddressId = a.AddressId
                            join crm.leads l on l.LeadId = la.LeadId
                            where l.FranchiseId = " + SessionManager.CurrentFranchise.FranchiseId;
            query += @" and a.Address1 =@address1 and a.Address2 = @address2
                        and a.City = @city and a.State = @state
                        and a.ZipCode = @zipCode";
            if (model.LeadId > 0)
            {
                query += " and l.LeadId <> " + model.LeadId;
            }
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var count = connection.ExecuteScalar(query,
                    new
                    {
                        address1 = model.Address1,
                        address2 = model.Address2,
                        city = model.City,
                        state = model.State,
                        zipCode = model.ZipCode
                    });

                if ((int)count > 0) result = true;
            }

            return result;
        }

        /// <summary>
        /// Creates the lead.
        /// </summary>
        /// <param name="leadId">The lead identifier.</param>
        /// <param name="data">The data.</param>
        /// <returns>System.String.</returns>
        public string CreateLead(out int leadId, Lead data)
        {
            // Check source and campaign exist or not
            if (data.CampaignId == null && data.SourcesTPId == 0)
            {
                throw new Exception("Primary source was not assigned!");
            }

            var leadStatus = statusChangesMgr.StatusChanges(1, data.LeadId, data.LeadStatusId);
            if (!leadStatus)
                throw new ValidationException("Not a valid Lead Status");

            leadId = 0;
            foreach (var source in data.LeadSources)
            {
                if (source.SourceId == data.SourcesTPId)
                    return LeadSourceDuplicate;
            }

            if (data == null)
                return DataCannotBeNullOrEmpty;

            if (!data.FranchiseId.HasValue || data.FranchiseId <= 0)
                data.FranchiseId = Franchise.FranchiseId;
            else if (data.FranchiseId != Franchise.FranchiseId)
                return "Invalid franchise";

            if (data.LeadStatusId <= 0)
                data.LeadStatusId = AppConfigManager.NewLeadStatusId;
            data.BrandId = SessionManager.CurrentFranchise.BrandId;
            bool validEmail = true, validZip = true;
            if (data.PrimCustomer != null)
            {
                data.PrimCustomer.HomePhone = RegexUtil.GetNumbersOnly(data.PrimCustomer.HomePhone);
                data.PrimCustomer.WorkPhone = RegexUtil.GetNumbersOnly(data.PrimCustomer.WorkPhone);
                data.PrimCustomer.CellPhone = RegexUtil.GetNumbersOnly(data.PrimCustomer.CellPhone);
                data.PrimCustomer.FaxPhone = RegexUtil.GetNumbersOnly(data.PrimCustomer.FaxPhone);

                if (validEmail && !string.IsNullOrEmpty(data.PrimCustomer.PrimaryEmail))
                    validEmail = RegexUtil.IsValidEmail(data.PrimCustomer.PrimaryEmail);
                if (validEmail && !string.IsNullOrEmpty(data.PrimCustomer.SecondaryEmail))
                    validEmail = RegexUtil.IsValidEmail(data.PrimCustomer.SecondaryEmail);
                //convert datetime to universal
                if (data.PrimCustomer.UnsubscribedOn.HasValue &&
                    data.PrimCustomer.UnsubscribedOn.Value.Kind == DateTimeKind.Unspecified)
                    data.PrimCustomer.UnsubscribedOn = data.PrimCustomer.UnsubscribedOn.Value.ToUniversalTime();
            }
            if (data.SecCustomer != null)
            {
                foreach (var person in data.SecCustomer)
                {
                    person.HomePhone = RegexUtil.GetNumbersOnly(person.HomePhone);
                    person.WorkPhone = RegexUtil.GetNumbersOnly(person.WorkPhone);
                    person.CellPhone = RegexUtil.GetNumbersOnly(person.CellPhone);
                    person.FaxPhone = RegexUtil.GetNumbersOnly(person.FaxPhone);
                    if (validEmail && !string.IsNullOrEmpty(person.PrimaryEmail))
                        validEmail = RegexUtil.IsValidEmail(person.PrimaryEmail);
                    if (validEmail && !string.IsNullOrEmpty(person.SecondaryEmail))
                        validEmail = RegexUtil.IsValidEmail(person.SecondaryEmail);
                    // convert datetime to universal
                    if (person.UnsubscribedOn.HasValue &&
                        person.UnsubscribedOn.Value.Kind == DateTimeKind.Unspecified)
                        person.UnsubscribedOn = person.UnsubscribedOn.Value.ToUniversalTime();
                }
            }
            if (data.Addresses != null && data.Addresses.Count > 0)
            {
                foreach (var addr in data.Addresses)
                {
                    if (!string.IsNullOrEmpty(addr.ZipCode))
                        addr.ZipCode = addr.ZipCode.ToUpper();

                    if (string.IsNullOrEmpty(addr.CountryCode2Digits))
                        addr.CountryCode2Digits = Franchise.CountryCode;
                    if (validZip && !string.IsNullOrEmpty(addr.ZipCode))
                        validZip = RegexUtil.IsValidZipOrPostal(addr.ZipCode);
                }
            }

            if (!validEmail || !validZip)
            {
                if (!validEmail)
                    return "Invalid email address";
                else
                    return string.Format("Invalid {0}", Franchise.ZipText);
            }

            try
            {
                // Get Lead Number
                var leadnumber = this.spLeadNumber_New(Franchise.FranchiseId);

                if (leadnumber.HasValue && leadnumber.Value > 0)
                {
                    data.LeadGuid = Guid.NewGuid();
                    data.CreatedOnUtc = DateTime.UtcNow;
                    data.CreatedByPersonId = User.PersonId;
                    data.LeadNumber = leadnumber.Value;
                    //TODO we need to check this code later
                    //data.EditHistories.Add(new EditHistory()
                    //{
                    //    IPAddress = HttpContext.Current.Request.UserHostAddress,
                    //    LoggedByPersonId = User.PersonId,
                    //    HistoryValue = "<Lead operation=\"insert\" />"
                    //});
                    string[] TerritoryDetails = new string[2];
                    if (data.Addresses != null && data.Addresses.Count > 0)
                    {
                        TerritoryDetails = TerritoryManager.GetTerritoryDetails(data.Addresses.FirstOrDefault().ZipCode, data.Addresses.FirstOrDefault().CountryCode2Digits).Split(';');
                    }

                    using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                    {
                        foreach (var item in data.Addresses)
                        {
                            var addressId = connection.Insert(item);
                            item.AddressId = (int)addressId;
                        }
                        data.PrimCustomer.CreatedOn = DateTime.UtcNow;
                        data.PrimCustomer.CreatedBy = User.PersonId;
                        data.CustomerId = connection.Insert(data.PrimCustomer);
                        data.PersonId = data.CustomerId;

                        if ((TerritoryDetails != null && TerritoryDetails[0] != null))
                        {
                            if (TerritoryDetails[0] == "0")
                            {
                                data.TerritoryType = TerritoryDetails[1];
                                data.TerritoryId = null;
                            }
                            else
                            {
                                data.TerritoryType = TerritoryDetails[1];
                                data.TerritoryId = Convert.ToInt32(TerritoryDetails[0]);
                            }
                        }

                        // Campaign is not a required field.
                        if (data.CampaignId != null && data.CampaignId == 0) data.CampaignId = null;

                        data.IndustryId = data.IndustryId > 0 ? data.IndustryId : (int?)null;

                        data.CommercialTypeId = data.CommercialTypeId > 0 ? data.CommercialTypeId : (int?)null;

                        data.RelationshipTypeId = data.RelationshipTypeId > 0 ? data.RelationshipTypeId : (int?)null;

                        data.ImpersonatorPersonId = this.ImpersonatorPersonId;
                        data.EmailSent = false;

                        leadId = (int)connection.Insert(data);
                        //Added for Qualification/Question Answers
                        SessionManager.LeadId = leadId;
                        connection.Query("insert into crm.LeadCustomers values(@leadid, @customerId,@isPrimaryCustomer)", new { leadid = leadId, customerId = data.CustomerId, isPrimaryCustomer = true });
                        if (data.LeadSources != null || data.LeadSources.Count == 0)
                        {
                            foreach (var item in data.LeadSources)
                            {
                                connection.Query("insert into crm.LeadSources (SourceId,LeadId,CreatedOnUtc,IsManuallyAdded,IsPrimarySource) values(@sourceId,@leadId,@createdonutc,@isManuallyAdded,@isPrimarySource)", new { sourceId = item.SourceId, leadId = leadId, createdonutc = DateTime.UtcNow, isManuallyAdded = true, isPrimarySource = false });
                            }
                        }
                        if (data.SourcesTPId > 0)
                        {
                            connection.Query("insert into crm.LeadSources (SourceId,LeadId,CreatedOnUtc,IsManuallyAdded,IsPrimarySource) values(@sourceId,@leadId,@createdonutc,@isManuallyAdded,@isPrimarySource)", new { sourceId = data.SourcesTPId, leadId = leadId, createdonutc = DateTime.UtcNow, isManuallyAdded = true, isPrimarySource = true });
                        }
                        List<int> secCustomerIDlst = new List<int>();
                        if (data.SecCustomer != null)
                        {
                            foreach (var item in data.SecCustomer)
                            {
                                item.CreatedOn = DateTime.UtcNow;
                                var secPersonId = (int)connection.Insert(item);
                                secCustomerIDlst.Add(secPersonId);
                            }
                            foreach (var id in secCustomerIDlst)
                            {
                                connection.Query("insert into crm.LeadCustomers values(@leadid, @customerId,@isPrimaryCustomer)", new { leadid = leadId, customerId = id, isPrimaryCustomer = false });
                            }
                        }

                        foreach (var item in data.Addresses)
                        {
                            connection.Query("insert into crm.LeadAddresses values(@leadid, @addressid, @IsPrimaryAddress)", new { leadid = leadId, addressid = item.AddressId, IsPrimaryAddress = 1 });
                        }

                        //Added for Notes and Attachment
                        //SaveNoteAttachment(leadId, data, connection);

                        //Close the connection
                        connection.Close();

                        // Log the History data
                        EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(data), LeadId: leadId);
                    }
                    return Success;
                }

                return "Unable to generate a lead number";
            }
            catch (ValidationException ex)
            {
                return ex.Message;
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        /// <summary>
        /// Updates the person.
        /// </summary>
        /// <param name="leadId">The lead identifier.</param>
        /// <param name="data">The data.</param>
        /// <returns>System.String.</returns>
        public string UpdatePerson(int leadId, Person data)
        {
            if (leadId <= 0 || data == null)
                return DataCannotBeNullOrEmpty;

            //if (!BasePermission.CanUpdate)
            //    return Unauthorized;

            try
            {
                var leadtoCheck =
                    CRMDBContext.Leads

                        // TODO : need to verify this code.
                        //.Where(
                        //w =>
                        //    w.LeadId == leadId && w.IsDeleted == false && w.FranchiseId == Franchise.FranchiseId &&
                        //    (w.PersonId == data.PersonId
                        //    || w.SecPersonId == data.PersonId))
                        .Select(s => new { Person = s.PrimCustomer }) //s.PersonId == data.PersonId ? s.PrimCustomer : s.SecCustomer})
                        .FirstOrDefault();

                if (leadtoCheck == null)
                    return LeadDoesNotExist;
                if (leadtoCheck.Person == null)
                    return "Person does not exist";

                //check for duplicate email
                if ((leadtoCheck.Person.PrimaryEmail != data.PrimaryEmail && !string.IsNullOrEmpty(data.PrimaryEmail)) ||
                    (leadtoCheck.Person.SecondaryEmail != data.SecondaryEmail &&
                     !string.IsNullOrEmpty(data.SecondaryEmail)))
                {
                    var exists =
                        CRMDBContext.Database.SqlQuery<bool>("SELECT [CRM].[fnsHasDuplicateEmail](@p0, @p1, @p2, @p3)",
                            Franchise.FranchiseId, data.PrimaryEmail, data.SecondaryEmail, data.PersonId)
                            .FirstOrDefault();
                    if (exists)
                    {
                        return "Duplicate email address found, unable to update person";
                    }
                }

                var lead = new Lead { LeadId = leadId };
                CRMDBContext.Leads.Attach(lead);

                leadtoCheck.Person.FirstName = data.FirstName;
                leadtoCheck.Person.LastName = data.LastName;
                leadtoCheck.Person.PrimaryEmail = data.PrimaryEmail;
                leadtoCheck.Person.MI = data.MI;
                if (leadtoCheck.Person.PrimaryEmail != data.SecondaryEmail)
                    leadtoCheck.Person.SecondaryEmail = data.SecondaryEmail;
                leadtoCheck.Person.HomePhone = RegexUtil.GetNumbersOnly(data.HomePhone);
                leadtoCheck.Person.CellPhone = RegexUtil.GetNumbersOnly(data.CellPhone);
                leadtoCheck.Person.WorkPhone = RegexUtil.GetNumbersOnly(data.WorkPhone);
                leadtoCheck.Person.WorkPhoneExt = data.WorkPhoneExt;
                leadtoCheck.Person.FaxPhone = RegexUtil.GetNumbersOnly(data.FaxPhone);
                leadtoCheck.Person.CompanyName = data.CompanyName;
                leadtoCheck.Person.WorkTitle = data.WorkTitle;
                leadtoCheck.Person.PreferredTFN = data.PreferredTFN;
                //for unsubscribe only change it if original has date and current doesnt
                //OR
                //original doesnt have date and current has date
                //this is due to the fact that we're using a date instead of a boolean to enable/disable subscription
                if ((leadtoCheck.Person.UnsubscribedOn.HasValue && !data.UnsubscribedOnUtc.HasValue) ||
                    (!leadtoCheck.Person.UnsubscribedOn.HasValue && data.UnsubscribedOnUtc.HasValue))
                    leadtoCheck.Person.UnsubscribedOn = data.UnsubscribedOnUtc;

                //add to history
                var entry = CRMDBContext.Entry(leadtoCheck.Person);
                if (entry.State != EntityState.Unchanged)
                {
                    lead.LastUpdatedOnUtc = DateTime.UtcNow;
                    lead.LastUpdatedByPersonId = User.PersonId;
                    var changes = Util.GetEFChangedProperties(entry, leadtoCheck.Person);

                    lead.EditHistories.Add(new EditHistory()
                    {
                        IPAddress = HttpContext.Current.Request.UserHostAddress,
                        LoggedByPersonId = User.PersonId,
                        HistoryValue = changes.ToString(SaveOptions.DisableFormatting)
                    });

                    CRMDBContext.SaveChanges();
                    return Success;
                }
                else //no changes detected
                    return Success;
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        ///<summary>
        ///</summary>

        public string UpdateCustomer(int leadId, CustomerTP data)
        {
            if (leadId <= 0 || data == null)
                return DataCannotBeNullOrEmpty;
            //if (!BasePermission.CanUpdate)
            //    return Unauthorized;

            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var leadexist = connection.Query<Lead>("select * from CRM.Leads where LeadId = @leadid", new { leadid = leadId }).FirstOrDefault();
                    var customer = connection.Query<CustomerTP>("select * from CRM.Customer where CustomerId =@customerId", new { customerId = data.CustomerId }).FirstOrDefault();
                    if (leadexist == null)
                        return LeadDoesNotExist;
                    if (customer == null)
                    {
                        data.CreatedOn = DateTime.UtcNow;
                        //TODO Created by has to be here.
                        var customerId = (int)connection.Insert(data);
                        connection.Query("insert into crm.LeadCustomers values(@leadid, @customerId,@isPrimaryCustomer)", new { leadid = leadId, customerId = customerId, isPrimaryCustomer = false });
                        return "Success";
                    }
                    else if (customer != null)
                    {
                        //check for duplicate email
                        //TODO Check this email validation will check in other customer.
                        if ((customer.PrimaryEmail != data.PrimaryEmail && !string.IsNullOrEmpty(data.PrimaryEmail)) ||
                            (customer.SecondaryEmail != data.SecondaryEmail &&
                             !string.IsNullOrEmpty(data.SecondaryEmail)))
                        {
                            using (var conn = new SqlConnection(EntityCnxStringBuilder.ProviderConnectionString))
                            {
                                conn.OpenAsync();
                                var exists = conn.Query<bool>(
                                    "Select [CRM].[fnsHasDuplicateEmail](@p0, @p1, @p2, @p3)",
                                    new
                                    {
                                        p0 = Franchise.FranchiseId,
                                        p1 = data.PrimaryEmail,
                                        p2 = data.SecondaryEmail,
                                        p3 = data.CustomerId
                                    }).FirstOrDefault();
                                if (exists)
                                {
                                    return "Duplicate email address found, unable to update person";
                                }
                            }
                        }

                        //var lead = new Lead { LeadId = leadId };
                        //CRMDBContext.Leads.Attach(lead);

                        customer.FirstName = data.FirstName;
                        customer.LastName = data.LastName;
                        customer.PrimaryEmail = data.PrimaryEmail;
                        customer.MI = data.MI;

                        if (!string.IsNullOrEmpty(data.PrimaryEmail) && customer.PrimaryEmail != data.SecondaryEmail)
                            customer.SecondaryEmail = data.SecondaryEmail;
                        else
                            customer.SecondaryEmail = data.SecondaryEmail;

                        customer.HomePhone = RegexUtil.GetNumbersOnly(data.HomePhone);
                        customer.CellPhone = RegexUtil.GetNumbersOnly(data.CellPhone);
                        customer.WorkPhone = RegexUtil.GetNumbersOnly(data.WorkPhone);
                        customer.WorkPhoneExt = data.WorkPhoneExt;
                        customer.FaxPhone = RegexUtil.GetNumbersOnly(data.FaxPhone);
                        customer.CompanyName = data.CompanyName;
                        customer.WorkTitle = data.WorkTitle;
                        customer.PreferredTFN = data.PreferredTFN;
                        customer.IsReceiveEmails = data.IsReceiveEmails;
                        //for unsubscribe only change it if original has date and current doesnt
                        //OR
                        //original doesnt have date and current has date
                        //this is due to the fact that we're using a date instead of a boolean to enable/disable subscription
                        if ((customer.UnsubscribedOn.HasValue && !data.UnsubscribedOn.HasValue) ||
                            (!customer.UnsubscribedOn.HasValue && data.UnsubscribedOn.HasValue))
                            customer.UnsubscribedOn = data.UnsubscribedOn;

                        var CustomerAffected = connection.Update(customer);
                        return Success;
                        //add to history
                        //TODO Handle History Later
                        //var entry = CRMDBContext.Entry(customer);
                        //if (entry.State != EntityState.Unchanged)
                        //{
                        //    lead.LastUpdatedOnUtc = DateTime.Now;
                        //    lead.LastUpdatedByPersonId = User.PersonId;
                        //    var changes = Util.GetEFChangedProperties(entry, customer);

                        //    lead.EditHistories.Add(new EditHistory()
                        //    {
                        //        IPAddress = HttpContext.Current.Request.UserHostAddress,
                        //        LoggedByPersonId = User.PersonId,
                        //        HistoryValue = changes.ToString(SaveOptions.DisableFormatting)
                        //    });

                        //    CRMDBContext.SaveChanges();
                        //    return Success;

                        //}
                        //else //no changes detected
                        //    return Success;
                    }
                    else
                        return Success;
                }
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        /// <summary>
        /// Adds the secondary customer.
        /// </summary>
        /// <param name="leadId">The lead identifier.</param>
        /// <param name="data">The data.</param>
        /// <param name="personId">The person identifier.</param>
        /// <returns>System.String.</returns>
        public string AddSecondaryCustomer(int leadId, Person data, out int personId)
        {
            personId = 0;

            if (leadId <= 0 || data == null)
                return DataCannotBeNullOrEmpty;

            //if (!BasePermission.CanUpdate)
            //    return Unauthorized;

            try
            {
                var leadtoCheck =
                    CRMDBContext.Leads.Where(
                        w => w.LeadId == leadId && w.IsDeleted == false && w.FranchiseId == Franchise.FranchiseId)
                        .Select(s => new { s.SecCustomerId }).FirstOrDefault();
                if (leadtoCheck == null)
                    return LeadDoesNotExist;
                if (leadtoCheck.SecCustomerId.HasValue)
                    return "Secondary person already exist, please use update instead of insert";

                //check for duplicate email
                if (!string.IsNullOrEmpty(data.PrimaryEmail) ||
                    !string.IsNullOrEmpty(data.SecondaryEmail))
                {
                    if (CRMDBContext.Database.SqlQuery<bool>("SELECT [CRM].[fnsHasDuplicateEmail](@p0, @p1, @p2, null)",
                        Franchise.FranchiseId, data.PrimaryEmail, data.SecondaryEmail).FirstOrDefault())
                    {
                        return "Duplicate email address found, unable to add person";
                    }
                }

                var lead = new Lead { LeadId = leadId };
                CRMDBContext.Leads.Attach(lead);

                data.PersonId = 0; //just to make sure we insert a new record;

                //TODO: verify whether it works well or not
                //lead.SecPerson = data;

                lead.LastUpdatedOnUtc = DateTime.UtcNow;
                lead.LastUpdatedByPersonId = User.PersonId;

                XElement history =
                    new XElement("Lead",
                        new XElement("SecPerson", new XAttribute("name", "Secondary Person"),
                            new XAttribute("operation", "insert"),
                            new XElement("Current", data.FullName)
                            )
                        );

                lead.EditHistories.Add(new EditHistory()
                {
                    IPAddress = HttpContext.Current.Request.UserHostAddress,
                    LoggedByPersonId = User.PersonId,
                    HistoryValue = history.ToString(SaveOptions.DisableFormatting)
                });

                CRMDBContext.SaveChanges();
                personId = data.PersonId;

                return Success;
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        /// <summary>
        /// Updates the address.
        /// </summary>
        /// <param name="leadId">The lead identifier.</param>
        /// <param name="data">The data.</param>
        /// <returns>System.String.</returns>
        /// TODO: changed this method to Dapper
        public string UpdateAddress(int leadId, AddressTP data)
        {
            if (leadId <= 0 || data == null || data.AddressId <= 0)
                return DataCannotBeNullOrEmpty;

            //if (!BasePermission.CanUpdate)
            //    return Unauthorized;

            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var leadToCheck = connection.Query<Lead>("select * from CRM.Leads where LeadId = @leadid", new { leadid = leadId }).FirstOrDefault();
                    var address = connection.Query<AddressTP>("select * from CRM.Addresses where AddressId =@addressId", new { addressId = data.AddressId }).FirstOrDefault();
                    //var leadToCheck = (from l in CRMDBContext.Leads
                    //    where l.LeadId == leadId && l.IsDeleted == false && l.FranchiseId == Franchise.FranchiseId
                    //    select new {Address = l.Addresses.FirstOrDefault(f => f.AddressId == data.AddressId)})
                    //    .FirstOrDefault();

                    if (leadToCheck == null)
                        return LeadDoesNotExist;
                    if (address == null)
                    {
                        address.CreatedOnUtc = DateTime.UtcNow;
                        //TODO Created by has to be here.
                        var addressId = (int)connection.Insert(address);
                        connection.Query("insert into crm.LeadAddresses values(@leadid, @addressid, @IsPrimaryAddress)", new { leadid = leadId, addressid = addressId, IsPrimaryAddress = 1 });
                        return "Success";
                    }

                    address.AttentionText = data.AttentionText;
                    address.Address1 = data.Address1;
                    address.Address2 = data.Address2;
                    address.City = data.City;
                    address.State = data.State;
                    if (!string.IsNullOrEmpty(data.ZipCode))
                        address.ZipCode = data.ZipCode.ToUpper();
                    else
                        address.ZipCode = null;
                    address.Location = data.Location;
                    address.CrossStreet = data.CrossStreet;
                    address.CountryCode2Digits = string.IsNullOrEmpty(data.CountryCode2Digits)
                        ? Franchise.CountryCode
                        : data.CountryCode2Digits;
                    address.IsResidential = data.IsResidential;
                    address.IsValidated = data.IsValidated;

                    connection.Update(address);
                    connection.Close();

                    return Success;
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                return ex.Message;
#else
                return EventLogger.LogEvent(ex);
#endif
            }
        }

        // The followings are no more used as these are taken care by the address component

        ///// <summary>
        ///// Adds the address.
        ///// </summary>
        ///// <param name="leadId">The lead identifier.</param>
        ///// <param name="data">The data.</param>
        ///// <param name="addressId">The address identifier.</param>
        ///// <returns>System.String.</returns>
        //public string AddAddress(int leadId, Address data, out int addressId)
        //{
        //    addressId = 0;
        //    if (leadId <= 0 || data == null)
        //        return DataCannotBeNullOrEmpty;

        //    //if (!BasePermission.CanUpdate)
        //    //    return Unauthorized;

        //    try
        //    {
        //        var leadToCheck = (from l in CRMDBContext.Leads
        //                           where l.LeadId == leadId && l.IsDeleted == false && l.FranchiseId == Franchise.FranchiseId
        //                           select l.LeadId).Any();
        //        if (leadToCheck == false)
        //            return LeadDoesNotExist;

        //        var lead = new Lead { LeadId = leadId, Addresses = new List<Address>() };
        //        CRMDBContext.Leads.Attach(lead);

        //        if (string.IsNullOrEmpty(data.CountryCode2Digits))
        //            data.CountryCode2Digits = Franchise.CountryCode;
        //        if (!string.IsNullOrEmpty(data.ZipCode))
        //            data.ZipCode = data.ZipCode.ToUpper();

        //        lead.Addresses.Add(data);

        //        //var changes = new XElement("Lead",
        //        //    new XElement("Address", new XAttribute("operation", "insert"),
        //        //        new XElement("Current", data.ToString())));

        //        //lead.LastUpdatedOnUtc = DateTime.Now;
        //        //lead.LastUpdatedByPersonId = User.PersonId;
        //        //lead.EditHistories.Add(new EditHistory()
        //        //{
        //        //    IPAddress = HttpContext.Current.Request.UserHostAddress,
        //        //    LoggedByPersonId = User.PersonId,
        //        //    HistoryValue = changes.ToString(System.Xml.Linq.SaveOptions.DisableFormatting)
        //        //});

        //        CRMDBContext.SaveChanges();
        //        if (data.AddressId > 0)
        //            addressId = data.AddressId;

        //        return Success;
        //    }
        //    catch (Exception ex)
        //    {
        //        return EventLogger.LogEvent(ex);
        //    }
        //}

        ///// <summary>
        ///// Deletes the address.
        ///// </summary>
        ///// <param name="leadId">The lead identifier.</param>
        ///// <param name="addressId">The address identifier.</param>
        ///// <returns>System.String.</returns>
        //public string DeleteAddress(int leadId, int addressId)
        //{
        //    if (leadId <= 0 || addressId <= 0)
        //        return DataCannotBeNullOrEmpty;

        //    try
        //    {
        //        var count = (from l in CRMDBContext.Leads
        //                     from a in l.Addresses
        //                     where l.LeadId == leadId && l.FranchiseId == Franchise.FranchiseId && l.IsDeleted == false
        //                     select a).Count();
        //        if (count == 1)
        //        {
        //            return LeadsAddressOneLeft;
        //        }

        //        var addr = (from l in CRMDBContext.Leads
        //                    from a in l.Addresses
        //                    where l.LeadId == leadId && l.FranchiseId == Franchise.FranchiseId && l.IsDeleted == false
        //                          && a.AddressId == addressId
        //                    select a).FirstOrDefault();
        //        if (addr == null)
        //            return AddressDoesNotExist;

        //        var lead = new Lead { LeadId = leadId, Addresses = new List<Address> { addr } };
        //        CRMDBContext.Leads.Attach(lead);

        //        var history = new XElement("Lead",
        //            new XElement("Address", new XAttribute("operation", "delete"),
        //                new XElement("Original", new XAttribute("id", addressId), addr.ToString())));

        //        lead.LastUpdatedOnUtc = DateTime.Now;
        //        lead.LastUpdatedByPersonId = User.PersonId;
        //        lead.EditHistories.Add(new EditHistory
        //        {
        //            LoggedByPersonId = User.PersonId,
        //            IPAddress = HttpContext.Current.Request.UserHostAddress,
        //            HistoryValue = history.ToString(SaveOptions.DisableFormatting)
        //        });
        //        lead.Addresses.Remove(addr);
        //        CRMDBContext.Addresses.Remove(addr);

        //        CRMDBContext.SaveChanges();
        //        return Success;
        //    }
        //    catch (DbUpdateException)
        //    {
        //        return "Unable to delete address, it is linked to a job or lead";
        //    }
        //    catch (Exception ex)
        //    {
        //        return EventLogger.LogEvent(ex);
        //    }
        //}
        public Lead GetLeadByLeadId(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var query = "select * from crm.leads where leadid = @leadId";

                    var lead = connection.Query<Lead>(query, new { leadId = id }).FirstOrDefault();
                    return lead;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public override Lead Get(int id)
        {
            if (id < 0)
                throw new ArgumentNullException(LeadDoesNotExist);

            // Handled in the controller itself
            //if (!BasePermission.CanRead)
            //    throw new AccessViolationException(Unauthorized);

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select ld.*,te.Name as DisplayTerritoryName from CRM.Leads ld
                                left join CRM.Territories te on ld.TerritoryDisplayId = te.TerritoryId where leadid = @leadId";

                var lead = connection.Query<Lead>(query, new { leadId = id }).FirstOrDefault();

                if (lead != null && lead.FranchiseId != Franchise.FranchiseId)
                {
                    // throw new AccessViolationException(Unauthorized);
                }

                if (lead.CreatedOnUtc != null)
                    lead.CreatedOnUtc = TimeZoneManager.ToLocal(lead.CreatedOnUtc);
                if (lead.LastUpdatedOnUtc != null)
                    lead.LastUpdatedOnUtc = TimeZoneManager.ToLocal((DateTime)lead.LastUpdatedOnUtc);

                //Add Account Name And Referral Type If it is available
                if (lead.RelatedAccountId != null)
                {
                    query = @"select  (select [CRM].[fnGetAccountNameByAccountId](l.RelatedAccountId)) as AccountName from  crm.Leads l
                          join crm.AccountCustomers ac on ac.AccountId = l.RelatedAccountId
                          join crm.Customer cus on cus.CustomerId = ac.CustomerId
                          where l.RelatedAccountId = @relatedAccountId";
                    var acccountName = connection.Query<string>(query, new { relatedAccountId = lead.RelatedAccountId }).FirstOrDefault();
                    lead.RelatedAccountName = acccountName;
                }
                if (lead.ReferralTypeId != null)
                {
                    query = @" select rt.Name from crm.Leads l
                           join crm.Type_Referral rt on rt.Id = l.ReferralTypeId
                           where l.ReferralTypeId =@referralId";
                    var referralName = connection.Query<string>(query, new { referralId = lead.ReferralTypeId }).FirstOrDefault();
                    lead.ReferralTypeName = referralName;
                }

                query = @"select * from crm.Customer where customerid in (
                            select customerid from crm.LeadCustomers where LeadId = @leadId and IsPrimaryCustomer = 0)";
                var secPerson = connection.Query<CustomerTP>(query, new { leadId = id }).ToList();
                lead.SecCustomer = secPerson;
                //Now the CustomerId will not be the part of the Lead Tabel
                // query = "select * from crm.customer where customerid = @customerId";
                query = @"select * from crm.Customer where customerid in (
                            select customerid from crm.LeadCustomers where LeadId = @leadId and IsPrimaryCustomer = 1)";
                var customer = connection.Query<CustomerTP>(query, new { leadId = id }).FirstOrDefault();
                lead.PrimCustomer = customer;
                if (lead.CampaignId != null)
                {
                    query = @"select * from CRM.Campaign Where CampaignId = @campaignId";
                    var campaign = connection.Query<Campaign>(query, new { campaignId = lead.CampaignId }).FirstOrDefault();
                    if (campaign != null)
                        lead.CampaignName = campaign.Name;
                }

                query = @"select * from crm.Addresses where AddressId in (
                            select AddressId from crm.LeadAddresses where LeadId = @leadId)";
                var addresses = connection.Query<AddressTP>(query, new { leadId = id }).ToList();
                lead.Addresses = addresses;

                query = @"select distinct stp.SourcesTPId as SourceId,o.Name as OriginationName,s.Name as SourceName,c.Name as ChannelName,o.name +' - '+ c.Name + ' - ' + s.Name as name,ls.IsPrimarySource from crm.SourcesTP stp
                        join crm.Type_Channel c on c.ChannelId = stp.channelid
                        join crm.sources s on s.SourceId = stp.SourceId
                        join CRM.Type_Owner o on o.OwnerId = stp.OwnerId
                        join crm.LeadSources ls on ls.SourceId = stp.SourcesTPId
		                join crm.leads l on l.LeadId = ls.LeadId
		                where ls.LeadId = @leadId";
                var leadSources = connection.Query<SourceList>(query, new { leadId = id }).ToList();

                lead.SourcesList = leadSources;
                lead.LeadStatus = connection.Query<Type_LeadStatus>("select * from CRM.Type_LeadStatus where Id = @id", new { id = lead.LeadStatusId }).FirstOrDefault();
                lead.Disposition = connection.Query<Disposition>("select * from CRM.Disposition where DispositionId = @dispositionid", new { dispositionid = lead.DispositionId }).FirstOrDefault();

                // TODO : This is not required, need to verify whether it breaks???
                // var leadNotes = noteMgr.GetLeadNotes(new Note { LeadId = id }).ToList();
                // lead.LeadNotes = leadNotes;

                if (lead.IndustryId > 0)
                {
                    var queryIndustry = @"select * from [CRM].[Type_LookUpValues] where Id =@id";
                    var industry = connection.Query<Type_LookUpValues>(queryIndustry, new { id = lead.IndustryId }).FirstOrDefault();
                    lead.IndustryValue = industry.Name;
                }
                if (lead.CommercialTypeId > 0)
                {
                    var queryCommercial = @"select * from [CRM].[Type_LookUpValues] where Id =@id";
                    var commercialType = connection.Query<Type_LookUpValues>(queryCommercial, new { id = lead.CommercialTypeId }).FirstOrDefault();
                    lead.CommercialValue = commercialType.Name;
                }
                if (lead.RelationshipTypeId > 0)
                {
                    var queryRelationship = @"select * from [CRM].[Type_LookUpValues] where Id =@id";
                    var relationshipType = connection.Query<Type_LookUpValues>(queryRelationship, new { id = lead.RelationshipTypeId }).FirstOrDefault();
                    lead.RelationShipTypeValue = relationshipType.Name;
                }
                try
                {
                    // TODO: should not it return the brand id from the francise, of of the current lead.
                    int BrandId = (int)SessionManager.BrandId;
                    List<Type_Question> LeadQuestion = HFC.CRM.Managers.TypeQuestionManager.GetTypeQuestionsById(BrandId, lead.LeadId);
                    lead.LeadQuestionAns = LeadQuestion;
                }
                catch (Exception ex) { }
                finally { connection.Close(); }

                if (lead != null)
                {
                    // TODO: is this required in TP???
                    //lead.CanUpdateStatus = StatusPermission.CanUpdate;
                    lead.CanUpdateStatus = true;

                    //lead.CanUpdate = BasePermission.CanUpdate;
                    //lead.CanDelete = BasePermission.CanDelete;

                    var primarySource = leadSources.Where(x => x.IsPrimarySource == true).FirstOrDefault();
                    //if (lead.CampaignId == null && primarySource == null)
                    //{
                    //    throw new Exception("Primary source was not assigned!");
                    //}
                    lead.SourcesTPId = primarySource == null ? 0 : primarySource.SourceId;
                    if (lead.CampaignId == null) lead.CampaignId = 0;
                }
                return lead;
            }
        }

        /// <summary>
        /// Not implemented
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public override string Add(Lead data)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Not implemented
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public override string Update(Lead data)
        {
            try
            {
                //var primarySource = data.LeadSources.Where(x => x.IsPrimarySource == true).FirstOrDefault();
                if (data.CampaignId == null && data.SourcesTPId == 0)
                {
                    throw new Exception("Primary source was not assigned!");
                }

                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var leadStatus = statusChangesMgr.StatusChanges(1, data.LeadId, data.LeadStatusId);
                    if (!leadStatus)
                        throw new ValidationException("Not a valid Lead Status");

                    // Source and subsources need to be deleted first, then added based
                    // on the current selection.
                    var query = "delete from crm.LeadSources where LeadId = @leadId ";
                    connection.Execute(query, new { leadId = data.LeadId });

                    // Insert the primary source first....
                    if (data.SourcesTPId > 0)
                    {
                        connection.Query(@"insert into crm.LeadSources (SourceId,LeadId,CreatedOnUtc,IsManuallyAdded,IsPrimarySource)
                        values(@sourceId,@leadId,@createdonutc,@isManuallyAdded,@isPrimarySource)",
                        new
                        {
                            sourceId = data.SourcesTPId,
                            leadId = data.LeadId,
                            createdonutc = DateTime.UtcNow,
                            isManuallyAdded = true,
                            isPrimarySource = true
                        });
                    }

                    // TODO: need to be hanlded better.
                    // Right now we are just removing the primary source from the
                    // additional source list.
                    if (data.LeadSources != null && data.LeadSources.Count > 0)
                    {
                        var additionalSources = data.LeadSources.Where(x => x.SourceId != data.SourcesTPId).ToList();

                        foreach (var item in additionalSources)//data.LeadSources)
                        {
                            connection.Query(@"insert into crm.LeadSources (
                                SourceId,LeadId,CreatedOnUtc,IsManuallyAdded,IsPrimarySource)
                                values(@sourceId,@leadId,@createdonutc,@isManuallyAdded,@isPrimarySource)",
                               new
                               {
                                   sourceId = item.SourceId,
                                   leadId = data.LeadId,
                                   createdonutc = DateTime.UtcNow,
                                   isManuallyAdded = true,
                                   isPrimarySource = false
                               });
                        }
                    }

                    // TODO: this need to update only the primary address and not all the addresses.
                    if (data.Addresses != null)
                    {
                        foreach (var addr in data.Addresses)
                        {
                            this.UpdateAddress(data.LeadId, addr);
                        }
                    }

                    string[] TerritoryDetails = new string[2];
                    if (data.Addresses != null)
                    {
                        TerritoryDetails = TerritoryManager.GetTerritoryDetails(data.Addresses.FirstOrDefault().ZipCode, data.Addresses.FirstOrDefault().CountryCode2Digits).Split(';');
                    }

                    if (TerritoryDetails != null && TerritoryDetails[0] != null)
                    {
                        if (TerritoryDetails[0] == "0")
                        {
                            data.TerritoryType = TerritoryDetails[1];
                            data.TerritoryId = null;
                        }
                        else
                        {
                            data.TerritoryType = TerritoryDetails[1];
                            data.TerritoryId = Convert.ToInt32(TerritoryDetails[0]);
                        }
                    }

                    // We no need to update the Secondary customers as this is managed by separate service.
                    if (data.PrimCustomer != null)
                    {
                        this.UpdateCustomer(data.LeadId, data.PrimCustomer);
                    }

                    data.LastUpdatedOnUtc = DateTime.UtcNow;
                    data.LastUpdatedByPersonId = User.PersonId;

                    data.IndustryId = data.IndustryId > 0 ? data.IndustryId : (int?)null;
                    data.CommercialTypeId = data.CommercialTypeId > 0 ? data.CommercialTypeId : (int?)null;
                    data.RelationshipTypeId = data.RelationshipTypeId > 0 ? data.RelationshipTypeId : (int?)null;
                    if (data.CampaignId != null && data.CampaignId == 0) data.CampaignId = null;
                    if (data.EmailSent == false || data.EmailSent == null)
                        data.EmailSent = false;
                    data.ImpersonatorPersonId = this.ImpersonatorPersonId;
                    connection.Update(data);
                    connection.Close();

                    // Log History Data Audit trial
                    EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(data), LeadId: data.LeadId);
                    return Success;
                }
            }
            catch (ValidationException ex)
            {
                return ex.Message;
            }
        }

        public bool CheckDuplicateLeadData(string duplicateCheck)
        {
            var query = @" begin
                        declare @input varchar(200)
                        set @input=@value
                        SELECT *  FROM CRM.Customer where PrimaryEmail=@input or SecondaryEmail=@input or HomePhone=@input or CellPhone=@input or WorkPhone=@input
                        end";
            var result = ExecuteIEnumerableObject<CustomerTP>(ContextFactory.CrmConnectionString, query, new
            {
                value = duplicateCheck
            }).ToList();
            if (result.Count != 0)
            {
                return true;
            }
            else
                return false;
        }

        [Obsolete("This is replaced with GetAddressTP!")]
        public List<AddressTP> GetAddress(List<int> leadIds)
        {
            // Controller will take care of this.
            //if (!BasePermission.CanRead)
            //    throw new UnauthorizedAccessException(Unauthorized);

            if (leadIds == null || leadIds.Count == 0)
                return null;

            var result = (from l in CRMDBContext.Leads
                          from a in l.Addresses
                          where leadIds.Contains(l.LeadId) && l.IsDeleted == false && l.FranchiseId == Franchise.FranchiseId
                          select a).ToList();

            return result;
        }

        public List<AddressTP> GetAddressTP(int leadId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select a.* From crm.addresses a
                    inner join crm.leadaddresses la on a.AddressId = la.AddressId
                    inner join crm.leads l on la.LeadId = l.LeadId
                    where la.LeadId = @leadId and l.IsDeleted <> 1";

                var result = connection.Query<AddressTP>(query
                    , new { leadId = leadId }).ToList();

                connection.Close();

                return result;
            }
        }

        public Nullable<int> spLeadNumber_New(Nullable<int> franchiseId)
        {
            using (var conn = new SqlConnection(EntityCnxStringBuilder.ProviderConnectionString))
            {
                conn.Open();
                var dto = conn.Query(
                    "[CRM].[spLeadNumber_New] @FranchiseId",
                    new
                    {
                        FranchiseId = franchiseId,
                    }).ToList();

                var leadNumber = dto[0].LeadNumber;
                return leadNumber;
            }
        }

        public string UpdateLead(int leadId, int leadStatusId, int dispositionId)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    if (leadId != null && leadId != 0)
                    {
                        connection.Open();
                        var lead = connection.Query<Lead>("select * from CRM.Leads where LeadId = @leadid", new { leadid = leadId }).FirstOrDefault();
                        lead.LeadStatusId = Convert.ToInt16(leadStatusId);
                        lead.DispositionId = Convert.ToInt16(dispositionId);

                        connection.Update(lead);

                        connection.Close();
                    }
                    return Success;
                }
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

        #region ---- Touhchpoint ----
        public string UploadFileTP(byte[] bytes, string fileName)
        {
            string Query = "INSERT INTO FileTableTb (name, file_stream) OUTPUT INSERTED.[stream_id] values(@FileName, @stream)";

            Random rnd = new Random();
            string value = rnd.Next(1, 9999).ToString();
            fileName = DateTime.UtcNow.ToString("MMddyyyy") + "_" + DateTime.UtcNow.ToString("HHMMssfff") + value + "_" + fileName;

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var dParams = new DynamicParameters();
                dParams.Add("@FileName", fileName, DbType.AnsiString);
                dParams.Add("@stream", bytes, DbType.Binary);
                var streamid = connection.ExecuteScalar(Query, dParams);
                return streamid.ToString();
            }
        }

        #endregion ---- Touhchpoint ----

        #region ---- Convert Lead ----

        public override ICollection<Lead> Get(List<int> idList)
        {
            throw new NotImplementedException();
        }

        public override string Delete(int id)
        {
            throw new NotImplementedException();
        }

        #endregion ---- Convert Lead ----

        public List<NotesAttachmentslist> PrintLeadNotes(int id = 0, bool internalnotes = false, bool externalnotes = false, bool directionnotes = false)
        {
            string query = @"SELECT LeadId, '' AS 'FileName', '' AS 'FullFileName' , Notes AS 'Note',
                            (CASE TypeEnum WHEN 0 THEN 'Internal' WHEN 2 THEN 'Direction' WHEN 5 THEN 'External' END) AS 'Category',
                            Title, TypeEnum, NoteId AS 'Id' FROM [CRM].[Note] WHERE LeadId = @id and IsNote=1  and ("; //TypeEnum=0 or TypeEnum=5)";
            string query2 = null;

            if (internalnotes) query2 = " TypeEnum=0 ";
            if (externalnotes) query2 = string.IsNullOrEmpty(query2) ? " TypeEnum=5 " : " or TypeEnum=5 ";
            if (directionnotes) query2 = string.IsNullOrEmpty(query2) ? " TypeEnum=2 " : " or TypeEnum=2 ";

            query += query2 + @" UNION ALL  SELECT ATT.LeadId, PHY.FileName, PHY.UrlPath,'',PHY.FileCategory,PHY.FileDescription, 0,
                                PHY.PhysicalFileId FROM [CRM].[PhysicalFiles] PHY  INNER JOIN [CRM].[Attachments] ATT ON PHY.PhysicalFileId = ATT.PhysicalFileId  WHERE ATT.LeadId = @id
                                order by category desc";

            //if (internalnotes == true && externalnotes == false)
            //{
            //    query = @"SELECT LeadId, '' AS 'FileName', '' AS 'FullFileName' , Notes AS 'Note',
            //                (CASE TypeEnum WHEN 0 THEN 'Internal' WHEN 2 THEN 'Direction' WHEN 5 THEN 'External' END) AS 'Category',
            //                Title, TypeEnum, NoteId AS 'Id' FROM [CRM].[Note] WHERE LeadId = @id and IsNote=1  and (TypeEnum=0)
            //                UNION ALL  SELECT ATT.LeadId, PHY.FileName, PHY.UrlPath,'',PHY.FileCategory,PHY.FileDescription, 0,
            //                    PHY.PhysicalFileId FROM [CRM].[PhysicalFiles] PHY  INNER JOIN [CRM].[Attachments] ATT ON PHY.PhysicalFileId = ATT.PhysicalFileId  WHERE ATT.LeadId = @id
            //                    order by category desc";
            //}
            //if (internalnotes == false && externalnotes == true)
            //{
            //    query = @"SELECT LeadId, '' AS 'FileName', '' AS 'FullFileName' , Notes AS 'Note',
            //                (CASE TypeEnum WHEN 0 THEN 'Internal' WHEN 2 THEN 'Direction' WHEN 5 THEN 'External' END) AS 'Category',
            //                Title, TypeEnum, NoteId AS 'Id' FROM [CRM].[Note] WHERE LeadId = @id and IsNote=1  and (TypeEnum=5)
            //                UNION ALL  SELECT ATT.LeadId, PHY.FileName, PHY.UrlPath,'',PHY.FileCategory,PHY.FileDescription, 0,
            //                    PHY.PhysicalFileId FROM [CRM].[PhysicalFiles] PHY  INNER JOIN [CRM].[Attachments] ATT ON PHY.PhysicalFileId = ATT.PhysicalFileId  WHERE ATT.LeadId = @id
            //                    order by category desc";
            //}
            //if (internalnotes == true && externalnotes == true)
            //{
            //    query = @"SELECT LeadId, '' AS 'FileName', '' AS 'FullFileName' , Notes AS 'Note',
            //                (CASE TypeEnum WHEN 0 THEN 'Internal' WHEN 2 THEN 'Direction' WHEN 5 THEN 'External' END) AS 'Category',
            //                Title, TypeEnum, NoteId AS 'Id' FROM [CRM].[Note] WHERE LeadId = @id and IsNote=1  and (TypeEnum=0 or TypeEnum=5)
            //                UNION ALL  SELECT ATT.LeadId, PHY.FileName, PHY.UrlPath,'',PHY.FileCategory,PHY.FileDescription, 0,
            //                    PHY.PhysicalFileId FROM [CRM].[PhysicalFiles] PHY  INNER JOIN [CRM].[Attachments] ATT ON PHY.PhysicalFileId = ATT.PhysicalFileId  WHERE ATT.LeadId = @id
            //                    order by category desc";
            //}

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                var InternalExternalNotes = connection.Query<NotesAttachmentslist>(query, new { id = id }).ToList();
                return InternalExternalNotes;
            }
        }

        public int printMeasurements(int leadid)
        {
            string query = @"select * from crm.Opportunities op
                            inner join crm.accounts a on a.AccountId=op.AccountId
                            inner join crm.leads l on l.LeadId=a.LeadId
                            where l.LeadId=@id";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                Opportunity opportunity = connection.Query<Opportunity>(query, new { id = leadid }).SingleOrDefault();
                if (opportunity != null)
                {
                    return opportunity.OpportunityId;
                }
                return 0;
            }
        }

        public string UpdateSentEmail(int leadid)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = "select * from crm.Leads where LeadId = @leadId ";
                var result = connection.Query<Lead>(query, new { leadId = leadid }).FirstOrDefault();
                if (result != null)
                {
                    result.EmailSent = true;
                    var email = connection.Update(result);
                }
                connection.Close();
            }
            return "True";
        }

        public string GetLeadEmail(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = "select * from crm.Leads where LeadId = @leadId ";
                var result = connection.Query<Lead>(query, new { leadId = id }).FirstOrDefault();
                if (result != null)
                {
                    if (result.EmailSent == true)
                        return "True";
                    else return "False";
                }
            }
            return null;
        }

        public string SendThankYouEmail(int leadId, EmailType emailType)  //, int emailType)
        {
            //To send the Lead confirmation after lead saved
            if (leadId <= 0)
            {
                return "Invalid Lead id";
            }

            List<string> toAddresses = new List<string>();

            TerritoryDisplay data = emailMgr.GetTerritoryDisplayByLeadId(leadId);

            //From email address
            string fromEmailAddress = "";

            if (data != null && data.Id == null)
                fromEmailAddress = emailMgr.GetFranchiseEmail(Franchise.FranchiseId, emailType);
            else
            {
                if (Franchise.AdminEmail != "")
                    fromEmailAddress = Franchise.AdminEmail;
                else if (Franchise.OwnerEmail != "")
                    fromEmailAddress = Franchise.OwnerEmail;
            }

            //To get Franchise email and to lead email
            var LeadCustomer = emailMgr.GetLeadEmail(leadId);
            if (LeadCustomer != null)
            {
                if (!string.IsNullOrEmpty(LeadCustomer.PrimaryEmail) && LeadCustomer.PrimaryEmail != "")
                {
                    if (LeadCustomer.PrimaryEmail.Trim() != "")
                    {
                        toAddresses.Add(LeadCustomer.PrimaryEmail.Trim());
                    }
                }
                if (!string.IsNullOrEmpty(LeadCustomer.SecondaryEmail) && LeadCustomer.SecondaryEmail != "")
                {
                    if (LeadCustomer.SecondaryEmail.Trim() != "")
                    {
                        toAddresses.Add(LeadCustomer.SecondaryEmail.Trim());
                    }
                }
            }

            //If no from or to emails return
            if (toAddresses == null || toAddresses.Count == 0 || fromEmailAddress == "")
            {
                return "Success";
            }

            string TemplateSubject = "", bodyHtml = "";
            int FranchiseId = SessionManager.CurrentFranchise.FranchiseId;

            int BrandId = (int)SessionManager.BrandId;

            //Take Body Html from EmailTemplate Table
            var emailtemplate = emailMgr.GetHtmlBody(emailType, BrandId);
            TemplateSubject = emailtemplate.TemplateSubject;
            bodyHtml = emailtemplate.TemplateLayout;
            short TemplateEmailId = (short)emailtemplate.EmailTemplateId;

            var mailMsg = emailMgr.BuildMailMessageLeadThankYou(leadId, bodyHtml, TemplateSubject
                , TemplateEmailId, BrandId, FranchiseId
                , fromEmailAddress, toAddresses);

            var result = UpdateSentEmail(leadId);
            return result;
        }
    }
}