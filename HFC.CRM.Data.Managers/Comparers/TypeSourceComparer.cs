﻿namespace HFC.CRM.Managers.Comparers
{
    using System.Collections.Generic;

    using HFC.CRM.Data;

    public class TypeSourceComparer : IEqualityComparer<Source>
    {
        public bool Equals(Source x, Source y)
        {
            return x.SourceId == y.SourceId;
        }

        public int GetHashCode(Source obj)
        {
            return obj.SourceId.GetHashCode();
        }
    }
}