﻿using Microsoft.Exchange.WebServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using HFC.CRM.Core;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Core.Common;
using HFC.CRM.Data.Constants;
using System.Data.Entity;
using HFC.CRM.DTO.Calandar;
using HFC.CRM.DTO.SentEmail;
using Dapper;
using HFC.CRM.Data.Context;
using System.Data.SqlClient;
using System.Dynamic;
using System.Text;
using HFC.CRM.Core.Extensions;

namespace HFC.CRM.Managers
{
    /// <summary>
    /// ExchangeManager contains business logic for handling exchange sync to calendar/appointments
    /// </summary>
    public class ExchangeManager : IMessageConstants
    {
        #region Private Members

        private PropertySet AppointmentPropertySet;
        private List<string> AppointmentTypeEnumStrings = new List<string>();
        private List<string> AppointmentTypeEnumStringsFromCache = new List<string>();
        private CRMContext CRMDBContext;
        private CalendarManager CalendarMgr;
        private Sync_Tracker SyncTracker;

        #endregion

        #region Properties
        /// <summary>
        /// Exchange service attached to a user
        /// </summary>
        public ExchangeService service;

        /// <summary>
        /// Gets exchange domain name
        /// </summary>
        public static string Domain
        {
            get
            {
                return AppConfigManager.GetConfig<string>("DomainName", "Active Directory", "Authentication");
            }
        }

        /// <summary>
        /// Returns true if Exchange Push Notification Sync is enabled
        /// </summary>
        /// <value><c>true</c> if this instance is enabled; otherwise, <c>false</c>.</value>
        public static bool IsEnabled
        {
            get
            {
                return AppConfigManager.GetConfig<bool>("Enabled", "Exchange", "Synchronize");
            }
        }

        /// <summary>
        /// Gets the name of the provider.
        /// </summary>
        public static string ProviderName { get { return "Exchange"; } }

        /// <summary>
        /// Gets number of max retry count before stopping
        /// </summary>
        /// <value>The error retry count.</value>
        public static int ErrorRetryCount
        {
            get
            {
                int retryCount = AppConfigManager.GetConfig<int>("ErrorRetryCount", "Exchange", "Synchronize");
                if (retryCount < 0)
                    retryCount = 0;
                return retryCount;
            }
        }

        #endregion

        #region ctor

        /// <summary>
        /// Instantiate a new exchange manager for sync process
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        public ExchangeManager(Guid? userId = null, CRMContext dbContext = null)
        {
            this.CRMDBContext = dbContext ?? ContextFactory.Current.Context;

            service = new ExchangeService(ExchangeVersion.Exchange2010_SP2)
            {
                Credentials = new NetworkCredential(AppConfigManager.GetConfig<string>("Username", "Exchange", "EWS"), AppConfigManager.GetConfig<string>("Password", "Exchange", "EWS")),
                Url = new Uri(AppConfigManager.GetConfig<string>("ServiceUrl", "Exchange", "EWS"))
            };

            if (userId != null && userId != Guid.Empty)
            {
                var user = CacheManager.UserCollection.FirstOrDefault(f => f.UserId == userId);
                var admin = CacheManager.UserCollection.FirstOrDefault(f => f.UserName.Equals("admin", StringComparison.InvariantCultureIgnoreCase));
                CalendarMgr = new CalendarManager(admin, user);
                SyncTracker = CRMDBContext.Sync_Tracker.FirstOrDefault(f => f.UserId == userId.Value && f.ProviderName == ProviderName);

                foreach (var apt in Enum.GetValues(typeof(AppointmentTypeEnumTP)))
                {
                    AppointmentTypeEnumStrings.Add(apt.ToString());
                }

                foreach (var apt in CacheManager.AppointmentTypes)
                {
                    AppointmentTypeEnumStringsFromCache.Add(apt.Name.ToString());
                }
            }

            //auto accepts unsigned ssl 
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

            AppointmentPropertySet = new PropertySet(AppointmentSchema.Id, AppointmentSchema.Start, AppointmentSchema.End,
                            AppointmentSchema.Subject, AppointmentSchema.IsAllDayEvent, AppointmentSchema.Recurrence, AppointmentSchema.Organizer, AppointmentSchema.OriginalStart,
                            AppointmentSchema.Location, AppointmentSchema.Body, AppointmentSchema.IsReminderSet, AppointmentSchema.ReminderMinutesBeforeStart,
                            AppointmentSchema.LastModifiedTime, AppointmentSchema.DateTimeCreated, AppointmentSchema.RequiredAttendees, AppointmentSchema.OptionalAttendees,
                            AppointmentSchema.Sensitivity, AppointmentSchema.Categories, AppointmentSchema.FirstOccurrence, AppointmentSchema.AppointmentType,
                            AppointmentSchema.ModifiedOccurrences, AppointmentSchema.DeletedOccurrences, AppointmentSchema.AppointmentSequenceNumber);

            AppointmentPropertySet.RequestedBodyType = Microsoft.Exchange.WebServices.Data.BodyType.Text;
        }

        #endregion

        #region CRUD Methods for Appointments

        /// <summary>
        /// Gets a list of changes to be sync
        /// </summary>
        /// <param name="syncState">null state will return all changes</param>
        private ChangeCollection<ItemChange> GetSyncAppointments(string syncState = null)
        {
            if (CalendarMgr != null && CalendarMgr.User != null)
            {
                var icc = service.SyncFolderItems(new FolderId(WellKnownFolderName.Calendar, new Mailbox(CalendarMgr.User.Email)), PropertySet.IdOnly, null, 15, SyncFolderItemsScope.NormalItems, syncState);
                foreach (var record in icc)
                {
                    if (record.ChangeType == ChangeType.Create || record.ChangeType == ChangeType.Update)
                        record.Item.Load(AppointmentPropertySet);
                }
                return icc;
            }
            else
                return null;
        }

        /// <summary>
        /// Gets an appointment by exchange unique id
        /// </summary>
        /// <param name="uniqueId">The unique identifier.</param>
        private Appointment GetAppointment(string uniqueId, string AttendeeEmail)
        {
            if (!string.IsNullOrEmpty(uniqueId))
                return GetAppointment(new ItemId(uniqueId), AttendeeEmail);
            else
                return null;
        }

        /// <summary>
        /// Gets an appointment by exchange item id.
        /// </summary>
        /// <param name="Id">Exchange Item Id</param>
        private Appointment GetAppointment(ItemId Id, string AttendeeEmail = "")
        {
            try
            {
                if (AttendeeEmail != "")
                    service.ImpersonatedUserId = new ImpersonatedUserId(ConnectingIdType.SmtpAddress, AttendeeEmail);
                if (Id != null)
                    return Appointment.Bind(service, Id, AppointmentPropertySet);
                else
                    return null;
            }
            catch (Exception ex)
            {
                ApiEventLogger.LogEvent(ex);
                return null;
            }
        }

        /// <summary>
        /// Use this method to delete single occurrences in an recurring series
        /// </summary>
        /// <param name="uniqueId">Returns the id of the deleted appointment occurrence in recurring series</param>
        /// <param name="recurMasterId">The recur master identifier.</param>
        /// <param name="occurenceNumber">The occurence number.</param>
        /// <returns>System.String.</returns>
        public string DeleteAppointment(out string uniqueId, string recurMasterId, int occurenceNumber)
        {
            uniqueId = "";
            try
            {
                var occurrence = Appointment.BindToOccurrence(service, new ItemId(recurMasterId), occurenceNumber, PropertySet.IdOnly);
                occurrence.Delete(DeleteMode.SoftDelete);
                uniqueId = occurrence.Id.UniqueId;
                return Success;
            }
            catch (Exception ex)
            {
                if (ex.Message.ToString() == "Occurrence with this index was previously deleted from the recurrence.")
                    return "";
                ApiEventLogger.LogEvent(ex);
                throw;
            }
        }

        /// <summary>
        /// Use this method to delete appointments
        /// </summary>
        /// <param name="uniqueId">The unique identifier.</param>
        /// <returns>System.String.</returns>
        public string DeleteAppointment(string uniqueId)
        {
            try
            {
                var responses = service.DeleteItems(new List<ItemId>() { new ItemId(uniqueId) }, DeleteMode.HardDelete, SendCancellationsMode.SendToNone, AffectedTaskOccurrence.AllOccurrences);
                if (responses != null)
                {
                    var res = responses.FirstOrDefault();

                    //we can pass back success since the obj doesnt exist in EWS so technically it's already deleted
                    if (res.Result == ServiceResult.Success || res.ErrorMessage.Contains("The specified object was not found in the store."))
                        return Success;
                    else
                        return res.ErrorMessage;
                }
                else
                    return "Unknown response";
            }
            catch (Exception ex)
            {
                ApiEventLogger.LogEvent(ex);
                throw;
            }
        }

        private static string CreateApptMessageBody(Calendar calendar)
        {
            if (calendar.Lead == null) { return calendar.Message; }
            var preferredPhone = string.Empty;
            var preferred = calendar.Lead.PrimCustomer.PreferredTFN;
            if (!string.IsNullOrEmpty(preferred))
            {
                if (preferred.ToLower() == "c" && !string.IsNullOrEmpty(calendar.Lead.PrimCustomer.CellPhone))
                    preferredPhone = calendar.Lead.PrimCustomer.CellPhone;
                else if (preferred.ToLower() == "h" && !string.IsNullOrEmpty(calendar.Lead.PrimCustomer.HomePhone))
                    preferredPhone = calendar.Lead.PrimCustomer.HomePhone;
                else if (!string.IsNullOrEmpty(calendar.Lead.PrimCustomer.HomePhone))
                    preferredPhone = calendar.Lead.PrimCustomer.HomePhone;
                else if (!string.IsNullOrEmpty(calendar.Lead.PrimCustomer.CellPhone))
                    preferredPhone = calendar.Lead.PrimCustomer.CellPhone;
            }

            var body = calendar.Message;
            body += calendar.Lead.PrimCustomer.FullName + " - ";
            body += preferredPhone;

            return body;
        }

        public string CreateAppointment(Calendar calendar, string AttendeeEmail, string ServiceUrl)
        {
            EventLogger.LogEvent(calendar.CalendarId.ToString(), "CreateAppointment Call started");

            if (calendar == null) return "Calendar data can not be null";

            //if (!IsValidAccount(CalendarMgr.User.UserName, CalendarMgr.User.Email, Domain))
            //    return "Invalid Exchange account";

            // Franchise Timezone
            var timezonecode = calendar.Franchise.TimezoneCode.Description();

            try
            {
                service.Url = new Uri(AppConfigManager.GetConfig<string>(ServiceUrl, "Exchange", "EWS"));
                // Impersonate as Current User
                service.ImpersonatedUserId = new ImpersonatedUserId(ConnectingIdType.SmtpAddress, AttendeeEmail);

                Appointment apt = new Appointment(service);

                // Appointment Subject, Location, Body
                apt.Subject = calendar.Subject != null ? calendar.Subject : "";
                apt.Location = calendar.Location != null ? calendar.Location : "";
                apt.Body = CreateApptMessageBody(calendar);

                // Appointment is Whole day or not
                apt.IsAllDayEvent = calendar.IsAllDay;

                // Appointment Start date time and Timezone
                apt.Start = TimeZoneManager.ToLocal(calendar.StartDate.DateTime, calendar.Franchise.TimezoneCode);
                apt.StartTimeZone = calendar.Franchise.TimezoneCode == TimeZoneEnum.UTC ? TimeZoneInfo.Utc : TimeZoneInfo.FindSystemTimeZoneById(timezonecode);

                // Appointment End date time and Timezone
                if (calendar.IsAllDay)
                {
                    var diffdays = calendar.EndDate.DateTime - calendar.StartDate.DateTime;
                    apt.End = TimeZoneManager.ToLocal(calendar.StartDate.DateTime.AddDays(diffdays.Days + 1), calendar.Franchise.TimezoneCode);
                    apt.EndTimeZone = calendar.Franchise.TimezoneCode == TimeZoneEnum.UTC ? TimeZoneInfo.Utc : TimeZoneInfo.FindSystemTimeZoneById(calendar.Franchise.TimezoneCode.Description());
                }
                else
                {
                    apt.End = TimeZoneManager.ToLocal(calendar.EndDate.DateTime, calendar.Franchise.TimezoneCode);
                    apt.EndTimeZone = calendar.Franchise.TimezoneCode == TimeZoneEnum.UTC ? TimeZoneInfo.Utc : TimeZoneInfo.FindSystemTimeZoneById(timezonecode);
                }

                // Appointment is IsPrivate or Normal
                if (calendar.IsPrivate.HasValue && calendar.IsPrivate.Value)
                    apt.Sensitivity = Sensitivity.Private;
                else
                    apt.Sensitivity = Sensitivity.Normal;

                // Appointment Type
                var apptType = CacheManager.AppointmentTypes.Where(x => x.AppointmentTypeId == (int)calendar.AptTypeEnum).SingleOrDefault();
                if (apptType == null)
                    apptType = CacheManager.AppointmentTypes.Where(x => x.AppointmentTypeId == 1).SingleOrDefault();
                apt.Categories.Add(apptType.Name.ToString());

                if (calendar.Attendees != null && calendar.Attendees.Count > 0)
                {
                    //get get list of ppl who has ids
                    var attendeeToCheck = (from p in calendar.Attendees
                                           from u in CacheManager.UserCollection
                                           where p.PersonId.HasValue && u.PersonId == p.PersonId.Value && u.FranchiseId == calendar.FranchiseId
                                           select u.Email).ToList();

                    //now list of ppl who has email instead of Id
                    attendeeToCheck = attendeeToCheck.Union(calendar.Attendees.Where(w => !w.PersonId.HasValue && !string.IsNullOrEmpty(w.PersonEmail)).Select(s => s.PersonEmail)).ToList();

                    foreach (var email in attendeeToCheck)
                    {
                        if (email != AttendeeEmail)
                        {
                            apt.RequiredAttendees.Add(email);
                        }
                    }
                }

                // Appointment Reminder
                if (calendar.ReminderMinute > 0)
                {
                    apt.IsReminderSet = true;
                    apt.ReminderMinutesBeforeStart = calendar.ReminderMinute;
                }
                else
                {
                    apt.IsReminderSet = false;
                    apt.ReminderMinutesBeforeStart = 0;
                }

                // Appointment Recurring
                if (calendar.EventRecurring != null)
                {
                    // If Daily
                    if (calendar.EventRecurring.PatternEnum == RecurringPatternEnum.Daily)
                    {
                        apt.Recurrence = new Recurrence.DailyPattern(calendar.EventRecurring.StartDate.DateTime, calendar.EventRecurring.RecursEvery);
                    }
                    // If Weekly
                    else if (calendar.EventRecurring.PatternEnum == RecurringPatternEnum.Weekly)
                    {
                        List<DayOfTheWeek> daysOfTheWeek = new List<DayOfTheWeek>();
                        if (calendar.EventRecurring.DayOfWeekEnum.HasValue)
                        {
                            foreach (var dow in calendar.EventRecurring.DayOfWeekEnum.Value.ToList())
                            {
                                daysOfTheWeek.Add((DayOfTheWeek)(Math.Log((byte)dow) / Math.Log(2)));
                            }
                        }
                        else
                            daysOfTheWeek.Add((DayOfTheWeek)((int)calendar.EventRecurring.StartDate.DayOfWeek));

                        apt.Recurrence = new Recurrence.WeeklyPattern(calendar.EventRecurring.StartDate.DateTime, calendar.EventRecurring.RecursEvery, daysOfTheWeek.ToArray());
                    }
                    //If Monthly
                    else if (calendar.EventRecurring.PatternEnum == RecurringPatternEnum.Monthly)
                    {
                        if (calendar.EventRecurring.DayOfWeekIndex.HasValue)
                        {
                            var dotw = (DayOfTheWeek)(Math.Log((byte)calendar.EventRecurring.DayOfWeekEnum) / Math.Log(2));
                            var dotwIndex = (DayOfTheWeekIndex)(calendar.EventRecurring.DayOfWeekIndex - 1);
                            apt.Recurrence = new Recurrence.RelativeMonthlyPattern(calendar.EventRecurring.StartDate.DateTime, calendar.EventRecurring.RecursEvery, dotw, dotwIndex);
                        }
                        else
                        {
                            apt.Recurrence = new Recurrence.MonthlyPattern(calendar.EventRecurring.StartDate.DateTime, calendar.EventRecurring.RecursEvery, calendar.EventRecurring.DayOfMonth.Value);
                        }
                    }
                    // If Yearly
                    else if (calendar.EventRecurring.PatternEnum == RecurringPatternEnum.Yearly)
                    {
                        if (calendar.EventRecurring.DayOfWeekIndex.HasValue)
                        {
                            var dotw = (DayOfTheWeek)(Math.Log((byte)calendar.EventRecurring.DayOfWeekEnum) / Math.Log(2));
                            var dotwIndex = (DayOfTheWeekIndex)(calendar.EventRecurring.DayOfWeekIndex - 1);
                            apt.Recurrence = new Recurrence.RelativeYearlyPattern(calendar.EventRecurring.StartDate.DateTime, (Month)((byte)calendar.EventRecurring.MonthEnum), dotw, dotwIndex);
                        }
                        else
                        {
                            apt.Recurrence = new Recurrence.YearlyPattern(calendar.EventRecurring.StartDate.DateTime, (Month)((byte)calendar.EventRecurring.MonthEnum), calendar.EventRecurring.DayOfMonth.Value);
                        }
                    }
                    // Recurring Number Of Occurrences
                    apt.Recurrence.NumberOfOccurrences = calendar.EventRecurring.EndsAfterXOccurrences;

                    // Check recurring end date
                    if (calendar.EventRecurring.EndsOn.HasValue)
                        apt.Recurrence.EndDate = calendar.EventRecurring.EndsOn.Value.DateTime;
                }

                // Save the appointment
                apt.Save(WellKnownFolderName.Calendar, SendInvitationsMode.SendToNone);

                ////get the new modified date to save for sync
                apt.Load(PropertySet.IdOnly);

                return apt.Id.UniqueId;
            }
            catch (Exception ex)
            {
                ApiEventLogger.LogEvent(ex);
                throw;
            }
        }

        public string UpdateAppointment(Appointment apt, Calendar calendar, string AttendeeEmail, string ServiceUrl)
        {
            if (calendar == null) return DataCannotBeNullOrEmpty;

            if (apt == null) return AppointmentDoesNotExist;

            //if both are same then dont need to update anything
            // if (apt.LastModifiedTime.ToUniversalTime() == calendar.LastUpdatedUtc)
            // for now we only update when the appt has been modified in mycrm
            //  if (apt.LastModifiedTime.ToUniversalTime() > calendar.LastUpdatedUtc)
            //if (calendar.LastUpdatedUtc < calendar.CalendarSyncs.Max(x => x.LastSyncDateUtc))
            //{
            //    EventLogger.LogEvent(calendar.LastUpdatedUtc.ToString() + " < " + calendar.CalendarSyncs.Max(x => x.LastSyncDateUtc).ToString(), "UpdateAppointment", LogSeverity.Information);
            //    return Success;
            //}

            try
            {
                service.Url = new Uri(AppConfigManager.GetConfig<string>(ServiceUrl, "Exchange", "EWS"));
                // Impersonate as Current User
                service.ImpersonatedUserId = new ImpersonatedUserId(ConnectingIdType.SmtpAddress, AttendeeEmail);

                // Appointment Subject, Location, Body
                apt.Subject = calendar.Subject != null ? calendar.Subject : "";
                apt.Location = calendar.Location != null ? calendar.Location : "";
                apt.Body = CreateApptMessageBody(calendar);

                // Appointment is Whole day or not
                apt.IsAllDayEvent = calendar.IsAllDay;

                // Appointment Start date time and Timezone
                apt.Start = TimeZoneManager.ToLocal(calendar.StartDate.DateTime, calendar.Franchise.TimezoneCode);
                apt.StartTimeZone = calendar.Franchise.TimezoneCode == TimeZoneEnum.UTC ? TimeZoneInfo.Utc : TimeZoneInfo.FindSystemTimeZoneById(calendar.Franchise.TimezoneCode.Description());

                // Appointment End date time and Timezone
                if (calendar.IsAllDay)
                {
                    var diffdays = calendar.EndDate.DateTime - calendar.StartDate.DateTime;
                    apt.End = TimeZoneManager.ToLocal(calendar.StartDate.DateTime.AddDays(diffdays.Days + 1), calendar.Franchise.TimezoneCode);
                    apt.EndTimeZone = calendar.Franchise.TimezoneCode == TimeZoneEnum.UTC ? TimeZoneInfo.Utc : TimeZoneInfo.FindSystemTimeZoneById(calendar.Franchise.TimezoneCode.Description());
                }
                else
                {
                    apt.End = TimeZoneManager.ToLocal(calendar.EndDate.DateTime, calendar.Franchise.TimezoneCode);
                    apt.EndTimeZone = calendar.Franchise.TimezoneCode == TimeZoneEnum.UTC ? TimeZoneInfo.Utc : TimeZoneInfo.FindSystemTimeZoneById(calendar.Franchise.TimezoneCode.Description());
                }
                // Appointment is IsPrivate or Normal
                if (calendar.IsPrivate.HasValue && calendar.IsPrivate.Value)
                    apt.Sensitivity = Sensitivity.Private;
                else
                    apt.Sensitivity = Sensitivity.Normal;

                //remove old categories that is an AptTypeEnum //keep this for a while
                foreach (var type in AppointmentTypeEnumStrings)
                {
                    apt.Categories.Remove(type);
                }
                foreach (var type in AppointmentTypeEnumStringsFromCache)
                {
                    apt.Categories.Remove(type);
                }
                apt.Categories.Clear();
                // Appointment Type
                if (!apt.Categories.Any(a => a.Equals(calendar.AptTypeEnum.ToString(), StringComparison.InvariantCultureIgnoreCase)))
                {
                    var apptType = CacheManager.AppointmentTypes.Where(x => x.AppointmentTypeId == (int)calendar.AptTypeEnum).SingleOrDefault();
                    if (apptType == null)
                        apptType = CacheManager.AppointmentTypes.Where(x => x.AppointmentTypeId == 1).SingleOrDefault();
                    apt.Categories.Add(apptType.Name.ToString());
                }

                apt.OptionalAttendees.Clear();
                apt.RequiredAttendees.Clear();
                if (calendar.Attendees != null && calendar.Attendees.Count > 0)
                {
                    //get get list of ppl who has ids
                    var attendeeToCheck = (from p in calendar.Attendees
                                           from u in CacheManager.UserCollection
                                           where p.PersonId.HasValue && u.PersonId == p.PersonId.Value && u.FranchiseId == calendar.FranchiseId
                                           select u.Email).ToList();

                    //now list of ppl who has email instead of Id
                    attendeeToCheck = attendeeToCheck.Union(calendar.Attendees.Where(w => !w.PersonId.HasValue && !string.IsNullOrEmpty(w.PersonEmail)).Select(s => s.PersonEmail)).ToList();

                    foreach (var email in attendeeToCheck)
                    {
                        if (email != AttendeeEmail)
                        {
                            apt.RequiredAttendees.Add(email);
                        }
                    }
                }

                // Appointment Reminder
                if (calendar.ReminderMinute > 0)
                {
                    apt.IsReminderSet = true;
                    apt.ReminderMinutesBeforeStart = calendar.ReminderMinute;
                }
                else
                {
                    apt.IsReminderSet = false;
                    apt.ReminderMinutesBeforeStart = 0;
                }

                // Recurring
                //if (apt.AppointmentType == AppointmentType.RecurringMaster)
                //{
                if (calendar.EventRecurring != null)
                {
                    if (calendar.EventRecurring.PatternEnum == RecurringPatternEnum.Daily)
                    {
                        apt.Recurrence = new Recurrence.DailyPattern(calendar.EventRecurring.StartDate.DateTime, calendar.EventRecurring.RecursEvery);
                    }
                    else if (calendar.EventRecurring.PatternEnum == RecurringPatternEnum.Weekly)
                    {
                        List<DayOfTheWeek> daysOfTheWeek = new List<DayOfTheWeek>();
                        if (calendar.EventRecurring.DayOfWeekEnum.HasValue)
                        {
                            foreach (var dow in calendar.EventRecurring.DayOfWeekEnum.Value.ToList())
                            {
                                daysOfTheWeek.Add((DayOfTheWeek)(Math.Log((byte)dow) / Math.Log(2)));
                            }
                        }
                        else
                            daysOfTheWeek.Add((DayOfTheWeek)((int)calendar.EventRecurring.StartDate.DayOfWeek));

                        apt.Recurrence = new Recurrence.WeeklyPattern(calendar.EventRecurring.StartDate.DateTime, calendar.EventRecurring.RecursEvery, daysOfTheWeek.ToArray());
                    }
                    else if (calendar.EventRecurring.PatternEnum == RecurringPatternEnum.Monthly)
                    {
                        if (calendar.EventRecurring.DayOfWeekIndex.HasValue)
                        {
                            var dotw = (DayOfTheWeek)(Math.Log((byte)calendar.EventRecurring.DayOfWeekEnum) / Math.Log(2));
                            var dotwIndex = (DayOfTheWeekIndex)(calendar.EventRecurring.DayOfWeekIndex - 1);
                            apt.Recurrence = new Recurrence.RelativeMonthlyPattern(calendar.EventRecurring.StartDate.DateTime, calendar.EventRecurring.RecursEvery, dotw, dotwIndex);
                        }
                        else
                        {
                            apt.Recurrence = new Recurrence.MonthlyPattern(calendar.EventRecurring.StartDate.DateTime, calendar.EventRecurring.RecursEvery, calendar.EventRecurring.DayOfMonth.Value);
                        }
                    }
                    else if (calendar.EventRecurring.PatternEnum == RecurringPatternEnum.Yearly)
                    {
                        if (calendar.EventRecurring.DayOfWeekIndex.HasValue)
                        {
                            var dotw = (DayOfTheWeek)(Math.Log((byte)calendar.EventRecurring.DayOfWeekEnum) / Math.Log(2));
                            var dotwIndex = (DayOfTheWeekIndex)(calendar.EventRecurring.DayOfWeekIndex - 1);
                            apt.Recurrence = new Recurrence.RelativeYearlyPattern(calendar.EventRecurring.StartDate.DateTime, (Month)((byte)calendar.EventRecurring.MonthEnum), dotw, dotwIndex);
                        }
                        else
                        {
                            apt.Recurrence = new Recurrence.YearlyPattern(calendar.EventRecurring.StartDate.DateTime, (Month)((byte)calendar.EventRecurring.MonthEnum), calendar.EventRecurring.DayOfMonth.Value);
                        }
                    }

                    apt.Recurrence.NumberOfOccurrences = calendar.EventRecurring.EndsAfterXOccurrences;
                    if (calendar.EventRecurring.EndsOn.HasValue)
                        apt.Recurrence.EndDate = calendar.EventRecurring.EndsOn.Value.DateTime;
                }
                else
                {
                    apt.Recurrence = null;
                }
                //}

                apt.Update(ConflictResolutionMode.AutoResolve, SendInvitationsOrCancellationsMode.SendToNone);
                apt.Load(PropertySet.IdOnly);

                return Success;
            }
            catch (Exception ex)
            {
                ApiEventLogger.LogEvent(ex);
                throw;
            }
        }

        public List<CalendarSync> GetSyncrecord(int CalendarId)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    return connection.Query<CalendarSync>("select * from [API].[CalendarSync] where CalendarId=@CalendarId", new { CalendarId }).ToList();
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return null;
            }
        }

        public void InsertSyncrecord(CalendarSync sync)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Insert(sync);
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
            }
        }
        public void UpdateSyncrecord(CalendarSync sync)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Update(sync);
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
            }
        }

        public void DeleteSyncRecord(int SyncRecId)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Execute("delete from[API].[CalendarSync] where SyncRecId = @SyncRecId", new { SyncRecId });
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
            }

        }

        public bool SendEmail(string From, string Subject, string Body, List<string> ToRecipients, List<string> CcRecipients = null, List<string> BccRecipients = null, string streamid = "")
        {
            try
            {
                //Imporsonate User email to keep emails in sent box
                service.ImpersonatedUserId = new ImpersonatedUserId(ConnectingIdType.SmtpAddress, From);
                // Create an email message and identify the Exchange service.
                EmailMessage message = new EmailMessage(service);

                // Add properties to the email message.
                message.From = From;
                message.Subject = Subject;
                message.Body = Body;

                ToRecipients = ToRecipients.Distinct().ToList();
                if (CcRecipients != null)
                    CcRecipients = CcRecipients.Distinct().ToList();
                if (BccRecipients != null)
                    BccRecipients = BccRecipients.Distinct().ToList();

                if (ToRecipients != null && ToRecipients.Count > 0)
                {
                    foreach (var to in ToRecipients)
                        message.ToRecipients.Add(to);
                }
                else
                    return true;
                if (CcRecipients != null && CcRecipients.Count > 0)
                {
                    foreach (var cc in CcRecipients)
                        message.CcRecipients.Add(cc);
                }
                if (BccRecipients != null && BccRecipients.Count > 0)
                {
                    foreach (var bcc in BccRecipients)
                        message.BccRecipients.Add(bcc);
                }

                if (streamid != "")
                {
                    var data = FileTable.GetFileData(streamid);
                    if (data != null)
                    {
                        message.Attachments.AddFileAttachment(data.fileName, data.bytes);
                    }
                }

                // Send the email message and save a copy.
                //message.SendAndSaveCopy();
                FolderId SentFolderForUser = new FolderId(WellKnownFolderName.SentItems, From);
                message.SendAndSaveCopy(SentFolderForUser);

                EventLogger.LogEvent(From + "||" + string.Join(",", ToRecipients.ToArray()) + "||" + Subject, "SendEmail SendAndSaveCopy", LogSeverity.Information);
                return true;
            }
            catch (Exception ex)
            {
                dynamic obj = new ExpandoObject();
                obj.From = From;
                obj.Subject = Subject;
                obj.ToRecipients = string.Join(",", ToRecipients.ToArray());
                EventLogger.LogEvent(ex, obj: obj);
                return false;
            }
        }

        public void SendEmailMultipleAttachment(string From, string Subject, string Body, List<string> ToRecipients, List<string> CcRecipients = null, List<string> BccRecipients = null, string[] streamid = null, byte[] pdf = null, string filename = "")
        {
            try
            {
                //Imporsonate User email to keep emails in sent box
                service.ImpersonatedUserId = new ImpersonatedUserId(ConnectingIdType.SmtpAddress, From);
                // Create an email message and identify the Exchange service.
                EmailMessage message = new EmailMessage(service);

                // Add properties to the email message.
                message.From = From;
                message.Subject = Subject;
                message.Body = Body;

                ToRecipients = ToRecipients.Distinct().ToList();
                if (CcRecipients != null)
                    CcRecipients = CcRecipients.Distinct().ToList();
                if (BccRecipients != null)
                    BccRecipients = BccRecipients.Distinct().ToList();

                if (ToRecipients != null && ToRecipients.Count > 0)
                {
                    foreach (var to in ToRecipients)
                    {
                        if (!string.IsNullOrEmpty(to) && to != "")
                            message.ToRecipients.Add(to);
                    }
                }
                else
                    return;
                if (CcRecipients != null && CcRecipients.Count > 0)
                {
                    foreach (var cc in CcRecipients)
                    {
                        if (!string.IsNullOrEmpty(cc) && cc != "")
                            message.CcRecipients.Add(cc);
                    }

                }
                if (BccRecipients != null && BccRecipients.Count > 0)
                {
                    foreach (var bcc in BccRecipients)
                    {
                        if (!string.IsNullOrEmpty(bcc) && bcc != "")
                            message.BccRecipients.Add(bcc);
                    }
                }

                if (streamid != null)
                {
                    foreach (var sid in streamid)
                    {
                        var data = FileTable.GetFileData(sid);
                        if (data != null)
                        {
                            message.Attachments.AddFileAttachment(data.fileName, data.bytes);
                        }
                    }
                }

                if (pdf != null)
                {
                    message.Attachments.AddFileAttachment(filename, pdf);
                }
                message.Body.BodyType = BodyType.HTML;
                // Send the email message and save a copy.
                //message.SendAndSaveCopy();
                FolderId SentFolderForUser = new FolderId(WellKnownFolderName.SentItems, From);
                message.SendAndSaveCopy(SentFolderForUser);

                EventLogger.LogEvent(From + "||" + string.Join(",", ToRecipients.ToArray()) + "||" + Subject, "SendEmail SendAndSaveCopy", LogSeverity.Information);
            }
            catch (Exception ex)
            {
                dynamic obj = new ExpandoObject();
                obj.From = From;
                obj.Subject = Subject;
                obj.ToRecipients = string.Join(",", ToRecipients.ToArray());
                EventLogger.LogEvent(ex, obj: obj);
            }
        }

        public void SendVPOErrorEmail(string From, string Subject, string Body, List<string> ToRecipients, List<string> CcRecipients = null, List<string> BccRecipients = null, string streamid = "")
        {
            try
            {
                // Create an email message and identify the Exchange service.
                EmailMessage message = new EmailMessage(service);

                // Add properties to the email message.
                message.From = ToRecipients[0];
                message.Subject = Subject;
                message.Body = Body;

                if (ToRecipients != null && ToRecipients.Count > 0)
                {
                    foreach (var to in ToRecipients)
                        message.ToRecipients.Add(to);
                }
                else
                    return;
                if (CcRecipients != null && CcRecipients.Count > 0)
                {
                    foreach (var cc in CcRecipients)
                        message.CcRecipients.Add(cc);
                }
                if (BccRecipients != null && BccRecipients.Count > 0)
                {
                    foreach (var bcc in BccRecipients)
                        message.BccRecipients.Add(bcc);
                }

                if (streamid != "")
                {
                    var data = FileTable.GetFileData(streamid);
                    if (data != null)
                    {
                        message.Attachments.AddFileAttachment(data.fileName, data.bytes);
                    }
                }

                // Send the email message 
                message.Send();
                EventLogger.LogEvent(From + "||" + string.Join(",", ToRecipients.ToArray()) + "||" + Subject, "SendEmail SendAndSaveCopy", LogSeverity.Information);
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
            }
        }

        #endregion

        #region Misc Helper Methods

        /// <summary>
        /// Converts an exchange appointment to CRM calendar
        /// </summary>
        /// <param name="apt">Source appointment</param>
        private CalendarVM ConvertToCalendar(Appointment apt)
        {
            AppointmentTypeEnumTP apttype = AppointmentTypeEnumTP.Appointment;
            if (apt.Categories != null && apt.Categories.Count > 0)
            {
                var cat = apt.Categories.FirstOrDefault(f => AppointmentTypeEnumStrings.Contains(f.ToLower()));
                Enum.TryParse(cat, out apttype);
            }

            DateTimeOffset startDate, endDate;
            // we need to figure out the correct local time for the correct timezone
            // ie: east coast time zone (-05:00) 
            // apt.Start returns 07:00 AM local time based on PST
            // apt.StartTimeZone used is EST (-05:00)
            // so the correct local time for EST is 10:00 (-05:00 UTC offset)

            // start time zone - server timezone = -05:00 - -08:00 = 03:00
            // start date + server and local offset = 07:00 + 03:00 = 10:00
            //Do we need this
            TimeSpan startOffsetDiff = apt.StartTimeZone.BaseUtcOffset.Subtract(service.TimeZone.BaseUtcOffset);
            //startDate = new DateTimeOffset(apt.Start.Add(startOffsetDiff).Ticks, apt.StartTimeZone.BaseUtcOffset);

            //TimeSpan endOffsetDiff = apt.EndTimeZone.BaseUtcOffset.Subtract(service.TimeZone.BaseUtcOffset);
            //endDate = new DateTimeOffset(apt.End.Add(endOffsetDiff).Ticks, apt.EndTimeZone.BaseUtcOffset);

            var calendarToUpdate = new CalendarVM
            {
                Subject = apt.Subject,
                Message = apt.Body.Text,
                StartDate = apt.Start,
                EndDate = apt.End,
                AptTypeEnum = apttype,
                CreatedOnUtc = apt.DateTimeCreated.ToUniversalTime(),
                IsAllDay = apt.IsAllDayEvent,
                LastUpdatedUtc = apt.LastModifiedTime.ToUniversalTime(),
                RevisionSequence = apt.AppointmentSequenceNumber,
                //LastUpdatedByPersonId = _userToSync.PersonId, //hard to tell this since AD only store LastUpdatedName, not email so we can't convert to a user                
                FranchiseId = CalendarMgr.User.FranchiseId.Value,
                IsPrivate = apt.Sensitivity.HasFlag(Microsoft.Exchange.WebServices.Data.Sensitivity.Private)
            };
            var organizer = CacheManager.UserCollection.FirstOrDefault(f =>
                f.FranchiseId == CalendarMgr.User.FranchiseId &&
                f.Email.Equals(apt.Organizer.Address, StringComparison.InvariantCultureIgnoreCase));

            if (organizer != null)
            {
                calendarToUpdate.CreatedByPersonId = organizer.PersonId;
                calendarToUpdate.OrganizerPersonId = organizer.PersonId;
            }

            calendarToUpdate.OrganizerEmail = apt.Organizer.Address;
            calendarToUpdate.OrganizerName = apt.Organizer.Name;

            if (apt.IsReminderSet)
            {
                calendarToUpdate.ReminderMinute = (short)apt.ReminderMinutesBeforeStart;
                //default to all
                calendarToUpdate.RemindMethodEnum = RemindMethodEnum.Popup | RemindMethodEnum.Email | RemindMethodEnum.SMS;
                calendarToUpdate.FirstRemindDate = apt.Start.AddMinutes(-apt.ReminderMinutesBeforeStart);
            }

            calendarToUpdate.Attendees = new List<EventToPerson>();
            calendarToUpdate.Attendees.Add(new EventToPerson
            {
                PersonEmail = apt.Organizer.Address,
                PersonName = apt.Organizer.Name,
                PersonId = calendarToUpdate.OrganizerPersonId,
                RemindDate = calendarToUpdate.FirstRemindDate,
                RemindMethodEnum = RemindMethodEnum.Popup | RemindMethodEnum.Email | RemindMethodEnum.SMS
            });

            if ((apt.RequiredAttendees != null && apt.RequiredAttendees.Count > 0) || (apt.OptionalAttendees != null && apt.OptionalAttendees.Count > 0))
            {
                List<Attendee> joined = null;
                if (apt.RequiredAttendees != null && apt.OptionalAttendees != null && apt.RequiredAttendees.Count > 0 && apt.OptionalAttendees.Count > 0)
                    joined = apt.RequiredAttendees.Union(apt.OptionalAttendees).ToList();
                else if (apt.RequiredAttendees != null && apt.RequiredAttendees.Count > 0)
                    joined = apt.RequiredAttendees.ToList();
                else
                    joined = apt.OptionalAttendees.ToList();

                foreach (var atndee in joined)
                {
                    var additionalUser = CacheManager.UserCollection.FirstOrDefault(f => f.Email.Equals(atndee.Address, StringComparison.InvariantCultureIgnoreCase) &&
                        f.FranchiseId == CalendarMgr.User.FranchiseId);
                    if (!calendarToUpdate.Attendees.Any(a => a.PersonEmail == atndee.Address))
                    {
                        calendarToUpdate.Attendees.Add(new EventToPerson
                        {
                            PersonEmail = atndee.Address,
                            PersonName = atndee.Name,
                            RemindMethodEnum = RemindMethodEnum.Popup | RemindMethodEnum.Email | RemindMethodEnum.SMS,
                            RemindDate = calendarToUpdate.FirstRemindDate,
                            PersonId = additionalUser != null ? (int?)additionalUser.PersonId : null
                        });
                    }
                }
            }

            if (apt.Recurrence != null && apt.AppointmentType == AppointmentType.RecurringMaster)
            {
                calendarToUpdate.EventRecurring = new EventRecurring
                {
                    CreatedByPersonId = CalendarMgr.User.PersonId,
                    CreatedOnUtc = (DateTime)calendarToUpdate.CreatedOnUtc,
                    EndsAfterXOccurrences = apt.Recurrence.NumberOfOccurrences,
                    EndsOn = apt.Recurrence.EndDate,
                    StartDate = apt.Start,
                    EndDate = apt.End
                };

                //has deleted occurrences so cycle through and match our deleted calendar in crm
                if (apt.DeletedOccurrences != null && apt.DeletedOccurrences.Count > 0)
                {
                    calendarToUpdate.EventRecurring.SingleOccurrences = apt.DeletedOccurrences.Select(s => new CalendarSingleOccurrence
                    {
                        OriginalStartDate = new DateTimeOffset(s.OriginalStart.Add(startOffsetDiff).Ticks, apt.StartTimeZone.BaseUtcOffset),
                        DeletedOnUtc = DateTime.UtcNow
                    }).ToList();
                }

                if (apt.Recurrence is Recurrence.DailyPattern)
                {
                    var pattern = (Recurrence.DailyPattern)apt.Recurrence;
                    calendarToUpdate.EventRecurring.PatternEnum = RecurringPatternEnum.Daily;
                    calendarToUpdate.EventRecurring.RecursEvery = pattern.Interval;
                }
                else if (apt.Recurrence is Recurrence.WeeklyPattern)
                {
                    var pattern = (Recurrence.WeeklyPattern)apt.Recurrence;
                    calendarToUpdate.EventRecurring.PatternEnum = RecurringPatternEnum.Weekly;
                    calendarToUpdate.EventRecurring.RecursEvery = pattern.Interval;

                    //set start date default day of week if there is no collection of DaysOfTheWeek
                    DayOfWeekEnum dowCollection = (DayOfWeekEnum)Math.Pow(2, (int)apt.Recurrence.StartDate.DayOfWeek);
                    if (pattern.DaysOfTheWeek.Count > 0)
                    {
                        dowCollection = (DayOfWeekEnum)Math.Pow(2, (int)pattern.DaysOfTheWeek[0]);
                        for (int i = 1; i < pattern.DaysOfTheWeek.Count; i++)
                        {
                            var dow = pattern.DaysOfTheWeek[i];
                            if (dow == DayOfTheWeek.Sunday)
                                dowCollection = dowCollection | DayOfWeekEnum.Sunday;
                            else if (dow == DayOfTheWeek.Monday)
                                dowCollection = dowCollection | DayOfWeekEnum.Monday;
                            else if (dow == DayOfTheWeek.Tuesday)
                                dowCollection = dowCollection | DayOfWeekEnum.Tuesday;
                            else if (dow == DayOfTheWeek.Wednesday)
                                dowCollection = dowCollection | DayOfWeekEnum.Wednesday;
                            else if (dow == DayOfTheWeek.Thursday)
                                dowCollection = dowCollection | DayOfWeekEnum.Thursday;
                            else if (dow == DayOfTheWeek.Friday)
                                dowCollection = dowCollection | DayOfWeekEnum.Friday;
                            else if (dow == DayOfTheWeek.Saturday)
                                dowCollection = dowCollection | DayOfWeekEnum.Saturday;
                        }
                    }
                    calendarToUpdate.EventRecurring.DayOfWeekEnum = dowCollection;
                }
                else if (apt.Recurrence is Recurrence.MonthlyPattern)
                {
                    var pattern = (Recurrence.MonthlyPattern)apt.Recurrence;
                    calendarToUpdate.EventRecurring.PatternEnum = RecurringPatternEnum.Monthly;
                    calendarToUpdate.EventRecurring.RecursEvery = pattern.Interval;
                    calendarToUpdate.EventRecurring.DayOfMonth = (short)pattern.DayOfMonth;
                }
                else if (apt.Recurrence is Recurrence.RelativeMonthlyPattern)
                {
                    var pattern = (Recurrence.RelativeMonthlyPattern)apt.Recurrence;
                    calendarToUpdate.EventRecurring.PatternEnum = RecurringPatternEnum.Monthly;
                    calendarToUpdate.EventRecurring.RecursEvery = pattern.Interval;
                    calendarToUpdate.EventRecurring.DayOfWeekEnum = (DayOfWeekEnum)Math.Pow(2, (int)pattern.DayOfTheWeek);
                    calendarToUpdate.EventRecurring.DayOfWeekIndex = (DayOfWeekIndexEnum)((int)pattern.DayOfTheWeekIndex + 1);
                }
                else if (apt.Recurrence is Recurrence.YearlyPattern)
                {
                    var pattern = (Recurrence.YearlyPattern)apt.Recurrence;
                    calendarToUpdate.EventRecurring.PatternEnum = RecurringPatternEnum.Yearly;
                    calendarToUpdate.EventRecurring.MonthEnum = (MonthEnum)((int)pattern.Month);
                    calendarToUpdate.EventRecurring.DayOfMonth = (short)pattern.DayOfMonth;
                    //EWS missing Interval for yearly pattern?
                    //var p = apt.Recurrence as Microsoft.Exchange.WebServices.Data.Recurrence.IntervalPattern;
                    //calSync.Calendar.EventRecurring.RecursEvery = p != null ? p.Interval : 1;
                    calendarToUpdate.EventRecurring.RecursEvery = 1;
                }
                else if (apt.Recurrence is Recurrence.RelativeYearlyPattern)
                {
                    var pattern = (Recurrence.RelativeYearlyPattern)apt.Recurrence;
                    calendarToUpdate.EventRecurring.PatternEnum = RecurringPatternEnum.Yearly;
                    calendarToUpdate.EventRecurring.MonthEnum = (MonthEnum)((int)pattern.Month);
                    calendarToUpdate.EventRecurring.DayOfWeekEnum = (DayOfWeekEnum)Math.Pow(2, (int)pattern.DayOfTheWeek);
                    calendarToUpdate.EventRecurring.DayOfWeekIndex = (DayOfWeekIndexEnum)((int)pattern.DayOfTheWeekIndex + 1);
                    //EWS missing Interval for yearly pattern?
                    //var p = apt.Recurrence as Microsoft.Exchange.WebServices.Data.Recurrence.IntervalPattern;
                    //calSync.Calendar.EventRecurring.RecursEvery = p != null ? p.Interval : 1;
                    calendarToUpdate.EventRecurring.RecursEvery = 1;
                }
            }

            return calendarToUpdate;
        }

        /// <summary>
        /// Gets unread email count for Inbox
        /// </summary>
        /// <param name="targetEmail">The target email.</param>
        /// <param name="totalCount">The total count.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public bool GetUnreadEmailCount(string targetEmail, out int totalCount)
        {
            totalCount = 0;
            bool result = false;

            try
            {
                var folderId = new FolderId(WellKnownFolderName.Inbox, new Mailbox(targetEmail));
                Folder folder = Folder.Bind(service, folderId, new PropertySet(FolderSchema.UnreadCount));
                totalCount = folder.UnreadCount;
                result = true;
            }
            catch (ServiceResponseException) { }
            catch (Exception ex) { ApiEventLogger.LogEvent(ex, targetEmail); }

            return result;
        }

        /// <summary>
        /// Performs a pull request to exchange for any changes to update CRM
        /// </summary>
        public void SyncChannel_Down()
        {
            string warning;

            //must have an up to date sync state to continue down sync
            if (SyncTracker != null && !string.IsNullOrEmpty(SyncTracker.LastSyncState))
            {
                try
                {
                    var changes = GetSyncAppointments(SyncTracker.LastSyncState);
                    if (changes.Count > 0)
                    {
                        //group the ids so we can do one time DB call instead of inside the loop
                        var guids = changes.Select(s => s.ItemId.UniqueId).Distinct().ToList();
                        var calSyncList = CRMDBContext.CalendarSyncs.Where(w => guids.Contains(w.ProviderUniqueId) && w.ProviderName == ProviderName).ToList();

                        foreach (var icc in changes)
                        {
                            string result = "";
                            DateTime lastModDateUtc = DateTime.UtcNow;
                            if (icc.Item != null)
                                lastModDateUtc = icc.Item.LastModifiedTime.ToUniversalTime();

                            var calSync = calSyncList.FirstOrDefault(f => f.ProviderUniqueId == icc.ItemId.UniqueId);

                            //if there is no sync record create one for tracking
                            if (calSync == null)
                            {
                                calSync = new CalendarSync
                                {
                                    ProviderUniqueId = icc.ItemId.UniqueId,
                                    ProviderName = ProviderName
                                };
                                CRMDBContext.CalendarSyncs.Add(calSync);
                            }

                            if (icc.ChangeType == ChangeType.Delete)
                            {
                                if (calSync.CalendarId.HasValue)
                                    result = CalendarMgr.DeleteEvent(calSync.CalendarId.Value, lastModDateUtc);
                                else
                                    result = Success;// we dont have a matching record to delete so return success;                                
                            }
                            else if (!calSync.CalendarId.HasValue) //CRM doesn't have a record so we can create one
                            {
                                var apt = icc.Item as Appointment;
                                int calendarId = 0;
                                var convertedCal = ConvertToCalendar(apt);
                                if (apt.AppointmentType == AppointmentType.Single || apt.AppointmentType == AppointmentType.RecurringMaster)
                                    result = CalendarMgr.InsertEvent(out calendarId, convertedCal, out warning);

                                //else do nothing for occurrence and exceptions since they will be in ModifiedOccurrences and 
                                //EWS push will only give us master appointment id to do the rest so look at update for more info below
                                if (result == CalendarManager.Success && calendarId > 0)
                                {
                                    calSync.CalendarId = calendarId;
                                }
                            }
                            //when an occurrence is deleted from a recurring series, it will be flagged as "Update" instead of delete so we need to cycle through deleted to remove from our CRM                            
                            else if (icc.ChangeType == ChangeType.Update && calSync.CalendarId.HasValue)
                            {
                                CRMDBContext.Entry(calSync).Reference("Calendar").Load();
                                var apt = icc.Item as Appointment;

                                //we also give last updated a 10 second tolerance so we dont have duplicates

                                if (apt.LastModifiedTime.Subtract((DateTime)calSync.Calendar.LastUpdatedUtc).TotalSeconds > 10)
                                {
                                    var convertedCal = ConvertToCalendar(apt);

                                    convertedCal.CalendarId = calSync.CalendarId.Value;
                                    //since we're not storing lead id and number information in exchange we need to set it here so the Calendar Manager doesnt clear out the lead id
                                    convertedCal.LeadId = calSync.Calendar.LeadId;
                                    convertedCal.LeadNumber = calSync.Calendar.LeadNumber;
                                    convertedCal.JobId = calSync.Calendar.JobId;
                                    convertedCal.JobNumber = calSync.Calendar.JobNumber;
                                    //set the most current revision number from CRM so we are updating the correct data that has not been changed since we ran this sync
                                    convertedCal.RevisionSequence = calSync.Calendar.RevisionSequence;

                                    #region Update/Add modified occurrences in a series
                                    //has single occurrences so cycle through and match with our system
                                    if (apt.ModifiedOccurrences != null && apt.ModifiedOccurrences.Count > 0)
                                    {
                                        foreach (var occur in apt.ModifiedOccurrences)
                                        {
                                            try
                                            {
                                                var modApt = GetAppointment(occur.ItemId);
                                                if (modApt != null)
                                                {
                                                    var modConvertedApt = ConvertToCalendar(modApt);
                                                    modConvertedApt.RecurringEventId = calSync.Calendar.RecurringEventId;

                                                    TimeSpan startOffsetDiff = modApt.StartTimeZone.BaseUtcOffset.Subtract(service.TimeZone.BaseUtcOffset);
                                                    var originalStartDTO = new DateTimeOffset(modApt.OriginalStart.Add(startOffsetDiff).Ticks, modApt.StartTimeZone.BaseUtcOffset);

                                                    var calOccur = (from oc in CRMDBContext.CalendarSingleOccurrences
                                                                    join s in CRMDBContext.CalendarSyncs.Where(w => w.ProviderName == ProviderName) on oc.CalendarId equals s.CalendarId into leftjoin
                                                                    from sync in leftjoin.DefaultIfEmpty()
                                                                    where oc.RecurringEventId == calSync.Calendar.RecurringEventId &&
                                                                         oc.OriginalStartDate == originalStartDTO
                                                                    select new { ModOccur = oc, SyncTracker = sync, oc.Calendar.RevisionSequence }).FirstOrDefault();

                                                    string subresult = "";
                                                    int newOccurCalId = 0;
                                                    CalendarSingleOccurrence singleOccurrence = null;
                                                    if (calOccur == null)
                                                    {
                                                        subresult = CalendarMgr.InsertEvent(out newOccurCalId, modConvertedApt, out warning);
                                                        if (subresult == CalendarManager.Success && newOccurCalId > 0)
                                                        {
                                                            singleOccurrence = new CalendarSingleOccurrence
                                                            {
                                                                CalendarId = newOccurCalId,
                                                                RecurringEventId = calSync.Calendar.RecurringEventId.Value,
                                                                OriginalStartDate = originalStartDTO
                                                            };
                                                            CRMDBContext.CalendarSingleOccurrences.Add(singleOccurrence);
                                                        }
                                                    }
                                                    else if (modConvertedApt.LastUpdatedUtc > calOccur.ModOccur.LastUpdatedUtc)
                                                    {
                                                        singleOccurrence = calOccur.ModOccur;
                                                        modConvertedApt.CalendarId = calOccur.ModOccur.CalendarId.Value;
                                                        modConvertedApt.RevisionSequence = calOccur.RevisionSequence;

                                                        subresult = CalendarMgr.UpdateEvent(modConvertedApt, out warning, true);
                                                    }

                                                    if (!string.IsNullOrEmpty(subresult)) //only need to do something if there are changes
                                                    {
                                                        CalendarSync occurTracker = calOccur == null ? null : calOccur.SyncTracker;
                                                        if (occurTracker == null)
                                                        {
                                                            occurTracker = new CalendarSync
                                                            {
                                                                ProviderName = ProviderName,
                                                                LastSyncDateUtc = DateTime.UtcNow
                                                            };
                                                            if (singleOccurrence != null && singleOccurrence.CalendarId.HasValue && singleOccurrence.CalendarId.Value > 0)
                                                                occurTracker.CalendarId = singleOccurrence.CalendarId;
                                                            CRMDBContext.CalendarSyncs.Add(occurTracker);
                                                        }
                                                        occurTracker.ProviderUniqueId = modApt.Id.UniqueId;
                                                        occurTracker.ProviderLastModifiedUtc = modConvertedApt.LastUpdatedUtc;
                                                        if (subresult == CalendarManager.Success)
                                                        {
                                                            if (singleOccurrence != null)
                                                                singleOccurrence.LastUpdatedUtc = (DateTime)modConvertedApt.LastUpdatedUtc;
                                                            occurTracker.LastSyncDateUtc = DateTime.UtcNow;
                                                        }
                                                        else
                                                        {
                                                            occurTracker.LastErrorDateUtc = DateTime.UtcNow;
                                                            occurTracker.LastErrorMessage = subresult;
                                                            occurTracker.ErrorCount++;
                                                        }
                                                        CRMDBContext.SaveChanges();
                                                    }
                                                }
                                            }
                                            catch (Exception exx)
                                            {
                                                ApiEventLogger.LogEvent(exx);
                                            }
                                        }
                                    }
                                    #endregion

                                    result = CalendarMgr.UpdateEvent(convertedCal, out warning, true);
                                }
                                //else do nothing if CRM last updated is more current than Exchange last updated
                            }

                            if (result == CalendarManager.Success)
                            {
                                calSync.LastSyncDateUtc = DateTime.UtcNow;
                                calSync.LastErrorMessage = null;
                                calSync.LastErrorDateUtc = null;
                                calSync.ErrorCount = 0;
                                calSync.ProviderLastModifiedUtc = lastModDateUtc;
                            }
                            else if (!string.IsNullOrEmpty(result))
                            {
                                calSync.ErrorCount++;
                                calSync.LastErrorDateUtc = DateTime.UtcNow;
                                calSync.LastErrorMessage = result;
                                //only do exit here if there is less than the max retry error count otherwise we continue to next event
                                if (calSync.ErrorCount <= ErrorRetryCount)
                                    throw new Exception(result);
                            }
                        }
                    }

                    SyncTracker.LastSyncDateUtc = DateTime.UtcNow;
                    SyncTracker.LastSyncState = changes.SyncState;
                }
                catch (Exception ex)
                {
                    SyncTracker.LastErrorDateUtc = DateTime.UtcNow;
                    SyncTracker.LastErrorMessage = ex.Message;
                    SyncTracker.ErrorCount++;
                }

                CRMDBContext.SaveChanges();
            }
        }

        /// <summary>
        /// Performs up sync from CRM to Exchange
        /// </summary>
        /// <param name="calendarId">The calendar identifier.</param>
        /// <returns>System.String.</returns>
        public string SyncChannel_Up(int calendarId)
        {
            EventLogger.LogEvent(calendarId.ToString(), "SyncChannel_Up Call started");
            if (calendarId <= 0)
                return DataCannotBeNullOrEmpty;

            try
            {
                string status = "";

                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {

                    //var calendar = db.Calendars.Where(f => f.CalendarId == calendarId)
                    //                .Include(i => i.Attendees)
                    //                .Include(i => i.CalendarSyncs)
                    //                .Include(i => i.ModifiedOccurrences)
                    //                .Include(i => i.EventRecurring)
                    //                .Include(i => i.EventRecurring.SingleOccurrences)
                    //                .Include(i => i.Franchise)
                    //                .FirstOrDefault();

                    var calendar = connection.Query<Calendar>("select * from CRM.Calendar where CalendarId=@CalendarId", new { CalendarId = calendarId }).FirstOrDefault();
                    calendar.Franchise = connection.Query<Franchise>("select * from CRM.Franchise where FranchiseId=@FranchiseId", new { FranchiseId = calendar.FranchiseId }).FirstOrDefault();
                    calendar.Attendees = connection.Query<EventToPerson>("select * from CRM.EventToPeople where CalendarId=@CalendarId", new { CalendarId = calendarId }).ToList();
                    if (calendar.RecurringEventId != null && calendar.RecurringEventId > 0)
                    {
                        calendar.EventRecurring = connection.Query<EventRecurring>("select * from CRM.EventRecurring where RecurringEventId=@RecurringEventId", new { RecurringEventId = calendar.RecurringEventId }).FirstOrDefault();
                        calendar.ModifiedOccurrences = connection.Query<CalendarSingleOccurrence>("select * from CRM.CalendarSingleOccurrences where RecurringEventId=@RecurringEventId", new { RecurringEventId = calendar.RecurringEventId }).ToList();
                    }

                    if (calendar == null)
                        return CalendarDoesNotExist;


                    // Get EWS API Sync record
                    var syncRecord = GetSyncrecord(calendarId);// calendar.CalendarSyncs.Where(f => f.ProviderName == ExchangeManager.ProviderName).ToList();

                    if (syncRecord == null || syncRecord.Count == 0) //doesn't have record in exchange
                    {
                        if (!calendar.IsDeleted && (calendar.IsCancelled == null || calendar.IsCancelled == false)) //if crm calendar isn't already deleted
                        {
                            // If Insert 
                            if (calendar.EventTypeEnum == EventTypeEnum.Occurrence)
                            {
                                //Delete Occurance in the master Recurrance

                                var calendarMaster = connection.Query<Calendar>("select * from CRM.Calendar where RecurringEventId=@RecurringEventId and EventTypeEnum=2", new { RecurringEventId = calendar.RecurringEventId }).FirstOrDefault();
                                calendarMaster.Franchise = connection.Query<Franchise>("select * from CRM.Franchise where FranchiseId=@FranchiseId", new { FranchiseId = calendarMaster.FranchiseId }).FirstOrDefault();
                                calendarMaster.Attendees = connection.Query<EventToPerson>("select * from CRM.EventToPeople where CalendarId=@CalendarId", new { CalendarId = calendarMaster.CalendarId }).ToList();
                                if (calendarMaster.RecurringEventId != null && calendarMaster.RecurringEventId > 0)
                                {
                                    calendarMaster.EventRecurring = connection.Query<EventRecurring>("select * from CRM.EventRecurring where RecurringEventId=@RecurringEventId", new { RecurringEventId = calendarMaster.RecurringEventId }).FirstOrDefault();
                                    calendarMaster.ModifiedOccurrences = connection.Query<CalendarSingleOccurrence>("select * from CRM.CalendarSingleOccurrences where RecurringEventId=@RecurringEventId", new { RecurringEventId = calendarMaster.RecurringEventId }).ToList();
                                }

                                var syncRecordMaster = GetSyncrecord(calendarMaster.CalendarId);
                                DeleteOccuranceMasterapt(calendarMaster, syncRecordMaster);

                                // Insert Occurance with out recurring info
                                calendar.EventRecurring = null;
                            }

                            CreateEWSAppointment(calendar);

                        }
                    }
                    else //this is an update or delete
                    {
                        if (calendar.IsDeleted || (calendar.IsCancelled.HasValue && calendar.IsCancelled.Value == true))
                        {
                            DeleteAptSyncRecord(syncRecord);

                            if (calendar.EventTypeEnum == EventTypeEnum.Series)
                            {
                                var caloccurencelst = connection.Query<Calendar>("select * from CRM.Calendar where RecurringEventId=@RecurringEventId and EventTypeEnum=1 and IsDeleted=0", new { RecurringEventId = calendar.RecurringEventId }).ToList();

                                foreach (var occ in caloccurencelst)
                                {
                                    var syncRecordOccurence = GetSyncrecord(occ.CalendarId);
                                    DeleteAptSyncRecord(syncRecordOccurence);
                                    occ.IsDeleted = true;
                                    connection.Update(occ);
                                }
                            }
                        }
                        else
                        {
                            if (calendar.EventTypeEnum == EventTypeEnum.Occurrence)
                                calendar.EventRecurring = null;

                            UpdateEWSAppointment(calendar, syncRecord);

                            if (calendar.EventTypeEnum == EventTypeEnum.Series)
                            {
                                if (calendar.ModifiedOccurrences != null && calendar.ModifiedOccurrences.Count > 0)
                                    DeleteOccuranceMasterapt(calendar, syncRecord);
                            }

                        }
                    }

                }
                return status;
            }
            catch (Exception ex)
            {
                ApiEventLogger.LogEvent(ex);
                return ex.Message;
            }
        }

        private void CreateEWSAppointment(Calendar calendar)
        {
            var syncRecordnew = new CalendarSync
            {
                CalendarId = calendar.CalendarId,
                ProviderName = ExchangeManager.ProviderName
            };
            var attendees = calendar.Attendees.ToList();
            foreach (var attendee in attendees)
            {
                try
                {
                    syncRecordnew.ProviderUniqueId = CreateAppointment(calendar, attendee.PersonEmail, "ServiceUrl");
                    syncRecordnew.ProviderLastModifiedUtc = calendar.LastUpdatedUtc;
                    syncRecordnew.LastSyncDateUtc = DateTime.UtcNow;
                    syncRecordnew.LastErrorDateUtc = null;
                    syncRecordnew.LastErrorMessage = null;
                    syncRecordnew.ErrorCount = 0;
                    syncRecordnew.EventPersonId = (int)attendee.PersonId;
                    syncRecordnew.SyncURL = "ServiceUrl";
                    InsertSyncrecord(syncRecordnew);
                }
                catch (Exception e)
                {
                    try
                    {
                        syncRecordnew.ProviderUniqueId = CreateAppointment(calendar, attendee.PersonEmail, "ServiceUrl1");
                        syncRecordnew.ProviderLastModifiedUtc = calendar.LastUpdatedUtc;
                        syncRecordnew.LastSyncDateUtc = DateTime.UtcNow;
                        syncRecordnew.LastErrorDateUtc = null;
                        syncRecordnew.LastErrorMessage = null;
                        syncRecordnew.ErrorCount = 0;
                        syncRecordnew.EventPersonId = (int)attendee.PersonId;
                        syncRecordnew.SyncURL = "ServiceUrl1";
                        InsertSyncrecord(syncRecordnew);
                    }
                    catch (Exception ex)
                    {
                        syncRecordnew.LastErrorDateUtc = DateTime.UtcNow;
                        syncRecordnew.LastErrorMessage = ex.Message.ToString();
                        syncRecordnew.ErrorCount++;
                        InsertSyncrecord(syncRecordnew);
                    }
                }

            }
        }

        private void UpdateEWSAppointment(Calendar calendar, List<CalendarSync> syncRecord)
        {
            var attendees = calendar.Attendees.ToList();

            var syncdelete = syncRecord.Where(x => !attendees.Any(a => a.PersonId == x.EventPersonId)).ToList();
            foreach (var sync in syncdelete)
            {
                try
                {
                    if (sync.ProviderUniqueId != null)
                    {
                        try
                        {
                            DeleteAppointment(sync.ProviderUniqueId);
                        }
                        catch (Exception e1)
                        {
                            try
                            {
                                DeleteAppointment(sync.ProviderUniqueId);
                            }
                            catch (Exception e2)
                            {
                            }
                        }
                    }

                    DeleteSyncRecord(sync.SyncRecId);
                }
                catch (Exception)
                {
                }
            }

            var syncRecordnew = new CalendarSync
            {
                CalendarId = calendar.CalendarId,
                ProviderName = ExchangeManager.ProviderName
            };

            foreach (var attendee in attendees)
            {
                var syncrec = syncRecord.Where(s => s.EventPersonId == attendee.PersonId).FirstOrDefault();

                if (syncrec != null)
                {
                    try
                    {
                        if (syncrec.ProviderUniqueId == null) continue;
                        var app = GetAppointment(syncrec.ProviderUniqueId, attendee.PersonEmail);
                        if (app == null) throw new Exception("app is null GetAppointment");
                        UpdateAppointment(app, calendar, attendee.PersonEmail, "ServiceUrl");

                        syncrec.ProviderLastModifiedUtc = calendar.LastUpdatedUtc;
                        syncrec.LastSyncDateUtc = DateTime.UtcNow;
                        syncrec.LastErrorDateUtc = null;
                        syncrec.LastErrorMessage = null;
                        syncrec.ErrorCount = 0;
                        syncrec.SyncURL = "ServiceUrl";
                        UpdateSyncrecord(syncrec);
                    }
                    catch (Exception e)
                    {
                        try
                        {
                            var app = GetAppointment(syncrec.ProviderUniqueId, attendee.PersonEmail);
                            if (app == null) throw new Exception("app is null GetAppointment");
                            UpdateAppointment(app, calendar, attendee.PersonEmail, "ServiceUrl1");

                            syncrec.ProviderLastModifiedUtc = calendar.LastUpdatedUtc;
                            syncrec.LastSyncDateUtc = DateTime.UtcNow;
                            syncrec.LastErrorDateUtc = null;
                            syncrec.LastErrorMessage = null;
                            syncrec.ErrorCount = 0;
                            syncrec.SyncURL = "ServiceUrl1";
                            UpdateSyncrecord(syncrec);
                        }
                        catch (Exception ex)
                        {
                            syncrec.LastErrorDateUtc = DateTime.UtcNow;
                            syncrec.LastErrorMessage = ex.Message.ToString();
                            syncrec.ErrorCount++;
                            UpdateSyncrecord(syncrec);
                        }
                    }
                }
                else
                {
                    try
                    {
                        syncRecordnew.ProviderUniqueId = CreateAppointment(calendar, attendee.PersonEmail, "ServiceUrl");
                        syncRecordnew.ProviderLastModifiedUtc = calendar.LastUpdatedUtc;
                        syncRecordnew.LastSyncDateUtc = DateTime.UtcNow;
                        syncRecordnew.LastErrorDateUtc = null;
                        syncRecordnew.LastErrorMessage = null;
                        syncRecordnew.ErrorCount = 0;
                        syncRecordnew.EventPersonId = (int)attendee.PersonId;
                        syncRecordnew.SyncURL = "ServiceUrl";
                        InsertSyncrecord(syncRecordnew);
                    }
                    catch (Exception e)
                    {
                        try
                        {
                            syncRecordnew.ProviderUniqueId = CreateAppointment(calendar, attendee.PersonEmail, "ServiceUrl1");
                            syncRecordnew.ProviderLastModifiedUtc = calendar.LastUpdatedUtc;
                            syncRecordnew.LastSyncDateUtc = DateTime.UtcNow;
                            syncRecordnew.LastErrorDateUtc = null;
                            syncRecordnew.LastErrorMessage = null;
                            syncRecordnew.ErrorCount = 0;
                            syncRecordnew.EventPersonId = (int)attendee.PersonId;
                            syncRecordnew.SyncURL = "ServiceUrl1";
                            InsertSyncrecord(syncRecordnew);
                        }
                        catch (Exception ex)
                        {
                            syncRecordnew.LastErrorDateUtc = DateTime.UtcNow;
                            syncRecordnew.LastErrorMessage = ex.Message.ToString();
                            syncRecordnew.ErrorCount++;
                            InsertSyncrecord(syncRecordnew);
                        }
                    }
                }

            }
        }

        //Master calendar obj data
        private void DeleteOccuranceMasterapt(Calendar calendar, List<CalendarSync> syncRecord)
        {
            var attendees = calendar.Attendees.ToList();

            foreach (var attendee in attendees)
            {
                var syncrec = syncRecord.Where(s => s.EventPersonId == attendee.PersonId).FirstOrDefault();

                if (syncrec != null)
                {
                    try
                    {
                        if (syncrec.ProviderUniqueId == null) continue;
                        var masterApt = GetAppointment(syncrec.ProviderUniqueId, attendee.PersonEmail);
                        foreach (var occur in calendar.ModifiedOccurrences)
                        {
                            occur.OriginalStartDate = DateTime.SpecifyKind(occur.OriginalStartDate.DateTime, DateTimeKind.Utc);
                            //if not already deleted
                            if (masterApt.DeletedOccurrences == null || !masterApt.DeletedOccurrences.Any(a => a.OriginalStart == occur.OriginalStartDate.LocalDateTime))
                            {
                                var computedOccurrence = calendar.EventRecurring.GetOccurrenceAt(TimeZoneManager.ToLocal(occur.OriginalStartDate));
                                if (computedOccurrence != null)
                                {
                                    string deletedAptUniqueId = "";
                                    DeleteAppointment(out deletedAptUniqueId, syncrec.ProviderUniqueId, computedOccurrence.OccurrenceNumber);
                                }
                            }
                        }

                    }
                    catch (Exception e)
                    {
                        try
                        {
                            var masterApt = GetAppointment(syncrec.ProviderUniqueId, attendee.PersonEmail);
                            foreach (var occur in calendar.ModifiedOccurrences)
                            {
                                //if not already deleted
                                if (masterApt.DeletedOccurrences == null || !masterApt.DeletedOccurrences.Any(a => a.OriginalStart == occur.OriginalStartDate.LocalDateTime))
                                {
                                    var computedOccurrence = calendar.EventRecurring.GetOccurrenceAt(TimeZoneManager.ToLocal(occur.OriginalStartDate));
                                    if (computedOccurrence != null)
                                    {
                                        string deletedAptUniqueId = "";
                                        DeleteAppointment(out deletedAptUniqueId, syncrec.ProviderUniqueId, computedOccurrence.OccurrenceNumber);
                                    }
                                }
                            }

                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }


            }
        }

        private void DeleteAptSyncRecord(List<CalendarSync> syncRecord)
        {
            if (syncRecord != null && syncRecord.Count > 0)
            {
                foreach (var sync in syncRecord)
                {
                    try
                    {
                        if (sync.ProviderUniqueId != null)
                            try
                            {
                                DeleteAppointment(sync.ProviderUniqueId);
                            }
                            catch (Exception e1)
                            {
                                try
                                {
                                    DeleteAppointment(sync.ProviderUniqueId);
                                }
                                catch (Exception e2)
                                {
                                }
                            }
                        DeleteSyncRecord(sync.SyncRecId);
                    }
                    catch (Exception)
                    {

                    }
                }
            }
        }

        public string SyncChannel_UpAssignedChanged(int calendarId, int historyId)
        {
            if (calendarId <= 0)
                return DataCannotBeNullOrEmpty;

            try
            {
                string status = "";

                var calendar = CRMDBContext.Calendars.Where(f => f.CalendarId == calendarId)
                                .Include(i => i.CalendarSyncs)
                                .FirstOrDefault();
                if (calendar == null)
                    return CalendarDoesNotExist;


                var syncRecord = calendar.CalendarSyncs.FirstOrDefault(f => f.ProviderName == ExchangeManager.ProviderName);

                if (syncRecord != null && !string.IsNullOrEmpty(syncRecord.ProviderUniqueId))
                {
                    try
                    {
                        status = DeleteAppointment(syncRecord.ProviderUniqueId);
                    }
                    catch (Exception ex)
                    {
                        ApiEventLogger.LogEvent(ex);
                        status = ExchangeManager.AppointmentDoesNotExist;
                    }

                    if (status == ExchangeManager.Success)
                    {
                        checkForRemoveFromHistory(historyId);
                        syncRecord.ProviderLastModifiedUtc = calendar.LastUpdatedUtc;
                        syncRecord.LastSyncDateUtc = DateTime.UtcNow;
                        syncRecord.LastErrorDateUtc = null;
                        syncRecord.LastErrorMessage = null;
                        syncRecord.ErrorCount = 0;
                    }
                    else
                    {
                        syncRecord.LastErrorDateUtc = DateTime.UtcNow;
                        syncRecord.LastErrorMessage = status;
                        syncRecord.ErrorCount++;
                    }

                    CRMDBContext.SaveChanges();
                }

                return status;
            }
            catch (Exception ex)
            {
                ApiEventLogger.LogEvent(ex);
                return ex.Message;
            }

        }

        private void checkForRemoveFromHistory(int historyId)
        {
            var entity = CRMDBContext.EventToPeople_History.FirstOrDefault(v => v.EventToPeople_HistoryID == historyId);
            if (entity.IsExchangeSynced.HasValue && entity.IsExchangeSynced.Value == true)
            {
                CRMDBContext.EventToPeople_History.Remove(entity); /// Next time just delete the history record? 
            }
            else
            {
                entity.IsExchangeSynced = true;
            }
            CRMDBContext.SaveChanges();
        }

        #endregion

        #region Static Method

        /// <summary>
        /// Push CRM calendar changes up to exchange
        /// </summary>
        public static string PushUp()
        {
            using (var db = new CRMContextEx())
            {
                try
                {

                    var calToPushItems = from c in db.Calendars
                                         join user in db.Users
                                         on c.AssignedPersonId equals user.PersonId
                                         where (
                                         //c.CalendarId == 16203628 &&
                                         user.IsDeleted == false &&
                                         user.Domain == ExchangeManager.Domain &&
                                         user.DomainPath == "LDAP://bbi.corp/DC=bbi,DC=corp" &&
                                         user.SyncStartsOnUtc.HasValue)
                                           && user.CalSync == true &&
                                           user.SyncStartsOnUtc != null &&
                                           c.LastUpdatedUtc > user.SyncStartsOnUtc &&
                                           c.IsDeleted == false &&
                                           c.OrganizerName != null &&
                                           c.OrganizerEmail != null &&
                                            !(from sync in db.CalendarSyncs
                                              where
                                               //sync.CalendarId == 16246084 &&
                                               (Math.Abs(sync.ErrorCount) >= 10 && (sync.ErrorCount < 0) && sync.ProviderUniqueId == null) ||
                                                (sync.ProviderUniqueId != null && ProviderName == ExchangeManager.ProviderName)
                                              select sync.CalendarId).Contains(c.CalendarId)
                                         select new { user.UserId, c.CalendarId };

                    var calToAdd = calToPushItems.ToList();

                    //this get all updated calendars
                    var calToUpdateItems = from c in db.Calendars
                                           join user in db.Users on c.AssignedPersonId equals user.PersonId
                                           join sync in db.CalendarSyncs on c.CalendarId equals sync.CalendarId
                                           where (
                                           //c.CalendarId == 16203628 &&
                                           Math.Abs(sync.ErrorCount) <= 10 &&
                                           user.IsDeleted == false &&
                                           c.LastUpdatedUtc > sync.LastSyncDateUtc &&
                                           user.Domain == ExchangeManager.Domain &&
                                           user.DomainPath == "LDAP://bbi.corp/DC=bbi,DC=corp" &&
                                           user.SyncStartsOnUtc.HasValue) &&
                                           user.CalSync == true &&
                                           user.SyncStartsOnUtc != null &&
                                           c.LastUpdatedUtc > user.SyncStartsOnUtc &&
                                           c.OrganizerName != null &&
                                           c.OrganizerEmail != null
                                           select new { user.UserId, c.CalendarId };


                    /* //This code does not make any sense. For more details go to JIRA MYC30-140
                    var assignedChangedGoogleUsers = (from c in db.Calendars
                                join history in db.EventToPeople_History
                                 .Where(v => v.IsGoogleSynced == null || v.IsGoogleSynced.Value == false)
                                         on c.CalendarId equals history.CalendarId
                                join user in db.Users on c.AssignedPersonId equals user.PersonId
                                where (
                                user.IsDeleted == false &&
                                user.Domain == ExchangeManager.Domain &&
                                user.DomainPath == "LDAP://bbi.corp/DC=bbi,DC=corp" &&
                                user.SyncStartsOnUtc.HasValue) &&
                                user.CalSync == true &&
                                c.OrganizerName != null &&
                                c.OrganizerEmail != null
                                select new { user.UserId, user.Domain, c.CalendarId, history.EventToPeople_HistoryID }).ToList();      
                                */

                    //Records which are deleted or cancelled appointments
                    var test = (from c in db.Calendars
                                join sync in db.CalendarSyncs on c.CalendarId equals sync.CalendarId
                                join history in db.EventToPeople_History
                                        on c.CalendarId equals history.CalendarId
                                join user in db.Users on history.PersonId equals user.PersonId
                                where (
                                user.IsDeleted == false &&
                                (c.IsDeleted == true || c.IsCancelled == true) &&
                                (!history.IsExchangeSynced.HasValue || history.IsExchangeSynced.Value == false) &&
                                user.Domain == ExchangeManager.Domain &&
                                user.DomainPath == "LDAP://bbi.corp/DC=bbi,DC=corp" &&
                                user.SyncStartsOnUtc.HasValue) &&
                                user.CalSync == true && sync.ProviderName == ExchangeManager.ProviderName &&
                                c.OrganizerName != null &&
                                c.OrganizerEmail != null &&
                                (Math.Abs(sync.ErrorCount) <= 10) && (sync.ErrorCount >= 0)
                                select new { user.UserId, c.CalendarId, history.EventToPeople_HistoryID });
                    var apptDeletedOrCanceledAssigneed = test.ToList();
                    var calToUpdate = calToUpdateItems.ToList();
                    var calToPush = calToAdd.Concat(calToUpdate).ToList();

                    if (calToPush != null && calToPush.Count > 0)
                    {
                        foreach (var cal in calToPush.GroupBy(g => g.UserId))
                        {
                            ExchangeManager mgr = new ExchangeManager(userId: cal.Key);
                            foreach (var user in cal)
                            {
                                mgr.SyncChannel_Up(user.CalendarId);
                            }
                        }
                    }
                    /* //This code does not make any sense. For more details go to JIRA MYC30-140
                    if (assignedChangedGoogleUsers != null && assignedChangedGoogleUsers.Count > 0)
                    {
                        foreach (var cal in assignedChangedGoogleUsers.GroupBy(g => g.UserId))
                        {

                            foreach (var user in cal)
                            {
                                if (user.Domain != ExchangeManager.Domain)
                                {
                                    GoogleManager mgr = new GoogleManager(cal.Key.ToString());
                                    mgr.SyncChannel_UpAssignedChanged(user.CalendarId, user.EventToPeople_HistoryID);
                                }
                            }
                        }
                    }
                    */
                    if (apptDeletedOrCanceledAssigneed != null && apptDeletedOrCanceledAssigneed.Count > 0)
                    {
                        foreach (var cal in apptDeletedOrCanceledAssigneed.GroupBy(g => g.UserId))
                        {
                            ExchangeManager ExManager = new ExchangeManager(userId: cal.Key);
                            foreach (var user in cal)
                            {
                                ExManager.SyncChannel_UpAssignedChanged(user.CalendarId, user.EventToPeople_HistoryID);
                            }
                        }
                    }
                    return Success;
                }
                catch (Exception ex)
                {
                    return ApiEventLogger.LogEvent(ex);
                }
            }
        }

        public static string PullDown()
        {
            if (!ExchangeManager.IsEnabled)
                return "Exchange Sync not enabled";

            try
            {
                using (var db = new CRMContextEx())
                {
                    //get any user that is active, with sync start date less than now, is either not already connected or the connection has already expired
                    var linq = (from u in db.Users
                                from t in u.Sync_Trackers.Where(w => w.ProviderName == ExchangeManager.ProviderName).DefaultIfEmpty()
                                where !u.IsDeleted && u.SyncStartsOnUtc.HasValue && u.SyncStartsOnUtc.Value < DateTime.Now &&
                                    u.Domain == ExchangeManager.Domain &&
                                    u.DomainPath == @"LDAP://bbi.corp/DC=bbi,DC=corp"
                                //&&
                                //(t == null || 
                                //(t.ErrorCount < ExchangeManager.ErrorRetryCount && (t.IsConnected == false || t.ExpiresOnUtc < DateTime.UtcNow)
                                //)
                                select new { u.UserId, u.Email, Tracker = t }).ToList();

                    foreach (var user in linq)
                    {
                        var tracker = user.Tracker;
                        if (tracker == null)
                        {
                            tracker = new Sync_Tracker
                            {
                                UserId = user.UserId,
                                ProviderName = ExchangeManager.ProviderName
                            };
                            db.Sync_Tracker.Add(tracker);
                        }

                        try
                        {
                            var folderId = new FolderId(WellKnownFolderName.Calendar, new Mailbox(user.Email));
                            string syncState = tracker.LastSyncState;

                            var exchangeMgr = new ExchangeManager(user.UserId);
                            if (string.IsNullOrEmpty(syncState)) //requires a first time item sync so we are caught up
                            {
                                try
                                {
                                    var hasChanges = true;
                                    do
                                    {
                                        var icc = exchangeMgr.service.SyncFolderItems(folderId, PropertySet.IdOnly, null, 500, SyncFolderItemsScope.NormalItems, syncState);
                                        syncState = icc.SyncState;
                                        hasChanges = icc.MoreChangesAvailable;
                                    }
                                    while (hasChanges);
                                }
                                catch (Exception loopex)
                                {
                                    ApiEventLogger.LogEvent(loopex, user.Email);
                                }
                            }

                            tracker.LastSyncState = syncState;
                            exchangeMgr.SyncChannel_Down();
                        }
                        catch (Exception ex)
                        {
                            tracker.LastErrorMessage = ex.Message;
                            tracker.LastErrorDateUtc = DateTime.UtcNow;
                            tracker.ErrorCount++;
                        }
                        db.SaveChanges();
                    }
                }

                return Success;
            }
            catch (Exception ex)
            {
                return ApiEventLogger.LogEvent(ex);
            }
        }

        #endregion


        #region Sync Failed Items Resync
        public bool SyncFailedCalendarAppointmentsByTime()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = string.Empty;

                query = @"exec CRM.SyncFailedCalendarAppointmentsByTime";

                connection.Open();
                var result = connection.Query<Calendar>(query).ToList();
                connection.Close();
                foreach (var item in result)
                {
                    try
                    {
                        ExchangeManager mgr = new ExchangeManager();
                        {
                            mgr.SyncChannel_Up(item.CalendarId);
                        }
                    }
                    catch (Exception ex)
                    {
                        EventLogger.LogEvent(ex);
                        continue;
                    }
                }
                return true;
            }

        }

        public bool BatchSyncFailedCalendarAppointments()
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    var query = string.Empty;

                    query = @"exec CRM.SyncFailedCalendarAppointments";

                    var result = connection.Query<SyncfailedEmail>(query).ToList();

                    // if no records return
                    if (result.Count == 0)
                        return true;

                    StringBuilder sb = new StringBuilder();
                    sb.Append("<table border='1'>");
                    sb.Append("<tr>");
                    sb.Append("<th>FranchiseName</th>");
                    sb.Append("<th>Subject</th>");
                    sb.Append("<th>Message</th>");
                    sb.Append("<th>AppointmentDate</th>");
                    sb.Append("<th>OrganizerName</th>");
                    sb.Append("<th>OrganizerEmail</th>");
                    sb.Append("<th>Attendees</th>");
                    sb.Append("<th>AttendeesEmail</th>");
                    sb.Append("</tr>");
                    foreach (var item in result)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>" + Convert.ToString(item.FranchiseName) + "</td>");
                        sb.Append("<td>" + Convert.ToString(item.Subject) + "</td>");
                        sb.Append("<td>" + Convert.ToString(item.Message) + "</td>");
                        sb.Append("<td>" + Convert.ToDateTime(item.AppointmentDate).GlobalDateFormat() + "</td>");
                        sb.Append("<td>" + Convert.ToString(item.OrganizerName) + "</td>");
                        sb.Append("<td>" + Convert.ToString(item.OrganizerEmail) + "</td>");
                        sb.Append("<td>" + Convert.ToString(item.Attendees) + "</td>");
                        sb.Append("<td>" + Convert.ToString(item.AttendeesEmail) + "</td>");
                        sb.Append("</tr>");
                    }
                    sb.Append("</table>");

                    string fromemail = AppConfigManager.GetConfig<string>("HFCAdminEmailFromEamil", "Exchange", "EWS");
                    string subject = "Failed Calendar Sync Appointment";
                    List<string> toEmail = AppConfigManager.GetConfig<string>("HFCAdminEmail", "Exchange", "EWS").Split(',').ToList();

                    // sent email
                    SendEmail(fromemail, subject, sb.ToString(), toEmail);

                    // update sync table email send
                    connection.Execute("update API.CalendarSync set EmailSent=1 where SyncRecId in @SyncRecId", new { SyncRecId = result.Select(x => x.SyncRecId).ToArray() });

                    return true;
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }

        }
        #endregion
    }
}
