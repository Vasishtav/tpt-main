﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.IO;
using HFC.CRM.DTO;

namespace HFC.CRM.Managers
{
    public class AdditionalAddressManager : DataManager<AddressTP>
    {
        public AdditionalAddressManager(User user, Franchise franchise) : this(user, user, franchise)
        {
        }

        public AdditionalAddressManager(User user, User authorizingUser, Franchise franchise)
        {
            base.User = user;
            base.AuthorizingUser = authorizingUser;
            base.Franchise = franchise;
        }
        //global search
        public List<AddressTP> GetGlobalAddress(int AddressId)
        {
            List<AddressTP> contacts = new List<AddressTP>();

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"SELECT 
                    ADR.AddressId
                    , ADR.AttentionText
                    , ADR.IsValidated
                    , ADR.Address1
                    , ADR.Address2
                    , ISNULL(ADR.City,'') AS 'City'
                    , ISNULL(ADR.State,'') AS 'State'
                    , ADR.ZipCode, ADR.CountryCode2Digits
                    , ADR.CreatedOnUtc, ADR.IsDeleted
                    , CASE IsResidential WHEN 1 THEN 'Residential' ELSE 'Commercial' END AS 'AddressType'
                    , ADR.IsResidential
                    , ADR.CrossStreet
                    , ADR.Location
                    , ADR.IsInstallationAddress
                    , ADR.CreatedBy
                    , ADR.UpdatedOn
                    --, LAR.LeadId AS 'LeadId'
                    , ADR.ContactId
                    , cus.FirstName+' '+cus.LastName as ContactName
                FROM CRM.Addresses ADR 
                --INNER JOIN CRM.LeadAddresses LAR ON ADR.AddressId = LAR.AddressId
                LEFT JOIN CRM.Customer cus
				on cus.CustomerId = ADR.ContactId
                where ADR.AddressId = @AddressId";
               
                contacts = connection.Query<AddressTP>(query,
                    new
                    {
                        AddressId = AddressId
                    }
                    ).ToList();
                foreach (var add in contacts)
                {
                    var fullAddress = "";
                    if (add.Address1 != null)
                    {
                        fullAddress += add.Address1 + ",";
                    }
                    if (add.Address2 != null)
                    {
                        fullAddress += add.Address2 + ",";
                    }
                    if (add.City != null)
                    {
                        fullAddress += add.City + ",";
                    }
                    if (add.State != null)
                    {
                        fullAddress += add.State + ",";
                    }
                    if (add.ZipCode != null)
                    {
                        fullAddress += add.ZipCode;
                    }
                    add.FullAddress = fullAddress;
                }
                connection.Close();
            }

            return contacts;
        }

        public List<AddressTP> GetLeadAddress(int leadId, bool includeDeleted)
        {
            List<AddressTP> contacts = new List<AddressTP>();

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"SELECT 
                    ADR.AddressId
                    , ADR.AttentionText
                    , ADR.IsValidated
                    , ADR.Address1
                    , ADR.Address2
                    , ISNULL(ADR.City,'') AS 'City'
                    , ISNULL(ADR.State,'') AS 'State'
                    , ADR.ZipCode, ADR.CountryCode2Digits
                    , ADR.CreatedOnUtc, ADR.IsDeleted
                    , CASE IsResidential WHEN 1 THEN 'Residential' ELSE 'Commercial' END AS 'AddressType'
                    , ADR.IsResidential
                    , ADR.CrossStreet
                    , ADR.Location
                    , ADR.IsInstallationAddress
                    , ADR.CreatedBy
                    , ADR.UpdatedOn
                    , LAR.LeadId AS 'LeadId'
                    , ADR.ContactId
                    , cus.FirstName+' '+cus.LastName as ContactName
                FROM CRM.Addresses ADR INNER JOIN CRM.LeadAddresses LAR 
                ON ADR.AddressId = LAR.AddressId
                LEFT JOIN CRM.Customer cus
				on cus.CustomerId = ADR.ContactId
                WHERE LAR.LeadId = @leadId
                AND ADR.AddressId NOT IN (SELECT MIN(AddressId) FROM CRM.LeadAddresses WHERE LeadId = @leadId)";
                if (!includeDeleted) query += " and ADR.IsDeleted <> 1";

                contacts = connection.Query<AddressTP>(query, 
                    new {
                        leadId = leadId }
                    ).ToList();
                foreach(var add in contacts)
                {
                    var fullAddress = "";
                    if(add.Address1 != null)
                    {
                        fullAddress += add.Address1+",";
                    }
                    if(add.Address2 != null)
                    {
                        fullAddress += add.Address2 + ",";
                    }
                    if(add.City != null)
                    {
                        fullAddress += add.City + ",";
                    }
                    if(add.State != null)
                    {
                        fullAddress += add.State + ",";
                    }
                    if(add.ZipCode != null)
                    {
                        fullAddress += add.ZipCode;
                    }
                    add.FullAddress = fullAddress;
                }
                connection.Close();
            }

            return contacts;
        }

        public List<AddressTP> GetAccountAddress(int accountId, bool includeDeleted)
        {
            List<AddressTP> addressList = new List<AddressTP>();

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"  SELECT ADR.AddressId
                                ,ADR.IsValidated
                                ,ADR.AttentionText
                                , ADR.Address1
                                , ADR.Address2
                                , ISNULL(ADR.City,'') AS 'City'
                                , ISNULL(ADR.State,'') AS 'State'
                                , ADR.ZipCode, ADR.CountryCode2Digits
                                , ADR.CreatedOnUtc, ADR.IsDeleted
                                , CASE IsResidential WHEN 1 THEN 'Residential' ELSE 'Commercial' END AS 'AddressType'
                                , ADR.IsResidential
                                , ADR.CrossStreet
                                , ADR.Location
                                , ADR.IsInstallationAddress
                                , ADR.CreatedBy
                                , ADR.UpdatedOn
                                , ADR.ContactId
                                , cus.FirstName+' '+cus.LastName as ContactName
                                , LAR.[AccountId] AS 'LeadId'FROM CRM.Addresses ADR INNER JOIN CRM.[AccountAddresses] LAR 
                                ON ADR.AddressId = LAR.AddressId
                                LEFT JOIN CRM.Customer cus
				                on cus.CustomerId = ADR.ContactId
                                WHERE LAR.[AccountId] = @accountId
                                AND ADR.AddressId NOT IN (SELECT MIN(AddressId) FROM CRM.[AccountAddresses] WHERE [AccountId] = @accountId)";
                if (!includeDeleted) query += " and ADR.IsDeleted <> 1";
                addressList = connection.Query<AddressTP>(query, new { accountId = accountId }).ToList();
                foreach (var add in addressList)
                {
                    var fullAddress = "";
                    if (add.Address1 != null)
                    {
                        fullAddress += add.Address1 + ",";
                    }
                    if (add.Address2 != null)
                    {
                        fullAddress += add.Address2 + ",";
                    }
                    if (add.City != null)
                    {
                        fullAddress += add.City + ",";
                    }
                    if (add.State != null)
                    {
                        fullAddress += add.State + ",";
                    }
                    if (add.ZipCode != null)
                    {
                        fullAddress += add.ZipCode;
                    }
                    add.FullAddress = fullAddress;
                }
                connection.Close();
            }

            return addressList;
        }
        public List<CustomerTP> GetLeadContactsIncludingPrimary(int leadId, bool includeDeleted)
        {
            List<CustomerTP> contacts = new List<CustomerTP>();

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select * from crm.customer c join crm.leadcustomers lc 
                            on c.CustomerId = lc.CustomerId
                            where lc.LeadId = @leadId";
                if (!includeDeleted) query += " and IsDeleted <> 1";

                contacts = connection.Query<CustomerTP>(query, new { leadId = leadId }).ToList();
                connection.Close();
            }

            return contacts;
        }

        public List<CustomerTP> GetAccountContactsIncludingPrimary(int accountId, bool includeDeleted)
        {
            List<CustomerTP> contacts = new List<CustomerTP>();

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select * from crm.customer c join crm.accountcustomers ac 
                            on c.CustomerId = ac.CustomerId
                            where ac.AccountId = @accountId";
                if (!includeDeleted) query += " and IsDeleted <> 1";
                contacts = connection.Query<CustomerTP>(query, new { accountId = accountId }).ToList();
                connection.Close();
            }

            return contacts;
        }

        public override string Add(AddressTP model)
        {
            throw new NotImplementedException();
        }

        public int AddAddress(AddressTP model)
        {
            var addressId = 0;
            using (SqlConnection connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                addressId = (int)connection.Insert(model);

                switch (model.AssociatedSource)
                {
                    case CustomerSource.Lead:
                        connection.Query(@"INSERT INTO CRM.LeadAddresses VALUES (
                                        @leadid, @AddressId, @IsPrimaryAddress)"
                        , new
                        {
                            leadid = model.LeadId
                            ,
                            AddressId = addressId,
                            IsPrimaryAddress = 0
                        });
                        break;
                    case CustomerSource.Account:
                        connection.Query(@"Insert into [CRM].[AccountAddresses] Values(@AccountId,@AddressId,@IsPrimaryAddress)"
                                , new
                                {
                                    AccountId = model.AccountId,
                                    AddressId = addressId,
                                    IsPrimaryAddress = 0
                                });
                        break;
                    default:
                        break;
                }
            }

            return addressId;
        }

        public override string Update(AddressTP model)
        {
            using (SqlConnection connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var addressId = (int)connection.Update(model);
                //var IsResidential = 0;
                //if (model.AddressType == "Residential")
                //{
                //    IsResidential = 1;
                //}
                //else
                //{
                //    IsResidential = 0;
                //}
                //
                var query = @"UPDATE CRM.Addresses SET
                            AttentionText=@AttentionText
                            , Address1=@Address1
                            , Address2=@Address2
                            , City=@City
                            , State=@State
                            , ZipCode=@ZipCode
                            , CountryCode2Digits=@CountryCode2Digits
                            , IsResidential=@IsResidential
                            , CrossStreet=@CrossStreet
                            , Location = @location
                            , Updatedon=@UpdatedOn
                            ,IsInstallationAddress=@IsInstallationAddress
                            WHERE AddressId=@AddressId";
                connection.Execute(query, new
                {
                    AddressId = model.AddressId
                    , AttentionText = model.AttentionText
                    , Address1 = model.Address1
                    , Address2 = model.Address2
                    , City = model.City
                    , State = model.State
                    , ZipCode = model.ZipCode
                    , CountryCode2Digits = model.CountryCode2Digits
                    , IsResidential = model.IsResidential
                    , CrossStreet = model.CrossStreet
                    , location = model.Location
                    , UpdatedOn = model.CreatedOnUtc
                    ,IsInstallationAddress=model.IsInstallationAddress
                    , UpdatedBy = model.CreatedBy
                });                 
            }
            return Success;
        }

        public override string Delete(int id)
        {
            using (SqlConnection connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {

                var query = @"update crm.Addresses set IsDeleted = 1
                            where AddressId = @addressId";

                connection.Execute(query, new
                {
                    addressId = id,
                });
            }

            return Success;
        }

        public string Recover(int id)
        {
            using (SqlConnection connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = @"update crm.Addresses set IsDeleted = 0
                            where AddressId = @addressId";

                connection.Execute(query, new
                {
                    addressId = id,
                });
            }

            return Success;
        }

        public bool CanbeDeleted(int addressId)
        {
            var result = false;

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select count(1) from crm.Opportunities 
                            where InstallationAddressId = @addressId or BillingAddressId = @addressId";
                result = connection.ExecuteScalar<bool>(query, new { addressId = addressId });
                connection.Close();
            }

            return !result;
        }

        public override ICollection<AddressTP> Get(List<int> idList)
        {
            throw new NotImplementedException();
        }

        public override AddressTP Get(int id)
        {
            throw new NotImplementedException();
        }
        public CustomerTP GetContactDetails(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                
                connection.Open();
                string query = @"Select CustomerId,isnull(FirstName,'') as FirstName,isnull(LastName,'') as LastName,isnull(MI,'') as MI
                                , isnull(HomePhone,'') as HomePhone,isnull(CellPhone, '') as CellPhone,isnull(CompanyName, '') as CompanyName,isnull(WorkTitle, '') as WorkTitle,
                                isnull(WorkPhone, '') as WorkPhone,isnull(WorkPhoneExt, '') as WorkPhoneExt,
                                isnull(FaxPhone, '') as FaxPhone,isnull(PrimaryEmail, '') as PrimaryEmail,isnull(SecondaryEmail, '') as SecondaryEmail,PreferredTFN,
                                isnull(FranchiseId, '') as FranchiseId,isnull(IsReceiveEmails, '') as IsReceiveEmails
                                 from CRM.Customer where CustomerId = @customerId";
                var contacts = connection.Query<CustomerTP>(query, new { customerId = id }).FirstOrDefault();
                connection.Close();
                return contacts;
            }
            
        }
        public bool GetDuplicateAddress(AddressTP address)
        {
            using (SqlConnection connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = @"Select * from CRM.Addresses where Address1= @address1 and
                            Address2 = @address2 and City =@city and State = @state and
                            ZipCode =@zipcode and CountryCode2Digits = @countrycode";
                var result = connection.Query<AddressTP>(query, new
                {
                    address1 = address.Address1
                    ,
                    address2 = address.Address2
                    ,
                    city = address.City
                    ,
                    state = address.State
                    ,
                    zipcode = address.ZipCode
                    ,
                    countrycode = address.CountryCode2Digits
                    ,
                    
                }).ToList();
                if (result.Count != 0)
                {
                    return true;
                }
                else
                    return false;

            }
            
        }
    }
}


