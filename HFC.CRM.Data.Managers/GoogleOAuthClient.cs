﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using DotNetOpenAuth.AspNet.Clients;
using DotNetOpenAuth.Messaging;
using Newtonsoft.Json;

/// <summary>
/// The Managers namespace.
/// </summary>
namespace HFC.CRM.Managers
{
    /// <summary>
    /// Class GoogleOAuthClient.
    /// </summary>
    public class GoogleOAuthClient : OAuth2Client
    {
        /// <summary>
        /// The authorization endpoint
        /// </summary>
        private const string AuthorizationEndpoint = "https://accounts.google.com/o/oauth2/auth";
        /// <summary>
        /// The token endpoint
        /// </summary>
        private const string TokenEndpoint = "https://accounts.google.com/o/oauth2/token";
        /// <summary>
        /// The user information endpoint
        /// </summary>
        private const string UserInfoEndpoint = "https://www.googleapis.com/oauth2/v1/userinfo";
        /// <summary>
        /// The provider name
        /// </summary>
        private const string providerName = "Google";
        /// <summary>
        /// The client identifier
        /// </summary>
        private readonly string clientId;
        /// <summary>
        /// The client secret
        /// </summary>
        private readonly string clientSecret;
        /// <summary>
        /// The expires_in
        /// </summary>
        private short expires_in = 0;
        /// <summary>
        /// The refresh_token
        /// </summary>
        private string refresh_token = "";

        /// <summary>
        /// The _scope list
        /// </summary>
        private List<string> _scopeList = new List<string>()
        {
            "https://www.googleapis.com/auth/userinfo.email",
            "https://www.googleapis.com/auth/userinfo.profile",
            "https://www.googleapis.com/auth/calendar",
            //"https://www.googleapis.com/auth/tasks"
            //"https://mail.google.com/mail/feed/atom", //Gmail inbox feed
            //"https://www.googleapis.com/auth/devstorage.full_control", //Cloud storage if we use later
            //"https://www.googleapis.com/auth/devstorage.read_only",
            //"https://www.googleapis.com/auth/devstorage.read_write",
            //"https://www.googleapis.com/auth/plus.login" //Google+ info
        };

        /// <summary>
        /// Gets or sets the scope list.
        /// </summary>
        /// <value>The scope list.</value>
        public List<string> ScopeList
        {
            get { return _scopeList; }
            set { _scopeList = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GoogleOAuthClient"/> class.
        /// </summary>
        /// <param name="clientId">The client identifier.</param>
        /// <param name="clientSecret">The client secret.</param>
        /// <exception cref="System.ArgumentNullException">
        /// clientId
        /// or
        /// clientSecret
        /// </exception>
        public GoogleOAuthClient(string clientId, string clientSecret)
            : base(providerName)
        {
            //if (string.IsNullOrEmpty(clientId))
            //    throw new ArgumentNullException("clientId");
            //if (string.IsNullOrEmpty(clientSecret))
            //    throw new ArgumentNullException("clientSecret");

            this.clientId = clientId;
            this.clientSecret = clientSecret;
        }

        /// <summary>
        /// Gets the full url pointing to the login page for this client. The url should include the specified return url so that when the login completes, user is redirected back to that url.
        /// </summary>
        /// <param name="returnUrl">The return URL.</param>
        /// <returns>An absolute URL.</returns>
        protected override Uri GetServiceLoginUrl(Uri returnUrl)
        {
            UriBuilder uriBuilder = new UriBuilder(AuthorizationEndpoint);
            uriBuilder.AppendQueryArgument("client_id", this.clientId);
            uriBuilder.AppendQueryArgument("redirect_uri", returnUrl.GetLeftPart(UriPartial.Path));
            uriBuilder.AppendQueryArgument("response_type", "code");
            uriBuilder.AppendQueryArgument("access_type", "offline"); //important to receive a refresh token
            uriBuilder.AppendQueryArgument("scope", string.Join(" ", _scopeList));
            uriBuilder.AppendQueryArgument("state", returnUrl.Query.Substring(1));

            return uriBuilder.Uri;
        }

        /// <summary>
        /// Given the access token, gets the logged-in user's data. The returned dictionary must include two keys 'id', and 'username'.
        /// </summary>
        /// <param name="accessToken">The access token of the current user.</param>
        /// <returns>A dictionary contains key-value pairs of user data</returns>
        protected override IDictionary<string, string> GetUserData(string accessToken)
        {
            UriBuilder uriBuilder = new UriBuilder(UserInfoEndpoint);
            uriBuilder.AppendQueryArgument("access_token", accessToken);

            WebRequest webRequest = WebRequest.Create(uriBuilder.Uri);
            using (HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse())
            {
                if (webResponse.StatusCode == HttpStatusCode.OK)
                {
                    using (var responseStream = webResponse.GetResponseStream())
                    {
                        if (responseStream == null)
                            return null;

                        StreamReader streamReader = new StreamReader(responseStream);

                        Dictionary<string, string> values = JsonConvert.DeserializeObject<Dictionary<string, string>>(streamReader.ReadToEnd());

                        values.Add("expires_in", expires_in.ToString());
                        if(!string.IsNullOrEmpty(refresh_token))
                            values.Add("refresh_token", refresh_token);
                        // Add a username field in the dictionary
                        if (values.ContainsKey("email") && !values.ContainsKey("username"))
                            values.Add("username", values["email"]);

                        return values;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Queries the access token from the specified authorization code.
        /// </summary>
        /// <param name="returnUrl">The return URL.</param>
        /// <param name="authorizationCode">The authorization code.</param>
        /// <returns>The access token</returns>
        protected override string QueryAccessToken(Uri returnUrl, string authorizationCode)
        {
            // Build up the form post data
            Dictionary<string, string> values = new Dictionary<string, string>();
            values.Add("code", authorizationCode);
            values.Add("client_id", this.clientId);
            values.Add("client_secret", this.clientSecret);
            values.Add("redirect_uri", returnUrl.GetLeftPart(UriPartial.Path));
            values.Add("grant_type", "authorization_code");
            string postData = String.Join("&",
                values.Select(x => Uri.EscapeDataString(x.Key) + "=" + Uri.EscapeDataString(x.Value))
                      .ToArray());

            // Build up the request
            WebRequest webRequest = WebRequest.Create(TokenEndpoint);
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.ContentLength = postData.Length;
            webRequest.Method = "POST";
            using (Stream requestStream = webRequest.GetRequestStream())
            {
                StreamWriter streamWriter = new StreamWriter(requestStream);
                streamWriter.Write(postData);
                streamWriter.Flush();
            }

            // Process the response
            using (HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse())
            {
                if (webResponse.StatusCode == HttpStatusCode.OK)
                {
                    using (Stream responseStream = webResponse.GetResponseStream())
                    {
                        StreamReader streamReader = new StreamReader(responseStream);

                        dynamic response = JsonConvert.DeserializeObject<dynamic>(streamReader.ReadToEnd());
                        if(response.expires_in != null)
                            expires_in = (short)response.expires_in;
                        if (response.refresh_token != null)
                            refresh_token = (string)response.refresh_token;
                        return (string)response.access_token;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Refreshes the access token.
        /// </summary>
        /// <param name="refreshToken">The refresh token.</param>
        /// <param name="expiresIn">The expires in.</param>
        /// <returns>System.String.</returns>
        public string RefreshAccessToken(string refreshToken, out short expiresIn)
        {
            expiresIn = 0;

            if (!string.IsNullOrEmpty(refreshToken))
            {
                // Build up the form post data
                Dictionary<string, string> values = new Dictionary<string, string>();
                values.Add("client_id", this.clientId);
                values.Add("client_secret", this.clientSecret);
                values.Add("grant_type", "refresh_token");
                values.Add("refresh_token", refreshToken);
                string postData = String.Join("&",
                    values.Select(x => Uri.EscapeDataString(x.Key) + "=" + Uri.EscapeDataString(x.Value))
                          .ToArray());

                // Build up the request
                WebRequest webRequest = WebRequest.Create(TokenEndpoint);
                webRequest.ContentType = "application/x-www-form-urlencoded";
                webRequest.ContentLength = postData.Length;
                webRequest.Method = "POST";
                using (Stream requestStream = webRequest.GetRequestStream())
                {
                    StreamWriter streamWriter = new StreamWriter(requestStream);
                    streamWriter.Write(postData);
                    streamWriter.Flush();
                }

                // Process the response
                using (HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse())
                {
                    if (webResponse.StatusCode == HttpStatusCode.OK)
                    {
                        using (Stream responseStream = webResponse.GetResponseStream())
                        {
                            StreamReader streamReader = new StreamReader(responseStream);

                            dynamic response = JsonConvert.DeserializeObject<dynamic>(streamReader.ReadToEnd());
                            if (response.expires_in != null)
                                expiresIn = (short)response.expires_in;
                            return (string)response.access_token;
                        }
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Rewrites the request.
        /// </summary>
        public static void RewriteRequest()
        {
            var ctx = HttpContext.Current;

            var stateString = HttpUtility.UrlDecode(ctx.Request.QueryString["state"]);
            if (stateString == null || !stateString.Contains("__provider__=" + providerName))
                return;

            var q = HttpUtility.ParseQueryString(stateString);
            q.Add(ctx.Request.QueryString);
            q.Remove("state");

            ctx.RewritePath(ctx.Request.Path + "?" + q);
        }

    }
}