﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Core.Membership;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using HFC.CRM.Data.Geolocator;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Web;
using System.IO;
using HFC.CRM.DTO;
using System.Globalization;
using HFC.CRM.DTO.Calandar;

namespace HFC.CRM.Managers
{

   
    public class TextTemplateManager : DataManagerFE<TextTemplateManager> // DataManager<Note>
    {
        private EmailManager email;

        public TextTemplateManager(User user, Franchise franchise) : base(user, franchise)
        {
            email = new EmailManager(user, franchise);
        }

        public string GetTextTemplate(int BrandId, EmailType TextType,string salesperson,DateTime AppointmentDate,string fromEmailAddress,string FeName,bool IsAllDay)
        {
            var stringvalue = ""; string AppintmentDateTime = "";
            /*BB*/
            if (BrandId == 1)
            {
                if(TextType == EmailType.AppointmentConfirmation) //== 2)
                {
                    AppintmentDateTime = DateinFormat(AppointmentDate, IsAllDay);

                    stringvalue = "Budget Blinds Consultation Appt Conf: " + AppintmentDateTime + " with " + FeName + ". Questions? Email " + fromEmailAddress ;

                    return stringvalue;
                }
                if(TextType == EmailType.Installation) //== 5)
                {
                    AppintmentDateTime = DateinFormat(AppointmentDate, IsAllDay);
                    stringvalue = "Budget Blinds Install Appt Conf: " + AppintmentDateTime + " with " + FeName + ". Questions? Email " + fromEmailAddress ;
                }
                if(TextType == EmailType.Reminder)
                {
                    AppintmentDateTime = DateinFormat(AppointmentDate, IsAllDay);
                    stringvalue = " Rmdr! Appt with "+ FeName + " of Budget Blinds: "+ AppintmentDateTime + " Questions? Email "+ fromEmailAddress ;
                }
            }
            /*TL*/
            else if (BrandId == 2)
            {
                if (TextType == EmailType.AppointmentConfirmation) //== 2)
                {
                    AppintmentDateTime = DateinFormat(AppointmentDate, IsAllDay);

                    stringvalue = "Tailored Living Appt Conf: " + AppintmentDateTime + " with " + FeName + ". Questions? Email " +  fromEmailAddress ;
                    return stringvalue;
                }
                if (TextType == EmailType.Installation) //== 5)
                {
                    AppintmentDateTime = DateinFormat(AppointmentDate, IsAllDay);
                    stringvalue = "Tailored Living Install Appt Conf: " + AppintmentDateTime + " with " + FeName + ". Questions? Email " + fromEmailAddress ;
                }
                if (TextType == EmailType.Reminder)
                {
                    AppintmentDateTime = DateinFormat(AppointmentDate, IsAllDay);
                    stringvalue = " Rmdr! Appt with " + FeName + " of Tailored Living: " + AppintmentDateTime + " Questions? Email " + fromEmailAddress ;
                }
            }
            /*CC*/
            else
            {
                if (TextType == EmailType.AppointmentConfirmation) //== 3)
                {
                    AppintmentDateTime = DateinFormat(AppointmentDate, IsAllDay);

                    stringvalue = "Concrete Craft Appt Conf: " + AppintmentDateTime + " with " + FeName + ". Questions? Email " + fromEmailAddress ;
                    return stringvalue;
                }
                if (TextType == EmailType.Installation)  //== 5)
                {
                    AppintmentDateTime = DateinFormat(AppointmentDate, IsAllDay);
                    stringvalue = "Concrete Craft Install Appt Conf: " + AppintmentDateTime + " with " + FeName + ". Questions? Email " + fromEmailAddress ;
                }
                if (TextType == EmailType.Reminder)
                {
                    AppintmentDateTime = DateinFormat(AppointmentDate, IsAllDay);
                    stringvalue = " Rmdr! Appt with " + FeName + " of Concrete Craft: " + AppintmentDateTime + " Questions? Email " + fromEmailAddress ;
                }
            }
            return stringvalue;
        }

        private string DateinFormat(DateTime AppointmentDate,bool IsAllDay)
        {
            try
            {
                string AppintmentDateTime = ""; string ApptTime;
                //string ApptMonth = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(AppointmentDate.Month);
                string ApptMonth = Convert.ToString(AppointmentDate.Month).PadLeft(2, '0');
                string ApptDay = Convert.ToString(AppointmentDate.Day).PadLeft(2, '0');
                string ApptYear = Convert.ToString(AppointmentDate.Year);
                ApptTime = AppointmentDate.ToString("h:mm tt").ToLower().Replace(" ", string.Empty);
               
                if (IsAllDay == true)
                {
                    AppintmentDateTime = ApptMonth + "/" + ApptDay + "/" + ApptYear;
                }
                else
                    AppintmentDateTime = ApptMonth + "/" + ApptDay + "/" + ApptYear + "  " + ApptTime;

                string ApptDayName = Convert.ToString(AppointmentDate.DayOfWeek);
                return AppintmentDateTime;
            }
            catch(Exception ex)
            {
                EventLogger.LogEvent(ex);
            }
            return "";
        }
        public string GetOrderTextTemplate(int brandid, string fromemailid)
        {
            string StringValue = "";
            //BB
            if (brandid == 1)
            {
                StringValue = "Thank you from Budget Blinds! We hope you are enjoying your new window coverings, and we look forward to keeping you as a valued customer! " + fromemailid ;
                return StringValue;
            }
            //Tl
            else if (brandid == 2)
            {
                StringValue = "Thank you from Tailored Living! We hope you are enjoying your new home organization solution, and we look forward to keeping you as a valued customer! " + fromemailid ;
                return StringValue;
            }
            //CC
            else
            {
                StringValue = "Thank you from Concrete Craft! We hope you are enjoying your new concrete overlay, and we look forward to keeping you as a valued customer! " + fromemailid ;
                return StringValue;
            }

        }
    }
}


