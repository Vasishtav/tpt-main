﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using HFC.CRM.Data.Extensions.EnumAttr;
using HFC.CRM.DTO.Sync;
using Intuit.Ipp.Core;
using Intuit.Ipp.Core.Configuration;
using Intuit.Ipp.Data;
using Intuit.Ipp.DataService;
using Intuit.Ipp.Exception;
using Intuit.Ipp.GlobalTaxService;
using Intuit.Ipp.QueryFilter;
using Invoice = Intuit.Ipp.Data.Invoice;
using Payment = Intuit.Ipp.Data.Payment;
using Vendor = Intuit.Ipp.Data.Vendor;
using Intuit.Ipp.Security;
using System.Threading;
using System.Data.SqlClient;
using Dapper;
using HFC.CRM.DTO.Lookup;

namespace HFC.CRM.Managers.QBSync
{
    public class QBOSyncHandler
    {
        private ServiceContext _ctx;
        private string _owner;
        private QBSyncInfoManager _QBInfoManager;
        private QBSyncRequest _request;
        private DataService _service;
        private Profile _profile;
        private Franchise franchise;
        private QBApps app;
        private string _username;
        private string arAccount;
        private string apAccount;
        private string saleAccount;
        private string saleDiscount;
        private string taxAgencyAccount;
        private string invoiceSuffix = ContantStrings.DefaultSuffix;
        public string InvoiceSuffix { get; set; }
        private string Region;


        private Dictionary<int, Customer> _CustomersCache;
        private Dictionary<string, Account> _AcountCache;
        private Dictionary<string, PaymentMethod> _PaymentMethodsCache;



        private DataService service
        {
            get
            {
                if (_service == null)
                    _service = new DataService(ctx);

                //_service = new DataService(RestProfile.GetServiceContext(AppConfig.GetSetting(ContantStrings.ConsumerKey), AppConfig.GetSetting(ContantStrings.ConsumerSecret), _username, _profile));
                return _service;
            }
        }

        private ServiceContext ctx
        {
            get
            {
                if (_ctx == null)
                {
                    OAuth2RequestValidator oauthValidator = new OAuth2RequestValidator(_profile.accesstoken);
                    _ctx = new ServiceContext(_profile.RealmId, IntuitServicesType.QBO, oauthValidator);

                    // _ctx = RestProfile.GetServiceContext(AppConfig.GetSetting(ContantStrings.ConsumerKey), AppConfig.GetSetting(ContantStrings.ConsumerSecret), _username, _profile);
                }
                return _ctx;
            }
        }

        public static QBOSyncHandler For(QBSyncRequest request, string site, string username, string owner, QBSyncInfoManager QBInfoManager, Profile profile, Franchise fe, QBApps _app)
        {
            return new QBOSyncHandler
            {
                _request = request,
                arAccount = _app.AR == null || _app.AR == "" ? AccountFactory.ARAccount(site) : _app.AR,
                saleAccount = _app.Sales == null || _app.Sales == "" ? AccountFactory.SaleAccount(site) : _app.Sales,
                apAccount = _app.COG == null || _app.COG == "" ? AccountFactory.APAccount(site) : _app.COG,
                saleDiscount = AccountFactory.SalesDiscount(site),
                taxAgencyAccount = AccountFactory.GeneralTaxAgency(site),
                _username = username,
                _owner = owner,
                _QBInfoManager = QBInfoManager,
                _CustomersCache = new Dictionary<int, Customer>(),
                _AcountCache = new Dictionary<string, Account>(),
                _PaymentMethodsCache = new Dictionary<string, PaymentMethod>(),
                _profile = profile,
                franchise = fe,
                Region = fe.Region,
                app = _app
            };
        }

        public void Sync()
        {
            this.InvoiceSuffix = invoiceSuffix;
            syncAccount();
            //syncCustomerPrepare();
            SessionManager.SetQbduplicateCustomersLog(app.OwnerID.ToString(), "");
            syncCustomer();
            syncVendor();
            syncNonInventoryItem();
            ////syncSalesTax();
            syncInvoice();
            syncPaymentMethods();
            syncPayment();
            syncVendorBill();
        }

        private void syncAccount()
        {
            _QBInfoManager.AddStep(SyncMessages.SyncAccountStep + " (" + _request.ChartOfAccounts.Count + ")");
            try
            {
                foreach (var _account in _request.ChartOfAccounts)
                {
                    try
                    {
                        var EXISTING_ACCOUNTS_QUERY = string.Format("select * from Account where Name = '{0}'", _account.Name);
                        var resultFound = GetQBdata<Account>(EXISTING_ACCOUNTS_QUERY).FirstOrDefault();

                        var accountType = AccountTypeEnum.AccountsReceivable;
                        var accountSubType = AccountSubTypeEnum.AccountsReceivable;

                        switch (_account.AccountType)
                        {
                            case "Income":
                                accountType = AccountTypeEnum.Income;
                                accountSubType = AccountSubTypeEnum.SalesOfProductIncome;
                                break;
                            case "CostOfGoodsSold":
                                accountType = AccountTypeEnum.CostofGoodsSold;
                                accountSubType = AccountSubTypeEnum.SuppliesMaterials;
                                break;
                            case "AccountsReceivable":
                                accountType = AccountTypeEnum.AccountsReceivable;
                                accountSubType = AccountSubTypeEnum.AccountsReceivable;
                                break;
                            case "Taxes":
                                accountType = AccountTypeEnum.OtherCurrentLiability;
                                accountSubType = AccountSubTypeEnum.SalesTaxPayable;
                                break;
                            case "SalesDiscount":
                                accountType = AccountTypeEnum.OtherIncome;
                                accountSubType = AccountSubTypeEnum.DiscountsRefundsGiven;
                                break;
                        }

                        if (resultFound == null)
                        {
                            var account = new Account
                            {
                                Name = _account.Name,
                                AccountType = accountType,
                                AccountTypeSpecified = true,
                                AccountSubType = accountSubType.ToString(),
                                SubAccountSpecified = true,
                                AcctNum = _account.AccountNumber.Safely()
                            };
                            var accountAdded = QBAdd(account);
                        }
                    }
                    catch (Exception e)
                    {
                        LogException(e, _account);
                    }
                }
            }
            catch (Exception ex)
            {
                LogException(ex);
                //var actualerror = ((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions;
            }
            finally
            {
                _QBInfoManager.CompleteLastStep();
            }
        }

        private Customer GetCustomer(int id)
        {
            try
            {
                if (_CustomersCache.ContainsKey(id))
                {
                    return _CustomersCache[id];
                }
                var EXISTING_CUSTOMER_QUERY = string.Format("select * from customer where active = true and id = '{0}'", id);
                var customer = GetQBdata<Customer>(EXISTING_CUSTOMER_QUERY).FirstOrDefault();

                if (customer != null)
                {
                    _CustomersCache[id] = customer;
                }
                return customer;
            }
            catch (Exception e)
            {
                LogException(e);
                return null;
            }
        }

        private Account GetAccount(string name)
        {
            try
            {
                if (_AcountCache.ContainsKey(name))
                {
                    return _AcountCache[name];
                }
                var accountQuery = string.Format("select * from Account where Name = '{0}'", name);
                var account = GetQBdata<Account>(accountQuery).FirstOrDefault();

                if (account != null)
                {
                    _AcountCache[name] = account;
                }
                return account;
            }
            catch (Exception e)
            {
                LogException(e);
                return null;
            }
        }

        private PaymentMethod GetPayMethod(string name)
        {
            try
            {
                if (_PaymentMethodsCache.ContainsKey(name))
                {
                    return _PaymentMethodsCache[name];
                }
                var accountQuery = string.Format("select * from PaymentMethod where Name = '{0}'", name);
                var pMethod = GetQBdata<PaymentMethod>(accountQuery).FirstOrDefault();

                if (pMethod != null)
                {
                    _PaymentMethodsCache[name] = pMethod;
                }
                return pMethod;
            }
            catch (Exception e)
            {
                LogException(e);
                return null;
            }
        }

        private void syncCustomerPrepare()
        {
            _QBInfoManager.AddStep(SyncMessages.SyncCustomerSetup + " (" + _request.Customers.Count + ")");

            System.Threading.Thread.Sleep(4000);

            _QBInfoManager.CompleteLastStep();

        }

        private void syncCustomer()
        {
            _QBInfoManager.AddStep(SyncMessages.SyncCustomerStep + " (" + _request.Customers.Count + ")");
            //_QBInfoManager.AddProgress();

            // The following to facilitates to display the message in the client.
            //System.Threading.Thread.Sleep(40000);

            foreach (var c in _request.Customers)
            {
                try
                {

                    var id = 0;
                    // var needToSendId = true;

                    var resultFound = c.QbId > 0 ? GetCustomer(c.QbId) : null;

                    if (resultFound != null)
                    {
                        AddSynchLog(_request.OwnerID, SyncEntityType.Customer, c.ID, c.Name, resultFound.Id);
                        int.TryParse(resultFound.Id, out id);
                    }

                    if (resultFound == null)
                    {
                        var EXISTING_CUSTOMER_QUERY = string.Format("select * from customer where active = true and givenName like '{0}' and familyName like '{1}'",
                                c.FirstName.Trim().Safely(), c.LastName.Trim().Safely());

                        resultFound = GetQBdata<Customer>(EXISTING_CUSTOMER_QUERY).FirstOrDefault();

                        if (resultFound != null)
                        {
                            _QBInfoManager.AddCustomer(c.AccountId.ToString(), c.Name, c.BillAddr1, c.Phone, c.Email);
                            throw new Exception(string.Format("Duplicate Customer"));
                        }
                    }

                    if (resultFound == null)
                    {
                        var account = GetAccount(arAccount);
                        if (account == null)
                        {
                            throw new Exception(string.Format("Account: {0} not found in Qb", arAccount));
                        }

                        var customer = new Customer();
                        customer.GivenName = c.FirstName.Safely();
                        customer.FamilyName = c.LastName.Safely();
                        customer.MiddleName = c.MiddleName.Safely();
                        customer.CompanyName = c.CompanyName.Safely();
                        if (true == c.IsCommercial)
                            customer.DisplayName = !string.IsNullOrEmpty(customer.CompanyName) && customer.CompanyName != "" ? customer.CompanyName : c.Name;
                        else
                            customer.DisplayName = c.Name;
                        customer.PrimaryEmailAddr = new EmailAddress { Address = c.Email.EmailSafely() };
                        customer.PrimaryPhone = new TelephoneNumber
                        {
                            FreeFormNumber = c.Phone.Safely().ToPhoneFormat()
                        };
                        customer.Mobile = new TelephoneNumber
                        {
                            FreeFormNumber = c.CellPhone.Safely().ToPhoneFormat()
                        };
                        customer.AlternatePhone = new TelephoneNumber
                        {
                            FreeFormNumber = c.AltPhone.Safely().ToPhoneFormat()
                        };
                        customer.Fax = new TelephoneNumber { FreeFormNumber = c.Fax.Safely().ToPhoneFormat() };
                        customer.BillAddr = new PhysicalAddress
                        {
                            Line1 = c.BillAddr1.Safely(),
                            City = c.BillCity.Safely(),
                            CountrySubDivisionCode = c.BillState.Safely(),
                            PostalCode = c.BillPostalCode.Safely(),
                            Country = c.BillCountry.Safely()
                        };
                        customer.ShipAddr = new PhysicalAddress
                        {
                            Line1 = c.ShipAddr1.Safely(),
                            City = c.ShipCity.Safely(),
                            CountrySubDivisionCode = c.ShipState.Safely(),
                            PostalCode = c.ShipPostalCode.Safely(),
                            Country = c.ShipCountry.Safely()
                        };
                        customer.ARAccountRef = new ReferenceType
                        {
                            name = account.Name,
                            Value = account.Id
                        };

                        var customerAdded = QBAdd(customer);
                        AddSynchLog(_request.OwnerID, SyncEntityType.Customer, c.ID, c.Name, customerAdded.Id);

                        int.TryParse(customerAdded.Id, out id);
                    }


                    if (id != 0)
                    {
                        foreach (var i in _request.Invoices.Where(x => x.CustomerListID == c.ID.ToString()))
                        {
                            i.CustomerQbID = id;
                        }
                        foreach (var p in _request.Payments.Where(x => x.CustomerListID == c.ID.ToString()))
                        {
                            p.CustomerQbID = id;
                        }
                    }
                    //DB.SaveChanges();
                }
                catch (Exception e)
                {
                    AddSynchLog(_request.OwnerID, SyncEntityType.Customer, c.ID, c.Name, null, e);
                    LogException(e, c);
                }
            }

            _QBInfoManager.CompleteLastStep();
        }

        private void syncVendor()
        {
            _QBInfoManager.AddStep(SyncMessages.SyncVendorStep + " (" + _request.Vendors.Count + ")");

            foreach (var v in _request.Vendors.Where(x => x.Name != taxAgencyAccount))
            {
                try
                {
                    if (v.Name == null)
                    {
                        continue;
                    }
                    var ctx = this.ctx;
                    var service = this.service;

                    var EXISTING_QUERY = string.Format("select * from vendor where active = true and CompanyName = '{0}'", v.Name.Trim().Safely());
                    var resultFound = GetQBdata<Vendor>(EXISTING_QUERY).FirstOrDefault();

                    if (resultFound == null)
                    {
                        var vendor = new Vendor
                        {
                            GivenName = v.FirstName.Safely(),
                            FamilyName = v.LastName.Safely(),
                            MiddleName = v.MiddleName.Safely(),
                            DisplayName = v.Name.Safely(),
                            CompanyName = v.Name.Safely(),
                            PrimaryEmailAddr = new EmailAddress { Address = v.Email.EmailSafely() },
                            PrimaryPhone = new TelephoneNumber { FreeFormNumber = v.Phone.Safely().ToPhoneFormat() },
                            AlternatePhone = new TelephoneNumber
                            {
                                FreeFormNumber = v.AltPhone.Safely().ToPhoneFormat()
                            },
                            Fax = new TelephoneNumber { FreeFormNumber = v.Fax.Safely().ToPhoneFormat() },
                            BillAddr = new PhysicalAddress
                            {
                                Line1 = v.Addr1.Safely(),
                                City = v.City.Safely(),
                                CountrySubDivisionCode = v.State.Safely(),
                                PostalCode = v.PostalCode.Safely(),
                                Country = v.Country.Safely()
                            }
                        };
                        var vendorAdded = QBAdd(vendor);
                        AddSynchLog(_request.OwnerID, SyncEntityType.Vendor, v.ID, v.Name, vendorAdded.Id);
                    }
                    else
                    {
                        AddSynchLog(_request.OwnerID, SyncEntityType.Vendor, v.ID, v.Name, resultFound.Id);
                    }

                    //DB.SaveChanges();
                }

                catch (Exception e)
                {
                    AddSynchLog(_request.OwnerID, SyncEntityType.Vendor, v.ID, v.Name, null, e);
                    LogException(e, v);
                }
            }

            _QBInfoManager.CompleteLastStep();
        }

        private void syncNonInventoryItem()
        {
            _QBInfoManager.AddStep(SyncMessages.SyncNonInventoryItemStep + " (" + (_request.LineItems.Count + _request.SalesTax.Count) + ")");
            try
            {
                var account = GetAccount(saleAccount);
                if (account == null)
                {
                    throw new Exception(string.Format("Account: {0} not found in Qb", saleAccount));
                }

                var accountPayable = GetAccount(taxAgencyAccount);
                if (accountPayable == null)
                {
                    throw new Exception(string.Format("Account: {0} not found in Qb", taxAgencyAccount));
                }

                var accountDiscount = GetAccount(saleDiscount);
                if (accountDiscount == null)
                {
                    throw new Exception(string.Format("Account: {0} not found in Qb", accountDiscount));
                }

                foreach (var i in _request.LineItems)
                {
                    try
                    {
                        if (i.Name.Trim().Safely() != "Discounts")
                        {
                            var EXISTING_ITEM_QUERY = string.Format("select * from item where Name = '{0}'", i.Name.Trim().Safely());
                            var resultFound = GetQBdata<Item>(EXISTING_ITEM_QUERY).FirstOrDefault();

                            if (resultFound == null)
                            {
                                var item = new Item
                                {
                                    Active = true,
                                    Name = i.Name.Trim().Safely(),
                                    Description = i.Desc.Safely(),
                                    Type = ItemTypeEnum.NonInventory,
                                    TypeSpecified = true,
                                    Sku = i.ManufacturerPartNumber.Safely(),
                                    UnitPrice = string.IsNullOrEmpty(i.Price) ? decimal.Parse("0") : decimal.Parse(i.Price),
                                    IncomeAccountRef = new ReferenceType
                                    {
                                        name = saleAccount,
                                        Value = account.Id
                                    }
                                };


                                if (!string.IsNullOrEmpty(item.Name))
                                {
                                    var itemAdded = QBAdd(item);
                                    AddSynchLog(_request.OwnerID, SyncEntityType.ItemNonInventory, i.ID, i.Name, itemAdded.Id);
                                }
                            }
                            else
                            {
                                AddSynchLog(_request.OwnerID, SyncEntityType.ItemNonInventory, i.ID, i.Name, resultFound.Id);
                            }
                        }
                        else
                        {
                            var EXISTING_ITEM_QUERY = string.Format("select * from item where Name = '{0}'", i.Name.Trim().Safely());
                            var resultFound = GetQBdata<Item>(EXISTING_ITEM_QUERY).FirstOrDefault();

                            if (resultFound == null)
                            {
                                var item = new Item
                                {
                                    Active = true,
                                    Name = i.Name.Trim().Safely(),
                                    Description = i.Desc.Safely(),
                                    Type = ItemTypeEnum.NonInventory,
                                    TypeSpecified = true,
                                    Sku = i.ManufacturerPartNumber.Safely(),
                                    UnitPrice = string.IsNullOrEmpty(i.Price) ? decimal.Parse("0") : decimal.Parse(i.Price),
                                    IncomeAccountRef = new ReferenceType
                                    {
                                        name = saleDiscount,
                                        Value = accountDiscount.Id
                                    }
                                };

                                var itemAdded = QBAdd(item);
                                AddSynchLog(_request.OwnerID, SyncEntityType.ItemNonInventory, i.ID, i.Name, itemAdded.Id);
                            }
                            else
                                AddSynchLog(_request.OwnerID, SyncEntityType.ItemNonInventory, i.ID, i.Name, resultFound.Id);
                        }

                    }
                    catch (Exception e)
                    {
                        AddSynchLog(_request.OwnerID, SyncEntityType.ItemNonInventory, i.ID, i.Name, null, e);
                        LogException(e, i);
                    }
                }

                foreach (var i in _request.SalesTax)
                {
                    try
                    {
                        var EXISTING_ITEM_QUERY = string.Format("select * from item where Name = '{0}'", i.Name.Trim().Safely());
                        var resultFound = GetQBdata<Item>(EXISTING_ITEM_QUERY).FirstOrDefault();

                        if (resultFound == null)
                        {
                            var item = new Item
                            {
                                Active = true,
                                Name = i.Name.Trim().Safely(),
                                Type = ItemTypeEnum.NonInventory,
                                TypeSpecified = true,
                                UnitPrice = string.IsNullOrEmpty(i.TaxRate)
                                    ? decimal.Parse("0")
                                    : decimal.Parse(i.TaxRate),
                                Description = i.ItemDesc.Safely(), /// MYC-942
                                IncomeAccountRef = new ReferenceType
                                {
                                    name = taxAgencyAccount,
                                    Value = accountPayable.Id
                                }
                            };

                            var itemAdded = QBAdd(item);
                            AddSynchLog(_request.OwnerID, SyncEntityType.ItemSalesTax, i.ID, i.Name, itemAdded.Id);
                        }
                        else
                        {
                            AddSynchLog(_request.OwnerID, SyncEntityType.ItemSalesTax, i.ID, i.Name, resultFound.Id);
                        }
                    }
                    catch (Exception e)
                    {
                        AddSynchLog(_request.OwnerID, SyncEntityType.ItemNonInventory, i.ID, i.Name, null, e);
                        LogException(e, i);
                    }
                    //DB.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                LogException(ex);
            }
            finally
            {
                _QBInfoManager.CompleteLastStep();
            }
        }

        private void syncSalesTax()
        {
            _QBInfoManager.AddStep(SyncMessages.SyncSalesTaxStep + " (" + _request.SalesTax.Count + ")");
            ctx.IppConfiguration.Message.Request.SerializationFormat =
                SerializationFormat.Json;
            ctx.IppConfiguration.Message.Response.SerializationFormat =
                SerializationFormat.Json;

            var taxSvc = new GlobalTaxService(ctx);


            foreach (var t in _request.SalesTax)
            {
                try
                {
                    var queryService = new QueryService<TaxRate>(ctx);
                    var results = queryService.ExecuteIdsQuery("select * from TaxRate").ToList();
                    var resultFound = results.FirstOrDefault(x => x.Name.ToLower() == t.Name.Trim().ToLower());

                    var taxAgencyQueryService = new QueryService<TaxAgency>(ctx);
                    var agencies = taxAgencyQueryService.ExecuteIdsQuery("select * from TaxAgency");
                    var taxAgency = agencies.FirstOrDefault(x => x.DisplayName.ToLower() == taxAgencyAccount.ToLower());


                    if (taxAgency == null)
                    {
                        var taxA = new TaxAgency { DisplayName = taxAgencyAccount };
                        taxAgency = service.Add(taxA);
                    }

                    if (resultFound == null)
                    {
                        var svc = new TaxService { TaxCode = t.Name.Safely() };
                        var lstTaxRate = new List<TaxRateDetails>();
                        var taxdetail = new TaxRateDetails
                        {
                            TaxRateName = t.ItemDesc.Safely(), /// MYC-942
                            TaxAgencyId = taxAgency.Id,
                            RateValue = string.IsNullOrEmpty(t.TaxRate) ? 0 : decimal.Parse(t.TaxRate),
                            RateValueSpecified = true,
                            TaxApplicableOn = TaxRateApplicableOnEnum.Sales,
                            TaxApplicableOnSpecified = true
                        };
                        //taxdetail.TaxAgencyId = "0";

                        lstTaxRate.Add(taxdetail);

                        svc.TaxRateDetails = lstTaxRate.ToArray();
                        var taxCodeAdded = taxSvc.AddTaxCode(svc);

                        AddSynchLog(_request.OwnerID, SyncEntityType.ItemSalesTax, t.ID, t.Name, taxCodeAdded.Id);
                    }
                    else
                    {
                        AddSynchLog(_request.OwnerID, SyncEntityType.ItemSalesTax, t.ID, t.Name, resultFound.Id);
                    }
                    //DB.SaveChanges();
                }
                catch (Exception e)
                {
                    AddSynchLog(_request.OwnerID, SyncEntityType.ItemSalesTax, t.ID, t.Name, null, e);
                    LogException(e, t);
                }
            }

            _QBInfoManager.CompleteLastStep();
        }

        private void syncInvoice()
        {
            _QBInfoManager.AddStep(SyncMessages.SyncInvoiceStep + " (" + _request.Invoices.Count + ")");

            TaxCode taxcode = new TaxCode();
            TaxCode taxcodeCATaxable = new TaxCode();
            List<TaxRateMapping> TaxRateMapping = app.TaxRateMapping;
            if (Region == "CA")
            {
                var taxcodeQuery = string.Format("select * from TaxCode where Name='Exempt'");
                taxcode = GetQBdata<TaxCode>(taxcodeQuery).FirstOrDefault();

                if (taxcode != null && app.TaxCode != null && app.TaxCode != "")
                {
                    var taxcodeQueryCA = string.Format("select * from TaxCode where Name='{0}'", app.TaxCode);
                    taxcodeCATaxable = GetQBdata<TaxCode>(taxcodeQueryCA).FirstOrDefault();
                }
                else
                    taxcodeCATaxable = null;
            }
            else
            {
                taxcode = null;
                taxcodeCATaxable = null;
            }

            foreach (var i in _request.Invoices)
            {

                try
                {
                    ctx.IppConfiguration.Message.Request.SerializationFormat = SerializationFormat.Json;
                    ctx.IppConfiguration.Message.Response.SerializationFormat = SerializationFormat.Json;

                    var Invoice_QUERY = string.Format("select * from Invoice where DocNumber = '{0}'", string.Format("{0}-{1}", i.RefNumber, InvoiceSuffix));
                    var invoiceFound = GetQBdata<Invoice>(Invoice_QUERY).FirstOrDefault();

                    var customer = GetCustomer(i.CustomerQbID);

                    if (customer == null)
                    {
                        continue;
                        //throw new Exception(string.Format("Customer: {0} not found in Qb", i.CustomerQbID));
                    }

                    var account = GetAccount(saleAccount);


                    if (account == null)
                    {
                        throw new Exception(string.Format("Account: {0} not found in Qb", saleAccount));
                    }

                    var salesDiscountAccount = GetAccount(saleDiscount);
                    if (salesDiscountAccount == null)
                    {
                        throw new Exception(string.Format("Account: {0} not found in Qb", saleDiscount));
                    }
                    if (invoiceFound == null)
                    {
                        var lineItemsTaxable = new List<Line>();
                        var lineItemsNonTaxable = new List<Line>();
                        var lineItemsTaxList = new List<Line>();
                        var lineItems = new List<Line>();
                        var TxnTaxDetail = new TxnTaxDetail();
                        var TaxLine = new List<Line>();
                        if (i.InvoiceItems.Count == 0)
                        {
                            throw new ValidationException("Can not sync invoice without lineItems!");
                        }
                        foreach (var line in i.InvoiceItems)
                        {
                            var itemQuery = string.Format("select * from item where Name = '{0}'", line.Name.Trim().Safely());
                            var item = GetQBdata<Item>(itemQuery).FirstOrDefault();

                            if (line.IsSaleAdjustment && Convert.ToDecimal(line.Amount) < 0)
                            {
                                item.IncomeAccountRef = new ReferenceType
                                {
                                    name = salesDiscountAccount.Name,
                                    Value = salesDiscountAccount.Id
                                };
                            }

                            Line lineItem = new Line();

                            string TaxCodeRef = "";
                            if (Region == "CA" && taxcode != null)
                            {
                                if (line.Taxable == "True" && taxcodeCATaxable != null)
                                    TaxCodeRef = taxcodeCATaxable.Id;
                                else
                                    TaxCodeRef = taxcode.Id;
                            }
                            else if (line.Taxable == "True" && Region != "CA")
                                TaxCodeRef = "TAX";
                            else if (line.Taxable != "True" && Region != "CA")
                                TaxCodeRef = "NON";

                            if (item != null)
                            {
                                lineItem = new Line
                                {
                                    Id = item.Id,
                                    Description = item.Description.Safely(),
                                    Amount = string.IsNullOrEmpty(line.Amount) ? decimal.Parse("0") : decimal.Parse(line.Amount),
                                    DetailType = LineDetailTypeEnum.SalesItemLineDetail,
                                    DetailTypeSpecified = true,
                                    AmountSpecified = true,
                                    AnyIntuitObject = new SalesItemLineDetail
                                    {
                                        ItemRef = new ReferenceType
                                        {
                                            name = item.Name,
                                            Value = item.Id
                                        },
                                        Qty = string.IsNullOrEmpty(line.Quantity) ? 0 : decimal.Parse(line.Quantity),
                                        QtySpecified = true,
                                        TaxCodeRef = new ReferenceType { Value = TaxCodeRef }
                                    }
                                };

                                if (line.Taxable == "True" && line.ItemType == "ItemNonInventory")
                                    lineItemsTaxable.Add(lineItem);
                                if (line.Taxable != "True" && line.ItemType == "ItemNonInventory")
                                    lineItemsNonTaxable.Add(lineItem);
                                if (line.ItemType == "ItemSalesTax")
                                    lineItemsTaxList.Add(lineItem);
                            }
                        }

                        if (lineItemsTaxable != null && lineItemsTaxable.Count > 0)
                        {
                            // Taxable item add first
                            lineItems.AddRange(lineItemsTaxable);

                            // Add subtotal amount
                            var lineItemDescription = new Line
                            {
                                Description = "Subtotal: 0",
                                DetailType = LineDetailTypeEnum.DescriptionOnly,
                                DetailTypeSpecified = true,
                                AnyIntuitObject = new DescriptionLineDetail
                                {

                                }
                            };
                            lineItems.Add(lineItemDescription);
                        }


                        var InvoiceTaxItems = i.InvoiceItems.Where(x => x.ItemType == "ItemSalesTax").ToList();
                        // summary tax or a breakdown by jurisdiction based on a flag in QB settings
                        if (Region == "CA" && taxcodeCATaxable != null)
                        {
                            if (lineItemsTaxable.Count > 0)
                            {
                                var TaxRateDetail = taxcodeCATaxable.SalesTaxRateList.TaxRateDetail.Select(x => new QBOTaxRate { Value = x.TaxRateRef.Value, Name = x.TaxRateRef.name }).ToList();

                                foreach (var trd in TaxRateDetail)
                                {
                                    var RateMapping = TaxRateMapping.Where(x => x.QBOTaxName == trd.Name).ToList();

                                    var taxd = (from line in InvoiceTaxItems
                                                join rm in RateMapping on line.Desc equals rm.TPTTaxName
                                                select line).ToList();
                                    if (taxd.Count > 0)
                                    {
                                        Line lineItem = new Line
                                        {
                                            Amount = taxd.Sum(x => x.AmountDec),
                                            AmountSpecified = true,
                                            DetailType = LineDetailTypeEnum.TaxLineDetail,
                                            DetailTypeSpecified = true,
                                            AnyIntuitObject = new TaxLineDetail
                                            {
                                                TaxRateRef = new ReferenceType { Value = trd.Value },
                                                PercentBased = true,
                                                PercentBasedSpecified = true,
                                                TaxPercent = taxd.Select(x => x.TaxRate).FirstOrDefault(),
                                                TaxPercentSpecified = true,
                                                NetAmountTaxable = lineItemsTaxable.Sum(x => x.Amount),
                                                NetAmountTaxableSpecified = true
                                            }
                                        };
                                        TaxLine.Add(lineItem);
                                    }
                                    else
                                    {
                                        Line lineItem = new Line
                                        {
                                            Amount = 0,
                                            AmountSpecified = true,
                                            DetailType = LineDetailTypeEnum.TaxLineDetail,
                                            DetailTypeSpecified = true,
                                            AnyIntuitObject = new TaxLineDetail
                                            {
                                                TaxRateRef = new ReferenceType { Value = trd.Value },
                                                PercentBased = true,
                                                PercentBasedSpecified = true,
                                                TaxPercent = taxd.Select(x => x.TaxRate).FirstOrDefault(),
                                                TaxPercentSpecified = true,
                                                NetAmountTaxable = lineItemsTaxable.Sum(x => x.Amount),
                                                NetAmountTaxableSpecified = true
                                            }
                                        };
                                        TaxLine.Add(lineItem);
                                    }
                                }
                            }
                            if (lineItemsNonTaxable.Count > 0)
                            {
                                var TaxRateExemptDetail = taxcode.SalesTaxRateList.TaxRateDetail.Select(x => new QBOTaxRate { Value = x.TaxRateRef.Value, Name = x.TaxRateRef.name }).ToList();
                                foreach (var trd in TaxRateExemptDetail)
                                {
                                    Line lineItem = new Line
                                    {
                                        Amount = 0,
                                        AmountSpecified = true,
                                        DetailType = LineDetailTypeEnum.TaxLineDetail,
                                        DetailTypeSpecified = true,
                                        AnyIntuitObject = new TaxLineDetail
                                        {
                                            TaxRateRef = new ReferenceType { Value = trd.Value },
                                            PercentBased = true,
                                            PercentBasedSpecified = true,
                                            TaxPercent = 0,
                                            TaxPercentSpecified = true,
                                            NetAmountTaxable = lineItemsNonTaxable.Sum(x => x.Amount),
                                            NetAmountTaxableSpecified = true
                                        }
                                    };
                                    TaxLine.Add(lineItem);
                                }
                            }

                            TxnTaxDetail.TotalTax = TaxLine.Sum(x => x.Amount);
                            TxnTaxDetail.TotalTaxSpecified = true;
                            TxnTaxDetail.TaxLine = TaxLine.ToArray();
                        }
                        else
                            lineItems.AddRange(lineItemsTaxList);

                        // Non Taxable Items
                        lineItems.AddRange(lineItemsNonTaxable);

                        var invoice = new Invoice
                        {
                            DocNumber = string.Format("{0}-{1}", i.RefNumber.ToUpper(), InvoiceSuffix),
                            TxnDate = i.TxnDate,
                            TxnDateSpecified = true,
                            TotalAmt = i.InvoiceItems.Sum(x => decimal.Parse(x.Amount)),
                            Line = lineItems.ToArray(),
                            TxnTaxDetail = TxnTaxDetail,

                            CustomerRef = new ReferenceType
                            {
                                name = customer.DisplayName,
                                type = objectNameEnumType.Customer.ToString(),
                                Value = customer.Id
                            },
                            ARAccountRef = new ReferenceType
                            {
                                name = account.Name,
                                Value = account.Id
                            }
                        };
                        invoice.BillAddr = new PhysicalAddress
                        {
                            Line1 = i.BillAddr1.Safely(),
                            City = i.BillCity.Safely(),
                            CountrySubDivisionCode = i.BillState.Safely(),
                            PostalCode = i.BillPostalCode.Safely(),
                            Country = i.BillCountry.Safely()
                        };
                        invoice.ShipAddr = new PhysicalAddress
                        {
                            Line1 = i.ShipAddr1.Safely(),
                            City = i.ShipCity.Safely(),
                            CountrySubDivisionCode = i.ShipState.Safely(),
                            PostalCode = i.ShipPostalCode.Safely(),
                            Country = i.ShipCountry.Safely()
                        };
                        var invoiceAdded = QBAdd(invoice);
                        AddSynchLog(_request.OwnerID, SyncEntityType.Invoice, i.ID, i.RefNumber, invoiceAdded.Id);
                    }
                    else
                    {
                        AddSynchLog(_request.OwnerID, SyncEntityType.Invoice, i.ID, i.RefNumber, invoiceFound.Id);
                    }
                    // DB.SaveChanges();
                }


                catch (Exception e)
                {
                    AddSynchLog(_request.OwnerID, SyncEntityType.Invoice, i.ID, i.RefNumber, null, e);
                    LogException(e, i);
                }
            }

            _QBInfoManager.CompleteLastStep();
        }

        private void syncPayment()
        {
            _QBInfoManager.AddStep(SyncMessages.SyncPaymentStep + " (" + _request.Payments.Count + ")");
            foreach (var p in _request.Payments)
            {
                var Invoice_QUERY = string.Format("select * from Invoice where DocNumber = '{0}'", string.Format("{0}-{1}", p.InvoiceNumber, InvoiceSuffix));
                try
                {
                    var Payment_QUERY = string.Format("select * from Payment where PaymentRefNum = '{0}'", p.RefNumber);
                    var resultFound = GetQBdata<Payment>(Payment_QUERY).FirstOrDefault();

                    var customer = GetCustomer(p.CustomerQbID);
                    if (customer == null)
                    {
                        continue;
                        //throw new Exception(string.Format("Customer: {0} not found in Qb", p.CustomerQbID));
                    }

                    var invoice = GetQBdata<Invoice>(Invoice_QUERY).FirstOrDefault();

                    if (invoice == null)
                    {
                        throw new Exception(string.Format("Invoice: {0} not found in Qb", p.InvoiceNumber));
                    }

                    var pMethod = GetPayMethod(p.PaymentMethod);

                    if (resultFound == null)
                    {
                        var payment = new Payment
                        {
                            PaymentRefNum = p.RefNumber,
                            PaymentTypeSpecified = true,
                            PaymentType = PaymentTypeEnum.CreditCard,
                            TotalAmt = decimal.Parse(p.TotalAmount),
                            TotalAmtSpecified = true,
                            ProcessPayment = false,
                            ProcessPaymentSpecified = true,
                            TxnDateSpecified = true,
                            TxnDate = p.TxnDate,
                            CurrencyRef = new ReferenceType { name = "US Dollar", Value = "USD" },
                            Line = new[]
                            {
                                new Line
                                {
                                    Amount = decimal.Parse(p.PaymentAmount),
                                    AmountSpecified = true,
                                    DetailType = LineDetailTypeEnum.PaymentLineDetail,
                                    DetailTypeSpecified = true,
                                    LinkedTxn =
                                        new[] {new LinkedTxn {TxnId = invoice.Id, TxnType = "Invoice"}}
                                }
                            },
                            CustomerRef = new ReferenceType { Value = customer.Id }
                        };
                        if (pMethod != null)
                        {
                            payment.PaymentMethodRef = new ReferenceType { name = pMethod.Name, Value = pMethod.Id };
                        }

                        var paymentAdded = QBAdd(payment);
                        AddSynchLog(_request.OwnerID, SyncEntityType.Payment, p.ID, p.RefNumber, paymentAdded.Id);
                    }
                    else
                    {
                        AddSynchLog(_request.OwnerID, SyncEntityType.Payment, p.ID, p.RefNumber, resultFound.Id);
                    }
                    //DB.SaveChanges();
                }

                catch (Exception e)
                {
                    AddSynchLog(_request.OwnerID, SyncEntityType.Payment, p.ID, p.RefNumber, null, e);
                    LogException(e, p);
                }
            }

            _QBInfoManager.CompleteLastStep();
        }

        private void syncPaymentMethods()
        {
            _QBInfoManager.AddStep(SyncMessages.SyncPaymentMethodStep + " (" + _request.PaymentMethods.Count + ")");
            var creditCardNames = new[] { "MasterCard", "Visa", "Debit Card", "Gift Card", "E-Check" };


            var syncCache = new List<string>();
            foreach (var pm in _request.PaymentMethods)
            {
                try
                {
                    if (syncCache.Contains(pm.Name))
                    {
                        continue;
                    }
                    syncCache.Add(pm.Name);
                    var Account_Query = string.Format("select * from PaymentMethod where Name = '{0}'", pm.Name);
                    var pMethod = GetQBdata<PaymentMethod>(Account_Query).FirstOrDefault();

                    if (pMethod == null)
                    {
                        var paymentMethod = new PaymentMethod
                        {
                            Name = pm.Name,
                            Active = true,
                            Type = creditCardNames.Contains(pm.Name) ? "CREDIT_CARD" : "NON_CREDIT_CARD"
                        };

                        var paymentMethodAdded = QBAdd(paymentMethod);
                        AddSynchLog(_request.OwnerID, SyncEntityType.PayMethod, pm.ID, pm.Name, paymentMethodAdded.Id);
                    }
                    else
                    {
                        AddSynchLog(_request.OwnerID, SyncEntityType.PayMethod, pm.ID, pm.Name, pMethod.Id);
                    }
                    //DB.SaveChanges();
                }
                catch (Exception e)
                {
                    AddSynchLog(_request.OwnerID, SyncEntityType.PayMethod, pm.ID, pm.Name, null, e);
                    LogException(e, pm);
                }
            }

            _QBInfoManager.CompleteLastStep();
        }

        private void syncVendorBill()
        {
            _QBInfoManager.AddStep(SyncMessages.SyncVendorBillStep + " (" + _request.Invoices.Count + ")");
            if (_request.SyncOption.IncludeVendorBill)
            {
                foreach (var i in _request.Invoices)
                {
                    try
                    {
                        var account = GetAccount(apAccount);
                        // var Account_Query = "select * from Account";
                        // var account = GetQBdata<Account>(Account_Query).Where(x => x.Name.Trim().ToLower() == apAccount.Trim().ToLower()).FirstOrDefault();

                        foreach (var b in i.ToBills())
                        {

                            var EXISTING_QUERY = string.Format("select * from bill where DocNumber = '{0}'", b.ID);
                            var resultFound = GetQBdata<Bill>(EXISTING_QUERY).FirstOrDefault();

                            var Vendor_QUERY = string.Format("select * from vendor where active = true and CompanyName = '{0}'", b.VendorName.Safely());
                            var vendor = GetQBdata<Vendor>(Vendor_QUERY).FirstOrDefault();

                            if (resultFound == null)
                            {
                                var bill = new Bill
                                {
                                    TotalAmt = b.Amount,
                                    TotalAmtSpecified = true,
                                    DocNumber = b.ID.ToString(),
                                    TxnDate = DateTime.Parse(b.TxnDateString),
                                    TxnDateSpecified = true,
                                    Memo = b.ItemMemo,
                                    VendorRef = new ReferenceType
                                    {
                                        name = vendor.CompanyName,
                                        Value = vendor.Id
                                    }
                                };

                                var lines = new List<Line>();

                                foreach (var line in b.BillLineItems)
                                {
                                    try
                                    {
                                        var item = new Line
                                        {
                                            Amount = decimal.Parse(line.Amount ?? "0"),
                                            AmountSpecified = true,
                                            Description = line.Item + "(Qty:" + line.Quantity + ")",
                                            DetailType = LineDetailTypeEnum.AccountBasedExpenseLineDetail,
                                            DetailTypeSpecified = true,
                                            AnyIntuitObject = new AccountBasedExpenseLineDetail
                                            {
                                                AccountRef = new ReferenceType
                                                {
                                                    name = account.Name,
                                                    Value = account.Id
                                                }
                                            }
                                        };

                                        lines.Add(item);

                                    }
                                    catch (Exception ex)
                                    {
                                        AddSynchLog(_request.OwnerID, SyncEntityType.Bill, i.ID, b.RefNumber, null);
                                        LogException(ex, i);
                                    }
                                }
                                bill.Line = lines.ToArray();
                                var billAdded = QBAdd(bill);
                                AddSynchLog(_request.OwnerID, SyncEntityType.Bill, i.ID, b.RefNumber, billAdded.Id);

                            }
                        }
                        //DB.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        AddSynchLog(_request.OwnerID, SyncEntityType.Bill, i.ID, i.RefNumber, null, e);
                        LogException(e, i);
                    }
                }
            }

            _QBInfoManager.CompleteLastStep();
        }

        private void LogException(Exception e, object obj = null)
        {
            var errorForUser = "";
            try
            {
                errorForUser = EventLogger.LogEvent(e, _username, 0, _owner, obj);
            }
            finally
            {
                _QBInfoManager.ExceptionByLastStep(e, 0, errorForUser);
            }
        }

        public void AddSynchLog(Guid ownerId, SyncEntityType type, int id, string objRef, string qbId, Exception ex = null)
        {
            try
            {
                var qboIdInt = 0;
                int.TryParse(qbId, out qboIdInt);
                var message = "";
                if (ex != null)
                {
                    if (ex.GetType() == typeof(QBSyncValidationException))
                    {
                        message = ex.Message;
                    }
                    else if ((ex.GetType() == typeof(InvalidTokenException)) && (ex.Message == "Unauthorized-401"))
                    {
                        message = "Need to authorize to QBO!";
                    }
                    else
                    {
                        message = "Synchronization failed";
                    }
                }
                var status = 0;
                if (!string.IsNullOrEmpty(message))
                {
                    status = 1;
                }
                //var old = DB.SynchLogs.Where(x =>
                //    x.OwnerId == ownerId.ToString() &&
                //    x.ObjectID == id &&
                //    x.ObjectType == (int)type).OrderByDescending(x => x.SynchOn).FirstOrDefault();

                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    string query = @"select * from QB.SynchLog where OwnerId=@OwnerId 
                    and ObjectID=@ObjectID and ObjectType=@ObjectType order by SynchOn desc";

                    var old = connection.Query<SynchLog>(query, new { OwnerId = ownerId.ToString(), ObjectID = id, ObjectType = (int)type }).FirstOrDefault();

                    if (old != null)
                    {
                        if (qboIdInt != 0)
                        {
                            old.QbId = qboIdInt;
                        }
                        old.Message = message;
                        old.SynchOn = DateTime.UtcNow;
                        old.Status = status;
                        connection.Update(old);
                    }
                    else
                    {
                        var newlog = new SynchLog
                        {
                            OwnerId = _request.OwnerID.ToString(),
                            ObjectID = id,
                            ObjectRef = objRef,
                            ObjectType = (int)type,
                            QbId = qboIdInt,
                            SynchOn = DateTime.UtcNow,
                            Status = status,
                            Message = message
                        };
                        connection.Insert(newlog);
                        //DB.SynchLogs.Add(new SynchLog
                        //{
                        //    OwnerId = _request.OwnerID.ToString(),
                        //    ObjectID = id,
                        //    ObjectRef = objRef,
                        //    ObjectType = (int)type,
                        //    QbId = qboIdInt,
                        //    SynchOn = DateTime.UtcNow,
                        //    Status = status,
                        //    Message = message
                        //});
                    }
                }



                //DB.SaveChanges();
            }
            catch (Exception e)
            {
                LogException(e);
            }
        }

        public IEnumerable<T> GetQBdata<T>(string query)
        {
            try
            {
                var queryService = new QueryService<T>(ctx);
                var response = queryService.ExecuteIdsQuery(query);
                return response;
            }
            catch (Exception ex)
            {
                if (ex.Message == "429")
                {
                    int Count = 0;
                    var queryService = new QueryService<T>(ctx);
                    IEnumerable<T> response = null;
                    do
                    {
                        try
                        {
                            response = queryService.ExecuteIdsQuery(query);
                            break;
                        }
                        catch (Exception)
                        {
                            Thread.Sleep(1000);
                        }
                    } while (Count < 1);
                    return response;
                }
                else
                {
                    throw;
                }
            }
        }

        public T QBAdd<T>(T entity) where T : IEntity
        {
            try
            {
                var res = service.Add<T>(entity);
                return res;
            }
            catch (Exception ex)
            {
                if (ex.Message == "429")
                {
                    int Count = 0;
                    T response = default(T);
                    do
                    {
                        try
                        {
                            response = service.Add<T>(entity);
                            break;
                        }
                        catch (Exception)
                        {
                            Thread.Sleep(1000);
                        }
                    } while (Count < 1);
                    return response;
                }
                else
                {
                    throw;
                }
            }
        }

        public List<LookupBaseDTO> GetALLTaxCode()
        {
            var TaxCodeQuery = string.Format("select * from TaxCode");
            var TaxCodelst = GetQBdata<TaxCode>(TaxCodeQuery).ToList();

            return TaxCodelst.Select(x => new LookupBaseDTO { Name = x.Name }).ToList();
        }

        public List<LookupBaseDTO> GetTaxRatebyTaxCode(string TaxCode)
        {
            var TaxCodeQuery = string.Format("select * from TaxCode where Name='{0}'", TaxCode);
            var TaxCodelst = GetQBdata<TaxCode>(TaxCodeQuery).FirstOrDefault();

            if (TaxCodelst != null)
            {
                return TaxCodelst.SalesTaxRateList.TaxRateDetail.Select(x => new LookupBaseDTO { Name = x.TaxRateRef.name }).ToList();
            }

            return new List<LookupBaseDTO>();
        }

        public List<QBOCOA> GetALLAccount()
        {
            var Query = string.Format("select * from Account");
            var Accountlst = GetQBdata<Account>(Query).ToList();

            return Accountlst.Select(x => new QBOCOA { Name = x.Name, accountType = x.AccountType.ToString(), accountSubType = x.AccountSubType }).ToList();
        }

    }
}