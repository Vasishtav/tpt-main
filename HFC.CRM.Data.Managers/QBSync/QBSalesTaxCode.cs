﻿
namespace HFC.CRM.Managers.QBSync
{
    public sealed class QBSalesTaxCode
    {
        public string TaxableSalesTaxCodeRefListID { get; set; }
        public string NonTaxableSalesTaxCodeRefListID { get; set; }

        public string TaxableSalesTaxCodeRef { get { return "Tax"; } }
        public string NonTaxableSalesTaxCodeRef { get { return "Non"; } }
    }
}
