﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using DevDefined.OAuth.Consumer;
using DevDefined.OAuth.Framework;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.DTO.Sync;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.DataService;
using Intuit.Ipp.LinqExtender;
using Intuit.Ipp.QueryFilter;
using Intuit.Ipp.Security;

namespace HFC.CRM.Managers.QBSync
{
    public static class RestHelper
    {

        public static void disconnectRealm(Profile profile)
        {
            var res = RestHelper.callPlatform(profile, Constants.IppEndPoints.DisconnectUrl);

        }

       

        public static string callPlatform(Profile profile, string url)
        {

            OAuthConsumerContext consumerContext = new OAuthConsumerContext
            {
                ConsumerKey = AppConfig.GetSetting("consumerKey"),
                SignatureMethod = SignatureMethod.HmacSha1,
                ConsumerSecret = AppConfig.GetSetting("consumerSecret")                 
            };

            OAuthSession oSession = new OAuthSession(consumerContext, Constants.OauthEndPoints.IdFedOAuthBaseUrl + Constants.OauthEndPoints.UrlRequestToken,
                                  Constants.OauthEndPoints.AuthorizeUrl,
                                  Constants.OauthEndPoints.IdFedOAuthBaseUrl + Constants.OauthEndPoints.UrlAccessToken);

            oSession.ConsumerContext.UseHeaderForOAuthParameters = true;
            if (profile != null && profile.OAuthAccessToken.Length > 0)
            {
                oSession.AccessToken = new TokenBase
                {
                    Token = profile.OAuthAccessToken,
                    ConsumerKey = AppConfig.GetSetting("consumerKey") ,
                    TokenSecret = profile.OAuthAccessTokenSecret
                };

                IConsumerRequest conReq = oSession.Request();
                conReq = conReq.Get();
                conReq = conReq.ForUrl(url);
                try
                {
                    conReq = conReq.SignWithToken();
                    return conReq.ReadBody();
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
            }
            return "";
        }

        private static DataService getDataService(RestProfile profile)
        {
            ServiceContext serviceContext = getServiceContext(profile);
            var v = serviceContext.BaseUrl;
            return new DataService(serviceContext);
        }

        private static ServiceContext getServiceContext(RestProfile profile)
        {
            var consumerKey = AppConfig.GetSetting("consumerKey");
            var consumerSecret = AppConfig.GetSetting("consumerSecret");

            OAuthRequestValidator oauthValidator = new OAuthRequestValidator(profile.OAuthAccessToken, profile.OAuthAccessTokenSecret, consumerKey, consumerSecret);
            return new ServiceContext(profile.RealmId, (IntuitServicesType)profile.DataSource, oauthValidator);
        }

        public static List<Customer> getCustomerList(RestProfile profile)
        {
            ServiceContext serviceContext = getServiceContext(profile);
            serviceContext.IppConfiguration.BaseUrl.Qbo = ConfigurationManager.AppSettings["ServiceContext.BaseUrl.Qbo"];
           
            QueryService<Customer> customerQueryService = new QueryService<Customer>(serviceContext);
            var results=customerQueryService.Select(c => c).ToList();
            return results;
        }

        public static bool appTokensSet()
        {
            try
            {
                var consumerKey = AppConfig.GetSetting("consumerKey");
                var consumerSecret = AppConfig.GetSetting("consumerSecret");
                return consumerKey.Length > 0 && consumerSecret.Length > 0;
            }
            catch(Exception e)
            {
                EventLogger.LogEvent(e);
                return false;
            }
        }

        internal static void getCustomerList(RestProfile restProfile, List<QBOCustomer> list)
        {
            ServiceContext serviceContext = getServiceContext(restProfile);
            serviceContext.IppConfiguration.BaseUrl.Qbo = ConfigurationManager.AppSettings["ServiceContext.BaseUrl.Qbo"];
            DataService service = new DataService(serviceContext);

            foreach (var c in list)
            {
                
                {
                    var customer = new Customer();
                    customer.GivenName = c.FirstName;
                    customer.FamilyName = c.LastName;
                    var rc = service.Add(customer);

                }
            }
        }
    }
}