﻿using System.Runtime.Serialization;

namespace HFC.CRM.Managers.QBSync
{
    [KnownType(typeof(SyncOption))]
    public class SyncOption
    {
        public bool IncludeInvoice { get; set; }
        public bool IncludePayment { get; set; }
        public bool IncludeVendorBill { get; set; }
        public bool LastNameFirst { get; set; }
    }
}

