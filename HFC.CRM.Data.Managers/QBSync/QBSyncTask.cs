﻿using System;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Web;
using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using HFC.CRM.Managers.Quickbooks;
using System.Data.SqlClient;
using Dapper;
using HFC.CRM.DTO.Lookup;
using System.Collections.Generic;
using HFC.CRM.DTO.Sync;

namespace HFC.CRM.Managers.QBSync
{
    public class QBSyncTask
    {
        private string email;
        private string Site;
        private string _owner;
        private Profile _profile;
        private Thread thread;
        private HttpContext _context;
        private IPrincipal _user;
        private int _franchiseId;
        private QBSyncInfoManager _QBInfoManager;
        private Franchise franchise;
        private QBApps app;
        public QBSyncTask(QBApps _app, string email, QBSyncInfoManager QBInfoManager, Profile profile, Franchise fe)
        {
            if (fe.BrandId == 1)
                this.Site = "budgetblinds";
            else if (fe.BrandId == 2)
                this.Site = "tailoredliving";
            else
                this.Site = "concretecraft";

            //this.Site = AppConfigManager.HostDomain;
            if (HttpContext.Current != null)
            {
                this._context = HttpContext.Current;
                _user = HttpContext.Current.User;
            }
            this.email = email;
            thread = new Thread(this.ProcessSync);
            this._QBInfoManager = QBInfoManager;
            this._owner = _app.OwnerID;
            this._profile = profile;
            _franchiseId = fe.FranchiseId;
            franchise = fe;
            this.app = _app;
        }

        /// <summary>
        /// QBO sync to start
        /// </summary>
        /// <returns></returns>
        public bool Start()
        {
            using (var handler = QBSyncManager.For(new QBCredential() { OwnerID = _owner }))
            {
                if (!handler.LockState())
                {
                    thread = new Thread(this.ProcessSync);
                    thread.Start();
                    return true;
                }
                else
                {
                    return false;
                }
            }

        }


        /// <summary>
        /// Process the QBO sync in thread
        /// </summary>
        public void ProcessSync()
        {
            HttpContext.Current = this._context;

            var wasLocked = false;
            using (var handler = QBSyncManager.For(new QBCredential() { OwnerID = _owner }))
            {
                try
                {

                    if (handler.TryLock())
                    {
                        wasLocked = true;
                        _QBInfoManager.Clear();
                        _QBInfoManager.AddInfo(SyncMessages.SyncStart);
                        using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                        {
                            var sLogs = connection.GetList<SynchLog>("where OwnerId=@OwnerId", new { OwnerId = _owner }).ToList();
                            sLogs = sLogs.Where(x => x.QbId != null && x.QbId != 0).ToList();

                            var syncLog = sLogs.Select(x => new QBSyncLog
                            {
                                ObjectID = x.ObjectID,
                                QbId = x.QbId.HasValue ? x.QbId.Value : 0,
                                ObjectType = x.ObjectType,
                                ObjectRef = x.ObjectRef
                            }).ToList();

                            var qbSyncRequest = handler.Sync(syncLog);

                            QBOSyncHandler.For(qbSyncRequest, Site, email, _owner, _QBInfoManager, _profile, franchise, app).Sync();
                        }
                    }
                }
                catch (Exception e)
                {
                    LogException(e);
                }
                finally
                {
                    _QBInfoManager.AddInfo(SyncMessages.SyncFinish);
                    if (wasLocked)
                    {
                        handler.UnLock();
                    }
                    Thread.Sleep(15000);
                    if (_QBInfoManager.ToString().Contains(SyncMessages.SyncFinish))
                    {
                        _QBInfoManager.Clear();
                    }
                }
            }
        }

        public List<LookupBaseDTO> GetALLTaxCode()
        {
            using (var handler = QBSyncManager.For(new QBCredential() { OwnerID = _owner }))
            {
                return QBOSyncHandler.For(null, Site, email, _owner, _QBInfoManager, _profile, franchise, app).GetALLTaxCode();
            }
        }

        public List<LookupBaseDTO> GetTaxRatebyTaxCode(string TaxCode)
        {
            using (var handler = QBSyncManager.For(new QBCredential() { OwnerID = _owner }))
            {
                return QBOSyncHandler.For(null, Site, email, _owner, _QBInfoManager, _profile, franchise, app).GetTaxRatebyTaxCode(TaxCode);
            }
        }

        public List<QBOCOA> GetAllAccount()
        {
            using (var handler = QBSyncManager.For(new QBCredential() { OwnerID = _owner }))
            {
                return QBOSyncHandler.For(null, Site, email, _owner, _QBInfoManager, _profile, franchise, app).GetALLAccount();
            }
        }

        private void LogException(Exception e, object obj = null)
        {
            EventLogger.LogEvent(e, email, 0, _owner, obj);
        }
    }
}