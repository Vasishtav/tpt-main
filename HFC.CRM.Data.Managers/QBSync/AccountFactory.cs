﻿namespace HFC.CRM.Managers.QBSync
{
    public static class AccountFactory
    {
        public static string SaleAccount(string site)
        {
            switch (site)
            {
                case "budgetblinds":
                    return "Sales BB";
                case "tailoredliving":
                    return "Sales TL";
                case "concretecraft":
                    return "Sales CC";
                default:
                    return string.Empty;
            }
        }

        public static string APAccount(string site)
        {
            switch (site)
            {
                case "budgetblinds":
                    return "Cost of Goods Sold BB";
                case "tailoredliving":
                    return "Cost of Goods Sold TL";
                case "concretecraft":
                    return "Cost of Goods Sold CC";
                default:
                    return string.Empty;
            }
        }

        public static string ARAccount(string site)
        {
            switch (site)
            {
                case "budgetblinds":
                    return "Accounts Receivable BB";
                case "tailoredliving":
                    return "Accounts Receivable TL";
                case "concretecraft":
                    return "Accounts Receivable CC";
                default:
                    return string.Empty;
            }
        }

        public static string SalesDiscount(string site)
        {
            switch (site)
            {
                case "budgetblinds":
                    return "Sales Discount BB";
                case "tailoredliving":
                    return "Sales Discount TL";
                case "concretecraft":
                    return "Sales Discount CC";
                default:
                    return string.Empty;
            }
            
        }

        public static string GeneralTaxAgency(string site)
        {
            switch (site)
            {
                case "budgetblinds":
                    return "General Tax Agency Payable BB";
                case "tailoredliving":
                    return "General Tax Agency Payable TL";
                case "concretecraft":
                    return "General Tax Agency Payable CC";
                default:
                    return string.Empty;
            }
            
        }

        
    }
}