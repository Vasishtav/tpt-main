﻿using System;
using System.Text.RegularExpressions;

namespace HFC.CRM.Managers.QBSync
{
    public static class StringHelper
    {
        public static string Safely(this string str)
        {
            var result = string.IsNullOrEmpty(str) ? "" : Regex.Replace(str, "[^a-zA-Z0-9_. ]+", " ", RegexOptions.Compiled).Replace("  ", " ");
            return result;
        }

        public static string ToPhoneFormat(this string str)
        {
            str = Regex.Replace(str, "[^0-9]+", "", RegexOptions.Compiled);
            string result;
            if ((!string.IsNullOrEmpty(str)) && (str.Length >= 10))
                result = string.Format("{0:(###) ###-" + new string('#', str.Length - 6) + "}",
                Convert.ToInt64(str.Replace(".", "").Replace("-", string.Empty).Replace(" ", string.Empty))
                );
            else
                result = str;
            return result;
        }

        public static string NullToEmpty(this string str)
        {
            if (str == null)
            {
                return "";
            }
            return str;
        }

        public static string EmailSafely(this string email)
        {
            string expresion;
            expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (!String.IsNullOrEmpty(email) && Regex.IsMatch(email, expresion))
            {
                if (Regex.Replace(email, expresion, string.Empty).Length == 0)
                {
                    return email;
                }
                else
                {
                    return string.Empty;
                }
            }
            else
            {
                return string.Empty;
            }
        }


    }
   
}
