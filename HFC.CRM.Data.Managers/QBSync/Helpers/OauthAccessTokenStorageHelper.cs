﻿using System.Configuration;
using System.Web.Mvc;
using System.Xml;

namespace HFC.CRM.Managers.QBSync.Helpers
{
    /// <summary>
    /// This class stores and fetches Oauth Access token for an user from XML file. In real world it could be database or any other suitable storage.
    /// </summary>
    public static class OauthAccessTokenStorageHelper
    {
        /// <summary>
        /// Remove oauth access toekn from storage 
        /// </summary>
        /// <param name="emailID"></param>
        public static void RemoveInvalidOauthAccessToken(string emailID, Controller page)
        {
            string path = page.Server.MapPath("/") + @"OauthAccessTokenStorage.xml";
            string searchUserXpath = "//record[@usermailid='" + emailID + "']";
            XmlDocument doc = new XmlDocument();
            doc.Load(path);
            XmlNode record = doc.SelectSingleNode(searchUserXpath);
            if (record != null)
            {
                doc.DocumentElement.RemoveChild(record);
                doc.Save(path);
            }

            //Rermove it from session
            page.Session.Remove("realm");
            page.Session.Remove("dataSource");
            page.Session.Remove("accessToken");
            page.Session.Remove("accessTokenSecret");
            page.Session.Remove("Flag");
        }

        /// <summary>
        /// get the oauth access token for the user from OauthAccessTokenStorage.xml
        /// </summary>
        /// <param name="emailID"></param>
        //public static void GetOauthAccessTokenForUser(string emailID, Controller page)
        //{
        //    string path = page.Server.MapPath("/") + @"OauthAccessTokenStorage.xml";
        //    string searchUserXpath = "//record[@usermailid='" + emailID + "']";
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load(path);
        //    XmlNode record = doc.SelectSingleNode(searchUserXpath);
        //    if (record != null)
        //    {
        //        page.Session["realm"] = record.Attributes["realmid"].Value;
        //        page.Session["dataSource"] = record.Attributes["dataSource"].Value;
        //        string secuirtyKey = ConfigurationManager.AppSettings["securityKey"];
        //        page.Session["accessToken"] = CryptographyHelper.DecryptData(record.Attributes["encryptedaccesskey"].Value, secuirtyKey);
        //        page.Session["accessTokenSecret"] = CryptographyHelper.DecryptData(record.Attributes["encryptedaccesskeysecret"].Value, secuirtyKey);

        //        // Add flag to session which tells that accessToken is in session
        //        page.Session["Flag"] = true;
        //    }

        //}

    }
}