﻿#region Using



#endregion

namespace HFC.CRM.Managers.QBSync.Helpers
{
    public class CustomerData
    {

        public string ServiceEndPoint
        {
            get;
            set;
        }

        public string QuickBookType
        {
            get;
            set;
        }

        public string getCustomerData()
        {
            return "This has to be implemented using SDK M1 Components";
        }
    }
}
