﻿using System;
using System.Collections.Generic;
using HFC.CRM.DTO.Sync;

namespace HFC.CRM.Managers.QBSync
{
    public sealed class QBSyncRequest
    {
        public static QBSyncRequest Make(Guid OwnerID, Guid batchID, List<QBOCustomer> customers, List<QBVendor> vendors,
            List<QBItemNonInventory> lineItems, List<QBItemSalesTax> salesTax, List<QBOInvoice> invoices, List<QBPaymentMethod> paymentMethods, List<QBReceivePayment> payments, bool generateVendorBill, string region)
        {
            return new QBSyncRequest
            {
                OwnerID = OwnerID,
                BatchID = batchID,
                Customers = customers,
                Vendors = vendors,
                LineItems = lineItems,
                SalesTax = salesTax,
                Invoices = invoices,
                PaymentMethods = paymentMethods,
                Payments = payments,
                GenerateVendorBill = generateVendorBill,
                Region = region
            };
        }

        public string SuppressTax { get; set; }
        public int ID { get; set; }

        public string Region { get; set; }

        public Guid OwnerID { get; set; }

        public Guid BatchID { get; set; }

        public List<QBAccount> ChartOfAccounts { get; set; }

        public List<QBOCustomer> Customers { get; set; }

        public List<QBVendor> Vendors { get; set; }

        public List<QBItemNonInventory> LineItems { get; set; }

        public List<QBItemSalesTax> SalesTax { get; set; }

        public List<QBOInvoice> Invoices { get; set; }

        public List<QBPaymentMethod> PaymentMethods { get; set; }

        public List<QBReceivePayment> Payments { get; set; }

        public SyncOption SyncOption { get; set; }

        public bool GenerateVendorBill { get; set; }

        public string AppToken {get; set; }
        public string ConsumerKey {get; set; }
        public string ConsumerSecret {get; set; }

    }
}
