﻿using System;

namespace HFC.CRM.Managers.QBSync
{
    public class QBSyncValidationException : Exception
    {
        public QBSyncValidationException(string message) : base(message)
        {
            
        }
    }
}