﻿using System.Runtime.Serialization;

namespace HFC.CRM.Managers.QBSync
{
    [KnownType(typeof(QBVendor))]
    public sealed class QBVendor
    {
        public static QBVendor Make(int id, string name, string companyName, string salutation, string firstName, string middleName, string lastName,
            string addr1, string city, string state, string postalCode, string country, string note, string phone, string altPhone, string fax,
            string email, string contact, string altContact, string nameOnCheck, string accountNumber, string notes, string salesTaxCountry)
        {
            return new QBVendor
            {
                ID = id,
                Name = name ?? string.Empty,
                CompanyName = companyName ?? string.Empty,
                Salutation = salutation ?? string.Empty,
                FirstName = firstName ?? string.Empty,
                MiddleName = middleName ?? string.Empty,
                LastName = lastName ?? string.Empty,
                Addr1 = addr1 ?? string.Empty,
                City = city ?? string.Empty,
                State = state ?? string.Empty,
                PostalCode = postalCode ?? string.Empty,
                Country = country ?? string.Empty,
                Note = note ?? string.Empty,
                Phone = phone ?? string.Empty,
                AltPhone = altPhone ?? string.Empty,
                Fax = fax ?? string.Empty,
                Email = email ?? string.Empty,
                Contact = contact ?? string.Empty,
                AltContact = altContact ?? string.Empty,
                NameOnCheck = nameOnCheck ?? string.Empty,
                AccountNumber = accountNumber ?? string.Empty,
                Notes = notes ?? string.Empty,
                SalesTaxCountry = salesTaxCountry ?? string.Empty


            };
        }


        public int ID { get; set; }
        public string Name { get; set; }
        public string CompanyName { get; set; }
        public string Salutation { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }

        public string Addr1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string Note { get; set; }

        public string Phone { get; set; }
        public string AltPhone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Contact { get; set; }
        public string AltContact { get; set; }
        public string NameOnCheck { get; set; }
        public string AccountNumber { get; set; }
        public string Notes { get; set; }
        public string SalesTaxCountry { get; set; }

        public string  BillAddr1 { get; set; }

        public string BillCity { get; set; }
        public string BillState { get; set; }
                                        
        public string BillPostalCode { get; set; }
        public string BillAddress{get; set;}
        public string BillCountry { get; set; }
    }
}
