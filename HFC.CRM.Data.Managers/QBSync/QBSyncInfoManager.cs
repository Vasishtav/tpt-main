﻿using System;
using System.Collections.Generic;
using System.Linq;
using HFC.CRM.Core.Common;
using Intuit.Ipp.Exception;
using Newtonsoft.Json;

namespace HFC.CRM.Managers.QBSync
{
    public enum QBSyncInfoItemType
    {
        info,
        step
    }

    public class QBSyncInfoItem
    {
        public QBSyncInfoItemType type;
        public List<Exception> exception;
        public bool isDone;
        public string message;
        public string errorForUser;
        public object syncObj;
        public List<int> exceptionId;

        public bool IsSuccess
        {
            get { return (exception == null && isDone); }
        }

        public bool IsFail
        {
            get { return (exception != null && isDone); }
        }

        public override string ToString()
        {
            if (type == QBSyncInfoItemType.info)
            {
                return message;
            }
            if (IsSuccess)
            {
                return String.Format("{0} {1}", message, SyncMessages.SyncSuccessMessage);
            }
            else if (IsFail)
            {
                return String.Format("{0} {1} ({2}) {3}", message, SyncMessages.SyncFailMessage,
                    exception.Count, errorForUser);
            }
            else return message;
        }
    }

    public class QBSyncInfoManager
    {
        private List<QBSyncInfoItem> _data;
        private List<QBSyncInfoCustomer> _dataCustomers;
        public string _ownerId;

        public QBSyncInfoManager()
        {
            _data = new List<QBSyncInfoItem>();
            _dataCustomers = new List<QBSyncInfoCustomer>();
        }

        public QBSyncInfoManager(string ownerId)
        {
            _ownerId = ownerId;
            _data = new List<QBSyncInfoItem>();
            _dataCustomers = new List<QBSyncInfoCustomer>();
        }

        public void Clear()
        {
            lock (this)
            {
                _data = new List<QBSyncInfoItem>();
                //SessionManager.QbSyncUserLog = _data.Select(x => x.ToString()).ToList();
                SessionManager.SetQbSyncUserLog(_ownerId, _data.Select(x => x.ToString()).ToList());
            }
        }
        public void AddInfo(string text)
        {
            lock (this)
            {
                _data.Add(new QBSyncInfoItem()
                {
                    message = text,
                    type = QBSyncInfoItemType.info,
                    isDone = true
                });
                //SessionManager.QbSyncUserLog = _data.Select(x => x.ToString()).ToList();
                SessionManager.SetQbSyncUserLog(_ownerId, _data.Select(x => x.ToString()).ToList());
            }
        }

        public void AddCustomer(string id, string name, string address, string phone, string email)
        {
            lock (this)
            {
                _dataCustomers.Add(new QBSyncInfoCustomer
                {
                    Id = id,
                    Name = name,
                    Address = address,
                    Phone = phone,
                    Email = email
                });
                SessionManager.SetQbduplicateCustomersLog(_ownerId, JsonConvert.SerializeObject(_dataCustomers, Formatting.Indented));
                //SessionManager.QbSyncCustomersLog = JsonConvert.SerializeObject(_dataCustomers, Formatting.Indented);   //_dataCustomers.Select(x => x.ToString()).ToList();
            }
        }

        public void AddQBCustomer(string id, string name, string address, string phone, string email)
        {
            lock (this)
            {
                _dataCustomers.Add(new QBSyncInfoCustomer
                {
                    Id = id,
                    Name = name,
                    Address = address,
                    Phone = phone,
                    Email = email
                });
                SessionManager.QbSyncQBCustomersLog = Newtonsoft.Json.JsonConvert.SerializeObject(_dataCustomers, Newtonsoft.Json.Formatting.Indented);   //_dataCustomers.Select(x => x.ToString()).ToList();
            }
        }

        public void AddProgress()
        {
            lock (this)
            {
                var lastItem = _data[_data.Count - 1];
                lastItem.message += " Progress...";
                //SessionManager.QbSyncUserLog = _data.Select(x => x.ToString()).ToList();
                SessionManager.SetQbSyncUserLog(_ownerId, _data.Select(x => x.ToString()).ToList());
            }
        }

        public void AddStep(string text, object obj = null)
        {
            CompleteLastStep();
            lock (this)
            {

                _data.Add(new QBSyncInfoItem()
                {
                    message = text,
                    syncObj = obj,
                    isDone = false,
                    type = QBSyncInfoItemType.step
                });
                //SessionManager.QbSyncUserLog = _data.Select(x => x.ToString()).ToList();
                SessionManager.SetQbSyncUserLog(_ownerId, _data.Select(x => x.ToString()).ToList());
            }
        }

        public void CompleteLastStep()
        {
            lock (this)
            {

                foreach (var qbSyncInfoItem in _data.Where(x => x.isDone == false))
                {
                    qbSyncInfoItem.isDone = true;
                }
                //SessionManager.QbSyncUserLog = _data.Select(x => x.ToString()).ToList();
                SessionManager.SetQbSyncUserLog(_ownerId, _data.Select(x => x.ToString()).ToList());
            }
        }

        public void ExceptionByLastStep(Exception e, int exceptionId = 0, string errorForUser = "")
        {
            lock (this)
            {
                if ((e.GetType() == typeof(InvalidTokenException)) && (e.Message == "Unauthorized-401"))
                {
                    errorForUser = "Need to authorize to QBO!";
                }
                var step = _data.Where(x => x.type == QBSyncInfoItemType.step).LastOrDefault();
                if (step == null)
                {
                    return;
                }
                step.isDone = true;
                if (step.exception == null)
                {
                    step.exception = new List<Exception>();
                }
                if (step.exceptionId == null)
                {
                    step.exceptionId = new List<int>();
                }
                step.exception.Add(e);
                step.exceptionId.Add(exceptionId);
                if (String.IsNullOrEmpty(step.errorForUser))
                {
                    step.errorForUser = errorForUser;
                }
                else if (step.errorForUser.IndexOf(errorForUser) == -1)
                {
                    step.errorForUser += "<br/>" + errorForUser;
                }
                //SessionManager.QbSyncUserLog = _data.Select(x => x.ToString()).ToList();
                SessionManager.SetQbSyncUserLog(_ownerId, _data.Select(x => x.ToString()).ToList());
            }
        }

        public override string ToString()
        {
            lock (this)
            {
                return String.Join("<br/>", _data.Select(x => x.ToString()));
            }
        }
    }

    public class QBSyncInfoCustomer
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}