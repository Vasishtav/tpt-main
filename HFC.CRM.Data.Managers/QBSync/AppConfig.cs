﻿using System;
using System.Configuration;

namespace HFC.CRM.Managers.QBSync
{
    public static class AppConfig
    {

        public static string GetSetting(string key)
        {
            string value;
            value = ConfigurationManager.AppSettings[key];
            return value.ToString();
        }

        public static string GetSetting(string key, string defaultValue)
        {
            string value;
            value = ConfigurationManager.AppSettings[key];
            if (value == null)
                value = defaultValue;
            return value.ToString(); 
        }

        public static int GetSetting(string key, int defaultValue)
        {
            int ret;
            string value;
            value = ConfigurationManager.AppSettings[key];
            if (value == null)
                ret = defaultValue;
            else
                ret = Convert.ToInt32(value);
            return ret;
        }

    }
}