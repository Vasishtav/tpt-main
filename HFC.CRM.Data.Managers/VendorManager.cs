﻿
using System;
using System.Collections.Generic;
using System.Linq;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using System.Data.SqlClient;
using System.Dynamic;
using HFC.CRM.Data.Context;
using Dapper;
using HFC.CRM.Core.Common;
using HFC.CRM.DTO.Vendors;
using Newtonsoft.Json;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices;

public class VendorManager : DataManager<HFCVendor>
{

    public VendorManager()
    {

    }

    public VendorManager(User user, Franchise franchise)
    {
        Franchise = franchise;
        User = user;
    }

    PICManager PicManger = new PICManager();
    int BrandIdvalue = SessionManager.BrandId;
    public string Save(FranchiseVendor data)
    {
        var returnMsg = "";
        using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
        {
            FranchiseVendor vendor = new FranchiseVendor();
            if (data.VendorType == 1 || data.VendorType == 2 || data.VendorType == 4)
            {
                var query = "SELECT * from Acct.FranchiseVendors where VendorId = @VendorId and FranchiseId=@FranchiseId";
                var result = connection.Query<FranchiseVendor>(query, new { VendorId = data.VendorId, FranchiseId = SessionManager.CurrentUser.FranchiseId }).FirstOrDefault();
                if (result != null)
                {
                    if (result.AddressId == null)
                    {
                        var addressId = connection.Insert(data.VendorAddress);
                        data.AddressId = addressId;
                    }
                    else
                    {
                        data.AddressId = result.AddressId;
                    }
                    data.VendorAddress.AddressId = Convert.ToInt32(data.AddressId);
                    data.VendorIdPk = result.VendorIdPk;
                    returnMsg = Update(data);
                }
                else if (data.VendorType == 1)
                {
                    var HFCVendor = GetVendorsDatas(data.VendorId, 1).ToList().Where(x => x.VendorId == data.VendorId).FirstOrDefault();
                    if (data.Shipvalue != null)
                    {
                        foreach (var item in data.Shipvalue)
                        {
                            if(item.AccountNumber !=null)
                                item.AccountNumber = item.AccountNumber.Trim();
                            var Query = "select * from [CRM].[VendorTerritoryAccount] where FranchiseId=@FranchiseId and VendorId=@VendorId and [TerritoryId]=@TerritoryId";
                            var Terresult = connection.Query<VendorTerritoryAccount>(Query,
                                new
                                {
                                    VendorId = data.VendorId,
                                    FranchiseId = SessionManager.CurrentUser.FranchiseId,
                                    TerritoryId = item.TerritoryId
                                }).FirstOrDefault();
                            if (Terresult == null)
                            {
                                if (item.VendorId == 0)
                                {

                                    item.VendorId = data.VendorId;
                                }
                                if (item.FranchiseId == 0)
                                {
                                    item.FranchiseId = SessionManager.CurrentUser.FranchiseId;
                                }
                                item.Id = item.PreviousId;
                                if (HFCVendor.IsAlliance != null && HFCVendor.IsAlliance == true)
                                {
                                    var picresponse = PicManger.GETAllVendorCustomer(SessionManager.CurrentFranchise.Code);
                                    dynamic dynPicResponse = JsonConvert.DeserializeObject<ExpandoObject>(picresponse);

                                    if (dynPicResponse != null && dynPicResponse.valid)
                                    {
                                        var venext = false;
                                        foreach (dynamic cusvenAct in dynPicResponse.properties)
                                        {
                                            if (cusvenAct.Vendor == data.PICVendorId)
                                            {
                                                venext = true;
                                                if (((IEnumerable<dynamic>)cusvenAct.VendorCustomer).Contains(item.AccountNumber))
                                                {
                                                    connection.Insert(item);
                                                }
                                                else
                                                {
                                                    var test = "vendor " + data.Name + " and account " + item.AccountNumber + ".";
                                                    returnMsg = returnMsg + "<li>" + test + "</li>";
                                                }
                                            }

                                        }
                                        if (!venext)
                                        {
                                            returnMsg = returnMsg + "<li>" + "vendor " + data.Name + " and account " + item.AccountNumber + "." + "</li>";
                                        }
                                    }
                                }
                                else
                                {
                                    connection.Insert(item);
                                }


                            }
                            else
                            {
                                item.Id = item.PreviousId;
                                if (Terresult.AccountNumber != item.AccountNumber)
                                {
                                    // for now remove the delete as it can be used in other territories
                                    //var test = PicManger.DeleteVendorCustomer(data.PICVendorId,result.AccountNumber);
                                    if (HFCVendor.IsAlliance != null && HFCVendor.IsAlliance == true)
                                    {
                                        var picresponse = PicManger.GETAllVendorCustomer(SessionManager.CurrentFranchise.Code);
                                        dynamic dynPicResponse = JsonConvert.DeserializeObject<ExpandoObject>(picresponse);

                                        if (dynPicResponse != null && dynPicResponse.valid)
                                        {
                                            var venext = false;
                                            foreach (dynamic cusvenAct in dynPicResponse.properties)
                                            {
                                                if (cusvenAct.Vendor == data.PICVendorId)
                                                {
                                                    venext = true;

                                                    if (((IEnumerable<dynamic>)cusvenAct.VendorCustomer).Contains(item.AccountNumber))
                                                    {
                                                        connection.Update(item);
                                                    }
                                                    else
                                                    {
                                                        var test = "vendor " + data.Name + " and account " + item.AccountNumber + ".";
                                                        returnMsg = returnMsg + "<li>" + test + "</li>";
                                                    }


                                                }
                                            }
                                            if (!venext)
                                            {
                                                returnMsg = returnMsg + "<li>" + "vendor " + data.Name + " and account " + item.AccountNumber + "." + "</li>";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        connection.Update(item);
                                    }
                                }

                                //var Updatevalue = connection.Update(item);
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(returnMsg))
                    {
                        return returnMsg;
                    }
                    else
                    {
                        //Currency
                        var GetVendorCurrency = GetCurrencyTypeVal(data.VendorId);
                        vendor.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                        vendor.CreatedBy = SessionManager.CurrentUser.PersonId;
                        var addressId = connection.Insert(data.VendorAddress);
                        vendor.AddressId = addressId;
                        vendor.AccountNo = data.AccountNo;
                        vendor.Terms = data.Terms;
                        vendor.CreditLimit = data.CreditLimit;
                        vendor.VendorType = data.VendorType;
                        //vendor.IsActive = true;
                        vendor.VendorStatus = data.VendorStatus;
                        vendor.VendorId = data.VendorId;
                        //vendor.Currency = data.Currency;
                        vendor.Currency = GetVendorCurrency;
                        vendor.RetailBasis = data.RetailBasis;
                        vendor.AccountRepEmail = data.AccountRepEmail;
                        vendor.AccountRep = data.AccountRep;
                        vendor.AccountRepPhone = data.AccountRepPhone;
                        vendor.ContactDetails = data.ContactDetails;
                        //bulk
                        vendor.BulkPurchase = data.BulkPurchase;
                        vendor.ShipVia = data.ShipVia;
                        vendor.BPAccountNo = data.BPAccountNo;
                        vendor.BPShipLocation = data.BPShipLocation;
                        vendor.Carrier = data.Carrier;
                        vendor.ShipViaAccountNo = data.ShipViaAccountNo;

                        if (data.IsAlliance == false)
                        {
                            vendor.OrderingMethod = data.OrderingMethod;
                            vendor.ContactDetails = data.ContactDetails;
                        }
                        var vendorid = connection.Insert(vendor);
                        if (vendorid != null)
                            return Success;
                    }

                }
                else if (data.VendorType == 2)
                {
                    if (data.Shipvalue != null)
                    {
                        foreach (var item in data.Shipvalue)
                        {
                            if (item.AccountNumber != null)
                                item.AccountNumber = item.AccountNumber.Trim();
                            var Query = "select * from [CRM].[VendorTerritoryAccount] where FranchiseId=@FranchiseId and VendorId=@VendorId and [TerritoryId]=@TerritoryId";
                            var Terresult = connection.Query<VendorTerritoryAccount>(Query,
                                new
                                {
                                    VendorId = data.VendorId,
                                    FranchiseId = SessionManager.CurrentUser.FranchiseId,
                                    TerritoryId = item.TerritoryId
                                }).FirstOrDefault();
                            if (result == null)
                            {
                                if (item.VendorId == 0)
                                {
                                    item.VendorId = data.VendorId;
                                }
                                if (item.FranchiseId == 0)
                                {
                                    item.FranchiseId = SessionManager.CurrentUser.FranchiseId;
                                }
                                item.Id = item.PreviousId;
                                var Insertvalue = connection.Insert(item);
                            }
                            else
                            {
                                item.Id = item.PreviousId;
                                var Updatevalue = connection.Update(item);
                            }

                        }
                    }
                    //Currency
                    var GetVendorCurrency = GetCurrencyTypeVal(data.VendorId);
                    var addressId = connection.Insert(data.VendorAddress);
                    vendor.AddressId = addressId;
                    vendor.AccountRep = data.AccountRep;
                    vendor.AccountRepPhone = data.AccountRepPhone;
                    vendor.AccountRepEmail = data.AccountRepEmail;
                    vendor.AccountNo = data.AccountNo;
                    vendor.Terms = data.Terms;
                    vendor.CreditLimit = data.CreditLimit;
                    vendor.OrderingMethod = data.OrderingMethod;
                    vendor.ContactDetails = data.ContactDetails;
                    vendor.VendorStatus = data.VendorStatus;
                    //vendor.Currency = data.Currency;
                    vendor.Currency = GetVendorCurrency;
                    vendor.RetailBasis = data.RetailBasis;
                    //vendor.IsActive = true;
                    vendor.CreatedOn = DateTime.Now;
                    vendor.VendorId = data.VendorId;
                    //vendor.VendorType = Convert.ToByte(vendor.VendorType2);
                    vendor.VendorType = data.VendorType;
                    vendor.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                    vendor.CreatedBy = SessionManager.CurrentUser.PersonId;
                    //bulk
                    vendor.BulkPurchase = data.BulkPurchase;
                    vendor.ShipVia = data.ShipVia;
                    vendor.BPAccountNo = data.BPAccountNo;
                    vendor.BPShipLocation = data.BPShipLocation;
                    vendor.Carrier = data.Carrier;
                    vendor.ShipViaAccountNo = data.ShipViaAccountNo;
                    var vendorid = connection.Insert(vendor);
                    if (vendorid != null)
                        return Success;
                }
                else if (data.VendorType == 4)
                {
                    var addressId = connection.Insert(data.VendorAddress);
                    vendor.AddressId = addressId;
                    vendor.AccountRep = data.AccountRep;
                    vendor.AccountRepPhone = data.AccountRepPhone;
                    vendor.AccountRepEmail = data.AccountRepEmail;
                    vendor.AccountNo = data.AccountNo;
                    vendor.Terms = data.Terms;
                    vendor.CreditLimit = data.CreditLimit;
                    vendor.OrderingMethod = data.OrderingMethod;
                    vendor.ContactDetails = data.ContactDetails;
                    vendor.VendorStatus = data.VendorStatus;
                    vendor.Currency = data.Currency;
                    vendor.RetailBasis = data.RetailBasis;
                    vendor.CreatedOn = DateTime.Now;
                    vendor.VendorId = data.VendorId;
                    vendor.VendorType = data.VendorType;
                    vendor.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                    vendor.CreatedBy = SessionManager.CurrentUser.PersonId;
                    //bulk
                    vendor.BulkPurchase = data.BulkPurchase;
                    vendor.ShipVia = data.ShipVia;
                    vendor.BPAccountNo = data.BPAccountNo;
                    vendor.BPShipLocation = data.BPShipLocation;
                    vendor.Carrier = data.Carrier;
                    vendor.ShipViaAccountNo = data.ShipViaAccountNo;
                    var vendorid = connection.Insert(vendor);
                    if (vendorid != null)
                        return Success;
                }
            }
            else if (data.VendorIdPk == 0 && data.AddressId == null)
            {
                connection.Open();

                if (data != null)
                {
                    // HFCVendor nonedit = new HFCVendor();

                    var addressId = connection.Insert(data.VendorAddress);
                    //else if (data.VendorType.Equals(VendorType.NonAlianceVendor) && addressId !=null)
                    //else if(data.VendorType.Equals(VendorType.FranchiseVendor) && addressId != null)
                    if (data.VendorType == 3 && addressId != null)
                    {
                        if (data.Shipvalue != null)
                        {
                            foreach (var item in data.Shipvalue)
                            {
                                if (item.AccountNumber != null)
                                    item.AccountNumber = item.AccountNumber.Trim();
                                var Query = "select * from [CRM].[VendorTerritoryAccount] where FranchiseId=@FranchiseId and VendorId=@VendorId and [TerritoryId]=@TerritoryId";
                                var result = connection.Query<VendorTerritoryAccount>(Query,
                                    new
                                    {
                                        VendorId = data.VendorId,
                                        FranchiseId = SessionManager.CurrentUser.FranchiseId,
                                        TerritoryId = item.TerritoryId
                                    }).FirstOrDefault();
                                if (result == null)
                                {
                                    if (item.VendorId == 0)
                                    {
                                        item.VendorId = data.VendorId;
                                    }
                                    if (item.FranchiseId == 0)
                                    {
                                        item.FranchiseId = SessionManager.CurrentUser.FranchiseId;
                                    }
                                    item.Id = item.PreviousId;
                                    var Insertvalue = connection.Insert(item);
                                }
                                else
                                {
                                    item.Id = item.PreviousId;
                                    var Updatevalue = connection.Update(item);
                                }

                            }
                        }

                        data.CreatedBy = SessionManager.CurrentUser.PersonId;
                        data.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                        data.AddressId = addressId;
                        //data.IsActive = true;
                        data.CreatedOn = DateTime.Now;
                        var vendorid = connection.Insert(data);
                        if (vendorid != null)
                            return Success;
                    }

                }
                connection.Close();
            }
            else
            {

                returnMsg = Update(data);
            }

            if (!string.IsNullOrEmpty(returnMsg))
            {
                return returnMsg;
            }
            else
            {
                return Success;
            }

        }
    }

    public string Update(FranchiseVendor data)
    {
        var returnMsg = "";
        using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
        {
            try
            {
                if (data != null)
                {
                    FranchiseVendor vendor = new FranchiseVendor();
                    if (data.VendorType == 1)
                    {
                        if (data.Shipvalue != null)
                        {
                            foreach (var item in data.Shipvalue)
                            {
                                if (item.AccountNumber != null)
                                    item.AccountNumber = item.AccountNumber.Trim();
                                var Query = "select * from [CRM].[VendorTerritoryAccount] where FranchiseId=@FranchiseId and VendorId=@VendorId and [TerritoryId]=@TerritoryId";
                                var result = connection.Query<VendorTerritoryAccount>(Query,
                                    new
                                    {
                                        VendorId = data.VendorId,
                                        FranchiseId = SessionManager.CurrentUser.FranchiseId,
                                        TerritoryId = item.TerritoryId
                                    }).FirstOrDefault();
                                if (result == null)
                                {
                                    if (item.VendorId == 0)
                                    {
                                        item.VendorId = data.VendorId;
                                    }
                                    if (item.FranchiseId == 0)
                                    {
                                        item.FranchiseId = SessionManager.CurrentUser.FranchiseId;
                                    }
                                    item.Id = item.PreviousId;
                                    if (data.IsAlliance)
                                    {
                                        var picresponse = PicManger.GETAllVendorCustomer(SessionManager.CurrentFranchise.Code);
                                        dynamic dynPicResponse = JsonConvert.DeserializeObject<ExpandoObject>(picresponse);

                                        if (dynPicResponse != null && dynPicResponse.valid)
                                        {
                                            var venext = false;
                                            foreach (dynamic cusvenAct in dynPicResponse.properties)
                                            {
                                                if (cusvenAct.Vendor == data.PICVendorId)
                                                {
                                                    venext = true;
                                                    if (((IEnumerable<dynamic>)cusvenAct.VendorCustomer).Contains(item.AccountNumber))
                                                    {
                                                        connection.Insert(item);
                                                    }
                                                    else
                                                    {
                                                        var test = "vendor " + data.Name + " and account " + item.AccountNumber + ".";
                                                        returnMsg = returnMsg + "<li>" + test + "</li>";
                                                    }
                                                }
                                            }
                                            if (!venext)
                                            {
                                                returnMsg = returnMsg + "<li>" + "vendor " + data.Name + " and account " + item.AccountNumber + "." + "</li>";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        connection.Insert(item);
                                    }
                                }
                                else
                                {
                                    item.Id = item.PreviousId;
                                    if (result.AccountNumber != item.AccountNumber)
                                    {
                                        if (data.IsAlliance)
                                        {
                                            var picresponse = PicManger.GETAllVendorCustomer(SessionManager.CurrentFranchise.Code);
                                            dynamic dynPicResponse = JsonConvert.DeserializeObject<ExpandoObject>(picresponse);

                                            if (dynPicResponse != null && dynPicResponse.valid)
                                            {
                                                var venext = false;
                                                foreach (dynamic cusvenAct in dynPicResponse.properties)
                                                {
                                                    if (cusvenAct.Vendor == data.PICVendorId)
                                                    {
                                                        venext = true;
                                                        if (((IEnumerable<dynamic>)cusvenAct.VendorCustomer).Contains(item.AccountNumber))
                                                        {
                                                            connection.Update(item);
                                                        }
                                                        else
                                                        {
                                                            var test = "vendor " + data.Name + " and account " + item.AccountNumber + ".";
                                                            returnMsg = returnMsg + "<li>" + test + "</li>";
                                                        }
                                                    }
                                                    if (!venext)
                                                    {
                                                        returnMsg = returnMsg + "<li>" + "vendor " + data.Name + " and account " + item.AccountNumber + "." + "</li>";
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        // bulk purchase
                                        {
                                            connection.Update(item);
                                        }
                                    }
                                    else
                                        connection.Update(item);
                                }


                            }
                        }
                        //bulk purchase

                        // ship via
                        //if (data.ShipVia && data.IsAlliance)
                        //{
                        //    var picresponse = PicManger.GETAllVendorCustomer(Franchise.Code);
                        //    var dynPicResponse = JsonConvert.DeserializeObject<VendorCustomer>(picresponse);

                        //    if (dynPicResponse != null && dynPicResponse.valid && dynPicResponse.properties.Count > 0)
                        //    {
                        //        var prop = dynPicResponse.properties.Where(x => x.VendorCustomer.Contains(data.ShipViaAccountNo)).FirstOrDefault();
                        //        if (prop == null)
                        //        {
                        //            lstreturnMsg.Add("<li>vendor " + data.Name + " and account " + data.ShipViaAccountNo + ".</li>");
                        //        }
                        //    }
                        //}
                        //ship via

                        //Currency
                        var GetVendorCurrency = GetCurrencyTypeVal(data.VendorId);
                        vendor.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                        vendor.CreatedBy = SessionManager.CurrentUser.PersonId;
                        vendor.VendorIdPk = data.VendorIdPk;
                        vendor.AccountNo = data.AccountNo;
                        vendor.Terms = data.Terms;
                        vendor.CreditLimit = data.CreditLimit;
                        vendor.VendorId = data.VendorId;
                        //vendor.IsActive = data.IsActive;
                        vendor.VendorType = data.VendorType;
                        vendor.VendorStatus = data.VendorStatus;
                        //vendor.Currency = data.Currency;
                        vendor.Currency = GetVendorCurrency;
                        vendor.RetailBasis = data.RetailBasis;
                        vendor.AccountRepEmail = data.AccountRepEmail;
                        vendor.AccountRep = data.AccountRep;
                        vendor.AccountRepPhone = data.AccountRepPhone;
                        vendor.ContactDetails = data.ContactDetails;
                        //bulk
                        vendor.BulkPurchase = data.BulkPurchase;
                        vendor.ShipVia = data.ShipVia;
                        vendor.BPAccountNo = data.BPAccountNo;
                        vendor.BPShipLocation = data.BPShipLocation;
                        vendor.Carrier = data.Carrier;
                        vendor.ShipViaAccountNo = data.ShipViaAccountNo;
                        if (data.IsAlliance == false)
                        {
                            vendor.OrderingMethod = data.OrderingMethod;
                            vendor.ContactDetails = data.ContactDetails;
                        }
                        Update<AddressTP>(ContextFactory.CrmConnectionString, data.VendorAddress);
                        vendor.AddressId = data.VendorAddress.AddressId;
                        Update<FranchiseVendor>(ContextFactory.CrmConnectionString, vendor);
                        if (string.IsNullOrEmpty(returnMsg))
                        {
                            return Success;
                        }
                        else
                        {
                            return returnMsg;
                        }
                    }
                    if (data.VendorType == 2)
                    {
                        if (data.Shipvalue != null)
                        {
                            foreach (var item in data.Shipvalue)
                            {
                                if (item.AccountNumber != null)
                                    item.AccountNumber = item.AccountNumber.Trim();
                                var Query = "select * from [CRM].[VendorTerritoryAccount] where FranchiseId=@FranchiseId and VendorId=@VendorId and [TerritoryId]=@TerritoryId";
                                var result = connection.Query<VendorTerritoryAccount>(Query,
                                    new
                                    {
                                        VendorId = data.VendorId,
                                        FranchiseId = SessionManager.CurrentUser.FranchiseId,
                                        TerritoryId = item.TerritoryId
                                    }).FirstOrDefault();
                                if (result == null)
                                {
                                    if (item.VendorId == 0)
                                    {
                                        item.VendorId = data.VendorId;
                                    }
                                    if (item.FranchiseId == 0)
                                    {
                                        item.FranchiseId = SessionManager.CurrentUser.FranchiseId;
                                    }
                                    item.Id = item.PreviousId;
                                    var Insertvalue = connection.Insert(item);
                                }
                                else
                                {
                                    item.Id = item.PreviousId;
                                    var Updatevalue = connection.Update(item);
                                }

                            }
                        }

                        //Currency
                        var GetVendorCurrency = GetCurrencyTypeVal(data.VendorId);
                        vendor.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                        vendor.CreatedBy = SessionManager.CurrentUser.PersonId;
                        vendor.VendorIdPk = data.VendorIdPk;
                        vendor.VendorId = data.VendorId;
                        vendor.VendorType = data.VendorType;
                        Update(ContextFactory.CrmConnectionString, data.VendorAddress);
                        vendor.AddressId = data.VendorAddress.AddressId;
                        vendor.AccountRep = data.AccountRep;
                        vendor.AccountRepPhone = data.AccountRepPhone;
                        vendor.AccountRepEmail = data.AccountRepEmail;
                        vendor.AccountNo = data.AccountNo;
                        vendor.Terms = data.Terms;
                        vendor.CreditLimit = data.CreditLimit;
                        vendor.OrderingMethod = data.OrderingMethod;
                        vendor.ContactDetails = data.ContactDetails;
                        vendor.VendorStatus = data.VendorStatus;
                        //vendor.Currency = data.Currency;
                        vendor.Currency = GetVendorCurrency;
                        vendor.RetailBasis = data.RetailBasis;
                        //bulk
                        vendor.BulkPurchase = data.BulkPurchase;
                        vendor.ShipVia = data.ShipVia;
                        vendor.BPAccountNo = data.BPAccountNo;
                        vendor.BPShipLocation = data.BPShipLocation;
                        vendor.Carrier = data.Carrier;
                        vendor.ShipViaAccountNo = data.ShipViaAccountNo;
                        //vendor.IsActive = data.IsActive;
                        Update<FranchiseVendor>(ContextFactory.CrmConnectionString, vendor);
                        return Success;
                    }
                    if (data.VendorType == 4)
                    {
                        Update(ContextFactory.CrmConnectionString, data.VendorAddress);
                        vendor.AddressId = data.VendorAddress.AddressId;
                        vendor.AccountRep = data.AccountRep;
                        vendor.AccountRepPhone = data.AccountRepPhone;
                        vendor.AccountRepEmail = data.AccountRepEmail;
                        vendor.AccountNo = data.AccountNo;
                        vendor.Terms = data.Terms;
                        vendor.CreditLimit = data.CreditLimit;
                        vendor.OrderingMethod = data.OrderingMethod;
                        vendor.ContactDetails = data.ContactDetails;
                        vendor.VendorStatus = data.VendorStatus;
                        vendor.Currency = data.Currency;
                        vendor.RetailBasis = data.RetailBasis;
                        //vendor.CreatedOn = DateTime.Now;
                        vendor.VendorId = data.VendorId;
                        vendor.VendorType = data.VendorType;
                        vendor.VendorIdPk = data.VendorIdPk;
                        //bulk
                        vendor.BulkPurchase = data.BulkPurchase;
                        vendor.ShipVia = data.ShipVia;
                        vendor.BPAccountNo = data.BPAccountNo;
                        vendor.BPShipLocation = data.BPShipLocation;
                        vendor.Carrier = data.Carrier;
                        vendor.ShipViaAccountNo = data.ShipViaAccountNo;
                        vendor.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                        vendor.CreatedBy = SessionManager.CurrentUser.PersonId;
                        Update<FranchiseVendor>(ContextFactory.CrmConnectionString, vendor);
                        return Success;
                    }
                    if (data.VendorType == 3)
                    {
                        if (data.Shipvalue != null)
                        {
                            foreach (var item in data.Shipvalue)
                            {
                                if (item.AccountNumber != null)
                                    item.AccountNumber = item.AccountNumber.Trim();
                                var Query = "select * from [CRM].[VendorTerritoryAccount] where FranchiseId=@FranchiseId and VendorId=@VendorId and [TerritoryId]=@TerritoryId";
                                var result = connection.Query<VendorTerritoryAccount>(Query,
                                    new
                                    {
                                        VendorId = data.VendorId,
                                        FranchiseId = SessionManager.CurrentUser.FranchiseId,
                                        TerritoryId = item.TerritoryId
                                    }).FirstOrDefault();
                                if (result == null)
                                {
                                    if (item.VendorId == 0)
                                    {
                                        item.VendorId = data.VendorId;
                                    }
                                    if (item.FranchiseId == 0)
                                    {
                                        item.FranchiseId = SessionManager.CurrentUser.FranchiseId;
                                    }
                                    item.Id = item.PreviousId;
                                    var Insertvalue = connection.Insert(item);
                                }
                                else
                                {
                                    item.Id = item.PreviousId;
                                    var Updatevalue = connection.Update(item);
                                }

                            }
                        }

                        vendor.VendorIdPk = data.VendorIdPk;
                        Update<AddressTP>(ContextFactory.CrmConnectionString, data.VendorAddress);
                        Update<FranchiseVendor>(ContextFactory.CrmConnectionString, data);
                        return Success;
                    }
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw ex;
            }
            finally
            {
                connection.Close();
            }

        }

        return null;
    }

    public List<VendorListModel> GetVendors()
    {
        using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
        {
            connection.Open();

            //var query = @"SELECT hv.VendorIdPk,hv.VendorId,hv.Name,hv.VendorType as VendorTypeId,hvt.VendorType,hv.AccountRep,hv.AccountRepPhone,
            //              had.Address1+', '+had.City+', '+had.State+' '+had.CountryCode2Digits+' '+had.ZipCode as Location,
            //              case when fv.FranchiseId is not null then vs.VendorStatus else case when hv.IsActive=1 then 'Active' else 'InActive' end end VendorStatus FROM [Acct].[HFCVendors] hv 
            //              left JOIN [Acct].[FranchiseVendors] fv ON hv.VendorId = fv.VendorId and  fv.FranchiseId=@FranchiseId
            //              left join CRM.Type_VendorStatus vs on fv.VendorStatus=vs.Id
            //              left join [CRM].[Type_VendorType] hvt on hv.VendorType=hvt.Id
            //              left join [CRM].[Addresses] had on hv.AddressId=had.AddressId
            //              where hv.VendorType = 1
            //              union
            //              SELECT hv.VendorIdPk,hv.VendorId,hv.Name,hv.VendorType as VendorTypeId,hvt.VendorType,
            //              case when fv.FranchiseId is not null then fv.AccountRep else hv.AccountRep end as AccountRep,
            //              case when fv.FranchiseId is not null then fv.AccountRepPhone else hv.AccountRepPhone end as AccountRepPhone,
            //              case when fv.FranchiseId is not null then fad.Address1+', '+fad.City+', '+fad.State+' '+fad.CountryCode2Digits+' '+fad.ZipCode else had.Address1+', '+had.City+', '+had.State+' '+had.CountryCode2Digits+' '+had.ZipCode end as Location,
            //              case when fv.FranchiseId is not null then vs.VendorStatus else case when hv.IsActive=1 then 'Active' else 'InActive' end end VendorStatus FROM [Acct].[HFCVendors] hv 
            //              left join Acct.FranchiseVendors fv  ON hv.VendorId = fv.VendorId and  fv.FranchiseId=@FranchiseId
            //              left join CRM.Type_VendorStatus vs on fv.VendorStatus=vs.Id
            //              left join [CRM].[Type_VendorType] hvt on hv.VendorType=hvt.Id
            //              left join [CRM].[Addresses] fad on fv.AddressId=fad.AddressId
            //              left join [CRM].[Addresses] had on hv.AddressId=had.AddressId
            //              where hv.VendorType = 2
            //              union
            var query = @"SELECT VendorIdPk,VendorId,Name,fv.VendorType as VendorTypeId,vt.VendorType,AccountRep,AccountRepPhone,fv.OrderingMethod,
                          Address1+', '+City+', '+State+' '+CountryCode2Digits+' '+ZipCode as Location,vs.VendorStatus from Acct.FranchiseVendors fv 
                          left join CRM.Type_VendorStatus vs on fv.VendorStatus=vs.Id
                          left join [CRM].[Type_VendorType] vt on fv.VendorType=vt.Id
                          left join [CRM].[Addresses] ad on fv.AddressId=ad.AddressId
                          where fv.FranchiseId=@FranchiseId and fv.VendorType=3 and fv.VendorStatus=1 Order by Name asc";
            var result = connection.Query<VendorListModel>(query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
            if (result != null)
            {
                foreach (var item in result)
                {
                    if (item.OrderingMethod != "")
                    {
                        var OrderingId = item.OrderingMethod;
                        switch (OrderingId)
                        {
                            case "1": item.OrderingMethodName = "EMail"; break;
                            case "2": item.OrderingMethodName = "Phone"; break;
                            case "3": item.OrderingMethodName = "Fax"; break;
                            case "4": item.OrderingMethodName = "Vendor Portal"; break;
                            case "5": item.OrderingMethodName = "Will Call"; break;
                                //case "5": item.OrderingMethodName = "PIC"; break;
                        }
                    }
                }
            }
            if (result == null)
            {
                return null;
            }


            //var addresset = connection.Query<Address>(
            //   "select * from CRM.Addresses where AddressId in @Ids",
            //    new { Ids = result.Select(a => a.AddressId).Distinct() });

            //foreach (var item in result)
            //{
            //    if (item.AddressId != null)
            //    {
            //        var address = addresset.Where(x => x.AddressId == item.AddressId).FirstOrDefault();
            //        //item.VendorAddress = address;
            //        item.AddressCityZip = address.Address1 + "," + address.Address2 + "," + address.City + "," + address.State + "," + address.ZipCode;
            //    }
            //    else
            //    {
            //        item.AddressCityZip = " ";
            //    }
            //}

            connection.Close();
            return result.OrderBy(x => x.Name).ToList();
        }
    }

    public List<VendorListModel> GetVendorsDatas(int id, int value)
    {
        var query = "";
        using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
        {
            connection.Open();
            if (value == 1)
            {
                query = @"SELECT hv.VendorIdPk,hv.VendorId,hv.Name,hv.VendorType as VendorTypeId,hv.IsAlliance,
                          hvt.VendorType,hv.AccountRep,hv.AccountRepPhone,
                          had.Address1+', '+had.City+', '+had.State+' '+had.CountryCode2Digits+' '+had.ZipCode as Location,
                          case when fv.FranchiseId is not null then fv.OrderingMethod else hv.OrderingMethod end as OrderingMethod,
                          case when fv.FranchiseId is not null then vs.VendorStatus else 'InActive' end VendorStatus FROM [Acct].[HFCVendors] hv 
                          left JOIN [Acct].[FranchiseVendors] fv ON hv.VendorId = fv.VendorId and  fv.FranchiseId=@FranchiseId
                          left join CRM.Type_VendorStatus vs on fv.VendorStatus=vs.Id
                          left join [CRM].[Type_VendorType] hvt on hv.VendorType=hvt.Id
                          left join [CRM].[Addresses] had on hv.AddressId=had.AddressId
                          where hv.VendorType = 1 and hv.IsActive=1 ";
                if (BrandIdvalue == 1)
                {
                    query = query + " and hv.BudgetBlind = 1 Order by Name asc";
                }
                else if (BrandIdvalue == 2)
                {
                    query = query + " and hv.TailoredLiving = 1 Order by Name asc";
                }
                else
                {
                    query = query + " and hv.ConcreteCraft = 1 Order by Name asc";
                }
            }
            else if (value == 2)
            {
                query = @"SELECT hv.VendorIdPk,hv.VendorId,hv.Name,hv.VendorType as VendorTypeId,hvt.VendorType,
                            case when fv.FranchiseId is not null then fv.AccountRep else hv.AccountRep end as AccountRep,
                            case when fv.FranchiseId is not null then fv.OrderingMethod else hv.OrderingMethod end as OrderingMethod,
                            case when fv.FranchiseId is not null then fv.AccountRepPhone else hv.AccountRepPhone end as AccountRepPhone,
                            case when fv.FranchiseId is not null then fad.Address1 + ', ' + fad.City + ', ' + fad.State + ' ' + fad.CountryCode2Digits + ' ' + fad.ZipCode else had.Address1 + ', ' + had.City + ', ' + had.State + ' ' + had.CountryCode2Digits + ' ' + had.ZipCode end as Location,
                            case when fv.FranchiseId is not null then vs.VendorStatus else 'InActive' end VendorStatus FROM[Acct].[HFCVendors] hv
                            left join Acct.FranchiseVendors fv  ON hv.VendorId = fv.VendorId and fv.FranchiseId = @FranchiseId
                            left join CRM.Type_VendorStatus vs on fv.VendorStatus = vs.Id
                            left join[CRM].[Type_VendorType] hvt on hv.VendorType=hvt.Id
                            left join[CRM].[Addresses] fad on fv.AddressId=fad.AddressId
                            left join[CRM].[Addresses] had on hv.AddressId=had.AddressId
                            where hv.VendorType = 2 and hv.IsActive=1";
                if (BrandIdvalue == 1)
                {
                    query = query + " and hv.BudgetBlind = 1 Order by Name asc";
                }
                else if (BrandIdvalue == 2)
                {
                    query = query + " and hv.TailoredLiving = 1 Order by Name asc";
                }
                else
                {
                    query = query + " and hv.ConcreteCraft = 1 Order by Name asc";
                }
            }
            else if (value == 3)
            {
                query = @"SELECT hv.VendorIdPk,hv.VendorId,hv.Name,hv.VendorType as VendorTypeId,hvt.VendorType,
                          case when fv.FranchiseId is not null then fv.AccountRep else hv.AccountRep end as AccountRep,
                          case when fv.FranchiseId is not null then fv.AccountRepPhone else hv.AccountRepPhone end as AccountRepPhone,
                          case when fv.FranchiseId is not null then fad.Address1 + ', ' + fad.City + ', ' + fad.State + ' ' + 
                          fad.CountryCode2Digits + ' ' + fad.ZipCode else had.Address1 + ', ' + had.City + ', ' + had.State + ' ' + 
                          had.CountryCode2Digits + ' ' + had.ZipCode end as Location,
                          case when fv.FranchiseId is not null then vs.VendorStatus else 'InActive' end VendorStatus 
                          FROM[Acct].[HFCVendors] hv
                          left join Acct.FranchiseVendors fv  ON hv.VendorId = fv.VendorId and fv.FranchiseId = @FranchiseId
                          left join CRM.Type_VendorStatus vs on fv.VendorStatus = vs.Id
                          left join[CRM].[Type_VendorType] hvt on hv.VendorType=hvt.Id
                          left join[CRM].[Addresses] fad on fv.AddressId=fad.AddressId
                          left join[CRM].[Addresses] had on hv.AddressId=had.AddressId
                          where hv.VendorType = 4 Order by Name asc";
            }
            else
            {
                query = @"SELECT VendorIdPk,VendorId,Name,fv.VendorType as VendorTypeId,fv.OrderingMethod,vt.VendorType,AccountRep,AccountRepPhone,
                            Address1+', '+City+', '+State+' '+CountryCode2Digits+' '+ZipCode as Location,vs.VendorStatus from Acct.FranchiseVendors fv 
                            left join CRM.Type_VendorStatus vs on fv.VendorStatus=vs.Id
                            left join [CRM].[Type_VendorType] vt on fv.VendorType=vt.Id
                            left join [CRM].[Addresses] ad on fv.AddressId=ad.AddressId
                            where fv.VendorType=3 and fv.FranchiseId=@FranchiseId Order by Name asc";
            }
            //query = @"SELECT hv.VendorIdPk,hv.VendorId,hv.Name,hv.VendorType as VendorTypeId,hvt.VendorType,hv.AccountRep,hv.AccountRepPhone,
            //                had.Address1+', '+had.City+', '+had.State+' '+had.CountryCode2Digits+' '+had.ZipCode as Location,
            //                case when fv.FranchiseId is not null then vs.VendorStatus else 'InActive' end VendorStatus FROM [Acct].[HFCVendors] hv 
            //                left JOIN [Acct].[FranchiseVendors] fv ON hv.VendorId = fv.VendorId and  fv.FranchiseId=@FranchiseId
            //                left join CRM.Type_VendorStatus vs on fv.VendorStatus=vs.Id
            //                left join [CRM].[Type_VendorType] hvt on hv.VendorType=hvt.Id
            //                left join [CRM].[Addresses] had on hv.AddressId=had.AddressId
            //                where hv.VendorType = 1 
            //                union
            //                SELECT hv.VendorIdPk,hv.VendorId,hv.Name,hv.VendorType as VendorTypeId,hvt.VendorType,
            //                case when fv.FranchiseId is not null then fv.AccountRep else hv.AccountRep end as AccountRep,
            //                case when fv.FranchiseId is not null then fv.AccountRepPhone else hv.AccountRepPhone end as AccountRepPhone,
            //                case when fv.FranchiseId is not null then fad.Address1+', '+fad.City+', '+fad.State+' '+fad.CountryCode2Digits+' '+fad.ZipCode else had.Address1+', '+had.City+', '+had.State+' '+had.CountryCode2Digits+' '+had.ZipCode end as Location,
            //                case when fv.FranchiseId is not null then vs.VendorStatus else 'InActive' end VendorStatus FROM [Acct].[HFCVendors] hv 
            //                left join Acct.FranchiseVendors fv  ON hv.VendorId = fv.VendorId and  fv.FranchiseId=@FranchiseId
            //                left join CRM.Type_VendorStatus vs on fv.VendorStatus=vs.Id
            //                left join [CRM].[Type_VendorType] hvt on hv.VendorType=hvt.Id
            //                left join [CRM].[Addresses] fad on fv.AddressId=fad.AddressId
            //                left join [CRM].[Addresses] had on hv.AddressId=had.AddressId
            //                where hv.VendorType = 2
            //                union
            //                SELECT VendorIdPk,VendorId,Name,fv.VendorType as VendorTypeId,vt.VendorType,AccountRep,AccountRepPhone,
            //                Address1+', '+City+', '+State+' '+CountryCode2Digits+' '+ZipCode as Location,vs.VendorStatus from Acct.FranchiseVendors fv 
            //                left join CRM.Type_VendorStatus vs on fv.VendorStatus=vs.Id
            //                left join [CRM].[Type_VendorType] vt on fv.VendorType=vt.Id
            //                left join [CRM].[Addresses] ad on fv.AddressId=ad.AddressId
            //                where fv.VendorType=3 Order by Name asc";
            var result = connection.Query<VendorListModel>(query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();

            if (result != null)
            {
                foreach (var item in result)
                {
                    if (value == 1)
                    {
                        if (item.IsAlliance == true)
                        {
                            item.OrderingMethodName = "PIC";
                        }
                        else if (item.IsAlliance == false && item.OrderingMethod != null)
                        {
                            var OrderingId = item.OrderingMethod;
                            switch (OrderingId)
                            {
                                case "1": item.OrderingMethodName = "EMail"; break;
                                case "2": item.OrderingMethodName = "Phone"; break;
                                case "3": item.OrderingMethodName = "Fax"; break;
                                case "4": item.OrderingMethodName = "Vendor Portal"; break;
                                case "5": item.OrderingMethodName = "Will Call"; break;
                                    //case "5": item.OrderingMethodName = "PIC"; break;
                            }
                        }
                    }
                    else
                    {
                        var OrderingId = item.OrderingMethod;
                        switch (OrderingId)
                        {
                            case "1": item.OrderingMethodName = "EMail"; break;
                            case "2": item.OrderingMethodName = "Phone"; break;
                            case "3": item.OrderingMethodName = "Fax"; break;
                            case "4": item.OrderingMethodName = "Vendor Portal"; break;
                            case "5": item.OrderingMethodName = "Will Call"; break;
                                //case "5": item.OrderingMethodName = "PIC"; break;
                        }
                    }
                }
            }

            if (result == null)
            {
                return null;
            }
            connection.Close();
            return result.OrderBy(x => x.Name).ToList();
        }
    }

    public FranchiseVendor GetLocalvendor(int id)
    {
        using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
        {
            var vendorType = 1;
            //var results = null;
            FranchiseVendor results = new FranchiseVendor();
            connection.Open();


            var query = "select f.VendorType from [Acct].[FranchiseVendors] f where VendorId=@VendorId";
            var franchiseVendor = connection.Query<FranchiseVendor>(query, new { VendorId = id }).FirstOrDefault();

            if (franchiseVendor == null)
            {
                query = "select a.VendorType from [Acct].[HFCVendors] a where VendorId=@VendorId";
                //results = connection.Query<HFCVendor>(query, new { VendorId = id }).FirstOrDefault();
                var result = connection.Query<HFCVendor>(query, new { VendorId = id }).FirstOrDefault();
                if (result == null)
                {
                    return null;
                }
                vendorType = result.VendorType; /*== true ? 1 : 2;*/
            }
            else
            {
                // we have all the vendor type in the local vendor table, so the value can be dirctly assigned 
                vendorType = franchiseVendor.VendorType;
            }
            switch (vendorType)
            {
                case 1:
                    //query = "select a.*,b.AccountNo,b.Terms,b.CreditLimit from [Acct].[HFCVendors] a join Acct.FranchiseVendors as b on a.VendorId = b.VendorId";
                    query = @"SELECT hv.VendorIdPk,hv.VendorId,hv.Name,hv.VendorType as VendorType, fv.BulkPurchase, fv.ShipVia, fv.Carrier, fv.ShipViaAccountNo,
                              hv.OrderingMethod,hv.PICVendorId,hv.IsAlliance,hv.Beta,
                              case when fv.FranchiseId is not null then fv.CreditLimit else '' end as CreditLimit,
                              case when fv.FranchiseId is not null then fv.OrderingMethod else hv.OrderingMethod end as OrderingMethod,
                              case when fv.FranchiseId is not null then fv.Terms else '' end as Terms,
                              case when fv.FranchiseId is not null then fv.Currency else '' end as Currency,
                              case when fv.FranchiseId is not null then fv.RetailBasis else '' end as RetailBasis,
                              case when fv.FranchiseId is not null then fv.AccountNo else '' end as AccountNo,
                              case when fv.FranchiseId is not null then fv.AccountRep else hv.AccountRep end as AccountRep,
                              case when fv.FranchiseId is not null then fv.AccountRepPhone else hv.AccountRepPhone end as AccountRepPhone,
                              case when fv.FranchiseId is not null then fv.AccountRepEmail else hv.AccountRepEmail end as AccountRepEmail,
                              case when fv.FranchiseId is not null then fv.ContactDetails else hv.ContactDetails end as ContactDetails,
                              case when fv.FranchiseId is not null and (fv.AddressId is not null) then fv.AddressId else hv.AddressId end as AddressId,
                              case when fv.FranchiseId is not null then fv.VendorStatus else 2 end VendorStatus
                              FROM [Acct].[HFCVendors] hv 
                              left JOIN [Acct].[FranchiseVendors] fv ON hv.VendorId = fv.VendorId and  fv.FranchiseId=@FranchiseId
                              left join CRM.Type_VendorStatus vs on fv.VendorStatus=vs.Id
                              left join [CRM].[Type_VendorType] hvt on hv.VendorType=hvt.Id
                              where hv.VendorType = 1 and hv.VendorId=@VendorId";
                    results = connection.Query<FranchiseVendor>(query, new { VendorId = id, FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).FirstOrDefault();
                    results.VendorType = 1;
                    //results.OrderingName = "PIC";
                    //results.ContactDetails = "Touchpoint Supply Chain Enabled";
                    //results.OrderingMethod = results.OrderingName;
                    break;
                case 2:
                    //query = "select a.* ,b.AccountRep,b.AccountRepPhone,b.AccountRepEmail,b.OrderingMethod,b.ContactDetails from[Acct].[HFCVendors] a join Acct.FranchiseVendors as b on a.VendorId = b.VendorId";
                    query = @"SELECT hv.VendorIdPk,hv.VendorId,hv.Name,hv.VendorType as VendorTypeId,hv.Beta, fv.BulkPurchase, fv.ShipVia, fv.Carrier, fv.ShipViaAccountNo,
                               case when fv.FranchiseId is not null then fv.AccountRep else hv.AccountRep end as AccountRep,
                               case when fv.FranchiseId is not null then fv.AccountRepPhone else hv.AccountRepPhone end as AccountRepPhone,
                               case when fv.FranchiseId is not null then fv.AddressId else hv.AddressId end as AddressId,
                               case when fv.FranchiseId is not null then fv.AccountNo else '' end as AccountNo,
                               case when fv.FranchiseId is not null then fv.Terms else '' end as Terms,
                               case when fv.FranchiseId is not null then fv.CreditLimit else '' end as CreditLimit,
                               case when fv.FranchiseId is not null then fv.OrderingMethod else hv.OrderingMethod end as OrderingMethod,
                               case when fv.FranchiseId is not null then fv.ContactDetails else hv.ContactDetails end as ContactDetails,
                               case when fv.FranchiseId is not null then fv.Currency else '' end as Currency,
                               case when fv.FranchiseId is not null then fv.RetailBasis else '' end as RetailBasis,
                               case when fv.FranchiseId is not null then fv.AccountRepEmail else hv.AccountRepEmail end as AccountRepEmail,
                               case when fv.FranchiseId is not null then fv.VendorStatus else 2 end VendorStatus FROM [Acct].[HFCVendors] hv 
                               left join Acct.FranchiseVendors fv  ON hv.VendorId = fv.VendorId and  fv.FranchiseId=@FranchiseId
                               where hv.VendorType = 2 and hv.VendorId=@VendorId";
                    results = connection.Query<FranchiseVendor>(query, new { VendorId = id, FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).FirstOrDefault();
                    results.VendorType = 2;
                    break;
                case 3:
                    query = @"Select a.*,b.CurrencyCode From Acct.FranchiseVendors a
                             left join CRM.Type_CountryCodes b on a.Currency = b.CountryCodeId
                            where a.VendorId = @VendorId and a.FranchiseId = @FranchiseId";
                    results = connection.Query<FranchiseVendor>(query, new { VendorId = id, FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).FirstOrDefault();
                    results.VendorType = 3;
                    break;

                    //added the code for the non pic alliance vendor.

                    //case 4:
                    //    query = @"SELECT hv.VendorIdPk,hv.VendorId,hv.Name,hv.VendorType as VendorTypeId,
                    //              case when fv.FranchiseId is not null then fv.AccountRep else hv.AccountRep end as AccountRep,
                    //              case when fv.FranchiseId is not null then fv.AccountRepPhone else hv.AccountRepPhone end as AccountRepPhone,
                    //              case when fv.FranchiseId is not null then fv.AddressId else hv.AddressId end as AddressId,
                    //              case when fv.FranchiseId is not null then fv.AccountNo else '' end as AccountNo,
                    //              case when fv.FranchiseId is not null then fv.Terms else '' end as Terms,
                    //              case when fv.FranchiseId is not null then fv.CreditLimit else '' end as CreditLimit,
                    //              case when fv.FranchiseId is not null then fv.OrderingMethod else hv.OrderingMethod end as OrderingMethod,
                    //              case when fv.FranchiseId is not null then fv.ContactDetails else hv.ContactDetails end as ContactDetails,
                    //              case when fv.FranchiseId is not null then fv.Currency else '' end as Currency,
                    //              case when fv.FranchiseId is not null then fv.RetailBasis else '' end as RetailBasis,
                    //              case when fv.FranchiseId is not null then fv.AccountRepEmail else hv.AccountRepEmail end as AccountRepEmail,
                    //              case when fv.FranchiseId is not null then fv.VendorStatus else 2 end VendorStatus FROM [Acct].[HFCVendors] hv 
                    //              left join Acct.FranchiseVendors fv  ON hv.VendorId = fv.VendorId and  fv.FranchiseId=@FranchiseId
                    //              where hv.VendorType = 4 and hv.VendorId=@VendorId";

                    //    results = connection.Query<FranchiseVendor>(query, new { VendorId = id, FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).FirstOrDefault();
                    //    results.VendorType = 4;
                    //    break;
            }
            results.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;

            if (results.AddressId != null)
            {
                query = "Select * from CRM.Addresses where AddressId=@AddressId ";
                var address = connection.Query<AddressTP>(query, new { AddressId = results.AddressId }).FirstOrDefault();
                results.VendorAddress = address;

            }

            if (results != null)
            {
                if (results.VendorType == 1)
                {
                    if (results.IsAlliance == true)
                    {
                        results.OrderingName = "PIC";
                        results.ContactDetails = "Touchpoint Supply Chain Enabled";
                        results.OrderingMethod = results.OrderingName;
                    }
                    else if (results.IsAlliance == false && results.OrderingMethod != null)
                    {
                        var OrderingId = results.OrderingMethod;
                        switch (OrderingId)
                        {
                            case "1": results.OrderingName = "EMail"; break;
                            case "2": results.OrderingName = "Phone"; break;
                            case "3": results.OrderingName = "Fax"; break;
                            case "4": results.OrderingName = "Vendor Portal"; break;
                            case "5": results.OrderingName = "Will Call"; break;
                                //case "5": results.OrderingName = "PIC"; break;
                        }
                    }
                }
                else
                {
                    var OrderingId = results.OrderingMethod;
                    switch (OrderingId)
                    {
                        case "1": results.OrderingName = "EMail"; break;
                        case "2": results.OrderingName = "Phone"; break;
                        case "3": results.OrderingName = "Fax"; break;
                        case "4": results.OrderingName = "Vendor Portal"; break;
                        case "5": results.OrderingName = "Will Call"; break;
                            //case "5": results.OrderingName = "PIC"; break;
                    }
                }
            }

            //var OrderingId = results.OrderingMethod;
            //switch (OrderingId)
            //{
            //    case "1": results.OrderingName = "EMail"; break;
            //    case "2": results.OrderingName = "Phone"; break;
            //    case "3": results.OrderingName = "Fax"; break;
            //    case "4": results.OrderingName = "Vendor Portal"; break;
            //    case "5": results.OrderingName = "PIC"; break;
            //}


            var vendortypeId = results.VendorType;
            if (vendortypeId == 1)
            {
                results.VendorName = "Alliance Vendor";
            }
            else if (vendortypeId == 2)
            {
                results.VendorName = "Non-Alliance Vendor";
            }
            //else if (vendortypeId == 4)
            //{
            //    results.VendorName = "Non-PIC enabled Alliance Vendor";
            //}
            else
            {
                results.VendorName = "My Vendor";
            }

            var Acctstatus = results.VendorStatus;
            if (Acctstatus == 1)
            {
                results.AcctStatus = "Active";
            }
            else if (Acctstatus == 2)
            {
                results.AcctStatus = "Inactive";
            }
            else
            {
                results.AcctStatus = "Suspended";
            }
            connection.Close();

            return results;
            //return null;
        }
    }

    public FranchiseVendor GetLocalvendor(int id, int FranchiseID)
    {
        using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
        {
            var vendorType = 1;
            //var results = null;
            FranchiseVendor results = new FranchiseVendor();
            connection.Open();


            var query = "select f.VendorType from [Acct].[FranchiseVendors] f where VendorId=@VendorId";
            var franchiseVendor = connection.Query<FranchiseVendor>(query, new { VendorId = id }).FirstOrDefault();

            if (franchiseVendor == null)
            {
                query = "select a.VendorType from [Acct].[HFCVendors] a where VendorId=@VendorId";
                //results = connection.Query<HFCVendor>(query, new { VendorId = id }).FirstOrDefault();
                var result = connection.Query<HFCVendor>(query, new { VendorId = id }).FirstOrDefault();
                if (result == null)
                {
                    return null;
                }
                vendorType = result.VendorType; /*== true ? 1 : 2;*/
            }
            else
            {
                // we have all the vendor type in the local vendor table, so the value can be dirctly assigned 
                vendorType = franchiseVendor.VendorType;
            }
            switch (vendorType)
            {
                case 1:
                    //query = "select a.*,b.AccountNo,b.Terms,b.CreditLimit from [Acct].[HFCVendors] a join Acct.FranchiseVendors as b on a.VendorId = b.VendorId";
                    query = @"SELECT hv.VendorIdPk,hv.VendorId,hv.Name,hv.VendorType as VendorType,
                              hv.OrderingMethod,hv.PICVendorId,hv.IsAlliance,hv.Beta,
                              case when fv.FranchiseId is not null then fv.CreditLimit else '' end as CreditLimit,
                              case when fv.FranchiseId is not null then fv.OrderingMethod else hv.OrderingMethod end as OrderingMethod,
                              case when fv.FranchiseId is not null then fv.Terms else '' end as Terms,
                              case when fv.FranchiseId is not null then fv.Currency else '' end as Currency,
                              case when fv.FranchiseId is not null then fv.RetailBasis else '' end as RetailBasis,
                              case when fv.FranchiseId is not null then fv.AccountNo else '' end as AccountNo,
                              case when fv.FranchiseId is not null then fv.AccountRep else hv.AccountRep end as AccountRep,
                              case when fv.FranchiseId is not null then fv.AccountRepPhone else hv.AccountRepPhone end as AccountRepPhone,
                              case when fv.FranchiseId is not null then fv.AccountRepEmail else hv.AccountRepEmail end as AccountRepEmail,
                              case when fv.FranchiseId is not null then fv.ContactDetails else hv.ContactDetails end as ContactDetails,
                              case when fv.FranchiseId is not null and (fv.AddressId is not null) then fv.AddressId else hv.AddressId end as AddressId,
                              case when fv.FranchiseId is not null then fv.VendorStatus else 2 end VendorStatus
                              FROM [Acct].[HFCVendors] hv 
                              left JOIN [Acct].[FranchiseVendors] fv ON hv.VendorId = fv.VendorId and  fv.FranchiseId=@FranchiseId
                              left join CRM.Type_VendorStatus vs on fv.VendorStatus=vs.Id
                              left join [CRM].[Type_VendorType] hvt on hv.VendorType=hvt.Id
                              where hv.VendorType = 1 and hv.VendorId=@VendorId";
                    results = connection.Query<FranchiseVendor>(query, new { VendorId = id, FranchiseId = FranchiseID }).FirstOrDefault();
                    results.VendorType = 1;
                    //results.OrderingName = "PIC";
                    //results.ContactDetails = "Touchpoint Supply Chain Enabled";
                    //results.OrderingMethod = results.OrderingName;
                    break;
                case 2:
                    //query = "select a.* ,b.AccountRep,b.AccountRepPhone,b.AccountRepEmail,b.OrderingMethod,b.ContactDetails from[Acct].[HFCVendors] a join Acct.FranchiseVendors as b on a.VendorId = b.VendorId";
                    query = @"SELECT hv.VendorIdPk,hv.VendorId,hv.Name,hv.VendorType as VendorTypeId,hv.Beta,
                               case when fv.FranchiseId is not null then fv.AccountRep else hv.AccountRep end as AccountRep,
                               case when fv.FranchiseId is not null then fv.AccountRepPhone else hv.AccountRepPhone end as AccountRepPhone,
                               case when fv.FranchiseId is not null then fv.AddressId else hv.AddressId end as AddressId,
                               case when fv.FranchiseId is not null then fv.AccountNo else '' end as AccountNo,
                               case when fv.FranchiseId is not null then fv.Terms else '' end as Terms,
                               case when fv.FranchiseId is not null then fv.CreditLimit else '' end as CreditLimit,
                               case when fv.FranchiseId is not null then fv.OrderingMethod else hv.OrderingMethod end as OrderingMethod,
                               case when fv.FranchiseId is not null then fv.ContactDetails else hv.ContactDetails end as ContactDetails,
                               case when fv.FranchiseId is not null then fv.Currency else '' end as Currency,
                               case when fv.FranchiseId is not null then fv.RetailBasis else '' end as RetailBasis,
                               case when fv.FranchiseId is not null then fv.AccountRepEmail else hv.AccountRepEmail end as AccountRepEmail,
                               case when fv.FranchiseId is not null then fv.VendorStatus else 2 end VendorStatus FROM [Acct].[HFCVendors] hv 
                               left join Acct.FranchiseVendors fv  ON hv.VendorId = fv.VendorId and  fv.FranchiseId=@FranchiseId
                               where hv.VendorType = 2 and hv.VendorId=@VendorId";
                    results = connection.Query<FranchiseVendor>(query, new { VendorId = id, FranchiseId = FranchiseID }).FirstOrDefault();
                    results.VendorType = 2;
                    break;
                case 3:
                    query = @"Select a.*,b.CurrencyCode From Acct.FranchiseVendors a
                             left join CRM.Type_CountryCodes b on a.Currency = b.CountryCodeId
                            where a.VendorId = @VendorId and a.FranchiseId = @FranchiseId";
                    results = connection.Query<FranchiseVendor>(query, new { VendorId = id, FranchiseId = FranchiseID }).FirstOrDefault();
                    results.VendorType = 3;
                    break;

            }
            results.FranchiseId = FranchiseID;

            if (results.AddressId != null)
            {
                query = "Select * from CRM.Addresses where AddressId=@AddressId ";
                var address = connection.Query<AddressTP>(query, new { AddressId = results.AddressId }).FirstOrDefault();
                results.VendorAddress = address;

            }

            if (results != null)
            {
                if (results.VendorType == 1)
                {
                    if (results.IsAlliance == true)
                    {
                        results.OrderingName = "PIC";
                        results.ContactDetails = "Touchpoint Supply Chain Enabled";
                        results.OrderingMethod = results.OrderingName;
                    }
                    else if (results.IsAlliance == false && results.OrderingMethod != null)
                    {
                        var OrderingId = results.OrderingMethod;
                        switch (OrderingId)
                        {
                            case "1": results.OrderingName = "EMail"; break;
                            case "2": results.OrderingName = "Phone"; break;
                            case "3": results.OrderingName = "Fax"; break;
                            case "4": results.OrderingName = "Vendor Portal"; break;
                            case "5": results.OrderingName = "Will Call"; break;
                                //case "5": results.OrderingName = "PIC"; break;
                        }
                    }
                }
                else
                {
                    var OrderingId = results.OrderingMethod;
                    switch (OrderingId)
                    {
                        case "1": results.OrderingName = "EMail"; break;
                        case "2": results.OrderingName = "Phone"; break;
                        case "3": results.OrderingName = "Fax"; break;
                        case "4": results.OrderingName = "Vendor Portal"; break;
                        case "5": results.OrderingName = "Will Call"; break;
                            //case "5": results.OrderingName = "PIC"; break;
                    }
                }
            }

            //var OrderingId = results.OrderingMethod;
            //switch (OrderingId)
            //{
            //    case "1": results.OrderingName = "EMail"; break;
            //    case "2": results.OrderingName = "Phone"; break;
            //    case "3": results.OrderingName = "Fax"; break;
            //    case "4": results.OrderingName = "Vendor Portal"; break;
            //    case "5": results.OrderingName = "PIC"; break;
            //}


            var vendortypeId = results.VendorType;
            if (vendortypeId == 1)
            {
                results.VendorName = "Alliance Vendor";
            }
            else if (vendortypeId == 2)
            {
                results.VendorName = "Non-Alliance Vendor";
            }
            //else if (vendortypeId == 4)
            //{
            //    results.VendorName = "Non-PIC enabled Alliance Vendor";
            //}
            else
            {
                results.VendorName = "MyVendor";
            }

            var Acctstatus = results.VendorStatus;
            if (Acctstatus == 1)
            {
                results.AcctStatus = "Active";
            }
            else if (Acctstatus == 2)
            {
                results.AcctStatus = "Inactive";
            }
            else
            {
                results.AcctStatus = "Suspended";
            }
            connection.Close();

            return results;
            //return null;
        }
    }

    public override HFCVendor Get(int id)
    {
        throw new NotImplementedException();
    }

    public List<VendorListModel> Getvalue(int id)
    {
        using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
        {
            List<VendorListModel> result = new List<VendorListModel>();
            connection.Open();
            switch (id)
            {
                case 1:

                    //var query = @"SELECT hv.VendorIdPk,hv.VendorId,hv.Name,hv.VendorType as VendorTypeId,hvt.VendorType,
                    //              hv.AccountRep,hv.AccountRepPhone,
                    //              had.Address1 + ', ' + had.City + ', ' + had.State + ' ' + had.CountryCode2Digits + ' ' + had.ZipCode as Location,
                    //              case when fv.FranchiseId is not null then vs.VendorStatus else case when hv.IsActive = 1 then 
                    //              'Active' else 'InActive' end end VendorStatus FROM[Acct].[HFCVendors] hv 
                    //              join [Acct].[FranchiseVendors]fv ON hv.VendorId = fv.VendorId 
                    //              left join CRM.Type_VendorStatus vs on fv.VendorStatus= vs.Id
                    //              left join [CRM].[Type_VendorType] hvt on hv.VendorType= hvt.Id
                    //              left join [CRM].[Addresses] had on hv.AddressId= had.AddressId
                    //              where hv.VendorType = 1 and hv.IsActive = 1 and  fv.FranchiseId=@FranchiseId";
                    var query = @"SELECT hv.VendorIdPk,hv.VendorId,hv.Name,hv.VendorType as VendorTypeId,hvt.VendorType,hv.IsAlliance,
                                  case when fv.FranchiseId is not null and fv.AccountRep is not null then fv.AccountRep else hv.AccountRep end as AccountRep,
                                  case when fv.FranchiseId is not null and fv.OrderingMethod is not null then fv.OrderingMethod else hv.OrderingMethod end as OrderingMethod,
                                  case when fv.FranchiseId is not null and fv.AccountRepPhone is not null then fv.AccountRepPhone 
                                  else hv.AccountRepPhone end as AccountRepPhone,
                                  case when fv.FranchiseId is not null and fv.AddressId is not null then 
                                  fad.Address1 + ', '+fad.Address2+',' + fad.City + ', ' + fad.State + ' ' + fad.CountryCode2Digits + ' ' + 
                                  fad.ZipCode else
                                  had.Address1 + ', '+had.Address2+',' + had.City + ', ' + had.State + ' ' + had.CountryCode2Digits + ' ' + 
                                  had.ZipCode end as Location,
                                  case when fv.FranchiseId is not null then vs.VendorStatus else case when hv.IsActive = 1 then 
                                  'Active' else 'InActive' end end VendorStatus FROM[Acct].[HFCVendors] hv 
                                  join [Acct].[FranchiseVendors]fv ON hv.VendorId = fv.VendorId 
                                  left join CRM.Type_VendorStatus vs on fv.VendorStatus= vs.Id
                                  left join [CRM].[Type_VendorType] hvt on hv.VendorType= hvt.Id
                                  left join [CRM].[Addresses] had on hv.AddressId= had.AddressId
                                  left join [CRM].[Addresses] fad on fv.AddressId=fad.AddressId
                                  where hv.VendorType = 1 and hv.IsActive=1 and fv.VendorStatus = 1 and  fv.FranchiseId=@FranchiseId";
                    if (BrandIdvalue == 1)
                    {
                        query = query + " and hv.BudgetBlind = 1";
                    }
                    else if (BrandIdvalue == 2)
                    {
                        query = query + " and hv.TailoredLiving = 1";
                    }
                    else
                    {
                        query = query + " and hv.ConcreteCraft = 1";
                    }
                    result = connection.Query<VendorListModel>(query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                    break;
                case 2:
                    query = @" SELECT hv.VendorIdPk,hv.VendorId,hv.Name,hv.VendorType as VendorTypeId,hvt.VendorType,
                             case when fv.FranchiseId is not null then fv.AccountRep else hv.AccountRep end as AccountRep,
                             case when fv.FranchiseId is not null then fv.OrderingMethod else hv.OrderingMethod end as OrderingMethod,
                             case when fv.FranchiseId is not null then fv.AccountRepPhone else hv.AccountRepPhone end as AccountRepPhone,
                             case when fv.FranchiseId is not null then fad.Address1 + ', ' + fad.City + ', ' + fad.State + ' ' + fad.CountryCode2Digits + ' ' + fad.ZipCode else had.Address1 + ', ' + had.City + ', ' + had.State + ' ' + had.CountryCode2Digits + ' ' + had.ZipCode end as Location,
                             case when fv.FranchiseId is not null then vs.VendorStatus else case when hv.IsActive = 1 then 'Active' else 'InActive' end end VendorStatus FROM[Acct].[HFCVendors] hv
                             left join Acct.FranchiseVendors fv ON hv.VendorId = fv.VendorId and fv.FranchiseId= @FranchiseId
                             left join CRM.Type_VendorStatus vs on fv.VendorStatus= vs.Id
                             left join [CRM].[Type_VendorType] hvt on hv.VendorType= hvt.Id
                             left join [CRM].[Addresses] fad on fv.AddressId= fad.AddressId
                             left join [CRM].[Addresses] had on hv.AddressId= had.AddressId
                             where (fv.VendorStatus = 1) and hv.VendorType = 2 and hv.IsActive=1";
                    if (BrandIdvalue == 1)
                    {
                        query = query + " and hv.BudgetBlind = 1";
                    }
                    else if (BrandIdvalue == 2)
                    {
                        query = query + " and hv.TailoredLiving = 1";
                    }
                    else
                    {
                        query = query + " and hv.ConcreteCraft = 1";
                    }

                    result = connection.Query<VendorListModel>(query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                    break;

                case 3:
                    query = @" SELECT hv.VendorIdPk,hv.VendorId,hv.Name,hv.VendorType as VendorTypeId,hvt.VendorType,
                               case when fv.FranchiseId is not null then fv.AccountRep else hv.AccountRep end as AccountRep,
                               case when fv.FranchiseId is not null then fv.AccountRepPhone else hv.AccountRepPhone end as AccountRepPhone,
                               case when fv.FranchiseId is not null then fad.Address1 + ', ' + fad.City + ', ' + fad.State + ' ' + 
                               fad.CountryCode2Digits + ' ' + fad.ZipCode else had.Address1 + ', ' + had.City + ', ' + had.State + ' ' + 
                               had.CountryCode2Digits + ' ' + had.ZipCode end as Location,
                               case when fv.FranchiseId is not null then vs.VendorStatus else case when hv.IsActive = 1 then 'Active' 
                               else 'InActive' end end VendorStatus FROM[Acct].[HFCVendors] hv
                               left join Acct.FranchiseVendors fv ON hv.VendorId = fv.VendorId and fv.FranchiseId=@FranchiseId
                               left join CRM.Type_VendorStatus vs on fv.VendorStatus= vs.Id
                               left join [CRM].[Type_VendorType] hvt on hv.VendorType= hvt.Id
                               left join [CRM].[Addresses] fad on fv.AddressId= fad.AddressId
                               left join [CRM].[Addresses] had on hv.AddressId= had.AddressId
                               where (fv.VendorStatus = 1) and hv.VendorType = 4 order by VendorTypeId asc";
                    result = connection.Query<VendorListModel>(query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                    break;

                default:
                    query = @"SELECT VendorIdPk,VendorId,Name,fv.VendorType as VendorTypeId,vt.VendorType,AccountRep,AccountRepPhone,fv.OrderingMethod,
                            Address1 + ', ' + City + ', ' + State + ' ' + CountryCode2Digits + ' ' + ZipCode as Location,vs.VendorStatus from Acct.FranchiseVendors fv
                            left join CRM.Type_VendorStatus vs on fv.VendorStatus = vs.Id left join[CRM].[Type_VendorType] vt on fv.VendorType=vt.Id
                            left join[CRM].[Addresses] ad on fv.AddressId=ad.AddressId where fv.FranchiseId=@FranchiseId and fv.VendorType=3 and fv.VendorStatus=1 order by VendorTypeId asc";
                    result = connection.Query<VendorListModel>(query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                    break;
                    //case 4:
                    //    result = GetVendors();
                    //    break; 
            }

            foreach (var item in result)
            {
                if (id == 1)
                {
                    if (item.IsAlliance == true)
                    {
                        item.OrderingMethodName = "PIC";
                    }
                    else if (item.IsAlliance == false && item.OrderingMethod != null)
                    {
                        var OrderingId = item.OrderingMethod;
                        switch (OrderingId)
                        {
                            case "1": item.OrderingMethodName = "EMail"; break;
                            case "2": item.OrderingMethodName = "Phone"; break;
                            case "3": item.OrderingMethodName = "Fax"; break;
                            case "4": item.OrderingMethodName = "Vendor Portal"; break;
                            case "5": item.OrderingMethodName = "Will Call"; break;
                                //case "5": item.OrderingMethodName = "PIC"; break;
                        }
                    }
                }
                else
                {
                    if (item.OrderingMethod != null || item.OrderingMethod != "")
                    {
                        var OrderingId = item.OrderingMethod;
                        switch (OrderingId)
                        {
                            case "1": item.OrderingMethodName = "EMail"; break;
                            case "2": item.OrderingMethodName = "Phone"; break;
                            case "3": item.OrderingMethodName = "Fax"; break;
                            case "4": item.OrderingMethodName = "Vendor Portal"; break;
                            case "5": item.OrderingMethodName = "Will Call"; break;
                                //case "5": item.OrderingMethodName = "PIC"; break;
                        }
                    }
                }
            }


            //foreach (var item in result)
            //{
            //    if (item.AddressId != null)
            //    {
            //        var addresset = connection.Query<Address>("select * from CRM.Addresses where AddressId in @Ids",
            //                        new { Ids = result.Select(a => a.AddressId).Distinct() });

            //        var address = addresset.Where(x => x.AddressId == item.AddressId).FirstOrDefault();
            //        //item.VendorAddress = address;
            //        item.AddressCityZip = address.Address1 + "," + address.Address2 + "," + address.City + "," + address.State + "," + address.ZipCode;
            //    }
            //    else
            //    {
            //        item.AddressCityZip = " ";
            //    }
            //}
            //result = result.Where(x => x.VendorStatus == "Active").ToList();
            return result.OrderBy(x => x.Name).ToList();

        }
        // return null;

    }
    public List<VendorProductListModel> GetPrdtValue(int id)
    {

        var query = "";
        //query = @"select fp.ProductKey,fp.ProductName,fp.ProductID,fp.Description,
        //         pc.ProductCategory from CRM.HFCProducts fp
        //         left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
        //         where fp.VendorID=@Id";
        query = @"select ProductKey,ProductID,ProductName,VendorId,VendorName,ProductCategory,
                 ProductSubCategory,Description,FranchiseId,ProductStatus from 
                 (select hp.ProductKey,ProductID,ProductName,VendorId ,
                 case when exists(select VendorId from [Acct].[HFCVendors] where VendorId=hp.VendorID) 
                 then (select Name from [Acct].[HFCVendors] where VendorId=hp.VendorID)
                 else case when exists(select VendorId from [Acct].[FranchiseVendors]) 
                 then (select Name from [Acct].[FranchiseVendors] where VendorId=hp.VendorID) end end as VendorName,
                 VendorProductSKU,pc.ProductCategory,'' as ProductSubCategory,Description,0 as FranchiseId,
                 isnull(hps.ProductStatusId,1) as ProductStatusId from[CRM].[HFCProducts] hp
                 left join [CRM].[Type_ProductCategory] pc on hp.ProductCategory=pc.ProductCategoryId
                 left join [CRM].[HFCProductStatus] hps on hp.ProductKey=hps.ProductKey 
                 and hps.FranchiseId = @FranchiseId) as a 
                 left join [CRM].[Type_ProductStatus] ps on a.ProductStatusId=ps.ProductStatusId where VendorID=@Id 
                 union
                 select fp.ProductKey,fp.ProductID,fp.ProductName,fp.VendorID,fp.VendorName,
                 pc.ProductCategory,fp.ProductSubCategory,fp.Description,fp.FranchiseId,ps.ProductStatus
                 from CRM.FranchiseProducts fp 
                 left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
                 left join [CRM].[Type_ProductStatus] ps on ps.ProductStatusId=fp.ProductStatus
                 where fp.VendorID=@Id and fp.FranchiseId=@FranchiseId Order by ProductStatus,ProductName";
        var result = ExecuteIEnumerableObject<VendorProductListModel>(ContextFactory.CrmConnectionString, query, new
        {
            Id = id,
            FranchiseId = SessionManager.CurrentUser.FranchiseId
        }).ToList();
        if (result != null)
        {
            //result = result.OrderBy(x => x.ProductName).ToList();
            return result;
        }
        else
        {
            query = @"select fp.ProductKey,fp.ProductName,fp.ProductID,fp.Description,fp.FranchiseId,
                      pc.ProductCategory from CRM.FranchiseProducts fp 
                      left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
                      where fp.VendorID=@Id and fp.FranchiseId=@FranchiseId";
            var results = ExecuteIEnumerableObject<VendorProductListModel>(ContextFactory.CrmConnectionString, query, new
            {
                Id = id,
                FranchiseId = SessionManager.CurrentUser.FranchiseId
            }).ToList();
            if (results != null)
            {
                result = result.OrderBy(x => x.ProductName).ToList();
                return results;
            }
        }

        return null;
    }

    //to get list of alliance vendor for admin
    public List<AdminAllianceModel> GetAlliance(int Id)
    {
        List<AdminAllianceModel> result = new List<AdminAllianceModel>();
        using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
        {
            var Query = "";
            if (Id == 0)
            {
                //Query = @"select * from [Acct].[HFCVendors] where VendorType =1";
                Query = @"select hv.*, case when vs.VendorStatus is null then 'Inactive' 
                      else vs.VendorStatus end as StatusValue from [Acct].[HFCVendors] hv
                      left join CRM.Type_VendorStatus vs on hv.IsActive= vs.Id
                      where VendorType =1";
                result = connection.Query<AdminAllianceModel>(Query).ToList();
            }
            else
            {
                //Query = @"select * from [Acct].[HFCVendors] where VendorType =1";
                Query = @"select hv.*, case when vs.VendorStatus is null then 'Inactive' 
                        else vs.VendorStatus end as StatusValue from [Acct].[HFCVendors] hv
                        left join CRM.Type_VendorStatus vs on hv.IsActive= vs.Id
                        where VendorType =2";
                result = connection.Query<AdminAllianceModel>(Query).ToList();

            }

        }
        foreach (var item in result)
        {
            if (item.IsActive == true)
            {
                item.Status = 1;
            }
            else
            {
                item.Status = 2;
            }
            if (item.VendorType == 1)
                item.VendorTypeName = "Alliance Vendor";
            else
                item.VendorTypeName = "Non-Alliance Vendor";

            if (item.BudgetBlind == null)
                item.BudgetBlind = false;

            if (item.TailoredLiving == null)
                item.TailoredLiving = false;

            if (item.ConcreteCraft == null)
                item.ConcreteCraft = false;

            if (item.IsAlliance == null)
                item.IsAlliance = false;

            if (item.Beta == null)
                item.Beta = false;

        }
        result = result.OrderBy(x => x.Name).ToList();
        return result;
    }
    //when dropdown value changes to fetch value to grid
    public List<AdminAllianceModel> GetSelectedVendor(int id)
    {
        List<AdminAllianceModel> Value = new List<AdminAllianceModel>();
        using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
        {
            if (id == 1)
            {
                var Query = "";
                //Query = @"select * from [Acct].[HFCVendors] where VendorType =2";
                Query = @"select hv.*, case when vs.VendorStatus is null then 'Inactive' 
                        else vs.VendorStatus end as StatusValue from [Acct].[HFCVendors] hv
                        left join CRM.Type_VendorStatus vs on hv.IsActive= vs.Id
                        where VendorType =2";

                Value = connection.Query<AdminAllianceModel>(Query).ToList();
            }
            else
            {
                var Query = "";
                //Query = @"select * from [Acct].[HFCVendors] where VendorType =1";
                Query = @"select hv.*, case when vs.VendorStatus is null then 'Inactive' 
                      else vs.VendorStatus end as StatusValue from [Acct].[HFCVendors] hv
                      left join CRM.Type_VendorStatus vs on hv.IsActive= vs.Id
                      where VendorType =1";
                Value = connection.Query<AdminAllianceModel>(Query).ToList();
            }
            foreach (var valueset in Value)
            {
                if (valueset.IsActive == true)
                {
                    valueset.Status = 1;
                }
                else
                {
                    valueset.Status = 2;
                }
                if (valueset.VendorType == 1)
                    valueset.VendorTypeName = "Alliance Vendor";
                else
                    valueset.VendorTypeName = "Non-Alliance Vendor";

                if (valueset.BudgetBlind == null)
                    valueset.BudgetBlind = false;

                if (valueset.TailoredLiving == null)
                    valueset.TailoredLiving = false;

                if (valueset.ConcreteCraft == null)
                    valueset.ConcreteCraft = false;

                if (valueset.IsAlliance == null)
                    valueset.IsAlliance = false;

                if (valueset.Beta == null)
                    valueset.Beta = false;
            }
            Value = Value.OrderBy(x => x.Name).ToList();
            return Value;
        }
    }
    //to get status drop-down value
    public List<AdminAllianceStatusModel> GetStatusDropdown()
    {
        List<AdminAllianceStatusModel> dropvalue = new List<AdminAllianceStatusModel>();
        using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
        {
            var Query = "";
            Query = @"select Id,VendorStatus from [CRM].[Type_VendorStatus] where Id!=3 and IsActive=1";
            dropvalue = connection.Query<AdminAllianceStatusModel>(Query).ToList();

        }
        return dropvalue;
    }
    //save admin alliance vendor
    public string SaveAllianceVendor(int id, AdminAllianceModel data)
    {

        using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
        {
            if (data.VendorIdPk != 0)
            {
                var status = CheckVendorValue(id, data);
                if (status == "true" || status == "Invalid PIC Id" ||
                    status == "Pic Id Alredy Exists")
                {
                    if (status != "true")
                    {
                        return status;
                    }
                    else
                        return DuplicateFileName;
                }
                else
                {
                    if (data.Status == 1)
                    {
                        data.IsActive = true;
                    }
                    else if (data.Status == 2)
                    {
                        data.IsActive = false;
                    }
                    var Statuses = UpdateAllianceVendor(data);
                    return Statuses;
                }

            }
            else
            {
                if (data.VendorType == 0 && id == 0)
                {
                    data.VendorType = 1;
                }
                else if (data.VendorType == 0 && id == 1)
                {
                    data.VendorType = 2;
                }
                var status = CheckVendorValue(id, data);
                if (status == "true" || status == "Invalid PIC Id" ||
                    status == "Pic Id Alredy Exists")
                {
                    if (status != "true")
                    {
                        return status;
                    }
                    else
                        return DuplicateFileName;
                }
                else
                {
                    var dto = connection.Query(
                   "[CRM].[spVendor_New]").ToList();

                    var vendorNumber = dto[0].VendorNumber;
                    data.VendorId = vendorNumber;
                    if (data.Status == 1)
                    {
                        data.IsActive = true;
                    }
                    else if (data.Status == 2)
                    {
                        data.IsActive = false;
                    }

                    var VendorIdPk = connection.Insert(data);
                    if (VendorIdPk != null)
                        return Success;
                }

            }
        }
        return null;
    }
    //update admin alliance vendor
    public string UpdateAllianceVendor(AdminAllianceModel updatevalue)
    {

        if (updatevalue.VendorIdPk != 0)
        {
            Update<AdminAllianceModel>(ContextFactory.CrmConnectionString, updatevalue);
            UpdateAllianceCurrency(updatevalue);
            return Success;
        }

        return null;
    }
    /// <summary>
    /// Based on Vendor currency grids update Franchise vendor grids
    /// </summary>
    /// <param name="vendor"></param>
    /// <returns></returns>
    public bool UpdateAllianceCurrency(AdminAllianceModel vendor)
    {
        if (vendor.VendorIdPk != 0 && vendor.VendorType == 1 && vendor.IsAlliance.HasValue && vendor.IsAlliance.Value && vendor.IsActive.HasValue && vendor.IsActive.Value)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var procedure = @"exec [CRM].[UpdateVendorAllianceCurrency] @VendorId";
                var results = connection.Query(procedure, new { VendorId = vendor.VendorId }).FirstOrDefault();

                return true;
            }

        }

        return false;
    }

    //check value/name exists in table 
    public string CheckVendorValue(int Id, AdminAllianceModel ChkValue)
    {
        List<AdminAllianceModel> ListValue = new List<AdminAllianceModel>();
        using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
        {
            if (ChkValue.VendorIdPk != 0)//check if primary id is not 0
            {
                if (ChkValue.VendorType == 1 && ChkValue.IsAlliance == false)//alliance vendor
                {
                    var VendorStatus = Vendorexists(ChkValue);
                    return VendorStatus;

                }
                else if (ChkValue.VendorType == 1 && ChkValue.IsAlliance == true && ChkValue.PICVendorId != "")//has pic id and value
                {
                    var Query = "";
                    Query = @"select * from [Acct].[HFCVendors] where PICVendorId=@PICVendorId and VendorId!=@VendorId";
                    ListValue = ExecuteIEnumerableObject<AdminAllianceModel>(ContextFactory.CrmConnectionString, Query, new
                    {
                        //Name = ChkValue.Name,
                        VendorId = ChkValue.VendorId,
                        PICVendorId = ChkValue.PICVendorId
                    }).ToList();
                    if (ListValue != null && ListValue.Count != 0)
                    {
                        return "Pic Id Alredy Exists";
                    }
                    else
                    {
                        var VendorStatus = Vendorexists(ChkValue);
                        if (VendorStatus == "true")
                        {
                            return VendorStatus;
                        }
                        else
                        {
                            var Statuscheck = CheckPickValue(ChkValue);
                            return Statuscheck;
                        }

                    }
                }
                else if (ChkValue.VendorType == 2)//non-alliance vendor
                {
                    var VendorStatus = Vendorexists(ChkValue);
                    return VendorStatus;
                }
            }
            else
            {
                //non-pic-enable alliance vendor & pic-enable
                //if(ChkValue.VendorType==0 && Id == 0)
                if (Id == 0)
                {//non-pic-enable alliance vendor
                    if (ChkValue.PICVendorId == "" && ChkValue.IsAlliance == false)
                    {
                        var NameStatus = NameCheck(ChkValue);
                        return NameStatus;
                    }
                    // pic-enable vendor
                    else if (ChkValue.PICVendorId != "" && ChkValue.IsAlliance == true)
                    {
                        var Querys = "";
                        Querys = @"select * from [Acct].[HFCVendors] where PICVendorId=@PICVendorId";
                        var PicListValue = ExecuteIEnumerableObject<AdminAllianceModel>(ContextFactory.CrmConnectionString, Querys, new
                        {
                            //Name = ChkValue.Name,
                            PICVendorId = ChkValue.PICVendorId

                        }).ToList();
                        if (PicListValue != null && PicListValue.Count != 0)
                        {
                            return "Pic Id Alredy Exists";
                        }
                        else
                        {
                            var Statuscheck = CheckPickValue(ChkValue);
                            return Statuscheck;
                        }

                    }
                    //has pic-id but not pic enable
                    else if (ChkValue.PICVendorId != "" && ChkValue.IsAlliance == false)
                    {
                        var NameStatus = NameCheck(ChkValue);
                        return NameStatus;

                    }

                }
                else //for non-alliance vendor ChkValue.VendorType==0 && if (Id == 1)
                {
                    var NameStatus = NameCheck(ChkValue);
                    return NameStatus;
                }
            }
        }
        return "false";
    }

    //check pic-id value with pic call
    public string CheckPickValue(AdminAllianceModel ChkValue)
    {
        var PicStatusValue = PicManger.GetVendorInfo(ChkValue.PICVendorId);
        if (PicStatusValue != null && PicStatusValue.valid == true &&
            PicStatusValue.properties.Vendor.name != "" &&
            PicStatusValue.properties.Vendor.vendor != "")
        {
            var Query = "";
            if (ChkValue.VendorIdPk != 0)
            {
                Query = @"select * from [Acct].[HFCVendors] where Name=@Name and PICVendorId=@PICVendorId and VendorIdPk!=VendorIdPk";
            }
            else
            {
                Query = @"select * from [Acct].[HFCVendors] where Name=@Name and PICVendorId=@PICVendorId";
            }

            var ListValue = ExecuteIEnumerableObject<AdminAllianceModel>(ContextFactory.CrmConnectionString, Query, new
            {
                Name = ChkValue.Name,
                PICVendorId = ChkValue.PICVendorId,
                VendorIdPk = ChkValue.VendorIdPk

            }).ToList();
            if (ListValue != null && ListValue.Count != 0)
            {
                return "true";
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "Invalid PIC Id";
            //DisplayMessage();
        }

        throw new NotImplementedException();

    }
    //check duplicate name with table if it has primary key value
    public string Vendorexists(AdminAllianceModel ChkValue)
    {
        var Query = "";
        Query = @"select * from [Acct].[HFCVendors] where Name=@Name and VendorId!=@VendorId";
        var ListValue = ExecuteIEnumerableObject<AdminAllianceModel>(ContextFactory.CrmConnectionString, Query, new
        {
            Name = ChkValue.Name,
            VendorId = ChkValue.VendorId,
            //PICVendorId = ChkValue.PICVendorId
        }).ToList();
        if (ListValue != null && ListValue.Count != 0)
        {
            return "true";
        }
        else
        {
            return "false";
        }
    }

    // if pk-id is 0
    public string NameCheck(AdminAllianceModel ChkValue)
    {
        var Query = "";
        Query = @"select * from [Acct].[HFCVendors] where Name=@Name";
        var ListValue = ExecuteIEnumerableObject<AdminAllianceModel>(ContextFactory.CrmConnectionString, Query, new
        {
            Name = ChkValue.Name,

        }).ToList();
        if (ListValue != null && ListValue.Count != 0)
        {
            return "true";
        }
        else
        {
            return "false";
        }
    }

    public override string Add(HFCVendor data)
    {
        throw new NotImplementedException();
    }

    public override string Update(HFCVendor data)
    {
        throw new NotImplementedException();
    }

    public override string Delete(int id)
    {
        throw new NotImplementedException();
    }

    public override ICollection<HFCVendor> Get(List<int> idList)
    {
        throw new NotImplementedException();
    }

    public List<VendorPricingDropDown> GetVendorPricingDropDown()
    {
        List<VendorPricingDropDown> ddl = new List<VendorPricingDropDown>();
        using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
        {
            connection.Open();

            var query = @"SELECT hv.VendorId,hv.Name as VendorName,'Retail' as RetailBasis FROM [Acct].[HFCVendors] hv 
                        JOIN [Acct].[FranchiseVendors] fv ON hv.VendorId = fv.VendorId and  fv.FranchiseId=@FranchiseId and fv.VendorStatus =1
                        where hv.VendorType = 1 and hv.IsActive = 1
                        union
                        SELECT hv.VendorId,hv.Name as VendorName,'Cost' as RetailBasis FROM [Acct].[HFCVendors] hv 
                        join Acct.FranchiseVendors fv  ON hv.VendorId = fv.VendorId and  fv.FranchiseId=@FranchiseId and fv.VendorStatus =1
                        where hv.VendorType = 2 and hv.IsActive = 1
                        union
                        SELECT VendorId,Name as VendorName,'Cost' as RetailBasis from Acct.FranchiseVendors fv 
                        where fv.FranchiseId=@FranchiseId and fv.VendorType=3 and fv.VendorStatus =1
                        order by VendorName";

            ddl = connection.Query<VendorPricingDropDown>(query,
                new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();

            connection.Close();
        }
        return ddl;
    }

    public List<VendorPricingDropDown> FilterVendorPricingDropDown(int ProductCategory)
    {
        List<VendorPricingDropDown> ddl = new List<VendorPricingDropDown>();
        using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
        {
            connection.Open();

            var query = @"Begin
                        Declare @ProductCategory int,@FranchiseId int
                        set @ProductCategory=@ProductCategoryID
                        set @FranchiseId=@FeID
                        if(@ProductCategory>=1000)
                        begin
                        select distinct hv.VendorId,hv.Name as VendorName,'Retail' as RetailBasis from [CRM].HFCProducts p
                        join Acct.FranchiseVendors fv on p.VendorID=fv.VendorId and fv.VendorStatus=1
                        join Acct.HFCVendors hv on fv.VendorId=hv.VendorId and hv.IsActive=1
                        where p.ProductGroup=@ProductCategory and fv.FranchiseId=@FranchiseId
                        AND p.ProductKey NOT IN ( SELECT tps.ProductKey FROM crm.HFCProductStatus tps WHERE tps.FranchiseId =@FranchiseId AND tps.ProductStatusId IN (2,3))
                        order by VendorName asc
                        end
                        else
                        begin
                        select Distinct hv.VendorId,hv.Name as VendorName,'Retail' as RetailBasis from CRM.FranchiseProducts fp 
                        join CRM.[Type_ProductCategory] pc on pc.ProductCategoryId=fp.ProductCategory and pc.IsActive = 1 
                        join Acct.FranchiseVendors fv on fp.VendorID=fv.VendorId and fv.VendorStatus=1 and fv.VendorType=1
                        join Acct.HFCVendors hv on fv.VendorId=hv.VendorId and hv.IsActive=1
                        where  fp.ProductStatus=1 and fp.ProductType=2 and pc.ProductCategoryId=@ProductCategory and fp.FranchiseId=@FranchiseId and fv.FranchiseId=@FranchiseId
                        union
                        select Distinct hv.VendorId,hv.Name as VendorName,'Cost' as RetailBasis from CRM.FranchiseProducts fp 
                        join CRM.[Type_ProductCategory] pc on pc.ProductCategoryId=fp.ProductCategory and pc.IsActive = 1 
                        join Acct.FranchiseVendors fv on fp.VendorID=fv.VendorId and fv.VendorStatus=1 and fv.VendorType=2
                        join Acct.HFCVendors hv on fv.VendorId=hv.VendorId and hv.IsActive=1
                        where  fp.ProductStatus=1 and fp.ProductType=2 and pc.ProductCategoryId=@ProductCategory and fp.FranchiseId=@FranchiseId and fv.FranchiseId=@FranchiseId
                        union
                        select Distinct fv.VendorId,fv.Name as VendorName,'Cost' as RetailBasis from CRM.FranchiseProducts fp 
                        join CRM.[Type_ProductCategory] pc on pc.ProductCategoryId=fp.ProductCategory and pc.IsActive = 1 
                        join Acct.FranchiseVendors fv on fp.VendorID=fv.VendorId and fv.VendorStatus=1 and fv.VendorType=3
                        where  fp.ProductStatus=1 and fp.ProductType=2 and pc.ProductCategoryId=@ProductCategory and fp.FranchiseId=@FranchiseId and fv.FranchiseId=@FranchiseId
                        order by VendorName asc
                        end
                        End";

            ddl = connection.Query<VendorPricingDropDown>(query,
                new { FeID = SessionManager.CurrentFranchise.FranchiseId, ProductCategoryID = ProductCategory }).ToList();

            connection.Close();
        }
        return ddl;
    }

    public ICollection<VendorCaseConfig> GetVendorCaseConfigList()
    {
        try
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = @"select vc.Id,hv.VendorIdPk as VendorId, hv.Name as VendorName,vc.CaseEnabled,vc.CaseLogin,vc.VendorSupportEmail from [CRM].[VendorCaseConfig] vc
                          right join [Acct].[HFCVendors] hv
                          on vc.VendorId = hv.VendorIdPk
                          where IsActive =1 order by vc.CreatedOn Desc";
                var vendorCaseList = connection.Query<VendorCaseConfig>(query).ToList();
                return vendorCaseList;
            }
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    public string UpdateVendorCaseConfig(VendorCaseConfig model)
    {
        try
        {
            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var userObj = con.Query<User>("select * from Auth.Users where UserName=@UserName", new { UserName = model.CaseLogin }).FirstOrDefault();
                if (userObj != null)
                    model.PersonId = userObj.PersonId;

                if (model.Id == 0)
                {
                    model.CreatedBy = User.PersonId;
                    var result = con.Insert(model);
                }
                else
                {
                    var vendorcase = con.Query<VendorCaseConfig>("select * from CRM.VendorCaseConfig where Id=@Id", new { Id = model.Id }).FirstOrDefault();
                    vendorcase.CaseEnabled = model.CaseEnabled;
                    vendorcase.CaseLogin = model.CaseLogin;
                    vendorcase.VendorSupportEmail = model.VendorSupportEmail;
                    vendorcase.PersonId = model.PersonId;
                    model.LastUpdatedOn = DateTime.UtcNow;
                    model.LastUpdatedBy = User.PersonId;
                    var result = con.Update(vendorcase);
                }

                return Success;
            }
        }
        catch (Exception ex)
        {
            throw;
        }


    }

    public bool CheckUserBelongsToFe(string username)
    {
        try
        {
            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var userObj = con.Query<User>("select * from Auth.Users where UserName like @UserName", new { UserName = username }).FirstOrDefault();
                if (userObj == null)
                    return false;
                else if (userObj.FranchiseId == null || userObj.FranchiseId < 1)
                    return false;
                else
                    return true;
            }
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    public bool CheckUserBelongsToVendor(string username, int VendorId)
    {
        try
        {
            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var userObj = con.Query<User>(@"select uir.* from auth.UsersInRoles  uir
                                                join Auth.Users u on u.UserId = uir.UserId
                                                join Auth.Roles r on r.RoleId = uir.RoleId
                                                left join CRM.VendorCaseConfig v on u.PersonId=v.PersonId
                                                where u.UserName like @UserName and r.RoleName like @RoleName
                                                and v.VendorId!=@VendorId",
                                                new { UserName = username, RoleName = "Vendor", VendorId }).FirstOrDefault();
                if (userObj == null) return false; else return true;
            }
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    public bool CheckUserExist(string username)
    {
        try
        {
            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var userObj = con.Query<User>("select * from Auth.Users where UserName=@UserName", new { UserName = username }).FirstOrDefault();
                if (userObj == null)
                    return false;
                else
                    return true;
            }
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    public string GetADDetails(string userName)
    {
        string primaryEmail = "";
        string domain = null;

        if (string.IsNullOrEmpty(domain))
            domain = ExchangeManager.Domain;
        using (var context = new PrincipalContext(ContextType.Domain, domain, "synctest1", "Budget123"))
        {
            using (var searcher = new PrincipalSearcher(new UserPrincipal(context)))
            {
                try
                {
                    searcher.QueryFilter.SamAccountName = userName;
                    var result = searcher.FindOne();
                    if (result != null)
                    {
                        DirectoryEntry de = result.GetUnderlyingObject() as DirectoryEntry;
                        if (de.Properties["mail"] != null && de.Properties["mail"].Value != null)
                            primaryEmail = de.Properties["mail"].Value.ToString();
                    }
                }
                catch (Exception ex) { ApiEventLogger.LogEvent(ex, userName); }
            }
        }
        return primaryEmail;
    }

    public int GetCurrencyTypeVal(int id)
    {
        Franchise FeObj = new Franchise();
        AdminAllianceModel AdminVendor = new AdminAllianceModel();
        var CurrencyValue = "";
        try
        {
            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                FeObj = con.Query<Franchise>(@"select * from CRM.Franchise where FranchiseId=@franchiseid",
                                               new { franchiseid = Franchise.FranchiseId }).FirstOrDefault();
                AdminVendor = con.Query<AdminAllianceModel>(@"select * from Acct.HFCVendors where VendorId=@Vendorid",
                                                new { Vendorid = id }).FirstOrDefault();
                if (FeObj != null && AdminVendor != null)
                {
                    if (AdminVendor.CANCurrency == true && AdminVendor.USCurrency == true)
                    {
                        CurrencyValue = FeObj.Currency;
                        if (CurrencyValue == "US")
                        {
                            return 223;
                        }
                        else return 39;
                    }
                    else if (AdminVendor.CANCurrency == false && AdminVendor.USCurrency == false)
                    {
                        CurrencyValue = FeObj.Currency;
                        if (CurrencyValue == "US")
                        {
                            return 223;
                        }
                        else return 39;
                    }
                    else
                    {
                        if (AdminVendor.USCurrency == true)
                        {
                            return 223;
                        }
                        else { return 39; }
                    }
                }
                return 0;
            }
        }
        catch (Exception ex)
        {
            throw;
        }
        //return null;
    }

    public List<VendorTerritoryAccount> GetRelatedAccount(int id)
    {
        try
        {
            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var CurrentUserId = User.PersonId;
                //var query1 = @"select * from CRM.VendorCaseConfig where PersonId =@PersonId";
                var query1 = @"select hf.VendorId as VendorKey,vc.* from CRM.VendorCaseConfig  vc
                               left join Acct.HFCVendors hf on vc.VendorId = hf.VendorIdPk
                               where vc.PersonId =@PersonId";
                var query2 = @"select distinct vta.FranchiseId,vta.AccountNumber,vta.VendorId,f.Code as FeCode
                               from CRM.VendorTerritoryAccount vta 
                               join Acct.FranchiseVendors fv on vta.VendorId = fv.VendorId and fv.FranchiseId=vta.FranchiseId
                               join CRM.Franchise f on f.FranchiseId=vta.FranchiseId
                               where vta.VendorId=@Vendorid and fv.VendorStatus=1
                               order by vta.AccountNumber asc";
                var result1 = con.Query<VendorCaseConfig>(query1, new { PersonId = CurrentUserId }).FirstOrDefault();
                var result2 = con.Query<VendorTerritoryAccount>(query2, new { Vendorid = result1.VendorKey }).ToList();
                if (result2 != null && result2.Count != 0)
                {
                    return result2;
                }
                else
                    return null;
            }
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    public Nullable<int> spVendor_New()
    {

        using (var conn = new SqlConnection(ContextFactory.CrmConnectionString))
        {

            conn.Open();
            var dto = conn.Query(
                "[CRM].[spVendor_New]").ToList();

            var vendorNumber = dto[0].VendorNumber;
            //return 1;
            return vendorNumber;
        }
    }
}

