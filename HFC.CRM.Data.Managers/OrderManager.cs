﻿using Dapper;
using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using HFC.CRM.DTO.Order;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using HFC.CRM.Core.Logs;
using Newtonsoft.Json;
using HFC.CRM.DTO;

namespace HFC.CRM.Managers
{
    public class OrderManager : DataManager<Orders>
    {
        private EmailManager emailMgr;
        private MeasurementManager MeasurementMgr;
        private QuotesManager QuotesMgr;
        private AccountsManager accmgr;

        public OrderManager(User user, Franchise franchise)
        {
            this.User = user;
            this.Franchise = franchise;
            emailMgr = new EmailManager(user, franchise);
            MeasurementMgr = new MeasurementManager(user, franchise);
            QuotesMgr = new QuotesManager(user, franchise);
            accmgr = new AccountsManager(user, franchise);
        }

        public int CreateInvoice(Orders data, bool createInvoice = false, SqlConnection connection = null, SqlTransaction transaction = null)
        {
            //Add/update invoice table
            //Add invoice history table
            //Add filestable

            InvoicesTP invoice = new InvoicesTP();
            invoice.InvoiceDate = DateTime.UtcNow;
            invoice.InvoiceType = "Deposit Invoice";

            bool flagSQLCon = false;

            if (connection == null)
            {
                connection = new SqlConnection(ContextFactory.CrmConnectionString);
                flagSQLCon = true;
            }

            try
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();

                var usr = User.PersonId;

                //data.Streamid = 1;
                if (data != null)
                {
                    invoice.OrderID = data.OrderID;
                    int OrderId = data.OrderID;

                    var query = @"Select * from CRM.Payments where OrderId=@OrderId order by Paymentid desc ";
                    var payment = connection.Query<Payments>(query, new { OrderId = OrderId }, transaction).ToList();

                    if (payment != null)
                    {
                        if (payment.Count() > 1)
                        {
                            invoice.InvoiceType = "Invoice";//Payment made no due

                            invoice.Balance = payment.FirstOrDefault().BalanceDue;
                            invoice.TotalAmount = payment.FirstOrDefault().Total;
                        }
                        else
                        {
                            invoice.Balance = data.BalanceDue;
                            invoice.TotalAmount = data.OrderTotal;
                        }
                    }

                    query = @"select Top 1* from CRM.Invoices  where OrderId=@OrderId order by InvoiceId desc ";
                    var Invoices = connection.Query<InvoicesTP>(query, new { OrderId = OrderId }, transaction).ToList();
                    if (Invoices != null && Invoices.Count > 0)
                    {
                        InvoicesTP Invoice = Invoices.FirstOrDefault();
                        invoice.InvoiceId = Invoice.InvoiceId;
                        invoice.CreatedOn = Invoice.CreatedOn;
                        invoice.CreatedBy = Invoice.CreatedBy;
                        invoice.LastUpdatedBy = usr;
                        invoice.LastUpdatedOn = DateTime.UtcNow;
                        connection.Update<InvoicesTP>(invoice, transaction);
                    }
                    else
                    {
                        invoice.CreatedOn = DateTime.UtcNow;
                        invoice.CreatedBy = usr;
                        invoice.LastUpdatedBy = usr;
                        invoice.LastUpdatedOn = DateTime.UtcNow;
                        //InvoiceHistory.Streamid = fileId;
                        invoice.InvoiceId = connection.Insert<int, InvoicesTP>(invoice, transaction);
                    }

                    decimal? TotalAmount = data.OrderTotal;
                    decimal? Balance = data.BalanceDue;

                    var invoiceHistory = new InvoiceHistory
                    {
                        InvoiceId = invoice.InvoiceId,
                        OrderID = invoice.OrderID,
                        InvoiceType = invoice.InvoiceType,
                        InvoiceDate = invoice.InvoiceDate,
                        TotalAmount = TotalAmount,
                        Balance = Balance,
                        CreatedBy = usr,
                        LastUpdatedBy = usr,
                    };

                    if (createInvoice == false)
                    {
                        var invoicehistory = connection.Insert<int, InvoiceHistory>(invoiceHistory, transaction);
                    }
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex.Message);
                throw ex;
            }
            finally
            {
                if (flagSQLCon)
                    connection.Close();
            }


            return invoice.InvoiceId;

            //InvoicesTP invoiceresult = new InvoicesTP();
            //var query1 = @"select * from [CRM].[Invoices]
            //                where OrderID=@Id";
            //invoiceresult =   ExecuteIEnumerableObject<InvoicesTP>(ContextFactory.CrmConnectionString, query1, new
            //{
            //    Id = invoice.OrderID
            //}).FirstOrDefault();

            //int invoiceId = 0;
            //if (invoiceresult.InvoiceId>0)
            //{
            //    invoiceresult.Balance = invoice.Balance;
            //    invoiceresult.TotalAmount = invoice.TotalAmount;
            //    if (invoice.Balance == 0)
            //    {
            //        invoice.InvoiceType = "Invoice";//Payment made no due
            //    }
            //    //update  invoice
            //    Update<InvoicesTP>(ContextFactory.CrmConnectionString, invoiceresult);
            //    invoiceId = invoiceresult.InvoiceId;
            //}
            //else
            //{
            //    //Add invoice
            //    var result = Insert<int, InvoicesTP>(ContextFactory.CrmConnectionString, invoice);
            ////    invoiceId = result.InvoiceId;
            //}

            //return invoiceId;

            /*

            //Add Stream into FileTable
            Guid fileId = new Guid();// createInvoicePDF(invoiceresult.InvoiceId);

            //Add Invoice History table
            InvoiceHistory InvoiceHistory = new InvoiceHistory();
            InvoiceHistory.InvoiceId = invoiceresult.InvoiceId;
            InvoiceHistory.OrderID = invoiceresult.OrderID;
            InvoiceHistory.InvoiceType = invoiceresult.InvoiceType;
            InvoiceHistory.InvoiceDate = invoiceresult.InvoiceDate;
            InvoiceHistory.TotalAmount = invoiceresult.TotalAmount;
            InvoiceHistory.Balance = invoiceresult.Balance;
            InvoiceHistory.Streamid = fileId;
            var invoicehistory = Insert<int, InvoiceHistory>(ContextFactory.CrmConnectionString, InvoiceHistory);

            return "Success";
            */
        }

        public string CreateInvoice(int invoiceId, Guid fileId)
        {
            //Add Invoice History table
            InvoiceHistory InvoiceHistory = new InvoiceHistory();
            var query = @"select * from [CRM].[Invoices]
                            where invoiceId=@Id";
            var invoiceresult = ExecuteIEnumerableObject<InvoicesTP>(ContextFactory.CrmConnectionString, query, new
            {
                Id = invoiceId
            }).FirstOrDefault();

            query = @"select Top 1 * from [CRM].[InvoiceHistory]
                            where invoiceId=@Id order by Id desc";
            var InvoiceHistoryId = ExecuteIEnumerableObject<InvoiceHistory>(ContextFactory.CrmConnectionString, query, new
            {
                Id = invoiceId
            }).FirstOrDefault();

            InvoiceHistory.InvoiceId = invoiceId;
            InvoiceHistory.OrderID = invoiceresult.OrderID;
            InvoiceHistory.InvoiceType = invoiceresult.InvoiceType;
            InvoiceHistory.InvoiceDate = invoiceresult.InvoiceDate;
            InvoiceHistory.TotalAmount = invoiceresult.TotalAmount;
            InvoiceHistory.Balance = invoiceresult.Balance;
            InvoiceHistory.Streamid = Guid.Parse(fileId.ToString());
            InvoiceHistory.Id = InvoiceHistoryId.Id;
            InvoiceHistory.LastUpdatedBy = User.PersonId;
            InvoiceHistory.CreatedBy = invoiceresult.CreatedBy;
            InvoiceHistory.LastUpdatedOn = DateTime.Now;
            var invoicehistory = Update<InvoiceHistory>(ContextFactory.CrmConnectionString, InvoiceHistory);
            return "Success";
        }

        public Guid UploadFile(int InvoiceId, Byte[] file, string filename)
        {
            Guid fileId = new Guid();
            //url = null;
            //thmbUrl = null;
            Random rnd = new Random();
            string value = rnd.Next(1, 9999).ToString();
            string fileName = DateTime.UtcNow.ToString("MMddyyyy") + "_" + DateTime.UtcNow.ToString("HHMMssfff") + value + "_" + filename;

            Byte[] bytes = file;

            string Query = "INSERT INTO FileTableTb (name, file_stream) OUTPUT INSERTED.[stream_id] values(@FileName, @stream)";

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var dParams = new DynamicParameters();
                dParams.Add("@FileName", fileName, DbType.AnsiString);
                dParams.Add("@stream", bytes, DbType.Binary);

                var streamid = connection.ExecuteScalar(Query, dParams);

                // TODO: is this enough or do we need physicalfiletable guid???
                fileId = Guid.Parse(streamid.ToString());
            }

            return fileId;
        }

        /// <summary>
        /// The add order payment.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string AddOrderPayment(PaymentDTO data)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    var orderQuery = @"select * from [CRM].[Orders] where OrderID=@Id";
                    var orderResult = connection.Query<Orders>(orderQuery, new { Id = data.order.OrderID }, transaction).FirstOrDefault();

                    if (orderResult == null)
                        return "Fail";

                    var payAmountQ = @"select sum(case when Reversal=0 then isnull(Amount,0) else isnull(Amount,0)*-1 end) 
                                       from CRM.Payments where OrderID=@Id";
                    var paidAmount = connection.ExecuteScalar<decimal?>(payAmountQ, new { Id = data.order.OrderID }, transaction);

                    connection.Execute("update p set OverPaymentAmount=0 from CRM.Payments p where OrderID=@Id", new { Id = data.order.OrderID }, transaction);

                    if (paidAmount == null)
                        paidAmount = 0M;

                    // Calculate Balance Due = OrderTotal - (paidAmount + new paid amount (data.Amount))
                    paidAmount = paidAmount + data.Amount;
                    decimal? BalanceDue = orderResult.OrderTotal - paidAmount;

                    decimal OverPaymentAmount = 0m;
                    if (BalanceDue < 0)
                    {
                        OverPaymentAmount = (decimal)BalanceDue * -1;
                        //BalanceDue = 0; //-Commented since balance is needed in -ve value
                    }


                    var payment = new Payments
                    {
                        OpportunityID = data.order.OpportunityId,
                        PaymentID = data.PaymentID,
                        OrderID = data.order.OrderID,
                        Sidemark = data.Sidemark,
                        Total = orderResult.OrderTotal,
                        BalanceDue = BalanceDue,
                        PayBalance = data.PayBalance == null ? false : true,
                        PaymentMethod = data.PaymentMethod,
                        VerificationCheck = data.VerificationCheck,
                        PaymentDate = data.PaymentDate,
                        Amount = data.Amount,
                        Memo = data.Memo,
                        Reversal = false,
                        InvoiceId = null,
                        OverPaymentAmount = OverPaymentAmount
                    };

                    var result = connection.Insert<int, Payments>(payment, transaction);

                    orderResult.BalanceDue = BalanceDue;
                    orderResult.LastUpdatedOn = DateTime.UtcNow;
                    orderResult.LastUpdatedBy = User.PersonId;
                    var response = connection.Update(orderResult, transaction);

                    CreateInvoice(data: orderResult, connection: connection, transaction: transaction);

                    transaction.Commit();
                    return "Success";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public string UpdateReversePayment(PaymentDTO data)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    if (data == null)
                        return "Fail";

                    var orderQuery = @"select * from [CRM].[Orders] where OrderID=@Id";
                    var Orders = connection.Query<Orders>(orderQuery, new { Id = data.OrderID }, transaction).FirstOrDefault();

                    if (Orders == null)
                        return "Fail";

                    var payAmountQ = @"select sum(case when Reversal=0 then isnull(Amount,0) else isnull(Amount,0)*-1 end) 
                                       from CRM.Payments where OrderID=@Id";
                    var paidAmount = connection.ExecuteScalar<decimal?>(payAmountQ, new { Id = data.order.OrderID }, transaction);

                    connection.Execute("update p set OverPaymentAmount=0 from CRM.Payments p where OrderID=@Id", new { Id = data.order.OrderID }, transaction);

                    if (paidAmount == null)
                        paidAmount = 0M;

                    // Calculate Balance Due = OrderTotal - (paidAmount - new paid amount (data.Amount))
                    paidAmount = paidAmount - data.Amount;
                    decimal? BalanceDue = Orders.OrderTotal - paidAmount;

                    decimal OverPaymentAmount = 0m;
                    if (BalanceDue < 0)
                    {
                        OverPaymentAmount = (decimal)BalanceDue * -1;
                        //BalanceDue = 0; //-Commented since balance is needed in -ve value
                    }

                    var payment = new Payments();
                    payment.OpportunityID = data.OpportunityID;
                    payment.OrderID = data.OrderID;
                    payment.Total = Orders.OrderTotal;
                    payment.BalanceDue = BalanceDue;
                    payment.PayBalance = data.PayBalance;
                    payment.PaymentMethod = data.PaymentMethod;
                    payment.VerificationCheck = data.VerificationCheck;
                    payment.Amount = data.Amount;
                    payment.Memo = data.Memo;
                    payment.PaymentDate = data.PaymentDate;
                    payment.Reversal = true;
                    payment.ReasonCode = data.ReasonCode;
                    payment.OverPaymentAmount = OverPaymentAmount;
                    var datas = connection.Insert<int, Payments>(payment, transaction);

                    if (Orders != null)
                    {
                        if (Orders.OrderStatus == 6 || Orders.OrderStatus == 9)
                            Orders.BalanceDue = 0;
                        else
                            Orders.BalanceDue = BalanceDue;
                        Orders.LastUpdatedOn = DateTime.UtcNow;
                        Orders.LastUpdatedBy = User.PersonId;

                        var Ordersdata = connection.Update(Orders, transaction);
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return "Success";
        }

        public Orders updateContractedDate(int id, DateTime Date)
        {

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();

                    connection.Execute(@"Update CRM.Orders set
                            ContractedDateUpdatedOn = @ContractedDateUpdatedOn,
                            ContractedDateUpdatedBy=@ContractedDateUpdatedBy,
                            PreviousContractedDate=ContractedDate,
                            ContractedDate=@ContractedDate where OrderID=@OrderID ",
                            new
                            {
                                OrderID = id,
                                ContractedDateUpdatedOn = DateTime.UtcNow,
                                ContractedDateUpdatedBy = SessionManager.CurrentUser.PersonId,
                                ContractedDate = Date
                            });
                    var res = connection.Query<Orders>(@"select *,p.FirstName +' '+ p.LastName as ContractedDateUpdatedBy_Name  from CRM.Orders o
							left join CRm.Person P on o.ContractedDateUpdatedBy = p.PersonId where OrderId=@OrderId",
                        new { OrderId = id }).FirstOrDefault();
                    if (res != null)
                        res.ContractedDateUpdatedOn = TimeZoneManager.ToLocal(((DateTimeOffset)res.ContractedDateUpdatedOn).DateTime);

                    return res;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }

        //Status update to Installation appointment
        public string UpdateOrderStatus(int? OrderId = 0)
        {
            if (OrderId > 0)
            {
                //int orderId = OrderId;
                var query = @"Select * from CRM.orders where OrderId=@OrderId";
                var orderdata = ExecuteIEnumerableObject<Orders>(ContextFactory.CrmConnectionString, query, new
                {
                    orderId = OrderId
                }).FirstOrDefault();

                if (orderdata != null)
                {
                    //"Installation Scheduled" is set when Installation appointment has been scheduled
                    orderdata.OrderStatus = 4;
                    var datas = Update<Orders>(ContextFactory.CrmConnectionString, orderdata);
                }
            }
            return "Success";
        }

        public List<TaxDetails> GetTaxinfo(int quoteKey)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();

                    var taxDetailsList = connection.Query<TaxDetails>(@"SELECT td.Jurisdiction,td.JurisType,td.TaxName,td.Rate,SUM(td.Amount) AS Amount FROM [CRM].[tax] AS t INNER JOIN CRM.taxdetails td on t.taxid = td.taxid INNER JOIN crm.quotelines AS q ON t.quotelineId = q.quotelineId where q.quoteKey = @quoteKey
                    GROUP BY  td.Jurisdiction, td.JurisType,td.TaxName, td.Rate; ", new { quoteKey = quoteKey }).ToList();

                    return taxDetailsList;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public string OrderUpdate(OrderDTO data)
        {
            if (data != null)
            {
                int OrderId = data.OrderID;
                int? StatusID = data.OrderStatus;
                var query = @"Select * from CRM.Orders where OrderId=@OrderId";
                var orderdata = ExecuteIEnumerableObject<Orders>(ContextFactory.CrmConnectionString, query, new
                {
                    OrderId = OrderId
                }).FirstOrDefault();

                if (orderdata != null)
                {
                    orderdata.OrderStatus = StatusID;
                    orderdata.CancelReason = data.CancelReason;
                    //This is not needed any more as the tax exempt edit is moved to Opportunity level
                    //if (orderdata.IsTaxExempt != data.IsTaxExempt)
                    //{
                    //    if (data.IsTaxExempt)
                    //    {
                    //        DeleteTax(OrderId);
                    //        orderdata.OrderTotal = orderdata.OrderSubtotal;
                    //        orderdata.BalanceDue = orderdata.BalanceDue - orderdata.Tax;
                    //        orderdata.Tax = 0;
                    //        orderdata.TaxCalculated = false;
                    //    }

                    //    if (!data.IsTaxExempt)
                    //    {
                    //        // QuotesMgr.GetAndUpdateTax(orderdata.QuoteKey);

                    //        var response = QuotesMgr.GetAndUpdateTax(orderdata.QuoteKey);
                    //        dynamic responseObj = JsonConvert.DeserializeObject(response);

                    //        if (Convert.ToBoolean(responseObj.valid.Value))
                    //        {
                    //            QuotesMgr.DeleteTax(orderdata.QuoteKey);
                    //            QuotesMgr.AddorUpdateTax(orderdata.QuoteKey, responseObj);
                    //            var taxDetailsList = GetTaxinfo(orderdata.QuoteKey);
                    //            if (taxDetailsList != null && taxDetailsList.Count > 0)
                    //            {
                    //                orderdata.Tax = taxDetailsList.Sum(x => x.Amount);
                    //                orderdata.TaxCalculated = true;
                    //            }
                    //            else
                    //            {
                    //                orderdata.Tax = 0;
                    //                orderdata.TaxCalculated = false;
                    //            }

                    //            orderdata.OrderTotal = orderdata.OrderSubtotal + orderdata.Tax;
                    //            orderdata.BalanceDue = orderdata.OrderTotal;

                    //            //  OrderId = QuotesMgr.CreateOrder(orderdata.QuoteKey);
                    //            CreateInvoice(orderdata, true);
                    //        }
                    //        else
                    //        {
                    //            var err = string.Empty;
                    //            foreach (var item in responseObj.error)
                    //            {
                    //                //JurisdictionNotFoundError:
                    //                err = err + "<li>" + item.Value + "</li>";
                    //            }
                    //        }
                    //    }
                    //}
                    //orderdata.IsTaxExempt = data.IsTaxExempt;
                    //orderdata.TaxExemptID = data.TaxExemptID;

                    orderdata.ImpersonatorPersonId = this.ImpersonatorPersonId;

                    orderdata.LastUpdatedOn = DateTime.UtcNow;
                    orderdata.LastUpdatedBy = User.PersonId;
                    orderdata.SendReviewEmail = data.SendReviewEmail;
                    if (orderdata.OrderStatus == 6 || orderdata.OrderStatus == 9)
                        orderdata.BalanceDue = 0;
                    var datas = Update<Orders>(ContextFactory.CrmConnectionString, orderdata);
                    EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(orderdata), OrderId: orderdata.OrderID);
                }
            }
            return "Success";
        }

        public override string Add(Orders data)
        {
            throw new NotImplementedException();
        }

        public override string Delete(int id)
        {
            throw new NotImplementedException();
        }

        public override Orders Get(int id)
        {
            throw new NotImplementedException();
        }

        public override ICollection<Orders> Get(List<int> idList)
        {
            throw new NotImplementedException();
        }

        public override string Update(Orders data)
        {
            throw new NotImplementedException();
        }

        public bool DeleteTax(int orderId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();

                    var fQuery = @"DELETE td FROM crm.TaxDetails td
                                   INNER JOIN crm.Tax t ON td.TaxId = t.TaxId
                                   INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = t.QuoteLineId
                                   INNER JOIN crm.Orders o ON o.QuoteKey = ql.QuoteKey
                                   WHERE orderid = @orderid
                                   DELETE t FROM crm.TaxDetails td
                                   INNER JOIN crm.Tax t ON td.TaxId = t.TaxId
                                   INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = t.QuoteLineId
                                   INNER JOIN crm.Orders o ON o.QuoteKey = ql.QuoteKey
                                   WHERE orderid = @orderid";

                    var fDetails = connection.QueryMultiple(fQuery, new { orderid = orderId });
                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public OrderDTO GetOrderByOpportunityId(int opprtunityId)
        {
            var result = new OrderDTO();

            var query = @"select o.OrderNumber, o.OrderID,o.OpportunityId,o.QuoteKey,OrderName,o.OrderStatus,ContractedDate,op.AccountId,fd.name AS DiscountDescription,
                                         o.ProductSubtotal,o.AdditionalCharges,OrderDiscount,OrderSubtotal,Tax,OrderTotal,
                                         BalanceDue,p.FirstName+' '+p.LastName as SalesAgent,op.OpportunityName as Opportunity,
                                         cu.FirstName+''+cu.LastName + case when cu.CompanyName is not null and cu.CompanyName!='' then ' / '+cu.CompanyName else '' end  as AccountName,
                                         ad.Address1+' '+ad.Address2+' '+ad.City+','+ad.State+','+ad.ZipCode as InstallationAddress,
                                         bad.Address1+' '+bad.Address2+' '+bad.City+','+bad.State+','+bad.ZipCode as BillingAddress,
                                         os.OrderStatus as OrderStatusText,o.CreatedOn as CreatedOn,o.LastUpdatedOn as LastUpdate from [CRM].[Orders] o
                                         join [CRM].[Opportunities] op on o.OpportunityId=op.OpportunityId
										 JOIN crm.Quote q ON q.QuoteKey = o.QuoteKey
                                         left join [CRM].[Person] p on op.SalesAgentId=p.PersonId
                                         left join [CRM].[Accounts] ac on op.AccountId=ac.AccountId
                                         left join [CRM].[Customer] cu on ac.PersonId=cu.CustomerId
                                         left join [CRM].[Addresses] ad on op.InstallationAddressId=ad.AddressId
                                         left join [CRM].[Addresses] bad on op.BillingAddressId=bad.AddressId
                                         left join [CRM].[Type_OrderStatus] os on o.OrderStatus=os.OrderStatusId
										 LEFT  JOIN crm.FranchiseDiscount fd ON q.DiscountId = fd.DiscountId
                                         where o.orderstatus not in (6,9) and op.OpportunityId=@OpportunityId";
            result = ExecuteIEnumerableObject<OrderDTO>(ContextFactory.CrmConnectionString, query, new
            {
                OpportunityId = opprtunityId
            }).FirstOrDefault();

            return result;
        }

        public List<TaxDetails> GetTaxDetails(int OrderId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();

                    var fQuery = @"SELECT Jurisdiction, JurisType,td.TaxName, td.Rate, SUM(td.Amount) AS Amount
                                  FROM crm.tax AS t
                                  	 INNER JOIN
                                  	 crm.taxdetails AS td
                                  	 ON td.taxid = t.taxid
                                  	 INNER JOIN
                                  	 crm.quotelines AS ql
                                  	 ON ql.quotelineid = t.quotelineid
                                  	 INNER JOIN
                                  	 crm.quote AS q
                                  	 ON ql.quotekey = q.quotekey
                                  	 INNER JOIN
                                  	 crm.orders AS o
                                  	 ON o.quotekey = q.quotekey
                                  WHERE o.orderid = @orderid
                                  GROUP BY td.jurisType, td.Jurisdiction,td.TaxName, td.Rate order by JurisType asc;";

                    var fDetails = connection.QueryMultiple(fQuery, new { orderid = OrderId });

                    var taxDetails = fDetails.Read<TaxDetails>().ToList();
                    return taxDetails;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public Orders GetOrderByOrderId(int OrderId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();

                    var fQuery = @"select * from crm.Orders o
                                   INNER JOIN crm.Opportunities o2 ON o.OpportunityId = o2.OpportunityId
                                   WHERE o.OrderID = @OrderID and o2.FranchiseId = @FranchiseId";

                    var fDetails = connection.QueryMultiple(fQuery, new { OrderID = OrderId, FranchiseId = Franchise.FranchiseId });

                    var order = fDetails.Read<Orders>().FirstOrDefault();
                    return order;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        //Added
        public OrderDTO GetOrder(int id, int FranchiseId)
        {
            OrderDTO result = new OrderDTO();
            var query = "";
            result = ExecuteIEnumerableObject<OrderDTO>(ContextFactory.CrmConnectionString, "exec [CRM].[GetOrderDetails] @FranchiseId,@orderId", new
            {
                orderId = id,
                FranchiseId = FranchiseId
            }).FirstOrDefault();
            if (result != null)
            {
                query = @"select cus.FirstName+' '+cus.LastName as ContactName,* from CRM.Addresses addr
                      left join CRM.Customer cus on cus.CustomerId = addr.ContactId
                      where AddressId = @addressId";
                var insAddress = ExecuteIEnumerableObject<AddressTP>(ContextFactory.CrmConnectionString, query, new
                {
                    addressId = result.InstallationAddressId
                }).FirstOrDefault();
                if (insAddress != null)
                    result.InstallAddress = insAddress;
                var bilAddress = ExecuteIEnumerableObject<AddressTP>(ContextFactory.CrmConnectionString, query, new
                {
                    addressId = result.BillingAddressId
                }).FirstOrDefault();
                if (bilAddress != null)
                    result.BillAddress = bilAddress;
            }
            if (result != null)
            {
                if (result.CreatedOn != null)
                    result.CreatedOn = TimeZoneManager.ToLocal(((DateTimeOffset)result.CreatedOn).DateTime);
                if (result.LastUpdate != null)
                    result.LastUpdate = TimeZoneManager.ToLocal(((DateTimeOffset)result.LastUpdate).DateTime);
                if (result.ContractedDate != null)
                    result.ContractedDate = ((DateTimeOffset)result.ContractedDate).DateTime;
                // result.ContractedDate = TimeZoneManager.ToLocal(((DateTimeOffset)result.ContractedDate).DateTime);
                if (result.OpportunityCreatedDate == result.OpportunityReceivedDate)
                {
                    if (result.OpportunityReceivedDate != null)
                        result.OpportunityReceivedDate = TimeZoneManager.ToLocal(((DateTimeOffset)result.OpportunityReceivedDate).DateTime);
                }
                if (result.OpportunityCreatedDate != null)
                    result.OpportunityCreatedDate = TimeZoneManager.ToLocal(((DateTimeOffset)result.OpportunityCreatedDate).DateTime);
                if (result.ContractedDateUpdatedOn != null)
                    result.ContractedDateUpdatedOn = TimeZoneManager.ToLocal(((DateTimeOffset)result.ContractedDateUpdatedOn).DateTime);

                string quoteQ = "select q.* from CRM.Quote q join CRM.Orders o on q.QuoteKey=o.QuoteKey where OrderID=@OrderID";
                result.Quote = ExecuteIEnumerableObject<Quote>(ContextFactory.CrmConnectionString, quoteQ, new { OrderID = id }).FirstOrDefault();

                string queryOrderMPODetails = @"select distinct mpo.IsXMpo,
                                                case when mpo.IsXMpo=1 then 'X - ' + cast(mpo.MasterPONumber as varchar) 
                                                else cast(mpo.MasterPONumber as varchar) end as MasterPONumber,
                                                MPO.PurchaseOrderId
                                                from crm.QuoteLines ql
                                                left join crm.Orders o on o.QuoteKey = ql.QuoteKey
                                                Left join crm.MasterPurchaseOrderExtn mpox on mpox.OrderId = o.OrderID
                                                left join crm.MasterPurchaseOrder mpo on mpo.PurchaseOrderId = mpox.PurchaseOrderId
                                                where o.OrderID=@OrderID and mpo.POStatusId <> 8";
                result.MPODetails = ExecuteIEnumerableObject<OrderMPODetails>(ContextFactory.CrmConnectionString, queryOrderMPODetails, new { OrderID = id }).ToList();

                result.QuoteLinesVM = QuotesMgr.GetQuoteLine(result.QuoteKey);

                string quoteLine = @"select pod.QuoteLineId,pod.StatusId from CRM.Orders o 
                                    join CRM.MasterPurchaseOrderExtn mpox on mpox.OrderId = o.OrderID 
                                    join CRM.PurchaseOrderDetails pod on mpox.PurchaseOrderId = pod.PurchaseOrderId 
                                    where o.OrderID = @OrderID";
                var res_stat = ExecuteIEnumerableObject<dynamic>(ContextFactory.CrmConnectionString, quoteLine, new { OrderID = id }).ToList();

                foreach (var objj in result.QuoteLinesVM)
                {
                    if ((from s in res_stat where s.QuoteLineId == objj.QuoteLineId select s).ToList().Count > 0)
                    {
                        var obj11 = from s in res_stat where s.QuoteLineId == objj.QuoteLineId && s.StatusId != 8 && s.StatusId != 9 && s.StatusId != 10 select s.QuoteLineId;
                        if (obj11.Count() > 0) objj.vpoStatus = true; else objj.vpoStatus = false;
                    }
                    else objj.vpoStatus = true;

                    if (objj.ProductTypeId == 4)
                        objj.ProductName = objj.Description;
                }

                var queryOrderPayments = @"select PaymentID,pm.PaymentMethod as Method,rr.Reason as Reason,VerificationCheck as [Authorization],PaymentDate,Amount,
                 Memo,Reversal,ReasonCode from [CRM].[Payments] p
                 left join [CRM].[Type_PaymentMethod] pm on p.PaymentMethod=pm.Id
                 left join CRM.[Type_ReverseReasonCode] rr on p.ReasonCode=rr.ReasonCodeId
                 where OrderID=@Id";

                var OrderPayments = ExecuteIEnumerableObject<Payments>(ContextFactory.CrmConnectionString, queryOrderPayments, new
                {
                    Id = id
                }).ToList();
                foreach (var item in OrderPayments)
                {
                    if (item.Reversal == true)
                    {
                        item.Amount = item.Amount * -1;
                    }
                }

                result.OrderPayments = OrderPayments;

                var queryOrderInvoices = @"
                                    select [OrderID] as OrderID, Id as InvoiceId,
                                    InvoiceType as InvoiceType,
                                    InvoiceDate as  InvoiceDate,
                                    TotalAmount as TotalAmount,
                                    Streamid,
                                    Balance as  Balance from crm.InvoiceHistory
                                    where OrderID=@Id
                                    ";
                var OrderInvoices = ExecuteIEnumerableObject<InvoiceHistory>(ContextFactory.CrmConnectionString, queryOrderInvoices, new
                {
                    Id = id
                }).ToList();
                OrderInvoices = OrderInvoices.Where(n => n.Streamid != null).ToList();
                result.OrderInvoices = OrderInvoices;

                //HandleSalesTaxExempt(result.OpportunityId, result);

                result.TaxDetails = GetTaxDetails(id);

                string taxinfoQ = @"select t.* from CRM.Tax t
                                    join CRM.QuoteLines ql on t.QuoteLineId=ql.QuoteLineId
                                    join CRM.Orders o on ql.QuoteKey=o.QuoteKey
                                    where o.OrderID=@OrderID";
                result.Taxinfo = ExecuteIEnumerableObject<Tax>(ContextFactory.CrmConnectionString, taxinfoQ, new { OrderID = id }).ToList();

                string DiscountsAndPromoQ = @"select dp.* from CRM.DiscountsAndPromo dp
                                    join CRM.QuoteLines ql on dp.QuoteLineId=ql.QuoteLineId
                                    join CRM.Orders o on ql.QuoteKey=o.QuoteKey
                                    where o.OrderID=@OrderID";
                result.DiscountsAndPromo = ExecuteIEnumerableObject<DiscountsAndPromo>(ContextFactory.CrmConnectionString, DiscountsAndPromoQ, new { OrderID = id }).ToList();

                result.PaymnetsApplied = result.OrderTotal - result.BalanceDue;
            }

            return result;
        }

        public List<Type_OrderStatusTP> GetOrderStatues(int id)
        {
            List<Type_OrderStatusTP> result = new List<Type_OrderStatusTP>();
            if (id > 0)
            {
                var query = @"select OrderStatusId,OrderStatus
                             from [CRM].[Type_OrderStatus]
                            where OrderStatusId=@Id";
                result = ExecuteIEnumerableObject<Type_OrderStatusTP>(ContextFactory.CrmConnectionString, query, new
                {
                    Id = id
                }).ToList();
            }
            else
            {
                var query = @"select OrderStatusId,OrderStatus
                            from [CRM].[Type_OrderStatus] ";
                result = ExecuteIEnumerableObject<Type_OrderStatusTP>(ContextFactory.CrmConnectionString, query).ToList();
            }
            return result;
        }

        public List<Type_PaymentMethod> OrderPaymentMethods(int id)
        {
            List<Type_PaymentMethod> result = new List<Type_PaymentMethod>();
            if (id > 0)
            {
                var query = @"select Id,PaymentMethod
                             from [CRM].[Type_PaymentMethod]
                            where Id=@Id";
                result = ExecuteIEnumerableObject<Type_PaymentMethod>(ContextFactory.CrmConnectionString, query, new
                {
                    Id = id
                }).ToList();
            }
            else
            {
                var query = @"select Id,PaymentMethod
                            from [CRM].[Type_PaymentMethod] ";
                result = ExecuteIEnumerableObject<Type_PaymentMethod>(ContextFactory.CrmConnectionString, query).ToList();
            }
            return result;
        }

        public List<Type_ReverseReasonCode> OrderReversal(int id)
        {
            List<Type_ReverseReasonCode> result = new List<Type_ReverseReasonCode>();
            var query = @"select ReasonCodeId,Reason from [CRM].[Type_ReverseReasonCode]";
            result = ExecuteIEnumerableObject<Type_ReverseReasonCode>(ContextFactory.CrmConnectionString, query).ToList();
            return result;
        }

        public PaymentDTO OrderPaymentsByPaymentId(int paymentId, int orderId)
        {
            OrderDTO order = new OrderDTO();
            PaymentDTO payment = new PaymentDTO();

            var query = @"select o.OrderID,o.OpportunityId,o.QuoteKey,OrderName,o.OrderStatus,ContractedDate,
                            o.ProductSubtotal,o.AdditionalCharges,OrderDiscount,OrderSubtotal,Tax,OrderTotal,
                            BalanceDue,p.FirstName+' '+p.LastName as SalesAgent,op.OpportunityName as Opportunity,
                            (select [CRM].[fnGetAccountNameByAccountId](ac.AccountId)) as AccountName,
                            ad.Address1+' '+ad.City+','+ad.State+','+ad.ZipCode as InstallationAddress,
                            os.OrderStatus as OrderStatusText,o.CreatedOn as CreatedOn,o.LastUpdatedOn as LastUpdate,q.SideMark from [CRM].[Orders] o
                            join [CRM].[Opportunities] op on o.OpportunityId=op.OpportunityId
                            left join [CRM].[Person] p on op.SalesAgentId=p.PersonId
                            left join [CRM].[Accounts] ac on op.AccountId=ac.AccountId
                            left join [CRM].[Addresses] ad on op.InstallationAddressId=ad.AddressId
                            left join [CRM].[Type_OrderStatus] os on o.OrderStatus=os.OrderStatusId
                            left join [CRM].[Quote] q on o.QuoteKey=q.QuoteKey
                            where orderId=@Id";
            order = ExecuteIEnumerableObject<OrderDTO>(ContextFactory.CrmConnectionString, query, new
            {
                Id = orderId
            }).FirstOrDefault();

            //var queryOrderPayments = @"SELECT  [PaymentID]
            //              ,[OpportunityID]
            //              ,[OrderID]
            //              ,[Sidemark]
            //              ,[Total]
            //              ,[BalanceDue]
            //              ,[PayBalance]
            //              ,[PaymentMethod]
            //              ,[VerificationCheck]
            //              ,[PaymentDate]
            //              ,[Amount]
            //              ,[Memo]
            //              ,[CreatedOn]
            //              ,[CreatedBy]
            //              ,[LastUpdatedOn]
            //              ,[LastUpdatedBy]
            //              ,[Reversal]
            //          FROM [CRM].[Payments]
            //    where paymentId=@Id";

            //query modified to get the reversal amount
            var queryOrderPayments = @"select * from(SELECT sum(case when Reversal=1 then Amount*-1 else Amount end) as ReversalAmount,OrderID FROM CRM.Payments
                                    WHERE OrderID=@orderid group by OrderID) as p
                                    join CRM.Payments pa on p.OrderID=pa.OrderID
                                    where PaymentID=@Id";
            payment = ExecuteIEnumerableObject<PaymentDTO>(ContextFactory.CrmConnectionString, queryOrderPayments, new
            {
                orderid = orderId,
                Id = paymentId
            }).FirstOrDefault();

            payment.order = order;

            return payment;
        }

        public PaymentDTO OrderPayments(int id)
        {
            var payment = new PaymentDTO();

            var query = @"select o.OrderID,o.OpportunityId,o.QuoteKey,OrderName,o.OrderStatus,ContractedDate,
                          o.ProductSubtotal,o.AdditionalCharges,OrderDiscount,OrderSubtotal,Tax,OrderTotal,
                          o.BalanceDue,p.FirstName+' '+p.LastName as SalesAgent,op.OpportunityName as Opportunity,
                          (select [CRM].[fnGetAccountNameByAccountId](ac.AccountId)) as AccountName,
                          ad.Address1+' '+ad.City+','+ad.State+','+ad.ZipCode as InstallationAddress,
                          os.OrderStatus as OrderStatusText,o.CreatedOn as CreatedOn,o.LastUpdatedOn as LastUpdate 
                          ,(SELECT sum(isnull(OverPaymentAmount,0)) FROM CRM.Payments WHERE OrderID=o.OrderID ) as OverPaymentAmount
                          ,isnull(ac.IsNotifyemails,0) as IsNotifyemails, q.SideMark
                          from [CRM].[Orders] o
                          join [CRM].[Opportunities] op on o.OpportunityId=op.OpportunityId
                          left join [CRM].[Person] p on op.SalesAgentId=p.PersonId
                          left join [CRM].[Accounts] ac on op.AccountId=ac.AccountId
                          left join [CRM].[Customer] cu on ac.PersonId=cu.CustomerId
                          left join [CRM].[Addresses] ad on op.InstallationAddressId=ad.AddressId
                          left join [CRM].[Type_OrderStatus] os on o.OrderStatus=os.OrderStatusId
                          left join [CRM].[Quote] q on o.QuoteKey=q.QuoteKey
                          where o.OrderID=@Id";
            var order = ExecuteIEnumerableObject<OrderDTO>(ContextFactory.CrmConnectionString, query, new
            {
                Id = id
            }).FirstOrDefault();

            //payment.order = order;

            //var queryOrderPayments = @"SELECT  [PaymentID]
            //              ,[OpportunityID]
            //              ,[OrderID]
            //              ,[Sidemark]
            //              ,[Total]
            //              ,[BalanceDue]
            //              ,[PayBalance]
            //              ,[PaymentMethod]
            //              ,[VerificationCheck]
            //              ,[PaymentDate]
            //              ,[Amount]
            //              ,[Memo]
            //              ,[CreatedOn]
            //              ,[CreatedBy]
            //              ,[LastUpdatedOn]
            //              ,[LastUpdatedBy]
            //          FROM [CRM].[Payments]
            //    where OrderID=@Id";
            //var response = ExecuteIEnumerableObject<PaymentDTO>(ContextFactory.CrmConnectionString, queryOrderPayments, new
            //{
            //    Id = id
            //}).FirstOrDefault();

            //if (response != null)
            //{
            //    payment.OrderID = 6;
            //    payment.OpportunityID = order.OpportunityId;
            //    payment = response;
            //    payment.order = order;
            //}
            payment.OrderID = id;
            if (order != null)
            {
                payment.OpportunityID = order.OpportunityId;
                payment.order = order;
                payment.BalanceDue = order.BalanceDue;
                if (order.OverPaymentAmount == null)
                    payment.OverPaymentAmount = 0;
                else
                    payment.OverPaymentAmount = order.OverPaymentAmount;
                payment.IsNotifyemails = order.IsNotifyemails;
            }

            return payment;
        }

        public List<Payments> GetPaymentByOrderId(int orderId)
        {
            var query = @"Select * from CRM.Payments where OrderID=@orderId";
            var payments = ExecuteIEnumerableObject<Payments>(ContextFactory.CrmConnectionString, query, new { orderId }).ToList();
            return payments;
        }

        //to get reversal amount for pop-up
        public Payments GetReverseAmountByOrderId(int Id)
        {
            var query = @"SELECT sum(case when Reversal=1 then Amount*-1 else Amount end) as RevesalAmount  FROM CRM.Payments WHERE OrderID=@Id ";
            var payments = ExecuteIEnumerableObject<Payments>(ContextFactory.CrmConnectionString, query, new { Id }).FirstOrDefault();
            return payments;
        }

        public Person GetSalesAgentByOrderId(int orderId)
        {
            string query = @"select * from CRM.Person p
                            join CRM.Opportunities op on p.PersonId=op.SalesAgentId
                            join CRM.Orders o on op.OpportunityId=o.OpportunityId
                            where o.OrderID=@OrderID";
            return ExecuteIEnumerableObject<Person>(ContextFactory.CrmConnectionString, query, new { OrderID = orderId }).FirstOrDefault();
        }

        public CustomerTP GetCustomerByOrderId(int orderId)
        {
            string query = @"select * from CRM.Customer c
                            join CRM.AccountCustomers ac on c.CustomerId=ac.CustomerId and ac.IsPrimaryCustomer=1
                            join CRM.Accounts a on ac.AccountId=a.AccountId
                            join CRM.Opportunities op on a.AccountId=op.AccountId
                            join CRM.Orders o on op.OpportunityId=o.OpportunityId
                            where o.OrderID=@OrderID";
            return ExecuteIEnumerableObject<CustomerTP>(ContextFactory.CrmConnectionString, query, new { OrderID = orderId }).FirstOrDefault();
        }

        public AddressTP GetBillingAddressByOrderId(int orderId)
        {
            string query = @"select * from CRM.Addresses ad
                            join CRM.Opportunities op on ad.AddressId=op.BillingAddressId
                            join CRM.Orders o on op.OpportunityId=o.OpportunityId
                            where o.OrderID=@OrderID";
            return ExecuteIEnumerableObject<AddressTP>(ContextFactory.CrmConnectionString, query, new { OrderID = orderId }).FirstOrDefault();
        }

        public AddressTP GetInstallationAddressByOrderId(int orderId)
        {
            string query = @"select * from CRM.Addresses ad
                            join CRM.Opportunities op on ad.AddressId=op.InstallationAddressId
                            join CRM.Orders o on op.OpportunityId=o.OpportunityId
                            where o.OrderID=@OrderID";
            return ExecuteIEnumerableObject<AddressTP>(ContextFactory.CrmConnectionString, query, new { OrderID = orderId }).FirstOrDefault();
        }

        public Opportunity GetOpportunityByOrderId(int orderId)
        {
            string query = @"select * from CRM.Opportunities op
                            join CRM.Orders o on op.OpportunityId=o.OpportunityId
                            where o.OrderID=@OrderID";
            return ExecuteIEnumerableObject<Opportunity>(ContextFactory.CrmConnectionString, query, new { OrderID = orderId }).FirstOrDefault();
        }

        public DisclaimerModel GetFranchiseDisclaimer()
        {
            string query = @"select * from [CRM].[Disclaimer] where FranchiseId=@FranchiseId";
            return ExecuteIEnumerableObject<DisclaimerModel>(ContextFactory.CrmConnectionString, query, new { FranchiseId = Franchise.FranchiseId }).FirstOrDefault();
        }

        public InvoiceHistory GetInvoiceHistory(int OrderID)
        {
            string query = @"select top 1 * from CRM.InvoiceHistory where OrderID=@OrderID order by CreatedOn desc";
            return ExecuteIEnumerableObject<InvoiceHistory>(ContextFactory.CrmConnectionString, query, new { OrderID }).FirstOrDefault();
        }

        public void UpdateOrderStatustoOpen()
        {
            string query = @"select o.OrderID,o.CreatedOn,f.BuyerremorseDay from CRM.Orders o
                            join CRM.Opportunities op on o.OpportunityId=op.OpportunityId
                            join CRM.Franchise f on op.FranchiseId=f.FranchiseId
                            where o.OrderStatus=8 and o.CreatedOn is not null";

            var orderdata = ExecuteIEnumerableObject<OrderStatusOpen>(ContextFactory.CrmConnectionString, query).ToList();

            foreach (var d in orderdata)
            {
                DateTime today = DateTime.UtcNow.Date;
                DateTime createdt = d.CreatedOn.AddDays(d.BuyerremorseDay).Date;
                if (today >= createdt)
                {
                    string qUpdate = @"update CRM.Orders set OrderStatus=7 where OrderID=@OrderID;UPDATE crm.OrderLines SET OrderLineStatus = 7 WHERE OrderID=@OrderID";
                    ExecuteObject(ContextFactory.CrmConnectionString, qUpdate, new { d.OrderID });
                }
            }
        }

        public string UpdateOrderStatusToOpen(int orderId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    int BalanceDue = 0;
                    //var quote = connection.Query<Quote>("select * from CRM.Quote where  QuoteKey = @QuoteKey", new { QuoteKey = quoteKey }).FirstOrDefault();
                    // var query = @"Update [CRM].[Orders] set OrderStatus=@statusId,BalanceDue=@Balance where OrderID =@orderId";

                    var queryTemp = @"select count(*) from crm.OrderLines ol
                                        where ol.OrderLineStatus = 1 and ol.OrderID = @orderid";
                    var result0 = connection.ExecuteScalar(queryTemp, new { orderid = orderId });

                    // If there is record with Purchase Order status' dont update..
                    if ((int)result0 > 0) return Success;

                    var query = @"Update [CRM].[Orders] set OrderStatus=@statusId where OrderID =@orderId;UPDATE crm.OrderLines SET OrderLineStatus = @statusId WHERE OrderID=@orderId";

                    int orderStatus = 7; // Order status = Open
                    var result = ExecuteIEnumerableObject<int>(ContextFactory.CrmConnectionString, query, new
                    {
                        statusId = orderStatus,
                        orderId = orderId,
                        //Balance = BalanceDue
                    });
                    return "Sucessful";
                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    throw e;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public bool UpdateOrderTotal(int orderId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();
                    var datetime = DateTime.UtcNow;
                    var usr = User.PersonId;

                    DeleteTax(orderId);
                    int OrderId = orderId;
                    var query = @"Select * from CRM.Orders where OrderId=@OrderId";
                    var orderdata = ExecuteIEnumerableObject<Orders>(ContextFactory.CrmConnectionString, query, new
                    {
                        OrderId = OrderId
                    }).FirstOrDefault();

                    var quoteQuery = @"select * from crm.Quote q where q.QuoteKey=@QuoteKey;";
                    var quoteData = ExecuteIEnumerableObject<Quote>(ContextFactory.CrmConnectionString, quoteQuery, new
                    {
                        QuoteKey = orderdata.QuoteKey
                    }).FirstOrDefault();

                    var payments = GetPaymentByOrderId(orderId).OrderBy(x => x.PaymentID);

                    if (orderdata != null)
                    {
                        if (!orderdata.IsTaxExempt)
                        {
                            //QuotesMgr.DeleteTax(orderdata.QuoteKey);
                            //var response = QuotesMgr.GetAndUpdateTax(orderdata.QuoteKey);
                            //dynamic responseObj = JsonConvert.DeserializeObject(response);


                            QuotesMgr.CreateTaxOnSalesOrder(orderdata.QuoteKey, 0);

                            //if (Convert.ToBoolean(responseObj.valid.Value))
                            //{
                            //QuotesMgr.AddorUpdateTax(orderdata.QuoteKey, responseObj);
                            var taxDetailsList = QuotesMgr.GetTaxinfo(orderdata.QuoteKey);
                            if (taxDetailsList != null && taxDetailsList.Count > 0)
                            {
                                orderdata.Tax = taxDetailsList.Sum(x => x.Amount);
                                orderdata.TaxCalculated = true;
                            }
                            else
                            {
                                orderdata.Tax = 0;
                                orderdata.TaxCalculated = false;
                            }

                            orderdata.ProductSubtotal = quoteData.ProductSubTotal;
                            orderdata.AdditionalCharges = quoteData.AdditionalCharges;
                            orderdata.OrderDiscount = quoteData.TotalDiscounts;
                            orderdata.OrderSubtotal = quoteData.QuoteSubTotal;

                            orderdata.OrderTotal = orderdata.OrderSubtotal + orderdata.Tax;

                            if (payments != null && payments.Count() > 0)
                            {
                                decimal balancedue = orderdata.OrderTotal.Value;
                                foreach (var py in payments.OrderBy(x => x.PaymentID))
                                {
                                    py.Total = orderdata.OrderTotal;
                                    if (py.Reversal.HasValue && py.Reversal.Value)
                                    {
                                        py.BalanceDue = balancedue + py.Amount;
                                    }
                                    else
                                    {
                                        py.BalanceDue = balancedue - py.Amount;
                                    }

                                    py.LastUpdatedBy = usr;
                                    py.LastUpdatedOn = datetime;
                                    connection.Update(py);
                                }

                                orderdata.BalanceDue =
                                    orderdata.OrderTotal - payments
                                        .Where(x => x.Reversal.HasValue && !x.Reversal.Value)
                                        .Sum(x => x.Amount) + payments
                                        .Where(x => x.Reversal.HasValue && x.Reversal.Value)
                                        .Sum(x => x.Amount);
                            }
                            else
                            {
                                orderdata.BalanceDue = orderdata.OrderTotal;
                            }
                            CreateInvoice(orderdata, true);
                            //}
                            //else
                            //{
                            //    var err = string.Empty;
                            //    foreach (var item in responseObj.error)
                            //    {
                            //        err = err + "<li>" + item.Value + "</li>";
                            //    }
                            //}
                        }
                        orderdata.LastUpdatedBy = usr;
                        orderdata.LastUpdatedOn = datetime;
                        connection.Update(orderdata);
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public string SendOrdermail(int OrderId, EmailType emailType, string Streamid, bool typestatus, string ResendMail)
        {
            try
            {
                List<string> toAddresses = new List<string>();
                List<string> ccAddresses = new List<string>();
                List<string> bccAddresses = new List<string>();
                CustomerTP tocustomerAddress = new CustomerTP();

                var orderInfo = emailMgr.GetOrderInfo(OrderId);

                var EmailSettings = emailMgr.GetConfiguredEmails().Where(x => x.EmailType == (int)emailType).FirstOrDefault();
                if (EmailSettings != null)
                {
                    if (OrderId > 0)
                    {
                        if (EmailSettings.CopyAssignedSales == true)
                        {
                            var salesemail = emailMgr.GetSalesPersonEmail(0, OrderId);
                            if (salesemail != null && !string.IsNullOrEmpty(salesemail.Email) && salesemail.IsDisabled != true)
                                //if (salesemail.Email != null && salesemail.Email != "" && salesemail.IsDisabled !=true)
                                ccAddresses.Add(salesemail.Email);
                        }
                        if (EmailSettings.CopyAssignedInstaller == true)
                        {
                            var installeremail = emailMgr.GetInstallerEmail(0, OrderId);
                            if (installeremail != null && !string.IsNullOrEmpty(installeremail.Email) && installeremail.IsDisabled != true)
                                //if (installeremail.Email != null && installeremail.Email != "" && installeremail.IsDisabled !=true)
                                ccAddresses.Add(installeremail.Email);
                        }
                    }
                }

                TerritoryDisplay data = emailMgr.GetTerritoryDisplayByOpp((int)orderInfo.OpportunityId);
                //BUILD - From email address
                string fromEmailAddress = "";
                if (data != null && data.Id == null)
                    fromEmailAddress = emailMgr.GetFranchiseEmail(Franchise.FranchiseId, emailType);
                else
                {
                    if (Franchise.AdminEmail != "")
                        fromEmailAddress = Franchise.AdminEmail;
                    else if (Franchise.OwnerEmail != "")
                        fromEmailAddress = Franchise.OwnerEmail;
                }

                tocustomerAddress = emailMgr.GetOrderCustomerEmail(OrderId);

                if (tocustomerAddress != null)
                {
                    if (tocustomerAddress.PrimaryEmail != null && tocustomerAddress.PrimaryEmail.Trim() != "")
                    {
                        toAddresses.Add(tocustomerAddress.PrimaryEmail.Trim());
                    }
                    if (tocustomerAddress.SecondaryEmail != null && tocustomerAddress.SecondaryEmail.Trim() != "")
                    {
                        toAddresses.Add(tocustomerAddress.SecondaryEmail.Trim());
                    }
                }

                //If no from or to emails return
                if (toAddresses == null || toAddresses.Count == 0 || fromEmailAddress == "")
                    return "Success";

                //Preparing email Body with formating
                string TemplateSubject = "", bodyHtml = "";
                int FranchiseId = 0;
                //Email Template: 1 Thanks Email; 2 Appointment Confirmation

                int BrandId = (int)SessionManager.BrandId;

                //Take Body Html from EmailTemplate Table
                var emailtemplate = emailMgr.GetHtmlBody(emailType, BrandId);
                TemplateSubject = emailtemplate.TemplateSubject;
                bodyHtml = emailtemplate.TemplateLayout;
                short TemplateEmailId = (short)emailtemplate.EmailTemplateId;

                if (emailType == EmailType.ThankyouReview)  //== 6)
                {
                    //do not send Invoice with Than you email
                    Streamid = "";
                    var mailMsg = emailMgr.BuildMailMessageThankyou(
                        0, orderInfo.AccountId, orderInfo.OpportunityId, OrderId, typestatus, ResendMail, bodyHtml, TemplateSubject, TemplateEmailId, BrandId, FranchiseId,
                        fromEmailAddress, toAddresses, ccAddresses, bccAddresses, Streamid);
                }

                return "Success";
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return "Failure";
            }
        }

        public TerritoryDisplay GetTerritoryDisplayData(int OrderId)
        {
            string query = @"select isnull(tes.UseFranchiseName,1) UseFranchiseName,tes.LicenseNumber,t.WebSiteURL, (select [CRM].[fnGetTerritoryDisplayName](t.BrandId,t.TerritoryId)) as Displayname, stl.PhoneNumber as Phone, ad.Address1,ad.Address2,ad.City,ad.State,ad.ZipCode
                             from CRM.Orders o
                             join CRM.Opportunities op on o.OpportunityId=op.OpportunityId
                             left join CRM.Territories t on op.TerritoryId=t.TerritoryId
                             left join CRM.TerritoryEmailSettings tes on t.TerritoryId=tes.TerritoryId
                             left join CRM.ShipToLocations stl on tes.ShipToLocationId=stl.Id
                             left join CRM.Addresses ad on stl.AddressId=ad.AddressId
                             where op.FranchiseId=@FranchiseId and o.OrderID=@OrderId";

            string query2 = @"select isnull(tes.UseFranchiseName,1) UseFranchiseName,tes.LicenseNumber,t.WebSiteURL, (select [CRM].[fnGetTerritoryDisplayName](t.BrandId,t.TerritoryId)) as Displayname, stl.PhoneNumber as Phone, ad.Address1,ad.Address2,ad.City,ad.State,ad.ZipCode
                             from CRM.Orders o
                             join CRM.Opportunities op on o.OpportunityId=op.OpportunityId
                             left join CRM.Territories t on op.TerritoryDisplayId=t.TerritoryId
                             left join CRM.TerritoryEmailSettings tes on t.TerritoryId=tes.TerritoryId
                             left join CRM.ShipToLocations stl on tes.ShipToLocationId=stl.Id
                             left join CRM.Addresses ad on stl.AddressId=ad.AddressId
                             where op.FranchiseId=@FranchiseId and o.OrderID=@OrderId";
            // return ExecuteIEnumerableObject<TerritoryDisplay>(ContextFactory.CrmConnectionString, query, new { Franchise.FranchiseId, OrderId }).FirstOrDefault();
            // var res = new TerritoryDisplay();

            var checkres = ExecuteIEnumerableObject<Opportunity>(ContextFactory.CrmConnectionString, @"select * from CRM.Opportunities Op
join CRM.Orders Ord on ord.OpportunityId = Op.OpportunityId
where  Op.FranchiseId= @FranchiseId  and Ord.OrderID=@OrderID", new { Franchise.FranchiseId, OrderID = OrderId }).FirstOrDefault();

            if (!(checkres.TerritoryType.ToUpper().Contains("GRAY AREA")))
                return ExecuteIEnumerableObject<TerritoryDisplay>(ContextFactory.CrmConnectionString, query, new { Franchise.FranchiseId, OrderId }).FirstOrDefault();
            else
                return ExecuteIEnumerableObject<TerritoryDisplay>(ContextFactory.CrmConnectionString, query2, new { Franchise.FranchiseId, OrderId }).FirstOrDefault();
        }

        public string UpdateOrderValue(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                Orders orderval = new Orders();
                var query = @"select * from CRM.Orders where OrderID=@orderid";
                orderval = ExecuteIEnumerableObject<Orders>(ContextFactory.CrmConnectionString, query,
                new { OrderId = id }).FirstOrDefault();
                if (orderval.OrderStatus == 5 && orderval.SendReviewEmail == false)
                {
                    orderval.SendReviewEmail = true;
                    Update<Orders>(ContextFactory.CrmConnectionString, orderval);
                }
                return "Success";
            }
        }

        //Below code is to get the order order confirmation form details
        public OrderCustomerView GetorderCustomerView(int orderId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();

                    var fQuery = @"select * from crm.orderCustomerView where OrderId=@OrderId";
                    var orderCustomerView = connection.Query<OrderCustomerView>(fQuery, new { orderid = orderId }).FirstOrDefault();

                    if (orderCustomerView == null)
                    {
                        orderCustomerView = new OrderCustomerView();
                        orderCustomerView.OrderId = orderId;
                        orderCustomerView.CreatedOn = DateTime.UtcNow;
                        orderCustomerView.CreatedBy = User.PersonId;
                        connection.Insert<OrderCustomerView>(orderCustomerView);
                        orderCustomerView = connection.Query<OrderCustomerView>(fQuery, new { orderid = orderId }).FirstOrDefault();
                    }

                    //Initialize all the the classes
                    orderCustomerView.cVHeader = new CVHeader();
                    orderCustomerView.opportunity = new OpportunityInformation();
                    orderCustomerView.opportunity.billToAddress = new BillToAddress();
                    orderCustomerView.opportunity.installationAddress = new InstallationAddress();
                    orderCustomerView.products = new List<ProductSummary>();
                    orderCustomerView.services = new List<AdditionalItems>();
                    orderCustomerView.discounts = new List<DiscountInformation>();
                    orderCustomerView.payment = new List<PaymentInformation>();
                    orderCustomerView.terms = new TermsAndConditions();
                    orderCustomerView.orderSummary = new OrderSummary();

                    string phoneFormat = "(###) ###-####";
                    var printVersion = PrintVersion.CondensedVersion;   //customer view has only less information similar to condensed version of print form

                    //Getting all the data related to the order
                    var salesAgentInfo = GetSalesAgentByOrderId(orderId);
                    var order = GetOrder(orderId, this.Franchise.FranchiseId);
                    var Opportunity = GetOpportunityByOrderId(orderId);
                    var customer = GetCustomerByOrderId(orderId);
                    var BillingAddress = GetBillingAddressByOrderId(orderId);
                    var InstallationAddress = GetInstallationAddressByOrderId(orderId);
                    if (BillingAddress == null) BillingAddress = InstallationAddress;
                    var disclaimer = GetFranchiseDisclaimer();
                    var FEdiscounts = new ProductManagerTP().GetListDetails(3);

                    //if (disclaimer != null && !string.IsNullOrEmpty(disclaimer.ShortDisclaimer) && disclaimer.ShortDisclaimer != "")
                    //    disclaimer.ShortDisclaimer = disclaimer.ShortDisclaimer.Replace(Environment.NewLine, "<br />").Replace("\r", "<br />").Replace("\n", "<br />");

                    //if (disclaimer != null && !string.IsNullOrEmpty(disclaimer.LongDisclaimer) && disclaimer.LongDisclaimer != "")
                    //    disclaimer.LongDisclaimer = disclaimer.LongDisclaimer.Replace(Environment.NewLine, "<br />").Replace("\r", "<br />").Replace("\n", "<br />");

                    //if (order != null && !string.IsNullOrEmpty(order.OrderInvoiceLevelNotes) && order.OrderInvoiceLevelNotes != "")
                    //    order.OrderInvoiceLevelNotes = order.OrderInvoiceLevelNotes.Replace(Environment.NewLine, "<br />").Replace("\r", "<br />").Replace("\n", "<br />");

                    var Measurementres = MeasurementMgr.Get(InstallationAddress.AddressId);
                    List<MeasurementLineItemBB> MeasurementData = new List<MeasurementLineItemBB>();
                    if (Measurementres != null)
                        MeasurementData = Measurementres.MeasurementBB;

                    if (customer.HomePhone != null && customer.HomePhone.Length > 0)
                    {
                        customer.HomePhone = Convert.ToInt64(customer.HomePhone).ToString(phoneFormat);
                        if (customer.PreferredTFN != null && customer.PreferredTFN == "H")
                            customer.HomePhone = "*" + customer.HomePhone;
                    }
                    else
                        customer.HomePhone = "";

                    if (customer.CellPhone != null && customer.CellPhone.Length > 0)
                    {
                        customer.CellPhone = Convert.ToInt64(customer.CellPhone).ToString(phoneFormat);
                        if (customer.PreferredTFN != null && customer.PreferredTFN == "C")
                            customer.CellPhone = "*" + customer.CellPhone;
                    }
                    else
                        customer.CellPhone = "";

                    if (customer.WorkPhone != null && customer.WorkPhone.Length > 0)
                    {
                        customer.WorkPhone = Convert.ToInt64(customer.WorkPhone).ToString(phoneFormat);
                        if (customer.PreferredTFN != null && customer.PreferredTFN == "W")
                            customer.WorkPhone = "*" + customer.WorkPhone;
                    }
                    else
                        customer.WorkPhone = "";

                    //Binding Header Information.
                    orderCustomerView.cVHeader.InvoiceNumber = order != null ? order.OrderNumber.ToString() : "0";
                    orderCustomerView.cVHeader.InvoiceDate = order?.ContractedDate;
                    orderCustomerView.cVHeader.SalesPersonName = salesAgentInfo != null ? salesAgentInfo.FirstName + " " + salesAgentInfo.LastName : "";

                    //Binding Opportunity Information
                    orderCustomerView.opportunity.billToAddress.AccountCustomerName = order.AccountName;
                    orderCustomerView.opportunity.billToAddress.BillingAddress1 = BillingAddress.Address1;
                    orderCustomerView.opportunity.billToAddress.BillingAddress2 = BillingAddress.Address2;
                    orderCustomerView.opportunity.billToAddress.BillingCityStateZip = BillingAddress.City + ", " + BillingAddress.State + " " + BillingAddress.ZipCode;
                    orderCustomerView.opportunity.billToAddress.AccountCellPhone = customer.CellPhone;
                    orderCustomerView.opportunity.billToAddress.AccountHomePhone = customer.HomePhone;
                    orderCustomerView.opportunity.billToAddress.AccountWorkPhone = customer.WorkPhone;
                    orderCustomerView.opportunity.billToAddress.AccountEmail = customer.PrimaryEmail;
                    orderCustomerView.opportunity.billToAddress.OppSideMark = order != null && order.SideMark != null ? order.SideMark : "";

                    orderCustomerView.opportunity.installationAddress.AccountCustomerName = order.AccountName;
                    orderCustomerView.opportunity.installationAddress.InstallationAddress1 = InstallationAddress.Address1;
                    orderCustomerView.opportunity.installationAddress.InstallationAddress2 = InstallationAddress.Address2;
                    orderCustomerView.opportunity.installationAddress.InstallationCityStateZip = InstallationAddress.City + ", " + InstallationAddress.State + " " + InstallationAddress.ZipCode;


                    //Binding Product Summary
                    if (order.QuoteLinesVM != null && order.QuoteLinesVM.Count > 0)
                    {
                        var products = (from ql in order.QuoteLinesVM where ql.ProductTypeId == 1 || ql.ProductTypeId == 2 || ql.ProductTypeId == 5 select ql).ToList();

                        if (products != null && products.Count > 0)
                        {
                            foreach (var p in products)
                            {
                                if (string.IsNullOrEmpty(p.CancelReason))
                                {
                                    ProductSummary product = new ProductSummary();
                                    var tax = (from t in order.Taxinfo where t.QuoteLineId == p.QuoteLineId select t).FirstOrDefault();
                                    string isTaxable = "";
                                    if (tax != null && tax.Amount > 0)
                                        isTaxable = "Y";
                                    else isTaxable = "N";

                                    // Get vendor name from picjson
                                    string vendorname = "";
                                    if (!string.IsNullOrEmpty(p.VendorDisplayname))
                                    {
                                        vendorname = p.VendorDisplayname;
                                    }
                                    else if (p.ProductTypeId != 1 && !string.IsNullOrEmpty(p.PICJson) && p.PICJson != "")
                                    {
                                        var picjson = JsonConvert.DeserializeObject<dynamic>(p.PICJson);
                                        if (picjson != null && picjson.VendorName != null)
                                            vendorname = picjson.VendorName;
                                    }
                                    else
                                        vendorname = p.VendorName;

                                    // Remove Hight and width in core product description
                                    string prodctDesc = p.Description;

                                    if (p.ProductTypeId == 1 && !string.IsNullOrEmpty(p.Description) && p.Description != "")
                                    {
                                        List<string> lstdesc = p.Description.Split(';').ToList();
                                        var filterWidth = lstdesc.Where(x => x.TrimStart().StartsWith("Width: ")).ToList();

                                        if (filterWidth != null && filterWidth.Count > 0)
                                        {
                                            foreach (var item in filterWidth)
                                                lstdesc.Remove(item);
                                        }

                                        var filterHeight = lstdesc.Where(x => x.TrimStart().StartsWith("Height: ")).ToList();
                                        if (filterHeight != null && filterHeight.Count > 0)
                                        {
                                            foreach (var item in filterHeight)
                                                lstdesc.Remove(item);
                                        }

                                        prodctDesc = string.Join(",", lstdesc.ToArray());
                                    }

                                    if (string.IsNullOrEmpty(vendorname) && p.VendorId.HasValue && p.VendorId.Value != 0)
                                    {
                                        var vendormgr = new VendorManager();
                                        var vendor = vendormgr.GetLocalvendor(p.VendorId.Value);
                                        if (vendor.VendorType == 2 ||
                                            vendor.VendorType == 3)
                                        {
                                            vendorname = "";
                                        }
                                    }

                                    if (vendorname != "")
                                    {
                                        if (printVersion == PrintVersion.CondensedVersion)
                                            prodctDesc = vendorname;
                                        else
                                            prodctDesc = vendorname + "; " + p.Description;
                                    }

                                    string picCategory = "";
                                    dynamic picData = null;

                                    if (SessionManager.BrandId == 2 || SessionManager.BrandId == 3)
                                    {
                                        if (!string.IsNullOrEmpty(p.PICJson))
                                            picData = Newtonsoft.Json.Linq.JObject.Parse(p.PICJson);

                                        if (picData != null)
                                        {
                                            if (picData.PCategory != null) picCategory = picData.PCategory;
                                        }
                                    }

                                    if (((p.PrintProductCategory != null && p.PrintProductCategory != "") || (picCategory != "")) && SessionManager.BrandId == 2 || SessionManager.BrandId == 3)
                                    {
                                        picCategory = p.PrintProductCategory != null && p.PrintProductCategory != "" ? p.PrintProductCategory : picCategory;
                                        if (printVersion == PrintVersion.CondensedVersion)
                                        {
                                            if (p.ProductTypeId == 5 && p.ProductCategory == null)
                                            {
                                                product.ProductName = picData.PCategory; product.ProductDesc = picCategory;
                                            }
                                            else
                                                product.ProductName = picCategory; product.ProductDesc = prodctDesc;
                                        }
                                        else
                                        {
                                            if (p.ProductTypeId == 5 && p.ProductCategory == null)
                                            {
                                                product.ProductName = picData.PCategory + "-" + picCategory + Environment.NewLine + p.ProductName; product.ProductDesc = prodctDesc;
                                            }
                                            else
                                                product.ProductName = picCategory + Environment.NewLine + p.ProductName; product.ProductDesc = prodctDesc;
                                        }
                                    }
                                    else
                                    {
                                        product.ProductName = p.ProductName;
                                        product.ProductDesc = prodctDesc;
                                    }

                                    var Quantity = p.Quantity;
                                    var ExtendedPrice = p.ExtendedPrice;

                                    if (printVersion == PrintVersion.CondensedVersion)
                                    {
                                        Quantity = products.Where(x => x.ProductName == p.ProductName && x.VendorName == p.VendorName && string.IsNullOrEmpty(x.CancelReason)).Sum(x => x.Quantity);
                                        ExtendedPrice = products.Where(x => x.ProductName == p.ProductName && x.VendorName == p.VendorName && string.IsNullOrEmpty(x.CancelReason)).Sum(x => x.ExtendedPrice);
                                    }

                                    product.QTY = Quantity;
                                    product.Total = ExtendedPrice;
                                    orderCustomerView.products.Add(product);
                                }
                            }
                        }
                    }

                    //Binding Additional Items (service)
                    if (order.QuoteLinesVM != null && order.QuoteLinesVM.Count > 0)
                    {
                        var Services = (from ql in order.QuoteLinesVM where ql.ProductTypeId == 3 select ql).ToList();

                        if (Services != null && Services.Count > 0)
                        {
                            foreach (var srv in Services)
                            {
                                AdditionalItems service = new AdditionalItems();
                                var tax = (from t in order.Taxinfo where t.QuoteLineId == srv.QuoteLineId select t).FirstOrDefault();
                                bool isTaxable = false;
                                if (tax != null && tax.Amount > 0)
                                    isTaxable = true;
                                else isTaxable = false;

                                if (!string.IsNullOrEmpty(srv.ModelDescription) && srv.ProductName != srv.ModelDescription)
                                    service.Product = srv.ProductName + " - " + srv.ModelDescription;
                                else
                                    service.Product = srv.ProductName;

                                service.Memo = srv.Description;
                                service.isTaxable = isTaxable;
                                service.UnitPrice = srv.UnitPrice;
                                service.QTY = srv.Quantity;
                                service.Total = srv.ExtendedPrice;

                                orderCustomerView.services.Add(service);
                            }
                        }
                    }

                    //Binding Discount Information
                    if (order.QuoteLinesVM != null && order.QuoteLinesVM.Count > 0)
                    {
                        var discountd = (from ql in order.QuoteLinesVM where ql.ProductTypeId == 4 select ql).ToList();

                        if ((discountd != null && discountd.Count > 0) || (order.Quote != null && order.Quote.Discount != null && order.Quote.Discount > 0))
                        {
                            if (order.Quote != null && order.Quote.Discount != null && order.Quote.Discount > 0)
                            {
                                DiscountInformation discount = new DiscountInformation();
                                string DiscountName = "", DiscountDesc = "Discount";
                                if (order.Quote.DiscountId > 0 && FEdiscounts != null && FEdiscounts.Count > 0)
                                {
                                    var dis = (from d in FEdiscounts where d.ProductID == order.Quote.DiscountId select d).FirstOrDefault();
                                    if (dis != null)
                                    {
                                        DiscountName = dis.ProductName;
                                        DiscountDesc = dis.Description;
                                    }
                                }
                                string discountSummary = "0%";
                                if (order.Quote.Discount != null && order.Quote.DiscountType == "$")
                                {
                                    discountSummary = string.Format(System.Globalization.CultureInfo.GetCultureInfo(1033), "{0:C2}", order.Quote.Discount);
                                    if (DiscountName != "")
                                        discountSummary += " - " + DiscountName;
                                }
                                if (order.Quote.Discount != null && order.Quote.DiscountType == "%")
                                {
                                    discountSummary = String.Format("{0:0.##}", order.Quote.Discount) + "%";
                                    if (DiscountName != "")
                                        discountSummary += " - " + DiscountName;
                                }

                                discount.DiscountSummary = discountSummary;
                                discount.Memo = DiscountDesc;

                                orderCustomerView.discounts.Add(discount);
                            }
                            if (discountd != null && discountd.Count > 0)
                            {
                                foreach (var d in discountd)
                                {
                                    DiscountInformation discount = new DiscountInformation();
                                    string DiscountName = "", DiscountDesc = "";
                                    if (d.ProductId > 0 && FEdiscounts != null && FEdiscounts.Count > 0)
                                    {
                                        var dis = (from di in FEdiscounts where di.ProductID == d.ProductId select di).FirstOrDefault();
                                        if (dis != null)
                                        {
                                            DiscountName = dis.ProductName;
                                            DiscountDesc = d.Description;
                                        }
                                    }
                                    if (d.Discount != null && d.Discount > 0)
                                    {
                                        string discountSummary = "";
                                        if (d.Discount != null)
                                        {
                                            if (d.DiscountType == "$")
                                            {
                                                discountSummary += "$" + String.Format("{0:0.##}", d.Discount);
                                            }
                                            else
                                            {
                                                discountSummary += String.Format("{0:0.##}", d.Discount) + "%";
                                            }
                                            if (DiscountName != "")
                                                discountSummary += " - " + DiscountName;
                                        }

                                        discount.DiscountSummary = discountSummary;
                                        discount.Memo = d.Description;

                                        orderCustomerView.discounts.Add(discount);
                                    }
                                }
                            }
                        }
                    }

                    //Binding Payment Information
                    if (order.OrderPayments != null && order.OrderPayments.Count > 0)
                    {
                        foreach (var pay in order.OrderPayments)
                        {
                            PaymentInformation payment = new PaymentInformation();
                            payment.PaymentDate = pay.PaymentDate;
                            payment.Method = pay.Method;
                            payment.Memo = pay.Memo;
                            payment.Amount = pay.Amount;
                            orderCustomerView.payment.Add(payment);
                        }
                    }

                    //Binding Terms and Conditions
                    orderCustomerView.terms.TC = disclaimer != null ? disclaimer.LongDisclaimer : "";

                    //Binding Order Summary
                    decimal Totalorderamount = order.OrderTotal != null ? Convert.ToDecimal(order.OrderTotal) : 0m;
                    decimal BalanceDue = order.BalanceDue != null ? Convert.ToDecimal(order.BalanceDue) : 0m;
                    decimal TotalPayments = Totalorderamount - BalanceDue;

                    orderCustomerView.orderSummary.ProductSubtotal = order.ProductSubtotal;
                    orderCustomerView.orderSummary.AdditionalCharges = order.AdditionalCharges;
                    orderCustomerView.orderSummary.SpecialDiscounts = order.OrderDiscount;
                    orderCustomerView.orderSummary.TotalAmount = order.OrderSubtotal;
                    orderCustomerView.orderSummary.TaxTotal = order.Tax;
                    orderCustomerView.orderSummary.GrandTotal = order.OrderTotal;
                    orderCustomerView.orderSummary.TotalPayments = TotalPayments;
                    orderCustomerView.orderSummary.BalanceDue = order.BalanceDue;

                    return orderCustomerView;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public OrderCustomerView AcceptTerms(int orderId, bool isAccepted)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();

                    var fQuery = @"select * from crm.orderCustomerView where OrderId=@OrderId";
                    var orderCustomerView = connection.Query<OrderCustomerView>(fQuery, new { orderid = orderId }).FirstOrDefault();

                    if (orderCustomerView == null)
                    {
                        orderCustomerView = new OrderCustomerView();
                        orderCustomerView.OrderId = orderId;
                        orderCustomerView.TermsAccepted = isAccepted;
                        if (isAccepted == true)
                            orderCustomerView.TermsAcceptedDate = DateTime.UtcNow;
                        else
                            orderCustomerView.TermsAcceptedDate = null;
                        orderCustomerView.CreatedOn = DateTime.UtcNow;
                        orderCustomerView.CreatedBy = User.PersonId;
                        orderCustomerView.UpdatedOn = DateTime.UtcNow;
                        orderCustomerView.UpdatedBy = User.PersonId;
                        connection.Insert<OrderCustomerView>(orderCustomerView);
                        orderCustomerView = connection.Query<OrderCustomerView>(fQuery, new { orderid = orderId }).FirstOrDefault();
                    }
                    else
                    {
                        orderCustomerView.TermsAccepted = isAccepted;
                        if (isAccepted == true)
                            orderCustomerView.TermsAcceptedDate = DateTime.UtcNow;
                        else
                            orderCustomerView.TermsAcceptedDate = null;
                        orderCustomerView.UpdatedOn = DateTime.UtcNow;
                        orderCustomerView.UpdatedBy = User.PersonId;
                        connection.Update<OrderCustomerView>(orderCustomerView);
                    }

                    return orderCustomerView;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public Byte[] ImageToByte(System.Drawing.Bitmap bitmap)
        {
            using (var memoryStream = new System.IO.MemoryStream())
            {
                bitmap.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                return memoryStream.ToArray();
            }
        }

        public string UploadSign(string SignJson, string fileName)
        {
            var sigToImg = new SignatureToImage.SignatureToImage();
            var signatureImage = sigToImg.SigJsonToImage(SignJson);

            byte[] fileData = ImageToByte(signatureImage);

            var LeadMgr = new LeadsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            var streamId = LeadMgr.UploadFileTP(fileData, fileName);

            List<string> ImageExtensions = new List<string> { ".JPG", ".JPEG", ".BMP", ".GIF", ".PNG" };
            string extension = System.IO.Path.GetExtension(fileName).ToUpper();
            var streamId1 = "";
            if (ImageExtensions.Contains(extension))
            {
                System.Web.Helpers.WebImage img = new System.Web.Helpers.WebImage(fileData);
                img.Resize(50, 30);
                byte[] thump = img.GetBytes();
                string thumpfileName = DateTime.UtcNow.ToString("MMddyyyy") + "_thump_" + DateTime.UtcNow.ToString("HHMMssfff") + "_" + fileName;
                streamId1 = LeadMgr.UploadFileTP(thump, thumpfileName);
            }
            return streamId;
        }

        public OrderCustomerView UpdateSign(int orderId, OrderCustomerView model, string type)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();

                    string fileName = orderId + "_" + type + "_Sign.PNG";
                    var SignJson = "[]";
                    if (type == "Customer")
                        SignJson = model.CustomerSignJson;
                    else if (type == "SalesRep")
                        SignJson = model.SalesRepSignJson;

                    var streamId = UploadSign(SignJson, fileName);

                    model.CustomerSignStreamId = streamId;

                    var fQuery = @"select * from crm.orderCustomerView where OrderId=@OrderId";
                    var orderCustomerView = connection.Query<OrderCustomerView>(fQuery, new { orderid = orderId }).FirstOrDefault();

                    if (orderCustomerView != null)
                    {
                        if (type == "Customer")
                        {
                            orderCustomerView.CustomerSignStreamId = streamId;
                            orderCustomerView.CustomerSignJson = model.CustomerSignJson;
                            if (orderCustomerView.CustomerSignStreamId != null)
                                orderCustomerView.CustomerSignedDate = DateTime.UtcNow;
                            else
                                orderCustomerView.CustomerSignedDate = null;
                        }
                        else if (type == "SalesRep")
                        {
                            orderCustomerView.SalesRepSignStreamId = streamId;
                            orderCustomerView.SalesRepSignJson = model.SalesRepSignJson;
                            if (orderCustomerView.SalesRepSignStreamId != null)
                                orderCustomerView.SalesRepSignedDate = DateTime.UtcNow;
                            else
                                orderCustomerView.SalesRepSignedDate = null;
                        }

                        orderCustomerView.UpdatedOn = DateTime.UtcNow;
                        orderCustomerView.UpdatedBy = User.PersonId;
                        connection.Update<OrderCustomerView>(orderCustomerView);
                    }

                    return orderCustomerView;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool GetSignedOrNot(int orderId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();

                    var fQuery = @";WITH CTE AS
                                    (	
                                    select EnableElectronicSignAdmin as AdminEnabled,EnableElectronicSignFranchise as FranchiseEnabled
                                    from crm.Franchise where FranchiseId=@FranchiseId
                                    ), CTE2 AS
                                    (Select o.OrderId,case when cv.CustomerSignedDate is not null then 1 else 0 end as CustomerSigned,
                                    case when cv.SalesRepSignedDate is not null then 1 else 0 end as SalesRepSigned
                                    from crm.Orders o left join crm.OrderCustomerView cv on o.OrderID=cv.OrderId where o.OrderId=@orderId
                                    ) select case when AdminEnabled=1 and FranchiseEnabled=1
	                                    and (CustomerSigned=0 or SalesRepSigned=0) then 0 
	                                    else 1 end as isSigned
                                    from cte,cte2";
                    var isSigned = connection.Query<bool>(fQuery, new { orderId, FranchiseId = this.Franchise.FranchiseId}).SingleOrDefault();

                    if (isSigned != null)
                        return isSigned;
                    else
                        return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public OrderCustomerView getOrderCustomerViewTable(int orderId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    connection.Open();

                    var fQuery = @"select * from crm.OrderCustomerView where orderId=@orderId";
                    var data = connection.Query<OrderCustomerView>(fQuery, new { orderId }).FirstOrDefault();
                    return data;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }

        public dynamic getPhoneEmailOrder(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var result = connection.Query(@"select (select 
                             case when PreferredTFN='C' then CellPhone
                             else case when PreferredTFN='H' then HomePhone 
                             else case when PreferredTFN='W' then WorkPhone
                             else case when CellPhone is not null then CellPhone 
                             else case when HomePhone is not null then HomePhone 
                             else case when WorkPhone is not null then WorkPhone 
                             else '' 
                             end end end end end end) as CellPhone,c.PrimaryEmail
                             from  CRM.Orders o	
							  join CRM.Opportunities op on o.OpportunityId=op.OpportunityId
                             join CRM.Accounts ac on op.AccountId=ac.AccountId
                             join CRM.AccountCustomers acc on ac.AccountId=acc.AccountId and IsPrimaryCustomer=1
                             join CRM.Customer c on acc.CustomerId=c.CustomerId
							 where o.OrderID=@id",
                                     new { id = id }).FirstOrDefault();
                connection.Close();
                return result;
            }
        }
    }
}