﻿using Dapper;
using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Extensions;
using HFC.CRM.Core.Logs;
using HFC.CRM.Core.Membership;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using HFC.CRM.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace HFC.CRM.Managers
{
    public class ReportManager : DataManager
    {
        public ReportManager(User user, Franchise franchise)
        {
            this.User = user;
            this.Franchise = franchise;
        }

        public List<SalesAgentDTO> getSalesAgent(DateTime startdate, DateTime enddate)
        {
            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                return con.Query<SalesAgentDTO>("exec CRM.Report_SalesAgent @FranchiseId,@startdate,@enddate", new { FranchiseId = Franchise.FranchiseId, startdate, enddate }).ToList();
            }
        }

        public List<MarketingDTO> getMarketing(DateTime startdate, DateTime enddate)
        {
            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                return con.Query<MarketingDTO>("exec CRM.Report_Marketing @FranchiseId,@startdate,@enddate", new { FranchiseId = Franchise.FranchiseId, startdate, enddate }).ToList();
            }
        }

        public List<MonthlyStatisticsDTO> getMonthlyStatistics(DateTime startdate, DateTime enddate)
        {
            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                return con.Query<MonthlyStatisticsDTO>("exec CRM.Report_MonthlyStatistics @FranchiseId,@startdate,@enddate", new { FranchiseId = Franchise.FranchiseId, startdate, enddate }).ToList();
            }
        }

        public List<RAWDTO> getRAWData(DateTime startdate, DateTime enddate)
        {
            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                return con.Query<RAWDTO>("exec CRM.Report_RAW @FranchiseId,@startdate,@enddate", new { FranchiseId = Franchise.FranchiseId, startdate, enddate }).ToList();
            }
        }

        public dynamic getSalesandPurchasingDetail(ReportFilter data)
        {
            List<int> PersonCheck = ReportPersonCheck();
            if (PersonCheck == null || PersonCheck.Count == 0)
                throw new Exception(reportDataUnauthException);

            string personid = string.Join(",", PersonCheck);

            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                return con.Query(@"exec CRM.Report_SalesandPurchasingDetail @FranchiseId,@PersonId,
                    @VendorID,@IndustryId,@CommercialTypeId,@CustomerName,@StartDate,@EndDate",
                    new
                    {
                        FranchiseId = Franchise.FranchiseId,
                        PersonId = personid,
                        VendorID = data.VendorID,
                        IndustryId = data.IndustryId,
                        CommercialTypeId = data.CommercialTypeId,
                        CustomerName = data.CustomerName,
                        StartDate = data.StartDate,
                        EndDate = data.EndDate
                    }).ToList();
            }
        }

        public dynamic getFranchisePerformance_SalesAgent(ReportFilter data)
        {
            List<int> PersonCheck = ReportPersonCheck();
            if (PersonCheck == null || PersonCheck.Count == 0)
                throw new Exception(reportDataUnauthException);
            string personid = string.Join(",", PersonCheck);
            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                return con.Query(@"exec CRM.Report_FranchisePerformance_SalesAgent @FranchiseId,
                    @StartDate,@EndDate,@PersonId",
                new
                {
                    FranchiseId = Franchise.FranchiseId,
                    StartDate = data.StartDate,
                    EndDate = data.EndDate,
                    PersonId = personid
                }, commandTimeout: CommandTimeout).ToList();
            }
        }

        public dynamic getFranchisePerformance_Territory(ReportFilter data)
        {
            List<int> PersonCheck = ReportPersonCheck();
            if (PersonCheck == null || PersonCheck.Count == 0)
                throw new Exception(reportDataUnauthException);

            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                return con.Query(@"exec CRM.Report_FranchisePerformance_Territory @FranchiseId,
                    @StartDate,@EndDate",
                    new
                    {
                        FranchiseId = Franchise.FranchiseId,
                        StartDate = data.StartDate,
                        EndDate = data.EndDate
                    }, commandTimeout: CommandTimeout).ToList();
            }
        }

        public dynamic getFranchisePerformance_Franchise(ReportFilter data)
        {
            List<int> PersonCheck = ReportPersonCheck();
            if (PersonCheck == null || PersonCheck.Count == 0)
                throw new Exception(reportDataUnauthException);

            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                return con.Query(@"exec CRM.Report_FranchisePerformance_Franchise @FranchiseId,
                    @StartDate,@EndDate",
                    new
                    {
                        FranchiseId = Franchise.FranchiseId,
                        StartDate = data.StartDate,
                        EndDate = data.EndDate
                    }, commandTimeout: CommandTimeout).ToList();
            }
        }

        public dynamic getFranchisePerformance_Zip(ReportFilter data)
        {
            List<int> PersonCheck = ReportPersonCheck();
            if (PersonCheck == null || PersonCheck.Count == 0)
                throw new Exception(reportDataUnauthException);

            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                return con.Query(@"exec CRM.Report_FranchisePerformance_Zip @FranchiseId,
                    @StartDate,@EndDate",
                    new
                    {
                        FranchiseId = Franchise.FranchiseId,
                        StartDate = data.StartDate,
                        EndDate = data.EndDate
                    }, commandTimeout: CommandTimeout).ToList();
            }
        }

        public object GetUseTaxDetail(ReportFilter data)
        {
            List<int> PersonCheck = ReportPersonCheck();
            if (PersonCheck == null || PersonCheck.Count == 0)
                throw new Exception(reportDataUnauthException);

            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                return con.Query(@"exec [CRM].[Report_UseTax] 
                    @FranchiseID,@Year,@Month,@TerritoryID,@Zip",
                    new
                    {
                        FranchiseID = Franchise.FranchiseId,
                        Year = data.StartDate?.Year,
                        Month = data.StartDate?.Month,
                        TerritoryID = data.TerritoryId,
                        Zip = data.Zip
                    }).ToList();
            }
        }

        public dynamic getFranchisePerformance_Vendor(ReportFilter data)
        {
            List<int> PersonCheck = ReportPersonCheck();
            if (PersonCheck == null || PersonCheck.Count == 0)
                throw new Exception(reportDataUnauthException);

            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                return con.Query(@"exec CRM.Report_FranchisePerformance_Vendor @FranchiseId,
                    @StartDate,@EndDate",
                    new
                    {
                        FranchiseId = Franchise.FranchiseId,
                        StartDate = data.StartDate,
                        EndDate = data.EndDate
                    }, commandTimeout: CommandTimeout).ToList();
            }
        }

        public dynamic getFranchisePerformance_ProductCategory(ReportFilter data)
        {
            List<int> PersonCheck = ReportPersonCheck();
            if (PersonCheck == null || PersonCheck.Count == 0)
                throw new Exception(reportDataUnauthException);

            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                return con.Query(@"exec CRM.Report_FranchisePerformance_ProductCategory @FranchiseId,
                    @StartDate,@EndDate",
                    new
                    {
                        FranchiseId = Franchise.FranchiseId,
                        StartDate = data.StartDate,
                        EndDate = data.EndDate
                    }, commandTimeout: CommandTimeout).ToList();
            }
        }

        public dynamic getSalesSummaryByMonth(ReportFilter rdata)
        {
            List<int> PersonCheck = ReportPersonCheck();
            if (PersonCheck == null || PersonCheck.Count == 0)
                throw new Exception(reportDataUnauthException);

            // Passing Null value to SP when ALL is selected
            //if (rdata.PersonID == null && PersonCheck.Count == 1)
            //    rdata.PersonID = PersonCheck[0];

            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var Summarydata = con.QueryMultiple(@"exec CRM.Report_SalesSummaryByMonth @FranchiseId,@TerritoryId,@PersonId,@Zipcode",
                    new
                    {
                        FranchiseId = Franchise.FranchiseId,
                        TerritoryId = rdata.TerritoryId,
                        PersonId = rdata.PersonID,
                        Zipcode = rdata.Zip
                    });

                var ThisYearData = Summarydata.Read<SalesSummaryByMonth>().ToList();
                var LastYearData = Summarydata.Read<SalesSummaryByMonth>().ToList();
                var TwoYearOldData = Summarydata.Read<SalesSummaryByMonth>().ToList();
                var ThreeYearOldData = Summarydata.Read<SalesSummaryByMonth>().ToList();
                var FourYearOldData = Summarydata.Read<SalesSummaryByMonth>().ToList();

                dynamic obj = new ExpandoObject();
                obj.ThisYearData = ConvertToString(ThisYearData);
                obj.LastYearData = ConvertToString(LastYearData);
                obj.TwoYearOldData = ConvertToString(TwoYearOldData);
                obj.ThreeYearOldData = ConvertToString(ThreeYearOldData);
                obj.FourYearOldData = ConvertToString(FourYearOldData);
                return obj;
            }
        }

        public List<SalesSummaryByMonth_VM> ConvertToString(List<SalesSummaryByMonth> SalesSummarybyMonth)
        {
            List<SalesSummaryByMonth_VM> SalesSummaryByMonth_VM_Ty = new List<SalesSummaryByMonth_VM>();
            foreach (var data in SalesSummarybyMonth)
            {
                SalesSummaryByMonth_VM ss = new SalesSummaryByMonth_VM();

                if (data.rowno == 5)
                {
                    ss.Year = data.Year;
                    ss.ThisYear = data.ThisYear;
                    ss.rowno = data.Year;
                    ss.Jan = data.Jan.ToString() + "%";
                    ss.Feb = data.Feb.ToString() + "%";
                    ss.Mar = data.Mar.ToString() + "%";
                    ss.Apr = data.Apr.ToString() + "%";
                    ss.May = data.May.ToString() + "%";
                    ss.Jun = data.Jun.ToString() + "%";
                    ss.Jul = data.Jul.ToString() + "%";
                    ss.Aug = data.Aug.ToString() + "%";
                    ss.Sep = data.Sep.ToString() + "%";
                    ss.Oct = data.Oct.ToString() + "%";
                    ss.Nov = data.Nov.ToString() + "%";
                    ss.Dec = data.Dec.ToString() + "%";
                    ss.tot = data.tot.ToString() + "%";
                    SalesSummaryByMonth_VM_Ty.Add(ss);
                }
                else if (data.rowno == 6)
                {
                    ss.Year = data.Year;
                    ss.ThisYear = data.ThisYear;
                    ss.rowno = data.Year;
                    ss.Jan = Convert.ToInt32(data.Jan).ToString();
                    ss.Feb = Convert.ToInt32(data.Feb).ToString();
                    ss.Mar = Convert.ToInt32(data.Mar).ToString();
                    ss.Apr = Convert.ToInt32(data.Apr).ToString();
                    ss.May = Convert.ToInt32(data.May).ToString();
                    ss.Jun = Convert.ToInt32(data.Jun).ToString();
                    ss.Jul = Convert.ToInt32(data.Jul).ToString();
                    ss.Aug = Convert.ToInt32(data.Aug).ToString();
                    ss.Sep = Convert.ToInt32(data.Sep).ToString();
                    ss.Oct = Convert.ToInt32(data.Oct).ToString();
                    ss.Nov = Convert.ToInt32(data.Nov).ToString();
                    ss.Dec = Convert.ToInt32(data.Dec).ToString();
                    ss.tot = Convert.ToInt32(data.tot).ToString();
                    SalesSummaryByMonth_VM_Ty.Add(ss);
                }
                else
                {
                    ss.Year = data.Year;
                    ss.ThisYear = data.ThisYear;
                    ss.rowno = data.Year;
                    ss.Jan = data.Jan.ToString("C2");
                    ss.Feb = data.Feb.ToString("C2");
                    ss.Mar = data.Mar.ToString("C2");
                    ss.Apr = data.Apr.ToString("C2");
                    ss.May = data.May.ToString("C2");
                    ss.Jun = data.Jun.ToString("C2");
                    ss.Jul = data.Jul.ToString("C2");
                    ss.Aug = data.Aug.ToString("C2");
                    ss.Sep = data.Sep.ToString("C2");
                    ss.Oct = data.Oct.ToString("C2");
                    ss.Nov = data.Nov.ToString("C2");
                    ss.Dec = data.Dec.ToString("C2");
                    ss.tot = data.tot.ToString("C2");
                    SalesSummaryByMonth_VM_Ty.Add(ss);
                }
            }

            return SalesSummaryByMonth_VM_Ty;
        }

        public dynamic getVendorSalesPerformance(ReportFilter rfdata)
        {
            string personid = "";
            List<int> PersonCheck = ReportPersonCheck();
            if (PersonCheck == null || PersonCheck.Count == 0)
                throw new Exception(reportDataUnauthException);

            if (rfdata.PersonID != null && rfdata.PersonID > 0)
                personid = rfdata.PersonID.ToString();
            else
                // Pass Null value to SP when ALL is selected
                //personid = string.Join(",", PersonCheck);
                personid = null;

            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var VendorSalesdata = con.Query<dynamic>(@"exec CRM.Report_VendorSalesPerformance @FranchiseId,@PersonId
                    ,@VendorID,@StartDate,@EndDate,@ProductCategory",
                    new
                    {
                        FranchiseId = Franchise.FranchiseId,
                        PersonId = personid,
                        VendorID = rfdata.VendorID,
                        StartDate = rfdata.StartDate,
                        EndDate = rfdata.EndDate,
                        ProductCategory = rfdata.ProductCategory
                    }).ToList();

                // List<VendorSalesperformance_vm> lstVendorSalesperformance = new List<VendorSalesperformance_vm>();

                //foreach (var data in VendorSalesdata)
                //{
                //    VendorSalesperformance_vm vsp = new VendorSalesperformance_vm();
                //    //vsp.TotSale = data.TotSale.ToString("C2");
                //    vsp.VendorName = data.VendorName;
                //    vsp.COGS_Jan = data.COGS_Jan.ToString("C2");
                //    vsp.TotSale_Jan = data.TotSale_Jan.ToString("C2");
                //    vsp.GPValue_Jan = data.GPValue_Jan.ToString("C2");
                //    vsp.GPPerc_Jan = data.GPPerc_Jan.ToString("F") + "%";
                //    vsp.COGS_Feb = data.COGS_Feb.ToString("C2");
                //    vsp.TotSale_Feb = data.TotSale_Feb.ToString("C2");
                //    vsp.GPValue_Feb = data.GPValue_Feb.ToString("C2");
                //    vsp.GPPerc_Feb = data.GPPerc_Feb.ToString("F") + "%";
                //    vsp.COGS_Mar = data.COGS_Mar.ToString("C2");
                //    vsp.TotSale_Mar = data.TotSale_Mar.ToString("C2");
                //    vsp.GPValue_Mar = data.GPValue_Mar.ToString("C2");
                //    vsp.GPPerc_Mar = data.GPPerc_Mar.ToString("F") + "%";
                //    vsp.COGS_Apr = data.COGS_Apr.ToString("C2");
                //    vsp.TotSale_Apr = data.TotSale_Apr.ToString("C2");
                //    vsp.GPValue_Apr = data.GPValue_Apr.ToString("C2");
                //    vsp.GPPerc_Apr = data.GPPerc_Apr.ToString("F") + "%";
                //    vsp.COGS_May = data.COGS_May.ToString("C2");
                //    vsp.TotSale_May = data.TotSale_May.ToString("C2");
                //    vsp.GPValue_May = data.GPValue_May.ToString("C2");
                //    vsp.GPPerc_May = data.GPPerc_May.ToString("F") + "%";
                //    vsp.COGS_Jun = data.COGS_Jun.ToString("C2");
                //    vsp.TotSale_Jun = data.TotSale_Jun.ToString("C2");
                //    vsp.GPValue_Jun = data.GPValue_Jun.ToString("C2");
                //    vsp.GPPerc_Jun = data.GPPerc_Jun.ToString("F") + "%";
                //    vsp.COGS_Jul = data.COGS_Jul.ToString("C2");
                //    vsp.TotSale_Jul = data.TotSale_Jul.ToString("C2");
                //    vsp.GPValue_Jul = data.GPValue_Jul.ToString("C2");
                //    vsp.GPPerc_Jul = data.GPPerc_Jul.ToString("F") + "%";
                //    vsp.COGS_Aug = data.COGS_Aug.ToString("C2");
                //    vsp.TotSale_Aug = data.TotSale_Aug.ToString("C2");
                //    vsp.GPValue_Aug = data.GPValue_Aug.ToString("C2");
                //    vsp.GPPerc_Aug = data.GPPerc_Aug.ToString("F") + "%";
                //    vsp.COGS_Sep = data.COGS_Sep.ToString("C2");
                //    vsp.TotSale_Sep = data.TotSale_Sep.ToString("C2");
                //    vsp.GPValue_Sep = data.GPValue_Sep.ToString("C2");
                //    vsp.GPPerc_Sep = data.GPPerc_Sep.ToString("F") + "%";
                //    vsp.COGS_Oct = data.COGS_Oct.ToString("C2");
                //    vsp.TotSale_Oct = data.TotSale_Oct.ToString("C2");
                //    vsp.GPValue_Oct = data.GPValue_Oct.ToString("C2");
                //    vsp.GPPerc_Oct = data.GPPerc_Oct.ToString("F") + "%";
                //    vsp.COGS_Nov = data.COGS_Nov.ToString("C2");
                //    vsp.TotSale_Nov = data.TotSale_Nov.ToString("C2");
                //    vsp.GPValue_Nov = data.GPValue_Nov.ToString("C2");
                //    vsp.GPPerc_Nov = data.GPPerc_Nov.ToString("F") + "%";
                //    vsp.COGS_Dec = data.COGS_Dec.ToString("C2");
                //    vsp.TotSale_Dec = data.TotSale_Dec.ToString("C2");
                //    vsp.GPValue_Dec = data.GPValue_Dec.ToString("C2");
                //    vsp.GPPerc_Dec = data.GPPerc_Dec.ToString("F") + "%";
                //    lstVendorSalesperformance.Add(vsp);
                //}

                //return lstVendorSalesperformance;
                return VendorSalesdata;
            }
        }

        public dynamic GetPICProductGroupForConfigurator(string franchiseCode, int? vendorId = null)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                try
                {
                    var query = "";
                    var result = new List<HFCProducts>();
                    query = @"SELECT distinct hp.ProductGroup, hp.ProductGroupDesc
                          FROM crm.HFCProducts AS hp
                          inner join acct.HFCVendors hv on HP.Vendorid = hv.Vendorid
                          inner join acct.FranchiseVendors fv on fv.vendorid = hp.vendorid and fv.vendortype = 1 and fv.vendorstatus = 1 and hp.Active=1
						  INNER JOIN crm.Franchise f ON f.FranchiseId = fv.FranchiseId
						  where  f.Code = @franchiseCode AND isNULL(hv.IsActive,0)<>0 and hv.VendorId = coalesce(@VendorId,hv.VendorId)
						  group by hp.ProductKey, hp.ProductID, hp.ProductName, hp.PICProductID, hp.VendorID, hv.Name , hp.VendorProductSKU, hp.Description, hp.ProductCategory, hp.ProductCollection,
                          hp.ProductModelID,  hp.ProductGroup, hp.ProductGroupDesc,hv.PICVendorId,hp.DiscontinuedDate,hp.PhasedoutDate order by hp.ProductGroupDesc ASC;";

                    result = connection.Query<HFCProducts>(query,
                  new { franchiseCode = franchiseCode, VendorId = vendorId }).ToList();


                    var pvpListDynamic = new List<dynamic>();

                    var vendorsproductGroup = result; //result.Select(x => new { x.VendorID, x.ProductGroup, x.VendorName, x.ProductGroupDesc, x.PICVendorId }).Distinct().ToList();

                    foreach (var ven in vendorsproductGroup)
                    {
                        dynamic DyObj = new ExpandoObject();

                        //DyObj.PICVendorId = ven.PICVendorId;
                        // DyObj.VendorId = ven.VendorID;
                        // DyObj.VendorName = ven.VendorName;
                        DyObj.PICProductGroupId = ven.ProductGroup;
                        DyObj.PICProductGroupName = ven.ProductGroupDesc;
                        var dyprdlist = new List<dynamic>();

                        DyObj.PICProductList = dyprdlist;
                        pvpListDynamic.Add(DyObj);
                    }
                    return pvpListDynamic;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public dynamic getMarketingPerformance(ReportFilter data)
        {
            string personid = "";
            List<int> PersonCheck = ReportPersonCheck();
            if (PersonCheck == null || PersonCheck.Count == 0)
                throw new Exception(reportDataUnauthException);

            if (data.PersonID != null && data.PersonID > 0)
                personid = data.PersonID.ToString();
            else
                // Passing Null value to SP when ALL is selected
                //personid = string.Join(",", PersonCheck);
                personid = null;

            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                return con.Query(@"exec [CRM].[Report_MarketingPerformance] 
                    @FranchiseId,@SourceId,@campaignId,@CommercialTypeId,@IndustryId,@Zip,@PersonId
                    ,@StartDate,@EndDate",
                    new
                    {
                        FranchiseId = Franchise.FranchiseId,
                        SourceId = data.SourceId,
                        campaignId = data.campaignId,
                        CommercialTypeId = data.CommercialTypeId,
                        IndustryId = data.IndustryId,
                        Zip = data.Zip,
                        PersonId = personid,
                        StartDate = data.StartDate,
                        EndDate = data.EndDate
                    }, commandTimeout: CommandTimeout).ToList();
            }
        }

        public dynamic getSalesTax(ReportFilter data)
        {
            List<int> PersonCheck = ReportPersonCheck();
            if (PersonCheck == null || PersonCheck.Count == 0)
                throw new Exception(reportDataUnauthException);

            string personid = string.Join(",", PersonCheck);

            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                return con.Query(@"exec CRM.Report_SalesTax @FranchiseId,@PersonId,
                    @StartDate,@EndDate",
                    new
                    {
                        FranchiseId = Franchise.FranchiseId,
                        PersonId = personid,
                        StartDate = data.StartDate,
                        EndDate = data.EndDate
                    }).ToList();
            }
        }

        public dynamic getSalesTaxSummary(ReportFilter data)
        {
            List<int> PersonCheck = ReportPersonCheck();
            if (PersonCheck == null || PersonCheck.Count == 0)
                throw new Exception(reportDataUnauthException);

            string personid = string.Join(",", PersonCheck);

            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                return con.Query(@"exec CRM.Report_SalesTaxSummary @FranchiseId,@PersonId,
                    @StartDate,@EndDate,@ReportType",
                    new
                    {
                        FranchiseId = Franchise.FranchiseId,
                        PersonId = personid,
                        StartDate = data.StartDate,
                        EndDate = data.EndDate,
                        ReportType = data.ReportType
                    }).ToList();
            }
        }

        private List<int> ReportPersonCheck()
        {
            List<int> personid = new List<int>();

            var user = LocalMembership.GetUser(User.PersonId, "Person", "Roles", "OAuthUsers", "UserWidgets");
            if (user != null && user.IsInRole(AppConfigManager.FranchiseAdminRole.RoleId, AppConfigManager.DefaultOwnerRole.RoleId))
            {
                using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    personid = con.Query<int>("select PersonId from Auth.Users where FranchiseId=@FranchiseId", new { FranchiseId = Franchise.FranchiseId }).ToList();
                    return personid;
                }
            }
            else if (user != null && user.IsInRole(AppConfigManager.DefaultSalesRole.RoleId, AppConfigManager.DefaultSalesNoMargineRole.RoleId))
                personid.Add(user.PersonId);

            return personid;
        }

        public dynamic getSalesSummaryDetail(ReportFilter data)
        {
            string personid = "";
            List<int> PersonCheck = ReportPersonCheck();
            if (PersonCheck == null || PersonCheck.Count == 0)
                throw new Exception(reportDataUnauthException);

            if (data.PersonID != null && data.PersonID > 0)
                personid = data.PersonID.ToString();
            else
                // Pass Null value to SP when ALL is selected
                //personid = string.Join(",", PersonCheck);
                personid = null;

            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                return con.Query(@"exec CRM.Report_SalesSummary @FranchiseId,
                    @StartDate,@EndDate,@PersonId,@IndustryId,@CommercialTypeId,@CustomerName",
                    new
                    {
                        FranchiseId = Franchise.FranchiseId,
                        StartDate = data.StartDate,
                        EndDate = data.EndDate,
                        PersonId = personid,
                        IndustryId = data.IndustryId,
                        CommercialTypeId = data.CommercialTypeId,
                        CustomerName = data.CustomerName
                    }).ToList();
            }
        }

        public dynamic getPPCMatchBack(ReportFilter data)
        {

            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                return con.Query(@"exec CRM.Report_PPCMatchBack @FranchiseId,@SourceList,@PersonId,@IndustryId,
                    @CommercialTypeId,@CustomerName,@StartDate,@EndDate",
                    new
                    {
                        FranchiseId = Franchise.FranchiseId,
                        SourceList = data.SourceList,
                        PersonId = data.PersonID,
                        IndustryId = data.IndustryId,
                        CommercialTypeId = data.CommercialTypeId,
                        CustomerName = data.CustomerName,
                        StartDate = data.StartDate,
                        EndDate = data.EndDate
                    }).ToList();
            }
        }

        public dynamic getPaymentReport(ReportFilter data)
        {

            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                return con.Query(@"exec CRM.Report_Payments @FranchiseId,@StartDate,@EndDate",
                    new
                    {
                        FranchiseId = Franchise.FranchiseId,
                        StartDate = data.StartDate,
                        EndDate = data.EndDate
                    }).ToList();
            }
        }

        public dynamic getIncomeSummaryReport(ReportFilter data)
        {

            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                return con.Query(@"exec CRM.Report_IncomeSummary @FranchiseId,@Year",
                    new
                    {
                        FranchiseId = Franchise.FranchiseId,
                        Year = data.Year
                    }).ToList();
            }
        }

        public DataTable getRawDataDetailDT(ReportFilter data)
        {

            try
            {
                using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("CRM.Report_RawData", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = CommandTimeout;
                    cmd.Parameters.Add(new SqlParameter("@FranchiseId", Franchise.FranchiseId));
                    cmd.Parameters.Add(new SqlParameter("@StartDate", data.StartDate));
                    cmd.Parameters.Add(new SqlParameter("@EndDate", data.EndDate));
                    SqlDataAdapter sda = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    sda.Fill(dt);
                    con.Close();
                    return dt;

                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public dynamic getOpenArDetail(ReportFilter data)
        {
            string personid = "";
            List<int> PersonCheck = ReportPersonCheck();
            if (PersonCheck == null || PersonCheck.Count == 0)
                throw new Exception(reportDataUnauthException);

            if (data.PersonID != null && data.PersonID > 0)
                personid = data.PersonID.ToString();
            else
                // Passing Null value to SP when ALL is selected
                //personid = string.Join(",", PersonCheck);
                personid = null;

            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                return con.Query(@"exec [CRM].[Report_OpenBalance] @FranchiseId,
                                @PersonId,@IndustryId,@CommercialTypeId,@CustomerName,@StartDate,@EndDate",
                    new
                    {
                        FranchiseId = Franchise.FranchiseId,
                        PersonId = personid,
                        IndustryId = data.IndustryId,
                        CommercialTypeId = data.CommercialTypeId,
                        CustomerName = data.CustomerName,
                        StartDate = data.StartDate,
                        EndDate = data.EndDate
                    }).ToList();
            }
        }

        public dynamic GetOrdersPaidInFullDetail(ReportFilter data)
        {
            string PersonId = "";
            List<int> PersonCheck = ReportPersonCheck();
            if (PersonCheck == null || PersonCheck.Count == 0)
                throw new Exception(reportDataUnauthException);

            if (data.PersonID != null && data.PersonID > 0)
                PersonId = data.PersonID.ToString();
            else
                PersonId = string.Join(",", PersonCheck);

            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                return con.Query(@"exec [CRM].[Report_PaidOrders] 
                    @FranchiseId,@PersonID,@Year,@Mth",
                    new
                    {
                        FranchiseId = Franchise.FranchiseId,
                        PersonID = PersonId,
                        //Method = data.Value,
                        Year = data.StartDate?.Year,
                        Mth = data.StartDate?.Month
                    }).ToList();
            }
        }

        public dynamic getSalesJournalDetail(ReportFilter data)
        {
            string personid = "";
            List<int> PersonCheck = ReportPersonCheck();
            if (PersonCheck == null || PersonCheck.Count == 0)
                throw new Exception(reportDataUnauthException);

            if (data.PersonID != null && data.PersonID > 0)
                personid = data.PersonID.ToString();
            else
                personid = string.Join(",", PersonCheck);

            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                return con.Query(@"exec [CRM].[Report_SalesJournal] 
                    @FranchiseId,@PersonID,@Method,@Year,@Mth",
                    new
                    {
                        FranchiseId = Franchise.FranchiseId,
                        PersonID = personid,
                        Method = data.Value,
                        Year = data.StartDate?.Year,
                        Mth = data.StartDate?.Month
                    }).ToList();
            }
        }

        public string getMSRHTML(int TerritoryID, DateTime Mth)
        {
            try
            {
                List<MSR_DM> msrData = getMSRData(TerritoryID, Mth);
                string Path = string.Format("/Templates/Base/MSR_ReportTemplate.html");


                string srcFilePath = HostingEnvironment.MapPath(Path);
                string data = System.IO.File.ReadAllText(srcFilePath);

                //Show or hide brand logo on msr report header
                if (SessionManager.CurrentFranchise.BrandId == 3)
                {
                    data = data.Replace("{bb_logo}", "none");
                    data = data.Replace("{cc_logo}", "");
                    data = data.Replace("{tl_logo}", "none");
                }
                else if (SessionManager.CurrentFranchise.BrandId == 2)
                {
                    data = data.Replace("{bb_logo}", "none");
                    data = data.Replace("{cc_logo}", "none");
                    data = data.Replace("{tl_logo}", "");
                }
                else
                {
                    data = data.Replace("{bb_logo}", "");
                    data = data.Replace("{cc_logo}", "none");
                    data = data.Replace("{tl_logo}", "none");
                }


                if (msrData != null && msrData.Count > 0)
                {
                    var Product = msrData.Where(x => x.DisplaySort == 1).ToList();
                    if (Product != null && Product.Count > 0)
                    {
                        StringBuilder sbProduct = new StringBuilder();
                        sbProduct.Append("<table class='table table-bordered msr_table'>");
                        sbProduct.Append("<thead class='expand_lhead'>");
                        sbProduct.Append("<tr>");
                        sbProduct.Append("<th>CATEGORY</th>");
                        sbProduct.Append("<th class='msrborder'></th>");
                        sbProduct.Append("<th>COST</th>");
                        sbProduct.Append("<th>RETAIL</th>");
                        sbProduct.Append("</tr>");
                        sbProduct.Append("</thead>");
                        sbProduct.Append("<tbody>");

                        foreach (var p in Product)
                        {
                            sbProduct.Append("<tr>");
                            sbProduct.Append("<td class='msr_htext'>" + p.Category + "</td>");
                            sbProduct.Append("<td class='msrborder'></td>");
                            sbProduct.Append("<td>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", p.Cost) + "</td>");
                            sbProduct.Append("<td>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", p.Retail) + "</td>");
                            sbProduct.Append("</tr>");
                        }
                        sbProduct.Append("<tr>");
                        sbProduct.Append("<td></td>");
                        sbProduct.Append("<td class='msrptotal'>Product Total:</td>");
                        sbProduct.Append("<td>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", Product.Sum(x => x.Cost)) + "</td>");
                        sbProduct.Append("<td>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", Product.Sum(x => x.Retail)) + "</td>");
                        sbProduct.Append("</tr>");
                        sbProduct.Append("</tbody>");
                        sbProduct.Append("</table>");
                        data = data.Replace("{msrdataproduct}", sbProduct.ToString());
                    }
                    else
                        data = data.Replace("{msrdataproduct}", "");

                    StringBuilder sbtot = new StringBuilder();
                    sbtot.Append("<table class='table table-bordered msr_table'>");
                    sbtot.Append("<tbody>");

                    sbtot.Append("<tr>");
                    sbtot.Append("<td class='col_bold'>Surcharges</td>");
                    sbtot.Append("<td class='msrborder'></td>");
                    sbtot.Append("<td>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", msrData.Where(x => x.DisplaySort == 2).Select(x => x.Retail).FirstOrDefault()) + "</td>");
                    sbtot.Append("</tr>");

                    sbtot.Append("<tr>");
                    sbtot.Append("<td class='col_bold'>Discounts</td>");
                    sbtot.Append("<td class='msrborder'></td>");
                    sbtot.Append("<td>(" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", msrData.Where(x => x.DisplaySort == 3).Select(x => x.Retail).FirstOrDefault()) + ")</td>");
                    sbtot.Append("</tr>");

                    decimal AdjustmentTotal = msrData.Where(x => x.DisplaySort == 2).Select(x => x.Retail).FirstOrDefault() - msrData.Where(x => x.DisplaySort == 3).Select(x => x.Retail).FirstOrDefault();
                    sbtot.Append("<tr>");
                    sbtot.Append("<td class='msr_bhide'></td>");
                    sbtot.Append("<td class='msr_bbotright'>Adjustment Total:</td>");
                    sbtot.Append("<td class='msr_btopright'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", AdjustmentTotal) + "</td>");
                    sbtot.Append("</tr>");

                    decimal GrandTotal = Product.Sum(x => x.Retail) + AdjustmentTotal;
                    sbtot.Append("<tr>");
                    sbtot.Append("<td class='msr_bhide'></td>");
                    sbtot.Append("<td class='msrptotal'>Grand Total:</td>");
                    sbtot.Append("<td class='msr_retail'>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", GrandTotal) + "</td>");
                    sbtot.Append("</tr>");

                    decimal GrandTotalwGrayArea = GrandTotal + msrData.Where(x => x.DisplaySort == 4).Select(x => x.Retail).FirstOrDefault();
                    sbtot.Append("<tr>");
                    sbtot.Append("<td class='msr_bhide'></td>");
                    sbtot.Append("<td class='msrptotal'>Total w/Gray Area:</td>");
                    sbtot.Append("<td>" + string.Format(CultureInfo.GetCultureInfo(1033), "{0:C2}", GrandTotalwGrayArea) + "</td>");
                    sbtot.Append("</tr>");

                    sbtot.Append("</tbody>");
                    sbtot.Append("</table>");

                    data = data.Replace("{Headerinfo}", "Gross Sales for " + Mth.ToString("MMMM yyyy") + " | " + msrData.Select(x => x.Territory).FirstOrDefault() + "");
                    data = data.Replace("{msrdatatotal}", sbtot.ToString());
                    data = data.Replace("{royaltiesdue}", "");
                    data = data.Replace("{displaynote}", "block");
                }
                else
                {
                    data = data.Replace("{Headerinfo}", "No Sales for this time period");
                    data = data.Replace("{msrdatatotal}", "");
                    data = data.Replace("{msrdataproduct}", "");
                    data = data.Replace("{royaltiesdue}", "");
                    data = data.Replace("{displaynote}", "none");
                }
                data = data.Replace("{printdatetime}", TimeZoneManager.GetLocal().ToString("MM/dd/yyyy | hh:mm tt") + " (" + Franchise.TimezoneValue + ")");
                return data;
            }
            catch (Exception)
            {
                return "";
            }
        }

        public List<MSR_DM> getMSRData(int TerritoryID, DateTime Mth)
        {

            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                return con.Query<MSR_DM>(@"exec [CRM].[Report_FranchiseMSR] 
                    @TerritoryID,@Mth",
                    new
                    {
                        TerritoryID,
                        Mth
                    }).ToList();
            }
        }

        public dynamic GetAgingReport()
        {

            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                return con.Query(@"exec CRM.Report_Aging @FranchiseId",
                    new
                    {
                        FranchiseId = Franchise.FranchiseId
                    }).ToList();
            }
        }

        public int CommandTimeout
        {
            get
            {
                try
                {
                    var SQLCommandTimeout = AppConfigManager.GetConfig<string>("SQLCommandTimeout", "SQL", "Timeout");
                    int CommandTimeout = SQLCommandTimeout != null ? Convert.ToInt32(SQLCommandTimeout) : 300;
                    return CommandTimeout;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    return 300;
                }
            }
        }

    }
}
