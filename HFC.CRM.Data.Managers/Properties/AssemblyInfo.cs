﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("HFC.CRM")]
#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif
[assembly: AssemblyCompany("Home Franchise Concepts")]
[assembly: AssemblyProduct("HFC.CRM")]
[assembly: AssemblyCopyright("Copyright © Home Franchise Concepts 2013")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("63ed5f07-fe87-4868-9aff-251197c6535f")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]


[assembly: AssemblyInformationalVersion("Build date: 2014-07-21 12:58:46; Revision date: 2014-04-15 10:05:25; Revision(s) in working copy: 763.")]

[assembly: AssemblyVersion("1.17.0.7")]
[assembly: AssemblyFileVersion("1.17.0.7")]



