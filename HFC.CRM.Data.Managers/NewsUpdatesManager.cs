﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Core.Membership;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using HFC.CRM.Data.Geolocator;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Web;
using System.IO;
using HFC.CRM.DTO;

namespace HFC.CRM.Managers
{
    public class NewsUpdatesManager : DataManager<NewsUpdates>
    {
        public NewsUpdatesManager(User user, Franchise franchise) : this(user, user, franchise)
        {
        }

        public NewsUpdatesManager(User user, User authorizingUser, Franchise franchise)
        {
            base.User = user;
            base.AuthorizingUser = authorizingUser;
            base.Franchise = franchise;
        }

        public NewsUpdatesManager(User user)
        {
            base.User = user;
            base.AuthorizingUser = user;
        }

        public override ICollection<NewsUpdates> Get(List<int> idList)
        {
            throw new NotImplementedException();
        }

        public override NewsUpdates Get(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                
                var query = @"select * from Crm.NewsUpdates where 
                            NewsUpdatesId = @newsupdatesId";
                var model = connection.Query<NewsUpdates>(query, new { newsupdatesId = id }).FirstOrDefault();

                query = @"select b.*, IsApplicable = 
	                        case
		                        when n.BrandId is null Then 0
		                        else 1
	                        end
	                          from crm.Type_Brands b 
                        left join crm.NewsUpdatesBrands n 
                            on n.BrandId = b.id and n.NewsUpdatesId = @newsupdatesId";
                var brands = connection.Query<Type_Brands>(query, new { newsupdatesId = id }).ToList();
                model.Brands = brands;

                connection.Close();
                return model;
            }
        }

        public List<NewsUpdates> GetAllNews()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                //var query = @"select * from Crm.NewsUpdates
                //            order by sortorder";
                var query = @"select nu.*
                    ,case when nub1.BrandId is null then 0 else 1 end BB
                    ,case when nub2.BrandId is null then 0 else 1 end TL
                    ,case when nub3.BrandId is null then 0 else 1 end CC
                    from CRM.NewsUpdates nu
                    left join CRM.NewsUpdatesBrands nub1 on 
                        nu.NewsUpdatesId=nub1.NewsUpdatesId and nub1.BrandId=1
                    left join CRM.NewsUpdatesBrands nub2 on 
                        nu.NewsUpdatesId=nub2.NewsUpdatesId and nub2.BrandId=2
                    left join CRM.NewsUpdatesBrands nub3 on 
                        nu.NewsUpdatesId=nub3.NewsUpdatesId and nub3.BrandId=3
                    order by nu.SortOrder";

                var model = connection.Query<NewsUpdates>(query).ToList();
                connection.Close();
                return model;
            }
        }

        public List<NewsUpdates> GetCurrentNews(int brandId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                var query = @"select n.* from Crm.NewsUpdates n 
	                join crm.newsupdatesbrands b on b.NewsUpdatesId = n.NewsUpdatesId 
                    and b.BrandId = @brandid
	                where  (n.enddate is null or n.enddate >= CONVERT(date, getdate()))
	                and n.isactive = 1 
	                order by n.sortorder";
                var result = connection.Query<NewsUpdates>(query
                    , new { brandid = brandId} ).ToList();
                connection.Close();
                return result;
            }
        }

        public override string Add(NewsUpdates data)
        {
            throw new NotImplementedException();
        }

        public string CreateNews( out int newsUpdatesId, NewsUpdates model)
        {
            var query = @"insert into crm.newsupdates(
                        Headline, IsActive, StartDate, EndDate, 
                        ShortMessage, Message, SortOrder, CreatedOn, CreatedBy)
                    values ( @headline, 1, @startdate, @enddate,
                        @shortMessage, @message, (select max(NewsUpdatesId ) + 1 from crm.newsupdates),
                        @createdon, @createdby);
                        select cast(scope_identity() as int);";
            
            using (SqlConnection connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                newsUpdatesId = connection.Query<int>(query, new
                {
                    headline = model.Headline,
                    startdate = model.StartDate,
                    enddate = model.EndDate,
                    shortMessage = model.ShortMessage,
                    message = model.Message,
                    createdon = model.CreatedOn,
                    createdby = model.CreatedBy
                }).Single();

                model.NewsUpdatesId = newsUpdatesId;
                HandleBrandAssociation(model, connection);
            }

            return Success;
        }

        public override string Update(NewsUpdates data)
        {
            string Query = "";
            int? nullable;
            CommandType? nullable1;
            
                Query = @"UPDATE CRM.NewsUpdates SET 
                    Headline=@headLine, IsActive=@isActive
                    , StartDate=@startDate, EndDate=@endDate
                    , ShortMessage = @shortMessage
                    , Message = @message, SortOrder=@sortOrder
                    , lastUpdatedOn=@updatedOn, lastUpdatedBy=@updatedBy 
                    where NewsUpdatesId=@newsupdatesId";
            
            using (SqlConnection connectionUpdate = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                nullable = null;
                nullable1 = null;
                connectionUpdate.Execute(Query, new {
                    headLine = data.Headline,
                    isActive = data.IsActive,
                    startDate = data.StartDate,
                    endDate = data.EndDate,
                    shortMessage = data.ShortMessage,
                    message = data.Message,
                    sortOrder = data.SortOrder,
                    updatedOn = data.LastUpdatedOn,
                    updatedBy = data.LastUpdatedBy,
                    NewsUpdatesId = data.NewsUpdatesId
                }, null, nullable, nullable1);

                HandleBrandAssociation(data, connectionUpdate);
                
            }

            return Success;
        }

        private void HandleBrandAssociation(NewsUpdates data, SqlConnection connection)
        {
            var Query = @"delete from crm.NewsUpdatesBrands where NewsUpdatesId = @newsupdatesId";
            connection.Execute(Query, new { NewsUpdatesId = data.NewsUpdatesId });

            Query = @"insert into crm.NewsUpdatesbrands(newsupdatesid, brandid) 
                    values(@newsupdatesId, @brandId)";

            var temp = data.Brands.Where(x => x.IsApplicable == true).ToList();
            foreach (var item in temp)
            {
                connection.Execute(Query
                    , new
                    {
                        NewsUpdatesId = data.NewsUpdatesId,
                        brandId = item.Id
                    });

            }
        }

        public string MoveUp(int newsId)
        {
            var querya = "select * from crm.newsupdates where NewsUpdatesId = @newsupdatesId";
            var queryb = @"select top(1) * from crm.newsupdates where SortOrder <= (
                        select sortorder - 1 from  crm.newsupdates where NewsUpdatesId = @newsupdatesId)
                        and IsActive = 1 order by SortOrder desc";

            using (SqlConnection connectionUpdate = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var source = connectionUpdate.Query<NewsUpdates>(
                    querya, new { newsupdatesId = newsId }).FirstOrDefault();
                var destination = connectionUpdate.Query<NewsUpdates>(
                    queryb, new { newsupdatesId = newsId }).FirstOrDefault();

                if (destination == null)
                {
                    return Success;
                    //throw new Exception("Destination news not found to move up or down");
                }

                var tempSortOrder = destination.SortOrder;

                querya = @"update crm.newsupdates 
                            set SortOrder = @sortOrder where NewsUpdatesId = @newsupdatesId";
                queryb = @"update crm.newsupdates 
                            set SortOrder = @sortOrder where NewsUpdatesId = @newsupdatesId";

                connectionUpdate.Execute(querya
                    , new
                    {
                        newsupdatesId = newsId,
                        sortOrder = destination.SortOrder
                    });

                connectionUpdate.Execute(queryb
                , new
                {
                    newsupdatesId = destination.NewsUpdatesId,
                    sortOrder = source.SortOrder
                });
            }

            return Success;
        }

        public string MoveDown(int newsId)
        {
            var querya = "select * from crm.newsupdates where NewsUpdatesId = @newsupdatesId";
            var queryb = @"select top(1) * from crm.newsupdates where SortOrder >= (
                        select sortorder + 1 from  crm.newsupdates where NewsUpdatesId = @newsupdatesId
                        and IsActive = 1) order by SortOrder asc";

            using (SqlConnection connectionUpdate = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var source = connectionUpdate.Query<NewsUpdates>(
                    querya, new { newsupdatesId = newsId }).FirstOrDefault();
                var destination = connectionUpdate.Query<NewsUpdates>(
                    queryb, new { newsupdatesId = newsId }).FirstOrDefault();

                if (destination == null)
                {
                    return Success;
                    //throw new Exception("Destination news not found to move up or down");
                }

                var tempSortOrder = destination.SortOrder;

                querya = @"update crm.newsupdates 
                            set SortOrder = @sortOrder where NewsUpdatesId = @newsupdatesId";
                queryb = @"update crm.newsupdates 
                            set SortOrder = @sortOrder where NewsUpdatesId = @newsupdatesId";

                connectionUpdate.Execute(querya
                    , new
                    {
                        newsupdatesId = newsId,
                        sortOrder = destination.SortOrder
                    });

                connectionUpdate.Execute(queryb
                , new
                {
                    newsupdatesId = destination.NewsUpdatesId,
                    sortOrder = source.SortOrder
                });
            }

            return Success;
        }

        public bool ChangeSorting(int newsId, int move)
        {

            string Query = @"Begin                            
                            Declare @OldSortOrder int
                            select @OldSortOrder=SortOrder from CRM.NewsUpdates where NewsUpdatesId=@NewsUpdatesId
                            	if(@Move>0)
                            	Begin
                            	update CRM.NewsUpdates set SortOrder=SortOrder-1 where SortOrder>=@OldSortOrder and SortOrder<= (@OldSortOrder+@Move) and NewsUpdatesId!=@NewsUpdatesId
                            	update CRM.NewsUpdates set SortOrder=@OldSortOrder+@Move where NewsUpdatesId=@NewsUpdatesId
                            	end
                            	if(@Move<0)
                            	Begin
                            	update CRM.NewsUpdates set SortOrder=SortOrder+1 where SortOrder>=(@OldSortOrder+@Move) and SortOrder<=@OldSortOrder and NewsUpdatesId!=@NewsUpdatesId
                            	update CRM.NewsUpdates set SortOrder=@OldSortOrder+@Move where NewsUpdatesId=@NewsUpdatesId
                            	end
                            end";

            using (SqlConnection con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                con.Execute(Query, new { NewsUpdatesId = newsId, Move = move });
            }

            return true;
        }

        public  string Activate(int newsId)
        {
            string Query = "";
            int? nullable;
            CommandType? nullable1;

            Query = @"UPDATE CRM.NewsUpdates SET 
                    IsActive=@isActive
                    , lastUpdatedOn=@updatedOn, lastUpdatedBy=@updatedBy 
                    where NewsUpdatesId=@newsupdatesId";

            using (SqlConnection connectionUpdate = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                nullable = null;
                nullable1 = null;
                var news = new NewsUpdates();
                connectionUpdate.Execute(Query, new
                {
                    isActive = 1,
                    updatedOn = news.LastUpdatedOn,
                    updatedBy = SessionManager.CurrentUser.PersonId,
                    NewsUpdatesId = newsId
                }, null, nullable, nullable1);
            }

            return Success;
        }

        public string Deactivate(int newsId)
        {
            string Query = "";
            int? nullable;
            CommandType? nullable1;

            Query = @"UPDATE CRM.NewsUpdates SET 
                    IsActive=@isActive
                    , lastUpdatedOn=@updatedOn, lastUpdatedBy=@updatedBy 
                    where NewsUpdatesId=@newsupdatesId";

            using (SqlConnection connectionUpdate = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                nullable = null;
                nullable1 = null;
                var news = new NewsUpdates();
                connectionUpdate.Execute(Query, new
                {
                    isActive = 0,
                    updatedOn = news.LastUpdatedOn,
                    updatedBy = SessionManager.CurrentUser.PersonId,
                    NewsUpdatesId = newsId
                }, null, nullable, nullable1);
            }

            return Success;
        }

        public override string Delete(int id)
        {
            throw new NotImplementedException();
        }
       
    }
}


