﻿using Dapper;
using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Http;

namespace HFC.CRM.Managers
{
    public class CaseCommentsManager : DataManager
    {
        public CaseCommentsManager()
        {
        }
        public dynamic loadInitialComments(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var res = new List<CaseComments>();
                res = connection.Query<CaseComments>(@"Select C.*,P.FirstName + ' ' + LastName as UserName from CRM.CaseComments C 
                                                     Inner Join CRM.Person P on P.PersonId = C.PersonId  where CaseId=@CaseId and DeletedOn is null", new { CaseId = id }).ToList();
                foreach (var cm in res)
                {
                    cm.CreatedOn = TimeZoneManager.ToLocal(cm.CreatedOn);
                    cm.LastUpdatedOn = TimeZoneManager.ToLocal(cm.LastUpdatedOn);
                }
                connection.Close();
                return res;
            }
        }

        public dynamic loadUserlist(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var res = connection.Query("select u.UserName name , p.PrimaryEmail content from Auth.Users u Inner join CRM.Person p on p.PersonId = u.PersonId where u.Domain = 'bbi.corp' and u.FranchiseId = ( select FranchiseId from Auth.Users where PersonId in (select createdBy from CRM.[Case] where CaseId = @CaseId))", new { CaseId = id }).ToList();
                connection.Close();
                return res;
            }
        }

        public string FindFranchiseTimeZone(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var TimezoneCode = connection.Query<int>(@"select f.TimezoneCode
                                                    from CRM.[Case] fc 
                                                    join Auth.Users u on u.PersonId = fc.CreatedBy
                                                    join CRM.Franchise f on f.FranchiseId = u.FranchiseId
                                                    where fc.CaseId =@CaseId", new { CaseId = id }).FirstOrDefault();
                connection.Close();
                if (TimezoneCode > 0)
                    return ((TimeZoneEnum)TimezoneCode).ToString();
                else
                    return "";
            }
        }

        public dynamic SaveParentComment(CaseComments Comments)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = "";
                query = @"insert into CRM.CaseComments (CaseId, PersonId, ParentId, HasChildId, htmlComment, IsReply, IsEdit,CreatedOn, LastUpdatedOn, BelongsTo) values (@CaseId, @PersonId, @ParentId, @HaschildId, @htmlComment, @IsReply, @IsEdit, @CreatedOn, @LastUpdatedOn, @BelongsTo )";
                int PersonId = 0;

                if (SessionManager.CurrentUser.Person != null)
                {
                    PersonId = SessionManager.CurrentUser.Person.PersonId;
                }
                else if (SessionManager.SecurityUser.Person != null)
                {
                    PersonId = SessionManager.SecurityUser.Person.PersonId;
                }

                var rrr = connection.Execute(query, new
                {
                    CaseId = Comments.CaseId,
                    PersonId = PersonId,
                    ParentId = Comments.ParentId,
                    HaschildId = Comments.HasChildId,
                    HtmlComment = Comments.HTMLComment,
                    HelongsTo = Comments.BelongsTo,
                    IsReply = Comments.IsReply,
                    IsEdit = Comments.IsEdit,
                    CreatedOn = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc),
                    LastUpdatedOn = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc),
                    BelongsTo = Comments.BelongsTo
                });


                CaseManager ca = new CaseManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
                CaseFeedbackDetails details = new CaseFeedbackDetails();
                var res = connection.Query<CaseComments>(@"Select C.*,P.FirstName + ' ' + LastName as UserName from CRM.CaseComments C Inner Join CRM.Person P on P.PersonId=C.PersonId 
                            where C.Id=(select max(id) from CRM.CaseComments where DeletedOn is null and CaseId = @CaseId)", new { CaseId = Comments.CaseId }).ToList();

                res[0].CreatedOn = TimeZoneManager.ToLocal(res[0].CreatedOn);
                res[0].LastUpdatedOn = TimeZoneManager.ToLocal(res[0].LastUpdatedOn);
                connection.Close();
                return res[0];
            }
        }

        public dynamic updateParentComment(int id, CaseComments Comments)
        {

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                DateTime dt = DateTime.UtcNow;
                connection.Open();
                connection.Execute("update CRM.CaseComments set HTMLComment=@HTMLComment,LastUpdatedOn=@LastUpdatedOn where id=@id", new { htmlComment = Comments.HTMLComment, id = id, LastUpdatedOn = dt });
                connection.Close();

                var rr = TimeZoneManager.ToLocal(dt);
                return rr;
            }
        }

        public bool deleteComment(int id, int parentId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                connection.Execute("update CRM.CaseComments set LastUpdatedOn=@LastUpdatedOn,DeletedOn=@DeletedOn where id=@id", new { id = id, LastUpdatedOn = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc), DeletedOn = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc) });
                var res = connection.Query<CaseComments>(@"Select C.*,P.FirstName + ' ' + LastName as UserName from CRM.CaseComments C 
	                                                                Inner Join CRM.Person P on P.PersonId=C.PersonId 
                                                                        where C.parentId=@parentId and C.DeletedOn is null", new { parentId = parentId }).ToList();
                if (!(res.Count > 0))
                {
                    connection.Execute("update CRM.CaseComments set haschildId=@haschildId,LastUpdatedOn=@LastUpdatedOn where id=@id", new { haschildId = false, id = parentId, LastUpdatedOn = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc) });
                }
                connection.Close();
            }
            return true;
        }

        public dynamic SaveReplyComment(CaseComments Comments)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = "";
                query = @"insert into CRM.CaseComments (CaseId, PersonId, ParentId, HasChildId, htmlComment, IsReply, IsEdit,CreatedOn, LastUpdatedOn, BelongsTo) values (@CaseId, @PersonId, @ParentId, @HaschildId, @htmlComment, @IsReply, @IsEdit, @CreatedOn, @LastUpdatedOn, @BelongsTo )";
                int PersonId = 0;

                if (SessionManager.CurrentUser.Person != null)
                {
                    PersonId = SessionManager.CurrentUser.Person.PersonId;
                }
                else if (SessionManager.SecurityUser.Person != null)
                {
                    PersonId = SessionManager.SecurityUser.Person.PersonId;
                }

                var rrr = connection.Execute(query, new
                {
                    CaseId = Comments.CaseId,
                    PersonId = PersonId,
                    ParentId = Comments.ParentId,
                    HaschildId = Comments.HasChildId,
                    HtmlComment = Comments.HTMLComment,
                    BelongsTo = Comments.BelongsTo,
                    IsReply = Comments.IsReply,
                    IsEdit = Comments.IsEdit,
                    CreatedOn = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc),
                    LastUpdatedOn = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc),
                });

                connection.Execute("Update CRM.CaseComments set haschildId=@haschildId where id=@id", new { haschildId = true, id = Comments.ParentId });

                var res = new List<CaseComments>();

                res = connection.Query<CaseComments>(@"Select C.*,P.FirstName + ' ' + LastName as UserName from CRM.CaseComments C 
	                                                            Inner Join CRM.Person P on P.PersonId=C.PersonId 
                                                                        where id=(select max(id) from CRM.CaseComments where DeletedOn is null and CaseId = @CaseId and PersonId=@PersonId)", new { CaseId = Comments.CaseId, PersonId }).ToList();

                var parentcomment = connection.Query<CaseComments>(@"Select C.*,P.FirstName + ' ' + LastName as UserName from CRM.CaseComments C 
	                                                                        Inner Join CRM.Person P on P.PersonId=C.PersonId 
                                                                                    where C.id= @id", new { id = Comments.ParentId }).ToList();
                var ress = connection.Query<Person>("Select * from CRM.Person where PersonId= @id", new { id = parentcomment[0].PersonId }).ToList();
                connection.Close();
                res[0].CreatedOn = TimeZoneManager.ToLocal(res[0].CreatedOn);
                res[0].LastUpdatedOn = TimeZoneManager.ToLocal(res[0].LastUpdatedOn);

                return res[0];
            }
        }
    }
}
