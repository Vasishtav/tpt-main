﻿using Dapper;
using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Extensions;
using HFC.CRM.Core.Membership;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using HFC.CRM.DTO;

namespace HFC.CRM.Managers
{
    public class CaseAddEditManager : DataManager<FranchiseCaseModel>
    {
        public CaseAddEditManager(Franchise franchise, User user)
        {
            Franchise = franchise;
            User = user;
        }

        public FranchiseCaseModel UserDetails(int id)
        {
            FranchiseCaseModel Result_set = new FranchiseCaseModel();
            {

                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    if (id == 0)
                    {
                        Result_set.Owner_PersonId = SessionManager.CurrentUser.PersonId;
                        if (Result_set.Owner_PersonId != 0)
                        {
                            var Query = "";

                            Query = @"select FirstName +' '+ LastName as Name from CRM.Person Where PersonId=@PersonId";

                            var Value = connection.Query<FranchiseCaseModel>(Query, new { PersonId = Result_set.Owner_PersonId }).FirstOrDefault();
                            Result_set.Name = Value.Name;
                            Result_set.CreatedOn = TimeZoneManager.ToLocal(Result_set.CreatedOn);
                            Result_set.IncidentDate = Result_set.CreatedOn;
                        }
                    }
                    else
                    {

                        var Query = "";

                        Query = @"select FirstName +''+ LastName as Name from CRM.Person Where PersonId=@PersonId";

                        var Value = connection.Query<FranchiseCaseModel>(Query, new { PersonId = id }).FirstOrDefault();
                        Result_set.Name = Value.Name;
                        Result_set.CreatedOn = TimeZoneManager.ToLocal(Result_set.CreatedOn);
                        Result_set.IncidentDate = Result_set.CreatedOn;
                    }
                }

                return (Result_set);
            }
        }

        public FranchiseCaseDropModel GetValue(int value, string orderIds)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {

                {
                    //if (orderIds == null)
                    //    orderIds = "";
                    //int[] ia = orderIds.Split(',').Select(n => Convert.ToInt32(n)).ToArray();
                    var Query = "";
                    // c.FirstName+''+c.LastName as AccountName,
                    if (orderIds == null) //(ia.Length == 0)
                    {
                        Query = @"select mpo.PurchaseOrderId,mpo.MasterPONumber as MPO_MasterPONum_POId,o.OrderId,o.OrderNumber as SalesOrderId 
                             ,ac.AccountId,acc.CustomerId , (select [CRM].[fnGetAccountNameByAccountId](ac.AccountId)) as AccountName ,
                             (select 
                                                             case when PreferredTFN='C' then CellPhone
                                                             else case when PreferredTFN='H' then HomePhone 
                                                             else case when PreferredTFN='W' then WorkPhone
                                                             else case when CellPhone is not null then CellPhone 
                                                             else case when HomePhone is not null then HomePhone 
                                                             else case when WorkPhone is not null then WorkPhone 
                                                             else '' 
                                                             end end end end end end) as CellPhone,c.PrimaryEmail,q.SideMark
                             from CRM.MasterPurchaseOrder mpo
                             Join crm.MasterPurchaseOrderExtn mpox on mpox.purchaseorderid = mpo.purchaseorderid
                             join CRM.Opportunities op on mpox.OpportunityId=op.OpportunityId
                             join CRM.Orders o on mpox.OrderId=o.OrderID
                             join CRM.Accounts ac on mpox.AccountId=ac.AccountId
                             join CRM.AccountCustomers acc on ac.AccountId=acc.AccountId
                             join CRM.Customer c on acc.CustomerId=c.CustomerId
                             join CRM.Quote q on o.QuoteKey=q.QuoteKey
                             join CRM.PurchaseOrderDetails po on mpo.PurchaseOrderId=po.PurchaseOrderId
                             where op.FranchiseId=@FranchiseId and po.PurchaseOrdersDetailId=@PurchaseOrderId";

                        var Value = connection.Query<FranchiseCaseDropModel>(Query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId, PurchaseOrderId = value }).ToList();

                        var ret_Obj = new FranchiseCaseDropModel();
                        if (Value.Count > 0)
                            ret_Obj = Value[0];

                        List<int> intlist = new List<int>();

                        foreach (var fdm in Value)
                        {
                            intlist.Add(fdm.OrderId);
                        }
                        ret_Obj.Orderlist = intlist;

                        ret_Obj.noOfLines = this.getLineNos(null, value, Value[0].PurchaseOrderId.ToString());
                        return ret_Obj;
                    }
                    else
                    {
                        var ret_Obj = new FranchiseCaseDropModel();
                        var result = connection.Query<PurchaseOrderDetails>(@"select top 1 * from CRM.PurchaseOrderDetails where PurchaseOrdersDetailId=@PurchaseOrdersDetailId",
                            new { PurchaseOrdersDetailId = value }).FirstOrDefault();
                        ret_Obj.noOfLines = this.getLineNos(orderIds.Split(',').Select(n => Convert.ToInt32(n)).ToArray(), value, result.PurchaseOrderId.ToString());
                        return ret_Obj;

                    }
                }
            }

        }



        public FranchiseCaseDropModel getDataForNonVPO(int value, int Mpo)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {

                {
                    var Query = "";
                    // c.FirstName+''+c.LastName as AccountName,
                    Query = @"select mpo.PurchaseOrderId,mpo.MasterPONumber as MPO_MasterPONum_POId,o.OrderId,o.OrderNumber as SalesOrderId 
                             ,ac.AccountId,acc.CustomerId , (select [CRM].[fnGetAccountNameByAccountId](ac.AccountId)) as AccountName ,
                             (select 
                                                             case when PreferredTFN='C' then CellPhone
                                                             else case when PreferredTFN='H' then HomePhone 
                                                             else case when PreferredTFN='W' then WorkPhone
                                                             else case when CellPhone is not null then CellPhone 
                                                             else case when HomePhone is not null then HomePhone 
                                                             else case when WorkPhone is not null then WorkPhone 
                                                             else '' 
                                                             end end end end end end) as CellPhone,c.PrimaryEmail,q.SideMark
                             from CRM.MasterPurchaseOrder mpo
                                join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                             join CRM.Opportunities op on mpox.OpportunityId=op.OpportunityId
                             join CRM.Orders o on mpox.OrderId=o.OrderID
                             join CRM.Accounts ac on mpox.AccountId=ac.AccountId
                             join CRM.AccountCustomers acc on ac.AccountId=acc.AccountId
                             join CRM.Customer c on acc.CustomerId=c.CustomerId
                             join CRM.Quote q on o.QuoteKey=q.QuoteKey
                             join CRM.PurchaseOrderDetails po on mpo.PurchaseOrderId=po.PurchaseOrderId
                             where op.FranchiseId=@FranchiseId and po.PurchaseOrderId=@Mpo";

                    var Value = connection.Query<FranchiseCaseDropModel>(Query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId, Mpo = Mpo }).FirstOrDefault();

                    var quoteline = connection.Query<QuoteLines>(@"Select  ql.* from CRM.PurchaseOrderDetails pod 
                            join CRM.MasterPurchaseOrder mpo on pod.PurchaseOrderId=mpo.PurchaseOrderId
                            Left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                            join CRM.Orders o on mpox.OrderId=o.OrderID
                            join CRM.Quote q on o.QuoteKey=q.QuoteKey 
                            join CRM.QuoteLines ql on q.QuoteKey=ql.QuoteKey and pod.QuoteLineId=ql.QuoteLineId
                            where mpo.PurchaseOrderId = @PurchaseOrderId and ql.QuoteLineNumber=@QuoteLineNumber",
                            new { PurchaseOrderId = Value.PurchaseOrderId, QuoteLineNumber = value }).FirstOrDefault();

                    if (quoteline.VendorId != null && quoteline.VendorId != 0)
                    {
                        var count = connection.Query<QuoteLines>(@"select * from CRM.QuoteLines where QuoteKey=@QuoteKey and VendorId = @VendorId",
                            new { QuoteKey = quoteline.QuoteKey, VendorId = quoteline.VendorId }).ToList();
                        Value.noOfLines = count.Count;

                    }
                    else
                    {
                        Value.noOfLines = 1;

                    }

                    // Value.noOfLines = this.getLineNos(Value.PurchaseOrdersDetailId, Value.PurchaseOrderId.ToString());
                    return Value;
                }
            }

        }



        public FranchiseCaseDropModel GetMpoValue(int value, int lineNumber, string orderStr, int poid, int podid)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {

                {
                    // if (orderStr.Split(',').Select(n => Convert.ToInt32(n)).ToArray().Length > 0)
                    int len = orderStr.Split(',').Select(n => Convert.ToInt32(n)).ToArray().Length;

                    int[] orderarray = orderStr.Split(',').Select(n => Convert.ToInt32(n)).ToArray();

                    var Query = "";
                    // c.FirstName+''+c.LastName as AccountName,
                    //Query = @"select mpo.PurchaseOrderId,mpo.MasterPONumber as MPO_MasterPONum_POId, o.OrderId,o.OrderNumber as SalesOrderId 
                    //          ,ac.AccountId,acc.CustomerId ,(select [CRM].[fnGetAccountNameByAccountId](ac.AccountId)) as AccountName,
                    //          (select 
                    //                                         case when PreferredTFN='C' then CellPhone
                    //                                         else case when PreferredTFN='H' then HomePhone 
                    //                                         else case when PreferredTFN='W' then WorkPhone
                    //                                         else case when CellPhone is not null then CellPhone 
                    //                                         else case when HomePhone is not null then HomePhone 
                    //                                         else case when WorkPhone is not null then WorkPhone 
                    //                                         else '' 
                    //                                         end end end end end end) as CellPhone,c.PrimaryEmail,
                    //          q.SideMark  
                    //          from CRM.Orders o
                    //          Left join crm.MasterPurchaseOrderExtn mpox on mpox.OrderId = o.OrderId
                    //          join CRM.MasterPurchaseOrder mpo on mpo.PurchaseOrderId=mpox.PurchaseOrderId
                    //          join CRM.Opportunities op on mpox.OpportunityId=op.OpportunityId
                    //          join CRM.Accounts ac on mpox.AccountId=ac.AccountId
                    //          join CRM.AccountCustomers acc on ac.AccountId=acc.AccountId
                    //          join CRM.Customer c on acc.CustomerId=c.CustomerId
                    //          join CRM.Quote q on o.QuoteKey=q.QuoteKey
                    //          where op.FranchiseId=@FranchiseId and o.OrderID=@OrderNumber";

                    var Value = new FranchiseCaseDropModel();
                    if (len == 1) // for only one order
                    {
                        if (lineNumber > 0)
                            Query = @"select isnull(mpo.PurchaseOrderId, 0) as PurchaseOrderId,isnull(mpo.MasterPONumber,0) as MPO_MasterPONum_POId,  o.OrderId,o.OrderNumber as SalesOrderId 
                              ,ac.AccountId,acc.CustomerId ,(select [CRM].[fnGetAccountNameByAccountId](ac.AccountId)) as AccountName,
                              (select 
                                                             case when PreferredTFN='C' then CellPhone
                                                             else case when PreferredTFN='H' then HomePhone 
                                                             else case when PreferredTFN='W' then WorkPhone
                                                             else case when CellPhone is not null then CellPhone 
                                                             else case when HomePhone is not null then HomePhone 
                                                             else case when WorkPhone is not null then WorkPhone 
                                                             else '' 
                                                             end end end end end end) as CellPhone,c.PrimaryEmail,
                              q.SideMark  , ql.QuoteLineId,ql.QuoteLineNumber
                              from CRM.Orders o                                     
                              join CRM.Opportunities op on o.OpportunityId=op.OpportunityId
                              join CRM.Accounts ac on op.AccountId=ac.AccountId
                              join CRM.AccountCustomers acc on ac.AccountId=acc.AccountId
                              join CRM.Customer c on acc.CustomerId=c.CustomerId
                              join CRM.Quote q on o.QuoteKey=q.QuoteKey
							  join CRM.QuoteLines ql on ql.QuoteKey= q.QuoteKey and ql.QuoteLineNumber=@lineNumber
							  left join crm.PurchaseOrderDetails pod on pod.QuoteLineId= ql.QuoteLineId
                              Left join CRM.MasterPurchaseOrder mpo on mpo.PurchaseOrderId=pod.PurchaseOrderId  
                              where op.FranchiseId=@FranchiseId and o.OrderID in @OrderNumber";
                        else
                            Query = @"select isnull(mpo.PurchaseOrderId, 0) as PurchaseOrderId,isnull(mpo.MasterPONumber,0) as MPO_MasterPONum_POId,  o.OrderId,o.OrderNumber as SalesOrderId 
                              ,ac.AccountId,acc.CustomerId ,(select [CRM].[fnGetAccountNameByAccountId](ac.AccountId)) as AccountName,
                              (select 
                                                             case when PreferredTFN='C' then CellPhone
                                                             else case when PreferredTFN='H' then HomePhone 
                                                             else case when PreferredTFN='W' then WorkPhone
                                                             else case when CellPhone is not null then CellPhone 
                                                             else case when HomePhone is not null then HomePhone 
                                                             else case when WorkPhone is not null then WorkPhone 
                                                             else '' 
                                                             end end end end end end) as CellPhone,c.PrimaryEmail,
                              q.SideMark  
                              from CRM.Orders o   
                              Left join crm.MasterPurchaseOrderExtn mpox on mpox.OrderId = o.OrderId
                              Left join CRM.MasterPurchaseOrder mpo on mpo.PurchaseOrderId=mpox.PurchaseOrderId        
                              join CRM.Opportunities op on o.OpportunityId=op.OpportunityId
                              join CRM.Accounts ac on op.AccountId=ac.AccountId
                              join CRM.AccountCustomers acc on ac.AccountId=acc.AccountId
                              join CRM.Customer c on acc.CustomerId=c.CustomerId
                              join CRM.Quote q on o.QuoteKey=q.QuoteKey
                              where op.FranchiseId=@FranchiseId and o.OrderID in @OrderNumber";
                        Value = connection.Query<FranchiseCaseDropModel>(Query, new { lineNumber = lineNumber, FranchiseId = SessionManager.CurrentFranchise.FranchiseId, OrderNumber = orderarray }).FirstOrDefault();
                        if (podid > 0 || poid > 0)
                        {
                            Value.noOfLines = this.getLineNos(orderarray, podid, poid.ToString());

                        }
                        else if (lineNumber > 0) // coming from line level of order
                        {
                            if (Value.PurchaseOrderId != null && Value.PurchaseOrderId != 0)
                            {
                                var result = connection.Query<PurchaseOrderDetails>(@"with cte as
                                                                   ( Select  pod.*
                                                                  ,row_number() over (partition by pod.PICPO order by pod.PurchaseOrdersDetailId asc) rn  
                                                                   ,ql.QuoteLineNumber
                                                                   from CRM.PurchaseOrderDetails pod
                                                                   join CRM.MasterPurchaseOrder mpo on pod.PurchaseOrderId=mpo.PurchaseOrderId
                                                                   join CRM.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId=mpo.PurchaseOrderId
                                                                   join CRM.Orders o on mpox.OrderId=o.OrderID
                                                                   join CRM.Quote q on o.QuoteKey=q.QuoteKey
                                                                  join CRM.QuoteLines ql on q.QuoteKey=ql.QuoteKey and pod.QuoteLineId=ql.QuoteLineId
                                                                  where mpo.PurchaseOrderId = @PurchaseOrderId 
                                                                  ) select * from cte where rn=1 and PICPO = (select PICPO from cte where QuoteLineNumber=@QuoteLineNumber)",
                                                                   new { PurchaseOrderId = Value.PurchaseOrderId, QuoteLineNumber = lineNumber }).FirstOrDefault();
                                if (result != null)
                                {
                                    if (result.PICPO != null && result.PICPO > 0)
                                    {
                                        Value.VPO_PICPO_PODId = result.PICPO;
                                    }
                                    if (result.PurchaseOrdersDetailId != null && result.PurchaseOrdersDetailId > 0)
                                    {
                                        Value.PurchaseOrdersDetailId = result.PurchaseOrdersDetailId;
                                    }
                                    else Value.PurchaseOrdersDetailId = 0;

                                    Value.noOfLines = this.getLineNos(orderarray, result.PurchaseOrdersDetailId, Value.PurchaseOrderId.ToString());

                                }
                                else
                                {

                                    Value.noOfLines = connection.ExecuteScalar<int>(@"select count(*) from CRM.QuoteLines ql
                                                                                     join CRM.Quote q on q.QuoteKey = ql.QuoteKey
                                                                                     join CRM.Orders o on o.QuoteKey = q.QuoteKey
                                                                                     where O.OrderID in @OrderNumber and ql.VendorId = (select qll.VendorId from CRM.QuoteLines qll
                                                                                     join CRM.Quote qq on qq.QuoteKey = qll.QuoteKey
                                                                                     join CRM.Orders oo on oo.QuoteKey = qq.QuoteKey
                                                                                     where Oo.OrderID in @OrderNumber and  qll.QuoteLineNumber = @QuoteLineNumber)",
                                                                                     new { OrderNumber = orderarray, QuoteLineNumber = lineNumber });
                                }

                            }
                            else
                            {

                                if (lineNumber > 0)
                                {
                                    var re1 = connection.Query<QuoteLines>(@" select * from CRM.QuoteLines ql
                                join CRM.Quote q on q.QuoteKey = ql.QuoteKey
                                join CRM.Orders o on o.QuoteKey = q.QuoteKey                               
                                where o.OrderID in @OrderNumber and  ql.QuoteLineNumber = @lineNumber", new { lineNumber = lineNumber, OrderNumber = orderarray }).FirstOrDefault();

                                    if (re1.ProductTypeId != 3)
                                        Value.noOfLines = connection.ExecuteScalar<int>(@"select count(*) from CRM.QuoteLines ql
                                                                                     join CRM.Quote q on q.QuoteKey = ql.QuoteKey
                                                                                     join CRM.Orders o on o.QuoteKey = q.QuoteKey
                                                                                     where O.OrderID in @OrderNumber and ql.VendorId = (select qll.VendorId from CRM.QuoteLines qll
                                                                                     join CRM.Quote qq on qq.QuoteKey = qll.QuoteKey
                                                                                     join CRM.Orders oo on oo.QuoteKey = qq.QuoteKey
                                                                                     where Oo.OrderID in @OrderNumber and  qll.QuoteLineNumber = @QuoteLineNumber)",
                                                                                     new { OrderNumber = orderarray, QuoteLineNumber = lineNumber });
                                    else
                                        Value.noOfLines = connection.ExecuteScalar<int>(@"select count(*) from CRM.QuoteLines ql
                                                                                            join CRM.Quote q on q.QuoteKey = ql.QuoteKey
                                                                                            join CRM.Orders o on o.QuoteKey = q.QuoteKey
                                                                                            where O.OrderID in @OrderNumber and ql.ProductTypeId=3",
                                                                                       new { OrderNumber = orderarray, QuoteLineNumber = lineNumber });

                                }
                                else
                                {
                                    Query = @" select Count(*) from CRM.QuoteLines ql
								 join CRM.Quote q on q.QuoteKey = ql.QuoteKey
								 join CRM.Orders o on o.QuoteKey = q.QuoteKey
								 where O.OrderID in @OrderNumber";
                                    Value.noOfLines = connection.ExecuteScalar<int>(Query, new { lineNumber = lineNumber, OrderNumber = orderarray });
                                }

                            }

                        }
                        else
                        {
                            Query = @" select Count(*) from CRM.QuoteLines ql
								 join CRM.Quote q on q.QuoteKey = ql.QuoteKey
								 join CRM.Orders o on o.QuoteKey = q.QuoteKey
								 where O.OrderID in @OrderNumber";
                            Value.noOfLines = connection.ExecuteScalar<int>(Query, new { OrderNumber = orderarray });
                        }
                    }
                    else
                    {
                        Value.noOfLines = this.getLineNos(orderarray, podid, poid.ToString());
                        // return Value;
                    }


                    return Value;

                }
            }

        }


        public FranchiseCaseDropModel GetSalesOrder(int value, string OrderIds)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {

                {
                    //int[] ia = OrderIds.Split(',').Select(n => Convert.ToInt32(n)).ToArray();                  
                    var Query = "";

                    if (OrderIds == null)
                    {
                        //,c.FirstName+''+c.LastName as AccountName,
                        Query = @"select mpo.PurchaseOrderId,mpo.MasterPONumber as MPO_MasterPONum_POId,o.OrderId,o.OrderNumber as SalesOrderId 
                              ,ac.AccountId,acc.CustomerId, (select [CRM].[fnGetAccountNameByAccountId](ac.AccountId)) as AccountName ,
                              (select 
                                                             case when PreferredTFN='C' then CellPhone
                                                             else case when PreferredTFN='H' then HomePhone 
                                                             else case when PreferredTFN='W' then WorkPhone
                                                             else case when CellPhone is not null then CellPhone 
                                                             else case when HomePhone is not null then HomePhone 
                                                             else case when WorkPhone is not null then WorkPhone 
                                                             else '' 
                                                             end end end end end end) as CellPhone,c.PrimaryEmail,
                              q.SideMark
                              from CRM.MasterPurchaseOrder mpo

                              Left join crm.MasterPurchaseOrderExtn mpox on mpo.PurchaseOrderId=mpox.PurchaseOrderId

                              join CRM.Opportunities op on mpox.OpportunityId=op.OpportunityId
                              join CRM.Orders o on mpox.OrderId=o.OrderID
                              join CRM.Accounts ac on mpox.AccountId=ac.AccountId
                              join CRM.AccountCustomers acc on ac.AccountId=acc.AccountId
                              join CRM.Customer c on acc.CustomerId=c.CustomerId
                              join CRM.Quote q on o.QuoteKey=q.QuoteKey
                              where op.FranchiseId=@FranchiseId and mpo.PurchaseOrderId=@PurchaseOrderId";
                        var Value = connection.Query<FranchiseCaseDropModel>(Query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId, PurchaseOrderId = value }).ToList();

                        var ret_Obj = new FranchiseCaseDropModel();
                        if (Value.Count > 0)
                            ret_Obj = Value[0];

                        List<int> intlist = new List<int>();

                        foreach (var fdm in Value)
                        {
                            intlist.Add(fdm.OrderId);
                        }
                        ret_Obj.Orderlist = intlist;
                        ret_Obj.noOfLines = this.getLineNos(null, 0, value.ToString());
                        return ret_Obj;
                    }
                    else
                    {
                        var ret_Obj = new FranchiseCaseDropModel();
                        ret_Obj.noOfLines = this.getLineNos(OrderIds.Split(',').Select(n => Convert.ToInt32(n)).ToArray(), 0, value.ToString());
                        return ret_Obj;
                    }
                }
            }
        }
        public List<FranchiseCaseAddInfoModel> GetData(int id, int CaseId)
        {

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                if (CaseId == 0)
                {
                    return new List<FranchiseCaseAddInfoModel>();
                }
                else
                {
                    var Query = "";

                    //           Query = @"Select ql.ProductTypeId,ql.QuoteLineNumber as QuoteLineNumber,po.PICPO as VPO,fci.*,ql.Description,
                    //                     tltype.Name as Typevalue,tlrea.Name as CaseReason,tlTripApp.Name as TripChargeApprovedName,
                    //                     tltsta.Name as StatusName,tltres.Name as ResolutionName,                              
                    //                     case when po.PartNumber='' then '0' else po.PartNumber end as ProductId,
                    //                     case when isnull(ql.productname,'')!='' then ql.productname
                    //else
                    //                         case when ql.ProductId=0 then '' 
                    //    else case when ql.ProductTypeId <> 4 then
                    //    case when exists(select ProductId from CRM.HFCProducts where ProductId=ql.ProductId) 
                    //                         then (select ProductGroupDesc from CRM.HFCProducts where ProductId=ql.ProductId) end 
                    //                         else case when exists(select ProductId from CRM.FranchiseProducts where ProductKey=ql.ProductId) 
                    //                         then (select ProductName from CRM.FranchiseProducts where ProductKey=ql.ProductId) else '' end end end 
                    //                       end  as ProductName,
                    //                     case when ql.VendorId is null and ql.VendorId=0 then '' else case when exists(select VendorId from Acct.HFCVendors where VendorId=ql.VendorId) 
                    //                     then (select VendorName from Acct.HFCVendors where VendorId=ql.VendorId) 
                    //                     else case when exists(select VendorId from Acct.FranchiseVendors where VendorId=ql.VendorId) 
                    //                     then (select VendorName from Acct.FranchiseVendors where VendorId=ql.VendorId) else '' end end end  as VendorName,
                    //                        case when exists(select top(1) vc.VendorCaseNumber from CRM.VendorCase vc where vc.CaseId = fci.CaseId  and ql.VendorId=vc.VendorId) then 
                    //(select top(1) vcc.VendorCaseNumber from CRM.VendorCase vcc where vcc.CaseId = fci.CaseId and ql.VendorId=vcc.VendorId) else null end as VendorCaseNo,
                    //                     VendorId, po.vendorReference as VendorReference, po.PurchaseOrderId,fci.Amount
                    //                     from CRM.FranchiseCaseAddInfo fci
                    //                     left join CRM.PurchaseOrderDetails po on fci.QuoteLineId=po.QuoteLineId
                    //                     join CRM.QuoteLines ql on fci.QuoteLineId=ql.QuoteLineId
                    //                     left join CRM.Type_LookUpValues tltype on fci.Type=tltype.Id
                    //                     left join CRM.Type_LookUpValues tlrea on fci.ReasonCode=tlrea.Id
                    //                     left join CRM.Type_LookUpValues tltsta on fci.Status=tltsta.Id
                    //                     left join CRM.Type_LookUpValues tltres on fci.Resolution=tltres.Id
                    //                     left join CRM.Type_LookUpValues tlTripApp on fci.TripChargeApproved = tlTripApp.Id
                    //                     where fci.CaseId=@Id";

                    Query = @"Select ql.ProductTypeId,CAST(o.OrderNumber as varchar(10)) + '-' + CAST(ql.QuoteLineNumber as varchar(3)) as QuoteLineNumber,po.PICPO as VPO,fci.*,ql.Description,
                              tltype.Name as Typevalue,tlrea.Name as CaseReason,tlTripApp.Name as TripChargeApprovedName,
                              tltsta.Name as StatusName,tltres.Name as ResolutionName,   
                                case when ql.ProductTypeId <> 3 then                           
                              case when po.PartNumber='' then '0' else po.PartNumber end
							  else
							   ( select ProductID from CRM.FranchiseProducts where ProductKey = ql.ProductId and FranchiseId=(
                                    select FranchiseId from CRM.Opportunities op
                                      join crm.Orders o on o.OpportunityId= op.OpportunityId
                                      join crm.FranchiseCase fc on fc.SalesOrderId= o.OrderID
                                      where fc.CaseId=@Id))
							   end as ProductId,   
                              case when isnull(ql.productname,'')!='' then ql.productname
							  else
                                  case when ql.ProductId=0 then '' 
							      else case when ql.ProductTypeId <> 4 then
							      case when exists(select ProductId from CRM.HFCProducts where ProductId=ql.ProductId) 
                                  then (select ProductGroupDesc from CRM.HFCProducts where ProductId=ql.ProductId) end 
                                  else case when exists(select ProductId from CRM.FranchiseProducts where ProductKey=ql.ProductId) 
                                  then (select ProductName from CRM.FranchiseProducts where ProductKey=ql.ProductId) else '' end end end 
                                end  as ProductName,
                              case when ql.VendorId is null and ql.VendorId=0 then '' else case when exists(select VendorId from Acct.HFCVendors where VendorId=ql.VendorId) 
                              then (select VendorName from Acct.HFCVendors where VendorId=ql.VendorId) 
                              else case when exists(select VendorId from Acct.FranchiseVendors where VendorId=ql.VendorId) 
                              then (select VendorName from Acct.FranchiseVendors where VendorId=ql.VendorId) else '' end end end  as VendorName,
                                 case when exists(select top(1) vc.VendorCaseNumber from CRM.VendorCase vc where vc.CaseId = fci.CaseId  and ql.VendorId=vc.VendorId) then 
							  (select top(1) vcc.VendorCaseNumber from CRM.VendorCase vcc where vcc.CaseId = fci.CaseId and ql.VendorId=vcc.VendorId) else null end as VendorCaseNo,
                              VendorId, po.vendorReference as VendorReference, po.PurchaseOrderId,fci.Amount
                              from CRM.FranchiseCaseAddInfo fci
                              left join CRM.PurchaseOrderDetails po on fci.QuoteLineId=po.QuoteLineId
                              join CRM.QuoteLines ql on fci.QuoteLineId=ql.QuoteLineId
							  join CRM.Orders o on ql.QuoteKey= o.QuoteKey
                              left join CRM.Type_LookUpValues tltype on fci.Type=tltype.Id
                              left join CRM.Type_LookUpValues tlrea on fci.ReasonCode=tlrea.Id
                              left join CRM.Type_LookUpValues tltsta on fci.Status=tltsta.Id
                              left join CRM.Type_LookUpValues tltres on fci.Resolution=tltres.Id
                              left join CRM.Type_LookUpValues tlTripApp on fci.TripChargeApproved = tlTripApp.Id
                              where fci.CaseId=@Id"; //case when po.PartNumber='' then '0' else po.PartNumber end as ProductId,

                    var Value = connection.Query<FranchiseCaseAddInfoModel>(Query, new { Id = CaseId}).ToList();
                    return Value;

                }
            }
        }

        public List<FranchiseCaseAddInfoModel> GetFranchiseCaseVendorView(int id, string VendorcaseId)
        {

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                if (id == 0)
                {
                    return null;
                }
                else
                {
                    var Query = "";
                    //var CaseId_value = GetId(CaseId);
                    //Query = @"Select ql.QuoteLineNumber as QuoteLineNumber,po.PICPO as VPO,fci.*,hfp.ProductName,
                    //          ql.ProductId,ql.VendorId,hf.Name as VendorName,ql.Description,
                    //          tltype.Name as Typevalue,tlrea.Name as CaseReason,
                    //          tltsta.Name as StatusName,tltres.Name as ResolutionName 
                    //          from CRM.FranchiseCaseAddInfo fci
                    //          join CRM.PurchaseOrderDetails po on fci.QuoteLineId=po.QuoteLineId
                    //          join CRM.QuoteLines ql on fci.QuoteLineId=ql.QuoteLineId
                    //          join CRM.HFCProducts hfp on ql.ProductId=hfp.ProductID
                    //          join Acct.HFCVendors hf on ql.VendorId=hf.VendorId
                    //          join CRM.Type_LookUpValues tltype on fci.Type=tltype.Id
                    //          join CRM.Type_LookUpValues tlrea on fci.ReasonCode=tlrea.Id
                    //          join CRM.Type_LookUpValues tltsta on fci.Status=tltsta.Id
                    //          join CRM.Type_LookUpValues tltres on fci.Resolution=tltres.Id
                    //          where fci.CaseId=@Id";
                    Query = @"
Select distinct CAST(o.OrderNumber as varchar(10)) + '-' + CAST(ql.QuoteLineNumber as varchar(3)) as QuoteLineNumber,po.PICPO as VPO,fci.*,ql.Description,
                              tltype.Name as Typevalue,tlrea.Name as CaseReason,tlTripApp.Name as TripChargeApprovedName,
                              tltsta.Name as StatusName,tltres.Name as ResolutionName,
                               case when ql.ProductTypeId <> 3 then
                            (select PICProductID from CRM.HFCProducts where ProductID =ql.ProductId)
                            else 
                            ( select ProductID from CRM.FranchiseProducts where ProductKey = ql.ProductId and FranchiseId=@fId) end as  ProductId,
                              case when ql.ProductId=0 then '' else case when exists(select ProductId from CRM.HFCProducts where ProductId=ql.ProductId) 
                              then (select ProductGroupDesc from CRM.HFCProducts where ProductId=ql.ProductId) 
                              else case when exists(select ProductId from CRM.FranchiseProducts where ProductId=ql.ProductId) 
                              then (select ProductName from CRM.FranchiseProducts where ProductId=ql.ProductId) else '' end end end  as ProductName,
                              case when ql.VendorId is null and ql.VendorId=0 then '' else case when exists(select VendorId from Acct.HFCVendors where VendorId=ql.VendorId) 
                              then (select VendorName from Acct.HFCVendors where VendorId=ql.VendorId) 
                              else case when exists(select VendorId from Acct.FranchiseVendors where VendorId=ql.VendorId) 
                              then (select VendorName from Acct.FranchiseVendors where ProductId=ql.VendorId) else '' end end end  as VendorName,po.VendorReference,
                              VendorId,fci.Amount
                              from CRM.FranchiseCaseAddInfo fci
                              left join CRM.PurchaseOrderDetails po on fci.QuoteLineId=po.QuoteLineId
                              join CRM.QuoteLines ql on fci.QuoteLineId=ql.QuoteLineId
							  join CRM.Quote q on q.QuoteKey=ql.QuoteKey
							  join CRM.Orders o on o.QuoteKey= q.QuoteKey
                               left join CRM.Type_LookUpValues tltype on fci.Type=tltype.Id
                              left join CRM.Type_LookUpValues tlrea on fci.ReasonCode=tlrea.Id
                              left join CRM.Type_LookUpValues tltsta on fci.Status=tltsta.Id
                              left join CRM.Type_LookUpValues tltres on fci.Resolution=tltres.Id
							  left join CRM.Type_LookUpValues tlTripApp on fci.TripChargeApproved = tlTripApp.Id
                              where  fci.CaseId = @CaseId and ql.VendorId=(
							  select top(1) ql.VendorId from CRM.QuoteLines ql
                                join CRM.FranchiseCaseAddInfo fcai on  fcai.QuoteLineId = ql.QuoteLineId
                                where fcai.VendorCaseNumber = @VendorCaseNumber)";
                    var Value = connection.Query<FranchiseCaseAddInfoModel>(Query, new { CaseId = id, VendorCaseNumber = VendorcaseId, fId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                    return Value;

                }
            }
        }

        public string Save_Initial(FranchiseCaseModel data)
        {
            var connection = new SqlConnection(ContextFactory.CrmConnectionString);
            {
                int casenum = 1000;
                if (data.CaseNumber == 0)
                {
                    var Query = "";
                    Query = @"Select Top 1 * from CRM.FranchiseCase order by CaseId desc";
                    var Value = connection.Query<FranchiseCaseModel>(Query).FirstOrDefault();
                    if (Value != null)
                    {
                        data.CaseNumber = Value.CaseNumber + 1;
                        data.Owner_PersonId = SessionManager.CurrentUser.PersonId;
                        if (data.Status == null || data.Status == 0)
                        {
                            data.Status = 7000;
                        }
                        data.CreatedOn = DateTime.UtcNow;
                        data.LastUpdatedOn = data.CreatedOn;
                        var result = Insert<int, FranchiseCaseModel>(ContextFactory.CrmConnectionString, data);
                        return data.CaseNumber + "|" + result;
                        // return data.CaseNumber;
                    }
                    else
                    {
                        data.CaseNumber = casenum;
                        data.Owner_PersonId = SessionManager.CurrentUser.PersonId;
                        if (data.Status == null || data.Status == 0)
                        {
                            data.Status = 7000;
                        }
                        data.CreatedOn = DateTime.UtcNow;
                        data.LastUpdatedOn = data.CreatedOn;
                        var result = Insert<int, FranchiseCaseModel>(ContextFactory.CrmConnectionString, data);
                        return data.CaseNumber + "|" + result;
                        // return data.CaseNumber;
                    }
                }
                return null;

            }

        }

        public bool update_Historycase(int CaseId, string Type, List<string> LinesDeleted = null, List<string> LinesAdded = null, List<Tuple<string, string, string, string>> LineUpdates = null, List<Tuple<string, string>> LineChange = null, List<Tuple<string, string, string>> CaseChange = null, FranchiseCaseModel data = null, FranchiseCaseModel CaseDetails = null)
        {
            if (Type == "FranchiseCase")
            {
                HistoryTable tab = new HistoryTable();
                tab.Table = "FranchiseCase"; tab.TableId = CaseId; tab.Date = DateTime.UtcNow;
                tab.UserId = SessionManager.CurrentUser.PersonId;


                if (LinesDeleted != null)
                    foreach (var r in LinesDeleted)
                    {
                        tab.Field = "Line " + r;
                        tab.OriginalValue = "";
                        tab.NewValue = "Removed";
                        Insert<int, HistoryTable>(ContextFactory.CrmConnectionString, tab);
                    }
                if (LinesAdded != null)
                    foreach (var r in LinesAdded)
                    {
                        tab.Field = "Line " + r;
                        tab.OriginalValue = "";
                        tab.NewValue = "Added";
                        Insert<int, HistoryTable>(ContextFactory.CrmConnectionString, tab);
                    }
                if (CaseChange != null)
                    foreach (var r in CaseChange)
                    {
                        tab.Field = Regex.Replace(r.Item1, @"((?<=\p{Ll})\p{Lu})|((?!\A)\p{Lu}(?>\p{Ll}))", " $0");
                        tab.OriginalValue = r.Item2;
                        tab.NewValue = r.Item3;
                        Insert<int, HistoryTable>(ContextFactory.CrmConnectionString, tab);
                    }

                if (LineChange != null)
                    foreach (var r in LineChange)
                    {
                        tab.Field = "Line ";
                        tab.OriginalValue = r.Item1;
                        tab.NewValue = r.Item2;
                        Insert<int, HistoryTable>(ContextFactory.CrmConnectionString, tab);
                    }
                if (LineUpdates != null)
                    foreach (var r in LineUpdates)
                    {
                        tab.Field = "Line " + r.Item1 + " - " + Regex.Replace(r.Item2, @"((?<=\p{Ll})\p{Lu})|((?!\A)\p{Lu}(?>\p{Ll}))", " $0");
                        tab.OriginalValue = r.Item3;
                        tab.NewValue = r.Item4;
                        Insert<int, HistoryTable>(ContextFactory.CrmConnectionString, tab);
                    }
            }
            else
            {
                HistoryTable tab = new HistoryTable();
                tab.Table = "VendorCase"; tab.TableId = CaseId; tab.Date = DateTime.Today;
                tab.UserId = SessionManager.CurrentUser.PersonId;


                if (LinesDeleted != null)
                    foreach (var r in LinesDeleted)
                    {
                        tab.Field = "Line " + r;
                        tab.OriginalValue = "";
                        tab.NewValue = "Removed";
                        Insert<int, HistoryTable>(ContextFactory.CrmConnectionString, tab);
                    }
                if (LinesAdded != null)
                    foreach (var r in LinesAdded)
                    {
                        tab.Field = "Line " + r;
                        tab.OriginalValue = "";
                        tab.NewValue = "Added";
                        Insert<int, HistoryTable>(ContextFactory.CrmConnectionString, tab);
                    }
                if (CaseChange != null)
                    foreach (var r in CaseChange)
                    {
                        tab.Field = Regex.Replace(r.Item1, @"((?<=\p{Ll})\p{Lu})|((?!\A)\p{Lu}(?>\p{Ll}))", " $0");
                        tab.OriginalValue = r.Item2;
                        tab.NewValue = r.Item3;
                        Insert<int, HistoryTable>(ContextFactory.CrmConnectionString, tab);
                    }

                if (LineChange != null)
                    foreach (var r in LineChange)
                    {
                        tab.Field = "Line ";
                        tab.OriginalValue = r.Item1;
                        tab.NewValue = r.Item2;
                        Insert<int, HistoryTable>(ContextFactory.CrmConnectionString, tab);
                    }
                if (LineUpdates != null)
                    foreach (var r in LineUpdates)
                    {
                        tab.Field = "Line " + r.Item1 + " - " + Regex.Replace(r.Item2, @"((?<=\p{Ll})\p{Lu})|((?!\A)\p{Lu}(?>\p{Ll}))", " $0");
                        tab.OriginalValue = r.Item3;
                        tab.NewValue = r.Item4;
                        Insert<int, HistoryTable>(ContextFactory.CrmConnectionString, tab);
                    }
            }



            //if (data.VPO_PICPO_PODId != CaseDetails.VPO_PICPO_PODId && CaseDetails.VPO_PICPO_PODId != null)
            //{

            //    tab.Field = "VPO_PICPO_PODId";
            //    tab.OriginalValue = CaseDetails.VPO_PICPO_PODId.ToString();
            //    tab.NewValue = data.VPO_PICPO_PODId.ToString();
            //    Insert<int, HistoryTable>(ContextFactory.CrmConnectionString, tab);
            //}
            //if (data.MPO_MasterPONum_POId != CaseDetails.MPO_MasterPONum_POId && CaseDetails.MPO_MasterPONum_POId != null)
            //{
            //    tab.Field = "MPO_MasterPONum_POId";
            //    tab.OriginalValue = CaseDetails.MPO_MasterPONum_POId.ToString();
            //    tab.NewValue = data.MPO_MasterPONum_POId.ToString();
            //    Insert<int, HistoryTable>(ContextFactory.CrmConnectionString, tab);
            //}
            //if (data.SalesOrderId != CaseDetails.SalesOrderId && CaseDetails.SalesOrderId != null)
            //{
            //    tab.Field = "SalesOrderId";
            //    tab.OriginalValue = CaseDetails.SalesOrderId.ToString();
            //    tab.NewValue = data.SalesOrderId.ToString();
            //    Insert<int, HistoryTable>(ContextFactory.CrmConnectionString, tab);
            //}
            //if (data.Description != CaseDetails.Description && CaseDetails.Description != null)
            //{
            //    tab.Field = "Description";
            //    tab.OriginalValue = CaseDetails.Description.ToString();
            //    tab.NewValue = data.Description.ToString();
            //    Insert<int, HistoryTable>(ContextFactory.CrmConnectionString, tab);
            //}

            return true;

        }
        public string Update_Data(FranchiseCaseModel data)
        {
            var CaseDetails = GetData<FranchiseCaseModel>(ContextFactory.CrmConnectionString, data.CaseId);
            var HistoryType = "FranchiseCase";
            if (CaseDetails != null)
            {
                var conn = new SqlConnection(ContextFactory.CrmConnectionString);


                List<string> LinesDeleted = new List<string>();
                List<string> LinesAdded = new List<string>();
                List<Tuple<string, string, string, string>> LineUpdates = new List<Tuple<string, string, string, string>>();
                List<Tuple<string, string>> LineChange = new List<Tuple<string, string>>();
                List<Tuple<string, string, string>> CaseChange = new List<Tuple<string, string, string>>();

                //  var resCaseFollow = conn.Query<Person>("select p.* from [CRM].[Person] as p join CRM.CaseFollow as cf on cf.PersonId = p.PersonId  where cf.CaseId =@CaseId", new { CaseId = data.CaseId }).ToList();
                // if (resCaseFollow.Count > 0)
                //  {

                if (data.VPO_PICPO_PODId != CaseDetails.VPO_PICPO_PODId && CaseDetails.VPO_PICPO_PODId != null)
                {
                    CaseChange.Add(new Tuple<string, string, string>("VPO", CaseDetails.VPO_PICPO_PODId.ToString(), data.VPO_PICPO_PODId.ToString()));

                }
                if (data.MPO_MasterPONum_POId != CaseDetails.MPO_MasterPONum_POId && CaseDetails.MPO_MasterPONum_POId != null)
                {
                    var mpoRes = conn.Query<MasterPurchaseOrder>("select * from [CRM].[MasterPurchaseOrder] where PurchaseOrderId=@id", new { id = data.MPO_MasterPONum_POId }).FirstOrDefault();
                    if(mpoRes != null)
                    CaseChange.Add(new Tuple<string, string, string>("MPO", data.MPOID != null ? data.MPOID.ToString(): "0", mpoRes.MasterPONumber != null ? mpoRes.MasterPONumber.ToString(): "0"));
                    else
                        CaseChange.Add(new Tuple<string, string, string>("MPO", data.MPOID != null ? data.MPOID.ToString() : "0", "0"));

                }
                if (data.SalesOrderId != CaseDetails.SalesOrderId && CaseDetails.SalesOrderId != null)
                {
                    CaseChange.Add(new Tuple<string, string, string>("SalesOrderId", CaseDetails.SalesOrderId.ToString(), data.SalesOrderId.ToString()));

                }
                if (data.Description != CaseDetails.Description && CaseDetails.Description != null)
                {
                    CaseChange.Add(new Tuple<string, string, string>("Description", CaseDetails.Description.ToString(), data.Description.ToString()));

                }

                var res = conn.Query<FranchiseCaseAddInfoModel>("Select * from CRM.FranchiseCaseAddInfo where CaseId=@CaseId", new { CaseId = data.CaseId }).ToList();

                if (res.Count > 0)
                {
                    foreach (var item in res)
                    {

                        if (!((from ram in data.AdditionalInfo where ram.Id == item.Id select ram).ToList().Count > 0))
                        {
                            var VendorCaseRes = conn.Query<VendorCase>(@"select * from CRM.VendorCase where VendorCaseId =( select VendorCaseId from
                                CRM.VendorCaseDetail where CaseLineId =  @Id )", new { Id = item.Id }).FirstOrDefault();


                            if (VendorCaseRes != null)
                            {
                                var VendorLineCount = conn.Query<VendorCase>("select * from CRM.VendorCase where CaseId= @CaseId and VendorId=@VendorId",
                                    new { CaseId = VendorCaseRes.CaseId, VendorId = VendorCaseRes.VendorId }).ToList();
                                if (VendorLineCount.Count == 1)
                                {
                                    conn.Execute("delete from CRM.VendorCaseDetail where CaseLineId= @Id", new { Id = item.Id });
                                    conn.Execute("delete from CRM.VendorCase where VendorCaseId = (select VendorCaseID from CRM.VendorCaseDetail where CaseLineId= @Id)", new { Id = item.Id });

                                }
                                else if (VendorLineCount.Count > 1)
                                {
                                    conn.Execute("delete from CRM.VendorCaseDetail where CaseLineId= @Id", new { Id = item.Id });
                                }
                            }
                            var resQuoteLines = conn.Query<QuoteLines>("Select * from CRM.QuoteLines where QuoteLineId=@QuoteLineId", new { QuoteLineId = item.QuoteLineId }).FirstOrDefault();
                            conn.Execute("Delete from CRM.FranchiseCaseAddInfo where Id=@Id", new { Id = item.Id });
                            LinesDeleted.Add(resQuoteLines.QuoteLineNumber.ToString());
                        }
                    }
                }

                // if(data.AdditionalInfo != null)
                foreach (var res1 in data.AdditionalInfo)
                {
                    if (res1.CaseId == 0)
                    {
                        //LinesAdded.Add(res1.QuoteLineId2.QuoteLineNumber.ToString());
                        LinesAdded.Add(res1.QuoteLineNumber.ToString());
                    }

                    if (res1.Id > 0)
                    {
                        var line1 = conn.Query<QuoteLines>(@"Select ql.*,CAST(o.OrderNumber as varchar(10)) + '-' + CAST(ql.QuoteLineNumber as varchar(3)) as QuoteLineNumberr from CRM.QuoteLines ql
                                 join CRM.Orders o on o.QuoteKey = ql.QuoteKey  where QuoteLineId = @QuoteLineId", new { QuoteLineId = res1.QuoteLineId }).FirstOrDefault();
                        var line2 = conn.Query<QuoteLines>(@"Select ql.*,CAST(o.OrderNumber as varchar(10)) + '-' + CAST(ql.QuoteLineNumber as varchar(3)) as QuoteLineNumberr from CRM.QuoteLines ql
                                 join CRM.Orders o on o.QuoteKey = ql.QuoteKey  where QuoteLineId = @QuoteLineId", new { QuoteLineId = (from ram in res where ram.Id == res1.Id select ram).ToList()[0].QuoteLineId }).FirstOrDefault();

                        if (res1.QuoteLineId != (from ram in res where ram.Id == res1.Id select ram).ToList()[0].QuoteLineId)
                        {
                            LineChange.Add(new Tuple<string, string>(line2.QuoteLineNumberr.ToString(), line1.QuoteLineNumberr.ToString()));


                        }
                        else
                        {
                            var Lookupvalues = conn.Query<Type_LookUpValues>("Select * from CRM.Type_LookUpValues").ToList();
                            var resLinestocompare_before = (from ram in res where ram.Id == res1.Id select ram).ToList();

                            if (res1.Type != resLinestocompare_before[0].Type && (from ram in Lookupvalues where ram.Id == resLinestocompare_before[0].Type select ram).ToList().Count > 0 && (from ram in Lookupvalues where ram.Id == res1.Type select ram).ToList().Count > 0)
                                LineUpdates.Add(new Tuple<string, string, string, string>(res1.QuoteLineNumber.ToString(), " Type", (from ram in Lookupvalues where ram.Id == resLinestocompare_before[0].Type select ram).ToList()[0].Name, (from ram in Lookupvalues where ram.Id == res1.Type select ram).ToList()[0].Name));
                            if (res1.ReasonCode != resLinestocompare_before[0].ReasonCode && (from ram in Lookupvalues where ram.Id == resLinestocompare_before[0].ReasonCode select ram).ToList().Count > 0 && (from ram in Lookupvalues where ram.Id == res1.ReasonCode select ram).ToList().Count > 0)
                                LineUpdates.Add(new Tuple<string, string, string, string>(res1.QuoteLineNumber.ToString(), "Case Reason", (from ram in Lookupvalues where ram.Id == resLinestocompare_before[0].ReasonCode select ram).ToList()[0].Name, (from ram in Lookupvalues where ram.Id == res1.ReasonCode select ram).ToList()[0].Name));

                            if (res1.IssueDescription != resLinestocompare_before[0].IssueDescription)
                                LineUpdates.Add(new Tuple<string, string, string, string>(res1.QuoteLineNumber.ToString(), " Issue Description", resLinestocompare_before[0].IssueDescription, res1.IssueDescription));
                            if (res1.ExpediteReq != resLinestocompare_before[0].ExpediteReq)
                                LineUpdates.Add(new Tuple<string, string, string, string>(res1.QuoteLineNumber.ToString(), " Expedite Req", resLinestocompare_before[0].ExpediteReq.ToString(), res1.ExpediteReq.ToString()));
                            if (res1.ExpediteApproved != resLinestocompare_before[0].ExpediteApproved)
                                LineUpdates.Add(new Tuple<string, string, string, string>(res1.QuoteLineNumber.ToString(), " Expedite Approved", resLinestocompare_before[0].ExpediteApproved.ToString(), res1.ExpediteApproved.ToString()));
                            if (res1.ReqTripCharge != resLinestocompare_before[0].ReqTripCharge)
                                LineUpdates.Add(new Tuple<string, string, string, string>(res1.QuoteLineNumber.ToString(), " Req Trip Charge", resLinestocompare_before[0].ReqTripCharge.ToString(), res1.ReqTripCharge.ToString()));

                            if (res1.Status != resLinestocompare_before[0].Status)
                                LineUpdates.Add(new Tuple<string, string, string, string>(res1.QuoteLineNumber.ToString(), " Status", (from ram in Lookupvalues where ram.Id == resLinestocompare_before[0].Status select ram).ToList()[0].Name, (from ram in Lookupvalues where ram.Id == res1.Status select ram).ToList()[0].Name));
                            if (res1.Resolution != resLinestocompare_before[0].Resolution && (from ram in Lookupvalues where ram.Id == resLinestocompare_before[0].Resolution select ram).ToList().Count > 0 && (from ram in Lookupvalues where ram.Id == res1.Resolution select ram).ToList().Count > 0)
                                LineUpdates.Add(new Tuple<string, string, string, string>(res1.QuoteLineNumber.ToString(), " Resolution", (from ram in Lookupvalues where ram.Id == resLinestocompare_before[0].Resolution select ram).ToList()[0].Name, (from ram in Lookupvalues where ram.Id == res1.Resolution select ram).ToList()[0].Name));


                        }

                    }
                }


                //  }




                //if (data.Status != CaseDetails.Status || data.VPO_PICPO_PODId != CaseDetails.VPO_PICPO_PODId
                //    || data.MPO_MasterPONum_POId != CaseDetails.MPO_MasterPONum_POId ||
                //    data.SalesOrderId != CaseDetails.SalesOrderId || data.Description != CaseDetails.Description)
                //{
                //    update_Historycase(data, CaseDetails);
                //}

                CaseDetails.VPO_PICPO_PODId = data.VPO_PICPO_PODId;
                CaseDetails.MPO_MasterPONum_POId = data.MPO_MasterPONum_POId;
                CaseDetails.SalesOrderId = data.SalesOrderId;
                CaseDetails.Description = data.Description.Length > 1000 ? data.Description.Substring(0, 1000) : data.Description;
                CaseDetails.CreatedOn = TimeZoneManager.ToUTC(CaseDetails.CreatedOn);

                CaseDetails.LastUpdatedOn = DateTime.UtcNow;

                int ttt = CaseDetails.Description.Length;
                Update<FranchiseCaseModel>(ContextFactory.CrmConnectionString, CaseDetails);

                //  if(data.AdditionalInfo != null)
                foreach (var item in data.AdditionalInfo)
                {
                    if (item.Id != 0)
                    {
                        var CaseAddinfoDetails = GetData<FranchiseCaseAddInfoModel>(ContextFactory.CrmConnectionString, item.Id);
                        if (CaseAddinfoDetails != null)
                        {
                            CaseAddinfoDetails.CaseId = CaseAddinfoDetails.CaseId;
                            CaseAddinfoDetails.Type = item.Type;
                            if (item.QuoteLineId2 != null)
                            {
                                CaseAddinfoDetails.QuoteLineId = item.QuoteLineId2.QuoteLineId;
                            }
                            else if (item.QuoteLineId != 0)
                            {
                                CaseAddinfoDetails.QuoteLineId = item.QuoteLineId;
                            }
                            else
                                CaseAddinfoDetails.QuoteLineId = CaseAddinfoDetails.QuoteLineId;


                            CaseAddinfoDetails.ReasonCode = item.ReasonCode;
                            CaseAddinfoDetails.IssueDescription = item.IssueDescription;
                            CaseAddinfoDetails.ExpediteReq = item.ExpediteReq;

                            CaseAddinfoDetails.ExpediteApproved = item.ExpediteApproved;
                            CaseAddinfoDetails.ReqTripCharge = item.ReqTripCharge;
                            CaseAddinfoDetails.TripChargeApproved = item.TripChargeApproved;
                            CaseAddinfoDetails.Status = item.Status;
                            CaseAddinfoDetails.Resolution = item.Resolution;
                            // CaseAddinfoDetails.ConvertToVendor = item.ConvertToVendor;
                            Update<FranchiseCaseAddInfoModel>(ContextFactory.CrmConnectionString, CaseAddinfoDetails);
                        }


                    }
                    else
                    {
                        FranchiseCaseAddInfoModel Addinfo = new FranchiseCaseAddInfoModel();
                        //  int vendorcasenum = 1000;
                        //var Value = GetCaseDataValue();
                        //if (Value == null)
                        //{
                        //    Addinfo.VendorCaseNumber = vendorcasenum.ToString();
                        //}
                        //else Addinfo.VendorCaseNumber = (Value.VendorCaseNumber + 1).ToString();
                        Addinfo.CaseId = data.CaseId;
                        Addinfo.Type = item.Type;

                        //Addinfo.QuoteLineId = item.QuoteLineId2.QuoteLineId;
                        Addinfo.QuoteLineId = item.QuoteLineId;
                        Addinfo.ReasonCode = item.ReasonCode;
                        Addinfo.IssueDescription = item.IssueDescription;
                        Addinfo.ExpediteReq = item.ExpediteReq;

                        Addinfo.ExpediteApproved = item.ExpediteApproved;
                        Addinfo.ReqTripCharge = item.ReqTripCharge;
                        Addinfo.TripChargeApproved = item.TripChargeApproved;
                        Addinfo.Status = item.Status;
                        Addinfo.Resolution = item.Resolution;
                        var result = Insert<int, FranchiseCaseAddInfoModel>(ContextFactory.CrmConnectionString, Addinfo);
                    }
                }



                var connection = new SqlConnection(ContextFactory.CrmConnectionString);


                var fcai = connection.Query<FranchiseCaseAddInfoModel>(" select * from [CRM].[FranchiseCaseAddInfo] where CaseId=@CaseId and Status=( select min(Status) from [CRM].[FranchiseCaseAddInfo] where CaseId = @CaseId)", new { CaseId = data.CaseId }).FirstOrDefault();
                var fc = connection.Query<FranchiseCaseModel>("select *  from [CRM].[FranchiseCase] where CaseId = @CaseId", new { CaseId = data.CaseId }).FirstOrDefault();
                connection.Execute("Update [CRM].[FranchiseCase] set IncidentDate=@IncidentDate where CaseId = @CaseId", new { CaseId = data.CaseId, IncidentDate = data.IncidentDate });


                if (fcai != null)
                {
                    if (fcai.Status != fc.Status)
                    {


                        if (fcai.Status == 7006 || fcai.Status == 7007)
                        {
                            connection.Execute("Update [CRM].[FranchiseCase] set Status=@Status,DateTimeClosed=@Date where CaseId = @CaseId", new { CaseId = data.CaseId, Status = fcai.Status, Date = DateTime.UtcNow });

                        }
                        else
                            connection.Execute("Update [CRM].[FranchiseCase] set Status=@Status,DateTimeClosed=null where CaseId = @CaseId", new { CaseId = data.CaseId, Status = fcai.Status });

                    }
                    else
                    {
                        if ((fcai.Status == 7006 || fcai.Status == 7007) && fc.DateTimeClosed == null)
                        {
                            connection.Execute("Update [CRM].[FranchiseCase] set Status=@Status,DateTimeClosed=@Date where CaseId = @CaseId", new { CaseId = data.CaseId, Status = fcai.Status, Date = DateTime.UtcNow });

                        }
                    }
                }



                string htmlText = "";

                foreach (var r in LinesDeleted)
                {
                    htmlText += "<p>Line <b>" + r + "</b> has been deleted</p>";
                }
                foreach (var r in LinesAdded)
                {
                    htmlText += "<p>Line <b>" + r + "</b> has been added</p>";
                }

                foreach (var r in LineChange)
                {
                    htmlText += "<p>Line <b>" + r.Item1 + "</b> has been changed to <b>" + r.Item2 + "</b></p>";
                }
                foreach (var r in CaseChange)
                {
                    htmlText += "<p>Case <b>" + r.Item1 + "</b> has been changed from <b>" + r.Item2 + "</b> to  <b>" + r.Item3 + "</b></p>";

                }
                string tablestring = "";
                if (LineUpdates.Count > 0)
                {
                    htmlText += "<p><b> Line Level Update:</b></p>";
                    tablestring += "<tr> <th> Line Number </th> <th> Field </th ><th> Previous value </th> <th> Current value </th></tr>";
                    var rewq = LineUpdates.Select(x => x.Item1).Distinct().ToList();
                    foreach (var r in rewq)
                    {
                        int op = 0;
                        foreach (var qwer in LineUpdates)
                        {
                            if (r == qwer.Item1)
                                if (op == 0)
                                {
                                    tablestring += " <tr><td> " + qwer.Item1 + " </td> <td> " + qwer.Item2 + "</td><td>  " + qwer.Item3 + " </td > <td> " + qwer.Item4 + " </td> </tr> ";
                                    op++;
                                }
                                else
                                    tablestring += " <tr><td> </td> <td> " + qwer.Item2 + "</td><td>  " + qwer.Item3 + " </td> <td> " + qwer.Item4 + " </td> </tr> ";


                        }


                    }
                }

                update_Historycase(data.CaseId, HistoryType, LinesDeleted, LinesAdded, LineUpdates, LineChange, CaseChange);

                if (htmlText != "")
                    sendEmailCase(data.CaseId, fc.CaseNumber, htmlText, "Update", tablestring);



            }
            else
            {
                return null;
            }
            var opt = CaseDetails.CaseNumber + "|" + data.CaseId;
            return opt.ToString();
        }

        public bool sendEmailCase(int CaseId, int CaseNumber, string htmlcontent, string updation, string tablestring = null)
        {
            var connection = new SqlConnection(ContextFactory.CrmConnectionString);
            var res = connection.Query<Person>("select p.* from [CRM].[Person] as p join CRM.CaseFollow as cf on cf.PersonId = p.PersonId  where cf.CaseId =@CaseId and cf.ModuleId in (1,2) and cf.IsFollow = 1", new { CaseId = CaseId }).ToList();
            // var frr_admin = connection.Query<Person>("select top 1 p.* from Auth.Users u join Auth.UsersInRoles ur on u.UserId = ur.UserId join CRM.Person p on u.PersonId = p.PersonId where u.FranchiseId = @FranchiseId and RoleId = 'A725282F-E606-4353-BB53-693F695D42A5'", new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).FirstOrDefault();
            var frr_adminn = connection.Query<Person>("select top 1 p.* from Auth.Users u join Auth.UsersInRoles ur on u.UserId = ur.UserId join CRM.Person p on u.PersonId = p.PersonId where u.FranchiseId = @FranchiseId and RoleId = 'A725282F-E606-4353-BB53-693F695D42A5'", new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
            Person frr_admin = new Person();

            if (SessionManager.BrandId == 1)
                foreach (Person p in frr_adminn)
                {
                    if (p.PrimaryEmail.ToUpper().Contains("BUDGETBLINDS.COM"))
                    {
                        frr_admin = p;
                    }
                }

            if (SessionManager.BrandId == 2)
                foreach (Person p in frr_adminn)
                {
                    if (p.PrimaryEmail.ToUpper().Contains("TAILOREDLIVING.COM"))
                    {
                        frr_admin = p;
                    }
                }

            if (SessionManager.BrandId == 3)
                foreach (Person p in frr_adminn)
                {
                    if (p.PrimaryEmail.ToUpper().Contains("CONCRETECRAFT.COM"))
                    {
                        frr_admin = p;
                    }
                }



            HttpContext context = HttpContext.Current;
            string baseUrl = context.Request.Url.Authority + context.Request.ApplicationPath.TrimEnd('/');

            //  string ServerImgPath = "http://" + baseUrl + "/#!/CaseView/" + CaseId;


            if (frr_admin.PrimaryEmail != null)
            {
                List<string> toRecipients = new List<string>();
                toRecipients = res.AsEnumerable().Select(r => r.PrimaryEmail).ToList();
                var resss = connection.Query<Person>("select * from [CRM].[Person]  where PersonId =(Select Owner_PersonId from [CRM].[FranchiseCase] where CaseId=@CaseId)", new { CaseId = CaseId }).ToList();
                toRecipients.Add(resss[0].PrimaryEmail);
                // toRecipients.Add("synctest8@budgetblinds.com");
                ExchangeManager mgr = new ExchangeManager();
                string HtmlText = File.ReadAllText(HttpContext.Current.Server.MapPath("~/Templates/Base/CaseUpdateEmailTemplate.html"));

                //    mgr.SendEmail(frr_admin.PrimaryEmail, "Case #: " + CaseNumber + " Comment added or Record Updated",
                //    "<a href=" + ServerImgPath + " target=" + "_blank" + ">View Case " + CaseNumber + "</a>", toRecipients);
                string subject = "";
                if (updation == "Update")
                {
                    subject += "Case #: " + CaseNumber.ToString() + " Record Updated ";

                    htmlcontent = "<p><b>Case # " + CaseNumber.ToString() + " has been Updated as follows.</b></p> <p><b>Updates to the Case:</b></p>" + htmlcontent;
                }
                else
                {
                    subject += "Case #: " + CaseNumber.ToString() + " Comments Added";
                    htmlcontent = " <p><b>New Comment has been added:</b></p>" + htmlcontent;   // <p><b>Case has been Updated as follows.</b></p>
                }

                HtmlText = HtmlText.Replace("{{MailContent}}", htmlcontent);
                HtmlText = HtmlText.Replace("{{TableData}}", tablestring);

                HtmlText = HtmlText.Replace("{{EndContent}}", "</br></br><p>Note: You have been sent this email because you are either noted on this Case as the Owner, have been tagged in the comment, or have chosen to Follow updates for this Case </p>");

                mgr.SendEmail(frr_admin.PrimaryEmail, subject, HtmlText, toRecipients);
            }

            return true;

        }


        public bool sendEmailForComments(int moduleId, int module, string htmlcontent, string updation, string[] emails)
        {
            var connection = new SqlConnection(ContextFactory.CrmConnectionString);

            if (module == 1)
            {

                var res = connection.Query<Person>("select p.* from [CRM].[Person] as p join CRM.CaseFollow as cf on cf.PersonId = p.PersonId  where cf.CaseId =@CaseId and cf.ModuleId in (1,2) and cf.IsFollow = 1", new { CaseId = moduleId }).ToList();
                // Getting franchise admin Email id
                var frr_admin = connection.Query<Person>("select top 1 p.* from Auth.Users u join Auth.UsersInRoles ur on u.UserId = ur.UserId join CRM.Person p on u.PersonId = p.PersonId where u.FranchiseId = @FranchiseId and RoleId = 'A725282F-E606-4353-BB53-693F695D42A5'", new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).FirstOrDefault();


                HttpContext context = HttpContext.Current;
                string baseUrl = context.Request.Url.Authority + context.Request.ApplicationPath.TrimEnd('/');

                if (frr_admin.PrimaryEmail != null)
                {
                    List<string> toRecipients = new List<string>();
                    toRecipients = res.AsEnumerable().Select(r => r.PrimaryEmail).ToList();
                    var resss = connection.Query<Person>("select * from [CRM].[Person]  where PersonId =(Select Owner_PersonId from [CRM].[FranchiseCase] where CaseId=@CaseId)", new { CaseId = moduleId }).ToList();
                    toRecipients.Add(resss[0].PrimaryEmail);
                    ExchangeManager mgr = new ExchangeManager();
                    var res_caseid = connection.Query<FranchiseCaseModel>("select * from [CRM].[FranchiseCase]  where CaseId=@CaseId", new { CaseId = moduleId }).FirstOrDefault();

                    // Add emails specified in comment to send list
                    foreach (string str in emails)
                    {
                        toRecipients.Add(str);
                    }

                    toRecipients = toRecipients.Distinct().ToList();
                    //toRecipients = null;
                    //toRecipients.Add("Muthurajs@outlook.com");

                    string subject = "";
                    //if (updation == "New")
                    //    subject = "New Comment Added For case " + res_caseid.CaseNumber;
                    //else if (updation == "Update")
                    //    subject = "Comment Updated For case " + res_caseid.CaseNumber;
                    //else if (updation == "Reply")    /// Case #: <1234> "Comment added
                    //    subject = "Replied comment For case " + res_caseid.CaseNumber; "Case #: "+ res_caseid.CaseNumber +"Comment added";
                    //else { }

                    if (updation == "New")
                        subject = "Case #: " + res_caseid.CaseNumber + " Comment added";
                    else if (updation == "Update")
                        subject = "Case #: " + res_caseid.CaseNumber + " Comment Updated";
                    else if (updation == "Reply")    /// Case #: <1234> "Comment added
                        subject = "Case #: " + res_caseid.CaseNumber + " Comment Replied"; //"Case #: " + res_caseid.CaseNumber + "Comment added";
                    else { }

                    string HtmlText = htmlcontent + "</br></br><p>Note: You have been sent this email because you are either noted on this Case as the Owner, have been tagged in the comment, or have chosen to Follow updates for this Case </p>";

                    mgr.SendEmail(frr_admin.PrimaryEmail, subject, HtmlText, toRecipients);
                }
            }
            else if (module == 2)
            {

                var res = connection.Query<Person>("select p.* from [CRM].[Person] as p join CRM.CaseFollow as cf on cf.PersonId = p.PersonId  where cf.CaseId =@CaseId and cf.ModuleId in (1,2) and cf.IsFollow = 1", new { CaseId = moduleId }).ToList();

                var frr_admin = connection.Query<Person>("select top 1 p.* from Auth.Users u join Auth.UsersInRoles ur on u.UserId = ur.UserId join CRM.Person p on u.PersonId = p.PersonId where u.FranchiseId = @FranchiseId and RoleId = 'A725282F-E606-4353-BB53-693F695D42A5'", new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).FirstOrDefault();


                HttpContext context = HttpContext.Current;
                string baseUrl = context.Request.Url.Authority + context.Request.ApplicationPath.TrimEnd('/');

                if (frr_admin.PrimaryEmail != null)
                {
                    List<string> toRecipients = new List<string>();
                    toRecipients = res.AsEnumerable().Select(r => r.PrimaryEmail).ToList();
                    var resss = connection.Query<Person>("select * from [CRM].[Person]  where PersonId =(Select Owner_PersonId from [CRM].[FranchiseCase] where CaseId=@CaseId)", new { CaseId = moduleId }).ToList();
                    toRecipients.Add(resss[0].PrimaryEmail);
                    ExchangeManager mgr = new ExchangeManager();
                    var res_caseid = connection.Query<FranchiseCaseModel>("select * from [CRM].[FranchiseCase]  where CaseId=@CaseId", new { CaseId = moduleId }).FirstOrDefault();

                    foreach (string str in emails)
                    {
                        toRecipients.Add(str);
                    }

                    toRecipients = toRecipients.Distinct().ToList();


                    string subject = "";
                    //if (updation == "New")
                    //    subject = "New Comment Added in Vendor view For case " + res_caseid.CaseNumber;
                    //else if (updation == "Update")
                    //    subject = "Comment Updated in Vendor view For case " + res_caseid.CaseNumber;
                    //else if (updation == "Reply")
                    //    subject = "Replied comment in Vendor view For case " + res_caseid.CaseNumber;
                    //else { }


                    if (updation == "New")
                        subject = "Case #: " + res_caseid.CaseNumber + " Comment added";
                    else if (updation == "Update")
                        subject = "Case #: " + res_caseid.CaseNumber + " Comment Updated";
                    else if (updation == "Reply")    /// Case #: <1234> "Comment added
                        subject = "Case #: " + res_caseid.CaseNumber + " Comment Replied"; //"Case #: " + res_caseid.CaseNumber + "Comment added";
                    else { }

                    string HtmlText = htmlcontent + "</br></br><p>Note: You have been sent this email because you are either noted on this Case as the Owner, have been tagged in the comment, or have chosen to Follow updates for this Case </p>";

                    mgr.SendEmail(frr_admin.PrimaryEmail, subject, HtmlText, toRecipients);

                }
            }
            else if (module == 3 || module == 4)
            {
                var res = connection.Query<Person>("select p.* from [CRM].[Person] as p join CRM.CaseFollow as cf on cf.PersonId = p.PersonId  where cf.CaseId =@CaseId and cf.ModuleId in (1,2) and cf.IsFollow = 1", new { CaseId = moduleId }).ToList();

                var userRes = connection.Query<User>(@"select * from Auth.Users u
                                                        join CRM.FranchiseCase fc on fc.Owner_PersonId = u.PersonId
                                                        where CaseId=@CaseId", new { CaseId = moduleId }).FirstOrDefault();
                var frr_admin = connection.Query<Person>("select top 1 p.* from Auth.Users u join Auth.UsersInRoles ur on u.UserId = ur.UserId join CRM.Person p on u.PersonId = p.PersonId where u.FranchiseId = @FranchiseId and RoleId = 'A725282F-E606-4353-BB53-693F695D42A5'", new { FranchiseId = userRes.FranchiseId }).FirstOrDefault();


                HttpContext context = HttpContext.Current;
                string baseUrl = context.Request.Url.Authority + context.Request.ApplicationPath.TrimEnd('/');

                if (frr_admin.PrimaryEmail != null)
                {
                    List<string> toRecipients = new List<string>();
                    toRecipients = res.AsEnumerable().Select(r => r.PrimaryEmail).ToList();
                    var resss = connection.Query<Person>("select * from [CRM].[Person]  where PersonId =(Select Owner_PersonId from [CRM].[FranchiseCase] where CaseId=@CaseId)", new { CaseId = moduleId }).ToList();
                    toRecipients.Add(resss[0].PrimaryEmail);
                    ExchangeManager mgr = new ExchangeManager();
                    var res_caseid = connection.Query<FranchiseCaseModel>("select * from [CRM].[FranchiseCase]  where CaseId=@CaseId", new { CaseId = moduleId }).FirstOrDefault();

                    foreach (string str in emails)
                    {
                        toRecipients.Add(str);
                    }

                    toRecipients = toRecipients.Distinct().ToList();


                    string subject = "";
                    //if (updation == "New")
                    //    subject = "New Comment Added in Vendor view For case " + res_caseid.CaseNumber;
                    //else if (updation == "Update")
                    //    subject = "Comment Updated in Vendor view For case " + res_caseid.CaseNumber;
                    //else if (updation == "Reply")
                    //    subject = "Replied comment in Vendor view For case " + res_caseid.CaseNumber;
                    //else { }


                    if (updation == "New")
                        subject = "Case #: " + res_caseid.CaseNumber + " Comment added";
                    else if (updation == "Update")
                        subject = "Case #: " + res_caseid.CaseNumber + " Comment Updated";
                    else if (updation == "Reply")    /// Case #: <1234> "Comment added
                        subject = "Case #: " + res_caseid.CaseNumber + " Comment Replied"; //"Case #: " + res_caseid.CaseNumber + "Comment added";
                    else { }

                    string HtmlText = htmlcontent + "</br></br><p>Note: You have been sent this email because you are either noted on this Case as the Owner, have been tagged in the comment, or have chosen to Follow updates for this Case </p>";

                    mgr.SendEmail(frr_admin.PrimaryEmail, subject, HtmlText, toRecipients);

                }
            }
            else
            {

            }

            return true;

        }

        public FranchiseCaseAddInfoModel GetCaseDataValue()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {

                {
                    var Query = "";

                    Query = @"Select Top 1 * from CRM.FranchiseCaseAddInfo order by Id desc";
                    var Value = connection.Query<FranchiseCaseAddInfoModel>(Query).FirstOrDefault();
                    return Value;
                }
            }
        }

        public FranchiseCaseModel GetDetailes(int Id)
        {
            if (Id != 0)
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {

                    {

                        var ress = connection.Query<FranchiseCaseModel>("Select * from CRM.FranchiseCase where CaseId=@CaseId", new { CaseId = Id }).FirstOrDefault();


                        //              var Query = @"Select fc.*,tl.Name as StatusValue,mpo.MasterPONumber as MPOID,pod.PICPO as POID,o.OrderNumber as SalesorderNumber,ql.VendorName from CRM.FranchiseCase fc 
                        //                           join CRM.Type_LookUpValues tl on fc.Status = tl.Id
                        //                           left join CRM.MasterPurchaseOrder mpo on fc.MPO_MasterPONum_POId = mpo.PurchaseOrderId
                        //                           left join CRM.PurchaseOrderDetails pod on pod.PurchaseOrdersDetailId = fc.VPO_PICPO_PODId
                        //                           join CRM.Orders o on o.OrderID = fc.SalesOrderId                                     
                        //left join CRM.FranchiseCaseAddInfo fcai on fcai.CaseId= fc.CaseId
                        //left join CRM.QuoteLines ql on fcai.QuoteLineId=ql.QuoteLineId
                        //                          where fc.CaseId =  @CaseId";

                        var Query = @"Select fc.*,tl.Name as StatusValue,mpo.MasterPONumber as MPOID,pod.PICPO as POID,ql.VendorName from CRM.FranchiseCase fc 
                                     join CRM.Type_LookUpValues tl on fc.Status = tl.Id
                                     left join CRM.MasterPurchaseOrder mpo on fc.MPO_MasterPONum_POId = mpo.PurchaseOrderId
                                     left join CRM.PurchaseOrderDetails pod on pod.PurchaseOrdersDetailId = fc.VPO_PICPO_PODId
									 left join CRM.FranchiseCaseAddInfo fcai on fcai.CaseId= fc.CaseId
									 left join CRM.QuoteLines ql on fcai.QuoteLineId=ql.QuoteLineId
                                    where fc.CaseId =  @CaseId";

                        var Value = connection.Query<FranchiseCaseModel>(Query, new { CaseId = Id }).FirstOrDefault();

                        var fcai = connection.Query<FranchiseCaseAddInfoModel>(@"select * from CRM.FranchiseCaseAddInfo where CaseId=@CaseId", new { CaseId = Id }).FirstOrDefault();

                        //var quotelinescount = connection.Query<QuoteLines>(@"select ql.* from CRM.Orders o
                        //                    join CRM.MasterPurchaseOrderExtn mpox on mpox.OrderId=o.OrderId
                        //                    join CRM.MasterPurchaseOrder mpo on mpo.PurchaseOrderId = mpox.PurchaseOrderId
                        //                    join CRM.QuoteLines ql on o.QuoteKey=ql.QuoteKey
                        //                    join CRM.PurchaseOrderDetails pod on mpo.PurchaseOrderId=pod.PurchaseOrderId and pod.QuoteLineId=ql.QuoteLineId
                        //                    where o.OrderId=@OrderId
                        //                    and ql.VendorId in (select VendorId from CRM.QuoteLines where QuoteLineId = @QuoteLineId) 
                        //                    and pod.StatusId not in (8,9,10)", new { QuoteLineId = fcai.QuoteLineId, OrderId = Value.SalesOrderId }).ToList();

                        int[] ia = Value.SalesOrderId.Split(',').Select(n => Convert.ToInt32(n)).ToArray();
                        Value.SalesorderNumber = connection.ExecuteScalar<string>(@"Select SUBSTRING(
                                                                            (
                                                                                 SELECT ',' + Cast(OrderNumber as varchar(max)) AS 'data()'
                                                                                     FROM CRM.Orders  where OrderID in @OrderId  FOR XML PATH('')
                                                                            ), 2, 9999) As OrderNumber", new { OrderId = ia });

                        var quotelinescount = connection.Query<QuoteLines>(@"select ql.* from CRM.Orders o
                                            left join CRM.MasterPurchaseOrderExtn mpox on mpox.OrderId=o.OrderId
                                            left join CRM.MasterPurchaseOrder mpo on mpo.PurchaseOrderId = mpox.PurchaseOrderId
                                            join CRM.QuoteLines ql on o.QuoteKey=ql.QuoteKey
                                            left join CRM.PurchaseOrderDetails pod on mpo.PurchaseOrderId=pod.PurchaseOrderId and pod.QuoteLineId=ql.QuoteLineId
                                            where o.OrderId in @OrderId
                                            and ql.VendorId in (select VendorId from CRM.QuoteLines where QuoteLineId = @QuoteLineId) 
                                            and isnull(pod.StatusId,-1) not in (8,9,10)", new { QuoteLineId = fcai.QuoteLineId, OrderId = ia }).ToList();
                        if (quotelinescount != null)
                            Value.noOfLines = quotelinescount.Count;
                        else
                            Value.noOfLines = 1;
                        //  Value.noOfLines = this.getLineNos(Convert.ToInt32(Value.VPO_PICPO_PODId), Value.MPO_MasterPONum_POId.ToString());
                        if (Value != null)
                        {
                            var User_Value = UserDetails(Value.CreatedBy);
                            Value.Name = User_Value.Name;
                            if (Value.CreatedBy == Value.LastUpdatedBy)
                            {
                                Value.LastUPdatedName = Value.Name;
                            }
                            else
                            {
                                var query = "";
                                query = @"select FirstName +''+ LastName as LastUPdatedName from CRM.Person Where PersonId=@PersonId";
                                var Last_modi = connection.Query<FranchiseCaseModel>(query, new { PersonId = Value.LastUpdatedBy }).FirstOrDefault();
                                Value.LastUPdatedName = Last_modi.LastUPdatedName;
                            }
                            var OthDetails = Details(Value.CaseId);
                            Value.AccountName = OthDetails.AccountName;
                            Value.CellPhone = OthDetails.CellPhone;
                            Value.PrimaryEmail = OthDetails.PrimaryEmail;
                            Value.SideMark = OthDetails.SideMark;
                            Value.FranchiseId = OthDetails.FranchiseId;
                            Value.FranchiseName = OthDetails.FranchiseName;

                            Value.CreatedOn = TimeZoneManager.ToLocal(Value.CreatedOn);
                            Value.LastUpdatedOn = TimeZoneManager.ToLocal(Value.LastUpdatedOn);
                            Value.DateTimeOpened = TimeZoneManager.ToLocal(Value.DateTimeOpened);
                            Value.DateTimeClosed = TimeZoneManager.ToLocal(Value.DateTimeClosed);

                        }

                        return Value;
                    }
                }
            }
            return null;
        }


        public FranchiseCaseModel GetDetailesHO(int Id)
        {
            if (Id != 0)
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {

                    {

                        var ress = connection.Query<FranchiseCaseModel>("Select * from CRM.FranchiseCase where CaseId=@CaseId", new { CaseId = Id }).FirstOrDefault();
                        //var Query = @"Select fc.*,tl.Name as StatusValue,mpo.MasterPONumber as MPOID,pod.PICPO as POID,o.OrderNumber as SalesorderNumber,ql.VendorName 
                        //             from CRM.FranchiseCase fc 
                        //             join [CRM].[FranchiseCaseAddInfo] fca on fc.CaseId=fca.CaseId
                        //             join CRM.Type_LookUpValues tl on fc.Status = tl.Id
                        //             join CRM.MasterPurchaseOrder mpo on fc.MPO_MasterPONum_POId = mpo.PurchaseOrderId
                        //             left join CRM.PurchaseOrderDetails pod on fca.QuoteLineId = pod.QuoteLineId
                        //             join CRM.Orders o on o.OrderID = fc.SalesOrderId
                        //             left join CRM.QuoteLines ql on pod.QuoteLineId=ql.QuoteLineId
                        //            where fc.CaseId = @CaseId and fca.VendorCaseNumber=@VendorCaseNumber";

                        var Query = @"Select fc.*,tl.Name as StatusValue,mpo.MasterPONumber as MPOID,pod.PICPO as POID,o.OrderNumber as SalesorderNumber,ql.VendorName 
                                     from CRM.FranchiseCase fc 
                                     join [CRM].[FranchiseCaseAddInfo] fca on fc.CaseId=fca.CaseId
                                     join CRM.Type_LookUpValues tl on fc.Status = tl.Id
									 join CRM.Orders o on o.OrderID = fc.SalesOrderId
									 left join CRM.QuoteLines ql on fca.QuoteLineId=ql.QuoteLineId
                                     left join CRM.MasterPurchaseOrder mpo on fc.MPO_MasterPONum_POId = mpo.PurchaseOrderId
                                     left join CRM.PurchaseOrderDetails pod on fca.QuoteLineId = pod.QuoteLineId
                                    where fc.CaseId =@CaseId";

                        var Value = connection.Query<FranchiseCaseModel>(Query, new { CaseId = Id}).FirstOrDefault();


                        // Commenting this coz not required ~sm
                        //var fcai = connection.Query<FranchiseCaseAddInfoModel>(@"select * from CRM.FranchiseCaseAddInfo where CaseId=@CaseId", new { CaseId = Id }).FirstOrDefault();

                        //var quotelinescount = connection.Query<QuoteLines>(@"select ql.* from CRM.Orders o
                        //                   join CRM.MasterPurchaseOrderExtn mpox on mpox.OrderId = o.OrderId

                        //                    join CRM.MasterPurchaseOrder mpo on mpo.PurchaseOrderId = mpox.PurchaseOrderId
                        //                    join CRM.QuoteLines ql on o.QuoteKey=ql.QuoteKey
                        //                    join CRM.PurchaseOrderDetails pod on mpo.PurchaseOrderId=pod.PurchaseOrderId and pod.QuoteLineId=ql.QuoteLineId
                        //                    where o.OrderId=@OrderId
                        //                    and ql.VendorId in (select VendorId from CRM.QuoteLines where QuoteLineId = @QuoteLineId) 
                        //                    and pod.StatusId not in (8,9,10)", new { QuoteLineId = fcai.QuoteLineId, OrderId = Value.SalesOrderId }).ToList();
                        //if (quotelinescount != null)
                        //    Value.noOfLines = quotelinescount.Count;
                        //else
                        //    Value.noOfLines = 1;
                       
                        if (Value != null)
                        {
                            var User_Value = UserDetails(Value.CreatedBy);
                            Value.Name = User_Value.Name;
                            if (Value.CreatedBy == Value.LastUpdatedBy)
                            {
                                Value.LastUPdatedName = Value.Name;
                            }
                            else
                            {
                                var query = "";
                                query = @"select FirstName +''+ LastName as LastUPdatedName from CRM.Person Where PersonId=@PersonId";
                                var Last_modi = connection.Query<FranchiseCaseModel>(query, new { PersonId = Value.LastUpdatedBy }).FirstOrDefault();
                                Value.LastUPdatedName = Last_modi.LastUPdatedName;
                            }
                            var OthDetails = Details(Value.CaseId);
                            Value.AccountName = OthDetails.AccountName;
                            Value.CellPhone = OthDetails.CellPhone;
                            Value.PrimaryEmail = OthDetails.PrimaryEmail;
                            Value.SideMark = OthDetails.SideMark;
                            Value.FranchiseId = OthDetails.FranchiseId;
                            Value.FranchiseName = OthDetails.FranchiseName;

                            Value.CreatedOn = TimeZoneManager.ToLocal(Value.CreatedOn);
                            Value.LastUpdatedOn = TimeZoneManager.ToLocal(Value.LastUpdatedOn);
                            Value.DateTimeOpened = TimeZoneManager.ToLocal(Value.DateTimeOpened);
                            Value.DateTimeClosed = TimeZoneManager.ToLocal(Value.DateTimeClosed);

                        }

                        return Value;
                    }
                }
            }
            return null;
        }
        public FranchiseCaseModel GetDetailesFranchiseVendor(int Id, string vendorid)
        {
            if (Id != 0)
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {

                    {

                        // var ress = connection.Query<FranchiseCaseModel>("Select * from CRM.FranchiseCase where CaseId=@CaseId", new { CaseId = Id }).FirstOrDefault();
                        var Query = @"Select fc.*,tl.Name as StatusValue,mpo.MasterPONumber as MPOID,pod.PICPO as POID,o.OrderNumber as SalesorderNumber from CRM.FranchiseCase fc 
                                     join CRM.Type_LookUpValues tl on fc.Status = tl.Id
                                     left join CRM.MasterPurchaseOrder mpo on fc.MPO_MasterPONum_POId = mpo.PurchaseOrderId
                                     left join CRM.PurchaseOrderDetails pod on pod.PurchaseOrdersDetailId = fc.VPO_PICPO_PODId
                                     join CRM.Orders o on o.OrderID = fc.SalesOrderId
                                    where fc.CaseId = @CaseId";

                        var Value = connection.Query<FranchiseCaseModel>(Query, new { CaseId = Id }).FirstOrDefault();
                        if (Value != null)
                        {
                            var User_Value = UserDetails(Value.CreatedBy);
                            Value.Name = User_Value.Name;
                            if (Value.CreatedBy == Value.LastUpdatedBy)
                            {
                                Value.LastUPdatedName = Value.Name;
                            }
                            else
                            {
                                var query = "";
                                query = @"select FirstName +''+ LastName as LastUPdatedName from CRM.Person Where PersonId=@PersonId";
                                var Last_modi = connection.Query<FranchiseCaseModel>(query, new { PersonId = Value.LastUpdatedBy }).FirstOrDefault();
                                Value.LastUPdatedName = Last_modi.LastUPdatedName;
                            }
                            var OthDetails = Details(Value.CaseId);
                            Value.AccountName = OthDetails.AccountName;
                            Value.CellPhone = OthDetails.CellPhone;
                            Value.PrimaryEmail = OthDetails.PrimaryEmail;
                            Value.SideMark = OthDetails.SideMark;

                            Value.CreatedOn = TimeZoneManager.ToLocal(Value.CreatedOn);
                            Value.LastUpdatedOn = TimeZoneManager.ToLocal(Value.LastUpdatedOn);
                            Value.DateTimeOpened = TimeZoneManager.ToLocal(Value.DateTimeOpened);
                            Value.DateTimeClosed = TimeZoneManager.ToLocal(Value.DateTimeClosed);

                        }

                        var vendorcase = connection.Query<VendorCase>(@"select * from CRM.VendorCase 
                                                                                    where CaseId = @CaseId and VendorId = (select top(1) ql.VendorId from CRM.QuoteLines ql
                                                                                    join CRM.FranchiseCaseAddInfo fcai on fcai.QuoteLineId = ql.QuoteLineId
                                                                                    where fcai.VendorCaseNumber = @VendorCaseNumber)", new { VendorCaseNumber = vendorid, CaseId = Id }).FirstOrDefault();
                        if (vendorcase != null)
                        {
                            Value.VendorCaseNumber = vendorcase.VendorCaseNumber;
                        }



                        return Value;
                    }
                }
            }
            return null;
        }

        public FranchiseCaseDropModel Details(int Id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {

                {
                    var Query = "";
                    //           Query = @"Select mpo.PurchaseOrderId,(select [CRM].[fnGetAccountNameByAccountId](ac.AccountId)) as AccountName,
                    //                                                    (select 
                    //                                                    case when PreferredTFN='C' then CellPhone
                    //                                                    else case when PreferredTFN='H' then HomePhone 
                    //                                                    else case when PreferredTFN='W' then WorkPhone
                    //                                                    else case when CellPhone is not null then CellPhone 
                    //                                                    else case when HomePhone is not null then HomePhone 
                    //                                                    else case when WorkPhone is not null then WorkPhone 
                    //                                                    else '' 
                    //                                                    end end end end end end) as CellPhone,c.PrimaryEmail,q.SideMark,
                    //                     fci.MPO_MasterPONum_POId,fe.FranchiseId,fe.Name as FranchiseName from CRM.FranchiseCase fci 
                    //                     join CRM.MasterPurchaseOrder mpo on fci.MPO_MasterPONum_POId=mpo.PurchaseOrderId
                    //                       join CRM.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                    //                     join CRM.Orders o on mpox.OrderId=o.OrderID
                    //                     join CRM.Accounts ac on mpox.AccountId=ac.AccountId
                    //                     join CRM.AccountCustomers acc on ac.AccountId=acc.AccountId
                    //                     join CRM.Customer c on acc.CustomerId=c.CustomerId
                    //                     join CRM.Quote q on o.QuoteKey=q.QuoteKey
                    //join CRM.Opportunities op on mpox.OpportunityId=op.OpportunityId
                    //join CRM.Franchise fe on op.FranchiseId=fe.FranchiseId
                    //                     where CaseId =@CaseId";

                    Query = @"Select mpo.PurchaseOrderId,(select [CRM].[fnGetAccountNameByAccountId](ac.AccountId)) as AccountName,
                                                             (select 
                                                             case when PreferredTFN='C' then CellPhone
                                                             else case when PreferredTFN='H' then HomePhone 
                                                             else case when PreferredTFN='W' then WorkPhone
                                                             else case when CellPhone is not null then CellPhone 
                                                             else case when HomePhone is not null then HomePhone 
                                                             else case when WorkPhone is not null then WorkPhone 
                                                             else '' 
                                                             end end end end end end) as CellPhone,c.PrimaryEmail,q.SideMark,
                              fci.MPO_MasterPONum_POId,fe.FranchiseId,fe.Name as FranchiseName from CRM.FranchiseCase fci 
							  join CRM.FranchiseCaseAddInfo fcai on fci.CaseId= fcai.CaseId
							  join CRM.QuoteLines ql on ql.QuoteLineId= fcai.QuoteLineId
							  join CRM.Quote q on ql.QuoteKey=q.QuoteKey
							  join CRM.Opportunities op on q.OpportunityId=op.OpportunityId
							  join CRM.Orders o on ql.QuoteKey=o.QuoteKey                             
                              join CRM.Accounts ac on op.AccountId=ac.AccountId
                              join CRM.AccountCustomers acc on ac.AccountId=acc.AccountId
                              join CRM.Customer c on acc.CustomerId=c.CustomerId							  
                              left join CRM.MasterPurchaseOrder mpo on fci.MPO_MasterPONum_POId=mpo.PurchaseOrderId
                              left join CRM.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId                              
							  join CRM.Franchise fe on op.FranchiseId=fe.FranchiseId
                              where fci.CaseId = @CaseId";
                    var Value = connection.Query<FranchiseCaseDropModel>(Query, new { CaseId = Id }).FirstOrDefault();
                    return Value;
                }
            }
        }

        public FranchiseCaseDropModel GetVpo(int num)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {

                {
                    var Query = "";
                    Query = @"select PurchaseOrderId,PICPO as VPO_PICPO_PODId,PurchaseOrdersDetailId, QuoteLineId
                             from CRM.PurchaseOrderDetails where PurchaseOrdersDetailId=@Id";
                    var Value = connection.Query<FranchiseCaseDropModel>(Query, new { Id = num }).FirstOrDefault();
                    return Value;
                }
            }
        }

        public int getVendorId(int FranchiseVendorcaseNumber)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {

                {
                    var Query = "";
                    Query = @"select QL.* from CRM.QuoteLines  QL
                                join CRM.FranchiseCaseAddInfo FCAI on FCAI.QuoteLineId = ql.QuoteLineId
                                where FCAI.VendorCaseNumber = @Id";
                    var Value = connection.Query<QuoteLines>(Query, new { Id = FranchiseVendorcaseNumber }).FirstOrDefault();
                    Value.VendorId = Value.VendorId != null ? Value.VendorId : 0;
                    return Convert.ToInt32(Value.VendorId);
                }
            }
        }

        public List<dynamic> GetLineData(int id, int Mpoid, string linenos, string lineNo, string OrderId)
        {
            int[] ia = OrderId.Split(',').Select(n => Convert.ToInt32(n)).ToArray();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                // var podres = connection.ExecuteScalar<int>()
            }

            if (id != 0)
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {

                    {
                        var Query = "";
                        var get_value = GetVpo(id);
                        if (get_value.VPO_PICPO_PODId != 0)
                        {
                            //Query = @"select po.QuoteLineId,ql.QuoteLineNumber as QuoteLineNumber
                            //          from CRM.PurchaseOrderDetails po
                            //          join CRM.QuoteLines ql on po.QuoteLineId=ql.QuoteLineId
                            //          where po.PICPO=@PicPo and po.PurchaseOrderId=@PurchaseOrderId and ql.ProductTypeId not in (4)  and po.StatusId not in(8,9,10) order by QuoteLineNumber";

                            Query = @"select po.QuoteLineId,CAST(o.OrderNumber as varchar(10)) + '-' + CAST(ql.QuoteLineNumber as varchar(3)) as QuoteLineNumber
                                      from CRM.PurchaseOrderDetails po
									  join CRM.MasterPurchaseOrderExtn mpoe on mpoe.PurchaseOrderId=po.PurchaseOrderId
									  join CRM.Orders o on o.OrderID = mpoe.OrderId
                                      join CRM.QuoteLines ql on po.QuoteLineId=ql.QuoteLineId and ql.ProductTypeId <> 0
                                      where po.PICPO=@PicPo and po.PurchaseOrderId=@PurchaseOrderId
                                        and o.OrderID in @OrderID and ql.ProductTypeId not in (4)  and po.StatusId not in(8,9,10) order by QuoteLineNumber";
                            var Value = connection.Query(Query, new { OrderID = ia, PicPo = get_value.VPO_PICPO_PODId, PurchaseOrderId = get_value.PurchaseOrderId }).ToList();

                            //  var arrint = Array.ConvertAll(linenos.Split(','), string.Parse);
                            var arrint = linenos.Split(',');

                            foreach (string nn in arrint)
                            {
                                Value.Remove(Value.SingleOrDefault(r => r.QuoteLineNumber == nn));
                            }
                            return Value;

                        }
                        else
                            return null;

                    }
                }

            }
            if (Mpoid > 0)
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {

                    if (lineNo == "0")
                    {
                        var Query = "";

                        Query = @"select pod.QuoteLineId,CAST(o.OrderNumber as varchar(10)) + '-' + CAST(ql.QuoteLineNumber as varchar(3)) as QuoteLineNumber
                                 from CRM.MasterPurchaseOrder po
                                 join CRM.PurchaseOrderDetails pod on po.PurchaseOrderId=pod.PurchaseOrderId
                                 join CRM.QuoteLines ql on pod.QuoteLineId=ql.QuoteLineId and ql.ProductTypeId <> 0
                                 join CRM.Orders o on ql.QuoteKey=o.QuoteKey
                                 where o.OrderID in @OrderID and pod.PurchaseOrderId=@PurchaseOrderId and ql.ProductTypeId not in (4) and pod.StatusId not in(8,9,10) order by QuoteLineNumber";
                        var Value = connection.Query(Query, new { OrderID = ia, PurchaseOrderId = Mpoid }).ToList();

                        var arrint = linenos.Split(',');

                        foreach (string nn in arrint)
                        {
                            Value.Remove(Value.SingleOrDefault(r => r.QuoteLineNumber == nn));
                        }

                        return Value;
                    }
                    else
                    {
                        var Query = "";
                        int val = 0;
                        if (lineNo.Contains('-'))
                        {
                            var arrrint = lineNo.Split('-');
                            val = Convert.ToInt32(arrrint[1]);
                        }
                        else if (lineNo != "0") val = Convert.ToInt32(lineNo);


                        var result = connection.Query<QuoteLines>(@"Select  ql.* from CRM.PurchaseOrderDetails pod
                                 join CRM.MasterPurchaseOrder mpo on pod.PurchaseOrderId=mpo.PurchaseOrderId  
                                    join CRM.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId=mpo.PurchaseOrderId  
                                    join CRM.Orders o on mpox.OrderId=o.OrderID
                                 join CRM.Quote q on o.QuoteKey=q.QuoteKey 
                                 join CRM.QuoteLines ql on q.QuoteKey=ql.QuoteKey and pod.QuoteLineId=ql.QuoteLineId 
                                 where mpo.PurchaseOrderId = @PurchaseOrderId and ql.ProductTypeId not in (4) and ql.QuoteLineNumber=@QuoteLineNumber",
                                new { PurchaseOrderId = Mpoid, QuoteLineNumber = val }).FirstOrDefault();

                        if (result.VendorId != null && result.VendorId != 0)
                        {

                            //var Queryy = @"select pod.QuoteLineId,ql.QuoteLineNumber
                            //     from CRM.MasterPurchaseOrder po
                            //     join CRM.PurchaseOrderDetails pod on po.PurchaseOrderId=pod.PurchaseOrderId
                            //     join CRM.QuoteLines ql on pod.QuoteLineId=ql.QuoteLineId
                            //     where pod.PurchaseOrderId=@PurchaseOrderId and ql.ProductTypeId not in (4) and pod.StatusId not in(8,9,10) and
                            //     ql.QuoteKey=@QuoteKey and ql.VendorId = @VendorId order by QuoteLineNumber";

                            var Queryy = @"select pod.QuoteLineId,CAST(o.OrderNumber as varchar(10)) + '-' + CAST(ql.QuoteLineNumber as varchar(3)) as QuoteLineNumber
                                 from CRM.MasterPurchaseOrder po
                                 join CRM.PurchaseOrderDetails pod on po.PurchaseOrderId=pod.PurchaseOrderId
                                 join CRM.QuoteLines ql on pod.QuoteLineId=ql.QuoteLineId and ql.ProductTypeId <> 0
                                 join CRM.Orders o on o.QuoteKey = ql.QuoteKey
                                 where o.OrderID in @OrderID and pod.PurchaseOrderId=@PurchaseOrderId and ql.ProductTypeId not in (4) and pod.StatusId not in(8,9,10) and
                                  ql.VendorId = @VendorId order by QuoteLineNumber";  // ql.QuoteKey=@QuoteKey and

                            var Value = connection.Query(Queryy, new { OrderID = ia, PurchaseOrderId = Mpoid, VendorId = @result.VendorId }).ToList();

                            var arrint = linenos.Split(',');
                            foreach (string nn in arrint)
                            {
                                Value.Remove(Value.SingleOrDefault(r => r.QuoteLineNumber == nn));
                            }

                            return Value;
                        }
                        else
                        {
                            //Query = @"select pod.QuoteLineId,ql.QuoteLineNumber
                            //     from CRM.MasterPurchaseOrder po
                            //     join CRM.PurchaseOrderDetails pod on po.PurchaseOrderId=pod.PurchaseOrderId
                            //     join CRM.QuoteLines ql on pod.QuoteLineId=ql.QuoteLineId
                            //     where pod.PurchaseOrderId=@PurchaseOrderId  and pod.StatusId not in(8,9,10) and
                            //     ql.QuoteKey=@QuoteKey and ql.ProductTypeId not in (4) and ql.QuoteLineNumber = @QuoteLineNumber  order by QuoteLineNumber";

                            int vall = 0;
                            if (lineNo.Contains('-'))
                            {
                                var arrrint = lineNo.Split('-');
                                vall = Convert.ToInt32(arrrint[1]);
                            }
                            else if (lineNo != "0") val = Convert.ToInt32(lineNo);

                            Query = @"select pod.QuoteLineId,CAST(o.OrderNumber as varchar(10)) + '-' + CAST(ql.QuoteLineNumber as varchar(3)) as QuoteLineNumber
                                 from CRM.MasterPurchaseOrder po
                                 join CRM.PurchaseOrderDetails pod on po.PurchaseOrderId=pod.PurchaseOrderId
                                 join CRM.QuoteLines ql on pod.QuoteLineId=ql.QuoteLineId and ql.ProductTypeId <> 0
                                 join CRM.Orders o on o.QuoteKey = ql.QuoteKey
                                 where o.OrderID in @OrderID and pod.PurchaseOrderId=@PurchaseOrderId  and pod.StatusId not in(8,9,10) and
                                 ql.QuoteKey=@QuoteKey and ql.ProductTypeId not in (4) and ql.QuoteLineNumber = @QuoteLineNumber  order by QuoteLineNumber";
                            var Value = connection.Query(Query, new { OrderID = ia, PurchaseOrderId = Mpoid, QuoteKey = result.QuoteKey, QuoteLineNumber = vall }).ToList();
                            var arrint = linenos.Split(',');

                            foreach (string nn in arrint)
                            {
                                Value.Remove(Value.SingleOrDefault(r => r.QuoteLineNumber == nn));
                            }
                            return Value;
                        }
                    }


                }
            }
            if (OrderId != "" && OrderId != null)
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    if (lineNo == "0")
                    {
                        var Query = "";

                        Query = @" select ql.QuoteLineId,CAST(po.OrderNumber as varchar(10)) + '-' + CAST(ql.QuoteLineNumber as varchar(3)) as QuoteLineNumber
                                 from CRM.Orders po
								 join CRM.QuoteLines ql on ql.QuoteKey=po.QuoteKey and ql.ProductTypeId <> 0
								 left join CRM.PurchaseOrderDetails pod on pod.QuoteLineId = ql.QuoteLineId 
								 where po.OrderID in @OrderID and
								 isnull(pod.StatusId,-1) not in(8,9,10) and
								 ql.ProductTypeId not in (4) order by QuoteLineNumber"; //   --to get linenumber on mpo";
                        var Value = connection.Query(Query, new { OrderID = ia, }).ToList();
                        var arrint = linenos.Split(',');

                        foreach (string nn in arrint)
                        {
                            Value.Remove(Value.SingleOrDefault(r => r.QuoteLineNumber == nn));
                        }
                        return Value;
                    }
                    else
                    {

                        var Query = "";

                        int val = 0;
                        if (lineNo.Contains('-'))
                        {
                            var arrrint = lineNo.Split('-');
                            val = Convert.ToInt32(arrrint[1]);
                        }
                        else if (lineNo != "0") val = Convert.ToInt32(lineNo);

                        Query = @"with CTE
                                    AS(
                                    select top 1 QL.* from CRM.QuoteLines QL
                                    join CRM.Orders O on O.QuoteKey = QL.QuoteKey
                                    where O.OrderID in @OrderID and QL.QuoteLineNumber=@QuoteLineNumber
                                    )
                                 select ql.QuoteLineId,CAST(po.OrderNumber as varchar(10)) + '-' + CAST(ql.QuoteLineNumber as varchar(3)) as QuoteLineNumber
                                 from CRM.Orders po
								 join CRM.QuoteLines ql on ql.QuoteKey=po.QuoteKey and ql.ProductTypeId <> 0
								 left join CRM.PurchaseOrderDetails pod on pod.QuoteLineId = ql.QuoteLineId 
								 where OrderID=@OrderID and
								 isnull(pod.StatusId,-1) not in(8,9,10) and
								 ql.ProductTypeId not in (4) and ql.VendorId= isnull((select VendorId from CTE),-1) 
								 order by QuoteLineNumber"; //   --to get linenumber on mpo";
                        var Value = connection.Query(Query, new { OrderID = ia, QuoteLineNumber = val }).ToList();
                        var arrint = linenos.Split(',');

                        foreach (string nn in arrint)
                        {
                            Value.Remove(Value.SingleOrDefault(r => r.QuoteLineNumber == nn));
                        }
                        return Value;
                    }
                }

            }
            return null;
        }

        public int getLineNos(int[] orderid, int id, string Mpoid)
        {
            if (id != 0)
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {

                    {
                        var Query = "";
                        var get_value = GetVpo(id);
                        if (get_value.VPO_PICPO_PODId != 0)
                        {
                            if (orderid == null)
                            {
                                Query = @"select po.QuoteLineId,ql.QuoteLineNumber as QuoteLineNumber
                                      from CRM.PurchaseOrderDetails po
                                      join CRM.QuoteLines ql on po.QuoteLineId=ql.QuoteLineId and ql.ProductTypeId <> 0
                                      where po.PICPO=@PicPo and po.PurchaseOrderId=@PurchaseOrderId  and po.StatusId not in(8,9,10) order by QuoteLineNumber";
                            }
                            else
                                Query = @"
			                            select po.QuoteLineId,ql.QuoteLineNumber as QuoteLineNumber
                                        from CRM.PurchaseOrderDetails po
                                        join CRM.QuoteLines ql on po.QuoteLineId=ql.QuoteLineId and ql.ProductTypeId <> 0
			                            join CRM.Quote q on q.QuoteKey= ql.QuoteKey
			                            join CRM.Orders o on o.QuoteKey= q.QuoteKey
                                        where
			                            o.OrderID in @orderid and po.PICPO=@PicPo and po.PurchaseOrderId=@PurchaseOrderId  and po.StatusId not in(8,9,10) order by QuoteLineNumber";
                            var Value = connection.Query(Query, new { orderid = orderid, PicPo = get_value.VPO_PICPO_PODId, PurchaseOrderId = get_value.PurchaseOrderId }).ToList();


                            if (Value.Count > 0) return Value.Count; else return 0;


                        }
                        else
                            return 0;

                    }
                }

            }
            if (Mpoid != "null")
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {

                    {
                        var Query = "";

                        if (orderid == null)
                        {
                            Query = @"select pod.QuoteLineId,ql.QuoteLineNumber
                                 from CRM.MasterPurchaseOrder po
                                 join CRM.PurchaseOrderDetails pod on po.PurchaseOrderId=pod.PurchaseOrderId
                                 join CRM.QuoteLines ql on pod.QuoteLineId=ql.QuoteLineId and ql.ProductTypeId <> 0
                                 where pod.PurchaseOrderId=@PurchaseOrderId  and pod.StatusId not in(8,9,10) order by QuoteLineNumber"; //   --to get linenumber on mpo";
                        }
                        else
                        {
                            Query = @"select pod.QuoteLineId,ql.QuoteLineNumber
                                 from CRM.MasterPurchaseOrder po
                                 join CRM.PurchaseOrderDetails pod on po.PurchaseOrderId=pod.PurchaseOrderId
                                 join CRM.QuoteLines ql on pod.QuoteLineId=ql.QuoteLineId and ql.ProductTypeId <> 0
								 join CRM.Quote q on q.QuoteKey= ql.QuoteKey
							 	 join CRM.Orders o on o.QuoteKey= q.QuoteKey
                                 where
								 o.OrderID in @orderid and pod.PurchaseOrderId=@PurchaseOrderId  and pod.StatusId not in(8,9,10) order by QuoteLineNumber";
                        }
                        var Value = connection.Query(Query, new { orderid = orderid, PurchaseOrderId = Mpoid }).ToList();


                        if (Value.Count > 0)
                            return Value.Count;
                        else
                            return 0;
                    }
                }
            }
            return 0;
        }

        public FranchiseCaseAddInfoModel GetOtherLineData(int id, int OrderId, int POID, int PODID)
        {

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {

                {
                    var Query = "";
                    var ql = connection.Query<QuoteLines>("Select * from CRM.QuoteLines where QuoteLineId =@QuoteLineId", new { QuoteLineId = id }).FirstOrDefault();

                    if (ql != null)
                    {
                        //if (ql.ProductTypeId == 2)
                        //    Query = @"select ql.VendorId,                               
                        //           case when po.PartNumber ='' then '0' else po.PartNumber end as ProductId,
                        //        ql.Description,
                        //        po.PurchaseOrdersDetailId,
                        //        po.PICPO as VPO,
                        //        (select distinct ProductName from CRM.FranchiseProducts where ProductKey=ql.ProductId) as ProductName,
                        //        (select distinct VendorName from Acct.FranchiseVendors where VendorId=ql.VendorId) as VendorName,po.VendorReference
                        //        from CRM.QuoteLines ql
                        //        left left join CRM.PurchaseOrderDetails po on ql.QuoteLineId=po.QuoteLineId
                        //        where ql.QuoteLineId=@QuoteLineId";
                        //else
                        Query = @"select ql.VendorId,
                               case when po.PartNumber ='' and ql.ProductId < 1 then '0' else
							   case when po.PartNumber <> '' then  po.PartNumber else
							   case when ql.ProductId > 0 and ql.ProductTypeId > 1 then 
                                ( select ProductID from CRM.FranchiseProducts where ProductKey = ql.ProductId and FranchiseId=@fId)
							    else 0 end end end as ProductId 
							    ,ql.Description,po.PurchaseOrdersDetailId,po.PICPO as VPO,
                            case when ql.ProductName <> '' and ql.ProductName IS NOT NULL  then ql.ProductName else
                            case when ql.ProductId=0 then '' else case when exists(select ProductId from CRM.HFCProducts where ProductId=ql.ProductId) 
                            then (select ProductGroupDesc from CRM.HFCProducts where ProductId=ql.ProductId) 
                            else case when exists(select ProductId from CRM.FranchiseProducts where ProductId=ql.ProductId) 
                            then (select distinct ProductName from CRM.FranchiseProducts where ProductId=ql.ProductId) else '' end end end end  as ProductName,
                            case when ql.VendorId is null and ql.VendorId=0 then '' else case when exists(select VendorId from Acct.HFCVendors where VendorId=ql.VendorId) 
                            then (select distinct VendorName from Acct.HFCVendors where VendorId=ql.VendorId) 
                            else case when exists(select VendorId from Acct.FranchiseVendors where VendorId=ql.VendorId) 
                            then (select distinct VendorName from Acct.FranchiseVendors where ProductId=ql.VendorId) else '' end end end  as VendorName, po.VendorReference
                            from CRM.QuoteLines ql
                            left join CRM.PurchaseOrderDetails po on ql.QuoteLineId=po.QuoteLineId
                            where ql.QuoteLineId=@QuoteLineId";  // (select distinct ProductID from CRM.FranchiseProducts where ProductKey = ql.ProductId)


                        //                 Query = @"select ql.VendorId,
                        //                        case when po.PartNumber ='' then '0' else po.PartNumber end as ProductId,ql.Description,po.PurchaseOrdersDetailId,po.PICPO as VPO,
                        //                     case when ql.ProductName <> '' and ql.ProductName IS NOT NULL  then ql.ProductName
                        //else
                        //                     case when ql.ProductId=0 then '' else case when exists(select ProductId from CRM.HFCProducts where ProductId=ql.ProductId) 
                        //                     then (select ProductGroupDesc from CRM.HFCProducts where ProductId=ql.ProductId) 
                        //                     else case when exists(select ProductId from CRM.FranchiseProducts where ProductId=ql.ProductId) 
                        //                     then (select distinct ProductName from CRM.FranchiseProducts where ProductId=ql.ProductId) else '' end end end end  as ProductName,
                        //                     case when ql.VendorId is null and ql.VendorId=0 then '' else case when exists(select VendorId from Acct.HFCVendors where VendorId=ql.VendorId) 
                        //                     then (select distinct VendorName from Acct.HFCVendors where VendorId=ql.VendorId) 
                        //                     else case when exists(select VendorId from Acct.FranchiseVendors where VendorId=ql.VendorId) 
                        //                     then (select distinct VendorName from Acct.FranchiseVendors where ProductId=ql.VendorId) else '' end end end  as VendorName, po.VendorReference
                        //                     from CRM.QuoteLines ql
                        //                     left join CRM.PurchaseOrderDetails po on ql.QuoteLineId=po.QuoteLineId
                        //                     where ql.QuoteLineId=@QuoteLineId";

                        var Value = connection.Query<FranchiseCaseAddInfoModel>(Query, new { QuoteLineId = id, fId = SessionManager.CurrentFranchise.FranchiseId }).FirstOrDefault();

                        var quotelines = connection.Query<QuoteLines>("select VendorId from CRM.QuoteLines where QuoteLineId=@QuoteLineId", new { QuoteLineId = id }).FirstOrDefault();
                        if (quotelines.VendorId != null)  //  && quotelines.VendorId > 0
                        {
                            //var quotelinescount = connection.Query<QuoteLines>(@"select ql.* from CRM.Orders o
                            //                join CRM.MasterPurchaseOrderExtn mpox on mpox.OrderId = o.OrderId
                            //                join CRM.MasterPurchaseOrder mpo on mpo.PurchaseOrderId = mpox.PurchaseOrderId
                            //                join CRM.QuoteLines ql on o.QuoteKey=ql.QuoteKey
                            //                join CRM.PurchaseOrderDetails pod on mpo.PurchaseOrderId=pod.PurchaseOrderId and pod.QuoteLineId=ql.QuoteLineId
                            //                where o.OrderId=@OrderId
                            //                and ql.VendorId in (select VendorId from CRM.QuoteLines where QuoteLineId = @QuoteLineId) 
                            //                and pod.StatusId not in (8,9,10)", new { QuoteLineId = id, OrderId = OrderId }).ToList();
                            var quotelinescount = connection.Query<QuoteLines>(@"select ql.* from CRM.Orders o
                                            left join CRM.MasterPurchaseOrderExtn mpox on mpox.OrderId = o.OrderId
                                            left join CRM.MasterPurchaseOrder mpo on mpo.PurchaseOrderId = mpox.PurchaseOrderId
                                            join CRM.QuoteLines ql on o.QuoteKey=ql.QuoteKey
                                            left join CRM.PurchaseOrderDetails pod on mpo.PurchaseOrderId=pod.PurchaseOrderId and pod.QuoteLineId=ql.QuoteLineId
                                            where o.OrderId=@OrderId
                                            and ql.VendorId in (select isnull(VendorId,-1) from CRM.QuoteLines where QuoteLineId = @QuoteLineId) 
                                            and isnull(pod.StatusId,-1) not in (8,9,10)", new { QuoteLineId = id, OrderId = OrderId }).ToList();

                            Value.NoofLines = quotelinescount.Count;
                        }
                        else
                        {
                            Value.NoofLines = 1;
                        }
                        return Value;
                    }

                    var val = SessionManager.CurrentFranchise.FranchiseId;
                    return null;
                }
            }
        }

        public List<FranchiseCaseAddInfoModel> GetLinesForVpoChange(int id, int lineNo, int OrderId)
        {

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {

                {
                    var Query = "";
                    //Query = @"select ql.VendorId,
                    //         (select PICProductID from CRM.HFCProducts where ProductID =ql.ProductId) as  ProductId,ql.Description,po.PurchaseOrdersDetailId,po.PICPO as VPO,
                    //        case when ql.ProductId=0 then '' else case when exists(select ProductId from CRM.HFCProducts where ProductId=ql.ProductId) 
                    //        then (select ProductGroupDesc from CRM.HFCProducts where ProductId=ql.ProductId) 
                    //        else case when exists(select ProductId from CRM.FranchiseProducts where ProductId=ql.ProductId) 
                    //        then (select ProductName from CRM.FranchiseProducts where ProductId=ql.ProductId) else '' end end end  as ProductName,
                    //        case when ql.VendorId is null and ql.VendorId=0 then '' else case when exists(select VendorId from Acct.HFCVendors where VendorId=ql.VendorId) 
                    //        then (select VendorName from Acct.HFCVendors where VendorId=ql.VendorId) 
                    //        else case when exists(select VendorId from Acct.FranchiseVendors where VendorId=ql.VendorId) 
                    //        then (select VendorName from Acct.FranchiseVendors where ProductId=ql.VendorId) else '' end end end  as VendorName,
                    //        ql.QuoteLineNumber,ql.QuoteLineId, Status= 7000, StatusName='New',po.VendorReference
                    //        from CRM.QuoteLines ql
                    //        join CRM.PurchaseOrderDetails po on ql.QuoteLineId=po.QuoteLineId
                    //        where po.PICPO in (select PICPO from CRM.PurchaseOrderDetails where PurchaseOrdersDetailId=@id) and  po.StatusId not in (8,9,10)";

                    Query = @"select ql.VendorId,
                              case when ql.ProductTypeId <> 3 then
                            (select PICProductID from CRM.HFCProducts where ProductID =ql.ProductId)
                            else 
                            ( select ProductID from CRM.FranchiseProducts where ProductKey = ql.ProductId and FranchiseId=@fId) end as  ProductId
                            ,ql.Description,po.PurchaseOrdersDetailId,po.PICPO as VPO,
                            case when ql.ProductId=0 then '' else case when exists(select ProductId from CRM.HFCProducts where ProductId=ql.ProductId) 
                            then (select ProductGroupDesc from CRM.HFCProducts where ProductId=ql.ProductId) 
                            else case when exists(select ProductId from CRM.FranchiseProducts where ProductId=ql.ProductId) 
                            then (select ProductName from CRM.FranchiseProducts where ProductId=ql.ProductId) else '' end end end  as ProductName,
                            case when ql.VendorId is null and ql.VendorId=0 then '' else case when exists(select VendorId from Acct.HFCVendors where VendorId=ql.VendorId) 
                            then (select VendorName from Acct.HFCVendors where VendorId=ql.VendorId) 
                            else case when exists(select VendorId from Acct.FranchiseVendors where VendorId=ql.VendorId) 
                            then (select VendorName from Acct.FranchiseVendors where ProductId=ql.VendorId) else '' end end end  as VendorName,
                            CAST(o.OrderNumber as varchar(10)) + '-' + CAST(ql.QuoteLineNumber as varchar(3)) as QuoteLineNumber,ql.QuoteLineId, Status= 7000, StatusName='New',po.VendorReference
                            from CRM.QuoteLines ql
							join CRM.Orders o on o.QuoteKey= ql.QuoteKey
                            join CRM.PurchaseOrderDetails po on ql.QuoteLineId=po.QuoteLineId
                            where po.PICPO in (select PICPO from CRM.PurchaseOrderDetails where PurchaseOrdersDetailId=@id) and  po.StatusId not in (8,9,10)";
                    var Value = connection.Query<FranchiseCaseAddInfoModel>(Query, new { id = id, fId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                    if (lineNo != 0)
                    {
                        if (lineNo > 0 && OrderId > 0)
                        {
                            var ordObj = connection.Query<Order>(@"select * from CRM.Orders where OrderID=@OrderID",
                                                                new { OrderID = OrderId }).FirstOrDefault();
                            return (from s in Value where s.QuoteLineNumber == (ordObj.OrderNumber + "-" + lineNo).ToString() select s).ToList();
                        }
                        else
                            return (from s in Value where s.QuoteLineNumber == lineNo.ToString() select s).ToList();
                    }
                    return Value;
                }
            }
        }

        public List<FranchiseCaseAddInfoModel> GetLinesForVpoChangeDropchange(int id)
        {

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {

                {
                    var Query = "";
                    //Query = @"select ql.VendorId,
                    //         (select PICProductID from CRM.HFCProducts where ProductID =ql.ProductId) as  ProductId,ql.Description,po.PurchaseOrdersDetailId,po.PICPO as VPO,
                    //        case when ql.ProductId=0 then '' else case when exists(select ProductId from CRM.HFCProducts where ProductId=ql.ProductId) 
                    //        then (select ProductGroupDesc from CRM.HFCProducts where ProductId=ql.ProductId) 
                    //        else case when exists(select ProductId from CRM.FranchiseProducts where ProductId=ql.ProductId) 
                    //        then (select ProductName from CRM.FranchiseProducts where ProductId=ql.ProductId) else '' end end end  as ProductName,
                    //        case when ql.VendorId is null and ql.VendorId=0 then '' else case when exists(select VendorId from Acct.HFCVendors where VendorId=ql.VendorId) 
                    //        then (select VendorName from Acct.HFCVendors where VendorId=ql.VendorId) 
                    //        else case when exists(select VendorId from Acct.FranchiseVendors where VendorId=ql.VendorId) 
                    //        then (select VendorName from Acct.FranchiseVendors where ProductId=ql.VendorId) else '' end end end  as VendorName,
                    //        ql.QuoteLineNumber,ql.QuoteLineId, Status= 7000, StatusName='New',po.VendorReference
                    //        from CRM.QuoteLines ql
                    //        join CRM.PurchaseOrderDetails po on ql.QuoteLineId=po.QuoteLineId
                    //        where po.PICPO in (select PICPO from CRM.PurchaseOrderDetails where PurchaseOrdersDetailId=@id) and  po.StatusId not in (8,9,10)";

                    Query = @"select ql.VendorId,
                            case when po.PartNumber ='' and ql.ProductId < 1 then '0' else
							case when po.PartNumber <> '' then  po.PartNumber else
							case when ql.ProductId > 0 and ql.ProductTypeId > 1 then 
                            ( select ProductID from CRM.FranchiseProducts where ProductKey = ql.ProductId and FranchiseId=2)
							else 0 end end end as  ProductId,ql.Description,po.PurchaseOrdersDetailId,po.PICPO as VPO,
                            case when ql.ProductName <> '' and ql.ProductName IS NOT NULL  then ql.ProductName else
                            case when ql.ProductId=0 then '' else case when exists(select ProductId from CRM.HFCProducts where ProductId=ql.ProductId) 
                            then (select ProductGroupDesc from CRM.HFCProducts where ProductId=ql.ProductId) 
                            else case when exists(select ProductId from CRM.FranchiseProducts where ProductId=ql.ProductId) 
                            then (select distinct ProductName from CRM.FranchiseProducts where ProductId=ql.ProductId) else '' end end end end as ProductName,
                            case when ql.VendorId is null and ql.VendorId=0 then '' else case when exists(select VendorId from Acct.HFCVendors where VendorId=ql.VendorId) 
                            then (select VendorName from Acct.HFCVendors where VendorId=ql.VendorId) 
                            else case when exists(select VendorId from Acct.FranchiseVendors where VendorId=ql.VendorId) 
                            then (select VendorName from Acct.FranchiseVendors where ProductId=ql.VendorId) else '' end end end  as VendorName,
                            CAST(o.OrderNumber as varchar(10)) + '-' + CAST(ql.QuoteLineNumber as varchar(3)) as QuoteLineNumber,ql.QuoteLineId, Status= 7000, StatusName='New',po.VendorReference
                            from CRM.QuoteLines ql
							join CRM.Orders o on o.QuoteKey= ql.QuoteKey
                            join CRM.PurchaseOrderDetails po on ql.QuoteLineId=po.QuoteLineId
                            where po.PICPO in (select PICPO from CRM.PurchaseOrderDetails where PurchaseOrdersDetailId=@id) and  po.StatusId not in (8,9,10)";
                    var Value = connection.Query<FranchiseCaseAddInfoModel>(Query, new { id = id, fId = SessionManager.CurrentFranchise.FranchiseId }).ToList();                    
                    return Value;
                }
            }
        }

        public FranchiseCaseAddInfoModel GetLinesOrderAndLineno(int id, int lineNo)
        {

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {

                {

                    var res = (connection.Query<MasterPurchaseOrder>(@"select pod.* from CRM.MasterPurchaseOrder mpo
                              join CRM.MasterPurchaseOrderExtn mpoe on mpoe.PurchaseOrderId= mpo.PurchaseOrderId
	                          join CRM.PurchaseOrderDetails pod on pod.PurchaseOrderId = mpo.PurchaseOrderId
	                          join CRM.QuoteLines ql on ql.QuoteLineId = pod.QuoteLineId
                              where mpoe.OrderId=@OrderId and mpo.POStatusId not in (8,9,10)
                              and ql.QuoteLineNumber=@QuoteLineNumber", new { OrderId = id, QuoteLineNumber = lineNo }).FirstOrDefault());

                    if (res != null)
                        return GetLinesForPurchaseOrderIdAndLineNo(id, res.PurchaseOrderId, lineNo);
                    else
                    {

                        var Queryy = @"select ql.VendorId,
                             case when ql.ProductTypeId <> 3 then
                            (select PICProductID from CRM.HFCProducts where ProductID =ql.ProductId)
                            else 
                            ( select ProductID from CRM.FranchiseProducts where ProductKey = ql.ProductId and FranchiseId=@fId) end as ProductId,ql.Description,
                            case when ql.ProductName <> '' and ql.ProductName IS NOT NULL  then ql.ProductName
							else
                             case when ql.ProductId=0 then '' 
							      else 
								  case when ql.ProductTypeId <> 4 then
							      case when exists(select ProductId from CRM.HFCProducts where ProductId=ql.ProductId) 
                                  then (select ProductGroupDesc from CRM.HFCProducts where ProductId=ql.ProductId) end 
                                  else case when exists(select ProductId from CRM.FranchiseProducts where ProductKey=ql.ProductId) 
                                  then (select ProductName from CRM.FranchiseProducts where ProductKey=ql.ProductId) else '' end end end end
                                as ProductName,
                            case when ql.VendorId is null and ql.VendorId=0 then '' else case when exists(select VendorId from Acct.HFCVendors where VendorId=ql.VendorId) 
                            then (select VendorName from Acct.HFCVendors where VendorId=ql.VendorId) 
                            else case when exists(select VendorId from Acct.FranchiseVendors where VendorId=ql.VendorId) 
                            then (select VendorName from Acct.FranchiseVendors where ProductId=ql.VendorId) else '' end end end  as VendorName,
                            CAST(o.OrderNumber as varchar(10)) + '-' + CAST(ql.QuoteLineNumber as varchar(3)) as QuoteLineNumber,ql.QuoteLineId, Status= 7000, StatusName='New'
                            from CRM.QuoteLines ql
							join CRM.Quote q on q.QuoteKey = ql.QuoteKey 
                            join CRM.Orders o on o.QuoteKey=q.QuoteKey
                            where o.OrderID=@OrderID and ql.QuoteLineNumber=@QuoteLineNumber";

                        return connection.Query<FranchiseCaseAddInfoModel>(Queryy, new { OrderID = id, QuoteLineNumber = lineNo, fId= SessionManager.CurrentFranchise.FranchiseId }).FirstOrDefault();
                        // var lll = (from s in Value where s.QuoteLineNumber == lineNo select s).FirstOrDefault();
                        //  return (from s in Value where s.QuoteLineNumber == lineNo select s).FirstOrDefault();


                    }



                }
            }
        }



        public FranchiseCaseAddInfoModel GetLinesForPurchaseOrderIdAndLineNo(int orderId, int id, int lineNo)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {

                {
                    var Query = "";
                    Query = @"Select  ql.* from CRM.PurchaseOrderDetails pod 
                            join CRM.MasterPurchaseOrder mpo on pod.PurchaseOrderId=mpo.PurchaseOrderId
                            join CRM.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId=mpo.PurchaseOrderId
                            join CRM.Orders o on mpox.OrderId=o.OrderID
                            join CRM.Quote q on o.QuoteKey=q.QuoteKey 
                            join CRM.QuoteLines ql on q.QuoteKey=ql.QuoteKey and pod.QuoteLineId=ql.QuoteLineId
                            where mpo.PurchaseOrderId = @PurchaseOrderId and ql.QuoteLineNumber=@QuoteLineNumber";
                    var quoteline = connection.Query<QuoteLines>(Query, new { PurchaseOrderId = id, QuoteLineNumber = lineNo }).FirstOrDefault();

                    if (quoteline.VendorId != null && quoteline.VendorId != 0)
                    {
                        //                 Query = @"select ql.VendorId,
                        //                      (select PICProductID from CRM.HFCProducts where ProductID =ql.ProductId) as  ProductId,ql.Description,po.PurchaseOrdersDetailId,po.PICPO as VPO,
                        //                     case when ql.ProductName <> '' and ql.ProductName IS NOT NULL  then ql.ProductName
                        //else
                        //                     case when ql.ProductId=0 then '' else case when exists(select ProductId from CRM.HFCProducts where ProductId=ql.ProductId) 
                        //                     then (select ProductGroupDesc from CRM.HFCProducts where ProductId=ql.ProductId) 
                        //                     else case when exists(select ProductId from CRM.FranchiseProducts where ProductId=ql.ProductId) 
                        //                     then (select ProductName from CRM.FranchiseProducts where ProductId=ql.ProductId) else '' end end end end  as ProductName,
                        //                     case when ql.VendorId is null and ql.VendorId=0 then '' else case when exists(select VendorId from Acct.HFCVendors where VendorId=ql.VendorId) 
                        //                     then (select VendorName from Acct.HFCVendors where VendorId=ql.VendorId) 
                        //                     else case when exists(select VendorId from Acct.FranchiseVendors where VendorId=ql.VendorId) 
                        //                     then (select VendorName from Acct.FranchiseVendors where ProductId=ql.VendorId) else '' end end end  as VendorName,
                        //                     CAST(o.OrderNumber as varchar(10)) + '-' + CAST(ql.QuoteLineNumber as varchar(3)) as QuoteLineNumber,ql.QuoteLineId, Status= 7000, StatusName='New',po.VendorReference
                        //                     from CRM.QuoteLines ql
                        //                     join CRM.PurchaseOrderDetails po on ql.QuoteLineId=po.QuoteLineId
                        //   where po.PurchaseOrderId=@PurchaseOrderId and  po.StatusId not in (8,9,10)
                        //and ql.VendorId in(Select  ql.VendorId from CRM.PurchaseOrderDetails pod 
                        //                     join CRM.MasterPurchaseOrder mpo on pod.PurchaseOrderId=mpo.PurchaseOrderId
                        //                     join CRM.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId=mpo.PurchaseOrderId
                        //                     join CRM.Orders o on mpox.OrderId=o.OrderID
                        //                     join CRM.Quote q on o.QuoteKey=q.QuoteKey 
                        //                     join CRM.QuoteLines ql on q.QuoteKey=ql.QuoteKey and pod.QuoteLineId=ql.QuoteLineId
                        //                     where mpo.PurchaseOrderId = @PurchaseOrderId and ql.QuoteLineNumber=@QuoteLineNumber)";
                        Query = @"select ql.VendorId,
                              case when ql.ProductTypeId <> 3 then
                            (select PICProductID from CRM.HFCProducts where ProductID =ql.ProductId)
                            else 
                            ( select ProductID from CRM.FranchiseProducts where ProductKey = ql.ProductId and FranchiseId=@fId) end as  ProductId,ql.Description,po.PurchaseOrdersDetailId,po.PICPO as VPO,
                            case when ql.ProductName <> '' and ql.ProductName IS NOT NULL  then ql.ProductName
							else
                            case when ql.ProductId=0 then '' else case when exists(select ProductId from CRM.HFCProducts where ProductId=ql.ProductId) 
                            then (select ProductGroupDesc from CRM.HFCProducts where ProductId=ql.ProductId) 
                            else case when exists(select ProductId from CRM.FranchiseProducts where ProductId=ql.ProductId) 
                            then (select ProductName from CRM.FranchiseProducts where ProductId=ql.ProductId) else '' end end end end  as ProductName,
                            case when ql.VendorId is null and ql.VendorId=0 then '' else case when exists(select VendorId from Acct.HFCVendors where VendorId=ql.VendorId) 
                            then (select VendorName from Acct.HFCVendors where VendorId=ql.VendorId) 
                            else case when exists(select VendorId from Acct.FranchiseVendors where VendorId=ql.VendorId) 
                            then (select VendorName from Acct.FranchiseVendors where ProductId=ql.VendorId) else '' end end end  as VendorName,
                            CAST(ord.OrderNumber as varchar(10)) + '-' + CAST(ql.QuoteLineNumber as varchar(3)) as QuoteLineNumber,
							ql.QuoteLineId, Status= 7000, StatusName='New',po.VendorReference
                            from CRM.QuoteLines ql
                            join CRM.PurchaseOrderDetails po on ql.QuoteLineId=po.QuoteLineId
							join CRM.MasterPurchaseOrderExtn mpoxx on mpoxx.PurchaseOrderId=po.PurchaseOrderId
							join CRM.Orders ord on ord.OrderID = mpoxx.OrderId
						    where po.PurchaseOrderId=@PurchaseOrderId and  po.StatusId not in (8,9,10)
							and ql.VendorId in (Select  ql.VendorId from CRM.PurchaseOrderDetails pod 
                            join CRM.MasterPurchaseOrder mpo on pod.PurchaseOrderId=mpo.PurchaseOrderId
                            join CRM.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId=mpo.PurchaseOrderId
                            join CRM.Orders o on mpox.OrderId=o.OrderID
                            join CRM.Quote q on o.QuoteKey=q.QuoteKey 
                            join CRM.QuoteLines ql on q.QuoteKey=ql.QuoteKey and pod.QuoteLineId=ql.QuoteLineId
                            where mpo.PurchaseOrderId = @PurchaseOrderId and ql.QuoteLineNumber=@QuoteLineNumber)";
                    }
                    else
                    {
                        Query = @"select ql.VendorId,
                              case when ql.ProductTypeId <> 3 then
                            (select PICProductID from CRM.HFCProducts where ProductID =ql.ProductId)
                            else 
                            ( select ProductID from CRM.FranchiseProducts where ProductKey = ql.ProductId and FranchiseId=@fId) end as ProductId,ql.Description,po.PurchaseOrdersDetailId,po.PICPO as VPO,
                            case when ql.ProductName <> '' and ql.ProductName IS NOT NULL  then ql.ProductName
							else
                            case when ql.ProductId = 0 then '' else case when exists(select ProductId from CRM.HFCProducts where ProductId = ql.ProductId) 
                            then(select ProductGroupDesc from CRM.HFCProducts where ProductId = ql.ProductId)
                            else case when exists(select ProductId from CRM.FranchiseProducts where ProductId = ql.ProductId) 
                            then(select ProductName from CRM.FranchiseProducts where ProductId = ql.ProductId) else '' end end end end as ProductName,
                            case when ql.VendorId is null and ql.VendorId = 0 then '' else case when exists(select VendorId from Acct.HFCVendors where VendorId = ql.VendorId) 
                            then(select VendorName from Acct.HFCVendors where VendorId = ql.VendorId)
                            else case when exists(select VendorId from Acct.FranchiseVendors where VendorId = ql.VendorId) 
                            then(select VendorName from Acct.FranchiseVendors where ProductId = ql.VendorId) else '' end end end as VendorName,
                            CAST(o.OrderNumber as varchar(10)) + '-' + CAST(ql.QuoteLineNumber as varchar(3)) as QuoteLineNumber,ql.QuoteLineId, Status = 7000, StatusName = 'New',po.VendorReference
                            from CRM.QuoteLines ql
                            join CRM.Orders o on o.QuoteKey= ql.QuoteKey
                            join CRM.PurchaseOrderDetails po on ql.QuoteLineId = po.QuoteLineId
                           where po.PurchaseOrderId = @PurchaseOrderId and po.StatusId not in (8,9,10) and ql.QuoteLineNumber = @QuoteLineNumber";
                    }
                    var Value = connection.Query<FranchiseCaseAddInfoModel>(Query, new { PurchaseOrderId = id, QuoteLineNumber = lineNo, fId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                    // var lll = (from s in Value where s.QuoteLineNumber == lineNo select s).FirstOrDefault();
                    var ordObj = connection.Query<Order>(@"select * from CRM.Orders where OrderID=@OrderID",
                                                             new { OrderID = orderId }).FirstOrDefault();
                    return (from s in Value where s.QuoteLineNumber == (ordObj.OrderNumber + "-" + lineNo).ToString() select s).FirstOrDefault();
                }
            }
        }

        public string DeleteData(int id, int caseid)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                {
                    var Query = "";
                    Query = @"Select * from [CRM].[FranchiseCaseAddInfo] where Id=@id";
                    var Value = connection.Query<FranchiseCaseAddInfoModel>(Query, new { id = id }).FirstOrDefault();
                    if (Value != null)
                    {
                        var query = "";
                        query = @"Delete from [CRM].[FranchiseCaseAddInfo] where Id=@id";
                        var result = connection.Query<FranchiseCaseAddInfoModel>(query, new { id = id }).FirstOrDefault();
                        return "Data Deleted";
                    }
                    else
                        return "In-Valid Data";
                }
            }
        }

        public string RecordCopy(int id, FranchiseCaseAddInfoModel data)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var Query = "";
                Query = @"Select * from [CRM].[FranchiseCaseAddInfo] where Id=@id";
                var Value = connection.Query<FranchiseCaseAddInfoModel>(Query, new { id = data.Id }).FirstOrDefault();
                if (Value != null)
                {
                    Value.Id = 0;
                    //int vendorcasenum = 1000;
                    //var Vendr_CaseNum = GetCaseDataValue();
                    //if (Vendr_CaseNum == null)
                    //{
                    //    Value.VendorCaseNumber = vendorcasenum;
                    //}
                    //else Value.VendorCaseNumber = Value.VendorCaseNumber + 1;
                    var result = Insert<int, FranchiseCaseAddInfoModel>(ContextFactory.CrmConnectionString, Value);
                    var dataid = Value.Id.ToString();
                    return "Success";

                }
                else
                    return "Invalid Data";
            }
        }

        public string CopyContent(int id, FranchiseCaseAddInfoModel model)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                if (model.Id != 0 && model.Todo == "Copy")
                {
                    var Result = RecordCopy(id, model);
                    return Result;
                }
                else
                {
                    var newrecord = Insert<int, FranchiseCaseAddInfoModel>(ContextFactory.CrmConnectionString, model);
                    int newid = newrecord;
                    if (newid != 0 && model.Todo == "Copy")
                    {
                        var Query = "";
                        Query = @"Select * from [CRM].[FranchiseCaseAddInfo] where Id=@id";
                        var CopyData = connection.Query<FranchiseCaseAddInfoModel>(Query, new { id = newid }).FirstOrDefault();
                        if (CopyData != null)
                        {
                            CopyData.Id = 0;
                            //int vendorcasenum = 1000;
                            //var Vendr_CaseNum = GetCaseDataValue();
                            //if (Vendr_CaseNum == null)
                            //{
                            //    CopyData.VendorCaseNumber = vendorcasenum;
                            //}
                            //else CopyData.VendorCaseNumber = CopyData.VendorCaseNumber + 1;
                            var result = Insert<int, FranchiseCaseAddInfoModel>(ContextFactory.CrmConnectionString, CopyData);
                            var dataid = CopyData.Id.ToString();
                            return "Success";

                        }
                        else
                            return "Invalid Data";
                    }

                }
                return null;
            }
        }

        public override string Add(FranchiseCaseModel data)
        {
            throw new NotImplementedException();
        }

        public override string Delete(int id)
        {
            throw new NotImplementedException();
        }

        public override FranchiseCaseModel Get(int id)
        {
            throw new NotImplementedException();
        }

        public override ICollection<FranchiseCaseModel> Get(List<int> idList)
        {
            throw new NotImplementedException();
        }

        public override string Update(FranchiseCaseModel data)
        {
            throw new NotImplementedException();
        }

        public List<FranchiseCaseList> GetFranchiseCaseList(int SelectedCaseType, bool Closed, int? FranchiseId, int? VendorId)
        {

            //var Query = @"select distinct fc.CaseId, fc.[CaseNumber] as CaseNo
            //            ,op.FranchiseId
            //            ,fe.Name FranchiseName
            //            , fca.VendorCaseNumber
            //            ,mpo.PurchaseOrderId, mpo.MasterPONumber as MPO
            //            ,pod.PICPO as VPO
            //            ,ql.VendorId,ql.VendorName as Vendor
            //            ,acc.AccountId, cu.FirstName+ ' '+cu.LastName as AccountName
            //            ,fca.IssueDescription as Description
            //            ,fca.[Status] as Status
            //            ,lvstatus.[Name] as StatusName 
            //            ,fca.[Type] as Type
            //            ,op.InstallerId
            //            ,op.SalesAgentId
            //            ,lvtype.[Name] as TypeName
            //            ,fc.DateTimeOpened,fc.CreatedOn
            //            ,p.FirstName+' '+p.LastName as CaseOwner, ql.QuoteLineNumber,
            //             case when exists(select top(1) vc.VendorCaseNumber from CRM.VendorCase vc where vc.CaseId = fca.CaseId  and ql.VendorId=vc.VendorId) then 
            //                     (select top(1) vcc.VendorCaseNumber from CRM.VendorCase vcc where vcc.CaseId = fca.CaseId and ql.VendorId=vcc.VendorId) else null end as VendorCaseNo
            //             ,fca.ConvertToVendor,ql.ProductTypeId
            //            from[CRM].[FranchiseCase] fc
            //            inner join[CRM].[FranchiseCaseAddInfo] fca on fc.CaseId=fca.CaseId
            //            inner join CRM.PurchaseOrderDetails pod on fca.QuoteLineId = pod.QuoteLineId
            //            inner join CRM.QuoteLines ql on pod.QuoteLineId= ql.QuoteLineId
            //            inner join CRM.MasterPurchaseOrder mpo on fc.MPO_MasterPONum_POId = mpo.PurchaseOrderId
            //            inner join CRM.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
            //            inner join CRM.Opportunities op on mpox.OpportunityId= op.OpportunityId
            //            inner join CRM.Accounts acc on mpox.AccountId= acc.AccountId
            //            inner join CRM.AccountCustomers accu on acc.AccountId= accu.AccountId
            //            inner join CRM.Customer cu on accu.CustomerId= cu.CustomerId and accu.IsPrimaryCustomer= 1
            //            inner join CRM.Person p on fc.Owner_PersonId = p.PersonId
            //            inner join CRM.Franchise fe on op.FranchiseId=fe.FranchiseId
            //            inner join CRM.Type_LookUpValues lvstatus on fca.[Status]=lvstatus.Id
            //            inner join CRM.Type_LookUpValues lvtype on fca.[Type]=lvtype.Id";



            var Query = @"select distinct fc.CaseId, fc.[CaseNumber] as CaseNo
                        ,op.FranchiseId
                        ,fe.Name FranchiseName
                        , fca.VendorCaseNumber
                       ,mpo.PurchaseOrderId, mpo.MasterPONumber as MPO
                       ,pod.PICPO as VPO
                        ,ql.VendorId,ql.VendorName as Vendor
                        ,acc.AccountId, cu.FirstName+ ' '+cu.LastName as AccountName
                        ,fca.IssueDescription as Description
                        ,fca.[Status] as Status
                        ,lvstatus.[Name] as StatusName 
                        ,fca.[Type] as Type
                        ,op.InstallerId
                        ,op.SalesAgentId
                        ,lvtype.[Name] as TypeName
                        ,fc.DateTimeOpened,fc.CreatedOn
                        ,p.FirstName+' '+p.LastName as CaseOwner, cast(o.OrderNumber as nvarchar(10))+'-'+CAST(ql.QuoteLineNumber as nvarchar(3)) as QuoteLineNumber,
                         case when exists(select top(1) vc.VendorCaseNumber from CRM.VendorCase vc where vc.CaseId = fca.CaseId  and ql.VendorId=vc.VendorId) then 
							                          (select top(1) vcc.VendorCaseNumber from CRM.VendorCase vcc where vcc.CaseId = fca.CaseId and ql.VendorId=vcc.VendorId) else null end as VendorCaseNo
                         ,fca.ConvertToVendor,ql.ProductTypeId
                        from[CRM].[FranchiseCase] fc
                        inner join [CRM].[FranchiseCaseAddInfo] fca on fc.CaseId=fca.CaseId
						join CRM.QuoteLines ql on fca.QuoteLineId= ql.QuoteLineId
						join CRM.Quote q on q.QuoteKey= ql.QuoteKey
                        join CRM.Orders o on q.QuoteKey = o.QuoteKey
						inner join CRM.Opportunities op on q.OpportunityId= op.OpportunityId
						inner join CRM.Accounts acc on op.AccountId= acc.AccountId
						inner join CRM.AccountCustomers accu on acc.AccountId= accu.AccountId
                        inner join CRM.Customer cu on accu.CustomerId= cu.CustomerId and accu.IsPrimaryCustomer= 1
                        inner join CRM.Person p on fc.Owner_PersonId = p.PersonId
                        inner join CRM.Franchise fe on op.FranchiseId=fe.FranchiseId
                        inner join CRM.Type_LookUpValues lvstatus on fca.[Status]=lvstatus.Id
						left join CRM.PurchaseOrderDetails pod on ql.QuoteLineId = pod.QuoteLineId
						left join CRM.MasterPurchaseOrder mpo on fc.MPO_MasterPONum_POId = mpo.PurchaseOrderId
						left join CRM.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                        inner join CRM.Type_LookUpValues lvtype on fca.[Type]=lvtype.Id";
            if (SelectedCaseType == 4)
                Query = Query + " inner join CRM.CaseFollow cf on cf.CaseId= fc.CaseId ";

            Query = Query + " where op.FranchiseId= isnull( @FranchiseId,op.FranchiseId) ";
            Query += " and ql.VendorId= isnull( @VendorId,ql.VendorId) ";
            if (Closed != true)
            {
                Query = Query + " and fca.Status < 7006 ";
            }

            if (SelectedCaseType == 1) Query = Query + " and fc.Owner_PersonId = @Owner_PersonId ";
            else if (SelectedCaseType == 2) Query = Query + " and fc.CreatedBy =  @Owner_PersonId ";
            else if (SelectedCaseType == 3) Query = Query + " and fca.Status = 7001 ";
            else if (SelectedCaseType == 4) Query = Query + " and fc.CaseId in ( select distinct gg.CaseId from [CRM].[CaseFollow] gg where PersonId=@Owner_PersonId and Isfollow=1) ";
            else if (SelectedCaseType == 5) Query = Query + " ";
            else Query = Query + "";


            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                {
                    int UserId = 0;
                    if (SessionManager.CurrentUser.Person != null)
                    {
                        UserId = SessionManager.CurrentUser.PersonId;
                    }
                    if (SessionManager.SecurityUser.Person != null && UserId == 0)
                    {
                        UserId = SessionManager.SecurityUser.PersonId;
                    }
                    var Value = connection.Query<FranchiseCaseList>(Query, new
                    {
                        FranchiseId = FranchiseId,
                        VendorId = VendorId,
                        Owner_PersonId = UserId
                    }).ToList();
                    if (Value.Count != 0)
                    {
                        foreach (var item in Value)
                        {
                            if (item.DateTimeOpened != null)
                                item.DateTimeOpened = (DateTime)TimeZoneManager.ToLocal(item.CreatedOn);
                            item.DateTimeOpen = item.DateTimeOpened.ToString("MM/dd/yyyy h:mm tt");
                        }
                        return Value;
                    }
                }
                return null;
            }
        }

        public dynamic getlineIdList(int CaseId, string Module)
        {
            var Query = "";
            if (Module == "FranchiseCase")
                Query = @"select fci.Id as lineId,CAST(o.OrderNumber as nvarchar(10))+'-'+CAST(ql.QuoteLineNumber as nvarchar(3)) as lineText from CRM.QuoteLines ql
                        join [CRM].[FranchiseCaseAddInfo] fci on ql.QuoteLineId=fci.QuoteLineId
						join CRM.Quote q on q.QuoteKey = ql.QuoteKey
						join CRM.Orders o on o.QuoteKey = q.QuoteKey
                        where fci.CaseId=@CaseId order by ql.QuoteLineId";
            //Query = @"select fci.Id as lineId,ql.QuoteLineNumber as lineText from CRM.QuoteLines ql
            //        join [CRM].[FranchiseCaseAddInfo] fci on ql.QuoteLineId=fci.QuoteLineId
            //        where fci.CaseId=@CaseId";

            if (Module == "VendorCase")
            {
                Query = @"select vcd.VendorCaseDetailId as lineId,CAST(o.OrderNumber as nvarchar(10))+'-'+CAST(ql.QuoteLineNumber as nvarchar(3)) as lineText from CRM.VendorCaseDetail vcd
                        join CRM.FranchiseCaseAddInfo fci on vcd.CaseLineId=fci.Id
                        join CRM.QuoteLines ql on fci.QuoteLineId=ql.QuoteLineId
						join CRM.Quote q on q.QuoteKey = ql.QuoteKey
						join CRM.Orders o on o.QuoteKey = q.QuoteKey
                        where vcd.VendorCaseId=@CaseId order by ql.QuoteLineId";

                //Query = @"select vcd.VendorCaseDetailId as lineId,ql.QuoteLineNumber as lineText from CRM.VendorCaseDetail vcd
                //        join CRM.FranchiseCaseAddInfo fci on vcd.CaseLineId=fci.Id
                //        join CRM.QuoteLines ql on fci.QuoteLineId=ql.QuoteLineId
                //        where vcd.VendorCaseId=@CaseId";

            }

            //if (Module == "VendorCase")
            //    sdsd

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                {
                    var Value = connection.Query<dynamic>(Query, new { CaseId = CaseId }).ToList();
                    return Value;
                }
            }
        }

        public List<franchiseCaseAttachmentDetails> getfranchiseCaseDocList(int CaseId, string Module)
        {
            var Query = "";
            if (Module == "FranchiseCase")
            {
                Query = @"select * from [CRM].[franchiseCaseAttachmentDetails] where ModuleId=@CaseId and Module=@Module";
            }

            if (Module == "VendorCase")
            {
                Query = @"  select fcad.* from [CRM].[VendorCase] vc 
  join CRM.VendorCaseDetail vcd on vc.VendorCaseId=vcd.VendorCaseId
  join CRM.FranchiseCase fc on vc.CaseId = fc.CaseId
  join CRM.FranchiseCaseAddInfo fca on vcd.CaseLineId=fca.Id
  join CRM.FranchiseCaseAttachmentDetails fcad on fcad.ModuleId=fca.CaseId and fcad.LineId=fca.Id
  where vc.VendorCaseId=@CaseId
  and Module='FranchiseCase'
  union
  select * from CRM.FranchiseCaseAttachmentDetails fcad where ModuleId=@CaseId and Module='VendorCase'";
            }

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                {
                    var Value = connection.Query<franchiseCaseAttachmentDetails>(Query, new { CaseId = CaseId, Module = Module }).ToList();
                    return Value;
                }
            }
        }
        public MasterPurchaseOrder getAccOrderOpportunity(int Id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                {
                    var chkOrder = connection.Query<FranchiseCaseModel>(@"select * from CRM.FranchiseCase where CaseId=@CaseId and SalesOrderId like '%,%'",
                        new { CaseId = Id }).FirstOrDefault();


                    if (chkOrder == null)
                    {
                        var Value = connection.Query<MasterPurchaseOrder>(@"select fc.*,cu.HomePhone,cu.CellPhone,cu.WorkPhone,cu.PrimaryEmail,cu.PreferredTFN 
                                                                        ,ac.AccountId, op.OpportunityId, ord.OrderId, q.QuoteId,
                                                                        ac.FranchiseId,ac.TerritoryId,ac.PersonId,ord.OrderName,op.InstallationAddressId,
                                                                        ac.IsNotifyemails
                                                                        from [CRM].[FranchiseCase] fc
                                                                        join CRM.Orders ord on ord.OrderId = fc.SalesOrderId
																		join CRM.Quote q on q.QuoteKey = ord.QuoteKey
																		join CRM.Opportunities op on op.OpportunityId = q.OpportunityId
                                                                        join crm.Accounts ac on op.AccountId = ac.AccountId
                                                                        join CRM.AccountCustomers acc on ac.AccountId = acc.AccountId and acc.IsPrimaryCustomer = 1
                                                                        join CRM.Customer cu on acc.CustomerId = cu.CustomerId
                                                                        where fc.CaseId = @CaseId",
                            new { CaseId = Id }).FirstOrDefault();
                        if (Value != null)
                        {
                            var query = @"select cus.FirstName+' '+cus.LastName as ContactName,* from CRM.Addresses addr
                      left join CRM.Customer cus on cus.CustomerId = addr.ContactId
                      where AddressId = @addressId";
                            var insAddress = ExecuteIEnumerableObject<AddressTP>(ContextFactory.CrmConnectionString, query, new
                            {
                                addressId = Value.InstallationAddressId
                            }).FirstOrDefault();
                            if (insAddress != null)
                                Value.InstallAddress = insAddress;
                        }
                        return Value;
                    }
                    else
                        return null;
                }
            }
        }

        public franchiseCaseAttachmentDetails saveFranchiseCaseAttachmentDetails(int CaseId, int VendorCaseId, string Module, string fileIconType, string fileSize, string fileName, string color, string streamId, string streamId1, bool flag)
        {

            var Query = @"insert into CRM.franchiseCaseAttachmentDetails values(@Module,@ModuleId,@LineId,@fileIconType,@fileSize,@fileName,@color,@streamId,@streamId1,@CreatedOn,@CreatedBy, @LastUpdatedOn, @LastUpdatedBy)";
            VendorCaseId = 0;
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                {
                    //franchiseCaseAttachmentDetails fcad = new franchiseCaseAttachmentDetails();
                    //fcad.Module = Module;
                    //fcad.ModuleId = CaseId;
                    //fcad.LineId = VendorCaseId;
                    //fcad.fileIconType = fileIconType;
                    //fcad.fileSize = fileSize;
                    //fcad.fileName = fileName;
                    //fcad.color = color;
                    //fcad.streamId = streamId;
                    //fcad.streamId1 = streamId1;
                    //fcad.CreatedOn = DateTime.UtcNow;
                    //fcad.CreatedBy = SessionManager.CurrentUser.PersonId;
                    //fcad.LastUpdatedOn = DateTime.UtcNow;
                    //fcad.LastUpdatedBy = SessionManager.CurrentUser.PersonId;

                    // var rr = Insert<int, franchiseCaseAttachmentDetails>(ContextFactory.CrmConnectionString, fcad);
                    var Value = connection.Execute(Query, new { ModuleId = CaseId, LineId = VendorCaseId, fileIconType = fileIconType, fileSize = fileSize, fileName = fileName, color = color, streamId = streamId, streamId1 = streamId1, Module = Module, CreatedOn = DateTime.UtcNow, CreatedBy = SessionManager.CurrentUser.PersonId, LastUpdatedOn = DateTime.UtcNow, LastUpdatedBy = SessionManager.CurrentUser.PersonId });
                    var qry = @"Select * from CRM.[franchiseCaseAttachmentDetails] where streamId =@streamId";
                    var res = connection.Query<franchiseCaseAttachmentDetails>(qry, new { streamId = streamId }).FirstOrDefault();


                    if (flag == true)
                    {
                        var tz = this.getFranchiseTimeZone(CaseId).TimezoneCode;

                        if (res.CreatedOn == null)
                        {
                            res.fileSize = res.fileSize.Insert(10, " " + tz.ToString() + " ");
                        }
                        else
                        {
                            DateTime lt = TimeZoneManager.ToLocal((DateTime)res.CreatedOn, tz);
                            res.fileSize = res.fileSize.Substring(10, res.fileSize.Length - 10);
                            res.fileSize = lt.Date.GlobalDateFormat() + " " + tz.ToString() + res.fileSize;
                        }

                    }
                    else
                    {
                        var tz = this.getFranchiseTimeZone(CaseId).TimezoneCode;

                        if (res.CreatedOn != null)
                        {
                            DateTime lt = TimeZoneManager.ToLocal((DateTime)res.CreatedOn, tz);
                            res.fileSize = res.fileSize.Substring(10, res.fileSize.Length - 10);
                            res.fileSize = lt.Date.GlobalDateFormat() + res.fileSize;
                        }
                    }

                    return res;
                }
            }

            //  return null;
        }

        public bool removeFranchiseCaseDetail(int Id)
        {
            var query = @"Delete from CRM.[franchiseCaseAttachmentDetails] where Id=@Id";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                {
                    connection.Execute(query, new { Id = Id });
                }
            }
            return true;
        }

        public bool removeFranchiseCaseDetails(int Id, int CaseId)
        {
            var query = @"Delete from CRM.[franchiseCaseAttachmentDetails] where VendorCaseId=@Id and CaseId=@CaseId";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                {
                    connection.Execute(query, new { Id = Id, CaseId = CaseId });
                }
            }
            return true;
        }

        public bool changeLineId(int Id, int VendorCaseId)
        {
            var query = @"Update CRM.[franchiseCaseAttachmentDetails] set LineId =@LineId where Id=@Id";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                {
                    connection.Execute(query, new { Id = Id, LineId = VendorCaseId });
                }
            }
            return true;
        }

        public List<HistoryTable> GetCaseHistoryForCase(int id)
        {
            //          var query = @"SELECT ht.Date,ht.Field, ht.UserId, ht.OriginalValue, ht.NewValue, per.FirstName +' '+ per.LastName as UserName
            //FROM [TptHFC_dev1].[CRM].[HistoryTable] ht
            //join  [TptHFC_dev1].crm.person per on ht.UserId = per.PersonId
            //Where ht.TableId =@Id";

            //            var query = @"SELECT ht.Date,ht.Field, ht.UserId, per.FirstName +' '+ per.LastName as UserName,ht.LastUpdatedOn
            //,case when Field='Status' then (select [Name] from CRM.Type_LookUpValues where Id=ht.OriginalValue)
            //  else case when Field='VPO_PICPO_PODId' then (select cast(PICPO as varchar) from CRM.PurchaseOrderDetails where PurchaseOrdersDetailId=ht.OriginalValue)
            //  else case when Field='MPO_MasterPONum_POId' then (select cast(MasterPONumber as varchar) from CRM.MasterPurchaseOrder where PurchaseOrderId=ht.OriginalValue)
            //  else case when Field='SalesOrderId' then (select cast(OrderNumber as varchar) from CRM.Orders where OrderID=ht.OriginalValue)
            //  else ht.OriginalValue 
            //  end end end end as OriginalValue
            //  ,case when Field='Status' then (select [Name] from CRM.Type_LookUpValues where Id=ht.NewValue)
            //   else case when Field='VPO_PICPO_PODId' then (select cast(PICPO as varchar) from CRM.PurchaseOrderDetails where PurchaseOrdersDetailId=ht.NewValue)
            //  else case when Field='MPO_MasterPONum_POId' then (select cast(MasterPONumber as varchar) from CRM.MasterPurchaseOrder where PurchaseOrderId=ht.NewValue)
            //  else case when Field='SalesOrderId' then (select cast(OrderNumber as varchar) from CRM.Orders where OrderID=ht.NewValue)
            //  else ht.NewValue  
            //   end end end end as NewValue
            //  FROM [CRM].[HistoryTable] ht
            //  join  crm.person per on ht.UserId = per.PersonId
            //  Where ht.TableId=@Id order by LastUpdatedOn desc";
            var query = @"SELECT ht.Date,ht.Field, ht.UserId, ht.LastUpdatedOn,
            ht.OriginalValue as OriginalValue,ht.NewValue as NewValue,  P.FirstName + '  ' + P.LastName as UserName  FROM [CRM].[HistoryTable] ht
            left join CRM.Person P on ht.UserId = P.PersonId
            Where ht.TableId = @Id order by LastUpdatedOn desc";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                {
                    var res = connection.Query<HistoryTable>(query, new { Id = id }).ToList();

                    return res;
                }
            }
            //return true;
        }

        public bool GetFollowOrNot(int id)
        {
            var query = @"Select * from CRM.CaseFollow where CaseId =@CaseId and PersonId = @PersonId and IsFollow=1 and ModuleId in (1,2,4)";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {

                int UserId = 0;
                if (SessionManager.CurrentUser.Person != null)
                {
                    UserId = SessionManager.CurrentUser.Person.PersonId;
                }
                else if (SessionManager.SecurityUser.Person != null)
                {
                    UserId = SessionManager.SecurityUser.Person.PersonId;
                }
                var res = connection.Query<CaseFollow>(query, new { CaseId = id, PersonId = UserId }).FirstOrDefault();
                if (res != null)
                    return res.IsFollow;
                else return false;
            }

        }

        public bool GetFollowOrNotForVendorView(int id)
        {
            var query = @"Select * from CRM.CaseFollow where CaseId =@CaseId and PersonId = @PersonId and IsFollow=1 and ModuleId in (1,2,4)";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                int UserId = 0;
                if (SessionManager.CurrentUser.Person != null)
                {
                    UserId = SessionManager.CurrentUser.Person.PersonId;
                }
                else if (SessionManager.SecurityUser.Person != null)
                {
                    UserId = SessionManager.SecurityUser.Person.PersonId;
                }
                var res = connection.Query<CaseFollow>(query, new { CaseId = id, PersonId = UserId }).FirstOrDefault();
                if (res != null)
                    return res.IsFollow;
                else return false;
            }

        }

        public bool UnfollowThisCase(int id, int module)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                if (module == 1)
                    connection.Execute("Update CRM.CaseFollow set IsFollow=@IsFollow where CaseId =@CaseId and PersonId = @PersonId and  ModuleId in (1,2)", new { CaseId = id, PersonId = SessionManager.CurrentUser.PersonId, IsFollow = false });
                if (module == 2)
                    connection.Execute("Update CRM.CaseFollow set IsFollow=@IsFollow where CaseId =@CaseId and PersonId = @PersonId and  ModuleId in (1,2)", new { CaseId = id, PersonId = SessionManager.CurrentUser.PersonId, IsFollow = false });
                if (module == 4)
                    connection.Execute("Update CRM.CaseFollow set IsFollow=@IsFollow where CaseId =@CaseId and PersonId = @PersonId and  ModuleId in (1,2)", new { CaseId = id, PersonId = SessionManager.SecurityUser.PersonId, IsFollow = false });

                return false;
            }
        }
        public bool FollowThisCase(int id, int module)
        {
            var query = "";
            var res = new CaseFollow();
            if (module == 1)
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    query = @"Select * from CRM.CaseFollow where CaseId =@CaseId and PersonId = @PersonId and ModuleId in(1,2)";
                    res = connection.Query<CaseFollow>(query, new { CaseId = id, PersonId = SessionManager.CurrentUser.PersonId }).FirstOrDefault();
                    if (res != null)
                        connection.Execute("Update CRM.CaseFollow set IsFollow=@IsFollow where CaseId =@CaseId and PersonId = @PersonId and ModuleId in (1,2)", new { CaseId = id, PersonId = SessionManager.CurrentUser.PersonId, IsFollow = true });
                    else
                        connection.Execute("insert into CRM.CaseFollow values(@CaseId,@PersonId,@Isfollow,@ModuleId, @VendorId)", new { CaseId = id, PersonId = SessionManager.CurrentUser.PersonId, Isfollow = true, ModuleId = module, VendorId = 0 });


                    return true;
                }
            }
            else if (module == 2)
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    query = @"Select * from CRM.CaseFollow where CaseId =@CaseId and PersonId = @PersonId and ModuleId in(1,2)";
                    res = connection.Query<CaseFollow>(query, new { CaseId = id, PersonId = SessionManager.CurrentUser.PersonId }).FirstOrDefault();

                    if (res != null)
                        connection.Execute("Update CRM.CaseFollow set IsFollow=@IsFollow where CaseId =@CaseId and PersonId = @PersonId and ModuleId in (1,2)", new { CaseId = id, PersonId = SessionManager.CurrentUser.PersonId, IsFollow = true });
                    else
                        connection.Execute("insert into CRM.CaseFollow values(@CaseId,@PersonId,@Isfollow,@ModuleId)", new { CaseId = id, PersonId = SessionManager.CurrentUser.PersonId, Isfollow = true, ModuleId = module });


                    return true;

                }
            }
            else if (module == 4)
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    query = @"Select * from CRM.CaseFollow where CaseId =@CaseId and PersonId = @PersonId and ModuleId in(1,2,4)";
                    res = connection.Query<CaseFollow>(query, new { CaseId = id, PersonId = SessionManager.SecurityUser.PersonId }).FirstOrDefault();

                    if (res != null)
                        connection.Execute("Update CRM.CaseFollow set IsFollow=@IsFollow where CaseId =@CaseId and PersonId = @PersonId and ModuleId in (1,2,4)", new { CaseId = id, PersonId = SessionManager.SecurityUser.PersonId, IsFollow = true });
                    else
                        connection.Execute("insert into CRM.CaseFollow(CaseId,PersonId,IsFollow,ModuleId) values(@CaseId,@PersonId,@Isfollow,@ModuleId)", new { CaseId = id, PersonId = SessionManager.SecurityUser.PersonId, Isfollow = true, ModuleId = module });

                    return true;
                }
            }

            return false;
            //using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            //{
            //    var res = new CaseFollow();
            //    if (ModuleId == 1)
            //    res = connection.Query<CaseFollow>(query, new { CaseId = id, PersonId = SessionManager.CurrentUser.PersonId, ModuleId= ModuleId }).FirstOrDefault();

            //    if(ModuleId == 2)
            //        res = connection.Query<CaseFollow>(query, new { CaseId = id, PersonId = SessionManager.CurrentUser.PersonId, ModuleId = ModuleId, VendorId = RefId }).FirstOrDefault();

            //    if (res != null)
            //        connection.Execute("Update CRM.CaseFollow set IsFollow=@IsFollow where CaseId =@CaseId and PersonId = @PersonId and ModuleId=@ModuleId", new { CaseId = id, PersonId = SessionManager.CurrentUser.PersonId, IsFollow = true, ModuleId = ModuleId });
            //    else
            //        connection.Execute("insert into CRM.CaseFollow values(@CaseId,@PersonId,@Isfollow,@ModuleId, @VendorId)", new { CaseId = id, PersonId = SessionManager.CurrentUser.PersonId, Isfollow = true, ModuleId = ModuleId, VendorId = RefId});

            //    return true;
            //}
        }

        public List<KanbanViewModel> GetFranchiseKanbanCaseList(int SelectedCaseType, bool Closed, int FranchiseId)
        {

            var Query = @"select fca.Id,fc.CaseId
                        ,fc.[CaseNumber] 
                        , fca.[VendorCaseNumber]
                        ,acc.AccountId, cu.FirstName+ ' '+cu.LastName as AccountName
                        ,fca.IssueDescription as Description
                        ,fca.[Status] as Status
                        ,lvstatus.[Name] as StatusName 
                        ,lvtype.[Name] as Type
                        ,DATEDIFF(DD,fc.[DateTimeOpened],GETDATE()) as Age
                        ,p.FirstName+' '+p.LastName as CaseOwner
                        ,'Edit' as tags
                        from[CRM].[FranchiseCase] fc
                        inner join[CRM].[FranchiseCaseAddInfo] fca on fc.CaseId=fca.CaseId
                        inner join CRM.PurchaseOrderDetails pod on fca.QuoteLineId = pod.QuoteLineId
                        inner join CRM.QuoteLines ql on pod.QuoteLineId= ql.QuoteLineId
                        inner join CRM.MasterPurchaseOrder mpo on fc.MPO_MasterPONum_POId = mpo.PurchaseOrderId

                        inner join CRM.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId

                        inner join CRM.Opportunities op on mpox.OpportunityId= op.OpportunityId
                        inner join CRM.Accounts acc on mpox.AccountId= acc.AccountId
                        inner join CRM.AccountCustomers accu on acc.AccountId= accu.AccountId
                        inner join CRM.Customer cu on accu.CustomerId= cu.CustomerId and accu.IsPrimaryCustomer= 1
                        inner join CRM.Person p on fc.Owner_PersonId = p.PersonId
                        inner join CRM.Type_LookUpValues lvstatus on fca.[Status]=lvstatus.Id
                        inner join CRM.Type_LookUpValues lvtype on fca.[Type]=lvtype.Id
                        where op.FranchiseId= @FranchiseId ";


            if (Closed != true)
            {
                Query = Query + " and fca.Status < 7007 ";
            }

            if (SelectedCaseType == 1) Query = Query + " and fc.Owner_PersonId = @Owner_PersonId ";
            else if (SelectedCaseType == 2) Query = Query + " and fc.CreatedBy =  @Owner_PersonId ";
            else if (SelectedCaseType == 3) Query = Query + " and fca.Status = 7001 ";
            else if (SelectedCaseType == 4) Query = Query + " ";
            else if (SelectedCaseType == 5) Query = Query + " ";
            else Query = Query + "";




            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {

                {
                    var Value = connection.Query<KanbanViewModel>(Query, new
                    {
                        FranchiseId = SessionManager.CurrentFranchise.FranchiseId,
                        Owner_PersonId = SessionManager.CurrentUser.PersonId


                    }).ToList();
                    return Value;
                }
            }

        }
        public bool UpdateCaseStatus(int id, string Status)
        {
            var HistoryType = "FranchiseCase";
            int statusId = 0;
            switch (Status)
            {
                case "New":
                    statusId = 7000;
                    break;
                case "In Process":
                    statusId = 7001;
                    break;
                case "Vendor":
                    statusId = 7002;
                    break;
                case "Needs Review":
                    statusId = 7003;
                    break;
                case "On Hold":
                    statusId = 7004;
                    break;
                case "Escalated":
                    statusId = 7005;
                    break;
                case "Completed":
                    statusId = 7006;
                    break;
                case "Cancelled":
                    statusId = 7007;
                    break;
                default:
                    break;
            }
            var query = @"select * from [CRM].[FranchiseCaseAddInfo] where Id=@id";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var caseAddInfo = connection.Query<FranchiseCaseAddInfoModel>(query, new { id = id }).FirstOrDefault();
                string previousStatus = "";
                if (caseAddInfo.Status == 7000) previousStatus = "New";
                else if (caseAddInfo.Status == 7001) previousStatus = "In Process";
                else if (caseAddInfo.Status == 7002) previousStatus = "Vendor";
                else if (caseAddInfo.Status == 7003) previousStatus = "Needs Review";
                else if (caseAddInfo.Status == 7004) previousStatus = "On Hold";
                else if (caseAddInfo.Status == 7005) previousStatus = "Escalated";
                else if (caseAddInfo.Status == 7006) previousStatus = "Completed";
                else if (caseAddInfo.Status == 7007) previousStatus = "Cancelled";
                else previousStatus = "";

                var resQuotelines = connection.Query<QuoteLines>(@"select * from [CRM].[QuoteLines] where QuoteLineId=@QuoteLineId", new { QuoteLineId = caseAddInfo.QuoteLineId }).FirstOrDefault();
                List<Tuple<string, string, string, string>> LineUpdates = new List<Tuple<string, string, string, string>>();
                LineUpdates.Add(new Tuple<string, string, string, string>(resQuotelines.QuoteLineNumber.ToString(), "Status", previousStatus, Status));

                caseAddInfo.Status = statusId;
                connection.Update(caseAddInfo);

                var fcai = connection.Query<FranchiseCaseAddInfoModel>(" select * from [CRM].[FranchiseCaseAddInfo] where CaseId=@CaseId and Status=( select min(Status) from [CRM].[FranchiseCaseAddInfo] where CaseId = @CaseId)", new { CaseId = caseAddInfo.CaseId }).FirstOrDefault();
                var fc = connection.Query<FranchiseCaseModel>("select *  from [CRM].[FranchiseCase] where CaseId = @CaseId", new { CaseId = caseAddInfo.CaseId }).FirstOrDefault();

                if (fcai.Status != fc.Status)
                {
                    //HistoryTable tab = new HistoryTable();
                    //tab.Table = "FranchiseCase"; tab.TableId = caseAddInfo.CaseId; tab.Date = DateTime.Today;
                    //tab.UserId = SessionManager.CurrentUser.PersonId;

                    //tab.Field = "Status";
                    //tab.OriginalValue = fc.Status.ToString();
                    //tab.NewValue = fcai.Status.ToString();
                    //Insert<int, HistoryTable>(ContextFactory.CrmConnectionString, tab);

                    connection.Execute("Update [CRM].[FranchiseCase] set Status=@Status where CaseId = @CaseId", new { CaseId = caseAddInfo.CaseId, Status = fcai.Status });

                }
                string htmltext = "<p><b> Line Level Update:</b></p>";
                string tableString = "<tr> <th> Line Number </th> <th> Field </th ><th> Previous value </th> <th> Current value </th></tr>";
                tableString += " <tr><td> " + LineUpdates[0].Item1 + " </td> <td> " + LineUpdates[0].Item2 + "</td><td>  " + LineUpdates[0].Item3 + " </td > <td> " + LineUpdates[0].Item4 + " </td> </tr> ";



                update_Historycase(caseAddInfo.CaseId, HistoryType, null, null, LineUpdates, null, null);
                var re = sendEmailCase(caseAddInfo.CaseId, fc.CaseNumber, htmltext, "Update", tableString);



            }
            return true;
        }

        public string GetUserTextColor(int personId)
        {
            var users = SessionManager.CurrentFranchise.UserCollection().Select(s => new
            {
                s.UserId,
                s.Person.FullName,
                s.PersonId,
                s.ColorId,
                s.Email,
                color = string.Format("{1}", s.ColorId, CacheManager.ColorPalettes.FirstOrDefault(f => f.ColorId == s.ColorId).FGColorToRGB(), true ? " !important" : ""),
            }).ToList();
            //background-color:rgb(23,168,102) !important;border-color:rgb(23,168,102) !important;color:rgb(0,0,0) !important
            if (users.Where(u => u.PersonId == personId).FirstOrDefault() != null)
            {
                var color = users.Where(u => u.PersonId == personId).FirstOrDefault().color;
                return color;
            }
            else
            {
                return null;
            }


        }

        public string Save_Grid(int id, FranchiseCaseModel data)
        {
            foreach (var item in data.AdditionalInfo)
            {
                FranchiseCaseAddInfoModel Addinfo = new FranchiseCaseAddInfoModel();
                if (item.Id != 0)
                {
                    var CaseAddinfoDetails = GetData<FranchiseCaseAddInfoModel>(ContextFactory.CrmConnectionString, item.Id);
                    if (CaseAddinfoDetails != null)
                    {
                        CaseAddinfoDetails.CaseId = CaseAddinfoDetails.CaseId;
                        CaseAddinfoDetails.Type = item.Type;
                        if (item.QuoteLineId2 != null)
                        {
                            CaseAddinfoDetails.QuoteLineId = item.QuoteLineId2.QuoteLineId;
                        }
                        else if (item.QuoteLineId != 0)
                        {
                            CaseAddinfoDetails.QuoteLineId = item.QuoteLineId;
                        }
                        else
                            CaseAddinfoDetails.QuoteLineId = CaseAddinfoDetails.QuoteLineId;


                        CaseAddinfoDetails.ReasonCode = item.ReasonCode;
                        CaseAddinfoDetails.IssueDescription = item.IssueDescription;
                        CaseAddinfoDetails.ExpediteReq = item.ExpediteReq;

                        CaseAddinfoDetails.ExpediteApproved = item.ExpediteApproved;
                        CaseAddinfoDetails.ReqTripCharge = item.ReqTripCharge;
                        CaseAddinfoDetails.TripChargeApproved = item.TripChargeApproved;
                        CaseAddinfoDetails.Status = item.Status;
                        CaseAddinfoDetails.Resolution = item.Resolution;
                        Update<FranchiseCaseAddInfoModel>(ContextFactory.CrmConnectionString, CaseAddinfoDetails);
                    }

                }
                else
                {
                    int vendorcasenum = 1000;
                    //var Value = GetCaseDataValue();
                    //if (Value == null)
                    //{
                    //    Addinfo.VendorCaseNumber = vendorcasenum;
                    //}
                    //else Addinfo.VendorCaseNumber = Value.VendorCaseNumber + 1;
                    Addinfo.CaseId = data.CaseId;
                    Addinfo.Type = item.Type;

                    Addinfo.QuoteLineId = item.QuoteLineId2.QuoteLineId;
                    //Addinfo.QuoteLineId = item.QuoteLineNumber.QuoteLineId;
                    Addinfo.ReasonCode = item.ReasonCode;
                    Addinfo.IssueDescription = item.IssueDescription;
                    Addinfo.ExpediteReq = item.ExpediteReq;

                    Addinfo.ExpediteApproved = item.ExpediteApproved;
                    Addinfo.ReqTripCharge = item.ReqTripCharge;
                    Addinfo.TripChargeApproved = item.TripChargeApproved;
                    Addinfo.Status = item.Status;
                    Addinfo.Resolution = item.Resolution;
                    var result = Insert<int, FranchiseCaseAddInfoModel>(ContextFactory.CrmConnectionString, Addinfo);
                }
            }
            var opt = data.CaseNumber + "|" + data.CaseId;
            return opt.ToString();
            //return null;
        }

        public List<VendorCaseDetailInfo> GetfranchiseCase(int caseid)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var Query = "";
                Query = @"select * from CRM.VendorCase where VendorCaseId=@Vendorcaseid";
                var result = connection.Query<VendorCase>(Query, new { Vendorcaseid = caseid }).FirstOrDefault();
                if (result != null)
                {
                    Query = @"select Vc.VendorCaseId,Vc.CaseId,Vc.VendorCaseNumber,Vc.VendorId,
                              Vc.CreatedOn,Vc.CreatedBy,Vc.LastUpdatedOn,Vc.LastUpdatedBy,
                              CAST(o.OrderNumber as varchar(10)) + '-' + CAST(ql.QuoteLineNumber as varchar(3)) as QuoteLineNumber,
                              Fc.Description as IssueDescription,ql.Description as Description,
                              tltype.Name as Typevalue,tlrea.Name as CaseReason,
                               case when ql.ProductTypeId <> 3 then
                            (select PICProductID from CRM.HFCProducts where ProductID =ql.ProductId)
                            else 
                            ( select ProductID from CRM.FranchiseProducts where ProductKey = ql.ProductId and FranchiseId=opp.FranchiseId) end as  ProductId,
                              case when ql.ProductName <> '' and ql.ProductName IS NOT NULL  then ql.ProductName
							else
                              case when ql.ProductId=0 then '' else case when exists(select ProductId from CRM.HFCProducts where ProductId=ql.ProductId) 
                              then (select ProductGroupDesc from CRM.HFCProducts where ProductId=ql.ProductId) 
                              else case when exists(select ProductId from CRM.FranchiseProducts where ProductId=ql.ProductId) 
                              then (select ProductName from CRM.FranchiseProducts where ProductId=ql.ProductId) else '' end end end end  as ProductName,
                              case when ql.VendorId is null and ql.VendorId=0 then '' else case when exists(select VendorId from Acct.HFCVendors where VendorId=ql.VendorId) 
                              then (select VendorName from Acct.HFCVendors where VendorId=ql.VendorId) 
                              else case when exists(select VendorId from Acct.FranchiseVendors where VendorId=ql.VendorId) 
                              then (select VendorName from Acct.FranchiseVendors fv where ProductId=ql.VendorId) else '' end end end  as VendorName,
                              fca.Status,fca.TripChargeApproved,fca.Resolution,
                              Vcd.CaseLineId,Vcd.VendorCaseDetailId,
                              fca.ExpediteReq,fca.ExpediteApproved,fca.ReqTripCharge,po.PICPO as VPO,
                              tlre.Name as StatusName,tlr.Name as ResolutionName,tl.Name as TripChargeName,fca.Amount
                              from CRM.VendorCase Vc
                              join CRM.FranchiseCase Fc on Vc.CaseId=Fc.CaseId
                              join CRM.VendorCaseDetail  Vcd on Vc.VendorCaseId=Vcd.VendorCaseId
                              join CRM.FranchiseCaseAddInfo fca on Vcd.CaseLineId=fca.Id
                              join CRM.QuoteLines ql on fca.QuoteLineId=ql.QuoteLineId
                              join CRM.Quote q on q.QuoteKey= ql.QuoteKey
							  join CRM.Orders o on o.QuoteKey= q.QuoteKey
                              join CRM.Opportunities opp on opp.OpportunityId = q.OpportunityId
                              join CRM.Type_LookUpValues tltype on fca.Type=tltype.Id
                              join CRM.Type_LookUpValues tlrea on fca.ReasonCode=tlrea.Id
                              left join CRM.PurchaseOrderDetails po on fca.QuoteLineId=po.QuoteLineId
                              left join CRM.Type_LookUpValues tlre on fca.Status=tlre.Id
                              left join CRM.Type_LookUpValues tlr on fca.Resolution=tlr.Id
                              left join CRM.Type_LookUpValues tl on fca.TripChargeApproved=tl.Id
                              where Vc.VendorCaseId=@caseid";

                    var Value = connection.Query<VendorCaseDetailInfo>(Query, new { caseid = result.VendorCaseId }).ToList();


                    return Value;
                }
            }
            return null;
        }

        public string SaveVendor(VendorCaseAddInfoModel data)
        {
            VendorCaseDetail VenCasDet = new VendorCaseDetail();
            VendorCase VenCas = new VendorCase();
            VendorCase Searchvalue = new VendorCase();
            // int Vendorcasenum = 1000;
            var Connection = new SqlConnection(ContextFactory.CrmConnectionString);
            {
                var result = 0; var results = 0;
                if (data.Id != 0)
                {
                    var query = "";
                    query = @"select * from CRM.VendorCase where CaseId=@caseid and VendorId=@VendorId";
                    Searchvalue = Connection.Query<VendorCase>(query, new { caseid = data.CaseId, VendorId = data.VendorId }).FirstOrDefault();
                    if (Searchvalue != null)
                    {
                        if (Searchvalue.CaseId == data.CaseId && Searchvalue.VendorId == data.VendorId)
                        {
                            result = Searchvalue.VendorCaseId;
                        }
                    }
                    else
                    {
                        //data to insert in vendorcase table as new value
                        var Query = "";

                        Query = @"Select count(*) from CRM.VendorCase where VendorId=@VendorId";
                        var Value = Connection.ExecuteScalar<int>(Query, new { VendorId = data.VendorId });
                        if (Value != 0)
                        {
                            VenCas.VendorCaseNumber = data.VendorId.ToString() + "-" + (Value + 1001).ToString();
                        }
                        else
                        {
                            VenCas.VendorCaseNumber = data.VendorId.ToString() + "-1001";
                        }

                        //Query = @"Select Top 1 * from CRM.VendorCase order by VendorCaseId desc";
                        //var Value = Connection.Query<VendorCase>(Query).FirstOrDefault();
                        //if (Value != null)
                        //{
                        //    VenCas.VendorCaseNumber = Value.VendorCaseNumber + 1;
                        //}
                        //else
                        //{
                        //    VenCas.VendorCaseNumber = Vendorcasenum;
                        //}
                        VenCas.CaseId = data.CaseId;
                        VenCas.Status = 7002;
                        VenCas.VendorId = data.VendorId;
                        result = Insert<int, VendorCase>(ContextFactory.CrmConnectionString, VenCas);
                    }

                }
                if (result != 0)
                {
                    //gets vendor caseid from vendorcase table
                    var querys = "";
                    querys = @"select * from CRM.VendorCaseDetail where CaseLineId=@caselineid and VendorCaseId=@vendorcaseid";
                    var Searchvalues = Connection.Query<VendorCaseDetail>(querys, new { caselineid = data.Id, vendorcaseid = result }).FirstOrDefault();
                    if (Searchvalues != null)
                    {
                        results = Searchvalues.VendorCaseDetailId;
                    }
                    else
                    {
                        VenCasDet.CaseLineId = data.Id;
                        //VenCasDet.Status = 7002;
                        //VenCasDet.Resolution = data.Resolution;
                        //VenCasDet.TripChargeApproved = Convert.ToInt32( data.TripChargeApproved);
                        VenCasDet.VendorCaseId = result;
                        results = Insert<int, VendorCaseDetail>(ContextFactory.CrmConnectionString, VenCasDet);
                    }

                }

                var res1 = new Type_LookUpValues();
                if (results != 0)
                {

                    var convert = GetData<FranchiseCaseAddInfoModel>(ContextFactory.CrmConnectionString, data.Id);
                    if (convert != null)
                    {
                        convert.Status = 7002;
                        convert.ConvertToVendor = true;
                        convert.VendorCaseNumber = Searchvalue != null ? Searchvalue.VendorCaseNumber : VenCas.VendorCaseNumber;
                    }
                    Update<FranchiseCaseAddInfoModel>(ContextFactory.CrmConnectionString, convert);

                    if (convert.Status == 7002)
                    {
                        var connection = new SqlConnection(ContextFactory.CrmConnectionString);

                        // var fc = connection.Query<FranchiseCaseModel>("select *  from [CRM].[FranchiseCase] where CaseId = @CaseId", new { CaseId = data.CaseId }).FirstOrDefault();
                        var fc = connection.Query<FranchiseCaseAddInfoModel>("select * from [CRM].[FranchiseCaseAddInfo] where Id in ( select Id from [CRM].[FranchiseCaseAddInfo] where Status= (select min(status) from [CRM].[FranchiseCaseAddInfo] where CaseId =@CaseId) and CaseId=@CaseId)", new { CaseId = data.CaseId }).FirstOrDefault();

                        //if (convert.Status < fc.Status)
                        //{                         
                        connection.Execute("Update [CRM].[FranchiseCase] set Status=@Status where CaseId = @CaseId", new { CaseId = data.CaseId, Status = fc.Status });
                        res1 = Connection.Query<Type_LookUpValues>("select * from[CRM].[Type_LookUpValues] where Id = @Id and TableId = 7", new { Id = fc.Status }).FirstOrDefault();

                        // }
                    }

                }

                //GetFrachiseAttachment(data,result,results);
                //var opt = result+"|"+ results;
                // var opt = result;
                return res1.Name.ToString();
            }

        }

        public Tuple<string, string> SaveVendorVendorView(VendorCaseAddInfoModel data)
        {
            VendorCaseDetail VenCasDet = new VendorCaseDetail();
            VendorCase VenCas = new VendorCase();
            VendorCase Searchvalue = new VendorCase();
            //  int Vendorcasenum = 1000;
            var Connection = new SqlConnection(ContextFactory.CrmConnectionString);
            {
                var result = 0; var results = 0;
                if (data.Id != 0)
                {
                    var query = "";
                    query = @"select * from CRM.VendorCase where CaseId=@caseid and VendorId=@vendorid";
                    Searchvalue = Connection.Query<VendorCase>(query, new { caseid = data.CaseId, vendorid = data.VendorId }).FirstOrDefault();
                    if (Searchvalue != null)
                    {
                        if (Searchvalue.CaseId == data.CaseId && Searchvalue.VendorId == data.VendorId)
                        {
                            result = Searchvalue.VendorCaseId;
                        }
                    }
                    else
                    {
                        //data to insert in vendorcase table as new value
                        var Query = "";
                        Query = @"Select count(*) from CRM.VendorCase where VendorId=@vendorid";
                        var Value = Connection.ExecuteScalar<int>(Query, new { vendorid = data.VendorId });
                        if (Value != 0)
                        {
                            VenCas.VendorCaseNumber = data.VendorId.ToString() + "-" + (Value + 1001).ToString();
                        }
                        else
                        {
                            VenCas.VendorCaseNumber = data.VendorId.ToString() + "-1001";
                        }
                        VenCas.CaseId = data.CaseId;
                        VenCas.Status = 7000;
                        VenCas.VendorId = data.VendorId;
                        result = Insert<int, VendorCase>(ContextFactory.CrmConnectionString, VenCas);
                    }

                }
                if (result != 0)
                {
                    //gets vendor caseid from vendorcase table
                    var querys = "";
                    querys = @"select * from CRM.VendorCaseDetail where CaseLineId=@caselineid and VendorCaseId=@vendorcaseid";
                    var Searchvalues = Connection.Query<VendorCaseDetail>(querys, new { caselineid = data.Id, vendorcaseid = result }).FirstOrDefault();
                    if (Searchvalues != null)
                    {
                        results = Searchvalues.VendorCaseDetailId;
                    }
                    else
                    {
                        VenCasDet.CaseLineId = data.Id;
                        VenCasDet.VendorCaseId = result;
                        results = Insert<int, VendorCaseDetail>(ContextFactory.CrmConnectionString, VenCasDet);
                    }

                }

                var res1 = new Type_LookUpValues();
                if (results != 0)
                {

                    var convert = GetData<FranchiseCaseAddInfoModel>(ContextFactory.CrmConnectionString, data.Id);
                    if (convert != null)
                    {
                        convert.Status = 7002;
                        convert.ConvertToVendor = true;
                        convert.VendorCaseNumber = Searchvalue != null ? Searchvalue.VendorCaseNumber : VenCas.VendorCaseNumber;
                    }
                    Update<FranchiseCaseAddInfoModel>(ContextFactory.CrmConnectionString, convert);

                    if (convert.Status == 7002)
                    {
                        var connection = new SqlConnection(ContextFactory.CrmConnectionString);

                        // var fc = connection.Query<FranchiseCaseModel>("select *  from [CRM].[FranchiseCase] where CaseId = @CaseId", new { CaseId = data.CaseId }).FirstOrDefault();
                        var fc = connection.Query<FranchiseCaseAddInfoModel>("select * from [CRM].[FranchiseCaseAddInfo] where Id in ( select Id from [CRM].[FranchiseCaseAddInfo] where Status= (select min(status) from [CRM].[FranchiseCaseAddInfo] where CaseId =@CaseId) and CaseId=@CaseId)", new { CaseId = data.CaseId }).FirstOrDefault();

                        //if (convert.Status < fc.Status)
                        //{                         
                        connection.Execute("Update [CRM].[FranchiseCase] set Status=@Status where CaseId = @CaseId", new { CaseId = data.CaseId, Status = fc.Status });
                        res1 = Connection.Query<Type_LookUpValues>("select * from[CRM].[Type_LookUpValues] where Id = @Id and TableId = 7", new { Id = fc.Status }).FirstOrDefault();

                        // }
                    }

                }

                //GetFrachiseAttachment(data,result,results);
                //var opt = result+"|"+ results;
                // var opt = result;
                return new Tuple<string, string>(res1.Name.ToString(), VenCas.VendorCaseNumber);
            }

        }

        public string Complete_Convert(List<VendorCaseAddInfoModel> data)
        {
            VendorCaseDetail VenCasDet = new VendorCaseDetail();
            VendorCase VenCas = new VendorCase();
            VendorCase Searchvalue = new VendorCase();
            // int Vendorcasenum = 1000;
            var Connection = new SqlConnection(ContextFactory.CrmConnectionString);
            {
                var result = 0; var results = 0;
                int caseidd = data[0].CaseId;
                foreach (var item in data)
                {
                    if (item.Id != 0)
                    {
                        var query = "";
                        query = @"select * from CRM.VendorCase where CaseId=@caseid and VendorId=@vendorid";
                        Searchvalue = Connection.Query<VendorCase>(query, new { caseid = item.CaseId, vendorid = item.VendorId }).FirstOrDefault();
                        if (Searchvalue != null)
                        {
                            if (Searchvalue.CaseId == item.CaseId && Searchvalue.VendorId == item.VendorId)
                            {
                                result = Searchvalue.VendorCaseId;
                            }
                        }
                        else
                        {
                            //data to insert in vendorcase table as new value
                            var Query = "";
                            Query = @"Select count(*) as totalLines from CRM.VendorCase where VendorId=@Vendorid";
                            var Value = Connection.ExecuteScalar<int>(Query, new { Vendorid = item.VendorId });
                            if (Value != 0)
                            {
                                VenCas.VendorCaseNumber = item.VendorId.ToString() + "-" + (Value + 1001).ToString();
                            }
                            else
                            {
                                VenCas.VendorCaseNumber = item.VendorId.ToString() + "-1001";
                            }
                            //Query = @"Select Top 1 * from CRM.VendorCase order by VendorCaseId desc";
                            //var Value = Connection.Query<VendorCase>(Query).FirstOrDefault();
                            //if (Value != null)
                            //{
                            //    VenCas.VendorCaseNumber = Value.VendorCaseNumber + 1;
                            //}
                            //else
                            //{
                            //    VenCas.VendorCaseNumber = Vendorcasenum;
                            //}
                            VenCas.CaseId = item.CaseId;
                            VenCas.Status = 7000;
                            VenCas.VendorId = item.VendorId;
                            result = Insert<int, VendorCase>(ContextFactory.CrmConnectionString, VenCas);
                        }

                        //insert new value to VendorCaseDetail
                        if (result != 0)
                        {
                            //gets vendor caseid from vendorcase table
                            var querys = "";
                            querys = @"select * from CRM.VendorCaseDetail where CaseLineId=@caselineid and VendorCaseId=@vendorcaseid";
                            var Searchvalues = Connection.Query<VendorCaseDetail>(querys, new { caselineid = item.Id, vendorcaseid = result }).FirstOrDefault();
                            if (Searchvalues != null)
                            {
                                results = Searchvalues.VendorCaseDetailId;
                            }
                            else
                            {
                                VenCasDet.CaseLineId = item.Id;
                                VenCasDet.VendorCaseId = result;
                                results = Insert<int, VendorCaseDetail>(ContextFactory.CrmConnectionString, VenCasDet);
                            }

                        }
                        //update the FranchiseCaseAddInfoModel
                        if (results != 0)
                        {

                            var convert = GetData<FranchiseCaseAddInfoModel>(ContextFactory.CrmConnectionString, item.Id);
                            if (convert != null)
                            {
                                convert.ConvertToVendor = true;
                                convert.Status = 7002;
                                convert.VendorCaseNumber = Searchvalue != null ? Searchvalue.VendorCaseNumber : VenCas.VendorCaseNumber;

                            }
                            Update<FranchiseCaseAddInfoModel>(ContextFactory.CrmConnectionString, convert);
                        }
                        //GetFrachiseAttachment(item, result, results);
                    }

                }


                var connection = new SqlConnection(ContextFactory.CrmConnectionString);

                // var fc = connection.Query<FranchiseCaseModel>("select *  from [CRM].[FranchiseCase] where CaseId = @CaseId", new { CaseId = data.CaseId }).FirstOrDefault();
                var fc = connection.Query<FranchiseCaseAddInfoModel>("select * from [CRM].[FranchiseCaseAddInfo] where Id in ( select Id from [CRM].[FranchiseCaseAddInfo] where Status= (select min(status) from [CRM].[FranchiseCaseAddInfo] where CaseId =@CaseId) and CaseId=@CaseId)", new { CaseId = caseidd }).FirstOrDefault();

                connection.Execute("Update [CRM].[FranchiseCase] set Status=@Status where CaseId = @CaseId", new { CaseId = caseidd, Status = fc.Status });
                Type_LookUpValues res1 = Connection.Query<Type_LookUpValues>("select * from[CRM].[Type_LookUpValues] where Id = @Id and TableId = 7", new { Id = fc.Status }).FirstOrDefault();


                //var opt = "Success";
                return res1.Name.ToString();
            }




        }

        public Tuple<string, string> Complete_ConvertVendorView(List<VendorCaseAddInfoModel> data)
        {
            VendorCaseDetail VenCasDet = new VendorCaseDetail();
            VendorCase VenCas = new VendorCase();
            VendorCase Searchvalue = new VendorCase();
            //   int Vendorcasenum = 1000;
            var Connection = new SqlConnection(ContextFactory.CrmConnectionString);
            {
                var result = 0; var results = 0;
                int caseidd = data[0].CaseId;
                foreach (var item in data)
                {
                    if (item.Id != 0)
                    {
                        var query = "";
                        query = @"select * from CRM.VendorCase where CaseId=@caseid and VendorId=@vendorid";
                        Searchvalue = Connection.Query<VendorCase>(query, new { caseid = item.CaseId, vendorid = item.VendorId }).FirstOrDefault();
                        if (Searchvalue != null)
                        {
                            if (Searchvalue.CaseId == item.CaseId && Searchvalue.VendorId == item.VendorId)
                            {
                                result = Searchvalue.VendorCaseId;
                            }
                        }
                        else
                        {
                            //data to insert in vendorcase table as new value
                            var Query = "";
                            Query = @"Select count(*) from CRM.VendorCase where VendorId=@Vendorid";
                            var Value = Connection.ExecuteScalar<int>(Query, new { Vendorid = item.VendorId });
                            if (Value != 0)
                            {
                                VenCas.VendorCaseNumber = item.VendorId.ToString() + "-" + (Value + 1001).ToString();
                            }
                            else
                            {
                                VenCas.VendorCaseNumber = item.VendorId.ToString() + "-1001";
                            }
                            //Query = @"Select Top 1 * from CRM.VendorCase order by VendorCaseId desc";
                            //var Value = Connection.Query<VendorCase>(Query).FirstOrDefault();
                            //if (Value != null)
                            //{
                            //    VenCas.VendorCaseNumber = Value.VendorCaseNumber + 1;
                            //}
                            //else
                            //{
                            //    VenCas.VendorCaseNumber = Vendorcasenum;
                            //}
                            VenCas.CaseId = item.CaseId;
                            VenCas.Status = 7000;
                            VenCas.VendorId = item.VendorId;
                            result = Insert<int, VendorCase>(ContextFactory.CrmConnectionString, VenCas);
                        }

                        //insert new value to VendorCaseDetail
                        if (result != 0)
                        {
                            //gets vendor caseid from vendorcase table
                            var querys = "";
                            querys = @"select * from CRM.VendorCaseDetail where CaseLineId=@caselineid and VendorCaseId=@vendorcaseid";
                            var Searchvalues = Connection.Query<VendorCaseDetail>(querys, new { caselineid = item.Id, vendorcaseid = result }).FirstOrDefault();
                            if (Searchvalues != null)
                            {
                                results = Searchvalues.VendorCaseDetailId;
                            }
                            else
                            {
                                VenCasDet.CaseLineId = item.Id;
                                VenCasDet.VendorCaseId = result;
                                results = Insert<int, VendorCaseDetail>(ContextFactory.CrmConnectionString, VenCasDet);
                            }

                        }
                        //update the FranchiseCaseAddInfoModel
                        if (results != 0)
                        {

                            var convert = GetData<FranchiseCaseAddInfoModel>(ContextFactory.CrmConnectionString, item.Id);
                            if (convert != null)
                            {
                                convert.ConvertToVendor = true;
                                convert.Status = 7002;
                                convert.VendorCaseNumber = Searchvalue != null ? Searchvalue.VendorCaseNumber : VenCas.VendorCaseNumber;
                            }
                            Update<FranchiseCaseAddInfoModel>(ContextFactory.CrmConnectionString, convert);
                        }
                        //GetFrachiseAttachment(item, result, results);
                    }

                }


                var connection = new SqlConnection(ContextFactory.CrmConnectionString);

                // var fc = connection.Query<FranchiseCaseModel>("select *  from [CRM].[FranchiseCase] where CaseId = @CaseId", new { CaseId = data.CaseId }).FirstOrDefault();
                var fc = connection.Query<FranchiseCaseAddInfoModel>("select * from [CRM].[FranchiseCaseAddInfo] where Id in ( select Id from [CRM].[FranchiseCaseAddInfo] where Status= (select min(status) from [CRM].[FranchiseCaseAddInfo] where CaseId =@CaseId) and CaseId=@CaseId)", new { CaseId = caseidd }).FirstOrDefault();

                connection.Execute("Update [CRM].[FranchiseCase] set Status=@Status where CaseId = @CaseId", new { CaseId = caseidd, Status = fc.Status });
                Type_LookUpValues res1 = Connection.Query<Type_LookUpValues>("select * from[CRM].[Type_LookUpValues] where Id = @Id and TableId = 7", new { Id = fc.Status }).FirstOrDefault();


                //var opt = "Success";
                return new Tuple<string, string>(res1.Name.ToString(), VenCas.VendorCaseNumber);
            }




        }

        public List<VendorCaseList> VendorGridList(int SelectedCase, bool Closed, int FranchiseId)
        {
            var Connection = new SqlConnection(ContextFactory.CrmConnectionString);
            {
                var query = "";
                List<VendorCaseList> result;
                //query = @"select Vc.VendorCaseId,Vc.CaseId,Vc.VendorCaseNumber,Vc.VendorId,Vc.CreatedOn,Vc.CreatedBy,Vc.LastUpdatedOn,Vc.LastUpdatedBy
                //         ,mpo.MasterPONumber as MPO,po.PICPO as VPO,
                //         fca.Status,lvstatus.Name as StatusName,fca.IssueDescription,
                //         lvtype.Name as TypeName,fc.CreatedOn as IncidentDate,
                //         p.FirstName+' '+p.LastName as CaseOwner,
                //         F.Name as FranchiseName,cu.FirstName+ ' '+cu.LastName as AccountName,
                //         (select f.TimezoneCode  from auth.Users u left join CRM.Franchise f on f.FranchiseId = u.FranchiseId  where u.PersonId=Fc.Owner_PersonId) as TimezoneEnum
                //         from CRM.VendorCase vc
                //         join CRM.FranchiseCase Fc on Vc.CaseId=Fc.CaseId
                //         join CRM.VendorCaseDetail  Vcd on Vc.VendorCaseId=Vcd.VendorCaseId
                //         join CRM.FranchiseCaseAddInfo fca on Vcd.CaseLineId=fca.Id
                //         join CRM.QuoteLines ql on fca.QuoteLineId=ql.QuoteLineId
                //         join CRM.Type_LookUpValues tltype on fca.Type=tltype.Id
                //         join CRM.Type_LookUpValues tlrea on fca.ReasonCode=tlrea.Id
                //         join CRM.PurchaseOrderDetails po on fca.QuoteLineId=po.QuoteLineId
                //         join CRM.MasterPurchaseOrder mpo on Fc.MPO_MasterPONum_POId = mpo.PurchaseOrderId
                //         join CRM.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId

                //         join CRM.Opportunities op on mpox.OpportunityId= op.OpportunityId
                //         join CRM.Type_LookUpValues lvstatus on fca.Status=lvstatus.Id
                //         join CRM.Type_LookUpValues lvtype on fca.[Type]=lvtype.Id
                //         join CRM.Person p on fc.Owner_PersonId = p.PersonId
                //         join CRM.Franchise F on op.FranchiseId=F.FranchiseId
                //         join CRM.Accounts acc on mpox.AccountId= acc.AccountId
                //         join CRM.AccountCustomers accu on acc.AccountId= accu.AccountId
                //         join CRM.Customer cu on accu.CustomerId= cu.CustomerId and accu.IsPrimaryCustomer= 1";//where  op.FranchiseId= @Franchiseid;

                query = @"select distinct Vc.VendorCaseId,Vc.CaseId,Vc.VendorCaseNumber,Vc.VendorId,Vc.CreatedOn,Vc.CreatedBy,Vc.LastUpdatedOn,Vc.LastUpdatedBy
                        ,mpo.MasterPONumber as MPO,po.PICPO as VPO,
                         fca.Status,lvstatus.Name as StatusName,fca.IssueDescription,
                         lvtype.Name as TypeName,fc.CreatedOn as IncidentDate,
                         p.FirstName+' '+p.LastName as CaseOwner,
                         F.Name as FranchiseName,cu.FirstName+ ' '+cu.LastName as AccountName,Vcd.CreatedOn,
                         (select f.TimezoneCode  from auth.Users u left join CRM.Franchise f on f.FranchiseId = u.FranchiseId  where u.PersonId=Fc.Owner_PersonId) as TimezoneEnum
                         from CRM.VendorCase vc
                         join CRM.FranchiseCase Fc on Vc.CaseId=Fc.CaseId
                         join CRM.VendorCaseDetail  Vcd on Vc.VendorCaseId=Vcd.VendorCaseId
                         join CRM.FranchiseCaseAddInfo fca on Vcd.CaseLineId=fca.Id
                         join CRM.QuoteLines ql on fca.QuoteLineId=ql.QuoteLineId
						 join CRM.Quote q on ql.QuoteKey= q.QuoteKey
                         join CRM.Type_LookUpValues tltype on fca.Type=tltype.Id
                         join CRM.Type_LookUpValues tlrea on fca.ReasonCode=tlrea.Id
                         left join CRM.PurchaseOrderDetails po on fca.QuoteLineId=po.QuoteLineId
                         left join CRM.MasterPurchaseOrder mpo on Fc.MPO_MasterPONum_POId = mpo.PurchaseOrderId
                         left join CRM.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId

                         join CRM.Opportunities op on q.OpportunityId= op.OpportunityId
                         join CRM.Type_LookUpValues lvstatus on fca.Status=lvstatus.Id
                         join CRM.Type_LookUpValues lvtype on fca.[Type]=lvtype.Id
                         join CRM.Person p on fc.Owner_PersonId = p.PersonId
                         join CRM.Franchise F on op.FranchiseId=F.FranchiseId
                         join CRM.Accounts acc on op.AccountId= acc.AccountId
                         join CRM.AccountCustomers accu on acc.AccountId= accu.AccountId
                         join CRM.Customer cu on accu.CustomerId= cu.CustomerId and
						 accu.IsPrimaryCustomer= 1";
                if (SelectedCase == 2)
                {
                    query = query + " " + "and fca.Status=7001";
                }
                if (Closed == true)
                {
                    query = query + " " + "and fca.Status>=7006";
                }
                else query = query + " " + "and fca.Status < 7006";

                query = query + " " + " and vc.VendorId=(select VendorId from Acct.HFCVendors where VendorIdPk in (select VendorId from CRM.VendorCaseConfig where PersonId=@PersonId))";

                if (FranchiseId == 0)
                {
                    query = query + " order by Vcd.CreatedOn desc";
                    //result = Connection.Query<VendorCaseList>(query, new { Franchiseid = User.FranchiseId }).ToList();
                    result = Connection.Query<VendorCaseList>(query, new { PersonId = SessionManager.CurrentUser.PersonId }).ToList();
                }
                else
                {
                    query = query + " where op.FranchiseId= @Franchiseid order by Vcd.CreatedOn desc";
                    result = Connection.Query<VendorCaseList>(query, new { Franchiseid = FranchiseId, PersonId = SessionManager.CurrentUser.PersonId }).ToList();
                }

                return result;
            }

        }

        public string SaveVendorGridData(int vendorcaseid, List<VendorCaseDetailInfo> model)
        {
            VendorCaseDetail VenCasDet = new VendorCaseDetail();
            VendorCase VenCas = new VendorCase();
            var query = ""; var HistoryType = "VendorCase";


            var Connection = new SqlConnection(ContextFactory.CrmConnectionString);
            {

                List<string> LinesDeleted = new List<string>();
                List<string> LinesAdded = new List<string>();
                List<Tuple<string, string, string, string>> LineUpdates = new List<Tuple<string, string, string, string>>();
                List<Tuple<string, string>> LineChange = new List<Tuple<string, string>>();
                List<Tuple<string, string, string>> CaseChange = new List<Tuple<string, string, string>>();


                foreach (var item in model)
                {
                    if (item.VendorCaseDetailId != 0)
                    {

                        var res = Connection.Query<VendorCaseDetail>("Select * from CRM.VendorCaseDetail where VendorCaseId=@CaseId", new { CaseId = item.VendorCaseId }).ToList();

                        var Lookupvalues = Connection.Query<Type_LookUpValues>("Select * from CRM.Type_LookUpValues").ToList();
                        var resLinestocompare_before = (from ram in res where ram.VendorCaseDetailId == item.VendorCaseDetailId select ram).ToList();


                        //if (resLinestocompare_before[0].TripChargeApproved != null)
                        //{
                        //    if (item.TripChargeApproved != resLinestocompare_before[0].TripChargeApproved)
                        //        LineUpdates.Add(new Tuple<string, string, string, string>(item.QuoteLineNumber.ToString(), "TripChargeApproved", (from ram in Lookupvalues where ram.Id == resLinestocompare_before[0].TripChargeApproved select ram).ToList()[0].Name, (from ram in Lookupvalues where ram.Id == item.TripChargeApproved select ram).ToList()[0].Name));
                        //}
                        //else
                        //{
                        //    if (item.TripChargeApproved != resLinestocompare_before[0].TripChargeApproved)
                        //        LineUpdates.Add(new Tuple<string, string, string, string>(item.QuoteLineNumber.ToString(), "TripChargeApproved", "", (from ram in Lookupvalues where ram.Id == item.TripChargeApproved select ram).ToList()[0].Name));

                        //}
                        //if (resLinestocompare_before[0].Resolution != null)
                        //{
                        //    if (item.Resolution != resLinestocompare_before[0].Resolution)
                        //        LineUpdates.Add(new Tuple<string, string, string, string>(item.QuoteLineNumber.ToString(), "Resolution", (from ram in Lookupvalues where ram.Id == resLinestocompare_before[0].Resolution select ram).ToList()[0].Name, (from ram in Lookupvalues where ram.Id == item.Resolution select ram).ToList()[0].Name));
                        //}
                        //else
                        //{
                        //    if (item.Resolution != resLinestocompare_before[0].Resolution)
                        //        LineUpdates.Add(new Tuple<string, string, string, string>(item.QuoteLineNumber.ToString(), "Resolution","", (from ram in Lookupvalues where ram.Id == item.Resolution select ram).ToList()[0].Name));

                        //}
                        //if (resLinestocompare_before[0].Status != null)
                        //{
                        //    if (item.Status != resLinestocompare_before[0].Status)
                        //        LineUpdates.Add(new Tuple<string, string, string, string>(item.QuoteLineNumber.ToString(), " Status", (from ram in Lookupvalues where ram.Id == resLinestocompare_before[0].Status select ram).ToList()[0].Name, (from ram in Lookupvalues where ram.Id == item.Status select ram).ToList()[0].Name));
                        //}
                        //else
                        //{
                        //    if (item.Status != resLinestocompare_before[0].Status)
                        //        LineUpdates.Add(new Tuple<string, string, string, string>(item.QuoteLineNumber.ToString(), " Status", "", (from ram in Lookupvalues where ram.Id == item.Status select ram).ToList()[0].Name));

                        //}

                        var Fraaddinfo = Connection.Query<FranchiseCaseAddInfoModel>("select  * from CRM.FranchiseCaseAddInfo where Id=@Id", new { Id = item.CaseLineId }).FirstOrDefault();


                        if (Fraaddinfo.Resolution != null)
                        {
                            if (item.Resolution != Fraaddinfo.Resolution)
                                LineUpdates.Add(new Tuple<string, string, string, string>(item.QuoteLineNumber.ToString(), "Resolution", (from ram in Lookupvalues where ram.Id == Fraaddinfo.Resolution select ram).ToList()[0].Name, (from ram in Lookupvalues where ram.Id == item.Resolution select ram).ToList()[0].Name));
                        }
                        else
                        {
                            if (item.Resolution != Fraaddinfo.Resolution)
                                LineUpdates.Add(new Tuple<string, string, string, string>(item.QuoteLineNumber.ToString(), "Resolution", "", (from ram in Lookupvalues where ram.Id == item.Resolution select ram).ToList()[0].Name));

                        }
                        if (Fraaddinfo.Status != null)
                        {
                            if (item.Status != Fraaddinfo.Status)
                                LineUpdates.Add(new Tuple<string, string, string, string>(item.QuoteLineNumber.ToString(), " Status", (from ram in Lookupvalues where ram.Id == Fraaddinfo.Status select ram).ToList()[0].Name, (from ram in Lookupvalues where ram.Id == item.Status select ram).ToList()[0].Name));
                        }
                        else
                        {
                            if (item.Status != Fraaddinfo.Status)
                                LineUpdates.Add(new Tuple<string, string, string, string>(item.QuoteLineNumber.ToString(), " Status", "", (from ram in Lookupvalues where ram.Id == item.Status select ram).ToList()[0].Name));

                        }

                        if (item.Amount > 0) item.Amount = Convert.ToDecimal(string.Format("{0:0.00}", item.Amount));
                        if (resLinestocompare_before[0].TripChargeApproved != null)
                        {
                            if (Fraaddinfo.Amount == null)
                                Fraaddinfo.Amount = 0;
                            if (item.TripChargeApproved != Fraaddinfo.TripChargeApproved)
                            {
                                if (item.TripChargeApproved == 9000)
                                    LineUpdates.Add(new Tuple<string, string, string, string>(item.QuoteLineNumber.ToString(), "TripChargeApproved", (from ram in Lookupvalues where ram.Id == Fraaddinfo.TripChargeApproved select ram).ToList()[0].Name, item.Amount == null ? "" : string.Format("{0:C}", item.Amount.Value)));
                                else
                                    LineUpdates.Add(new Tuple<string, string, string, string>(item.QuoteLineNumber.ToString(), "TripChargeApproved", Fraaddinfo.Amount == null ? "" : string.Format("{0:C}", Fraaddinfo.Amount.Value), (from ram in Lookupvalues where ram.Id == item.TripChargeApproved select ram).ToList()[0].Name));
                            }
                            else
                            {
                                if (item.Amount != Fraaddinfo.Amount)
                                    LineUpdates.Add(new Tuple<string, string, string, string>(item.QuoteLineNumber.ToString(), "TripChargeApproved", Fraaddinfo.Amount == null ? "" : string.Format("{0:C}", Fraaddinfo.Amount.Value), item.Amount == null ? "" : string.Format("{0:C}", item.Amount.Value)));
                            }

                        }
                        else
                        {
                            if (item.TripChargeApproved != Fraaddinfo.TripChargeApproved)
                            {
                                if (item.TripChargeApproved == 9000)
                                    LineUpdates.Add(new Tuple<string, string, string, string>(item.QuoteLineNumber.ToString(), "TripChargeApproved", "", item.Amount == null ? "" : string.Format("{0:C}", item.Amount.Value)));
                                else
                                    LineUpdates.Add(new Tuple<string, string, string, string>(item.QuoteLineNumber.ToString(), "TripChargeApproved", "", (from ram in Lookupvalues where ram.Id == item.TripChargeApproved select ram).ToList()[0].Name));
                            }
                        }

                        if (item.ExpediteApproved != Fraaddinfo.ExpediteApproved)

                            if (item.ExpediteApproved != Fraaddinfo.ExpediteApproved)
                                LineUpdates.Add(new Tuple<string, string, string, string>(item.QuoteLineNumber.ToString(), " Expedite Approved", Fraaddinfo.ExpediteApproved.ToString(), item.ExpediteApproved.ToString()));



                        query = @"select * from CRM.VendorCaseDetail where VendorCaseDetailId=@Vendorcasedetid";
                        var result = Connection.Query<VendorCaseDetail>(query, new { Vendorcasedetid = item.VendorCaseDetailId }).FirstOrDefault();
                        if (result != null)
                        {
                            VenCasDet = result;
                            result.Status = item.Status;
                            result.Resolution = item.Resolution;
                            result.TripChargeApproved = item.TripChargeApproved;
                            result.Amount = item.TripChargeApproved != 9000 ? 0 : item.Amount;
                            Update<VendorCaseDetail>(ContextFactory.CrmConnectionString, result);

                        }

                        Connection.Execute(@"Update CRM.FranchiseCaseAddInfo set ExpediteApproved=@ExpediteApproved,Resolution=@Resolution,Status=@Status,TripChargeApproved=@TripChargeApproved,Amount=@Amount,
                                              LastUpdatedOn=@LastUpdatedOn,LastUpdatedBy=@LastUpdatedBy where Id=@Id",
                                              new
                                              {
                                                  ExpediteApproved = item.ExpediteApproved,
                                                  Resolution = item.Resolution,
                                                  Status = item.Status,
                                                  TripChargeApproved = item.TripChargeApproved,
                                                  Amount = item.TripChargeApproved != 9000 ? 0 : item.Amount,
                                                  Id = item.CaseLineId
                                             ,
                                                  LastUpdatedOn = DateTime.UtcNow,
                                                  LastUpdatedBy = SessionManager.CurrentUser.PersonId
                                              });
                        //update_Historycase(vendorcaseid, HistoryType, LinesDeleted, LinesAdded, LineUpdates, LineChange, CaseChange);
                    }
                }

                query = @"select * from CRM.VendorCaseDetail where VendorCaseId=@VendorCaseId and 
                          Status=(select min(Status) from CRM.VendorCaseDetail where VendorCaseId=@VendorCaseId)";
                var Ven = Connection.Query<VendorCaseDetail>(query, new { VendorCaseId = vendorcaseid }).FirstOrDefault();
                query = @"Select * from CRM.VendorCase where VendorCaseId=@VendorCaseId";
                var VenCase = Connection.Query<VendorCase>(query, new { VendorCaseId = vendorcaseid }).FirstOrDefault();
                if (Ven != null)
                {
                    if (Ven.Status != VenCase.Status)
                    {
                        Connection.Execute("Update [CRM].[VendorCase] set Status=@Status where VendorCaseId = @CaseId", new { CaseId = vendorcaseid, Status = Ven.Status });
                    }
                }

                var resultt = Connection.Query<VendorCase>("select * from CRM.VendorCase where VendorCaseId=@Vendorcasedetid", new { Vendorcasedetid = vendorcaseid }).FirstOrDefault();

                if (resultt != null)
                {
                    resultt.LastUpdatedOn = DateTime.UtcNow;
                    Update<VendorCase>(ContextFactory.CrmConnectionString, resultt);

                }



                var statusNumber = Connection.Query("select min(status) as Status from CRM.FranchiseCaseAddInfo where CaseId=@CaseId", new { CaseId = resultt.CaseId }).FirstOrDefault();
                //  var dfdfdf = 0;
                var FraStatus = Connection.Query("select  Status from CRM.FranchiseCase where CaseId=@CaseId", new { CaseId = resultt.CaseId }).FirstOrDefault();
                if (statusNumber.Status != FraStatus.Status && (statusNumber.Status == 7006 || statusNumber.Status == 7007))
                    Connection.Execute(@"Update CRM.Franchisecase set Status=@Status,
                                    DateTimeClosed= @DateTimeClosed,LastUpdatedOn=@LastUpdatedOn,LastUpdatedBy=@LastUpdatedBy where CaseId =@CaseId",
                                        new
                                        {
                                            Status = statusNumber.Status,
                                            CaseId = resultt.CaseId,
                                            DateTimeClosed = DateTime.UtcNow,
                                            LastUpdatedOn = DateTime.UtcNow,
                                            LastUpdatedBy = SessionManager.CurrentUser.PersonId
                                        });
                else
                {
                    Connection.Execute(@"Update CRM.Franchisecase set Status=@Status,LastUpdatedOn=@LastUpdatedOn,LastUpdatedBy=@LastUpdatedBy where CaseId =@CaseId",
                                   new
                                   {
                                       Status = statusNumber.Status,
                                       CaseId = resultt.CaseId,
                                       LastUpdatedOn = DateTime.UtcNow,
                                       LastUpdatedBy = SessionManager.CurrentUser.PersonId
                                   });
                }

                // Connection.Execute("Update [CRM].[VendorCase] set LastUpdatedOn=@datee, LastUpdatedBy=@LastUpdatedBy where VendorCaseId = @CaseId", new { CaseId = vendorcaseid, datee= DateTime.UtcNow, LastUpdatedBy=});
                update_Historycase(resultt.CaseId, HistoryType, LinesDeleted, LinesAdded, LineUpdates, LineChange, CaseChange);
                var returnresult = VenCase.VendorCaseId;
                return returnresult.ToString();
            }
            //return null;

        }

        public FranchiseCaseModel GetFranchiseDetails(int vendorcaseid)
        {
            if (vendorcaseid != 0)
            {
                var query = "";
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    query = @"select * from CRM.VendorCase where VendorCaseId=@VendorCaseId";
                    var result = connection.Query<VendorCase>(query, new { VendorCaseId = vendorcaseid }).FirstOrDefault();
                    if (result != null)
                    {
                        var ress = connection.Query<FranchiseCaseModel>("Select * from CRM.FranchiseCase where CaseId=@CaseId", new { CaseId = result.CaseId }).FirstOrDefault();
                        var Query = "";
                        Query = @"Select fc.CaseId,fc.CaseNumber,vc.VendorCaseId as VendorCaseId,vc.VendorCaseNumber as VendorCaseNumber,fc.Owner_PersonId,fc.IncidentDate,fc.DateTimeOpened,fc.DateTimeClosed,fc.Description,
                                      fc.VPO_PICPO_PODId,pod.PICPO as POID,
                                      fc.MPO_MasterPONum_POId,fc.SalesOrderId,vc.CreatedOn,vc.CreatedBy,vc.LastUpdatedOn,vc.LastUpdatedBy,
                                      mpo.MasterPONumber as MPOID,o.OrderNumber as SalesorderNumber,fc.Status,tl.Name as StatusValue, 
                                      fra.Name as FranchiseName,fra.LocalPhoneNumber,Per.FirstName +' ' + Per.LastName as OwnerName
                                      from CRM.VendorCase vc
                                      join CRM.FranchiseCase Fc on Vc.CaseId=Fc.CaseId
									  join CRM.VendorCaseDetail  Vcd on Vc.VendorCaseId=Vcd.VendorCaseId
									  join CRM.FranchiseCaseAddInfo fca on Vcd.CaseLineId=fca.Id
									  join CRM.QuoteLines ql on fca.QuoteLineId=ql.QuoteLineId
									  join CRM.Quote q on ql.QuoteKey= q.QuoteKey                                     
                                      join CRM.Orders o on o.QuoteKey=q.QuoteKey
                                      join CRM.Type_LookUpValues tl on fc.Status=tl.Id
                                      join CRM.Opportunities opp on q.OpportunityId=opp.OpportunityId
                                      join CRM.Franchise fra on opp.FranchiseId=fra.FranchiseId
                                      left join CRM.Person Per on Per.PersonId = fc.Owner_PersonId
									  left join CRM.PurchaseOrderDetails pod on pod.QuoteLineId = fca.QuoteLineId
                                      left join CRM.MasterPurchaseOrder mpo on pod.PurchaseOrderId=mpo.PurchaseOrderId 
                                      left join CRM.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId=mpo.PurchaseOrderId
                                      where vc.VendorCaseId=@VendorCaseId";
                        var Value = connection.Query<FranchiseCaseModel>(Query, new { VendorCaseId = result.VendorCaseId }).FirstOrDefault();


                        var fff = SessionManager.CurrentUser;

                        if (Value != null)
                        {
                            query = @"select * from CRM.Person Where PersonId=@PersonId";
                            var User_Value = connection.Query<Person>(query, new { PersonId = result.CreatedBy }).FirstOrDefault();
                            // var User_Value = UserDetails(result.CreatedBy);
                            Value.Name = User_Value.FirstName + " " + User_Value.LastName;
                            if (result.CreatedBy == result.LastUpdatedBy)
                            {
                                Value.LastUPdatedName = Value.Name;
                            }
                            else
                            {
                                query = @"select * from CRM.Person Where PersonId=@PersonId";
                                var Last_modi = connection.Query<Person>(query, new { PersonId = result.LastUpdatedBy }).FirstOrDefault();
                                Value.LastUPdatedName = Last_modi.FirstName + " " + Last_modi.LastName;
                            }
                            var OthDetails = Details(Value.CaseId);
                            Value.AccountName = OthDetails.AccountName;
                            Value.CellPhone = OthDetails.CellPhone;
                            Value.PrimaryEmail = OthDetails.PrimaryEmail;
                            Value.SideMark = OthDetails.SideMark;

                            //Value.CreatedOn = TimeZoneManager.ToLocal(Value.CreatedOn);
                            //Value.LastUpdatedOn = TimeZoneManager.ToLocal(Value.LastUpdatedOn);
                            //Value.DateTimeOpened = TimeZoneManager.ToLocal(Value.DateTimeOpened);
                            //Value.DateTimeClosed = TimeZoneManager.ToLocal(Value.DateTimeClosed);

                        }
                        return Value;
                    }

                }
            }
            return null;
        }

        public List<VendorFranchiseDetails> GetFranchiseData()
        {
            var query = "";
            var id = User.FranchiseId;
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                query = @"Select Distinct fra.FranchiseId,fra.Name as FranchiseName from CRM.VendorCase vc
                         join CRM.FranchiseCase fc on vc.CaseId=fc.CaseId
                         join CRM.MasterPurchaseOrder mpo on fc.MPO_MasterPONum_POId=mpo.PurchaseOrderId

                        join CRM.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId=mpo.PurchaseOrderId

                         join CRM.Opportunities opp on mpox.OpportunityId=opp.OpportunityId
                         join CRM.Franchise fra on opp.FranchiseId=fra.FranchiseId
                         where vc.VendorId=(select VendorId from Acct.HFCVendors where VendorIdPk in (select VendorId from CRM.VendorCaseConfig where PersonId=@PersonId))";
                var result = connection.Query<VendorFranchiseDetails>(query, new { PersonId = SessionManager.CurrentUser.PersonId }).ToList();
                if (result != null)
                {
                    foreach (var item in result)
                    {
                        if (item.FranchiseId == id)
                        {
                            item.Valid = true;
                        }
                        else
                            item.Valid = false;
                    }
                }
                return result;
            }

        }

        public VendorCalendar GetVenodorDetailsCal(int vendorcaseid)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                {
                    var Value = connection.Query<VendorCalendar>
                    (@"select vc.VendorCaseId,vc.CaseId,vc.VendorCaseNumber,
                       mpox.AccountId,mpox.OpportunityId,mpox.OrderId,ac.IsNotifyemails
                       from CRM.VendorCase vc
                       join CRM.FranchiseCase fc on vc.CaseId=fc.CaseId
                       join CRM.MasterPurchaseOrder mpo on fc.MPO_MasterPONum_POId=mpo.PurchaseOrderId
                        join CRM.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId=mpo.PurchaseOrderId

                       left join CRM.Accounts ac on mpox.AccountId=ac.AccountId
                       Where vc.VendorCaseId=@CaseId", new { CaseId = vendorcaseid }).FirstOrDefault();
                    return Value;
                }
            }
        }
        public List<HistoryTable> GetVendorCaseHistoryForCase(int id)
        {
            //var query = @"SELECT ht.Date,ht.Field, ht.UserId, per.FirstName +' '+ per.LastName as UserName,
            //              ht.OriginalValue as OriginalValue,ht.NewValue  as NewValue FROM [CRM].[HistoryTable] ht
            //              join  crm.person per on ht.UserId = per.PersonId
            //              Where ht.TableId=@Id and [Table] like'%vendor%'";
            var query = @"SELECT ht.Date,ht.Field, ht.UserId, ht.LastUpdatedOn,
                          ht.OriginalValue as OriginalValue,ht.NewValue as NewValue,  P.FirstName + '  ' +P.LastName as UserName  FROM [CRM].[HistoryTable] ht
                          left join CRM.Person P on ht.UserId= P.PersonId
                          Where ht.TableId = @Id order by LastUpdatedOn desc";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                {
                    var res = connection.Query<HistoryTable>(query, new { Id = id }).ToList();

                    var frran = connection.Query<Franchise>(@"select f.* from CRM.Franchise f join Auth.Users u on u.FranchiseID = f.FranchiseId
                          join CRM.Person p on p.PersonId = u.PersonId join CRM.FranchiseCase fc on fc.Owner_PersonId = p.PersonId
                          where fc.CaseId =@CaseId", new { CaseId = id }).FirstOrDefault();


                    foreach (var obj in res)
                    {
                        obj.LastUpdatedOn = TimeZoneManager.ToLocal(obj.LastUpdatedOn, frran.TimezoneCode);
                        obj.FranTimezone = frran.TimezoneCode.ToString();


                    }
                    return res;
                }
            }
        }

        public Franchise getFranchiseTimeZone(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                {
                    return connection.Query<Franchise>(@"select f.* from CRM.Franchise f join Auth.Users u on u.FranchiseID = f.FranchiseId
                          join CRM.Person p on p.PersonId = u.PersonId join CRM.FranchiseCase fc on fc.Owner_PersonId = p.PersonId
                          where fc.CaseId =@CaseId", new { CaseId = id }).FirstOrDefault();

                }
            }

        }
        public List<Franchise> GetFranchiseList()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var result = connection.Query<Franchise>(@"select FranchiseId,Name from CRM.Franchise
                    where IsDeleted=0 and IsSuspended=0 order by Name Asc").ToList();

                return result;
            }
        }
        public List<VendorList> GetVendorList()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var result = connection.Query<VendorList>(@";with cte as (
				  select distinct F.VendorId,V.Name,V.VendorType from Acct.FranchiseVendors F
                  inner join Acct.HFCVendors V on F.VendorId=V.VendorId and F.VendorStatus=1
				  union
				  select distinct V.VendorId, (F.Code + ' - ' + V.Name )as Name,V.VendorType from Acct.FranchiseVendors V
                  inner join crm.Franchise F on V.FranchiseId=F.FranchiseID
				  where VendorType=3 and VendorStatus=1)
                  select * from cte order by VendorType,Name").ToList();
                return result;
            }

           
        }

        #region Comments


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param> for case management this is case id
        /// <param name="module"></param>  for casemanagement home office this is 4.
        /// <returns></returns>
        public List<Comments> loadInitialComments(int id, int module)
        { 
        
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var res = new List<Comments>();
                    if (module == 1)
                    {
                        res = connection.Query<Comments>("Select * from CRM.Comments where module in (1,2,3,4) and moduleId = @moduleId and isDeleted=@isDeleted", new { module = module, moduleId = id, isDeleted = false }).ToList();
                        foreach (Comments cm in res)
                        {
                            cm.createdAt = TimeZoneManager.ToLocal(cm.createdAt);
                            cm.updatedAt = TimeZoneManager.ToLocal(cm.updatedAt);
                        }

                    }
                    if (module == 2) //module 4 is for homeoffice view
                    {
                        res = connection.Query<Comments>("Select * from CRM.Comments where module in (1,2,3,4) and moduleId = @moduleId and isDeleted=@isDeleted", new { module = module, moduleId = id, isDeleted = false }).ToList();
                        foreach (Comments cm in res)
                        {
                            cm.createdAt = TimeZoneManager.ToLocal(cm.createdAt);
                            cm.updatedAt = TimeZoneManager.ToLocal(cm.updatedAt);
                        }
                    }
                    if (module == 3)
                    {
                        res = connection.Query<Comments>("Select * from CRM.Comments where module in (1,2,3,4) and moduleId = @moduleId and isDeleted=@isDeleted and belongsTo=@belongsTo", new { module = module, moduleId = id, isDeleted = false, belongsTo = 0 }).ToList();
                        foreach (Comments cm in res)
                        {
                            var queryy = @"select f.* from CRM.VendorCase vc
                                     join CRM.FranchiseCase Fc on Vc.CaseId=Fc.CaseId
										 join CRM.VendorCaseDetail  Vcd on Vc.VendorCaseId=Vcd.VendorCaseId
										 join CRM.FranchiseCaseAddInfo fca on Vcd.CaseLineId=fca.Id
										 join CRM.QuoteLines ql on fca.QuoteLineId=ql.QuoteLineId
										 join CRM.Quote q on ql.QuoteKey= q.QuoteKey  
                                     join CRM.Orders o on o.QuoteKey=q.QuoteKey
                                     join CRM.Opportunities op on op.OpportunityId=o.OpportunityId
                                     join CRM.Franchise f on f.FranchiseId=op.FranchiseId
	                                 where fc.CaseId  = @CaseId ";
                            var resss = connection.Query(queryy, new { CaseId = id }).FirstOrDefault();

                            cm.createdAt = TimeZoneManager.ToLocal(cm.createdAt, (TimeZoneEnum)resss.TimezoneCode);
                            cm.updatedAt = TimeZoneManager.ToLocal(cm.updatedAt, (TimeZoneEnum)resss.TimezoneCode);

                        }
                    }
                    if (module == 4) //module 4 is for homeoffice view
                    {
                        res = connection.Query<Comments>("Select * from CRM.Comments where module in (1,2,3,4) and moduleId = @moduleId and isDeleted=@isDeleted", new { module = module, moduleId = id, isDeleted = false }).ToList();
                        foreach (Comments cm in res)
                        {
                            //                 var queryy = @"select f.TimezoneCode from CRM.VendorCase vc
                            //                          join CRM.FranchiseCase Fc on Vc.CaseId=Fc.CaseId
                            //join CRM.VendorCaseDetail  Vcd on Vc.VendorCaseId=Vcd.VendorCaseId
                            //join CRM.FranchiseCaseAddInfo fca on Vcd.CaseLineId=fca.Id
                            //join CRM.QuoteLines ql on fca.QuoteLineId=ql.QuoteLineId
                            //join CRM.Quote q on ql.QuoteKey= q.QuoteKey  
                            //                          join CRM.Orders o on o.QuoteKey=q.QuoteKey
                            //                          join CRM.Opportunities op on op.OpportunityId=o.OpportunityId
                            //                          join CRM.Franchise f on f.FranchiseId=op.FranchiseId
                            //                       where fc.CaseId  = @CaseId ";

                            var queryy = @"select Fr.TimezoneCode from CRM.Franchise Fr
									   join CRM.Opportunities op on op.FranchiseId= fr.FranchiseId
										join crm.Orders o on o.OpportunityId= op.OpportunityId
										join crm.FranchiseCase fc on fc.SalesOrderId= o.OrderID
										where fc.CaseId=@CaseId";
                            var result = connection.Query(queryy, new { CaseId = id }).FirstOrDefault();
                            cm.createdAt = TimeZoneManager.ToLocal(cm.createdAt, (TimeZoneEnum)result.TimezoneCode);
                            cm.updatedAt = TimeZoneManager.ToLocal(cm.updatedAt, (TimeZoneEnum)result.TimezoneCode);
                        }
                    }
                    connection.Close();
                    return res;
                }
           
            
        }



        public List<dynamic> LoadUserList(int id,int module)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                if (module == 1)
                {
                    var res = connection.Query(@"select u.UserName name , p.PrimaryEmail content from Auth.Users u 
                              Inner join CRM.Person p on p.PersonId = u.PersonId where u.Domain = 'bbi.corp' 
                              and u.FranchiseId = ( select FranchiseId from Auth.Users
                              where PersonId in (select Owner_PersonId from CRM.FranchiseCase where CaseId = @moduleId))
                              union
							  select u.UserName name , p.PrimaryEmail content from Auth.Users u
							  Inner join CRM.Person p on p.PersonId = u.PersonId
							  Inner join CRM.VendorCaseConfig vcf on vcf.CaseLogin = u.UserName
							  Inner Join acct.HFCVendors  hv on vcf.VendorId= hv.VendorIdPk
							  Inner Join CRM.QuoteLines ql on ql.VendorId = hv.VendorId
							  Inner Join CRM.FranchiseCaseAddInfo fcai on fcai.QuoteLineId= ql.QuoteLineId
							  where  fcai.CaseId=@moduleId", new { moduleId = id }).ToList();
                    connection.Close();
                    return res;
                }
                if (module == 2)
                {
                    var res = connection.Query(@"select u.UserName name , p.PrimaryEmail content from Auth.Users u
                            Inner join CRM.Person p on p.PersonId = u.PersonId where u.Domain = 'bbi.corp'
                            and u.FranchiseId = ( select FranchiseId from Auth.Users
                            where PersonId in (select Owner_PersonId from CRM.FranchiseCase where CaseId = @moduleId))
                            union
							select u.UserName name , p.PrimaryEmail content from Auth.Users u
							Inner join CRM.Person p on p.PersonId = u.PersonId
							Inner join CRM.VendorCaseConfig vcf on vcf.CaseLogin = u.UserName
							Inner Join acct.HFCVendors  hv on vcf.VendorId= hv.VendorIdPk
							Inner Join CRM.QuoteLines ql on ql.VendorId = hv.VendorId
							Inner Join CRM.FranchiseCaseAddInfo fcai on fcai.QuoteLineId= ql.QuoteLineId
							where  fcai.CaseId=@moduleId", new { moduleId = id }).ToList();
                    connection.Close();
                    return res;
                }
                if (module == 3 || module == 4)
                {
                    var res = connection.Query(@"select u.UserName name , p.PrimaryEmail content from Auth.Users u 
                                                Inner join CRM.Person p on p.PersonId = u.PersonId
                                                where u.Domain = 'bbi.corp' and u.FranchiseId = ( select FranchiseId from Auth.Users where PersonId in (select Owner_PersonId from CRM.FranchiseCase where CaseId = @moduleId))", new { moduleId = id }).ToList();
                    connection.Close();
                    return res;
                }
                return null;
            }
        }


        public string FindFranchiseTimeZone(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var TimezoneCode = connection.Query<int>(@"select f.TimezoneCode
                                                    from CRM.FranchiseCase fc 
                                                    join Auth.Users u on u.PersonId = fc.Owner_PersonId
                                                    join CRM.Franchise f on f.FranchiseId = u.FranchiseId
                                                    where fc.CaseId =@CaseId", new { CaseId = id }).FirstOrDefault();
                connection.Close();
                if (TimezoneCode > 0)
                    return ((TimeZoneEnum)TimezoneCode).ToString();
                else
                    return "";
            }
        }

        
        public Comments SaveParentComment(string unique, int module, int moduleId, Comments Comments)
        {
          
                string[] emailids = new string[] { };
                if (unique != null)
                    emailids = unique.Split(',');

                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var query = "";
                    int VendorId = 0;
                    if (module == 1)
                        query = @"insert into CRM.Comments (module, moduleId, userId, parentId, haschildId, userImageUrl, userName, htmlComment, htmlCurrentComment, isReply, isEdit, createdAt, updatedAt,  isDeleted,VendorId,belongsTo) values (@module, @moduleId, @userId, @parentId, @haschildId, @userImageUrl, @userName, @htmlComment, @htmlCurrentComment, @isReply, @isEdit, @createdAt, @updatedAt,  @isDeleted, @VendorId, @belongsTo )";
                    //  VendorId = 0;


                    if (module == 2)
                        query = @"insert into CRM.Comments (module, moduleId, userId, parentId, haschildId, userImageUrl, userName, htmlComment, htmlCurrentComment, isReply, isEdit, createdAt, updatedAt,  isDeleted,VendorId,belongsTo) values (@module, @moduleId, @userId, @parentId, @haschildId, @userImageUrl, @userName, @htmlComment, @htmlCurrentComment, @isReply, @isEdit, @createdAt, @updatedAt,  @isDeleted, @VendorId, @belongsTo )";
                    // VendorId = RefId;

                    if (module == 3 || module == 4)
                        query = @"insert into CRM.Comments (module, moduleId, userId, parentId, haschildId, userImageUrl, userName, htmlComment, htmlCurrentComment, isReply, isEdit, createdAt, updatedAt,  isDeleted,VendorId,belongsTo) values (@module, @moduleId, @userId, @parentId, @haschildId, @userImageUrl, @userName, @htmlComment, @htmlCurrentComment, @isReply, @isEdit, @createdAt, @updatedAt,  @isDeleted, @VendorId, @belongsTo )";
                    // VendorId = RefId;

                    int UserPersonId = 0;
                    string UserName = "";

                    if (SessionManager.CurrentUser.Person != null)
                    {
                        UserPersonId = SessionManager.CurrentUser.Person.PersonId;
                        UserName = SessionManager.CurrentUser.Person.FirstName + " " + SessionManager.CurrentUser.Person.LastName;
                    }
                    else if (SessionManager.SecurityUser.Person != null)
                    {
                        UserPersonId = SessionManager.SecurityUser.Person.PersonId;
                        UserName = UserName = SessionManager.SecurityUser.Person.FirstName + " " + SessionManager.SecurityUser.Person.LastName;
                    }

                    var rrr = connection.Execute(query, new
                    {
                        module = module,
                        moduleId = moduleId,
                        userId = UserPersonId,
                        parentId = Comments.parentId,
                        haschildId = Comments.haschildId,
                        userImageUrl = Comments.userImageUrl,
                        userName = UserName,
                        htmlComment = Comments.htmlComment,
                        htmlCurrentComment = Comments.htmlComment,
                        belongsTo = Comments.belongsTo,
                        isReply = Comments.isReply,
                        isEdit = Comments.isReply,
                        createdAt = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc),
                        updatedAt = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc),
                        isDeleted = false,
                        VendorId = VendorId
                    });

                    // var res = new List<Comments>();




                    if (module == 1)
                    {
                        CaseAddEditManager ca = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                        ca.sendEmailForComments(moduleId, module, Comments.htmlComment, "New", emailids);
                        var res = connection.Query<Comments>("Select * from CRM.Comments where id= (select max(id) from CRM.Comments where module=@module and moduleId = @moduleId  and isDeleted= @isDeleted)", new { module = module, moduleId = moduleId, isDeleted = false }).ToList();
                        res[0].createdAt = TimeZoneManager.ToLocal(res[0].createdAt);
                        res[0].updatedAt = TimeZoneManager.ToLocal(res[0].updatedAt);
                        connection.Close();
                        return res[0];

                    }
                    else if (module == 2)
                    {

                        CaseAddEditManager ca = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                        ca.sendEmailForComments(moduleId, module, Comments.htmlComment, "New", emailids);
                        var res = connection.Query<Comments>("Select * from CRM.Comments where id= (select max(id) from CRM.Comments where module=@module and moduleId = @moduleId  and isDeleted= @isDeleted)", new { module = module, moduleId = moduleId, isDeleted = false }).ToList();
                        res[0].createdAt = TimeZoneManager.ToLocal(res[0].createdAt);
                        res[0].updatedAt = TimeZoneManager.ToLocal(res[0].updatedAt);
                        connection.Close();
                        return res[0];

                    }
                    else if (module == 3)
                    {
                        CaseAddEditManager ca = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                        ca.sendEmailForComments(moduleId, module, Comments.htmlComment, "New", emailids);
                        var res = connection.Query<Comments>("Select * from CRM.Comments where id= (select max(id) from CRM.Comments where module=@module and moduleId = @moduleId  and isDeleted= @isDeleted)", new { module = module, moduleId = moduleId, isDeleted = false }).ToList();
                        // res[0].createdAt = TimeZoneManager.ToLocal(res[0].createdAt);
                        // res[0].updatedAt = TimeZoneManager.ToLocal(res[0].updatedAt);

                        var queryy = @"select f.* from CRM.VendorCase vc
                                     join CRM.FranchiseCase Fc on Vc.CaseId=Fc.CaseId
										 join CRM.VendorCaseDetail  Vcd on Vc.VendorCaseId=Vcd.VendorCaseId
										 join CRM.FranchiseCaseAddInfo fca on Vcd.CaseLineId=fca.Id
										 join CRM.QuoteLines ql on fca.QuoteLineId=ql.QuoteLineId
										 join CRM.Quote q on ql.QuoteKey= q.QuoteKey  
                                     join CRM.Orders o on o.QuoteKey=q.QuoteKey
                                     join CRM.Opportunities op on op.OpportunityId=o.OpportunityId
                                     join CRM.Franchise f on f.FranchiseId=op.FranchiseId
	                                 where fc.CaseId  = @CaseId ";

                        var ress = connection.Query(queryy, new { CaseId = moduleId }).FirstOrDefault();

                        if (res[0] != null) res[0].createdAt = TimeZoneManager.ToLocal(res[0].createdAt, (TimeZoneEnum)ress.TimezoneCode);
                        if (res[0] != null) res[0].updatedAt = TimeZoneManager.ToLocal(res[0].updatedAt, (TimeZoneEnum)ress.TimezoneCode);
                        // if (ress[0] != null) ress[0].DateTimeClosed = TimeZoneManager.ToLocal(ress[0].DateTimeClosed, (TimeZoneEnum)res.TimezoneCode);

                        connection.Close();
                        return res[0];
                    }
                    else
                    {
                        CaseAddEditManager ca = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                        ca.sendEmailForComments(moduleId, module, Comments.htmlComment, "New", emailids);
                        var res = connection.Query<Comments>("Select * from CRM.Comments where id= (select max(id) from CRM.Comments where module=@module and moduleId = @moduleId  and isDeleted= @isDeleted)", new { module = module, moduleId = moduleId, isDeleted = false }).ToList();

                        //             var queryy = @"select f.* from CRM.VendorCase vc
                        //                          join CRM.FranchiseCase Fc on Vc.CaseId=Fc.CaseId
                        //join CRM.VendorCaseDetail  Vcd on Vc.VendorCaseId=Vcd.VendorCaseId
                        //join CRM.FranchiseCaseAddInfo fca on Vcd.CaseLineId=fca.Id
                        //join CRM.QuoteLines ql on fca.QuoteLineId=ql.QuoteLineId
                        //join CRM.Quote q on ql.QuoteKey= q.QuoteKey  
                        //                          join CRM.Orders o on o.QuoteKey=q.QuoteKey
                        //                          join CRM.Opportunities op on op.OpportunityId=o.OpportunityId
                        //                          join CRM.Franchise f on f.FranchiseId=op.FranchiseId
                        //                       where fc.CaseId  = @CaseId ";
                        var queryy = @"select Fr.* from CRM.Franchise Fr
									   join CRM.Opportunities op on op.FranchiseId= fr.FranchiseId
										join crm.Orders o on o.OpportunityId= op.OpportunityId
										join crm.FranchiseCase fc on fc.SalesOrderId= o.OrderID
										where fc.CaseId=@CaseId";

                        var ress = connection.Query(queryy, new { CaseId = moduleId }).FirstOrDefault();

                        if (res[0] != null) res[0].createdAt = TimeZoneManager.ToLocal(res[0].createdAt, (TimeZoneEnum)ress.TimezoneCode);
                        if (res[0] != null) res[0].updatedAt = TimeZoneManager.ToLocal(res[0].updatedAt, (TimeZoneEnum)ress.TimezoneCode);
                        // if (ress[0] != null) ress[0].DateTimeClosed = TimeZoneManager.ToLocal(ress[0].DateTimeClosed, (TimeZoneEnum)res.TimezoneCode);

                        connection.Close();
                        return res[0];
                    }

                }          
        }


        
        public Comments SaveReplyComment(string unique, int module, int moduleId, Comments Comments)
        {
            
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    int VendorId = 0;
                    var query = "";

                    var parentobj = connection.Query<Comments>("Select * from CRM.Comments where id= @id", new { id = Comments.parentId }).FirstOrDefault();
                    if (module == 1)
                    {
                        query = @"insert into CRM.Comments (module, moduleId, userId, parentId, haschildId, userImageUrl, userName, htmlComment, htmlCurrentComment, isReply, isEdit, createdAt, updatedAt,  isDeleted, VendorId, belongsTo) values (@module, @moduleId, @userId, @parentId, @haschildId, @userImageUrl, @userName, @htmlComment, @htmlCurrentComment, @isReply, @isEdit, @createdAt, @updatedAt,  @isDeleted, @VendorId, @belongsTo )";
                        // VendorId = 0;
                    }

                    if (module == 2)
                    {
                        query = @"insert into CRM.Comments (module, moduleId, userId, parentId, haschildId, userImageUrl, userName, htmlComment, htmlCurrentComment, isReply, isEdit, createdAt, updatedAt,  isDeleted, VendorId, belongsTo) values (@module, @moduleId, @userId, @parentId, @haschildId, @userImageUrl, @userName, @htmlComment, @htmlCurrentComment, @isReply, @isEdit, @createdAt, @updatedAt,  @isDeleted, @VendorId, @belongsTo )";
                        //  VendorId = RefId;
                    }

                    if (module == 3 || module == 4)
                    {
                        query = @"insert into CRM.Comments (module, moduleId, userId, parentId, haschildId, userImageUrl, userName, htmlComment, htmlCurrentComment, isReply, isEdit, createdAt, updatedAt,  isDeleted, VendorId, belongsTo) values (@module, @moduleId, @userId, @parentId, @haschildId, @userImageUrl, @userName, @htmlComment, @htmlCurrentComment, @isReply, @isEdit, @createdAt, @updatedAt,  @isDeleted, @VendorId, @belongsTo )";
                        //  VendorId = RefId;
                    }

                    int UserPersonId = 0;
                    string UserName = "";

                    if (SessionManager.CurrentUser.Person != null)
                    {
                        UserPersonId = SessionManager.CurrentUser.Person.PersonId;
                        UserName = SessionManager.CurrentUser.Person.FirstName + " " + SessionManager.CurrentUser.Person.LastName;
                    }
                    else if (SessionManager.SecurityUser.Person != null)
                    {
                        UserPersonId = SessionManager.SecurityUser.Person.PersonId;
                        UserName = UserName = SessionManager.SecurityUser.Person.FirstName + " " + SessionManager.SecurityUser.Person.LastName;
                    }

                    connection.Execute(query, new
                    {
                        module = module,
                        moduleId = moduleId,
                        userId = UserPersonId,
                        parentId = Comments.parentId,
                        haschildId = Comments.haschildId,
                        userImageUrl = Comments.userImageUrl,
                        userName = UserName,
                        htmlComment = Comments.htmlComment,
                        htmlCurrentComment = Comments.htmlComment,
                        belongsTo = parentobj.belongsTo,
                        isReply = Comments.isReply,
                        isEdit = Comments.isReply,
                        createdAt = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc),
                        updatedAt = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc),
                        isDeleted = false,
                        VendorId = VendorId
                    });

                    // connection.Execute("Update CRM.Comments set haschildId=@haschildId,updatedAt=@updatedAt where id=@id", new { haschildId=true,id= Comments.parentId, updatedAt = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc) });
                    connection.Execute("Update CRM.Comments set haschildId=@haschildId where id=@id", new { haschildId = true, id = Comments.parentId });

                    // var res = connection.Query<Comments>("Select * from CRM.Comments where where id=@id", new { id=id}).ToList();
                    var res = new List<Comments>();
                    if (module == 1)
                        res = connection.Query<Comments>("Select * from CRM.Comments where id= (select max(id) from CRM.Comments where module=@module and moduleId = @moduleId  and isDeleted= @isDeleted and userId=@userId)", new { module = module, moduleId = moduleId, isDeleted = false, userId = UserPersonId }).ToList();

                    if (module == 2)
                        res = connection.Query<Comments>("Select * from CRM.Comments where id= (select max(id) from CRM.Comments where module=@module and moduleId = @moduleId  and isDeleted= @isDeleted and userId=@userId)", new { module = module, moduleId = moduleId, isDeleted = false, userId = UserPersonId }).ToList();

                    if (module == 3 || module == 4)
                        res = connection.Query<Comments>("Select * from CRM.Comments where id= (select max(id) from CRM.Comments where module=@module and moduleId = @moduleId  and isDeleted= @isDeleted and userId=@userId)", new { module = module, moduleId = moduleId, isDeleted = false, userId = UserPersonId }).ToList();


                    var parentcomment = connection.Query<Comments>("Select * from CRM.Comments where id= @id", new { id = Comments.parentId }).ToList();
                    var ress = connection.Query<Person>("Select * from CRM.Person where PersonId= @id", new { id = parentcomment[0].userId }).ToList();
                    connection.Close();

                    // var result = Comments;
                    // string val = Comments.module;
                    string[] emailids = new string[] { };
                    if (unique != null)
                        emailids = unique.Split(',');

                    string[] emailidss = new string[] { };

                    if (module == 1)
                    {

                        if (ress.Count > 0)
                        {
                            List<string> item = new List<string>();
                            item = emailids.ToList();
                            item.Add(ress[0].PrimaryEmail);

                            emailidss = item.ToArray();

                        }
                        CaseAddEditManager ca = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                        ca.sendEmailForComments(moduleId, module, Comments.htmlComment, "Reply", emailidss);

                        res[0].createdAt = TimeZoneManager.ToLocal(res[0].createdAt);
                        res[0].updatedAt = TimeZoneManager.ToLocal(res[0].updatedAt);

                    }
                    else if (module == 2)
                    {
                        if (ress.Count > 0)
                        {
                            List<string> item = new List<string>();
                            item = emailids.ToList();
                            item.Add(ress[0].PrimaryEmail);

                            emailidss = item.ToArray();

                        }
                        CaseAddEditManager ca = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                        ca.sendEmailForComments(moduleId, module, Comments.htmlComment, "Reply", emailidss);

                        res[0].createdAt = TimeZoneManager.ToLocal(res[0].createdAt);
                        res[0].updatedAt = TimeZoneManager.ToLocal(res[0].updatedAt);

                    }
                    else if (module == 3 || module == 4)
                    {
                        if (ress.Count > 0)
                        {
                            List<string> item = new List<string>();
                            item = emailids.ToList();
                            item.Add(ress[0].PrimaryEmail);

                            emailidss = item.ToArray();

                        }
                        CaseAddEditManager ca = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                        ca.sendEmailForComments(moduleId, module, Comments.htmlComment, "Reply", emailidss);
                        var queryy = "";
                        if (module == 3)
                            queryy = @"select f.* from CRM.VendorCase vc
                                     join CRM.FranchiseCase Fc on Vc.CaseId=Fc.CaseId
										 join CRM.VendorCaseDetail  Vcd on Vc.VendorCaseId=Vcd.VendorCaseId
										 join CRM.FranchiseCaseAddInfo fca on Vcd.CaseLineId=fca.Id
										 join CRM.QuoteLines ql on fca.QuoteLineId=ql.QuoteLineId
										 join CRM.Quote q on ql.QuoteKey= q.QuoteKey  
                                     join CRM.Orders o on o.QuoteKey=q.QuoteKey
                                     join CRM.Opportunities op on op.OpportunityId=o.OpportunityId
                                     join CRM.Franchise f on f.FranchiseId=op.FranchiseId
	                                 where fc.CaseId  = @CaseId ";
                        else
                            queryy = @"select Fr.* from CRM.Franchise Fr
                                       join CRM.Opportunities op on op.FranchiseId = fr.FranchiseId
                                        join crm.Orders o on o.OpportunityId = op.OpportunityId
                                        join crm.FranchiseCase fc on fc.SalesOrderId = o.OrderID
                                        where fc.CaseId = @CaseId";
                        connection.Open();
                        var resss = connection.Query(queryy, new { CaseId = moduleId }).FirstOrDefault();
                        connection.Close();

                        if (res[0] != null) res[0].createdAt = TimeZoneManager.ToLocal(res[0].createdAt, (TimeZoneEnum)resss.TimezoneCode);
                        if (res[0] != null) res[0].updatedAt = TimeZoneManager.ToLocal(res[0].updatedAt, (TimeZoneEnum)resss.TimezoneCode);


                        //res[0].createdAt = TimeZoneManager.ToLocal(res[0].createdAt);
                        //res[0].updatedAt = TimeZoneManager.ToLocal(res[0].updatedAt);

                    }
                    else
                    {
                    }

                    return res[0];

                }
            

        }


        public bool DeleteComment(int id, int parentId)
        {

                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    connection.Execute("update CRM.Comments set isDeleted=@isDeleted,updatedAt=@updatedAt,deletedAt=@deletedAt where id=@id", new { isDeleted = true, id = id, updatedAt = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc), deletedAt = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc) });
                    var res = connection.Query<Comments>("select * from CRM.Comments where parentId=@parentId and isDeleted=@isDeleted", new { parentId = parentId, isDeleted = false }).ToList();
                    if (!(res.Count > 0))
                    {
                        connection.Execute("update CRM.Comments set haschildId=@haschildId,updatedAt=@updatedAt where id=@id", new { haschildId = false, id = parentId, updatedAt = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc) });
                    }
                    connection.Close();
                return true;
            }  
        }



        public DateTime UpdateParentComment(int id, string unique, int module, int moduleId, Comments Comments)
        {
            DateTime dt = DateTime.UtcNow;
            
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    connection.Execute("update CRM.Comments set htmlComment= @htmlComment, htmlCurrentComment=@htmlCurrentComment,updatedAt=@updatedAt where id=@id", new { htmlComment = Comments.htmlCurrentComment, htmlCurrentComment = Comments.htmlCurrentComment, id = id, updatedAt = dt });
                    connection.Close();

                    string[] emailids = new string[] { };
                    if (unique != null)
                        emailids = unique.Split(',');



                    if (Comments.module == 1)
                    {
                        CaseAddEditManager ca = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                        ca.sendEmailForComments(moduleId, module, Comments.htmlCurrentComment, "Update", emailids);


                        var rr = TimeZoneManager.ToLocal(dt);
                        return rr;

                    }
                    else if (Comments.module == 2) //|| Comments.module == 4
                    {
                        CaseAddEditManager ca = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                        ca.sendEmailForComments(moduleId, module, Comments.htmlCurrentComment, "Update", emailids);


                        var rr = TimeZoneManager.ToLocal(dt);
                        return rr;
                    }
                    else if (Comments.module == 3)
                    {
                        CaseAddEditManager ca = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                        ca.sendEmailForComments(moduleId, module, Comments.htmlCurrentComment, "Update", emailids);


                        var queryy = @"select f.* from CRM.VendorCase vc
                                     join CRM.FranchiseCase Fc on Vc.CaseId=Fc.CaseId
										 join CRM.VendorCaseDetail  Vcd on Vc.VendorCaseId=Vcd.VendorCaseId
										 join CRM.FranchiseCaseAddInfo fca on Vcd.CaseLineId=fca.Id
										 join CRM.QuoteLines ql on fca.QuoteLineId=ql.QuoteLineId
										 join CRM.Quote q on ql.QuoteKey= q.QuoteKey  
                                     join CRM.Orders o on o.QuoteKey=q.QuoteKey
                                     join CRM.Opportunities op on op.OpportunityId=o.OpportunityId
                                     join CRM.Franchise f on f.FranchiseId=op.FranchiseId
	                                 where fc.CaseId  = @CaseId ";
                        connection.Open();
                        var resss = connection.Query(queryy, new { CaseId = moduleId }).FirstOrDefault();
                        connection.Close();

                        var rr = TimeZoneManager.ToLocal(dt, (TimeZoneEnum)resss.TimezoneCode);
                        return rr;
                    }
                    else // if (Comments.module == 4)
                    {
                        CaseAddEditManager ca = new CaseAddEditManager(SessionManager.CurrentFranchise, SessionManager.CurrentUser);
                        ca.sendEmailForComments(moduleId, module, Comments.htmlCurrentComment, "Update", emailids);


                        var queryy = @"select Fr.* from CRM.Franchise Fr
									   join CRM.Opportunities op on op.FranchiseId= fr.FranchiseId
										join crm.Orders o on o.OpportunityId= op.OpportunityId
										join crm.FranchiseCase fc on fc.SalesOrderId= o.OrderID
										where fc.CaseId = @CaseId ";
                        connection.Open();
                        var resss = connection.Query(queryy, new { CaseId = moduleId }).FirstOrDefault();
                        connection.Close();

                        var rr = TimeZoneManager.ToLocal(dt, (TimeZoneEnum)resss.TimezoneCode);
                        return rr;
                    }

                }
                         
        }



        #endregion

        public dynamic GetVPOData(int? id, int? orderid)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = @"select * from(select pod.PurchaseOrdersDetailId,
                                CONCAT(pod.PICPO, ' - ', q.SideMark) VPO_PICPO_PODId,
                                mpo.PurchaseOrderId, mpo.MasterPONumber, o.OrderId, o.OrderNumber,
                                row_number() over(partition by pod.PICPO order by pod.PurchaseOrdersDetailId asc) rn
                                from CRM.PurchaseOrderDetails pod
                                join CRM.MasterPurchaseOrder mpo on pod.PurchaseOrderId = mpo.PurchaseOrderId
                                join CRM.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                join CRM.Orders o on mpox.OrderId = o.OrderID
                                join CRM.Opportunities op on mpox.OpportunityId = op.OpportunityId
                                join CRM.Accounts a on a.AccountId = op.AccountId
                                left
                                join crm.Quote q on q.QuoteKey = mpox.QuoteId
                                where op.FranchiseId = @FranchiseId
                                    and pod.PICPO != 0  and pod.StatusId not in (8, 9, 10) and";

                if (orderid != null && orderid != 0 && (id == null || id == 0)) query = query + "  mpox.OrderId = isnull(@Orderid, mpox.OrderId)) as p where rn = 1";
                else query = query + "  mpo.PurchaseOrderId = isnull(@MPOID, mpo.PurchaseOrderId)) as p where rn = 1";

                connection.Open();
                var result = connection.Query(query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId, MPOID = id, Orderid = orderid }).ToList();
                connection.Close();
                return result;
            }
        }

        public dynamic GetMPOData(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = @"select distinct mpo.PurchaseOrderId, CONCAT(mpo.MasterPONumber, ' - ', q.SideMark) as MPO_MasterPONum_POId 
                            from CRM.MasterPurchaseOrder mpo 
                                left join CRM.MasterPurchaseOrderextn  mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
	                            join CRM.Opportunities op on mpox.OpportunityId=op.OpportunityId
	                            left join crm.Quote q on q.QuoteKey = mpox.QuoteId
                            where op.FranchiseId=@FranchiseId  and  mpo.POStatusId not in (8, 9, 10)";

                if (id > 0) query = query + " and mpox.OrderId=@OrderId";
                connection.Open();
                var result = connection.Query(query, new
                {
                    FranchiseId = SessionManager.CurrentFranchise.FranchiseId,
                    OrderId = id
                }).ToList();
                connection.Close();
                return result;
            }
        }

        public dynamic GetSalesOrderData(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = "";
                if (id > 0)
                    query = @"select  o.OrderId,CAST(o.OrderNumber as varchar) as SalesOrderId  from CRM.Orders o
                              join CRM.Opportunities op on op.OpportunityId = o.OpportunityId
                              join CRM.MasterPurchaseOrderExtn mpoe on mpoe.OrderId = o.OrderID
                              where op.FranchiseId = @FranchiseId and o.OrderStatus not in (6,9) and mpoe.PurchaseOrderId = @PurchaseOrderId";
                else
                    query = @"with cte as 
                                (
		                                select distinct o.orderid,o.orderNumber,
		                                case when o.OrderStatus not in (9,6) 
			                                then 1 else 0 end as voidcancelled,
		                                case when isnull(pod.StatusId,0) not in (8,9,10) and ql.ProductTypeId not in (4,5)
			                                then 1 else 0 end as linecancelled
			                                ,pod.StatusId
		                                from CRM.Orders o
		                                join crm.Quote q on q.QuoteKey=o.QuoteKey
		                                join crm.QuoteLines ql on q.QuoteKey=ql.QuoteKey
		                                join crm.Opportunities op on op.OpportunityId = q.OpportunityId
		                                left join crm.PurchaseOrderDetails pod on pod.QuoteLineId=ql.QuoteLineId
		                                where op.FranchiseId=@FranchiseId
                                ) select distinct
                                OrderId,CAST(OrderNumber as varchar) as SalesOrderId ,
                                case when voidcancelled!=0 and count( case when linecancelled=1 then orderid end)>0 then 1 else 0 end as createcancel
                                from cte 
                                group by orderid,orderNumber,linecancelled,voidcancelled order by orderid";
                connection.Open();
                var result = connection.Query(query, new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId, PurchaseOrderId = id }).ToList();
                connection.Close();
                if (id > 0)
                    return result;
                else
                    return (from aa in result where aa.createcancel == 1 select aa).ToList();
            }
        }

        public dynamic GetCaseType(int Tableid)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select Id as Type,Name as Typevalue from [CRM].[Type_LookUpValues] where TableId =@tableId";
                var result = connection.Query(query, new { tableId = Tableid }).ToList();
                connection.Close();
                return result;
            }
        }

        public dynamic GetCaseReason(int Tableid)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select Id as ReasonCode,Name as Reasonvalue from [CRM].[Type_LookUpValues] where TableId =@tableId";
                var result = connection.Query(query, new { tableId = Tableid }).ToList();
                connection.Close();
                return result;
            }
        }

        public dynamic GetCaseStatus(int Tableid)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select Id as Status,Name as Statusvalue from [CRM].[Type_LookUpValues] where TableId =@tableId";
                var result = connection.Query(query, new { tableId = Tableid }).ToList();
                connection.Close();
                return result;
            }
        }
    }
}


public class VendorList
{

    public int VendorIdPk { get; set; }
    public int VendorID { get; set; }
    public string Name { get; set; }
    public int VendorType { get; set; }
}
