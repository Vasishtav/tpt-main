﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Core.Membership;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using HFC.CRM.Data.Geolocator;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Web;
using System.IO;
using HFC.CRM.DTO;
using System.Globalization;

namespace HFC.CRM.Managers
{
    public class TextingHistoryManager : DataManagerFE<TextingHistory> // DataManager<Note>
    {
        public TextingHistoryManager(User user, Franchise franchise) : base(user, franchise)
        {

        }

        public override string Add(TextingHistory modal)
        {
            return base.Add(modal);
        }

        public override TextingHistory Get(int id)
        {
            var result = base.Get(id);

            TimeZoneManager.ConvertToLocal(result);
            //TimeZoneManager.ConvertToLocal(result);
            return result;
        }
    }
}


