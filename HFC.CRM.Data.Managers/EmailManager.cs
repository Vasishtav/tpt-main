﻿
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using HFC.CRM.Core;
using HFC.CRM.Data;
using HFC.CRM.Core.Common;
using System.Net;
using HFC.CRM.Core.Logs;
using System.Web;
using System.IO;
using System.Web.Hosting;
using FluentEmail;
using System.Data.SqlClient;
using HFC.CRM.Data.Context;
using Dapper;
using System.Data;
using System.Net.Mime;
using System.Collections;
using HFC.CRM.DTO.Order;
using HFC.CRM.Core.Membership;
using HFC.CRM.DTO;
using HFC.CRM.DTO.Calandar;

namespace HFC.CRM.Managers
{
    /// <summary>
    /// Manager for sending Email with a pre-built template, can also implement permission checking with this manager if we need to in the future
    /// </summary>
    public class EmailManager : DataManager
    {

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="user">Must pass in the current user so we can use their email as the "From" address.  If there is permission checking, this user is what it will check against</param>
        /// <param name="franchise">Required franchise information to send</param>

        public EmailManager(User user, Franchise franchise)
        {
            this.User = user;
            this.Franchise = franchise;

        }

        public bool BuildMailMessageLeadThankYou(int LeadId, string bodyHtml, string subject, int templateEmailId, int brandId, int franchiseId, string FromAddress, List<string> toAddresses, List<string> ccAddresses = null, List<string> bccAddresses = null)
        {
            //Checking validation
            if (toAddresses == null || toAddresses.Count == 0)
                throw new ArgumentNullException("At least one recipient is required");
            if (toAddresses.Any(a => string.IsNullOrEmpty(a) || !RegexUtil.IsValidEmail(a)))
                throw new ArgumentException("Recipient email address is invalid");
            if (ccAddresses != null && ccAddresses.Any(a => !RegexUtil.IsValidEmail(a)))
                throw new ArgumentException("One or more CC recipient email address is invalid");
            if (bccAddresses != null && bccAddresses.Any(a => !RegexUtil.IsValidEmail(a)))
                throw new ArgumentException("One or more BCC recipient email address is invalid");

            //Get Franchise ID
            if (franchiseId == 0)
            {
                franchiseId = Franchise.FranchiseId;
            }

            string FranchiseName = "", FranchiseOwner = "", FranchisePhone = "";
            FranchiseOwner = GetFranchiseOwner(franchiseId);
            TerritoryDisplay data = GetTerritoryDisplayByLeadId(LeadId);

            if (data != null && data.UseFranchiseName == false)
            {
                if (data.Displayname != "")
                    FranchiseName = data.Displayname + (data.LicenseNumber != null && data.LicenseNumber != "" ? " - " + data.LicenseNumber : "");
                else
                    FranchiseName = GetFranchiseName(franchiseId);
                FranchisePhone = data.Phone;
                if (data.PrimaryEmail != null && data.PrimaryEmail != "")
                    FromAddress = data.PrimaryEmail;
            }
            else
            {
                //Territory te = new Territory();

                //te = GetTerritoryDisplayForGray(LeadId, 3);

                //if (te != null)
                //    FranchiseName = te.Name;
                //else
                if (data != null)
                    FranchiseName = GetFranchiseName(franchiseId) + (data.LicenseNumber != null && data.LicenseNumber != "" ? " - " + data.LicenseNumber : "");
                else
                    FranchiseName = GetFranchiseName(franchiseId);

                FranchisePhone = !string.IsNullOrEmpty(Franchise.LocalPhoneNumber) ? Franchise.LocalPhoneNumber : Franchise.TollFreeNumber;
            }

            //For to refer Brand wise Image folder
            string[] BrandNameAbbr = { "BB", "TL", "CC" };
            //To get base server url to send with image 
            HttpContext context = HttpContext.Current;
            string baseUrl = context.Request.Url.Authority + context.Request.ApplicationPath.TrimEnd('/');
            string ServerImgPath = "https://" + baseUrl + "/Images/brand/" + BrandNameAbbr[brandId - 1] + "/";

            //Format the phone number
            if (FranchisePhone != null && FranchisePhone.Length > 0)
            {
                string phoneFormat = "(###) ###-####";
                FranchisePhone = Convert.ToInt64(FranchisePhone).ToString(phoneFormat);
            }
            else
            {
                FranchisePhone = "";
            }

            //Replace the text in email content - Image path, brand, owner and phone
            bodyHtml = bodyHtml.Replace("{IMAGEPATH}", ServerImgPath) //For replace image path
                                                                      //.Replace("{BRAND}", BrandName[brandId - 1]) //For replace Brand name
                .Replace("{FranchiseName}", FranchiseName)
                .Replace("{OWNER}", FranchiseOwner) //For replace Brand name
                .Replace("{PHONE}", FranchisePhone) //For replace Phone
                .Replace("{EMAIL}", FromAddress) //For replace Email address
                ;

            ExchangeManager mgr = new ExchangeManager();
            var mailres = mgr.SendEmail(FromAddress, subject, bodyHtml, toAddresses, ccAddresses, bccAddresses);

            AddSentEmail(string.Join(",", toAddresses), subject, bodyHtml, mailres, "", ccAddresses, bccAddresses, FromAddress, LeadId, null, null, null, (short)templateEmailId, null);

            return true;

        }

        public bool BuildMailMessageCaseFeedbackStatus(dynamic OldFeedback, dynamic NewFeedback, string bodyHtml, string subject, int templateEmailId, int brandId, int franchiseId, string FromAddress, List<string> toAddresses, List<string> ccAddresses = null, List<string> bccAddresses = null)
        {
            if (franchiseId == 0 && Franchise != null)
            {
                franchiseId = Franchise.FranchiseId;
            }
            var toEmailAddresses = new Dictionary<string, string>();//email address, app path by user role

            var CaseURL = HttpContext.Current.Request.UrlReferrer.ToString();
            if (HttpContext.Current.Request.UrlReferrer.AbsolutePath.ToString().Length > 0 && HttpContext.Current.Request.UrlReferrer.AbsolutePath.ToString() != "/")
                CaseURL = CaseURL.Replace(HttpContext.Current.Request.UrlReferrer.AbsolutePath.ToString(), "/{AppPath}");
            else
                CaseURL = CaseURL + "{AppPath}";
            //CaseURL = CaseURL + "" + "#!/FeedbackView/" + model.CaseId;
            CaseURL = CaseURL + "#!/FeedbackView/" + NewFeedback.CaseId;
            var CaseViewLabel = "You can view the feedback by clicking here: <a href='{CaseURL}' style='font - size:16px; ' >{CaseNumber}</a> or by following the link below: <br/><a href='{CaseURL}'>{CaseURL}</a>";
            CaseViewLabel = CaseViewLabel.Replace("{CaseURL}", CaseURL);
            CaseViewLabel = CaseViewLabel.Replace("{CaseNumber}", NewFeedback.ReportID);
            CaseManager case_Mngr = new CaseManager();
            //Replace the text in email content - 
            string CreatedOrChanged = ""; string CurrentStatus = ""; string PreviousStatus = ""; var ClosedReasonLBL = "";
            if (NewFeedback.CurrentStatusId == (int)CaseFeedbackStatusEnum.Submitted)
            {
                CreatedOrChanged = "created";
                CurrentStatus = case_Mngr.getCaseStatus(NewFeedback.CurrentStatusId);// Notify Vendor, TPT support

                string VendorEmail = case_Mngr.GetEmailAddressByCaseIdUserRole(NewFeedback.CaseId, "Vendor");
                string SupportEmail = case_Mngr.GetEmailAddressByCaseIdUserRole(NewFeedback.CaseId, "TPTSupport");

                if (VendorEmail != null)
                    toEmailAddresses.Add(VendorEmail, "VendorCP");

                if (SupportEmail != null)
                    toEmailAddresses.Add(SupportEmail, "");
            }
            else if (NewFeedback.CurrentStatusId == (int)CaseFeedbackStatusEnum.VendorReview)
            {
                CreatedOrChanged = "changed";
                PreviousStatus = case_Mngr.getCaseStatus(OldFeedback.CurrentStatusId);
                CurrentStatus = case_Mngr.getCaseStatus(NewFeedback.CurrentStatusId);  //Notify Originator, TPT support

                string FranchiseEmail = case_Mngr.GetEmailAddressByCaseIdUserRole(NewFeedback.CaseId, "Franchise");
                string SupportEmail = case_Mngr.GetEmailAddressByCaseIdUserRole(NewFeedback.CaseId, "TPTSupport");

                if (FranchiseEmail != null)
                    toEmailAddresses.Add(FranchiseEmail, "");
                if (SupportEmail != null)
                    toEmailAddresses.Add(SupportEmail, "");
            }
            else
            {
                CreatedOrChanged = "changed";
                PreviousStatus = case_Mngr.getCaseStatus(OldFeedback.CurrentStatusId);
                CurrentStatus = case_Mngr.getCaseStatus(NewFeedback.CurrentStatusId);//Notify Originator, TPT support
                ClosedReasonLBL = "Closed Reason";//label visibility only when the status is closed.  
                string FranchiseEmail = case_Mngr.GetEmailAddressByCaseIdUserRole(NewFeedback.CaseId, "Franchise");
                string SupportEmail = case_Mngr.GetEmailAddressByCaseIdUserRole(NewFeedback.CaseId, "TPTSupport");

                if (FranchiseEmail != null)
                    toEmailAddresses.Add(FranchiseEmail, "");
                if (SupportEmail != null)
                    toEmailAddresses.Add(SupportEmail, "");
            }

            //TimeZoneManager mgr = new TimeZoneManager();
            var CreatedOn = "";
            string CreatedBy = "";
            if (CreatedOrChanged == "created" && NewFeedback.CurrentStatusId != (int)CaseFeedbackStatusEnum.VendorReview)
            {
                CreatedBy = NewFeedback.CreatedByName;
                CreatedOn = TimeZoneManager.ToLocal(NewFeedback.CreatedOn).ToString();
            }
            else
            {
                CreatedBy = NewFeedback.UpdatedBy;
                CreatedOn = TimeZoneManager.ToLocal(NewFeedback.LastUpdatedOn).ToString();
            }

            string ProblemType = "" + NewFeedback.ProblemType;
            string Vendor = "" + NewFeedback.Vendor;
            string ClosedReason = "" + NewFeedback.CloseReason;
            string ReportDate = TimeZoneManager.ToLocal(NewFeedback.CreatedOn).ToString();
            string ProductCategory = "" + NewFeedback.ProductCategory;
            subject = "Report ID " + NewFeedback.ReportID + " was " + CreatedOrChanged;
            bodyHtml = bodyHtml.Replace("{CaseNumber}", NewFeedback.ReportID) //For replace Case Report ID
                .Replace("{CreatedChanged}", CreatedOrChanged) // For replace created or changed
                .Replace("{CreatedBy}", CreatedBy) //For replace CreatedBy
                .Replace("{CreatedOn}", CreatedOn) //For replace CreatedOn
                .Replace("{PreviousStatus}", PreviousStatus) //For replace CreatedOn
                .Replace("{CurrentStatus}", CurrentStatus) //For replace CurrentStatus
                .Replace("{ProblemType}", ProblemType) //For replace ProblemType
                .Replace("{Vendor}", Vendor) //For replace Vendor
                .Replace("{ClosedReasonLBL}", ClosedReasonLBL) //For replace Closed reason label
                .Replace("{ClosedReason}", ClosedReason) //For replace closed reason
                .Replace("{ReportDate}", ReportDate) //For replace Report date
                .Replace("{ProductCategory}", ProductCategory)//For replace ProductCategory
                .Replace("{CaseViewLabel}", CaseViewLabel);

            var HTMLTemplateBody = "";
            foreach (var toAddress in toEmailAddresses)
            {
                HTMLTemplateBody = bodyHtml;
                HTMLTemplateBody = HTMLTemplateBody.Replace("{AppPath}", toAddress.Value); // URL to view case
                toAddresses.Clear();
                toAddresses.Add(toAddress.Key);
                ExchangeManager mgr = new ExchangeManager();
                var mailres = mgr.SendEmail(FromAddress, subject, HTMLTemplateBody, toAddresses, ccAddresses, bccAddresses);
                AddSentEmail(string.Join(",", toAddresses), subject, HTMLTemplateBody, mailres, "", ccAddresses, bccAddresses, FromAddress, null, null, null, null, (short)templateEmailId, null);
            }
            return true;

        }

        public bool BuildMailMessageCaseChangeRequestStatus(dynamic OldChangeDetails, dynamic UpdatedChangeDetails, string bodyHtml, string subject, int templateEmailId, int brandId, int franchiseId, string FromAddress, List<string> toAddresses, List<string> ccAddresses = null, List<string> bccAddresses = null)
        {
            //Get Franchise ID
            if (franchiseId == 0 && Franchise != null)
            {
                franchiseId = Franchise.FranchiseId;
            }

            CaseManager case_Mngr = new CaseManager();
            //Replace the text in email content -             
            var toEmailAddresses = new Dictionary<string, string>();//add email address, app path by user role
            string CreatedOrChanged = ""; string CurrentStatus = ""; string PreviousStatus = ""; var RequestedEffectiveDate = "";
            if (UpdatedChangeDetails.StatusId == (int)CaseChangeStatusEnum.Submitted || UpdatedChangeDetails.StatusId == 0)
            {
                CreatedOrChanged = "created";
                var PMEmail = case_Mngr.GetEmailAddressByCaseIdUserRole(0, "HFC Product Strategy Mgt (PSM)");
                if (PMEmail != null)
                    toEmailAddresses.Add(PMEmail, "ControlPanel");
            }
            else if (UpdatedChangeDetails.StatusId == (int)CaseChangeStatusEnum.Approved)
            {
                CreatedOrChanged = "changed";
                string VendorEmail = case_Mngr.GetEmailAddressByCaseIdUserRole(UpdatedChangeDetails.CaseId, "Vendor");
                if (VendorEmail != null)
                    toEmailAddresses.Add(VendorEmail, "VendorCP");

                string PICEmail = case_Mngr.GetEmailAddressByCaseIdUserRole(UpdatedChangeDetails.CaseId, "Supply Chain Partner (PIC)");
                if (PICEmail != null)
                    toEmailAddresses.Add(PICEmail, "PICCP");

                string SupportEmail = case_Mngr.GetEmailAddressByCaseIdUserRole(0, "TPTSupport");
                if (SupportEmail != null)
                    toEmailAddresses.Add(SupportEmail, "");
            }
            else if (UpdatedChangeDetails.StatusId == (int)CaseChangeStatusEnum.Declined)
            {
                CreatedOrChanged = "changed";
                string VendorEmail = case_Mngr.GetEmailAddressByCaseIdUserRole(UpdatedChangeDetails.CaseId, "Vendor");
                if (VendorEmail != null)
                    toEmailAddresses.Add(VendorEmail, "VendorCP");
            }
            else
            {
                CreatedOrChanged = "changed";
                string VendorEmail = case_Mngr.GetEmailAddressByCaseIdUserRole(UpdatedChangeDetails.CaseId, "Vendor");
                if (VendorEmail != null)
                    toEmailAddresses.Add(VendorEmail, "VendorCP");

                var PMEmail = case_Mngr.GetEmailAddressByCaseIdUserRole(0, "HFC Product Strategy Mgt (PSM)");
                if (PMEmail != null)
                    toEmailAddresses.Add(PMEmail, "ControlPanel");

                string SupportEmail = case_Mngr.GetEmailAddressByCaseIdUserRole(0, "TPTSupport");
                if (SupportEmail != null)
                    toEmailAddresses.Add(SupportEmail, "");
            }

            string CreatedBy = "";
            string CreatedOn = "";
            if (CreatedOrChanged == "created")
            {
                CurrentStatus = case_Mngr.getCaseStatus(UpdatedChangeDetails.StatusId);
                CreatedBy = UpdatedChangeDetails.CreatedByName;
                CreatedOn = TimeZoneManager.ToLocal(UpdatedChangeDetails.CreatedOn).ToString();
            }
            else
            {
                PreviousStatus = case_Mngr.getCaseStatus(OldChangeDetails.StatusId);
                CurrentStatus = case_Mngr.getCaseStatus(UpdatedChangeDetails.StatusId);
                CreatedBy = UpdatedChangeDetails.LastUpdateByName;
                CreatedOn = TimeZoneManager.ToLocal(UpdatedChangeDetails.LastUpdatedOn).ToString();
            }
            RequestedEffectiveDate = TimeZoneManager.ToLocal(UpdatedChangeDetails.EffectiveDate).ToString();

            string ProblemType = "" + UpdatedChangeDetails.RequestType;
            string Vendor = "" + UpdatedChangeDetails.Vendor;

            var CaseURL = HttpContext.Current.Request.UrlReferrer.ToString();
            CaseURL = CaseURL.Replace(HttpContext.Current.Request.UrlReferrer.AbsolutePath.ToString(), "/{AppPath}");
            CaseURL = CaseURL + "" + "#!/ChangeRequestView/" + UpdatedChangeDetails.CaseId;
            //CaseURL = CaseURL + "#!/ChangeRequestView/" + result.CaseId;
            var CaseViewLabel = "You can view the change request by clicking here: <a href='{CaseURL}' style='font - size:16px; ' >{CaseNumber}</a> or by following the link below: <br/><a href='{CaseURL}'>{CaseURL}</a>";
            CaseViewLabel = CaseViewLabel.Replace("{CaseURL}", CaseURL);
            CaseViewLabel = CaseViewLabel.Replace("{CaseNumber}", UpdatedChangeDetails.ReportID);

            string RequestDate = TimeZoneManager.ToLocal(UpdatedChangeDetails.CreatedOn).ToString(); ;
            string ProductCategory = "" + UpdatedChangeDetails.ProductCategory;
            subject = "Report ID " + UpdatedChangeDetails.ReportID + " was " + CreatedOrChanged;
            bodyHtml = bodyHtml.Replace("{CaseNumber}", UpdatedChangeDetails.ReportID) //For replace of Case Report ID
                .Replace("{CreatedChanged}", CreatedOrChanged) // For replace created or changed
                .Replace("{CreatedBy}", CreatedBy) //For replace CreatedBy
                .Replace("{CreatedOn}", CreatedOn) //For replace CreatedOn
                .Replace("{PreviousStatus}", PreviousStatus) //For replace CreatedOn
                .Replace("{CurrentStatus}", CurrentStatus) //For replace CurrentStatus
                .Replace("{ProblemType}", ProblemType) //For replace ProblemType
                .Replace("{Vendor}", Vendor) //For replace Vendor
                .Replace("{RequestedEffectiveDate}", RequestedEffectiveDate) //For replace Closed reason label               
                .Replace("{RequestDate}", RequestDate) //For replace RequestedEffectiveDate
                .Replace("{ProductCategory}", ProductCategory) //For replace ProductCategory
                .Replace("{CaseViewLabel}", CaseViewLabel); // Case View URL
            var HTMLTemplateBody = "";
            foreach (var toAddress in toEmailAddresses)
            {
                HTMLTemplateBody = bodyHtml;
                HTMLTemplateBody = HTMLTemplateBody.Replace("{AppPath}", toAddress.Value); // Case View URL
                toAddresses.Clear();
                toAddresses.Add(toAddress.Key);

                ExchangeManager mgr = new ExchangeManager();
                var mailres = mgr.SendEmail(FromAddress, subject, HTMLTemplateBody, toAddresses, ccAddresses, bccAddresses);
                AddSentEmail(string.Join(",", toAddresses), subject, HTMLTemplateBody, mailres, "", ccAddresses, bccAddresses, FromAddress, null, null, null, null, (short)templateEmailId, null);
            }
            return true;

        }

        public bool BuildMailMessageAppointment(CalendarVM model, string SalesPerson, string bodyHtml, string subject, short templateEmailId, string FromAddress, List<string> toAddresses, List<string> ccAddresses = null, List<string> bccAddresses = null)
        {

            int LeadId = model.AccountId != null && model.AccountId > 0 ? 0 : (int)model.LeadId;
            int AccountId = model.AccountId != null && model.AccountId > 0 ? (int)model.AccountId : 0;
            int OpportunityId = model.OpportunityId != null && model.OpportunityId > 0 ? (int)model.OpportunityId : 0;
            int OrderId = model.OrderId != null && model.OrderId > 0 ? (int)model.OrderId : 0;
            int CalendarId = model.CalendarId;
            bool IsAllday = model.IsAllDay;
            DateTime AppointmentDate = model.StartDate;
            string ClientAddress = model.Location;
            int brandId = Franchise.BrandId;
            int franchiseId = Franchise.FranchiseId;

            //Checking validation
            if (toAddresses == null || toAddresses.Count == 0)
                throw new ArgumentNullException("At least one recipient is required");
            if (toAddresses.Any(a => string.IsNullOrEmpty(a) || !RegexUtil.IsValidEmail(a)))
                throw new ArgumentException("Recipient email address is invalid");
            if (ccAddresses != null && ccAddresses.Any(a => !RegexUtil.IsValidEmail(a)))
                throw new ArgumentException("One or more CC recipient email address is invalid");
            if (bccAddresses != null && bccAddresses.Any(a => !RegexUtil.IsValidEmail(a)))
                throw new ArgumentException("One or more BCC recipient email address is invalid");

            //Location decalaration
            string AppintmentDateTime = "";
            string Client = "", clientEmail = "", clientPhone = "";

            string FranchiseName = "", FranchiseOwner = "", FranchisePhone = "";
            FranchiseOwner = GetFranchiseOwner(franchiseId);

            TerritoryDisplay data = new TerritoryDisplay();

            if (OpportunityId > 0)
                data = GetTerritoryDisplayByOpp(OpportunityId);
            else if (AccountId > 0)
                data = GetTerritoryDisplayByAccountId(AccountId);
            else if (LeadId > 0)
                data = GetTerritoryDisplayByLeadId(LeadId);

            if (data != null && data.UseFranchiseName == false)
            {
                if (data.Displayname != "")
                    FranchiseName = data.Displayname + (data.LicenseNumber != null && data.LicenseNumber != "" ? " - " + data.LicenseNumber : "");
                else
                    FranchiseName = GetFranchiseName(franchiseId);
                FranchisePhone = data.Phone;
                if (data.PrimaryEmail != null && data.PrimaryEmail != "")
                    FromAddress = data.PrimaryEmail;
            }
            else
            {
                //Territory te = new Territory();
                //if (OpportunityId > 0)
                //    te = GetTerritoryDisplayForGray(OpportunityId, 1);
                //else if (AccountId > 0)
                //    te = GetTerritoryDisplayForGray(AccountId, 2);
                //else if (LeadId > 0)
                //    te = GetTerritoryDisplayForGray(LeadId, 3);

                //if (te != null)
                //    FranchiseName = te.Name;
                //else
                if (data != null)
                    FranchiseName = GetFranchiseName(franchiseId) + (data.LicenseNumber != null && data.LicenseNumber != "" ? " - " + data.LicenseNumber : "");
                else
                    FranchiseName = GetFranchiseName(franchiseId);

                FranchisePhone = !string.IsNullOrEmpty(Franchise.LocalPhoneNumber) ? Franchise.LocalPhoneNumber : Franchise.TollFreeNumber;
            }

            string phoneFormat = "(###) ###-####";
            //For to refer Brand wise Image folder
            string[] BrandNameAbbr = { "BB", "TL", "CC" };

            //To get base server url to send with image 
            HttpContext context = HttpContext.Current;
            string baseUrl = context.Request.Url.Authority + context.Request.ApplicationPath.TrimEnd('/');
            string ServerImgPath = "https://" + baseUrl + "/Images/brand/" + BrandNameAbbr[brandId - 1] + "/";

            string ApptMonth = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(AppointmentDate.Month);
            string ApptDay = Convert.ToString(AppointmentDate.Day);
            string ApptYear = Convert.ToString(AppointmentDate.Year);
            string ApptTime = AppointmentDate.ToString("h:mm tt").ToLower().Replace(" ", string.Empty);
            string ApptDayName = Convert.ToString(AppointmentDate.DayOfWeek);
            if (IsAllday == false)
                AppintmentDateTime = ApptMonth + " " + ApptDay + ", " + ApptYear + ", " + ApptTime;
            else
                AppintmentDateTime = ApptMonth + " " + ApptDay + ", " + ApptYear;

            //string SalesPerson = GetSalesPersonName(OpportunityId);
            string[] clientDetails = GetClientDetails(LeadId, AccountId).Split(';');
            Client = clientDetails[0];
            clientEmail = clientDetails[1];
            clientPhone = clientDetails[2];
            clientPhone = clientPhone != "" && clientPhone.Length > 0 ? Convert.ToInt64(clientPhone).ToString(phoneFormat) : "";

            //Format the phone number
            FranchisePhone = FranchisePhone != null && FranchisePhone.Length > 0 ? Convert.ToInt64(FranchisePhone).ToString(phoneFormat) : "";

            //Replace the text in email content - Image path, brand, owner and phone
            bodyHtml = bodyHtml.Replace("{IMAGEPATH}", ServerImgPath) //For replace image path
                .Replace("{FranchiseName}", FranchiseName)
                .Replace("{OWNER}", FranchiseOwner) //For replace Owner name
                .Replace("{PHONE}", FranchisePhone) //For replace Phone
                .Replace("{EMAIL}", FromAddress) //For replace Email address                                                 //
                .Replace("{ADDRESS}", ClientAddress) //Client Address
                .Replace("{CLIENT}", Client) //Client
                .Replace("{CLIENTEMAIL}", clientEmail) //Client Email
                .Replace("{CLIENTPHONE}", clientPhone) //client Phone 
                .Replace("{APPOINTMENTDATE}", AppintmentDateTime) //Appointment Date Time
                .Replace("{SalesPerson}", SalesPerson)
                ;

            #region ics

            //Reference: https://generacodice.com/en/articolo/24976/Creating-iCal-Files-in-c

            //create a new stringbuilder instance
            StringBuilder sb = new StringBuilder();

            //begin the calendar item
            sb.AppendLine("BEGIN:VCALENDAR");
            sb.AppendLine("VERSION:2.0");
            //sb.AppendLine("PRODID:stackoverflow.com");
            sb.AppendLine("CALSCALE:GREGORIAN");
            sb.AppendLine("METHOD:PUBLISH");

            //add the event
            sb.AppendLine("BEGIN:VEVENT");

            if (IsAllday)
            {
                sb.AppendLine("DTSTART:" + model.StartDate.ToString("yyyyMMdd"));
                sb.AppendLine("DTEND:" + model.EndDate.ToString("yyyyMMdd"));
            }
            else
            {
                sb.AppendLine("DTSTART:" + model.StartDate.ToString("yyyyMMddTHHmm00"));
                sb.AppendLine("DTEND:" + model.EndDate.ToString("yyyyMMddTHHmm00"));
            }

            string IcsDescription = "APPOINTMENT CONFIRMATION FOR: \\n ";
            IcsDescription = IcsDescription + Client + " \\n ";
            IcsDescription = IcsDescription + ClientAddress + " \\n ";
            IcsDescription = IcsDescription + clientEmail + " " + clientPhone + " \\n \\n ";
            IcsDescription = IcsDescription + "DATE AND TIME OF APPOINTMENT: \\n ";
            IcsDescription = IcsDescription + AppintmentDateTime + " \\n \\n ";
            IcsDescription = IcsDescription + "WITH \\n ";
            IcsDescription = IcsDescription + SalesPerson + " \\n ";
            IcsDescription = IcsDescription + FranchiseName;

            //contents of the calendar item
            sb.AppendLine("SUMMARY:" + subject + "");
            sb.AppendLine("LOCATION:" + ClientAddress + "");
            sb.AppendLine("DESCRIPTION:" + IcsDescription + "");
            sb.AppendLine("PRIORITY:3");
            sb.AppendLine("END:VEVENT");

            //close calendar item
            sb.AppendLine("END:VCALENDAR");

            //create a string from the stringbuilder
            string CalendarItemAsString = sb.ToString();

            byte[] bytes = null;
            using (var ms = new MemoryStream())
            {
                TextWriter tw = new StreamWriter(ms);
                tw.Write(CalendarItemAsString);
                tw.Flush();
                ms.Position = 0;
                bytes = ms.ToArray();
            }
            #endregion

            ExchangeManager mgr = new ExchangeManager();
            mgr.SendEmailMultipleAttachment(FromAddress, subject, bodyHtml, toAddresses, ccAddresses, bccAddresses, null, bytes, icsAppointmentfilename);

            AddSentEmail(string.Join(",", toAddresses), subject, bodyHtml, true, "", ccAddresses, bccAddresses, FromAddress, LeadId, AccountId, OpportunityId, null, templateEmailId, OrderId, CalendarId);

            return true;

        }

        public bool BuildMailMessageReminder(int LeadId, int AccountId, int OpportunityId, int OrderId, bool IsAllDay, DateTime AppointmentDate, string ClientAddress, string SalesPerson, string bodyHtml, string subject, short templateEmailId, int brandId, Franchise Franchiseinfo, string FromAddress, List<string> toAddresses, List<string> ccAddresses = null, List<string> bccAddresses = null)
        {
            this.Franchise = Franchiseinfo;

            //Checking validation
            if (toAddresses == null || toAddresses.Count == 0)
                throw new ArgumentNullException("At least one recipient is required");
            if (toAddresses.Any(a => string.IsNullOrEmpty(a) || !RegexUtil.IsValidEmail(a)))
                throw new ArgumentException("Recipient email address is invalid");
            if (ccAddresses != null && ccAddresses.Any(a => !RegexUtil.IsValidEmail(a)))
                throw new ArgumentException("One or more CC recipient email address is invalid");
            if (bccAddresses != null && bccAddresses.Any(a => !RegexUtil.IsValidEmail(a)))
                throw new ArgumentException("One or more BCC recipient email address is invalid");

            //Location decalaration
            string AppintmentDateTime = "";
            string Client = "";

            string FranchiseName = "", FranchiseOwner = "", FranchisePhone = "";
            FranchiseOwner = GetFranchiseOwner(Franchiseinfo.FranchiseId);
            TerritoryDisplay data = new TerritoryDisplay();

            if (OpportunityId > 0)
                data = GetTerritoryDisplayByOpp(OpportunityId);
            else if (AccountId > 0)
                data = GetTerritoryDisplayByAccountId(AccountId);
            else if (LeadId > 0)
                data = GetTerritoryDisplayByLeadId(LeadId);

            if (data != null && data.UseFranchiseName == false)
            {
                if (data.Displayname != "")
                    FranchiseName = data.Displayname + (data.LicenseNumber != null && data.LicenseNumber != "" ? " - " + data.LicenseNumber : "");
                else
                    FranchiseName = GetFranchiseName(Franchiseinfo.FranchiseId);
                FranchisePhone = data.Phone;
                if (data.PrimaryEmail != null && data.PrimaryEmail != "")
                    FromAddress = data.PrimaryEmail;
            }
            else
            {

                //Territory te = new Territory();
                //if (OpportunityId > 0)
                //    te = GetTerritoryDisplayForGray(OpportunityId, 1);
                //else if (AccountId > 0)
                //    te = GetTerritoryDisplayForGray(AccountId, 2);
                //else if (LeadId > 0)
                //    te = GetTerritoryDisplayForGray(LeadId, 3);

                //if (te != null)
                //    FranchiseName = te.Name;
                //else
                if (data != null)
                    FranchiseName = GetFranchiseName(Franchiseinfo.FranchiseId) + (data.LicenseNumber != null && data.LicenseNumber != "" ? " - " + data.LicenseNumber : "");
                else
                    FranchiseName = GetFranchiseName(Franchiseinfo.FranchiseId);

                FranchisePhone = !string.IsNullOrEmpty(Franchise.LocalPhoneNumber) ? Franchise.LocalPhoneNumber : Franchise.TollFreeNumber;
            }

            //For to refer Brand wise Image folder
            string[] BrandNameAbbr = { "BB", "TL", "CC" };

            //To get base server url to send with image 
            HttpContext context = HttpContext.Current;
            string baseUrl = context.Request.Url.Authority + context.Request.ApplicationPath.TrimEnd('/');
            string ServerImgPath = "https://" + baseUrl + "/Images/brand/" + BrandNameAbbr[brandId - 1] + "/";

            //If appointment present
            DateTime AppDate = TimeZoneManager.ToLocal(AppointmentDate, Franchiseinfo.TimezoneCode);
            string ApptMonth = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(AppDate.Month);
            string ApptDay = Convert.ToString(AppDate.Day);
            string ApptYear = Convert.ToString(AppDate.Year);
            string ApptTime = AppDate.ToString("h:mm tt").ToLower().Replace(" ", string.Empty);
            string ApptDayName = Convert.ToString(AppDate.DayOfWeek);
            //Format Month Day, Year, timeam/pm
            if (IsAllDay == false)
                AppintmentDateTime = ApptMonth + " " + ApptDay + ", " + ApptYear + ", " + ApptTime;
            else
                AppintmentDateTime = ApptMonth + " " + ApptDay + ", " + ApptYear;

            //string SalesPerson = GetSalesPersonName(OpportunityId);
            string[] clientDetails = GetClientDetails(LeadId, AccountId).Split(';');
            Client = clientDetails[0];

            //Format the phone number
            string phoneFormat = "(###) ###-####";
            FranchisePhone = FranchisePhone != null && FranchisePhone.Length > 0 ? Convert.ToInt64(FranchisePhone).ToString(phoneFormat) : "";
            var GuidlinkPath = "";
            if (this.Franchise.BrandId == 1)
            {
                if (this.Franchise.CountryCode == "CA")
                {
                    GuidlinkPath = "http://docs.budgetblinds.com/odg-CAN";
                }
                else
                {
                    GuidlinkPath = "http://docs.budgetblinds.com/odg";
                }
            }

            //Replace the text in email content - Image path, brand, owner and phone
            bodyHtml = bodyHtml.Replace("{IMAGEPATH}", ServerImgPath) //For replace image path
                .Replace("{FranchiseName}", FranchiseName)
                .Replace("{OWNER}", FranchiseOwner) //For replace Owner name
                .Replace("{PHONE}", FranchisePhone) //For replace Phone
                .Replace("{EMAIL}", FromAddress) //For replace Email address

                .Replace("{hours}", "24 hours") //hours remaining
                .Replace("{ADDRESS}", ClientAddress) //Client Address
                .Replace("{CLIENT}", Client) //Client
                .Replace("{APPOINTMENTDATE}", AppintmentDateTime) //Appointment Date Time
                .Replace("{SalesPerson}", SalesPerson)
                .Replace("{DesignGuidlink}", GuidlinkPath);
            ;

            ExchangeManager mgr = new ExchangeManager();
            var mailres = mgr.SendEmail(FromAddress, subject, bodyHtml, toAddresses, ccAddresses, bccAddresses);

            AddSentEmail(string.Join(",", toAddresses), subject, bodyHtml, mailres, "", ccAddresses, bccAddresses, FromAddress, LeadId, AccountId, OpportunityId, null, templateEmailId, OrderId);


            return true;

        }

        public bool BuildMailMessageOrderSummary(int LeadId, int AccountId, int OpportunityId, int OrderId, string bodyHtml, string subject, short templateEmailId, int brandId, int franchiseId, string FromAddress, List<string> toAddresses, List<string> ccAddresses = null, List<string> bccAddresses = null, string streamid = "")
        {
            //Checking validation
            if (toAddresses == null || toAddresses.Count == 0)
                throw new ArgumentNullException("At least one recipient is required");
            if (toAddresses.Any(a => string.IsNullOrEmpty(a) || !RegexUtil.IsValidEmail(a)))
                throw new ArgumentException("Recipient email address is invalid");
            if (ccAddresses != null && ccAddresses.Any(a => !RegexUtil.IsValidEmail(a)))
                throw new ArgumentException("One or more CC recipient email address is invalid");
            if (bccAddresses != null && bccAddresses.Any(a => !RegexUtil.IsValidEmail(a)))
                throw new ArgumentException("One or more BCC recipient email address is invalid");

            //Get Franchise ID
            {
                franchiseId = Franchise.FranchiseId;
            }

            string FranchiseName = "", FranchiseOwner = "", FranchisePhone = "";
            FranchiseOwner = GetFranchiseOwner(franchiseId);
            TerritoryDisplay data = GetTerritoryDisplayByOpp(OpportunityId);

            if (data != null && data.UseFranchiseName == false)
            {
                if (data.Displayname != "")
                    FranchiseName = data.Displayname + (data.LicenseNumber != null && data.LicenseNumber != "" ? " - " + data.LicenseNumber : "");
                else
                    FranchiseName = GetFranchiseName(franchiseId);
                FranchisePhone = data.Phone;
                if (data.PrimaryEmail != null && data.PrimaryEmail != "")
                    FromAddress = data.PrimaryEmail;
            }
            else
            {

                //Territory te = new Territory();
                //te = GetTerritoryDisplayForGray(OpportunityId, 1);

                //if (te != null)
                //    FranchiseName = te.Name;
                //else
                if (data != null)
                    FranchiseName = GetFranchiseName(franchiseId) + (data.LicenseNumber != null && data.LicenseNumber != "" ? " - " + data.LicenseNumber : "");
                else
                    FranchiseName = GetFranchiseName(franchiseId);
                FranchisePhone = !string.IsNullOrEmpty(Franchise.LocalPhoneNumber) ? Franchise.LocalPhoneNumber : Franchise.TollFreeNumber;
            }

            //For to refer Brand wise Image folder
            string[] BrandNameAbbr = { "BB", "TL", "CC" };
            //To get base server url to send with image 
            HttpContext context = HttpContext.Current;
            string baseUrl = context.Request.Url.Authority + context.Request.ApplicationPath.TrimEnd('/');
            string ServerImgPath = "https://" + baseUrl + "/Images/brand/" + BrandNameAbbr[brandId - 1] + "/";

            //Format the phone number
            string phoneFormat = "(###) ###-####";
            FranchisePhone = FranchisePhone != null && FranchisePhone.Length > 0 ? Convert.ToInt64(FranchisePhone).ToString(phoneFormat) : "";
            string SalesPerson = GetSalesPersonName(OpportunityId);
            //Replace the text in email content - Image path, brand, owner and phone
            bodyHtml = bodyHtml.Replace("{IMAGEPATH}", ServerImgPath) //For replace image path
                .Replace("{FranchiseName}", FranchiseName)
                .Replace("{OWNER}", FranchiseOwner) //For replace Owner name
                .Replace("{PHONE}", FranchisePhone) //For replace Phone
                .Replace("{EMAIL}", FromAddress) //For replace Email address 
                .Replace("{SalesPerson}", SalesPerson)
                ;

            ExchangeManager mgr = new ExchangeManager();
            var mailres = mgr.SendEmail(FromAddress, subject, bodyHtml, toAddresses, ccAddresses, bccAddresses, streamid);

            AddSentEmail(string.Join(",", toAddresses), subject, bodyHtml, mailres, "", ccAddresses, bccAddresses, FromAddress, LeadId, AccountId, OpportunityId, null, templateEmailId, OrderId);
            return true;

        }

        public bool BuildMailMessageInstallSchedule(CalendarVM model, string bodyHtml, string subject, short templateEmailId, string FromAddress, List<string> toAddresses, List<string> ccAddresses = null, List<string> bccAddresses = null)
        {
            int LeadId = model.AccountId != null && model.AccountId > 0 ? 0 : (int)model.LeadId;
            int AccountId = model.AccountId != null && model.AccountId > 0 ? (int)model.AccountId : 0;
            int OpportunityId = model.OpportunityId != null && model.OpportunityId > 0 ? (int)model.OpportunityId : 0;
            int OrderId = model.OrderId != null && model.OrderId > 0 ? (int)model.OrderId : 0;
            int CalendarId = model.CalendarId;
            bool IsAllDay = model.IsAllDay;
            DateTime Intallationdate = model.StartDate;
            string ClientAddress = model.Location;
            int brandId = Franchise.BrandId;
            int franchiseId = Franchise.FranchiseId;

            //Checking validation
            if (toAddresses == null || toAddresses.Count == 0)
                throw new ArgumentNullException("At least one recipient is required");
            if (toAddresses.Any(a => string.IsNullOrEmpty(a) || !RegexUtil.IsValidEmail(a)))
                throw new ArgumentException("Recipient email address is invalid");
            if (ccAddresses != null && ccAddresses.Any(a => !RegexUtil.IsValidEmail(a)))
                throw new ArgumentException("One or more CC recipient email address is invalid");
            if (bccAddresses != null && bccAddresses.Any(a => !RegexUtil.IsValidEmail(a)))
                throw new ArgumentException("One or more BCC recipient email address is invalid");

            //Location decalaration
            string InstallationDateTime = "";
            string Client = "", clientEmail = "", clientPhone = "";

            //Get Franchise ID
            if (franchiseId == 0)
            {
                franchiseId = Franchise.FranchiseId;
            }

            string FranchiseName = "", FranchiseOwner = "", FranchisePhone = "";
            FranchiseOwner = GetFranchiseOwner(franchiseId);
            TerritoryDisplay data = new TerritoryDisplay();
            //   GetTerritoryDisplayByOpp(OpportunityId);


            if (OpportunityId > 0)
                data = GetTerritoryDisplayByOpp(OpportunityId);
            else if (AccountId > 0)
                data = GetTerritoryDisplayByAccountId(AccountId);
            else if (LeadId > 0)
                data = GetTerritoryDisplayByLeadId(LeadId);

            if (data != null && data.UseFranchiseName == false)
            {
                if (data.Displayname != "")
                    FranchiseName = data.Displayname + (data.LicenseNumber != null && data.LicenseNumber != "" ? " - " + data.LicenseNumber : "");
                else
                    FranchiseName = GetFranchiseName(franchiseId);
                FranchisePhone = data.Phone;
                if (data.PrimaryEmail != null && data.PrimaryEmail != "")
                    FromAddress = data.PrimaryEmail;
            }
            else
            {
                //Territory te = new Territory();
                //te = GetTerritoryDisplayForGray(OpportunityId, 1);             

                //if (te != null)
                //    FranchiseName = te.Name;
                //else
                if (data != null)
                    FranchiseName = GetFranchiseName(franchiseId) + (data.LicenseNumber != null && data.LicenseNumber != "" ? " - " + data.LicenseNumber : "");
                else
                    FranchiseName = GetFranchiseName(franchiseId);

                FranchisePhone = !string.IsNullOrEmpty(Franchise.LocalPhoneNumber) ? Franchise.LocalPhoneNumber : Franchise.TollFreeNumber;
            }

            //For to refer Brand wise Image folder
            string[] BrandNameAbbr = { "BB", "TL", "CC" };
            //To get base server url to send with image 
            HttpContext context = HttpContext.Current;
            string baseUrl = context.Request.Url.Authority + context.Request.ApplicationPath.TrimEnd('/');
            string ServerImgPath = "https://" + baseUrl + "/Images/brand/" + BrandNameAbbr[brandId - 1] + "/";

            string ApptMonth = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Intallationdate.Month);
            string ApptDay = Convert.ToString(Intallationdate.Day);
            string ApptYear = Convert.ToString(Intallationdate.Year);
            string ApptTime = Intallationdate.ToString("h:mm tt").ToLower().Replace(" ", string.Empty);
            string ApptDayName = Convert.ToString(Intallationdate.DayOfWeek);
            if (IsAllDay == false)
                InstallationDateTime = ApptMonth + " " + ApptDay + ", " + ApptYear + ", " + ApptTime;
            else
                InstallationDateTime = ApptMonth + " " + ApptDay + ", " + ApptYear;

            //Format the phone number
            string phoneFormat = "(###) ###-####";
            FranchisePhone = FranchisePhone != null && FranchisePhone.Length > 0 ? Convert.ToInt64(FranchisePhone).ToString(phoneFormat) : "";

            string[] clientDetails = GetClientDetails(LeadId, AccountId).Split(';');
            Client = clientDetails[0];
            clientEmail = clientDetails[1];
            clientPhone = clientDetails[2];
            clientPhone = clientPhone != "" && clientPhone.Length > 0 ? Convert.ToInt64(clientPhone).ToString(phoneFormat) : "";

            //Replace the text in email content - Image path, brand, owner and phone
            bodyHtml = bodyHtml.Replace("{IMAGEPATH}", ServerImgPath) //For replace image path
                .Replace("{FranchiseName}", FranchiseName)
                .Replace("{OWNER}", FranchiseOwner) //For replace Owner name
                .Replace("{PHONE}", FranchisePhone) //For replace Phone
                .Replace("{EMAIL}", FromAddress) //For replace Email address

                .Replace("{INSTALLATIONDATE}", InstallationDateTime) //Appointment Date Time
                .Replace("{ADDRESS}", ClientAddress) //Client Address
                .Replace("{CLIENT}", Client) //Client
                .Replace("{CLIENTEMAIL}", clientEmail) //Client Email
                .Replace("{CLIENTPHONE}", clientPhone) //client Phone 
                ;

            #region ics

            //Reference: https://generacodice.com/en/articolo/24976/Creating-iCal-Files-in-c

            //create a new stringbuilder instance
            StringBuilder sb = new StringBuilder();

            //begin the calendar item
            sb.AppendLine("BEGIN:VCALENDAR");
            sb.AppendLine("VERSION:2.0");
            //sb.AppendLine("PRODID:stackoverflow.com");
            sb.AppendLine("CALSCALE:GREGORIAN");
            sb.AppendLine("METHOD:PUBLISH");

            //add the event
            sb.AppendLine("BEGIN:VEVENT");

            if (IsAllDay)
            {
                sb.AppendLine("DTSTART:" + model.StartDate.ToString("yyyyMMdd"));
                sb.AppendLine("DTEND:" + model.EndDate.ToString("yyyyMMdd"));
            }
            else
            {
                sb.AppendLine("DTSTART:" + model.StartDate.ToString("yyyyMMddTHHmm00"));
                sb.AppendLine("DTEND:" + model.EndDate.ToString("yyyyMMddTHHmm00"));
            }

            string IcsDescription = "Please note you've selected the installation date: \\n ";
            IcsDescription = IcsDescription + InstallationDateTime + " \\n ";
            IcsDescription = IcsDescription + Client + " \\n ";
            IcsDescription = IcsDescription + ClientAddress + " \\n ";

            //contents of the calendar item
            sb.AppendLine("SUMMARY:" + subject + "");
            sb.AppendLine("LOCATION:" + ClientAddress + "");
            sb.AppendLine("DESCRIPTION:" + IcsDescription + "");
            sb.AppendLine("PRIORITY:3");
            sb.AppendLine("END:VEVENT");

            //close calendar item
            sb.AppendLine("END:VCALENDAR");

            //create a string from the stringbuilder
            string CalendarItemAsString = sb.ToString();

            byte[] bytes = null;
            using (var ms = new MemoryStream())
            {
                TextWriter tw = new StreamWriter(ms);
                tw.Write(CalendarItemAsString);
                tw.Flush();
                ms.Position = 0;
                bytes = ms.ToArray();
            }
            #endregion

            ExchangeManager mgr = new ExchangeManager();
            mgr.SendEmailMultipleAttachment(FromAddress, subject, bodyHtml, toAddresses, ccAddresses, bccAddresses, null, bytes, icsAppointmentfilename);

            AddSentEmail(string.Join(",", toAddresses), subject, bodyHtml, true, "", ccAddresses, bccAddresses, FromAddress, LeadId, AccountId, OpportunityId, null, templateEmailId, OrderId, CalendarId);

            return true;

        }

        public bool BuildMailForProcurementError(dynamic purchaseOrder, string bodyHtml, string subject, short templateEmailId, int brandId, int franchiseId, string FromAddress, List<string> toAddresses, List<string> ccAddresses = null, List<string> bccAddresses = null, string Streamid = "")
        {
            var quotemgr = new QuotesManager();
            //Checking validation
            if (toAddresses == null || toAddresses.Count == 0)
                throw new ArgumentNullException("At least one recipient is required");
            if (toAddresses.Any(a => string.IsNullOrEmpty(a) || !RegexUtil.IsValidEmail(a)))
                throw new ArgumentException("Recipient email address is invalid");

            StringBuilder sbPay = new StringBuilder();
            sbPay.Append("");
            sbPay.Append("<table class='table table-quoteprint' style='border-bottom:1px solid #ddd;'>");
            sbPay.Append("<thead>");
            sbPay.Append("<tr>");
            sbPay.Append("<th>Franchise Name</th>");
            sbPay.Append("<th>Franchise Code</th>");
            sbPay.Append("<th>Sidemark</th>");
            sbPay.Append("<th>Counter Id</th>");
            sbPay.Append("<th>Submitted Date</th>");
            sbPay.Append("<th>MPO Number</th>");
            sbPay.Append("</tr>");
            sbPay.Append("</thead>");
            sbPay.Append("<tbody>");

            for (int i = 0; i < purchaseOrder.Count; i++)
            {

                sbPay.Append("<div>");
                sbPay.Append("<tr class='border_quoteprintmemo'>");
                sbPay.Append("<td>" + purchaseOrder[i].Name + "</td>");
                sbPay.Append("<td>" + purchaseOrder[i].Code + "</td>");
                sbPay.Append("<td>" + purchaseOrder[i].sidemark + "</td>");
                sbPay.Append("<td>" + purchaseOrder[i].CounterId + "</td>");
                sbPay.Append("<td>" + purchaseOrder[i].SubmittedDate.ToString("MM/dd/yyyy") + "</td>");
                sbPay.Append("<td>" + purchaseOrder[i].MasterPONumber + "</td>");
                sbPay.Append("</tr>");

            }

            sbPay.Append("</tbody>");
            sbPay.Append("</table>");


            bodyHtml = bodyHtml.Replace("{ErrorSection}", sbPay.ToString());



            //Replace the text in email content - Image path, brand, owner and phone
            //bodyHtml = bodyHtml.Replace("{IMAGEPATH}", ServerImgPath); //For replace image path

            ExchangeManager mgr = new ExchangeManager();
            mgr.SendVPOErrorEmail(FromAddress, subject, bodyHtml, toAddresses, ccAddresses, bccAddresses, Streamid);

            AddSentEmail(string.Join(",", toAddresses), subject, bodyHtml, true, "", ccAddresses, bccAddresses, FromAddress, null, null, null, null, templateEmailId, null);

            return true;

        }

        public bool BuildMailMessageError(MasterPurchaseOrder masterPurchaseOrder, int purchaseOrderId, string bodyHtml, string subject, short templateEmailId, int brandId, int franchiseId, string FromAddress, List<string> toAddresses, List<string> ccAddresses = null, List<string> bccAddresses = null, string Streamid = "")
        {
            var quotemgr = new QuotesManager();
            //Checking validation
            if (toAddresses == null || toAddresses.Count == 0)
                throw new ArgumentNullException("At least one recipient is required");
            if (toAddresses.Any(a => string.IsNullOrEmpty(a) || !RegexUtil.IsValidEmail(a)))
                throw new ArgumentException("Recipient email address is invalid");

            var Opportunity = quotemgr.GetOpportunityByQuoteKey(masterPurchaseOrder.QuoteId);
            var customer = quotemgr.GetCustomerByQuoteKey(masterPurchaseOrder.QuoteId);
            var BillingAddress = quotemgr.GetBillingAddressByQuoteKey(masterPurchaseOrder.QuoteId);
            var InstallationAddress = quotemgr.GetInstallationAddressByQuoteKey(masterPurchaseOrder.QuoteId);
            if (BillingAddress == null) BillingAddress = InstallationAddress;



            if (customer.HomePhone != null && customer.HomePhone.Length > 0)
            {
                string phoneFormat = "(###) ###-####";
                customer.HomePhone = Convert.ToInt64(customer.HomePhone).ToString(phoneFormat);
            }
            else
                customer.HomePhone = "";

            if (customer.CellPhone != null && customer.CellPhone.Length > 0)
            {
                string phoneFormat = "(###) ###-####";
                customer.CellPhone = Convert.ToInt64(customer.CellPhone).ToString(phoneFormat);
            }
            else
                customer.CellPhone = "";

            //Read lead html and append the data
            bodyHtml = bodyHtml.Replace("{OpportunityName}", Opportunity.OpportunityName);
            bodyHtml = bodyHtml.Replace("{orderNumber}", masterPurchaseOrder.OrderNumber);

            bodyHtml = bodyHtml.Replace("{AccountHomePhone}", customer.HomePhone);
            bodyHtml = bodyHtml.Replace("{AccountCellPhone}", customer.CellPhone);
            bodyHtml = bodyHtml.Replace("{AccountEmail}", customer.PrimaryEmail);
            bodyHtml = bodyHtml.Replace("{OppSideMark}", Opportunity.SideMark);

            bodyHtml = bodyHtml.Replace("{AccountCustomerName}", customer.FirstName + " " + customer.LastName);
            bodyHtml = bodyHtml.Replace("{InstallationAddress1}", InstallationAddress.Address1);
            bodyHtml = bodyHtml.Replace("{InstallationAddress2}", InstallationAddress.Address2);
            bodyHtml = bodyHtml.Replace("{InstallationCityStateZip}", InstallationAddress.City + ", " + InstallationAddress.State + " " + InstallationAddress.ZipCode);
            bodyHtml = bodyHtml.Replace("{purchaseOrder}", masterPurchaseOrder.MasterPONumber);

            bodyHtml = bodyHtml.Replace("{HeaderError}", masterPurchaseOrder.Message);



            StringBuilder sbPay = new StringBuilder();
            sbPay.Append("");
            sbPay.Append("<table class='table table-quoteprint' style='border-bottom:1px solid #ddd;'>");
            sbPay.Append("<thead>");
            sbPay.Append("<tr>");
            sbPay.Append("<th>Line</th>");
            sbPay.Append("<th>Vendor Name</th>");
            sbPay.Append("<th>Product</th>");
            sbPay.Append("<th>QTY</th>");
            sbPay.Append("<th>Error Message</th>");
            sbPay.Append("</tr>");
            sbPay.Append("</thead>");
            sbPay.Append("<tbody>");

            for (int i = 0; i < masterPurchaseOrder.CoreProductVPOs.Count; i++)
            {
                if (masterPurchaseOrder.CoreProductVPOs[i].StatusId == 10)
                {
                    sbPay.Append("<div>");
                    sbPay.Append("<tr class='border_quoteprintmemo'>");
                    sbPay.Append("<td>" + (i + 1) + "</td>");
                    sbPay.Append("<td>" + masterPurchaseOrder.CoreProductVPOs[i].VendorName + "</td>");
                    sbPay.Append("<td>" + masterPurchaseOrder.CoreProductVPOs[i].ProductName + "</td>");
                    sbPay.Append("<td>" + masterPurchaseOrder.CoreProductVPOs[i].Quantity + "</td>");
                    sbPay.Append("<td>" + masterPurchaseOrder.CoreProductVPOs[i].Errors + "</td>");
                    sbPay.Append("</tr>");
                }
            }

            sbPay.Append("</tbody>");
            sbPay.Append("</table>");


            bodyHtml = bodyHtml.Replace("{ErrorSection}", sbPay.ToString());



            //Replace the text in email content - Image path, brand, owner and phone
            //bodyHtml = bodyHtml.Replace("{IMAGEPATH}", ServerImgPath); //For replace image path

            ExchangeManager mgr = new ExchangeManager();
            mgr.SendVPOErrorEmail(FromAddress, subject, bodyHtml, toAddresses, ccAddresses, bccAddresses, Streamid);

            AddSentEmailWithFeId(Opportunity.FranchiseId, string.Join(",", toAddresses), subject, bodyHtml, ccAddresses, bccAddresses, null, null, null, null, templateEmailId, masterPurchaseOrder.OrderId);

            return true;

        }

        public bool BuildMailMessageThankyou(int LeadId, int AccountId, int OpportunityId, int OrderId, bool typestatus, string ResendMail, string bodyHtml, string subject, short templateEmailId, int brandId, int franchiseId, string FromAddress, List<string> toAddresses, List<string> ccAddresses = null, List<string> bccAddresses = null, string Streamid = "")
        {
            //Checking validation
            if (toAddresses == null || toAddresses.Count == 0)
                throw new ArgumentNullException("At least one recipient is required");
            if (toAddresses.Any(a => string.IsNullOrEmpty(a) || !RegexUtil.IsValidEmail(a)))
                throw new ArgumentException("Recipient email address is invalid");
            if (ccAddresses != null && ccAddresses.Any(a => !RegexUtil.IsValidEmail(a)))
                throw new ArgumentException("One or more CC recipient email address is invalid");
            if (bccAddresses != null && bccAddresses.Any(a => !RegexUtil.IsValidEmail(a)))
                throw new ArgumentException("One or more BCC recipient email address is invalid");

            //Get Franchise ID
            if (franchiseId == 0)
                franchiseId = Franchise.FranchiseId;

            string FranchiseName = "", FranchiseOwner = "", FranchisePhone = "";
            FranchiseOwner = GetFranchiseOwner(franchiseId);
            TerritoryDisplay data = GetTerritoryDisplayByOpp(OpportunityId);

            if (data != null && data.UseFranchiseName == false)
            {
                if (data.Displayname != "")
                    FranchiseName = data.Displayname + (data.LicenseNumber != null && data.LicenseNumber != "" ? " - " + data.LicenseNumber : "");
                else
                    FranchiseName = GetFranchiseName(franchiseId);
                FranchisePhone = data.Phone;
                if (data.PrimaryEmail != null && data.PrimaryEmail != "")
                    FromAddress = data.PrimaryEmail;
            }
            else
            {

                //Territory te = new Territory();
                //te = GetTerritoryDisplayForGray(OpportunityId, 1);


                //if (te != null)
                //    FranchiseName = te.Name;
                //else
                if (data != null)
                    FranchiseName = GetFranchiseName(franchiseId) + (data.LicenseNumber != null && data.LicenseNumber != "" ? " - " + data.LicenseNumber : "");
                else
                    FranchiseName = GetFranchiseName(franchiseId);

                FranchisePhone = !string.IsNullOrEmpty(Franchise.LocalPhoneNumber) ? Franchise.LocalPhoneNumber : Franchise.TollFreeNumber;
            }

            //For to refer Brand wise Image folder
            string[] BrandNameAbbr = { "BB", "TL", "CC" };
            //To get base server url to send with image 
            HttpContext context = HttpContext.Current;
            string baseUrl = context.Request.Url.Authority + context.Request.ApplicationPath.TrimEnd('/');
            string ServerImgPath = "https://" + baseUrl + "/Images/brand/" + BrandNameAbbr[brandId - 1] + "/";

            string homestarsdispay = "";
            if (Franchise != null && Franchise.Address != null && Franchise.Address.CountryCode2Digits.ToLower() == "us")
                homestarsdispay = "none";
            else
                homestarsdispay = "block";

            //Replace the text in email content - Image path, brand, owner and phone
            bodyHtml = bodyHtml.Replace("{IMAGEPATH}", ServerImgPath)
                .Replace("{FranchiseName}", FranchiseName).Replace("{homestarsdispay}", homestarsdispay); //For replace image path

            ExchangeManager mgr = new ExchangeManager();
            if (typestatus == true)
            {

                var mailres = mgr.SendEmail(FromAddress, subject, bodyHtml, toAddresses, ccAddresses, bccAddresses, Streamid);
                AddSentEmail(string.Join(",", toAddresses), subject, bodyHtml, mailres, ResendMail, ccAddresses, bccAddresses, FromAddress, LeadId, AccountId, OpportunityId, null, templateEmailId, OrderId);
            }
            else
            {
                AddSentEmail(string.Join(",", toAddresses), subject, bodyHtml, typestatus, ResendMail, ccAddresses, bccAddresses, FromAddress, LeadId, AccountId, OpportunityId, null, templateEmailId, OrderId);
            }

            return true;

        }

        public EmailTemplate GetHtmlBody(EmailType emailTemplateTypeId, int templateBrand)
        {
            if (emailTemplateTypeId <= 0 || templateBrand < 0)
                throw new ArgumentNullException("Template type id should be greater than zero");

            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    string Query = @"SELECT EmailTemplateId,TemplateSubject,TemplateLayout
                                    FROM CRM.EmailTemplates WHERE IsDeleted=0 AND IsVisible=1 AND 
                                    EmailTemplateTypeId=@EmailTemplateTypeId AND TemplateBrand=@TemplateBrand ";

                    //Execute and get details
                    var EmailBody = connection.Query<EmailTemplate>(Query,
                     new
                     {
                         EmailTemplateTypeId = (int)emailTemplateTypeId,
                         TemplateBrand = templateBrand
                     }).FirstOrDefault();
                    connection.Close();
                    return EmailBody;
                }

            }
            catch { return null; }
        }

        /// <summary>
        /// Get Lead Email Address
        /// <param name="leadId">Lead id</param>
        /// </summary>
        public CustomerTP GetLeadEmail(int leadId)
        {
            if (leadId <= 0)
                throw new ArgumentNullException("Lead id should be greater than zero");
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    string Query = @"select c.* from CRM.Customer c
                                    join CRM.LeadCustomers lc on c.CustomerId=lc.CustomerId
                                    where lc.LeadId=@LeadId and lc.IsPrimaryCustomer=1";

                    return connection.Query<CustomerTP>(Query, new { LeadId = leadId }).FirstOrDefault();
                }
            }
            catch (Exception e) { return null; }
        }

        public CustomerTP GetVPOSubmitEmail(int personId)
        {
            if (personId <= 0)
                throw new ArgumentNullException("organizer id should be greater than zero");
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {

                    var Query = @"select *  FROM CRM.Person where PersonId=@PersonId ";
                    var person = connection.Query<CustomerTP>(Query, new { PersonId = personId }).FirstOrDefault();


                    return person;
                }
            }
            catch { return null; }
        }
        /// <summary>
        /// Get Account Email Address
        /// <param name="accountId">Lead id</param>
        /// </summary>
        public CustomerTP GetAccountEmail(int AccountId)
        {
            if (AccountId <= 0)
                throw new ArgumentNullException("Account id should be greater than zero");

            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    string query = @"select c.* from CRM.Customer c
                                    join CRM.AccountCustomers ac on c.CustomerId=ac.CustomerId                                                       
                                    where ac.AccountId=@AccountId and ac.IsPrimaryCustomer=1";
                    //Execute and get details
                    return connection.Query<CustomerTP>(query, new { AccountId }).FirstOrDefault();
                }
            }
            catch (Exception ex) { return null; }
        }

        public CustomerTP GetOrderCustomerEmail(int orderId)
        {
            if (orderId <= 0)
                throw new ArgumentNullException("orderId should be greater than zero");

            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    string query = @"select c.* from CRM.Customer c
                                    join CRM.AccountCustomers ac on c.CustomerId=ac.CustomerId
                                    join CRM.Accounts a on ac.AccountId=a.AccountId
                                    join CRM.Opportunities op on a.AccountId=op.AccountId
                                    join CRM.Orders o on op.OpportunityId=o.OpportunityId
                                    where o.OrderID=@OrderId and ac.IsPrimaryCustomer=1";
                    //Execute and get details
                    return connection.Query<CustomerTP>(query, new { OrderId = orderId }).FirstOrDefault();
                }
            }
            catch (Exception ex) { return null; }
        }

        public List<Calendar> GetEMailReminder()
        {
            var reminders = new List<Calendar>();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {

                var reader = connection.QueryMultiple("CRM.GetCalendarListforReminders", commandType: CommandType.StoredProcedure);
                var calendarList = reader.Read<Calendar>().ToList();
                var etpList = reader.Read<EventToPerson>().ToList();
                connection.Close();

                foreach (var cal in calendarList)
                {
                    //var caltemp = new Calendar();
                    //caltemp.Attendees = new List<EventToPerson>();
                    cal.Attendees = etpList.Where(x => x.CalendarId == cal.CalendarId).OrderBy(x => x.EventPersonId).ToList();
                    reminders.Add(cal);
                }

            }
            return reminders;
        }
        /// <summary>
        /// Get Lead Name
        /// <param name="leadId">Lead id</param>
        /// </summary>
        public string GetClientDetails(int LeadId, int AccountId)
        {
            if (LeadId <= 0 && AccountId <= 0)
                throw new ArgumentNullException("Lead id should be greater than zero");

            string Email = "";
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    if (LeadId > 0)
                    {
                        string Query = @"select case when l.IsCommercial=1 
		                                 then 
		                                 case when CompanyName is not null and CompanyName!='' 
		                                 then CompanyName else FirstName + ' ' + LastName end  
		                                 else 
		                                 FirstName+' '+LastName+CASE
                                                WHEN CompanyName IS NOT NULL
                                                     AND CompanyName!=''
                                                THEN ' / '+CompanyName
                                                ELSE '' end
		                                 end
		                                 +';'+ISNULL(PrimaryEmail,'')+';'+ISNULL(CellPhone,'')
		                                 from CRM.Leads l
		                                 Inner join CRM.LeadCustomers lcus on l.LeadId=lcus.LeadId and lcus.IsPrimaryCustomer=1
		                                 Inner join [CRM].[Customer] cus on cus.CustomerId=lcus.CustomerId 
		                                 where l.LeadId=@LeadId ";

                        Email = connection.Query<string>(Query, new { LeadId }).FirstOrDefault();
                    }
                    else
                    {
                        string query = @"select case when acc.IsCommercial=1 
		                                 then 
		                                 case when CompanyName is not null and CompanyName!='' 
		                                 then CompanyName else FirstName + ' ' + LastName end  
		                                 else 
		                                 FirstName+' '+LastName+CASE
                                                WHEN CompanyName IS NOT NULL
                                                     AND CompanyName!=''
                                                THEN ' / '+CompanyName
                                                ELSE '' end
		                                 end
		                                 +';'+ISNULL(PrimaryEmail,'')+';'+ISNULL(CellPhone,'')  
		                                 from CRM.Accounts acc
		                                 Inner join CRM.AccountCustomers acccus on acc.AccountId=acccus.AccountId and acccus.IsPrimaryCustomer=1
		                                 Inner join [CRM].[Customer] cus on cus.CustomerId=acccus.CustomerId 
		                                 where acc.AccountId=@AccountId";

                        Email = connection.Query<string>(query, new { AccountId }).FirstOrDefault();
                    }
                    connection.Close();
                }
                return Email;
            }
            catch (Exception ex) { return Email; }
        }

        /// <summary>
        /// Get Franchise Email Address
        /// <param name="Franchise ID">Franchise id</param>
        /// </summary>
        public string GetFranchiseEmail(int franchiseId, EmailType emailType) //int emailType)
        {
            if (franchiseId <= 0)
                throw new ArgumentNullException("Franchise id should be greater than zero");
            //else if (emailType <= 0)
            //    throw new ArgumentNullException("Email type should be greater than zero");

            string Email = "";
            string Query = "";
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    //Query = @"SELECT top 1
                    //          ISNULL(case when USR.UserId=fo.UserId then f.OwnerEmail else USR.Email end,'')
                    //          FROM Auth.Users USR 
                    //          join CRM.Franchise f on USR.FranchiseId=f.FranchiseId
                    //          join CRM.FranchiseOwners fo on USR.FranchiseId=fo.FranchiseId
                    //          join CRM.EmailSettings EML ON USR.UserId = EML.FromEmailId
                    //          WHERE EML.FranchiseId = @FranchiseId AND EML.EmailType = @EmailType ";

                    ////Execute and get details
                    //Email = connection.Query<string>(Query, new { FranchiseId = franchiseId, EmailType = (int)emailType }).FirstOrDefault();

                    //If Email is not assigned then take from Person
                    if (Email == null || Email == "")
                    {
                        Query = @"SELECT case when ISNULL(AdminEmail,'') !='' then AdminEmail else ISNULL(OwnerEmail, '') end
                                  FROM CRM.Franchise WHERE FranchiseID = @FranchiseId ";
                        //Execute and get details
                        Email = connection.Query<string>(Query, new { FranchiseId = franchiseId }).FirstOrDefault();
                    }
                    connection.Close();
                }
                return Email;
            }
            catch { return Email; }
        }

        /// <summary>
        /// Get Franchise owner name and franchise name
        /// <param name="leadId">Lead id</param>
        /// </summary>
        public string GetFranchiseOwner(int FranchiseId)
        {
            var ReturnDetail = "";
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    //Open connection
                    connection.Open();
                    //Query

                    string Query = @"select p.FirstName+' '+p.LastName as OwnerName from Auth.Users au
                                    	join CRM.Person p on au.PersonId=p.PersonId
                                    	join CRM.FranchiseOwners fo on au.UserId=fo.UserId
                                    	where au.FranchiseId =@FranchiseId";

                    //Execute the query and get the value
                    ReturnDetail = connection.Query<string>(Query, new { FranchiseId }).FirstOrDefault();
                    //Close connection
                    connection.Close();
                }
                return ReturnDetail;
            }
            catch { return ReturnDetail; }
        }

        /// <summary>
        /// Get Franchise owner name, email and brand
        /// <param name="leadId">Lead id</param>
        /// </summary>
        public string GetFranchiseDetails(int LeadId, int AccountId)
        {
            if (LeadId <= 0 && AccountId <= 0)
                throw new ArgumentNullException("At least one lead id is required");

            var ReturnDetail = "";
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    //Open connection
                    connection.Open();
                    //Query
                    if (LeadId > 0)
                    {
                        string Query = @"SELECT Name + ';' + ISNULL(PER.FirstName + ' ' + PER.LastName,'') + ';' + CONVERT(VARCHAR,FRN.BrandId) + ';' + ISNULL(SupportEmail,'')+ ';' + ISNULL(AdminEmail,'') + ';' + CONVERT(VARCHAR,FRN.FranchiseId)
                                    FROM CRM.Franchise FRN 
                                    INNER JOIN CRM.FranchiseOwners FOW ON FRN.FranchiseId = FOW.FranchiseId 
                                    INNER JOIN Auth.Users USR ON USR.UserId = FOW.UserId 
                                    INNER JOIN CRM.Person PER ON USR.PersonId = PER.PersonId 
                                    INNER JOIN CRM.Leads LDS ON LDS.FranchiseId = FOW.FranchiseId 
                                    WHERE LDS.LeadId=@LeadId ";
                        //Execute the query and get the value
                        ReturnDetail = connection.Query<string>(Query, new { LeadId }).FirstOrDefault();
                    }
                    else
                    {
                        string Query = @"SELECT Name + ';' + ISNULL(PER.FirstName + ' ' + PER.LastName,'') + ';' + CONVERT(VARCHAR,FRN.BrandId) + ';' + ISNULL(SupportEmail,'')+ ';' + ISNULL(AdminEmail,'') + ';' + CONVERT(VARCHAR,FRN.FranchiseId)
                                        FROM CRM.Franchise FRN 
                                        INNER JOIN CRM.FranchiseOwners FOW ON FRN.FranchiseId = FOW.FranchiseId 
                                        INNER JOIN Auth.Users USR ON USR.UserId = FOW.UserId 
                                        INNER JOIN CRM.Person PER ON USR.PersonId = PER.PersonId 
                                        INNER JOIN CRM.Accounts acc ON acc.FranchiseId = FOW.FranchiseId 
                                        WHERE acc.AccountId=@AccountId";
                        ReturnDetail = connection.Query<string>(Query, new { AccountId }).FirstOrDefault();
                    }



                    //Close connection
                    connection.Close();
                }
                return ReturnDetail;
            }
            catch { return ReturnDetail; }
        }

        /// <summary>
        /// Get Franchise owner name and franchise name
        /// <param name="franchiseId">FranchiseId id</param>
        /// </summary>
        public string GetFranchiseName(int franchiseId)
        {
            if (franchiseId <= 0)
                throw new ArgumentNullException("Franchise id is required");

            var ReturnDetail = "";
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    //Open connection
                    connection.Open();
                    //Query
                    string Query = "SELECT ISNULL(Name, '') AS 'FranchiseName' ";
                    Query = Query + "FROM CRM.Franchise ";
                    Query = Query + "WHERE FranchiseId=@FranchiseId";
                    //Execute the query and get the value
                    ReturnDetail = connection.Query<string>(Query,
                    new
                    {
                        FranchiseId = franchiseId
                    }).FirstOrDefault();
                    //Close connection
                    connection.Close();
                }
                return ReturnDetail;
            }
            catch { return ReturnDetail; }
        }

        public string GetSalesPersonName(int OpportunityId)
        {
            if (OpportunityId <= 0)
                return "";

            var ReturnDetail = "";
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    //Open connection
                    connection.Open();
                    //Query
                    string Query = @"select p.FirstName+' '+p.LastName as Salesperson from CRM.Opportunities op
                                     join CRM.Person p on op.SalesAgentId=p.PersonId
                                     where OpportunityId=@OpportunityId";
                    ReturnDetail = connection.Query<string>(Query, new { OpportunityId }).FirstOrDefault();
                    //Close connection
                    connection.Close();
                }
                return ReturnDetail;
            }
            catch { return ReturnDetail; }
        }

        public User GetSalesPersonEmail(int OpportunityId, int OrderId = 0)
        {
            try
            {
                if (OpportunityId > 0)
                {
                    using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                    {
                        string Query = @"select u.Email,u.IsDisabled from CRM.Opportunities opp
                                     join CRM.Person p on opp.SalesAgentId=p.PersonId
                                     join Auth.Users u on p.PersonId=u.PersonId 
                                     where opp.OpportunityId=@OpportunityId";
                        return connection.Query<User>(Query, new { OpportunityId }).FirstOrDefault();
                    }
                }
                else
                {
                    using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                    {
                        string Query = @"select u.Email,u.IsDisabled from CRM.Orders o
                                         join CRM.Opportunities opp on o.OpportunityId=opp.OpportunityId
                                         join CRM.Person p on opp.SalesAgentId=p.PersonId
                                         join Auth.Users u on p.PersonId=u.PersonId 
                                         where o.OrderID=@OrderId";
                        return connection.Query<User>(Query, new { OrderId }).FirstOrDefault();
                    }
                }

            }
            catch { return null; }
        }

        public User GetInstallerEmail(int OpportunityId, int OrderId = 0)
        {
            try
            {
                if (OpportunityId > 0)
                {
                    using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                    {
                        string Query = @"select u.Email,u.IsDisabled from CRM.Opportunities opp
                                     join CRM.Person p on opp.InstallerId=p.PersonId
                                     join Auth.Users u on p.PersonId=u.PersonId 
                                     where opp.OpportunityId=@OpportunityId";
                        return connection.Query<User>(Query, new { OpportunityId }).FirstOrDefault();
                    }
                }
                else
                {
                    using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                    {
                        string Query = @"select u.Email,u.IsDisabled from CRM.Orders o
                                         join CRM.Opportunities opp on o.OpportunityId=opp.OpportunityId
                                         join CRM.Person p on opp.InstallerId=p.PersonId
                                         join Auth.Users u on p.PersonId=u.PersonId 
                                         where o.OrderID=@OrderId";
                        return connection.Query<User>(Query, new { OrderId }).FirstOrDefault();
                    }
                }
            }
            catch { return null; }
        }

        public void AddSentEmail(string recipients, string subject, string body, bool typestatus
            , string ResendMail
            , List<string> ccAddress, List<string> bccAddress, string FromAddress
            , int? leadId = null, int? accountId = null, int? opportunityId = null, int? jobId = null, Int16? templateId = null, int? orderId = null
            , int? CalendarId = null
            )
        {
            try
            {
                if (ccAddress == null)
                    ccAddress = new List<string>();
                if (bccAddress == null)
                    bccAddress = new List<string>();

                //Save email to sent email history table - History.SentEmail
                var htmlBody = WebUtility.HtmlDecode(body);
                if (leadId == 0) { leadId = null; }
                if (jobId == 0) { jobId = null; }
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {

                    connection.Open();
                    if (orderId != null && orderId > 0 && ResendMail == "")
                    {
                        var Querys = @"select * from History.SentEmails where OrderId=@OrderId and TemplateId=@TemplateId";
                        var result = connection.Query<dynamic>(Querys, new { OrderId = orderId, TemplateId = templateId }).FirstOrDefault();
                        if (result != null)
                        {
                            int templateEmailId = getemailtypeid(Convert.ToInt32(result.TemplateId));
                            if (result.IsSent == false && templateEmailId == 6)
                            {
                                string quryes = @"Update History.SentEmails set IsSent=@IsSent where SentEmailId=@SentEmailId";
                                var results = connection.Query<string>(quryes, new { IsSent = typestatus, SentEmailId = result.SentEmailId });
                                return;
                            }
                            else if (result.IsSent == true && templateEmailId == 6)
                            {
                                string quryes = @"Update History.SentEmails set IsSent=@IsSent where SentEmailId=@SentEmailId";
                                var results = connection.Query<string>(quryes, new { IsSent = typestatus, SentEmailId = result.SentEmailId });
                                return;
                            }
                        }
                    }

                    string Query = "";
                    Query = "INSERT INTO History.SentEmails ";
                    Query = Query + "(LeadId,JobId,Recipients,Subject,Body,SentAt";
                    Query = Query + ",FranchiseId,TemplateId,SentByPersonId, CCAddress, BCCAddress,AccountId,OpportunityId,OrderId,CalendarId,IsSent,LogType)";
                    Query = Query + "VALUES (@LeadId,@JobId,@Recipients,@Subject,@Body,@SentAt";
                    Query = Query + ",@FranchiseId,@TemplateId,@SentByPersonId,@CCAddress,@BCCAddress,@AccountId,@OpportunityId,@OrderId,@CalendarId,@IsSent,@LogType)";
                    var SentEmail = connection.Query<string>(Query,
                    new
                    {
                        JobId = jobId,
                        LeadId = leadId,
                        FranchiseId = Franchise == null ? (int?)null : this.Franchise.FranchiseId,
                        SentAt = DateTime.UtcNow,
                        SentByPersonId = User != null && User.PersonId > 0 ? User.PersonId : (int?)null,
                        Recipients = recipients,
                        Subject = subject,
                        Body = htmlBody,
                        TemplateId = templateId,
                        CCAddress = string.Join(",", ccAddress),
                        BCCAddress = string.Join(",", bccAddress),
                        AccountId = accountId,
                        OpportunityId = opportunityId,
                        OrderId = orderId,
                        CalendarId = CalendarId,
                        IsSent = typestatus,
                        LogType = ContantStrings.LogTypeId,
                    });
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
            }

        }
        public void AddSentEmailWithFeId(int? franchiseId, string recipients, string subject, string body
        , List<string> ccAddress, List<string> bccAddress
        , int? leadId = null, int? accountId = null, int? opportunityId = null, int? jobId = null, Int16? templateId = null, int? orderId = null
        )
        {
            try
            {
                //Save email to sent email history table - History.SentEmail
                var htmlBody = WebUtility.HtmlDecode(body);
                if (leadId == 0) { leadId = null; }
                if (jobId == 0) { jobId = null; }
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {

                    connection.Open();
                    string Query = "";
                    Query = "INSERT INTO History.SentEmails ";
                    Query = Query + "(LeadId,JobId,Recipients,Subject,Body,SentAt";
                    Query = Query + ",FranchiseId,TemplateId,SentByPersonId, CCAddress, BCCAddress,AccountId,OpportunityId,OrderId,LogType)";
                    Query = Query + "VALUES (@LeadId,@JobId,@Recipients,@Subject,@Body,@SentAt";
                    Query = Query + ",@FranchiseId,@TemplateId,@SentByPersonId,@CCAddress,@BCCAddress,@AccountId,@OpportunityId,@OrderId,@LogType)";
                    var SentEmail = connection.Query<string>(Query,
                    new
                    {
                        JobId = jobId,
                        LeadId = leadId,
                        FranchiseId = franchiseId.HasValue ? franchiseId.Value : 2,
                        SentAt = DateTime.UtcNow,
                        SentByPersonId = User != null && User.PersonId > 0 ? User.PersonId : (int?)null,
                        Recipients = recipients,
                        Subject = subject,
                        Body = htmlBody,
                        TemplateId = templateId,
                        CCAddress = ccAddress,
                        BCCAddress = bccAddress,
                        AccountId = accountId,
                        OpportunityId = opportunityId,
                        OrderId = orderId,
                        LogType = ContantStrings.LogTypeId,
                    });
                    connection.Close();
                }
            }
            catch (Exception ex)
            {

            }

        }

        public int getemailtypeid(int templateid)
        {
            switch (templateid)
            {
                case 501:
                case 502:
                case 503:
                    return 1;
                case 504:
                case 505:
                case 506:
                    return 2;
                case 507:
                case 508:
                case 509:
                    return 3;
                case 510:
                case 511:
                case 512:
                    return 4;
                case 513:
                case 514:
                case 515:
                    return 5;
                case 516:
                case 517:
                case 518:
                    return 6;
                case 521:
                case 522:
                case 523:
                    return 10;
                default: return 1;
            }
        }

        public static string CreateMessageWithAttachment(int id, EmailUrlParam emaildata)
        {

            string file = emaildata.AttachmentLink;
            // Create a message and set up the recipients.
            if (emaildata.fromAddresses == null)
            {
                emaildata.fromAddresses = SessionManager.CurrentUser.Person.PrimaryEmail;
            }

            //MailMessage message = new MailMessage(
            //   emaildata.fromAddresses,
            //   emaildata.ToAddresses.ToString(),
            //   emaildata.Subject,
            // emaildata.Body);
            // MailMessage message = new MailMessage();

            var fromAddresses = new MailAddress(emaildata.fromAddresses, SessionManager.CurrentUser.Person.FullName);
            MailMessage message = new MailMessage();
            message.From = fromAddresses;
            message.To.Add(emaildata.ToAddresses);
            message.Subject = emaildata.Subject;
            message.IsBodyHtml = true;
            message.Body = emaildata.Body;

            // Create  the file attachment for this e-mail message.
            Attachment data = new Attachment(file, MediaTypeNames.Application.Octet);
            // Add time stamp information for the file.
            ContentDisposition disposition = data.ContentDisposition;
            disposition.CreationDate = System.IO.File.GetCreationTime(file);
            disposition.ModificationDate = System.IO.File.GetLastWriteTime(file);
            disposition.ReadDate = System.IO.File.GetLastAccessTime(file);
            // Add the file attachment to this e-mail message.
            message.Attachments.Add(data);

            //Send the message.
            SmtpClient client = new SmtpClient();
            // Add credentials if the SMTP server requires them.

            try
            {
                client.Send(message);
                return "Success";
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception caught in CreateMessageWithAttachment(): {0}",
                            ex.ToString());
            }
            // Display the values in the ContentDisposition for the attachment.
            ContentDisposition cd = data.ContentDisposition;
            Console.WriteLine("Content disposition");
            Console.WriteLine(cd.ToString());
            Console.WriteLine("File {0}", cd.FileName);
            Console.WriteLine("Size {0}", cd.Size);
            Console.WriteLine("Creation {0}", cd.CreationDate);
            Console.WriteLine("Modification {0}", cd.ModificationDate);
            Console.WriteLine("Read {0}", cd.ReadDate);
            Console.WriteLine("Inline {0}", cd.Inline);
            Console.WriteLine("Parameters: {0}", cd.Parameters.Count);
            foreach (DictionaryEntry d in cd.Parameters)
            {
                Console.WriteLine("{0} = {1}", d.Key, d.Value);
            }
            data.Dispose();
            return "Success";
        }

        public List<EmailSettings> GetConfiguredEmails()
        {
            var query = @"select * from CRM.EmailSettings where FranchiseId = @franchiseId";
            var result = ExecuteIEnumerableObject<EmailSettings>(ContextFactory.CrmConnectionString, query, new
            {
                franchiseId = Franchise.FranchiseId
            }).ToList();
            if (result != null && result.Count > 0)
            {
                foreach (var item in result)
                {
                    var subQuery = @"select u.Email  from Auth.Users u where UserId =@id";
                    var subResult = ExecuteIEnumerableObject<string>(ContextFactory.CrmConnectionString, subQuery, new
                    {
                        id = item.FromEmailId
                    }).FirstOrDefault();
                    item.Email = subResult;
                    if (item.EmailType == 1)
                    {
                        item.EmailTypeName = "Confirm Receipt of Lead";
                    }
                    else if (item.EmailType == 2)
                    {
                        item.EmailTypeName = "Design/Sales Appointment";
                    }
                    else if (item.EmailType == 3)
                    {
                        item.EmailTypeName = "Design/Sales Appointment Reminder";
                    }
                    else if (item.EmailType == 4)
                    {
                        item.EmailTypeName = "Order Summary";
                    }
                    else if (item.EmailType == 5)
                    {
                        item.EmailTypeName = "Installation Schedule Confirmation";
                    }
                    else if (item.EmailType == 6)
                    {
                        //item.EmailTypeName = "Thank You/Review Request";
                        item.EmailTypeName = "Thank You";
                    }
                }
                return result;
            }
            else
            {
                string queryuser = "select * from Auth.Users where FranchiseId=@FranchiseId";
                var queryuserresult = ExecuteIEnumerableObject<User>(ContextFactory.CrmConnectionString, queryuser, new { FranchiseId = Franchise.FranchiseId }).FirstOrDefault();

                for (int i = 1; i <= 6; i++)
                {
                    EmailSettings e = new EmailSettings();
                    e.FranchiseId = (int)queryuserresult.FranchiseId;
                    e.EmailType = i;
                    e.FromEmailId = queryuserresult.UserId;
                    e.CopyAssignedSales = false;
                    e.CopyAssignedInstaller = false;
                    Insert<EmailSettings>(ContextFactory.CrmConnectionString, e);
                }

                var resultemail = ExecuteIEnumerableObject<EmailSettings>(ContextFactory.CrmConnectionString, query, new
                {
                    franchiseId = Franchise.FranchiseId
                }).ToList();
                if (resultemail != null && resultemail.Count > 0)
                {
                    foreach (var item in resultemail)
                    {
                        var subQuery = @"select u.Email  from Auth.Users u where UserId =@id";
                        var subResult = ExecuteIEnumerableObject<string>(ContextFactory.CrmConnectionString, subQuery, new
                        {
                            id = item.FromEmailId
                        }).FirstOrDefault();
                        item.Email = subResult;
                        if (item.EmailType == 1)
                        {
                            item.EmailTypeName = "Confirm Receipt of Lead";
                        }
                        else if (item.EmailType == 2)
                        {
                            item.EmailTypeName = "Design/Sales Appointment";
                        }
                        else if (item.EmailType == 3)
                        {
                            item.EmailTypeName = "Design/Sales Appointment Reminder";
                        }
                        else if (item.EmailType == 4)
                        {
                            item.EmailTypeName = "Order Summary";
                        }
                        else if (item.EmailType == 5)
                        {
                            item.EmailTypeName = "Installation Schedule Confirmation";
                        }
                        else if (item.EmailType == 6)
                        {
                            item.EmailTypeName = "Thank You/Review Request";
                        }
                    }
                    return resultemail;
                }
                else
                    return null;
            }

        }

        public class DisplayTerritory
        {
            public int FranchiseId { get; set; }
            public int TerritoryId { get; set; }

            public string TerritoryName { get; set; }

        }

        public DisplayTerritory GetDisplayTerritoryId()
        {
            //  var res = select TerritoryId from CRM.DefaultTerritory where FranchiseId = 2;

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var res = connection.Query<DisplayTerritory>(@"Select dt.*,t.Name as TerritoryName from CRM.DefaultTerritory dt
                                                                     join CRM.Territories t on t.TerritoryId = dt.TerritoryId
                                                                     where dt.FranchiseId =@FranchiseId", new { FranchiseId = Franchise.FranchiseId }).FirstOrDefault();
                if (res != null)
                    return res;
                else
                    return new DisplayTerritory();
            }

        }

        public string UpdateConfiguredEmails(int DisplayTerritoryId, List<EmailSettings> lstEmailSettings)
        {
            foreach (var item in lstEmailSettings)
            {
                var query = @"update  CRM.EmailSettings 
                        set FromEmailId = @fromEmailId,
                            CopyAssignedSales= @copyAssignedSales,
                            CopyAssignedInstaller=@copyAssignedInstaller,
                            LastUpdatedOn =@lastUpdatedOn   
                            where FranchiseId = @franchiseId and Id =@id";
                ExecuteIEnumerableObject<EmailSettings>(ContextFactory.CrmConnectionString, query, new
                {
                    fromEmailId = item.FromEmailId,
                    copyAssignedSales = item.CopyAssignedSales,
                    copyAssignedInstaller = item.CopyAssignedInstaller,
                    lastUpdatedOn = DateTime.Now,
                    franchiseId = Franchise.FranchiseId,
                    id = item.Id
                });
            }


            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                if (DisplayTerritoryId > 0)
                {
                    string Query = @" insert into CRM.DefaultTerritory values(@FranchiseId,@TerritoryId)";
                    string Query1 = @" Update CRM.DefaultTerritory  set TerritoryId=@TerritoryId where FranchiseId=@FranchiseId";

                    var res = connection.Query("Select * from CRM.DefaultTerritory where FranchiseId=@FranchiseId", new { FranchiseId = Franchise.FranchiseId }).FirstOrDefault();
                    if (res != null)
                        connection.Execute(Query1, new { TerritoryId = DisplayTerritoryId, FranchiseId = Franchise.FranchiseId });
                    else
                        connection.Execute(Query, new { TerritoryId = DisplayTerritoryId, FranchiseId = Franchise.FranchiseId });

                }
                else
                {
                    connection.Execute("delete from CRM.DefaultTerritory where FranchiseId=@FranchiseId", new { FranchiseId = Franchise.FranchiseId });
                }
            }
            return "Success";
        }

        public OrderDTO GetOrderInfo(int orderId)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {

                    string Query = @"select op.AccountId,op.OpportunityId,o.OrderID from CRM.Orders o
                                    join CRM.Opportunities op on o.OpportunityId=op.OpportunityId
                                    where o.OrderID=@orderId";
                    return connection.Query<OrderDTO>(Query, new { orderId }).FirstOrDefault();
                }
            }
            catch { return null; }
        }

        public Opportunity GetOpportunityInfo(int OpportunityId)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    string Query = @"select AccountId from CRM.Opportunities where OpportunityId=@OpportunityId";
                    return connection.Query<Opportunity>(Query, new { OpportunityId }).FirstOrDefault();
                }
            }
            catch { return null; }
        }


        public string SendRemaindarEmail()
        {
            FranchiseManager _franMgr = new FranchiseManager(null);

            //
            //Get all the Calendar Events to whom the email reminders should be sent
            var CalendarList = GetEMailReminder();

            //To sending Reminder email
            //foreach (int leadId in LeadsIds)
            foreach (var cal in CalendarList)
            {
                var franchise = _franMgr.GetFEinfo(cal.FranchiseId);
                this.Franchise = franchise;
                int LeadId = 0;
                int AccountId = 0;
                if (cal.LeadId != null && cal.LeadId > 0)
                {
                    LeadId = (int)cal.LeadId;

                }
                if (cal.AccountId != null && cal.AccountId > 0)
                {
                    AccountId = (int)cal.AccountId;
                }
                var Notify = Getemailnotification(LeadId, AccountId);
                if (LeadId != 0 || AccountId != 0)
                {
                    List<string> toAddresses = new List<string>(), ccAddresses = new List<string>(), bccAddresses = new List<string>();

                    var EmailSettings = GetConfiguredEmails().Where(x => x.EmailType == 3).FirstOrDefault();
                    if (EmailSettings != null)
                    {
                        if (cal.OpportunityId != null && cal.OpportunityId > 0)
                        {
                            if (EmailSettings.CopyAssignedSales == true)
                            {
                                var salesemail = GetSalesPersonEmail((int)cal.OpportunityId);
                                if (salesemail != null && !string.IsNullOrEmpty(salesemail.Email) && salesemail.IsDisabled != true)
                                    //if (salesemail.Email != null && salesemail.Email != "" && salesemail.IsDisabled != true)
                                    ccAddresses.Add(salesemail.Email);
                            }
                            if (EmailSettings.CopyAssignedInstaller == true)
                            {
                                var installeremail = GetInstallerEmail((int)cal.OpportunityId);
                                if (installeremail != null && !string.IsNullOrEmpty(installeremail.Email) && installeremail.IsDisabled != true)
                                    //if (installeremail.Email != null && installeremail.Email != "" && installeremail.IsDisabled !=true)
                                    ccAddresses.Add(installeremail.Email);
                            }
                        }
                    }

                    string FranchiseDetail = GetFranchiseDetails(LeadId, AccountId);
                    string[] FranchiseDetails = null;
                    if (FranchiseDetail.Length > 0)
                    {
                        FranchiseDetails = FranchiseDetail.Split(';');
                    }


                    TerritoryDisplay data = new TerritoryDisplay();

                    if (cal.OpportunityId != null && cal.OpportunityId > 0)
                        data = GetTerritoryDisplayByOpp((int)cal.OpportunityId);
                    else if (cal.AccountId != null && cal.AccountId > 0)
                        data = GetTerritoryDisplayByAccountId((int)cal.AccountId);
                    else if (cal.LeadId != null && cal.LeadId > 0)
                        data = GetTerritoryDisplayByLeadId((int)cal.LeadId);

                    //BUILD - From email address
                    string fromEmail = "";
                    if (data != null && data.Id == null)
                        fromEmail = GetFranchiseEmail(franchise.FranchiseId, EmailType.Reminder); //, 3);
                    else
                    {
                        if (franchise.AdminEmail != "")
                            fromEmail = franchise.AdminEmail;
                        else if (franchise.OwnerEmail != "")
                            fromEmail = franchise.OwnerEmail;
                    }

                    CustomerTP LeadCustomer = new CustomerTP();
                    if (AccountId > 0)
                    {
                        LeadCustomer = GetAccountEmail(AccountId);
                    }
                    else if (LeadId > 0)
                    {
                        LeadCustomer = GetLeadEmail(LeadId);
                    }

                    //If Valid email then process the email for send
                    if (LeadCustomer != null || fromEmail != null)
                    {
                        if (LeadCustomer != null)
                        {
                            if (LeadCustomer.PrimaryEmail != null && LeadCustomer.PrimaryEmail.Trim() != "")
                            {
                                toAddresses.Add(LeadCustomer.PrimaryEmail.Trim());
                            }
                            if (LeadCustomer.SecondaryEmail != null && LeadCustomer.SecondaryEmail.Trim() != "")
                            {
                                toAddresses.Add(LeadCustomer.SecondaryEmail.Trim());
                            }
                        }

                        //If no from or to emails return
                        if (toAddresses == null || toAddresses.Count == 0 || fromEmail == "")
                        {
                            continue;
                        }
                        //return Json("Success");

                        string TemplateSubject = "", bodyHtml = "";

                        //Email Template: 1 Thanks Email; 2 Appointment Confirmation; 3 Reminder Email
                        int BrandId = Convert.ToInt16(FranchiseDetails[2]);

                        //Take Body Html from EmailTemplate Table
                        var emailtemplate = GetHtmlBody(EmailType.Reminder, BrandId); //3, BrandId);
                        TemplateSubject = emailtemplate.TemplateSubject;
                        bodyHtml = emailtemplate.TemplateLayout;
                        short TemplateEmailId = (short)emailtemplate.EmailTemplateId;

                        //cc all the users from calendarevent
                        //ccAddresses = cal.Attendees.Select(x => x.PersonEmail).ToList();

                        var AppDate = (DateTimeOffset)cal.StartDate;

                        var Franchiseinfo = GetFranchiseInfo(cal.FranchiseId);

                        if (Franchiseinfo == null) continue;

                        string salesperson = "";

                        foreach (var person in cal.Attendees)
                        {
                            var user = LocalMembership.GetUser((int)person.PersonId, "Person", "Roles", "OAuthUsers", "UserWidgets");
                            var res = user.IsInRole(AppConfigManager.DefaultSalesRole.RoleId, AppConfigManager.DefaultSalesNoMargineRole.RoleId);
                            salesperson = user.Person.FullName;
                            if (res == true) break;
                        }

                        if (Notify.IsNotifyemails == true)
                        {

                            var mailMsg = BuildMailMessageReminder(LeadId, AccountId,
                            cal.OpportunityId != null && cal.OpportunityId > 0 ? (int)cal.OpportunityId : 0,
                            cal.OrderId != null && cal.OrderId > 0 ? (int)cal.OrderId : 0,
                            cal.IsAllDay,
                            AppDate.DateTime, cal.Location, salesperson,
                            bodyHtml, TemplateSubject
                            , TemplateEmailId, BrandId, Franchiseinfo
                            , fromEmail, toAddresses, ccAddresses, bccAddresses);
                        }
                    }
                }

            }
            return "Success";
        }

        private Franchise GetFranchiseInfo(int FranchiseId)
        {
            var frnManager = new FranchiseManager(null);
            if (FranchiseId > 0)
            {
                var fedata = frnManager.GetFEinfo(FranchiseId);
                string query = @"Select * from CRM.Addresses where AddressId=@addressId";
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    var AddressData = connection.Query<Address>(query, new { addressId = fedata.AddressId }).FirstOrDefault();
                    if (AddressData != null)
                    {
                        fedata.Address = AddressData;
                    }
                    return fedata;
                }
            }
            else
                return null;
        }

        public TerritoryDisplay GetTerritoryDisplayByOpp(int OpportunityId)
        {
            string query = @"select tes.Id,isnull(tes.UseFranchiseName,1) UseFranchiseName,(select [CRM].[fnGetTerritoryDisplayName](t.BrandId,t.TerritoryId)) as Displayname,tes.LicenseNumber,p.PrimaryEmail, stl.PhoneNumber as Phone 
                            from CRM.Opportunities op
                            left join CRM.Territories t on op.TerritoryId=t.TerritoryId
                            left join CRM.TerritoryEmailSettings tes on t.TerritoryId=tes.TerritoryId
                            left join CRM.Person p on tes.FromEmailPersonId=p.PersonId
                            left join CRM.ShipToLocations stl on tes.ShipToLocationId=stl.Id
                            left join CRM.Addresses ad on stl.AddressId=ad.AddressId
                            where op.FranchiseId=@FranchiseId and op.OpportunityId=@OpportunityId";

            string query2 = @"select tes.Id,isnull(tes.UseFranchiseName,1) UseFranchiseName,(select [CRM].[fnGetTerritoryDisplayName](t.BrandId,t.TerritoryId)) as Displayname,tes.LicenseNumber,p.PrimaryEmail, stl.PhoneNumber as Phone 
                            from CRM.Opportunities op
                            left join CRM.Territories t on op.TerritoryDisplayId=t.TerritoryId
                            left join CRM.TerritoryEmailSettings tes on t.TerritoryId=tes.TerritoryId
                            left join CRM.Person p on tes.FromEmailPersonId=p.PersonId
                            left join CRM.ShipToLocations stl on tes.ShipToLocationId=stl.Id
                            left join CRM.Addresses ad on stl.AddressId=ad.AddressId
                            where op.FranchiseId=@FranchiseId and op.OpportunityId=@OpportunityId";

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var checkres = connection.Query<Opportunity>("select * from CRM.Opportunities where FranchiseId=@FranchiseId and OpportunityId = @OpportunityId", new { Franchise.FranchiseId, OpportunityId }).FirstOrDefault();

                if (!(checkres.TerritoryType.ToUpper().Contains("GRAY AREA")))
                    return connection.Query<TerritoryDisplay>(query, new { Franchise.FranchiseId, OpportunityId }).FirstOrDefault();
                else return connection.Query<TerritoryDisplay>(query2, new { Franchise.FranchiseId, OpportunityId }).FirstOrDefault();
            }
        }

        public TerritoryDisplay GetTerritoryDisplayByLeadId(int LeadId)
        {
            string query = @"Begin
                             Declare @BrandId int,@TerritoryId int,@ZipCode varchar(15),@CountryCode varchar(2)
                             
                             If exists( select 1 from CRM.Leads l
                             join CRM.LeadAddresses lad on l.LeadId=lad.LeadId
                             join CRM.Addresses ad on lad.AddressId=ad.AddressId
                             where FranchiseId=@FranchiseId and l.LeadId=@LeadId and ISNULL(ad.IsInstallationAddress,0)=1)
                             	Begin
                             	select @ZipCode=ad.ZipCode,@CountryCode=ad.CountryCode2Digits from CRM.Leads l
                             	join CRM.LeadAddresses lad on l.LeadId=lad.LeadId
                             	join CRM.Addresses ad on lad.AddressId=ad.AddressId
                             	where FranchiseId=@FranchiseId and l.LeadId=@LeadId and ISNULL(ad.IsInstallationAddress,0)=1
                             	end
                             else
                             	begin
                             	select top 1 @ZipCode=ad.ZipCode,@CountryCode=ad.CountryCode2Digits from CRM.Leads l
                             	join CRM.LeadAddresses lad on l.LeadId=lad.LeadId
                             	join CRM.Addresses ad on lad.AddressId=ad.AddressId
                             	where FranchiseId=@FranchiseId and l.LeadId=@LeadId
                             	end
                             
                             	select @BrandId=BrandId from CRM.Franchise where FranchiseId=@FranchiseId
                             
                             	set @TerritoryId=(select CRM.fnGetTerritory_Id(@BrandId,@Zipcode,@CountryCode,@FranchiseId))
                             
                             	select tes.Id,isnull(tes.UseFranchiseName,1) UseFranchiseName,(select [CRM].[fnGetTerritoryDisplayName](t.BrandId,t.TerritoryId)) as Displayname,tes.LicenseNumber,p.PrimaryEmail, stl.PhoneNumber as Phone 
                             	from CRM.Territories t
                             	left join CRM.TerritoryEmailSettings tes on t.TerritoryId=tes.TerritoryId
                             	left join CRM.Person p on tes.FromEmailPersonId=p.PersonId
                             	left join CRM.ShipToLocations stl on tes.ShipToLocationId=stl.Id
                             	left join CRM.Addresses ad on stl.AddressId=ad.AddressId
                             	where t.TerritoryId=@TerritoryId 
                             
                             end";

            string query2 = @"select tes.Id,isnull(tes.UseFranchiseName,1) UseFranchiseName,(select [CRM].[fnGetTerritoryDisplayName](t.BrandId,t.TerritoryId)) as Displayname,tes.LicenseNumber,p.PrimaryEmail, stl.PhoneNumber as Phone 
                            from CRM.Leads op
                            left join CRM.Territories t on op.TerritoryDisplayId=t.TerritoryId
                            left join CRM.TerritoryEmailSettings tes on t.TerritoryId=tes.TerritoryId
                            left join CRM.Person p on tes.FromEmailPersonId=p.PersonId
                            left join CRM.ShipToLocations stl on tes.ShipToLocationId=stl.Id
                            left join CRM.Addresses ad on stl.AddressId=ad.AddressId
                            where op.FranchiseId=@FranchiseId and op.LeadId=@LeadId";

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var checkres = connection.Query<Lead>("select * from CRM.Leads where FranchiseId=@FranchiseId and LeadId = @LeadId", new { Franchise.FranchiseId, LeadId }).FirstOrDefault();

                if (!(checkres.TerritoryType.ToUpper().Contains("GRAY AREA")))
                    return connection.Query<TerritoryDisplay>(query, new { Franchise.FranchiseId, LeadId }).FirstOrDefault();
                else return connection.Query<TerritoryDisplay>(query2, new { Franchise.FranchiseId, LeadId }).FirstOrDefault();
            }
        }

        public TerritoryDisplay GetTerritoryDisplayByAccountId(int AccountId)
        {
            string query = @"Begin
                             Declare @BrandId int,@TerritoryId int,@ZipCode varchar(15),@CountryCode varchar(2)
                             
                             If exists( select 1 from CRM.Accounts a
                             join CRM.AccountAddresses aad on a.AccountId=aad.AccountId
                             join CRM.Addresses ad on aad.AddressId=ad.AddressId
                             where FranchiseId=@FranchiseId and a.AccountId=@AccountId and ISNULL(ad.IsInstallationAddress,0)=1)
                             	Begin
                             	select @ZipCode=ad.ZipCode,@CountryCode=ad.CountryCode2Digits from CRM.Accounts a
								join CRM.AccountAddresses aad on a.AccountId=aad.AccountId
								join CRM.Addresses ad on aad.AddressId=ad.AddressId
								where FranchiseId=@FranchiseId and a.AccountId=@AccountId and ISNULL(ad.IsInstallationAddress,0)=1
                             	end
                             else
                             	begin
                             	select top 1 @ZipCode=ad.ZipCode,@CountryCode=ad.CountryCode2Digits from CRM.Accounts a
                             	join CRM.AccountAddresses aad on a.AccountId=aad.AccountId
								join CRM.Addresses ad on aad.AddressId=ad.AddressId
								where FranchiseId=@FranchiseId and a.AccountId=@AccountId
                             	end
                             
                             	select @BrandId=BrandId from CRM.Franchise where FranchiseId=@FranchiseId
                             
                             	set @TerritoryId=(select CRM.fnGetTerritory_Id(@BrandId,@Zipcode,@CountryCode,@FranchiseId))
                             
                             	select tes.Id,isnull(tes.UseFranchiseName,1) UseFranchiseName,(select [CRM].[fnGetTerritoryDisplayName](t.BrandId,t.TerritoryId)) as Displayname,tes.LicenseNumber,p.PrimaryEmail, stl.PhoneNumber as Phone 
                             	from CRM.Territories t
                             	left join CRM.TerritoryEmailSettings tes on t.TerritoryId=tes.TerritoryId
                             	left join CRM.Person p on tes.FromEmailPersonId=p.PersonId
                             	left join CRM.ShipToLocations stl on tes.ShipToLocationId=stl.Id
                             	left join CRM.Addresses ad on stl.AddressId=ad.AddressId
                             	where t.TerritoryId=@TerritoryId 
                             
                             end";

            string query2 = @"	select tes.Id,isnull(tes.UseFranchiseName,1) UseFranchiseName,(select [CRM].[fnGetTerritoryDisplayName](t.BrandId,t.TerritoryId)) as Displayname,tes.LicenseNumber,p.PrimaryEmail, stl.PhoneNumber as Phone 
                            from CRM.Accounts op
                            left join CRM.Territories t on op.TerritoryDisplayId=t.TerritoryId
                            left join CRM.TerritoryEmailSettings tes on t.TerritoryId=tes.TerritoryId
                            left join CRM.Person p on tes.FromEmailPersonId=p.PersonId
                            left join CRM.ShipToLocations stl on tes.ShipToLocationId=stl.Id
                            left join CRM.Addresses ad on stl.AddressId=ad.AddressId
                            where op.FranchiseId=@FranchiseId and op.AccountId=@AccountId";

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var checkres = connection.Query<AccountTP>("select * from CRM.Accounts where FranchiseId=@FranchiseId and AccountId = @AccountId", new { Franchise.FranchiseId, AccountId }).FirstOrDefault();

                if (!(checkres.TerritoryType.ToUpper().Contains("GRAY AREA")))
                    return connection.Query<TerritoryDisplay>(query, new { Franchise.FranchiseId, AccountId }).FirstOrDefault();
                else return connection.Query<TerritoryDisplay>(query2, new { Franchise.FranchiseId, AccountId }).FirstOrDefault();
            }
        }

        public NotifyValue Getemailnotification(int leadid, int accountid)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                if (leadid > 0)
                {
                    var LeadData = connection.Query<NotifyValue>("select IsNotifyemails,IsNotifyText from CRM.Leads where LeadId=@leadid", new { leadid }).FirstOrDefault();
                    return LeadData;
                }
                else if (accountid > 0)
                {
                    var AccountData = connection.Query<NotifyValue>("select IsNotifyemails,IsNotifyText from CRM.Accounts where AccountId=@accountid", new { accountid }).FirstOrDefault();
                    return AccountData;
                }
            }
            return null;
        }
    }

}
