﻿using HFC.CRM.Core;
using HFC.CRM.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;


namespace HFC.CRM.Managers
{
    public class OAuthManager
    {
        private static readonly string clientId = AppConfigManager.GetPICUrlclientId;
        private static readonly string clientpwd = AppConfigManager.GetPICUrlclientpwd;
        private static readonly string url = AppConfigManager.PICUrl;



        public PICAccessToken GetAccessToken()
        {
            string url = AppConfigManager.PICUrl;
            url = url + "/get_token";
            var request = WebRequest.Create(url);
            request.Method = "POST";

            string data = "grant_type=client_credentials&client_id=" + clientId + "&client_secret=" + clientpwd;
            request.ContentType = "application/x-www-form-urlencoded";
            byte[] dataStream = Encoding.UTF8.GetBytes(data);
            request.ContentLength = dataStream.Length;
            Stream newStream = request.GetRequestStream();

            newStream.Write(dataStream, 0, dataStream.Length);
            newStream.Close();

            WebResponse response = request.GetResponse();
            using (var reader = new StreamReader(response.GetResponseStream()))
            {
                string result = reader.ReadToEnd();
                var accessToken = JsonConvert.DeserializeObject<AccessToken>(result);
                var picToken = new PICAccessToken();
                picToken.token = accessToken;
                picToken.expires_at = DateTime.Now.AddHours(5);
                return picToken;
            }
         
           
        }
       
    }
  
}
