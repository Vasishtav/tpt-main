﻿using Dapper;
using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using System;
using System.Data.SqlClient;
using System.Linq;

namespace HFC.CRM.Managers
{
    public class TerritoryManager
    {
        public static string GetTerritoryDetails(string Zipcode, string Country)
        {
            if (Zipcode.Contains('-'))
            {
                var str = Zipcode.Trim().Split('-');
                if (str.Count() > 0)
                {
                    Zipcode = str[0];
                }
            }

            if (Country.ToLower() == "us")
                Zipcode = Zipcode.Substring(0, 5);

            var BrandId = SessionManager.BrandId;

            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                string retId = (string)con.ExecuteScalar("select CRM.fnGetTerritory_Id(@BrandId,@Zipcode,@Country,@FranchiseId)", new { BrandId, Zipcode, Country, SessionManager.CurrentFranchise.FranchiseId });
                var ret = (string)con.ExecuteScalar("select CRM.fnGetTerritory(@BrandId,@Zipcode,@Country,@FranchiseId)", new { BrandId, Zipcode, Country, SessionManager.CurrentFranchise.FranchiseId });
                if (retId != null && retId != "")
                    return retId + ";" + ret;
                else
                    return "0;" + ret;
            }
        }
    }
}
