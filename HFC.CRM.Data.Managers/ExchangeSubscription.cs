﻿using HFC.CRM.Core;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using Microsoft.Exchange.WebServices.Autodiscover;
using Microsoft.Exchange.WebServices.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// The Managers namespace.
/// </summary>
namespace HFC.CRM.Managers
{
    /// <summary>
    /// Class ExchangeSubscriptionManager.
    /// </summary>
    public class ExchangeSubscriptionManager
    {
        /// <summary>
        /// Class SubscriptionTracker.
        /// </summary>
        private class SubscriptionTracker
        {
            /// <summary>
            /// Also known as email address
            /// </summary>
            /// <value>The SMTP address.</value>
            public string SmtpAddress { get; set; }
            /// <summary>
            /// Gets or sets the service.
            /// </summary>
            /// <value>The service.</value>
            public ExchangeService Service { get; set; }
            /// <summary>
            /// Gets or sets the subscription.
            /// </summary>
            /// <value>The subscription.</value>
            public ExchangeSubscription Subscription { get; set; }
        }

        /// <summary>
        /// The CRMDB context
        /// </summary>
        private CRMContext CRMDBContext = new CRMContextEx();
        /// <summary>
        /// Exchange Server Uri
        /// </summary>
        /// <value>The service URI.</value>
        public static Uri ServiceUri { get { return new Uri(AppConfigManager.GetConfig<string>("ServiceUrl", "Exchange", "EWS")); } }

        /// <summary>
        /// Username of the primary account with impersonation access
        /// </summary>
        /// <value>The service username.</value>
        public static string ServiceUsername { get { return AppConfigManager.GetConfig<string>("Username", "Exchange", "EWS"); } }
        /// <summary>
        /// Password of the primary account with impersonation access
        /// </summary>
        /// <value>The service password.</value>
        public static string ServicePassword { get { return AppConfigManager.GetConfig<string>("Password", "Exchange", "EWS"); } }

        /// <summary>
        /// Gets the service version.
        /// </summary>
        /// <value>The service version.</value>
        public static ExchangeVersion ServiceVersion { get { return ExchangeVersion.Exchange2010_SP1; } }

        /// <summary>
        /// Gets a value indicating whether [impersonation is enabled].
        /// </summary>
        /// <value><c>true</c> if [impersonation is enabled]; otherwise, <c>false</c>.</value>
        public static bool ImpersonationIsEnabled { get { return AppConfigManager.GetConfig<bool>("ImpersonationIsEnabled", "Exchange", "Synchronize"); } }

        /// <summary>
        /// Gets or sets the subscription trackers.
        /// </summary>
        /// <value>The subscription trackers.</value>
        private List<SubscriptionTracker> SubscriptionTrackers { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExchangeSubscriptionManager"/> class.
        /// </summary>
        public ExchangeSubscriptionManager()
        {
            //auto accepts unsigned ssl 
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

            SubscriptionTrackers = new List<SubscriptionTracker>();
        }

        /// <summary>
        /// Impersonate one user at a time and without using the autodiscovery method to find the proper url for the userSmtp,
        /// and copy the provided url to the usersmtp.
        /// </summary>
        /// <param name="userSmtp">user smtp</param>
        /// <param name="tracker">The tracker.</param>
        /// <param name="enableTrace">to enable logging from the XML tracing</param>
        /// <returns>System.String.</returns>
        private string ImpersonateUser(string userSmtp, out SubscriptionTracker tracker, bool enableTrace = false)
        {
            tracker = null;

            try
            {
                if (SubscriptionTrackers.Any(a => string.Equals(a.SmtpAddress, userSmtp, StringComparison.InvariantCultureIgnoreCase))) //already exists so we can just return true                
                    return "Subscription already exists for this user";
                                
                var service = new ExchangeService(ServiceVersion, TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time"))
                {
                    Credentials = new NetworkCredential(ServiceUsername, ServicePassword),
                    Url = ServiceUri
                };
                
                service.TraceEnabled = enableTrace;
                service.ImpersonatedUserId = new ImpersonatedUserId(ConnectingIdType.SmtpAddress, userSmtp);

                try
                {
                    //this will validate the user
                    RuleCollection rulecoll = service.GetInboxRules();
                }
                catch (Exception ex)
                {
                    int hr = System.Runtime.InteropServices.Marshal.GetHRForException(ex);

                    if (hr == -2146233088) // We do not have right to impersonate this user.
                    {
                        return "Validation failed, service user does not have impersonation rights.";
                    }
                    else
                    {
                        var autoDiscoverService = new AutodiscoverService(ServiceVersion)
                        {
                            Credentials = new NetworkCredential(ServiceUsername, ServicePassword)
                        };
                        var response = autoDiscoverService.GetUserSettings(userSmtp, UserSettingName.AutoDiscoverSMTPAddress);
                        return ImpersonateUser(response.SmtpAddress, out tracker, enableTrace);
                    } 
                }

                tracker = new SubscriptionTracker
                {
                    SmtpAddress = userSmtp,
                    Service = service
                };
                SubscriptionTrackers.Add(tracker);
                return "Success";
            }
            catch (Exception ex)
            {
                return ApiEventLogger.LogEvent(ex);
            }
        }

        /// <summary>
        /// Closes subscription stream connection
        /// </summary>
        /// <param name="smtpAddress">If provided then it will only close the connection for this address, otherwise all connection will be closed</param>
        public void Disconnect(string smtpAddress = null)
        {
            if (!string.IsNullOrEmpty(smtpAddress))
            {
                var sub = SubscriptionTrackers.FirstOrDefault(f => string.Equals(f.SmtpAddress, smtpAddress, StringComparison.InvariantCultureIgnoreCase));
                if (sub != null)
                    sub.Subscription.CloseConnection();
            }
            else
            {
                SubscriptionTrackers.ForEach(f => f.Subscription.CloseConnection());
            }
        }

        /// <summary>
        /// To Impersonate users in order to get the info from them.
        /// </summary>
        public void SubscribeUsers()
        {
            if (!ExchangeManager.IsEnabled)
                return;

            //get any user that is active, with sync start date less than now, is either not already connected or the connection has already expired
            var linq = (from u in CRMDBContext.Users
                        from t in u.Sync_Trackers.Where(w => w.ProviderName == ExchangeManager.ProviderName).DefaultIfEmpty()
                        where !u.IsDeleted && u.SyncStartsOnUtc.HasValue && u.SyncStartsOnUtc.Value < DateTime.Now &&
                            u.Domain == ExchangeManager.Domain &&
                            (t == null || (t.ErrorCount < ExchangeManager.ErrorRetryCount && (t.IsConnected == false || t.ExpiresOnUtc < DateTime.Now)))
                        select new { u.UserId, u.Email, Tracker = t }).ToList();
                                
            foreach (var user in linq)
            {
                try
                {
                    string status = null;

                    var subTracker = SubscriptionTrackers.FirstOrDefault(f => string.Equals(user.Email, f.SmtpAddress, StringComparison.InvariantCultureIgnoreCase));
                    if (subTracker == null)
                    {
                        if (ImpersonationIsEnabled)
                        {
                            status = ImpersonateUser(user.Email, out subTracker);
                        }
                        else
                        {
                            subTracker = new SubscriptionTracker
                            {
                                SmtpAddress = user.Email,
                                Service = new ExchangeService(ServiceVersion, TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time"))
                                {
                                    Credentials = new NetworkCredential(ServiceUsername, ServicePassword),
                                    Url = ServiceUri
                                }
                            };
                            SubscriptionTrackers.Add(subTracker);
                        }
                    }
                    else if (subTracker.Subscription != null)
                    {
                        //connection is closed so reconnection and continue to next user
                        //or connection is already open, then just continue to next user
                        if (!subTracker.Subscription.IsOpen)
                            subTracker.Subscription.OpenConnection(); //reconnect
                        continue;
                    }

                    var tracker = user.Tracker;

                    if (tracker == null)
                    {
                        tracker = new Sync_Tracker
                        {
                            UserId = user.UserId,
                            ProviderName = ExchangeManager.ProviderName
                        };
                        CRMDBContext.Sync_Tracker.Add(tracker);
                    }

                    if (subTracker == null)
                    {
                        tracker.ErrorCount++;
                        tracker.LastErrorDateUtc = DateTime.Now;
                        tracker.LastErrorMessage = status ?? "Unable to subscribe user to stream notification";

                        CRMDBContext.SaveChanges();
                    }
                    else
                    {
                        var service = subTracker.Service;
                        try
                        {
                            var folderId = new FolderId(WellKnownFolderName.Calendar, new Mailbox(user.Email));
                            string syncState = tracker.LastSyncState;

                            if (string.IsNullOrEmpty(syncState)) //requires a first time item sync so we are caught up
                            {
                                try
                                {
                                    var hasChanges = true;
                                    do
                                    {
                                        var icc = service.SyncFolderItems(folderId, PropertySet.IdOnly, null, 500, SyncFolderItemsScope.NormalItems, syncState);
                                        syncState = icc.SyncState;
                                        hasChanges = icc.MoreChangesAvailable;
                                    }
                                    while (hasChanges);
                                }
                                catch (Exception loopex)
                                {
                                    ApiEventLogger.LogEvent(loopex, user.Email);
                                }
                            }

                            tracker.LastSyncState = syncState;
                            subTracker.Subscription = new ExchangeSubscription(service, user.Email, tracker);
                            subTracker.Subscription.OpenConnection();
                        }
                        catch (Exception ex)
                        {
                            tracker.LastErrorMessage = ex.Message;
                            tracker.LastErrorDateUtc = DateTime.Now;
                            tracker.ErrorCount++;

                            CRMDBContext.SaveChanges();
                        }
                    }
                }
                catch (Exception exx)
                {
                    ApiEventLogger.LogEvent(exx);
                }
            }

        }

    }
    /// <summary>
    /// Class ExchangeSubscription.
    /// </summary>
    public class ExchangeSubscription
    {
        /// <summary>
        /// Gets a value indicating whether this instance is open.
        /// </summary>
        /// <value><c>true</c> if this instance is open; otherwise, <c>false</c>.</value>
        public bool IsOpen 
        { 
            get 
            {
                if (Connection != null)
                    return Connection.IsOpen;
                else
                    return false;
            } 
        }

        /// <summary>
        /// Gets or sets the connection.
        /// </summary>
        /// <value>The connection.</value>
        private StreamingSubscriptionConnection Connection { get; set; }
        /// <summary>
        /// Gets the stream.
        /// </summary>
        /// <value>The stream.</value>
        public StreamingSubscription Stream { get; private set; }

        /// <summary>
        /// Gets the push_ TTL.
        /// </summary>
        /// <value>The push_ TTL.</value>
        public int Push_TTL
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the SMTP address.
        /// </summary>
        /// <value>The SMTP address.</value>
        public string SMTPAddress { get; set; }

        /// <summary>
        /// Gets the tracker.
        /// </summary>
        /// <value>The tracker.</value>
        public Sync_Tracker Tracker { get; private set; }

        /// <summary>
        /// Gets or sets the ews service.
        /// </summary>
        /// <value>The ews service.</value>
        private ExchangeService EWSService { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExchangeSubscription"/> class.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="smtpAddress">The SMTP address.</param>
        /// <param name="tracker">The tracker.</param>
        /// <param name="overridingTTL">The overriding TTL.</param>
        public ExchangeSubscription(ExchangeService service, string smtpAddress, Sync_Tracker tracker, int? overridingTTL = null)
        {
            //auto accepts unsigned ssl 
            //ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
            
            //service = new ExchangeService(ExchangeVersion.Exchange2010_SP1, TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time"))
            //{
            //    Credentials = new NetworkCredential(ExchangeSubscriptionManager.ServiceUsername, ExchangeSubscriptionManager.ServicePassword),
            //    Url = ExchangeSubscriptionManager.ServiceUri
            //};

            if (overridingTTL.HasValue && overridingTTL.Value > 0)
                Push_TTL = overridingTTL.Value;
            else
                Push_TTL = AppConfigManager.GetConfig<int>("Push_TTL", "Exchange", "Synchronize");

            SMTPAddress = smtpAddress;
            EWSService = service;

            Connection = new StreamingSubscriptionConnection(service, Push_TTL);

            Connection.OnNotificationEvent += connection_OnNotificationEvent;
            Connection.OnSubscriptionError += connection_OnSubscriptionError;
            Connection.OnDisconnect += connection_OnDisconnect;
            
            Tracker = tracker;
        }              
        
        ///// <param name="watermark">An optional watermark representing a previously opened subscription</param>
        ///// <param name="frequency">The frequency, in minutes, at which Exchange server should contact the Web service endpoint. Frequency must be between 1 and 1440</param>
        //[Obsolete("Use Streaming Notification instead")]
        //public string SubscribeToPushNotifications(out string subscriptionId, string mailboxEmail, string watermark = null, string callerData = null, int frequency = 1)
        //{
        //    PushSubscription subscription = service.SubscribeToPushNotifications(
        //        new FolderId[] { new FolderId(WellKnownFolderName.Calendar, new Mailbox(mailboxEmail)) },
        //        NotificationServiceUri,
        //        frequency,
        //        watermark,
        //        callerData,
        //        EventType.Created, EventType.Deleted, EventType.Modified, EventType.Moved);
            
        //    subscriptionId = subscription.Id;
        //    return subscription.Watermark;
        //}

        //private GetUserSettingsResponse GetUserSettings(string emailAddress, int maxHops = 4)
        //{
        //    Uri url = null;
        //    GetUserSettingsResponse response = null;

        //    for (int attempt = 0; attempt < maxHops; attempt++)
        //    {
        //        service.Url = url;
        //        service.EnableScpLookup = (attempt < 2);

        //        response = autoDiscoverService.GetUserSettings(emailAddress, UserSettingName.ExternalEwsUrl, UserSettingName.AutoDiscoverSMTPAddress);

        //        if (response.ErrorCode == AutodiscoverErrorCode.RedirectAddress)
        //        {
        //            url = new Uri(response.RedirectTarget);
        //        }
        //        else if (response.ErrorCode == AutodiscoverErrorCode.RedirectUrl)
        //        {
        //            url = new Uri(response.RedirectTarget);
        //        }
        //        else
        //        {
        //            return response;
        //        }
        //    }

        //    throw new Exception("No suitable Autodiscover endpoint was found.");
        //}

        //public void GetMailboxes()
        //{
        //    using (var db = new CRM.Data.CRMContext())
        //    {
        //        List<GetUserSettingsResponse> responses = new List<GetUserSettingsResponse>();
        //        var users = db.Users.Where(w => w.Domain != null && w.Domain.Length > 0).ToList();
        //        foreach (var user in users)
        //        {
        //            try
        //            {
        //                responses.Add(GetUserSettings(user.Email));
        //            }
        //            catch { }
        //        }
        //    }
        //}

        /// <summary>
        /// Opens the connection.
        /// </summary>
        public void OpenConnection()
        {
            if (!Connection.IsOpen)
            {
                try
                {
                    //it seems that the subscription gets removed when connection is closed, so we have to recreate it before opening new connection
                    Stream = EWSService.SubscribeToStreamingNotifications(
                            new FolderId[] { new FolderId(WellKnownFolderName.Calendar, new Mailbox(SMTPAddress)) },
                            EventType.Created,
                            EventType.Deleted,
                            EventType.Modified
                        );

                    Connection.AddSubscription(Stream);
                    Connection.Open();

                    using (var CRM = new CRMContextEx())
                    {
                        if (Tracker.SyncRecId <= 0)
                            CRM.Sync_Tracker.Add(Tracker);
                        else
                            CRM.Sync_Tracker.Attach(Tracker);

                        Tracker.IsConnected = true;
                        Tracker.LastSyncDateUtc = DateTime.Now;
                        Tracker.ResourceId = Stream.Id;
                        Tracker.ExpiresOnUtc = DateTime.Now.AddMinutes(Push_TTL);
                        Tracker.ErrorCount = 0;
                        Tracker.LastErrorDateUtc = null;
                        Tracker.LastErrorMessage = null;
                        CRM.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    ApiEventLogger.LogEvent(ex);
                    if(Connection.IsOpen)
                        Connection.Close();
                }
            }
        }

        /// <summary>
        /// Closes the connection.
        /// </summary>
        public void CloseConnection()
        {
            if (Connection.IsOpen)
                Connection.Close();
        }

        /// <summary>
        /// Handles the OnDisconnect event of the connection control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="args">The <see cref="SubscriptionErrorEventArgs"/> instance containing the event data.</param>
        void connection_OnDisconnect(object sender, SubscriptionErrorEventArgs args)
        {
            //StreamingSubscriptionConnection connection = (StreamingSubscriptionConnection)sender;
            
            try
            {
                using (var CRMDBContext = new CRMContextEx())
                {
                    Tracker.IsConnected = true;
                    if (Tracker.SyncRecId <= 0)
                        CRMDBContext.Sync_Tracker.Add(Tracker);
                    else
                        CRMDBContext.Sync_Tracker.Attach(Tracker);

                    Tracker.IsConnected = false;
                    CRMDBContext.SaveChanges();
                }
            }
            catch (Exception Ex)
            {
                ApiEventLogger.LogEvent(Ex);
            }
        }

        /// <summary>
        /// Handles the OnSubscriptionError event of the connection control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="args">The <see cref="SubscriptionErrorEventArgs"/> instance containing the event data.</param>
        void connection_OnSubscriptionError(object sender, SubscriptionErrorEventArgs args)
        {            
            ApiEventLogger.LogEvent(args.Exception, args.Subscription.Id);
        }

        /// <summary>
        /// Handles the OnNotificationEvent event of the connection control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="args">The <see cref="NotificationEventArgs"/> instance containing the event data.</param>
        void connection_OnNotificationEvent(object sender, NotificationEventArgs args)
        {
            StreamingSubscription subscription = args.Subscription;
            
            if (Tracker != null && Tracker.UserId != Guid.Empty)
            {
                ExchangeManager mgr = new ExchangeManager(Tracker.UserId);
                mgr.SyncChannel_Down();
            }
            
        }

    }
}
