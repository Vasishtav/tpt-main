﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HFC.CRM.Core.Logs;
using HFC.CRM.Core;
using System.Web;
using HFC.CRM.Core.Common;
using System.Xml.Linq;
using System.Data.Entity;
using HFC.CRM.Core.Membership;
using HFC.CRM.Data.Constants;
using HFC.CRM.Data;
using System.Data.SqlClient;
using Dapper;

/// <summary>
/// The Managers namespace.
/// </summary>
namespace HFC.CRM.Managers
{
    using HFC.CRM.Data.Context;

    /// <summary>
    /// Class TaskManager.
    /// </summary>
    public class TaskManager : IMessageConstants
    {
        /// <summary>
        /// The CRMDB context
        /// </summary>
        private CRMContext CRMDBContext = ContextFactory.Current.Context;

        /// <summary>
        /// Gets the user.
        /// </summary>
        /// <value>The user.</value>
        public User User { get; private set; }
        /// <summary>
        /// Gets the authorizing user.
        /// </summary>
        /// <value>The authorizing user.</value>
        public User AuthorizingUser { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TaskManager"/> class.
        /// </summary>
        /// <param name="user">The user.</param>
        public TaskManager(User user) : this(user, user)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TaskManager"/> class.
        /// </summary>
        /// <param name="authorizingUser">The authorizing user.</param>
        /// <param name="user">The user.</param>
        /// <exception cref="System.ArgumentOutOfRangeException">User must belong to a franchise</exception>
        public TaskManager(User authorizingUser, User user)
        {
            this.User = user;
            this.AuthorizingUser = authorizingUser;
            if (!this.User.FranchiseId.HasValue || this.User.FranchiseId.Value <= 0)
                throw new ArgumentOutOfRangeException("User must belong to a franchise");
        }

        /// <summary>
        /// Gets list of tasks sorted by asc priority order then by desc created on utc date
        /// </summary>
        /// <param name="totalRecords">The total records.</param>
        /// <param name="organizerPersonIds">The organizer person ids.</param>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="dueDateStart">The due date start.</param>
        /// <param name="dueDateEnd">The due date end.</param>
        /// <param name="createdOnUtcStart">Optional filter for createdOnUtc start range</param>
        /// <param name="createdOnUtcEnd">Optional filter for createdOnUtc end range</param>
        /// <returns>List&lt;Task&gt;.</returns>
        public List<Task> GetTasks(
            out int totalRecords,
            List<int> assignedPersonIds,
            int? pageIndex = null,
            int pageSize = 25,
            DateTime? dueDateStart = null,
            DateTime? dueDateEnd = null,
            DateTimeOffset? createdOnUtcStart = null,
            DateTimeOffset? createdOnUtcEnd = null,
            int TaskId = 0,
            string PersonIds = null)
        {
            return _GetTasks(out totalRecords,
                assignedPersonIds,
                pageIndex,
                pageSize,
                dueDateStart,
                dueDateEnd,
                createdOnUtcStart,
                createdOnUtcEnd, TaskId: TaskId, PersonIds: PersonIds);
        }

        /// <summary>
        /// _s the get tasks.
        /// </summary>
        /// <param name="totalRecords">The total records.</param>
        /// <param name="organizerPersonIds">The organizer person ids.</param>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="dueDateStart">The due date start.</param>
        /// <param name="dueDateEnd">The due date end.</param>
        /// <param name="createdOnUtcStart">The created on UTC start.</param>
        /// <param name="createdOnUtcEnd">The created on UTC end.</param>
        /// <param name="jobIds">The job ids.</param>
        /// <returns>List&lt;Task&gt;.</returns>
        /// <exception cref="System.UnauthorizedAccessException"></exception>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// Job Ids are invalid
        /// or
        /// Invalid organizer ids
        /// </exception>
        private List<Task> _GetTasks(
            out int totalRecords,
            List<int> assignedPersonIds = null,
            int? pageIndex = null,
            int pageSize = 25,
            DateTime? dueDateStart = null,
            DateTime? dueDateEnd = null,
            DateTimeOffset? createdOnUtcStart = null,
            DateTimeOffset? createdOnUtcEnd = null,
            List<int> jobIds = null,
            int TaskId = 0,
            string PersonIds = null)
        {
            totalRecords = 0;

            try
            {
                //var linq = from t in CRMDBContext.Tasks
                //           select t;


                List<Task> result = TasksList(SessionManager.CurrentFranchise.FranchiseId, null, dueDateStart, dueDateEnd, TaskId, PersonIds);

                if (result != null && result.Count > 0 && TaskId > 0)
                {
                    foreach (var task in result)
                    {
                        //bool iseditable = false, isdeletable = false;
                        //if its assigned to the the current user then its editable for them
                        // TODO: is this required in TP???
                        //iseditable = true; //AssignedPermission.CanUpdate && task.OrganizerPersonId == User.PersonId;
                        //isdeletable = true; // AssignedPermission.CanDelete && task.OrganizerPersonId == User.PersonId;

                        //or if the current user is in any of the additional user, only allow edit if they are in the additional ppl, don't let them delete, only the person assigned can delete
                        //if (!iseditable)
                        //  iseditable = true; //AssignedPermission.CanUpdate && task.Attendees.Any(a => a.PersonId == User.PersonId);

                        //mask subject and desc if its private unless they are the organizer or part of attendee
                        //if (task.IsPrivate.HasValue && task.IsPrivate.Value && !task.Attendees.Any(a => a.PersonId == User.PersonId))
                        //{
                        //    //task.Subject = "Busy";
                        //    //task.Message = null;
                        //}

                        //lastly use base permission if there is they are still not approved
                        // TODO: is this required in TP???
                        //if (!iseditable)
                        //    iseditable = true; // BasePermission.CanUpdate; //permission for calender lets them edit 
                        //if (!isdeletable)
                        //    isdeletable = true; // BasePermission.CanDelete;

                        //var person = CacheManager.UserCollection.FirstOrDefault(f => f.PersonId == task.AssignedPersonId);
                        var eventpeople = CRMDBContext.EventToPeople.Where(e => e.TaskId == task.TaskId).ToList();
                        task.Attendees = eventpeople;
                        //if (person != null)
                        //    task.ColorClassName = string.Format("color-{0}", person.ColorId);
                        //else
                        //{
                        //    var personOrgniser = CacheManager.UserCollection.FirstOrDefault(f => f.PersonId == task.OrganizerPersonId);
                        //    if (personOrgniser != null)
                        //        task.ColorClassName = string.Format("color-{0}", personOrgniser.ColorId);
                        //    else
                        //        task.ColorClassName = "color-none";
                        //}

                        //task.IsEditable = iseditable;
                        //task.IsDeletable = isdeletable;
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                //rethrow error after logging it.
                EventLogger.LogEvent(ex);
                throw;
            }


        }
        public List<Task> TasksList(int? FranchiseId, int? PersonId, DateTime? StartDate, DateTime? EndDate, int TaskId = 0, string PersonIds = "")
        {
            //if (PersonIds == "" || PersonIds == null)
            //{
            //    PersonIds = Convert.ToString(PersonId.ToString());
            //    PersonIds = SessionManager.CurrentUser.PersonId.ToString();
            //}
            //string query = @"SELECT 
            //         [Extent2].[PersonId] AS [PersonId],
            //        [Extent1].[TaskId] AS [TaskId], 
            //        [Extent1].[Subject] AS [Subject], 
            //        [Extent1].[Message] AS [Message], 
            //        [Extent1].[CreatedOnUtc] AS [CreatedOnUtc], 
            //        [Extent1].[LeadId] AS [LeadId], 
            //        [Extent1].[AccountId] AS [AccountId],
            //        [Extent1].[OpportunityId] AS [OpportunityId],
            //        [Extent1].[OrderId] AS [OrderId],
            //        [Extent1].CaseId as [CaseId],
            //        [Extent1].[CreatedByPersonId] AS [CreatedByPersonId], 
            //        [Extent1].[IsDeleted] AS [IsDeleted], 
            //        [Extent1].[LastUpdatedOnUtc] AS [LastUpdatedOnUtc], 
            //        [Extent1].[LastUpdatedPersonId] AS [LastUpdatedPersonId], 
            //        [Extent1].[PriorityOrder] AS [PriorityOrder], 
            //        [Extent1].[LeadNumber] AS [LeadNumber], 
            //        [Extent1].[TaskGuid] AS [TaskGuid], 
            //        [Extent1].[FranchiseId] AS [FranchiseId], 
            //        [Extent1].[IsPrivate] AS [IsPrivate], 
            //        [Extent1].[DueDate] AS [DueDate], 
            //        [Extent1].[OrganizerPersonId] AS [OrganizerPersonId], 
            //        [Extent1].[OrganizerEmail] AS [OrganizerEmail], 
            //        [Extent1].[ReminderMinute] AS [ReminderMinute], 
            //        [Extent1].[RemindMethodEnum] AS [RemindMethodEnum], 
            //        [Extent1].[CompletedDate] AS [CompletedDate], 
            //        [Extent1].[FirstRemindDate] AS [FirstRemindDate], 
            //        [Extent1].[RevisionSequence] AS [RevisionSequence], 
            //        [Extent1].[JobId] AS [JobId], 
            //        [Extent1].[JobNumber] AS [JobNumber], 
            //        [Extent1].[AssignedPersonId] AS [AssignedPersonId], 
            //        [Extent1].[AssignedName] AS [AssignedName], 
            //        [Extent1].[PhoneNumber] AS [PhoneNumber]
            //        FROM  [CRM].[Tasks] AS [Extent1]
            //        INNER JOIN [CRM].[EventToPeople] AS [Extent2] ON [Extent1].[TaskId] = [Extent2].[TaskId]
            //        WHERE (0 = [Extent1].[IsDeleted]) AND ([Extent2].[PersonId] IS NOT NULL) AND ([Extent2].[PersonId] IN (@PersionIds)) AND ([Extent2].[PersonId] IS NOT NULL)
            //        ";
            //query = query.Replace("@PersionIds", PersonIds);

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var CalendarTasks = connection.Query<Task>("exec CRM.spGETTaskData @FranchiseId,@TaskId,@PersonIds,@StartDate,@EndDate",
                    new { FranchiseId, TaskId, PersonIds, StartDate, EndDate }).ToList();
                connection.Close();
                return CalendarTasks;
            }

        }

        /// <summary>
        /// Deletes the tasks.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>System.String.</returns>
        public string DeleteTasks(int id)
        {
            return DeleteTasks(new List<int> { id });
        }

        /// <summary>
        /// Deletes the tasks.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <returns>System.String.</returns>
        public string DeleteTasks(List<int> ids)
        {
            // TODO: is this required in TP???
            //if (!BasePermission.CanDelete)
            //    return Unauthorized;

            if (ids.Count == 0)
                return DataCannotBeNullOrEmpty;

            try
            {
                //select distinct
                ids = ids.Distinct().ToList();

                var taskList = CRMDBContext.Tasks.Where(f => f.IsDeleted == false && ids.Contains(f.TaskId)).ToList();

                if (taskList == null || taskList.Count == 0)
                    return TaskDoesNotExist;

                // TODO: is this required in TP???
                ////check permission
                //foreach (var task in taskList)
                //{
                //    if ((AssignedPermission.CanDelete && task.OrganizerPersonId == User.PersonId) || BasePermission.CanDelete)
                //        continue;
                //    else
                //        return Unauthorized;
                //}

                List<Lead> attachedLeads = new List<Lead>();
                foreach (var task in taskList)
                {
                    task.IsDeleted = true;
                    task.LastUpdatedOnUtc = DateTime.Now;
                    task.LastUpdatedPersonId = User.PersonId;
                    //log to lead history if there is a lead attached
                    if (task.LeadId > 0)
                    {
                        var leadToLog = attachedLeads.FirstOrDefault(f => f.LeadId == task.LeadId.Value);
                        if (leadToLog == null)
                        {
                            leadToLog = new Lead { LeadId = task.LeadId.Value };
                            CRMDBContext.Leads.Attach(leadToLog);
                            attachedLeads.Add(leadToLog);
                        }
                        XElement change =
                            new XElement("Task", new XAttribute("id", task.TaskId), new XAttribute("operation", "delete"),
                                new XElement("Subject", new XElement("Original", task.Subject)),
                                new XElement("Message", new XElement("Original", task.Message))
                            );
                    }
                }
                //Set Delete from Kendo Scheduler
                if (!taskList.SingleOrDefault().IsDeleted)
                {
                    taskList.SingleOrDefault().IsDeleted = true;
                    taskList.SingleOrDefault().LastUpdatedOnUtc = DateTime.Now;
                    taskList.SingleOrDefault().LastUpdatedPersonId = User.PersonId;
                }
                if (CRMDBContext.SaveChanges() > 0)
                    return Success;
                else
                    return FailedSaveChanges;

            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }

        }

        /// <summary>
        /// Marks the complete.
        /// </summary>
        /// <param name="taskId">The task identifier.</param>
        /// <param name="isComplete">if set to <c>true</c> [is complete].</param>
        /// <returns>System.String.</returns>
        public string MarkComplete(int taskId, bool isComplete)
        {
            if (taskId <= 0)
                return DataCannotBeNullOrEmpty;

            try
            {
                var task = CRMDBContext.Tasks.FirstOrDefault(f => f.IsDeleted == false && f.TaskId == taskId);
                if (task == null)
                    return TaskDoesNotExist;

                // TODO: is this required in TP???
                //if ((AssignedPermission.CanUpdate && task.OrganizerPersonId == User.PersonId) || BasePermission.CanUpdate)
                //{
                //}
                //else
                //    return Unauthorized;

                if (isComplete)
                    task.CompletedDate = DateTimeOffset.Now;
                else
                    task.CompletedDate = null;

                //log to lead history if there is a lead attached
                if (task.LeadId > 0)
                {
                    var entry = CRMDBContext.Entry(task);
                    var changes = Util.GetEFChangedProperties(entry, task);

                    if (changes != null)
                    {
                        var leadToLog = new Lead() { LeadId = task.LeadId.Value };
                        CRMDBContext.Leads.Attach(leadToLog);
                    }
                }

                task.LastUpdatedOnUtc = DateTime.Now;
                task.LastUpdatedPersonId = User.PersonId;
                if (CRMDBContext.SaveChanges() > 0)
                    return Success;
                else
                    return FailedSaveChanges;

            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        /// <summary>
        /// Deletes all completed tasks for the current user
        /// </summary>
        /// <returns>System.String.</returns>
        public string ClearCompleted()
        {
            // TODO: is this required in TP???
            //if (AssignedPermission.CanDelete || BasePermission.CanDelete)
            //{
            //     //nothing to delete so return success                
            //}
            //else
            //    return Unauthorized;

            var taskIdsToDelete = CRMDBContext.Tasks.Where(w => !w.IsDeleted && w.CompletedDate.HasValue && w.OrganizerPersonId == User.PersonId)
                    .Select(s => s.TaskId).ToList();
            if (taskIdsToDelete != null && taskIdsToDelete.Count > 0)
                return DeleteTasks(taskIdsToDelete);
            else
                return Success;
        }

        /// <summary>
        /// Quick update for due date
        /// </summary>
        /// <param name="taskId">The task identifier.</param>
        /// <param name="dayDelta">The day delta.</param>
        /// <param name="minuteDelta">The minute delta.</param>
        /// <returns>System.String.</returns>
        public string QuickUpdate(int taskId, int dayDelta, int minuteDelta)
        {
            if (taskId <= 0)
                return DataCannotBeNullOrEmpty;
            if (dayDelta == 0 && minuteDelta == 0)
                return Success;

            try
            {
                var origtask = CRMDBContext.Tasks.FirstOrDefault(f => !f.IsDeleted && f.TaskId == taskId);
                if (origtask == null)
                    return TaskDoesNotExist;

                // TODO: is this required in TP???
                //if ((AssignedPermission.CanUpdate && origtask.OrganizerPersonId == User.PersonId) || BasePermission.CanUpdate)
                //{
                //}
                //else
                //    return Unauthorized;

                if (origtask.DueDate.HasValue)
                {
                    origtask.DueDate = origtask.DueDate.Value.AddDays(dayDelta).AddMinutes(minuteDelta);
                    if (origtask.ReminderMinute > 0 && origtask.RemindMethodEnum.HasValue && origtask.FirstRemindDate.HasValue)
                        origtask.FirstRemindDate = origtask.FirstRemindDate.Value.AddDays(dayDelta).AddMinutes(minuteDelta);

                    if (CRMDBContext.SaveChanges() > 0)
                        return Success;
                    else
                        return FailedSaveChanges;
                }
                else
                    return "Task does not have a due date to update";
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);

            }

        }

        /// <summary>
        /// Creates the specified task identifier.
        /// </summary>
        /// <param name="taskId">The task identifier.</param>
        /// <param name="data">The data.</param>
        /// <returns>System.String.</returns>
        public string Create(out int taskId, Task data, out string warning)
        {
            warning = null;

            taskId = 0;

            DateTime DueDate = ((DateTimeOffset)data.DueDate).Date;
            //if (!BasePermission.CanCreate)
            //    return Unauthorized;
            if (data == null)
                return DataCannotBeNullOrEmpty;
            if (string.IsNullOrEmpty(data.Subject))
                return EntityRequiresSubject;
            if ((!data.AssignedPersonId.HasValue || data.AssignedPersonId <= 0))
                return EntityRequiresAssigner;
            if (data.Attendees == null || data.Attendees.Count == 0)
                return EntityRequiresAttendee;

            if (data.FranchiseId <= 0)
                data.FranchiseId = User.FranchiseId.Value;
            if (data.CreatedByPersonId <= 0)
                data.CreatedByPersonId = User.PersonId;

            data.Subject = HttpContext.Current.Server.UrlDecode(data.Subject);
            if (data.Message != null)
                data.Message = HttpContext.Current.Server.UrlDecode(data.Message);

            data.TaskGuid = Guid.NewGuid();

            if (data.LeadId <= 0)
            {
                data.LeadId = null;
                data.LeadNumber = null;
            }
            if (data.JobId <= 0)
            {
                data.JobId = null;
                data.JobNumber = null;
            }
            if (data.IsPrivate == null) { data.IsPrivate = false; }
            if (data.DueDate.HasValue)
            {
                var conflictedTasks =
                    data.Attendees.Where(attendee => this.CRMDBContext.Tasks.Any(t => !t.IsDeleted && t.DueDate.HasValue && t.DueDate == data.DueDate && t.Attendees.Any(a => a.PersonId == attendee.PersonId))).
                        ToList();

                if (conflictedTasks.Any())
                {
                    warning = string.Format("This appointment conflicts with an existing appointment for {0}",
                        string.Join(", ", conflictedTasks.Select(c => string.Format("{0} ({1})", c.PersonName, c.PersonEmail))));
                }
            }

            data.AssignedName = this.CRMDBContext.People.FirstOrDefault(f => f.PersonId == data.AssignedPersonId).FullName;

            //Task repleaced from Entity to Dapper
            try
            {

                if (data.TaskId > 0)
                {
                    using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                    {
                        connection.Open();
                        var Query = @"update [CRM].[Tasks] set 
                        [Subject]=@Subject,
                        [Message]=@Message,
                        [DueDate]=@DueDate,
                        [LeadId]=@LeadId,
                        [AssignedPersonId]=@AssignedPersonId,
						[AssignedName] =@AssignedName,
                        [AccountId]=@AccountId,
                        [OpportunityId]=@OpportunityId,
                        [OrderId]=@OrderId,
                        [CaseId]=@CaseId,
                        [VendorCaseId]=@VendorCaseId,
                        [LastUpdatedOnUtc]=@LastUpdatedOnUtc,
                        [LastUpdatedPersonId]=@LastUpdatedPersonId,
                        [IsPrivate]=@IsPrivate,
						[OrganizerEmail] =@OrganizerEmail,
                        [ReminderMinute]=@ReminderMinute 
                        where TaskId=@TaskId";

                        var result = connection.Query<int>(Query, new
                        {
                            TaskId = data.TaskId,
                            Subject = data.Subject,
                            Message = data.Message,
                            DueDate = DueDate,
                            LeadId = data.LeadId,
                            AssignedPersonId = data.AssignedPersonId,
                            AssignedName = data.AssignedName,
                            AccountId = data.AccountId,
                            OpportunityId = data.OpportunityId,
                            OrderId = data.OrderId,
                            LastUpdatedOnUtc = DateTime.UtcNow,
                            LastUpdatedPersonId = data.LastUpdatedPersonId,
                            IsPrivate = data.IsPrivate,
                            OrganizerEmail = User.Person.PrimaryEmail,
                            ReminderMinute = data.ReminderMinute,
                            CaseId = data.CaseId,
                            VendorCaseId = data.VendorCaseId
                        }).FirstOrDefault();

                        //ReAssigning Attendees to Task    
                        //Delete EventToPeople from attendees
                        Query = "Delete  from Crm.EventToPeople where TaskId=@TaskId";
                        var deleteTask = connection.Query<int>(Query, new { TaskId = data.TaskId });

                        foreach (var attende in data.Attendees)
                        {
                            attende.TaskId = data.TaskId;
                            var newEventToTaskId = connection.Insert<EventToPerson>(attende);
                        }
                        connection.Close();
                    }
                    taskId = data.TaskId;
                }
                else
                {
                    //Save using stored procedure CRM.SaveQuestionAnswer
                    using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                    {
                        connection.Open();
                        //
                        var Query = "INSERT INTO [CRM].[Tasks] ";
                        Query = Query + "(TaskGuid,Subject,Message,CreatedOnUtc,DueDate,CompletedDate ";
                        Query = Query + ",LeadId,OrganizerPersonId,OrganizerEmail,CreatedByPersonId ";
                        Query = Query + ",IsDeleted,LastUpdatedOnUtc,LastUpdatedPersonId,PriorityOrder ";
                        Query = Query + ",LeadNumber,FranchiseId,IsPrivate,ReminderMinute ";
                        Query = Query + ",RemindMethodEnum,FirstRemindDate,RevisionSequence ";
                        Query = Query + ",JobId,JobNumber,AssignedPersonId,AssignedName,PhoneNumber,AccountId,OpportunityId,OrderId,CaseId,VendorCaseId) ";
                        Query = Query + "VALUES ";
                        Query = Query + "(@TaskGuid,@Subject,@Message,@CreatedOnUtc,@DueDate,@CompletedDate ";
                        Query = Query + ",@LeadId,@OrganizerPersonId,@OrganizerEmail,@CreatedByPersonId ";
                        Query = Query + ",@IsDeleted,@LastUpdatedOnUtc,@LastUpdatedPersonId,@PriorityOrder ";
                        Query = Query + ",@LeadNumber,@FranchiseId,@IsPrivate,@ReminderMinute ";
                        Query = Query + ",@RemindMethodEnum,@FirstRemindDate,@RevisionSequence ";
                        Query = Query + ",@JobId,@JobNumber,@AssignedPersonId,@AssignedName,@PhoneNumber,@AccountId,@OpportunityId,@OrderId,@CaseId,@VendorCaseId); ";
                        Query = Query + " SELECT CAST(SCOPE_IDENTITY() AS INT)";

                        //
                        var newTaskId = connection.Query<int>(Query, new
                        {
                            TaskGuid = data.TaskGuid,
                            Subject = data.Subject,
                            Message = data.Message,
                            CreatedOnUtc = data.CreatedOnUtc,
                            DueDate = DueDate,
                            CompletedDate = data.CompletedDate,
                            LeadId = data.LeadId,
                            OrganizerPersonId = data.OrganizerPersonId,
                            OrganizerEmail = data.OrganizerEmail,
                            CreatedByPersonId = data.CreatedByPersonId,
                            IsDeleted = data.IsDeleted,
                            LastUpdatedOnUtc = DateTime.UtcNow,
                            LastUpdatedPersonId = data.LastUpdatedPersonId,
                            PriorityOrder = data.PriorityOrder,
                            LeadNumber = data.LeadNumber,
                            FranchiseId = data.FranchiseId,
                            IsPrivate = data.IsPrivate,
                            ReminderMinute = data.ReminderMinute,
                            RemindMethodEnum = data.RemindMethodEnum,
                            FirstRemindDate = data.FirstRemindDate == null ? data.FirstRemindDate : TimeZoneManager.ToUTC(DateTime.Parse(data.FirstRemindDate.ToString())),
                            RevisionSequence = data.RevisionSequence,
                            JobId = data.JobId,
                            JobNumber = data.JobNumber,
                            AssignedPersonId = data.AssignedPersonId,
                            AssignedName = data.AssignedName,
                            PhoneNumber = data.PhoneNumber,
                            AccountId = data.AccountId,
                            OpportunityId = data.OpportunityId,
                            OrderId = data.OrderId,
                            CaseId = data.CaseId,
                            VendorCaseId = data.VendorCaseId
                        }).FirstOrDefault();
                        taskId = newTaskId;
                        connection.Close();
                        //Create Assignee 
                        var tasktopersonQuery = @"INSERT [CRM].[EventToPeople]([TaskId], [CalendarId], [PersonId], [PersonEmail], [PersonName], [RemindDate], 
                                                    [CreatedOnUtc], [DismissedDate], [SnoozedDate], [RemindMethodEnum], [RecurringNextStartDate], [RecurringNextEndDate])
                                                 VALUES (@TaskId, null, @PersonId, @PersonEmail, @PersonName, null, @CreatedOnUtc, NULL, NULL, NULL, NULL, NULL)";
                        connection.Open();
                        foreach (var attende in data.Attendees)
                        {
                            var newEventToTaskId = connection.Query<int>(tasktopersonQuery, new
                            {
                                TaskId = newTaskId,
                                PersonId = attende.PersonId,
                                PersonEmail = attende.PersonEmail,
                                PersonName = attende.PersonName,
                                CreatedOnUtc = DateTime.UtcNow
                            }).FirstOrDefault();
                        }
                        connection.Close();
                    }
                }

                return Success;
            }
            catch (Exception innerEX)
            {
                EventLogger.LogEvent(innerEX);
                return "Error";
            }

        }

        public Task GetTask(int taskId, int franchiseId)
        {
            return CRMDBContext.Tasks.Include("Attendees").FirstOrDefault(f => f.TaskId == taskId && !f.IsDeleted && f.FranchiseId == franchiseId);
        }

        /// <summary>
        /// Updates the specified data.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="warning">The warning.</param>
        /// <returns>System.String.</returns>
        public string Update(Task data, out string warning)
        {
            warning = null;

            if (!data.AssignedPersonId.HasValue)
            {
                data.AssignedPersonId = SessionManager.CurrentUser.PersonId;
            }
            if (data == null || data.TaskId <= 0)
                return DataCannotBeNullOrEmpty;
            if (string.IsNullOrEmpty(data.Subject))
                return EntityRequiresSubject;
            if ((!data.AssignedPersonId.HasValue || data.AssignedPersonId <= 0))
                return EntityRequiresAssigner;
            if (data.Attendees == null || data.Attendees.Count == 0)
                return EntityRequiresAttendee;

            if (data.FranchiseId <= 0)
                data.FranchiseId = User.FranchiseId.Value;

            try
            {

                //var current = CRMDBContext.Tasks.Include("Attendees").FirstOrDefault(f => f.TaskId == data.TaskId && !f.IsDeleted && f.FranchiseId == data.FranchiseId);
                //Reset franchise
                var current = CRMDBContext.Tasks.Include("Attendees").FirstOrDefault(f => f.TaskId == data.TaskId && !f.IsDeleted);
                if (current == null)
                    return TaskDoesNotExist;

                // TODO: is this required in TP???
                //if ((AssignedPermission.CanUpdate && current.OrganizerPersonId == User.PersonId) || BasePermission.CanUpdate)
                //{
                //}
                //else
                //    return Unauthorized;

                current.Subject = HttpContext.Current.Server.UrlDecode(data.Subject.Trim());
                current.Message = HttpContext.Current.Server.UrlDecode(data.Message);
                current.DueDate = data.DueDate;
                current.ReminderMinute = data.ReminderMinute;
                current.RemindMethodEnum = data.RemindMethodEnum;
                current.FirstRemindDate = data.FirstRemindDate;
                current.PriorityOrder = data.PriorityOrder;
                current.OrganizerPersonId = data.OrganizerPersonId;
                current.AssignedPersonId = data.AssignedPersonId;
                current.AssignedName = this.CRMDBContext.People.FirstOrDefault(f => f.PersonId == data.AssignedPersonId).FullName;

                if (data.LeadId <= 0)
                {
                    current.LeadId = null;
                    current.LeadNumber = null;
                }
                else
                {
                    current.LeadId = data.LeadId;
                    current.LeadNumber = data.LeadNumber;
                }
                //only update this if it is null and has a date, vice versa
                if ((!current.CompletedDate.HasValue && data.CompletedDate.HasValue) ||
                    (current.CompletedDate.HasValue && !data.CompletedDate.HasValue))
                    current.CompletedDate = data.CompletedDate;

                var entry = CRMDBContext.Entry(current);
                var changes = Util.GetEFChangedProperties(entry, current);
                if (changes != null && entry.Property(i => i.OrganizerPersonId).IsModified)
                {
                    var origUser = CacheManager.UserCollection.FirstOrDefault(f => f.PersonId == current.OrganizerPersonId);
                    var curUser = CacheManager.UserCollection.FirstOrDefault(f => f.PersonId == data.OrganizerPersonId);
                    var assignedElem = changes.Element("OrganizerPersonId");
                    if (origUser != null && curUser != null && assignedElem != null)
                    {
                        var cur = assignedElem.Element("Current");
                        if (cur != null)
                            cur.Value = curUser.Person.FullName;
                        var ori = assignedElem.Element("Original");
                        if (ori != null)
                            ori.Value = origUser.Person.FullName;
                    }
                }

                var peopleMatched = (from orig in current.Attendees
                                     join cur in data.Attendees on orig.PersonId equals cur.PersonId
                                     select new { Current = cur, Original = orig });
                var peopleToRemove = current.Attendees.Except(peopleMatched.Select(s => s.Original)).ToList();
                var peopleToAdd = data.Attendees.Except(peopleMatched.Select(s => s.Current)).ToList();

                if (data.DueDate.HasValue)
                {
                    var conflictedTasks = peopleToAdd.Where(attendee => this.CRMDBContext.Tasks.Any(t => !t.IsDeleted && t.DueDate.HasValue && t.DueDate == data.DueDate && t.Attendees.Any(a => a.PersonId == attendee.PersonId))).ToList();

                    if (conflictedTasks.Any())
                    {
                        warning = string.Format("This appointment conflicts with an existing appointment for {0}",
                            string.Join(", ", conflictedTasks.Select(c => string.Format("{0} ({1})", c.PersonName, c.PersonEmail))));
                    }
                }

                foreach (var per in peopleMatched)
                {
                    //remind on changed so reset reminders
                    if (per.Original.RemindDate != per.Current.RemindDate)
                    {
                        per.Original.RemindDate = per.Current.RemindDate;
                        per.Original.RemindMethodEnum = per.Current.RemindMethodEnum;
                        per.Original.DismissedDate = null;
                        per.Original.SnoozedDate = null;
                    }
                }

                foreach (var per in peopleToRemove)
                {
                    current.Attendees.Remove(per);
                }
                foreach (var per in peopleToAdd)
                {
                    current.Attendees.Add(per);
                }

                current.LastUpdatedPersonId = data.LastUpdatedPersonId;
                current.LastUpdatedOnUtc = data.LastUpdatedOnUtc;
                if (!current.LastUpdatedOnUtc.HasValue)
                    current.LastUpdatedOnUtc = DateTime.Now;

                if (CRMDBContext.SaveChanges() > 0)
                {
                    if (current.LeadId > 0)
                    {
                        try
                        {
                            var lead = new Lead { LeadId = current.LeadId.Value };
                            CRMDBContext.Leads.Attach(lead);
                            lead.EditHistories.Add(new EditHistory
                            {
                                LoggedByPersonId = current.LastUpdatedPersonId,
                                IPAddress = HttpContext.Current.Request.UserHostAddress,
                                HistoryValue = changes.ToString(SaveOptions.DisableFormatting)
                            });
                            CRMDBContext.SaveChanges();
                        }
                        catch (Exception inner) { EventLogger.LogEvent(inner); }
                    }
                    return Success;
                }
                else
                    return FailedSaveChanges;

            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }

        }
    }
}
