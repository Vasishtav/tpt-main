﻿using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using HFC.CRM.DTO.Product;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HFC.CRM.Core.Logs;
using System.Data.SqlClient;
using Dapper;
using HFC.CRM.Core.Extensions;
using HFC.CRM.DTO.Vendors;
using System.Runtime.Caching;

namespace HFC.CRM.Managers
{
    public class ProductManagerTP : DataManager<FranchiseProducts>
    {
        //public override Permission BasePermission
        //{
        //    get
        //    {
        //        throw new NotImplementedException();
        //    }
        //}
        public ProductManagerTP()
        {
        }

        public ProductManagerTP(User user, Franchise franchise) : this(user, user, franchise)
        {
        }

        public ProductManagerTP(User user, User authorizingUser, Franchise franchise)
        {
            this.User = user;
            this.AuthorizingUser = authorizingUser;
            this.Franchise = franchise;
        }

        public string SaveDiscount(FranchiseDiscount data)
        {
            var query = @"select DiscountIdPk from Crm.FranchiseDiscount where DiscountId=@DiscountId
                          and FranchiseId=@FranchiseId";
            var results = ExecuteIEnumerableObject<FranchiseDiscount>(ContextFactory.CrmConnectionString, query, new
            {
                FranchiseId = SessionManager.CurrentUser.FranchiseId,
                DiscountId = data.DiscountId,
            }).FirstOrDefault();
            if (results != null && results.DiscountIdPk > 0)
            {
                data.DiscountIdPk = results.DiscountIdPk;
                var Check = CheckDupli(data);
                if (Check == "True")
                {
                    return "Discount Name Already Exists";
                }
                else
                {
                    data.DiscountIdPk = results.DiscountIdPk;
                    data.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                    DiscountUpdate(data);
                }
            }
            else
            {
                var Check = CheckDupli(data);
                if (Check == "True")
                {
                    return "Discount Name Already Exists";
                }
                else
                {
                    data.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                    var result = Insert<int, FranchiseDiscount>(ContextFactory.CrmConnectionString, data);
                    if (result != 0)
                    {
                        var output = data.DiscountId + "|" + SessionManager.CurrentFranchise.FranchiseId;
                        return output;
                    }
                }
            }
            return Success;
        }

        public string DiscountUpdate(FranchiseDiscount data)
        {
            if (data.FranchiseId != 0)
            {
                var query = @"select * from Crm.FranchiseDiscount where DiscountIdPk=@DiscountIdPk";
                var result = ExecuteIEnumerableObject<FranchiseDiscount>(ContextFactory.CrmConnectionString, query, new
                {
                    DiscountIdPk = data.DiscountIdPk,
                }).FirstOrDefault();
                if (result != null)
                {
                    data.CreatedBy = result.CreatedBy;
                    data.LastUpdatedBy = result.LastUpdatedBy;
                    var updatedata = Update<FranchiseDiscount>(ContextFactory.CrmConnectionString, data);
                }
            }

            return "Success";
        }

        public string CheckDupli(FranchiseDiscount data)
        {
            if (data.DiscountIdPk != 0)
            {
                var query = @"select * from Crm.FranchiseDiscount where Name=@Name and FranchiseId=@Id and DiscountIdPk!=@IdPk";
                var resultvalue = ExecuteIEnumerableObject<FranchiseDiscount>(ContextFactory.CrmConnectionString, query, new
                {
                    Name = data.Name,
                    Id = SessionManager.CurrentFranchise.FranchiseId,
                    IdPk = data.DiscountIdPk
                }).ToList();
                if (resultvalue.Count != 0)
                {
                    return "True";
                }
                else if (resultvalue.Count == 0)
                {
                    return "False";
                }
            }
            else
            {
                if (data.DiscountId != 0)
                {
                    var query = @"select * from Crm.FranchiseDiscount where Name=@Name and FranchiseId=@Id";
                    var resultvalue = ExecuteIEnumerableObject<FranchiseDiscount>(ContextFactory.CrmConnectionString, query, new
                    {
                        Name = data.Name,
                        Id = SessionManager.CurrentFranchise.FranchiseId
                    }).ToList();
                    if (resultvalue.Count != 0)
                    {
                        return "True";
                    }
                    else if (resultvalue.Count == 0)
                    {
                        return "False";
                    }
                }
            }

            return null;
        }

        public bool PrdtVenNameCheck(FranchiseProducts data)
        {
            var query = "";

            if (data.ProductKey != 0)
            {
                if (data.ProductType == 3)
                {
                    query = @"select * from CRM.FranchiseProducts where ProductName=@Name and
                              ProductKey!=@Key  and FranchiseId=@Id and ProductType=@Type";
                    var resultvalue = ExecuteIEnumerableObject<FranchiseProducts>(ContextFactory.CrmConnectionString, query, new
                    {
                        Type = data.ProductType,
                        Key = data.ProductKey,
                        VendorID = data.VendorID,
                        VendorName = data.VendorName,
                        Name = data.ProductName,
                        Id = SessionManager.CurrentFranchise.FranchiseId
                    }).ToList();
                    if (resultvalue.Count != 0)
                    {
                        return true;
                    }
                    else if (resultvalue.Count == 0)
                    {
                        return false;
                    }
                }
                else
                {
                    query = @"select * from CRM.FranchiseProducts where ProductName=@Name and ProductKey!=@Key  and FranchiseId=@Id and ProductType=@Type and VendorID=@VendorID and VendorName=@VendorName ";
                    var result = ExecuteIEnumerableObject<FranchiseProducts>(ContextFactory.CrmConnectionString, query, new
                    {
                        Type = data.ProductType,
                        Key = data.ProductKey,
                        VendorID = data.VendorID,
                        VendorName = data.VendorName,
                        Name = data.ProductName,
                        Id = SessionManager.CurrentFranchise.FranchiseId
                    }).ToList();
                    if (result.Count != 0)
                    {
                        return true;
                    }
                    else if (result.Count == 0)
                    {
                        return false;
                    }
                }
            }
            else
            {
                if (data.ProductName != null || data.ProductName != "")
                {
                    if (data.ProductType == 3)
                    {
                        query = @"select * from CRM.FranchiseProducts where ProductName=@Name and FranchiseId=@Id and ProductType=@Type";
                        var result1 = ExecuteIEnumerableObject<FranchiseProducts>(ContextFactory.CrmConnectionString, query, new
                        {
                            Type = data.ProductType,
                            Name = data.ProductName,
                            Id = SessionManager.CurrentFranchise.FranchiseId
                        }).ToList();

                        if (result1.Count != 0)
                        {
                            return true;
                        }
                        else if (result1.Count == 0)
                        {
                            return false;
                        }
                    }
                    else
                    {
                        query = @"select * from CRM.FranchiseProducts where ProductName=@Name and FranchiseId=@Id and ProductType=@Type and VendorID=@VendorID and VendorName=@VendorName ";
                        var result = ExecuteIEnumerableObject<FranchiseProducts>(ContextFactory.CrmConnectionString, query, new
                        {
                            Type = data.ProductType,
                            Name = data.ProductName,
                            VendorID = data.VendorID,
                            VendorName = data.VendorName,
                            Id = SessionManager.CurrentFranchise.FranchiseId
                        }).ToList();

                        if (result.Count != 0)
                        {
                            return true;
                        }
                        else if (result.Count == 0)
                        {
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        public string SaveTempdata(FranchiseProducts model)
        {
            if (model.ProductID != 0)
            {
                Guid TempId = Guid.NewGuid();
                var jsonString = JsonConvert.SerializeObject(model);
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    var query = @"INSERT INTO [History].[TempStorage]([TempId],[TempData])VALUES (@TempId,@TempData)";
                    connection.Execute(query, new
                    {
                        TempId = TempId,
                        TempData = jsonString,
                    });
                }
                return TempId.ToString();
            }
            return null;
        }

        public FranchiseProducts GetTempProd(string gid)
        {
            if (gid != null || gid != "")
            {
                var query = @"select * from [History].[TempStorage] where TempId=@Id";
                var data = ExecuteIEnumerableObject<dynamic>(ContextFactory.CrmConnectionString, query, new
                {
                    Id = gid
                }).FirstOrDefault();
                if (data != null)
                {
                    var FP = JsonConvert.DeserializeObject<FranchiseProducts>(Convert.ToString(data.TempData));
                    return FP;
                }
            }
            return null;
        }

        //To save the product
        public string Save(FranchiseProducts data)
        {
            if (data.ProductKey == 0)
            {
                bool resultcheck = PrdtVenNameCheck(data);
                if (resultcheck == true && data.ProductType == 3)
                {
                    return "Service Name Already Exists";
                }
                else if (resultcheck == true)
                {
                    return "My Product Already Exists";
                }
                else
                {
                    data.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                    if (data.ProductSurfaceSetup != null)
                    {
                        if (SessionManager.BrandId == 2 && data.ProductSurfaceSetup.ProductSurfaceSetupForTL != null)
                        {
                            data.SurfaceLaborSet = JsonConvert.SerializeObject(data.ProductSurfaceSetup);
                        }
                        if (SessionManager.BrandId == 3 && data.ProductSurfaceSetup.ProductSurfaceSetupForCC != null)
                        {
                            data.SurfaceLaborSet = JsonConvert.SerializeObject(data.ProductSurfaceSetup);
                        }
                    }
                    if (data.MultipleSurfaceProduct != null)
                    {
                        data.MultipleSurfaceProductSet = JsonConvert.SerializeObject(data.MultipleSurfaceProduct);
                    }
                    if (data.Attributes != null)
                    {
                        data.MultipartAttributeSet = JsonConvert.SerializeObject(data.Attributes);
                    }
                    var result = Insert<int, FranchiseProducts>(ContextFactory.CrmConnectionString, data);
                    data.ProductKey = result;
                    // Log the History data
                    EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(data), ProductId: data.ProductID);

                    var output = Convert.ToString(result) + "|" + SessionManager.CurrentFranchise.FranchiseId;
                    return output;
                }
            }
            else if (data.ProductKey != 0)
            {
                bool resultcheckupdate = PrdtVenNameCheck(data);
                if (resultcheckupdate == true && data.ProductType == 3)
                {
                    return "Service Name Already Exists";
                }
                else if (resultcheckupdate == true)
                {
                    return "My Product Already Exists";
                }
                else
                {
                    if (data.ProductSurfaceSetup != null)
                    {
                        if (SessionManager.BrandId == 2 && data.ProductSurfaceSetup.ProductSurfaceSetupForTL != null)
                        {
                            data.SurfaceLaborSet = JsonConvert.SerializeObject(data.ProductSurfaceSetup);
                        }
                        if (SessionManager.BrandId == 3 && data.ProductSurfaceSetup.ProductSurfaceSetupForCC != null)
                        {
                            data.SurfaceLaborSet = JsonConvert.SerializeObject(data.ProductSurfaceSetup);
                        }
                    }
                    else
                    {
                        data.SurfaceLaborSet = null;
                    }
                    if (data.MultipleSurfaceProduct != null)
                    {
                        data.MultipleSurfaceProductSet = JsonConvert.SerializeObject(data.MultipleSurfaceProduct);
                    }
                    else
                    {
                        data.MultipleSurfaceProductSet = null;
                    }
                    if (data.Attributes != null)
                    {
                        data.MultipartAttributeSet = JsonConvert.SerializeObject(data.Attributes);
                    }
                    else
                    {
                        data.MultipartAttributeSet = null;
                    }
                    ProductUpdate(data);
                }
            }

            return Success;
        }


        //update product
        public string ProductUpdate(FranchiseProducts data)
        {
            if (data.FranchiseId != 0)
            {
                var query = @"Select * from CRM.FranchiseProducts where ProductKey=@ProductKey";
                var result = ExecuteIEnumerableObject<FranchiseProducts>(ContextFactory.CrmConnectionString, query, new
                {
                    ProductKey = data.ProductKey
                }).FirstOrDefault();
                if (result != null)
                {
                    data.CreatedOn = result.CreatedOn;
                    data.CreatedBy = result.CreatedBy;
                    data.LastUpdatedOn = DateTime.UtcNow;
                    data.LastUpdatedBy = User.PersonId;
                    if (data.ProductSurfaceSetup != null)
                    {
                        if (SessionManager.BrandId == 2 && data.ProductSurfaceSetup.ProductSurfaceSetupForTL != null)
                        {
                            data.SurfaceLaborSet = JsonConvert.SerializeObject(data.ProductSurfaceSetup);
                        }
                        if (SessionManager.BrandId == 3 && data.ProductSurfaceSetup.ProductSurfaceSetupForCC != null)
                        {
                            data.SurfaceLaborSet = JsonConvert.SerializeObject(data.ProductSurfaceSetup);
                        }
                    }
                    if (data.MultipleSurfaceProduct != null)
                    {
                        data.MultipleSurfaceProductSet = JsonConvert.SerializeObject(data.MultipleSurfaceProduct);
                    }
                    if (data.Attributes != null)
                    {
                        data.MultipartAttributeSet = JsonConvert.SerializeObject(data.Attributes);
                    }
                    var datas = Update<FranchiseProducts>(ContextFactory.CrmConnectionString, data);
                    // Log the History data
                    EventLogger.LogHistoryChanges_Audit(JsonConvert.SerializeObject(data), ProductId: data.ProductID);
                }
            }
            //update status of hfc core product
            else
            {
                var query = @"select * from [CRM].[HFCProductStatus] where ProductKey=@ProductKey and FranchiseId=@FranchiseId";
                var result = ExecuteIEnumerableObject<object>(ContextFactory.CrmConnectionString, query, new
                {
                    ProductKey = data.ProductKey,
                    FranchiseId = SessionManager.CurrentUser.FranchiseId
                }).FirstOrDefault();
                if (result != null)
                {
                    var QUdate = @"update [CRM].[HFCProductStatus] set ProductStatusId=@ProductStatus where ProductKey=@ProductKey and FranchiseId=@FranchiseId";
                    var resultINSERT = ExecuteIEnumerableObject<bool>(ContextFactory.CrmConnectionString, QUdate, new
                    {
                        ProductKey = data.ProductKey,
                        FranchiseId = SessionManager.CurrentUser.FranchiseId,
                        ProductStatus = data.ProductStatus
                    }).FirstOrDefault();
                }
                else
                {
                    var Qinsert = @"insert into [CRM].[HFCProductStatus] values(@ProductKey,@FranchiseId,@ProductStatus)";
                    var resultINSERT = ExecuteIEnumerableObject<int>(ContextFactory.CrmConnectionString, Qinsert, new
                    {
                        ProductKey = data.ProductKey,
                        FranchiseId = SessionManager.CurrentUser.FranchiseId,
                        ProductStatus = data.ProductStatus
                    }).FirstOrDefault();
                }
            }
            //var result = Update(ContextFactory.CrmConnectionString, data);
            return "Success";
        }

        public List<HistoryProductPricingLog> GetHistoryProductPricingLog(int ProductKey)
        {
            /// [History].[AuditTrail] maintain the Product history data
            List<HistoryProductPricingLog> Logdata = new List<HistoryProductPricingLog>();
            string query = @"  select TempData from [History].[AuditTrail] hat
                            join CRM.FranchiseProducts fp on hat.ProductId=fp.ProductID
                            where ProductKey=@ProductKey and FranchiseId=@FranchiseId order by hat.CreatedOn asc";
            using (var con = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var data = con.Query<HistoryProductPricingLog>(query, new { ProductKey, Franchise.FranchiseId }).ToList();

                for (int i = 0; i < data.Count; i++)
                {
                    HistoryProductPricingLog pl = new HistoryProductPricingLog();
                    if (i == 0)
                    {
                        // Default add the first record into list
                        var FranchiseProducts = JsonConvert.DeserializeObject<FranchiseProducts>(data[i].TempData);
                        pl.CreatedOn = TimeZoneManager.ToLocal(FranchiseProducts.LastUpdatedOn);
                        pl.UserName = GetUserName(FranchiseProducts.LastUpdatedBy);
                        pl.Cost = FranchiseProducts.Cost != null ? FranchiseProducts.Cost : 0;
                        pl.UnitPrice = FranchiseProducts.SalePrice != null ? FranchiseProducts.SalePrice : 0;
                        Logdata.Add(pl);
                    }
                    else
                    {
                        // compare the current record with last record in the list
                        var Previosdata = Logdata.Last();
                        var FranchiseProducts = JsonConvert.DeserializeObject<FranchiseProducts>(data[i].TempData);
                        FranchiseProducts.Cost = FranchiseProducts.Cost != null ? FranchiseProducts.Cost : 0;
                        FranchiseProducts.SalePrice = FranchiseProducts.SalePrice != null ? FranchiseProducts.SalePrice : 0;

                        if (Previosdata.Cost != FranchiseProducts.Cost || Previosdata.UnitPrice != FranchiseProducts.SalePrice)
                        {
                            pl.CreatedOn = TimeZoneManager.ToLocal(FranchiseProducts.LastUpdatedOn);
                            pl.UserName = GetUserName(FranchiseProducts.LastUpdatedBy);
                            pl.Cost = FranchiseProducts.Cost;
                            pl.UnitPrice = FranchiseProducts.SalePrice;
                            Logdata.Add(pl);
                        }
                    }
                }
            }

            return Logdata;
        }

        /// Get the Name of the person by PersonId
        /// This used for getting the Created by or Modified by
        public string GetUserName(int PersonId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var Query = @"select FirstName +' '+ LastName as Name from CRM.Person Where PersonId=@PersonId";
                string Value = connection.ExecuteScalar<string>(Query, new { PersonId });
                return Value;
            }
        }

        public override string Add(FranchiseProducts data)
        {
            throw new NotImplementedException();
        }

        public override string Delete(int id)
        {
            throw new NotImplementedException();
        }

        public override FranchiseProducts Get(int id)
        {
            throw new NotImplementedException();
        }

        public override ICollection<FranchiseProducts> Get(List<int> idList)
        {
            throw new NotImplementedException();
        }

        public override string Update(FranchiseProducts data)
        {
            throw new NotImplementedException();
        }

        //To Generate the product id automatically
        public int GetProductNumberNew()
        {
            return ExecuteIEnumerableObject<int>(ContextFactory.CrmConnectionString, "[CRM].[spProductNumber_New]").FirstOrDefault();
        }

        //list to kendo
        public List<ProductListViewModel> GetListData()
        {
            if (SessionManager.CurrentFranchise != null)
            {
                //var query = @"select ProductKey,ProductID,ProductName,VendorId,VendorName,VendorProductSKU,ProductCategory,ProductSubCategory,Description,SalePrice,FranchiseId,ProductStatus from
                //             (select hp.ProductKey,ProductID,ProductName,VendorId ,
                //             case when exists(select VendorId from [Acct].[HFCVendors] where VendorId=hp.VendorID)
                //             then (select Name from [Acct].[HFCVendors] where VendorId=hp.VendorID)
                //             else case when exists(select VendorId from [Acct].[FranchiseVendors])
                //             then (select Name from [Acct].[FranchiseVendors] where VendorId=hp.VendorID) end end as VendorName,
                //             VendorProductSKU,pc.ProductCategory,'' as ProductSubCategory,Description,0 as SalePrice,0 as FranchiseId, isnull(hps.ProductStatusId,1) as ProductStatusId from[CRM].[HFCProducts] hp
                //             left join [CRM].[Type_ProductCategory] pc on hp.ProductCategory=pc.ProductCategoryId
                //             left join [CRM].[HFCProductStatus] hps on hp.ProductKey=hps.ProductKey and hps.FranchiseId = @franchiseid) as a
                //             left join [CRM].[Type_ProductStatus] ps on a.ProductStatusId=ps.ProductStatusId where ps.ProductStatusId=1
                //             union
                var query = @"select ProductKey,ProductID,ProductName,fp.VendorId,case when exists
                         (select VendorId from [Acct].[HFCVendors] where VendorId=fp.VendorID)
                         then (select Name from [Acct].[HFCVendors] where VendorId=fp.VendorID)
                         else case when exists(select VendorId from [Acct].[FranchiseVendors])
                         then (select Name from [Acct].[FranchiseVendors] where VendorId=fp.VendorID) end end as VendorName,
                         VendorProductSKU ,pc.ProductCategory,psc.ProductCategory as ProductSubCategory,Description,
                         SalePrice,fp.FranchiseId,ps.ProductStatus,fp.MultipleSurfaceProductSet,fp.MasterSurfacingProductID from [CRM].[FranchiseProducts] fp
                         join Acct.FranchiseVendors fv on fp.VendorID=fv.VendorId
                         left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
                         left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
                         left join [CRM].[Type_ProductCategory] psc on fp.ProductSubCategory=psc.ProductCategoryId
                         where fv.FranchiseId = @franchiseid and fv.VendorStatus=1 and fp.FranchiseId= @franchiseid and ProductStatusId=1
                         and fp.ProductType!=3  
                         union
                         select ProductKey,ProductID,ProductName,fp.VendorId,'Multiple' as VendorName,
                         VendorProductSKU ,pc.ProductCategory,psc.ProductCategory as ProductSubCategory,Description,
                         SalePrice,fp.FranchiseId,ps.ProductStatus,fp.MultipleSurfaceProductSet,fp.MasterSurfacingProductID from [CRM].[FranchiseProducts] fp
                         left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
                         left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
                         left join [CRM].[Type_ProductCategory] psc on fp.ProductSubCategory=psc.ProductCategoryId
                         where  fp.FranchiseId= @franchiseid and ProductStatusId=1 and fp.VendorID is null and fp.MultipartSurfacing=1
                         union
                         select ProductKey,ProductID,ProductName,fp.VendorId,case when exists
                         (select VendorId from [Acct].[HFCVendors] where VendorId=fp.VendorID)
                         then (select Name from [Acct].[HFCVendors] where VendorId=fp.VendorID)
                         else case when exists(select VendorId from [Acct].[FranchiseVendors])
                         then (select Name from [Acct].[FranchiseVendors] where VendorId=fp.VendorID) end end as VendorName,
                         VendorProductSKU ,
                         pc.ProductCategory,psc.ProductCategory as ProductSubCategory,fp.Description,fp.SalePrice,fp.FranchiseId, ps.ProductStatus,fp.MultipleSurfaceProductSet,fp.MasterSurfacingProductID from [CRM].[FranchiseProducts] fp
                         left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
                         left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
                         left join [CRM].[Type_ProductCategory] psc on fp.ProductSubCategory=psc.ProductCategoryId
                         where  fp.FranchiseId= @franchiseid and fp.ProductStatus=1 and fp.ProductType=3";

                var result = ExecuteIEnumerableObject<MultipartProductListViewModel>(ContextFactory.CrmConnectionString, query, new
                {
                    franchiseid = SessionManager.CurrentFranchise.FranchiseId
                }).ToList();
                if (result != null)
                {
                    return FilterNonVendorItem(result);
                }
            }
            else
            {
                List<Role> roles = SessionManager.CurrentUser.Roles.ToList();
                if (roles.FindAll(delegate (Role role) { return role.RoleName == "Global Admin"; }).Count != 0)
                {
                    var query = @"select ProductKey,ProductID,ProductName,fp.VendorId,case when exists
                        (select VendorId from [Acct].[HFCVendors] where VendorId=fp.VendorID)
                        then (select Name from [Acct].[HFCVendors] where VendorId=fp.VendorID)
                        else case when exists(select VendorId from [Acct].[FranchiseVendors])
                        then (select Name from [Acct].[FranchiseVendors] where VendorId=fp.VendorID) end end as VendorName,
                        VendorProductSKU ,pc.ProductCategory,psc.ProductCategory as ProductSubCategory,Description,
                        SalePrice,fp.FranchiseId,ps.ProductStatus,fp.MultipleSurfaceProductSet from [CRM].[SurfacingProducts] fp
						join Acct.FranchiseVendors fv on fp.VendorID=fv.VendorId
                        left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
                        left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
                        left join [CRM].[Type_ProductCategory] psc on fp.ProductSubCategory=psc.ProductCategoryId
                        where  fv.VendorStatus=1 and ProductStatusId=1
						union
                        select ProductKey,ProductID,ProductName,fp.VendorId,'Multiple' as VendorName,
                        VendorProductSKU ,pc.ProductCategory,psc.ProductCategory as ProductSubCategory,Description,
                        SalePrice,fp.FranchiseId,ps.ProductStatus,fp.MultipleSurfaceProductSet from [CRM].[SurfacingProducts] fp
                        left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
                        left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
                        left join [CRM].[Type_ProductCategory] psc on fp.ProductSubCategory=psc.ProductCategoryId
                        where ProductStatusId=1 and fp.VendorID is null and fp.MultipartSurfacing=1
                        union
                        select ProductKey,ProductID,ProductName,Null as VendorId ,'' as VendorName,'' as VendorProductSKU,
                        pc.ProductCategory,psc.ProductCategory as ProductSubCategory,fp.Description,fp.SalePrice,fp.FranchiseId, ps.ProductStatus,fp.MultipleSurfaceProductSet from [CRM].[SurfacingProducts] fp
                        left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
                        left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
                        left join [CRM].[Type_ProductCategory] psc on fp.ProductSubCategory=psc.ProductCategoryId
                        where fp.ProductStatus=1 and fp.ProductType=3";

                    var result = ExecuteIEnumerableObject<MultipartProductListViewModel>(ContextFactory.CrmConnectionString, query).ToList();
                    if (result != null)
                    {
                        List<ProductListViewModel> output = new List<ProductListViewModel>();
                        var vendorlist = ExecuteIEnumerableObject<ProductListNameModel>(ContextFactory.CrmConnectionString, @";with cte as (
				                         select distinct F.VendorId,V.Name,V.VendorType from Acct.FranchiseVendors F inner join Acct.HFCVendors V on F.VendorId=V.VendorId
				                         and F.VendorStatus=1
				                         union
				                         select distinct V.VendorId, (F.Code + ' - ' + V.Name )as Name,V.VendorType from Acct.FranchiseVendors V inner join crm.Franchise F on V.FranchiseId=F.FranchiseID
				                         where VendorType=3 and VendorStatus=1) select * from cte order by VendorType,Name").ToList();
                        foreach (MultipartProductListViewModel product in result)
                        {
                            if (product.VendorName == null && product.VendorId == 0 && product.MultipleSurfaceProductSet != null)
                            {
                                product.MultipleSurfaceProduct = JsonConvert.DeserializeObject<List<MultipleSurfaceProduct>>(product.MultipleSurfaceProductSet);
                                var missedproducts = product.MultipleSurfaceProduct.Where(a => !vendorlist.Any(x => x.VendorID == a.VendorID));
                                if (missedproducts.Count() == 0)
                                {
                                    output.Add(new ProductListViewModel
                                    {
                                        ProductKey = product.ProductKey,
                                        ProductID = product.ProductID,
                                        VendorId = product.VendorId,
                                        ProductName = product.ProductName,
                                        VendorName = product.VendorName,
                                        VendorProductSKU = product.VendorProductSKU,
                                        ProductCategory = product.ProductCategory,
                                        ProductSubCategory = product.ProductSubCategory,
                                        Description = product.Description,
                                        SalePrice = product.SalePrice,
                                        FranchiseId = product.FranchiseId,
                                        ProductStatus = product.ProductStatus
                                    });
                                }
                            }
                            else
                            {
                                output.Add(new ProductListViewModel
                                {
                                    ProductKey = product.ProductKey,
                                    ProductID = product.ProductID,
                                    VendorId = product.VendorId,
                                    ProductName = product.ProductName,
                                    VendorName = product.VendorName,
                                    VendorProductSKU = product.VendorProductSKU,
                                    ProductCategory = product.ProductCategory,
                                    ProductSubCategory = product.ProductSubCategory,
                                    Description = product.Description,
                                    SalePrice = product.SalePrice,
                                    FranchiseId = product.FranchiseId,
                                    ProductStatus = product.ProductStatus
                                });
                            }
                        }
                        return output.OrderBy(x => x.ProductName).ToList();
                    }
                }
            }
            return null;
        }

        public ProductViewModel GetProduct(int id, int FranchiseId)
        {
            ProductViewModel result = new ProductViewModel();
            if (FranchiseId == 0)
            {
                var query = @"select ProductKey,ProductID,PICProductID,ProductName,VendorName,sku,VendorID,ProductCategory,
                            ProductSubCategory,Description,SalePrice,FranchiseId,[ProductCollection],ProductGroupDesc,
                            PhasedoutDate,DiscontinuedDate,
                            ProductCollection,ProductModelID,[ProductModelID],ProductStatus from
                            (select hp.ProductKey,ProductID,hp.PICProductID,ProductName,
                            case when exists(select VendorId from [Acct].[HFCVendors]) then (select Name from [Acct].[HFCVendors] where VendorId=hp.VendorID) else case when exists(select VendorId from [Acct].[FranchiseVendors]) then (select Name from [Acct].[FranchiseVendors] where VendorId=hp.VendorID) end end as VendorName,
                            VendorProductSKU as sku,VendorID,
                            pc.ProductCategory,'' as ProductSubCategory,Description,0 as SalePrice,0 as FranchiseId,[ProductCollection],
                            PhasedoutDate,DiscontinuedDate,ProductGroupDesc,
                            [ProductModelID], isnull(hps.ProductStatusId,1) as ProductStatusId from[CRM].[HFCProducts] hp
                            left join [CRM].[Type_ProductCategory] pc on hp.ProductCategory=pc.ProductCategoryId
                            left join [CRM].[HFCProductStatus] hps on hp.ProductKey=hps.ProductKey and hps.FranchiseId = @FranchiseId) as a
                            left join [CRM].[Type_ProductStatus] ps on a.ProductStatusId=ps.ProductStatusId
                            where ProductKey=@Id";
                result = ExecuteIEnumerableObject<ProductViewModel>(ContextFactory.CrmConnectionString, query, new
                {
                    Id = id,
                    FranchiseId = SessionManager.CurrentFranchise.FranchiseId
                }).FirstOrDefault();
                result.ProductType = "Core Product";
            }
            else
            {
                var query = @"select ProductKey,ProductID,ProductName,(select hp.[PICProductID] from [CRM].[HFCProducts] hp
                              where ProductID=fp.[ProductID]) as [PICProductID],
                              case when exists(select VendorId from [Acct].[HFCVendors] where VendorId=fp.VendorID) then
                              (select Name from [Acct].[HFCVendors] where VendorId=fp.VendorID)
                              else case when exists(select VendorId from [Acct].[FranchiseVendors]) then
                              (select Name from [Acct].[FranchiseVendors] where VendorId=fp.VendorID) end end as VendorName,

                              case when exists(select VendorId from [Acct].[HFCVendors] where VendorId=fp.VendorID) then
                              (select VendorType from [Acct].[HFCVendors] where VendorId=fp.VendorID)
                              else case when exists(select VendorId from [Acct].[FranchiseVendors]) then
                              (select VendorType from [Acct].[FranchiseVendors] where VendorId=fp.VendorID) end end as VendorType,

                              [VendorID],VendorProductSKU,pc.ProductCategory,pc.ProductCategoryId,psc.ProductCategory as ProductSubCategory,Description,Taxable,[Cost],
                              SalePrice,FranchiseId,Model,Color,Collection,ps.ProductStatus,tpt.ProductType,
                              [MarkUp],[MarkupType],[Discount],[DiscountType],[CalculatedSalesPrice],
                              fp.SurfaceLaborSet,fp.MultipartSurfacing,fp.MultipleSurfaceProductSet,fp.MultipartAttributeSet
                              from [CRM].[FranchiseProducts] fp
                              left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
                              left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
                              left join [CRM].[Type_ProductCategory] psc on fp.ProductSubCategory=psc.ProductCategoryId
                              left join [CRM].[Type_ProductType] tpt on fp.ProductType=tpt.Id
                              where ProductKey=@Id";
                result = ExecuteIEnumerableObject<ProductViewModel>(ContextFactory.CrmConnectionString, query, new
                {
                    Id = id
                }).FirstOrDefault();
                if (result != null && result.SurfaceLaborSet != null)
                {
                    result.ProductSurfaceSetup = JsonConvert.DeserializeObject<ProductSurfaceSetup>(result.SurfaceLaborSet);
                }
                if (result != null && result.MultipleSurfaceProductSet != null)
                {
                    result = RearrangeVendorName(result);
                    result.MultipleSurfaceProduct = JsonConvert.DeserializeObject<List<MultipleSurfaceProduct>>(result.MultipleSurfaceProductSet);
                }
                if (result != null && result.MultipartAttributeSet != null)
                {
                    result.Attributes = JsonConvert.DeserializeObject<MultipartAttributes>(result.MultipartAttributeSet);
                }
                //result.ProductType = "MYProduct";
                if (result == null)
                {
                    var query1 = @"select fp.DiscountId as ProductID,fp.Name as ProductName,fp.Description,
                                  fp.DiscountValue as Discount,fp.DiscountFactor as DiscountType,
                                  ProductStatus,fp.Status,fp.DiscountIdPk as ProductKey from Crm.FranchiseDiscount fp
                                  left join [CRM].[Type_ProductStatus] ps on fp.Status=ps.ProductStatusId
                                  where fp.DiscountId=@Id";
                    result = ExecuteIEnumerableObject<ProductViewModel>(ContextFactory.CrmConnectionString, query1, new
                    {
                        Id = id
                    }).FirstOrDefault();
                    result.ProductType = "Discount";
                    if (result.DiscountType == "1")
                        result.DiscountType = "%";
                    else
                        result.DiscountType = "$";
                }
            }


            return result;
        }

        public FranchiseProducts GetDetails(int id, int FranchiseId)
        {
            FranchiseProducts result = new FranchiseProducts();
            if (FranchiseId == 0)
            {
                //var query = @"select hp.[ProductKey],[ProductID],[ProductName],[PICProductID],[VendorID],[VendorName],[VendorProductSKU],[Description]
                //                ,[ProductCategory],[ProductCollection],[ProductModelID],[CreatedOn],[CreatedBy],[LastUpdatedOn]
                //                ,[LastUpdatedBy],isnull(hps.ProductStatusId,1) as ProductStatus from [CRM].[HFCProducts] hp
                //                left join [CRM].[HFCProductStatus] hps on hp.ProductKey=hps.ProductKey and hps.FranchiseId =@FranchiseId
                //                where hp.ProductKey=@Id";
                var query = @"select hp.[ProductKey],[ProductID],[ProductName],[PICProductID],[VendorID],[VendorName],[VendorProductSKU],
                             [Description],[ProductGroupDesc],[PhasedoutDate],[DiscontinuedDate],''as Model
                             ,[ProductCategory],[ProductCollection],[ProductModelID],[CreatedOn],[CreatedBy],[LastUpdatedOn]
                             ,[LastUpdatedBy],isnull(hps.ProductStatusId,1) as ProductStatus from [CRM].[HFCProducts] hp
                             left join [CRM].[HFCProductStatus] hps on hp.ProductKey=hps.ProductKey and hps.FranchiseId =@FranchiseId
                             where hp.ProductKey=@Id";
                result = ExecuteIEnumerableObject<FranchiseProducts>(ContextFactory.CrmConnectionString, query, new
                {
                    FranchiseId = SessionManager.CurrentFranchise.FranchiseId,
                    Id = id
                }).FirstOrDefault();
                result.ProductType = 1;
            }
            else
            {
                var query = @"select DISTINCT ds.*,fv.VendorType from CRM.FranchiseProducts  ds
                              left join Acct.FranchiseVendors fv on ds.VendorID=fv.VendorId
                               where ds.ProductKey=@ProductKey and ds.FranchiseId=@FranchiseId";
                result = ExecuteIEnumerableObject<FranchiseProducts>(ContextFactory.CrmConnectionString, query, new
                {
                    FranchiseId = SessionManager.CurrentFranchise.FranchiseId,
                    ProductKey = id
                }).FirstOrDefault();

                if (result != null && result.SurfaceLaborSet != null)
                {
                    result.ProductSurfaceSetup = JsonConvert.DeserializeObject<ProductSurfaceSetup>(result.SurfaceLaborSet);
                }
                if (result != null && result.MultipleSurfaceProductSet != null)
                {
                    result.MultipleSurfaceProduct = JsonConvert.DeserializeObject<List<MultipleSurfaceProduct>>(result.MultipleSurfaceProductSet);
                }
                if (result != null && result.MultipartAttributeSet != null)
                {
                    result.Attributes = JsonConvert.DeserializeObject<MultipartAttributes>(result.MultipartAttributeSet);
                }

                //result.ProductType = "MYProduct";
                if (result == null)
                {
                    var query1 = @"select DiscountId as ProductID,Name as ProductName,Description,DiscountIdPk as ProductKey,
                                   DiscountValue as Discount,DiscountFactor as DiscountType,
                                   Status as ProductStatus from Crm.FranchiseDiscount where DiscountId=@Ids";
                    result = ExecuteIEnumerableObject<FranchiseProducts>(ContextFactory.CrmConnectionString, query1, new
                    {
                        Ids = id
                    }).FirstOrDefault();
                    result.ProductType = 4;
                    //if (result.DiscountType == "1")
                    //    result.DiscountType = "%";
                }
            }

            return result;
        }

        public List<ProductListViewModel> GetListDetails(int id)
        {
            if (id == 1)
            {
                //var query = @"select ProductKey,ProductID,ProductName,VendorName,sku,ProductCategory,ProductSubCategory,Description,SalePrice,FranchiseId,ProductStatus from
                //        (select hp.ProductKey,ProductID,ProductName,VendorName,VendorProductSKU as sku,
                //        pc.ProductCategory,'' as ProductSubCategory,Description,0 as SalePrice,0 as FranchiseId, isnull(hps.ProductStatusId,1) as ProductStatusId from[CRM].[HFCProducts] hp
                //        left join [CRM].[Type_ProductCategory] pc on hp.ProductCategory=pc.ProductCategoryId
                //        left join [CRM].[HFCProductStatus] hps on hp.ProductKey=hps.ProductKey and hps.FranchiseId = @franchiseid) as a
                //        left join [CRM].[Type_ProductStatus] ps on a.ProductStatusId=ps.ProductStatusId where ps.ProductStatusId=1";
                var query = @"select ProductKey,ProductID,ProductName,VendorId,VendorName,VendorProductSKU,ProductCategory,ProductSubCategory,Description,SalePrice,FranchiseId,ProductStatus from
                             (select hp.ProductKey,ProductID,ProductName,fv.VendorId ,
                             case when exists(select VendorId from [Acct].[HFCVendors] where VendorId=hp.VendorID)
                             then (select Name from [Acct].[HFCVendors] where VendorId=hp.VendorID)
                             else case when exists(select VendorId from [Acct].[FranchiseVendors])
                             then (select Name from [Acct].[FranchiseVendors] where VendorId=hp.VendorID) end end as VendorName,
                             VendorProductSKU,pc.ProductCategory,'' as ProductSubCategory,Description,0 as SalePrice,0 as FranchiseId, isnull(hps.ProductStatusId,1) as ProductStatusId from[CRM].[HFCProducts] hp
                             join Acct.FranchiseVendors fv on hp.VendorID=fv.VendorId
                             left join [CRM].[Type_ProductCategory] pc on hp.ProductCategory=pc.ProductCategoryId
                             left join [CRM].[HFCProductStatus] hps on hp.ProductKey=hps.ProductKey and hps.FranchiseId = @franchiseid and fv.VendorStatus=1
                             where fv.FranchiseId=@franchiseid and fv.VendorStatus=1) as a
                             left join [CRM].[Type_ProductStatus] ps on a.ProductStatusId=ps.ProductStatusId where ps.ProductStatusId=1 ";
                var result = ExecuteIEnumerableObject<ProductListViewModel>(ContextFactory.CrmConnectionString, query, new
                {
                    franchiseid = SessionManager.CurrentFranchise.FranchiseId
                }).ToList();
                if (result != null)
                {
                    return result.OrderBy(x => x.ProductName).ToList();
                }
            }
            else if (id == 2)
            {
                //var query = @"select ProductKey,ProductID,ProductName,VendorName,VendorProductSKU as sku,
                //              pc.ProductCategory,psc.ProductCategory as ProductSubCategory,Description,SalePrice,FranchiseId,ps.ProductStatus from[CRM].[FranchiseProducts] fp
                //              left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
                //              left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
                //              left join [CRM].[Type_ProductCategory] psc on fp.ProductSubCategory=psc.ProductCategoryId
                //              where FranchiseId = @franchiseid and ps.ProductStatusId=1";
                var query = @"select ProductKey,ProductID,ProductName,VendorId,
                              case when exists(select VendorId from [Acct].[HFCVendors] where VendorId=fp.VendorID)
                              then (select Name from [Acct].[HFCVendors] where VendorId=fp.VendorID)
                              else case when exists(select VendorId from [Acct].[FranchiseVendors])
                              then (select Name from [Acct].[FranchiseVendors] where VendorId=fp.VendorID) end end as VendorName,
                              VendorProductSKU,
                              pc.ProductCategory,psc.ProductCategory as ProductSubCategory,Description,SalePrice,FranchiseId,ps.ProductStatus from[CRM].[FranchiseProducts] fp
                              left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
                              left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
                              left join [CRM].[Type_ProductCategory] psc on fp.ProductSubCategory=psc.ProductCategoryId
                              where FranchiseId = @franchiseid and ps.ProductStatusId=1";
                var result = ExecuteIEnumerableObject<ProductListViewModel>(ContextFactory.CrmConnectionString, query, new
                {
                    franchiseid = SessionManager.CurrentFranchise.FranchiseId
                }).ToList();
                if (result != null)
                {
                    return result.OrderBy(x => x.ProductName).ToList();
                }
            }
            else if (id == 3)
            {
                var query = @"select fp.DiscountId as ProductKey,fp.DiscountId as ProductID, fp.Name as ProductName,fp.Description,fp.FranchiseId,
                              fp.Status,ps.ProductStatus
                              from [CRM].[FranchiseDiscount] fp
                              left join [CRM].[Type_ProductStatus] ps on fp.Status=ps.ProductStatusId
                              where FranchiseId=@franchiseid and fp.Status=1";
                var result = ExecuteIEnumerableObject<ProductListViewModel>(ContextFactory.CrmConnectionString, query, new
                {
                    franchiseid = SessionManager.CurrentFranchise.FranchiseId
                }).ToList();
                if (result != null)
                {
                    return result.OrderBy(x => x.ProductName).ToList();
                }
            }
            else
            {
                var query = @"select ProductKey,ProductID,ProductName,fp.VendorId,case when exists
                            (select VendorId from [Acct].[HFCVendors] where VendorId=fp.VendorID)
                            then (select Name from [Acct].[HFCVendors] where VendorId=fp.VendorID)
                            else case when exists(select VendorId from [Acct].[FranchiseVendors])
                            then (select Name from [Acct].[FranchiseVendors] where VendorId=fp.VendorID) end end as VendorName,
                            VendorProductSKU ,pc.ProductCategory,psc.ProductCategory as ProductSubCategory,Description,
                            SalePrice,fp.FranchiseId,ps.ProductStatus,fp.MultipleSurfaceProductSet,fp.MasterSurfacingProductID  from [CRM].[FranchiseProducts] fp
                            join Acct.FranchiseVendors fv on fp.VendorID=fv.VendorId
                            left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
                            left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
                            left join [CRM].[Type_ProductCategory] psc on fp.ProductSubCategory=psc.ProductCategoryId
                            where fv.FranchiseId = @franchiseid and fv.VendorStatus=1 and fp.FranchiseId= @franchiseid and ProductStatusId=1
                            and fp.ProductType !=3
                            union
                            select ProductKey,ProductID,ProductName,fp.VendorId,'Multiple' as VendorName,
                            VendorProductSKU ,pc.ProductCategory,psc.ProductCategory as ProductSubCategory,Description,
                            SalePrice,fp.FranchiseId,ps.ProductStatus,fp.MultipleSurfaceProductSet,fp.MasterSurfacingProductID from [CRM].[FranchiseProducts] fp
                            left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
                            left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
                            left join [CRM].[Type_ProductCategory] psc on fp.ProductSubCategory=psc.ProductCategoryId
                            where fp.FranchiseId= @franchiseid and ProductStatusId=1 and fp.VendorID is null and fp.MultipartSurfacing=1
                            union
                            select ProductKey,ProductID,ProductName,fp.VendorId,case when exists
                            (select VendorId from [Acct].[HFCVendors] where VendorId=fp.VendorID)
                            then (select Name from [Acct].[HFCVendors] where VendorId=fp.VendorID)
                            else case when exists(select VendorId from [Acct].[FranchiseVendors])
                            then (select Name from [Acct].[FranchiseVendors] where VendorId=fp.VendorID) end end as VendorName,
                            VendorProductSKU,
                            pc.ProductCategory,psc.ProductCategory as ProductSubCategory,fp.Description,fp.SalePrice,fp.FranchiseId, ps.ProductStatus,fp.MultipleSurfaceProductSet,fp.MasterSurfacingProductID from [CRM].[FranchiseProducts] fp
                            left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
                            left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
                            left join [CRM].[Type_ProductCategory] psc on fp.ProductSubCategory=psc.ProductCategoryId
                            where  fp.FranchiseId= @franchiseid and fp.ProductStatus=1 and fp.ProductType=3";
                //Union
                //select ProductKey,ProductID,ProductName,Null as VendorId ,'' as VendorName,'' as VendorProductSKU,
                //pc.ProductCategory,'' as ProductSubCategory,fp.Description,fp.SalePrice,fp.FranchiseId, ps.ProductStatus from [CRM].[FranchiseProducts] fp
                //left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
                //left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
                //where  fp.FranchiseId= @franchiseid and fp.ProductStatus=1 and fp.ProductType=3";

                var result = ExecuteIEnumerableObject<MultipartProductListViewModel>(ContextFactory.CrmConnectionString, query, new
                {
                    franchiseid = SessionManager.CurrentFranchise.FranchiseId
                }).ToList();
                if (result != null)
                {
                    return FilterNonVendorItem(result);
                }
            }
            return null;
        }

        public List<ProductListViewModel> GetListDataAll(int Id, int Value)
        {
            var query = "";
            if (SessionManager.CurrentFranchise == null)
            {
                List<Role> roles = SessionManager.CurrentUser.Roles.ToList();
                if (roles.FindAll(delegate (Role role) { return role.RoleName == "Global Admin"; }).Count != 0)
                {
                    query = @"select ProductKey,ProductID,ProductName,fp.VendorId,case when exists(select VendorId
                          from [Acct].[HFCVendors] where VendorId=fp.VendorID)
                          then (select Name from [Acct].[HFCVendors] where VendorId=fp.VendorID)
                          else case when exists(select VendorId from [Acct].[FranchiseVendors])
                          then (select Name from [Acct].[FranchiseVendors] where VendorId=fp.VendorID) end end as VendorName,
                          VendorProductSKU ,pc.ProductCategory,psc.ProductCategory as ProductSubCategory,Description,
                          SalePrice,fp.FranchiseId,ps.ProductStatus from[CRM].[FranchiseProducts] fp
                          join Acct.FranchiseVendors fv on fp.VendorID=fv.VendorId
                          left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
                          left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
                          left join [CRM].[Type_ProductCategory] psc on fp.ProductSubCategory=psc.ProductCategoryId
                          where fp.ProductStatus in(1,2) and fv.VendorStatus in(1,2)
                         union
                         select ProductKey,ProductID,ProductName,Null as VendorId ,'' as VendorName,'' as VendorProductSKU,
                         pc.ProductCategory,psc.ProductCategory as ProductSubCategory,fp.Description,fp.SalePrice,fp.FranchiseId, ps.ProductStatus from [CRM].[FranchiseProducts] fp
                         left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
                         left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
                         left join [CRM].[Type_ProductCategory] psc on fp.ProductSubCategory=psc.ProductCategoryId
                         where fp.ProductStatus=1 and fp.ProductType=3";
                }
            }
            if (Value == 1)
            {
                //query = @"select ProductKey,ProductID,ProductName,VendorId,VendorName,VendorProductSKU,ProductCategory,ProductSubCategory,Description,SalePrice,FranchiseId,ProductStatus from
                //         (select hp.ProductKey,ProductID,ProductName,VendorId ,
                //         case when exists(select VendorId from [Acct].[HFCVendors] where VendorId=hp.VendorID)
                //         then (select Name from [Acct].[HFCVendors] where VendorId=hp.VendorID)
                //         else case when exists(select VendorId from [Acct].[FranchiseVendors])
                //         then (select Name from [Acct].[FranchiseVendors] where VendorId=hp.VendorID) end end as VendorName,
                //         VendorProductSKU,pc.ProductCategory,'' as ProductSubCategory,Description,0 as SalePrice,0 as FranchiseId, isnull(hps.ProductStatusId,1) as ProductStatusId from[CRM].[HFCProducts] hp
                //         left join [CRM].[Type_ProductCategory] pc on hp.ProductCategory=pc.ProductCategoryId
                //         left join [CRM].[HFCProductStatus] hps on hp.ProductKey=hps.ProductKey and hps.FranchiseId = @franchiseid) as a
                //         left join [CRM].[Type_ProductStatus] ps on a.ProductStatusId=ps.ProductStatusId";
                query = @"select ProductKey,ProductID,ProductName,VendorId,VendorName,VendorProductSKU,ProductCategory,
                          ProductSubCategory,Description,SalePrice,FranchiseId,ProductStatus from
                          (select hp.ProductKey,ProductID,ProductName,fv.VendorId ,
                          case when exists(select VendorId from [Acct].[HFCVendors] where VendorId=hp.VendorID)
                          then (select Name from [Acct].[HFCVendors] where VendorId=hp.VendorID)
                          else case when exists(select VendorId from [Acct].[FranchiseVendors])
                          then (select Name from [Acct].[FranchiseVendors] where VendorId=hp.VendorID) end end as VendorName,
                          VendorProductSKU,pc.ProductCategory,'' as ProductSubCategory,Description,0 as SalePrice,0 as FranchiseId,
                          isnull(hps.ProductStatusId,1) as ProductStatusId from[CRM].[HFCProducts] hp
                          join Acct.FranchiseVendors fv on hp.VendorID=fv.VendorId
                          left join [CRM].[Type_ProductCategory] pc on hp.ProductCategory=pc.ProductCategoryId
                          left join [CRM].[HFCProductStatus] hps on hp.ProductKey=hps.ProductKey and hps.FranchiseId = @franchiseid and fv.VendorStatus=1
                          where fv.FranchiseId=@franchiseid and fv.VendorStatus in(1,2)) as a
                          left join [CRM].[Type_ProductStatus] ps on a.ProductStatusId=ps.ProductStatusId where ps.ProductStatusId in(1,2)";
            }
            else if (Value == 3)
            {
                query = @"select fp.DiscountId as ProductKey,fp.DiscountId as ProductID, fp.Name as ProductName,Null,Null,Null,Null,Null,
                          fp.Description,Null,fp.FranchiseId,ps.ProductStatus from Crm.FranchiseDiscount fp
                          left join [CRM].[Type_ProductStatus] ps on fp.Status=ps.ProductStatusId
                          where FranchiseId=@franchiseid";
            }
            else
            {
                query = @"select ProductKey,ProductID,ProductName,fp.VendorId,case when exists(select VendorId
                          from [Acct].[HFCVendors] where VendorId=fp.VendorID)
                          then (select Name from [Acct].[HFCVendors] where VendorId=fp.VendorID)
                          else case when exists(select VendorId from [Acct].[FranchiseVendors])
                          then (select Name from [Acct].[FranchiseVendors] where VendorId=fp.VendorID) end end as VendorName,
                          VendorProductSKU ,pc.ProductCategory,psc.ProductCategory as ProductSubCategory,Description,
                          SalePrice,fp.FranchiseId,ps.ProductStatus,fp.MultipleSurfaceProductSet from[CRM].[FranchiseProducts] fp
                          join Acct.FranchiseVendors fv on fp.VendorID=fv.VendorId
                          left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
                          left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
                          left join [CRM].[Type_ProductCategory] psc on fp.ProductSubCategory=psc.ProductCategoryId
                          where fv.FranchiseId = @franchiseid and fp.ProductStatus in(1,2) and fv.VendorStatus in(1,2) and fp.FranchiseId=@franchiseid
                          and fp.ProductType!=3 
                          union
                          select ProductKey,ProductID,ProductName,fp.VendorId,'Multiple' as VendorName,
                          VendorProductSKU ,pc.ProductCategory,psc.ProductCategory as ProductSubCategory,Description,
                          SalePrice,fp.FranchiseId,ps.ProductStatus,fp.MultipleSurfaceProductSet from [CRM].[FranchiseProducts] fp
                          left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
                          left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
                          left join [CRM].[Type_ProductCategory] psc on fp.ProductSubCategory=psc.ProductCategoryId
                          where fp.FranchiseId = @franchiseid and ProductStatusId=1 and fp.VendorID is null and fp.MultipartSurfacing=1
                          union
                          select ProductKey,ProductID,ProductName,fp.VendorId,case when exists(select VendorId
                          from [Acct].[HFCVendors] where VendorId=fp.VendorID)
                          then (select Name from [Acct].[HFCVendors] where VendorId=fp.VendorID)
                          else case when exists(select VendorId from [Acct].[FranchiseVendors])
                          then (select Name from [Acct].[FranchiseVendors] where VendorId=fp.VendorID) end end as VendorName,
                          VendorProductSKU ,
                          pc.ProductCategory,psc.ProductCategory as ProductSubCategory,fp.Description,fp.SalePrice,fp.FranchiseId, ps.ProductStatus,fp.MultipleSurfaceProductSet from [CRM].[FranchiseProducts] fp
                          left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
                          left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
                          left join [CRM].[Type_ProductCategory] psc on fp.ProductSubCategory=psc.ProductCategoryId
                          where  fp.FranchiseId= @franchiseid and fp.ProductStatus in(1,2) and fp.ProductType=3";
            }
            //var query = @"select ProductKey,ProductID,ProductName,VendorId,VendorName,VendorProductSKU,ProductCategory,ProductSubCategory,Description,SalePrice,FranchiseId,ProductStatus from
            //             (select hp.ProductKey,ProductID,ProductName,VendorId ,
            //             case when exists(select VendorId from [Acct].[HFCVendors] where VendorId=hp.VendorID)
            //             then (select Name from [Acct].[HFCVendors] where VendorId=hp.VendorID)
            //             else case when exists(select VendorId from [Acct].[FranchiseVendors])
            //             then (select Name from [Acct].[FranchiseVendors] where VendorId=hp.VendorID) end end as VendorName,
            //             VendorProductSKU,pc.ProductCategory,'' as ProductSubCategory,Description,0 as SalePrice,0 as FranchiseId, isnull(hps.ProductStatusId,1) as ProductStatusId from[CRM].[HFCProducts] hp
            //             left join [CRM].[Type_ProductCategory] pc on hp.ProductCategory=pc.ProductCategoryId
            //             left join [CRM].[HFCProductStatus] hps on hp.ProductKey=hps.ProductKey and hps.FranchiseId = @franchiseid) as a
            //             left join [CRM].[Type_ProductStatus] ps on a.ProductStatusId=ps.ProductStatusId
            //             union
            //             select ProductKey,ProductID,ProductName,VendorId,case when exists(select VendorId from [Acct].[HFCVendors] where VendorId=fp.VendorID)
            //             then (select Name from [Acct].[HFCVendors] where VendorId=fp.VendorID)
            //             else case when exists(select VendorId from [Acct].[FranchiseVendors])
            //             then (select Name from [Acct].[FranchiseVendors] where VendorId=fp.VendorID) end end as VendorName,
            //             VendorProductSKU ,pc.ProductCategory,psc.ProductCategory as ProductSubCategory,Description,SalePrice,FranchiseId,ps.ProductStatus from[CRM].[FranchiseProducts] fp
            //             left join [CRM].[Type_ProductStatus] ps on fp.ProductStatus=ps.ProductStatusId
            //             left join [CRM].[Type_ProductCategory] pc on fp.ProductCategory=pc.ProductCategoryId
            //             left join [CRM].[Type_ProductCategory] psc on fp.ProductSubCategory=psc.ProductCategoryId
            //             where FranchiseId = @franchiseid
            //             union
            //             select fp.DiscountId as ProductKey,fp.DiscountId as ProductID, fp.Name as ProductName,Null,Null,Null,Null,Null,
            //             fp.Description,Null,fp.FranchiseId,ps.ProductStatus
            //             from Crm.FranchiseDiscount fp
            //             left join [CRM].[Type_ProductStatus] ps on fp.Status=ps.ProductStatusId
            //             where FranchiseId=@franchiseid";

            var result = ExecuteIEnumerableObject<ProductListViewModel>(ContextFactory.CrmConnectionString, query, new
            {
                franchiseid = SessionManager.CurrentFranchise.FranchiseId
            }).ToList();
            if (result != null)
            {
                return result.OrderBy(x => x.ProductName).ToList();
            }
            else
                return null;
        }

        public List<ProductListViewModel> GetRefreshProductsFromHFCList(int id)
        {
            var ProductList = ExecuteIEnumerableObject<MultipartProductListViewModel>(ContextFactory.CrmConnectionString, @"select * from crm.SurfacingProducts where VendorName='' and VendorID is null and MultipartSurfacing=1 and MultipleSurfaceProductSet is not null and BrandID='" + SessionManager.BrandId + "'").ToList();
            var strproduct = "";
            var spName = "";
            if (ProductList != null)
            {
                List<ProductListNameModel> vendorlist = new List<ProductListNameModel>();
                LookupManager lookup = new LookupManager();
                vendorlist = lookup.GetProductVendorname(2);
                List<int> syncproduct = new List<int>();

                foreach (var product in ProductList)
                {
                    product.MultipleSurfaceProduct = JsonConvert.DeserializeObject<List<MultipleSurfaceProduct>>(product.MultipleSurfaceProductSet);
                    var missedproducts = product.MultipleSurfaceProduct.Where(a => !vendorlist.Any(x => x.VendorID == a.VendorID));
                    if (missedproducts.Count() == 0)
                    {
                        syncproduct.Add(product.ProductID);
                    }
                }
                if (syncproduct != null) strproduct = string.Join(",", syncproduct);
                else strproduct = null;

            }
            if (id == 1)
            {
                spName = "[CRM].[RefreshSurfacingMasterList]";
            }
            else if (id == 2)
            {
                spName = "[CRM].[AddNewProductFromHFCMaster]";
            }
            else if (id == 3)
            {
                spName = "[CRM].[UpdateVendorCostFromHFCList]";

                if (strproduct != "")
                {
                    var surface = ExecuteIEnumerableObject<MultipartProductListViewModel>(ContextFactory.CrmConnectionString, @"select * from [crm].[SurfacingProducts] where ProductID in (select convert(int, value)as ID FROM string_split(@strProductID, ',')) and BrandID=@BrandId", new
                    { strProductID = strproduct, BrandId = SessionManager.BrandId }).ToList();

                    var franchise = ExecuteIEnumerableObject<MultipartProductListViewModel>(ContextFactory.CrmConnectionString, @"select * from [crm].[FranchiseProducts] where MasterSurfacingProductID in (select convert(int, value)as ID FROM string_split(@strProductID, ','))", new
                    { strProductID = strproduct }).ToList();

                    if (surface != null && franchise != null)
                    {
                        foreach (var sproduct in surface)
                        {
                            foreach (var fproduct in franchise)
                            {
                                if (sproduct.ProductID == fproduct.MasterSurfacingProductID)
                                {
                                    fproduct.MultipleSurfaceProduct = JsonConvert.DeserializeObject<List<MultipleSurfaceProduct>>(fproduct.MultipleSurfaceProductSet);
                                    sproduct.MultipleSurfaceProduct = JsonConvert.DeserializeObject<List<MultipleSurfaceProduct>>(sproduct.MultipleSurfaceProductSet);
                                    foreach (var sitem in sproduct.MultipleSurfaceProduct)
                                    {
                                        foreach (var fitem in fproduct.MultipleSurfaceProduct)
                                        {
                                            if (sitem.Id == fitem.Id)
                                            {
                                                fitem.CostPrice = sitem.CostPrice;
                                                fitem.UnitPrice = sitem.UnitPrice;
                                            }
                                        }
                                    }
                                    fproduct.MultipleSurfaceProductSet = JsonConvert.SerializeObject(fproduct.MultipleSurfaceProduct);
                                    var exec = ExecuteIEnumerableObject<bool>(ContextFactory.CrmConnectionString, @"update [crm].[FranchiseProducts] set MultipleSurfaceProductSet=@updatevalue where productkey=@keyvalue", new
                                    { updatevalue = fproduct.MultipleSurfaceProductSet, keyvalue = fproduct.ProductKey }).FirstOrDefault();
                                }
                            }
                        }
                    }
                }
            }
            var result = ExecuteIEnumerableObject<bool>(ContextFactory.CrmConnectionString, "EXEC " + spName + " @FranchiseId, @PersonId, @strProductID, @BrandID", new
            {
                FranchiseId = SessionManager.CurrentFranchise.FranchiseId,
                PersonId = SessionManager.CurrentUser.PersonId,
                strProductID = (strproduct == "") ? null : strproduct,
                BrandID = SessionManager.BrandId
            }).FirstOrDefault();

            return GetListData();
        }

        public ProductViewModel RearrangeVendorName(ProductViewModel product)
        {
            if (product.MultipleSurfaceProductSet != null && product.MultipleSurfaceProductSet != "")
            {
                var items = JsonConvert.DeserializeObject<List<MultipleSurfaceProduct>>(product.MultipleSurfaceProductSet);
                foreach (MultipleSurfaceProduct i in items)
                {
                    string str = i.VendorName;
                    int j = str.IndexOf("-");
                    if (j >= 0) str = str.Remove(0, j + 2);
                    i.VendorName = str;
                }
                product.MultipleSurfaceProductSet = JsonConvert.SerializeObject(items);
            }
            return product;
        }
        public List<ProductListViewModel> FilterNonVendorItem(List<MultipartProductListViewModel> result)
        {
            List<ProductListViewModel> output = new List<ProductListViewModel>();
            List<ProductListNameModel> vendorlist = new List<ProductListNameModel>();
            LookupManager lookup = new LookupManager();
            vendorlist = lookup.GetProductVendorname(2);

            foreach (MultipartProductListViewModel product in result)
            {
                if (product.VendorName == null && product.VendorId == 0 && product.MultipleSurfaceProductSet != null && product.MultipleSurfaceProductSet != "")
                {
                    product.MultipleSurfaceProduct = JsonConvert.DeserializeObject<List<MultipleSurfaceProduct>>(product.MultipleSurfaceProductSet);
                    var missedproducts = product.MultipleSurfaceProduct.Where(a => !vendorlist.Any(x => x.VendorID == a.VendorID));
                    if (missedproducts.Count() == 0)
                    {
                        output.Add(new ProductListViewModel
                        {
                            ProductKey = product.ProductKey,
                            ProductID = product.ProductID,
                            VendorId = product.VendorId,
                            ProductName = product.ProductName,
                            VendorName = product.VendorName,
                            VendorProductSKU = product.VendorProductSKU,
                            ProductCategory = product.ProductCategory,
                            ProductSubCategory = product.ProductSubCategory,
                            Description = product.Description,
                            SalePrice = product.SalePrice,
                            FranchiseId = product.FranchiseId,
                            ProductStatus = product.ProductStatus
                        });
                    }
                }
                else
                {
                    output.Add(new ProductListViewModel
                    {
                        ProductKey = product.ProductKey,
                        ProductID = product.ProductID,
                        VendorId = product.VendorId,
                        ProductName = product.ProductName,
                        VendorName = product.VendorName,
                        VendorProductSKU = product.VendorProductSKU,
                        ProductCategory = product.ProductCategory,
                        ProductSubCategory = product.ProductSubCategory,
                        Description = product.Description,
                        SalePrice = product.SalePrice,
                        FranchiseId = product.FranchiseId,
                        ProductStatus = product.ProductStatus
                    });
                }
            }
            return output.OrderBy(x => x.ProductName).ToList();
        }

        public List<ProductTypeModel> GetProductType()
        {
            var VendorStatusList = ExecuteIEnumerableObject<ProductTypeModel>(ContextFactory.CrmConnectionString, @"select * from CRM.Type_ProductType where Id not in (1,5)").ToList();
            return VendorStatusList;
        }

        public dynamic GetAllPICProducts()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                try
                {
                    var query =
                  @"SELECT hp.ProductKey, hp.ProductID, hp.ProductName, hp.PICProductID, hp.VendorID, hv.Name as VendorName, hp.VendorProductSKU, hp.Description, hp.ProductCategory, hp.ProductCollection,
                          hp.ProductModelID, hp.ProductGroup, hp.ProductGroupDesc,hv.PICVendorId,hp.DiscontinuedDate,hp.PhasedoutDate
                          FROM crm.HFCProducts AS hp
                          inner join acct.HFCVendors hv on HP.Vendorid = hv.Vendorid
                          inner join acct.FranchiseVendors fv on fv.vendorid = hp.vendorid and fv.vendortype = 1 and fv.vendorstatus = 1 and hp.Active=1
						  where  fv.franchiseid = @franchiseid AND isNULL(hv.IsActive,0)<>0
						  group by hp.ProductKey, hp.ProductID, hp.ProductName, hp.PICProductID, hp.VendorID, hv.Name , hp.VendorProductSKU, hp.Description, hp.ProductCategory, hp.ProductCollection,
                          hp.ProductModelID,  hp.ProductGroup, hp.ProductGroupDesc,hv.PICVendorId,hp.DiscontinuedDate,hp.PhasedoutDate order by VendorName,hp.ProductGroupDesc,hp.ProductName ASC;";

                    var result = connection.Query<HFCProducts>(query,
                        new { franchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();
                    var pvpListDynamic = new List<dynamic>();

                    var vendorsproductGroup = result.Select(x => new { x.VendorID, x.ProductGroup, x.VendorName, x.ProductGroupDesc, x.PICVendorId }).Distinct().ToList();

                    foreach (var ven in vendorsproductGroup)
                    {
                        dynamic DyObj = new ExpandoObject();

                        DyObj.PICVendorId = ven.PICVendorId;
                        DyObj.VendorId = ven.VendorID;
                        DyObj.VendorName = ven.VendorName;
                        DyObj.PICProductGroupId = ven.ProductGroup;
                        DyObj.PICProductGroupName = ven.ProductGroupDesc;
                        var dyprdlist = new List<dynamic>();

                        foreach (var item in result)
                        {
                            if (item.VendorID == ven.VendorID && item.ProductGroup == ven.ProductGroup)
                            {
                                dynamic DypdObj = new ExpandoObject();
                                DypdObj.ProductName = item.ProductName;
                                DypdObj.PICProductId = item.PICProductID;
                                DypdObj.ProductId = item.ProductID;
                                DypdObj.PhasedoutDate = item.PhasedoutDate;
                                DypdObj.DiscontinuedDate = item.DiscontinuedDate;

                                if (!item.DiscontinuedDate.HasValue)
                                {
                                    DypdObj.IsActive = true;
                                }
                                else if ((item.DiscontinuedDate.HasValue && Convert.ToDateTime(item.DiscontinuedDate) > DateTime.Now))
                                {
                                    DypdObj.IsActive = true;
                                    DypdObj.Discontinued = "Product will be Discontinued from " + Convert.ToDateTime(item.DiscontinuedDate).GlobalDateFormat();
                                }
                                else if ((item.DiscontinuedDate.HasValue && Convert.ToDateTime(item.DiscontinuedDate) < DateTime.Now))
                                {
                                    DypdObj.IsActive = false;
                                    DypdObj.Discontinued = "Product is Discontinued on " + Convert.ToDateTime(item.DiscontinuedDate).GlobalDateFormat();
                                }
                                if (item.PhasedoutDate.HasValue)
                                {
                                    DypdObj.WarningPhasingOut = "Product is Phasing out from" + Convert.ToDateTime(item.PhasedoutDate.Value).GlobalDateFormat();
                                }
                                else
                                {
                                    DypdObj.WarningPhasingOut = "";
                                }
                                dyprdlist.Add(DypdObj);
                            }
                        }

                        DyObj.PICProductList = dyprdlist;
                        pvpListDynamic.Add(DyObj);
                    }
                    return pvpListDynamic;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public string GetAllPICBBProducts()
        {

            string CacheKey = "GetPICProductGroup_" + SessionManager.CurrentFranchise.FranchiseId.ToString();
            ObjectCache cache = MemoryCache.Default;

            if (cache.Contains(CacheKey))
            {
                return cache.Get(CacheKey).ToString();
            }
            else
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();

                    try
                    {
                        var query = @"CRM.[GetBBCoreProductModels] @franchiseid";

                        var result = connection.Query<string>(query,
                            new { franchiseId = SessionManager.CurrentFranchise.FranchiseId }).FirstOrDefault();
                        CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
                        cacheItemPolicy.SlidingExpiration = TimeSpan.FromHours(6);
                        cache.Add(CacheKey, result, cacheItemPolicy);

                        return result;
                    }
                    catch (Exception ex)
                    {
                        EventLogger.LogEvent(ex);
                        throw ex;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }

        public dynamic GetPICProductGroupForConfigurator(string franchiseCode, bool vendorconfig)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                try
                {
                    var query = "";
                    var result = new List<HFCProducts>();
                    if (!vendorconfig)
                    {
                        query = @"SELECT hp.ProductKey, hp.ProductID, hp.ProductName, hp.PICProductID, hp.VendorID, hv.Name as VendorName, hp.VendorProductSKU, hp.Description, hp.ProductCategory, hp.ProductCollection,
                          hp.ProductModelID, hp.ProductGroup, hp.ProductGroupDesc,hv.PICVendorId,hp.DiscontinuedDate,hp.PhasedoutDate
                          FROM crm.HFCProducts AS hp
                          inner join acct.HFCVendors hv on HP.Vendorid = hv.Vendorid
                          inner join acct.FranchiseVendors fv on fv.vendorid = hp.vendorid and fv.vendortype = 1 and fv.vendorstatus = 1 and hp.Active=1
						  INNER JOIN crm.Franchise f ON f.FranchiseId = fv.FranchiseId
						  where  f.Code = @franchiseCode AND isNULL(hv.IsActive,0)<>0
						  group by hp.ProductKey, hp.ProductID, hp.ProductName, hp.PICProductID, hp.VendorID, hv.Name , hp.VendorProductSKU, hp.Description, hp.ProductCategory, hp.ProductCollection,
                          hp.ProductModelID,  hp.ProductGroup, hp.ProductGroupDesc,hv.PICVendorId,hp.DiscontinuedDate,hp.PhasedoutDate order by VendorName,hp.ProductGroupDesc,hp.ProductName ASC;";

                        result = connection.Query<HFCProducts>(query,
                      new { franchiseCode = franchiseCode }).ToList();
                    }
                    else
                    {
                        query = @"SELECT hp.ProductKey, hp.ProductID, hp.ProductName, hp.PICProductID, hp.VendorID, hv.Name as VendorName, hp.VendorProductSKU, hp.Description, hp.ProductCategory, hp.ProductCollection,
                          hp.ProductModelID, hp.ProductGroup, hp.ProductGroupDesc,hv.PICVendorId,hp.DiscontinuedDate,hp.PhasedoutDate
                          FROM crm.HFCProducts AS hp
                          inner join acct.HFCVendors hv on HP.Vendorid = hv.Vendorid
                          inner join acct.FranchiseVendors fv on fv.vendorid = hp.vendorid and fv.vendortype = 1 and fv.vendorstatus = 1 and hp.Active=1
						  INNER JOIN crm.Franchise f ON f.FranchiseId = fv.FranchiseId
						  where  f.Code = @franchiseCode AND isNULL(hv.IsActive,0)<>0 and
                          hv.VendorId = (select VendorId from Acct.HFCVendors where VendorIdPk in (select VendorId from CRM.VendorCaseConfig where CaseLogin like @CaseLogin))
						  group by hp.ProductKey, hp.ProductID, hp.ProductName, hp.PICProductID, hp.VendorID, hv.Name , hp.VendorProductSKU, hp.Description, hp.ProductCategory, hp.ProductCollection,
                          hp.ProductModelID,  hp.ProductGroup, hp.ProductGroupDesc,hv.PICVendorId,hp.DiscontinuedDate,hp.PhasedoutDate order by VendorName,hp.ProductGroupDesc,hp.ProductName ASC;";

                        result = connection.Query<HFCProducts>(query,
                      new { franchiseCode = franchiseCode, CaseLogin = SessionManager.CurrentUser.UserName }).ToList();
                    }

                    var pvpListDynamic = new List<dynamic>();

                    var vendorsproductGroup = result.Select(x => new { x.VendorID, x.ProductGroup, x.VendorName, x.ProductGroupDesc, x.PICVendorId }).Distinct().ToList();

                    foreach (var ven in vendorsproductGroup)
                    {
                        dynamic DyObj = new ExpandoObject();

                        DyObj.PICVendorId = ven.PICVendorId;
                        DyObj.VendorId = ven.VendorID;
                        DyObj.VendorName = ven.VendorName;
                        DyObj.PICProductGroupId = ven.ProductGroup;
                        DyObj.PICProductGroupName = ven.ProductGroupDesc;
                        var dyprdlist = new List<dynamic>();

                        foreach (var item in result)
                        {
                            if (item.VendorID == ven.VendorID && item.ProductGroup == ven.ProductGroup)
                            {
                                dynamic DypdObj = new ExpandoObject();
                                DypdObj.ProductName = item.ProductName;
                                DypdObj.PICProductId = item.PICProductID;
                                DypdObj.ProductId = item.ProductID;
                                DypdObj.PhasedoutDate = item.PhasedoutDate;
                                DypdObj.DiscontinuedDate = item.DiscontinuedDate;

                                if (!item.DiscontinuedDate.HasValue)
                                {
                                    DypdObj.IsActive = true;
                                }
                                else if ((item.DiscontinuedDate.HasValue && Convert.ToDateTime(item.DiscontinuedDate) > DateTime.Now))
                                {
                                    DypdObj.IsActive = true;
                                    DypdObj.Discontinued = "Product will be Discontinued from " + Convert.ToDateTime(item.DiscontinuedDate).GlobalDateFormat();
                                }
                                else if ((item.DiscontinuedDate.HasValue && Convert.ToDateTime(item.DiscontinuedDate) < DateTime.Now))
                                {
                                    DypdObj.IsActive = false;
                                    DypdObj.Discontinued = "Product is Discontinued on " + Convert.ToDateTime(item.DiscontinuedDate).GlobalDateFormat();
                                }
                                if (item.PhasedoutDate.HasValue)
                                {
                                    DypdObj.WarningPhasingOut = "Product is Phasing out from" + Convert.ToDateTime(item.PhasedoutDate.Value).GlobalDateFormat();
                                }
                                else
                                {
                                    DypdObj.WarningPhasingOut = "";
                                }
                                dyprdlist.Add(DypdObj);
                            }
                        }

                        DyObj.PICProductList = dyprdlist;
                        pvpListDynamic.Add(DyObj);
                    }
                    return pvpListDynamic;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public List<FranchiseProducts> GetMaterialForTLorCCJobSurfacing()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var res = connection.Query<FranchiseProducts>(@"select ProductKey,ProductID, ProductName, [Description], MultipleSurfaceProductSet from CRM.FranchiseProducts  where FranchiseId=@FranchiseId and ProductType = @ProductType and MultipartSurfacing=@MultipartSurfacing and ProductStatus=1",
                          new { FranchiseId = SessionManager.CurrentUser.FranchiseId, ProductType = 2, MultipartSurfacing = 1 }).ToList();
                return res;
            }
        }

        public List<FranchiseProducts> GetLaborForTLorCCJobSurfacing()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                string productCategory = "";
                int ProductCategoryId = 0;
                if (SessionManager.CurrentFranchise.BrandId == 2)
                {
                    productCategory = "Flooring System Installation";
                    ProductCategoryId = 128;

                }
                else if (SessionManager.CurrentFranchise.BrandId == 3)
                {
                    productCategory = "Concrete System Installation";
                    ProductCategoryId = 134;
                }

                var res = connection.Query<FranchiseProducts>(@" select * from CRM.FranchiseProducts where
                          FranchiseId=@FranchiseId and ProductType = @ProductType and MultipartSurfacing=0 and ProductCategory= @ProductCategoryId and
                          ProductSubCategory = (select ProductCategoryId from [CRM].[Type_ProductCategory] where parant=@ProductCategoryId and ProductCategory=@ProductCategory)",
                          new { FranchiseId = SessionManager.CurrentUser.FranchiseId, ProductType = 3, ProductCategory = productCategory, ProductCategoryId = ProductCategoryId }).ToList();
                return res;
            }
        }

        public bool CreateModelCache()
        {
            List<string> cacheKeys = MemoryCache.Default.Select(x => x.Key).ToList().Where(x => x.Contains("GetPICProductGroup_")).ToList();
            foreach (string cacheKey in cacheKeys)
            {
                MemoryCache.Default.Remove(cacheKey);
            }
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var res = connection.Query<Franchise>(@"SELECT * FROM crm.Franchise f WHERE f.BrandId=1").ToList();
                foreach (var item in res)
                {
                    string CacheKey = "GetPICProductGroup_" + item.FranchiseId.ToString();
                    ObjectCache cache = MemoryCache.Default;

                    try
                    {
                        var query = @"CRM.[GetBBCoreProductModels] @franchiseid";

                        var result = connection.Query<string>(query,
                            new { franchiseId = item.FranchiseId }).FirstOrDefault();
                        CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
                        cacheItemPolicy.SlidingExpiration = TimeSpan.FromHours(6);
                        cache.Add(CacheKey, result, cacheItemPolicy);
                    }
                    catch (Exception ex)
                    {
                        EventLogger.LogEvent(ex);
                    }
                }
                return true;
            }
        }
    }
}