﻿using Dapper;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using HFC.CRM.DTO.Source;
using System;
using System.Data.SqlClient;
using System.Linq;
using HFC.CRM.Core.Common;
using System.Collections.Generic;

namespace HFC.CRM.Managers
{
    public class SourceManager
    {

        public string AddSourceHFC(SourcesHFC data)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    //var result = "";
                    if (data.LMDBSourceID != null)
                    {
                        var Query = @"select * from CRM.SourcesHFC where LMDBSourceID=@lmdbid";
                        var result = connection.Query<dynamic>(Query, new { lmdbid = data.LMDBSourceID }).FirstOrDefault();
                        if (result == null)
                        {
                            var query = @"INSERT INTO CRM.SourcesHFC 
                                 (OwnerId,ChannelId,
                                 SourceId,SourcesTPId,
                                 AllowFranchisetoReassign,IsActive,IsDeleted,LeadType,LMDBSourceID)  
                                 VALUES(@OwnerId,@ChannelId,
                                 @SourceId,@SourcesTPId,@AllowFranchisetoReassign,@IsActive,@IsDeleted,@LeadType,@LMDBSourceID)";
                            connection.Execute(query, data);
                            return "Success";
                        }
                        else return "Duplicate HFC SourceID";

                    }
                    else
                    {
                        var query = @"INSERT INTO CRM.SourcesHFC 
                                 (OwnerId,ChannelId,
                                 SourceId,SourcesTPId,
                                 AllowFranchisetoReassign,IsActive,IsDeleted,LeadType,LMDBSourceID)  
                                 VALUES(@OwnerId,@ChannelId,
                                 @SourceId,@SourcesTPId,@AllowFranchisetoReassign,@IsActive,@IsDeleted,@LeadType,@LMDBSourceID)";
                        connection.Execute(query, data);
                        return "Success";
                    }
                }
                //return true;
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public bool AddSourceTP(SourcesTP data)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var query = @"INSERT INTO CRM.SourcesTP 
                                 (OwnerId,ChannelId,
                                 SourceId,
                                 IsActive,IsDeleted)  
                                 VALUES(@OwnerId,@ChannelId,
                                 @SourceId,@IsActive,@IsDeleted)";
                    connection.Execute(query, data);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public string AddSource(SourceDTO data)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var Query = @"select * from CRM.Sources where Name=@Name";
                    var result= connection.Query<dynamic>(Query, new { Name = data.Name}).FirstOrDefault();
                    if (result == null)
                    {
                        var query = @"Insert into CRM.Sources 
                                 (Name,IsActive,isDeleted,CreatedOn,StartDate) 
                                  values (@Name,@IsActive,@isDeleted,@CreatedOn,@StartDate)";

                        connection.Execute(query, new
                        {
                            Name = data.Name,
                            IsActive = data.IsActive,
                            isDeleted = false,
                            CreatedOn = DateTime.UtcNow,
                            StartDate = DateTime.UtcNow
                        });
                        return "Success";
                    }
                    else
                        return "Source Name Exists Already";
                        //return ContantStrings.DuplicateFileName;
                }
                //return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string AddType_Channel(Type_Channel data)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    var queryVal = @"select * from CRM.Type_Channel where Name=@Name";
                    var result = connection.Query<dynamic>(queryVal, new { Name = data.Name }).FirstOrDefault();
                    if (result == null)
                    {
                        var query = @"Insert into CRM.Type_Channel 
                                  (Name,CreatedOn,IsActive,IsDeleted)
                                   Values(@Name,@CreatedOn,@IsActive,@IsDeleted)";
                        connection.Execute(query, new
                        {
                            Name = data.Name,
                            CreatedOn = DateTime.UtcNow,
                            IsActive = data.IsActive,
                            IsDeleted = false
                        });
                        return "Success";
                    }
                    else
                        return "Channel Name Exists Already";
                        //return ContantStrings.DuplicateFileName;
                }
                
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool AddType_Owner(Type_Owner data)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    var query = @"Insert into CRM.Type_Owner
                                (Name,CreatedOn,CreatedBy,IsActive,IsDeleted) 
                                values(@Name,@CreatedOn,@CreatedBy,@IsActive,@IsDeleted)";
                    connection.Execute(query, new
                    {
                        Name = data.Name,
                        CreatedOn = DateTime.UtcNow,
                        CreatedBy = ContantStrings.AdminPersonId,    //Created by Admin User
                        IsActive = data.IsActive,
                        IsDeleted = false
                    });
                }
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string UpdateSourceHFC(SourcesHFC data)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    var query = "select * from CRM.SourcesHFC where LMDBSourceID=@lmdbid and SourcesHFCId!=@Sourcehfcid";
                    var result = connection.Query<dynamic>(query, new { lmdbid = data.LMDBSourceID, Sourcehfcid = data.SourcesHFCId }).FirstOrDefault();
                    if (result == null)
                    {
                        string updateQuery = @"update CRM.SourcesHFC 
                                           SET ChannelId = @ChannelId, OwnerId = @OwnerId,
                                           SourceId = @SourceId, SourcesTPId = @SourcesTPId,
                                           AllowFranchisetoReassign = @AllowFranchisetoReassign,
                                           IsActive = @IsActive, IsDeleted = @IsDeleted ,
                                           LeadType=@LeadType,
                                           LMDBSourceID=@LMDBSourceID
                                           where SourcesHFCId=@SourcesHFCId";
                        connection.Execute(updateQuery, data);
                        return "Success";
                    }
                    else return "Duplicate HFC SourceID";
                    
                }
                //return "Updated Successfully";
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string UpdateSourceTP(SourcesTP data)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    string updateQuery = @"update CRM.SourcesTP SET OwnerId = @OwnerId,ChannelId=@ChannelId,
                                           SourceId=@SourceId,IsActive=@IsActive,IsDeleted=@IsDeleted
                                           where SourcesTPId=@SourcesTPId ";
                    var result = connection.Execute(updateQuery, data);
                }
                return "Updated Successfully";
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string UpdateSource(SourceDTO data)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var query = @"select * from CRM.Sources where Name=@Name and SourceId !=@sourceid";
                    var result = connection.Query<dynamic>(query, new { Name = data.Name, sourceid = data.SourceId }).FirstOrDefault();
                    if (result == null)
                    {
                        var updatequery = @"Update CRM.Sources
                                        SET Name=@Name,IsActive=@IsActive,UpdatedOn=@UpdatedOn
                                        where SourceId=@SourceId";
                        connection.Execute(updatequery, new
                        {
                            SourceId = data.SourceId,
                            Name = data.Name,
                            IsActive = data.IsActive,
                            UpdatedOn = DateTime.UtcNow
                        });
                        return "Success";
                    }
                    else
                        return "Source Name Exists Already";
                    //return ContantStrings.DuplicateFileName;

                }
                //return "Updated Successfully";
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public string UpdateType_Channel(Type_Channel data)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var queryVal = @"select * from CRM.Type_Channel where Name=@Name and ChannelId != @Channelid";
                    var result = connection.Query<dynamic>(queryVal, new { Name = data.Name, Channelid=data.ChannelId }).FirstOrDefault();
                    if (result == null)
                    {
                        var updatequery = @"Update CRM.Type_Channel
                                        Set Name=@Name,IsActive=@IsActive,UpdatedOn=@UpdatedOn
                                        where ChannelId=@ChannelId";
                        connection.Execute(updatequery, new
                        {
                            ChannelId = data.ChannelId,
                            Name = data.Name,
                            IsActive = data.IsActive,
                            UpdatedOn = DateTime.UtcNow
                        });
                        return "Success";
                    }
                    else
                        return "Channel Name Exists Already";
                        //return ContantStrings.DuplicateFileName;
                }
                //return "Updated Successfully";
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public string UpdateType_Owner(Type_Owner data)
        {
            try
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var updatequery = @"Update CRM.Type_Owner
                                        Set Name=@Name,IsActive=@IsActive,UpdatedOn=@UpdatedOn
                                        where OwnerId=@OwnerId";
                    connection.Execute(updatequery, new
                    {
                        OwnerId = data.OwnerId,
                        Name = data.Name,
                        IsActive = data.IsActive,
                        UpdatedOn = DateTime.UtcNow
                    });
                }
                return "Updated Successfully";
            }
            catch (Exception ex)
            {
                throw;
            }

        }
        public dynamic GetInactiveSources()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    string query = @"SELECT st.SourcesTPId,
                                    st.OwnerId, st.ChannelId, st.SourceId, 
                                    ow.[Name] as [Owner],
                                    tc.[Name] AS Channel,
                                    s.[Name] as [Source], 
                                    st.IsActive,st.IsDeleted
                                    FROM CRM.SourcesTP st 
                                    INNER JOIN CRM.Sources s ON s.sourceId = st.sourceId 
                                    INNER JOIN CRM.Type_Channel tc ON tc.ChannelId = st.ChannelId 
                                    INNER JOIN CRM.Type_Owner ow ON ow.ownerid = st.ownerid";

                    return con.Query(query).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public dynamic GetSourcesTP()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    string query = @"SELECT st.SourcesTPId,
                                    st.OwnerId, st.ChannelId, st.SourceId, 
                                    ow.[Name] as [Owner],
                                    tc.[Name] AS Channel,
                                    s.[Name] as [Source], 
                                    st.IsActive,st.IsDeleted
                                    FROM CRM.SourcesTP st 
                                    INNER JOIN CRM.Sources s ON s.sourceId = st.sourceId 
                                    INNER JOIN CRM.Type_Channel tc ON tc.ChannelId = st.ChannelId 
                                    INNER JOIN CRM.Type_Owner ow ON ow.ownerid = st.ownerid
                                    where st.IsActive=1";

                    return con.Query(query).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public dynamic GetGlobaldocList()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    string query = @"SELECT * from CRM.GlobalDocuments order by UpdatedOn desc";

                    return con.Query(query).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public dynamic GetStreamIdForDocument(int id)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    string query = @"SELECT * from CRM.GlobalDocuments where Id=@Id";

                    return con.Query(query, new { Id = id }).ToList();
                    
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public dynamic GetOwnerSources()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    string query = @"select OwnerId,Name as Owner from CRM.Type_Owner order by Name Asc";

                    return con.Query(query).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public dynamic GetChannelSources()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    string query = @"select ChannelId,Name as Channel from CRM.Type_Channel where IsActive=1 order by Name Asc";

                    return con.Query(query).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public dynamic GetSource()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    string query = @"select SourceId,Name as Source from CRM.Sources where IsActive=1 order by Name Asc";

                    return con.Query(query).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public dynamic GetSourcesHFC()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    string query = @"SELECT sHFC.SourcesHFCId,
                                     sHFC.OwnerId,sHFC.ChannelId,sHFC.SourceId, 
                                     ow.[Name] as [Owner], 
                                     tc.[Name] AS Channel,
                                     s.[Name] as [Source], 
                                     sHFC.IsActive,sHFC.IsDeleted
                                     FROM CRM.SourcesHFC sHFC 
                                     INNER JOIN CRM.Sources s ON s.sourceId = sHFC.sourceId 
                                     INNER JOIN CRM.Type_Channel tc ON tc.ChannelId = sHFC.ChannelId 
                                     INNER JOIN CRM.Type_Owner ow ON ow.ownerid = sHFC.ownerid";
                    return con.Query(query).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public dynamic GetSourcesHFCmapTP()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    string query = @"SELECT sHFC.SourcesHFCId,sHFC.SourcesTPId,
                                    sHFC.OwnerId as OwnerIdHFC, 
                                    sHFC.ChannelId as ChannelIdHFC,
                                    sHFC.SourceId as SourceIdHFC, 
                                    owHFC.[Name] as [OwnerHFC], 
                                    tcHFC.[Name] AS ChannelHFC,
                                    ssHFC.[Name] as [SourceHFC], 
                                    sTP.OwnerId as OwnerIdTP, 
                                    sTP.ChannelId as ChannelIdTP, 
                                    sTP.SourceId as SourceIdTP,
                                    owTP.[Name] as [OwnerTP],
                                    tcTP.[Name] AS ChannelTP, 
                                    ssTP.[Name] as [SourceTP],
                                    owTP.[Name]+'->'+tcTP.[Name]+'->'+ssTP.[Name] as TPTSource,
                                    sHFC.AllowFranchisetoReassign,
                                    sHFC.IsActive,sHFC.IsDeleted,sHFC.LeadType,sHFC.LMDBSourceID
                                    FROM CRM.SourcesHFC sHFC 
                                    INNER JOIN CRM.SourcesTP sTP ON sTP.SourcesTPId=sHFC.SourcesTPId
                                    INNER JOIN CRM.Sources ssHFC ON ssHFC.sourceId = sHFC.sourceId 
                                    INNER JOIN CRM.Type_Channel tcHFC ON tcHFC.ChannelId = sHFC.ChannelId 
                                    INNER JOIN CRM.Type_Owner owHFC ON owHFC.ownerid = sHFC.OwnerId
                                    INNER JOIN CRM.Sources ssTP ON ssTP.sourceId = sTP.sourceId 
                                    INNER JOIN CRM.Type_Channel tcTP ON tcTP.ChannelId = sTP.ChannelId 
                                    INNER JOIN CRM.Type_Owner owTP ON owTP.ownerid = sTP.OwnerId where sHFC.IsActive=1
                                    order by sHFC.SourcesHFCId";
                    return con.Query(query).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public dynamic GetInactiveSourcesHFCmapTP()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    string query = @"SELECT sHFC.SourcesHFCId,sHFC.SourcesTPId,
                                    sHFC.OwnerId as OwnerIdHFC, 
                                    sHFC.ChannelId as ChannelIdHFC,
                                    sHFC.SourceId as SourceIdHFC, 
                                    owHFC.[Name] as [OwnerHFC], 
                                    tcHFC.[Name] AS ChannelHFC,
                                    ssHFC.[Name] as [SourceHFC], 
                                    sTP.OwnerId as OwnerIdTP, 
                                    sTP.ChannelId as ChannelIdTP, 
                                    sTP.SourceId as SourceIdTP,
                                    owTP.[Name] as [OwnerTP],
                                    tcTP.[Name] AS ChannelTP, 
                                    ssTP.[Name] as [SourceTP],
                                    owTP.[Name]+'->'+tcTP.[Name]+'->'+ssTP.[Name] as TPTSource,
                                    sHFC.AllowFranchisetoReassign,
                                    sHFC.IsActive,sHFC.IsDeleted,sHFC.LeadType,sHFC.LMDBSourceID
                                    FROM CRM.SourcesHFC sHFC 
                                    INNER JOIN CRM.SourcesTP sTP ON sTP.SourcesTPId=sHFC.SourcesTPId
                                    INNER JOIN CRM.Sources ssHFC ON ssHFC.sourceId = sHFC.sourceId 
                                    INNER JOIN CRM.Type_Channel tcHFC ON tcHFC.ChannelId = sHFC.ChannelId 
                                    INNER JOIN CRM.Type_Owner owHFC ON owHFC.ownerid = sHFC.OwnerId
                                    INNER JOIN CRM.Sources ssTP ON ssTP.sourceId = sTP.sourceId 
                                    INNER JOIN CRM.Type_Channel tcTP ON tcTP.ChannelId = sTP.ChannelId 
                                    INNER JOIN CRM.Type_Owner owTP ON owTP.ownerid = sTP.OwnerId
                                    order by sHFC.SourcesHFCId";
                    return con.Query(query).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public dynamic GetTPTSources()
        {
         try
            {
                using (SqlConnection con = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    string query = @"SELECT st.SourcesTPId,
                                     ow.[Name]+'->'+tc.[Name]+'->'+s.[Name] as TPTSource
                                     FROM CRM.SourcesTP st 
                                     INNER JOIN CRM.Sources s ON s.sourceId = st.sourceId 
                                     INNER JOIN CRM.Type_Channel tc ON tc.ChannelId = st.ChannelId 
                                     INNER JOIN CRM.Type_Owner ow ON ow.ownerid = st.ownerid order by ow.[Name],tc.[Name],s.[Name] Asc";
                    return con.Query(query).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public dynamic GetTPTOwnerSources()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    string query = @"select OwnerId as OwnerIdHFC,Name as OwnerHFC from CRM.Type_Owner order by Name Asc";

                    return con.Query(query).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public dynamic GetTPTChannelSources()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    string query = @"select ChannelId as ChannelIdHFC,Name as ChannelHFC from CRM.Type_Channel where IsActive=1 order by Name Asc";

                    return con.Query(query).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public dynamic GetTPTListSource()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    string query = @"select SourceId as SourceIdHFC,Name as SourceHFC from CRM.Sources where IsActive=1 order by Name Asc";

                    return con.Query(query).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public dynamic GetLeadSourcesmapTP(int id)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    string query = @"select Id,UTM.UTMCode,UTM.SourcesHFCId,CONVERT(date,UTM.StartDate) StartDate,CONVERT(date,UTM.EndDate) EndDate,
                                     case when UTM.Amount is null then 0.00 else UTM.Amount end as Amount,UTM.Memo,UTM.IsActive,
                                    SHF.OwnerId,SHF.ChannelId,SHF.SourceID,
                                    typo.Name+' -> '+typc.Name+' -> '+stp.Name as HFCSourceName
                                    from [CRM].[UTMCampaign] UTM
                                    inner join CRM.SourcesHFC SHF on UTM.SourcesHFCId=SHF.SourcesHFCId
                                    inner join CRM.Type_Channel typc on typc.ChannelId=SHF.ChannelId
                                    inner join CRM.Type_Owner typo on typo.OwnerId=SHF.OwnerId
                                    inner join CRM.Sources stp on stp.SourceId=SHF.SourceId
                                    where UTM.IsDeleted=0 and UTM.IsActive=1";
                    return con.Query(query).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public dynamic GetUTMSourcesmap(int id)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    string query = @"select Id,UTM.UTMCode,UTM.SourcesHFCId,UTM.StartDate,UTM.EndDate,
                                    UTM.Amount,UTM.Memo,UTM.IsActive,
                                    SHF.OwnerId,SHF.ChannelId,SHF.SourceID,
                                    typo.Name+' -> '+typc.Name+' -> '+stp.Name as HFCSourceName
                                    from [CRM].[UTMCampaign] UTM
                                    inner join CRM.SourcesHFC SHF on UTM.SourcesHFCId=SHF.SourcesHFCId
                                    inner join CRM.Type_Channel typc on typc.ChannelId=SHF.ChannelId
                                    inner join CRM.Type_Owner typo on typo.OwnerId=SHF.OwnerId
                                    inner join CRM.Sources stp on stp.SourceId=SHF.SourceId
                                    where UTM.IsDeleted=0";
                    return con.Query(query).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public string SaveUtmCampgn(UTMCampaign model)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                if (model != null)
                {
                    if (model.Id == 0)
                    {
                        model.IsDeleted = false;

                        model.CreatedBy = ContantStrings.AdminPersonId;
                        var UTMId = connection.Insert(model);
                        if (UTMId != null)
                        {
                            var SourceUpdate = UpdateLeadSourceId(model.UTMCode,model.SourcesHFCId);
                            return "Success";
                        }
                            
                    }
                    else {
                        var data = GetUTMCampaign(model.Id);
                        data.IsDeleted = false;
                        data.LastUpdatedBy = ContantStrings.AdminPersonId;
                        data.Amount = model.Amount;
                        data.EndDate = model.EndDate;
                        data.IsActive = model.IsActive;
                        data.Memo = model.Memo;
                        data.SourcesHFCId = model.SourcesHFCId;
                        data.StartDate = model.StartDate;
                        data.UTMCode = model.UTMCode;
                        UpdateUTMCampaign(data);
                        var SourceUpdate = UpdateLeadSourceId(model.UTMCode,model.SourcesHFCId);
                        return "Success";
                    }
                   
                   
                    
                }
                return null;
            }
        }

        public string UpdateUTMCampaign(UTMCampaign data)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                if (data.Id != 0)
                {
                    var updatedata = connection.Update(data);
                    return "Success";
                }
                else
                    return "Failed";
            }

        }

        public UTMCampaign GetUTMCampaign(int Id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                return connection.Get<UTMCampaign>(Id);
            }

        }

        public string UpdateLeadSourceId(string UTMCode,int SourcesHFCId)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    string query = @"select laf.* from CRM.LeadAdditionalInfo  laf
                                    INNER JOIN crm.Leads l ON l.leadid = laf.LeadId AND ISNULL(l.HFCUTMCampaignCode,'')=''
                                    where Value like @UtmCode+'%'";
                    var result = con.Query<LeadAdditionalInfo>(query, new { UtmCode = UTMCode }).ToList();
                    if (result.Count != 0)
                    {
                        var updateLead = @"update l set l.SourcesHFCId=@SourcesHfcId,l.HFCUTMCampaignCode=@UtmCode  from CRM.LeadAdditionalInfo la inner join CRM.Leads l 
                                           on la.LeadId=l.LeadId  AND ISNULL(l.HFCUTMCampaignCode,'')='' WHERE la.Name='Campaign' AND la.Value LIKE @UtmCode+'%'";
                        var updateAccount = @"update a set a.SourcesHFCId=@SourcesHfcId,a.HFCUTMCampaignCode= @UtmCode  from CRM.LeadAdditionalInfo la inner join CRM.Accounts a
                                              on la.LeadId=a.LeadId AND ISNULL(a.HFCUTMCampaignCode,'')='' where la.Name='Campaign'  AND la.Value LIKE @UtmCode+'%'";
                        var Leadresult = con.Execute(updateLead, new { UtmCode = UTMCode, SourcesHfcId = SourcesHFCId });
                        var Accountresult = con.Execute(updateAccount, new { UtmCode = UTMCode, SourcesHfcId= SourcesHFCId });
                        return "Success";
                    }
                    return "Success";
                }
            }
            catch(Exception ex)
            {
                throw;
            }
        }

    }
}
