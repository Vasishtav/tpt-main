﻿using System;
using System.Linq;
using DocuSign.Integrations.Client;
using HFC.CRM.Core.Common;

namespace HFC.CRM.Managers.Docusign
{
    public class ESignatureManager
    {
        private const string AccountEmail = "3609783@gmail.com";
        private const string  AccountPassword = "19832401";
        protected const string IntegratorKey = "TEST-f46631ed-fd6b-4e78-b537-13c0e9240c8f";
        protected const string Environment = "http://demo.docusign.net"; 

        public ESignatureManager()
        {
            RestSettings.Instance.IntegratorKey = IntegratorKey;
            RestSettings.Instance.DocuSignAddress = Environment;
            RestSettings.Instance.WebServiceUrl = Environment + "/restapi/v2";
        }

        public bool RequestSignatureOnDocument(byte[] bytes, string fileName, string recipientName, string recipientEmail, out string envelopeId)
        {
            envelopeId = null;

            var account = new Account { Email = AccountEmail, Password = AccountPassword };

            bool result = account.Login();
            if (!result)
            {
                throw new Exception(string.Format("Docusign login API call failed for user {0}.\nError Code:  {1}\nMessage:  {2}", account.Email, account.RestError.errorCode, account.RestError.message));
            }

            var envelope = new Envelope
                           {
                               Login = account,
                               
                               Recipients = new Recipients()
                               {
                                   
                                   signers = new Signer[] { new Signer() { email = recipientEmail, name = recipientName, routingOrder = "1", recipientId = "1" ,   tabs = new Tabs() {  
                                       dateSignedTabs = new Tab[]
                                                        {
                                                            new Tab()
                                                            {
                                                                anchorString = "DATE:", 
                                                                                        anchorUnits = "inches", 
                                                                                        anchorXOffset = 1, 
                                                                                        anchorYOffset = 0,
                                                                                        documentId = 1,
                                                                                        recipientId = "1",
                                                                                        required = true
                                                            },
                                                        },
                                       signHereTabs = new Tab[] { new Tab()
                                                                                    {
                                                                                        anchorString = "SIGN HERE AND RETURN:", 
                                                                                        anchorUnits = "inches", 
                                                                                        anchorXOffset = 2, 
                                                                                        anchorYOffset = 0,
                                                                                        documentId = 1,
                                                                                        recipientId = "1",
                                                                                        required = true
                                                                                        
                                                                                    } } }} }
                               },
                               Status = "sent",
                               EmailSubject = "DocuSign .NET Client - Signature Request on Document",
                               
                               
                               
                           };

            result = envelope.Create(bytes, fileName);

            if (!result)
            {
                if (envelope.RestError != null)
                {
                    throw new Exception(string.Format("Docusign error code:  {0}\nMessage:  {1}", envelope.RestError.errorCode, envelope.RestError.message));
                }
                
                throw new Exception("Error encountered while requesting signature from template, please review your envelope and recipient data.");
            }

            envelopeId = envelope.EnvelopeId;
            return true;
        }

        public string GetStatus(string envelopeId, out DateTime sentTime)
        {
            var account = new Account { Email = AccountEmail, Password = AccountPassword };

            bool result = account.Login();
            if (!result)
            {
                throw new Exception(string.Format("Docusign login API call failed for user {0}.\nError Code:  {1}\nMessage:  {2}", account.Email, account.RestError.errorCode, account.RestError.message));
            }

            var envelope = new Envelope { Login = account };

            sentTime = envelope.GetStatus(envelopeId);

            if (envelope.RestError != null)
            {
                throw new Exception(string.Format("Error code:  {0}\nMessage:  {1}", envelope.RestError.errorCode, envelope.RestError.message));
              
            }

            return envelope.Status;
        }

        public void DownloadDocument(int jobId, string envelopeId)
        {
            var account = new Account { Email = AccountEmail, Password = AccountPassword };

            bool result = account.Login();
            if (!result)
            {
                throw new Exception(string.Format("Docusign login API call failed for user {0}.\nError Code:  {1}\nMessage:  {2}", account.Email, account.RestError.errorCode, account.RestError.message));
            }

            var envelope = new Envelope { Login = account };

            var docNames = envelope.GetDocNames(envelopeId);

            byte[] completedDocBytes = envelope.GetCompletedDocument(envelopeId, true);

            //var JobMgr = new JobsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

            //int fileId;
            //string fileName;
            //string url;

            //JobMgr.UploadFile(jobId, "Job Quotes", "Signed Complted", completedDocBytes, docNames.First(), out fileId, out fileName, out url);

            if (envelope.RestError != null)
            {
                throw new Exception(string.Format("Error code:  {0}\nMessage:  {1}", envelope.RestError.errorCode, envelope.RestError.message));

            }
        }
    }
}