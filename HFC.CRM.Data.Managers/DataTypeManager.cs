﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HFC.CRM.Data;

namespace HFC.CRM.Operations
{
    public class DataTypeManager
    {
        public static List<Type_Appointment> AppointmentTypeCollection
        {
            get 
            {
                LocalCacheManager cache = new LocalCacheManager();
                if (cache.AppointmentTypeCollection == null)
                {
                    using (var db = new HFCCRMEntities())
                    {                     
                        cache.AppointmentTypeCollection = db.Type_Appointment.ToList();
                        //clear appointments property so it is not serialized
                        cache.AppointmentTypeCollection.ForEach(e => e.Appointments = null);
                    }
                }
                return cache.AppointmentTypeCollection;
            }
        }

        public static List<Type_LeadStatus> LeadStatusTypeCollection
        {
            get
            {
                LocalCacheManager cache = new LocalCacheManager();
                if (cache.LeadStatusTypeCollection == null)
                {
                    using (var db = new HFCCRMEntities())
                    {
                        cache.LeadStatusTypeCollection = db.Type_LeadStatus.ToList();
                        //clear appointments property so it is not serialized
                        cache.LeadStatusTypeCollection.ForEach(e => e.Leads = null);
                    }
                }
                return cache.LeadStatusTypeCollection;
            }
        }
    }
}
