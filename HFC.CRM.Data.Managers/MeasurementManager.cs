﻿using Dapper;
using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;
using System.Web.Helpers;

namespace HFC.CRM.Managers
{
    public class MeasurementManager : DataManager<MeasurementHeader>
    {
        private static AccountsManager acctMgr = new AccountsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        LeadsManager LeadMgr = new LeadsManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        public MeasurementManager(User user, Franchise franchise) : this(user, user, franchise)
        {
        }
        public MeasurementManager(User user, User authorizingUser, Franchise franchise)
        {
            this.User = user;
            this.AuthorizingUser = authorizingUser;
            this.Franchise = franchise;
        }
        int brandId = SessionManager.BrandId;

        public override string Add(MeasurementHeader data)
        {

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                var measurementId = connection.Insert(data);

                connection.Close();

            }
            return null;
            //throw new NotImplementedException();
        }

        public override string Delete(int id)
        {
            throw new NotImplementedException();
        }

        public override MeasurementHeader Get(int id)
        {
            // id = 1;
            if (id > 0)
            {
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    var query = "select * from [CRM].[MeasurementHeader] where InstallationAddressId = @installationAddressId";
                    var result = connection.Query<MeasurementHeader>(query, new { installationAddressId = id }).FirstOrDefault();
                    brandId = SessionManager.BrandId;
                    if (result != null)
                    {
                        if (brandId == 1)
                        {
                            var response = new List<MeasurementLineItemBB>();
                            if (result != null && result.MeasurementSet != null)
                            {
                                response = JsonConvert.DeserializeObject<List<MeasurementLineItemBB>>(result.MeasurementSet);
                                result.MeasurementBB = response;
                            }
                        }
                        else if (brandId == 2)
                        {
                            var response = new List<MeasurementLineItemTL>();
                            if (result != null && result.MeasurementSet != null)
                            {
                                response = JsonConvert.DeserializeObject<List<MeasurementLineItemTL>>(result.MeasurementSet);
                                result.MeasurementTL = response;
                            }
                        }
                        else if (brandId == 3)
                        {
                            var response = new List<MeasurementLineItemCC>();
                            if (result != null && result.MeasurementSet != null)
                            {
                                response = JsonConvert.DeserializeObject<List<MeasurementLineItemCC>>(result.MeasurementSet);
                                result.MeasurementCC = response;
                            }
                        }
                    }
                    connection.Close();
                    return result;
                }
            }
            return null;
        }

        public override ICollection<MeasurementHeader> Get(List<int> idList)
        {
            throw new NotImplementedException();
        }

        public override string Update(MeasurementHeader data)
        {
            if (brandId == 1)
                data.MeasurementSet = JsonConvert.SerializeObject(data.MeasurementBB);
            else if (brandId == 2)
                data.MeasurementSet = JsonConvert.SerializeObject(data.MeasurementTL);
            else if (brandId == 3)
                data.MeasurementSet = JsonConvert.SerializeObject(data.MeasurementCC);

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                // check the data with opportunityid is available or not
                var query = "select * from [CRM].[MeasurementHeader] where InstallationAddressId = @installationAddressId";
                var result = connection.Query<MeasurementHeader>(query, new { installationAddressId = data.InstallationAddressId }).FirstOrDefault();

                //commmon data for both add and update
                data.CreatedOnUtc = DateTime.Now;
                data.LastUpdatedOnUtc = DateTime.Now;
                data.CreatedByPersonId = User.PersonId;


                if (data.Id == 0 && result != null)
                {
                    data.Id = result.Id;
                }
                if (result == null)
                {

                    this.Add(data);
                }
                else
                {
                    var measurementId = connection.Update(data);

                }
                connection.Close();
            }
            return null;
        }

        public AccountTP GetAccountInfo(int? opportunityId)
        {
            AccountTP account;
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                // check the data with opportunityid is available or not
                var query = "select AccountId from [CRM].[Opportunities] where OpportunityId = @opportunityid";
                var result = connection.Query<Opportunity>(query, new { opportunityid = opportunityId }).FirstOrDefault();
                account = acctMgr.Get(result.AccountId);
            }
            return account;
        }

        public List<Address> GetInstallationAddress(int? opportunityId)
        {
            List<Address> address;
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                // check the data with opportunityid is available or not
                var query = "select InstallationAddressId from [CRM].[Opportunities] where OpportunityId = @opportunityid";
                var result = connection.Query<Opportunity>(query, new { opportunityid = opportunityId }).FirstOrDefault();
                address = GetAddress(result.InstallationAddressId);
            }
            return address;
        }
        //HFC.CRM.Data.Calendar
        public HFC.CRM.Data.Calendar GetInstaller(int? opportunityId)
        {
            HFC.CRM.Data.Calendar Installerdetails;
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = "select InstallerId from [CRM].[Opportunities] where OpportunityId = @opportunityid";
                var result = connection.Query<Opportunity>(query, new { opportunityid = opportunityId }).FirstOrDefault();
                var assignedpersonid = result.InstallerId;
                query = "select * from [CRM].[Calendar] where assignedpersonid = @assignedpersonid";
                Installerdetails = connection.Query<HFC.CRM.Data.Calendar>(query, new { assignedpersonid = assignedpersonid }).FirstOrDefault();
            }
            return Installerdetails;
        }
        public List<Address> GetAddress(int? AddressId)
        {

            List<Address> addrlist = new List<Address>();
            try
            {
                if (AddressId == 0)
                    return null;

                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    //var query = @"select PersonId as SalesAgentId,FirstName + ' ' + LastName  as Name FROM [CRM].[Person] ";
                    connection.Open();


                    var query = @"select * from CRM.Addresses where AddressId=@AddressId";
                    var addresslist = connection.Query(query, new { AddressId = AddressId }).ToList();

                    connection.Close();
                    for (int i = 0; i < addresslist.Count; ++i)
                    {
                        addrlist.Add(new Address
                        {
                            AddressId = addresslist[i].AddressId,
                            Address1 = addresslist[i].Address1,
                            Address2 = addresslist[i].Address2,
                            City = addresslist[i].City,
                            State = addresslist[i].State,
                            ZipCode = addresslist[i].ZipCode,
                            CountryCode2Digits = addresslist[i].CountryCode2Digits,
                            CreatedOnUtc = addresslist[i].CreatedOnUtc,
                            IsDeleted = addresslist[i].IsDeleted,
                            IsResidential = addresslist[i].IsResidential,
                            CrossStreet = addresslist[i].CrossStreet,
                            Location = addresslist[i].Location,
                            AddressesGuid = addresslist[i].AddressesGuid
                        });

                    }
                    return addrlist;
                }

                //return (from l in CRMDBContext.Opportunities

            }
            catch (Exception ex)
            {
                //log error
            }
            return addrlist;


        }

        public List<string> UploadFile(measurementfiles file)
        {
            Random rnd = new Random();
            string value = rnd.Next(1, 9999).ToString();
            string fileName = DateTime.UtcNow.ToString("MMddyyyy") + "_" + DateTime.UtcNow.ToString("HHMMssfff") + value + "_" + file.name;

            string Query = "INSERT INTO FileTableTb (name, file_stream) OUTPUT INSERTED.[stream_id] values(@FileName, @stream)";

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var dParams = new DynamicParameters();
                dParams.Add("@FileName", fileName, DbType.AnsiString);
                dParams.Add("@stream", file.base64, DbType.Binary);

                var streamid = connection.ExecuteScalar(Query, dParams);


                List<string> ImageExtensions = new List<string> { ".JPG", ".JPEG", ".BMP", ".GIF", ".PNG" };
                string extension = Path.GetExtension(file.name).ToUpper();
                var streamId1 = "";
                if (ImageExtensions.Contains(extension))
                {
                    WebImage img = new WebImage(file.base64);
                    img.Resize(50, 30);
                    byte[] thump = img.GetBytes();
                    string thumpfileName = DateTime.UtcNow.ToString("MMddyyyy") + "_thump_" + DateTime.UtcNow.ToString("HHMMssfff") + "_" + file.name;
                    
                    streamId1 = LeadMgr.UploadFileTP(thump, thumpfileName);
                }

                return new List<string>() { streamid.ToString(), streamId1.ToString()};
            }
        }

        public MeasurementHeader getMeasurementForJobSurfacing(int opportunityId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var res = connection.Query<MeasurementHeader>(@"select mh.* from CRM.MeasurementHeader mh
                                                                    join CRM.Opportunities op on mh.InstallationAddressId=op.InstallationAddressId
                                                                    where op.OpportunityId = @OpportunityId",
                                                                        new { OpportunityId = opportunityId }).FirstOrDefault();
                return res;
            }
        }

        public dynamic InstallationAddresses(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var act = acctMgr.Get(id);
                var query = string.Empty;

                query = @"select a.AccountId, ad.AddressId as InstallAddressId,ad.IsValidated
                        ,ISNULL(Address1,'') + ' ' + ISNULL(Address2,'') + ' ' + ISNULL(City,'') + ' ' + ISNULL(State,'') + ' ' + ISNULL(ZipCode,'') as Name,
                       (select [CRM].[fnGetAccountNameByAccountId](a.AccountId)) as AccountName
                        from crm.accounts a
                        inner join crm.AccountAddresses aa on aa.AccountId = a.AccountId
                        inner join crm.Addresses ad on ad. AddressId = aa.AddressId
                        where a.accountId = @accountId and ad.IsDeleted not in(1)
                        order by  ad.CreatedOn";
                var result = connection.Query(query, new { accountId = id }).ToList();
                return result;
            }
        }

        public dynamic GetOpportunityMeasurement(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                var query = @"SELECT o.OpportunityId, o.OpportunityName,  o.InstallationAddressId,o.AccountId, 
	                            (select [CRM].[fnGetAccountNameByAccountId](acc.AccountId)) as AccountName
                                 ,ad.IsValidated,
	                            isnull(ad.Address1,'')+' '+isnull(ad.Address2,'')+' '+isnull(ad.City,'')+' '+isnull(ad.State,'')+' '+isnull(ad.ZipCode,'') as InstallationAddress
	                            FROM crm.Opportunities o
                                join CRM.Accounts acc on o.AccountId=acc.AccountId		
	                            join CRM.Addresses ad on ad.AddressId=o.InstallationAddressId
	                            join CRM.AccountCustomers acu on o.AccountId=acu.AccountId 
	                            join CRM.Customer cu on acu.CustomerId=cu.CustomerId                                
	                            WHERE o.opportunityid = @opportunityid and acu.IsPrimaryCustomer=1";
                var result = connection.Query(query, new { opportunityid = id }).FirstOrDefault();

                return new { valid = true, error = "", data = result };
            }
        }

        public dynamic saveInternalNotesFromConfigurator(int id, string valuee)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var res = connection.Execute("Update CRM.QuoteLines set InternalNotes=@InternalNotes where QuoteLineId=@QuoteLines", new { QuoteLines = id, InternalNotes = valuee });
                return true;
            }
        }

        public dynamic SaveMeasurementsFromQuoteLineToAccount(int id, List<MeasurementLineItemBB> measurements)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var res = connection.Query<dynamic>("Select * from CRM.Opportunities Where OpportunityId=(select OpportunityId from CRM.Quote where QuoteKey=@QuoteKey)", new { QuoteKey = id }).ToList();
                if (res.Count > 0)
                {
                    int val = res[0].InstallationAddressId;
                    var res_mesHeader = connection.Query<MeasurementHeader>("Select * from CRM.MeasurementHeader where InstallationAddressId=@Id", new { Id = val }).FirstOrDefault();

                    if (res_mesHeader != null)
                    {
                        List<MeasurementLineItemBB> exis_measurement = JsonConvert.DeserializeObject<List<MeasurementLineItemBB>>(res_mesHeader.MeasurementSet);
                        int mea_count = exis_measurement.Count + 1;
                        foreach (var obj in measurements)
                        {
                            var internalNote = connection.Query<dynamic>("select * from CRM.QuoteLines where QuoteLineId= @QuoteLineId", new { QuoteLineId = obj.QuoteLineId }).ToList();
                            var quotee = connection.Query<Quote>("Select * from CRM.Quote where QuoteKey= @QuoteKey", new { QuoteKey = id }).FirstOrDefault();
                            if (internalNote[0].InternalNotes != null && internalNote[0].InternalNotes != "")
                                obj.Comments = internalNote[0].InternalNotes;
                            else
                            {
                                obj.Comments = "Added from Quote " + quotee.QuoteID.ToString();
                            }
                            // obj.Comments = internalNote[0].InternalNotes;
                            obj.id = mea_count;
                            mea_count++;
                        }
                        exis_measurement.AddRange(measurements);
                        string json = JsonConvert.SerializeObject(exis_measurement);

                        var res_query = connection.Execute("update CRM.MeasurementHeader set MeasurementSet=@MeasurementSet where Id=@Id", new { MeasurementSet = json, Id = res_mesHeader.Id });

                        var re_qq = connection.Query<dynamic>("select * from CRM.QuoteLines where QuoteKey=(select QuoteKey from CRM.QuoteLines where QuoteLineId= @QuoteLineId) and MeasurementsetId=( select max(MeasurementsetId) from CRM.QuoteLines where QuoteKey=(select QuoteKey from CRM.QuoteLines where QuoteLineId= @QuoteLineId))", new { QuoteLineId = measurements[0].QuoteLineId }).ToList();

                        int meaSetId = Convert.ToInt32(re_qq[0].MeasurementsetId);
                        foreach (var obj in measurements)
                        {
                            meaSetId++;

                            connection.Execute("update CRM.QuoteLines set MeasurementSetId=@MeasurementSetId where QuoteLineId=@QuoteLineId",
                                new { MeasurementSetId = meaSetId, QuoteLineId = obj.QuoteLineId });
                        }
                        return res[0].InstallationAddressId;
                    }
                    else
                    {
                        int qq = 1;
                        foreach (var obj in measurements)
                        {
                            var internalNote = connection.Query<dynamic>("select * from CRM.QuoteLines where QuoteLineId= @QuoteLineId", new { QuoteLineId = obj.QuoteLineId }).ToList();
                            var quotee = connection.Query<Quote>("Select * from CRM.Quote where QuoteKey= @QuoteKey", new { QuoteKey = id }).FirstOrDefault();
                            if (internalNote[0].InternalNotes != null && internalNote[0].InternalNotes != "")
                                obj.Comments = internalNote[0].InternalNotes;
                            else
                            {
                                obj.Comments = "Added from Quote " + quotee.QuoteID.ToString();
                            }
                            obj.id = qq;
                            qq++;
                        }

                        MeasurementHeader MH = new MeasurementHeader();
                        MH.MeasurementSet = JsonConvert.SerializeObject(measurements);
                        MH.IsDeleted = false;
                        MH.CreatedOnUtc = DateTime.UtcNow;
                        MH.CreatedByPersonId = SessionManager.CurrentUser.PersonId;
                        MH.LastUpdatedOnUtc = DateTime.UtcNow;
                        MH.LastUpdatedByPersonId = null;
                        MH.InstallationAddressId = res[0].InstallationAddressId;

                        var res_me = connection.Insert(MH);

                        connection.Execute("Update CRM.Quote set MeasurementId=@MeasurementId Where QuoteKey=@QuoteKey", new { MeasurementId = res_me, QuoteKey = id });

                        var re_qq = connection.Query<dynamic>("select * from CRM.QuoteLines where QuoteKey=(select QuoteKey from CRM.QuoteLines where QuoteLineId= @QuoteLineId) and MeasurementsetId=( select max(MeasurementsetId) from CRM.QuoteLines where QuoteKey=(select QuoteKey from CRM.QuoteLines where QuoteLineId= @QuoteLineId))", new { QuoteLineId = measurements[0].QuoteLineId }).ToList();

                        int meaSetId = Convert.ToInt32(re_qq[0].MeasurementsetId);
                        foreach (var obj in measurements)
                        {
                            meaSetId++;
                            connection.Execute("update CRM.QuoteLines set MeasurementSetId=@MeasurementSetId where QuoteLineId=@QuoteLineId",
                                new { MeasurementSetId = meaSetId, QuoteLineId = obj.QuoteLineId });
                        }
                        return res[0].InstallationAddressId;
                    }
                }
            }
            return false;
        }
    }

}
