﻿using Dapper;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Extensions;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace HFC.CRM.Managers
{
    public class CaseManager : DataManager<CaseFeedbackDetails>
    {
        private EmailManager emailMgr;
        private PermissionManager permissionManager;
        public CaseManager()
        {
        }

        //public ConfiguratorFeedbackManager(User user,Franchise franchise) : this(user, franchise)
        //{
        //}       
        public CaseManager(User user, Franchise franchise) : this(user, user, franchise)
        {
            this.User = user;
            this.Franchise = franchise;
            emailMgr = new EmailManager(user, franchise);
            permissionManager = new PermissionManager(user, franchise);
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="LeadsManager"/> class.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="franchise">The franchise.</param>
        public CaseManager(User user, User authorizingUser, Franchise franchise)
        {
            this.User = user;
            this.AuthorizingUser = authorizingUser;
            this.Franchise = franchise;
        }

        public int UpdateFeedbackHistory(int CaseId, dynamic data)
        {
            var CaseDetails = new CaseFeedbackDetails();// = GetData<CaseFeedbackDetails>(ContextFactory.CrmConnectionString, data.CaseId);
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = @"select C.StatusId,CFD.CloseReason,CFD.ClosedOn,CFD.ChangeCaseId from CRM.[Case] C left join CRM.[CaseFeedbackDetails] CFD on C.CaseId=CFD.CaseId
                                    where C.CaseId =@CaseId";
                CaseDetails = ExecuteIEnumerableObject<CaseFeedbackDetails>(ContextFactory.CrmConnectionString, query, new { CaseId = CaseId }).FirstOrDefault();
                //CaseDetails = result;
            }
            List<Tuple<string, string, string>> CaseChange = new List<Tuple<string, string, string>>();
            if (CaseDetails != null)
            {
                if (data.StatusId != CaseDetails.StatusId) // only editable field in feedback update.
                {
                    //CaseChange.Add(new Tuple<string, string, string>("Status", CaseDetails.StatusId == (int)CaseFeedbackStatusEnum.Submitted ? "Submitted" : CaseDetails.StatusId == (int)CaseFeedbackStatusEnum.VendorReview ? "Vendor Review" : "Closed", data.StatusId == (int)CaseFeedbackStatusEnum.Submitted ? "Submitted" : data.StatusId == (int)CaseFeedbackStatusEnum.Submitted ? "Vendor Review" : "Closed"));
                    CaseChange.Add(new Tuple<string, string, string>("Status", getCaseStatus(CaseDetails.StatusId), getCaseStatus(data.StatusId)));
                }
                updateHistory(data.CaseId, CaseChange);
            }
            return CaseChange.Count;
        }


        public object GetFeedbackListByFranchise(bool IncludeClosed)
        {
            var query = @"select C.CaseId,V.[Name] as Vendor,C.CaseNumber as ID,lv.[Name] as [ErrorType],tlv.[name] as [Status],
                C.CreatedOn as [Created],C.LastUpdatedOn as [LastModified],
                case when tlv.[name]='Closed' then DATEDIFF(day, CC.CreatedOn, CC.LastUpdatedOn) else DATEDIFF(day, CC.CreatedOn, GETUTCDATE()) end AS[DaysOpen],
                case when tlv.[name]='Closed' then null else DATEDIFF(day, CC.LastUpdatedOn, GETUTCDATE()) end AS [DaysSinceModified] ,
				PP.FirstName+' '+PP.LastName as [Originator],Luv.[Name] as CloseReason
                 from CRM.CaseFeedbackDetails CC
				left join CRM.[Case] C on C.CaseId = CC.CaseId 
                left join [Acct].[HFCVendors] V on C.VendorId = V.VendorId                
                left join CRM.Type_LookUpValues lv on  CC.RecordTypeId= lv.Id
                left join CRM.Type_LookUpValues tlv on C.StatusId=tlv.Id
				left join CRM.Person P on C.LastUpdatedBy=P.PersonId
				left join CRM.Type_LookUpValues Luv on CC.CloseReason=Luv.Id
			    left join CRM.Person PP on C.CreatedBy=PP.PersonId
                where CaseType = 'Feedback' and C.FranchiseId=@FranchiseId";

            if (!IncludeClosed)
                query += " and tlv.[Name] not in ('Closed')";
            query += " order by LastModified desc";
            var result = ExecuteIEnumerableObject<dynamic>(ContextFactory.CrmConnectionString, query, new { FranchiseId = SessionManager.CurrentFranchise == null ? 0 : SessionManager.CurrentFranchise.FranchiseId }).ToList();
            return result;
        }

        public object GetOverviewListByFranchise(bool IncludeClosed)
        {
            var query = @"select 0 as VendorCaseId, C.CaseId,C.CaseType as [Type],V.[Name] as Vendor,C.CaseNumber as ID,
					 lv.[Name] as [RecordType]
					,tlv.[Name] as [Status],C.CreatedOn as Created,
					null  as EffectiveDate,
                    C.LastUpdatedOn as LastModified,
					case when tlv.[name]='Closed' then DATEDIFF(day, C.CreatedOn, C.LastUpdatedOn) else DATEDIFF(day, C.CreatedOn, GETUTCDATE()) end AS [DaysOpen],
                    case when tlv.[name]='Closed'  then null else DATEDIFF(day, C.LastUpdatedOn, GETUTCDATE()) end AS [DaysSinceModified],
                     luv.[Name] as CloseReason,
					PP.FirstName+' '+PP.LastName as [Originator],'' as QuoteLineNumber
					from CRM.[Case] C 
					left join CRM.CaseFeedbackDetails CFD on CFD.CaseId=C.CaseId					
					left join [Acct].[HFCVendors] V on V.VendorId=C.VendorId
					left join CRM.Type_LookUpValues tlv on C.StatusId=tlv.Id 
					left join CRM.Type_LookUpValues lv on  CFD.RecordTypeId= lv.Id 										
					left join CRM.Person PP on C.CreatedBy=PP.PersonId
					left join CRM.Type_LookUpValues Luv on CFD.CloseReason=Luv.Id where CaseType='Feedback' and C.FranchiseId=@FranchiseId";

            if (!IncludeClosed)
                query += " and tlv.[Name] not in ('Closed')";

            var query2 = @"select distinct  fc. CaseId as VendorCaseId,fc.CaseId,'Case'as [Type],ql.VendorName as Vendor ,'Case-'+CAST(fc.[CaseNumber] as varchar(10)) as ID, lvtype.[Name] as [RecordType],
                        lvstatus.[Name] as [Status],fc.CreatedOn as Created,null
                        as EffectiveDate,fc.LastUpdatedOn as LastModified,
                        case when lvstatus.[Name]='Canceled' then DATEDIFF(day, fc.CreatedOn, fc.LastUpdatedOn) else DATEDIFF(day, fc.CreatedOn, GETUTCDATE()) end AS [DaysOpen],
						case when lvstatus.[Name]='Canceled' then null else DATEDIFF(day, fc.LastUpdatedOn, GETUTCDATE()) end AS[DaysSinceModified], 
                        null as CloseReason ,p.FirstName+' '+p.LastName as  [Originator],cast(o.OrderNumber as nvarchar(10))+'-'+CAST(ql.QuoteLineNumber as nvarchar(3)) as QuoteLineNumber
                        from[CRM].[FranchiseCase] fc
                        inner join [CRM].[FranchiseCaseAddInfo] fca on fc.CaseId=fca.CaseId
						join CRM.QuoteLines ql on fca.QuoteLineId= ql.QuoteLineId
						join CRM.Quote q on q.QuoteKey= ql.QuoteKey
                        join CRM.Orders o on q.QuoteKey = o.QuoteKey
						inner join CRM.Opportunities op on q.OpportunityId= op.OpportunityId
						inner join CRM.Accounts acc on op.AccountId= acc.AccountId
						inner join CRM.AccountCustomers accu on acc.AccountId= accu.AccountId
                        inner join CRM.Customer cu on accu.CustomerId= cu.CustomerId and accu.IsPrimaryCustomer= 1
                        inner join CRM.Person p on fc.Owner_PersonId = p.PersonId
                        inner join CRM.Franchise fe on op.FranchiseId=fe.FranchiseId						
                        inner join CRM.Type_LookUpValues lvstatus on fca.[Status]=lvstatus.Id
						left join CRM.PurchaseOrderDetails pod on ql.QuoteLineId = pod.QuoteLineId
						left join CRM.MasterPurchaseOrder mpo on fc.MPO_MasterPONum_POId = mpo.PurchaseOrderId
						left join CRM.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                        inner join CRM.Type_LookUpValues lvtype on fca.[Type]=lvtype.Id where op.FranchiseId=@FranchiseId";
            if (!IncludeClosed)
                query2 += " and lvstatus.[Name] not in ('Cancelled')";

            var GetPermission = permissionManager.GetUserRolesPermissions();
            var hasPermission = GetPermission.Find(x => x.ModuleCode == "CaseManagement ").CanRead;
            if (hasPermission)
                query = "select * from (" + query + " union " + query2 + " ) a order by LastModified desc";
            else
                query = "select * from (" + query + ") a order by LastModified desc";

            var result = ExecuteIEnumerableObject<dynamic>(ContextFactory.CrmConnectionString, query, new { FranchiseId = SessionManager.CurrentFranchise == null ? 0 : SessionManager.CurrentFranchise.FranchiseId }).ToList();
            return result;
        }

        private void updateHistory(int caseId, List<Tuple<string, string, string>> caseChange)
        {
            HistoryTable tab = new HistoryTable();
            tab.Table = "Case"; tab.TableId = caseId; tab.Date = DateTime.UtcNow;
            tab.UserId = SessionManager.CurrentUser.PersonId;
            if (caseChange != null)
                foreach (var r in caseChange)
                {
                    tab.Field = r.Item1;
                    tab.OriginalValue = r.Item2;
                    tab.NewValue = r.Item3;
                    Insert<int, HistoryTable>(ContextFactory.CrmConnectionString, tab);
                }
        }

        public int UpdateChangeHistory(int caseId, CaseChangeDetails data)
        {
            var CaseDetails = new CaseChangeDetails();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = @"select C.StatusId,CFD.PublishMethod,CFD.RecordTypeId,CFD.AttachmentURL,CFD.ProductGroupId,CFD.EffectiveDate,CFD.DeclinedReasonId,CFD.Comments from CRM.[Case] C left join CRM.[CaseChangeDetails] CFD on C.CaseId=CFD.CaseId
                                    where C.CaseId =@CaseId";
                //CaseDetails= connection.Query<CaseChangeDetails>(query, new { CaseId = caseId }).FirstOrDefault();
                var result = connection.Query<dynamic>(query, new { CaseId = caseId }).FirstOrDefault();
                if (result != null)
                {
                    string productGroupId = result.ProductGroupId;
                    List<int> ProductGroupId = new List<int>(productGroupId.Split(',').Select(s => int.Parse(s)));
                    CaseDetails.StatusId = result.StatusId;
                    CaseDetails.PublishMethod = result.PublishMethod;
                    CaseDetails.RecordTypeId = result.RecordTypeId;
                    CaseDetails.AttachmentURL = result.AttachmentURL;
                    CaseDetails.ProductGroupId = ProductGroupId;
                    CaseDetails.EffectiveDate = result.EffectiveDate;
                    CaseDetails.DeclinedReasonId = result.DeclinedReasonId;
                    CaseDetails.Comments = result.Comments;
                }
            }
            List<Tuple<string, string, string>> CaseChange = new List<Tuple<string, string, string>>();
            if (CaseDetails != null)
            {
                if (data.StatusId != CaseDetails.StatusId)
                {
                    CaseChange.Add(new Tuple<string, string, string>("Status", getCaseStatus(CaseDetails.StatusId), getCaseStatus(data.StatusId)));
                }
                if (data.PublishMethod != CaseDetails.PublishMethod)
                {
                    CaseChange.Add(new Tuple<string, string, string>("Publish Method", CaseDetails.PublishMethod == null ? "" : CaseDetails.PublishMethod.ToString(), data.PublishMethod == null ? "" : data.PublishMethod.ToString()));
                }
                if (data.RecordTypeId != CaseDetails.RecordTypeId)
                {
                    CaseChange.Add(new Tuple<string, string, string>("Record Type", getCaseStatus(CaseDetails.RecordTypeId), getCaseStatus(data.RecordTypeId)));
                }
                if (data.AttachmentURL != CaseDetails.AttachmentURL)
                {
                    CaseChange.Add(new Tuple<string, string, string>("Attachment URL", CaseDetails.AttachmentURL.ToString(), data.AttachmentURL.ToString()));
                }
                if (string.Join(",", data.ProductGroupId) != string.Join(",", CaseDetails.ProductGroupId))
                {
                    string productCategoryOld = getProductCategoryById(string.Join(",", CaseDetails.ProductGroupId), false);
                    string productCategoryNew = getProductCategoryById(string.Join(",", data.ProductGroupId), true);
                    CaseChange.Add(new Tuple<string, string, string>("Product Category", productCategoryOld, productCategoryNew));
                }
                if (data.EffectiveDate != CaseDetails.EffectiveDate)
                {
                    CaseChange.Add(new Tuple<string, string, string>("Effective Date", CaseDetails.EffectiveDate.ToString(), data.EffectiveDate.ToString()));
                }
                if (data.DeclinedReasonId != CaseDetails.DeclinedReasonId)
                {
                    CaseChange.Add(new Tuple<string, string, string>("Declined Reason", CaseDetails.DeclinedReasonId == 0 ? "" : getCaseStatus(Convert.ToInt32(CaseDetails.DeclinedReasonId)), getCaseStatus(Convert.ToInt32(data.DeclinedReasonId))));
                }
                if (data.Comments != CaseDetails.Comments)
                {
                    CaseChange.Add(new Tuple<string, string, string>("Comments", CaseDetails.Comments.ToString(), data.Comments.ToString()));
                }
                updateHistory(data.CaseId, CaseChange);
            }
            return CaseChange.Count;
        }

        private string getProductCategoryById(string productGroupId, bool isNew)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var result = "";
                var query = "";
                if (!isNew)
                {
                    query = @";With cte	                            as	                            (			                            select distinct CC.CaseId,HP.ProductGroupDesc			                            from CRM.CaseChangeDetails CC with (nolock)			                            Cross apply			                            (select *			                            from CRM.CSVToTable (ProductGroupId)) m2			                            Inner join CRM.HFCProducts HP with (nolock) on HP.ProductGroup = m2.id			                            where CC.ProductGroupId=@ProductGroupId	                            )					                    select stuff((select ', ' + ProductGroupDesc from cte m1 where m1.CaseId = C.CaseId for xml path('')),1,1,'') as ProductGroupDesc from CRM.[CaseChangeDetails] C 		                    where C.ProductGroupId=@ProductGroupId";

                }
                else
                {
                    query = @";With cte
                               as

                               (
                               select distinct HP.ProductGroupDesc from
                               (select *
                               from CRM.CSVToTable (@ProductGroupId)) m2
                               left join CRM.HFCProducts HP with(nolock) on HP.ProductGroup = m2.id
	                           )			
		                       select stuff((select ', ' + ProductGroupDesc from cte m1 for xml path('')),1,1,'') as ProductGroupDesc";
                }
                result = connection.Query<string>(query, new { ProductGroupId = productGroupId }).FirstOrDefault();
                return result;
            }

        }

        public string getCaseStatus(int StatusId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = "select Name from CRM.Type_LookUpValues where Id=@Id";
                return connection.Query<string>(query, new { Id = StatusId }).FirstOrDefault();
            }
        }


        public bool sendEmailForComments(int CaseId, string CaseNumber, string htmlcontent, string updation, string[] emails, CaseFeedbackDetails model)
        {
            var connection = new SqlConnection(ContextFactory.CrmConnectionString);

            var res = connection.Query<Person>("select p.* from [CRM].[Person] as p join CRM.CaseFollow as cf on cf.PersonId = p.PersonId  where cf.CaseId =@CaseId and cf.ModuleId in (1,2) and cf.IsFollow = 1", new { CaseId = CaseId }).ToList();

            var userRes = connection.Query<User>(@"select * from Auth.Users u
                                                        join CRM.FranchiseCase fc on fc.Owner_PersonId = u.PersonId
                                                        where CaseId=@CaseId", new { CaseId = CaseId }).FirstOrDefault();
            var frr_admin = connection.Query<Person>("select top 1 p.* from Auth.Users u join Auth.UsersInRoles ur on u.UserId = ur.UserId join CRM.Person p on u.PersonId = p.PersonId where u.FranchiseId = @FranchiseId and RoleId = 'A725282F-E606-4353-BB53-693F695D42A5'", new { FranchiseId = userRes.FranchiseId }).FirstOrDefault();


            HttpContext context = HttpContext.Current;
            string baseUrl = context.Request.Url.Authority + context.Request.ApplicationPath.TrimEnd('/');

            if (frr_admin.PrimaryEmail != null)
            {
                List<string> toRecipients = new List<string>();
                toRecipients = res.AsEnumerable().Select(r => r.PrimaryEmail).ToList();
                var resss = connection.Query<Person>("select * from [CRM].[Person]  where PersonId =(Select Owner_PersonId from [CRM].[FranchiseCase] where CaseId=@CaseId)", new { CaseId = CaseId }).ToList();
                toRecipients.Add(resss[0].PrimaryEmail);
                ExchangeManager mgr = new ExchangeManager();
                var res_caseid = connection.Query<FranchiseCaseModel>("select * from [CRM].[FranchiseCase]  where CaseId=@CaseId", new { CaseId = CaseId }).FirstOrDefault();

                foreach (string str in emails)
                {
                    toRecipients.Add(str);
                }

                toRecipients = toRecipients.Distinct().ToList();


                string subject = "";

                if (model.StatusId == 12000)
                    subject = "Request ID #: " + CaseNumber + " was created";
                else if (model.StatusId == 12001)
                    subject = "Request ID #: " + CaseNumber + " was changed";
                else if (model.StatusId == 12002)    /// Case #: <1234> "Comment added
                    subject = "Request ID #: " + CaseNumber + " was changed";
                else { }

                string HtmlText = htmlcontent + "</br></br><p>Note: You have been sent this email because you are either noted on this Case as the Owner, have been tagged in the comment, or have chosen to Follow updates for this Case </p>";

                mgr.SendEmail(frr_admin.PrimaryEmail, subject, HtmlText, toRecipients);

            }

            return true;

        }

        public List<HistoryTable> GetCaseHistoryForCase(int id)
        {
            var query = @"SELECT ht.Date,ht.Field, ht.UserId, ht.LastUpdatedOn,
            ht.OriginalValue as OriginalValue,ht.NewValue as NewValue,  P.FirstName + '  ' + P.LastName as UserName  FROM [CRM].[HistoryTable] ht
            left join CRM.Person P on ht.UserId = P.PersonId
            Where ht.TableId = @Id and ht.[Table]='Case' order by LastUpdatedOn desc";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                {
                    var res = connection.Query<HistoryTable>(query, new { Id = id }).ToList();

                    return res;
                }
            }
            //return true;
        }

        public CaseFeedbackDetails Save(CaseFeedbackDetails data)
        {
            var updatedCount = 0;
            var franchiseId = 0;
            dynamic OldFeedbackDetails = new CaseFeedbackDetails();
            if (data.CaseId == 0)
            {
                data.CreatedBy = SessionManager.CurrentUser.PersonId;
                data.LastUpdatedBy = SessionManager.CurrentUser.PersonId;
                franchiseId = SessionManager.CurrentFranchise.FranchiseId;
            }
            else
            {
                data.LastUpdatedBy = SessionManager.CurrentUser.PersonId;
                dynamic newFeedbackDetails = new CaseFeedbackDetails();
                newFeedbackDetails = data;
                updatedCount = UpdateFeedbackHistory(data.CaseId, newFeedbackDetails);
                OldFeedbackDetails=GetFeedbackByCaseId(data.CaseId);
            }

            if (data.StatusId == (int)CaseFeedbackStatusEnum.Closed)//Closed status
            {
                data.ClosedOn = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc);
            }

            List<dynamic> result = new List<dynamic>();
            var query = @"exec [CRM].[spInsertConfiguratorFeedback] @CaseId,@CaseNumber,@CaseType,@FranchiseId,@VendorId,@Details,@StatusId,@CaseFeedbackId,@RecordTypeId
                        ,@ProductGroupId,@TouchpointURL,@TouchpointVersion,@BrowserDetails,@PICJson,@FieldErrors,@ChangeCaseId,@CloseReason,@ClosedOn,@CreatedOn,@CreatedBy,@LastUpdatedOn,@LastUpdatedBy";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                result = connection.Query<CaseFeedbackDetails>(query
                    , new
                    {
                        CaseId = data.CaseId,
                        CaseNumber = data.CaseNumber,
                        CaseType = data.CaseType,
                        FranchiseId = franchiseId,
                        VendorId = data.VendorId,
                        Details = data.Details,
                        StatusId = data.StatusId, //Default Submitted status
                        CaseFeedbackId = data.CaseFeedbackId,
                        RecordTypeId = data.RecordTypeId,
                        ProductGroupId = data.ProductGroupId,
                        TouchpointURL = data.TouchpointURL,
                        TouchpointVersion = data.TouchpointVersion,
                        BrowserDetails = data.BrowserDetails,
                        PICJson = data.PICJson,
                        FieldErrors = data.FieldErrors,
                        ChangeCaseId = data.ChangeCaseId,
                        CloseReason = data.CloseReason,
                        ClosedOn = data.ClosedOn,
                        CreatedOn = data.CreatedOn,
                        CreatedBy = data.CreatedBy,
                        LastUpdatedOn = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc),
                        LastUpdatedBy = data.LastUpdatedBy
                    }).ToList<object>();
            }
            if (result != null && result.Count != 0)
            {
                // data.CaseId = Convert.toInt16(result.CaseId);
                if ((data.CaseId != 0 && updatedCount != 0) || data.CaseId == 0)
                {
                    var UpdatedFeedbackDetails = GetFeedbackByCaseId(result[0].CaseId);
                    SendFeedbackStatusEmail(EmailType.CaseFeedback, OldFeedbackDetails, UpdatedFeedbackDetails);
                }
                return result[0];
            }
            else
                return result[0];
            //return ExecuteIEnumerableObject<CaseFeedbackDetails>(ContextFactory.CrmConnectionString, query,).FirstOrDefault();            
        }

        public CaseChangeDetails SaveChangeRequest(CaseChangeDetails data)
        {
            dynamic OldChangeDetails = new CaseChangeDetails();
            var updatedCount = 0;
            if (data.CaseId == 0)
            {
                data.CreatedBy = SessionManager.CurrentUser.PersonId;
                data.LastUpdatedBy = SessionManager.CurrentUser.PersonId;
            }
            else
            {
                data.LastUpdatedBy = SessionManager.CurrentUser.PersonId;
                updatedCount = UpdateChangeHistory(data.CaseId, data);
                OldChangeDetails = GetChangeRequestById(data.CaseId);
            }

            List<dynamic> result = new List<dynamic>();
            var query = @"exec [CRM].[spInsertChangeRequest] @CaseId,@CaseNumber,@CaseType,@FranchiseId,@VendorId,@Details,@StatusId,@CaseChangeId
                        ,@RecordTypeId,@ProductGroupId,@PublishMethod,@AttachmentURL,@EffectiveDate,@CreatedOn,@CreatedBy,@LastUpdatedOn,@LastUpdatedBy,@DeclinedReasonId,@Comments,@IsAllSelected";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                result = connection.Query<CaseChangeDetails>(query
                    , new
                    {
                        CaseId = data.CaseId,
                        CaseNumber = data.CaseNumber,
                        CaseType = data.CaseType,
                        FranchiseId = data.FranchiseId,
                        VendorId = data.VendorId,
                        Details = data.Details,
                        StatusId = data.CaseId == 0 ? (int)CaseChangeStatusEnum.Submitted : data.StatusId, //Default Submitted status 13000
                        CaseChangeId = data.CaseChangeId,
                        RecordTypeId = data.RecordTypeId,
                        ProductGroupId = string.Join(",", data.ProductGroupId),
                        PublishMethod = data.PublishMethod,
                        AttachmentURL = data.AttachmentURL,
                        EffectiveDate = data.EffectiveDate,
                        CreatedOn = data.CreatedOn,
                        CreatedBy = data.CreatedBy,
                        LastUpdatedOn = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc),
                        LastUpdatedBy = data.LastUpdatedBy,
                        DeclinedReasonId = data.DeclinedReasonId,
                        Comments = data.Comments,
                        IsAllSelected = data.IsAllSelected
                    }).ToList<object>();
            }
            if (result != null && result.Count != 0)
            {
                if ((updatedCount != 0 && data.CaseId != 0) || data.CaseId == 0)
                {
                   var UpdatedChangeDetails = GetChangeRequestById(result[0].CaseId);
                    SendChangeRequestStatusEmail(EmailType.CaseChange, OldChangeDetails, UpdatedChangeDetails);
                }
                return result[0];
            }
            else
                return result[0];
        }

        public CaseAttachments saveCaseAttachmentDetails(int CaseId, string fileIconType, string fileDetails, string fileName, string streamId, string streamId1, bool flag)
        {
            var Query = @"insert into CRM.CaseAttachments values(@CaseId,@FileIconType,@FileDetails,@FileName,@StreamId,@CreatedOn,@CreatedBy, @LastUpdatedOn, @LastUpdatedBy)";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                {
                    var Value = connection.Execute(Query, new { CaseId = CaseId, FileIconType = fileIconType, FileDetails = fileDetails, FileName = fileName, streamId = streamId, CreatedOn = DateTime.UtcNow, CreatedBy = SessionManager.CurrentUser.PersonId, LastUpdatedOn = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc), LastUpdatedBy = SessionManager.CurrentUser.PersonId });
                    var qry = @"Select * from CRM.[CaseAttachments] where streamId =@streamId";
                    var res = connection.Query<CaseAttachments>(qry, new { streamId = streamId }).FirstOrDefault();


                    if (flag == true)
                    {
                        var tz = this.getFranchiseTimeZone(CaseId).TimezoneCode;

                        if (res.CreatedOn == null)
                        {
                            res.FileDetails = res.FileDetails.Insert(10, " " + tz.ToString() + " ");
                        }
                        else
                        {
                            DateTime lt = TimeZoneManager.ToLocal((DateTime)res.CreatedOn, tz);
                            res.FileDetails = res.FileDetails.Substring(10, res.FileDetails.Length - 10);
                            res.FileDetails = lt.Date.GlobalDateFormat() + " " + tz.ToString() + res.FileDetails;
                        }

                    }
                    else
                    {
                        var tz = this.getFranchiseTimeZone(CaseId).TimezoneCode;

                        if (res.CreatedOn != null)
                        {
                            DateTime lt = TimeZoneManager.ToLocal((DateTime)res.CreatedOn, tz);
                            res.FileDetails = res.FileDetails.Substring(10, res.FileDetails.Length - 10);
                            res.FileDetails = lt.Date.GlobalDateFormat() + res.FileDetails;
                        }
                    }

                    return res;
                }
            }

        }

        public List<CaseAttachments> getCaseAttachmentsList(int CaseId)
        {
            var Query = @"select ca.* from [CRM].[Case] c                                   
                  join CRM.CaseAttachments ca on ca.CaseId=c.CaseId
                  where c.CaseId=@CaseId";

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                {
                    var Value = connection.Query<CaseAttachments>(Query, new { CaseId = CaseId }).ToList();
                    return Value;
                }
            }
        }

        public bool removeCaseAttachmentsDetail(int Id)
        {
            var query = @"Delete from CRM.[CaseAttachments] where Id=@Id";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                {
                    connection.Execute(query, new { Id = Id });
                }
            }
            return true;
        }

        public bool removeCaseAttachmentsDetails(int Id, int CaseId)
        {
            var query = @"Delete from CRM.[CaseAttachments] where Id=@Id and CaseId=@CaseId";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                {
                    connection.Execute(query, new { CaseId = CaseId });
                }
            }
            return true;
        }
        public dynamic GetOverviewOfCaseFeedbackChangeList(bool IsPIC, bool IncludeClosed)
        {
            var query = @"select 0 as VendorCaseId,C.CaseId,C.CaseType as [Type],V.[Name] as Vendor,C.CaseNumber as ID,
		                lv.[Name] as [RecordType],tlv.[Name] as [Status],C.CreatedOn as Created,null as EffectiveDate,
		                C.LastUpdatedOn as LastModified,
		                case when tlv.[name]='Closed'  then DATEDIFF(day, C.CreatedOn, C.LastUpdatedOn) else DATEDIFF(day, C.CreatedOn, GETUTCDATE()) end AS [DaysOpen],
		                case when tlv.[name]='Closed' then null else DATEDIFF(day, C.LastUpdatedOn, GETUTCDATE()) end AS [DaysSinceModified],	luv.[Name]  as CloseReason,
		                PP.FirstName+' '+PP.LastName as [Originator],'' as QuoteLineNumber
					    from CRM.[Case] C 
					    left join CRM.CaseFeedbackDetails CFD on CFD.CaseId=C.CaseId					
					    left join [Acct].[HFCVendors] V on V.VendorId=C.VendorId
					    left join CRM.Type_LookUpValues tlv on C.StatusId=tlv.Id 
					    left join CRM.Type_LookUpValues lv on  CFD.RecordTypeId= lv.Id 										
					    left join CRM.Person PP on C.CreatedBy=PP.PersonId
					    left join CRM.Type_LookUpValues Luv on CFD.CloseReason=Luv.Id where C.CaseType='Feedback'";

            if (!IncludeClosed)
                query += "and tlv.[Name] not in ('Closed')"; //C.StatusId not in (12002,13005)";

            var query1 = @"select 0 as VendorCaseId,C.CaseId,C.CaseType as [Type],V.[Name] as Vendor,C.CaseNumber as ID,
		                tv.[Name] as [RecordType],tlv.[Name] as [Status],C.CreatedOn as Created,CCD.EffectiveDate as EffectiveDate,
		                C.LastUpdatedOn as LastModified,
		                case when tlv.[name]='In Production'  then DATEDIFF(day, C.CreatedOn, C.LastUpdatedOn) else DATEDIFF(day, C.CreatedOn, GETUTCDATE()) end AS [DaysOpen],
		                case when tlv.[name]='In Production' then null else DATEDIFF(day, C.LastUpdatedOn, GETUTCDATE()) end AS [DaysSinceModified],	null  as CloseReason,
		                PP.FirstName+' '+PP.LastName as [Originator],'' as QuoteLineNumber
					    from CRM.[Case] C 
					    left join CRM.CaseChangeDetails CCD on CCD.CaseId=C.CaseId			
					    left join [Acct].[HFCVendors] V on V.VendorId=C.VendorId
					    left join CRM.Type_LookUpValues tlv on C.StatusId=tlv.Id 
					    left join CRM.Type_LookUpValues tv on  CCD.RecordTypeId= tv.Id								
					    left join CRM.Person PP on C.CreatedBy=PP.PersonId where C.CaseType='Change'";
            if (IsPIC)
            {
                query1 += "and tlv.[Name] not in ('Submitted','Declined')";// C.StatusId not in (12000,13000,13002)";
            }
            if (!IncludeClosed)
                query1 += " and tlv.[Name] not in ('In Production')"; //C.StatusId not in (12002,13005)";          


            var query2 = @"select distinct  fc. CaseId as VendorCaseId,fc.CaseId,'Case'as [Type],ql.VendorName as Vendor ,'Case-'+CAST(fc.[CaseNumber] as varchar(10)) as ID, lvtype.[Name] as [RecordType],
                        lvstatus.[Name] as [Status],fc.CreatedOn as Created,null
                        as EffectiveDate,fc.LastUpdatedOn as LastModified,
                        case when  lvstatus.[Name]='Canceled' then DATEDIFF(day, fc.CreatedOn, fc.LastUpdatedOn) else DATEDIFF(day, fc.CreatedOn, GETUTCDATE()) end AS [DaysOpen],
						case when lvstatus.[Name]='Canceled' then null else DATEDIFF(day, fc.LastUpdatedOn, GETUTCDATE()) end AS[DaysSinceModified], 
                        null as CloseReason ,p.FirstName+' '+p.LastName as  [Originator],cast(o.OrderNumber as nvarchar(10))+'-'+CAST(ql.QuoteLineNumber as nvarchar(3)) as QuoteLineNumber
                        from[CRM].[FranchiseCase] fc
                        inner join [CRM].[FranchiseCaseAddInfo] fca on fc.CaseId=fca.CaseId
						join CRM.QuoteLines ql on fca.QuoteLineId= ql.QuoteLineId
						join CRM.Quote q on q.QuoteKey= ql.QuoteKey
                        join CRM.Orders o on q.QuoteKey = o.QuoteKey
						inner join CRM.Opportunities op on q.OpportunityId= op.OpportunityId
						inner join CRM.Accounts acc on op.AccountId= acc.AccountId
						inner join CRM.AccountCustomers accu on acc.AccountId= accu.AccountId
                        inner join CRM.Customer cu on accu.CustomerId= cu.CustomerId and accu.IsPrimaryCustomer= 1
                        inner join CRM.Person p on fc.Owner_PersonId = p.PersonId
                        inner join CRM.Franchise fe on op.FranchiseId=fe.FranchiseId						
                        inner join CRM.Type_LookUpValues lvstatus on fca.[Status]=lvstatus.Id
						left join CRM.PurchaseOrderDetails pod on ql.QuoteLineId = pod.QuoteLineId
						left join CRM.MasterPurchaseOrder mpo on fc.MPO_MasterPONum_POId = mpo.PurchaseOrderId
						left join CRM.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                        inner join CRM.Type_LookUpValues lvtype on fca.[Type]=lvtype.Id where 1=1 and ql.VendorId=(ql.VendorId)";

            if (!IncludeClosed)
                query2 += " and lvstatus.[Name] not in ('Cancelled')";

            List<Role> roles = SessionManager.CurrentUser.Roles.ToList();
            if (roles.FindAll(delegate (Role role) { return role.RoleName == "Global Admin"; }).Count != 0)
            {
                //query2 = query2 + " " + " and vc.VendorId=(select VendorId from Acct.HFCVendors where VendorIdPk in (select VendorId from CRM.VendorCaseConfig where PersonId=@PersonId))";
                query = "select * from (" + query + " union " + query1 + " union " + query2 + ") a order by LastModified desc";
            }
            else
            {
                query = "select * from (" + query + " union " + query1 + ") a order by LastModified desc";
            }
            var result = ExecuteIEnumerableObject<dynamic>(ContextFactory.CrmConnectionString, query, new { PersonId = SessionManager.CurrentUser.PersonId }).ToList();
            return result;
        }

        public dynamic GetOverviewOfCaseFeedbackChangeListByVendor(bool IncludeClosed)
        {
            var query = @"select 0 as VendorCaseId, C.CaseId,C.CaseType as [Type],V.[Name] as Vendor,C.CaseNumber as ID,
					case when C.CaseType='Change' then  tv.[Name] when C.CaseType='Feedback' then lv.[Name] end as [RecordType]
					,tlv.[Name] as [Status],C.CreatedOn as Created,
					case when C.CaseType='Change' then (Select EffectiveDate from CRM.CaseChangeDetails where CaseId=C.CaseId)else null end as EffectiveDate,
                    C.LastUpdatedOn as LastModified,
					case when tlv.[name]='Closed' or tlv.[name]='In Production'  then DATEDIFF(day, C.CreatedOn, C.LastUpdatedOn) else DATEDIFF(day, C.CreatedOn, GETUTCDATE()) end AS [DaysOpen],
                    case when tlv.[name]='Closed' or tlv.[name]='In Production'  then null else DATEDIFF(day, C.LastUpdatedOn, GETUTCDATE()) end AS [DaysSinceModified],
                    case when C.CaseType='Feedback' then luv.[Name] else null end as CloseReason,
					PP.FirstName+' '+PP.LastName as [Originator]
					from CRM.[Case] C 
					left join CRM.CaseFeedbackDetails CFD on CFD.CaseId=C.CaseId
					left join CRM.CaseChangeDetails CCD on CCD.CaseId=C.CaseId
					left join [Acct].[HFCVendors] V on V.VendorId=C.VendorId
					left join CRM.Type_LookUpValues tlv on C.StatusId=tlv.Id 
					left join CRM.Type_LookUpValues lv on  CFD.RecordTypeId= lv.Id 					
					left join CRM.Type_LookUpValues tv on  CCD.RecordTypeId= tv.Id
					left join CRM.Person PP on C.CreatedBy=PP.PersonId
					left join CRM.Type_LookUpValues Luv on CFD.CloseReason=Luv.Id where C.VendorId in (select VendorId from Acct.HFCVendors where VendorIdPk in (select VendorId from CRM.VendorCaseConfig where PersonId = @PersonId))";
            if (!IncludeClosed)
                query += " and tlv.[Name] not in ('Closed','In Production')";

            var query2 = @"select distinct Vc.VendorCaseId, Vc.CaseId,'Case'as [Type],V.[Name] as Vendor,'Case-'+Vc.VendorCaseNumber as ID, lvtype.[Name] as [RecordType],
                        lvstatus.[Name] as [Status],Vc.CreatedOn as Created,null
                        as EffectiveDate,Vc.LastUpdatedOn as LastModified,
                        case when lvstatus.[Name]='Canceled' then DATEDIFF(day, Vc.CreatedOn, Vc.LastUpdatedOn) else DATEDIFF(day, Vc.CreatedOn, GETUTCDATE()) end AS [DaysOpen],
						case when lvstatus.[Name]='Canceled' then null else DATEDIFF(day, Vc.LastUpdatedOn, GETUTCDATE()) end AS[DaysSinceModified], 
                        null as CloseReason,p.FirstName+' '+p.LastName as [Originator]                      
                         from CRM.VendorCase vc
                         join CRM.FranchiseCase Fc on Vc.CaseId=Fc.CaseId
                         join CRM.VendorCaseDetail  Vcd on Vc.VendorCaseId=Vcd.VendorCaseId
                         join CRM.FranchiseCaseAddInfo fca on Vcd.CaseLineId=fca.Id
                         join CRM.QuoteLines ql on fca.QuoteLineId=ql.QuoteLineId
						 join CRM.Quote q on ql.QuoteKey= q.QuoteKey
                         join CRM.Type_LookUpValues tltype on fca.Type=tltype.Id
                         join CRM.Type_LookUpValues tlrea on fca.ReasonCode=tlrea.Id
                         left join CRM.PurchaseOrderDetails po on fca.QuoteLineId=po.QuoteLineId
                         left join CRM.MasterPurchaseOrder mpo on Fc.MPO_MasterPONum_POId = mpo.PurchaseOrderId
                         left join CRM.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
						 left join [Acct].[HFCVendors] V on V.VendorId=Vc.VendorId
                         join CRM.Opportunities op on q.OpportunityId= op.OpportunityId
                         join CRM.Type_LookUpValues lvstatus on fca.Status=lvstatus.Id
                         join CRM.Type_LookUpValues lvtype on fca.[Type]=lvtype.Id
                         join CRM.Person p on fc.Owner_PersonId = p.PersonId
                         join CRM.Franchise F on op.FranchiseId=F.FranchiseId
                         join CRM.Accounts acc on op.AccountId= acc.AccountId
                         join CRM.AccountCustomers accu on acc.AccountId= accu.AccountId
                         join CRM.Customer cu on accu.CustomerId= cu.CustomerId and
						 accu.IsPrimaryCustomer= 1 where vc.VendorId=(select VendorId from Acct.HFCVendors where VendorIdPk in (select VendorId from CRM.VendorCaseConfig where PersonId=@PersonId))";
            if (!IncludeClosed)
                query2 += " and lvstatus.[Name] not in ('Cancelled')";

            //query = "select * from (" + query + " union " + query2 + " ) a order by LastModified desc";
            query = "select * from (" + query + ") a order by LastModified desc";
            var result = ExecuteIEnumerableObject<dynamic>(ContextFactory.CrmConnectionString, query, new { PersonId = SessionManager.CurrentUser.PersonId }).ToList();
            return result;
        }

        public dynamic GetChangeRequestById(int CaseId)
        {
            var query = @";With cte	        as	        (			        select distinct CC.CaseId,HP.ProductGroupDesc			        from CRM.CaseChangeDetails CC with (nolock)			        Cross apply			        (select *			        from CRM.CSVToTable (ProductGroupId)) m2			        Inner join CRM.HFCProducts HP with (nolock) on HP.ProductGroup = m2.id			        where CC.CaseId=@CaseId	        )	        select C.CaseId,CC.CaseChangeId,V.[Name] as Vendor,C.CaseNumber as [ReportID],lv.[Name] as [RequestType],tlv.[Name] as [CurrentStatus],C.VendorId,		        C.Details, C.CreatedOn ,CC.EffectiveDate,C.LastUpdatedOn,GETUTCDATE() as CurrentTime, CC.PublishMethod,CC.ProductGroupId,CC.CreatedBy,CC.RecordTypeId,CC.AttachmentURL,C.StatusId,		        P.FirstName+' '+P.LastName as [CreatedByName],PP.FirstName+' '+pp.LastName as [LastUpdateByName],CC.Comments,CC.DeclinedReasonId,luv.[Name] as DeclinedReason		        ,stuff((select ', ' + ProductGroupDesc from cte m1 where m1.CaseId = C.CaseId for xml path('')),1,1,'') as ProductCategory                from CRM.CaseChangeDetails CC                left join CRM.[Case] C on C.CaseId=CC.CaseId                 left join [Acct].[HFCVendors] V on C.VendorId=V.VendorId                left join CRM.Type_LookUpValues lv on  CC.RecordTypeId= lv.Id                left join CRM.Type_LookUpValues tlv on C.StatusId=tlv.Id			        left join CRM.Type_LookUpValues luv on CC.DeclinedReasonId=luv.Id 							        left join CRM.Person P on CC.CreatedBy=P.PersonId		        left join CRM.Person PP on CC.LastUpdatedBy=PP.PersonId	                where CaseType='Change' and CC.CaseId=@CaseId";
            var result = ExecuteIEnumerableObject<dynamic>(ContextFactory.CrmConnectionString, query, new { CaseId = CaseId }).FirstOrDefault();
            if (result != null)
            {
                string productGroupId = result.ProductGroupId;
                List<int> ProductGroupId = new List<int>(productGroupId.Split(',').Select(s => int.Parse(s)));
                result.ProductGroupId = ProductGroupId;

                result.CreatedOn = TimeZoneManager.ToLocal(result.CreatedOn);
                result.LastUpdatedOn = TimeZoneManager.ToLocal(result.LastUpdatedOn);
                result.CurrentTime = TimeZoneManager.ToLocal(result.CurrentTime);
            }
            return result;
        }

        public List<dynamic> GetFeedbackChangeLookup(string TableName)
        {
            var query = @"select Name,Id from[CRM].[Type_LookUpValues] where TableId in(Select Id from crm.Type_LookUpTable where TableName = @TableName)";
            var result = ExecuteIEnumerableObject<dynamic>(ContextFactory.CrmConnectionString, query, new { TableName = TableName }).ToList();
            return result;
        }

        public dynamic GetConfiguratorFeedbackById(int CaseId, bool IsVendor)
        {
            if (IsVendor)
            {
                //IsVendor==true && status is submitted ==> update Status to Vendor review
                var connection = new SqlConnection(ContextFactory.CrmConnectionString);
                dynamic OldFeedback = new CaseFeedbackDetails();
                 OldFeedback = GetFeedbackByCaseId(CaseId);
                OldFeedback.StatusId = OldFeedback.CurrentStatusId;
                List<Role> roles = SessionManager.CurrentUser.Roles.ToList();
                if (roles.FindAll(delegate (Role role) { return role.RoleName == "Vendor"; }).Count != 0)
                {
                    if (OldFeedback.StatusId == (int)CaseFeedbackStatusEnum.Submitted)
                    {
                        dynamic Updatedfeedback = GetFeedbackByCaseId(CaseId); ;
                        Updatedfeedback.CurrentStatusId = (dynamic)((int)CaseFeedbackStatusEnum.VendorReview);
                        Updatedfeedback.StatusId = Updatedfeedback.CurrentStatusId;
                        int updateCount = UpdateFeedbackHistory(CaseId, Updatedfeedback);
                        connection.Execute("Update [CRM].[Case] set StatusId=@Status,LastUpdatedOn=@Date,LastUpdatedBy=@PersonId where CaseId = @CaseId",
                            new { CaseId = CaseId, Status = (int)CaseFeedbackStatusEnum.VendorReview, Date = DateTime.UtcNow, PersonId = SessionManager.CurrentUser.PersonId });
                        if (updateCount != 0)
                        {
                            SendFeedbackStatusEmail(EmailType.CaseFeedback, OldFeedback, Updatedfeedback);
                        }
                    }
                }
            }           
            return GetFeedbackByCaseId(CaseId);
        }

        public dynamic GetFeedbackByCaseId(int CaseId)
        {
            var query = @"select distinct Hp.ProductGroupDesc as ProductCategory ,C.CaseId,CFD.CaseFeedbackId,Tlv.[Name] as [CurrentStatus],C.StatusId as [CurrentStatusId],Lv.[Name] as [ProblemType],V.[Name] as Vendor,C.VendorId
                    ,C.Details as [ProblemDetails],C.CaseNumber as [ReportID],
                    CFD.PICJson,CFD.FieldErrors, CFD.CreatedBy,CFD.CreatedOn,CFD.TouchpointVersion,CFD.TouchpointURL,CFD.BrowserDetails,
                    CFD.LastUpdatedBy,CFD.LastUpdatedOn, GETUTCDATE() as CurrentTime, Luv.[Name] as CloseReason,P.FirstName+' '+P.LastName as [UpdatedBy],PP.FirstName+' '+PP.LastName as [CreatedByName],
					CFD.ChangeCaseId,CC.[CaseNumber] as ChangeCaseNumber,
                    F.[Name] as FranchiseName, F.Code as OwnerId, F.LocalPhoneNumber as PhoneNumber, fv.AccountNo as VendorAccountNo
                    from CRM.CaseFeedbackDetails CFD
                    left join CRM.[Case] C on CFD.CaseId=C.CaseId
                    left join CRM.[Case] CC on CFD.ChangeCaseId=CC.CaseId
					left join [Acct].[HFCVendors] V on V.VendorId=C.VendorId
                    --left join acct.FranchiseVendors fv on c.VendorId=fv.VendorId
                    left join crm.HFCProducts Hp on Hp.ProductGroup=CFD.ProductGroupId 
                    left join CRM.Type_LookUpValues Lv on  CFD.RecordTypeId= Lv.Id
					left join CRM.Type_LookUpValues Tlv on C.StatusId=Tlv.Id
					left join CRM.Type_LookUpValues Luv on CFD.CloseReason=Luv.Id
					--left join CRM.Type_LookUpValues Tluv on CFD.ChangeCaseId=Tluv.Id
					left join CRM.Person P on CFD.LastUpdatedBy=P.PersonId
					left join CRM.Person PP on CFD.CreatedBy=PP.PersonId
                    left join CRM.Franchise F on C.FranchiseId=F.FranchiseId
                    left join [acct].[FranchiseVendors] fv on c.VendorId=fv.VendorId and fv.FranchiseId=c.FranchiseId
                    where C.CaseType='Feedback' and C.CaseId=@CaseId";
            var result = ExecuteIEnumerableObject<dynamic>(ContextFactory.CrmConnectionString, query, new { CaseId = CaseId }).FirstOrDefault();

            if (result != null)
            {
                result.CreatedOn = TimeZoneManager.ToLocal(result.CreatedOn);
                result.LastUpdatedOn = TimeZoneManager.ToLocal(result.LastUpdatedOn);
                result.CurrentTime = TimeZoneManager.ToLocal(result.CurrentTime);
            }
            return result;
        }

        public dynamic GetFeedbackList(bool IncludeClosed)
        {
            var query = @"select C.CaseId,V.[Name] as Vendor,C.CaseNumber as ID,lv.[Name] as [ErrorType],tlv.[name] as [Status],
                C.CreatedOn as [Created],C.LastUpdatedOn as [LastModified], 
                case when tlv.[name]='Closed' then DATEDIFF(day, CC.CreatedOn, CC.LastUpdatedOn) else DATEDIFF(day, CC.CreatedOn, GETUTCDATE()) end AS [DaysOpen],
                case when tlv.[name]='Closed' then null else DATEDIFF(day, CC.LastUpdatedOn, GETUTCDATE()) end AS [DaysSinceModified] ,
				PP.FirstName+' '+PP.LastName as [Originator],Luv.[Name] as CloseReason
                 from CRM.CaseFeedbackDetails CC
				left join CRM.[Case] C on C.CaseId = CC.CaseId 
                left join [Acct].[HFCVendors] V on C.VendorId = V.VendorId                
                left join CRM.Type_LookUpValues lv on  CC.RecordTypeId= lv.Id
                left join CRM.Type_LookUpValues tlv on C.StatusId=tlv.Id
				left join CRM.Person P on C.LastUpdatedBy=P.PersonId
				left join CRM.Type_LookUpValues Luv on CC.CloseReason=Luv.Id
					left join CRM.Person PP on C.CreatedBy=PP.PersonId
                where CaseType = 'Feedback'";
            if (!IncludeClosed)
                query += " and tlv.[Name] not in ('Closed')";

            query = query + " order by LastModified desc";
            var result = ExecuteIEnumerableObject<dynamic>(ContextFactory.CrmConnectionString, query).ToList();
            return result;
        }

        public dynamic GetFeedbackListByVendor(bool IncludeClosed)
        {

            var query = @"select C.CaseId,V.[Name] as Vendor,C.CaseNumber as ID,lv.[Name] as [ErrorType],tlv.[name] as [Status],
                C.CreatedOn as [Created],C.LastUpdatedOn as [LastModified],
                case when tlv.[name]='Closed' then DATEDIFF(day, CC.CreatedOn, CC.LastUpdatedOn) else DATEDIFF(day, CC.CreatedOn, GETUTCDATE()) end AS[DaysOpen],
                case when tlv.[name]='Closed' then null else DATEDIFF(day, CC.LastUpdatedOn, GETUTCDATE()) end AS [DaysSinceModified] ,
				PP.FirstName+' '+PP.LastName as [Originator],Luv.[Name] as CloseReason
                 from CRM.CaseFeedbackDetails CC
				left join CRM.[Case] C on C.CaseId = CC.CaseId 
                left join [Acct].[HFCVendors] V on C.VendorId = V.VendorId                
                left join CRM.Type_LookUpValues lv on  CC.RecordTypeId= lv.Id
                left join CRM.Type_LookUpValues tlv on C.StatusId=tlv.Id
				left join CRM.Person P on C.LastUpdatedBy=P.PersonId
				left join CRM.Type_LookUpValues Luv on CC.CloseReason=Luv.Id
					left join CRM.Person PP on C.CreatedBy=PP.PersonId
                where CaseType = 'Feedback' and C.VendorId in (select VendorId from Acct.HFCVendors where VendorIdPk in (select VendorId from CRM.VendorCaseConfig where PersonId = @PersonId))";

            if (!IncludeClosed)
                query += " and tlv.[Name] not in ('Closed')";

            query = query + " order by LastModified desc";

            var result = ExecuteIEnumerableObject<dynamic>(ContextFactory.CrmConnectionString, query, new { PersonId = SessionManager.CurrentUser.PersonId }).ToList();
            return result;
        }

        public dynamic GetChangeRequestList(bool IsPIC, bool IncludeClosed)
        {
            //need to join status table.
            var query = @"select C.CaseId,V.[Name] as Vendor,C.CaseNumber as ID,lv.[Name] as [ChangeType],tlv.[Name] as [Status],
                        C.CreatedOn as [RequestDate],C.LastUpdatedOn as [LastModified],CC.EffectiveDate,
                        case when tlv.[name]='In Production' then DATEDIFF(day, CC.CreatedOn, CC.LastUpdatedOn) else DATEDIFF(day, CC.CreatedOn, GETUTCDATE()) end AS [DaysOpen],
                        case when tlv.[name]='In Production' then null else DATEDIFF(day, CC.LastUpdatedOn, GETUTCDATE()) end AS [DaysSinceModified],
                        PP.FirstName+' '+PP.LastName as [Originator]
                         from CRM.CaseChangeDetails CC
                        left join CRM.[Case] C on C.CaseId=CC.CaseId 
                        left join [Acct].[HFCVendors] V on C.VendorId=V.VendorId
                        left join CRM.Type_LookUpValues lv on  CC.RecordTypeId= lv.Id
                        left join CRM.Type_LookUpValues tlv on C.StatusId=tlv.Id 
					    left join CRM.Person P on C.LastUpdatedBy=P.PersonId				 
					    left join CRM.Person PP on C.CreatedBy=PP.PersonId
                        where CaseType='Change'";
            if (IsPIC)
            {
                query = query + " and tlv.[Name] not in ('Submitted','Declined')"; //C.StatusId not in (13000,13002)";
            }
            if (!IncludeClosed)
                query += " and tlv.[Name] not in ('In Production')"; //C.StatusId not in (13005)";

            query = query + " order by LastModified desc";

            var result = ExecuteIEnumerableObject<dynamic>(ContextFactory.CrmConnectionString, query).ToList();
            return result;
        }

        public dynamic GetChangeRequestListByVendor(bool IncludeClosed)
        {
            var query = @"select C.CaseId,V.[Name] as Vendor,C.CaseNumber as ID,lv.[Name] as [ChangeType],tlv.[Name] as [Status],
                    C.CreatedOn as [RequestDate],'' as [EffectiveDate],C.LastUpdatedOn as [LastModified], CC.EffectiveDate,
                    case when tlv.[name]='In Production' then DATEDIFF(day, CC.CreatedOn, CC.LastUpdatedOn) else DATEDIFF(day, CC.CreatedOn, GETUTCDATE()) end AS [DaysOpen],
                    case when tlv.[name]='In Production' then null else DATEDIFF(day, CC.LastUpdatedOn, GETUTCDATE()) end AS [DaysSinceModified],
                    PP.FirstName+' '+PP.LastName as [Originator]
                     from CRM.CaseChangeDetails CC
                    left join CRM.[Case] C on C.CaseId=CC.CaseId 
                    left join [Acct].[HFCVendors] V on C.VendorId=V.VendorId
                    left join CRM.Type_LookUpValues lv on  CC.RecordTypeId= lv.Id
                    left join CRM.Type_LookUpValues tlv on C.StatusId=tlv.Id 
					left join CRM.Person P on C.LastUpdatedBy=P.PersonId				 
					left join CRM.Person PP on C.CreatedBy=PP.PersonId
                    where CaseType='Change' and C.VendorId in (select VendorId from Acct.HFCVendors where VendorIdPk in (select VendorId from CRM.VendorCaseConfig where PersonId = @PersonId))";
            if (!IncludeClosed)
                query += " and tlv.[Name] not in ('In Production')";
            query = query + " order by LastModified desc";
            var result = ExecuteIEnumerableObject<dynamic>(ContextFactory.CrmConnectionString, query, new { PersonId = SessionManager.CurrentUser.PersonId }).ToList();
            return result;
        }

        public string SendFeedbackStatusEmail(EmailType emailType, dynamic OldFeedback, dynamic UpdatedFeedback)  //, int emailType)
        {

            List<string> toAddresses = new List<string>();

            string fromEmailAddress = "";

            if (SessionManager.CurrentUser.Franchise == null)
                fromEmailAddress = SessionManager.CurrentUser.Email;
            else
            {
                if (Franchise.AdminEmail != "")
                    fromEmailAddress = Franchise.AdminEmail;
                else if (Franchise.OwnerEmail != "")
                    fromEmailAddress = Franchise.OwnerEmail;

            }

            string TemplateSubject = "", bodyHtml = "";
            int FranchiseId = SessionManager.CurrentFranchise == null ? 0 : SessionManager.CurrentFranchise.FranchiseId;

            int BrandId = (int)SessionManager.BrandId;

            //Take Body Html from EmailTemplate Table
            var emailtemplate = emailMgr.GetHtmlBody(emailType, BrandId);
            TemplateSubject = emailtemplate.TemplateSubject;
            bodyHtml = emailtemplate.TemplateLayout;
            short TemplateEmailId = (short)emailtemplate.EmailTemplateId;

            var mailMsg = emailMgr.BuildMailMessageCaseFeedbackStatus(OldFeedback, UpdatedFeedback, bodyHtml, TemplateSubject
                , TemplateEmailId, BrandId, FranchiseId
                , fromEmailAddress, toAddresses);

            return "Success";
        }

        public string SendChangeRequestStatusEmail(EmailType emailType, dynamic OldChangeDetails, dynamic UpdatedChangeDetails)  //, int emailType)
        {           

            List<string> toAddresses = new List<string>();

            //From email address
            string fromEmailAddress = "";

            if (SessionManager.CurrentUser.Franchise == null)
                fromEmailAddress = SessionManager.CurrentUser.Email;            

            string TemplateSubject = "", bodyHtml = "";
            int FranchiseId = 0;
            if (SessionManager.CurrentFranchise != null)
                FranchiseId = SessionManager.CurrentFranchise.FranchiseId;

            int BrandId = (int)SessionManager.BrandId;

            //Take Body Html from EmailTemplate Table
            var emailtemplate = emailMgr.GetHtmlBody(emailType, BrandId);
            TemplateSubject = emailtemplate.TemplateSubject;
            bodyHtml = emailtemplate.TemplateLayout;
            short TemplateEmailId = (short)emailtemplate.EmailTemplateId;

            var mailMsg = emailMgr.BuildMailMessageCaseChangeRequestStatus(OldChangeDetails, UpdatedChangeDetails, bodyHtml, TemplateSubject
                , TemplateEmailId, BrandId, FranchiseId
                , fromEmailAddress, toAddresses);

            //var result = UpdateSentEmail(leadId);
            return "Success";
        }

        public string GetEmailAddressByCaseIdUserRole(int CaseId, string RoleName)
        {
            var result = "";
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var query = "";
                if (CaseId != 0 && RoleName == "Vendor")
                {
                    query = @"select P.PrimaryEmail from crm.Person P inner join  
                            CRM.VendorCaseConfig VCC on P.PersonId = VCC.PersonId
                            left join Acct.HFCVendors HV on VCC.VendorId = hv.VendorIdPk
                            left join crm.[Case] C on hv.VendorId = C.VendorId where CaseId = @CaseId and ISNULL(VCC.VGEmailNotify,0)=1";
                    result = connection.Query<string>(query, new { CaseId = CaseId }).FirstOrDefault();
                }
                else if (CaseId != 0 && RoleName == "Franchise")
                {
                    //query = @"select  case when AdminEmail is not null then AdminEmail else OwnerEmail end as Email from crm.Franchise F inner join crm.[Case] C on F.FranchiseId=C.FranchiseId where C.CaseId=@CaseId";
                    query = @"select PrimaryEmail from CRM.Person P inner join crm.[Case] C on P.PersonId=C.CreatedBy where C.CaseId=@CaseId";
                    result = connection.Query<string>(query, new { CaseId = CaseId }).FirstOrDefault();
                }
                else if (RoleName == "TPTSupport")
                {
                    query = @"select value from crm.AppConfig where Name ='HFCTPTSupportEmail'";
                    result = connection.Query<string>(query).FirstOrDefault();
                }
                else
                {
                    query = @"select P.PrimaryEmail from crm.Person P 
                             inner join  auth.Users  U on P.PersonId=U.PersonId
                             left join Auth.UsersInRoles UIR on  U.UserId=UIR.UserId
                             left join Auth.Roles R on UIR.RoleId=R.RoleId where RoleName=@RoleName";
                    var res = connection.Query<dynamic>(query, new { RoleName = RoleName }).FirstOrDefault();

                    if (res != null)
                    {
                        if (res.Count > 1)
                        {
                            result = string.Join(",", res);
                        }
                        else
                            result = res.PrimaryEmail;
                    }
                    else
                        result = null;

                }
                return result;
            }
        }
        public Franchise getFranchiseTimeZone(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                {
                    return connection.Query<Franchise>(@"select f.* from CRM.Franchise f where FranchiseId in (select fv.FranchiseId from acct.FranchiseVendors fv join CRM.[Case] fc on fc.CreatedBy = fv.VendorIdPk
                          where fc.CaseId=@CaseId)", new { CaseId = id }).FirstOrDefault();

                }
            }

        }
        public override string Add(CaseFeedbackDetails data)
        {
            throw new NotImplementedException();
        }

        public override string Delete(int id)
        {
            throw new NotImplementedException();
        }

        public override CaseFeedbackDetails Get(int id)
        {
            throw new NotImplementedException();
        }

        public override ICollection<CaseFeedbackDetails> Get(List<int> idList)
        {
            throw new NotImplementedException();
        }

        public List<Case> GetChangeRequestCaseNumberByVendor(int VendorId)
        {
            var query = "";
            var result = new List<Case>();
            List<Role> roles = SessionManager.CurrentUser.Roles.ToList();
            if (roles.FindAll(delegate (Role role) { return role.RoleName == "Global Admin"; }).Count != 0)
            {
                query = @"select CaseNumber,CaseId from CRM.[Case] where CaseType='Change'order by CreatedOn desc";
                //  and VendorId in (select VendorId from Acct.HFCVendors where VendorIdPk in (select VendorId from CRM.VendorCaseConfig where PersonId = @PersonId)) ";
                result = ExecuteIEnumerableObject<Case>(ContextFactory.CrmConnectionString, query).ToList();
            }
            else
            {
                query = @"select CaseNumber,CaseId from CRM.[Case] where CaseType='Change' and 
                        VendorId in (select VendorId from Acct.HFCVendors where VendorIdPk in (select VendorId from CRM.VendorCaseConfig where PersonId = @PersonId)) order by CreatedOn desc";
                result = ExecuteIEnumerableObject<Case>(ContextFactory.CrmConnectionString, query, new { PersonId = SessionManager.CurrentUser.PersonId }).ToList();
            }

            return result;
        }

        public override string Update(CaseFeedbackDetails data)
        {
            throw new NotImplementedException();
        }
    }
}
