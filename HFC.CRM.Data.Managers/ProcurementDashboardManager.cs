﻿using HFC.CRM.Core.Membership;
using HFC.CRM.Data;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Data;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using HFC.CRM.Data.Context;
using HFC.CRM.DTO;
using HFC.CRM.DTO.ProcurementDashboard;
using System.Data.SqlClient;
using System.Dynamic;
using HFC.CRM.Core.Common;
using Dapper;
using HFC.CRM.Core;
using HFC.CRM.Core.Logs;
using HFC.CRM.DTO.Product;

namespace HFC.CRM.Managers
{
    public class ProcurementDashboardManager : DataManager
    {
        public ProcurementDashboardManager(User user, Franchise franchise)
        {
            this.User = user;
            this.Franchise = franchise;
        }

        private EmailManager eManager = new EmailManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);

        #region Procurement Dashboard

        public List<ProcurementDashboardDTO> Get(int vendorId, List<int> VPOStatusId, string date)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    string MPOStatusId = "", vpoStatusId = "";
                    if (VPOStatusId.Count > 0)
                    {
                        var linst = new List<int>();
                        if (VPOStatusId.Contains(0)) //// OPEN MPO's
                            MPOStatusId = "1";
                        if (VPOStatusId.Contains(1)) //// OPEN VPO
                            linst.Add(1);
                        if (VPOStatusId.Contains(2)) ///Shipped
                        {
                            linst.Add(3); linst.Add(4);
                        }
                        if (VPOStatusId.Contains(3)) //Invoiced
                            linst.Add(5);
                        if (VPOStatusId.Contains(4)) //Cancelled
                            linst.Add(8);
                        if (VPOStatusId.Contains(10)) //Cancelled
                            linst.Add(10);
                        if (linst.Count > 0)
                            vpoStatusId = string.Join(",", linst.ToArray());
                    }

                    var res = connection.Query<ProcurementDashboardDTO>("exec CRM.ProcurementDashboard @FranchiseId,@vendorId,@MPOStatusId,@VPOStatusId,@datefilter",
                        new
                        {
                            FranchiseId = Franchise.FranchiseId,
                            vendorId = vendorId,
                            MPOStatusId = MPOStatusId,
                            VPOStatusId = vpoStatusId,
                            datefilter = date
                        }).ToList();

                    if (VPOStatusId.Count == 1 && VPOStatusId[0] == 1)
                        res = res.Where(x => x.VPOStatus?.ToLower() == "open").ToList();

                    var listPurchaseOrderId = String.Join(",", res.Select(x => x.PurchaseOrderId).Distinct().ToList());
                    var test = res.Select(x => x.PICPO).Distinct().ToList();
                    var VPOs = res.Where(x => x.PICPO != 0).Select(x => x.PICPO).Distinct().ToList();
                    var snlquery1 =
                        @"SELECT distinct sn.PICVpo,sn.ShipNoticeId, sn.ShipmentId, sn.ShippedDate, sn.EstDeliveryDate, sn.ShippedVia, sn.BOL, sn.Weight, sn.TotalBoxes, sn.PurchaseOrderId, sn.PurchaseOrdersDetailId, sn.VendorName, sn.ReceivedComplete, sn.ReceivedCompleteDate, sn.CreatedBy, sn.CreatedOn, sn.UpdatedBy, sn.UpdatedOn FROM crm.ShipNotice sn
                                       INNER JOIN crm.ShipmentDetail sd ON sd.ShipNoticeId = sn.ShipNoticeId
                                       WHERE sn.PICVpo IN @listPurchaseOrderId;
                                       SELECT distinct sd.* FROM crm.ShipNotice sn
                                       INNER JOIN crm.ShipmentDetail sd ON sd.ShipNoticeId = sn.ShipNoticeId
                                       WHERE sn.PICVpo IN @listPurchaseOrderId";
                    var st = "('" + String.Join("','", VPOs) + "')";
                    string str = snlquery1.Replace("@listPurchaseOrderId", st);
                    var fDetails = connection.QueryMultiple(str);
                    var listSN = fDetails.Read<ShipNotice>().ToList();
                    var listSD = fDetails.Read<ShipmentDetail>().ToList();

                    for (var i = 0; i < listSN.Count; i++)
                    {
                        var data = res.Where(x => x.PICPO.ToString() == listSN[i].PICVpo).FirstOrDefault();
                        if (data.ListShipment == null)
                            data.ListShipment = new List<ShipNotice>();
                        data.ListShipment.Add(listSN[i]);
                        //if (res[i].PICPO != 0)
                        //{
                        //    res[i].ListShipment = new List<ShipNotice>();
                        //    res[i].ListShipment =
                        //        listSN.Where(x => x.PICVpo == res[i].PICPO.ToString()).ToList();
                        //}
                    }

                    var viQuery = @"SELECT vi.PICVpo,vi.VendorInvoiceId, vi.PurchaseOrderId, vi.PurchaseOrdersDetailId, vi.InvoiceNumber, vi.[Date], vi.Terms, vi.RemitByDate, vi.Quantity, vi.Subtotal, vi.Tax, vi.Freight, vi.Discount, vi.DiscountDate, vi.Total, vi.CreatedOn, vi.CreatedBy, vi.UpdatedOn, vi.UpdatedBy, vi.VendorName, vi.PICAP, vi.PICVpo as PICPO FROM crm.VendorInvoice vi
                                    WHERE vi.PICVpo IN @listPurchaseOrderId";
                    var multiDetails = connection.QueryMultiple(viQuery, new { listPurchaseOrderId = test });
                    var vnDetails = multiDetails.Read<VendorInvoice>().ToList();
                    for (var i = 0; i < vnDetails.Count; i++)
                    {
                        var data = res.Where(x => x.PICPO.ToString() == vnDetails[i].PICVpo).FirstOrDefault();
                        if (data.ListVendorInvoice == null)
                            data.ListVendorInvoice = new List<VendorInvoice>();
                        data.ListVendorInvoice.Add(vnDetails[i]);
                        //if (res[i].PICPO != 0)
                        //{
                        //    res[i].ListVendorInvoice = new List<VendorInvoice>();
                        //    res[i].ListVendorInvoice = vnDetails
                        //        .Where(x => x.PICVpo == res[i].PICPO.ToString()).ToList();
                        //}
                    }

                    var fe_timezone = Franchise.TimezoneCode;
                    foreach (var item in res)
                    {
                        item.MPODate = TimeZoneManager.ToLocal(item.MPODate, fe_timezone).Date;
                        item.OrderDate = TimeZoneManager.ToLocal(item.OrderDate, fe_timezone).Date;
                        item.VPODate = TimeZoneManager.ToLocal(item.VPODate, fe_timezone).Date;
                        var temp = TimeZoneManager.ToLocal(item.SubmittedDate, fe_timezone);
                        if (temp.HasValue)
                        {
                            item.SubmittedDate = temp.Value.Date;
                        }
                        //item.SubmittedDate = TimeZoneManager.ToLocal(item.SubmittedDate, fe_timezone);

                        // If the item is an xmpo, it is not specific to one Order/Quote/Opportunity/Account
                        // so we are not displaying those information to the user.
                        if (item.IsXMpo)
                        {
                            item.AccountId = 0;
                            item.AccountName = "";
                            item.OrderID = 0;
                            item.OrderNumber = "";
                            item.OpportunityId = 0;
                            item.OpportunityName = "";
                        }
                    }
                    return res;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public List<ProductListNameModel> GetVendorname(int id)
        {
            var NameList = ExecuteIEnumerableObject<ProductListNameModel>(ContextFactory.CrmConnectionString,
            @" select hv.VendorIdPk,hv.VendorId, hv.Name,hv.VendorType from Acct.HFCVendors hv
                   join Acct.FranchiseVendors fv on hv.VendorId=fv.VendorId
                   where FranchiseId=@FranchiseId and fv.VendorStatus=1 ",
            new { FranchiseId = SessionManager.CurrentFranchise.FranchiseId }).ToList();

            return NameList;
        }

        #endregion Procurement Dashboard

        #region Global/Admin Procurement Dashboard

        public List<ProductListNameModel> GetAdminVendorname(int id)
        {
            // var BrandId = SessionManager.BrandIdOld;
            var NameList = ExecuteIEnumerableObject<ProductListNameModel>(ContextFactory.CrmConnectionString,
                               @" select hv.VendorIdPk,hv.VendorId, hv.Name,hv.VendorType from Acct.HFCVendors hv
                  where hv.VendorType=1 and hv.isalliance=1 and hv.BudgetBlind=1").ToList();

            return NameList;
        }

        public List<Franchise> GetAdminFranchisename(int id)
        {
            var FranchisenameList = ExecuteIEnumerableObject<Franchise>(ContextFactory.CrmConnectionString,
           @"select FranchiseId,Name from CRM.Franchise
             where IsDeleted=0 and IsSuspended=0 and BrandId=1 order by Name Asc",
           new { BrandId = 1 }).ToList();//Removed SessionManager.BrandIdOld as in query it is hardcoded.
            return FranchisenameList;
        }

        public dynamic Type_DateTime(int Tableid)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = @"select * from [CRM].[Type_LookUpValues] where TableId =@tableId";
                var result = connection.Query(query, new { tableId = Tableid }).ToList();
                connection.Close();
                return result;
            }
        }

        public List<ProcurementDashboardDTO> GetData(int franchiseid, int vendorId, string date)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var query =
                        @"SELECT DISTINCT top 2000 f.Name AS FranchiseName,CONVERT(date, sn.ShippedDate) ShippedDate,ql.ProductTypeId,mpo.MasterPONumber,mpo.PurchaseOrderId,pod.PICPO,o.OrderID,o.OrderNumber,CONVERT(date, o.CreatedOn) AS OrderDate,CONVERT(date, mpo.CreateDate) AS MPODate,
                        o2.OpportunityId, o2.OpportunityName, a.AccountId, case when p.[CompanyName] is not null and p.[CompanyName]!='' then p.[CompanyName] else p.FirstName + ' ' +p.LastName end as AccountName,
                        CONVERT(date,pod.CreatedOn) as VPODate,CONVERT(date,pod.PromiseByDate) PromiseByDate,CONVERT(date, pod.ShipDate) as EstShipDate, pod.StatusId, pod.Status as VPOStatus, ql.VendorId, ql.VendorName,ql.VendorId,
                        q.CoreProductMargin, q.CoreProductMarginPercent, pod.Errors, sum (qld.BaseCost) MPOCost, sum(ql.ExtendedPrice) MPOValue,CONVERT(date, o.ContractedDate) ContractedDate,CONVERT(date, MPO.SubmittedDate) SubmittedDate,
                        (Select  Count(quotekey) from crm.quotelines where quotekey=o.QuoteKey and producttypeid=1 group by quotekey) as Orderlines, CASE WHEN pod.StatusId = 10 THEN 99
              WHEN pod.StatusId = 9 THEN 98
			    WHEN pod.StatusId = 4 THEN 97
              ELSE pod.StatusId END  AS sortorder
                        FROM crm.MasterPurchaseOrder mpo
                        INNER JOIN crm.PurchaseOrderDetails pod ON pod.PurchaseOrderId = mpo.PurchaseOrderId
                        Left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                        INNER JOIN crm.Orders o ON o.OrderId = mpox.OrderId --mpo.OrderID
                        INNER JOIN crm.Opportunities o2 ON o.OpportunityId = o2.OpportunityId

                        INNER JOIN crm.Quote q ON q.OpportunityId = o2.OpportunityId

                        INNER JOIN crm.Accounts a ON mpox.AccountId = a.AccountId
                        INNER JOIN crm.Customer p ON p.CustomerId = a.PersonId
                        INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId AND ql.ProductTypeId = 1

                        INNER JOIN crm.QuoteLineDetail qld ON qld.QuoteLineId = ql.QuoteLineId

                        INNER JOIN CRM.Franchise f ON f.FranchiseId = o2.FranchiseId
                        LEFT JOIN crm.ShipNotice sn ON sn.PICVpo = pod.PICPO WHERE 1=1";

                    if (vendorId != 0)
                    {
                        query = query + " " + "AND ql.VendorId = " + vendorId;
                    }

                    if (franchiseid != 0)
                    {
                        query = query + " " + "AND f.FranchiseId = " + franchiseid;
                    }

                    switch (date)
                    {
                        case "4000": //Today
                            query = query + " " + "AND mpo.CreateDate >= GETDATE()";
                            break;

                        case "4001": //Lastday
                            query = query + " " + "AND mpo.CreateDate > GETDATE()-1";
                            break;

                        case "4002": //Week to Date
                            query = query + " " + "AND mpo.CreateDate BETWEEN (SELECT DATEADD(WEEKDAY, DATEDIFF(WEEKDAY, 0, GETDATE()), -1)) AND GETDATE() ";
                            break;

                        case "4003": //Month to Date
                            query = query + " " + "AND mpo.CreateDate >= (SELECT DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0))";
                            break;

                        case "4004": //Year to Date
                            query = query + " " + "AND mpo.CreateDate >= (SELECT DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0))";
                            break;

                        case "4005": //Last 7 days
                            query = query + " " + "AND mpo.CreateDate > GETDATE()-7 ";
                            break;

                        case "4006": //Last 14 days
                            query = query + " " + "AND mpo.CreateDate > GETDATE()-14";
                            break;

                        case "4007": //Last 30 days
                            query = query + " " + "AND mpo.CreateDate > GETDATE()-30";
                            break;

                        case "4008": //Last 90 days
                            query = query + " " + "AND mpo.CreateDate > GETDATE()-90";
                            break;

                        case "4009": //Last 180 day
                            query = query + " " + "AND mpo.CreateDate > GETDATE()-180";
                            break;

                        default:
                            break;
                    }

                    var mpoQuery = string.Empty;
                    var vpoQuery = string.Empty;

                    var strquery = string.Empty;
                    if (mpoQuery != string.Empty && vpoQuery != string.Empty)
                    {
                        strquery = mpoQuery + "union all " + vpoQuery;
                    }
                    else if (mpoQuery == string.Empty && vpoQuery != string.Empty)
                    {
                        strquery = vpoQuery;
                    }
                    else if (mpoQuery != string.Empty && vpoQuery == string.Empty)
                    {
                        strquery = mpoQuery;
                    }
                    else
                    {
                        strquery = query;
                    }

                    strquery = strquery +
                        @" GROUP BY
                        f.Name, sn.ShippedDate,ql.ProductTypeId,mpo.MasterPONumber,mpo.PurchaseOrderId,pod.PICPO,pod.PurchaseOrdersDetailId,o.OrderID,o.OrderNumber,o.CreatedOn,mpo.CreateDate,
                        o2.OpportunityId, o2.OpportunityName, a.AccountId, p.[CompanyName], p.FirstName,p.LastName,
                        pod.CreatedOn, pod.PromiseByDate, pod.ShipDate , pod.StatusId, pod.Status, ql.VendorId, ql.VendorName,ql.VendorId,
                        q.CoreProductMargin, q.CoreProductMarginPercent, pod.Errors,qld.BaseCost,ql.ExtendedPrice,o.QuoteKey, o.ContractedDate,MPO.SubmittedDate order by CONVERT(date, mpo.CreateDate),sortorder desc;";

                    connection.Open();
                    var res = connection.Query<ProcurementDashboardDTO>(strquery, commandType: CommandType.Text).ToList();
                    res = res.OrderBy(x => x.MPODate).ToList();
                    var listPurchaseOrderId = String.Join(",", res.Select(x => x.PurchaseOrderId).Distinct().ToList());
                    var test = res.Select(x => x.PurchaseOrderId).Distinct().ToList();
                    var snSQLQuery = @"SELECT distinct sn.ShipNoticeId, sn.ShipmentId, sn.ShippedDate, sn.EstDeliveryDate, sn.ShippedVia, sn.BOL, sn.Weight, sn.TotalBoxes, sn.PurchaseOrderId, sn.PurchaseOrdersDetailId, sn.VendorName, sn.ReceivedComplete, sn.ReceivedCompleteDate, sn.CreatedBy, sn.CreatedOn, sn.UpdatedBy, sn.UpdatedOn FROM crm.ShipNotice sn
                                       INNER JOIN crm.ShipmentDetail sd ON sd.ShipmentId = sn.ShipmentId
                                       WHERE sn.PurchaseOrderId IN @listPurchaseOrderId;
                                       SELECT distinct sd.* FROM crm.ShipNotice sn
                                       INNER JOIN crm.ShipmentDetail sd ON sd.ShipmentId = sn.ShipmentId
                                       WHERE sn.PurchaseOrderId IN @listPurchaseOrderId;";

                    var fDetails = connection.QueryMultiple(snSQLQuery, new { listPurchaseOrderId = test });
                    var listSN = fDetails.Read<ShipNotice>().ToList();
                    var listSD = fDetails.Read<ShipmentDetail>().ToList();
                    for (var i = 0; i < res.Count; i++)
                    {
                        if (res[i].PICPO != 0)
                        {
                            res[i].ListShipment = new List<ShipNotice>();
                            res[i].ListShipment =
                                 listSN.Where(x => x.PICVpo == res[i].PICPO.ToString()).ToList();
                        }
                    }

                    var viQuery = @"SELECT vi.VendorInvoiceId, vi.PurchaseOrderId, vi.PurchaseOrdersDetailId, vi.InvoiceNumber, vi.[Date], vi.Terms, vi.RemitByDate, vi.Quantity, vi.Subtotal, vi.Tax, vi.Freight, vi.Discount, vi.DiscountDate, vi.Total, vi.CreatedOn, vi.CreatedBy, vi.UpdatedOn, vi.UpdatedBy, vi.VendorName, vi.PICAP, pod.PICPO FROM crm.VendorInvoice vi
                                    INNER JOIN crm.PurchaseOrderDetails pod ON pod.PurchaseOrdersDetailId = vi.PurchaseOrdersDetailId
                                    WHERE pod.PurchaseOrderId IN @listPurchaseOrderId;";
                    var multiDetails = connection.QueryMultiple(viQuery, new { listPurchaseOrderId = test });
                    var vnDetails = multiDetails.Read<VendorInvoice>().ToList();
                    for (var i = 0; i < res.Count; i++)
                    {
                        if (res[i].PICPO != 0)
                        {
                            res[i].ListVendorInvoice = new List<VendorInvoice>();
                            res[i].ListVendorInvoice = vnDetails
                                .Where(x => x.PICVpo == res[i].PICPO.ToString()).ToList();
                        }
                    }

                    return res.OrderByDescending(x => x.VPODate).ToList();
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public string SendProcurementIssueEmail()
        {
            List<string> toAddresses = new List<string>();
            List<string> ccAddresses = null;
            List<string> bccAddresses = null;
            CustomerTP tocustomerAddress = new CustomerTP();
            string Streamid = "";

            dynamic obj = new ExpandoObject();
            obj = MPOswithIssues();
            string fromEmailAddress = "noreply@budgetblinds.com";/*new MailAddress(fromEmailAddress, SessionManager.CurrentUser.Person.FullName);*/
            var toemadd = AppConfigManager.GetEmailForProcurementNotification.ToString();

            string[] emailadds = toemadd.Split(',');

            for (int i = 0; i < emailadds.Length; i++)
            {
                toAddresses.Add(emailadds[i]);
            }

            //If no from or to emails return
            if (toAddresses == null || toAddresses.Count == 0)
                return "Success";

            //Preparing email Body with formating
            string TemplateSubject = "", bodyHtml = "";
            int FranchiseId = 0;

            //Take Body Html from EmailTemplate Table
            var emailtemplate = eManager.GetHtmlBody(EmailType.ProcurementProcessingIssues, 1); //9, 1);
            TemplateSubject = emailtemplate.TemplateSubject;
            bodyHtml = emailtemplate.TemplateLayout;
            short TemplateEmailId = (short)emailtemplate.EmailTemplateId;
            if (obj.Count > 0)
            {
                var mailMsg = eManager.BuildMailForProcurementError(obj,
                bodyHtml, TemplateSubject, TemplateEmailId, 1, FranchiseId,
                fromEmailAddress, toAddresses, ccAddresses, bccAddresses, Streamid);
            }

            return "Success";
        }

        public dynamic MPOswithIssues()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    var query =
                        @"SELECT f.Name,f.Code,isNULL(q.SideMark,q.QuoteName) AS sidemark,mpo.CounterId,mpo.SubmittedDate,mpo.MasterPONumber ,mpo.PurchaseOrderId,mpo.* FROM crm.MasterPurchaseOrder mpo
	                        INNER JOIN crm.MasterPurchaseOrderExtn mpox on mpo.PurchaseOrderId = mpox.PurchaseOrderId
                            INNER JOIN crm.Quote q ON q.QuoteKey = mpox.QuoteId
                            INNER JOIN crm.Accounts a ON a.AccountId = mpox.AccountId
                            INNER JOIN crm.Franchise f ON f.FranchiseId = a.FranchiseId
                            WHERE mpo.POStatusId = 9 AND DATEDIFF(hh,mpo.SubmittedDate,GETUTCDATE()  )>8;";
                    var listMasterPurchaseOrders = connection.Query<dynamic>(query).ToList();
                    return listMasterPurchaseOrders;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw ex;
                    //continue;
                }
            }
        }

        #endregion Global/Admin Procurement Dashboard
    }
}