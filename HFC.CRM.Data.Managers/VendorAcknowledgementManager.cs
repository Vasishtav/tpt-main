﻿using Dapper;
using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using HFC.CRM.Core.Logs;
using HFC.CRM.DTO.VendorInvoice;
using Z.Dapper.Plus;
using HFC.CRM.Core;

namespace HFC.CRM.Managers
{
    public class VendorAcknowledgementManager : DataManager
    {
        public VendorAcknowledgementManager()
        {
        }
        public VendorAcknowledgementManager(User user, Franchise franchise) : this(user, user, franchise)
        {
        }
        public VendorAcknowledgementManager(User user, User authorizingUser, Franchise franchise)
        {
            this.User = user;
            this.AuthorizingUser = authorizingUser;
            this.Franchise = franchise;
        }
        PICManager picMgr = new PICManager();
        PurchaseOrderManager poManager = new PurchaseOrderManager();

        /// <summary>
        /// Get Vendor Acknowledgement Details by VendorReferenceNumber
        /// </summary>
        /// <param name="VendorRefenceNumber"></param>
        /// <returns></returns>
        public VendorAcknowledgement GetDetails(int PurchaseOrderId, string VendorReferenceNumber)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {

                    var sQuery = @"select * from crm.VendorPurchaseOrder where PurchaseOrderId = @PurchaseOrderId 
                                    AND vendorReference=@VendorReferenceNumber;";

                    var vpoData = connection.Query<VendorPurchaseOrder>(sQuery, new { PurchaseOrderId, VendorReferenceNumber }).FirstOrDefault();

                    if (vpoData == null)
                    {
                        poManager.UpdateVPOPICResponse(PurchaseOrderId, VendorReferenceNumber);
                        vpoData = connection.Query<VendorPurchaseOrder>(sQuery, new { PurchaseOrderId, VendorReferenceNumber }).FirstOrDefault();
                    }

                    var sQueryMultiple = @"select pod.*,
                                    case when tuom.UomDefinition is null then pod.UoM else tuom.UomDefinition end as UoMDesc
                                    FROM crm.PurchaseOrderDetails pod
                                    inner join crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = pod.PurchaseOrderId
                                    inner join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                    inner join crm.Opportunities opp ON opp.OpportunityId = mpox.OpportunityId
                                    inner join crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
                                    left join crm.Type_ShipmentUoM tuom on pod.UoM=tuom.UomCode
                                    WHERE ql.ProductTypeId = 1 AND pod.StatusId NOT IN (7,8) --7 is closed and 8 is cancelled
		                            AND opp.FranchiseId = @FranchiseId AND pod.PurchaseOrderId = @PurchaseOrderId 
                                    AND pod.vendorReference=@VendorReferenceNumber;

                                select pod.PurchaseOrderId,pod.QuoteLineId,ql.QuoteLineNumber as OrderLineNumber,o.OrderNumber,o.OrderId,
                                    case when ISNULL(q.sidemark, '') <> '' then q.SideMark
	                                    when ISNULL(opp.SideMark, '') <> '' then opp.SideMark
	                                    else q.QuoteName 
	                                end as OrderSideMark,qld.BaseCost,ql.ProductName +' - '+ ql.[Description] AS ItemDescription
                                    from crm.PurchaseOrderDetails pod
	                                inner join crm.MasterPurchaseOrder mpo on pod.PurchaseOrderId=mpo.PurchaseOrderId
	                                inner join crm.MasterPurchaseOrderExtn mpox on mpo.PurchaseOrderId=mpox.PurchaseOrderId
	                                inner join crm.Orders o on o.OrderID=mpox.OrderId
                                    inner join crm.Opportunities opp on opp.OpportunityId=mpox.OpportunityId
									inner join crm.Quote q on o.QuoteKey=q.QuoteKey
	                                inner join crm.QuoteLines ql on pod.QuoteLineId=ql.QuoteLineId
									inner join crm.QuoteLineDetail qld on ql.QuoteLineId=qld.QuoteLineId 
                                    where pod.PurchaseOrderId = @PurchaseOrderId and vendorReference=@VendorReferenceNumber;";

                    var dbData = connection.QueryMultiple(sQueryMultiple,
                                                    new
                                                    {
                                                        FranchiseId = SessionManager.CurrentFranchise.FranchiseId,
                                                        PurchaseOrderId,
                                                        VendorReferenceNumber
                                                    });
                    var LineItems = dbData.Read<PurchaseOrderDetails>().ToList();
                    var orderQuoteData = dbData.Read<POOrderQuoteDetails>().ToList();  //Second Query is to take the OrderNumber and QuoteLineNumber

                    VendorAcknowledgement vendorAckData = new VendorAcknowledgement();

                    if (vpoData != null)
                    {
                        var ackHeader = new VendorAcknowledgementHeader();
                        vendorAckData.VendorAcknowledgementLineDetails = new List<VendorAcknowledgementLineDetail>();

                        //Updating Header details from VendorPurchaseOrder Table
                        ackHeader.PONumber = vpoData.PICPO.ToString();
                        ackHeader.VendorRef = vpoData.VendorReference;
                        ackHeader.Currency = vpoData.Currency;

                        ackHeader.Terms = vpoData.Terms;
                        ackHeader.VendorNote = vpoData.VendorMSG;
                        ackHeader.Comments = vpoData.Comment;

                        ackHeader.AckDate = vpoData.DateAcknowledged;


                        if (!string.IsNullOrEmpty(vpoData.ShipName))
                            ackHeader.SoldTo = vpoData.ShipName;
                        else
                            ackHeader.SoldTo = (this.Franchise == null ? "" : this.Franchise.Name);

                        ackHeader.ShipTo = CombineAddressFields(vpoData.ShipAddress1, vpoData.ShipAddress2, vpoData.ShipCity,
                                                                vpoData.ShipState, vpoData.ShipZip, vpoData.ShipCountry);

                        ackHeader.TotalLines = LineItems.Count;

                        int LowestStatusId = 0;
                        DateTime? farthestEstShipDate = null;
                        //Updating Line Details
                        foreach (PurchaseOrderDetails podetail in LineItems)
                        {
                            ackHeader.TotalQTY += podetail.QTY;
                            ackHeader.TotalCost += podetail.AckCost;

                            int PICPOStatusId = poManager.ReturnPICPOStatusId(podetail.StatusId);
                            if (LowestStatusId == 0 || PICPOStatusId < LowestStatusId)
                                LowestStatusId = PICPOStatusId;

                            if (farthestEstShipDate == null)
                            {
                                farthestEstShipDate = podetail.estShipDate;
                            }
                            else
                            {
                                if (podetail.estShipDate > farthestEstShipDate)
                                {
                                    farthestEstShipDate = podetail.estShipDate;
                                }
                            }

                            VendorAcknowledgementLineDetail linedetail = new VendorAcknowledgementLineDetail();
                            linedetail.POLine = podetail.woItem;
                            linedetail.QTY = podetail.QTY;
                            linedetail.UoM = podetail.UoMDesc;
                            linedetail.Item = podetail.item;
                            linedetail.PartNumber = podetail.PartNumber;
                            //linedetail.Description = VPOItem.description;
                            linedetail.Discount = podetail.Discount;
                            linedetail.Cost = podetail.AckCost;
                            linedetail.Status = podetail.Status;
                            linedetail.EstShipDate = podetail.estShipDate;

                            var poOrderQuoteDetail = orderQuoteData.Where(x => x.QuoteLineId == podetail.QuoteLineId).FirstOrDefault();

                            if (poOrderQuoteDetail != null)
                            {
                                linedetail.OrderId = poOrderQuoteDetail.OrderId;
                                linedetail.OrderNumber = poOrderQuoteDetail.OrderNumber;
                                linedetail.OrderSideMark = poOrderQuoteDetail.OrderSideMark;
                                linedetail.OrderLineNumber = poOrderQuoteDetail.OrderLineNumber;
                                linedetail.QuotePrice = poOrderQuoteDetail.BaseCost;
                                linedetail.Description = poOrderQuoteDetail.ItemDescription;
                            }
                            vendorAckData.VendorAcknowledgementLineDetails.Add(linedetail);
                        }
                        ackHeader.AckStatus = poManager.ReturnPOStatus(LowestStatusId);
                        ackHeader.EstShipDate = farthestEstShipDate;
                        vendorAckData.VendorAcknowledgementHeader = ackHeader;
                        vendorAckData.PurchaseOrderId = PurchaseOrderId;
                        vendorAckData.PICPO = vpoData.PICPO.ToString();
                        vendorAckData.vendorReference = VendorReferenceNumber;
                    }

                    return vendorAckData;
                }
                catch (Exception ex) { throw ex; }
            }
        }

        public string CombineAddressFields(string Address1, string Address2, string City, string State, string Zip, string Country)
        {
            string FullAddress = (((Address1 != null && Address1 != string.Empty) ? Address1 + ", " : "")
                            + ((Address2 != null && Address2 != string.Empty) ? Address2 + ", " : "")
                            + ((City != null && City != string.Empty) ? City + ", " : "")
                            + ((State != null && State != string.Empty) ? State + ", " : "")
                            + ((Zip != null && Zip != string.Empty) ? Zip + ", " : "")
                            + ((Country != null && Country != string.Empty) ? Country : ""));
            return FullAddress;
        }
    }
}
