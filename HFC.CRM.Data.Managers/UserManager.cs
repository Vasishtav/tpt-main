﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Core.Membership;
using System.Data.SqlClient;
using HFC.CRM.Data.Context;
using Dapper;
using HFC.CRM.DTO.Person;
using HFC.CRM.Core;
using HFC.CRM.Core.Common;

namespace HFC.CRM.Managers
{
    public class UserManager : DataManager
    {
        public UserManager(User user, Franchise franchise)
        {
            Franchise = franchise;
            User = user;
        }
        public string GetADDetails(string userName)
        {
            string primaryEmail = "";
            string domain = null;

            if (string.IsNullOrEmpty(domain))
                domain = ExchangeManager.Domain;
            using (var context = new PrincipalContext(ContextType.Domain, domain, "synctest1", "Budget123"))
            {
                using (var searcher = new PrincipalSearcher(new UserPrincipal(context)))
                {
                    try
                    {
                        searcher.QueryFilter.SamAccountName = userName;
                        var result = searcher.FindOne();
                        if (result != null)
                        {
                            DirectoryEntry de = result.GetUnderlyingObject() as DirectoryEntry;
                            if (de.Properties["mail"] != null && de.Properties["mail"].Value != null)
                                primaryEmail = de.Properties["mail"].Value.ToString();
                            if (de.Properties["GivenName"] != null && de.Properties["GivenName"].Value != null)
                            {
                                var gn = de.Properties["GivenName"].Value.ToString();
                            }
                            if (de.Properties["sn"] != null && de.Properties["sn"].Value != null)
                            {
                                var sn = de.Properties["sn"].Value.ToString();
                            }
                        }
                    }
                    catch (Exception ex) { ApiEventLogger.LogEvent(ex, userName); }
                }
            }
            return primaryEmail;
        }

        public string[] GetADDetail(string userName)
        {
            string primaryEmail = "", GivenName = "", Surname = "";
            string domain = null;

            if (string.IsNullOrEmpty(domain))
                domain = ExchangeManager.Domain;
            using (var context = new PrincipalContext(ContextType.Domain, domain, "synctest1", "Budget123"))
            {
                using (var searcher = new PrincipalSearcher(new UserPrincipal(context)))
                {
                    try
                    {
                        searcher.QueryFilter.SamAccountName = userName;
                        var result = searcher.FindOne();
                        if (result != null)
                        {
                            DirectoryEntry de = result.GetUnderlyingObject() as DirectoryEntry;
                            if (de.Properties["mail"] != null && de.Properties["mail"].Value != null)
                                primaryEmail = de.Properties["mail"].Value.ToString();
                            if (de.Properties["GivenName"] != null && de.Properties["GivenName"].Value != null)
                                GivenName = de.Properties["GivenName"].Value.ToString();
                            if (de.Properties["sn"] != null && de.Properties["sn"].Value != null)
                                Surname = de.Properties["sn"].Value.ToString();
                        }
                    }
                    catch (Exception ex) { ApiEventLogger.LogEvent(ex, userName); }
                }
            }
            return new string[] { primaryEmail, GivenName, Surname };
        }

        public User GetUser(string userid)
        {
            var uid = new Guid(userid);
            string[] includes = { "Person", "Address", "Roles", "ColorType" };

            var model = LocalMembership.GetUser(uid, includes);
            TimeZoneManager.ConvertToLocal(model);


            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                string qTZ = @"select TimezoneCode from CRM.Person where PersonId=@PersonId";
                int? TimezoneCode = connection.Query<int?>(qTZ, new { PersonId = model.Person.PersonId }).FirstOrDefault();
                model.Person.TimezoneCode = TimezoneCode != null && TimezoneCode != 0 ? (TimeZoneEnum)TimezoneCode : TimeZoneEnum.UTC;
                connection.Close();
            }
            return model;
        }

        public List<User> GetUsersInfo()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                string quser = @"select UserId,UserName,u.Email,IsDisabled,p.FirstName,p.LastName,p.PrimaryEmail from Auth.Users u
                                        join CRM.Person p on u.PersonId=p.PersonId
                                        where u.FranchiseId is null";
                var usersinfo = connection.Query<UserInfoDTO>(quser).ToList();
                connection.Close();
                List<User> Users = new List<User>();
                foreach (var u in usersinfo)
                {
                    User us = new User();
                    us.UserId = u.UserId;
                    us.UserName = u.UserName;
                    us.Email = u.Email;
                    us.IsDisabled = u.IsDisabled;
                    us.Person = new Person() { FirstName = u.FirstName, LastName = u.LastName, PrimaryEmail = u.PrimaryEmail };
                    Users.Add(us);
                }
                return Users;
            }
        }

        public string CreateVendorUser(string username)
        {
            var status = "Invalid user data";

            Guid userId = Guid.Empty;
            if (username != null && username != "")
            {
                var ADDetail = GetADDetail(username);
                if (ADDetail != null && ADDetail[0] == "")
                    return "Invalid HFC Username.  Please check the spelling of the username issued by HFC Tech Support and contact them if the problem persists.";

                var person = new Person
                {
                    FirstName = ADDetail[1],
                    LastName = ADDetail[2],
                    CellPhone = "",
                    HomePhone = "",
                    PreferredTFN = "",
                    PrimaryEmail = ADDetail[0]
                };

                var RoleIds = new List<Guid>() { AppConfigManager.AppVendorRole.RoleId };

                int ColorId = 0;
                using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    connection.Open();
                    string qgetcolorid = @"select top 1 tc.ColorId from Auth.Users u
                                        right join [CRM].[Type_Colors] tc on u.ColorId=tc.ColorId
                                        where u.FranchiseId is null and u.UserId is null";
                    ColorId = connection.Query<int>(qgetcolorid).FirstOrDefault();
                    connection.Close();
                }

                var user = new User()
                {
                    CreationDateUtc = DateTime.UtcNow,
                    UserName = username,
                    Person = person,
                    Email = ADDetail[0],
                    Address = null,
                    FranchiseId = null,
                    IsApproved = true,
                    CalSync = false,
                    Comment = "",
                    CalendarFirstHour = 8,
                    Domain = "bbi.corp",
                    DomainPath = @"LDAP://bbi.corp/DC=bbi,DC=corp",
                    Password = string.Empty,
                    ColorId = ColorId,
                    Roles = RoleIds != null ? CacheManager.RoleCollection.Where(w => RoleIds.Contains(w.RoleId)).ToList() : new List<Role>(),
                    PermissionSets = null
                };

                status = LocalMembership.CreateUser(user, true, out userId);

            }
            var result = new { status, UserId = userId };
            return "Success";

        }
    }
}
