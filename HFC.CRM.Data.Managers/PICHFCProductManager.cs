﻿using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using HFC.CRM.DTO.PIC;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Newtonsoft.Json;
using HFC.CRM.Core.Extensions;
using Dapper;

namespace HFC.CRM.Managers
{
    public class PICHFCProductManager : DataManager<HFCProducts>
    {
        public override string Add(HFCProducts data)
        {
            throw new NotImplementedException();
        }

        public override string Delete(int id)
        {
            throw new NotImplementedException();
        }

        public override HFCProducts Get(int id)
        {
            throw new NotImplementedException();
        }

        public override ICollection<HFCProducts> Get(List<int> idList)
        {
            throw new NotImplementedException();
        }

        public override string Update(HFCProducts data)
        {
            throw new NotImplementedException();
        }

        public int InsertProductCategory(string productCategory)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                int categoryId = 0;
                connection.Open();
                try
                {
                    var category = new Type_ProductCategory();
                    category.ProductCategory = productCategory;
                    //Marking parent as  WINDOW COVERINGS
                    category.Parant = 9;
                    category.IsActive = true;
                    category.IsDeleted = false;
                    category.CreatedOn = DateTime.UtcNow;
                    category.CreatedBy = 1107554;
                    category.LastUpdatedBy = 1107554;
                    category.LastUpdatedOn = DateTime.UtcNow;
                    //For now default values these will be used only for My products and one time Products
                    category.PICGroupId = 100;
                    category.PICProductId = 10000;
                    //This is only for Budgetblinds
                    category.BrandId = 1;
                    category.SalesTaxCode = null;
                    category.ProductTypeId = 1;

                    categoryId = connection.Insert<int, Type_ProductCategory>(category);
                    return categoryId;

                }
                catch(Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    return categoryId;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        /// <summary>
        /// Insert Or Update Product Id and Description
        /// </summary>
        public bool ImportProducts(int VendorID, ProductRoot pr)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                try
                {
                    string query = "select * from [CRM].[HFCProducts]";
                    var listhfcproduct = connection.Query<HFCProducts>(query).ToList();
                    var catQuery = "SELECT * FROM crm.Type_ProductCategory";
                    var listProductCategory = connection.Query<Type_ProductCategory>(catQuery).ToList();

                    foreach (var productgroup in pr.properties.Groups)
                    {
                        foreach (var product in productgroup.Products)
                        {
                            var hfcproduct = listhfcproduct
                                .Where(x => x.PICProductID == Convert.ToInt32(product.Product) && x.VendorID == VendorID && x.ProductGroup == productgroup.GGroup).FirstOrDefault();

                            // Check if a product is Inactive check if it is made active again
                            //If it is activated again Create it as a new product line
                            if (hfcproduct != null && hfcproduct.Active!=null && !hfcproduct.Active.Value)
                            {
                                var prod = PICManager.GetProductInfo(product.Product.ToString());
                                var pobj = JsonConvert.DeserializeObject<sProduct>(prod);


                                if (pobj.discontinuedDate != "" && pobj.discontinuedDate != "0000-00-00" && Convert.ToDateTime(pobj.DiscontinuedDate) != null && Convert.ToDateTime(pobj.DiscontinuedDate) < DateTime.Now)
                                {

                                }
                                else
                                {
                                    hfcproduct = null;
                                }
                            }
                            if (hfcproduct != null)
                            {
                                bool cchange = false;

                                if (hfcproduct.ProductName != product.Description)
                                {
                                    hfcproduct.ProductName = product.Description;
                                    hfcproduct.Description = product.Description;
                                    cchange = true;
                                }
                                if (hfcproduct.ProductGroup != productgroup.GGroup)
                                {
                                    hfcproduct.ProductGroup = productgroup.GGroup;
                                    cchange = true;
                                }
                                if (hfcproduct.ProductGroupDesc != productgroup.Description)
                                {
                                    hfcproduct.ProductGroupDesc = productgroup.Description;
                                    cchange = true;
                                }
                                var category = listProductCategory.Where(x => x.ProductCategory.Trim().ToUpper() == productgroup.Description.ToUpper().Trim() && x.IsActive == true && x.IsDeleted == false).FirstOrDefault();

                                if(category!=null && category.ProductCategoryId != 0)
                                {
                                    hfcproduct.ProductCategoryId = category.ProductCategoryId;
                                }
                                else
                                {
                                    var id = InsertProductCategory(productgroup.Description.Trim());
                                    if (id != 0)
                                    {
                                        hfcproduct.ProductCategoryId = id;
                                    }

                                }

                                if (cchange)
                                {
                                    string qarchive = @"insert into [CRM].[HFCProducts_Archive] ([ProductKey],[ProductID],[ProductName],[PICProductID],[VendorID],[VendorName]
                                    ,[VendorProductSKU],[Description],[ProductCategory],[ProductCollection]
                                    ,[ProductModelID],[CreatedOn],[CreatedBy],[LastUpdatedOn],[LastUpdatedBy]
                                    ,[ProductGroup],[ProductGroupDesc],[Active],[DiscontinuedDate]
                                    ,[PhasedoutDate],[TS_product])
                                    SELECT
                                     [ProductKey],[ProductID],[ProductName],[PICProductID],[VendorID],[VendorName]
                                    ,[VendorProductSKU],[Description],[ProductCategory],[ProductCollection]
                                    ,[ProductModelID],[CreatedOn],[CreatedBy],[LastUpdatedOn],[LastUpdatedBy]
                                    ,[ProductGroup],[ProductGroupDesc],[Active],[DiscontinuedDate]
                                    ,[PhasedoutDate],[TS_product]
                                      FROM [CRM].[HFCProducts] where ProductKey=@ProductKey";
                                    connection.Execute(qarchive, new { ProductKey = hfcproduct.ProductKey });
                                    connection.Update(hfcproduct);
                                }
                            }
                            else
                            {
                                HFCProducts hp = new HFCProducts();
                                hp.ProductID = ExecuteIEnumerableObject<int>(ContextFactory.CrmConnectionString,
                                    "exec [CRM].[spProductNumber_New]").FirstOrDefault();
                                hp.ProductName = product.Description;
                                hp.PICProductID = Convert.ToInt32(product.Product);
                                hp.VendorID = VendorID;
                                hp.Description = product.Description;
                                hp.ProductGroup = productgroup.GGroup;
                                hp.ProductGroupDesc = productgroup.Description;
                                hp.Active = true;
                                var category = listProductCategory.Where(x => x.ProductCategory.Trim().ToUpper() == productgroup.Description.ToUpper().Trim() && x.IsActive == true && x.IsDeleted == false).FirstOrDefault();

                                if (category != null && category.ProductCategoryId != 0)
                                {
                                    hp.ProductCategoryId = category.ProductCategoryId;
                                }
                                else
                                {
                                    var id = InsertProductCategory(productgroup.Description.Trim());
                                    if (id != 0)
                                    {
                                        hp.ProductCategoryId = id;
                                    }

                                }
                                Insert<HFCProducts>(ContextFactory.CrmConnectionString, hp);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    return false;
                }
            }

            return true;
        }

        public bool UpdateProducts(HFCProducts hp, sProduct p)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                try
                {
                    if (p.phaseOutDate != "" && p.phaseOutDate != "0000-00-00")
                    {
                        hp.PhasedoutDate = Convert.ToDateTime(p.phaseOutDate);
                    }
                    else
                    {
                        hp.PhasedoutDate = null;
                    }

                    if (p.discontinuedDate != "" && p.discontinuedDate != "0000-00-00")
                    {
                        hp.DiscontinuedDate = Convert.ToDateTime(p.discontinuedDate);
                    }
                    else
                    {
                        hp.DiscontinuedDate = null;
                    }
                    if (hp.DiscontinuedDate != null && hp.DiscontinuedDate < DateTime.Now)
                    {
                        hp.Active = false;
                    }
                    else
                        hp.Active = true;
                    if (p.TS_product != "" && p.TS_product != "0000-00-00")
                    {
                        hp.TS_product = Convert.ToDateTime(p.TS_product);
                    }
                    else
                    {
                        hp.TS_product = null;
                    }
                    hp.ProductName = p.description;
                    hp.Description = p.description;

                    //Configurator Prompts
                    hp.WidthPrompt = p.widthPrompt;
                    hp.HeightPrompt = p.heightPrompt;
                    hp.RoomPrompt = p.roomPrompt;
                    hp.MountPrompt = p.mountPrompt;
                    hp.FabricPrompt = p.fabricPrompt;
                    hp.ColorPrompt = p.colorPrompt;

                    // for now all the products come from PIC are Budget blinds
                    hp.BrandId = 1;
                    hp.LastUpdatedOn = DateTime.UtcNow;
                    connection.Update(hp);
                    InsertOrUpdateProductModels(hp.PICProductID.Value);
                    //Update<HFCProducts>(ContextFactory.CrmConnectionString, hp);
                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    return false;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool InsertOrUpdateProductModels(int ProductId)
        {

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var temp = DateTime.Now;
                int adminusr = -1;


                try
                {
                    var query = @"SELECT * FROM CRM.HFCProducts h WHERE h.PICProductID=@ProductId;";
                    var modelquery = @"SELECT * FROM crm.HFCModels h WHERE h.model=@model AND h.PICProductId=@PICProductId";
                    var product = connection.Query<HFCProducts>(query, new { ProductId = ProductId }).FirstOrDefault();

                    var picMgr = new PICManager();

                    var mod = picMgr.PICModelsByProduct(ProductId);
                    var mObj = JsonConvert.DeserializeObject<PICModelJson>(mod);  // Convert Json to Object

                    if (mObj.valid)
                    {
                        if (mObj.properties.Count > 0)
                        {
                            foreach(var item in mObj.properties)
                            {
                                var changed = false;

                                var model = connection.Query<HFCModels>(modelquery, new { model = item.Model, @PICProductId= ProductId }).FirstOrDefault();
                                //var modJson = JsonConvert.SerializeObject(model);
                                var extmodel = model;
                                //HFCModels model = new HFCModels();


                                if (model == null || (model != null&& model.ModelKey==0))
                                {
                                    model = new HFCModels();
                                }
                                if(product!=null && product.ProductKey != 0)
                                {

                                    model.VendorId = product.VendorID;
                                }

                                model.ProductId = product.ProductKey; ;
                                model.PICProductId = ProductId;
                                model.Model = item.Model;
                                model.Description = item.Description;

                                if ((item.PhaseOutDate != "0000-00-00"))
                                {
                                    model.PhasedoutDate = Convert.ToDateTime(item.DiscontinuedDate);
                                }
                                if ((item.DiscontinuedDate != "0000-00-00"))
                                {
                                    model.DiscontinuedDate = Convert.ToDateTime(item.DiscontinuedDate);
                                }


                                // Set Flags
                                var activeflg = false;
                                model.WarningMessage = "";

                                ////Set if Model is active or not based on the discontinued date. if there is a discountinue date show warning message on screen for FE
                                if (item.DiscontinuedDate == "0000-00-00" || item.DiscontinuedDate == "9999-12-31")
                                {
                                    activeflg = true;
                                }
                                else if ((item.DiscontinuedDate != "0000-00-00" && Convert.ToDateTime(item.DiscontinuedDate) > DateTime.Now))
                                {
                                    activeflg = true;
                                    temp = Convert.ToDateTime(item.DiscontinuedDate);
                                    model.WarningMessage = "Model will be Discontinued from " + temp.GlobalDateFormat();
                                }
                                else if ((item.DiscontinuedDate != "0000-00-00" && Convert.ToDateTime(item.DiscontinuedDate) < DateTime.Now))
                                {
                                    activeflg = false;
                                    temp = Convert.ToDateTime(item.DiscontinuedDate);
                                    model.WarningMessage = "Model is Discontinued on " + temp.GlobalDateFormat();
                                }

                                //Change the flag only when active flag is not set by user
                                if (!model.IsManualFLag)
                                {
                                    model.Active = activeflg;
                                }
                                if (item.PhaseOutDate != "0000-00-00" && item.PhaseOutDate != "9999-12-31")
                                {
                                    temp = Convert.ToDateTime(item.PhaseOutDate);
                                    if (string.IsNullOrEmpty(model.WarningMessage))
                                    {
                                        model.WarningMessage =  "Model is Phasing out from " + temp.GlobalDateFormat();
                                    }else
                                    {
                                        model.WarningMessage = model.WarningMessage + ", " +"Model is Phasing out from " + temp.GlobalDateFormat();
                                    }
                                }
                                else
                                {
                                    model.WarningMessage = "";
                                }



                                if (extmodel == null)
                                {
                                    changed = true;
                                }
                                else if (JsonConvert.SerializeObject(model).Trim() != JsonConvert.SerializeObject(extmodel).Trim())
                                {
                                    changed = true;
                                }

                                if (model!=null&& model.ModelKey == 0)
                                {
                                    model.USActive = true;
                                    model.CAActive = true;

                                    model.CreatedBy = adminusr;
                                    model.CreatedOn = DateTime.UtcNow;
                                    model.LastUpdatedBy = adminusr;
                                    model.LastUpdatedOn = DateTime.UtcNow;
                                    model.ModelKey = connection.Insert<int, HFCModels>(model);
                                }
                                else
                                {
                                    model.LastUpdatedBy = adminusr;
                                    model.LastUpdatedOn = DateTime.UtcNow;
                                    connection.Update(model);

                                }
                                if (changed)
                                {
                                    var arch = new HFCModelsArchive();
                                    arch.ModelKey = model.ModelKey;
                                    arch.OldModelJson = JsonConvert.SerializeObject(extmodel).Trim();
                                    arch.NewModelJson = JsonConvert.SerializeObject(model).Trim();
                                    arch.CreatedBy = adminusr;
                                    arch.CreatedOn = DateTime.UtcNow;
                                    connection.Insert<HFCModelsArchive>(arch);
                                }
                            }
                        }
                    }


                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    return false;
                }

            }
            return true;
        }
        /// <summary>
        /// Get List of Core Product
        /// </summary>
        public List<HFCProducts> GetListCoreProduct()
        {
            string productQ = @"select hp.* from [CRM].[HFCProducts]  hp
                                join [Acct].[HFCVendors] hv on hp.VendorID=hv.VendorId
                                where VendorType=1 and PICVendorId is not NULL AND ISNULL(hp.Active,1) =1 AND ISNULL(hv.IsActive,1)=1;";
            return ExecuteIEnumerableObject<HFCProducts>(ContextFactory.CrmConnectionString, productQ).ToList();
        }
    }
}