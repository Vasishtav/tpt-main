﻿using HFC.CRM.Core.Logs;
using HFC.CRM.Core.Membership;
using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using HFC.CRM.Data.Context;
using System.Data.SqlClient;
using Dapper;
using HFC.CRM.Core.Common;
using System.Net.Mail;
using HFC.CRM.DTO;

namespace HFC.CRM.Managers
{
    public class PaymentManager : DataManager<Payment>
    {
        public PaymentManager(User user, Franchise fran) : this(user, user, fran)
        {
            this.User = user;
            this.Franchise = fran;
        }

        EmailManager emailMgr = new EmailManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
        public PaymentManager(User authorizingUser, User user, Franchise franchise)
        {
            this.AuthorizingUser = authorizingUser;
            this.User = user;
            this.Franchise = franchise;
        }
       
        public override ICollection<Payment> Get(List<int> idList)
        {
            throw new NotImplementedException();
        }

        public override Payment Get(int id)
        {
            //if (!BasePermission.CanRead)
            //    throw new UnauthorizedAccessException(Unauthorized);

            using (var db = ContextFactory.Create())
            {
                return _Get(id, db);

            }
        }

        public List<Payment> GetPayments(int invoiceId)
        {
            //if (!BasePermission.CanRead)
            //    throw new UnauthorizedAccessException(Unauthorized);

            return (from p in CRMDBContext.Payments.AsNoTracking()
                    where p.InvoiceId == invoiceId && p.Invoice.Job.Lead.FranchiseId == Franchise.FranchiseId
                    select p).ToList();
        }
        public bool GetInvoicePrintOptions(int FranchiseId)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {

                connection.Open();
                var query = @"select Top 1* from CRM.Franchise   where FranchiseId=@FranchiseId order by FranchiseId desc ";
                var val = connection.Query<Franchise>(query, new { FranchiseId = FranchiseId }).ToList();
                connection.Close();
                if (val != null)
                {
                    if (val.Count > 0)
                    {
                        return val.FirstOrDefault().FEPrintOption;
                    }
                }

            }

            return false;
        }

        public override string Add(Payment data)
        {
            Invoice invoice = null;
            return Add(data, out invoice);
        }

        /// <summary>
        /// Adds a new payment and also calculate necessary invoice status and paid in full date
        /// </summary>
        /// <param name="data">Payment to add</param>
        /// <param name="invoice">Dynamic_Invoice information related to this payment if alsoUpdateInvoice is true</param>
        /// <param name="alsoUpdateInvoice">If true, the invoice will also calculate the paid in full amount and date</param>
        /// <returns></returns>
        public string Add(Payment data, out Invoice invoice, bool alsoUpdateInvoice = true)
        {
            invoice = null;
            //if (!BasePermission.CanCreate)
            //    return Unauthorized;

            try
            {
                using (var db = ContextFactory.Create())
                {

                    data.CreatedOn = this.GetNow();

                    data.PaymentGuid = Guid.NewGuid();

                    db.Payments.Add(data);

                    db.SaveChanges();

                    if (alsoUpdateInvoice)
                    {
                        invoice = _UpdateInvoice(data.InvoiceId, db);
                    }

                    return Success;
                }
            }
            catch (Exception ex)
            {
                return EventLogger.LogEvent(ex);
            }
        }

        public override string Update(Payment data)
        {
            Invoice invoice = null;
            return Update(data, out invoice);
        }

        public string Update(Payment data, out Invoice invoice, bool alsoUpdateInvoice = true)
        {
            invoice = null;
            //if (!BasePermission.CanUpdate)
            //    return Unauthorized;

            using (var db = ContextFactory.Create())
            {
                var original = _Get(data.PaymentId, db);

                if (original == null)
                    return "Payment not found";

                try
                {
                    original.Amount = data.Amount;
                    original.PaymentMethod = data.PaymentMethod;
                    original.PaymentDate = data.PaymentDate;
                    original.ReferenceNum = data.ReferenceNum;

                    db.SaveChanges();

                    if (alsoUpdateInvoice)
                    {
                        invoice = _UpdateInvoice(data.InvoiceId, db);
                    }

                    return Success;
                }
                catch (Exception ex)
                {
                    return EventLogger.LogEvent(ex);
                }
            }
        }

        public override string Delete(int id)
        {
            Invoice invoice = null;
            return Delete(id, out invoice);
        }

        public string Delete(int id, out Invoice invoice, bool alsoUpdateInvoice = true)
        {
            invoice = null;
            //if (!BasePermission.CanDelete)
            //    return Unauthorized;

            using (var db = ContextFactory.Create())
            {
                var original = _Get(id, db);

                if (original == null)
                    return "Payment not found";

                try
                {

                    db.Payments.Remove(original);

                    db.SaveChanges();

                    if (alsoUpdateInvoice)
                    {
                        invoice = _UpdateInvoice(original.InvoiceId, db);
                    }

                    return Success;
                }

                catch (Exception ex)
                {
                    return EventLogger.LogEvent(ex);
                }
            }
        }

        private Payment _Get(int paymentId, CRMContextEx db)
        {
            return (from p in db.Payments
                    where p.PaymentId == paymentId && p.Invoice.Job.Lead.FranchiseId == Franchise.FranchiseId
                    select p).SingleOrDefault();
        }

        private Invoice _GetInvoice(int invoiceId, CRMContextEx db)
        {
            return db.Invoices.Where(i => i.InvoiceId == invoiceId && i.Job.Lead.FranchiseId == Franchise.FranchiseId)
                    .Include(i => i.Job)
                    .Include(i => i.Payments)
                    .Include(i => i.JobQuote)
                    .SingleOrDefault();
        }

        public Invoice CalculatePayments(int invoiceId, CRMContextEx db)
        {
            return _UpdateInvoice(invoiceId, db);
        }

        private Invoice _UpdateInvoice(int invoiceId, CRMContextEx db)
        {
            var invoice = _GetInvoice(invoiceId, db);
            var payTotal = invoice.Payments.Sum(s => s.Amount);
            if (payTotal >= invoice.JobQuote.NetTotal)
            {
                invoice.StatusEnum = InvoiceStatusEnum.Paid;
                invoice.PaidInFullOn = DateTimeOffset.Now;
            }
            else if (payTotal > 0 && payTotal < invoice.JobQuote.NetTotal)
            {
                invoice.StatusEnum = InvoiceStatusEnum.Partial;
                invoice.PaidInFullOn = null;
            }
            else
            {
                invoice.StatusEnum = InvoiceStatusEnum.Open;
                invoice.PaidInFullOn = null;
            }

            invoice.Job.Balance = invoice.JobQuote.NetTotal - payTotal;
            invoice.Job.LastUpdated = invoice.LastUpdated = DateTimeOffset.Now;
            invoice.Job.LastUpdatedByPersonId = invoice.LastUpdatedPersonId = User.PersonId;

            db.SaveChanges();

            return invoice;
        }

        public string SendOrderSummaryEmail(int OrderId, EmailType emailType, string streamid = "")
        {

            List<string> toAddresses = new List<string>();
            List<string> ccAddresses = new List<string>();
            List<string> bccAddresses = new List<string>();
            CustomerTP tocustomerAddress = new CustomerTP();

            var orderInfo = emailMgr.GetOrderInfo(OrderId);

            var EmailSettings = emailMgr.GetConfiguredEmails().Where(x => x.EmailType == (int)emailType).FirstOrDefault();
            if (EmailSettings != null)
            {
                if (OrderId > 0)
                {
                    if (EmailSettings.CopyAssignedSales == true)
                    {
                        var salesemail = emailMgr.GetSalesPersonEmail(0, OrderId);
                        if(salesemail !=null && !string.IsNullOrEmpty(salesemail.Email) && salesemail.IsDisabled != true)
                        //if (salesemail.Email != null && salesemail.Email != "" && salesemail.IsDisabled != true)
                            ccAddresses.Add(salesemail.Email);
                    }
                    if (EmailSettings.CopyAssignedInstaller == true)
                    {
                        var installeremail = emailMgr.GetInstallerEmail(0, OrderId);
                        if(installeremail != null && !string.IsNullOrEmpty(installeremail.Email) && installeremail.IsDisabled != true)
                        //if (installeremail.Email != null && installeremail.Email != "" && installeremail.IsDisabled != true)
                            ccAddresses.Add(installeremail.Email);
                    }
                }
            }

            TerritoryDisplay data = emailMgr.GetTerritoryDisplayByOpp((int)orderInfo.OpportunityId);
            //BUILD - From email address
            string fromEmailAddress = "";
            if (data != null && data.Id == null)
                fromEmailAddress = emailMgr.GetFranchiseEmail(Franchise.FranchiseId, emailType);
            else
            {
                if (Franchise.AdminEmail != "")
                    fromEmailAddress = Franchise.AdminEmail;
                else if (Franchise.OwnerEmail != "")
                    fromEmailAddress = Franchise.OwnerEmail;
            }
          
            tocustomerAddress = emailMgr.GetOrderCustomerEmail(OrderId);

            if (tocustomerAddress != null)
            {
                if (tocustomerAddress.PrimaryEmail != null && tocustomerAddress.PrimaryEmail.Trim() != "")
                {
                    toAddresses.Add(tocustomerAddress.PrimaryEmail.Trim());
                }
                if (tocustomerAddress.SecondaryEmail != null && tocustomerAddress.SecondaryEmail.Trim() != "")
                {
                    toAddresses.Add(tocustomerAddress.SecondaryEmail.Trim());
                }
            }

            //If no from or to emails return
            if (toAddresses == null || toAddresses.Count == 0 || fromEmailAddress == "")
            {
                return "Success";
                //return "No valid from or to email";
            }

            //Preparing email Body with formating
            string TemplateSubject = "", bodyHtml = "";
            int FranchiseId = 0;
            int BrandId = (int)SessionManager.BrandId;

            //Take Body Html from EmailTemplate Table
            var emailtemplate = emailMgr.GetHtmlBody(emailType, BrandId);
            TemplateSubject = emailtemplate.TemplateSubject;
            bodyHtml = emailtemplate.TemplateLayout;
            short TemplateEmailId = (short)emailtemplate.EmailTemplateId;

            if (emailType == EmailType.OrderSummary) //== 4)
            {
                var mailMsg = emailMgr.BuildMailMessageOrderSummary(
                    0, orderInfo.AccountId, orderInfo.OpportunityId, OrderId, bodyHtml, TemplateSubject
                    , TemplateEmailId, BrandId, FranchiseId, fromEmailAddress
                    , toAddresses, ccAddresses, bccAddresses, streamid);
            }

            return "Success";

        }

    }
}
