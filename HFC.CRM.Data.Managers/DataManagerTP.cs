﻿using HFC.CRM.Data;
using HFC.CRM.Core.Common;
using System;
using Dapper;
using HFC.CRM.Data.Context;
using System.Data.SqlClient;
using HFC.CRM.Core.Logs;

namespace HFC.CRM.Managers
{

    /// <summary>
    /// Commom base class for managing database for both from Controll panel and FE
    /// <para>User: is the logged user should be set while initiating any derived class</para>
    /// </summary>
    public abstract class DataManagerBase : ContantStrings
    {
        public User User { get; protected set; }

        public DataManagerBase(User user)
        {
            User = user;
        }
    }

    /// <summary>
    /// Common base class which provides boiler plates codes for basic crud operation.
    /// <para>It does not depends on Franchise, as the common database operation 
    /// requires both from Controll panel as well as in the FE's context</para>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class DataManagerBase<T> : DataManagerBase //ContantStrings
    {

        protected DataManagerBase(User user) : base(user)
        {
            //this.User = user;
        }

        protected virtual T AfterAdd(int newId, T modal, SqlConnection connection, SqlTransaction transaction = null)
        {
            return modal;
        }

        public virtual string Add(T modal)
        {
            using (SqlConnection connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var transaction = connection.BeginTransaction();

                try
                {
                    var newid = (int)connection.Insert(modal, transaction);

                    var model = AfterAdd(newid, modal, connection, transaction);

                    transaction.Commit();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    EventLogger.LogEvent(ex);
                    throw;
                }


            }

            return Success;
        }

        public virtual string Add(T modal, SqlConnection connection, SqlTransaction transaction)
        {
            

                try
                {
                    var newid = (int)connection.Insert(modal, transaction);

                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }

            return Success;
        }

        public virtual T Get(int id)
        {
            T result;
            using (SqlConnection connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {

                try
                {
                    result = connection.Get<T>(id);

                    connection.Close();
                }
                catch (Exception ex)
                {
                    EventLogger.LogEvent(ex);
                    throw;
                }
            }

            return result;
        }
    }

    /// <summary>
    /// Common Database manager not sepecific to one entity witing the FE context. 
    /// For example Print or search which is not rely on any specific entity.
    /// <para>Current Franchise need to provided while initiating.</para>
    /// </summary>
    public abstract class DataManagerFE : DataManagerBase
    {
        public Franchise Franchise { get; protected set; }
        public DataManagerFE(User user, Franchise franchise) : base(user)
        {
            Franchise = franchise;
        }
    }

    /// <summary>
    /// Common base class which provides boiler plates codes for basic crud operation
    /// in FE's context
    /// <para>Current Franchise need to provided to intiate this</para>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class DataManagerFE<T> : DataManagerBase<T>
    {
        public Franchise Franchise { get; protected set; }

        protected DataManagerFE(User user, Franchise franchise) : base(user)
        {
            this.Franchise = franchise;
        }
    }
}
