﻿using HFC.CRM.Data;
using HFC.CRM.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;

/// <summary>
/// The Managers namespace.
/// </summary>
namespace HFC.CRM.Managers
{
    using HFC.CRM.Data.Context;

    public class IDataManager : ContantStrings, IDisposable
    {

        /// <summary>
        /// This is the user that leads manager uses to check all permission
        /// </summary>
        /// <value>The user.</value>
        public User User { get; protected set; }
        /// <summary>
        /// Gets or sets the authorizing user.
        /// </summary>
        /// <value>The authorizing user.</value>
        public User AuthorizingUser { get; protected set; }
        /// <summary>
        /// Gets or sets the franchise.
        /// </summary>
        /// <value>The franchise.</value>
        public Franchise Franchise { get; protected set; }

        /// <summary>
        /// DB db for this manager
        /// </summary>
        protected CRMContext CRMDBContext = ContextFactory.Current.Context;

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            //CRMDBContext.Dispose();
        }
        public void SaveChanges(DbContext db)
        {
            // THIS IS WORK IN PROGRESS, THE real LOGGER will substitute this in ver near future, this is for debug only.
            try
            {
                db.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                var EntityValidationErrors = e.EntityValidationErrors;
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException e)
            {
                var errData = e.Data;
                var Entity = e.Entries.FirstOrDefault().Entity;
            }
            catch (Exception e)
            {
                var errData = e.InnerException.Data;
            }
        }

        protected DateTimeOffset GetNow()
        {
            var now = DateTime.Now;
            return new DateTimeOffset(now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second, TimeSpan.Zero);
        }
    }

    /// <summary>
    /// Class IDataManager.
    /// </summary>
    public abstract class IDataManager<T> : IDataManager
    {       
        public abstract Permission BasePermission { get; }

        public abstract ICollection<T> Get(List<int> idList);

        public abstract T Get(int id);

        public abstract string Add(T data);

        public abstract string Update(T data);

        public abstract string Delete(int id); 
    }
}
