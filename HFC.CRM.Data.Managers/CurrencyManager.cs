﻿using Dapper;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using HFC.CRM.DTO.Product;
using HFC.CRM.DTO.Vendors;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Managers
{
    public class CurrencyManager : DataManager
    {
        //get all currency value to grid code(View Grid)
        public List<CurrencygridView> GetCurrencydata()
        {
            List<CurrencygridView> result = new List<CurrencygridView>();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var Query = "";

                //Query = @"select * from [Acct].[HFCVendors] where VendorType =1";
                Query = @"select ce1.CountryTo as Currency,CONVERT(date, ce1.StartDate) as StartDate,CONVERT(date, ce1.EndDate) as EndDate,
                          case when ce1.CalculationType=1 then '*'+cast(ce1.Rate as varchar) else '/'+cast(ce1.Rate as varchar) 
                          end as FromRate,case when ce2.CalculationType=1 then '*'+cast(ce2.Rate as varchar) else '/'+cast(ce2.Rate as varchar) 
                          end as ToRate from CRM.CurrencyExchange ce1 join CRM.CurrencyExchange ce2 on ce1.CountryFrom=ce2.CountryTo 
                          and ce1.CountryTo=ce2.CountryFrom and ce1.StartDate=ce2.StartDate 
                          where ce1.CountryFrom='US' order by ce1.StartDate desc";
                result = connection.Query<CurrencygridView>(Query).ToList();

            }
            return result;
        }

        //get value for add grid code
        public List<CurrencyAdd> GetCurrencyCountry()
        {
            List<CurrencyAdd> result = new List<CurrencyAdd>();
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var Query = "";


                //Query = @"select distinct ISOCode2Digits as Country,1 as FromTypeValue from CRM.Type_CountryCodes where CurrencyCode is not null and CurrencyCode!='USD'";
                Query = @"select distinct ISOCode2Digits as Country,
                          (select top 1 CalculationType from CRM.CurrencyExchange where CountryTo=tcc.ISOCode2Digits order by StartDate desc) as FromTypeValue,
                          (select top 1 Rate from CRM.CurrencyExchange where CountryTo=tcc.ISOCode2Digits  order by StartDate desc) as FromRate,
                          (select top 1 CalculationType from CRM.CurrencyExchange where CountryFrom=tcc.ISOCode2Digits  order by StartDate desc) as ToTypeValue, 
                          (select top 1 Rate from CRM.CurrencyExchange where CountryFrom=tcc.ISOCode2Digits  order by StartDate desc) as ToRate 
                          from CRM.Type_CountryCodes tcc 
                          where CurrencyCode is not null and CurrencyCode!= 'USD'";
                result = connection.Query<CurrencyAdd>(Query).ToList();
                foreach (var item in result)
                {
                    if (item.FromTypeValue == 1)
                    {
                        item.FromType = "*";
                    }
                    else
                        item.FromType = "/";
                    if (item.ToTypeValue == 2)
                        item.ToType = "/";
                    else item.ToType = "*";
                }


            }
            return result;
        }

        //Save grid value
        public string Savedata(List<CurrencyAdd> modeldata)
        {
            if (modeldata != null)
            {
                foreach (var model in modeldata)
                {
                    CurrencyExchange Curr_Exec1 = new CurrencyExchange();
                    Curr_Exec1.Year = Convert.ToDateTime(model.DateValue).Year;
                    Curr_Exec1.Period = Convert.ToDateTime(model.DateValue).Month;
                    Curr_Exec1.StartDate = Convert.ToDateTime(model.DateValue);
                    if (model.EndDateValue != "")
                    {
                        Curr_Exec1.EndDate = Convert.ToDateTime(model.EndDateValue);
                    }
                    Curr_Exec1.CountryFrom = "US";
                    Curr_Exec1.CountryTo = model.Country;
                    Curr_Exec1.CalculationType = model.FromTypeValue;
                    Curr_Exec1.Rate = Convert.ToDecimal(model.FromRate);

                    CurrencyExchange Curr_Exec2 = new CurrencyExchange();
                    Curr_Exec2.Year = Convert.ToDateTime(model.DateValue).Year;
                    Curr_Exec2.Period = Convert.ToDateTime(model.DateValue).Month;
                    Curr_Exec2.StartDate = Convert.ToDateTime(model.DateValue);
                    if (model.EndDateValue != "")
                    {
                        Curr_Exec2.EndDate = Convert.ToDateTime(model.EndDateValue);
                    }
                    Curr_Exec2.CountryFrom = model.Country;
                    Curr_Exec2.CountryTo = "US";
                    Curr_Exec2.CalculationType = model.ToTypeValue;
                    Curr_Exec2.Rate = Convert.ToDecimal(model.ToRate);

                    using (SqlConnection connection = new SqlConnection(ContextFactory.CrmConnectionString))
                    {
                        connection.Open();
                        var transaction = connection.BeginTransaction();
                        try
                        {
                            var queryCountryFrom = "select top 1 * from[CRM].[CurrencyExchange] where CountryFrom = @CountryFrom order by StartDate desc";
                            var cxangeCountryFrom = connection.Query<CurrencyExchange>(queryCountryFrom, new { CountryFrom = model.Country }, transaction).FirstOrDefault();

                            // Update from end date for the old record
                            if (cxangeCountryFrom != null)
                            {
                                if (cxangeCountryFrom.StartDate < Convert.ToDateTime(model.DateValue))
                                    cxangeCountryFrom.EndDate = Convert.ToDateTime(model.DateValue).AddDays(-1);
                                else
                                    cxangeCountryFrom.EndDate = cxangeCountryFrom.StartDate;

                                connection.Update<CurrencyExchange>(cxangeCountryFrom, transaction);
                            }                            

                            var queryCountryTo = "select top 1 * from[CRM].[CurrencyExchange] where CountryTo = @CountryTo order by StartDate desc";
                            var cxangeCountryTo = connection.Query<CurrencyExchange>(queryCountryTo, new { CountryTo = model.Country }, transaction).FirstOrDefault();

                            // Update To end date for the old record
                            if (cxangeCountryTo != null)
                            {
                                if (cxangeCountryTo.StartDate < Convert.ToDateTime(model.DateValue))
                                    cxangeCountryTo.EndDate = Convert.ToDateTime(model.DateValue).AddDays(-1);
                                else
                                    cxangeCountryTo.EndDate = cxangeCountryTo.StartDate;

                                connection.Update<CurrencyExchange>(cxangeCountryTo, transaction);
                            }
                           
                            connection.Insert<CurrencyExchange>(Curr_Exec1, transaction);
                            connection.Insert<CurrencyExchange>(Curr_Exec2, transaction);
                            transaction.Commit();
                            connection.Close();

                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            EventLogger.LogEvent(ex);
                        }
                    }

                }
            }

            return "Success";
        }
    }
}