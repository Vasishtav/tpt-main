﻿using HFC.CRM.Data;
using System.Collections.Generic;
using System.Linq;
using HFC.CRM.Data.Context;

namespace HFC.CRM.Managers 
{
    using Core.Membership;

    public class ManufacturerManager : DataManager
    {
        public ManufacturerManager(Franchise franchise, User user)
        {
            Franchise = franchise;
            AuthorizingUser = user;
        }

        public List<Manufacturer> GetManufacturers()
        {
            var manufacturers = this.CRMDBContext.Manufacturers.AsNoTracking().OrderBy(v => v.Name).ToList();
            var vendors = this.CRMDBContext.Vendors.AsNoTracking().Where(w => w.IsManual.HasValue && w.IsManual == true && w.IsActive == false).Select(s => s.VendorGuid).ToList();
            manufacturers.RemoveAll(v=> vendors.Contains(v.ManufacturerGuid));
            return manufacturers;

        }

        public ManufacturerContact GetManufacturerContact(int manufacturerId)
        {
            var contact = this.CRMDBContext.ManufacturerContacts.AsNoTracking().FirstOrDefault(c => c.ManufacturerId == manufacturerId && c.FranchiseId == this.Franchise.FranchiseId);
            return contact ?? new ManufacturerContact() { IsActive = true };
        }

        public string SetManufacturerContact(ManufacturerContact contact)
        {
            using (var db = ContextFactory.Create())
            {
                var currentContact = db.ManufacturerContacts.AsNoTracking()
                    .FirstOrDefault(
                        c => c.ManufacturerId == contact.ManufacturerId && c.FranchiseId == this.Franchise.FranchiseId)
                                     ??
                                     new ManufacturerContact()
                                     {
                                         ManufacturerId = contact.ManufacturerId,
                                         FranchiseId = this.Franchise.FranchiseId
                                     };

                currentContact.Contact = contact.Contact;
                currentContact.ContactEmail = contact.ContactEmail;
                currentContact.ContactNumber = contact.ContactNumber;
                currentContact.AccountNumber = contact.AccountNumber;
                currentContact.IsActive = contact.IsActive;

                if (currentContact.ManufacturerContactId == 0)
                {
                    db.ManufacturerContacts.Add(currentContact);
                }

                db.SaveChanges();
            }

            return Success;
        }
    }
}
