﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Core.Membership;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using HFC.CRM.Data.Geolocator;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Web;
using System.IO;
using HFC.CRM.DTO;
using System.Globalization;

namespace HFC.CRM.Managers
{
    public class NoteManager : DataManagerFE<Note> // DataManager<Note>
    {
        public NoteManager(User user, Franchise franchise) : base(user, franchise)
        {

        }

        private string GetBaseQuery()
        {
            //var query = @"select n.*, ft.name as FileName from crm.note n left join filetabletb ft 
            //    on ft.stream_id = n.AttachmentStreamId";

            var query = @"select stuff((select ',' + cast(nc.typeenum as varchar)
                                from crm.notescategories nc
                                where nc.noteid = n.noteid
                                for xml path('')), 1, 1, '' ) Categories
                            , n.notes, n.leadid, n.quoteid, n.customerid, n.createdon, n.createdby
                            , n.updatedon, n.updatedby, n.accountid
                            , (select top 1 TypeEnum from crm.notescategories where noteid = n.noteid) as TypeEnum
                            , n.Title, n.OpportunityId, n.NoteId, n.AttachmentStreamId, n.IsNote, n.FranchiseId
                            , n.OrderId, n.IsEnabled, n.IsDeleted
                            , ft.name as FileName 
                            , case 
	                            when n.UpdatedBy is not null then  pm.FirstName + ' ' + pm.LastName
	                            else p.FirstName + ' ' + p.LastName 
	                            end DisplayUsername
                            , case 
	                            when n.UpdatedOn is not null then n.UpdatedOn 
	                            else n.CreatedOn
	                            end DisplayNotesDate
                    from crm.note n 
                    left join filetabletb ft 
                    on ft.stream_id = n.AttachmentStreamId
                    join crm.Person p on p.PersonId = n.CreatedBy
	                left join crm.Person pm  on pm.PersonId = n.UpdatedBy";
            return query;
        }

        //global search
        public List<Note> GetGlobalNotes(Note note)
        {
            List<Note> notes = new List<Note>();

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = GetBaseQuery();
                query += " where n.NoteId = @NoteId";
                //, new { NoteId = note.NoteId }
                notes = connection.Query<Note>(query, new { NoteId = note.NoteId }).ToList();
                connection.Close();
                if (notes != null && notes.Count() > 0)
                {
                    SetCreatorFullName(ref notes);
                }

            }

            notes = TimeZoneManager.ConvertToLocalList<Note>(notes);
            notes = UpdateDisplayNotesDate(notes);
            notes = notes.OrderByDescending(p => p.CreatedOn).ToList();
            return notes;
        }
        public List<Note> GetLeadNotes(Note note, bool showDeleted)
        {
            List<Note> notes = new List<Note>();

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = GetBaseQuery();
                query += " where n.LeadId = @leadId";
                if ( !showDeleted) query += " and n.IsDeleted != @isdeleted";
                notes = connection.Query<Note>(query, new { leadId = note.LeadId, isdeleted = 1 }).ToList();
                connection.Close();
                if (notes != null && notes.Count() > 0)
                {
                    SetCreatorFullName(ref notes);
                }

            }

            notes = TimeZoneManager.ConvertToLocalList<Note>(notes);
            notes = UpdateDisplayNotesDate(notes);
            notes = notes.OrderByDescending(p => p.CreatedOn).ToList();
            return notes;
        }

        private List<Note> UpdateDisplayNotesDate(List<Note> notes)
        {
            foreach (var x in notes)
            {
                x.DisplayNotesDate = TimeZoneManager.ToLocal(x.DisplayNotesDate);
            }
            return notes;
        }

        public List<Note> GetAccountNotes(Note note, bool showDeleted)
        {
            List<Note> notes = new List<Note>();

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = GetBaseQuery();
                query += " where n.AccountId = @AccountId"; //and n.IsDeleted != @isdeleted";
                if (!showDeleted) query += " and n.IsDeleted != @isdeleted";
                notes = connection.Query<Note>(query, new { AccountId = note.AccountId, isdeleted = 1 }).ToList();
                connection.Close();

                if (notes != null && notes.Count() > 0)
                {
                    SetCreatorFullName(ref notes);
                }

            }

            notes = TimeZoneManager.ConvertToLocalList<Note>(notes);
            notes = UpdateDisplayNotesDate(notes);
            notes = notes.OrderByDescending(p => p.CreatedOn).ToList();
            return notes;
        }
        public List<Note> GetOpportunityNotes(Note note, bool showDeleted)
        {
            List<Note> notes = new List<Note>();

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = GetBaseQuery();
                query += " where n.OpportunityId = @OpportunityId"; // and n.IsDeleted != @isdeleted";
                if (!showDeleted) query += " and n.IsDeleted != @isdeleted";
                notes = connection.Query<Note>(query, new { OpportunityId = note.OpportunityId, isdeleted = 1 }).ToList();

                connection.Close();
                if (notes != null && notes.Count() > 0)
                {
                    SetCreatorFullName(ref notes);
                }

            }

            notes = TimeZoneManager.ConvertToLocalList<Note>(notes);
            notes = UpdateDisplayNotesDate(notes);
            notes = notes.OrderByDescending(p => p.CreatedOn).ToList();
            return notes;
        }

        public List<Note> GetOrderNotes(Note note, bool showDeleted)
        {
            List<Note> notes = new List<Note>();

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var query = GetBaseQuery();
                query += " where n.OrderId = @OrderId";
                if (!showDeleted) query += " and n.IsDeleted != @isdeleted";
                notes = connection.Query<Note>(query, new { OrderId = note.OrderId, isdeleted = 1 }).ToList();

                connection.Close();
                if (notes != null && notes.Count() > 0)
                {
                    SetCreatorFullName(ref notes);
                }

            }

            notes = TimeZoneManager.ConvertToLocalList<Note>(notes);
            notes = UpdateDisplayNotesDate(notes);
            notes = notes.OrderByDescending(p => p.CreatedOn).ToList();
            return notes;
        }

        public List<Note> GetFranchiseDocuments(Note note)
         {
            List<Note> notes = new List<Note>();

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                var qry = @"select stuff((select ',' + cast(nc.typeenum as varchar)
                                from crm.notescategories nc
                                where nc.noteid = n.noteid
                                for xml path('')), 1, 1, '' ) Categories
                            , n.notes, n.leadid, n.quoteid, n.customerid,CONVERT(date, n.createdon) createdon, n.createdby
                            , n.updatedon, n.updatedby, n.accountid
                            , (select top 1 TypeEnum from crm.notescategories where noteid = n.noteid) as TypeEnum
                            , n.Title, n.OpportunityId, n.NoteId, n.AttachmentStreamId, n.IsNote, n.FranchiseId
                            , n.OrderId, n.IsEnabled
                            , ft.name as FileName, op.OpportunityName
                            , c.FirstName + ' ' +  c.LastName as AccountName from crm.note n  
                            left join filetabletb ft on ft.stream_id = n.AttachmentStreamId
                            left join[CRM].[Opportunities]
                                    op on n.OpportunityId = op.[OpportunityId]
                            left join[CRM].[Accounts]
                                    ac on n.AccountId = ac.AccountId
                            left JOIN[CRM].[Customer]
                                    c on c.CustomerId = ac.[PersonId]
                            where isnull(n.isdeleted, 0) = 0
                            and (n.LeadId in (select LeadId from CRM.Leads where FranchiseId = @FranchiseId and IsNote = 0)
                            or n.AccountId in (select AccountId from CRM.Accounts where FranchiseId = @FranchiseId and IsNote = 0)
                            or n.OpportunityId in (select OpportunityId from CRM.Opportunities where FranchiseId = @FranchiseId and IsNote = 0)
                            or n.FranchiseId = @FranchiseId and IsNote = 0) order by COALESCE(n.UpdatedOn,n.CreatedOn) desc";

               
                notes = connection.Query<Note>(qry, new { FranchiseId = note.FranchiseId }).ToList();
                connection.Close();
                if (notes != null && notes.Count() > 0)
                {
                    SetCreatorFullName(ref notes);
                }
            }

            notes = TimeZoneManager.ConvertToLocalList<Note>(notes);
            notes = UpdateDisplayNotesDate(notes);
            return notes;
        }
        private void SetCreatorFullName(ref List<Note> notes)
        {

            // TODO : why do we need to sort by desc on created date???
            // notes = notes.OrderByDescending(o => o.CreatedOn).ToList();
            foreach (var noteresult in notes)
            {
                var person = CacheManager.UserCollection.FirstOrDefault(f => f.PersonId == noteresult.CreatedBy);
                if (person != null)
                {
                    //noteresult.CreatorFullName = person.Person.FullName;
                    //noteresult.AvatarSrc = person.AvatarSrc ?? AppConfigManager.DefaultAvatarSrc;
                }
                else
                {
                    //noteresult.CreatorFullName = "Unknown";
                    //noteresult.AvatarSrc = AppConfigManager.DefaultAvatarSrc;
                }

                if (!string.IsNullOrEmpty(noteresult.FileName))
                {
                    var result = noteresult.FileName.Split('_');
                    string filename = string.Join("_", result.Skip(2).ToArray());
                    if (filename == "")
                        filename = string.Join("_", result.ToArray());
                    noteresult.FileName = filename;
                }


            }
        }

        public Note Get(int id)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();

                var query = @"select stuff((select ',' + cast(nc.typeenum as varchar)
                                from crm.notescategories nc
                                where nc.noteid = n.noteid
                                for xml path('')), 1, 1, '' ) Categoreis
                            , n.notes, n.leadid, n.quoteid, n.customerid, n.createdon, n.createdby
                            , n.updatedon, n.updatedby, n.accountid
                            , (select top 1 TypeEnum from crm.notescategories where noteid = n.noteid) as TypeEnum
                            , n.Title, n.OpportunityId, n.NoteId, n.AttachmentStreamId, n.IsNote, n.FranchiseId
                            , n.OrderId, n.IsEnabled
                    from Crm.Note n where n.noteid = @noteid and n.IsDeleted != @isdeleted";
                var note = connection.Query<Note>(query, new { noteid = id, isdeleted = 1 }).FirstOrDefault();
                connection.Close();

                note.CreatedOn = TimeZoneManager.ToLocal(note.CreatedOn);
                note.UpdatedOn = TimeZoneManager.ToLocal(note.UpdatedOn);
                note.DisplayNotesDate = TimeZoneManager.ToLocal(note.DisplayNotesDate);
                return note;
            }
        }
        public string UpdateGlobalDocumentStreamId(int Id, string sysWideTitle, bool? sysWideIsEnabled, int? sysWideCategory, int? sysWideConcept, string streamId, string Name)
        {
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
            sysWideTitle = textInfo.ToTitleCase(sysWideTitle.ToLower());
            try
            {
                using (SqlConnection con = new SqlConnection(ContextFactory.CrmConnectionString))
                {
                    var res = con.Query<GlobalDocuments>("Select * from CRM.GlobalDocuments where Title=@Title and Category=@Category and Concept=@Concept", new { Title = sysWideTitle, Category = sysWideCategory, Concept = sysWideConcept }).ToList();

                    if (res.Count > 0 && Id > 0)
                    {
                        if (Id != res[0].Id)
                            con.Execute("delete from CRM.GlobalDocuments where Id =@Idd", new { Idd = res[0].Id });
                    }
                    if (res.Count > 0 && Id == 0) Id = res[0].Id;

                    if (res.Count > 0 || Id > 0)
                    {

                        if (streamId != "")
                            con.Execute("Update CRM.GlobalDocuments set Title=@Title,IsEnabled=@IsEnabled,Category=@Category,Concept=@Concept, streamId= @streamId,Name=@Name,UpdatedOn = @UpdatedOn where  Id=@Id",
                                new
                                {
                                    Id = Id,
                                    Title = sysWideTitle,
                                    IsEnabled = sysWideIsEnabled,
                                    Category = sysWideCategory,
                                    Concept = sysWideConcept,
                                    streamId = streamId,
                                    Name = Name,
                                    UpdatedOn = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc)
                                });
                        else
                        {
                            con.Execute("Update CRM.GlobalDocuments set Title=@Title,IsEnabled=@IsEnabled, Category=@Category,Concept=@Concept,UpdatedOn=@UpdatedOn where Id=@Id",
                            new
                            {
                                Id = Id,
                                Title = sysWideTitle,
                                IsEnabled = sysWideIsEnabled,
                                Category = sysWideCategory,
                                Concept = sysWideConcept,
                                UpdatedOn = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc)
                            });
                        }
                    }
                    else
                    {
                        con.Execute("insert into CRM.GlobalDocuments values(@Title,@Name,@Category,@Concept,@streamId,@IsEnabled,@CreatedOn,@CreatedOn)",
                            new
                            {
                                Title = sysWideTitle,
                                Name = Name,
                                Category = sysWideCategory,
                                Concept = sysWideConcept,
                                streamId = streamId,
                                IsEnabled = sysWideIsEnabled,
                                CreatedOn = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc)
                            });
                    }

                }
                return Success;
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                return "";
            }
        }

        public override string Add(Note data)
        {
            string Query = "";

            OpportunitiesManager oppmgr = new OpportunitiesManager(User, Franchise);
            OrderManager ordermgr = new OrderManager(User, Franchise);
            if (data!=null)
            {
                data.LeadId = (data.LeadId > 0) ? data.LeadId : null;
                data.AccountId = (data.AccountId > 0) ? data.AccountId : null;
                data.OpportunityId = (data.OpportunityId > 0) ? data.OpportunityId : null;
                data.OrderId = (data.OrderId > 0) ? data.OrderId : null;
                data.IsNote = true;
                data.CreatedOn = DateTime.Now;
                data.CreatedBy = SessionManager.CurrentUser.PersonId;
            }

            if (data.OrderId != null && data.OrderId > 0)
            {
                var orderdata = ordermgr.GetOrder((int)data.OrderId, Franchise.FranchiseId);
                data.OpportunityId = orderdata.OpportunityId;
                data.AccountId = orderdata.AccountId;
            }
            else if (data.OpportunityId != null && data.OpportunityId > 0)
            {
                var oppdata = oppmgr.Get((int)data.OpportunityId);
                data.AccountId = oppdata.AccountId;
            }

            data.NoteId = 0;
            data.CreatedOn = DateTime.UtcNow; //data.CreatedOn,
            data.CreatedBy = this.User.PersonId;
            data.TypeEnum = 0; 

            var result = base.Add(data);
            return result;
        }

        protected override Note AfterAdd(int newId, Note modal, SqlConnection connection,  SqlTransaction transaction = null)
        {
            var typeenums = modal.Categories.Split(',').Select(int.Parse).ToArray();
            InsertNotesCategory(connection, transaction,  newId, typeenums); //(int)data.TypeEnum);

            return modal;
        }

        public string Update(Note data)
        {
            string Query = "";

            if (data != null)
            {
                data.IsNote = true;
                data.UpdatedOn = DateTime.Now;
                data.UpdatedBy = SessionManager.CurrentUser.PersonId;
            }

            if (data.IsNote)
            {
                Query = @"UPDATE CRM.Note SET Notes=@Notes, TypeEnum=@typeEnum
                        , Title=@title,UpdatedOn=@UpdatedOn,UpdatedBy=@UpdatedBy 
                        , IsEnabled = @isEnabled ";
            }
            else
            {
                Query = @"UPDATE CRM.Note SET TypeEnum=@typeEnum
                        , Title=@title,UpdatedOn=@UpdatedOn,UpdatedBy=@UpdatedBy
                        , IsEnabled = @isEnabled ";
            }

            Query += " WHERE NoteId=@noteId ";
            using (SqlConnection connectionUpdate = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connectionUpdate.Execute(Query, new
                {
                    noteId = data.NoteId,
                    Notes = data.Notes,
                    typeEnum =  0, // TODO: time being just store as 0  //data.TypeEnum,
                    title = data.Title,
                    UpdatedOn = DateTime.UtcNow, //data.UpdatedOn,
                    UpdatedBy = data.UpdatedBy,
                    isEnabled = data.IsEnabled
                });

                var typeenums = data.Categories.Split(',').Select(int.Parse).ToArray();
                InsertNotesCategory(connectionUpdate, null, data.NoteId, typeenums);//(int)data.TypeEnum);
            }

            return Success;
        }

        private void InsertNotesCategory(SqlConnection connection, SqlTransaction transaction,  int noteId, params int[] typeEnums)
        {
            if(noteId > 0)
            {
                var query1 = "Delete from CRM.NotesCategories where noteId = @noteid";
                connection.Execute(query1, new { noteid = noteId }, transaction);
            }

            var query = "Insert into CRM.NotesCategories VALUES(@noteid, @typeenum)";
            foreach (var te in typeEnums)
            {
                connection.Execute(query, new
                {
                    noteid = noteId
                    ,
                    typeenum = te
                }, transaction);
            }
        }

        public string Delete(int id)
        {

            string Query = "";
            int @a = id;
            Query = @"UPDATE CRM.Note SET IsDeleted=1";
            Query += "WHERE NoteId=@noteId";

            using (SqlConnection connectionUpdate = new SqlConnection(ContextFactory.CrmConnectionString))
            {

                connectionUpdate.Execute(Query, new { noteId = id });
            }
            return Success;
        }
        public string Recover(int id)
        {

            string Query = "";
            int @a = id;
            Query = @"UPDATE CRM.Note SET IsDeleted=0";
            Query += "WHERE NoteId=@noteId";

            using (SqlConnection connectionUpdate = new SqlConnection(ContextFactory.CrmConnectionString))
            {

                connectionUpdate.Execute(Query, new { noteId = id });
            }
            return Success;
        }

        public string DeleteDocuments(int id)
        {
            int @a = id;
            string Queryy = @"Delete from CRM.Note WHERE NoteId=@NoteId";

            using (SqlConnection connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var res = connection.Query<Note>("Select * from CRM.Note where NoteId = @NoteId", new { NoteId = id }).FirstOrDefault();

                if (res != null)
                {
                    if (res.AttachmentStreamId != null)
                    {
                        connection.Query("Delete from [dbo].[FileTableTb] where stream_id = @stream_id", new { stream_id = res.AttachmentStreamId });

                        string query1 = "Delete from crm.notescategories where noteid = @noteid";
                        connection.Query(query1, new { noteid = id });
                    }
                }
                connection.Query(Queryy, new { NoteId = id });
                connection.Close();
            }

            return Success;

        }

        public List<GlobalDocuments> GetsalesPacketsDocList(int Id)
        {
            using (SqlConnection connectionUpdate = new SqlConnection(ContextFactory.CrmConnectionString))
            {

                var res = connectionUpdate.Query<GlobalDocuments>("select * from CRM.GlobalDocuments where Concept = @Id and Category = 1 and IsEnabled=1", new { Id = Id }).ToList();
                return res;
            }
        }

        public List<GlobalDocuments> GetInstallPacketsDocList(int Id)
        {
            using (SqlConnection connectionUpdate = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var res = connectionUpdate.Query<GlobalDocuments>("select * from CRM.GlobalDocuments where Concept = @Id and Category = 2 and IsEnabled=1", new { Id = Id }).ToList();
                return res;
            }
        }

        public bool DeleteGlobalDocument(int Id)
        {
            using (SqlConnection connectionUpdate = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var res = connectionUpdate.Execute("Delete from CRM.GlobalDocuments where Id = @Id", new { Id = Id });
                return true;
            }
        }
    }
}


