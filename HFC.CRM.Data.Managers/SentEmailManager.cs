﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using HFC.CRM.Data;
using System.Data.Entity;
using System;
using HFC.CRM.DTO.SentEmail;
using HFC.CRM.Managers.AdvancedEmailManager;
using HFC.CRM.Data.Context;
using System.Net;

namespace HFC.CRM.Managers 
{
    public class SentEmailManager : DataManager
    {
        public SentEmailManager(Franchise franchise, User user)
        {
            Franchise = franchise;
            AuthorizingUser = user;
        }

        public void AddSentEmail(string recipients, string subject, string body, int? leadId = null, int? jobId = null, Int16? templateId = null)
        {
            var bd = WebUtility.HtmlDecode(body);
            var sentEmail = new SentEmail()
            {
                JobId = jobId.HasValue && jobId > 0 ? jobId : null,
                LeadId = leadId,
                FranchiseId = this.Franchise.FranchiseId,
                SentAt = DateTime.Now,
                SentByPersonId = this.AuthorizingUser.PersonId,
                Recipients = recipients,
                Subject = subject,
                Body = bd,
                TemplateId = templateId
            };

            using (var db = ContextFactory.Create())
            {
                if (leadId == null && jobId.HasValue && jobId > 0)
                {
                    var job = db.Jobs.FirstOrDefault(j => j.JobId == jobId);
                    
                    if (job != null)
                    {
                        job.EditHistories.Add(new EditHistory
                        {
                            LoggedByPersonId = this.AuthorizingUser.PersonId,
                            IPAddress = HttpContext.Current.Request.UserHostAddress,
                            HistoryValue =
                                string.Format("<Job operation=\"information\">Email '{0}' has been sent to: {1}</Job>",
                                    subject, recipients),
                            CreatedOnUtc = DateTime.Now
                        });

                        sentEmail.LeadId = job.LeadId;
                    }
                }

                db.SentEmails.Add(sentEmail);
                db.SaveChanges();
            }
        }

    }
}
