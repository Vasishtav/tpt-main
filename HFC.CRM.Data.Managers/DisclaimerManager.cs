﻿using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.CRM.Managers
{
    public class DisclaimerManager : DataManager<DisclaimerModel>
    {
        
        public string Save(DisclaimerModel data)
        {
            if (data.Id != 0)
            {
                DisclaimerUpdate(data);
            }
            else
            {
                data.FranchiseId = SessionManager.CurrentFranchise.FranchiseId;
                var result = Insert<int, DisclaimerModel>(ContextFactory.CrmConnectionString, data);
                return "Success";
            }
            return "Success";
        }
        //to update
        public string DisclaimerUpdate(DisclaimerModel data)
        {
            var Result = ExecuteIEnumerableObject<DisclaimerModel>(ContextFactory.CrmConnectionString, @"select * from [CRM].[Disclaimer] where FranchiseId=@FranchiseId and Id=@Id",
                                                                               new { FranchiseId = SessionManager.CurrentUser.FranchiseId,Id=data.Id}).FirstOrDefault();
            if (Result != null)
            {
                var datas = Update<DisclaimerModel>(ContextFactory.CrmConnectionString, data);
            }
                return "Success";
        }
        //to fetch the value
        public DisclaimerModel GetDisclaimer()
        {
            var ResultList= ExecuteIEnumerableObject<DisclaimerModel>(ContextFactory.CrmConnectionString, @"select * from [CRM].[Disclaimer] where FranchiseId=@FranchiseId",
                                                                               new { FranchiseId = SessionManager.CurrentUser.FranchiseId }).FirstOrDefault();
            return ResultList;
        }

        //to get value for the edit
        public DisclaimerModel GetEdit(int FranchiseId,int Id)
        {
           var Result= ExecuteIEnumerableObject<DisclaimerModel>(ContextFactory.CrmConnectionString, @"select * from [CRM].[Disclaimer] where FranchiseId=@FranchiseId and Id=@Id",
                                                                               new { FranchiseId = SessionManager.CurrentUser.FranchiseId,Id=Id }).FirstOrDefault();
            return Result;
        }
        public override string Add(DisclaimerModel data)
        {
            throw new NotImplementedException();
        }

        public override string Delete(int id)
        {
            throw new NotImplementedException();
        }

        public override DisclaimerModel Get(int id)
        {
            throw new NotImplementedException();
        }

        public override ICollection<DisclaimerModel> Get(List<int> idList)
        {
            throw new NotImplementedException();
        }

        public override string Update(DisclaimerModel data)
        {
            throw new NotImplementedException();
        }
    }
}
