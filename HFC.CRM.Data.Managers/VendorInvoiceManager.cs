﻿using Dapper;
using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Data.Context;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using HFC.CRM.Core.Logs;
using HFC.CRM.DTO.VendorInvoice;
using Z.Dapper.Plus;
using HFC.CRM.Core;

namespace HFC.CRM.Managers
{
    public class VendorInvoiceManager : DataManager<VendorInvoice>
    {
        public VendorInvoiceManager()
        {
        }
        public VendorInvoiceManager(User user, Franchise franchise) : this(user, user, franchise)
        {
        }
        public VendorInvoiceManager(User user, User authorizingUser, Franchise franchise)
        {
            this.User = user;
            this.AuthorizingUser = authorizingUser;
            this.Franchise = franchise;
        }
        PICManager picMgr = new PICManager();
        PurchaseOrderManager poManager = new PurchaseOrderManager();

        public List<VendorInvoiceDashboardDTO> VendorInvoiceDashboard(int VendorId, string datefilter)
        {
            // For this sp, have to pass personId if only required (particular person to filter) else pass null
            string personid = "";
            //if (User.IsInRole(AppConfigManager.FranchiseAdminRole.RoleId, AppConfigManager.DefaultOwnerRole.RoleId))
            //    personid = null;
            if (User.IsInRole(AppConfigManager.DefaultSalesRole.RoleId, AppConfigManager.DefaultSalesNoMargineRole.RoleId))
                personid = User.PersonId.ToString();
            else
                personid = null;

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var viDashboard = connection.Query<VendorInvoiceDashboardDTO>("exec [CRM].[VendorInvoiceDashboard] @FranchiseId,@vendorId,@datefilter",
                    new
                    {
                        FranchiseId = Franchise.FranchiseId,
                        vendorId = VendorId,
                        datefilter = datefilter,
                        PersonId = personid
                    }).ToList();
                return viDashboard;
            }
        }

        /// <summary>
        /// Get VendorInvoice, VendorInvoiceDetail, QuoteLines, AccountNumber (by Territory or shiptolocation)
        /// </summary>
        /// <param name="invoiceNumber"></param>
        /// <returns></returns>
        public VendorInvoiceDTO getdetail(string invoiceNumber)
        {

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var vendorinvoiceq = @"SELECT vi.* FROM crm.VendorInvoice vi
                                        INNER JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = vi.PurchaseOrderId		
				                        WHERE vi.InvoiceNumber = @InvoiceNumber and PICVpo is not null;
                                        SELECT vid.* FROM crm.VendorInvoiceDetail vid
                                        INNER JOIN crm.VendorInvoice vi ON vid.VendorInvoiceId = vi.VendorInvoiceId
                                        WHERE vi.InvoiceNumber = @InvoiceNumber;
                                        SELECT ql.* FROM crm.VendorInvoiceDetail vid
                                        INNER JOIN crm.VendorInvoice vi ON vid.VendorInvoiceId = vi.VendorInvoiceId
                                        INNER JOIN CRM.QuoteLines ql on vid.QuoteLineId=ql.QuoteLineId
                                        WHERE vi.InvoiceNumber = @InvoiceNumber;
                                        SELECT
                                        case 
                                        when mpo.TerritoryId is not null then 
                                        (select top 1 AccountNumber from CRM.VendorTerritoryAccount 
                                        where TerritoryId=mpo.TerritoryId and VendorId=ql.VendorId)
                                        else 
                                        case when mpo.ShipToLocation is not null then (
                                        select top 1 AccountNumber from CRM.VendorTerritoryAccount vta
                                        join CRM.ShipToLocationsMap stlm on vta.TerritoryId=stlm.TerritoryId
                                        where stlm.ShipToLocation=mpo.ShipToLocation and VendorId=ql.VendorId) 
                                        else '' 
                                        end end AccountNumber
                                         FROM crm.VendorInvoice vi
                                        INNER JOIN crm.VendorInvoiceDetail vid ON vid.VendorInvoiceId = vi.VendorInvoiceId
                                        join CRM.MasterPurchaseOrder mpo on vi.PurchaseOrderId=mpo.PurchaseOrderId
                                        INNER JOIN CRM.QuoteLines ql on vid.QuoteLineId=ql.QuoteLineId
                                        WHERE vi.InvoiceNumber = @InvoiceNumber
                                        group by vi.VendorInvoiceId,mpo.PurchaseOrderId, mpo.TerritoryId,mpo.ShipToLocation,ql.VendorId;";
                var data = connection.QueryMultiple(vendorinvoiceq, new { InvoiceNumber = invoiceNumber });
                var vInvoice = data.Read<VendorInvoice>().FirstOrDefault();
                var vInvoiceDetailList = data.Read<VendorInvoiceDetail>().ToList();
                var quotelines = data.Read<QuoteLines>().ToList();
                var AccountNumber = data.Read<string>().FirstOrDefault();

                VendorInvoiceDTO VendorInvoice = new VendorInvoiceDTO();
                List<VendorInvoiceDetailDTO> VendorInvoiceDetail = new List<VendorInvoiceDetailDTO>();
                if (vInvoice != null)
                {
                    VendorInvoice.VendorInvoiceId = vInvoice.VendorInvoiceId;
                    VendorInvoice.PurchaseOrderId = vInvoice.PurchaseOrderId;
                    VendorInvoice.Vendor = vInvoice.VendorName;
                    VendorInvoice.InvoiceNo = vInvoice.InvoiceNumber;
                    VendorInvoice.InvoiceDate = vInvoice.Date;
                    VendorInvoice.Currency = vInvoice.Currency;
                    VendorInvoice.SoldTo = Franchise.Name;
                    VendorInvoice.BillTo = Franchise.Address.ToString();
                    VendorInvoice.CustomerID = AccountNumber;
                    VendorInvoice.Terms = vInvoice.Terms;
                    VendorInvoice.RemitBy = vInvoice.RemitByDate;
                    VendorInvoice.VendorComments = vInvoice.Comments;
                    VendorInvoice.Subtotal = vInvoice.Subtotal;
                    VendorInvoice.Discount = vInvoice.Discount;
                    VendorInvoice.Freight = vInvoice.Freight;
                    VendorInvoice.ProcessingFee = vInvoice.ProcessingFee;
                    VendorInvoice.MiscCharge = vInvoice.MiscCharge;
                    VendorInvoice.Tax = vInvoice.Tax;
                    VendorInvoice.TotalAmount = vInvoice.Total;
                    VendorInvoice.PurchaseOrder = vInvoice.PICVpo;

                    VendorInvoice.QTYOrdered = vInvoiceDetailList != null && vInvoiceDetailList.Count > 0 ? vInvoiceDetailList.Sum(x => x.Quantity) : 0;
                    VendorInvoice.QTYInvoiced = vInvoiceDetailList != null && vInvoiceDetailList.Count > 0 ? vInvoiceDetailList.Sum(x => x.Quantity) : 0; ;
                    VendorInvoice.ShippedDate = vInvoiceDetailList != null && vInvoiceDetailList.Count > 0 ? vInvoiceDetailList.Select(x => x.ShipDate).FirstOrDefault() : null;
                    VendorInvoice.ShippedVia = vInvoiceDetailList != null && vInvoiceDetailList.Count > 0 ? vInvoiceDetailList.Select(x => x.FOB).FirstOrDefault() : "";

                    VendorInvoice.AdjSubtotal = vInvoice.AdjSubtotal;
                    VendorInvoice.AdjDiscount = vInvoice.AdjDiscount;
                    VendorInvoice.AdjFreight = vInvoice.AdjFreight;
                    VendorInvoice.AdjProcessingFee = vInvoice.AdjProcessingFee;
                    VendorInvoice.AdjMiscCharge = vInvoice.AdjMiscCharge;
                    VendorInvoice.AdjTax = vInvoice.AdjTax;
                    VendorInvoice.AdjTotal = vInvoice.AdjTotal;
                    VendorInvoice.AdjComments = vInvoice.AdjComments;
                    VendorInvoice.IsInvoiceRevised = vInvoice.IsInvoiceRevised;
                }

                if (vInvoiceDetailList != null && vInvoiceDetailList.Count > 0)
                {
                    foreach (var vid in vInvoiceDetailList)
                    {
                        var ql = quotelines.Where(x => x.QuoteLineId == vid.QuoteLineId).FirstOrDefault();
                        VendorInvoiceDetailDTO vInvoiceDetail = new VendorInvoiceDetailDTO();
                        vInvoiceDetail.VendorInvoiceId = vid.VendorInvoiceId;
                        vInvoiceDetail.VendorInvoiceDetailId = vid.VendorInvoiceDetailId;
                        vInvoiceDetail.POLineNum = vid.POLineNumber;
                        vInvoiceDetail.ProductNumber = vid.ProductNumber;
                        vInvoiceDetail.Item = vid.Name;
                        vInvoiceDetail.ItemDescription = ql != null ? ql.Description : "";
                        vInvoiceDetail.QTY = vid.Quantity;
                        vInvoiceDetail.Cost = vid.Cost;
                        vInvoiceDetail.Discount = vid.ItemDiscount;
                        vInvoiceDetail.Amount = vid.ItemAmount;
                        vInvoiceDetail.Comment = vid.Comment;

                        vInvoiceDetail.AdjCost = vid.AdjCost;
                        vInvoiceDetail.AdjDiscount = vid.AdjDiscount;
                        vInvoiceDetail.AdjAmount = vid.AdjAmount;
                        vInvoiceDetail.AdjComments = vid.AdjComments;

                        VendorInvoiceDetail.Add(vInvoiceDetail);
                    }
                    VendorInvoice.VendorInvoiceDetail = VendorInvoiceDetail;
                }
                else VendorInvoice.VendorInvoiceDetail = VendorInvoiceDetail;
                return VendorInvoice;
            }
        }

        /// <summary>
        /// Vendor Invocie History and Audit trial
        /// </summary>
        /// <param name="invoiceNumber"></param>
        /// <returns></returns>
        public VendorInvoiceDTO getdetailwithhistory(string invoiceNumber)
        {

            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                var vendorinvoiceq = @"SELECT vich.CreatedOn, p.FirstName+' '+p.LastName as PersonName,vid.POLineNumber,vich.Change,
                                        case 
                                        when vich.VendorInvoiceDetailId is null and vich.Change='Order Subtotal' then  cast(vi.Subtotal as varchar)
                                        when vich.VendorInvoiceDetailId is null and vich.Change='Discount' then  cast(vi.Discount as varchar)
                                        when vich.VendorInvoiceDetailId is null and vich.Change='Freight' then  cast(vi.Freight as varchar)
                                        when vich.VendorInvoiceDetailId is null and vich.Change='Processing Fee' then  cast(vi.ProcessingFee as varchar)
                                        when vich.VendorInvoiceDetailId is null and vich.Change='Misc. Charge' then  cast(vi.MiscCharge as varchar)
                                        when vich.VendorInvoiceDetailId is null and vich.Change='Tax' then  cast(vi.Tax as varchar)
                                        when vich.VendorInvoiceDetailId is null and vich.Change='Total Amount' then  cast(vi.Total as varchar)
                                        when vich.VendorInvoiceDetailId is null and vich.Change='Comments' then  vi.Comments
                                        when vich.VendorInvoiceDetailId is not null and vich.Change='Cost' then  cast(vid.Cost as varchar)
                                        when vich.VendorInvoiceDetailId is not null and vich.Change='Discount' then  cast(vid.ItemDiscount as varchar)
                                        when vich.VendorInvoiceDetailId is not null and vich.Change='Amount' then  cast(vid.ItemAmount as varchar)
                                        when vich.VendorInvoiceDetailId is not null and vich.Change='Comments' then  vid.Comment
                                        else '' end as OrgValue,
                                        vich.AdjValue FROM CRM.VendorInvoiceChangeHistory vich
                                        left join crm.VendorInvoiceDetail vid on vich.VendorInvoiceDetailId=vid.VendorInvoiceDetailId
                                        join crm.VendorInvoice vi ON vich.VendorInvoiceId = vi.VendorInvoiceId
                                        join CRM.Person p on vich.PersonId=p.PersonId
                                        WHERE vi.InvoiceNumber = @InvoiceNumber order by CreatedOn desc;";
                var vInvoiceHistoryList = connection.Query<VendorInvoiceChangeHistoryDTO>(vendorinvoiceq, new { InvoiceNumber = invoiceNumber }).ToList();

                VendorInvoiceDTO VendorInvoice = getdetail(invoiceNumber);
                List<string> audittrail = new List<string>();
                if (vInvoiceHistoryList != null && vInvoiceHistoryList.Count > 0)
                {
                    foreach (var vih in vInvoiceHistoryList)
                    {
                        vih.CreatedOn = TimeZoneManager.ToLocal(vih.CreatedOn);
                        string at = (vih.POLineNumber != null ? "Line" + vih.POLineNumber + ": " : "") + vih.Change + " has been adjusted by " + vih.PersonName + " " + vih.CreatedOn.ToString("MM/dd/yyyy hh:mm tt");
                        audittrail.Add(at);
                    }
                    VendorInvoice.VendorInvoiceChangeHistory = vInvoiceHistoryList;
                    VendorInvoice.AuditTrail = audittrail;
                }
                else
                {
                    VendorInvoice.VendorInvoiceChangeHistory = vInvoiceHistoryList;
                    VendorInvoice.AuditTrail = audittrail;
                }

                return VendorInvoice;
            }
        }

        /// <summary>
        /// Save Revised Invoice info
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool SaveVendorInvoiceDetail(VendorInvoiceDTO model)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    var vendorinvoiceq = @"SELECT vi.* FROM crm.VendorInvoice vi
                                        INNER JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = vi.PurchaseOrderId		
				                        WHERE vi.InvoiceNumber = @InvoiceNumber  and PICVpo is not null;
                                        SELECT vid.* FROM crm.VendorInvoiceDetail vid
                                        INNER JOIN crm.VendorInvoice vi ON vid.VendorInvoiceId = vi.VendorInvoiceId
                                        WHERE vi.InvoiceNumber = @InvoiceNumber;";
                    var data = connection.QueryMultiple(vendorinvoiceq, new { InvoiceNumber = model.InvoiceNo }, transaction);
                    var vInvoice = data.Read<VendorInvoice>().FirstOrDefault();
                    var vInvoiceDetailList = data.Read<VendorInvoiceDetail>().ToList();

                    List<VendorInvoiceChangeHistory> vichistory = new List<VendorInvoiceChangeHistory>();
                    #region VendorInvoice update
                    if (vInvoice != null)
                    {
                        vInvoice.AdjSubtotal = vInvoice.AdjSubtotal ?? 0; model.AdjSubtotal = model.AdjSubtotal ?? 0;
                        vInvoice.AdjDiscount = vInvoice.AdjDiscount ?? 0; model.AdjDiscount = model.AdjDiscount ?? 0;
                        vInvoice.AdjFreight = vInvoice.AdjFreight ?? 0; model.AdjFreight = model.AdjFreight ?? 0;
                        vInvoice.AdjProcessingFee = vInvoice.AdjProcessingFee ?? 0; model.AdjProcessingFee = model.AdjProcessingFee ?? 0;
                        vInvoice.AdjMiscCharge = vInvoice.AdjMiscCharge ?? 0; model.AdjMiscCharge = model.AdjMiscCharge ?? 0;
                        vInvoice.AdjTax = vInvoice.AdjTax ?? 0; model.AdjTax = model.AdjTax ?? 0;
                        vInvoice.AdjTotal = vInvoice.AdjTotal ?? 0; model.AdjTotal = model.AdjTotal ?? 0;
                        vInvoice.AdjComments = vInvoice.AdjComments ?? ""; model.AdjComments = model.AdjComments ?? "";

                        if (vInvoice.AdjSubtotal != model.AdjSubtotal)
                            vichistory.Add(CreateObjVIHistory(vInvoice.VendorInvoiceId, null, "Order Subtotal", Convert.ToString(model.AdjSubtotal)));
                        if (vInvoice.AdjDiscount != model.AdjDiscount)
                            vichistory.Add(CreateObjVIHistory(vInvoice.VendorInvoiceId, null, "Discount", Convert.ToString(model.AdjDiscount)));
                        if (vInvoice.AdjFreight != model.AdjFreight)
                            vichistory.Add(CreateObjVIHistory(vInvoice.VendorInvoiceId, null, "Freight", Convert.ToString(model.AdjFreight)));
                        if (vInvoice.AdjProcessingFee != model.AdjProcessingFee)
                            vichistory.Add(CreateObjVIHistory(vInvoice.VendorInvoiceId, null, "Processing Fee", Convert.ToString(model.AdjProcessingFee)));
                        if (vInvoice.AdjMiscCharge != model.AdjMiscCharge)
                            vichistory.Add(CreateObjVIHistory(vInvoice.VendorInvoiceId, null, "Misc. Charge", Convert.ToString(model.AdjMiscCharge)));
                        if (vInvoice.AdjTax != model.AdjTax)
                            vichistory.Add(CreateObjVIHistory(vInvoice.VendorInvoiceId, null, "Tax", Convert.ToString(model.AdjTax)));
                        if (vInvoice.AdjTotal != model.AdjTotal)
                            vichistory.Add(CreateObjVIHistory(vInvoice.VendorInvoiceId, null, "Total Amount", Convert.ToString(model.AdjTotal)));
                        if (vInvoice.AdjComments != model.AdjComments)
                            vichistory.Add(CreateObjVIHistory(vInvoice.VendorInvoiceId, null, "Comments", Convert.ToString(model.AdjComments)));

                        vInvoice.AdjSubtotal = model.AdjSubtotal;
                        vInvoice.AdjDiscount = model.AdjDiscount;
                        vInvoice.AdjFreight = model.AdjFreight;
                        vInvoice.AdjProcessingFee = model.AdjProcessingFee;
                        vInvoice.AdjMiscCharge = model.AdjMiscCharge;
                        vInvoice.AdjTax = model.AdjTax;
                        vInvoice.AdjTotal = model.AdjTotal;
                        vInvoice.AdjComments = model.AdjComments;
                        connection.Update(vInvoice, transaction);
                    }
                    #endregion

                    #region VendorInvoiceDetail Update
                    if (vInvoiceDetailList != null && vInvoiceDetailList.Count > 0 && model.VendorInvoiceDetail != null && model.VendorInvoiceDetail.Count > 0)
                    {
                        foreach (var vid in vInvoiceDetailList)
                        {
                            var vidmodel = model.VendorInvoiceDetail.Where(x => x.VendorInvoiceDetailId == vid.VendorInvoiceDetailId).FirstOrDefault();
                            if (vidmodel != null)
                            {
                                vid.AdjCost = vid.AdjCost ?? 0; vidmodel.AdjCost = vidmodel.AdjCost ?? 0;
                                vid.AdjDiscount = vid.AdjDiscount ?? 0; vidmodel.AdjDiscount = vidmodel.AdjDiscount ?? 0;
                                vid.AdjAmount = vid.AdjAmount ?? 0; vidmodel.AdjAmount = vidmodel.AdjAmount ?? 0;
                                vid.AdjComments = vid.AdjComments ?? ""; vidmodel.AdjComments = vidmodel.AdjComments ?? "";

                                if (vid.AdjCost != vidmodel.AdjCost)
                                    vichistory.Add(CreateObjVIHistory(vid.VendorInvoiceId, vid.VendorInvoiceDetailId, "Cost", Convert.ToString(vidmodel.AdjCost)));
                                if (vid.AdjDiscount != vidmodel.AdjDiscount)
                                    vichistory.Add(CreateObjVIHistory(vid.VendorInvoiceId, vid.VendorInvoiceDetailId, "Discount", Convert.ToString(vidmodel.AdjDiscount)));
                                if (vid.AdjAmount != vidmodel.AdjAmount)
                                    vichistory.Add(CreateObjVIHistory(vid.VendorInvoiceId, vid.VendorInvoiceDetailId, "Amount", Convert.ToString(vidmodel.AdjAmount)));
                                if (vid.AdjComments != vidmodel.AdjComments)
                                    vichistory.Add(CreateObjVIHistory(vid.VendorInvoiceId, vid.VendorInvoiceDetailId, "Comments", Convert.ToString(vidmodel.AdjComments)));

                                vid.AdjCost = vidmodel.AdjCost;
                                vid.AdjDiscount = vidmodel.AdjDiscount;
                                vid.AdjAmount = vidmodel.AdjAmount;
                                vid.AdjComments = vidmodel.AdjComments;
                            }
                        }
                        transaction.BulkUpdate(vInvoiceDetailList);
                    }
                    #endregion

                    if (vichistory != null && vichistory.Count > 0)
                        transaction.BulkInsert(vichistory);
                    transaction.Commit();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    EventLogger.LogEvent(ex);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Enable Revised Invoice
        /// </summary>
        /// <param name="invoiceNumber"></param>
        /// <returns></returns>
        public bool EnableRevisedInvoice(string invoiceNumber)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                connection.Execute("update CRM.VendorInvoice set IsInvoiceRevised=1 where InvoiceNumber=@InvoiceNumber",
                    new { InvoiceNumber = invoiceNumber });
            }
            return true;
        }

        private VendorInvoiceChangeHistory CreateObjVIHistory(int VendorInvoiceId, int? VendorInvoiceDetailId, string Change, string AdjValue)
        {
            return new VendorInvoiceChangeHistory()
            {
                VendorInvoiceId = VendorInvoiceId,
                VendorInvoiceDetailId = VendorInvoiceDetailId,
                PersonId = User.Person.PersonId,
                Change = Change,
                AdjValue = AdjValue,
                CreatedOn = DateTime.UtcNow
            };
        }

        public List<VendorInvoiceDetail> GridValue(int id, string invoiceNumber)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                VendorInvoiceDetail results = new VendorInvoiceDetail();
                connection.Open();

                var query = @"SELECT vid.* FROM crm.VendorInvoiceDetail vid
                INNER JOIN crm.VendorInvoice vi ON vid.VendorInvoiceId = vi.VendorInvoiceId
                WHERE vi.InvoiceNumber = @InvoiceNumber";

                var GridVendor = connection.Query<VendorInvoiceDetail>(query, new { InvoiceNumber = invoiceNumber }).ToList();
                return GridVendor;
            }
        }
        public bool OneTimeUpdateVendorInvoice()
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {
                    VendorInvoiceDetail results = new VendorInvoiceDetail();
                    connection.Open();


                    var query = @"SELECT distinct CAST(vi.PICVpo AS int) FROM crm.VendorInvoice vi";
                    var ListPOs = connection.Query<int>(query).ToList();

                    foreach (var item in ListPOs)
                    {
                        InsertOrUpdateVendorInvoiceByVPO(item);
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
        }

        public bool InsertOrUpdateVendorInvoiceByVPO(int picVPO)
        {
            using (var connection = new SqlConnection(ContextFactory.CrmConnectionString))
            {
                try
                {

                    var usr = AdminPersonId;// Admin account
                    if (SessionManager.CurrentFranchise != null)
                    {
                        usr = SessionManager.CurrentUser.PersonId;
                    }

                    var dateTime = DateTime.UtcNow;

                    var query = @"SELECT vi.* FROM crm.VendorInvoice vi WHERE vi.PICVpo= @PICPO;

                                  SELECT vid.* FROM crm.VendorInvoice vi
                                  INNER JOIN crm.VendorInvoiceDetail vid ON vi.VendorInvoiceId = vid.VendorInvoiceId
                                  INNER JOIN crm.PurchaseOrderDetails pod ON pod.QuoteLineId = vid.QuoteLineId
                                  WHERE pod.PICPO = @PICPO;

                                  SELECT ql.QuoteLineNumber as LineNumber,pod.PurchaseOrdersDetailId,pod.PurchaseOrderId,pod.QuoteLineId,pod.PartNumber,ql.ProductName,ql.ProductTypeId,pod.QuantityPurchased AS purchased,
                                    ql.VendorName,ql.Description,ql.Quantity,pod.PartNumber,pod.ShipDate,pod.PromiseByDate,pod.Status,pod.NotesVendor,pod.PICPO,pod.POResponse,pod.StatusId,pod.errors,
                                    pod.PONumber,  pod.PickedBy,pod.vendorReference  FROM crm.PurchaseOrderDetails pod
                                    INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
                                    INNER JOIN crm.MasterPurchaseOrder mpo ON mpo.PurchaseOrderId = pod.PurchaseOrderId
                                    left join crm.MasterPurchaseOrderExtn mpox on mpox.PurchaseOrderId = mpo.PurchaseOrderId
                                    INNER JOIN crm.Opportunities o ON o.OpportunityId = mpox.OpportunityId
                                    WHERE ql.ProductTypeId = 1 AND   pod.PICPO = @PICPO;

                                    SELECT pod.* FROM crm.PurchaseOrderDetails pod WHERE pod.PICPO = @PICPO;

                                    SELECT ql.* FROM crm.PurchaseOrderDetails pod 
	                                INNER JOIN crm.QuoteLines ql ON ql.QuoteLineId = pod.QuoteLineId
	                                WHERE pod.PICPO = @PICPO;";

                    connection.Open();


                    var data = connection.QueryMultiple(query, new { PICPO = picVPO });

                    var vInvoiceList = data.Read<VendorInvoice>().ToList();
                    var vInvoiceDetailList = data.Read<VendorInvoiceDetail>().ToList();
                    var poDetailsList = data.Read<VPODetail>().ToList();
                    var poList = data.Read<PurchaseOrderDetails>().ToList();
                    var qlList = data.Read<QuoteLines>().ToList();

                    foreach (var poItem in poList)
                    {
                        poItem.QuoteLines = qlList.Where(x => x.QuoteLineId == poItem.QuoteLineId).FirstOrDefault();
                    }

                    var apResponse = picMgr.GetVendorInvoice(picVPO);
                    var apResponseObject = JsonConvert.DeserializeObject<RootObjectAP>(apResponse);

                    if (apResponseObject.valid)
                    {
                        if (vInvoiceList != null && vInvoiceList.Count > 0)
                        {
                            //var vInvoice = apResponseObject.properties[0].AccountsPayable;
                            //foreach (var viItem in vInvoiceList)
                            //{
                            //    viItem.InvoiceNumber = vInvoice.invoice;
                            //    if (vInvoice.date != null && vInvoice.date != "" && vInvoice.date != "0000-00-00")
                            //    {
                            //        viItem.Date = Convert.ToDateTime(vInvoice.date);
                            //    }

                            //    viItem.Terms = vInvoice.discountRate.ToString();
                            //    if (vInvoice.dueDate != null && vInvoice.dueDate != "" && vInvoice.dueDate != "0000-00-00")
                            //    {
                            //        viItem.RemitByDate = Convert.ToDateTime(vInvoice.dueDate);

                            //    }

                            //    viItem.Subtotal = vInvoice.totalGoods;
                            //    viItem.Tax = vInvoice.tax;
                            //    viItem.Freight = viItem.Freight;
                            //    viItem.Discount = viItem.Discount;
                            //    if (poDetailsList != null && poDetailsList.Count > 0)
                            //    {
                            //        viItem.VendorName = poDetailsList.FirstOrDefault().VendorName;
                            //    }
                            //    //viItem.Vendor = poDetailsLis
                            //    if (vInvoice.discountDate != null && vInvoice.discountDate != "" && vInvoice.discountDate != "0000-00-00")
                            //    {
                            //        viItem.DiscountDate = Convert.ToDateTime(vInvoice.discountDate);
                            //    }

                            //    viItem.Total = viItem.Subtotal - viItem.Discount;
                            //    viItem.PICInvoiceResponse = apResponse;

                            //    viItem.UpdatedBy = usr;
                            //    viItem.UpdatedOn = dateTime;
                            //    viItem.Quantity = Convert.ToInt32(apResponseObject.properties[0].Items.Sum(x => x.quantity));

                            //    connection.Update(viItem);
                            //    var details = vInvoiceDetailList.Where(x => x.VendorInvoiceId == viItem.VendorInvoiceId)
                            //        .ToList();
                            //    foreach (var vDItem in details)
                            //    {
                            //        var vDetails = apResponseObject.properties[0].Items
                            //            .Where(x => x.item == vDItem.POLineNumber.Value).FirstOrDefault();
                            //        var poItem = poDetailsList.Where(x =>
                            //            x.Quantity == Convert.ToInt32(vDetails.quantity) &&
                            //            x.Cost == vDetails.unitPrice).FirstOrDefault();
                            //        vDItem.POLineNumber = vDetails.item;
                            //        vDItem.Quantity = Convert.ToInt32(vDetails.quantity);
                            //        vDItem.Cost = vDetails.unitPrice;
                            //        if (poItem != null)
                            //        {
                            //            vDItem.Name = poItem.ProductName;
                            //            vDItem.ProductNumber = poItem.PartNumber;
                            //        }
                            //        else
                            //        {
                            //            vDItem.Name = poDetailsList.FirstOrDefault().ProductName;
                            //            vDItem.ProductNumber = poDetailsList.FirstOrDefault().PartNumber;
                            //        }

                            //        vDItem.Comment = vDetails.comment;
                            //        vDItem.UpdatedBy = usr;
                            //        vDItem.UpdatedOn = dateTime;
                            //        connection.Update(vDItem);
                            //    }

                            //}


                        }
                        else
                        {
                            if (apResponseObject.properties.Count > 0)
                            {
                                foreach (var item in apResponseObject.properties)
                                {
                                    var vInvoice = item.AccountsPayable;
                                    var vQuery = @"SELECT * FROM crm.VendorInvoice vi WHERE vi.InvoiceNumber=@invoiceNumber";

                                    var vendorInv = connection.Query<VendorInvoice>(vQuery, new { invoiceNumber = vInvoice.invoice }).FirstOrDefault();
                                    if(vendorInv!=null && vendorInv.VendorInvoiceId != 0)
                                    {

                                    }
                                    else
                                    {

                                        //var vInvoice = apResponseObject.properties[0].AccountsPayable;
                                        DateTime temp;
                                        var viItem = new VendorInvoice();
                                        viItem.InvoiceNumber = vInvoice.invoice;
                                        viItem.PurchaseOrderId = poList.FirstOrDefault().PurchaseOrderId;
                                        //viItem.PurchaseOrdersDetailId = poItem.PurchaseOrdersDetailId;
                                        viItem.VendorName = poDetailsList.FirstOrDefault().VendorName;
                                        viItem.PICAP = vInvoice.ap;
                                        viItem.PICVpo = picVPO.ToString();
                                        if (vInvoice.date != null && vInvoice.date != "" && vInvoice.date != "0000-00-00" && DateTime.TryParse(vInvoice.date, out temp))
                                        {
                                            viItem.Date = Convert.ToDateTime(vInvoice.date);
                                        }
                                        else
                                        {
                                            viItem.Date = null;
                                        }

                                        viItem.Terms = vInvoice.discountRate.ToString();
                                        if (vInvoice.dueDate != null && vInvoice.dueDate != "" && vInvoice.dueDate != "0000-00-00" && DateTime.TryParse(vInvoice.dueDate, out temp))
                                        {
                                            viItem.RemitByDate = Convert.ToDateTime(vInvoice.dueDate);
                                        }
                                        else
                                        {
                                            viItem.RemitByDate = null;
                                        }
                                        viItem.Subtotal = vInvoice.totalGoods;
                                        viItem.Tax = vInvoice.tax;
                                        viItem.Freight = vInvoice.freight;
                                        viItem.Discount = vInvoice.discount;
                                        if (vInvoice.GetType().GetProperty("date") != null)
                                        {
                                            if (vInvoice.date != null && vInvoice.date != "" && vInvoice.date != "0000-00-00" && DateTime.TryParse(vInvoice.date, out temp))
                                            {
                                                viItem.Date = Convert.ToDateTime(vInvoice.date);
                                            }
                                            else
                                            {
                                                viItem.Date = null;
                                            }
                                        }
                                        if (vInvoice.discountDate != null && vInvoice.discountDate != "" && vInvoice.discountDate != "0000-00-00" && DateTime.TryParse(vInvoice.discountDate, out temp))
                                        {
                                            viItem.DiscountDate = Convert.ToDateTime(vInvoice.discountDate);
                                        }
                                        else
                                        {
                                            viItem.DiscountDate = null;
                                        }

                            viItem.Total = vInvoice.total - vInvoice.discount;
                                        viItem.PICInvoiceResponse = apResponse;
                                        viItem.Quantity = Convert.ToInt32(apResponseObject.properties[0].Items.Sum(x => x.quantity));

                                        viItem.CreatedOn = dateTime;
                                        viItem.CreatedBy = usr;
                                        viItem.UpdatedBy = usr;
                                        viItem.UpdatedOn = dateTime;


                                        viItem.VendorInvoiceId = connection.Insert<int, VendorInvoice>(viItem);

                                        var details = vInvoiceDetailList.Where(x => x.VendorInvoiceId == viItem.VendorInvoiceId)
                                            .ToList();
                                        var obj = apResponseObject.properties[0];


                                        for (int i = 0; i < obj.Items.Count(); i++)
                                        {

                                            var vDItem = new VendorInvoiceDetail();

                                            vDItem.VendorInvoiceId = viItem.VendorInvoiceId;

                                            vDItem.Quantity = Convert.ToInt32(obj.Items[i].quantity);
                                            vDItem.Cost = obj.Items[i].unitPrice;
                                            vDItem.Comment = obj.Items[i].comment;

                                            vDItem.CreatedOn = viItem.CreatedOn;
                                            vDItem.CreatedBy = viItem.CreatedBy;
                                            vDItem.UpdatedBy = viItem.UpdatedBy;
                                            vDItem.UpdatedOn = viItem.UpdatedOn;


                                            if (obj.Items[i].ediCustomerItem.HasValue && obj.Items[i].ediCustomerItem != 0)
                                            {
                                                vDItem.QuoteLineId = obj.Items[i].ediCustomerItem.Value;

                                                var po = poList.Where(x => x.QuoteLineId == vDItem.QuoteLineId).FirstOrDefault();
                                                vDItem.ProductNumber = po.PartNumber;
                                                vDItem.Name = po.QuoteLines.ProductName;
                                                vDItem.POLineNumber = po.QuoteLines.QuoteLineNumber;
                                            }
                                            else
                                            {
                                                if (i <= poList.Count() - 1)
                                                {
                                                    vDItem.QuoteLineId = poList[i].QuoteLineId;
                                                    vDItem.ProductNumber = poList[i].PartNumber;
                                                    vDItem.Name = poList[i].QuoteLines.ProductName;
                                                    vDItem.POLineNumber = poList[i].QuoteLines.QuoteLineNumber;
                                                }
                                                else
                                                {
                                                    vDItem.QuoteLineId = 0;
                                                    vDItem.ProductNumber = "";
                                                    vDItem.Name = "";
                                                    vDItem.POLineNumber = null;
                                                }

                                            }

                                            connection.Insert<int, VendorInvoiceDetail>(vDItem);
                                        }

                                        foreach (var poDetailsitem in poList)
                                        {
                                            poDetailsitem.StatusId = (int)EnumExtension.PurchaseOrderStatus.Invoiced;
                                            poDetailsitem.Status = Enum.GetName(typeof(EnumExtension.PurchaseOrderStatus), 5);
                                            connection.Update(poDetailsitem);
                                        }
                                        poManager.SetMasterPurchaseOrderStatus(poList.FirstOrDefault().PurchaseOrderId);
                                    }


                                }
                            }
                            //foreach (var poItem in poDetailsList)
                            //{
                            //    var viItem = new VendorInvoice();
                            //    viItem.InvoiceNumber = vInvoice.invoice;
                            //    viItem.PurchaseOrderId = poItem.PurchaseOrderId;
                            //    viItem.PurchaseOrdersDetailId = poItem.PurchaseOrdersDetailId;
                            //    viItem.VendorName = poItem.VendorName;
                            //    viItem.PICAP = vInvoice.ap;
                            //    if (vInvoice.date != null && vInvoice.date != "" && vInvoice.date != "0000-00-00")
                            //    {
                            //        viItem.Date = Convert.ToDateTime(vInvoice.date);
                            //    }
                            //    else
                            //    {
                            //        viItem.Date = null;
                            //    }

                            //    viItem.Terms = vInvoice.discountRate.ToString();
                            //    if (vInvoice.dueDate != null && vInvoice.dueDate != "" && vInvoice.dueDate != "0000-00-00")
                            //    {
                            //        viItem.RemitByDate = Convert.ToDateTime(vInvoice.date);
                            //    }
                            //    else
                            //    {
                            //        viItem.RemitByDate = null;
                            //    }
                            //    viItem.Subtotal = vInvoice.totalGoods;
                            //    viItem.Tax = vInvoice.tax;
                            //    viItem.Freight = viItem.Freight;
                            //    viItem.Discount = viItem.Discount;
                            //    if (vInvoice.discountDate != null && vInvoice.discountDate != "" && vInvoice.discountDate != "0000-00-00")
                            //    {
                            //        viItem.DiscountDate = Convert.ToDateTime(vInvoice.discountDate);
                            //    }
                            //    else
                            //    {
                            //        viItem.DiscountDate = null;
                            //    }

                            //    viItem.Total = viItem.Subtotal - Convert.ToDecimal(viItem.Discount);
                            //    viItem.PICInvoiceResponse = apResponse;
                            //    viItem.Quantity = Convert.ToInt32(apResponseObject.properties[0].Items.Sum(x => x.quantity));

                            //    viItem.CreatedOn = dateTime;
                            //    viItem.CreatedBy = usr;
                            //    viItem.UpdatedBy = usr;
                            //    viItem.UpdatedOn = dateTime;


                            //    var id = connection.Insert<int, VendorInvoice>(viItem);
                            //    var details = vInvoiceDetailList.Where(x => x.VendorInvoiceId == viItem.VendorInvoiceId)
                            //        .ToList();
                            //    var vDetails = apResponseObject.properties[0].Items;
                            //    foreach (var item in vDetails)
                            //    {
                            //        var vDItem = new VendorInvoiceDetail();

                            //        vDItem.VendorInvoiceId = id;
                            //        vDItem.POLineNumber = poItem.LineNumber;
                            //        vDItem.Quantity = Convert.ToInt32(item.quantity);
                            //        vDItem.Cost = item.unitPrice;
                            //        vDItem.Name = poItem.ProductName;
                            //        vDItem.ProductNumber = poItem.PartNumber;
                            //        vDItem.Comment = item.comment;

                            //        vDItem.CreatedOn = dateTime;
                            //        vDItem.CreatedBy = usr;
                            //        vDItem.UpdatedBy = usr;
                            //        vDItem.UpdatedOn = dateTime;
                            //        connection.Insert<int, VendorInvoiceDetail>(vDItem);

                            //    }
                            //    //mpoDetails.POStatusId = (int)PurchaseOrderStatus.Canceled;
                            //    //mpoDetails.Status = Enum.GetName(typeof(PurchaseOrderStatus), 8);
                            //    var poDetailsitem = poList.Where(x => x.PurchaseOrdersDetailId == poItem.PurchaseOrdersDetailId).FirstOrDefault();
                            //    poDetailsitem.StatusId= (int)EnumExtension.PurchaseOrderStatus.Invoiced;
                            //    poDetailsitem.Status = Enum.GetName(typeof(EnumExtension.PurchaseOrderStatus),5 );
                            //    connection.Update(poDetailsitem);
                            //    //foreach (var vDItem in details)
                            //    //{
                            //    //    vDItem.VendorInvoiceId = id;
                            //    //    var vDetails = apResponseObject.properties[0].Items
                            //    //        .Where(x => x.item == vDItem.POLineNumber.Value).FirstOrDefault();
                            //    //    vDItem.POLineNumber = vDetails.item;
                            //    //    vDItem.Quantity = Convert.ToInt32(vDetails.quantity);
                            //    //    vDItem.Cost = vDetails.unitPrice;
                            //    //    vDItem.Name = vDetails.poItem.ToString();
                            //    //    vDItem.ProductNumber = vDetails.woItem.ToString();
                            //    //    vDItem.Comment = vDetails.comment;

                            //    //    vDItem.CreatedOn = dateTime;
                            //    //    vDItem.CreatedBy = usr;
                            //    //    vDItem.UpdatedBy = usr;
                            //    //    vDItem.UpdatedOn = dateTime;
                            //    //    connection.Insert<int, VendorInvoiceDetail>(vDItem);
                            //    //}
                            //    poManager.SetMasterPurchaseOrderStatus(poItem.PurchaseOrderId);
                            //}



                        }
                    }

                    //new { picVPO = picVPO }

                    //var vInvoiceList = connection.Query<VendorInvoice,VendorInvoiceDetail,VendorInvoice>(query,
                    //    (vInvoice, vInvoiceDetails) => { vInvoice.VendorInvoiceDetail=vInvoiceDetails;
                    //        return vInvoice;
                    //    },splitOn: "VendorInvoiceId").ToList();

                    //if (vInvoiceList != null && vInvoiceList.Count > 0)
                    //{

                    //}



                    return true;


                }
                catch (Exception e)
                {
                    EventLogger.LogEvent(e);
                    //throw e;
                    return false;

                }
                finally
                {
                    connection.Close();
                }
            }
            //return true;
        }

        public override string Add(VendorInvoice data)
        {
            throw new NotImplementedException();
        }

        public override string Delete(int id)
        {
            throw new NotImplementedException();
        }

        public override VendorInvoice Get(int id)
        {
            throw new NotImplementedException();
        }

        public override ICollection<VendorInvoice> Get(List<int> idList)
        {
            throw new NotImplementedException();
        }

        public override string Update(VendorInvoice data)
        {
            throw new NotImplementedException();
        }
    }
}
