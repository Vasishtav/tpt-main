USE [MyCrmTP03]
GO
/****** Object:  Trigger [CRM].[LeadStatusTrackerTrigger]    Script Date: 21-Dec-17 7:19:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER TRIGGER [CRM].[LeadStatusTrackerTrigger] ON [CRM].[Leads]
FOR INSERT, UPDATE
AS
BEGIN


	IF EXISTS
	(SELECT * FROM INSERTED 	) AND  EXISTS(SELECT * FROM DELETED	)
	BEGIN
		IF EXISTS
		(
			SELECT 1
			FROM INSERTED AS i
				 INNER JOIN
				 Deleted AS d
				 ON d.leadid = i.leadid
			WHERE i.LeadStatusId <> d.LeadStatusId OR i.DispositionId <> d.DispositionId
		)
		BEGIN
			INSERT INTO CRM.LeadStatusTracker( LeadId, FromLeadStatusId, ToLeadStatusId,DispositionId, Createdon, CreatedBy )

				   SELECT i.Leadid, d.LeadStatusId,i.LeadStatusId,i.DispositionId, GETUTCDATE(), i.[LastUpdatedByPersonId]
				   FROM inserted as i , Deleted as d ;
		END;
	END;
	ELSE
	BEGIN
		INSERT INTO CRM.LeadStatusTracker( [LeadId], FromLeadStatusId,[ToLeadStatusId], [DispositionId],[Createdon], [CreatedBy] )

		SELECT Leadid,LeadStatusId, LeadStatusId,DispositionId, GETUTCDATE(), [CreatedByPersonId]
		FROM inserted;
	END;
END;

 


