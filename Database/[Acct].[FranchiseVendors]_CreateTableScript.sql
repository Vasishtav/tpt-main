
if exists (select * from dbo.sysobjects where id = object_id(N'Acct.FranchiseVendors') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE Acct.FranchiseVendors  
GO   


/****** Object:  Table [Acct].[FranchiseVendors]    Script Date: 15-Dec-17 1:42:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [Acct].[FranchiseVendors](
	[VendorIdPk] [int] IDENTITY(1,1) NOT NULL,
	[VendorId] [int] NOT NULL,
	[Name] [varchar](256) NULL,
	[AccountRep] [varchar](256) NULL,
	[AccountRepPhone] [varchar](15) NULL,
	[AccountRepEmail] [varchar](256) NULL,
	[AccountNo] [varchar](50) NULL,
	[Terms] [varchar](256) NULL,
	[CreditLimit] [varchar](50) NULL,
	[OrderingMethod] [varchar](50) NULL,
	[ContactDetails] [varchar](256) NULL,
	[AddressId] [int] NULL,
	[IsActive] [bit] NULL,
	[VendorType] [int] NOT NULL,
	[FranchiseId] [int] NOT NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastUpdatedOn] [datetime2](7) NULL,
	[LastUpdatedBy] [int] NULL,
 CONSTRAINT [PK_FranchiseVendors] PRIMARY KEY CLUSTERED 
(
	[VendorIdPk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [Acct].[FranchiseVendors]  WITH CHECK ADD  CONSTRAINT [FK_FranchiseVendors_Address] FOREIGN KEY([AddressId])
REFERENCES [CRM].[Addresses] ([AddressId])
GO

ALTER TABLE [Acct].[FranchiseVendors] CHECK CONSTRAINT [FK_FranchiseVendors_Address]
GO

alter table [Acct].[FranchiseVendors] drop column IsActive
Go

alter table [Acct].[FranchiseVendors] add VendorStatus int null
Go

alter table [Acct].[FranchiseVendors] add Currency int null
Go

alter table [Acct].[FranchiseVendors] add RetailBasis varchar(20) null
Go



