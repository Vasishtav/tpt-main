USE [MyCrmTP03]
GO
/****** Object:  StoredProcedure [CRM].[LeadSearch]    Script Date: 08/01/2018 5:55:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [CRM].[LeadSearch]
(
@FranchiseId int
)
as
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

 SELECT 
     lead.[LeadId]
    ,lead.[LeadGuid]
    ,lead.[PersonId]
    ,lead.[LeadNumber]
    ,[LastUpdatedOnUtc]
    ,lead.[Notes]
    ,lead.[CreatedOnUtc]
    ,[LeadStatusID]
    ,[Address1]
    ,[Address2]
    ,[City]
    ,[State]
    ,[ZipCode]
    ,[AddressesGuid]
    ,a.[AddressId]
    ,p.[WorkPhone]
    ,p.[HomePhone]
    ,p.[CellPhone]
    ,p.[FirstName]
    ,p.[LastName]
    ,p.[PrimaryEmail]
    ,p.PreferredTFN
    ,p.WorkPhoneExt
    ,p.CompanyName
    ,p.[FaxPhone]
    ,lead.SecPersonId
    ,tls.Name AS Status
    ,c.Name AS Campaign
    ,case when c.CampaignId is not null and c.CampaignId>0 then  Campaignchannel.Name + ' - '+ Campaignsources.Name else sourcechannel.Name + ' - '+ sources.Name end AS Source
    ,Campaignchannel.Name AS Channel
 FROM crm.Leads lead 
 INNER JOIN CRM.Type_LeadStatus tls ON tls.id = lead.leadstatusid
 INNER JOIN [CRM].[Addresses] a on a.[AddressId]=(select top 1 la.AddressId from crm.LeadAddresses la where la.LeadId = lead.LeadId)
 INNER JOIN [CRM].[Customer] p on p.CustomerId=lead.[PersonId]
 LEFT JOIN [CRM].[Customer] secondPerson on secondPerson.CustomerId = lead.SecPersonId
 -- Based on Campaign
 left join crm.campaign c on c.CampaignId = lead.campaignid
 left join crm.sourcestp stp on stp.SourcesTPId = c.SourcesTPId
 left join [CRM].[Sources] Campaignsources on  stp.SourceId=Campaignsources.SourceId
 left join crm.type_channel Campaignchannel on Campaignchannel.ChannelId = stp.ChannelId
 --Based on source
 left join [CRM].[LeadSources] lsource on lead.LeadId=lsource.LeadId and lsource.IsPrimarySource=1
 left join [CRM].[SourcesTP] stpSource on lsource.SourceId=stpSource.SourcesTPId
 left join [CRM].[Sources] sources on  stpSource.SourceId=sources.SourceId
 left join crm.type_channel sourcechannel on sourcechannel.ChannelId = stpSource.ChannelId

 WHERE lead.LeadStatusId<>31422 AND lead.FranchiseId= @FranchiseId AND ISNULL(lead.IsDeleted,0)=0;