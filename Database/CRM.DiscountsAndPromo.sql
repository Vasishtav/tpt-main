if exists (select * from dbo.sysobjects where id = object_id(N'[CRM].[DiscountsAndPromo]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [CRM].[DiscountsAndPromo]  
GO   
CREATE TABLE [CRM].[DiscountsAndPromo] ( 
Id int  Not NULL identity(1,1) ,
Code varchar(100)  NULL,
Description varchar(500)  NULL,
Retail varchar(100)  NULL,
Surcharge numeric(18, 2)  NULL,
Discount numeric(18, 2)  NULL,
Misc varchar(200)  NULL,
CreatedOn datetime2(7)  not NULL,
CreatedBy int  not NULL,
LastUpdatedOn datetime2(7)  NULL,
LastUpdatedBy int  NULL,
Expiry Datetime  NULL,
QuoteLineDetailId int  NULL,
DiscountValue Numeric(18,2)  NULL,
Cost Numeric(18,2)  NULL,
CostWOPromo Numeric(18,2)  NULL,
PriceWOVendorPromo Numeric(18,2)  NULL,
PriceWithVendorPromo Numeric(18,2)  NULL,  
  CONSTRAINT PK_DiscountsAndPromo PRIMARY KEY (ID) 
 )

ALTER TABLE [CRM].[DiscountsAndPromo]  WITH CHECK ADD  CONSTRAINT [FK_DiscountsAndPromo_Person_Createdby] FOREIGN KEY([CreatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO

ALTER TABLE [CRM].[DiscountsAndPromo] CHECK CONSTRAINT [FK_DiscountsAndPromo_Person_Createdby]
GO

ALTER TABLE [CRM].[DiscountsAndPromo]  WITH CHECK ADD  CONSTRAINT [FK_DiscountsAndPromo_Person_LastUpdatedBy] FOREIGN KEY([LastUpdatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO

ALTER TABLE [CRM].[DiscountsAndPromo] CHECK CONSTRAINT [FK_DiscountsAndPromo_Person_LastUpdatedBy]
GO

ALTER TABLE [CRM].[DiscountsAndPromo]  WITH CHECK ADD  CONSTRAINT [FK_DiscountsAndPromo_QuoteLineDetail_QuoteLineDetailId] FOREIGN KEY(QuoteLineDetailId)
REFERENCES [CRM].QuoteLineDetail (QuoteLineDetailId)
GO

ALTER TABLE [CRM].[DiscountsAndPromo] CHECK CONSTRAINT [FK_DiscountsAndPromo_QuoteLineDetail_QuoteLineDetailId]
GO
