USE [MyCrmTP03]
GO
/****** Object:  Trigger [CRM].[OpportunityStatusTrackerTrigger]    Script Date: 21-Dec-17 7:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [CRM].[OpportunityStatusTrackerTrigger] ON [CRM].[Opportunities]
FOR INSERT, UPDATE
AS
     BEGIN
        IF EXISTS (SELECT * FROM INSERTED) AND  EXISTS(SELECT * FROM DELETED)
		BEGIN
		IF EXISTS
		(
			SELECT 1 FROM INSERTED AS i
			INNER JOIN Deleted AS d
				 ON d.OpportunityId = i.OpportunityId
			WHERE i.OpportunityId <> d.OpportunityId OR i.DispositionId <> d.DispositionId
		)
             BEGIN
                 INSERT INTO CRM.OpportunityStatusTracker
                 ([OpportunityId],[FromOpportunityStatusId],[ToOpportunityStatusId],DispositionId,
                  [CreatedOn],[CreatedBy]
                 )
                   SELECT i.[OpportunityId],d.[OpportunityStatusId],i.[OpportunityStatusId],i.DispositionId,
                               GETUTCDATE(),i.LastUpdatedByPersonId
                        FROM inserted i, Deleted as d ;
         END END
             ELSE
             BEGIN
                 INSERT INTO CRM.OpportunityStatusTracker
                  ([OpportunityId],[FromOpportunityStatusId],[ToOpportunityStatusId],DispositionId,
                  [Createdon],[CreatedBy]
                 )
                        SELECT [OpportunityId],
                               [OpportunityStatusId],
							   [OpportunityStatusId],
							   DispositionId,
                               GETUTCDATE(),
                               CreatedByPersonId
                        FROM inserted;
         END;
     END;

 


