USE [MyCrmTP03]
GO

/****** Object:  Table [CRM].[Invoices]    Script Date: 07-Dec-17 6:12:43 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [CRM].[Invoices](
	[InvoiceId] [int] IDENTITY(1,1) NOT NULL,
	[OrderID] [int] NOT NULL,
	[InvoiceType] [varchar](100) NOT NULL,
	[InvoiceDate] [datetime2](7) NULL,
	[TotalAmount] [numeric](18, 2) NULL,
	[Balance] [numeric](18, 2) NULL,
	[Streamid] [uniqueidentifier] NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastUpdatedOn] [datetime2](7) NULL,
	[LastUpdatedBy] [int] NULL,
 CONSTRAINT [PK_Invoices] PRIMARY KEY CLUSTERED 
(
	[InvoiceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [CRM].[Invoices]  WITH CHECK ADD  CONSTRAINT [FK_Invoices_Createdby] FOREIGN KEY([CreatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO

ALTER TABLE [CRM].[Invoices] CHECK CONSTRAINT [FK_Invoices_Createdby]
GO

ALTER TABLE [CRM].[Invoices]  WITH CHECK ADD  CONSTRAINT [FK_Invoices_Modifiedby] FOREIGN KEY([LastUpdatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO

ALTER TABLE [CRM].[Invoices] CHECK CONSTRAINT [FK_Invoices_Modifiedby]
GO

ALTER TABLE [CRM].[Invoices]  WITH CHECK ADD  CONSTRAINT [FK_Invoices_Orders] FOREIGN KEY([OrderID])
REFERENCES [CRM].[Orders] ([OrderID])
GO

ALTER TABLE [CRM].[Invoices] CHECK CONSTRAINT [FK_Invoices_Orders]
GO


