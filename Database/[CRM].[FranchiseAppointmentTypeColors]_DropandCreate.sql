USE [MyCrmTP03]
GO

ALTER TABLE [CRM].[FranchiseAppointmentTypeColors] DROP CONSTRAINT [FK_CRM.FranchiseAppointmentTypeColors_Modifiedby]
GO

ALTER TABLE [CRM].[FranchiseAppointmentTypeColors] DROP CONSTRAINT [FK_CRM.FranchiseAppointmentTypeColors_Createdby]
GO

/****** Object:  Table [CRM].[FranchiseAppointmentTypeColors]    Script Date: 1/16/2018 11:26:38 AM ******/
DROP TABLE [CRM].[FranchiseAppointmentTypeColors]
GO

/****** Object:  Table [CRM].[FranchiseAppointmentTypeColors]    Script Date: 1/16/2018 11:26:38 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [CRM].[FranchiseAppointmentTypeColors](
	[FranchiseAppointmentTypeColorId] [int] IDENTITY(1,1) NOT NULL,
	[FranchiseId] [int] NULL,
	[Appointment1] [varchar](50) NULL,
	[Appointment2] [varchar](50) NULL,
	[Appointment3] [varchar](50) NULL,
	[Appointment4] [varchar](50) NULL,
	[Sales_Design] [varchar](50) NULL,
	[Installation] [varchar](50) NULL,
	[DayOff] [varchar](50) NULL,
	[Other] [varchar](50) NULL,
	[Personal] [varchar](50) NULL,
	[Meeting_Training] [varchar](50) NULL,
	[Service] [varchar](50) NULL,
	[Vacation] [varchar](50) NULL,
	[Holiday] [varchar](50) NULL,
	[Followup] [varchar](50) NULL,
	[TimeBlock] [varchar](50) NULL,
	[EnableBorders] [bit] NOT NULL CONSTRAINT [DF_FranchiseAppointmentTypeColors_EnableBorders]  DEFAULT ((0)),
	[Enable] [bit] NOT NULL,
	[CreatedOn] [datetime2](7) NULL,
	[CreatedBy] [int] NULL,
	[LastUpdatedOn] [datetime2](7) NULL,
	[LastUpdatedBy] [int] NULL,
 CONSTRAINT [PK_FranchiseAppointmentTypeColors] PRIMARY KEY CLUSTERED 
(
	[FranchiseAppointmentTypeColorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [CRM].[FranchiseAppointmentTypeColors]  WITH CHECK ADD  CONSTRAINT [FK_CRM.FranchiseAppointmentTypeColors_Createdby] FOREIGN KEY([CreatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO

ALTER TABLE [CRM].[FranchiseAppointmentTypeColors] CHECK CONSTRAINT [FK_CRM.FranchiseAppointmentTypeColors_Createdby]
GO

ALTER TABLE [CRM].[FranchiseAppointmentTypeColors]  WITH CHECK ADD  CONSTRAINT [FK_CRM.FranchiseAppointmentTypeColors_Modifiedby] FOREIGN KEY([LastUpdatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO

ALTER TABLE [CRM].[FranchiseAppointmentTypeColors] CHECK CONSTRAINT [FK_CRM.FranchiseAppointmentTypeColors_Modifiedby]
GO


