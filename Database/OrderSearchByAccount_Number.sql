USE [TPT]
GO

/****** Object:  StoredProcedure [CRM].[OrderSearchByAccoutId]    Script Date: 04/10/2018 3:58:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO


Alter PROC [CRM].[OrderSearchByAccoutId]
(
@FranchiseId int,
@AccountId int
)
as
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
select OrderId, OrderName,OrderNumber, ac.AccountId as AccountId, 
cu.FirstName+''+cu.LastName + case when cu.CompanyName is not null and cu.CompanyName!='' then ' / '+
cu.CompanyName else '' end  as AccountName, 
op.OpportunityId as OpportunityId, op.OpportunityName,
os.OrderStatus as [Status], p.FirstName+' '+p.LastName as SalesAgentName,
OrderTotal as TotalAmount,
ContractedDate as ContractDate,o.CreatedOn as CreatedDate,o.LastUpdatedOn  as ModifiedDate 
from [CRM].[Orders] o
 join [CRM].[Opportunities] op on o.OpportunityId=op.OpportunityId
 left join [CRM].[Person] p on op.SalesAgentId=p.PersonId
 left join [CRM].[Accounts] ac on op.AccountId=ac.AccountId
 left join [CRM].[Customer] cu on ac.PersonId=cu.CustomerId
 left join [CRM].[Type_OrderStatus] os on o.OrderStatus=os.OrderStatusId
 where 
 op.FranchiseId=@FranchiseId and ac.AccountId=@AccountId and
 o.OrderStatus!=5 and o.OrderStatus!=6 
GO


