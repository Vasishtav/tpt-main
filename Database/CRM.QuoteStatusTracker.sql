USE [MyCrmTP03]
GO

Drop table [CRM].[QuoteStatusTracker]
GO

/****** Object:  Table [CRM].[QuoteStatusTracker]    Script Date: 21-Dec-17 6:59:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [CRM].[QuoteStatusTracker](
	[QuoteStatusTrackerId] [int] IDENTITY(1,1) NOT NULL,
	[QuoteKey] [int] NOT NULL,
	[FromQuoteStatusId] [int] NOT NULL,
	[ToQuoteStatusId] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NULL
) ON [PRIMARY]

GO

ALTER TABLE [CRM].[QuoteStatusTracker]  WITH CHECK ADD  CONSTRAINT [FK_Quote_QuoteStatusTracker] FOREIGN KEY([QuoteKey])
REFERENCES [CRM].[Quote] ([QuoteKey])
GO

ALTER TABLE [CRM].[QuoteStatusTracker] CHECK CONSTRAINT [FK_Quote_QuoteStatusTracker]
GO

ALTER TABLE [CRM].[QuoteStatusTracker]  WITH CHECK ADD  CONSTRAINT [FK_FromQuoteStatusTracker_Type_QuoteStatus] FOREIGN KEY([FromQuoteStatusId])
REFERENCES [CRM].[Type_QuoteStatus] ([QuoteStatusId])
GO

ALTER TABLE [CRM].[QuoteStatusTracker] CHECK CONSTRAINT [FK_FromQuoteStatusTracker_Type_QuoteStatus]
GO

ALTER TABLE [CRM].[QuoteStatusTracker]  WITH CHECK ADD  CONSTRAINT [FK_ToQuoteStatusTracker_Type_QuoteStatus] FOREIGN KEY([ToQuoteStatusId])
REFERENCES [CRM].[Type_QuoteStatus] ([QuoteStatusId])
GO

ALTER TABLE [CRM].[QuoteStatusTracker] CHECK CONSTRAINT [FK_ToQuoteStatusTracker_Type_QuoteStatus]
GO




