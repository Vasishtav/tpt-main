USE [MyCrmTP03]
GO

EXEC sp_rename 'CRM.FranchiseProducts', 'FranchiseProducts_old'
GO

EXEC sp_rename 'CRM.Orders', 'Orders_old'
GO

EXEC sp_rename 'CRM.Type_OrderStatus', 'Type_OrderStatus_old'
GO

/****** Object:  Table [Acct].[FranchiseVendors]    Script Date: 07-Dec-17 6:27:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Acct].[FranchiseVendors](
	[VendorIdPk] [int] IDENTITY(1,1) NOT NULL,
	[VendorId] [int] NOT NULL,
	[Name] [varchar](256) NULL,
	[AccountRep] [varchar](256) NULL,
	[AccountRepPhone] [varchar](15) NULL,
	[AccountRepEmail] [varchar](256) NULL,
	[AccountNo] [varchar](50) NULL,
	[Terms] [varchar](256) NULL,
	[CreditLimit] [varchar](50) NULL,
	[OrderingMethod] [varchar](50) NULL,
	[ContactDetails] [varchar](256) NULL,
	[CreatedOn] [datetimeoffset](2) NOT NULL CONSTRAINT [DF_FranchiseVendors_CreatedOnUtc]  DEFAULT (getdate()),
	[AddressId] [int] NULL,
	[IsActive] [bit] NULL,
	[VendorType] [tinyint] NOT NULL,
 CONSTRAINT [PK_FranchiseVendors] PRIMARY KEY CLUSTERED 
(
	[VendorIdPk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Acct].[HFCVendors]    Script Date: 07-Dec-17 6:27:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Acct].[HFCVendors](
	[VendorIdPk] [int] IDENTITY(1,1) NOT NULL,
	[VendorId] [int] NOT NULL,
	[IsAllianceVendor] [bit] NOT NULL CONSTRAINT [DF_HFCVendors_IsAllianceVendor]  DEFAULT ((0)),
	[Name] [varchar](256) NOT NULL,
	[AccountRep] [varchar](256) NULL,
	[AccountRepPhone] [varchar](15) NULL,
	[AccountRepEmail] [varchar](256) NULL,
	[OrderingMethod] [varchar](50) NULL,
	[ContactDetails] [varchar](256) NULL,
	[CreatedOn] [datetimeoffset](2) NOT NULL CONSTRAINT [DF_HFCVendors_CreatedOnUtc]  DEFAULT (getdate()),
	[AddressId] [int] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_HFCVendors] PRIMARY KEY CLUSTERED 
(
	[VendorIdPk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CRM].[FranchiseProducts]    Script Date: 07-Dec-17 6:27:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CRM].[FranchiseProducts](
	[ProductKey] [int] IDENTITY(1,1) NOT NULL,
	[ProductID] [int] NOT NULL,
	[ProductName] [varchar](100) NULL,
	[PICProductID] [int] NULL,
	[VendorID] [int] NULL,
	[VendorName] [varchar](150) NULL,
	[VendorProductSKU] [varchar](100) NULL,
	[Description] [varchar](30) NULL,
	[ProductCategory] [int] NULL,
	[Cost] [numeric](18, 2) NULL,
	[SalePrice] [numeric](18, 2) NULL,
	[ProductStatus] [int] NULL,
	[ProductSubCategory] [int] NULL,
	[MarkUp] [varchar](100) NULL,
	[MarkupType] [varchar](100) NULL,
	[Discount] [numeric](18, 2) NULL,
	[DiscountType] [varchar](30) NULL,
	[CalculatedSalesPrice] [numeric](18, 2) NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastUpdatedOn] [datetime2](7) NULL,
	[LastUpdatedBy] [int] NULL,
	[FranchiseId] [int] NOT NULL CONSTRAINT [DF__Franchise__Franc__125BED31]  DEFAULT ((0)),
 CONSTRAINT [PK__Franchis__B40CC6ED75928A1A] PRIMARY KEY CLUSTERED 
(
	[ProductKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CRM].[GetNewNumber]    Script Date: 07-Dec-17 6:27:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [CRM].[GetNewNumber](
	[VendorNumber] [int] NULL,
	[ProductNumber] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [CRM].[HFCProducts]    Script Date: 07-Dec-17 6:27:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CRM].[HFCProducts](
	[ProductKey] [int] IDENTITY(1,1) NOT NULL,
	[ProductID] [int] NOT NULL,
	[ProductName] [varchar](100) NULL,
	[PICProductID] [int] NULL,
	[VendorID] [int] NULL,
	[VendorName] [varchar](150) NULL,
	[VendorProductSKU] [varchar](100) NULL,
	[Description] [varchar](30) NULL,
	[ProductCategory] [int] NULL,
	[ProductCollection] [varchar](100) NULL,
	[ProductModelID] [varchar](100) NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastUpdatedOn] [datetime2](7) NULL,
	[LastUpdatedBy] [int] NULL,
 CONSTRAINT [PK_HFCProducts] PRIMARY KEY CLUSTERED 
(
	[ProductKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CRM].[HFCProductStatus]    Script Date: 07-Dec-17 6:27:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [CRM].[HFCProductStatus](
	[ProductKey] [int] NOT NULL,
	[FranchiseId] [int] NOT NULL,
	[ProductStatusId] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [CRM].[Invoices]    Script Date: 07-Dec-17 6:27:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CRM].[Invoices](
	[InvoiceId] [int] IDENTITY(1,1) NOT NULL,
	[OrderID] [int] NOT NULL,
	[InvoiceType] [varchar](100) NOT NULL,
	[InvoiceDate] [datetime2](7) NULL,
	[TotalAmount] [numeric](18, 2) NULL,
	[Balance] [numeric](18, 2) NULL,
	[Streamid] [uniqueidentifier] NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastUpdatedOn] [datetime2](7) NULL,
	[LastUpdatedBy] [int] NULL,
 CONSTRAINT [PK_Invoices] PRIMARY KEY CLUSTERED 
(
	[InvoiceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CRM].[OrderLines]    Script Date: 07-Dec-17 6:27:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [CRM].[OrderLines](
	[OrderLineID] [int] IDENTITY(1,1) NOT NULL,
	[QuoteKey] [int] NULL,
	[QuoteLineId] [int] NULL,
	[CreatedOn] [datetime2](7) NULL,
	[CreatedBy] [int] NULL,
	[LastUpdatedOn] [datetime2](7) NULL,
	[LastUpdatedBy] [int] NULL,
 CONSTRAINT [PK_OrderLines] PRIMARY KEY CLUSTERED 
(
	[OrderLineID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [CRM].[Orders]    Script Date: 07-Dec-17 6:27:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CRM].[Orders](
	[OrderID] [int] IDENTITY(1,1) NOT NULL,
	[OpportunityId] [int] NOT NULL,
	[QuoteKey] [int] NOT NULL,
	[OrderName] [varchar](100) NOT NULL,
	[OrderStatus] [int] NULL,
	[ContractedDate] [datetime2](7) NULL,
	[ProductSubtotal] [numeric](18, 2) NULL,
	[AdditionalCharges] [numeric](18, 2) NULL,
	[OrderDiscount] [numeric](18, 2) NULL,
	[OrderSubtotal] [numeric](18, 2) NULL,
	[Tax] [numeric](18, 2) NULL,
	[OrderTotal] [numeric](18, 2) NULL,
	[BalanceDue] [numeric](18, 2) NULL,
	[CreatedOn] [datetime2](7) NULL,
	[CreatedBy] [int] NULL,
	[LastUpdatedOn] [datetime2](7) NULL,
	[LastUpdatedBy] [int] NULL,
 CONSTRAINT [PK__Orders__C3905BAFC38F435F] PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CRM].[Payments]    Script Date: 07-Dec-17 6:27:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [CRM].[Payments](
	[PaymentID] [int] IDENTITY(1,1) NOT NULL,
	[OpportunityID] [int] NOT NULL,
	[OrderID] [int] NOT NULL,
	[Sidemark] [varchar](100) NULL,
	[Total] [numeric](18, 2) NULL,
	[BalanceDue] [numeric](18, 2) NULL,
	[PayBalance] [bit] NULL,
	[PaymentMethod] [int] NULL,
	[VerificationCheck] [varchar](200) NULL,
	[PaymentDate] [datetime2](7) NULL,
	[Amount] [numeric](18, 2) NULL,
	[Memo] [varchar](500) NULL,
	[CreatedOn] [datetime2](7) NULL,
	[CreatedBy] [int] NULL,
	[LastUpdatedOn] [datetime2](7) NULL,
	[LastUpdatedBy] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[PaymentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CRM].[Quote]    Script Date: 07-Dec-17 6:27:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CRM].[Quote](
	[QuoteKey] [int] IDENTITY(1,1) NOT NULL,
	[QuoteID] [int] NOT NULL,
	[OpportunityId] [int] NOT NULL,
	[QuoteName] [varchar](100) NOT NULL,
	[PrimaryQuote] [bit] NULL,
	[QuoteStatusId] [int] NOT NULL,
	[TotalCharges] [numeric](18, 2) NULL,
	[Discount] [numeric](18, 2) NULL,
	[DiscountType] [varchar](10) NULL,
	[TotalDiscounts] [numeric](18, 2) NULL,
	[NetCharges] [numeric](18, 2) NULL,
	[CreatedOn] [datetimeoffset](2) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastUpdatedOn] [datetimeoffset](2) NULL,
	[LastUpdatedBy] [int] NULL,
	[MeasurementsetId] [varchar](100) NULL,
 CONSTRAINT [PK_CRM.Quote] PRIMARY KEY CLUSTERED 
(
	[QuoteKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CRM].[QuoteLines]    Script Date: 07-Dec-17 6:27:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [CRM].[QuoteLines](
	[QuoteLineId] [int] IDENTITY(1,1) NOT NULL,
	[QuoteKey] [int] NOT NULL,
	[MeasurementsetId] [int] NULL,
	[VendorId] [int] NULL,
	[ProductId] [int] NULL,
	[SuggestedResale] [numeric](18, 2) NULL,
	[Discount] [numeric](18, 2) NULL,
	[CalculatedSalePrice] [numeric](18, 2) NULL,
	[CreatedOn] [datetimeoffset](2) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastUpdatedOn] [datetimeoffset](2) NULL,
	[LastUpdatedBy] [int] NULL,
 CONSTRAINT [PK_QuoteLines] PRIMARY KEY CLUSTERED 
(
	[QuoteLineId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [CRM].[Type_OpportunityStatus]    Script Date: 07-Dec-17 6:27:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CRM].[Type_OpportunityStatus](
	[OpportunityStatusId] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Description] [varchar](250) NULL,
	[BrandId] [tinyint] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_Type_OpportunityStatus] PRIMARY KEY CLUSTERED 
(
	[OpportunityStatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CRM].[Type_OrderStatus]    Script Date: 07-Dec-17 6:27:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CRM].[Type_OrderStatus](
	[OrderStatusId] [int] IDENTITY(1,1) NOT NULL,
	[OrderStatus] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastUpdatedOn] [datetime2](7) NULL,
	[LastUpdatedBy] [int] NULL,
 CONSTRAINT [PK_TypeOrderStatus] PRIMARY KEY CLUSTERED 
(
	[OrderStatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CRM].[Type_PaymentMethod]    Script Date: 07-Dec-17 6:27:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CRM].[Type_PaymentMethod](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PaymentMethod] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastUpdatedOn] [datetime2](7) NULL,
	[LastUpdatedBy] [int] NULL,
 CONSTRAINT [PK_Type_PaymentMethod] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CRM].[Type_ProductCategory]    Script Date: 07-Dec-17 6:27:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CRM].[Type_ProductCategory](
	[ProductCategoryId] [int] IDENTITY(1,1) NOT NULL,
	[ProductCategory] [varchar](100) NOT NULL,
	[Parant] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NULL,
	[CreatedOn] [datetimeoffset](2) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastUpdatedOn] [datetimeoffset](2) NULL,
	[LastUpdatedBy] [int] NULL,
 CONSTRAINT [PK_Type_ProductCategory] PRIMARY KEY CLUSTERED 
(
	[ProductCategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CRM].[Type_ProductStatus]    Script Date: 07-Dec-17 6:27:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CRM].[Type_ProductStatus](
	[ProductStatusId] [int] IDENTITY(1,1) NOT NULL,
	[ProductStatus] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NULL,
	[CreatedOn] [datetimeoffset](2) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastUpdatedOn] [datetimeoffset](2) NULL,
	[LastUpdatedBy] [int] NULL,
 CONSTRAINT [PK_Type_ProductStatus] PRIMARY KEY CLUSTERED 
(
	[ProductStatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CRM].[Type_QuoteStatus]    Script Date: 07-Dec-17 6:27:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CRM].[Type_QuoteStatus](
	[QuoteStatusId] [int] IDENTITY(1,1) NOT NULL,
	[QuoteStatus] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NULL,
	[CreatedOn] [datetimeoffset](2) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastUpdatedOn] [datetimeoffset](2) NULL,
	[LastUpdatedBy] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

/****** Object:  Table [CRM].[Type_VendorName]    Script Date: 07-Dec-17 8:19:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CRM].[Type_VendorName](
	[VendorNameId] [int] IDENTITY(1,1) NOT NULL,
	[VendorName] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NULL,
	[CreatedOn] [datetimeoffset](2) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastUpdatedOn] [datetimeoffset](2) NULL,
	[LastUpdatedBy] [int] NULL,
 CONSTRAINT [PK_Type_VendorName] PRIMARY KEY CLUSTERED 
(
	[VendorNameId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

SET IDENTITY_INSERT [Acct].[FranchiseVendors] ON 

INSERT [Acct].[FranchiseVendors] ([VendorIdPk], [VendorId], [Name], [AccountRep], [AccountRepPhone], [AccountRepEmail], [AccountNo], [Terms], [CreditLimit], [OrderingMethod], [ContactDetails], [CreatedOn], [AddressId], [IsActive], [VendorType]) VALUES (1, 5, NULL, NULL, NULL, NULL, N'65Test', N'2', N'600', NULL, NULL, CAST(N'0001-01-01T00:00:00.0000000+00:00' AS DateTimeOffset), NULL, 1, 1)
INSERT [Acct].[FranchiseVendors] ([VendorIdPk], [VendorId], [Name], [AccountRep], [AccountRepPhone], [AccountRepEmail], [AccountNo], [Terms], [CreditLimit], [OrderingMethod], [ContactDetails], [CreatedOn], [AddressId], [IsActive], [VendorType]) VALUES (4, 6, NULL, N'Test', N'9865321254', N'Test@gmail.com', N'Test', N'Test', N'6000', N'1', N'Test', CAST(N'2017-11-10T11:24:54.1200000+05:30' AS DateTimeOffset), 4817106, 1, 2)
INSERT [Acct].[FranchiseVendors] ([VendorIdPk], [VendorId], [Name], [AccountRep], [AccountRepPhone], [AccountRepEmail], [AccountNo], [Terms], [CreditLimit], [OrderingMethod], [ContactDetails], [CreatedOn], [AddressId], [IsActive], [VendorType]) VALUES (8, 7, NULL, NULL, NULL, NULL, N'b56', N'1', N'1', NULL, NULL, CAST(N'0001-01-01T00:00:00.0000000+00:00' AS DateTimeOffset), NULL, 1, 1)
INSERT [Acct].[FranchiseVendors] ([VendorIdPk], [VendorId], [Name], [AccountRep], [AccountRepPhone], [AccountRepEmail], [AccountNo], [Terms], [CreditLimit], [OrderingMethod], [ContactDetails], [CreatedOn], [AddressId], [IsActive], [VendorType]) VALUES (9, 8, N'Local Vendor', N'local vendor', N'8965322154', N'local@gmail.com', N'A12', N'2', N'36', N'3', N'7854120369', CAST(N'2017-11-14T18:19:15.0200000+05:30' AS DateTimeOffset), 4819133, 1, 3)
INSERT [Acct].[FranchiseVendors] ([VendorIdPk], [VendorId], [Name], [AccountRep], [AccountRepPhone], [AccountRepEmail], [AccountNo], [Terms], [CreditLimit], [OrderingMethod], [ContactDetails], [CreatedOn], [AddressId], [IsActive], [VendorType]) VALUES (12, 9, NULL, N'nonallience xy', N'3265568998', N'nonalli@gmail.com', N'23', N'5', N'2', NULL, N'9865325487', CAST(N'0001-01-01T00:00:00.0000000+00:00' AS DateTimeOffset), 4817105, 1, 2)
INSERT [Acct].[FranchiseVendors] ([VendorIdPk], [VendorId], [Name], [AccountRep], [AccountRepPhone], [AccountRepEmail], [AccountNo], [Terms], [CreditLimit], [OrderingMethod], [ContactDetails], [CreatedOn], [AddressId], [IsActive], [VendorType]) VALUES (13, 10, NULL, NULL, NULL, NULL, N'A6', N'1', N'2', NULL, NULL, CAST(N'0001-01-01T00:00:00.0000000+00:00' AS DateTimeOffset), NULL, 1, 1)
INSERT [Acct].[FranchiseVendors] ([VendorIdPk], [VendorId], [Name], [AccountRep], [AccountRepPhone], [AccountRepEmail], [AccountNo], [Terms], [CreditLimit], [OrderingMethod], [ContactDetails], [CreatedOn], [AddressId], [IsActive], [VendorType]) VALUES (14, 11, NULL, N'nonallience', N'9865215487', N'test@gmail.com', N'12ab', N'3', N'15', N'3', N'www.test.com', CAST(N'0001-01-01T00:00:00.0000000+00:00' AS DateTimeOffset), 4819141, 1, 2)
INSERT [Acct].[FranchiseVendors] ([VendorIdPk], [VendorId], [Name], [AccountRep], [AccountRepPhone], [AccountRepEmail], [AccountNo], [Terms], [CreditLimit], [OrderingMethod], [ContactDetails], [CreatedOn], [AddressId], [IsActive], [VendorType]) VALUES (15, 12, N'Test', N'Test', N'9865326598', N'Test@gmail.com', N'8965326598', N'2', N'3', N'2', N'9865548745', CAST(N'2017-11-16T16:57:34.2800000+05:30' AS DateTimeOffset), 4819161, 1, 3)
INSERT [Acct].[FranchiseVendors] ([VendorIdPk], [VendorId], [Name], [AccountRep], [AccountRepPhone], [AccountRepEmail], [AccountNo], [Terms], [CreditLimit], [OrderingMethod], [ContactDetails], [CreatedOn], [AddressId], [IsActive], [VendorType]) VALUES (17, 13, N'test', N'Test', N'9865326598', N'Test@gmail.com', N'12345', N'1', N'5', N'3', N'Test', CAST(N'2017-11-21T18:44:28.9500000+05:30' AS DateTimeOffset), 4819251, 1, 3)
SET IDENTITY_INSERT [Acct].[FranchiseVendors] OFF
SET IDENTITY_INSERT [Acct].[HFCVendors] ON 

INSERT [Acct].[HFCVendors] ([VendorIdPk], [VendorId], [IsAllianceVendor], [Name], [AccountRep], [AccountRepPhone], [AccountRepEmail], [OrderingMethod], [ContactDetails], [CreatedOn], [AddressId], [IsActive]) VALUES (1, 1, 1, N'Springs', N'Representive Xy', N'9898545712', N'xyz@email.com', N'3', N'8965325687', CAST(N'2017-11-09T17:37:20.1300000+00:00' AS DateTimeOffset), 4817106, 1)
INSERT [Acct].[HFCVendors] ([VendorIdPk], [VendorId], [IsAllianceVendor], [Name], [AccountRep], [AccountRepPhone], [AccountRepEmail], [OrderingMethod], [ContactDetails], [CreatedOn], [AddressId], [IsActive]) VALUES (2, 2, 0, N'Vendor Abc', NULL, NULL, NULL, NULL, NULL, CAST(N'2017-11-09T17:44:29.2600000+00:00' AS DateTimeOffset), NULL, 1)
INSERT [Acct].[HFCVendors] ([VendorIdPk], [VendorId], [IsAllianceVendor], [Name], [AccountRep], [AccountRepPhone], [AccountRepEmail], [OrderingMethod], [ContactDetails], [CreatedOn], [AddressId], [IsActive]) VALUES (4, 3, 1, N'Norman', N'Allience', N'8987543265', N'alli@gmail.com', N'3', N'9821547865', CAST(N'2017-11-15T13:50:42.3600000+00:00' AS DateTimeOffset), 4819133, 1)
INSERT [Acct].[HFCVendors] ([VendorIdPk], [VendorId], [IsAllianceVendor], [Name], [AccountRep], [AccountRepPhone], [AccountRepEmail], [OrderingMethod], [ContactDetails], [CreatedOn], [AddressId], [IsActive]) VALUES (5, 4, 0, N'Lowes', NULL, NULL, NULL, NULL, NULL, CAST(N'2017-11-15T16:20:13.8300000+00:00' AS DateTimeOffset), NULL, 1)
SET IDENTITY_INSERT [Acct].[HFCVendors] OFF
SET IDENTITY_INSERT [CRM].[FranchiseProducts] ON 

INSERT [CRM].[FranchiseProducts] ([ProductKey], [ProductID], [ProductName], [PICProductID], [VendorID], [VendorName], [VendorProductSKU], [Description], [ProductCategory], [Cost], [SalePrice], [ProductStatus], [ProductSubCategory], [MarkUp], [MarkupType], [Discount], [DiscountType], [CalculatedSalesPrice], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy], [FranchiseId]) VALUES (1, 3, N'asd', NULL, NULL, N'sad', N'asdasd', N'asdas', 1, CAST(23.00 AS Numeric(18, 2)), CAST(23.00 AS Numeric(18, 2)), 1, 10, NULL, NULL, NULL, NULL, NULL, CAST(N'2017-12-04 19:43:17.7500000' AS DateTime2), 2463912, CAST(N'2017-12-04 19:43:17.7500000' AS DateTime2), 2463912, 10391)
INSERT [CRM].[FranchiseProducts] ([ProductKey], [ProductID], [ProductName], [PICProductID], [VendorID], [VendorName], [VendorProductSKU], [Description], [ProductCategory], [Cost], [SalePrice], [ProductStatus], [ProductSubCategory], [MarkUp], [MarkupType], [Discount], [DiscountType], [CalculatedSalesPrice], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy], [FranchiseId]) VALUES (2, 91, N'modified test', NULL, NULL, N'test', N'12', N'test', 2, CAST(20.00 AS Numeric(18, 2)), CAST(30.00 AS Numeric(18, 2)), 1, 17, NULL, NULL, NULL, NULL, NULL, CAST(N'2017-12-05 17:51:25.3470000' AS DateTime2), 2463913, CAST(N'2017-12-05 17:51:25.3470000' AS DateTime2), 2463913, 10391)
INSERT [CRM].[FranchiseProducts] ([ProductKey], [ProductID], [ProductName], [PICProductID], [VendorID], [VendorName], [VendorProductSKU], [Description], [ProductCategory], [Cost], [SalePrice], [ProductStatus], [ProductSubCategory], [MarkUp], [MarkupType], [Discount], [DiscountType], [CalculatedSalesPrice], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy], [FranchiseId]) VALUES (3, 113, N'test data', NULL, NULL, N'test', N'98', N'Test', 7, CAST(30.00 AS Numeric(18, 2)), CAST(50.00 AS Numeric(18, 2)), 1, 33, NULL, NULL, NULL, NULL, NULL, CAST(N'2017-12-06 06:28:00.1930000' AS DateTime2), 2465016, CAST(N'2017-12-06 06:28:00.1930000' AS DateTime2), 2465016, 10429)
INSERT [CRM].[FranchiseProducts] ([ProductKey], [ProductID], [ProductName], [PICProductID], [VendorID], [VendorName], [VendorProductSKU], [Description], [ProductCategory], [Cost], [SalePrice], [ProductStatus], [ProductSubCategory], [MarkUp], [MarkupType], [Discount], [DiscountType], [CalculatedSalesPrice], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy], [FranchiseId]) VALUES (4, 120, N'test edit data', NULL, NULL, N'test', NULL, N'test', 4, CAST(20.00 AS Numeric(18, 2)), CAST(20.00 AS Numeric(18, 2)), 1, 24, NULL, NULL, NULL, NULL, NULL, CAST(N'2017-12-06 09:31:52.7600000' AS DateTime2), 2464988, CAST(N'2017-12-06 09:31:52.7600000' AS DateTime2), 2464988, 10401)
SET IDENTITY_INSERT [CRM].[FranchiseProducts] OFF
INSERT [CRM].[GetNewNumber] ([VendorNumber], [ProductNumber]) VALUES (47, 158)
SET IDENTITY_INSERT [CRM].[HFCProducts] ON 

INSERT [CRM].[HFCProducts] ([ProductKey], [ProductID], [ProductName], [PICProductID], [VendorID], [VendorName], [VendorProductSKU], [Description], [ProductCategory], [ProductCollection], [ProductModelID], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (1, 1, N'PIC product', 1, 1, N'PIC vendor', N'sku test', N'test descripe PIC', 1, N'test collection', N'test model id', CAST(N'2017-11-30 11:24:54.1200000' AS DateTime2), 2464994, CAST(N'2017-11-30 11:24:54.1200000' AS DateTime2), 2464994)
SET IDENTITY_INSERT [CRM].[HFCProducts] OFF
INSERT [CRM].[HFCProductStatus] ([ProductKey], [FranchiseId], [ProductStatusId]) VALUES (1, 10401, 3)
SET IDENTITY_INSERT [CRM].[Orders] ON 

INSERT [CRM].[Orders] ([OrderID], [OpportunityId], [QuoteKey], [OrderName], [OrderStatus], [ContractedDate], [ProductSubtotal], [AdditionalCharges], [OrderDiscount], [OrderSubtotal], [Tax], [OrderTotal], [BalanceDue], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (1, 1, 1, N'test order', 1, CAST(N'2017-12-10 00:00:00.0000000' AS DateTime2), CAST(100.00 AS Numeric(18, 2)), CAST(10.00 AS Numeric(18, 2)), CAST(10.00 AS Numeric(18, 2)), CAST(10.00 AS Numeric(18, 2)), CAST(10.00 AS Numeric(18, 2)), CAST(110.00 AS Numeric(18, 2)), CAST(50.00 AS Numeric(18, 2)), CAST(N'2017-12-10 00:00:00.0000000' AS DateTime2), 2463912, CAST(N'2017-12-10 00:00:00.0000000' AS DateTime2), 2463912)
SET IDENTITY_INSERT [CRM].[Orders] OFF
SET IDENTITY_INSERT [CRM].[Payments] ON 

INSERT [CRM].[Payments] ([PaymentID], [OpportunityID], [OrderID], [Sidemark], [Total], [BalanceDue], [PayBalance], [PaymentMethod], [VerificationCheck], [PaymentDate], [Amount], [Memo], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (1, 1, 1, N'test', CAST(100.00 AS Numeric(18, 2)), CAST(50.00 AS Numeric(18, 2)), 1, 1, NULL, CAST(N'2017-12-10 00:00:00.0000000' AS DateTime2), CAST(100.00 AS Numeric(18, 2)), N'asd asd asda dsdfaf sd', CAST(N'2017-12-10 00:00:00.0000000' AS DateTime2), 2463912, CAST(N'2017-12-10 00:00:00.0000000' AS DateTime2), 2463912)
SET IDENTITY_INSERT [CRM].[Payments] OFF
SET IDENTITY_INSERT [CRM].[Quote] ON 

INSERT [CRM].[Quote] ([QuoteKey], [QuoteID], [OpportunityId], [QuoteName], [PrimaryQuote], [QuoteStatusId], [TotalCharges], [Discount], [DiscountType], [TotalDiscounts], [NetCharges], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy], [MeasurementsetId]) VALUES (1, 1, 1, N'test quote', 1, 1, CAST(250.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), N'%', CAST(0.00 AS Numeric(18, 2)), CAST(250.00 AS Numeric(18, 2)), CAST(N'2017-11-30T11:24:54.1200000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-11-30T11:24:54.1200000+05:30' AS DateTimeOffset), 2464994, N'1,2')
INSERT [CRM].[Quote] ([QuoteKey], [QuoteID], [OpportunityId], [QuoteName], [PrimaryQuote], [QuoteStatusId], [TotalCharges], [Discount], [DiscountType], [TotalDiscounts], [NetCharges], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy], [MeasurementsetId]) VALUES (2, 2, 1, N'test quote2', 1, 1, CAST(240.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), N'%', CAST(0.00 AS Numeric(18, 2)), CAST(250.00 AS Numeric(18, 2)), CAST(N'2017-11-30T11:24:54.1200000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-11-30T11:24:54.1200000+05:30' AS DateTimeOffset), 2464994, N'1,2')
SET IDENTITY_INSERT [CRM].[Quote] OFF
SET IDENTITY_INSERT [CRM].[QuoteLines] ON 

INSERT [CRM].[QuoteLines] ([QuoteLineId], [QuoteKey], [MeasurementsetId], [VendorId], [ProductId], [SuggestedResale], [Discount], [CalculatedSalePrice], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (1, 1, 1, 1, 1, CAST(150.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(150.00 AS Numeric(18, 2)), CAST(N'2017-11-30T11:24:54.1200000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-11-30T11:24:54.1200000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[QuoteLines] ([QuoteLineId], [QuoteKey], [MeasurementsetId], [VendorId], [ProductId], [SuggestedResale], [Discount], [CalculatedSalePrice], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (2, 1, 1, 1, 1, CAST(150.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(150.00 AS Numeric(18, 2)), CAST(N'2017-11-30T11:24:54.1200000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-11-30T11:24:54.1200000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[QuoteLines] ([QuoteLineId], [QuoteKey], [MeasurementsetId], [VendorId], [ProductId], [SuggestedResale], [Discount], [CalculatedSalePrice], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (3, 2, 1, 1, 1, CAST(150.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(150.00 AS Numeric(18, 2)), CAST(N'2017-11-30T11:24:54.1200000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-11-30T11:24:54.1200000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[QuoteLines] ([QuoteLineId], [QuoteKey], [MeasurementsetId], [VendorId], [ProductId], [SuggestedResale], [Discount], [CalculatedSalePrice], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (4, 2, 2, 1, 1, CAST(150.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), CAST(150.00 AS Numeric(18, 2)), CAST(N'2017-11-30T11:24:54.1200000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-11-30T11:24:54.1200000+05:30' AS DateTimeOffset), 2464994)
SET IDENTITY_INSERT [CRM].[QuoteLines] OFF
SET IDENTITY_INSERT [CRM].[Type_OpportunityStatus] ON 

INSERT [CRM].[Type_OpportunityStatus] ([OpportunityStatusId], [Name], [Description], [BrandId], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy]) VALUES (1, N'Qualified', N'Carried from the Lead being converted to an Account and Opportunity', 1, CAST(N'2017-11-02 11:34:11.770' AS DateTime), 1, CAST(N'2017-11-02 11:34:11.770' AS DateTime), 1)
INSERT [CRM].[Type_OpportunityStatus] ([OpportunityStatusId], [Name], [Description], [BrandId], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy]) VALUES (2, N'Measurement', N'When a User adds the first measurement', 1, CAST(N'2017-11-02 11:34:11.773' AS DateTime), 1, CAST(N'2017-11-02 11:34:11.773' AS DateTime), 1)
INSERT [CRM].[Type_OpportunityStatus] ([OpportunityStatusId], [Name], [Description], [BrandId], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy]) VALUES (3, N'Quote', N'Set to Quote when the User has created a Quote (presumably, after the Measurements are completed)', 1, CAST(N'2017-11-02 11:34:11.773' AS DateTime), 1, CAST(N'2017-11-02 11:34:11.773' AS DateTime), 1)
INSERT [CRM].[Type_OpportunityStatus] ([OpportunityStatusId], [Name], [Description], [BrandId], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy]) VALUES (4, N'Ordering', N'SetThis is set to Ordering when a primary quote has been chosen, and an Order (job) has been created', 1, CAST(N'2017-11-02 11:34:11.773' AS DateTime), 1, CAST(N'2017-11-02 11:34:11.773' AS DateTime), 1)
INSERT [CRM].[Type_OpportunityStatus] ([OpportunityStatusId], [Name], [Description], [BrandId], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy]) VALUES (5, N'Order Accepted', N'Once a deposit has been accepted on the Order, at this point, the Opportunity will display as a job - the first step of a Job is the next row in this table (Procurement Pending)', 1, CAST(N'2017-11-02 11:34:11.773' AS DateTime), 1, CAST(N'2017-11-02 11:34:11.773' AS DateTime), 1)
INSERT [CRM].[Type_OpportunityStatus] ([OpportunityStatusId], [Name], [Description], [BrandId], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy]) VALUES (6, N'Lost', N'This is the only manually set status - the user can choose to set the status to this if the Opportunity is fully lost and no sale can be made. The user will also be required to choose a disposition, if the status is set to "Lost".', 1, CAST(N'2017-11-02 11:34:11.773' AS DateTime), 1, CAST(N'2017-11-02 11:34:11.773' AS DateTime), 1)
SET IDENTITY_INSERT [CRM].[Type_OpportunityStatus] OFF
SET IDENTITY_INSERT [CRM].[Type_OrderStatus] ON 

INSERT [CRM].[Type_OrderStatus] ([OrderStatusId], [OrderStatus], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (1, N'Procurement Pending', 1, 0, CAST(N'2017-12-06 10:41:32.0630000' AS DateTime2), 2464994, CAST(N'2017-12-06 10:41:32.0630000' AS DateTime2), 2464994)
INSERT [CRM].[Type_OrderStatus] ([OrderStatusId], [OrderStatus], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (2, N'Procurement Complete', 1, 0, CAST(N'2017-12-06 10:41:32.0630000' AS DateTime2), 2464994, CAST(N'2017-12-06 10:41:32.0630000' AS DateTime2), 2464994)
INSERT [CRM].[Type_OrderStatus] ([OrderStatusId], [OrderStatus], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (3, N'Product Received', 1, 0, CAST(N'2017-12-06 10:41:32.0630000' AS DateTime2), 2464994, CAST(N'2017-12-06 10:41:32.0630000' AS DateTime2), 2464994)
INSERT [CRM].[Type_OrderStatus] ([OrderStatusId], [OrderStatus], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (4, N'Installation Scheduled', 1, 0, CAST(N'2017-12-06 10:41:32.0630000' AS DateTime2), 2464994, CAST(N'2017-12-06 10:41:32.0630000' AS DateTime2), 2464994)
INSERT [CRM].[Type_OrderStatus] ([OrderStatusId], [OrderStatus], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (5, N'Job Complete', 1, 0, CAST(N'2017-12-06 10:41:32.0630000' AS DateTime2), 2464994, CAST(N'2017-12-06 10:41:32.0630000' AS DateTime2), 2464994)
INSERT [CRM].[Type_OrderStatus] ([OrderStatusId], [OrderStatus], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (6, N'Cancelled', 1, 0, CAST(N'2017-12-06 10:41:32.0630000' AS DateTime2), 2464994, CAST(N'2017-12-06 10:41:32.0630000' AS DateTime2), 2464994)
SET IDENTITY_INSERT [CRM].[Type_OrderStatus] OFF
SET IDENTITY_INSERT [CRM].[Type_PaymentMethod] ON 

INSERT [CRM].[Type_PaymentMethod] ([Id], [PaymentMethod], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (1, N'Cash', 1, 0, CAST(N'2017-12-06 12:30:49.9800000' AS DateTime2), 2464994, CAST(N'2017-12-06 12:30:49.9800000' AS DateTime2), 2464994)
INSERT [CRM].[Type_PaymentMethod] ([Id], [PaymentMethod], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (2, N'Check', 1, 0, CAST(N'2017-12-06 12:30:49.9800000' AS DateTime2), 2464994, CAST(N'2017-12-06 12:30:49.9800000' AS DateTime2), 2464994)
INSERT [CRM].[Type_PaymentMethod] ([Id], [PaymentMethod], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (3, N'American Express', 1, 0, CAST(N'2017-12-06 12:30:49.9800000' AS DateTime2), 2464994, CAST(N'2017-12-06 12:30:49.9800000' AS DateTime2), 2464994)
INSERT [CRM].[Type_PaymentMethod] ([Id], [PaymentMethod], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (4, N'Discover', 1, 0, CAST(N'2017-12-06 12:30:49.9800000' AS DateTime2), 2464994, CAST(N'2017-12-06 12:30:49.9800000' AS DateTime2), 2464994)
INSERT [CRM].[Type_PaymentMethod] ([Id], [PaymentMethod], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (5, N'MasterCard', 1, 0, CAST(N'2017-12-06 12:30:49.9830000' AS DateTime2), 2464994, CAST(N'2017-12-06 12:30:49.9830000' AS DateTime2), 2464994)
INSERT [CRM].[Type_PaymentMethod] ([Id], [PaymentMethod], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (6, N'Visa', 1, 0, CAST(N'2017-12-06 12:30:49.9830000' AS DateTime2), 2464994, CAST(N'2017-12-06 12:30:49.9830000' AS DateTime2), 2464994)
INSERT [CRM].[Type_PaymentMethod] ([Id], [PaymentMethod], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (7, N'Debit Card', 1, 0, CAST(N'2017-12-06 12:30:49.9830000' AS DateTime2), 2464994, CAST(N'2017-12-06 12:30:49.9830000' AS DateTime2), 2464994)
INSERT [CRM].[Type_PaymentMethod] ([Id], [PaymentMethod], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (8, N'Gift Card', 1, 0, CAST(N'2017-12-06 12:30:49.9830000' AS DateTime2), 2464994, CAST(N'2017-12-06 12:30:49.9830000' AS DateTime2), 2464994)
INSERT [CRM].[Type_PaymentMethod] ([Id], [PaymentMethod], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (9, N'e-Check', 1, 0, CAST(N'2017-12-06 12:30:49.9830000' AS DateTime2), 2464994, CAST(N'2017-12-06 12:30:49.9830000' AS DateTime2), 2464994)
INSERT [CRM].[Type_PaymentMethod] ([Id], [PaymentMethod], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (10, N'Third Party Financing', 1, 0, CAST(N'2017-12-06 12:30:49.9830000' AS DateTime2), 2464994, CAST(N'2017-12-06 12:30:49.9830000' AS DateTime2), 2464994)
SET IDENTITY_INSERT [CRM].[Type_PaymentMethod] OFF
SET IDENTITY_INSERT [CRM].[Type_ProductCategory] ON 

INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (1, N'Building Materials', 0, 1, 0, CAST(N'2017-12-04T20:33:48.6700000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:33:48.6700000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (2, N'Decor', 0, 1, 0, CAST(N'2017-12-04T20:33:48.6800000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:33:48.6800000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (3, N'Chemicals', 0, 1, 0, CAST(N'2017-12-04T20:33:48.6800000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:33:48.6800000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (4, N'Hardware', 0, 1, 0, CAST(N'2017-12-04T20:33:48.6800000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:33:48.6800000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (5, N'Lighting & Fixtures', 0, 1, 0, CAST(N'2017-12-04T20:33:48.6800000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:33:48.6800000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (6, N'Paint', 0, 1, 0, CAST(N'2017-12-04T20:33:48.6800000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:33:48.6800000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (7, N'Service', 0, 1, 0, CAST(N'2017-12-04T20:33:48.6800000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:33:48.6800000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (8, N'Storage & Organization', 0, 1, 0, CAST(N'2017-12-04T20:33:48.6800000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:33:48.6800000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (9, N'Window Coverings', 0, 1, 0, CAST(N'2017-12-04T20:33:48.6900000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:33:48.6900000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (10, N'Lumber & Composites', 1, 1, 0, CAST(N'2017-12-04T20:36:17.1100000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:36:17.1100000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (11, N'Molding', 1, 1, 0, CAST(N'2017-12-04T20:36:17.1100000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:36:17.1100000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (12, N'Drywall', 1, 1, 0, CAST(N'2017-12-04T20:36:17.1200000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:36:17.1200000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (13, N'Doors', 1, 1, 0, CAST(N'2017-12-04T20:36:17.1200000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:36:17.1200000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (14, N'Textile', 2, 1, 0, CAST(N'2017-12-04T20:37:41.5400000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:37:41.5400000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (15, N'Wall Décor', 2, 1, 0, CAST(N'2017-12-04T20:37:41.6100000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:37:41.6100000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (16, N'Wallpaper', 2, 1, 0, CAST(N'2017-12-04T20:37:41.6100000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:37:41.6100000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (17, N'Art', 2, 1, 0, CAST(N'2017-12-04T20:37:41.6100000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:37:41.6100000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (18, N'Mirrors', 2, 1, 0, CAST(N'2017-12-04T20:37:41.6100000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:37:41.6100000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (19, N'Cleaners', 3, 1, 0, CAST(N'2017-12-04T20:38:47.6400000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:38:47.6400000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (20, N'Degreaser', 3, 1, 0, CAST(N'2017-12-04T20:38:47.6400000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:38:47.6400000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (21, N'Screws', 4, 1, 0, CAST(N'2017-12-04T20:40:21.1200000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:40:21.1200000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (22, N'Brackets', 4, 1, 0, CAST(N'2017-12-04T20:40:21.1300000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:40:21.1300000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (23, N'Bolts', 4, 1, 0, CAST(N'2017-12-04T20:40:21.1300000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:40:21.1300000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (24, N'Wall Anchors', 4, 1, 0, CAST(N'2017-12-04T20:40:21.1300000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:40:21.1300000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (25, N'Nails', 4, 1, 0, CAST(N'2017-12-04T20:40:21.1300000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:40:21.1300000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (26, N'Picture Hanging', 4, 1, 0, CAST(N'2017-12-04T20:40:21.1300000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:40:21.1300000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (27, N'Light Bulbs', 5, 1, 0, CAST(N'2017-12-04T20:41:20.7200000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:41:20.7200000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (28, N'Switches', 5, 1, 0, CAST(N'2017-12-04T20:41:20.7300000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:41:20.7300000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (29, N'Stain', 6, 1, 0, CAST(N'2017-12-04T20:42:15.5200000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:42:15.5200000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (30, N'Lacquer', 6, 1, 0, CAST(N'2017-12-04T20:42:15.5200000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:42:15.5200000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (31, N'Installation', 7, 1, 0, CAST(N'2017-12-04T20:43:45.4400000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:43:45.4400000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (32, N'Removal', 7, 1, 0, CAST(N'2017-12-04T20:43:45.4400000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:43:45.4400000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (33, N'Cleaning', 7, 1, 0, CAST(N'2017-12-04T20:43:45.4400000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:43:45.4400000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (34, N'Service Call', 7, 1, 0, CAST(N'2017-12-04T20:43:45.4400000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:43:45.4400000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (35, N'Mileage', 7, 1, 0, CAST(N'2017-12-04T20:43:45.4500000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:43:45.4500000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (36, N'Storage Bins', 8, 1, 0, CAST(N'2017-12-04T20:45:46.1300000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:45:46.1300000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (37, N'Hooks', 8, 1, 0, CAST(N'2017-12-04T20:45:46.1300000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:45:46.1300000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (38, N'Shoe Storage', 8, 1, 0, CAST(N'2017-12-04T20:45:46.1400000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:45:46.1400000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (39, N'Garment Racks', 8, 1, 0, CAST(N'2017-12-04T20:45:46.1400000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:45:46.1400000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (40, N'Wall Organization', 8, 1, 0, CAST(N'2017-12-04T20:45:46.1400000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:45:46.1400000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (41, N'Cellular Shades', 9, 1, 0, CAST(N'2017-12-04T20:47:15.5900000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:47:15.5900000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (42, N'Blinds', 9, 1, 0, CAST(N'2017-12-04T20:47:15.6000000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:47:15.6000000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (43, N'Vertical Blinds', 9, 1, 0, CAST(N'2017-12-04T20:47:15.6000000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:47:15.6000000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (44, N'Roman Shades', 9, 1, 0, CAST(N'2017-12-04T20:47:15.6000000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:47:15.6000000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (45, N'Faux Wood Shutters', 9, 1, 0, CAST(N'2017-12-04T20:47:15.6000000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:47:15.6000000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductCategory] ([ProductCategoryId], [ProductCategory], [Parant], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (46, N'Wood Shutters', 9, 1, 0, CAST(N'2017-12-04T20:47:15.6000000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:47:15.6000000+05:30' AS DateTimeOffset), 2464994)
SET IDENTITY_INSERT [CRM].[Type_ProductCategory] OFF
SET IDENTITY_INSERT [CRM].[Type_ProductStatus] ON 

INSERT [CRM].[Type_ProductStatus] ([ProductStatusId], [ProductStatus], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (1, N'Acitvate', 1, 0, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductStatus] ([ProductStatusId], [ProductStatus], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (2, N'Deactive', 1, 0, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_ProductStatus] ([ProductStatusId], [ProductStatus], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (3, N'Discontinued', 1, 0, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994)
SET IDENTITY_INSERT [CRM].[Type_ProductStatus] OFF
SET IDENTITY_INSERT [CRM].[Type_QuoteStatus] ON 

INSERT [CRM].[Type_QuoteStatus] ([QuoteStatusId], [QuoteStatus], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (1, N'Draft', 1, 0, CAST(N'2017-11-30T11:24:54.1200000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-11-30T11:24:54.1200000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_QuoteStatus] ([QuoteStatusId], [QuoteStatus], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (2, N'Presented', 1, 0, CAST(N'2017-11-30T11:24:54.1200000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-11-30T11:24:54.1200000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_QuoteStatus] ([QuoteStatusId], [QuoteStatus], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (3, N'Accepted', 1, 0, CAST(N'2017-11-30T11:24:54.1200000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-11-30T11:24:54.1200000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_QuoteStatus] ([QuoteStatusId], [QuoteStatus], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (4, N'Declined', 1, 0, CAST(N'2017-11-30T11:24:54.1200000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-11-30T11:24:54.1200000+05:30' AS DateTimeOffset), 2464994)
SET IDENTITY_INSERT [CRM].[Type_QuoteStatus] OFF
SET IDENTITY_INSERT [CRM].[Type_VendorName] ON 

INSERT [CRM].[Type_VendorName] ([VendorNameId], [VendorName], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (1, N'Ace Hardware', 1, 0, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_VendorName] ([VendorNameId], [VendorName], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (2, N'Aubuchon Hardware', 1, 0, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_VendorName] ([VendorNameId], [VendorName], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (3, N'Do It Best', 1, 0, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_VendorName] ([VendorNameId], [VendorName], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (4, N'Harbor Freight Tools', 1, 0, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_VendorName] ([VendorNameId], [VendorName], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (5, N'Home Depot', 1, 0, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_VendorName] ([VendorNameId], [VendorName], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (6, N'Lehman’s Hardware', 1, 0, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_VendorName] ([VendorNameId], [VendorName], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (7, N'Lowes', 1, 0, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_VendorName] ([VendorNameId], [VendorName], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (8, N'Menard', 1, 0, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_VendorName] ([VendorNameId], [VendorName], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (9, N'Orchard Supply Hardware', 1, 0, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_VendorName] ([VendorNameId], [VendorName], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (10, N'True Value', 1, 0, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_VendorName] ([VendorNameId], [VendorName], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (11, N'United Hardware Distributing Company', 1, 0, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_VendorName] ([VendorNameId], [VendorName], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (12, N'Valu Home Centers', 1, 0, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_VendorName] ([VendorNameId], [VendorName], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (13, N'Hunter ', 1, 0, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_VendorName] ([VendorNameId], [VendorName], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (14, N'Skandia', 1, 0, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_VendorName] ([VendorNameId], [VendorName], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (15, N'HT Window Fashions', 1, 0, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_VendorName] ([VendorNameId], [VendorName], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (16, N'Polywood', 1, 0, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_VendorName] ([VendorNameId], [VendorName], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (17, N'Coulisse', 1, 0, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_VendorName] ([VendorNameId], [VendorName], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (18, N'Vertilux', 1, 0, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_VendorName] ([VendorNameId], [VendorName], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (19, N'Insolroll', 1, 0, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_VendorName] ([VendorNameId], [VendorName], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (20, N'Comfortex', 1, 0, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994)
INSERT [CRM].[Type_VendorName] ([VendorNameId], [VendorName], [IsActive], [IsDeleted], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (21, N'Wesco', 1, 0, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994, CAST(N'2017-12-04T20:00:26.8000000+05:30' AS DateTimeOffset), 2464994)
SET IDENTITY_INSERT [CRM].[Type_VendorName] OFF
ALTER TABLE [Acct].[FranchiseVendors]  WITH CHECK ADD  CONSTRAINT [FK_FranchiseVendors_Address] FOREIGN KEY([AddressId])
REFERENCES [CRM].[Addresses] ([AddressId])
GO
ALTER TABLE [Acct].[FranchiseVendors] CHECK CONSTRAINT [FK_FranchiseVendors_Address]
GO
ALTER TABLE [Acct].[FranchiseVendors]  WITH CHECK ADD  CONSTRAINT [FK_HFCVendors_Address] FOREIGN KEY([AddressId])
REFERENCES [CRM].[Addresses] ([AddressId])
GO
ALTER TABLE [Acct].[FranchiseVendors] CHECK CONSTRAINT [FK_HFCVendors_Address]
GO
ALTER TABLE [CRM].[Invoices]  WITH CHECK ADD  CONSTRAINT [FK_Invoices_Createdby] FOREIGN KEY([CreatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO
ALTER TABLE [CRM].[Invoices] CHECK CONSTRAINT [FK_Invoices_Createdby]
GO
ALTER TABLE [CRM].[Invoices]  WITH CHECK ADD  CONSTRAINT [FK_Invoices_Modifiedby] FOREIGN KEY([LastUpdatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO
ALTER TABLE [CRM].[Invoices] CHECK CONSTRAINT [FK_Invoices_Modifiedby]
GO
ALTER TABLE [CRM].[Invoices]  WITH CHECK ADD  CONSTRAINT [FK_Invoices_Orders] FOREIGN KEY([OrderID])
REFERENCES [CRM].[Orders] ([OrderID])
GO
ALTER TABLE [CRM].[Invoices] CHECK CONSTRAINT [FK_Invoices_Orders]
GO
ALTER TABLE [CRM].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Opportunities] FOREIGN KEY([OpportunityId])
REFERENCES [CRM].[Opportunities] ([OpportunityId])
GO
ALTER TABLE [CRM].[Orders] CHECK CONSTRAINT [FK_Orders_Opportunities]
GO
ALTER TABLE [CRM].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Person] FOREIGN KEY([CreatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO
ALTER TABLE [CRM].[Orders] CHECK CONSTRAINT [FK_Orders_Person]
GO
ALTER TABLE [CRM].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Person1] FOREIGN KEY([LastUpdatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO
ALTER TABLE [CRM].[Orders] CHECK CONSTRAINT [FK_Orders_Person1]
GO
ALTER TABLE [CRM].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Quote] FOREIGN KEY([QuoteKey])
REFERENCES [CRM].[Quote] ([QuoteKey])
GO
ALTER TABLE [CRM].[Orders] CHECK CONSTRAINT [FK_Orders_Quote]
GO
ALTER TABLE [CRM].[Payments]  WITH CHECK ADD  CONSTRAINT [FK_Payments_Opportunities] FOREIGN KEY([OpportunityID])
REFERENCES [CRM].[Opportunities] ([OpportunityId])
GO
ALTER TABLE [CRM].[Payments] CHECK CONSTRAINT [FK_Payments_Opportunities]
GO
ALTER TABLE [CRM].[Payments]  WITH CHECK ADD  CONSTRAINT [FK_Payments_Orders] FOREIGN KEY([OrderID])
REFERENCES [CRM].[Orders] ([OrderID])
GO
ALTER TABLE [CRM].[Payments] CHECK CONSTRAINT [FK_Payments_Orders]
GO
ALTER TABLE [CRM].[Quote]  WITH CHECK ADD  CONSTRAINT [FK_CRM.Quote_Createdby] FOREIGN KEY([CreatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO
ALTER TABLE [CRM].[Quote] CHECK CONSTRAINT [FK_CRM.Quote_Createdby]
GO
ALTER TABLE [CRM].[Quote]  WITH CHECK ADD  CONSTRAINT [FK_CRM.Quote_Modifiedby] FOREIGN KEY([LastUpdatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO
ALTER TABLE [CRM].[Quote] CHECK CONSTRAINT [FK_CRM.Quote_Modifiedby]
GO
ALTER TABLE [CRM].[Quote]  WITH CHECK ADD  CONSTRAINT [FK_CRM.Quote_Opportunities] FOREIGN KEY([OpportunityId])
REFERENCES [CRM].[Opportunities] ([OpportunityId])
GO
ALTER TABLE [CRM].[Quote] CHECK CONSTRAINT [FK_CRM.Quote_Opportunities]
GO
ALTER TABLE [CRM].[QuoteLines]  WITH CHECK ADD  CONSTRAINT [FK_QuoteLines_Person] FOREIGN KEY([CreatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO
ALTER TABLE [CRM].[QuoteLines] CHECK CONSTRAINT [FK_QuoteLines_Person]
GO
ALTER TABLE [CRM].[QuoteLines]  WITH CHECK ADD  CONSTRAINT [FK_QuoteLines_Person1] FOREIGN KEY([LastUpdatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO
ALTER TABLE [CRM].[QuoteLines] CHECK CONSTRAINT [FK_QuoteLines_Person1]
GO
ALTER TABLE [CRM].[QuoteLines]  WITH CHECK ADD  CONSTRAINT [FK_QuoteLines_Quote] FOREIGN KEY([QuoteKey])
REFERENCES [CRM].[Quote] ([QuoteKey])
GO
ALTER TABLE [CRM].[QuoteLines] CHECK CONSTRAINT [FK_QuoteLines_Quote]
GO
ALTER TABLE [CRM].[Type_OrderStatus]  WITH CHECK ADD  CONSTRAINT [FK_Type_OrderStatus_Createdby] FOREIGN KEY([CreatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO
ALTER TABLE [CRM].[Type_OrderStatus] CHECK CONSTRAINT [FK_Type_OrderStatus_Createdby]
GO
ALTER TABLE [CRM].[Type_OrderStatus]  WITH CHECK ADD  CONSTRAINT [FK_Type_OrderStatus_Modifiedby] FOREIGN KEY([LastUpdatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO
ALTER TABLE [CRM].[Type_OrderStatus] CHECK CONSTRAINT [FK_Type_OrderStatus_Modifiedby]
GO
ALTER TABLE [CRM].[Type_PaymentMethod]  WITH CHECK ADD  CONSTRAINT [FK_Type_PaymentMethod_Createdby] FOREIGN KEY([CreatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO
ALTER TABLE [CRM].[Type_PaymentMethod] CHECK CONSTRAINT [FK_Type_PaymentMethod_Createdby]
GO
ALTER TABLE [CRM].[Type_PaymentMethod]  WITH CHECK ADD  CONSTRAINT [FK_Type_PaymentMethod_Modifiedby] FOREIGN KEY([LastUpdatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO
ALTER TABLE [CRM].[Type_PaymentMethod] CHECK CONSTRAINT [FK_Type_PaymentMethod_Modifiedby]
GO
ALTER TABLE [CRM].[Type_ProductCategory]  WITH CHECK ADD  CONSTRAINT [FK_Type_ProductCategory_Createdby] FOREIGN KEY([CreatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO
ALTER TABLE [CRM].[Type_ProductCategory] CHECK CONSTRAINT [FK_Type_ProductCategory_Createdby]
GO
ALTER TABLE [CRM].[Type_ProductCategory]  WITH CHECK ADD  CONSTRAINT [FK_Type_ProductCategory_Modifiedby] FOREIGN KEY([LastUpdatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO
ALTER TABLE [CRM].[Type_ProductCategory] CHECK CONSTRAINT [FK_Type_ProductCategory_Modifiedby]
GO
ALTER TABLE [CRM].[Type_ProductStatus]  WITH CHECK ADD  CONSTRAINT [FK_Type_ProductStatus_Createdby] FOREIGN KEY([CreatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO
ALTER TABLE [CRM].[Type_ProductStatus] CHECK CONSTRAINT [FK_Type_ProductStatus_Createdby]
GO
ALTER TABLE [CRM].[Type_ProductStatus]  WITH CHECK ADD  CONSTRAINT [FK_Type_ProductStatus_Modifiedby] FOREIGN KEY([LastUpdatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO
ALTER TABLE [CRM].[Type_ProductStatus] CHECK CONSTRAINT [FK_Type_ProductStatus_Modifiedby]
GO
ALTER TABLE [CRM].[Type_QuoteStatus]  WITH CHECK ADD  CONSTRAINT [FK_CRM]].[Type_QuoteStatus_Createdby] FOREIGN KEY([CreatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO
ALTER TABLE [CRM].[Type_QuoteStatus] CHECK CONSTRAINT [FK_CRM]].[Type_QuoteStatus_Createdby]
GO
ALTER TABLE [CRM].[Type_QuoteStatus]  WITH CHECK ADD  CONSTRAINT [FK_CRM]].[Type_QuoteStatus_Modifiedby] FOREIGN KEY([LastUpdatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO
ALTER TABLE [CRM].[Type_QuoteStatus] CHECK CONSTRAINT [FK_CRM]].[Type_QuoteStatus_Modifiedby]
GO
ALTER TABLE [CRM].[Type_VendorName]  WITH CHECK ADD  CONSTRAINT [FK_Type_VendorName_Createdby] FOREIGN KEY([CreatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO
ALTER TABLE [CRM].[Type_VendorName] CHECK CONSTRAINT [FK_Type_VendorName_Createdby]
GO
ALTER TABLE [CRM].[Type_VendorName]  WITH CHECK ADD  CONSTRAINT [FK_Type_VendorName_Modifiedby] FOREIGN KEY([LastUpdatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO
ALTER TABLE [CRM].[Type_VendorName] CHECK CONSTRAINT [FK_Type_VendorName_Modifiedby]
GO
/****** Object:  StoredProcedure [CRM].[spProductNumber_New]    Script Date: 07-Dec-17 6:27:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [CRM].[spProductNumber_New]  	
AS
BEGIN
	
	SET NOCOUNT ON;

	IF EXISTS(SELECT ProductNumber FROM CRM.GetNewNumber)
	BEGIN
		UPDATE CRM.GetNewNumber SET ProductNumber = ISNULL(ProductNumber,0) + 1
		OUTPUT INSERTED.ProductNumber 
	END
	ELSE
	BEGIN
		INSERT INTO CRM.GetNewNumber (ProductNumber) 
		OUTPUT INSERTED.ProductNumber
		VALUES(1)
	END
END



GO
/****** Object:  StoredProcedure [CRM].[spQuoteNumber_New]    Script Date: 07-Dec-17 6:27:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


Create PROCEDURE [CRM].[spQuoteNumber_New]  
	@FranchiseId INT
AS
BEGIN
	
	SET NOCOUNT ON;

	IF EXISTS(SELECT * FROM CRM.LeadNJobNumbers WHERE FranchiseId = @FranchiseId)
	BEGIN
		UPDATE CRM.LeadNJobNumbers SET QuoteNumber = ISNULL(QuoteNumber,0) + 1
		OUTPUT INSERTED.QuoteNumber 
		WHERE FranchiseId = @FranchiseId
	END
	ELSE
	BEGIN
		INSERT INTO CRM.LeadNJobNumbers (FranchiseId,QuoteNumber) 
		OUTPUT INSERTED.QuoteNumber
		VALUES(@FranchiseId,1)
	END
END



GO
/****** Object:  StoredProcedure [CRM].[spVendor_New]    Script Date: 07-Dec-17 6:27:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [CRM].[spVendor_New]  	
AS
BEGIN
	
	SET NOCOUNT ON;

	IF EXISTS(SELECT VendorNumber FROM CRM.GetNewNumber)
	BEGIN
		UPDATE CRM.GetNewNumber SET VendorNumber = ISNULL(VendorNumber,0) + 1
		OUTPUT INSERTED.VendorNumber 
	END
	ELSE
	BEGIN
		INSERT INTO CRM.GetNewNumber (VendorNumber) 
		OUTPUT INSERTED.VendorNumber
		VALUES(1)
	END
END



GO
