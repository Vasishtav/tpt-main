if exists (select * from dbo.sysobjects where id = object_id(N'Acct.HFCVendors') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE Acct.HFCVendors  
GO   

/****** Object:  Table [Acct].[HFCVendors]    Script Date: 15-Dec-17 1:26:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [Acct].[HFCVendors](
	[VendorIdPk] [int] IDENTITY(1,1) NOT NULL,
	[VendorId] [int] NOT NULL,
	[VendorType] [int] not null,
	[Name] [varchar](256) NOT NULL,
	[AccountRep] [varchar](256) NULL,
	[AccountRepPhone] [varchar](15) NULL,
	[AccountRepEmail] [varchar](256) NULL,
	[OrderingMethod] [varchar](50) NULL,
	[ContactDetails] [varchar](256) NULL,	
	[AddressId] [int] NULL,
	[IsActive] [bit] NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastUpdatedOn] [datetime2](7) NULL,
	[LastUpdatedBy] [int] NULL,
 CONSTRAINT [PK_HFCVendors] PRIMARY KEY CLUSTERED 
(
	[VendorIdPk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


