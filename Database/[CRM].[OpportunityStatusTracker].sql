Drop table [CRM].[OpportunityStatusTracker]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TABLE [CRM].[OpportunityStatusTracker](
	[OpportunityTrackerId] [int] IDENTITY(1,1) NOT NULL,
	[OpportunityId] [int] NOT NULL,
	[FromOpportunityStatusId] [tinyint] NOT NULL,
	[ToOpportunityStatusId] [tinyint] NOT NULL,
	[DispositionId] [int] NULL,
	[Createdon] [datetime] NOT NULL,
	[CreatedBy] [int]  NULL
) ON [PRIMARY]

GO

ALTER TABLE [CRM].[OpportunityStatusTracker]  WITH CHECK ADD  CONSTRAINT [FK_Opportunities_OpportunityStatusTracker] FOREIGN KEY([OpportunityId])
REFERENCES [CRM].[Opportunities] ([OpportunityId])
GO

ALTER TABLE [CRM].[OpportunityStatusTracker] CHECK CONSTRAINT [FK_Opportunities_OpportunityStatusTracker]
GO

ALTER TABLE [CRM].[OpportunityStatusTracker]  WITH CHECK ADD  CONSTRAINT [FK_FromOpportunityStatus_OpportunityStatusTracker] FOREIGN KEY([FromOpportunityStatusId])
REFERENCES [CRM].[Type_OpportunityStatus] (OpportunityStatusId)
GO
ALTER TABLE [CRM].[OpportunityStatusTracker]  WITH CHECK ADD  CONSTRAINT [FK_ToOpportunityStatus_OpportunityStatusTracker] FOREIGN KEY([ToOpportunityStatusId])
REFERENCES [CRM].[Type_OpportunityStatus] (OpportunityStatusId)
GO

ALTER TABLE [CRM].[OpportunityStatusTracker] CHECK CONSTRAINT [FK_FromOpportunityStatus_OpportunityStatusTracker]
GO
ALTER TABLE [CRM].[OpportunityStatusTracker] CHECK CONSTRAINT [FK_ToOpportunityStatus_OpportunityStatusTracker]
GO

