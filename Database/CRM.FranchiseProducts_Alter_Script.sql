alter table [CRM].[FranchiseProducts] alter column MarkUp numeric(18,2)
Go
alter table [CRM].[FranchiseProducts] alter column MarkupType varchar(10)
Go
alter table [CRM].[FranchiseProducts] add ProductType int not null default(0)
Go
