USE [MyCrmTP03]
GO
/****** Object:  Trigger [CRM].[OrderStatusTrackerTrigger]    Script Date: 21-Dec-17 7:41:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [CRM].[OrderStatusTrackerTrigger] ON [CRM].[Orders]
FOR INSERT, UPDATE
AS
     BEGIN
         IF EXISTS (SELECT * FROM INSERTED) AND  EXISTS(SELECT * FROM DELETED)
		BEGIN
		IF EXISTS
		(
			SELECT 1 FROM INSERTED AS i
			INNER JOIN Deleted AS d
				 ON d.OrderID = i.OrderID
			WHERE i.OrderID <> d.OrderID
		)
             BEGIN
                 INSERT INTO CRM.OrderStatusTracker
                 ([OrderID],[FromOrderStatusId],[ToOrderStatusId],
                  [CreatedOn],
                  [CreatedBy]
                 )
                       SELECT i.[OrderID],
                              d.[OrderStatus],i.[OrderStatus],
                               GETUTCDATE(),
                               i.LastUpdatedBy
                        FROM inserted i, Deleted as d ;
         END END;
             ELSE
             BEGIN
                 INSERT INTO CRM.OrderStatusTracker
                  ([OrderID],
                  [FromOrderStatusId],[ToOrderStatusId],
                  [Createdon],
                  [CreatedBy]
                 )
                       SELECT [OrderID],
                              [OrderStatus],[OrderStatus],
                               GETUTCDATE(),
                               CreatedBy
                        FROM inserted;
         END;
     END;

 


