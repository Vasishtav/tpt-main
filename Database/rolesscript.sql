
DECLARE @LoopCounter INT , @maxpermissionTypeId int 
set @maxpermissionTypeId = 2074
set @LoopCounter = 2031

 
WHILE(@LoopCounter IS NOT NULL
      AND @LoopCounter <= @maxpermissionTypeId)
BEGIN 
  
INSERT [Auth].[Permissions] ( [CanCreate], [CanRead], [CanUpdate], [CanDelete], [PermissionTypeId], [FranchiseId], [RoleId], [CreatedOnUtc], [PersonId], [CanAccess]) VALUES 
 (1, 1, 1, 1, @LoopCounter, NULL, NULL, CAST(N'2017-12-06 10:31:32.000' AS DateTime), NULL, NULL)
,(1, 1, 1, 1, @LoopCounter, NULL, N'1709d38a-03b3-4cbe-96e4-193d0d3542d0', CAST(N'2017-12-06 10:31:32.000' AS DateTime), NULL, 1)
,(1, 1, 1, 1, @LoopCounter, NULL, N'b77161ac-2ff6-4672-a3d4-5008061faa34', CAST(N'2017-12-06 10:31:32.000' AS DateTime), NULL, 1)
,(1, 1, 1, 1, @LoopCounter, NULL, N'd6baab7b-d94f-41d3-88e9-601fd1c27398', CAST(N'2017-12-06 10:31:32.000' AS DateTime), NULL, 1)
,(1, 1, 1, 1, @LoopCounter, NULL, N'a725282f-e606-4353-bb53-693f695d42a5', CAST(N'2017-12-06 10:31:32.000' AS DateTime), NULL, 1)
,(1, 1, 1, 1, @LoopCounter, NULL, N'60333d84-ceca-435d-8281-70066e487a39', CAST(N'2017-12-06 10:31:32.000' AS DateTime), NULL, 1)
,(1, 1, 1, 1, @LoopCounter, NULL, N'3f1154e2-0718-40dc-8f8e-75dca2183ac1', CAST(N'2017-12-06 10:31:32.000' AS DateTime), NULL, 1)
,(1, 1, 1, 1, @LoopCounter, NULL, N'9cfe899f-a036-4d69-a58b-a1bfbdd8dca0', CAST(N'2017-12-06 10:31:32.000' AS DateTime), NULL, 1)
,(1, 1, 1, 1, @LoopCounter, NULL, N'7d067b3b-aba1-4782-8077-a8083da97984', CAST(N'2017-12-06 10:31:32.000' AS DateTime), NULL, 1)
,(1, 1, 1, 1, @LoopCounter, NULL, N'e563d1f7-9248-4ad2-8aeb-c4fe0d59d42e', CAST(N'2017-12-06 10:31:32.000' AS DateTime), NULL, 1)
,(1, 1, 1, 1, @LoopCounter, NULL, N'947a9352-e180-452a-a00a-e3364ff5dab9', CAST(N'2017-12-06 10:31:32.000' AS DateTime), NULL, 1)  
   
   SET @LoopCounter  = @LoopCounter  + 1        
END

