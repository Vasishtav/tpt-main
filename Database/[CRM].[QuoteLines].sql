if exists (select * from dbo.sysobjects where id = object_id(N'[CRM].[QuoteLines]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [CRM].[QuoteLines]  
GO   
CREATE TABLE [CRM].[QuoteLines] ( 
QuoteLineId int  Not NULL identity(1,1) ,
QuoteKey int not NULL,
MeasurementsetId int  NULL,
VendorId int  NULL,
ProductId int  NULL,
SuggestedResale numeric(18, 2)  NULL,
Discount numeric(18, 2)  NULL,
CreatedOn datetime2(7)  not NULL,
CreatedBy int  not NULL,
LastUpdatedOn datetime2(7)  NULL,
LastUpdatedBy int  NULL,
IsActive bit  NULL,
Width Numeric(18,2)  NULL,
Height Numeric(18,2)  NULL,
UnitPrice Numeric(18,2)  NULL,
Quantity int  NULL,
Description varchar(250)  NULL,
MountType varchar(10)  NULL,
ExtendedPrice Numeric(18,2),
    CONSTRAINT PK_QuoteLines PRIMARY KEY (QuoteLineId) 
   )

ALTER TABLE [CRM].[QuoteLines]  WITH CHECK ADD  CONSTRAINT [FK_QuoteLines_Person_Createdby] FOREIGN KEY(CreatedBy)
REFERENCES [CRM].[Person] ([PersonId])
GO

ALTER TABLE [CRM].[QuoteLines] CHECK CONSTRAINT [FK_QuoteLines_Person_Createdby]
GO

ALTER TABLE [CRM].[QuoteLines]  WITH CHECK ADD  CONSTRAINT [FK_QuoteLines_Person_LastUpdatedBy] FOREIGN KEY([LastUpdatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO

ALTER TABLE [CRM].[QuoteLines] CHECK CONSTRAINT [FK_QuoteLines_Person_LastUpdatedBy]
GO

ALTER TABLE [CRM].[QuoteLines]  WITH CHECK ADD  CONSTRAINT [FK_QuoteLines_Quote_QuoteKey] FOREIGN KEY([QuoteKey])
REFERENCES [CRM].[Quote] ([QuoteKey])
GO

ALTER TABLE [CRM].[QuoteLines] CHECK CONSTRAINT [FK_QuoteLines_Quote_QuoteKey]
GO