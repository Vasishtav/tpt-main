SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO


ALTER PROC [CRM].[GetSourcesByFranchise]
(
@FranchiseId int
 )
as
/*
	EXEC [CRM].[GetSourcesByFranchise] @FranchiseId=2268
*/


SELECT DISTINCT
	[SourceId]
   ,[Name]
   ,[Description]
   ,[ParentId]
   ,[FranchiseId]
   ,[StartDate]
   ,[EndDate]
   ,[Amount]
   ,[CreatedOn]
   ,[IsActive]
   ,[Memo]
   ,[isDeleted]
   ,[isCampaign]
FROM (SELECT
		[SourceId]
	   ,[Name]
	   ,[Description]
	   ,[ParentId]
	   ,[FranchiseId]
	   ,[StartDate]
	   ,[EndDate]
	   ,[Amount]
	   ,[CreatedOn]
	   ,[IsActive]
	   ,[Memo]
	   ,[isDeleted]
	   ,[isCampaign]
	FROM [CRM].[Sources]
	WHERE FranchiseId IS NULL
	AND ISNULL(isDeleted, 0) <> 1
	AND ParentId IS NULL
	UNION ALL
	SELECT
		[SourceId]
	   ,[Name]
	   ,[Description]
	   ,[ParentId]
	   ,[FranchiseId]
	   ,[StartDate]
	   ,[EndDate]
	   ,[Amount]
	   ,[CreatedOn]
	   ,[IsActive]
	   ,[Memo]
	   ,[isDeleted]
	   ,[isCampaign]
	FROM [CRM].[Sources]
	WHERE parentId IN (SELECT DISTINCT
			SourceId
		FROM [CRM].[Sources]
		WHERE FranchiseId IS NULL
		AND ISNULL(isDeleted, 0) <> 1
		AND ParentId IS NULL)
	AND (FranchiseId = @FranchiseId
	OR FranchiseId IS NULL)
	AND ISNULL(isDeleted, 0) <> 1) AS retVal
ORDER BY [ParentId], [CreatedOn], [SourceId]



GO


