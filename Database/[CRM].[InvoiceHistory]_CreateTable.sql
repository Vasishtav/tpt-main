USE [MyCrmTP03]
GO

/****** Object:  Table [CRM].[Invoices]    Script Date: 21-Dec-17 2:14:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [CRM].[InvoiceHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderID] [int] NOT NULL,
	[InvoiceId] [int] NOT NULL,
	[InvoiceType] [varchar](100) NULL,
	[InvoiceDate] [datetime2](7) NULL,
	[TotalAmount] [numeric](18, 2) NULL,
	[Balance] [numeric](18, 2) NULL,
	[Streamid] [uniqueidentifier] NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastUpdatedOn] [datetime2](7) NULL,
	[LastUpdatedBy] [int] NULL,
 CONSTRAINT [PK_InvoiceHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [CRM].[InvoiceHistory]  WITH CHECK ADD  CONSTRAINT [FK_InvoiceHistory_Orders] FOREIGN KEY([OrderID])
REFERENCES [CRM].[Orders] ([OrderID])
GO

ALTER TABLE [CRM].[InvoiceHistory] CHECK CONSTRAINT [FK_InvoiceHistory_Orders]
GO

ALTER TABLE [CRM].[InvoiceHistory]  WITH CHECK ADD  CONSTRAINT [FK_InvoiceHistory_Invoices] FOREIGN KEY([InvoiceId])
REFERENCES [CRM].[Invoices] ([InvoiceId])
GO

ALTER TABLE [CRM].[InvoiceHistory] CHECK CONSTRAINT [FK_InvoiceHistory_Invoices]
GO

