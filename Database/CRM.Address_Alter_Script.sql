alter table [CRM].[Addresses] add [CreatedOn] datetime2(7) not null default(GETUTCDATE()) 
Go
alter table [CRM].[Addresses] add [LastUpdatedOn] datetime2(7)  
Go
alter table [CRM].[Addresses] add [LastUpdatedBy] int
Go