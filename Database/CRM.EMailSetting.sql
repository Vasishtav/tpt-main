USE [MyCrmTP03]
GO

/****** Object:  Table [CRM].[Invoices]    Script Date: 22-Dec-17 5:00:43 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [CRM].[EmailSettings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FranchiseId] [int] NOT NULL,
	[EmailType] [varchar](100) NOT NULL,
	[FromEmailId] int NULL,
	CopyAssignedSales bit null,
	CopyAssignedInstaller bit null,
	[CreatedOn] [datetime2](7) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastUpdatedOn] [datetime2](7) NULL,
	[LastUpdatedBy] [int] NULL,
 CONSTRAINT [PK_EmailSettings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [CRM].[EmailSettings]  WITH CHECK ADD  CONSTRAINT [FK_EmailSettings_Createdby] FOREIGN KEY([CreatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO

ALTER TABLE [CRM].[EmailSettings] CHECK CONSTRAINT [FK_EmailSettings_Createdby]
GO

ALTER TABLE [CRM].[EmailSettings]  WITH CHECK ADD  CONSTRAINT [FK_EmailSettings_Modifiedby] FOREIGN KEY([LastUpdatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO

ALTER TABLE [CRM].[EmailSettings] CHECK CONSTRAINT [FK_EmailSettings_Modifiedby]
GO




