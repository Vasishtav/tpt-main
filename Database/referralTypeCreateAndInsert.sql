USE [MyCrmTP04]
GO

/****** Object:  Table [CRM].[Type_Royalty]    Script Date: 23/02/2018 1:43:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [CRM].[Type_Referral](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](500) NULL,
	
 CONSTRAINT [Pk_Type_Referral] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


insert into [CRM].[Type_Referral]
values('Business'), ('Co-worker'), ('Customer'), ('Friend'), ('Neighbor'), ('Relative')



