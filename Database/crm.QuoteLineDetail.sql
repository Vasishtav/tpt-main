
if exists (select * from dbo.sysobjects where id = object_id(N'crm.QuoteLineDetail') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE crm.QuoteLineDetail  
GO   
CREATE TABLE crm.QuoteLineDetail ( 
QuoteLineDetailId int  Not NULL identity(1,1) ,
QuoteLineId int  Not NULL,
Quantity int  Not NULL,
BaseCost Numeric(18,2)  NULL,
BaseResalePrice Numeric(18,2)  NULL,
BaseResalePriceWOPromos Numeric(18,2)  NULL,
PricingCode varchar(5)  NULL,
PricingRule varchar(50)  NULL,
MarkupFactor Numeric(18,2)  NULL,
Rounding varchar(100)  NULL,
SuggestedResale Numeric(18,2)  NULL,
Discount Numeric(18,2)  NULL,
unitPrice Numeric(18,2)  NULL,
DiscountAmount Numeric(18,2)  NULL,
ExtendedPrice Numeric(18,2)  NULL,
MarginPercentage Numeric(18,2)  NULL,
PricingId int  NULL,
ProductOrOption varchar(100)  NULL,
SelectedPrice varchar(100)  null,
CreatedOn datetime2(7)  not null,
CreatedBy int  not null,
LastUpdatedOn datetime2(7)  null,
LastUpdatedBy int  null,
  CONSTRAINT PK_QuoteLineDetail PRIMARY KEY (QuoteLineDetailId) 
   )


ALTER TABLE [CRM].[QuoteLineDetail]  WITH CHECK ADD  CONSTRAINT [FK_QuoteLineDetail_Person_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO

ALTER TABLE [CRM].[QuoteLineDetail] CHECK CONSTRAINT [FK_QuoteLineDetail_Person_CreatedBy]
GO

ALTER TABLE [CRM].[QuoteLineDetail]  WITH CHECK ADD  CONSTRAINT [FK_QuoteLineDetail_Person_LastUpdatedBy] FOREIGN KEY([LastUpdatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO

ALTER TABLE [CRM].[QuoteLineDetail] CHECK CONSTRAINT [FK_QuoteLineDetail_Person_LastUpdatedBy]
GO

ALTER TABLE [CRM].QuoteLineDetail  WITH CHECK ADD  CONSTRAINT [FK_QuoteLineDetail_QuoteLines_QuoteLineId] FOREIGN KEY(QuoteLineId)
REFERENCES [CRM].[QuoteLines] ([QuoteLineId])
GO

ALTER TABLE [CRM].QuoteLineDetail CHECK CONSTRAINT [FK_QuoteLineDetail_QuoteLines_QuoteLineId]
GO

ALTER TABLE [CRM].QuoteLineDetail  WITH CHECK ADD  CONSTRAINT [FK_QuoteLineDetail_PricingSettings_Id] FOREIGN KEY(PricingId)
REFERENCES [CRM].[PricingSettings] ([Id])
GO

ALTER TABLE [CRM].QuoteLineDetail CHECK CONSTRAINT [FK_QuoteLineDetail_PricingSettings_Id]
GO