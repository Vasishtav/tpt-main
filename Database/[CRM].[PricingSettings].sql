USE [MyCrmTP03]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'CRM.PricingSettings') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [CRM].[PricingSettings]
GO   


/****** Object:  Table [CRM].[PricingSettings]    Script Date: 12-Dec-17 4:58:20 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [CRM].[PricingSettings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FranchiseId] [int] NOT NULL,
	[PricingStrategyTypeId] [int] NOT NULL,
	[VendorId] [int] NULL,
	[ProductCategoryId] [int] NULL,
	[RetailBasis] [varchar](100) NULL,
	[MarkUp] [numeric](18, 2) NULL,
	[MarkUpFactor] [varchar](10) NULL,
	[Discount] [numeric](18, 2) NULL,
	[DiscountFactor] [varchar](10) NULL,
	[PriceRoundingOpt] [varchar](10) NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastUpdatedOn] [datetime2](7) NOT NULL,
	[LastUpdatedBy] [int] NOT NULL,
 CONSTRAINT [PK__PricingSettings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [CRM].[PricingSettings]  WITH CHECK ADD  CONSTRAINT [FK_PricingSettings_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO

ALTER TABLE [CRM].[PricingSettings] CHECK CONSTRAINT [FK_PricingSettings_CreatedBy]
GO

ALTER TABLE [CRM].[PricingSettings]  WITH CHECK ADD  CONSTRAINT [FK_PricingSettings_LastUpdatedBy] FOREIGN KEY([LastUpdatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO

ALTER TABLE [CRM].[PricingSettings] CHECK CONSTRAINT [FK_PricingSettings_LastUpdatedBy]
GO

ALTER TABLE [CRM].[PricingSettings]  WITH CHECK ADD  CONSTRAINT [FK_PricingSettings_PricingStrategyType] FOREIGN KEY([PricingStrategyTypeId])
REFERENCES [CRM].[Type_PricingStrategyType] ([Id])
GO

ALTER TABLE [CRM].[PricingSettings] CHECK CONSTRAINT [FK_PricingSettings_PricingStrategyType]
GO

ALTER TABLE [CRM].[PricingSettings]  WITH CHECK ADD  CONSTRAINT [FK_PricingSettings_ProductCategory] FOREIGN KEY([ProductCategoryId])
REFERENCES [CRM].[Type_ProductCategory] ([ProductCategoryId])
GO

ALTER TABLE [CRM].[PricingSettings] CHECK CONSTRAINT [FK_PricingSettings_ProductCategory]
GO


