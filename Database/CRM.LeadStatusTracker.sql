Drop table [CRM].[LeadStatusTracker]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TABLE [CRM].[LeadStatusTracker](
	[LeadStatusTrackerId] [int] IDENTITY(1,1) NOT NULL,
	[LeadId] [int] NOT NULL,
	[FromLeadStatusId] [smallint] NOT NULL,
	[ToLeadStatusId] [smallint] NOT NULL,
	[DispositionId] [int] NULL,
	[Createdon] [datetime] NOT NULL,
	[CreatedBy] [int]  NULL--This is  null only specific to Leads as  lead management gets it as null for all other make this as not null
) ON [PRIMARY]

GO

ALTER TABLE [CRM].[LeadStatusTracker]  WITH CHECK ADD  CONSTRAINT [FK_Leads_LeadStatusTracker] FOREIGN KEY([LeadId])
REFERENCES [CRM].[Leads] ([LeadId])
GO

ALTER TABLE [CRM].[LeadStatusTracker] CHECK CONSTRAINT [FK_Leads_LeadStatusTracker]
GO

ALTER TABLE [CRM].[LeadStatusTracker]  WITH CHECK ADD  CONSTRAINT [FK_FromLeadStatus_LeadStatusTracker] FOREIGN KEY([FromLeadStatusId])
REFERENCES [CRM].[Type_LeadStatus] ([Id])
GO
ALTER TABLE [CRM].[LeadStatusTracker]  WITH CHECK ADD  CONSTRAINT [FK_ToLeadStatus_LeadStatusTracker] FOREIGN KEY([ToLeadStatusId])
REFERENCES [CRM].[Type_LeadStatus] ([Id])
GO

ALTER TABLE [CRM].[LeadStatusTracker] CHECK CONSTRAINT [FK_ToLeadStatus_LeadStatusTracker]
GO
ALTER TABLE [CRM].[LeadStatusTracker] CHECK CONSTRAINT [FK_FromLeadStatus_LeadStatusTracker]
GO

