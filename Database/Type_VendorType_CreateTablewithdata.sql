USE [MyCrmTP03]
GO

/****** Object:  Table [CRM].[Type_VendorName]    Script Date: 18-Dec-17 4:09:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [CRM].[Type_VendorType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[VendorType] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NULL,
	[CreatedOn] datetime2(7) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastUpdatedOn] datetime2(7) NULL,
	[LastUpdatedBy] [int] NULL,
 CONSTRAINT [PK_Type_VendorType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

insert into [CRM].[Type_VendorType] values('Alliance Vendor',1,0,GETUTCDATE(),2464994,GETUTCDATE(),2464994)
insert into [CRM].[Type_VendorType] values('Non-Vendor Alliance',1,0,GETUTCDATE(),2464994,GETUTCDATE(),2464994)
insert into [CRM].[Type_VendorType] values('My Vendor',1,0,GETUTCDATE(),2464994,GETUTCDATE(),2464994)
Go




