if exists (select * from dbo.sysobjects where id = object_id(N'CRM.Type_ProductType') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE CRM.Type_ProductType  
GO   


/****** Object:  Table [CRM].[Type_PaymentMethod]    Script Date: 15-Dec-17 2:12:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [CRM].[Type_ProductType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductType] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastUpdatedOn] [datetime2](7) NULL,
	[LastUpdatedBy] [int] NULL,
 CONSTRAINT [PK_Type_ProductType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [CRM].[Type_ProductType]  WITH CHECK ADD  CONSTRAINT [FK_Type_ProductType_Createdby] FOREIGN KEY([CreatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO

ALTER TABLE [CRM].[Type_ProductType] CHECK CONSTRAINT [FK_Type_ProductType_Createdby]
GO

ALTER TABLE [CRM].[Type_ProductType]  WITH CHECK ADD  CONSTRAINT [FK_Type_ProductType_Modifiedby] FOREIGN KEY([LastUpdatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO

ALTER TABLE [CRM].[Type_ProductType] CHECK CONSTRAINT [FK_Type_ProductType_Modifiedby]
GO



insert into [CRM].[Type_ProductType] values('Core Product',1,0,getutcdate(),2464994,getutcdate(),2464994)
insert into [CRM].[Type_ProductType] values('My Product',1,0,getutcdate(),2464994,getutcdate(),2464994)
insert into [CRM].[Type_ProductType] values('Service',1,0,getutcdate(),2464994,getutcdate(),2464994)

Go