USE [MyCrmTP03]
GO

/****** Object:  Table [CRM].[Type_ProductStatus]    Script Date: 18-Dec-17 2:09:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [CRM].[Type_VendorStatus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[VendorStatus] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NULL,
	[CreatedOn] datetime2(7) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastUpdatedOn] datetime2(7) NULL,
	[LastUpdatedBy] [int] NULL,
 CONSTRAINT [PK_Type_VendorStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [CRM].[Type_VendorStatus]  WITH CHECK ADD  CONSTRAINT [FK_Type_VendorStatus_Createdby] FOREIGN KEY([CreatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO

ALTER TABLE [CRM].[Type_VendorStatus] CHECK CONSTRAINT [FK_Type_VendorStatus_Createdby]
GO

ALTER TABLE [CRM].[Type_VendorStatus]  WITH CHECK ADD  CONSTRAINT [FK_Type_VendorStatus_Modifiedby] FOREIGN KEY([LastUpdatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO

ALTER TABLE [CRM].[Type_VendorStatus] CHECK CONSTRAINT [FK_Type_VendorStatus_Modifiedby]
GO


insert into [CRM].[Type_VendorStatus] values('Active',1,0,GETUTCDATE(),2464994,GETUTCDATE(),2464994)
insert into [CRM].[Type_VendorStatus] values('Inactive',1,0,GETUTCDATE(),2464994,GETUTCDATE(),2464994)
insert into [CRM].[Type_VendorStatus] values('Suspended',1,0,GETUTCDATE(),2464994,GETUTCDATE(),2464994)
Go

