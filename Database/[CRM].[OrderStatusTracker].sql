USE [MyCrmTP03]
GO

ALTER TABLE [CRM].[OrderStatusTracker] DROP CONSTRAINT [FK_Type_OrderStatus_OrderStatusTracker]
GO

ALTER TABLE [CRM].[OrderStatusTracker] DROP CONSTRAINT [FK_Orders_OrderStatusTracker]
GO

/****** Object:  Table [CRM].[OrderStatusTracker]    Script Date: 21-Dec-17 7:03:28 PM ******/
DROP TABLE [CRM].[OrderStatusTracker]
GO

/****** Object:  Table [CRM].[OrderStatusTracker]    Script Date: 21-Dec-17 7:03:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [CRM].[OrderStatusTracker](
	[OrderStatusTrackerId] [int] IDENTITY(1,1) NOT NULL,
	[OrderID] [int] NOT NULL,
	[FromOrderStatusId] [int] NOT NULL,
	[ToOrderStatusId] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NULL
) ON [PRIMARY]

GO

ALTER TABLE [CRM].[OrderStatusTracker]  WITH CHECK ADD  CONSTRAINT [FK_Orders_OrderStatusTracker] FOREIGN KEY([OrderID])
REFERENCES [CRM].[Orders] ([OrderID])
GO

ALTER TABLE [CRM].[OrderStatusTracker] CHECK CONSTRAINT [FK_Orders_OrderStatusTracker]
GO

ALTER TABLE [CRM].[OrderStatusTracker]  WITH CHECK ADD  CONSTRAINT [FK_Type_OrderStatus_FromOrderStatusTracker] FOREIGN KEY([FromOrderStatusId])
REFERENCES [CRM].[Type_OrderStatus] ([OrderStatusId])
GO

ALTER TABLE [CRM].[OrderStatusTracker] CHECK CONSTRAINT [FK_Type_OrderStatus_FromOrderStatusTracker]
GO

ALTER TABLE [CRM].[OrderStatusTracker]  WITH CHECK ADD  CONSTRAINT [FK_Type_OrderStatus_ToOrderStatusTracker] FOREIGN KEY([ToOrderStatusId])
REFERENCES [CRM].[Type_OrderStatus] ([OrderStatusId])
GO

ALTER TABLE [CRM].[OrderStatusTracker] CHECK CONSTRAINT [FK_Type_OrderStatus_ToOrderStatusTracker]
GO


