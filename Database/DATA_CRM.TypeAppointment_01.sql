USE [MyCrmTP03]
GO

--To Clean existing record and add new record
DELETE FROM [CRM].[Type_Appointments];

IF NOT EXISTS (SELECT 1 FROM [CRM].[Type_Appointments])
BEGIN

SET IDENTITY_INSERT [CRM].[Type_Appointments] ON 

INSERT [CRM].[Type_Appointments] ([AppointmentTypeId], [Name], [FranchiseId]) 
	VALUES (1, N'Sales/Design Appointment', NULL);

INSERT [CRM].[Type_Appointments] ([AppointmentTypeId], [Name], [FranchiseId]) 
	VALUES (5, N'Installation', NULL);

INSERT [CRM].[Type_Appointments] ([AppointmentTypeId], [Name], [FranchiseId]) 
	VALUES (6, N'Service', NULL);

INSERT [CRM].[Type_Appointments] ([AppointmentTypeId], [Name], [FranchiseId]) 
	VALUES (7, N'Followup', NULL);

INSERT [CRM].[Type_Appointments] ([AppointmentTypeId], [Name], [FranchiseId]) 
	VALUES (8, N'Personal', NULL);

INSERT [CRM].[Type_Appointments] ([AppointmentTypeId], [Name], [FranchiseId]) 
	VALUES (9, N'Vacation', NULL);

INSERT [CRM].[Type_Appointments] ([AppointmentTypeId], [Name], [FranchiseId]) 
	VALUES (10, N'Holiday', NULL);

INSERT [CRM].[Type_Appointments] ([AppointmentTypeId], [Name], [FranchiseId]) 
	VALUES (11, N'Meeting/Training', NULL);

INSERT [CRM].[Type_Appointments] ([AppointmentTypeId], [Name], [FranchiseId]) 
	VALUES (12, N'Time Block', NULL);

SET IDENTITY_INSERT [CRM].[Type_Appointments] OFF

END

GO
