USE [MyCrmTP03]
GO
/****** Object:  Trigger [CRM].[QuoteStatusTrackerTrigger]    Script Date: 21-Dec-17 7:34:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [CRM].[QuoteStatusTrackerTrigger] ON [CRM].[Quote]
FOR INSERT, UPDATE
AS
     BEGIN
        IF EXISTS (SELECT * FROM INSERTED) AND  EXISTS(SELECT * FROM DELETED)
		BEGIN
		IF EXISTS
		(
			SELECT 1 FROM INSERTED AS i
			INNER JOIN Deleted AS d
				 ON d.QuoteKey = i.QuoteKey
			WHERE i.QuoteKey <> d.QuoteKey
		)
             BEGIN
                 INSERT INTO CRM.QuoteStatusTracker
                 ([QuoteKey],[FromQuoteStatusId],[ToQuoteStatusId],
                  [CreatedOn],
                  [CreatedBy]
                 )
                       SELECT  i.QuoteKey, d.[QuoteStatusId],i.[QuoteStatusId],
                               GETUTCDATE(),
                               i.LastUpdatedBy
                        FROM inserted i, Deleted as d ;
        END END;
             ELSE
             BEGIN
                 INSERT INTO CRM.QuoteStatusTracker
                  ([QuoteKey],[FromQuoteStatusId],[ToQuoteStatusId],
                  [Createdon],
                  [CreatedBy]
                 )
                       SELECT [QuoteKey],
                              [QuoteStatusId],
							   [QuoteStatusId],
                               GETUTCDATE(),
                               CreatedBy
                        FROM inserted;
         END;
     END;

 


