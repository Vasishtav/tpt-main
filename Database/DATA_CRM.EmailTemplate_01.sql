USE [MyCrmTP03]
GO

/* Delete existing records and insert new */

DELETE FROM [CRM].[EmailTemplates] WHERE [Description] IN
(
	'Budget Blinds - Thank you Email'
	,'Tailored Living - Thank you Email'
	,'Concrete Craft - Thank you Email'
	,'Budget Blinds - Confirmation of Your Upcoming Appointment'
	,'Tailored Living - Confirmation of Your Upcoming Appointment'
	,'Concrete Craft - Confirmation of Your Upcoming Appointment'
	,'Budget Blinds - Reminder! You have an upcoming appointment.'
	,'Tailored Living - Reminder! You have an upcoming appointment.'
	,'Concrete Craft - Reminder! You have an upcoming appointment.'
	,'Budget Blinds - Order Summary'
	,'Tailored Living - Order Summary'
	,'Concrete Craft - Order Summary'
	,'Budget Blinds -  Install Schedule - What to Expect'
	,'Tailored Living-  Install Schedule - What to Expect'
	,'Concrete Craft-  Install Schedule - What to Expect'
	,'Budget Blinds - Thank You - Review'
	,'Tailored Living - Thank You - Review'
	,'Concrete Craft -Thank You - Review'
);

GO

SET IDENTITY_INSERT [CRM].[EmailTemplates] ON 


INSERT [CRM].[EmailTemplates] ([EmailTemplateId], [EmailTemplateTypeId], [Description], [TemplateSubject], [TemplateBody], [FranchiseId], [isDeleted], [TemplateLayout], [IsVisible], [IsVisibleAfterDate], [TemplateBrand]) VALUES (501, 1, N'Budget Blinds - Thank you Email', N'Thank You for Your Inquiry!', N'<div>
 <h1>
  Thank You for Your Inquiry!
 </h1>
</div>', NULL, 0, N'<html>
<head>
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
body{
	  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
       width:100% !important; 
	  -webkit-text-size-adjust:100%; 
	  -ms-text-size-adjust:100%;  
      margin:0; 
      padding:0; 
      -webkit-font-smoothing: antialiased;
      background:#fff;
}
html{
      width: 100%; 
}
table td, table td * {
      font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
}
</style>  
</head>
<body style="width: 100%; text-align: center;  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;">

<table width="100%" bgcolor="#fff" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#f0f1f1" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="padding:30px;"> 
				<tbody>
				  <tr> 
				    <td> 
					  <table  border="0" align="center" cellpadding="0" cellspacing="0" >
					     <tbody>
						    <tr> 
							   <td width="100%"> 
                                  <tbody>


<tr>
<td>
  <tbody>
    <tr>
      <td style="padding: 40px 0px 10px; font-size: 11pt; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #434343;" valign="top" align="center">
        <table cellpadding="0" cellspacing="0" data-padding-converted="true" width="366" align="center">
          <tbody>
            <tr>
              <td style="padding: 0px;" width="100%">
                <div align="center"><a track="on" href="http://www.budgetblinds.com/?utm_source=ConstantContact&amp;utm_medium=email&amp;utm_content=Logo&amp;utm_campaign=Email-1stOctPromo2017" class="imgCaptionAnchor">
				<img alt="Logo" title="Logo" height="64" width="200" name="ACCOUNT.IMAGE.1208" vspace="0" hspace="0" style="display: block; max-width: 100%; height: auto;" border="0" src="{IMAGEPATH}blinds_logo.png"></a></div>
              </td>
            </tr>
			<tr>
		      <td height="20"></td>
	        </tr>
          </tbody>
        </table>
        <br></td>
    </tr>
  </tbody>
</td>
</tr>
<!--- end logo --->


<!--- start navigation code --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                            <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.budgetblinds.com/window-blinds/" linktype="1" target="_blank">Blinds</a></strong></div>
                            </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.budgetblinds.com/window-shutters/" linktype="1" target="_blank">Shutters</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.budgetblinds.com/window-shades/" linktype="1" target="_blank">Shades</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 75px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.budgetblinds.com/draperies/" linktype="1" target="_blank">Drapes & More</a></strong></div>
                           </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end navigation code --->
<!--- start banner --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                              <td>
                                 <div align="center"><a track="on" href="http://www.budgetblinds.com/shop-by-room/childrens-window-coverings/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=hero&amp;utm_campaign=Email-1stOctPromo2017" class="imgCaptionAnchor"><img width="644" name="ACCOUNT.IMAGE.1206" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}Thank-You-Hero.jpg"></a></div>
							  </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end banner code--->

<!--- body --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				   <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						<tbody>
						        <tr>
		                            <td height="35"></td>
	                            </tr>
	                            <tr>
	                				<td style="color:#69696d; text-align:center; font-size:40px; font-weight: normal;line-height: 2em;">
	                					Thank You for Your Inquiry!
	                				</td>
	    						</tr>
	    						<tr>
									<td height="12"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:16px; text-align: center; font-weight: normal; line-height:20px; padding:0 30px;">
	                					Our complimentary in-home consultation includes a discussion of your window covering needs, a review of samples and colors, free measuring and an on-the-spot quote.
	                				</td>
	    						</tr>
								<tr>
									<td height="50"></td>
								</tr>
								<tr>
									<td><img name="hborder" src="{IMAGEPATH}hborder.jpg" alt="hborder"/></td>
								</tr>
								<tr>
									<td height="40"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:16px; text-align: center;font-weight: normal; font-style:italic; line-height:20px; padding:0 30px;">
	                					{OWNER} of {BRAND} will be calling you within 48 hours to schedule your in-home consultation!
	                				</td>
	    						</tr>
								<tr>
									<td height="25"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:16px; text-align: center; font-weight: normal;line-height:20px; padding:0 30px;">
										<strong>Question before then?</strong>Call or email {OWNER} at</br>{PHONE} | <a href="#" style="color:#69696d;">{EMAIL}</a>
	                				</td>
	    						</tr>
								<tr>
									<td height="50"></td>
								</tr>
	
	    						
								<!-- 3 column image grid--->
								<tr>
									<td>
										<table style="width:620px;" border="0" align="center" cellpadding="0" cellspacing="0">
											<tbody><tr>
												<td align="center">
													<a href="http://www.budgetblinds.com/shop-by-room/childrens-window-coverings/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=hero&amp;utm_campaign=Email-1stOctPromo2017"><img src="{IMAGEPATH}woman.jpg" alt=" " style="width:190px;"></a>
												</td>
												<td align="center">
													<a href="http://www.budgetblinds.com/shop-by-room/childrens-window-coverings/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=hero&amp;utm_campaign=Email-1stOctPromo2017"><img src="{IMAGEPATH}drill.jpg" alt=" " style="width:190px;"></a>
												</td>
												<td align="center">
													<a href="http://www.budgetblinds.com/shop-by-room/childrens-window-coverings/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=hero&amp;utm_campaign=Email-1stOctPromo2017"><img src="{IMAGEPATH}man.jpg" alt=" " style="width:190px;"></a>
												</td>
													</tr>
													<tr>
		                                               <td height="20"></td>
	                                                </tr>
											 </tbody>
									   </table>
									</td>
								</tr>
								<!--- end 3 column image grid--->
							</tbody>
					  </table> 
					</td> 
				   
				   </tr>
				 </tbody>
		  </table>
	   </td>
	</tr>
   </tbody>
</table>
<!--- end body --->

<!--- start footer --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
			                  <td style="width:60px;">
				                 <p style="color:#fff; font-size:12px; text-transform:uppercase; line-height:17px; width:80px;">Franchise<br>Opportunities<br>available</p>
				              </td>
							  <td style="width:30px;"><img name="vborder" src="{IMAGEPATH}vborder.jpg" alt="vborder"/></td>
				              <td style="width:300px;">
				                 <p style="color:#fff; font-size:18px; text-transform:uppercase; line-height:18px; margin:15px 0 5px;">Be your own boss.</br> own a {BRAND} brand Franchise.</p>
				                 <p style="color:#fff; font-size:10px; line-height:14px; margin:5px 0;">This is not an offering to sell a franchise. Franchise offerings are made through a Franchise Disclosure Document.</p>
				              </td>
				              <td style="width:50px;">
				                  <a track="on" href="http://www.budget-blinds-franchise.com/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=franchise&amp;utm_campaign=Email-1stOctPromo2017" class="imgCaptionAnchor">
				                  <img  name="ACCOUNT.IMAGE.1203" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}learn-but.jpg"></a>
				              </td>
                           </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- start footer text --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				     <td width="100%"> 
					   <table width="644" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						 <tbody>
						    <tr>
							    <td height="20"></td>
							</tr>
							<tr>
                                <td style="padding: 0px 25px; font-size: 10pt; font-family: &quot;Lucida Sans Unicode&quot;, &quot;LucidaGrande&quot;, sans-serif; color: #69696d; line-height: 1.3;" valign="top" align="center">
                                   <div style="font-family: ''Century Gothic'', ''ITC Avant Garde'', Arial, Helvetica, sans-serif; font-size: 8pt;">
                                       <div style="text-align: left;" align="left"><span style="font-size: 8pt;">
                                          <div><span style="font-size:10pt;">This is a confirmation message only. No action is needed from you. {BRAND} Brand respects your privacy - view our <a style="text-decoration: underline;color: #3d7d8b;" href="http://www.budgetblinds.com/privacy-policy/">Privacy Policy.</a></span></div>
                                          <div><span style="font-size: 8pt;"><br></span></div>
                                          <div><span style="font-size: 10pt;">&copy;2018 {BRAND}, LLC. All Rights Reserved. <span>{BRAND}</span>is a trademark of <span>{BRAND}</span>, LLC and a Home Franchise Concepts Brand. Each franchise independently owned and operated. Franchise opportunities available.</span></div></span></div>
                                   </div>
                                </td>
	                        </tr>
						    <tr>
							    <td height="20"></td>
							</tr>
						 </tbody>
					   </table>
				     </td>
				  </tr>
			  </tbody>
		  </table>
	    </td>
	  </tr>
   </tbody>
</table>
<!--- end footer text --->

<!--- end footer --->
</tbody>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</body>
</html>
', 1, CAST(N'2018-12-31' AS Date), 1)



INSERT [CRM].[EmailTemplates] ([EmailTemplateId], [EmailTemplateTypeId], [Description], [TemplateSubject], [TemplateBody], [FranchiseId], [isDeleted], [TemplateLayout], [IsVisible], [IsVisibleAfterDate], [TemplateBrand]) VALUES (502, 1, N'Tailored Living - Thank you Email', N'Thank You for Your Inquiry!', N'<div>
 <h1>
  Thank You for Your Inquiry!
 </h1>
</div>', NULL, 0, N'<html>
<head>
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
body{
	  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
       width:100% !important; 
	  -webkit-text-size-adjust:100%; 
	  -ms-text-size-adjust:100%;  
      margin:0; 
      padding:0; 
      -webkit-font-smoothing: antialiased;
      background:#fff;
}
html{
      width: 100%; 
}
table td, table td * {
      font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
}
</style>  
</head>
<body style="width: 100%; text-align: center;  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;">

<table width="100%" bgcolor="#fff" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#f0f1f1" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="padding:30px;"> 
				<tbody>
				  <tr> 
				    <td> 
					  <table  border="0" align="center" cellpadding="0" cellspacing="0" >
					     <tbody>
						    <tr> 
							   <td width="100%"> 
                                  <tbody>


<tr>
<td>
  <tbody>
    <tr>
      <td style="padding: 40px 0px 10px; font-size: 11pt; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #434343;" valign="top" align="center">
        <table cellpadding="0" cellspacing="0" data-padding-converted="true" width="366" align="center">
          <tbody>
            <tr>
              <td style="padding: 0px;" width="100%">
                <div align="center"><a track="on" href="http://www.tailoredliving.com/" class="imgCaptionAnchor">
				<img alt="Logo" title="Logo" height="64" width="200" name="ACCOUNT.IMAGE.1208" vspace="0" hspace="0" style="display: block; max-width: 100%; height: auto;" border="0" src="{IMAGEPATH}tailored_logo.png"></a></div>
              </td>
            </tr>
			<tr>
		      <td height="20"></td>
	        </tr>
          </tbody>
        </table>
        <br></td>
    </tr>
  </tbody>
</td>
</tr>
<!--- end logo --->

<!--- start navigation code --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                            <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.tailoredliving.com/closet-organization/" linktype="1" target="_blank">CLOSETS</a></strong></div>
                            </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.tailoredliving.com/garage-storage/" linktype="1" target="_blank">GARAGES</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.tailoredliving.com/home-office-design/" linktype="1" target="_blank">OFFICES</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 75px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.tailoredliving.com/home-organization/" linktype="1" target="_blank">PANTRIES & MORE</a></strong></div>
                           </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end navigation code --->

<!--- start banner --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                              <td>
                                 <div align="center"><a track="on" href="http://www.tailoredliving.com/" class="imgCaptionAnchor"><img width="644" name="ACCOUNT.IMAGE.1206" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}Thank-You-Hero.jpg"></a></div>
							  </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end banner code--->

<!--- body --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				   <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						<tbody>
						        <tr>
		                           <td height="35"></td>
	                            </tr>
	                            <tr>
	                				<td style="color:#69696d; text-align:center; font-size:40px; font-weight: normal;line-height: 2em;">
	                					Thank You for Your Inquiry!
	                				</td>
	    						</tr>
	    						<tr>
									<td height="12"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:16px; text-align: center; font-weight: normal;line-height:20px; padding:0 30px;">
	                					Our complimentary in-home consultation includes a discussion of your window covering needs, a review of samples and colors, free measuring and an on-the-spot quote.
	                				</td>
	    						</tr>
								<tr>
									<td height="50"></td>
								</tr>
								<tr>
									<td><img name="hborder" src="{IMAGEPATH}hborder.jpg" alt="hborder"/></td>
								</tr>
								<tr>
									<td height="40"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:16px; text-align: center;font-weight: normal;line-height:20px; padding:0 30px;">
	                					Our complimentary in-home consultation includes a discussion of your window covering needs, a review of samples and colors, free measuring and an on-the-spot quote.
	                				</td>
	    						</tr>
								<tr>
									<td height="25"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:16px; text-align: center; font-weight: normal;line-height:20px; padding:0 30px;">
	                					Our complimentary in-home consultation includes a discussion of your window covering needs, a review of samples and colors, free measuring and an on-the-spot quote.
	                				</td>
	    						</tr>
								<tr>
									<td height="50"></td>
								</tr>
	    						
								<!-- 3 column image grid--->
								<tr>
									<td>
										<table style="width:620px;" border="0" align="center" cellpadding="0" cellspacing="0">
											<tbody><tr>
												<td align="center">
													<a href="http://www.tailoredliving.com/"><img src="{IMAGEPATH}Service-Grid-1.jpg" alt=" " style="width:190px;"/></a>
												</td>
												<td align="center">
													<a href="http://www.tailoredliving.com/"><img src="{IMAGEPATH}Service-Grid-2.jpg" alt=" " style="width:190px;"/></a>
												</td>
												<td align="center">
													<a href="http://www.tailoredliving.com/"><img src="{IMAGEPATH}Service-Grid-3.jpg" alt=" " style="width:190px;"/></a>
												</td>
													</tr>
											 </tbody></table>
									</td>
								</tr>
								<tr>
									<td height="20"></td>
								</tr>
								<!--- end 3 column image grid--->
							</tbody>
					  </table> 
					</td> 
				   
				   </tr>
				 </tbody>
		  </table>
	   </td>
	</tr>
   </tbody>
</table>
<!--- end body --->

<!--- start footer --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
			                  <td style="width:60px;">
				                 <p style="color:#fff; font-size:12px; text-transform:uppercase; line-height:17px; width:80px;">Franchise<br>Opportunities<br>available</p>
				              </td>
							  <td style="width:30px;"><img name="vborder" src="{IMAGEPATH}vborder.jpg" alt="vborder"/></td>
				              <td style="width:300px;">
				                 <p style="color:#fff; font-size:18px; text-transform:uppercase; line-height:18px; margin:15px 0 5px;">Be your own boss.</br> own a {BRAND} brand Franchise.</p>
				                 <p style="color:#fff; font-size:10px; line-height:14px; margin:5px 0;">This is not an offering to sell a franchise. Franchise offerings are made through a Franchise Disclosure Document.</p>
				              </td>
				              <td style="width:50px;">
				                  <a track="on" href="http://www.budget-blinds-franchise.com/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=franchise&amp;utm_campaign=Email-1stOctPromo2017" class="imgCaptionAnchor">
				                  <img  name="ACCOUNT.IMAGE.1203" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}learn-but.jpg"></a>
				              </td>
                           </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- start footer text --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				     <td width="100%"> 
					   <table width="644" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						 <tbody>
						    <tr>
							    <td height="20"></td>
							</tr>
							<tr>
                                <td style="padding: 0px 25px; font-size: 10pt; font-family: &quot;Lucida Sans Unicode&quot;, &quot;LucidaGrande&quot;, sans-serif; color: #69696d; line-height: 1.3;" valign="top" align="center">
                                   <div style="font-family: ''Century Gothic'', ''ITC Avant Garde'', Arial, Helvetica, sans-serif; font-size: 8pt;">
                                       <div style="text-align: left;" align="left"><span style="font-size: 8pt;">
                                          <div><span style="font-size:10pt;">This is a confirmation message only. No action is needed from you. {BRAND} Brand respects your privacy - view our <a style="text-decoration: underline;color: #3d7d8b;" href="http://www.tailoredliving.com/privacy-policy/">Privacy Policy.</a></span></div>
                                          <div><span style="font-size: 8pt;"><br></span></div>
                                          <div><span style="font-size: 10pt;">&copy;2018 {BRAND} Brand. All Rights Reserved. <span>{BRAND}</span>featuring PremierGarage is a trademark of  <span>{BRAND}</span>, LLC and a Home Franchise Concepts Brand. Each franchise independently owned and operated. Franchise opportunities available.</span></div></span></div>
                                   </div>
                                </td>
	                        </tr>
						    <tr>
							    <td height="20"></td>
							</tr>
						 </tbody>
					   </table>
				     </td>
				  </tr>
			  </tbody>
		  </table>
	    </td>
	  </tr>
   </tbody>
</table>
<!--- end footer text --->

<!--- end footer --->
</tbody>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</body>
</html>
', 1, CAST(N'2018-12-31' AS Date), 2)


INSERT [CRM].[EmailTemplates] ([EmailTemplateId], [EmailTemplateTypeId], [Description], [TemplateSubject], [TemplateBody], [FranchiseId], [isDeleted], [TemplateLayout], [IsVisible], [IsVisibleAfterDate], [TemplateBrand]) VALUES (503, 1, N'Concrete Craft - Thank you Email', N'Thank You for Your Inquiry!', N'<div>
 <h1>
  Thank You for Your Inquiry!
 </h1>
</div>', NULL, 0, N'<html>
<head>
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
body{
	  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
       width:100% !important; 
	  -webkit-text-size-adjust:100%; 
	  -ms-text-size-adjust:100%;  
      margin:0; 
      padding:0; 
      -webkit-font-smoothing: antialiased;
      background:#fff;
}
html{
      width: 100%; 
}
table td, table td * {
      font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
}
</style>  
</head>
<body style="width: 100%; text-align: center;  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;">

<table width="100%" bgcolor="#fff" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#f0f1f1" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="padding:30px;"> 
				<tbody>
				  <tr> 
				    <td> 
					  <table  border="0" align="center" cellpadding="0" cellspacing="0" >
					     <tbody>
						    <tr> 
							   <td width="100%"> 
                                  <tbody>


<tr>
<td>
  <tbody>
    <tr>
      <td style="padding: 40px 0px 10px; font-size: 11pt; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #434343;" valign="top" align="center">
        <table cellpadding="0" cellspacing="0" data-padding-converted="true" width="366" align="center">
          <tbody>
            <tr>
              <td style="padding: 0px;" width="100%">
                <div align="center"><a track="on" href="https://www.concretecraft.com/" class="imgCaptionAnchor">
				<img alt="Logo" title="Logo" height="64" width="200" name="ACCOUNT.IMAGE.1208" vspace="0" hspace="0" style="display: block; max-width: 100%; height: auto;" border="0" src="{IMAGEPATH}concrete_logo.png"></a></div>
              </td>
            </tr>
			<tr>
		      <td height="20"></td>
	        </tr>
          </tbody>
        </table>
        <br></td>
    </tr>
  </tbody>
</td>
</tr>
<!--- end logo --->

<!--- start navigation code --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                            <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 80px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.concretecraft.com/stamped-concrete/" linktype="1" target="_blank">STAMPED</a></strong></div>
                            </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 80px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.concretecraft.com/resurfaced-concrete/" linktype="1" target="_blank">RESURFACED</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 80px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.concretecraft.com/stained-concrete/" linktype="1" target="_blank">STAINED</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 80px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.concretecraft.com/stamped-concrete/walls-fireplaces/" linktype="1" target="_blank">VERTICAL</a></strong></div>
                           </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end navigation code --->

<!--- start banner --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                              <td>
                                 <div align="center"><a track="on" href="https://www.concretecraft.com/" class="imgCaptionAnchor"><img width="644" name="ACCOUNT.IMAGE.1206" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}Thank-You-Hero.jpg"></a></div>
							  </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end banner code--->

<!--- body --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				   <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						<tbody>
						        <tr>
		                           <td height="35"></td>
	                            </tr>
	                            <tr>
	                				<td style="color:#69696d; text-align:center; font-size:40px; font-weight: normal;line-height: 2em;">
	                					Thank You for Your Inquiry!
	                				</td>
	    						</tr>
	    						<tr>
									<td height="12"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:16px; text-align: center; font-weight: normal;line-height:20px; padding:0 30px;">
	                					Our complimentary in-home consultation includes a discussion of your window covering needs, a review of samples and colors, free measuring and an on-the-spot quote.
	                				</td>
	    						</tr>
								<tr>
									<td height="50"></td>
								</tr>
								<tr>
									<td><img name="hborder" src="{IMAGEPATH}hborder.jpg" alt="hborder"/></td>
								</tr>
								<tr>
									<td height="40"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:16px; text-align: center;font-weight: normal;line-height:20px; padding:0 30px;">
	                					Our complimentary in-home consultation includes a discussion of your window covering needs, a review of samples and colors, free measuring and an on-the-spot quote.
	                				</td>
	    						</tr>
								<tr>
									<td height="25"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:16px; text-align: center; font-weight: normal;line-height:20px; padding:0 30px;">
	                					Our complimentary in-home consultation includes a discussion of your window covering needs, a review of samples and colors, free measuring and an on-the-spot quote.
	                				</td>
	    						</tr>
								<tr>
									<td height="50"></td>
								</tr>
	    						
								<!-- 3 column image grid--->
								<tr>
									<td>
										<table style="width:620px;" border="0" align="center" cellpadding="0" cellspacing="0">
											<tbody><tr>
												<td align="center">
													<a href="https://www.concretecraft.com/"><img src="{IMAGEPATH}Service-Grid-1.jpg" alt=" " style="width:190px;"/></a>
												</td>
												<td align="center">
													<a href="https://www.concretecraft.com/"><img src="{IMAGEPATH}Service-Grid-2.jpg" alt=" " style="width:190px;"/></a>
												</td>
												<td align="center">
													<a href="https://www.concretecraft.com/"><img src="{IMAGEPATH}Service-Grid-3.jpg" alt=" " style="width:190px;"/></a>
												</td>
													</tr>
											 </tbody></table>
									</td>
								</tr>
								<tr>
									<td height="20"></td>
								</tr>
								<!--- end 3 column image grid--->
							</tbody>
					  </table> 
					</td> 
				   
				   </tr>
				 </tbody>
		  </table>
	   </td>
	</tr>
   </tbody>
</table>
<!--- end body --->

<!--- start footer --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
			                  <td style="width:60px;">
				                 <p style="color:#fff; font-size:12px; text-transform:uppercase; line-height:17px; width:80px;">Franchise<br>Opportunities<br>available</p>
				              </td>
							  <td style="width:30px;"><img name="vborder" src="{IMAGEPATH}vborder.jpg" alt="vborder"/></td>
				              <td style="width:300px;">
				                 <p style="color:#fff; font-size:18px; text-transform:uppercase; line-height:18px; margin:15px 0 5px;">Be your own boss.</br> own a {BRAND} brand Franchise.</p>
				                 <p style="color:#fff; font-size:10px; line-height:14px; margin:5px 0;">This is not an offering to sell a franchise. Franchise offerings are made through a Franchise Disclosure Document.</p>
				              </td>
				              <td style="width:50px;">
				                  <a track="on" href="http://www.budget-blinds-franchise.com/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=franchise&amp;utm_campaign=Email-1stOctPromo2017" class="imgCaptionAnchor">
				                  <img  name="ACCOUNT.IMAGE.1203" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}learn-but.jpg"></a>
				              </td>
                           </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- start footer text --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				     <td width="100%"> 
					   <table width="644" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						 <tbody>
						    <tr>
							    <td height="20"></td>
							</tr>
							<tr>
                                <td style="padding: 0px 25px; font-size: 10pt; font-family: &quot;Lucida Sans Unicode&quot;, &quot;LucidaGrande&quot;, sans-serif; color: #69696d; line-height: 1.3;" valign="top" align="center">
                                   <div style="font-family: ''Century Gothic'', ''ITC Avant Garde'', Arial, Helvetica, sans-serif; font-size: 8pt;">
                                       <div style="text-align: left;" align="left"><span style="font-size: 8pt;">
                                          <div><span style="font-size:10pt;">This is a confirmation message only. No action is needed from you. {BRAND} Brand respects your privacy - view our <a style="text-decoration: underline;color: #3d7d8b;" href="http://www.concretecraft.com/privacy-policy/">Privacy Policy.</a></span></div>
                                          <div><span style="font-size: 8pt;"><br></span></div>
                                          <div><span style="font-size: 10pt;">&copy;2018 American Decorative Coatings, LLC. All Rights Reserved. <span>{BRAND} Brand&nbsp;</span>is a trademark of American Decorative Coatings,LLC and a Home Franchise Concepts Brand. Each franchise independently owned and operated. Franchise opportunities available.</span></div></span></div>
                                   </div>
                                </td>
	                        </tr>
						    <tr>
							    <td height="20"></td>
							</tr>
						 </tbody>
					   </table>
				     </td>
				  </tr>
			  </tbody>
		  </table>
	    </td>
	  </tr>
   </tbody>
</table>
<!--- end footer text --->

<!--- end footer --->
</tbody>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</body>
</html>
', 1, CAST(N'2018-12-31' AS Date), 3)


INSERT [CRM].[EmailTemplates] ([EmailTemplateId], [EmailTemplateTypeId], [Description], [TemplateSubject], [TemplateBody], [FranchiseId], [isDeleted], [TemplateLayout], [IsVisible], [IsVisibleAfterDate], [TemplateBrand]) VALUES (504, 2, N'Budget Blinds - Confirmation of Your Upcoming Appointment', N'Confirmation of Your Upcoming Appointment', N'
<html>
<head>
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
body{
	  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
       width:100% !important; 
	  -webkit-text-size-adjust:100%; 
	  -ms-text-size-adjust:100%;  
      margin:0; 
      padding:0; 
      -webkit-font-smoothing: antialiased;
      background:#fff;
}
html{
      width: 100%; 
}
table td, table td * {
      font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
}
</style>  
</head>
<body style="width: 100%; text-align: center;  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;">

<table width="100%" bgcolor="#fff" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#f0f1f1" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="padding:30px;"> 
				<tbody>
				  <tr> 
				    <td> 
					  <table  border="0" align="center" cellpadding="0" cellspacing="0" >
					     <tbody>
						    <tr> 
							   <td width="100%"> 
                                  <tbody>


<tr>
<td>
  <tbody>
    <tr>
      <td style="padding: 40px 0px 10px; font-size: 11pt; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #434343;" valign="top" align="center">
        <table cellpadding="0" cellspacing="0" data-padding-converted="true" width="366" align="center">
          <tbody>
            <tr>
              <td style="padding: 0px;" width="100%">
                <div align="center"><a track="on" href="http://www.budgetblinds.com/?utm_source=ConstantContact&amp;utm_medium=email&amp;utm_content=Logo&amp;utm_campaign=Email-1stOctPromo2017" class="imgCaptionAnchor">
				<img alt="Logo" title="Logo" height="64" width="200" name="ACCOUNT.IMAGE.1208" vspace="0" hspace="0" style="display: block; max-width: 100%; height: auto;" border="0" src="{IMAGEPATH}blinds_logo.png"></a></div>
              </td>
            </tr>
          </tbody>
        </table>
        <br></td>
    </tr>
  </tbody>
</td>
</tr>
<!--- end logo --->



<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">  
						<tbody>
                          <tr>
                            <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 80px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.budgetblinds.com/window-blinds/" linktype="1" target="_blank">Blinds</a></strong></div>
                            </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 80px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.budgetblinds.com/window-shutters/" linktype="1" target="_blank">Shutters</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 80px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.budgetblinds.com/window-shades/" linktype="1" target="_blank">Shades</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 80px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.budgetblinds.com/draperies/" linktype="1" target="_blank">Drapes & More</a></strong></div>
                           </td>
                          </tr>
                        </tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end email test code --->



<!--- body --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				   <td width="100%"> 
					  <table width="540" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						 
	<tr>
		<td height="20"></td>
	</tr>
	<tr>
		<td style="color:#69696d;font-size:14px; text-align: center; font-weight: normal;text-transform:uppercase;">
			Confirmation Appointment for:
		</td>
	</tr>
	<tr>
		<td height="15"></td>
	</tr>
	<tr>
		<td style="color:#3d7d8b;font-size:24px; text-align: center; font-weight:600;">
			{CLIENT}
		</td>
	</tr>
	<tr>
		<td style="color:#3d7d8b;font-size:15px; text-align: center; line-height: 20px; font-weight:600;">
			{ADDRESS}<br />  {EMAIL}
		</td>
	</tr>
	<tr>
		<td height="30"></td>
	</tr>
	<tr>
		<td style="color:#69696d;font-size:14px; text-align: center; font-weight: normal;text-transform:uppercase;">
			Date and time of Appointment:
		</td>
	</tr>
	<tr>
		<td height="15"></td>
	</tr>
	<tr>
		<td style="color:#3d7d8b;font-size:24px; text-align: center; font-weight:600;">
			{APPOINTMENTDATE}
		</td>
	</tr>
	<tr>
		<td height="30"></td>
	</tr>
	<tr>
		<td style="color:#69696d;font-size:14px; text-align: center; font-weight: normal;text-transform:uppercase;">
			with
		</td>
	</tr>
	<tr>
		<td height="15"></td>
	</tr>
	<tr>
		<td style="color:#3d7d8b; font-size:24px; text-align: center; font-weight:600;">
			{OWNER} <br />{BRAND} brand and territory title
		</td>
	</tr>
	<tr>
		<td height="12"></td>
	</tr>
	
	<tr>
		<td><divider style="background:#b7b7b9; width:100px; height:2px;display: inline-block; margin: 40px 0 20px;"></divider></td>
	</tr>
	
	<tr>
		<td height="25"></td>
	</tr>
	<tr>
		<td style="color:#69696d;font-size:21px; text-align: center; font-weight:600; text-transform:uppercase;">
			What to expect at your upcoming Appointment
		</td>
	</tr>
	<tr>
		<td height="20"></td>
	</tr>
	
	
	<!--- icons --->
	
	<tr>
		<td>
		
			<table style="text-align: center; width:644px;" border="0" cellspacing="0" cellpadding="0" align="center">

				<tbody><tr>
					<td>
					
						<table align="left" width="160" border="0" cellpadding="0" cellspacing="0" style="border-right:1px solid #dadada;">
				
							<tbody><tr>
								<td>
									
									<table style="text-align: center;" border="0" align="center" cellpadding="0" cellspacing="0">
									
										<tbody><tr>	
											<td align="center">
												<a class="imgCaptionAnchor" track="on" shape="rect" href="http://www.budgetblinds.com/shop-by-room/childrens-window-coverings/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=hero&amp;utm_campaign=Email-1stOctPromo2017"><img alt="Content" title="Content" src="{IMAGEPATH}design-icon.png" alt=" " width="50"></a>
												
											</td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:12px;color:#000;line-height:1.8em; text-transform:uppercase;">
												We design
											</td>
										</tr>
										<tr>
											 <td height="5"></td>
										</tr>
										<tr>
											<td align="center" style="font-size:11px;color:#888;line-height:1.5em; padding:0 10px;">
												Our talented consultants closely collaborate with you to design a unique solution that brings your personal style to life.
											</td>
										</tr>
									</tbody></table>
									
								</td>
								
							</tr>
						
						</tbody></table>
						<table align="left" width="160" border="0" cellpadding="0" cellspacing="0" style="border-right:1px solid #dadada;">
				
							<tbody><tr>
								<td>
									
									<table style="text-align: center;" border="0" align="center" cellpadding="0" cellspacing="0">
									
										<tbody><tr>	
											<td align="center">
												<a class="imgCaptionAnchor" track="on" shape="rect" href="http://www.budgetblinds.com/shop-by-room/childrens-window-coverings/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=hero&amp;utm_campaign=Email-1stOctPromo2017"><img alt="Contact" title="Contact" src="{IMAGEPATH}measure-icon.png" width="50"></a>
											</td>
										</tr>
										
										
										
										<tr>
											<td align="center" style="font-size:12px;color:#000;line-height:1.8em; text-transform:uppercase;">
												Web Measure
											</td>
										</tr>
										<tr>
											 <td height="5"></td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:11px;color:#888;line-height:1.5em; padding:0 10px;">
												Our talented consultants closely collaborate with you to design a unique solution that brings your personal style to life.
											</td>
										</tr>
										
									</tbody></table>
									
								</td>
							</tr>
						
						</tbody></table>
						<table align="left" width="160" border="0" cellpadding="0" cellspacing="0" style="border-right:1px solid #dadada;">
				
							<tbody><tr>
								<td>
									
									<table style="text-align: center;" border="0" align="center" cellpadding="0" cellspacing="0">
									
										<tbody><tr>	
											<td align="center">
												<a class="imgCaptionAnchor" track="on" shape="rect" href="http://www.budgetblinds.com/shop-by-room/childrens-window-coverings/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=hero&amp;utm_campaign=Email-1stOctPromo2017"><img src="{IMAGEPATH}install-icon.png" alt=" " width="50"></a>
											</td>
										</tr>
										
										
										<tr>
											<td align="center" style="font-size:12px;color:#000;line-height:1.8em; text-transform:uppercase;">
												Web Install
											</td>
										</tr>
										<tr>
											 <td height="5"></td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:11px;color:#888;line-height:1.5em; padding:0 10px;">
												Our talented consultants closely collaborate with you to design a unique solution that brings your personal style to life.
											</td>
										</tr>
									</tbody></table>
									
								</td>
							</tr>
						
						</tbody></table>
						
						
						<table align="left" width="160" border="0" cellpadding="0" cellspacing="0">
				
							<tbody><tr>
								<td>
									
									<table style="text-align: center;" border="0" align="center" cellpadding="0" cellspacing="0">
									
										<tbody><tr>	
											<td align="center">
												<a class="imgCaptionAnchor" track="on" shape="rect" href="http://www.budgetblinds.com/shop-by-room/childrens-window-coverings/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=hero&amp;utm_campaign=Email-1stOctPromo2017">
												<img src="{IMAGEPATH}celebrate-icon.png" alt=" " width="50"></a>
											</td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:12px;color:#000;line-height:1.8em; text-transform:uppercase;">
												you celebrate
											</td>
										</tr>
										<tr>
											 <td height="5"></td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:11px;color:#888;line-height:1.5em; padding:0 10px;">
												Our talented consultants closely collaborate with you to design a unique solution that brings your personal style to life.
											</td>
										</tr>
									</tbody></table>
									
								</td>
							</tr>
						
						</tbody></table>
						
					</td>
				</tr>
				
			</tbody></table>
			
		</td>
	</tr>
								
<!--- end icons --->
								
<tr>
	<td height="30"></td>
</tr>	    						
							
<!--- account information --->
<tr>
    <td>
	   <table id="textEdit" bgcolor="#fff" border="0" width="100%" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td>
        <table style="width:644px; border-style: none; border-width: 0px; text-align: center; height: auto;" align="center">
          <tbody>
            <tr>
              <td style="color:#69696d; padding: 0px; font-size:16px;">
                  <strong>questions </strong>Call or email {OWNER} at</br>{PHONE} | <a href="#" style="color:#69696d;">{EMAIL}</a>
              </td>
            </tr>
          </tbody>
        </table>
		
      </td>
    </tr>
  </tbody>
</table>

	</td>
  </tr>
<tr>
	<td height="10"></td>
</tr>
<!--- end account information --->

</td>
</tr>
</tbody>
					  </table> 
					</td> 
				   
				   </tr>
				 </tbody>
		  </table>
	   </td>
	</tr>
   </tbody>
</table>
<!--- end body --->
</tbody>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</body>
</html>
', NULL, 0, N'<html>
<head>
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
body{
	  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
       width:100% !important; 
	  -webkit-text-size-adjust:100%; 
	  -ms-text-size-adjust:100%;  
      margin:0; 
      padding:0; 
      -webkit-font-smoothing: antialiased;
      background:#fff;
}
html{
      width: 100%; 
}
table td, table td * {
      font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
}
</style>  
</head>
<body style="width: 100%; text-align: center;  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;">

<table width="100%" bgcolor="#fff" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#f0f1f1" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="padding:30px;"> 
				<tbody>
				  <tr> 
				    <td> 
					  <table  border="0" align="center" cellpadding="0" cellspacing="0" >
					     <tbody>
						    <tr> 
							   <td width="100%"> 
                                  <tbody>


<tr>
<td>
  <tbody>
    <tr>
      <td style="padding: 40px 0px 10px; font-size: 11pt; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #434343;" valign="top" align="center">
        <table cellpadding="0" cellspacing="0" data-padding-converted="true" width="366" align="center">
          <tbody>
            <tr>
              <td style="padding: 0px;" width="100%">
                <div align="center"><a track="on" href="http://www.budgetblinds.com/?utm_source=ConstantContact&amp;utm_medium=email&amp;utm_content=Logo&amp;utm_campaign=Email-1stOctPromo2017" class="imgCaptionAnchor">
				<img alt="Logo" title="Logo" height="64" width="200" name="ACCOUNT.IMAGE.1208" vspace="0" hspace="0" style="display: block; max-width: 100%; height: auto;" border="0" src="{IMAGEPATH}blinds_logo.png"></a></div>
              </td>
            </tr>
			<tr>
		      <td height="20"></td>
	        </tr>
          </tbody>
        </table>
        <br></td>
    </tr>
  </tbody>
</td>
</tr>
<!--- end logo --->

<!--- start navigation code --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                            <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.budgetblinds.com/window-blinds/" linktype="1" target="_blank">Blinds</a></strong></div>
                            </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.budgetblinds.com/window-shutters/" linktype="1" target="_blank">Shutters</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.budgetblinds.com/window-shades/" linktype="1" target="_blank">Shades</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 75px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.budgetblinds.com/draperies/" linktype="1" target="_blank">Drapes & More</a></strong></div>
                           </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end navigation code --->

<!--- start banner --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                              <td>
                                 <div align="center"><a track="on" href="http://www.budgetblinds.com/shop-by-room/childrens-window-coverings/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=hero&amp;utm_campaign=Email-1stOctPromo2017" class="imgCaptionAnchor"><img width="644" name="ACCOUNT.IMAGE.1206" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}Appt-Confirm-Hero.jpg"></a></div>
							  </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end banner code--->

<!--- body --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				   <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						<tbody>
						 
	<tr>
		<td height="35"></td>
	</tr>
	<tr>
		<td style="color:#69696d;font-size:14px; text-align: center; font-weight: normal;text-transform:uppercase;">
			Confirmation Appointment for:
		</td>
	</tr>
	<tr>
		<td height="15"></td>
	</tr>
	<tr>
		<td style="color:#3d7d8b;font-size:24px; text-align: center; font-weight:600;">
			{CLIENT}
		</td>
	</tr>
	<tr>
		<td style="color:#3d7d8b;font-size:15px; text-align: center; line-height: 20px; font-weight:600;">
			{ADDRESS}<br />  {EMAIL}
		</td>
	</tr>
	<tr>
		<td height="30"></td>
	</tr>
	<tr>
		<td style="color:#69696d;font-size:14px; text-align: center; font-weight: normal;text-transform:uppercase;">
			Date and time of Appointment:
		</td>
	</tr>
	<tr>
		<td height="15"></td>
	</tr>
	<tr>
		<td style="color:#3d7d8b;font-size:24px; text-align: center; font-weight:600;">
			{APPOINTMENTDATE}
		</td>
	</tr>
	<tr>
		<td height="30"></td>
	</tr>
	<tr>
		<td style="color:#69696d;font-size:14px; text-align: center; font-weight: normal;text-transform:uppercase;">
			with
		</td>
	</tr>
	<tr>
		<td height="15"></td>
	</tr>
	<tr>
		<td style="color:#3d7d8b; font-size:24px; text-align: center; font-weight:600;">
			{OWNER} <br />{BRAND} brand and territory title
		</td>
	</tr>
	<tr>
	    <td height="50"></td>
	</tr>
	<tr>
		<td><img name="hborder" src="{IMAGEPATH}hborder.jpg" alt="hborder"/></td>
	</tr>
	<tr>
		<td height="40"></td>
	</tr>
	<tr>
		<td style="color:#69696d;font-size:21px; text-align: center; font-weight:600; text-transform:uppercase;">
			What to expect at your upcoming Appointment
		</td>
	</tr>
	<tr>
		<td height="20"></td>
	</tr>
	
	
	<!--- icons --->
	
	<tr>
		<td>
		
			<table style="text-align: center; width:630px;" border="0" cellspacing="0" cellpadding="0" align="center">

				<tbody><tr>
					<td>
					
						<table align="left" width="148" border="0" cellpadding="0" cellspacing="0" style="border-right:1px solid #dadada;">
				
							<tbody><tr>
								<td>
									
									<table style="text-align: center;" border="0" align="center" cellpadding="0" cellspacing="0">
									
										<tbody><tr>	
											<td align="center">
												<a class="imgCaptionAnchor" track="on" shape="rect" href="http://www.budgetblinds.com/shop-by-room/childrens-window-coverings/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=hero&amp;utm_campaign=Email-1stOctPromo2017"><img alt="Content" title="Content" src="{IMAGEPATH}design-icon.png" alt=" " width="50"></a>
												
											</td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:12px;color:#000;line-height:1.8em; text-transform:uppercase;">
												We design
											</td>
										</tr>
										<tr>
											 <td height="5"></td>
										</tr>
										<tr>
											<td align="center" style="font-size:11px;color:#888;line-height:1.5em; padding:0 10px;">
												Our talented consultants closely collaborate with you to design a unique solution that brings your personal style to life.
											</td>
										</tr>
									</tbody></table>
									
								</td>
								
							</tr>
						
						</tbody></table>
						<table align="left" width="148" border="0" cellpadding="0" cellspacing="0" style="border-right:1px solid #dadada;">
				
							<tbody><tr>
								<td>
									
									<table style="text-align: center;" border="0" align="center" cellpadding="0" cellspacing="0">
									
										<tbody><tr>	
											<td align="center">
												<a track="on" shape="rect" href="http://www.budgetblinds.com/shop-by-room/childrens-window-coverings/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=hero&amp;utm_campaign=Email-1stOctPromo2017"><img alt="Contact" title="Contact" src="{IMAGEPATH}measure-icon.png" width="50"></a>
											</td>
										</tr>
										
										
										
										<tr>
											<td align="center" style="font-size:12px;color:#000;line-height:1.8em; text-transform:uppercase;">
												Web Measure
											</td>
										</tr>
										<tr>
											 <td height="5"></td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:11px;color:#888;line-height:1.5em; padding:0 10px;">
												Our talented consultants closely collaborate with you to design a unique solution that brings your personal style to life.
											</td>
										</tr>
										
									</tbody></table>
									
								</td>
							</tr>
						
						</tbody></table>
						<table align="left" width="148" border="0" cellpadding="0" cellspacing="0" style="border-right:1px solid #dadada;">
				
							<tbody><tr>
								<td>
									
									<table style="text-align: center;" border="0" align="center" cellpadding="0" cellspacing="0">
									
										<tbody><tr>	
											<td align="center">
												<a track="on" shape="rect" href="http://www.budgetblinds.com/shop-by-room/childrens-window-coverings/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=hero&amp;utm_campaign=Email-1stOctPromo2017"><img src="{IMAGEPATH}install-icon.png" alt=" " width="50"></a>
											</td>
										</tr>
										
										
										<tr>
											<td align="center" style="font-size:12px;color:#000;line-height:1.8em; text-transform:uppercase;">
												Web Install
											</td>
										</tr>
										<tr>
											 <td height="5"></td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:11px;color:#888;line-height:1.5em; padding:0 10px;">
												Our talented consultants closely collaborate with you to design a unique solution that brings your personal style to life.
											</td>
										</tr>
									</tbody></table>
									
								</td>
							</tr>
						
						</tbody></table>
						
						
						<table align="left" width="148" border="0" cellpadding="0" cellspacing="0">
				
							<tbody><tr>
								<td>
									
									<table style="text-align: center;" border="0" align="center" cellpadding="0" cellspacing="0">
									
										<tbody><tr>	
											<td align="center">
												<a track="on" shape="rect" href="http://www.budgetblinds.com/shop-by-room/childrens-window-coverings/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=hero&amp;utm_campaign=Email-1stOctPromo2017">
												<img src="{IMAGEPATH}celebrate-icon.png" alt=" " width="50"></a>
											</td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:12px;color:#000;line-height:1.8em; text-transform:uppercase;">
												you celebrate
											</td>
										</tr>
										<tr>
											 <td height="5"></td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:11px;color:#888;line-height:1.5em; padding:0 10px;">
												Our talented consultants closely collaborate with you to design a unique solution that brings your personal style to life.
											</td>
										</tr>
									</tbody></table>
									
								</td>
							</tr>
						
						</tbody></table>
						
					</td>
				</tr>
				
			</tbody></table>
			
		</td>
	</tr>
								
<!--- end icons --->
								
<tr>
	<td height="30"></td>
</tr>	    						
							
<!--- account information --->
<tr>
  <td>
	 <table bgcolor="#fff" border="0" width="100%" cellpadding="0" cellspacing="0">
       <tbody>
         <tr>
           <td>
             <table style="width:600px; border-style: none; border-width: 0px; text-align: center; height: auto;" align="center">
               <tbody>
                 <tr>
                   <td style="color:#69696d; padding: 0px; font-size:16px;">
                     <strong>Question before then?</strong>Call or email {OWNER} at</br>{PHONE} | <a href="#" style="color:#69696d;">{EMAIL}</a>
                   </td>
                 </tr>
               </tbody>
             </table>
	       </td>
         </tr>
        </tbody>
    </table>
  </td>
</tr>
<tr>
	<td height="20"></td>
</tr>
<!--- end account information --->

</td>
</tr>
</tbody>
					  </table> 
					</td> 
				   
				   </tr>
				 </tbody>
		  </table>
	   </td>
	</tr>
   </tbody>
</table>
<!--- end body --->

<!--- start footer --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
			                  <td style="width:60px;">
				                 <p style="color:#fff; font-size:12px; text-transform:uppercase; line-height:17px; width:80px;">Franchise<br>Opportunities<br>available</p>
				              </td>
							  <td style="width:30px;"><img name="vborder" src="{IMAGEPATH}vborder.jpg" alt="vborder"/></td>
				              <td style="width:300px;">
				                 <p style="color:#fff; font-size:18px; text-transform:uppercase; line-height:18px; margin:15px 0 5px;">Be your own boss.</br> own a {BRAND} brand Franchise.</p>
				                 <p style="color:#fff; font-size:10px; line-height:14px; margin:5px 0;">This is not an offering to sell a franchise. Franchise offerings are made through a Franchise Disclosure Document.</p>
				              </td>
				              <td style="width:50px;">
				                  <a track="on" href="http://www.budget-blinds-franchise.com/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=franchise&amp;utm_campaign=Email-1stOctPromo2017" class="imgCaptionAnchor">
				                  <img  name="ACCOUNT.IMAGE.1203" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}learn-but.jpg"></a>
				              </td>
                           </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- start footer text --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				     <td width="100%"> 
					   <table width="644" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						 <tbody>
						    <tr>
							    <td height="20"></td>
							</tr>
							<tr>
                                <td style="padding: 0px 25px; font-size: 10pt; font-family: &quot;Lucida Sans Unicode&quot;, &quot;LucidaGrande&quot;, sans-serif; color: #69696d; line-height: 1.3;" valign="top" align="center">
                                   <div style="font-family: ''Century Gothic'', ''ITC Avant Garde'', Arial, Helvetica, sans-serif; font-size: 8pt;">
                                       <div style="text-align: left;" align="left"><span style="font-size: 8pt;">
                                          <div><span style="font-size:10pt;">This is a confirmation message only. No action is needed from you. {BRAND} Brand respects your privacy - view our <a style="text-decoration: underline;color: #3d7d8b;" href="http://www.budgetblinds.com/privacy-policy/">Privacy Policy.</a></span></div>
                                          <div><span style="font-size: 8pt;"><br></span></div>
                                          <div><span style="font-size: 10pt;">&copy;2018 {BRAND}, LLC. All Rights Reserved. <span>{BRAND}</span>is a trademark of <span>{BRAND}</span>, LLC and a Home Franchise Concepts Brand. Each franchise independently owned and operated. Franchise opportunities available.</span></div></span></div>
                                   </div>
                                </td>
	                        </tr>
						    <tr>
							    <td height="20"></td>
							</tr>
						 </tbody>
					   </table>
				     </td>
				  </tr>
			  </tbody>
		  </table>
	    </td>
	  </tr>
   </tbody>
</table>
<!--- end footer text --->

<!--- end footer --->
</tbody>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</body>
</html>', 1, CAST(N'2018-12-31' AS Date), 1)



INSERT [CRM].[EmailTemplates] ([EmailTemplateId], [EmailTemplateTypeId], [Description], [TemplateSubject], [TemplateBody], [FranchiseId], [isDeleted], [TemplateLayout], [IsVisible], [IsVisibleAfterDate], [TemplateBrand]) VALUES (505, 2, N'Tailored Living - Confirmation of Your Upcoming Appointment', N'Confirmation of Your Upcoming Appointment', N'<div>
 <h1>
  Confirmation of Your Upcoming Appointment
 </h1>
</div>', NULL, 0, N'<html>
<head>
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
body{
	  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
       width:100% !important; 
	  -webkit-text-size-adjust:100%; 
	  -ms-text-size-adjust:100%;  
      margin:0; 
      padding:0; 
      -webkit-font-smoothing: antialiased;
      background:#fff;
}
html{
      width: 100%; 
}
table td, table td * {
      font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
}
</style>  
</head>
<body style="width: 100%; text-align: center;  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;">

<table width="100%" bgcolor="#fff" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#f0f1f1" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="padding:30px;"> 
				<tbody>
				  <tr> 
				    <td> 
					  <table  border="0" align="center" cellpadding="0" cellspacing="0" >
					     <tbody>
						    <tr> 
							   <td width="100%"> 
                                  <tbody>


<tr>
<td>
  <tbody>
    <tr>
      <td style="padding: 40px 0px 10px; font-size: 11pt; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #434343;" valign="top" align="center">
        <table cellpadding="0" cellspacing="0" data-padding-converted="true" width="366" align="center">
          <tbody>
            <tr>
              <td style="padding: 0px;" width="100%">
                <div align="center"><a track="on" href="http://www.tailoredliving.com/" class="imgCaptionAnchor">
				<img alt="Logo" title="Logo" height="64" width="200" name="ACCOUNT.IMAGE.1208" vspace="0" hspace="0" style="display: block; max-width: 100%; height: auto;" border="0" src="{IMAGEPATH}tailored_logo.png"></a></div>
              </td>
            </tr>
			<tr>
		      <td height="20"></td>
	        </tr>
          </tbody>
        </table>
        <br></td>
    </tr>
  </tbody>
</td>
</tr>
<!--- end logo --->

<!--- start navigation code --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                            <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.tailoredliving.com/closet-organization/" linktype="1" target="_blank">CLOSETS</a></strong></div>
                            </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.tailoredliving.com/garage-storage/" linktype="1" target="_blank">GARAGES</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.tailoredliving.com/home-office-design/" linktype="1" target="_blank">OFFICES</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 75px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.tailoredliving.com/home-organization/" linktype="1" target="_blank">PANTRIES & MORE</a></strong></div>
                           </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end navigation code --->

<!--- start banner --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                              <td>
                                 <div align="center"><a track="on" href="http://www.tailoredliving.com/" class="imgCaptionAnchor"><img width="644" name="ACCOUNT.IMAGE.1206" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}Appt-Confirm-Hero.jpg"></a></div>
							  </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end banner code--->

<!--- body --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				   <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						<tbody>
						        <tr>
		                           <td height="35"></td>
	                            </tr>
	                            <tr>
	                				<td style="color:#69696d;font-size:14px; text-align: center; font-weight: normal;text-transform:uppercase;">
	                					Confirmation Appointment for:
	                				</td>
	    						</tr>
								<tr>
									<td height="15"></td>
								</tr>
								<tr>
	                				<td style="color:#3d7d8b;font-size:24px; text-align: center; font-weight:600;">
	                					{CLIENT}
	                				</td>
	    						</tr>
								<tr>
	                				<td style="color:#3d7d8b;font-size:15px; text-align: center; line-height: 20px; font-weight:600;">
	                					{ADDRESS}<br />  {EMAIL}
	                				</td>
	    						</tr>
								<tr>
									<td height="30"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:14px; text-align: center; font-weight: normal;text-transform:uppercase;">
	                					Date and time of Appointment:
	                				</td>
	    						</tr>
								<tr>
									<td height="15"></td>
								</tr>
								<tr>
	                				<td style="color:#3d7d8b;font-size:24px; text-align: center; font-weight:600;">
	                					{APPOINTMENTDATE}
	                				</td>
	    						</tr>
								<tr>
									<td height="30"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:14px; text-align: center; font-weight: normal;text-transform:uppercase;">
	                					with
	                				</td>
	    						</tr>
								<tr>
									<td height="15"></td>
								</tr>
								<tr>
	                				<td style="color:#3d7d8b; font-size:24px; text-align: center; font-weight:600;">
	                					{OWNER} <br />{BRAND} brand and territory title
	                				</td>
	    						</tr>
	    						<tr>
									<td height="50"></td>
								</tr>
								<tr>
									<td><img name="hborder" src="{IMAGEPATH}hborder.jpg" alt="hborder"/></td>
								</tr>
								<tr>
									<td height="40"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:21px; text-align: center; font-weight:600; text-transform:uppercase;">
	                					What to expect at your upcoming Appointment
	                				</td>
	    						</tr>
								<tr>
									<td height="20"></td>
								</tr>
								<!--- icons --->
	
	<tr>
		<td>
		
			<table style="text-align: center; width:630px;" border="0" cellspacing="0" cellpadding="0" align="center">

				<tbody><tr>
					<td>
					
						<table align="left" width="148" border="0" cellpadding="0" cellspacing="0" style="border-right:1px solid #dadada;">
				
							<tbody><tr>
								<td>
									
									<table style="text-align: center;" border="0" align="center" cellpadding="0" cellspacing="0">
									
										<tbody><tr>	
											<td align="center">
												<a class="imgCaptionAnchor" track="on" shape="rect" href="http://www.tailoredliving.com/"><img alt="Content" title="Content" src="{IMAGEPATH}design-icon.png" alt=" " width="50"></a>
												
											</td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:12px;color:#000;line-height:1.8em; text-transform:uppercase;">
												We design
											</td>
										</tr>
										<tr>
											 <td height="5"></td>
										</tr>
										<tr>
											<td align="center" style="font-size:11px;color:#888;line-height:1.5em; padding:0 10px;">
												Our talented consultants closely collaborate with you to design a unique solution that brings your personal style to life.
											</td>
										</tr>
									</tbody></table>
									
								</td>
								
							</tr>
						
						</tbody></table>
						<table align="left" width="148" border="0" cellpadding="0" cellspacing="0" style="border-right:1px solid #dadada;">
				
							<tbody><tr>
								<td>
									
									<table style="text-align: center;" border="0" align="center" cellpadding="0" cellspacing="0">
									
										<tbody><tr>	
											<td align="center">
												<a track="on" shape="rect" href="http://www.tailoredliving.com/"><img alt="Contact" title="Contact" src="{IMAGEPATH}measure-icon.png" width="50"></a>
											</td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:12px;color:#000;line-height:1.8em; text-transform:uppercase;">
												Web Measure
											</td>
										</tr>
										<tr>
											 <td height="5"></td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:11px;color:#888;line-height:1.5em; padding:0 10px;">
												Our talented consultants closely collaborate with you to design a unique solution that brings your personal style to life.
											</td>
										</tr>
										
									</tbody></table>
									
								</td>
							</tr>
						
						</tbody></table>
						<table align="left" width="148" border="0" cellpadding="0" cellspacing="0" style="border-right:1px solid #dadada;">
				
							<tbody><tr>
								<td>
									
									<table style="text-align: center;" border="0" align="center" cellpadding="0" cellspacing="0">
									
										<tbody><tr>	
											<td align="center">
												<a track="on" shape="rect" href="http://www.tailoredliving.com/"><img src="{IMAGEPATH}install-icon.png" alt=" " width="50"></a>
											</td>
										</tr>
										
										
										<tr>
											<td align="center" style="font-size:12px;color:#000;line-height:1.8em; text-transform:uppercase;">
												Web Install
											</td>
										</tr>
										<tr>
											 <td height="5"></td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:11px;color:#888;line-height:1.5em; padding:0 10px;">
												Our talented consultants closely collaborate with you to design a unique solution that brings your personal style to life.
											</td>
										</tr>
									</tbody></table>
									
								</td>
							</tr>
						
						</tbody></table>
						
						
						<table align="left" width="148" border="0" cellpadding="0" cellspacing="0">
				
							<tbody><tr>
								<td>
									
									<table style="text-align: center;" border="0" align="center" cellpadding="0" cellspacing="0">
									
										<tbody><tr>	
											<td align="center">
												<a track="on" shape="rect" href="http://www.tailoredliving.com/">
												<img src="{IMAGEPATH}celebrate-icon.png" alt=" " width="50"></a>
											</td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:12px;color:#000;line-height:1.8em; text-transform:uppercase;">
												you celebrate
											</td>
										</tr>
										<tr>
											 <td height="5"></td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:11px;color:#888;line-height:1.5em; padding:0 10px;">
												Our talented consultants closely collaborate with you to design a unique solution that brings your personal style to life.
											</td>
										</tr>
									</tbody></table>
									
								</td>
							</tr>
						
						</tbody></table>
						
					</td>
				</tr>
				
			</tbody></table>
			
		</td>
	</tr>
								
<!--- end icons --->
<tr>
											 <td height="20"></td>
										</tr>
							</tbody>
					  </table> 
					</td> 
				   
				   </tr>
				 </tbody>
		  </table>
	   </td>
	</tr>
   </tbody>
</table>
<!--- end body --->

<!--- start footer --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
			                  <td style="width:60px;">
				                 <p style="color:#fff; font-size:12px; text-transform:uppercase; line-height:17px; width:80px;">Franchise<br>Opportunities<br>available</p>
				              </td>
							  <td style="width:30px;"><img name="vborder" src="{IMAGEPATH}vborder.jpg" alt="vborder"/></td>
				              <td style="width:300px;">
				                 <p style="color:#fff; font-size:18px; text-transform:uppercase; line-height:18px; margin:15px 0 5px;">Be your own boss.</br> own a {BRAND} brand Franchise.</p>
				                 <p style="color:#fff; font-size:10px; line-height:14px; margin:5px 0;">This is not an offering to sell a franchise. Franchise offerings are made through a Franchise Disclosure Document.</p>
				              </td>
				              <td style="width:50px;">
				                  <a track="on" href="http://www.budget-blinds-franchise.com/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=franchise&amp;utm_campaign=Email-1stOctPromo2017" class="imgCaptionAnchor">
				                  <img  name="ACCOUNT.IMAGE.1203" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}learn-but.jpg"></a>
				              </td>
                           </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- start footer text --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				     <td width="100%"> 
					   <table width="644" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						 <tbody>
						    <tr>
							    <td height="20"></td>
							</tr>
							<tr>
                                <td style="padding: 0px 25px; font-size: 10pt; font-family: &quot;Lucida Sans Unicode&quot;, &quot;LucidaGrande&quot;, sans-serif; color: #69696d; line-height: 1.3;" valign="top" align="center">
                                   <div style="font-family: ''Century Gothic'', ''ITC Avant Garde'', Arial, Helvetica, sans-serif; font-size: 8pt;">
                                       <div style="text-align: left;" align="left"><span style="font-size: 8pt;">
                                          <div><span style="font-size:10pt;">This is a confirmation message only. No action is needed from you. {BRAND} Brand respects your privacy - view our <a style="text-decoration: underline;color: #3d7d8b;" href="http://www.tailoredliving.com/privacy-policy/">Privacy Policy.</a></span></div>
                                          <div><span style="font-size: 8pt;"><br></span></div>
                                          <div><span style="font-size: 10pt;">&copy;2018 {BRAND} Brand. All Rights Reserved. <span>{BRAND}</span>featuring PremierGarage is a trademark of  <span>{BRAND}</span>, LLC and a Home Franchise Concepts Brand. Each franchise independently owned and operated. Franchise opportunities available.</span></div></span></div>
                                   </div>
                                </td>
	                        </tr>
						    <tr>
							    <td height="20"></td>
							</tr>
						 </tbody>
					   </table>
				     </td>
				  </tr>
			  </tbody>
		  </table>
	    </td>
	  </tr>
   </tbody>
</table>
<!--- end footer text --->

<!--- end footer --->
</tbody>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</body>
</html>
', 1, CAST(N'2018-12-31' AS Date), 2)


INSERT [CRM].[EmailTemplates] ([EmailTemplateId], [EmailTemplateTypeId], [Description], [TemplateSubject], [TemplateBody], [FranchiseId], [isDeleted], [TemplateLayout], [IsVisible], [IsVisibleAfterDate], [TemplateBrand]) VALUES (506, 2, N'Concrete Craft - Confirmation of Your Upcoming Appointment', N'Confirmation of Your Upcoming Appointment', N'<div>
 <h1>
  Confirmation of Your Upcoming Appointment
 </h1>
</div>', NULL, 0, N'<html>
<head>
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
body{
	  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
       width:100% !important; 
	  -webkit-text-size-adjust:100%; 
	  -ms-text-size-adjust:100%;  
      margin:0; 
      padding:0; 
      -webkit-font-smoothing: antialiased;
      background:#fff;
}
html{
      width: 100%; 
}
table td, table td * {
      font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
}
</style>  
</head>
<body style="width: 100%; text-align: center;  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;">

<table width="100%" bgcolor="#fff" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#f0f1f1" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="padding:30px;"> 
				<tbody>
				  <tr> 
				    <td> 
					  <table  border="0" align="center" cellpadding="0" cellspacing="0" >
					     <tbody>
						    <tr> 
							   <td width="100%"> 
                                  <tbody>


<tr>
<td>
  <tbody>
    <tr>
      <td style="padding: 40px 0px 10px; font-size: 11pt; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #434343;" valign="top" align="center">
        <table cellpadding="0" cellspacing="0" data-padding-converted="true" width="366" align="center">
          <tbody>
            <tr>
              <td style="padding: 0px;" width="100%">
                <div align="center"><a track="on" href="https://www.concretecraft.com/" class="imgCaptionAnchor">
				<img alt="Logo" title="Logo" height="64" width="200" name="ACCOUNT.IMAGE.1208" vspace="0" hspace="0" style="display: block; max-width: 100%; height: auto;" border="0" src="{IMAGEPATH}concrete_logo.png"></a></div>
              </td>
            </tr>
			<tr>
		      <td height="20"></td>
	        </tr>
          </tbody>
        </table>
        <br></td>
    </tr>
  </tbody>
</td>
</tr>
<!--- end logo --->

<!--- start navigation code --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                            <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 80px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.concretecraft.com/stamped-concrete/" linktype="1" target="_blank">STAMPED</a></strong></div>
                            </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 80px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.concretecraft.com/resurfaced-concrete/" linktype="1" target="_blank">RESURFACED</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 80px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.concretecraft.com/stained-concrete/" linktype="1" target="_blank">STAINED</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 80px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.concretecraft.com/stamped-concrete/walls-fireplaces/" linktype="1" target="_blank">VERTICAL</a></strong></div>
                           </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end navigation code --->

<!--- start banner --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                              <td>
                                 <div align="center"><a track="on" href="https://www.concretecraft.com/" class="imgCaptionAnchor"><img width="644" name="ACCOUNT.IMAGE.1206" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}Appt-Confirm-Hero.jpg"></a></div>
							  </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end banner code--->

<!--- body --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				   <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						<tbody>
						        <tr>
		                           <td height="35"></td>
	                            </tr>
	                            <tr>
	                				<td style="color:#69696d;font-size:14px; text-align: center; font-weight: normal;text-transform:uppercase;">
	                					Confirmation Appointment for:
	                				</td>
	    						</tr>
								<tr>
									<td height="15"></td>
								</tr>
								<tr>
	                				<td style="color:#3d7d8b;font-size:24px; text-align: center; font-weight:600;">
	                					{CLIENT}
	                				</td>
	    						</tr>
								<tr>
	                				<td style="color:#3d7d8b;font-size:15px; text-align: center; line-height: 20px; font-weight:600;">
	                					{ADDRESS}<br />  {EMAIL}
	                				</td>
	    						</tr>
								<tr>
									<td height="30"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:14px; text-align: center; font-weight: normal;text-transform:uppercase;">
	                					Date and time of Appointment:
	                				</td>
	    						</tr>
								<tr>
									<td height="15"></td>
								</tr>
								<tr>
	                				<td style="color:#3d7d8b;font-size:24px; text-align: center; font-weight:600;">
	                					{APPOINTMENTDATE}
	                				</td>
	    						</tr>
								<tr>
									<td height="30"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:14px; text-align: center; font-weight: normal;text-transform:uppercase;">
	                					with
	                				</td>
	    						</tr>
								<tr>
									<td height="15"></td>
								</tr>
								<tr>
	                				<td style="color:#3d7d8b; font-size:24px; text-align: center; font-weight:600;">
	                					{OWNER} <br />{BRAND} brand and territory title
	                				</td>
	    						</tr>
	    						<tr>
									<td height="50"></td>
								</tr>
								<tr>
									<td><img name="hborder" src="{IMAGEPATH}hborder.jpg" alt="hborder"/></td>
								</tr>
								<tr>
									<td height="40"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:21px; text-align: center; font-weight:600; text-transform:uppercase;">
	                					What to expect at your upcoming Appointment
	                				</td>
	    						</tr>
								<tr>
									<td height="20"></td>
								</tr>
								<!--- icons --->
	
	<tr>
		<td>
		
			<table style="text-align: center; width:630px;" border="0" cellspacing="0" cellpadding="0" align="center">

				<tbody><tr>
					<td>
					
						<table align="left" width="148" border="0" cellpadding="0" cellspacing="0" style="border-right:1px solid #dadada;">
				
							<tbody><tr>
								<td>
									
									<table style="text-align: center;" border="0" align="center" cellpadding="0" cellspacing="0">
									
										<tbody><tr>	
											<td align="center">
												<a class="imgCaptionAnchor" track="on" shape="rect" href="https://www.concretecraft.com/"><img alt="Content" title="Content" src="{IMAGEPATH}design-icon.png" alt=" " width="50"></a>
												
											</td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:12px;color:#000;line-height:1.8em; text-transform:uppercase;">
												We design
											</td>
										</tr>
										<tr>
											 <td height="5"></td>
										</tr>
										<tr>
											<td align="center" style="font-size:11px;color:#888;line-height:1.5em; padding:0 10px;">
												Our talented consultants closely collaborate with you to design a unique solution that brings your personal style to life.
											</td>
										</tr>
									</tbody></table>
									
								</td>
								
							</tr>
						
						</tbody></table>
						<table align="left" width="148" border="0" cellpadding="0" cellspacing="0" style="border-right:1px solid #dadada;">
				
							<tbody><tr>
								<td>
									
									<table style="text-align: center;" border="0" align="center" cellpadding="0" cellspacing="0">
									
										<tbody><tr>	
											<td align="center">
												<a track="on" shape="rect" href="https://www.concretecraft.com/"><img alt="Contact" title="Contact" src="{IMAGEPATH}measure-icon.png" width="50"></a>
											</td>
										</tr>
										
										
										
										<tr>
											<td align="center" style="font-size:12px;color:#000;line-height:1.8em; text-transform:uppercase;">
												Web Measure
											</td>
										</tr>
										<tr>
											 <td height="5"></td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:11px;color:#888;line-height:1.5em; padding:0 10px;">
												Our talented consultants closely collaborate with you to design a unique solution that brings your personal style to life.
											</td>
										</tr>
										
									</tbody></table>
									
								</td>
							</tr>
						
						</tbody></table>
						<table align="left" width="148" border="0" cellpadding="0" cellspacing="0" style="border-right:1px solid #dadada;">
				
							<tbody><tr>
								<td>
									
									<table style="text-align: center;" border="0" align="center" cellpadding="0" cellspacing="0">
									
										<tbody><tr>	
											<td align="center">
												<a track="on" shape="rect" href="https://www.concretecraft.com/"><img src="{IMAGEPATH}install-icon.png" alt=" " width="50"></a>
											</td>
										</tr>
										
										
										<tr>
											<td align="center" style="font-size:12px;color:#000;line-height:1.8em; text-transform:uppercase;">
												Web Install
											</td>
										</tr>
										<tr>
											 <td height="5"></td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:11px;color:#888;line-height:1.5em; padding:0 10px;">
												Our talented consultants closely collaborate with you to design a unique solution that brings your personal style to life.
											</td>
										</tr>
									</tbody></table>
									
								</td>
							</tr>
						
						</tbody></table>
						
						
						<table align="left" width="148" border="0" cellpadding="0" cellspacing="0">
				
							<tbody><tr>
								<td>
									
									<table style="text-align: center;" border="0" align="center" cellpadding="0" cellspacing="0">
									
										<tbody><tr>	
											<td align="center">
												<a track="on" shape="rect" href="https://www.concretecraft.com/">
												<img src="{IMAGEPATH}celebrate-icon.png" alt=" " width="50"></a>
											</td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:12px;color:#000;line-height:1.8em; text-transform:uppercase;">
												you celebrate
											</td>
										</tr>
										<tr>
											 <td height="5"></td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:11px;color:#888;line-height:1.5em; padding:0 10px;">
												Our talented consultants closely collaborate with you to design a unique solution that brings your personal style to life.
											</td>
										</tr>
									</tbody></table>
									
								</td>
							</tr>
						
						</tbody></table>
						
					</td>
				</tr>
				
			</tbody></table>
			
		</td>
	</tr>
								
<!--- end icons --->
    <tr>
		<td height="20"></td>
	</tr>
							</tbody>
					  </table> 
					</td> 
				   
				   </tr>
				 </tbody>
		  </table>
	   </td>
	</tr>
   </tbody>
</table>
<!--- end body --->

<!--- start footer --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
			                  <td style="width:60px;">
				                 <p style="color:#fff; font-size:12px; text-transform:uppercase; line-height:17px; width:80px;">Franchise<br>Opportunities<br>available</p>
				              </td>
							  <td style="width:30px;"><img name="vborder" src="{IMAGEPATH}vborder.jpg" alt="vborder"/></td>
				              <td style="width:300px;">
				                 <p style="color:#fff; font-size:18px; text-transform:uppercase; line-height:18px; margin:15px 0 5px;">Be your own boss.</br> own a {BRAND} brand Franchise.</p>
				                 <p style="color:#fff; font-size:10px; line-height:14px; margin:5px 0;">This is not an offering to sell a franchise. Franchise offerings are made through a Franchise Disclosure Document.</p>
				              </td>
				              <td style="width:50px;">
				                  <a track="on" href="http://www.budget-blinds-franchise.com/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=franchise&amp;utm_campaign=Email-1stOctPromo2017" class="imgCaptionAnchor">
				                  <img  name="ACCOUNT.IMAGE.1203" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}learn-but.jpg"></a>
				              </td>
                           </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- start footer text --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				     <td width="100%"> 
					   <table width="644" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						 <tbody>
						    <tr>
							    <td height="20"></td>
							</tr>
							<tr>
                                <td style="padding: 0px 25px; font-size: 10pt; font-family: &quot;Lucida Sans Unicode&quot;, &quot;LucidaGrande&quot;, sans-serif; color: #69696d; line-height: 1.3;" valign="top" align="center">
                                   <div style="font-family: ''Century Gothic'', ''ITC Avant Garde'', Arial, Helvetica, sans-serif; font-size: 8pt;">
                                       <div style="text-align: left;" align="left"><span style="font-size: 8pt;">
                                          <div><span style="font-size:10pt;">This is a confirmation message only. No action is needed from you. {BRAND} Brand respects your privacy - view our <a style="text-decoration: underline;color: #3d7d8b;" href="http://www.concretecraft.com/privacy-policy/">Privacy Policy.</a></span></div>
                                          <div><span style="font-size: 8pt;"><br></span></div>
                                          <div><span style="font-size: 10pt;">&copy;2018 American Decorative Coatings, LLC. All Rights Reserved. <span>{BRAND} Brand&nbsp;</span>is a trademark of American Decorative Coatings,LLC and a Home Franchise Concepts Brand. Each franchise independently owned and operated. Franchise opportunities available.</span></div></span></div>
                                   </div>
                                </td>
	                        </tr>
						    <tr>
							    <td height="20"></td>
							</tr>
						 </tbody>
					   </table>
				     </td>
				  </tr>
			  </tbody>
		  </table>
	    </td>
	  </tr>
   </tbody>
</table>
<!--- end footer text --->

<!--- end footer --->
</tbody>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</body>
</html>
', 1, CAST(N'2018-12-31' AS Date), 3)


INSERT [CRM].[EmailTemplates] ([EmailTemplateId], [EmailTemplateTypeId], [Description], [TemplateSubject], [TemplateBody], [FranchiseId], [isDeleted], [TemplateLayout], [IsVisible], [IsVisibleAfterDate], [TemplateBrand]) VALUES (507, 3, N'Budget Blinds - Reminder! You have an upcoming appointment.', N'Reminder! You have an upcoming appointment', N'<div>
 <h1>
  Reminder! You have an upcoming appointment.
 </h1>
</div>', NULL, 0, N'<html>
<head>
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
body{
	  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
       width:100% !important; 
	  -webkit-text-size-adjust:100%; 
	  -ms-text-size-adjust:100%;  
      margin:0; 
      padding:0; 
      -webkit-font-smoothing: antialiased;
      background:#fff;
}
html{
      width: 100%; 
}
table td, table td * {
      font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
}
</style>  
</head>
<body style="width: 100%; text-align: center;  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;">

<table width="100%" bgcolor="#fff" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#f0f1f1" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="padding:30px;"> 
				<tbody>
				  <tr> 
				    <td> 
					  <table  border="0" align="center" cellpadding="0" cellspacing="0" >
					     <tbody>
						    <tr> 
							   <td width="100%"> 
                                  <tbody>


<tr>
<td>
  <tbody>
    <tr>
      <td style="padding: 40px 0px 10px; font-size: 11pt; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #434343;" valign="top" align="center">
        <table cellpadding="0" cellspacing="0" data-padding-converted="true" width="366" align="center">
          <tbody>
            <tr>
              <td style="padding: 0px;" width="100%">
                <div align="center"><a track="on" href="http://www.budgetblinds.com/?utm_source=ConstantContact&amp;utm_medium=email&amp;utm_content=Logo&amp;utm_campaign=Email-1stOctPromo2017" class="imgCaptionAnchor">
				<img alt="Logo" title="Logo" height="64" width="200" name="ACCOUNT.IMAGE.1208" vspace="0" hspace="0" style="display: block; max-width: 100%; height: auto;" border="0" src="{IMAGEPATH}blinds_logo.png"></a></div>
              </td>
            </tr>
			<tr>
		      <td height="20"></td>
	        </tr>
          </tbody>
        </table>
        <br></td>
    </tr>
  </tbody>
</td>
</tr>
<!--- end logo --->

<!--- start navigation code --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                            <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.budgetblinds.com/window-blinds/" linktype="1" target="_blank">Blinds</a></strong></div>
                            </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.budgetblinds.com/window-shutters/" linktype="1" target="_blank">Shutters</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.budgetblinds.com/window-shades/" linktype="1" target="_blank">Shades</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 75px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.budgetblinds.com/draperies/" linktype="1" target="_blank">Drapes & More</a></strong></div>
                           </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end navigation code --->

<!--- start banner --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                              <td>
                                 <div align="center"><a track="on" href="http://www.budgetblinds.com/shop-by-room/childrens-window-coverings/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=hero&amp;utm_campaign=Email-1stOctPromo2017" class="imgCaptionAnchor"><img width="644" name="ACCOUNT.IMAGE.1206" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}Excited-Yet-Title.jpg"></a></div>
							  </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end banner code--->

<!--- body --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				   <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						<tbody>
						 
	<tr>
		<td height="35"></td>
	</tr>
	<tr>
	                				<td style="color:#69696d; text-align:center; font-size:32px; font-weight: normal;">
	                					Only 48 hours Until your</br>appointment!
	                				</td>
	    						</tr>
	    						<tr>
									<td height="20"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:16px; text-align: center; font-weight: normal;line-height:20px; padding:0 30px;">
	                					we are looking forward to helping you with your upcoming project.If you''re looking for ideas check out our <a href="http://docs.budgetblinds.com/odg/files/assets/basic-html/page-1.html#" style="font-style:italic; color:#69696d;">Design Guide</a> and get inspired! You''re in good hands with {BRAND} Brand!
	                				</td>
	    						</tr>
								<tr>
									<td height="40"></td>
								</tr>
								<tr>
									<td><img name="remindborder" src="{IMAGEPATH}remind-border.jpg" alt="remindborder"/></td>
								</tr>
								<tr>
									<td height="40"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:14px; text-align: center; font-weight: normal;text-transform:uppercase;">
	                					Confirmation Appointment for:
	                				</td>
	    						</tr>
								<tr>
									<td height="15"></td>
								</tr>
								<tr>
	                				<td style="color:#3d7d8b;font-size:24px; text-align: center; font-weight:600;">
	                					{CLIENT}
	                				</td>
	    						</tr>
								<tr>
	                				<td style="color:#3d7d8b;font-size:15px; text-align: center; line-height: 20px; font-weight:600;">
	                					{ADDRESS}
	                				</td>
	    						</tr>
								<tr>
									<td height="30"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:14px; text-align: center; font-weight: normal;text-transform:uppercase;">
	                					Date and time of Appointment:
	                				</td>
	    						</tr>
								<tr>
									<td height="15"></td>
								</tr>
								<tr>
	                				<td style="color:#3d7d8b;font-size:24px; text-align: center; font-weight:600;">
	                					{APPOINTMENTDATE}
	                				</td>
	    						</tr>
								<tr>
									<td height="30"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:14px; text-align: center; font-weight: normal;text-transform:uppercase;">
	                					with
	                				</td>
	    						</tr>
								<tr>
									<td height="15"></td>
								</tr>
								<tr>
	                				<td style="color:#3d7d8b; font-size:24px; text-align: center; font-weight:600;">
	                					{OWNER} </br>{BRAND} brand and territory title
	                				</td>
	    						</tr>
	    						<tr>
									<td height="50"></td>
								</tr>
								<tr>
									<td><img name="remindborder" src="{IMAGEPATH}remind-border.jpg" alt="remindborder"/></td>
								</tr>
								<tr>
									<td height="40"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:21px; text-align: center; font-weight:600; text-transform:uppercase;">
	                					What to expect
	                				</td>
	    						</tr>
								<tr>
									<td height="20"></td>
								</tr>
	
	
	<!--- icons --->
	
	<tr>
		<td>
		
			<table style="text-align: center; width:630px;" border="0" cellspacing="0" cellpadding="0" align="center">

				<tbody><tr>
					<td>
					
						<table align="left" width="148" border="0" cellpadding="0" cellspacing="0" style="border-right:1px solid #dadada;">
				
							<tbody><tr>
								<td>
									
									<table style="text-align: center;" border="0" align="center" cellpadding="0" cellspacing="0">
									
										<tbody><tr>	
											<td align="center">
												<a class="imgCaptionAnchor" track="on" shape="rect" href="http://www.budgetblinds.com/shop-by-room/childrens-window-coverings/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=hero&amp;utm_campaign=Email-1stOctPromo2017"><img alt="Content" title="Content" src="{IMAGEPATH}design-icon.png" alt=" " width="50"></a>
												
											</td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:12px;color:#000;line-height:1.8em; text-transform:uppercase;">
												We design
											</td>
										</tr>
										<tr>
											 <td height="5"></td>
										</tr>
										<tr>
											<td align="center" style="font-size:11px;color:#888;line-height:1.5em; padding:0 10px;">
												Our talented consultants closely collaborate with you to design a unique solution that brings your personal style to life.
											</td>
										</tr>
									</tbody></table>
									
								</td>
								
							</tr>
						
						</tbody></table>
						<table align="left" width="148" border="0" cellpadding="0" cellspacing="0" style="border-right:1px solid #dadada;">
				
							<tbody><tr>
								<td>
									
									<table style="text-align: center;" border="0" align="center" cellpadding="0" cellspacing="0">
									
										<tbody><tr>	
											<td align="center">
												<a track="on" shape="rect" href="http://www.budgetblinds.com/shop-by-room/childrens-window-coverings/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=hero&amp;utm_campaign=Email-1stOctPromo2017"><img alt="Contact" title="Contact" src="{IMAGEPATH}measure-icon.png" width="50"></a>
											</td>
										</tr>
										
										
										
										<tr>
											<td align="center" style="font-size:12px;color:#000;line-height:1.8em; text-transform:uppercase;">
												Web Measure
											</td>
										</tr>
										<tr>
											 <td height="5"></td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:11px;color:#888;line-height:1.5em; padding:0 10px;">
												Our talented consultants closely collaborate with you to design a unique solution that brings your personal style to life.
											</td>
										</tr>
										
									</tbody></table>
									
								</td>
							</tr>
						
						</tbody></table>
						<table align="left" width="148" border="0" cellpadding="0" cellspacing="0" style="border-right:1px solid #dadada;">
				
							<tbody><tr>
								<td>
									
									<table style="text-align: center;" border="0" align="center" cellpadding="0" cellspacing="0">
									
										<tbody><tr>	
											<td align="center">
												<a track="on" shape="rect" href="http://www.budgetblinds.com/shop-by-room/childrens-window-coverings/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=hero&amp;utm_campaign=Email-1stOctPromo2017"><img src="{IMAGEPATH}install-icon.png" alt=" " width="50"></a>
											</td>
										</tr>
										
										
										<tr>
											<td align="center" style="font-size:12px;color:#000;line-height:1.8em; text-transform:uppercase;">
												Web Install
											</td>
										</tr>
										<tr>
											 <td height="5"></td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:11px;color:#888;line-height:1.5em; padding:0 10px;">
												Our talented consultants closely collaborate with you to design a unique solution that brings your personal style to life.
											</td>
										</tr>
									</tbody></table>
									
								</td>
							</tr>
						
						</tbody></table>
						
						
						<table align="left" width="148" border="0" cellpadding="0" cellspacing="0">
				
							<tbody><tr>
								<td>
									
									<table style="text-align: center;" border="0" align="center" cellpadding="0" cellspacing="0">
									
										<tbody><tr>	
											<td align="center">
												<a track="on" shape="rect" href="http://www.budgetblinds.com/shop-by-room/childrens-window-coverings/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=hero&amp;utm_campaign=Email-1stOctPromo2017">
												<img src="{IMAGEPATH}celebrate-icon.png" alt=" " width="50"></a>
											</td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:12px;color:#000;line-height:1.8em; text-transform:uppercase;">
												you celebrate
											</td>
										</tr>
										<tr>
											 <td height="5"></td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:11px;color:#888;line-height:1.5em; padding:0 10px;">
												Our talented consultants closely collaborate with you to design a unique solution that brings your personal style to life.
											</td>
										</tr>
									</tbody></table>
									
								</td>
							</tr>
						
						</tbody></table>
						
					</td>
				</tr>
				
			</tbody></table>
			
		</td>
	</tr>
								
<!--- end icons --->
								
<tr>
	<td height="30"></td>
</tr>	    						
							
<!--- account information --->
<tr>
  <td>
	 <table bgcolor="#fff" border="0" width="100%" cellpadding="0" cellspacing="0">
       <tbody>
         <tr>
           <td>
             <table style="width:600px; border-style: none; border-width: 0px; text-align: center; height: auto;" align="center">
               <tbody>
                 <tr>
                   <td style="color:#69696d; padding: 0px; font-size:16px;">
                     <strong>Question before then?</strong>Call or email {OWNER} at</br>{PHONE} | <a href="#" style="color:#69696d;">{EMAIL}</a>
                   </td>
                 </tr>
               </tbody>
             </table>
	       </td>
         </tr>
        </tbody>
    </table>
  </td>
</tr>
<tr>
	<td height="20"></td>
</tr>
<!--- end account information --->

</td>
</tr>
</tbody>
					  </table> 
					</td> 
				   
				   </tr>
				 </tbody>
		  </table>
	   </td>
	</tr>
   </tbody>
</table>
<!--- end body --->

<!--- start footer --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
			                  <td style="width:60px;">
				                 <p style="color:#fff; font-size:12px; text-transform:uppercase; line-height:17px; width:80px;">Franchise<br>Opportunities<br>available</p>
				              </td>
							  <td style="width:30px;"><img name="vborder" src="{IMAGEPATH}vborder.jpg" alt="vborder"/></td>
				              <td style="width:300px;">
				                 <p style="color:#fff; font-size:18px; text-transform:uppercase; line-height:18px; margin:15px 0 5px;">Be your own boss.</br> own a {BRAND} brand Franchise.</p>
				                 <p style="color:#fff; font-size:10px; line-height:14px; margin:5px 0;">This is not an offering to sell a franchise. Franchise offerings are made through a Franchise Disclosure Document.</p>
				              </td>
				              <td style="width:50px;">
				                  <a track="on" href="http://www.budget-blinds-franchise.com/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=franchise&amp;utm_campaign=Email-1stOctPromo2017" class="imgCaptionAnchor">
				                  <img  name="ACCOUNT.IMAGE.1203" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}learn-but.jpg"></a>
				              </td>
                           </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- start footer text --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				     <td width="100%"> 
					   <table width="644" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						 <tbody>
						    <tr>
							    <td height="20"></td>
							</tr>
							<tr>
                                <td style="padding: 0px 25px; font-size: 10pt; font-family: &quot;Lucida Sans Unicode&quot;, &quot;LucidaGrande&quot;, sans-serif; color: #69696d; line-height: 1.3;" valign="top" align="center">
                                   <div style="font-family: ''Century Gothic'', ''ITC Avant Garde'', Arial, Helvetica, sans-serif; font-size: 8pt;">
                                       <div style="text-align: left;" align="left"><span style="font-size: 8pt;">
                                          <div><span style="font-size:10pt;">This is a confirmation message only. No action is needed from you. {BRAND} Brand respects your privacy - view our <a style="text-decoration: underline;color: #3d7d8b;" href="http://www.budgetblinds.com/privacy-policy/">Privacy Policy.</a></span></div>
                                          <div><span style="font-size: 8pt;"><br></span></div>
                                          <div><span style="font-size: 10pt;">&copy;2018 {BRAND}, LLC. All Rights Reserved. <span>{BRAND}</span>is a trademark of <span>{BRAND}</span>, LLC and a Home Franchise Concepts Brand. Each franchise independently owned and operated. Franchise opportunities available.</span></div></span></div>
                                   </div>
                                </td>
	                        </tr>
						    <tr>
							    <td height="20"></td>
							</tr>
						 </tbody>
					   </table>
				     </td>
				  </tr>
			  </tbody>
		  </table>
	    </td>
	  </tr>
   </tbody>
</table>
<!--- end footer text --->

<!--- end footer --->
</tbody>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</body>
</html>', 1, CAST(N'2018-12-31' AS Date), 1)


INSERT [CRM].[EmailTemplates] ([EmailTemplateId], [EmailTemplateTypeId], [Description], [TemplateSubject], [TemplateBody], [FranchiseId], [isDeleted], [TemplateLayout], [IsVisible], [IsVisibleAfterDate], [TemplateBrand]) VALUES (508, 3, N'Tailored Living - Reminder! You have an upcoming appointment.', N'Reminder! You have an upcoming appointment', N'<div>
 <h1>
  Reminder! You have an upcoming appointment.
 </h1>
</div>', NULL, 0, N'<html>
<head>
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
body{
	  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
       width:100% !important; 
	  -webkit-text-size-adjust:100%; 
	  -ms-text-size-adjust:100%;  
      margin:0; 
      padding:0; 
      -webkit-font-smoothing: antialiased;
      background:#fff;
}
html{
      width: 100%; 
}
table td, table td * {
      font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
}
</style>  
</head>
<body style="width: 100%; text-align: center;  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;">

<table width="100%" bgcolor="#fff" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#f0f1f1" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="padding:30px;"> 
				<tbody>
				  <tr> 
				    <td> 
					  <table  border="0" align="center" cellpadding="0" cellspacing="0" >
					     <tbody>
						    <tr> 
							   <td width="100%"> 
                                  <tbody>


<tr>
<td>
  <tbody>
    <tr>
      <td style="padding: 40px 0px 10px; font-size: 11pt; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #434343;" valign="top" align="center">
        <table cellpadding="0" cellspacing="0" data-padding-converted="true" width="366" align="center">
          <tbody>
            <tr>
              <td style="padding: 0px;" width="100%">
                <div align="center"><a track="on" href="http://www.tailoredliving.com/" class="imgCaptionAnchor">
				<img alt="Logo" title="Logo" height="64" width="200" name="ACCOUNT.IMAGE.1208" vspace="0" hspace="0" style="display: block; max-width: 100%; height: auto;" border="0" src="{IMAGEPATH}tailored_logo.png"></a></div>
              </td>
            </tr>
			<tr>
		      <td height="20"></td>
	        </tr>
          </tbody>
        </table>
        <br></td>
    </tr>
  </tbody>
</td>
</tr>
<!--- end logo --->

<!--- start navigation code --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                            <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.tailoredliving.com/closet-organization/" linktype="1" target="_blank">CLOSETS</a></strong></div>
                            </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.tailoredliving.com/garage-storage/" linktype="1" target="_blank">GARAGES</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.tailoredliving.com/home-office-design/" linktype="1" target="_blank">OFFICES</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 75px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.tailoredliving.com/home-organization/" linktype="1" target="_blank">PANTRIES & MORE</a></strong></div>
                           </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end navigation code --->

<!--- start banner --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                              <td>
                                 <div align="center"><a track="on" href="http://www.tailoredliving.com/" class="imgCaptionAnchor"><img width="644" name="ACCOUNT.IMAGE.1206" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}Excited-Yet-Title.jpg"></a></div>
							  </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end banner code--->

<!--- body --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				   <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						<tbody>
						        <tr>
		                           <td height="35"></td>
	                            </tr>
	                            <tr>
	                				<td style="color:#69696d; text-align:center; font-size:32px; font-weight: normal;">
	                					Only 48 hours Until your</br>appointment!
	                				</td>
	    						</tr>
	    						<tr>
									<td height="20"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:16px; text-align: center; font-weight: normal;line-height:20px; padding:0 30px;">
	                					we are looking forward to helping you with your upcoming project.If you''re looking for ideas check out our <a href="http://docs.budgetblinds.com/odg/files/assets/basic-html/page-1.html#" style="font-style:italic; color:#69696d;">Design Guide</a> and get inspired! You''re in good hands with xx Brand!
	                				</td>
	    						</tr>
								<tr>
									<td height="40"></td>
								</tr>
								<tr>
									<td><img name="remindborder" src="{IMAGEPATH}remind-border.jpg" alt="remindborder"/></td>
								</tr>
								<tr>
									<td height="40"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:14px; text-align: center; font-weight: normal;text-transform:uppercase;">
	                					Confirmation Appointment for:
	                				</td>
	    						</tr>
								<tr>
									<td height="15"></td>
								</tr>
								<tr>
	                				<td style="color:#3d7d8b;font-size:24px; text-align: center; font-weight:600;">
	                					{CLIENT}
	                				</td>
	    						</tr>
								<tr>
	                				<td style="color:#3d7d8b;font-size:15px; text-align: center; line-height: 20px; font-weight:600;">
	                					{ADDRESS}
	                				</td>
	    						</tr>
								<tr>
									<td height="30"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:14px; text-align: center; font-weight: normal;text-transform:uppercase;">
	                					Date and time of Appointment:
	                				</td>
	    						</tr>
								<tr>
									<td height="15"></td>
								</tr>
								<tr>
	                				<td style="color:#3d7d8b;font-size:24px; text-align: center; font-weight:600;">
	                					{APPOINTMENTDATE}
	                				</td>
	    						</tr>
								<tr>
									<td height="30"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:14px; text-align: center; font-weight: normal;text-transform:uppercase;">
	                					with
	                				</td>
	    						</tr>
								<tr>
									<td height="15"></td>
								</tr>
								<tr>
	                				<td style="color:#3d7d8b; font-size:24px; text-align: center; font-weight:600;">
	                					{OWNER} </br>{BRAND} brand and territory title
	                				</td>
	    						</tr>
	    						<tr>
									<td height="50"></td>
								</tr>
								<tr>
									<td><img name="remindborder" src="{IMAGEPATH}remind-border.jpg" alt="remindborder"/></td>
								</tr>
								<tr>
									<td height="40"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:21px; text-align: center; font-weight:600; text-transform:uppercase;">
	                					What to expect
	                				</td>
	    						</tr>
								<tr>
									<td height="20"></td>
								</tr>
								<!--- icons --->
	
	<tr>
		<td>
		
			<table style="text-align: center; width:630px;" border="0" cellspacing="0" cellpadding="0" align="center">

				<tbody><tr>
					<td>
					
						<table align="left" width="148" border="0" cellpadding="0" cellspacing="0" style="border-right:1px solid #dadada;">
				
							<tbody><tr>
								<td>
									
									<table style="text-align: center;" border="0" align="center" cellpadding="0" cellspacing="0">
									
										<tbody><tr>	
											<td align="center">
												<a class="imgCaptionAnchor" track="on" shape="rect" href="http://www.tailoredliving.com/"><img alt="Content" title="Content" src="{IMAGEPATH}design-icon.png" alt=" " width="50"></a>
												
											</td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:12px;color:#000;line-height:1.8em; text-transform:uppercase;">
												We design
											</td>
										</tr>
										<tr>
											 <td height="5"></td>
										</tr>
										<tr>
											<td align="center" style="font-size:11px;color:#888;line-height:1.5em; padding:0 10px;">
												Our talented consultants closely collaborate with you to design a unique solution that brings your personal style to life.
											</td>
										</tr>
									</tbody></table>
									
								</td>
								
							</tr>
						
						</tbody></table>
						<table align="left" width="148" border="0" cellpadding="0" cellspacing="0" style="border-right:1px solid #dadada;">
				
							<tbody><tr>
								<td>
									
									<table style="text-align: center;" border="0" align="center" cellpadding="0" cellspacing="0">
									
										<tbody><tr>	
											<td align="center">
												<a track="on" shape="rect" href="http://www.tailoredliving.com/"><img alt="Contact" title="Contact" src="{IMAGEPATH}measure-icon.png" width="50"></a>
											</td>
										</tr>								
										
										<tr>
											<td align="center" style="font-size:12px;color:#000;line-height:1.8em; text-transform:uppercase;">
												Web Measure
											</td>
										</tr>
										<tr>
											 <td height="5"></td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:11px;color:#888;line-height:1.5em; padding:0 10px;">
												Our talented consultants closely collaborate with you to design a unique solution that brings your personal style to life.
											</td>
										</tr>
										
									</tbody></table>
									
								</td>
							</tr>
						
						</tbody></table>
						<table align="left" width="148" border="0" cellpadding="0" cellspacing="0" style="border-right:1px solid #dadada;">
				
							<tbody><tr>
								<td>
									
									<table style="text-align: center;" border="0" align="center" cellpadding="0" cellspacing="0">
									
										<tbody><tr>	
											<td align="center">
												<a track="on" shape="rect" href="http://www.tailoredliving.com/"><img src="{IMAGEPATH}install-icon.png" alt=" " width="50"></a>
											</td>
										</tr>
										
										
										<tr>
											<td align="center" style="font-size:12px;color:#000;line-height:1.8em; text-transform:uppercase;">
												Web Install
											</td>
										</tr>
										<tr>
											 <td height="5"></td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:11px;color:#888;line-height:1.5em; padding:0 10px;">
												Our talented consultants closely collaborate with you to design a unique solution that brings your personal style to life.
											</td>
										</tr>
									</tbody></table>
									
								</td>
							</tr>
						
						</tbody></table>
						
						
						<table align="left" width="148" border="0" cellpadding="0" cellspacing="0">
				
							<tbody><tr>
								<td>
									
									<table style="text-align: center;" border="0" align="center" cellpadding="0" cellspacing="0">
									
										<tbody><tr>	
											<td align="center">
												<a track="on" shape="rect" href="http://www.tailoredliving.com/">
												<img src="{IMAGEPATH}celebrate-icon.png" alt=" " width="50"></a>
											</td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:12px;color:#000;line-height:1.8em; text-transform:uppercase;">
												you celebrate
											</td>
										</tr>
										<tr>
											 <td height="5"></td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:11px;color:#888;line-height:1.5em; padding:0 10px;">
												Our talented consultants closely collaborate with you to design a unique solution that brings your personal style to life.
											</td>
										</tr>
									</tbody></table>
									
								</td>
							</tr>
						
						</tbody></table>
						
					</td>
				</tr>
				
			</tbody></table>
			
		</td>
	</tr>
								
<!--- end icons --->
<tr>
											 <td height="20"></td>
										</tr>
							</tbody>
					  </table> 
					</td> 
				   
				   </tr>
				 </tbody>
		  </table>
	   </td>
	</tr>
   </tbody>
</table>
<!--- end body --->

<!--- start footer --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
			                  <td style="width:60px;">
				                 <p style="color:#fff; font-size:12px; text-transform:uppercase; line-height:17px; width:80px;">Franchise<br>Opportunities<br>available</p>
				              </td>
							  <td style="width:30px;"><img name="vborder" src="{IMAGEPATH}vborder.jpg" alt="vborder"/></td>
				              <td style="width:300px;">
				                 <p style="color:#fff; font-size:18px; text-transform:uppercase; line-height:18px; margin:15px 0 5px;">Be your own boss.</br> own a {BRAND} brand Franchise.</p>
				                 <p style="color:#fff; font-size:10px; line-height:14px; margin:5px 0;">This is not an offering to sell a franchise. Franchise offerings are made through a Franchise Disclosure Document.</p>
				              </td>
				              <td style="width:50px;">
				                  <a track="on" href="http://www.budget-blinds-franchise.com/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=franchise&amp;utm_campaign=Email-1stOctPromo2017" class="imgCaptionAnchor">
				                  <img  name="ACCOUNT.IMAGE.1203" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}learn-but.jpg"></a>
				              </td>
                           </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- start footer text --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				     <td width="100%"> 
					   <table width="644" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						 <tbody>
						    <tr>
							    <td height="20"></td>
							</tr>
							<tr>
                                <td style="padding: 0px 25px; font-size: 10pt; font-family: &quot;Lucida Sans Unicode&quot;, &quot;LucidaGrande&quot;, sans-serif; color: #69696d; line-height: 1.3;" valign="top" align="center">
                                   <div style="font-family: ''Century Gothic'', ''ITC Avant Garde'', Arial, Helvetica, sans-serif; font-size: 8pt;">
                                       <div style="text-align: left;" align="left"><span style="font-size: 8pt;">
                                          <div><span style="font-size:10pt;">This is a confirmation message only. No action is needed from you. {BRAND} Brand respects your privacy - view our <a style="text-decoration: underline;color: #3d7d8b;" href="http://www.tailoredliving.com/privacy-policy/">Privacy Policy.</a></span></div>
                                          <div><span style="font-size: 8pt;"><br></span></div>
                                          <div><span style="font-size: 10pt;">&copy;2018 {BRAND} Brand. All Rights Reserved. <span>{BRAND}</span>featuring PremierGarage is a trademark of  <span>{BRAND}</span>, LLC and a Home Franchise Concepts Brand. Each franchise independently owned and operated. Franchise opportunities available.</span></div></span></div>
                                   </div>
                                </td>
	                        </tr>
						    <tr>
							    <td height="20"></td>
							</tr>
						 </tbody>
					   </table>
				     </td>
				  </tr>
			  </tbody>
		  </table>
	    </td>
	  </tr>
   </tbody>
</table>
<!--- end footer text --->

<!--- end footer --->
</tbody>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</body>
</html>
', 1, CAST(N'2018-12-31' AS Date), 2)


INSERT [CRM].[EmailTemplates] ([EmailTemplateId], [EmailTemplateTypeId], [Description], [TemplateSubject], [TemplateBody], [FranchiseId], [isDeleted], [TemplateLayout], [IsVisible], [IsVisibleAfterDate], [TemplateBrand]) VALUES (509, 3, N'Concrete Craft - Reminder! You have an upcoming appointment.', N'Reminder! You have an upcoming appointment', N'<div>
 <h1>
  Reminder! You have an upcoming appointment.
 </h1>
</div>', NULL, 0, N'<html>
<head>
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
body{
	  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
       width:100% !important; 
	  -webkit-text-size-adjust:100%; 
	  -ms-text-size-adjust:100%;  
      margin:0; 
      padding:0; 
      -webkit-font-smoothing: antialiased;
      background:#fff;
}
html{
      width: 100%; 
}
table td, table td * {
      font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
}
</style>  
</head>
<body style="width: 100%; text-align: center;  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;">

<table width="100%" bgcolor="#fff" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#f0f1f1" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="padding:30px;"> 
				<tbody>
				  <tr> 
				    <td> 
					  <table  border="0" align="center" cellpadding="0" cellspacing="0" >
					     <tbody>
						    <tr> 
							   <td width="100%"> 
                                  <tbody>


<tr>
<td>
  <tbody>
    <tr>
      <td style="padding: 40px 0px 10px; font-size: 11pt; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #434343;" valign="top" align="center">
        <table cellpadding="0" cellspacing="0" data-padding-converted="true" width="366" align="center">
          <tbody>
            <tr>
              <td style="padding: 0px;" width="100%">
                <div align="center"><a track="on" href="https://www.concretecraft.com/" class="imgCaptionAnchor">
				<img alt="Logo" title="Logo" height="64" width="200" name="ACCOUNT.IMAGE.1208" vspace="0" hspace="0" style="display: block; max-width: 100%; height: auto;" border="0" src="{IMAGEPATH}concrete_logo.png"></a></div>
              </td>
            </tr>
			<tr>
		      <td height="20"></td>
	        </tr>
          </tbody>
        </table>
        <br></td>
    </tr>
  </tbody>
</td>
</tr>
<!--- end logo --->


<!--- start navigation code --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                            <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 80px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.concretecraft.com/stamped-concrete/" linktype="1" target="_blank">STAMPED</a></strong></div>
                            </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 80px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.concretecraft.com/resurfaced-concrete/" linktype="1" target="_blank">RESURFACED</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 80px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.concretecraft.com/stained-concrete/" linktype="1" target="_blank">STAINED</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 80px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.concretecraft.com/stamped-concrete/walls-fireplaces/" linktype="1" target="_blank">VERTICAL</a></strong></div>
                           </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end navigation code --->
<!--- start banner --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                              <td>
                                 <div align="center"><a track="on" href="https://www.concretecraft.com/" class="imgCaptionAnchor"><img width="644" name="ACCOUNT.IMAGE.1206" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}Excited-Yet-Title.jpg"></a></div>
							  </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end banner code--->

<!--- body --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				   <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						<tbody>
						        <tr>
		                           <td height="35"></td>
	                            </tr>
	                            <tr>
	                				<td style="color:#69696d; text-align:center; font-size:32px; font-weight: normal;">
	                					Only 48 hours Until your</br>appointment!
	                				</td>
	    						</tr>
	    						<tr>
									<td height="20"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:16px; text-align: center; font-weight: normal;line-height:20px; padding:0 30px;">
	                					we are looking forward to helping you with your upcoming project.If you''re looking for ideas check out our <a href="http://docs.budgetblinds.com/odg/files/assets/basic-html/page-1.html#" style="font-style:italic; color:#69696d;">Design Guide</a> and get inspired! You''re in good hands with xx Brand!
	                				</td>
	    						</tr>
								<tr>
									<td height="40"></td>
								</tr>
								<tr>
									<td><img name="remindborder" src="{IMAGEPATH}remind-border.jpg" alt="remindborder"/></td>
								</tr>
								<tr>
									<td height="40"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:14px; text-align: center; font-weight: normal;text-transform:uppercase;">
	                					Confirmation Appointment for:
	                				</td>
	    						</tr>
								<tr>
									<td height="15"></td>
								</tr>
								<tr>
	                				<td style="color:#3d7d8b;font-size:24px; text-align: center; font-weight:600;">
	                					{CLIENT}
	                				</td>
	    						</tr>
								<tr>
	                				<td style="color:#3d7d8b;font-size:15px; text-align: center; line-height: 20px; font-weight:600;">
	                					{ADDRESS}
	                				</td>
	    						</tr>
								<tr>
									<td height="30"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:14px; text-align: center; font-weight: normal;text-transform:uppercase;">
	                					Date and time of Appointment:
	                				</td>
	    						</tr>
								<tr>
									<td height="15"></td>
								</tr>
								<tr>
	                				<td style="color:#3d7d8b;font-size:24px; text-align: center; font-weight:600;">
	                					{APPOINTMENTDATE}
	                				</td>
	    						</tr>
								<tr>
									<td height="30"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:14px; text-align: center; font-weight: normal;text-transform:uppercase;">
	                					with
	                				</td>
	    						</tr>
								<tr>
									<td height="15"></td>
								</tr>
								<tr>
	                				<td style="color:#3d7d8b; font-size:24px; text-align: center; font-weight:600;">
	                					{OWNER} </br>{BRAND} brand and territory title
	                				</td>
	    						</tr>
	    						<tr>
									<td height="50"></td>
								</tr>
								<tr>
									<td><img name="remindborder" src="{IMAGEPATH}remind-border.jpg" alt="remindborder"/></td>
								</tr>
								<tr>
									<td height="40"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:21px; text-align: center; font-weight:600; text-transform:uppercase;">
	                					What to expect
	                				</td>
	    						</tr>
								<tr>
									<td height="20"></td>
								</tr>
								<!--- icons --->
	
	<tr>
		<td>
		
			<table style="text-align: center; width:630px;" border="0" cellspacing="0" cellpadding="0" align="center">

				<tbody><tr>
					<td>
					
						<table align="left" width="148" border="0" cellpadding="0" cellspacing="0" style="border-right:1px solid #dadada;">
				
							<tbody><tr>
								<td>
									
									<table style="text-align: center;" border="0" align="center" cellpadding="0" cellspacing="0">
									
										<tbody><tr>	
											<td align="center">
												<a class="imgCaptionAnchor" track="on" shape="rect" href="https://www.concretecraft.com/"><img alt="Content" title="Content" src="{IMAGEPATH}design-icon.png" alt=" " width="50"></a>
												
											</td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:12px;color:#000;line-height:1.8em; text-transform:uppercase;">
												We design
											</td>
										</tr>
										<tr>
											 <td height="5"></td>
										</tr>
										<tr>
											<td align="center" style="font-size:11px;color:#888;line-height:1.5em; padding:0 10px;">
												Our talented consultants closely collaborate with you to design a unique solution that brings your personal style to life.
											</td>
										</tr>
									</tbody></table>
									
								</td>
								
							</tr>
						
						</tbody></table>
						<table align="left" width="148" border="0" cellpadding="0" cellspacing="0" style="border-right:1px solid #dadada;">
				
							<tbody><tr>
								<td>
									
									<table style="text-align: center;" border="0" align="center" cellpadding="0" cellspacing="0">
									
										<tbody><tr>	
											<td align="center">
												<a track="on" shape="rect" href="https://www.concretecraft.com/"><img alt="Contact" title="Contact" src="{IMAGEPATH}measure-icon.png" width="50"></a>
											</td>
										</tr>
																		
										<tr>
											<td align="center" style="font-size:12px;color:#000;line-height:1.8em; text-transform:uppercase;">
												Web Measure
											</td>
										</tr>
										<tr>
											 <td height="5"></td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:11px;color:#888;line-height:1.5em; padding:0 10px;">
												Our talented consultants closely collaborate with you to design a unique solution that brings your personal style to life.
											</td>
										</tr>
										
									</tbody></table>
									
								</td>
							</tr>
						
						</tbody></table>
						<table align="left" width="148" border="0" cellpadding="0" cellspacing="0" style="border-right:1px solid #dadada;">
				
							<tbody><tr>
								<td>
									
									<table style="text-align: center;" border="0" align="center" cellpadding="0" cellspacing="0">
									
										<tbody><tr>	
											<td align="center">
												<a track="on" shape="rect" href="https://www.concretecraft.com/"><img src="{IMAGEPATH}install-icon.png" alt=" " width="50"></a>
											</td>
										</tr>
										
										
										<tr>
											<td align="center" style="font-size:12px;color:#000;line-height:1.8em; text-transform:uppercase;">
												Web Install
											</td>
										</tr>
										<tr>
											 <td height="5"></td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:11px;color:#888;line-height:1.5em; padding:0 10px;">
												Our talented consultants closely collaborate with you to design a unique solution that brings your personal style to life.
											</td>
										</tr>
									</tbody></table>
									
								</td>
							</tr>
						
						</tbody></table>
						
						
						<table align="left" width="148" border="0" cellpadding="0" cellspacing="0">
				
							<tbody><tr>
								<td>
									
									<table style="text-align: center;" border="0" align="center" cellpadding="0" cellspacing="0">
									
										<tbody><tr>	
											<td align="center">
												<a track="on" shape="rect" href="https://www.concretecraft.com/">
												<img src="{IMAGEPATH}celebrate-icon.png" alt=" " width="50"></a>
											</td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:12px;color:#000;line-height:1.8em; text-transform:uppercase;">
												you celebrate
											</td>
										</tr>
										<tr>
											 <td height="5"></td>
										</tr>
										
										<tr>
											<td align="center" style="font-size:11px;color:#888;line-height:1.5em; padding:0 10px;">
												Our talented consultants closely collaborate with you to design a unique solution that brings your personal style to life.
											</td>
										</tr>
									</tbody></table>
									
								</td>
							</tr>
						
						</tbody></table>
						
					</td>
				</tr>
				
			</tbody></table>
			
		</td>
	</tr>
								
<!--- end icons --->
    <tr>
		<td height="20"></td>
	</tr>
	    						
							</tbody>
					  </table> 
					</td> 
				   
				   </tr>
				 </tbody>
		  </table>
	   </td>
	</tr>
   </tbody>
</table>
<!--- end body --->

<!--- start footer --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
			                  <td style="width:60px;">
				                 <p style="color:#fff; font-size:12px; text-transform:uppercase; line-height:17px; width:80px;">Franchise<br>Opportunities<br>available</p>
				              </td>
							  <td style="width:30px;"><img name="vborder" src="{IMAGEPATH}vborder.jpg" alt="vborder"/></td>
				              <td style="width:300px;">
				                 <p style="color:#fff; font-size:18px; text-transform:uppercase; line-height:18px; margin:15px 0 5px;">Be your own boss.</br> own a {BRAND} brand Franchise.</p>
				                 <p style="color:#fff; font-size:10px; line-height:14px; margin:5px 0;">This is not an offering to sell a franchise. Franchise offerings are made through a Franchise Disclosure Document.</p>
				              </td>
				              <td style="width:50px;">
				                  <a track="on" href="http://www.budget-blinds-franchise.com/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=franchise&amp;utm_campaign=Email-1stOctPromo2017" class="imgCaptionAnchor">
				                  <img  name="ACCOUNT.IMAGE.1203" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}learn-but.jpg"></a>
				              </td>
                           </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- start footer text --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				     <td width="100%"> 
					   <table width="644" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						 <tbody>
						    <tr>
							    <td height="20"></td>
							</tr>
							<tr>
                                <td style="padding: 0px 25px; font-size: 10pt; font-family: &quot;Lucida Sans Unicode&quot;, &quot;LucidaGrande&quot;, sans-serif; color: #69696d; line-height: 1.3;" valign="top" align="center">
                                   <div style="font-family: ''Century Gothic'', ''ITC Avant Garde'', Arial, Helvetica, sans-serif; font-size: 8pt;">
                                       <div style="text-align: left;" align="left"><span style="font-size: 8pt;">
                                          <div><span style="font-size:10pt;">This is a confirmation message only. No action is needed from you. {BRAND} Brand respects your privacy - view our <a style="text-decoration: underline;color: #3d7d8b;" href="http://www.concretecraft.com/privacy-policy/">Privacy Policy.</a></span></div>
                                          <div><span style="font-size: 8pt;"><br></span></div>
                                          <div><span style="font-size: 10pt;">&copy;2018 American Decorative Coatings, LLC. All Rights Reserved. <span>{BRAND} Brand&nbsp;</span>is a trademark of American Decorative Coatings,LLC and a Home Franchise Concepts Brand. Each franchise independently owned and operated. Franchise opportunities available.</span></div></span></div>
                                   </div>
                                </td>
	                        </tr>
						    <tr>
							    <td height="20"></td>
							</tr>
						 </tbody>
					   </table>
				     </td>
				  </tr>
			  </tbody>
		  </table>
	    </td>
	  </tr>
   </tbody>
</table>
<!--- end footer text --->

<!--- end footer --->
</tbody>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</body>
</html>
', 1, CAST(N'2018-12-31' AS Date), 3)


INSERT [CRM].[EmailTemplates] ([EmailTemplateId], [EmailTemplateTypeId], [Description], [TemplateSubject], [TemplateBody], [FranchiseId], [isDeleted], [TemplateLayout], [IsVisible], [IsVisibleAfterDate], [TemplateBrand]) VALUES (510, 4, N'Budget Blinds - Order Summary', N'Order Summary', N'<div>
 <h1>
  Order Summary.
 </h1>
</div>', NULL, 0, N'<html>
<head>
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
body{
	  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
       width:100% !important; 
	  -webkit-text-size-adjust:100%; 
	  -ms-text-size-adjust:100%;  
      margin:0; 
      padding:0; 
      -webkit-font-smoothing: antialiased;
      background:#fff;
}
html{
      width: 100%; 
}
table td, table td * {
      font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
}
</style>  
</head>
<body style="width: 100%; text-align: center;  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;">

<table width="100%" bgcolor="#fff" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#f0f1f1" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="padding:30px;"> 
				<tbody>
				  <tr> 
				    <td> 
					  <table  border="0" align="center" cellpadding="0" cellspacing="0" >
					     <tbody>
						    <tr> 
							   <td width="100%"> 
                                  <tbody>


<tr>
<td>
  <tbody>
    <tr>
      <td style="padding: 40px 0px 10px; font-size: 11pt; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #434343;" valign="top" align="center">
        <table cellpadding="0" cellspacing="0" data-padding-converted="true" width="366" align="center">
          <tbody>
            <tr>
              <td style="padding: 0px;" width="100%">
                <div align="center"><a track="on" href="http://www.budgetblinds.com/?utm_source=ConstantContact&amp;utm_medium=email&amp;utm_content=Logo&amp;utm_campaign=Email-1stOctPromo2017" class="imgCaptionAnchor">
				<img alt="Logo" title="Logo" height="64" width="200" name="ACCOUNT.IMAGE.1208" vspace="0" hspace="0" style="display: block; max-width: 100%; height: auto;" border="0" src="{IMAGEPATH}blinds_logo.png"></a></div>
              </td>
            </tr>
			<tr>
		      <td height="20"></td>
	        </tr>
          </tbody>
        </table>
        <br></td>
    </tr>
  </tbody>
</td>
</tr>
<!--- end logo --->

<!--- start navigation code --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                            <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.budgetblinds.com/window-blinds/" linktype="1" target="_blank">Blinds</a></strong></div>
                            </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.budgetblinds.com/window-shutters/" linktype="1" target="_blank">Shutters</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.budgetblinds.com/window-shades/" linktype="1" target="_blank">Shades</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 75px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.budgetblinds.com/draperies/" linktype="1" target="_blank">Drapes & More</a></strong></div>
                           </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end navigation code --->

<!--- start banner --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                              <td>
                                 <div align="center"><a track="on" href="http://www.budgetblinds.com/shop-by-room/childrens-window-coverings/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=hero&amp;utm_campaign=Email-1stOctPromo2017" class="imgCaptionAnchor"><img width="644" name="ACCOUNT.IMAGE.1206" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}Order-Hero.jpg"></a></div>
							  </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end banner code--->

<!--- body --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				   <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						<tbody>
						        <tr>
		                           <td height="35"></td>
	                            </tr>
	                            <tr>
	                				<td style="color:#69696d; text-align:center; font-size:32px; font-weight: normal;line-height: 2em;">
	                					Order Received!
	                				</td>
	    						</tr>
	    						<tr>
									<td height="12"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:16px; text-align: center; font-weight: normal;line-height:20px; padding:0 30px;">
	                					Thank you for your order - please take a look at the order summary <u style="font-style:italic;">here</u>. This summary reflects the order details, deposit received and balance due. We thank you for your business and look forward to completing your project!
	                				</td>
	    						</tr>
								<tr>
									<td height="40"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:16px; text-align: center; font-weight: normal;line-height:20px; padding:0 30px; font-style:italic;">
	                					{OWNER} will get in touch with you soon to discuss a date and time for your upcoming window covering installation.
	                				</td>
	    						</tr>
								<tr>
									<td height="50"></td>
								</tr>
								<tr>
									<td><img name="hborder" src="{IMAGEPATH}hborder.jpg" alt="hborder"/></td>
								</tr>
								<tr>
									<td height="40"></td>
								</tr>
								<tr>
								   <td style="color:#69696d; padding: 0px; font-size:16px;">
                                    <strong>Question before then?</strong>Call or email {OWNER} at</br>{PHONE} | <a href="#" style="color:#69696d;">{EMAIL}</a>
                                   </td>
								</tr>
								<tr>
									<td height="20"></td>
								</tr>
	    						
							</tbody>
					  </table> 
					</td> 
				   
				   </tr>
				 </tbody>
		  </table>
	   </td>
	</tr>
   </tbody>
</table>
<!--- end body --->

<!--- start footer --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
			                  <td style="width:60px;">
				                 <p style="color:#fff; font-size:12px; text-transform:uppercase; line-height:17px; width:80px;">Franchise<br>Opportunities<br>available</p>
				              </td>
							  <td style="width:30px;"><img name="vborder" src="{IMAGEPATH}vborder.jpg" alt="vborder"/></td>
				              <td style="width:300px;">
				                 <p style="color:#fff; font-size:18px; text-transform:uppercase; line-height:18px; margin:15px 0 5px;">Be your own boss.</br> own a {BRAND} brand Franchise.</p>
				                 <p style="color:#fff; font-size:10px; line-height:14px; margin:5px 0;">This is not an offering to sell a franchise. Franchise offerings are made through a Franchise Disclosure Document.</p>
				              </td>
				              <td style="width:50px;">
				                  <a track="on" href="http://www.budget-blinds-franchise.com/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=franchise&amp;utm_campaign=Email-1stOctPromo2017" class="imgCaptionAnchor">
				                  <img  name="ACCOUNT.IMAGE.1203" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}learn-but.jpg"></a>
				              </td>
                           </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- start footer text --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				     <td width="100%"> 
					   <table width="644" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						 <tbody>
						    <tr>
							    <td height="20"></td>
							</tr>
							<tr>
                                <td style="padding: 0px 25px; font-size: 10pt; font-family: &quot;Lucida Sans Unicode&quot;, &quot;LucidaGrande&quot;, sans-serif; color: #69696d; line-height: 1.3;" valign="top" align="center">
                                   <div style="font-family: ''Century Gothic'', ''ITC Avant Garde'', Arial, Helvetica, sans-serif; font-size: 8pt;">
                                       <div style="text-align: left;" align="left"><span style="font-size: 8pt;">
                                          <div><span style="font-size:10pt;">This is a confirmation message only. No action is needed from you. {BRAND} Brand respects your privacy - view our <a style="text-decoration: underline;color: #3d7d8b;" href="http://www.budgetblinds.com/privacy-policy/">Privacy Policy.</a></span></div>
                                          <div><span style="font-size: 8pt;"><br></span></div>
                                          <div><span style="font-size: 10pt;">&copy;2018 {BRAND}, LLC. All Rights Reserved. <span>{BRAND}</span>is a trademark of <span>{BRAND}</span>, LLC and a Home Franchise Concepts Brand. Each franchise independently owned and operated. Franchise opportunities available.</span></div></span></div>
                                   </div>
                                </td>
	                        </tr>
						    <tr>
							    <td height="20"></td>
							</tr>
						 </tbody>
					   </table>
				     </td>
				  </tr>
			  </tbody>
		  </table>
	    </td>
	  </tr>
   </tbody>
</table>
<!--- end footer text --->

<!--- end footer --->
</tbody>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</body>
</html>
', 1, CAST(N'2018-12-31' AS Date), 1)


INSERT [CRM].[EmailTemplates] ([EmailTemplateId], [EmailTemplateTypeId], [Description], [TemplateSubject], [TemplateBody], [FranchiseId], [isDeleted], [TemplateLayout], [IsVisible], [IsVisibleAfterDate], [TemplateBrand]) VALUES (511, 4, N'Tailored Living - Order Summary', N'Order Summary', N'<div>
 <h1>
  Order Summary.
 </h1>
</div>', NULL, 0, N'<html>
<head>
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
body{
	  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
       width:100% !important; 
	  -webkit-text-size-adjust:100%; 
	  -ms-text-size-adjust:100%;  
      margin:0; 
      padding:0; 
      -webkit-font-smoothing: antialiased;
      background:#fff;
}
html{
      width: 100%; 
}
table td, table td * {
      font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
}
</style>  
</head>
<body style="width: 100%; text-align: center;  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;">

<table width="100%" bgcolor="#fff" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#f0f1f1" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="padding:30px;"> 
				<tbody>
				  <tr> 
				    <td> 
					  <table  border="0" align="center" cellpadding="0" cellspacing="0" >
					     <tbody>
						    <tr> 
							   <td width="100%"> 
                                  <tbody>


<tr>
<td>
  <tbody>
    <tr>
      <td style="padding: 40px 0px 10px; font-size: 11pt; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #434343;" valign="top" align="center">
        <table cellpadding="0" cellspacing="0" data-padding-converted="true" width="366" align="center">
          <tbody>
            <tr>
              <td style="padding: 0px;" width="100%">
                <div align="center"><a track="on" href="http://www.tailoredliving.com/" class="imgCaptionAnchor">
				<img alt="Logo" title="Logo" height="64" width="200" name="ACCOUNT.IMAGE.1208" vspace="0" hspace="0" style="display: block; max-width: 100%; height: auto;" border="0" src="{IMAGEPATH}tailored_logo.png"></a></div>
              </td>
            </tr>
			<tr>
		      <td height="20"></td>
	        </tr>
          </tbody>
        </table>
        <br></td>
    </tr>
  </tbody>
</td>
</tr>
<!--- end logo --->

<!--- start navigation code --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                            <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.tailoredliving.com/closet-organization/" linktype="1" target="_blank">CLOSETS</a></strong></div>
                            </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.tailoredliving.com/garage-storage/" linktype="1" target="_blank">GARAGES</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.tailoredliving.com/home-office-design/" linktype="1" target="_blank">OFFICES</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 75px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.tailoredliving.com/home-organization/" linktype="1" target="_blank">PANTRIES & MORE</a></strong></div>
                           </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end navigation code --->

<!--- start banner --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                              <td>
                                 <div align="center"><a track="on" href="http://www.tailoredliving.com/" class="imgCaptionAnchor"><img width="644" name="ACCOUNT.IMAGE.1206" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}Order-Hero.jpg"></a></div>
							  </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end banner code--->

<!--- body --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				   <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						<tbody>
						        <tr>
		                           <td height="35"></td>
	                            </tr>
	                            <tr>
	                				<td style="color:#69696d; text-align:center; font-size:32px; font-weight: normal;line-height: 2em;">
	                					Order Received!
	                				</td>
	    						</tr>
	    						<tr>
									<td height="12"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:16px; text-align: center; font-weight: normal;line-height:20px; padding:0 30px;">
	                					Thank you for your order - please take a look at the order summary <u style="font-style:italic;">here</u>. This summary reflects the order details, deposit received and balance due. We thank you for your business and look forward to completing your project!
	                				</td>
	    						</tr>
								<tr>
									<td height="40"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:16px; text-align: center; font-weight: normal;line-height:20px; padding:0 30px; font-style:italic;">
	                					{OWNER} will get in touch with you soon to discuss a date and time for your upcoming window covering installation.
	                				</td>
	    						</tr>
								<tr>
									<td height="50"></td>
								</tr>
								<tr>
									<td><img name="hborder" src="{IMAGEPATH}hborder.jpg" alt="hborder"/></td>
								</tr>
								<tr>
									<td height="40"></td>
								</tr>
								<tr>
									<td height="25"></td>
								</tr>
								<tr>
								   <td style="color:#69696d; padding: 0px; font-size:16px;">
                                    <strong>Question before then?</strong>Call or email {OWNER} at</br>{PHONE} | <a href="#" style="color:#69696d;">{EMAIL}</a>
                                   </td>
								</tr>
								
								<tr>
									<td height="20"></td>
								</tr>
	    						
							</tbody>
					  </table> 
					</td> 
				   
				   </tr>
				 </tbody>
		  </table>
	   </td>
	</tr>
   </tbody>
</table>
<!--- end body --->

<!--- start footer --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
			                  <td style="width:60px;">
				                 <p style="color:#fff; font-size:12px; text-transform:uppercase; line-height:17px; width:80px;">Franchise<br>Opportunities<br>available</p>
				              </td>
							  <td style="width:30px;"><img name="vborder" src="{IMAGEPATH}vborder.jpg" alt="vborder"/></td>
				              <td style="width:300px;">
				                 <p style="color:#fff; font-size:18px; text-transform:uppercase; line-height:18px; margin:15px 0 5px;">Be your own boss.</br> own a {BRAND} brand Franchise.</p>
				                 <p style="color:#fff; font-size:10px; line-height:14px; margin:5px 0;">This is not an offering to sell a franchise. Franchise offerings are made through a Franchise Disclosure Document.</p>
				              </td>
				              <td style="width:50px;">
				                  <a track="on" href="http://www.budget-blinds-franchise.com/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=franchise&amp;utm_campaign=Email-1stOctPromo2017" class="imgCaptionAnchor">
				                  <img  name="ACCOUNT.IMAGE.1203" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}learn-but.jpg"></a>
				              </td>
                           </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- start footer text --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				     <td width="100%"> 
					   <table width="644" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						 <tbody>
						    <tr>
							    <td height="20"></td>
							</tr>
							<tr>
                                <td style="padding: 0px 25px; font-size: 10pt; font-family: &quot;Lucida Sans Unicode&quot;, &quot;LucidaGrande&quot;, sans-serif; color: #69696d; line-height: 1.3;" valign="top" align="center">
                                   <div style="font-family: ''Century Gothic'', ''ITC Avant Garde'', Arial, Helvetica, sans-serif; font-size: 8pt;">
                                       <div style="text-align: left;" align="left"><span style="font-size: 8pt;">
                                          <div><span style="font-size:10pt;">This is a confirmation message only. No action is needed from you. {BRAND} Brand respects your privacy - view our <a style="text-decoration: underline;color: #3d7d8b;" href="http://www.tailoredliving.com/privacy-policy/">Privacy Policy.</a></span></div>
                                          <div><span style="font-size: 8pt;"><br></span></div>
                                          <div><span style="font-size: 10pt;">&copy;2018 {BRAND} Brand. All Rights Reserved. <span>{BRAND}</span>featuring PremierGarage is a trademark of  <span>{BRAND}</span>, LLC and a Home Franchise Concepts Brand. Each franchise independently owned and operated. Franchise opportunities available.</span></div></span></div>
                                   </div>
                                </td>
	                        </tr>
						    <tr>
							    <td height="20"></td>
							</tr>
						 </tbody>
					   </table>
				     </td>
				  </tr>
			  </tbody>
		  </table>
	    </td>
	  </tr>
   </tbody>
</table>
<!--- end footer text --->

<!--- end footer --->
</tbody>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</body>
</html>
', 1, CAST(N'2018-12-31' AS Date), 2)


INSERT [CRM].[EmailTemplates] ([EmailTemplateId], [EmailTemplateTypeId], [Description], [TemplateSubject], [TemplateBody], [FranchiseId], [isDeleted], [TemplateLayout], [IsVisible], [IsVisibleAfterDate], [TemplateBrand]) VALUES (512, 4, N'Concrete Craft - Order Summary', N'Order Summary', N'<div>
 <h1>
  Order Summary.
 </h1>
</div>', NULL, 0, N'<html>
<head>
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
body{
	  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
       width:100% !important; 
	  -webkit-text-size-adjust:100%; 
	  -ms-text-size-adjust:100%;  
      margin:0; 
      padding:0; 
      -webkit-font-smoothing: antialiased;
      background:#fff;
}
html{
      width: 100%; 
}
table td, table td * {
      font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
}
</style>  
</head>
<body style="width: 100%; text-align: center;  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;">

<table width="100%" bgcolor="#fff" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#f0f1f1" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="padding:30px;"> 
				<tbody>
				  <tr> 
				    <td> 
					  <table  border="0" align="center" cellpadding="0" cellspacing="0" >
					     <tbody>
						    <tr> 
							   <td width="100%"> 
                                  <tbody>


<tr>
<td>
  <tbody>
    <tr>
      <td style="padding: 40px 0px 10px; font-size: 11pt; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #434343;" valign="top" align="center">
        <table cellpadding="0" cellspacing="0" data-padding-converted="true" width="366" align="center">
          <tbody>
            <tr>
              <td style="padding: 0px;" width="100%">
                <div align="center"><a track="on" href="https://www.concretecraft.com/" class="imgCaptionAnchor">
				<img alt="Logo" title="Logo" height="64" width="200" name="ACCOUNT.IMAGE.1208" vspace="0" hspace="0" style="display: block; max-width: 100%; height: auto;" border="0" src="{IMAGEPATH}concrete_logo.png"></a></div>
              </td>
            </tr>
			<tr>
		      <td height="20"></td>
	        </tr>
          </tbody>
        </table>
        <br></td>
    </tr>
  </tbody>
</td>
</tr>
<!--- end logo --->

<!--- start navigation code --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                            <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 80px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.concretecraft.com/stamped-concrete/" linktype="1" target="_blank">STAMPED</a></strong></div>
                            </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 80px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.concretecraft.com/resurfaced-concrete/" linktype="1" target="_blank">RESURFACED</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 80px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.concretecraft.com/stained-concrete/" linktype="1" target="_blank">STAINED</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 80px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.concretecraft.com/stamped-concrete/walls-fireplaces/" linktype="1" target="_blank">VERTICAL</a></strong></div>
                           </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end navigation code --->

<!--- start banner --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                              <td>
                                 <div align="center"><a track="on" href="https://www.concretecraft.com/" class="imgCaptionAnchor"><img width="644" name="ACCOUNT.IMAGE.1206" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}Order-Hero.jpg"></a></div>
							  </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end banner code--->

<!--- body --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				   <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						<tbody>
						        <tr>
		                           <td height="35"></td>
	                            </tr>
	                            <tr>
	                				<td style="color:#69696d; text-align:center; font-size:32px; font-weight: normal;line-height: 2em;">
	                					Order Received!
	                				</td>
	    						</tr>
	    						<tr>
									<td height="12"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:16px; text-align: center; font-weight: normal;line-height:20px; padding:0 30px;">
	                					Thank you for your order - please take a look at the order summary <u style="font-style:italic;">here</u>. This summary reflects the order details, deposit received and balance due. We thank you for your business and look forward to completing your project!
	                				</td>
	    						</tr>
								<tr>
									<td height="40"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:16px; text-align: center; font-weight: normal;line-height:20px; padding:0 30px; font-style:italic;">
	                					{OWNER} will get in touch with you soon to discuss a date and time for your upcoming window covering installation.
	                				</td>
	    						</tr>
								<tr>
									<td height="50"></td>
								</tr>
								<tr>
									<td><img name="hborder" src="{IMAGEPATH}border.jpg" alt="hborder"/></td>
								</tr>
								<tr>
									<td height="40"></td>
								</tr>
								<tr>
									<td height="25"></td>
								</tr>
								<tr>
								   <td style="color:#69696d; padding: 0px; font-size:16px;">
                                    <strong>Question before then?</strong>Call or email {OWNER} at</br>{PHONE} | <a href="#" style="color:#69696d;">{EMAIL}</a>
                                   </td>
								</tr>
								
								<tr>
									<td height="20"></td>
								</tr>
	    						
							</tbody>
					  </table> 
					</td> 
				   
				   </tr>
				 </tbody>
		  </table>
	   </td>
	</tr>
   </tbody>
</table>
<!--- end body --->

<!--- start footer --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
			                  <td style="width:60px;">
				                 <p style="color:#fff; font-size:12px; text-transform:uppercase; line-height:17px; width:80px;">Franchise<br>Opportunities<br>available</p>
				              </td>
							  <td style="width:30px;"><img name="vborder" src="{IMAGEPATH}vborder.jpg" alt="vborder"/></td>
				              <td style="width:300px;">
				                 <p style="color:#fff; font-size:18px; text-transform:uppercase; line-height:18px; margin:15px 0 5px;">Be your own boss.</br> own a {BRAND} brand Franchise.</p>
				                 <p style="color:#fff; font-size:10px; line-height:14px; margin:5px 0;">This is not an offering to sell a franchise. Franchise offerings are made through a Franchise Disclosure Document.</p>
				              </td>
				              <td style="width:50px;">
				                  <a track="on" href="http://www.budget-blinds-franchise.com/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=franchise&amp;utm_campaign=Email-1stOctPromo2017" class="imgCaptionAnchor">
				                  <img  name="ACCOUNT.IMAGE.1203" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}learn-but.jpg"></a>
				              </td>
                           </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- start footer text --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				     <td width="100%"> 
					   <table width="644" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						 <tbody>
						    <tr>
							    <td height="20"></td>
							</tr>
							<tr>
                                <td style="padding: 0px 25px; font-size: 10pt; font-family: &quot;Lucida Sans Unicode&quot;, &quot;LucidaGrande&quot;, sans-serif; color: #69696d; line-height: 1.3;" valign="top" align="center">
                                   <div style="font-family: ''Century Gothic'', ''ITC Avant Garde'', Arial, Helvetica, sans-serif; font-size: 8pt;">
                                       <div style="text-align: left;" align="left"><span style="font-size: 8pt;">
                                          <div><span style="font-size:10pt;">This is a confirmation message only. No action is needed from you. {BRAND} Brand respects your privacy - view our <a style="text-decoration: underline;color: #3d7d8b;" href="http://www.concretecraft.com/privacy-policy/">Privacy Policy.</a></span></div>
                                          <div><span style="font-size: 8pt;"><br></span></div>
                                          <div><span style="font-size: 10pt;">&copy;2018 American Decorative Coatings, LLC. All Rights Reserved. <span>{BRAND} Brand&nbsp;</span>is a trademark of American Decorative Coatings,LLC and a Home Franchise Concepts Brand. Each franchise independently owned and operated. Franchise opportunities available.</span></div></span></div>
                                   </div>
                                </td>
	                        </tr>
						    <tr>
							    <td height="20"></td>
							</tr>
						 </tbody>
					   </table>
				     </td>
				  </tr>
			  </tbody>
		  </table>
	    </td>
	  </tr>
   </tbody>
</table>
<!--- end footer text --->

<!--- end footer --->
</tbody>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</body>
</html>
', 1, CAST(N'2018-12-31' AS Date), 3)


INSERT [CRM].[EmailTemplates] ([EmailTemplateId], [EmailTemplateTypeId], [Description], [TemplateSubject], [TemplateBody], [FranchiseId], [isDeleted], [TemplateLayout], [IsVisible], [IsVisibleAfterDate], [TemplateBrand]) VALUES (513, 5, N'Budget Blinds -  Install Schedule - What to Expect', N'Install Schedule - What to Expect', N'<div>
 <h1>
  Install Schedule - What to Expect</h1>
</div>', NULL, 0, N'<html>
<head>
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
body{
	  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
       width:100% !important; 
	  -webkit-text-size-adjust:100%; 
	  -ms-text-size-adjust:100%;  
      margin:0; 
      padding:0; 
      -webkit-font-smoothing: antialiased;
      background:#fff;
}
html{
      width: 100%; 
}
table td, table td * {
      font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
}
</style>  
</head>
<body style="width: 100%; text-align: center;  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;">

<table width="100%" bgcolor="#fff" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#f0f1f1" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="padding:30px;"> 
				<tbody>
				  <tr> 
				    <td> 
					  <table  border="0" align="center" cellpadding="0" cellspacing="0" >
					     <tbody>
						    <tr> 
							   <td width="100%"> 
                                  <tbody>


<tr>
<td>
  <tbody>
    <tr>
      <td style="padding: 40px 0px 10px; font-size: 11pt; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #434343;" valign="top" align="center">
        <table cellpadding="0" cellspacing="0" data-padding-converted="true" width="366" align="center">
          <tbody>
            <tr>
              <td style="padding: 0px;" width="100%">
                <div align="center"><a track="on" href="http://www.budgetblinds.com/?utm_source=ConstantContact&amp;utm_medium=email&amp;utm_content=Logo&amp;utm_campaign=Email-1stOctPromo2017" class="imgCaptionAnchor">
				<img alt="Logo" title="Logo" height="64" width="200" name="ACCOUNT.IMAGE.1208" vspace="0" hspace="0" style="display: block; max-width: 100%; height: auto;" border="0" src="{IMAGEPATH}blinds_logo.png"></a></div>
              </td>
            </tr>
			<tr>
		      <td height="20"></td>
	        </tr>
          </tbody>
        </table>
        <br></td>
    </tr>
  </tbody>
</td>
</tr>
<!--- end logo --->

<!--- start navigation code --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                            <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.budgetblinds.com/window-blinds/" linktype="1" target="_blank">Blinds</a></strong></div>
                            </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.budgetblinds.com/window-shutters/" linktype="1" target="_blank">Shutters</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.budgetblinds.com/window-shades/" linktype="1" target="_blank">Shades</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 75px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.budgetblinds.com/draperies/" linktype="1" target="_blank">Drapes & More</a></strong></div>
                           </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end navigation code --->

<!--- start banner --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                              <td>
                                 <div align="center"><a track="on" href="http://www.budgetblinds.com/shop-by-room/childrens-window-coverings/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=hero&amp;utm_campaign=Email-1stOctPromo2017" class="imgCaptionAnchor"><img width="644" name="ACCOUNT.IMAGE.1206" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}Install-Hero.jpg"></a></div>
							  </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end banner code--->

<!--- body --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				   <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						<tbody>
						        <tr>
		                           <td height="35"></td>
	                            </tr>
	                            <tr>
	                				<td style="color:#69696d; text-align:center; font-size:16px; font-weight:600; font-style:italic;">
	                					Please note you''ve selected the installation date:
	                				</td>
	    						</tr>
	    						<tr>
									<td height="20"></td>
								</tr>
								<tr>
	                				<td style="color: #3d7d8b;font-size:24px; text-align: center; font-weight:600; line-height:20px; padding:0 30px;">
	                					{INSTALLATIONDATE}
	                				</td>
	    						</tr>
								<tr>
									<td height="50"></td>
								</tr>
								<tr>
									<td><img name="hborder" src="{IMAGEPATH}hborder.jpg" alt="hborder"/></td>
								</tr>
								<tr>
									<td height="40"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:24px; text-align: center;font-weight: normal;line-height:20px; font-weight:600; padding:0 30px;">
	                					What to Expect on Installation Day
	                				</td>
	    						</tr>
								<tr>
									<td height="25"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:16px; text-align: center; font-weight: normal;line-height:20px; padding:0 30px;">
	                					Get ready for your new look! In preparation for your window covering installation, please make sure that obstacles are removed from in front of the windows, and any breakables are secured. Our professional installers will take care of the rest!
	                				</td>
	    						</tr>
								<tr>
									<td height="40"></td>
								</tr>
								<tr>
								   <td style="color:#69696d; padding: 0px; font-size:16px;">
                                       <strong>Question before then?</strong>Call or email {OWNER} at</br>{PHONE} | <a href="#" style="color:#69696d;">{EMAIL}</a>
                                   </td>
								</tr>
								<tr>
									<td height="20"></td>
								</tr>
	    						
							</tbody>
					  </table> 
					</td> 
				   
				   </tr>
				 </tbody>
		  </table>
	   </td>
	</tr>
   </tbody>
</table>
<!--- end body --->

<!--- start footer --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
			                  <td style="width:60px;">
				                 <p style="color:#fff; font-size:12px; text-transform:uppercase; line-height:17px; width:80px;">Franchise<br>Opportunities<br>available</p>
				              </td>
							  <td style="width:30px;"><img name="vborder" src="{IMAGEPATH}vborder.jpg" alt="vborder"/></td>
				              <td style="width:300px;">
				                 <p style="color:#fff; font-size:18px; text-transform:uppercase; line-height:18px; margin:15px 0 5px;">Be your own boss.</br> own a {BRAND} brand Franchise.</p>
				                 <p style="color:#fff; font-size:10px; line-height:14px; margin:5px 0;">This is not an offering to sell a franchise. Franchise offerings are made through a Franchise Disclosure Document.</p>
				              </td>
				              <td style="width:50px;">
				                  <a track="on" href="http://www.budget-blinds-franchise.com/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=franchise&amp;utm_campaign=Email-1stOctPromo2017" class="imgCaptionAnchor">
				                  <img  name="ACCOUNT.IMAGE.1203" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}learn-but.jpg"></a>
				              </td>
                           </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- start footer text --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				     <td width="100%"> 
					   <table width="644" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						 <tbody>
						    <tr>
							    <td height="20"></td>
							</tr>
							<tr>
                                <td style="padding: 0px 25px; font-size: 10pt; font-family: &quot;Lucida Sans Unicode&quot;, &quot;LucidaGrande&quot;, sans-serif; color: #69696d; line-height: 1.3;" valign="top" align="center">
                                   <div style="font-family: ''Century Gothic'', ''ITC Avant Garde'', Arial, Helvetica, sans-serif; font-size: 8pt;">
                                       <div style="text-align: left;" align="left"><span style="font-size: 8pt;">
                                          <div><span style="font-size:10pt;">This is a confirmation message only. No action is needed from you. {BRAND} Brand respects your privacy - view our <a style="text-decoration: underline;color: #3d7d8b;" href="http://www.budgetblinds.com/privacy-policy/">Privacy Policy.</a></span></div>
                                          <div><span style="font-size: 8pt;"><br></span></div>
                                          <div><span style="font-size: 10pt;">&copy;2018 {BRAND}, LLC. All Rights Reserved. <span>{BRAND}</span>is a trademark of <span>{BRAND}</span>, LLC and a Home Franchise Concepts Brand. Each franchise independently owned and operated. Franchise opportunities available.</span></div></span></div>
                                   </div>
                                </td>
	                        </tr>
						    <tr>
							    <td height="20"></td>
							</tr>
						 </tbody>
					   </table>
				     </td>
				  </tr>
			  </tbody>
		  </table>
	    </td>
	  </tr>
   </tbody>
</table>
<!--- end footer text --->

<!--- end footer --->
</tbody>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</body>
</html>
', 1, CAST(N'2018-12-31' AS Date), 1)


INSERT [CRM].[EmailTemplates] ([EmailTemplateId], [EmailTemplateTypeId], [Description], [TemplateSubject], [TemplateBody], [FranchiseId], [isDeleted], [TemplateLayout], [IsVisible], [IsVisibleAfterDate], [TemplateBrand]) VALUES (514, 5, N'Tailored Living-  Install Schedule - What to Expect', N'Install Schedule - What to Expect', N'<div>
 <h1>
  Install Schedule - What to Expect</h1>
</div>', NULL, 0, N'<html>
<head>
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
body{
	  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
       width:100% !important; 
	  -webkit-text-size-adjust:100%; 
	  -ms-text-size-adjust:100%;  
      margin:0; 
      padding:0; 
      -webkit-font-smoothing: antialiased;
      background:#fff;
}
html{
      width: 100%; 
}
table td, table td * {
      font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
}
</style>  
</head>
<body style="width: 100%; text-align: center;  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;">

<table width="100%" bgcolor="#fff" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#f0f1f1" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="padding:30px;"> 
				<tbody>
				  <tr> 
				    <td> 
					  <table  border="0" align="center" cellpadding="0" cellspacing="0" >
					     <tbody>
						    <tr> 
							   <td width="100%"> 
                                  <tbody>


<tr>
<td>
  <tbody>
    <tr>
      <td style="padding: 40px 0px 10px; font-size: 11pt; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #434343;" valign="top" align="center">
        <table cellpadding="0" cellspacing="0" data-padding-converted="true" width="366" align="center">
          <tbody>
            <tr>
              <td style="padding: 0px;" width="100%">
                <div align="center"><a track="on" href="http://www.tailoredliving.com/" class="imgCaptionAnchor">
				<img alt="Logo" title="Logo" height="64" width="200" name="ACCOUNT.IMAGE.1208" vspace="0" hspace="0" style="display: block; max-width: 100%; height: auto;" border="0" src="{IMAGEPATH}tailored_logo.png"></a></div>
              </td>
            </tr>
			<tr>
		      <td height="20"></td>
	        </tr>
          </tbody>
        </table>
        <br></td>
    </tr>
  </tbody>
</td>
</tr>
<!--- end logo --->

<!--- start navigation code --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                            <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.tailoredliving.com/closet-organization/" linktype="1" target="_blank">CLOSETS</a></strong></div>
                            </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.tailoredliving.com/garage-storage/" linktype="1" target="_blank">GARAGES</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.tailoredliving.com/home-office-design/" linktype="1" target="_blank">OFFICES</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 75px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.tailoredliving.com/home-organization/" linktype="1" target="_blank">PANTRIES & MORE</a></strong></div>
                           </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end navigation code --->

<!--- start banner --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                              <td>
                                 <div align="center"><a track="on" href="http://www.tailoredliving.com/" class="imgCaptionAnchor"><img width="644" name="ACCOUNT.IMAGE.1206" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}Install-Hero.jpg"></a></div>
							  </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end banner code--->

<!--- body --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				   <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						<tbody>
						        <tr>
		                           <td height="35"></td>
	                            </tr>
	                            <tr>
	                				<td style="color:#69696d; text-align:center; font-size:16px; font-weight:600; font-style:italic;">
	                					Please note you''ve selected the installation date:
	                				</td>
	    						</tr>
	    						<tr>
									<td height="20"></td>
								</tr>
								<tr>
	                				<td style="color: #3d7d8b;font-size:24px; text-align: center; font-weight:600; line-height:20px; padding:0 30px;">
	                					{INSTALLATIONDATE}
	                				</td>
	    						</tr>
								<tr>
									<td height="50"></td>
								</tr>
								<tr>
									<td><img name="hborder" src="{IMAGEPATH}hborder.jpg" alt="hborder"/></td>
								</tr>
								<tr>
									<td height="40"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:24px; text-align: center;font-weight: normal;line-height:20px; font-weight:600; padding:0 30px;">
	                					What to Expect on Installation Day
	                				</td>
	    						</tr>
								<tr>
									<td height="25"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:16px; text-align: center; font-weight: normal;line-height:20px; padding:0 30px;">
	                					Get ready for your new look! In preparation for your window covering installation, please make sure that obstacles are removed from in front of the windows, and any breakables are secured. Our professional installers will take care of the rest!
	                				</td>
	    						</tr>
								<tr>
									<td height="40"></td>
								</tr>
								<tr>
								   <td style="color:#69696d; padding: 0px; font-size:16px;">
                                       <strong>Question before then?</strong>Call or email {OWNER} at</br>{PHONE} | <a href="#" style="color:#69696d;">{EMAIL}</a>
                                   </td>
								</tr>
								<tr>
									<td height="20"></td>
								</tr>
	    						
							</tbody>
					  </table> 
					</td> 
				   
				   </tr>
				 </tbody>
		  </table>
	   </td>
	</tr>
   </tbody>
</table>
<!--- end body --->

<!--- start footer --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
			                  <td style="width:60px;">
				                 <p style="color:#fff; font-size:12px; text-transform:uppercase; line-height:17px; width:80px;">Franchise<br>Opportunities<br>available</p>
				              </td>
							  <td style="width:30px;"><img name="vborder" src="{IMAGEPATH}vborder.jpg" alt="vborder"/></td>
				              <td style="width:300px;">
				                 <p style="color:#fff; font-size:18px; text-transform:uppercase; line-height:18px; margin:15px 0 5px;">Be your own boss.</br> own a {BRAND} brand Franchise.</p>
				                 <p style="color:#fff; font-size:10px; line-height:14px; margin:5px 0;">This is not an offering to sell a franchise. Franchise offerings are made through a Franchise Disclosure Document.</p>
				              </td>
				              <td style="width:50px;">
				                  <a track="on" href="http://www.budget-blinds-franchise.com/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=franchise&amp;utm_campaign=Email-1stOctPromo2017" class="imgCaptionAnchor">
				                  <img  name="ACCOUNT.IMAGE.1203" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}learn-but.jpg"></a>
				              </td>
                           </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- start footer text --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				     <td width="100%"> 
					   <table width="644" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						 <tbody>
						    <tr>
							    <td height="20"></td>
							</tr>
							<tr>
                                <td style="padding: 0px 25px; font-size: 10pt; font-family: &quot;Lucida Sans Unicode&quot;, &quot;LucidaGrande&quot;, sans-serif; color: #69696d; line-height: 1.3;" valign="top" align="center">
                                   <div style="font-family: ''Century Gothic'', ''ITC Avant Garde'', Arial, Helvetica, sans-serif; font-size: 8pt;">
                                       <div style="text-align: left;" align="left"><span style="font-size: 8pt;">
                                          <div><span style="font-size:10pt;">This is a confirmation message only. No action is needed from you. {BRAND} Brand respects your privacy - view our <a style="text-decoration: underline;color: #3d7d8b;" href="http://www.tailoredliving.com/privacy-policy/">Privacy Policy.</a></span></div>
                                          <div><span style="font-size: 8pt;"><br></span></div>
                                          <div><span style="font-size: 10pt;">&copy;2018 {BRAND} Brand. All Rights Reserved. <span>{BRAND}</span>featuring PremierGarage is a trademark of  <span>{BRAND}</span>, LLC and a Home Franchise Concepts Brand. Each franchise independently owned and operated. Franchise opportunities available.</span></div></span></div>
                                   </div>
                                </td>
	                        </tr>
						    <tr>
							    <td height="20"></td>
							</tr>
						 </tbody>
					   </table>
				     </td>
				  </tr>
			  </tbody>
		  </table>
	    </td>
	  </tr>
   </tbody>
</table>
<!--- end footer text --->

<!--- end footer --->
</tbody>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</body>
</html>
', 1, CAST(N'2018-12-31' AS Date), 2)


INSERT [CRM].[EmailTemplates] ([EmailTemplateId], [EmailTemplateTypeId], [Description], [TemplateSubject], [TemplateBody], [FranchiseId], [isDeleted], [TemplateLayout], [IsVisible], [IsVisibleAfterDate], [TemplateBrand]) VALUES (515, 5, N'Concrete Craft-  Install Schedule - What to Expect', N'Install Schedule - What to Expect', N'<div>
 <h1>
  Install Schedule - What to Expect</h1>
</div>', NULL, 0, N'<html>
<head>
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
body{
	  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
       width:100% !important; 
	  -webkit-text-size-adjust:100%; 
	  -ms-text-size-adjust:100%;  
      margin:0; 
      padding:0; 
      -webkit-font-smoothing: antialiased;
      background:#fff;
}
html{
      width: 100%; 
}
table td, table td * {
      font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
}
</style>  
</head>
<body style="width: 100%; text-align: center;  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;">

<table width="100%" bgcolor="#fff" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#f0f1f1" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="padding:30px;"> 
				<tbody>
				  <tr> 
				    <td> 
					  <table  border="0" align="center" cellpadding="0" cellspacing="0" >
					     <tbody>
						    <tr> 
							   <td width="100%"> 
                                  <tbody>


<tr>
<td>
  <tbody>
    <tr>
      <td style="padding: 40px 0px 10px; font-size: 11pt; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #434343;" valign="top" align="center">
        <table cellpadding="0" cellspacing="0" data-padding-converted="true" width="366" align="center">
          <tbody>
            <tr>
              <td style="padding: 0px;" width="100%">
                <div align="center"><a track="on" href="https://www.concretecraft.com/" class="imgCaptionAnchor">
				<img alt="Logo" title="Logo" height="64" width="200" name="ACCOUNT.IMAGE.1208" vspace="0" hspace="0" style="display: block; max-width: 100%; height: auto;" border="0" src="{IMAGEPATH}concrete_logo.png"></a></div>
              </td>
            </tr>
			<tr>
		      <td height="20"></td>
	        </tr>
          </tbody>
        </table>
        <br></td>
    </tr>
  </tbody>
</td>
</tr>
<!--- end logo --->

<!--- start navigation code --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                            <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 80px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.concretecraft.com/stamped-concrete/" linktype="1" target="_blank">STAMPED</a></strong></div>
                            </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 80px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.concretecraft.com/resurfaced-concrete/" linktype="1" target="_blank">RESURFACED</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 80px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.concretecraft.com/stained-concrete/" linktype="1" target="_blank">STAINED</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 80px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.concretecraft.com/stamped-concrete/walls-fireplaces/" linktype="1" target="_blank">VERTICAL</a></strong></div>
                           </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end navigation code --->

<!--- start banner --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                              <td>
                                 <div align="center"><a track="on" href="https://www.concretecraft.com/" class="imgCaptionAnchor"><img width="644" name="ACCOUNT.IMAGE.1206" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}Install-Hero.jpg"></a></div>
							  </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end banner code--->

<!--- body --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				   <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						<tbody>
						        <tr>
		                           <td height="35"></td>
	                            </tr>
	                            <tr>
	                				<td style="color:#69696d; text-align:center; font-size:16px; font-weight:600; font-style:italic;">
	                					Please note you''ve selected the installation date:
	                				</td>
	    						</tr>
	    						<tr>
									<td height="20"></td>
								</tr>
								<tr>
	                				<td style="color: #3d7d8b;font-size:24px; text-align: center; font-weight:600; line-height:20px; padding:0 30px;">
	                					{INSTALLATIONDATE}
	                				</td>
	    						</tr>
								<tr>
									<td height="50"></td>
								</tr>
								<tr>
									<td><img name="hborder" src="{IMAGEPATH}hborder.jpg" alt="hborder"/></td>
								</tr>
								<tr>
									<td height="40"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:24px; text-align: center;font-weight: normal;line-height:20px; font-weight:600; padding:0 30px;">
	                					What to Expect on Installation Day
	                				</td>
	    						</tr>
								<tr>
									<td height="25"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:16px; text-align: center; font-weight: normal;line-height:20px; padding:0 30px;">
	                					Get ready for your new look! In preparation for your window covering installation, please make sure that obstacles are removed from in front of the windows, and any breakables are secured. Our professional installers will take care of the rest!
	                				</td>
	    						</tr>
								<tr>
									<td height="40"></td>
								</tr>
								<tr>
								   <td style="color:#69696d; padding: 0px; font-size:16px;">
                                       <strong>Question before then?</strong>Call or email {OWNER} at</br>{PHONE} | <a href="#" style="color:#69696d;">{EMAIL}</a>
                                   </td>
								</tr>
								<tr>
									<td height="20"></td>
								</tr>
	    						
							</tbody>
					  </table> 
					</td> 
				   
				   </tr>
				 </tbody>
		  </table>
	   </td>
	</tr>
   </tbody>
</table>
<!--- end body --->

<!--- start footer --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
			                  <td style="width:60px;">
				                 <p style="color:#fff; font-size:12px; text-transform:uppercase; line-height:17px; width:80px;">Franchise<br>Opportunities<br>available</p>
				              </td>
							  <td style="width:30px;"><img name="vborder" src="{IMAGEPATH}vborder.jpg" alt="vborder"/></td>
				              <td style="width:300px;">
				                 <p style="color:#fff; font-size:18px; text-transform:uppercase; line-height:18px; margin:15px 0 5px;">Be your own boss.</br> own a {BRAND} brand Franchise.</p>
				                 <p style="color:#fff; font-size:10px; line-height:14px; margin:5px 0;">This is not an offering to sell a franchise. Franchise offerings are made through a Franchise Disclosure Document.</p>
				              </td>
				              <td style="width:50px;">
				                  <a track="on" href="http://www.budget-blinds-franchise.com/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=franchise&amp;utm_campaign=Email-1stOctPromo2017" class="imgCaptionAnchor">
				                  <img  name="ACCOUNT.IMAGE.1203" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}learn-but.jpg"></a>
				              </td>
                           </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- start footer text --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				     <td width="100%"> 
					   <table width="644" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						 <tbody>
						    <tr>
							    <td height="20"></td>
							</tr>
							<tr>
                                <td style="padding: 0px 25px; font-size: 10pt; font-family: &quot;Lucida Sans Unicode&quot;, &quot;LucidaGrande&quot;, sans-serif; color: #69696d; line-height: 1.3;" valign="top" align="center">
                                   <div style="font-family: ''Century Gothic'', ''ITC Avant Garde'', Arial, Helvetica, sans-serif; font-size: 8pt;">
                                       <div style="text-align: left;" align="left"><span style="font-size: 8pt;">
                                          <div><span style="font-size:10pt;">This is a confirmation message only. No action is needed from you. {BRAND} Brand respects your privacy - view our <a style="text-decoration: underline;color: #3d7d8b;" href="http://www.concretecraft.com/privacy-policy/">Privacy Policy.</a></span></div>
                                          <div><span style="font-size: 8pt;"><br></span></div>
                                          <div><span style="font-size: 10pt;">&copy;2018 American Decorative Coatings, LLC. All Rights Reserved. <span>{BRAND} Brand&nbsp;</span>is a trademark of American Decorative Coatings,LLC and a Home Franchise Concepts Brand. Each franchise independently owned and operated. Franchise opportunities available.</span></div></span></div>
                                   </div>
                                </td>
	                        </tr>
						    <tr>
							    <td height="20"></td>
							</tr>
						 </tbody>
					   </table>
				     </td>
				  </tr>
			  </tbody>
		  </table>
	    </td>
	  </tr>
   </tbody>
</table>
<!--- end footer text --->

<!--- end footer --->
</tbody>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</body>
</html>
', 1, CAST(N'2018-12-31' AS Date), 3)


INSERT [CRM].[EmailTemplates] ([EmailTemplateId], [EmailTemplateTypeId], [Description], [TemplateSubject], [TemplateBody], [FranchiseId], [isDeleted], [TemplateLayout], [IsVisible], [IsVisibleAfterDate], [TemplateBrand]) VALUES (516, 6, N'Budget Blinds - Thank You - Review', N'Thank You - Review', N'<div>
 <h1>
  Thank You - Review</h1>
</div>', NULL, 0, N'<html>
<head>
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
body{
	  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
       width:100% !important; 
	  -webkit-text-size-adjust:100%; 
	  -ms-text-size-adjust:100%;  
      margin:0; 
      padding:0; 
      -webkit-font-smoothing: antialiased;
      background:#fff;
}
html{
      width: 100%; 
}
table td, table td * {
      font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
}
</style>  
</head>
<body style="width: 100%; text-align: center;  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;">

<table width="100%" bgcolor="#fff" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#f0f1f1" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="padding:30px;"> 
				<tbody>
				  <tr> 
				    <td> 
					  <table  border="0" align="center" cellpadding="0" cellspacing="0" >
					     <tbody>
						    <tr> 
							   <td width="100%"> 
                                  <tbody>


<tr>
<td>
  <tbody>
    <tr>
      <td style="padding: 40px 0px 10px; font-size: 11pt; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #434343;" valign="top" align="center">
        <table cellpadding="0" cellspacing="0" data-padding-converted="true" width="366" align="center">
          <tbody>
            <tr>
              <td style="padding: 0px;" width="100%">
                <div align="center"><a track="on" href="http://www.budgetblinds.com/?utm_source=ConstantContact&amp;utm_medium=email&amp;utm_content=Logo&amp;utm_campaign=Email-1stOctPromo2017" class="imgCaptionAnchor">
				<img alt="Logo" title="Logo" height="64" width="200" name="ACCOUNT.IMAGE.1208" vspace="0" hspace="0" style="display: block; max-width: 100%; height: auto;" border="0" src="{IMAGEPATH}blinds_logo.png"></a></div>
              </td>
            </tr>
			<tr>
		      <td height="20"></td>
	        </tr>
          </tbody>
        </table>
        <br></td>
    </tr>
  </tbody>
</td>
</tr>
<!--- end logo --->

<!--- start navigation code --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                            <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.budgetblinds.com/window-blinds/" linktype="1" target="_blank">Blinds</a></strong></div>
                            </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.budgetblinds.com/window-shutters/" linktype="1" target="_blank">Shutters</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.budgetblinds.com/window-shades/" linktype="1" target="_blank">Shades</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 75px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.budgetblinds.com/draperies/" linktype="1" target="_blank">Drapes & More</a></strong></div>
                           </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end navigation code --->

<!--- start banner --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                              <td>
                                 <div align="center"><a track="on" href="http://www.budgetblinds.com/shop-by-room/childrens-window-coverings/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=hero&amp;utm_campaign=Email-1stOctPromo2017" class="imgCaptionAnchor"><img width="644" name="ACCOUNT.IMAGE.1206" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}Relax.jpg"></a></div>
							  </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end banner code--->

<!--- body --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				   <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						<tbody>
						        <tr>
		                           <td height="35"></td>
	                            </tr>
	                            <tr>
	                				<td style="color:#69696d; text-align:center; font-size:40px; font-weight: normal;line-height: 2em;">
	                					Thank you
	                				</td>
	    						</tr>
	    						<tr>
									<td height="12"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:16px; text-align: center; font-weight: normal;line-height:20px; padding:0 30px;">
	                					Thank you for your business! We truly hope you are enjoying your new window coverings, and we look forward to keeping you as a long-time customer!
	                				</td>
	    						</tr>
								<tr>
									<td height="50"></td>
								</tr>
								<tr>
									<td><img name="hborder" src="{IMAGEPATH}border.jpg" alt="hborder"/></td>
								</tr>
								<tr>
									<td height="40"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:24px; text-align: center;font-weight:600; line-height:20px; padding:0 30px;">
	                					Have a great experiance?
	                				</td>
	    						</tr>
								<tr>
									<td height="10"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:20px; text-align: center; font-weight:600; font-style:italic; line-height:20px; padding:0 30px;">
	                					Leave us a review!
	                				</td>
	    						</tr>
								<tr>
									<td height="50"></td>
								</tr>
								<!-- 5 column icons grid--->
								<tr>
									<td>
										<table style="width:644px;" border="0" align="center" cellpadding="0" cellspacing="0">
											<tbody><tr>
												<td align="center">
													<img src="{IMAGEPATH}fb.png" alt=" " style="width:59px;"/>
												</td>
												<td align="center">
													<img src="{IMAGEPATH}yelp.png" alt=" " style="width:59px;"/>
												</td>
												<td align="center">
													<img src="{IMAGEPATH}Houzz.png" alt=" " style="width:59px;"/>
												</td>
												<td align="center">
													<img src="{IMAGEPATH}google.png" alt=" " style="width:59px;"/>
												</td>
												<td align="center">
													<img src="{IMAGEPATH}homestars.png" alt=" " style="width:59px;"/>
												</td>
												</tr>
												<tr>
									             <td height="15"></td>
								                </tr>
											</tbody>
										</table>
									</td>
								</tr>
								<!--- end 5 column icons grid--->
							</tbody>
					  </table> 
					</td> 
				  </tr>
			  </tbody>
		  </table>
	   </td>
	</tr>
   </tbody>
</table>
<!--- end body --->

<!--- start footer --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
			                  <td style="width:60px;">
				                 <p style="color:#fff; font-size:12px; text-transform:uppercase; line-height:17px; width:80px;">Franchise<br>Opportunities<br>available</p>
				              </td>
							  <td style="width:30px;"><img name="vborder" src="{IMAGEPATH}vborder.jpg" alt="vborder"/></td>
				              <td style="width:300px;">
				                 <p style="color:#fff; font-size:18px; text-transform:uppercase; line-height:18px; margin:15px 0 5px;">Be your own boss.</br> own a {BRAND} brand Franchise.</p>
				                 <p style="color:#fff; font-size:10px; line-height:14px; margin:5px 0;">This is not an offering to sell a franchise. Franchise offerings are made through a Franchise Disclosure Document.</p>
				              </td>
				              <td style="width:50px;">
				                  <a track="on" href="http://www.budget-blinds-franchise.com/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=franchise&amp;utm_campaign=Email-1stOctPromo2017" class="imgCaptionAnchor">
				                  <img  name="ACCOUNT.IMAGE.1203" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}learn-but.jpg"></a>
				              </td>
                           </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- start footer text --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				     <td width="100%"> 
					   <table width="644" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						 <tbody>
						    <tr>
							    <td height="20"></td>
							</tr>
							<tr>
                                <td style="padding: 0px 25px; font-size: 10pt; font-family: &quot;Lucida Sans Unicode&quot;, &quot;LucidaGrande&quot;, sans-serif; color: #69696d; line-height: 1.3;" valign="top" align="center">
                                   <div style="font-family: ''Century Gothic'', ''ITC Avant Garde'', Arial, Helvetica, sans-serif; font-size: 8pt;">
                                       <div style="text-align: left;" align="left"><span style="font-size: 8pt;">
                                          <div><span style="font-size:10pt;">This is a confirmation message only. No action is needed from you. {BRAND} Brand respects your privacy - view our <a style="text-decoration: underline;color: #3d7d8b;" href="http://www.budgetblinds.com/privacy-policy/">Privacy Policy.</a></span></div>
                                          <div><span style="font-size: 8pt;"><br></span></div>
                                          <div><span style="font-size: 10pt;">&copy;2018 {BRAND}, LLC. All Rights Reserved. <span>{BRAND}</span>is a trademark of <span>{BRAND}</span>, LLC and a Home Franchise Concepts Brand. Each franchise independently owned and operated. Franchise opportunities available.</span></div></span></div>
                                   </div>
                                </td>
	                        </tr>
						    <tr>
							    <td height="20"></td>
							</tr>
						 </tbody>
					   </table>
				     </td>
				  </tr>
			  </tbody>
		  </table>
	    </td>
	  </tr>
   </tbody>
</table>
<!--- end footer text --->

<!--- end footer --->
</tbody>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</body>
</html>
', 1, CAST(N'2018-12-31' AS Date), 1)


INSERT [CRM].[EmailTemplates] ([EmailTemplateId], [EmailTemplateTypeId], [Description], [TemplateSubject], [TemplateBody], [FranchiseId], [isDeleted], [TemplateLayout], [IsVisible], [IsVisibleAfterDate], [TemplateBrand]) VALUES (517, 6, N'Tailored Living - Thank You - Review', N'Thank You - Review', N'<div>
 <h1>
  Thank You - Review</h1>
</div>', NULL, 0, N'<html>
<head>
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
body{
	  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
       width:100% !important; 
	  -webkit-text-size-adjust:100%; 
	  -ms-text-size-adjust:100%;  
      margin:0; 
      padding:0; 
      -webkit-font-smoothing: antialiased;
      background:#fff;
}
html{
      width: 100%; 
}
table td, table td * {
      font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
}
</style>  
</head>
<body style="width: 100%; text-align: center;  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;">

<table width="100%" bgcolor="#fff" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#f0f1f1" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="padding:30px;"> 
				<tbody>
				  <tr> 
				    <td> 
					  <table  border="0" align="center" cellpadding="0" cellspacing="0" >
					     <tbody>
						    <tr> 
							   <td width="100%"> 
                                  <tbody>


<tr>
<td>
  <tbody>
    <tr>
      <td style="padding: 40px 0px 10px; font-size: 11pt; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #434343;" valign="top" align="center">
        <table cellpadding="0" cellspacing="0" data-padding-converted="true" width="366" align="center">
          <tbody>
            <tr>
              <td style="padding: 0px;" width="100%">
                <div align="center"><a track="on" href="http://www.tailoredliving.com/" class="imgCaptionAnchor">
				<img alt="Logo" title="Logo" height="64" width="200" name="ACCOUNT.IMAGE.1208" vspace="0" hspace="0" style="display: block; max-width: 100%; height: auto;" border="0" src="{IMAGEPATH}tailored_logo.png"></a></div>
              </td>
            </tr>
			<tr>
		      <td height="20"></td>
	        </tr>
          </tbody>
        </table>
        <br></td>
    </tr>
  </tbody>
</td>
</tr>
<!--- end logo --->

<!--- start navigation code --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                            <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.tailoredliving.com/closet-organization/" linktype="1" target="_blank">CLOSETS</a></strong></div>
                            </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.tailoredliving.com/garage-storage/" linktype="1" target="_blank">GARAGES</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 60px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.tailoredliving.com/home-office-design/" linktype="1" target="_blank">OFFICES</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 75px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.tailoredliving.com/home-organization/" linktype="1" target="_blank">PANTRIES & MORE</a></strong></div>
                           </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end navigation code --->

<!--- start banner --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                              <td>
                                 <div align="center"><a track="on" href="http://www.tailoredliving.com/" class="imgCaptionAnchor"><img width="644" name="ACCOUNT.IMAGE.1206" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}Relax.jpg"></a></div>
							  </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end banner code--->

<!--- body --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				   <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						<tbody>
						        <tr>
		                           <td height="35"></td>
	                            </tr>
	                            <tr>
	                				<td style="color:#69696d; text-align:center; font-size:40px; font-weight: normal;line-height: 2em;">
	                					Thank you
	                				</td>
	    						</tr>
	    						<tr>
									<td height="12"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:16px; text-align: center; font-weight: normal;line-height:20px; padding:0 30px;">
	                					Thank you for your business! We truly hope you are enjoying your new window coverings, and we look forward to keeping you as a long-time customer!
	                				</td>
	    						</tr>
								<tr>
									<td height="50"></td>
								</tr>
								<tr>
									<td><img name="hborder" src="{IMAGEPATH}hborder.jpg" alt="hborder"/></td>
								</tr>
								<tr>
									<td height="40"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:24px; text-align: center;font-weight:600; line-height:20px; padding:0 30px;">
	                					Have a great experiance?
	                				</td>
	    						</tr>
								<tr>
									<td height="10"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:20px; text-align: center; font-weight:600; font-style:italic; line-height:20px; padding:0 30px;">
	                					Leave us a review!
	                				</td>
	    						</tr>
								<tr>
									<td height="50"></td>
								</tr>
								<!-- 5 column icons grid--->
								<tr>
									<td>
										<table style="width:644px;" border="0" align="center" cellpadding="0" cellspacing="0">
											<tbody><tr>
												<td align="center">
													<img src="{IMAGEPATH}fb.png" alt=" " style="width:59px;"/>
												</td>
												<td align="center">
													<img src="{IMAGEPATH}yelp.png" alt=" " style="width:59px;"/>
												</td>
												<td align="center">
													<img src="{IMAGEPATH}Houzz.png" alt=" " style="width:59px;"/>
												</td>
												<td align="center">
													<img src="{IMAGEPATH}google.png" alt=" " style="width:59px;"/>
												</td>
												<td align="center">
													<img src="{IMAGEPATH}HomeStars.png" alt=" " style="width:59px;"/>
												</td>
												</tr>
												<tr>
									             <td height="20"></td>
								                </tr>
											 </tbody></table>
									</td>
								</tr>
								<!--- end 5 column icons grid--->
							</tbody>
					  </table> 
					</td> 
				   
				   </tr>
				 </tbody>
		  </table>
	   </td>
	</tr>
   </tbody>
</table>
<!--- end body --->

<!--- start footer --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
			                  <td style="width:60px;">
				                 <p style="color:#fff; font-size:12px; text-transform:uppercase; line-height:17px; width:80px;">Franchise<br>Opportunities<br>available</p>
				              </td>
							  <td style="width:30px;"><img name="vborder" src="{IMAGEPATH}vborder.jpg" alt="vborder"/></td>
				              <td style="width:300px;">
				                 <p style="color:#fff; font-size:18px; text-transform:uppercase; line-height:18px; margin:15px 0 5px;">Be your own boss.</br> own a {BRAND} brand Franchise.</p>
				                 <p style="color:#fff; font-size:10px; line-height:14px; margin:5px 0;">This is not an offering to sell a franchise. Franchise offerings are made through a Franchise Disclosure Document.</p>
				              </td>
				              <td style="width:50px;">
				                  <a track="on" href="http://www.budget-blinds-franchise.com/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=franchise&amp;utm_campaign=Email-1stOctPromo2017" class="imgCaptionAnchor">
				                  <img  name="ACCOUNT.IMAGE.1203" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}learn-but.jpg"></a>
				              </td>
                           </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- start footer text --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				     <td width="100%"> 
					   <table width="644" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						 <tbody>
						    <tr>
							    <td height="20"></td>
							</tr>
							<tr>
                                <td style="padding: 0px 25px; font-size: 10pt; font-family: &quot;Lucida Sans Unicode&quot;, &quot;LucidaGrande&quot;, sans-serif; color: #69696d; line-height: 1.3;" valign="top" align="center">
                                   <div style="font-family: ''Century Gothic'', ''ITC Avant Garde'', Arial, Helvetica, sans-serif; font-size: 8pt;">
                                       <div style="text-align: left;" align="left"><span style="font-size: 8pt;">
                                          <div><span style="font-size:10pt;">This is a confirmation message only. No action is needed from you. {BRAND} Brand respects your privacy - view our <a style="text-decoration: underline;color: #3d7d8b;" href="http://www.tailoredliving.com/privacy-policy/">Privacy Policy.</a></span></div>
                                          <div><span style="font-size: 8pt;"><br></span></div>
                                          <div><span style="font-size: 10pt;">&copy;2018 {BRAND} Brand. All Rights Reserved. <span>{BRAND}</span>featuring PremierGarage is a trademark of  <span>{BRAND}</span>, LLC and a Home Franchise Concepts Brand. Each franchise independently owned and operated. Franchise opportunities available.</span></div></span></div>
                                   </div>
                                </td>
	                        </tr>
						    <tr>
							    <td height="20"></td>
							</tr>
						 </tbody>
					   </table>
				     </td>
				  </tr>
			  </tbody>
		  </table>
	    </td>
	  </tr>
   </tbody>
</table>
<!--- end footer text --->

<!--- end footer --->
</tbody>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</body>
</html>
', 1, CAST(N'2018-12-31' AS Date), 2)



INSERT [CRM].[EmailTemplates] ([EmailTemplateId], [EmailTemplateTypeId], [Description], [TemplateSubject], [TemplateBody], [FranchiseId], [isDeleted], [TemplateLayout], [IsVisible], [IsVisibleAfterDate], [TemplateBrand]) VALUES (518, 6, N'Concrete Craft -Thank You - Review', N'Thank You - Review', N'<div>
 <h1>
  Thank You - Review</h1>
</div>', NULL, 0, N'<html>
<head>
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
body{
	  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
       width:100% !important; 
	  -webkit-text-size-adjust:100%; 
	  -ms-text-size-adjust:100%;  
      margin:0; 
      padding:0; 
      -webkit-font-smoothing: antialiased;
      background:#fff;
}
html{
      width: 100%; 
}
table td, table td * {
      font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;
}
</style>  
</head>
<body style="width: 100%; text-align: center;  font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif;">

<table width="100%" bgcolor="#fff" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#f0f1f1" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="padding:30px;"> 
				<tbody>
				  <tr> 
				    <td> 
					  <table  border="0" align="center" cellpadding="0" cellspacing="0" >
					     <tbody>
						    <tr> 
							   <td width="100%"> 
                                  <tbody>


<tr>
<td>
  <tbody>
    <tr>
      <td style="padding: 40px 0px 10px; font-size: 11pt; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #434343;" valign="top" align="center">
        <table cellpadding="0" cellspacing="0" data-padding-converted="true" width="366" align="center">
          <tbody>
            <tr>
              <td style="padding: 0px;" width="100%">
                <div align="center"><a track="on" href="https://www.concretecraft.com/" class="imgCaptionAnchor">
				<img alt="Logo" title="Logo" height="64" width="200" name="ACCOUNT.IMAGE.1208" vspace="0" hspace="0" style="display: block; max-width: 100%; height: auto;" border="0" src="{IMAGEPATH}concrete_logo.png"></a></div>
              </td>
            </tr>
			<tr>
		      <td height="20"></td>
	        </tr>
          </tbody>
        </table>
        <br></td>
    </tr>
  </tbody>
</td>
</tr>
<!--- end logo --->

<!--- start navigation code --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                            <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 80px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.concretecraft.com/stamped-concrete/" linktype="1" target="_blank">STAMPED</a></strong></div>
                            </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 80px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.concretecraft.com/resurfaced-concrete/" linktype="1" target="_blank">RESURFACED</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 80px;">
                             <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.concretecraft.com/stained-concrete/" linktype="1" target="_blank">STAINED</a></strong></div>
                           </td>
                           <td style="vertical-align: top; font-family: Century Gothic,ITC Avant Garde,Arial,Helvetica,sans-serif; color: #ffffff; font-size: 13.6667px; font-style: normal; font-weight: normal; width: 80px;">
                              <div style="text-align: center;" align="center"><strong><a style="font-weight: bold; color: #ffffff; text-decoration: none;" track="on" shape="rect" href="http://www.concretecraft.com/stamped-concrete/walls-fireplaces/" linktype="1" target="_blank">VERTICAL</a></strong></div>
                           </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end navigation code --->

<!--- start banner --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
                              <td>
                                 <div align="center"><a track="on" href="https://www.concretecraft.com/" class="imgCaptionAnchor"><img width="644" name="ACCOUNT.IMAGE.1206" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}Relax.jpg"></a></div>
							  </td>
                          </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- end banner code--->

<!--- body --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				   <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						<tbody>
						        <tr>
		                           <td height="35"></td>
	                            </tr>
	                            <tr>
	                				<td style="color:#69696d; text-align:center; font-size:40px; font-weight: normal;line-height: 2em;">
	                					Thank you
	                				</td>
	    						</tr>
	    						<tr>
									<td height="12"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:16px; text-align: center; font-weight: normal;line-height:20px; padding:0 30px;">
	                					Thank you for your business! We truly hope you are enjoying your new window coverings, and we look forward to keeping you as a long-time customer!
	                				</td>
	    						</tr>
								<tr>
									<td height="50"></td>
								</tr>
								<tr>
									<td><img name="hborder" src="{IMAGEPATH}hborder.jpg" alt="hborder"/></td>
								</tr>
								<tr>
									<td height="40"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:24px; text-align: center;font-weight:600; line-height:20px; padding:0 30px;">
	                					Have a great experiance?
	                				</td>
	    						</tr>
								<tr>
									<td height="10"></td>
								</tr>
								<tr>
	                				<td style="color:#69696d;font-size:20px; text-align: center; font-weight:600; font-style:italic; line-height:20px; padding:0 30px;">
	                					Leave us a review!
	                				</td>
	    						</tr>
								<tr>
									<td height="50"></td>
								</tr>
								<!-- 4 column icons grid--->
								<tr>
									<td>
										<table style="width:644px;" border="0" align="center" cellpadding="0" cellspacing="0">
											<tbody><tr>
												<td align="center">
													<img src="{IMAGEPATH}fb.png" alt=" " style="width:59px;"/>
												</td>
												<td align="center">
													<img src="{IMAGEPATH}yelp.png" alt=" " style="width:59px;"/>
												</td>
												<td align="center">
													<img src="{IMAGEPATH}Houzz.png" alt=" " style="width:59px;"/>
												</td>
												<td align="center">
													<img src="{IMAGEPATH}google.png" alt=" " style="width:59px;"/>
												</td>
												</tr>
												<tr>
									             <td height="15"></td>
								                </tr>
											 </tbody></table>
									</td>
								</tr>
								<!--- end 4 column icons grid--->
							</tbody>
					  </table> 
					</td> 
				   
				   </tr>
				 </tbody>
		  </table>
	   </td>
	</tr>
   </tbody>
</table>
<!--- end body --->

<!--- start footer --->
<table width="100%" bgcolor="" cellpadding="0" cellspacing="0" border="0"> 
	 <tbody>
		 <tr> 
		   <td> 
			  <table width="644" bgcolor="#69696d" cellpadding="0" cellspacing="0" border="0" align="center" style="padding:8px;"> 
				<tbody>
				  <tr> 
				    <td width="100%"> 
					  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">  
						<tbody>
						  <tr>
			                  <td style="width:60px;">
				                 <p style="color:#fff; font-size:12px; text-transform:uppercase; line-height:17px; width:80px;">Franchise<br>Opportunities<br>available</p>
				              </td>
							  <td style="width:30px;"><img name="vborder" src="{IMAGEPATH}vborder.jpg" alt="vborder"/></td>
				              <td style="width:300px;">
				                 <p style="color:#fff; font-size:18px; text-transform:uppercase; line-height:18px; margin:15px 0 5px;">Be your own boss.</br> own a {BRAND} brand Franchise.</p>
				                 <p style="color:#fff; font-size:10px; line-height:14px; margin:5px 0;">This is not an offering to sell a franchise. Franchise offerings are made through a Franchise Disclosure Document.</p>
				              </td>
				              <td style="width:50px;">
				                  <a track="on" href="http://www.budget-blinds-franchise.com/?utm_source=ConstantContact&amp;utm_medium=Email&amp;utm_content=franchise&amp;utm_campaign=Email-1stOctPromo2017" class="imgCaptionAnchor">
				                  <img  name="ACCOUNT.IMAGE.1203" vspace="0" hspace="0" class="cc-image-resize" style="display: block; border="0" src="{IMAGEPATH}learn-but.jpg"></a>
				              </td>
                           </tr>
						</tbody>
					  </table> 
					</td> 
				  </tr> 
				</tbody>
			  </table> 
			</td> 
		</tr> 
	 </tbody>
</table>
<!--- start footer text --->
<table width="100%" cellpadding="0" cellspacing="0" border="0"> 
  <tbody>
	<tr> 
	   <td> 
		  <table width="644" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"> 
			  <tbody>
			      <tr> 
				     <td width="100%"> 
					   <table width="644" cellpadding="0" cellspacing="0" border="0" align="center" style="text-align: center;">  
						 <tbody>
						    <tr>
							    <td height="20"></td>
							</tr>
							<tr>
                                <td style="padding: 0px 25px; font-size: 10pt; font-family: &quot;Lucida Sans Unicode&quot;, &quot;LucidaGrande&quot;, sans-serif; color: #69696d; line-height: 1.3;" valign="top" align="center">
                                   <div style="font-family: ''Century Gothic'', ''ITC Avant Garde'', Arial, Helvetica, sans-serif; font-size: 8pt;">
                                       <div style="text-align: left;" align="left"><span style="font-size: 8pt;">
                                          <div><span style="font-size:10pt;">This is a confirmation message only. No action is needed from you. {BRAND} Brand respects your privacy - view our <a style="text-decoration: underline;color: #3d7d8b;" href="http://www.concretecraft.com/privacy-policy/">Privacy Policy.</a></span></div>
                                          <div><span style="font-size: 8pt;"><br></span></div>
                                          <div><span style="font-size: 10pt;">&copy;2018 American Decorative Coatings, LLC. All Rights Reserved. <span>{BRAND} Brand&nbsp;</span>is a trademark of American Decorative Coatings,LLC and a Home Franchise Concepts Brand. Each franchise independently owned and operated. Franchise opportunities available.</span></div></span></div>
                                   </div>
                                </td>
	                        </tr>
						    <tr>
							    <td height="20"></td>
							</tr>
						 </tbody>
					   </table>
				     </td>
				  </tr>
			  </tbody>
		  </table>
	    </td>
	  </tr>
   </tbody>
</table>
<!--- end footer text --->

<!--- end footer --->
</tbody>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</body>
</html>
', 1, CAST(N'2018-12-31' AS Date), 3)



SET IDENTITY_INSERT [CRM].[EmailTemplates] OFF


GO
