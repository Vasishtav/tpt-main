ALTER TABLE crm.franchise
  ADD QuoteExpiryDays INT NOT NULL DEFAULT(30)

GO

ALTER TABLE crm.franchise
  ADD Currency varchar(2) NOT NULL DEFAULT('US')
Go

--====================

ALTER TABLE [CRM].[QuoteLines]
ADD Memo varchar(500) NULL;

ALTER TABLE [CRM].[QuoteLines]
ADD PICJson varchar(max) NULL;

ALTER TABLE [CRM].[QuoteLines]
ADD ProductTypeId int null default(1);

ALTER TABLE [CRM].[QuoteLines]
ADD Quntity int not null default(1);

ALTER TABLE [CRM].[QuoteLines]
Alter column MountType varchar(20) null ;

--========================

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [CRM].[TPTNumbers](
	[FranchiseId] [int] NOT NULL,
	[AccountNumber] [int] NOT NULL DEFAULT ((1000)),
	[QuoteNumber] [int] NOT NULL DEFAULT ((1000)),
	[OrderNumber] [int] NOT NULL DEFAULT ((1000)),
	[InvoiceNumber] [int] NOT NULL DEFAULT ((1000)),
	[PONumber] [int] NOT NULL DEFAULT ((1000))
 CONSTRAINT [PK_TPTNumbers] PRIMARY KEY CLUSTERED 
(
	[FranchiseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

---====================================

IF EXISTS
(
	SELECT *
	FROM sysobjects
	WHERE id = OBJECT_ID(N'CRM.SPGetQuoteLines') AND 
		  OBJECTPROPERTY(id, N'IsProcedure') = 1
)
BEGIN
	DROP PROCEDURE CRM.SPGetQuoteLines;
END;
GO

SET ANSI_NULLS ON;
GO

-- ================================================

SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO
-- =============================================
-- Author:		Vasishta Veeramasuneni
-- Create date: 12/18/2017
-- Description:	Get all quote lines based on QuoteId for both My Products and Vendor alliance Products
-- =============================================

CREATE PROCEDURE CRM.SPGetQuoteLines 
				 @quoteId int
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT prd.ProductName, ven.Name AS VendorName, Q.MeasurementId, QL.[QuoteLineId], QL.[QuoteKey], QL.[MeasurementsetId], QL.[VendorId], QL.[ProductId], QL.[SuggestedResale], QL.[Discount], QL.[CreatedOn], QL.[CreatedBy], QL.[LastUpdatedOn], QL.[LastUpdatedBy], QL.[IsActive], QL.[Width], QL.[Height], QL.[UnitPrice], QL.[Quantity], QL.[Description], QL.[MountType], QL.[ExtendedPrice], QL.[PICJson], QL.[ProductTypeId], QL.[Memo]
	FROM [CRM].[QuoteLines] AS QL
		 INNER JOIN
		 CRM.Quote AS Q
		 ON Q.QuoteKey = QL.[QuoteKey]
		 LEFT JOIN
		 ACCT.HFCVendors AS ven
		 ON ven.VendorId = ql.VendorId
		 LEFT JOIN
		 [CRM].[HFCProducts] AS prd
		 ON prd.ProductId = ql.ProductId
	WHERE QL.ProductId = 1 AND  -- product alliance
		  ql.IsActive = 1 AND 
		  QL.[QuoteKey] = @quoteId
	UNION ALL

	SELECT prd.ProductName, ven.Name AS VendorName, Q.MeasurementId, QL.[QuoteLineId], QL.[QuoteKey], QL.[MeasurementsetId], QL.[VendorId], QL.[ProductId], QL.[SuggestedResale], QL.[Discount], QL.[CreatedOn], QL.[CreatedBy], QL.[LastUpdatedOn], QL.[LastUpdatedBy], QL.[IsActive], QL.[Width], QL.[Height], QL.[UnitPrice], QL.[Quantity], QL.[Description], QL.[MountType], QL.[ExtendedPrice], QL.[PICJson], QL.[ProductTypeId], QL.[Memo]
	FROM [CRM].[QuoteLines] AS QL
		 INNER JOIN
		 CRM.Quote AS Q
		 ON Q.QuoteKey = QL.[QuoteKey]
		 LEFT JOIN
		 Acct.FranchiseVendors AS ven
		 ON ven.VendorId = ql.VendorId
		 LEFT JOIN
		 [CRM].[FranchiseProducts] AS prd
		 ON prd.ProductId = ql.ProductId
	WHERE QL.ProductId IN( 3, 2 ) AND --My products or services 
		  ql.IsActive = 1 AND 
		  QL.[QuoteKey] = @quoteId
	UNION ALL

	SELECT '' AS ProductName, '' AS VendorName, Q.MeasurementId, QL.[QuoteLineId], QL.[QuoteKey], QL.[MeasurementsetId], QL.[VendorId], QL.[ProductId], QL.[SuggestedResale], QL.[Discount], QL.[CreatedOn], QL.[CreatedBy], QL.[LastUpdatedOn], QL.[LastUpdatedBy], QL.[IsActive], QL.[Width], QL.[Height], QL.[UnitPrice], QL.[Quantity], QL.[Description], QL.[MountType], QL.[ExtendedPrice], QL.[PICJson], QL.[ProductTypeId], QL.[Memo]
	FROM [CRM].[QuoteLines] AS QL
		 INNER JOIN
		 CRM.Quote AS Q
		 ON Q.QuoteKey = QL.[QuoteKey]
	WHERE ql.IsActive = 1 AND 
		  QL.[QuoteKey] = @quoteId;

END;
GO

---=============================================

IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[CRM].[spGetNextNumber]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE [CRM].[spGetNextNumber] 
END
GO
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO

-- =============================================
-- Author:		Vasishta Veeramasuneni
-- Create date: 12/15/2017
-- Description:	Get the next number for Account, order, Invoice etc..,
-- =============================================

CREATE PROCEDURE [CRM].[spGetNextNumber] 
				 @FranchiseId int, @numberType int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF EXISTS
	(
		SELECT *
		FROM [CRM].[TPTNumbers]
		WHERE FranchiseId = @FranchiseId
	)
	BEGIN

		IF(@numberType = 1)
		BEGIN
			UPDATE [CRM].[TPTNumbers]
			  SET AccountNumber = AccountNumber + 1
			OUTPUT INSERTED.AccountNumber AS nextNumber
			WHERE FranchiseId = @FranchiseId;
		END;
		IF(@numberType = 2)
		BEGIN
			UPDATE [CRM].[TPTNumbers]
			  SET [QuoteNumber] = [QuoteNumber] + 1
			OUTPUT INSERTED.[QuoteNumber] AS nextNumber
			WHERE FranchiseId = @FranchiseId;
		END;
		IF(@numberType = 3)
		BEGIN
			UPDATE [CRM].[TPTNumbers]
			  SET [OrderNumber] = [OrderNumber] + 1
			OUTPUT INSERTED.[OrderNumber] AS nextNumber
			WHERE FranchiseId = @FranchiseId;
		END;
		IF(@numberType = 4)
		BEGIN
			UPDATE [CRM].[TPTNumbers]
			  SET [InvoiceNumber] = [InvoiceNumber] + 1
			OUTPUT INSERTED.[InvoiceNumber] AS nextNumber
			WHERE FranchiseId = @FranchiseId;
		END;
		IF(@numberType = 5)
		BEGIN
			UPDATE [CRM].[TPTNumbers]
			  SET [PONumber] = [PONumber] + 1
			OUTPUT INSERTED.[PONumber] AS nextNumber
			WHERE FranchiseId = @FranchiseId;
		END;

	END;
	ELSE
	BEGIN

		IF(@numberType = 1)
		BEGIN
			INSERT INTO CRM.[TPTNumbers]( FranchiseId )
			OUTPUT INSERTED.AccountNumber AS nextNumber
			VALUES( @FranchiseId );
		END;
		IF(@numberType = 2)
		BEGIN

			INSERT INTO CRM.[TPTNumbers]( FranchiseId )
			OUTPUT INSERTED.[QuoteNumber] AS nextNumber
			VALUES( @FranchiseId );
		END;
		IF(@numberType = 3)
		BEGIN
			INSERT INTO CRM.[TPTNumbers]( FranchiseId )
			OUTPUT INSERTED.[OrderNumber] AS nextNumber
			VALUES( @FranchiseId );
		END;
		IF(@numberType = 4)
		BEGIN
			INSERT INTO CRM.[TPTNumbers]( FranchiseId )
			OUTPUT INSERTED.[InvoiceNumber] AS nextNumber
			VALUES( @FranchiseId );
		END;
		IF(@numberType = 5)
		BEGIN
			INSERT INTO CRM.[TPTNumbers]( FranchiseId )
			OUTPUT INSERTED.[PONumber] AS nextNumber
			VALUES( @FranchiseId );
		END;
	END;
END;


GO

---===============================================

IF EXISTS
(
	SELECT *
	FROM sysobjects
	WHERE id = OBJECT_ID(N'CRM.InsertQuote ') AND 
		  OBJECTPROPERTY(id, N'IsProcedure') = 1
)
BEGIN
	DROP PROCEDURE CRM.InsertQuote;
END;
GO

SET ANSI_NULLS ON;
GO

-- ================================================

SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

CREATE PROCEDURE CRM.InsertQuote 
				 @OpportunityId int, @QuoteName varchar(100), @PrimaryQuote bit, @QuoteStatusId int, @Discount numeric(18, 2), @DiscountType varchar(10), @TotalDiscounts numeric(18, 2), @NetCharges numeric(18, 2), @CreatedOn datetime2(7), @CreatedBy int, @LastUpdatedOn datetime2(7), @LastUpdatedBy int, @AdditionalCharges numeric(18, 2), @QuoteNumber int, @TotalMargin numeric(18, 2), @ProductSubTotal numeric(18, 2), @QuoteSubTotal numeric(18, 2), @MeasurementId int, @FranchiseId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @quoteId int, @ExpiryDate datetime2(7), @expiryDays int;

	SELECT @expiryDays = QuoteExpiryDays
	FROM crm.Franchise
	WHERE FranchiseId = @FranchiseId;

	SET @ExpiryDate = GETDATE();

	SELECT @ExpiryDate = DATEADD(dd, @expiryDays, GETUTCDATE());

	EXEC @quoteId = [CRM].[spGetNextNumber] @FranchiseId, 2; -- for Quote Number

	INSERT INTO CRM.Quote( QuoteID, OpportunityId, QuoteName, PrimaryQuote, QuoteStatusId, Discount, DiscountType, TotalDiscounts, NetCharges, CreatedOn, CreatedBy, LastUpdatedOn, LastUpdatedBy, ExpiryDate, AdditionalCharges, QuoteNumber, TotalMargin, ProductSubTotal, QuoteSubTotal, MeasurementId )
	VALUES( @quoteId, @OpportunityId, @QuoteName, @PrimaryQuote, @QuoteStatusId, @Discount, @DiscountType, @TotalDiscounts, @NetCharges, @CreatedOn, @CreatedBy, @LastUpdatedOn, @LastUpdatedBy, @ExpiryDate, @AdditionalCharges, @QuoteNumber, @TotalMargin, @ProductSubTotal, @QuoteSubTotal, @MeasurementId );

END;
GO

