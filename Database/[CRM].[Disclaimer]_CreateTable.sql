
CREATE TABLE [CRM].[Disclaimer](
	[Id] [int] IDENTITY(1,1) NOT NULL,	
	[FranchiseId] [int] NOT NULL,
	ShortDisclaimer varchar(500) null,
	LongDisclaimer varchar(max) null,
	[CreatedOn] [datetime2](7) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastUpdatedOn] [datetime2](7) NULL,
	[LastUpdatedBy] [int] NULL,
 CONSTRAINT [PK_Disclaimer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO