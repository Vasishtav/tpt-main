USE [MyCrmTP03]
GO

/****** Object:  Table [CRM].[Type_PaymentMethod]    Script Date: 10-Dec-17 8:32:39 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [CRM].[Type_PricingStrategyType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PricingStrategyType] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[LastUpdatedOn] [datetime2](7) NULL,
	[LastUpdatedBy] [int] NULL,
 CONSTRAINT [PK_Type_PricingStrategyType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [CRM].[Type_PricingStrategyType]  WITH CHECK ADD  CONSTRAINT [FK_Type_PricingStrategyType_Createdby] FOREIGN KEY([CreatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO

ALTER TABLE [CRM].[Type_PricingStrategyType] CHECK CONSTRAINT [FK_Type_PricingStrategyType_Createdby]
GO

ALTER TABLE [CRM].[Type_PricingStrategyType]  WITH CHECK ADD  CONSTRAINT [FK_Type_PricingStrategyType_Modifiedby] FOREIGN KEY([LastUpdatedBy])
REFERENCES [CRM].[Person] ([PersonId])
GO

ALTER TABLE [CRM].[Type_PricingStrategyType] CHECK CONSTRAINT [FK_Type_PricingStrategyType_Modifiedby]
GO


