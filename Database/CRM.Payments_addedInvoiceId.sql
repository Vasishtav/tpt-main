  Alter Table [CRM].[Payments] add [InvoiceId] int 
  CONSTRAINT [FK_Payment_Invoice] FOREIGN KEY ([InvoiceId]) 
  REFERENCES [CRM].[Invoices] (InvoiceId)