
alter table [CRM].[Quote] add 
ExpiryDate datetime2(7)  NULL,
AdditionalCharges Numeric(18,2)  NULL,
QuoteNumber int  NULL,
TotalMargin Numeric(18,2)  NULL,
ProductSubTotal Numeric(18,2)  NULL,
QuoteSubTotal Numeric(18,2)  NULL,
MeasurementId int  NULL

alter table [CRM].[Quote] drop column TotalCharges,MeasurementsetId
alter table [CRM].[Quote] alter column CreatedOn datetime2(7) not null
alter table [CRM].[Quote] alter column LastUpdatedOn datetime2(7)

ALTER TABLE [CRM].[Quote]  WITH CHECK ADD  CONSTRAINT [FK_Quote_MeasurementHeader_MeasurementId] FOREIGN KEY([MeasurementId])
REFERENCES [CRM].[MeasurementHeader] ([Id])
GO

ALTER TABLE [CRM].[Quote] CHECK CONSTRAINT [FK_Quote_MeasurementHeader_MeasurementId]
GO







