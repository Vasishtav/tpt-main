﻿using HFC.CRM.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HFC.CRM.Web
{
    /// <summary>
    /// This attribute will ensure that all our normal requests are linked to a franchise
    /// </summary>
    public class ValidateFranchise : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return SessionManager.CurrentFranchise != null
                && SessionManager.CurrentFranchise.FranchiseId > 0;
        }
    }
}