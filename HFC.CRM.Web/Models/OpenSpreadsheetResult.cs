﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HFC.CRM.Web
{
    public class OpenSpreadsheetResult : ActionResult
    {
        private MemoryStream _stream;
        private string _filename;

        public OpenSpreadsheetResult(MemoryStream stream, string filename)
        {
            if (string.IsNullOrEmpty(filename))
                throw new NullReferenceException("Open Spreadsheet filename can not be null/empty");
            _filename = filename;
            _stream = stream;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            context.HttpContext.Response.Clear();
            context.HttpContext.Response.AddHeader("content-disposition",
              String.Format("attachment;filename={0}", _filename));
            context.HttpContext.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            _stream.WriteTo(context.HttpContext.Response.OutputStream);
            _stream.Close();
            context.HttpContext.Response.End();
        }
    }
}