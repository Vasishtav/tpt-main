﻿using HFC.CRM.Data;
namespace HFC.CRM.Web
{
    using System;
    using System.Data;
    using System.Data.Entity;
    using System.Linq;
    using System.Web.Mvc;

    public class SchedulerTaskService
    {
        private CRMContext db;

        public SchedulerTaskService(CRMContext context)
        {
            db = context;
        }

        public SchedulerTaskService()
            : this(new CRMContext())
        {
        }

        public virtual IQueryable<Calendar> GetRange(DateTime? start, DateTime? end)
        {
            //return GetAll().Where(t => (t.Start >= start || t.Start <= start) && t.Start <= end
            //    && t.End >= start && (t.End >= end || t.End <= end) || t.RecurrenceRule != null);
            return GetAll();
        }

        public virtual IQueryable<Calendar> GetAll()
        {
            return db.Calendars.ToList().AsQueryable();
            //return db.Tasks.ToList().Select(task => new TaskViewModel
            //{
            //    TaskID = task.TaskID,
            //    Title = task.Title,
            //    Start = DateTime.SpecifyKind(task.Start, DateTimeKind.Utc),
            //    End = DateTime.SpecifyKind(task.End, DateTimeKind.Utc),
            //    StartTimezone = task.StartTimezone,
            //    EndTimezone = task.EndTimezone,
            //    Description = task.Description,
            //    IsAllDay = task.IsAllDay,
            //    RecurrenceRule = task.RecurrenceRule,
            //    RecurrenceException = task.RecurrenceException,
            //    RecurrenceID = task.RecurrenceID,
            //    OwnerID = task.OwnerID
            //}).AsQueryable();  
        }

        public virtual void Insert(TaskViewModel task, ModelStateDictionary modelState)
        {
            if (ValidateModel(task, modelState))
            {
               // var entity = task.ToEntity();

              //  db.Tasks.Add(entity);
                db.SaveChanges();

               // task.TaskID = entity.TaskID;
            }
        }

        public virtual void Update(TaskViewModel task, ModelStateDictionary modelState)
        {
            if (ValidateModel(task, modelState))
            {
             //   var entity = task.ToEntity();
              //  db.Tasks.Attach(entity);
              //  db.Entry(entity).State = EntityState.Modified;                
                db.SaveChanges();
            }
        }

        
        private bool ValidateModel(TaskViewModel appointment, ModelStateDictionary modelState)
        {
            if (appointment.Start > appointment.End)
            {
                modelState.AddModelError("errors", "End date must be greater or equal to Start date.");
                return false;
            }
            
            return true;
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}