﻿namespace HFC.CRM.Web.Models.Manufacturer
{
    using System.ComponentModel.DataAnnotations;

    public class ManufacturerContactModel
    {
        [Required]
        public string Contact { get; set; }
        public string ContactNumber { get; set; }
        public string ContactEmail { get; set; }
        public string AccountNumber { get; set; }
    }
}