﻿using System;
using System.Collections.Generic;
using System.Linq;

using HFC.CRM.Data;

namespace HFC.CRM.Web.Models
{
    public class FranchiseModel
    {

        public int FranchiseId { get; set; }
        public Guid FranchiseGuid { get; set; }
        public string Name { get; set; }
        
        public string Code { get; set; }
       
        public int BrandId { get; set; }
        public string Website { get; set; }
        private DateTime _CreatedOnUtc = DateTime.Now;
        public DateTime CreatedOnUtc
        {
            get { return _CreatedOnUtc; }
            set { _CreatedOnUtc = DateTime.SpecifyKind(value, DateTimeKind.Unspecified); }
        }
        public bool IsDeleted { get; set; }
        public TimeZoneEnum TimezoneCode { get; set; }
        public string LogoImgUrlRelativePath { get; set; }
        public int? AddressId { get; set; }
        public int? MailingAddressId { get; set; }
        public string TollFreeNumber { get; set; }
        public string LocalPhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string SupportEmail { get; set; }
        public string AdminEmail { get; set; }
        public string OwnerEmail { get; set; }
        private DateTime? _LastUpdatedUtc;
        public DateTime? LastUpdatedUtc
        {
            get { return _LastUpdatedUtc; }
            set
            {
                if (value == null)
                    _LastUpdatedUtc = null;
                else
                    _LastUpdatedUtc = DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified);
            }
        }
        public int? LastUpdatedByPersonId { get; set; }

        public bool? IsSuspended { get; set; }
        public bool? IsSolaTechEnabled { get; set; }
        public bool? IsQBEnabled { get; set; }        
        public AddressModel Address { get; set; }
        public AddressModel MailingAddress { get; set; }
        public string OwnerName { get; set; }

        //start:Newly added field
        public string DBAName { get; set; }
        public string DMA { get; set; }
        public bool IsAddressSame { get; set; }
        public string AdminName { get; set; }
        public string CoachName { get; set; }
        public string Region { get; set; }
        public string BusinessPhone { get; set; }
        public string Currency { get; set; }
        public int OwnerId { get; set; }
        public List<OwnerVM> lstOwnerName { get; set; }
        // end
        public virtual IList<TerritoriesModel> Territories { get; set; }
        public virtual IList<FranchiseOwnerModel> FranchiseOwners { get; set; }

        public static Franchise MapFrom(FranchiseModel model)
        {
            return new Franchise()
            {
                FranchiseId = model.FranchiseId,
                FranchiseGuid = model.FranchiseGuid,
                Name = model.Name,
                Code = model.Code,
                BrandId = model.BrandId,
                Website = model.Website,
                CreatedOnUtc = model.CreatedOnUtc,
                AddressId = model.AddressId,
                MailingAddressId=model.MailingAddressId,
                IsDeleted = model.IsDeleted,
                TimezoneCode = model.TimezoneCode,
                LogoImgUrlRelativePath = model.LogoImgUrlRelativePath,
                TollFreeNumber = model.TollFreeNumber,
                LocalPhoneNumber = model.LocalPhoneNumber,
                FaxNumber = model.FaxNumber,
                SupportEmail = model.SupportEmail,
                AdminEmail = model.AdminEmail,
                OwnerEmail = model.OwnerEmail,
                LastUpdatedUtc = model.LastUpdatedUtc,
                LastUpdatedByPersonId = model.LastUpdatedByPersonId,
                IsSuspended = model.IsSuspended,
                isSolaTechEnabled = model.IsSolaTechEnabled,
                Address = AddressModel.MapFrom(model.Address),
                MailingAddress= AddressModel.MapFrom(model.MailingAddress),
                FranchiseOwners = FranchiseOwnerModel.MapFrom(model.FranchiseOwners.ToList()),
                OwnerName=model.OwnerName,
                DBAName = model.DBAName,
                DMA= model.DMA,
                IsAddressSame = model.IsAddressSame,
                AdminName = model.AdminName,
                CoachName = model.CoachName,
                Region = model.Region,
                BusinessPhone= model.BusinessPhone,
                Currency = model.Currency,
                OwnerId = model.OwnerId,
                lstOwnerName = model.lstOwnerName
            };
        }

        public static FranchiseModel MapTo(Franchise model)
        {
            return new FranchiseModel() {
                FranchiseId = model.FranchiseId,
                FranchiseGuid = model.FranchiseGuid,
                Name = model.Name,
                Code = model.Code,
                BrandId = model.BrandId,
                Website = model.Website,
                CreatedOnUtc = model.CreatedOnUtc,
                AddressId = model.AddressId,
                MailingAddressId=model.MailingAddressId,
                IsDeleted = model.IsDeleted,
                TimezoneCode = model.TimezoneCode,
                LogoImgUrlRelativePath = model.LogoImgUrlRelativePath,
                TollFreeNumber =model.TollFreeNumber,
                //RegexUtil.FormatUSPhone(model.TollFreeNumber, RegexUtil.PhoneFormat.Parenthesis),
                //model.TollFreeNumber,
                LocalPhoneNumber = model.LocalPhoneNumber,
                FaxNumber = model.FaxNumber,
                SupportEmail = model.SupportEmail,
                AdminEmail = model.AdminEmail,
                OwnerEmail = model.OwnerEmail,
                LastUpdatedUtc = model.LastUpdatedUtc,
                LastUpdatedByPersonId = model.LastUpdatedByPersonId,
                IsSuspended = model.IsSuspended,
                IsSolaTechEnabled = model.isSolaTechEnabled,
                Address=AddressModel.MapTo( model.Address),
                MailingAddress= AddressModel.MapTo(model.MailingAddress),
                Territories = TerritoriesModel.MapTo(model.Territories),
                FranchiseOwners=FranchiseOwnerModel.MapTo(model.FranchiseOwners),
                IsQBEnabled=model.IsQBEnabled,
                DBAName = model.DBAName,
                DMA = model.DMA,
                IsAddressSame = model.IsAddressSame,
                AdminName = model.AdminName,
                CoachName = model.CoachName,
                Region = model.Region,
                BusinessPhone = model.BusinessPhone,
                Currency = model.Currency,
                OwnerId = model.OwnerId,
                lstOwnerName = model.lstOwnerName
            };
        }
    }
}
