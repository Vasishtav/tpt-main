﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HFC.CRM.Web.com.solatech.webOE;
using HFC.CRM.Core;
using System.Xml;
using System.Xml.Linq;
using HFC.CRM.Data;
using HFC.CRM.Data.webOE;
using System.Text;
using HFC.CRM.Core.Logs;

namespace HFC.CRM.Web
{
    public static class SolatechWebOEExtension
    {
        public static int WSAC { get { return AppConfigManager.GetConfig<int>("WSAC", "Solatech", "Auth"); } }
        public static string API_AccountID { get { return AppConfigManager.GetConfig<string>("SolatechID", "Solatech", "Auth"); } }
        public static string Password { get { return AppConfigManager.GetConfig<string>("password", "Solatech", "Auth"); } }

        public static short Session_Type { get { return AppConfigManager.GetConfig<short>("SessionType", "Solatech", "webOE"); } }

        public static bool PushOrderToWebOE(this com.solatech.webOE.Client webOESrv,  WebOE webOE, out WebOEResult result)
        {
            var valid = false;
            result = null;

            try
            {
                int sessionId = 0, companyId =0, orderId = 0, orderItemId =0, vendorOrderId = 0;
                var tat = new com.solatech.Auth.Client().LoginExt();
                
                if(!string.IsNullOrEmpty(tat))
                {
                    var auid = webOESrv.ExternalAccountIsEnabledExt(tat, webOE.FranchiseGuid);

                    if (!string.IsNullOrEmpty(auid))
                    {
                        sessionId = webOESrv.StartSessionExt(tat, auid);

                        companyId = webOESrv.AddCompanyExt(tat, auid, webOE.Company);

                        orderId = webOESrv.AddPurchaseOrderExt(tat, sessionId, companyId, webOE.Order);

                        if (orderId != 0)
                        {
                            valid = true;

                            vendorOrderId = webOESrv.AddPurchaseOrderVendorExt(tat, orderId, webOE.Vendor);

                            if (vendorOrderId != 0)
                            {
                                result = new WebOEResult()
                                {
                                    CompanyId = companyId.ToString(),
                                    OrderId = orderId.ToString(),
                                    OrderItemId = orderItemId.ToString(),
                                    VendorOrderId = vendorOrderId.ToString(),
                                    SessionId = sessionId
                                };

                                foreach (var orderItem in webOE.OrderItems)
                                {
                                    orderItemId = webOESrv.AddPurchaseOrderItemExt(tat, orderId, orderItem);
                                    var validOptions = webOESrv.AddPurchaseOrderItemOptionsExt(tat, orderItemId, orderItem.OptionsData);
                                }
                                result.OrderItemId = orderItemId.ToString();

                                //make sure eveything good before, process purchase orders.
                                if (orderItemId != 0)
                                {
                                    result.Process = webOESrv.ProcessPurchaseOrderExt(tat, auid, orderId);
                                    result.Commit = webOESrv.CommitSessionExt(tat, sessionId.ToString());
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                EventLogger.LogEvent(ex);
                throw;
            }

            return valid;
        }

        public static string LoginExt(this com.solatech.Auth.Client auth)
        {
           var msg = auth.Login(WSAC, API_AccountID, Password);
           return GetValue(msg, "TAT"); 
        }

        public static string CreateExternalAccountExt(this com.solatech.webOE.Client webOE, string franchiseGuid, string franchiseEmail)
        {
            var tat =  new com.solatech.Auth.Client().LoginExt();
            var msg = webOE.CreateExternalAccount(tat, franchiseGuid, franchiseEmail);
            return GetValue(msg, "WasCreated");
        }

        public static string  ExternalAccountIsEnabledExt(this com.solatech.webOE.Client webOE, string tat, string franchiseGuid)
        {
             var msg = webOE.ExtrnalAccountIsEnabled(tat, franchiseGuid);
             return GetValue(msg, "AUID"); 
        }

        public static int StartSessionExt(this com.solatech.webOE.Client webOE, string tat, string auid)
        {
            var msg = webOE.StartSession(tat, auid, Session_Type);
            return Int32.Parse(GetValue(msg, "ID"));
        }

        private static bool CommitSessionExt(this Client webOE, string tat, string sessionId)
        { 
            var valid = false;

            if(!string.IsNullOrEmpty(sessionId))
            {
                var session = Int32.Parse(sessionId);
                var msg = webOE.CommitSession(tat, session);
                if(GetValue(msg, "WasCommitted").Equals("True"))
                    valid = true;
            }

            return valid;
        }

        private static int AddCompanyExt(this Client webOE, string tat, string AUID, WebOECompany company)
        {
            var msg = webOE.AddCompany(tat
                                       ,AUID
                                       ,company.Name
                                       ,company.Phone1
                                       ,company.Phone2
                                       ,company.Fax1
                                       ,company.Fax2
                                       ,company.Email
                                       ,company.WebSite
                                       ,company.BillToAddress1
                                       ,company.BillToAddress2
                                       ,company.BillToCity
                                       ,company.BillToState
                                       ,company.BillToZip
                                       ,company.BillToCountry
                                       ,company.ShipToAddress1
                                       ,company.ShipToAddress2
                                       ,company.ShipToCity
                                       ,company.ShipToState
                                       ,company.ShipToZip
                                       ,company.ShipToCountry
                                       ,company.LicenseProductId
                                       ,company.UniqueId);
            var result = GetValue(msg, "ID");

           return Int32.Parse(result);
        }

        private static int AddPurchaseOrderExt(this Client webOE, string tat, int sessionId, int companyId, WebOEOrder order)
        {
            var msg = webOE.AddPurchaseOrder(tat
                            ,sessionId
                            ,companyId
                            ,order.POrderNoBB
                            ,order.POrderDateBB
                            ,order.AltPOrderNo
                            ,order.CustomerPOrderNo
                            ,order.CustomerName
                            ,order.SaleRep
                            ,order.SideMark
                            ,order.BillToName
                            ,order.BillToAddress1
                            ,order.BillToAddress2
                            ,order.BillToCity
                            ,order.BillToState
                            ,order.BillToZip
                            ,order.BillToCountry
                            ,order.IsDropShip
                            ,order.ShipToName
                            ,order.ShipToAddress1
                            ,order.ShipToAddress2
                            ,order.ShipToCity
                            ,order.ShipToState
                            ,order.ShipToZip
                            ,order.ShipToCountry
                            ,order.Cost
                            ,order.Origin
                            ,order.UniqueId
                            ,order.UniqueId);

            var result = GetValue(msg, "ID");
            int retValue = 0;

            if (string.IsNullOrEmpty(result))
            {
                result = GetValue(msg, "Exists");
                if (result.Equals("True"))
                {
                    Int32.TryParse(order.InternalRefNumber, out retValue);
                }
            }
            else {
                Int32.TryParse(result, out retValue);
            }

            return retValue;
        }

        private static int AddPurchaseOrderVendorExt(this Client webOE, string tat, int orderId, WebOEVendor vendor)
        {
            var msg = webOE.AddPurchaseOrderVendor(tat
                             ,orderId
                            , vendor.Name
                            , vendor.AccoutNumber
                            , vendor.BillToName
                            , vendor.BillToAddress1
                            , vendor.BillToAddress2
                            , vendor.BillToCity
                            , vendor.BillToState
                            , vendor.BillToZip
                            , vendor.BillToCountry
                            , vendor.ShipToName
                            , vendor.ShipToAddress1
                            , vendor.ShipToAddress2
                            , vendor.ShipToCity
                            , vendor.ShipToState
                            , vendor.ShipToZip
                            , vendor.ShipToCountry
                            , vendor.ShipVia
                            , vendor.Terms
                            , vendor.OOPProcess
                            , vendor.OOPPrintPO
                            , vendor.OOPVendorID
                            , vendor.UniqueId);

            var result = GetValue(msg, "ID");

            return Int32.Parse(result);
        }

        private static int AddPurchaseOrderItemExt(this Client webOE, string TAT, int OrderId, WebOEOrderItem orderItem)
        {
            var msg = webOE.AddPurchaseOrderItem(TAT
                                                 ,OrderId
                                                 ,orderItem.Quantity
                                                 ,orderItem.ProductName
                                                 ,orderItem.ProductType
                                                 ,orderItem.Manufacturer
                                                 ,orderItem.Style
                                                 ,orderItem.LineItem
                                                 ,orderItem.Description
                                                 ,orderItem.Description2
                                                 ,orderItem.Notes
                                                 ,orderItem.BeforeDiscountRetailPrice
                                                 ,orderItem.AfterDiscountRetailPrice
                                                 ,orderItem.CustomerDiscount
                                                 ,orderItem.VendorDiscount
                                                 ,orderItem.RetailPrice
                                                 ,orderItem.DiscountedPrice
                                                 ,orderItem.Discount
                                                 ,orderItem.DiscountAmount
                                                 ,orderItem.Price
                                                 ,orderItem.Cost);
            var result = GetValue(msg, "ID");

            return Int32.Parse(result);
        }

        private static string AddPurchaseOrderItemOptionsExt(this Client webOE, string TAT, int OrderItemId, List<KeyValuePair<string, string>> optionsData)
        {
            var strBuilder = new StringBuilder();
            var count = 0;

            foreach (var option in optionsData)
            {
                strBuilder.AppendFormat("{0,-10}", OrderItemId.ToString());
                strBuilder.AppendFormat("{0,-64}", option.Key);
                strBuilder.AppendFormat("{0,-100}", option.Value);
                strBuilder.AppendFormat("{0,-10}", count.ToString());
                strBuilder.AppendFormat("{0, 1}", "R");
                strBuilder.AppendFormat("{0,-5}|", "1");

                count++;
            }

            var optionInput = strBuilder.ToString();

            var msg = webOE.AddPurchaseOrderItemOptions(TAT
                                                        , OrderItemId
                                                        , optionInput);

            var result = GetValue(msg, "WereAdded");
            return result;
        }

        private static bool ProcessPurchaseOrderExt(this Client webOE, string TAT,string auid, int orderId)
        {
            var valid = false;
            var msg = webOE.ProcessPurchaseOrder(TAT
                 , auid
                 , orderId
                );

            if (GetValue(msg, "WasProcessed").Equals("True"))
                valid = true;

            return valid;
        }


        private static string GetValue(string xml, string nodeName)
        {
            var xmlDoc = XDocument.Parse(xml);
            if (xmlDoc.Element("Results") != null && xmlDoc.Element("Results").Element("Result") != null)
            {
                var node = xmlDoc.Element("Results").Element("Result").Element(nodeName);
                if (node != null)
                    return node.Value;
                else
                    return null;
            }
            else
                return string.Empty; 
        }
    }
}