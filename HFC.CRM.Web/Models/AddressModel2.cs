﻿using System;
using System.Collections.Generic;
using System.Linq;

using HFC.CRM.Data;

namespace HFC.CRM.Web.Models
{
    public class AddressModel
    {
        public int AddressId { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        private DateTime _CreatedOnUtc { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsResidential { get; set; }
        public string CrossStreet { get; set; }
        public string CountryCode2Digits { get; set; }
        public string AttentionText { get; set; }

        public static List<AddressModel> MapTo(ICollection<Address> model)
        {
            return model.Select(s => MapTo(s)).ToList();
        }
        public static AddressModel MapTo(Address model)
        {
            if (model == null)
                return null;
            return new AddressModel()
            {
                AddressId = model.AddressId,
                AttentionText = model.AttentionText,
                Address1 = model.Address1,
                Address2 = model.Address2,
                City = model.City,
                State = model.State,
                ZipCode = model.ZipCode,
                CountryCode2Digits = model.CountryCode2Digits,
                CreatedOnUtc = model.CreatedOnUtc,
                IsDeleted = model.IsDeleted,
                IsResidential = model.IsResidential,
                CrossStreet = model.CrossStreet
            };
        }
        public static Address MapFrom(AddressModel model)
        {
            return new Address()
            {
                AddressId = model.AddressId,
                AttentionText = model.AttentionText,
                Address1 = model.Address1,
                Address2 = model.Address2,
                City = model.City,
                State = model.State,
                ZipCode = model.ZipCode,
                CountryCode2Digits = model.CountryCode2Digits,
                CreatedOnUtc = model.CreatedOnUtc,
                IsDeleted = model.IsDeleted,
                IsResidential = model.IsResidential,
                CrossStreet = model.CrossStreet
            };
        }

    }
}
