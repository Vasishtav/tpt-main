﻿using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Web;

namespace HFC.CRM.Web.Models
{
    public class NoteAttachment
    {
        public int NoteId { get; set; }
        public Nullable<int> JobId { get; set; }
        public Nullable<int> AccountId { get; set; }
        public Nullable<int> LeadId { get; set; }
        public Nullable<int> QuoteId { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public Nullable<int> OpportunityId { get; set; }
        public string Title { get; set; }
        public NoteTypeEnum TypeEnum { get; set; }
        public string Notes { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public Nullable<int> UpdatedBy { get; set; }

        public string FileName { get; set; }

        public string AttachmentCategory { get; set; }

        public HttpPostedFileBase Attachment { get; set; }

        //Added model cration
        public string CreatorFullName { get; set; }

        public string AvatarSrc { get; set; }

        public ICollection<PhysicalFile> PhysicalFileList { get; set; }

    }
}