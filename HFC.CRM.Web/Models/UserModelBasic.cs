﻿using System;
using System.Collections.Generic;
using System.Linq;
using HFC.CRM.Data;

namespace HFC.CRM.Web.Models
{
    public class UserModelBasic
    {
        #region public
       
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public int PersonId { get; set; }
        private DateTime _CreationDateUtc = DateTime.Now;
        public DateTime CreationDateUtc
        {
            get { return _CreationDateUtc; }
            set { _CreationDateUtc = DateTime.SpecifyKind(value, DateTimeKind.Unspecified); }
        }
        public string Password { get; set; }
        public string Domain { get; set; }
        public string DomainPath { get; set; }
        private Nullable<DateTime> _LastActivityDateUtc;
        public Nullable<DateTime> LastActivityDateUtc
        {
            get { return _LastActivityDateUtc; }
            set
            {
                if (value == null)
                    _LastActivityDateUtc = null;
                else
                    _LastActivityDateUtc = DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified);
            }
        }
        public bool IsDeleted { get; set; }
        public bool IsApproved { get; set; }
        public bool IsLockedOut { get; set; }
        private Nullable<DateTime> _LastLoginDateUtc;
        public Nullable<DateTime> LastLoginDateUtc
        {
            get { return _LastLoginDateUtc; }
            set
            {
                if (value == null)
                    _LastLoginDateUtc = null;
                else
                    _LastLoginDateUtc = DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified);
            }
        }
        private Nullable<DateTime> _LastPasswordChangedDateUtc;
        public Nullable<DateTime> LastPasswordChangedDateUtc
        {
            get { return _LastPasswordChangedDateUtc; }
            set
            {
                if (value == null)
                    _LastPasswordChangedDateUtc = null;
                else
                    _LastPasswordChangedDateUtc = DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified);
            }
        }
        private Nullable<DateTime> _LastLockoutDateUtc;
        public Nullable<DateTime> LastLockoutDateUtc
        {
            get { return _LastLockoutDateUtc; }
            set
            {
                if (value == null)
                    _LastLockoutDateUtc = null;
                else
                    _LastLockoutDateUtc = DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified);
            }
        }
        public int FailedPasswordAttemptCount { get; set; }
        private Nullable<DateTime> _FailedPasswordAttemptStartDateUtc;
        public Nullable<DateTime> FailedPasswordAttemptStartDateUtc
        {
            get { return _FailedPasswordAttemptStartDateUtc; }
            set
            {
                if (value == null)
                    _FailedPasswordAttemptStartDateUtc = null;
                else
                    _FailedPasswordAttemptStartDateUtc = DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified);
            }
        }
        public string Comment { get; set; }
        public string LastKnownIPAddress { get; set; }
        public Nullable<int> FranchiseId { get; set; }
        public string Email { get; set; }
        public int ColorId { get; set; }
        private Nullable<DateTime> _SyncStartsOnUtc;
        public Nullable<DateTime> SyncStartsOnUtc
        {
            get { return _SyncStartsOnUtc; }
            set
            {
                if (value == null)
                    _SyncStartsOnUtc = null;
                else
                    _SyncStartsOnUtc = DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified);
            }
        }
        public int CalendarFirstHour { get; set; }
        public Nullable<int> AddressId { get; set; }
        public string AvatarSrc { get; set; }
        public string EmailSignature { get; set; }
        public Person Person { get; set; }

        #endregion
        public static User MapFrom(UserModelBasic model)
        {
            return new User()
            {
                AddressId = model.AddressId,
                AvatarSrc = model.AvatarSrc,
                CalendarFirstHour = model.CalendarFirstHour,
                ColorId = model.ColorId,
                Comment = model.Comment,
                CreationDateUtc = model.CreationDateUtc,
                Domain = model.Domain,
                DomainPath = model.DomainPath,
                Email = model.Email,
                EmailSignature = model.EmailSignature,
                FailedPasswordAttemptCount = model.FailedPasswordAttemptCount,
                FailedPasswordAttemptStartDateUtc = model.FailedPasswordAttemptStartDateUtc,
                FranchiseId = model.FranchiseId,
                IsApproved = model.IsApproved,
                IsDeleted = model.IsDeleted,
                IsLockedOut = model.IsLockedOut,
                LastActivityDateUtc = model.LastActivityDateUtc,
                LastKnownIPAddress = model.LastKnownIPAddress,
                LastLockoutDateUtc = model.LastLockoutDateUtc,
                LastLoginDateUtc = model.LastLoginDateUtc,
                LastPasswordChangedDateUtc = model.LastPasswordChangedDateUtc,
                Password = model.Password,
                PersonId = model.PersonId,
                SyncStartsOnUtc = model.SyncStartsOnUtc,
                UserId = model.UserId,
                UserName = model.UserName,
                Person=model.Person
            };
        }
        public static UserModelBasic MapTo(User model)
        {
            return new UserModelBasic()
            {
                AddressId = model.AddressId,
                AvatarSrc = model.AvatarSrc,
                CalendarFirstHour = model.CalendarFirstHour,
                ColorId = model.ColorId,
                Comment = model.Comment,
                CreationDateUtc = model.CreationDateUtc,
                Domain = model.Domain,
                DomainPath = model.DomainPath,
                Email = model.Email,
                EmailSignature = model.EmailSignature,
                FailedPasswordAttemptCount = model.FailedPasswordAttemptCount,
                FailedPasswordAttemptStartDateUtc = model.FailedPasswordAttemptStartDateUtc,
                FranchiseId = model.FranchiseId,
                IsApproved = model.IsApproved,
                IsDeleted = model.IsDeleted,
                IsLockedOut = model.IsLockedOut,
                LastActivityDateUtc = model.LastActivityDateUtc,
                LastKnownIPAddress = model.LastKnownIPAddress,
                LastLockoutDateUtc = model.LastLockoutDateUtc,
                LastLoginDateUtc = model.LastLoginDateUtc,
                LastPasswordChangedDateUtc = model.LastPasswordChangedDateUtc,
                Password = model.Password,
                PersonId = model.PersonId,
                Person=model.Person,
                SyncStartsOnUtc = model.SyncStartsOnUtc,
                UserId = model.UserId,
                UserName = model.UserName,
            };
        }
    }
}
