﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HFC.CRM.Web.Models
{
    public class Vendors
    {
        public int VendorId { get; set; }
        public string VendorName { get; set; }
        public string Location { get; set; }
        public string VendorType { get; set; }
        public string Account_Rep { get; set; }
        public long Rep_Phnone { get; set; }
        public string Comm_Method { get; set; }
    }
}