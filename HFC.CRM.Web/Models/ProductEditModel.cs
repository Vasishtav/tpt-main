﻿namespace HFC.CRM.Web.Models.Product
{
    public class ProductEditModel
    {
        public int? ProductId { get; set; }

        public int CategoryId { get; set; }

        public string ProductType { get; set; }

    }
}