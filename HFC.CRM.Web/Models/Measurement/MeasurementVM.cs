﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HFC.CRM.Web.Models.Measurement
{
    public class MeasurementVM
    {

    }

    public class RoomLocationVM
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public string AbbrValue { get; set; }
    }

    public class MountTypeVM
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
    public class FractionalInchVM
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
    public class StorageType
    {
        
        public string Id { get; set; }
        public string Name { get; set; }
    }
    
}