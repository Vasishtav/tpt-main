﻿namespace HFC.CRM.Web.Models.Email
{
    using System.Collections.Generic;

    using HFC.CRM.Data;
    using HFC.CRM.Managers.AdvancedEmailManager;

    public class EmailSendModel
    {
        public int EmailTemplateId { get; set; }
        //public EmailTemplateType EmailTemplateType { get; set; }

        public string[] ToAddresses { get; set; }

        public string[] BccAddresses { get; set; }

        public List<PhysicalFile> Attachments { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        public bool SaveBody { get; set; }
        public int PersonId { get; set; }
        public int EmailTemplateTypeId { get; set; }
        public int JobId { get; set; } 
        public int AppointmentId { get; set; }
    }
}