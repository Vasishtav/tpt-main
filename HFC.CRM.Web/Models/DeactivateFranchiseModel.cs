﻿using System;
using System.Collections.Generic;
using System.Linq;

using HFC.CRM.Data;

namespace HFC.CRM.Web.Models
{
    public class DeactivateFranchiseModel
    {
        public int DeactivationReasonId { get; set; }
        public int FranchiseId { get; set; }
        public string DeactivationAdditionalReason { get; set; }

    }
    
}
