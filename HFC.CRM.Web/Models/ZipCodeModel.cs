﻿using System;
using System.Collections.Generic;
using System.Linq;

using HFC.CRM.Data;

namespace HFC.CRM.Web.Models
{
    public class ZipCodeModel
    {
        #region publics
        public int Id { get; set; }
        public int TerritoryId { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ContractStatus { get; set; }
        private DateTime _CreatedOnUtc = DateTime.Now;
        public DateTime CreatedOnUtc
        {
            get { return _CreatedOnUtc; }
            set { _CreatedOnUtc = DateTime.SpecifyKind(value, DateTimeKind.Unspecified); }
        }
        private Nullable<DateTime> _AcquiredOnUtc;
        public Nullable<DateTime> AcquiredOnUtc
        {
            get { return _AcquiredOnUtc; }
            set
            {
                if (value == null)
                    _AcquiredOnUtc = null;
                else
                    _AcquiredOnUtc = DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified);
            }
        }
        private Nullable<DateTime> _EffectiveStartUtc;
        public Nullable<DateTime> EffectiveStartUtc
        {
            get { return _EffectiveStartUtc; }
            set
            {
                if (value == null)
                    _EffectiveStartUtc = null;
                else
                    _EffectiveStartUtc = DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified);
            }
        }
        private Nullable<DateTime> _EffectiveEndUtc;
        public Nullable<DateTime> EffectiveEndUtc
        {
            get { return _EffectiveEndUtc; }
            set
            {
                if (value == null)
                    _EffectiveEndUtc = null;
                else
                    _EffectiveEndUtc = DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified);
            }
        }
        #endregion
        #region mappers

        public static TerritoryZipCode MapFrom(ZipCodeModel model)
        {
            return new TerritoryZipCode()
            {
                Id = model.Id,
                TerritoryId = model.TerritoryId,
                ZipCode = model.ZipCode,
                City = model.City,
                State = model.State,
                ContractStatus = model.ContractStatus,
                CreatedOnUtc = model.CreatedOnUtc,
                AcquiredOnUtc = model.AcquiredOnUtc,
                EffectiveStartUtc = model.EffectiveStartUtc,
                EffectiveEndUtc = model.EffectiveEndUtc
            };
        }

        public static List<ZipCodeModel> MapTo(ICollection<TerritoryZipCode> model)
        {
            return model.Select(s => MapTo(s)).ToList();
        }
        public static ZipCodeModel MapTo(TerritoryZipCode model)
        {
            return new ZipCodeModel()
            {
                Id = model.Id,
                TerritoryId = model.TerritoryId,
                ZipCode = model.ZipCode,
                City = model.City,
                State = model.State,
                ContractStatus = model.ContractStatus,
                CreatedOnUtc = model.CreatedOnUtc,
                AcquiredOnUtc = model.AcquiredOnUtc,
                EffectiveStartUtc = model.EffectiveStartUtc,
                EffectiveEndUtc = model.EffectiveEndUtc
            };
        }
         #endregion
    }
}
