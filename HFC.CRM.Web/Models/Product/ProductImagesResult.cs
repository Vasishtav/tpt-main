﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace HFC.CRM.Web.Models.Product
{
    [XmlRoot(ElementName = "Result")]
    public class ProductImage
    {
        [XmlElement(ElementName = "ID")]
        public string ID { get; set; }

        [XmlElement(ElementName = "URL")]
        public string URL { get; set; }

        [XmlElement(ElementName = "URLThumbnail")]
        public string URLThumbnail { get; set; }
    }

    [XmlRoot(ElementName = "Results")]
    public class ProductImagesResult
    {
        [XmlElement(ElementName = "Result")]
        public List<ProductImage> Images { get; set; }
    }
}
