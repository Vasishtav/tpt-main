﻿using System.Collections.Generic;
using System.Xml.Serialization;
using DocumentFormat.OpenXml.Office2010.ExcelAc;

namespace HFC.CRM.Web.Models.Product
{

    [XmlRoot(ElementName = "Result")]
    public class ProductAttachment
    {
        [XmlElement(ElementName = "ID")]
        public string ID { get; set; }

        [XmlElement(ElementName = "Name")]
        public string Name { get; set; }

        [XmlElement(ElementName = "Description")]
        public string Description { get; set; }

        [XmlElement(ElementName = "FileName")]
        public string FileName { get; set; }

        [XmlElement(ElementName = "FileType")]
        public string FileType { get; set; }

        [XmlElement(ElementName = "FileSize")]
        public string FileSize { get; set; }

        [XmlElement(ElementName = "URL")]
        public string URL { get; set; }
    }

    [XmlRoot(ElementName = "Results")]
    public class ProductAttachmentResult
    {
        [XmlElement(ElementName = "Result")]
        public List<ProductAttachment> Attachments { get; set; }
    }

}