﻿namespace HFC.CRM.Web.Models.Settings
{
    public class RestrictionsSettingsModel
    {
        public bool RestrictInstallerRole { get; set; }

        public bool RestrictSalesRole { get; set; }
    }
}