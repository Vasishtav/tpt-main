﻿namespace HFC.CRM.Web.Models.Settings
{
    public class ReminderSettingsModel
    {
        public bool Enable { get; set; }

        public bool EnableCounters { get; set; }
    }
}