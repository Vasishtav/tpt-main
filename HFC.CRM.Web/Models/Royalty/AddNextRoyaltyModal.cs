﻿namespace HFC.CRM.Web.Models.Royalty
{
    public class AddNextRoyaltyModal
    {
        public int Monthes { get; set; }

        public decimal Amount { get; set; }
    }
}