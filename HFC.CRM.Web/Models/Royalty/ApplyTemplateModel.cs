﻿namespace HFC.CRM.Web.Models.Royalty
{
    using System;

    public class ApplyTemplateModel
    {
        public int TerritoryId { get; set; }

        public int RoyaltyProfileId { get; set; }

        public DateTime? StartDate { get; set; }
    }
}