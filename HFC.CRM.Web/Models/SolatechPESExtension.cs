﻿using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data;
using HFC.CRM.Web.com.solatech.svcpes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace HFC.CRM.Web
{
    public static class SolatechPESExtension
    {
        public static string API_AccountID { get { return /*AppConfigManager.GetConfig<string>("AccountId", "Solatech", "PES");*/ "8bb05851-74d4-4121-b50c-3c6b0efabf96"; } }
        public static bool LoggingEnabled { get { return AppConfigManager.GetConfig<bool>("LoggingEnabled", "Solatech", "PES"); } }

        public static List<string> GetMessagesExtended(this com.solatech.svcpes.Client pes, string sessionId)
        {
            List<string> result = new List<string>();            
            var messagesXml = pes.GetMessages(sessionId);

            if (!string.IsNullOrEmpty(messagesXml))
            {
                if (LoggingEnabled)
                    ApiEventLogger.LogEvent("PES", trace: "GetMessages(" + (sessionId ?? "") + ")\n\n" + messagesXml);

                XDocument doc = XDocument.Parse(messagesXml);
                foreach (var msg in doc.Element("Results").Elements("Result"))
                {
                    result.Add(msg.Element("Message").Value);
                }
            }
            
            return result;
        }

        public static bool SetProductExtended(this Client pes, string sessionId, Guid productGuid)
        {
            if (productGuid == Guid.Empty || string.IsNullOrEmpty(sessionId))
                return false;
                        
            var msg = pes.SetProduct(sessionId, productGuid.ToString().Replace("-", ""));
            if (LoggingEnabled)
                ApiEventLogger.LogEvent("PES", trace: "SetProduct(" + sessionId + ", " + productGuid.ToString() + ")\n\n" + msg);

            return Util.CastObject<bool>(GetValue(msg, "ProductSet"));                
        }

        public static bool SetOptionValuesExtended(this Client pes, string sessionId, ICollection<JobItemOption> options)
        {
            if (string.IsNullOrEmpty(sessionId))
                return false;
            if (options == null || options.Count == 0)
                return false;

            //set all the options so we can calculate the price and final errors if any
            System.Text.StringBuilder optionsToSet = new System.Text.StringBuilder();
            foreach (var opt in options)
            {
                optionsToSet.Append(opt.Name.PadRight(64));
                optionsToSet.Append((opt.Value ?? "").PadRight(100));
            }
            var xml = pes.SetOptionValues(sessionId, optionsToSet.ToString());
            if (LoggingEnabled)
                ApiEventLogger.LogEvent("PES", trace: "SetOptionValues(" + sessionId + ", " + optionsToSet.ToString() + ")\n\n" + xml);

            //if (Util.CastObject<bool>(GetValue(xml, "ValuesSet")))
            if (Util.CastObject<bool>(GetValue(xml, "ValueSet"))) // 1.7 
                return pes.ProcessOptionValuesExtended(sessionId);
            else
                return false;
        }

        public static decimal GetPriceBeforeDiscount(this Client pes, string sessionId)
        {
            if(string.IsNullOrEmpty(sessionId))
                return 0;

            var xml = pes.GetPrice(sessionId);
            if (LoggingEnabled)
                ApiEventLogger.LogEvent("PES", trace: "GetPrice(" + sessionId + ")\n\n" + xml);
            
            var result = Util.CastObject<decimal?>(GetValue(xml, "PriceBeforeDiscount"));
            if (result.HasValue)
                return result.Value;
            else
                return 0;
        }

        public static bool SetOptionValueExtended(this Client pes, string sessionId, string name, string value)
        {
            if(string.IsNullOrEmpty(name) || string.IsNullOrEmpty(sessionId))
                return false;

            var xml = pes.SetOptionValue(sessionId, name, value);
            if (LoggingEnabled)
                ApiEventLogger.LogEvent("PES", trace: "SetOptionValue(" + sessionId + ", " + name + ", " + (value ?? "") + ")\n\n" + xml);

            if (Util.CastObject<bool>(GetValue(xml, "ValueSet")))
                return pes.ProcessOptionValuesExtended(sessionId);
            else
                return false;
        }

        public static bool ProcessOptionValuesExtended(this Client pes, string sessionId)
        {
            if (string.IsNullOrEmpty(sessionId))
                return false;

            var xml = pes.ProcessOptionValues(sessionId);
            if (LoggingEnabled)
                ApiEventLogger.LogEvent("PES", trace: "ProcessOptionValues(" + sessionId + ")\n\n" + xml);

            return Util.CastObject<bool>(GetValue(xml, "Complete"));
        }

        public static bool SetStyleExtended(this Client pes, string sessionId, int styleId)
        {
            var style = CacheManager.ProductStyleCollection.FirstOrDefault(f => f.Id == styleId);
            if (style != null)
                return pes.SetStyleExtended(sessionId, style.Name);
            else
                return false;
        }

        public static List<JobItemOption> GetOptionValues2Extended(this Client pes, string sessionId, out List<string> Messages, out decimal priceBeforeDiscount, out decimal priceAfterDiscount, out decimal? vendorDiscount, out decimal vendorDiscountAlt, out decimal discountAlt)
        {
            Messages = new List<string>();
            priceBeforeDiscount = 0;
            discountAlt = 0;

            if (string.IsNullOrEmpty(sessionId))
                throw new ArgumentNullException(sessionId);

            var xml = pes.GetOptionValues2(sessionId);
            if (LoggingEnabled)
                ApiEventLogger.LogEvent("PES", trace: "GetOptionValues2(" + sessionId + ")\n\n"+xml);

            XDocument doc = XDocument.Parse(xml);

            List<JobItemOption> options = new List<JobItemOption>();

            foreach (var result in doc.Element("Results").Elements("Result"))
            {
                decimal? minValue = Util.CastObject<decimal>(result.Element("MinimumValue").Value),
                    maxValue = Util.CastObject<decimal>(result.Element("MaximumValue").Value);
                if (minValue.HasValue && minValue <= 0)
                    minValue = null;
                if (maxValue.HasValue && maxValue <= 0)
                    maxValue = null;

                var opt = new JobItemOption
                {
                    //OptionId = Util.CastObject<int>(result.Element("ID").Value),
                    DisplayOrder = Util.CastObject<int>(result.Element("Sequence")),
                    Name = result.Element("OptionName").Value,
                    Value = result.Element("OptionValue").Value,
                    IsList = result.Element("isList").Value != "0",
                    IsReadOnly = result.Element("ReadOnly").Value != "0",
                    AffectsPrice = result.Element("AffectsPrice").Value != "0",
                    IsVisible = result.Element("Visible").Value != "0",
                    ProcessIfHidden = (result.Element("ProcessIfHidden") != null && result.Element("ProcessIfHidden").Value != "0"),
                    MinValue = minValue,
                    MaxValue = maxValue,
                    AvailableOptions = new List<string>()
                };
                var lists = result.Element("ListValues");
                if (lists != null)
                {
                    foreach (var list in lists.Elements("ListValue"))
                    {
                        opt.AvailableOptions.Add(list.Element("Value").Value);
                    }
                }
                //this will ensure the available options list contains at least the default value
                if (opt.IsList && opt.AvailableOptions.All(a => a != opt.Value))
                {
                    opt.AvailableOptions.Add(opt.Value);
                }
                options.Add(opt);
            }

            // we will also re-order the display sequence of the options
            //int displayOrder = 3;
            //foreach (var opt in options.OrderByDescending(o => o.AffectsPrice).ThenBy(o => o.IsList).ThenByDescending(o => o.AvailableOptions.Count))
            //{
            //    if (opt.Name == "Room Location")
            //        opt.DisplayOrder = 0;
            //    else if (opt.Name == "Width")
            //        opt.DisplayOrder = 1;
            //    else if (opt.Name == "Height")
            //        opt.DisplayOrder = 2;
            //    else if (opt.Name == "Remarks")
            //        opt.DisplayOrder = options.Count + 1;
            //    else
            //    {
            //        opt.DisplayOrder = displayOrder;
            //        displayOrder++;
            //    }
            //}

            foreach (var msg in doc.Element("Results").Element("Messages").Elements("Message"))
            {
                Messages.Add(msg.Element("Message").Value);
            }

            priceBeforeDiscount = Util.CastObject<decimal>(doc.Element("Results").Element("PriceCost").Element("PriceBeforeDiscount").Value);
            priceAfterDiscount = Util.CastObject<decimal>(doc.Element("Results").Element("PriceCost").Element("PriceAfterDiscount").Value);
            
            if (doc.Element("Results").Element("PriceCost").Element("VendorDiscount") != null && !string.IsNullOrEmpty(doc.Element("Results").Element("PriceCost").Element("VendorDiscount").Value))
            {
                vendorDiscount = Util.CastObject<decimal>(doc.Element("Results").Element("PriceCost").Element("VendorDiscount").Value);
            }
            else
            {
                vendorDiscount = null;
            }
            
            vendorDiscountAlt = Util.CastObject<decimal>(doc.Element("Results").Element("PriceCost").Element("VendorDiscountAlt").Value);
            if (doc.Element("Results").Element("PriceCost").Element("DiscountAlt") != null)
            {
                discountAlt = Util.CastObject<decimal>(doc.Element("Results").Element("PriceCost").Element("DiscountAlt").Value);
            }
            
            return options.OrderBy(o=> o.DisplayOrder).ToList();
        }

        public static bool SetStyleExtended(this Client pes, string sessionId, string style)
        {
            if (string.IsNullOrEmpty(style) || string.IsNullOrEmpty(sessionId))
                return false;

            var styleResponse = pes.SetStyle(sessionId, style);
            if (LoggingEnabled)
                ApiEventLogger.LogEvent("PES", trace: "SetStyle(" + sessionId + ", " + style + ")\n\n" + styleResponse);

            return Util.CastObject<bool>(GetValue(styleResponse, "StyleSet"));
        }

        public static string StartSessionExtended(this Client pes)
        {
            var msg = pes.StartSession(API_AccountID);
            if (LoggingEnabled)
                ApiEventLogger.LogEvent("PES", trace: "StartSession()\n\n" + msg);
            return GetValue(msg, "SUID");
        }

        public static List<string> GetManufacturersExtended(this Client pes)
        {
            List<string> mfgs = new List<string>();

            var xmlStr = pes.GetManufacturers(API_AccountID);
            if (!string.IsNullOrEmpty(xmlStr))
            {
                if (LoggingEnabled)
                    ApiEventLogger.LogEvent("PES", trace: "GetManufacturers()\n\n" + xmlStr);

                XDocument doc = XDocument.Parse(xmlStr);
                foreach (var node in doc.Element("Results").Elements("Result"))
                {
                    var mfg = node.Element("Manufacturer").Value;
                    if(!string.IsNullOrEmpty(mfg) && !mfgs.Contains(mfg))
                        mfgs.Add(mfg);
                }
            }

            return mfgs;
        }

        public static List<Product> GetProductsExtended(this Client pes, DateTimeOffset? lastUpdated = null, int pageNum = 1, int pageSize = 200)
        {
            List<Product> products = new List<Product>();

            var xmlStr = pes.GetProductsByPage(API_AccountID, (short)pageNum, (short)pageSize);
            if (!string.IsNullOrEmpty(xmlStr))
            {
                if (LoggingEnabled)
                    ApiEventLogger.LogEvent("PES", trace: string.Format("GetProducts(pageNum: {0}, pageSize: {1}, lastUpdated: {2})\n\n{3}", pageNum, pageSize, lastUpdated, xmlStr));

                XDocument doc = XDocument.Parse(xmlStr);
                foreach (var node in doc.Element("Results").Elements("Result"))
                {
                    var puid = node.Element("PUID").Value;
                    if (!string.IsNullOrEmpty(puid)) 
                    {
                        try
                        {
                            Guid productGuid = Guid.Parse(puid);
                            if (!products.Any(a => a.ProductGuid == productGuid))
                            {
                                var type = new Type_Product { TypeName = HttpUtility.HtmlDecode(node.Element("ProductType").Value), isCustom = false };
                                var mfg = new Manufacturer { Name = HttpUtility.HtmlDecode(node.Element("Manufacturer").Value), ManufacturerGuid = Guid.Parse(HttpUtility.HtmlDecode(node.Element("VendorUniqueID").Value)), isCustom = false };

                                var prod = new Product
                                {
                                    ProductGuid = productGuid,
                                    Name = node.Element("Name").Value,
                                    Description = node.Element("Description").Value,
                                    Manufacturer = mfg,
                                    ProductType = type,
                                    Revision = Convert.ToInt32(node.Element("Revision").Value),
                                    CreatedOn = DateTimeOffset.Parse(node.Element("AddedOn").Value + "Z"),
                                    LastUpdated = DateTimeOffset.Parse(node.Element("UpdatedOn").Value + "Z"),
                                    ProductStyles = new List<ProductStyle>()
                                };

                                foreach (var styleNode in node.Element("Styles").Elements("Style"))
                                {
                                    var style = new Type_Style { Name = styleNode.Element("Name").Value };

                                    prod.ProductStyles.Add(new ProductStyle { Style = style });
                                }

                                products.Add(prod);
                            }
                        }
                        catch (Exception Ex)
                        {
                            ApiEventLogger.LogEvent("PES: " + Ex.Message, trace: string.Format("GetProducts() Loop, GUID: {0}\n\n{1}", puid, Ex.StackTrace));
                        }
                    }
                }
            }

            return products;
        }

        public static List<Vendor> GetVendorsExtended(this Client pes)
        {
            List<Vendor> result = new List<Vendor>();

            var xmlStr = pes.GetVendors(API_AccountID);
            if (!string.IsNullOrEmpty(xmlStr))
            {
                if (LoggingEnabled)
                    ApiEventLogger.LogEvent("PES", trace: string.Format("GetVendors()\n\n{0}", xmlStr));

                XDocument doc = XDocument.Parse(xmlStr);
                foreach (var node in doc.Element("Results").Elements("Result"))
                {
                    var uid = node.Element("UniqueID").Value;
                    if (!string.IsNullOrEmpty(uid))
                    {
                        try
                        {
                            Guid vendorGuid = Guid.Parse(uid);
                            if (!result.Any(a => a.VendorGuid == vendorGuid))
                            {
                                string billCountry = node.Element("BillToCountry").Value;                                                                
                                string shipCountry = node.Element("ShipToCountry").Value;
                                if (!string.IsNullOrEmpty(billCountry))
                                {
                                    var ccCode = CacheManager.StatesCollection.FirstOrDefault(f => string.Equals(f.Type_Country.Country, billCountry));
                                    if (ccCode != null)
                                        billCountry = ccCode.Type_Country.ISOCode2Digits;
                                    else
                                        billCountry = "US";
                                }
                                else
                                    billCountry = "US";
                                if (!string.IsNullOrEmpty(shipCountry))
                                {
                                    var ccCode = CacheManager.StatesCollection.FirstOrDefault(f => string.Equals(f.Type_Country.Country, shipCountry));
                                    if (ccCode != null)
                                        shipCountry = ccCode.Type_Country.ISOCode2Digits;
                                    else
                                        shipCountry = "US";
                                }
                                else
                                    shipCountry = "US";

                                var billAddress = new Address
                                {
                                    Address1 = node.Element("BillToAddress1").Value,
                                    Address2 = node.Element("BillToAddress2").Value,
                                    City = node.Element("BillToCity").Value,
                                    State = node.Element("BillToState").Value,
                                    ZipCode = node.Element("BillToZip").Value,
                                    CountryCode2Digits = billCountry
                                };
                                var shipAddress = new Address
                                {
                                    Address1 = node.Element("ShipToAddress1").Value,
                                    Address2 = node.Element("ShipToAddress2").Value,
                                    City = node.Element("ShipToCity").Value,
                                    State = node.Element("ShipToState").Value,
                                    ZipCode = node.Element("ShipToZip").Value,
                                    CountryCode2Digits = shipCountry
                                };
                                var vendor = new Vendor
                                {
                                    VendorGuid = vendorGuid,
                                    ReferenceId = Util.CastObject<int?>(node.Element("ID").Value),
                                    ReferenceNum = node.Element("OOPID").Value,
                                    Name = node.Element("Name").Value,
                                    Description = node.Element("Description").Value,
                                    Phone = node.Element("Phone").Value,
                                    AltPhone = node.Element("AltPhone").Value,
                                    Fax = node.Element("Fax").Value,
                                    Email = node.Element("Email").Value,
                                    Website = node.Element("WebSite").Value,
                                    SplitPOs = Util.CastObject<bool>(node.Element("SplitPOs").Value),
                                    CreatedOn = DateTimeOffset.Parse(node.Element("AddedOn").Value + "Z").UtcDateTime,
                                    LastUpdated = DateTimeOffset.Parse(node.Element("UpdatedOn").Value + "Z"),
                                    BillAddress = billAddress,
                                    ShipAddress = billAddress.Equals(shipAddress) ? billAddress : shipAddress,
                                    IsActive = true,
                                    Discounts = new List<Product>()
                                };

                                foreach (var discountNode in node.Element("Discounts").Elements("Discount"))
                                {
                                    var puid = discountNode.Element("PUID").Value;
                                    if (!string.IsNullOrEmpty(puid))
                                    {
                                        vendor.Discounts.Add(new Product
                                        {
                                            ProductGuid = Guid.Parse(puid),
                                            Name = discountNode.Element("ProductName").Value,
                                            JobberDiscountPercent = Util.CastObject<decimal>(discountNode.Element("Discount").Value) * 100M
                                        });
                                    }
                                }

                                result.Add(vendor);
                            }
                        }
                        catch (Exception Ex)
                        {
                            ApiEventLogger.LogEvent("PES: " + Ex.Message, trace: string.Format("GetVendors() Loop, GUID: {0}\n\n{1}", uid, Ex.StackTrace));
                        }
                    }
                }
            }

            return result;
        }

        private static string GetValue(string xml, string nodeName)
        {
            XDocument doc = System.Xml.Linq.XDocument.Parse(xml);
            if (doc.Element("Results") != null && doc.Element("Results").Element("Result") != null)
            {
                var node = doc.Element("Results").Element("Result").Element(nodeName);
                if (node != null)
                    return node.Value;
                else
                    return null;
            }
            else
                return null;
        }
    }
}