﻿namespace HFC.CRM.Web.Models.Color
{
    public class ColorEditModel
    {
        public string ColorName { get; set; }

        public int? CategoryId { get; set; }
    }
}