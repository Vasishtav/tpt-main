﻿namespace HFC.CRM.Web.Models.Color
{
    using System.Collections.Generic;

    public class SetColorsToCategoryModel
    {
        public List<int> Colors { get; set; }
    }
}