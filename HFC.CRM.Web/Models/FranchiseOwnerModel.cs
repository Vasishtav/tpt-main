﻿using System;
using System.Collections.Generic;
using System.Linq;

using HFC.CRM.Data;

namespace HFC.CRM.Web.Models
{
    public class FranchiseOwnerModel
    {
        public int OwnerId { get; set; }
        public int FranchiseId { get; set; }
        public decimal OwnershipPercent { get; set; }
        private DateTime _CreatedOnUtc = DateTime.Now;
        public DateTime CreatedOnUtc
        {
            get { return _CreatedOnUtc; }
            set { _CreatedOnUtc = DateTime.SpecifyKind(value, DateTimeKind.Unspecified); }
        }
        public Nullable<Guid> UserId { get; set; }
        public UserModelBasic User { get; set; }

        public static List<FranchiseOwnerModel> MapTo(ICollection<FranchiseOwner> model)
        {
            return model.Select(s => MapTo(s)).ToList();
        }
        public static FranchiseOwnerModel MapTo(FranchiseOwner model)
        {
            return new FranchiseOwnerModel()
            {
                OwnerId = model.OwnerId,
                UserId = model.UserId,
                FranchiseId = model.FranchiseId,
                OwnershipPercent = model.OwnershipPercent,
                CreatedOnUtc = model.CreatedOnUtc,
                User = UserModelBasic.MapTo(model.User)
            };
        }

        public static ICollection<FranchiseOwner> MapFrom(List<FranchiseOwnerModel> model)
        {
            return model.Select(s => MapFrom(s)).ToList();
        }

        public static FranchiseOwner MapFrom(FranchiseOwnerModel model)
        {
            return new FranchiseOwner()
            {
                OwnerId = model.OwnerId,
                UserId = model.UserId,
                FranchiseId = model.FranchiseId,
                OwnershipPercent = model.OwnershipPercent,
                CreatedOnUtc = model.CreatedOnUtc,
                User = UserModelBasic.MapFrom(model.User)
            };
        }
    }
}
