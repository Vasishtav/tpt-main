﻿using System;
using System.Collections.Generic;
using System.Linq;

using HFC.CRM.Data;
using System.ComponentModel.DataAnnotations;

namespace HFC.CRM.Web.Models
{
    public class FranchiseRoyaltiesModel
    {
        public int RoyaltyId { get; set; }
        public string Name { get; set; }
        public string EndDate { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<decimal> Percent { get; set; }
        private Nullable<DateTime> _StartOnUtc;
        [DataType(System.ComponentModel.DataAnnotations.DataType.Date)]
        public Nullable<DateTime> StartOnUtc
        {
            get { return _StartOnUtc; }
            set
            {
                if (value == null)
                    _StartOnUtc = null;
                else
                    _StartOnUtc = DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified);
            }
        }
        private Nullable<DateTime> _EndOnUtc;
        public Nullable<DateTime> EndOnUtc
        {
            get { return _EndOnUtc; }
            set
            {
                if (value == null)
                    _EndOnUtc = null;
                else
                    _EndOnUtc = DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified);
            }
        }
        public int TerritoryId { get; set; }

        public static FranchiseRoyalty  MapFrom(FranchiseRoyaltiesModel model)
        {
            return new FranchiseRoyalty()
            {
                RoyaltyId = model.RoyaltyId,
                Name = model.Name,
                Amount = model.Amount,
                Percent = model.Percent,
                StartOnUtc = model.StartOnUtc,
                EndOnUtc = model.EndOnUtc,
                TerritoryId = model.TerritoryId,
            };
        }

        public static List<FranchiseRoyaltiesModel> MapTo(ICollection<FranchiseRoyalty> model)
        {
            return model.Select(MapTo).ToList();
        }
        public static FranchiseRoyaltiesModel MapTo(FranchiseRoyalty model)
        {
            return new FranchiseRoyaltiesModel()
            {
                RoyaltyId = model.RoyaltyId,
                Name = model.Name,
                Amount = model.Amount,
                Percent = model.Percent,
                StartOnUtc = model.StartOnUtc,
                EndOnUtc = model.EndOnUtc,
                TerritoryId = model.TerritoryId,
            };
        }
    }
    
}
