﻿using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;

namespace HFC.CRM.Web
{
    public static class Extensions
    {
        public static string GetHeaderValue(this HttpRequestMessage request, string key)
        {
            IEnumerable<string> values;
            if (request.Headers.TryGetValues(key, out values))
            {
                if (values != null && values.ToList().Count > 0)
                    return values.FirstOrDefault();
                else
                    return null;
            }
            else return null;
        }        

        /// <summary>
        /// Sets the day for this date, if the day exceeds the number of days in the month, then it will be set to the last day of the month
        /// </summary>        
        /// <param name="dayOfMonth">Day to set</param>
        /*public static DateTime ToFranchiseLocal(this DateTime dateToConvert)
        {
            return Util.ConvertDate(dateToConvert, SessionManager.CurrentFranchise.TimezoneCode);
        }*/

        /*public static DateTime? ToFranchiseLocal(this DateTime? dateToConvert)
        {
            if (dateToConvert.HasValue)
                return Util.ConvertDate(dateToConvert.Value, SessionManager.CurrentFranchise.TimezoneCode);
            else
                return null;
        }*/

        /*public static DateTime ToUtcFromFranchiseLocal(this DateTime dateToConvert)
        {
            return Util.ConvertDate(dateToConvert, Data.TimeZoneEnum.UTC, SessionManager.CurrentFranchise.TimezoneCode);
        }*/

        /*public static DateTime? ToUtcFromFranchiseLocal(this DateTime? dateToConvert)
        {
            if (dateToConvert.HasValue)
                return Util.ConvertDate(dateToConvert.Value, Data.TimeZoneEnum.UTC, SessionManager.CurrentFranchise.TimezoneCode);
            else
                return null;
        }*/

        /*public static DateTimeOffset ToFranchiseLocal(this DateTimeOffset date)
        {
            return TimeZoneInfo.ConvertTime(date, TimeZoneInfo.FindSystemTimeZoneById(SessionManager.CurrentFranchise.TimezoneCode.Description()));     
        }*/

        /// <summary>
        /// Returns a friendly readable date string
        /// </summary>
        /// <param name="date"></param>
        /// <param name="includeTimeInfo">if true, include time information, otherwise string will only be date</param>
        /// <returns></returns>
        public static string ToString(this DateTimeOffset date, bool includeTimeInfo = false)
        {
            TimeSpan span = (DateTime.Now - date.UtcDateTime);

            // Normalize time span
            bool future = false;
            if (span.TotalSeconds < 0)
            {
                // In the future
                span = -span;
                future = true;
            }

            // Format both date and time
            string format = "";
            if (span.TotalSeconds < (24 * 60 * 60))
            {
                format = "Today";
            }
            if (span.TotalSeconds < (48 * 60 * 60))
            {
                // 1 Day
                format = (future) ? "Tomorrow" : "Yesterday";
            }
            else if (span.TotalSeconds < (3 * 24 * 60 * 60))
            {
                // 2 Days
                format = future ? "In 2 days" : "2 days ago";
            }
            else
            {
                // Absolute date
                if (date.Year == DateTime.Now.Year)
                    format = date.ToString(@"ddd, MMM d");
                else
                    format = date.ToString(@"ddd, MMM d yyyy");
            }

            // Add time
            if (includeTimeInfo)
                return string.Format("{0} {1:h:mm tt}", format, date);
            else
                return format;
        }
    }
}