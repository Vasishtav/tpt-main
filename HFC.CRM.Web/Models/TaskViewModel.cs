﻿namespace HFC.CRM.Web
{
    using Data;
    using Managers;
    using System;
    using System.Collections.Generic;

    public class TaskViewModel
    {
        public int TaskID { get; set; }
        public DateTime? CompletedDateUtc { get; set; }
        public int? OrganizerPersonId { get; set; }
        public int PersonId { get; set; }
        public int AccountId { get; set; }
        public int OpportunityId { get; set; }
        public int OrderId { get; set; }
        public int? LeadId { get; set; }

        public string Title { get; set; }
        public string Attendee { get; set; }
        public string Description { get; set; }
        public int eventType { get; set; }
        public EventTypeEnum EventType { get; set; }
        public string AppointmentTypeName { get; set; }
        public string UserRoleColor { get; set; }
        public string UserTextColor { get; set; }
        public int TimeSlot { get; set; }
        public string AppointmentTypeColor { get; set; }
        public Nullable<bool> IsPrivate { get; set; }
        public Nullable<bool> IsTask { get; set; }
        public string OwnerName { get; set; }
        public string AttendeeName { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime Start { get; set; }
        //private DateTime start { get; set; }
        //public DateTime Start
        //{
        //    get
        //    {
        //        return start;
        //    }
        //    set
        //    {
        //        start = DateTime.SpecifyKind(value, DateTimeKind.Utc);
        //    }
        //}

        public string StartTimezone { get; set; }
        public DateTime End { get; set; }
        public bool? IsPrivateAccess { get; set; }
        //private DateTime end { get; set; }
        //public DateTime End
        //{
        //    get
        //    {
        //        return end;
        //    }
        //    set
        //    {
        //        end = DateTime.SpecifyKind(value, DateTimeKind.Utc);
        //    }
        //}

        public string EndTimezone { get; set; }

        public string RecurrenceRule { get; set; }
        public int? RecurrenceID { get; set; }
        public string RecurrenceException { get; set; }
        public bool IsAllDay { get; set; }

        public virtual ICollection<EventToPerson> OwnerID1 { get; set; }
        public List<string> OwnerID2 { get; set; }
        public List<SalesAgents> OwnerID { get; set; }
        public int? ReminderId { get; set; }
        public string Location { get; set; }
        public Opportunity opportunity { get; set; }
        public string OpportunityName { get; set; }
        public string AccountName { get; set; }
        public string AccountPhone { get; set; }
        public string CalName { get; set; }
        //public TaskTP ToEntity()
        //{
        //    return new TaskTP
        //    {
        //        TaskID = TaskID,
        //        Title = Title,
        //        Start = Start,
        //        StartTimezone = StartTimezone,
        //        End = End,
        //        EndTimezone = EndTimezone,
        //        Description = Description,
        //        RecurrenceRule = RecurrenceRule,
        //        RecurrenceException = RecurrenceException,
        //        RecurrenceID = RecurrenceID,
        //        IsAllDay = IsAllDay,
        //        OwnerID = OwnerID
        //    };
        //}
    }
}