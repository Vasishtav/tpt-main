﻿using System;
using System.Collections.Generic;
using System.Linq;

using HFC.CRM.Data;

namespace HFC.CRM.Web.Models
{
    public class TerritoriesModel
    {
        public int TerritoryId { get; set; }
        public string Minion { get; set; }
        public int BrandId { get; set; }
        public int FranchiseId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string WebSiteURL { get; set; }
        public bool isArrears { get; set; }
        private DateTime _CreatedOnUtc = DateTime.Now;
        public DateTime CreatedOnUtc
        {
            get { return _CreatedOnUtc; }
            set { _CreatedOnUtc = DateTime.SpecifyKind(value, DateTimeKind.Unspecified); }
        }

        public IList<ZipCodeModel> ZipCodes { get; set; }
        public IList<FranchiseRoyaltiesModel> FranchiseRoyalties { get; set; }
        public bool IsInUsed { get; set; }

        public static Territory MapFrom(TerritoriesModel model)
        {
            return new Territory()
            {
                TerritoryId = model.TerritoryId,
                FranchiseId = model.FranchiseId,
                Name = model.Name,
                Code = model.Code,
                BrandId = model.BrandId,
                Minion = model.Minion,
                WebSiteURL= model.WebSiteURL,
                CreatedOnUtc = model.CreatedOnUtc,
                isArrears = model.isArrears
            };
        }
        public static ICollection<Territory> MapFrom(List<TerritoriesModel> model)
        {
            return model.Select(MapFrom).ToList();
        }
        public static List<TerritoriesModel> MapTo(ICollection<Territory> model)
        {
            return model.Select(MapToFunc).ToList();
        }
        private static TerritoriesModel MapToFunc(Territory model)
        {
            return new TerritoriesModel()
            {
                TerritoryId = model.TerritoryId,
                FranchiseId = (int)model.FranchiseId,
                Name = model.Name,
                Code = model.Code,
                Minion = model.Minion,
                BrandId = model.BrandId,
                WebSiteURL = model.WebSiteURL,
                CreatedOnUtc = model.CreatedOnUtc,
                ZipCodes = ZipCodeModel.MapTo(model.ZipCodes),
                isArrears = (bool)(model.isArrears==null)?false: (model.isArrears == true)?true:false,
                FranchiseRoyalties= FranchiseRoyaltiesModel.MapTo( model.FranchiseRoyalties )
            };
        }
    }
    
}
