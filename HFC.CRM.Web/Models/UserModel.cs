﻿using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HFC.CRM.Web.Models
{
    public class UserModel
    {
        public Guid UserId { get; set; }
        public int PersonId { get; set; }
        public int? AddressId { get; set; }

        [Display(Name = "Username")]
        public string UserName { get; set; }

        public DateTime CreationDateUtc { get; set; }
        public DateTime? LastLoginDateUtc { get; set; }

        //[Display(Name = "3rd Party Authentication Service")]
        //public List<string> AvailableOAuthList { get; set; }
        //public List<string> ActiveOAuthProviders { get; set; }
        //[Required]
        //[Display(Name = "Email")]
        //[RegularExpression(HFC.CRM.Data.Constants.RegexPatterns.Email, ErrorMessage = "Invalid email address pattern")]
        //public string Email { get; set; }
        //[Required]
        //[Display(Name = "First Name")]
        //public string FirstName { get; set; }
        //[Required]
        //[Display(Name = "Last Name")]
        //public string LastName { get; set; }
        //[Display(Name = "Title")]
        //public string Title { get; set; }

        [Display(Name = "Roles")]
        public List<Guid> RoleIds { get; set; }

        public List<string> PermissionSetIds { get; set; }

        [Display(Name = "Is Approved")]
        public bool IsApproved { get; set; }
        
        [Display(Name = "Is Locked")]
        public bool IsLockedOut { get; set; }

        [Display(Name = "Is Disabled")]
        public bool IsDisabled { get; set; }
        
        public bool IsADUser { get; set; }
        public bool? CalSync { get; set; }
        public bool IsUserProfile { get; set; }

        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public DateTime? SyncStartsOnUtc { get; set; }
        public int CalendarFirstHour { get; set; }

        public ICollection<OAuthUser> OAuthAccounts { get; set; }

        public string Domain { get; set; }
        public string DomainPath { get; set; }
        public string Comment { get; set; }
        /// <summary>
        /// Background Color
        /// </summary>
        [Display(Name = "Background Color")]
        public string BGColor { get; set; }
        /// <summary>
        /// Foreground Color
        /// </summary>
        [Display(Name = "Foreground Color")]
        public string FGColor { get; set; }

        public Person Person { get; set; }
        public Address Address { get; set; }
    }

    public static class UserExtension
    {
        public static UserModel ToUserModel(this User user, bool isUserProfile = false)
        {
            var userModel = new UserModel
            {
                UserId = user.UserId,
                UserName = user.UserName,
                Address = user.Address,
                AddressId = user.AddressId,
                CreationDateUtc = user.CreationDateUtc,
                LastLoginDateUtc = user.LastLoginDateUtc,
                BGColor = user.ColorType != null ? user.ColorType.BGColorToRGB() : "rgb(194,194,194)",
                CalendarFirstHour = user.CalendarFirstHour,
                FGColor = user.ColorType != null ? user.ColorType.FGColorToRGB() : "rgb(0,0,0)",
                OAuthAccounts = user.OAuthUsers,
                IsApproved = user.IsApproved,
                IsLockedOut = user.IsLockedOut,
                IsDisabled = user.IsDisabled ?? false,
                IsUserProfile = isUserProfile,
                PersonId = user.PersonId,
                Person = user.Person,
                Comment = user.Comment,
                SyncStartsOnUtc = user.SyncStartsOnUtc,
                RoleIds = user.Roles != null && user.Roles.Count > 0 ? user.Roles.Select(s => s.RoleId).ToList() : new List<Guid> { AppConfigManager.DefaultRole.RoleId },
                Domain = user.Domain,
                DomainPath=user.DomainPath,
                CalSync = user.CalSync
            };
            
            return userModel;            
        }
    }
}