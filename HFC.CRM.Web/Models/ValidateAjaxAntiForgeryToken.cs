﻿using System.Linq;
using System.Net.Http;
using System.Web.Helpers;
using System.Web.Http.Controllers;

namespace HFC.CRM.Web
{
    public class ValidateAjaxAntiForgeryToken : System.Web.Http.AuthorizeAttribute
    {
        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            var headerToken = actionContext.Request.GetHeaderValue("__RequestVerificationToken");
            var cookie = actionContext.Request.Headers.GetCookies().FirstOrDefault();
            if (cookie == null)
                return false;

            var cookieToken = cookie.Cookies.FirstOrDefault(a => a.Name.Equals(AntiForgeryConfig.CookieName, System.StringComparison.InvariantCultureIgnoreCase));
                //.Select(c => c.Cookies.FirstOrDefault)
                //.FirstOrDefault();

            // check for missing cookie or header
            if (cookieToken == null || string.IsNullOrEmpty(headerToken))
            {
                return false;
            }

            // ensure that the cookie matches the header
            try
            {
                AntiForgery.Validate(cookieToken.Value, headerToken);
            }
            catch
            {
                return false;
            }

            return base.IsAuthorized(actionContext);
        }

        /// <summary>
        /// Since this is an ajax CSRF check, we will return 403 status code instead of 401, which will trigger a redirect
        /// </summary>
        /// <param name="actionContext"></param>
        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden);
            return;
        }
         

    }


}