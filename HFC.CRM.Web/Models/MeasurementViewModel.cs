﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HFC.CRM.Web.Models
{
    public class MeasurementViewModel
    {
        public int RoomId { get; set; }
        public string RoomLocation { get; set; }
        public string RoomName { get; set; }
        public string WindowLocation { get; set; }
        public string MountTypeName { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }

        public string Comments { get; set; }
    }

    public class RoomLocationVal
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class MountTypeVal
    {
        public int MountTypeId { get; set; }
        public string MountTypeName { get; set; }
    }

    public class FranctionalInch
    {
        public int FractionId { get; set; }
        public double FractionValue { get; set; }
    }
}