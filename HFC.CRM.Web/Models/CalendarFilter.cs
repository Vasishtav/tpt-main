﻿using HFC.CRM.Core.Common;
using HFC.CRM.Data;
using HFC.CRM.Data.Constants;
using HFC.CRM.Managers;
using HFC.CRM.Managers.AdvancedEmailManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HFC.CRM.Web.Models
{
    public class CalendarFilter
    {
        public int LeadId { get; set; }
        public List<int> jobIds { get; set; }
        public List<int> personIds { get; set; }
        public DateTime? start { get; set; }
        public DateTime? end { get; set; }
        public bool includeLookup { get; set; }
        public bool CalendarEdit { get; set; }
        public bool IsMainCalendar { get; set; }
        public int? CalendarId { get; set; }

        public string GetCalendar(out List<CalendarModel> Events, out List<CalendarModel> RecurringMasters)
        {
            Events = null;
            RecurringMasters = null;
            var CalendarMgr = new CalendarManager(SessionManager.CurrentUser);
            var EmailManager = new EmailManager(SessionManager.CurrentUser, SessionManager.CurrentFranchise);
            //this.start = DateTime.Now;
            //this.end = DateTime.Now.AddDays(1);
            try
            {
                var merged = new List<CalendarModel>();
                var masterRecurring = new List<CalendarModel>();

                DateTime startDate, endDate;
                if (this.start != null)
                    startDate = TimeZoneManager.GetUTCstartdatetime((DateTime)this.start);
                else
                    startDate = TimeZoneManager.GetUTCstartdatetime(DateTime.Now.Date);

                if (this.end != null)
                    endDate = TimeZoneManager.GetUTCenddatetime((DateTime)this.end);
                else
                    endDate = TimeZoneManager.GetUTCenddatetime(DateTime.Now.Date);

                var calCollection = CalendarMgr.GetCalendar(this.personIds, startDate, endDate, this.CalendarId);

                if (calCollection != null && calCollection.Count > 0)
                {
                    merged = calCollection.Select(s => s.ToCalendarModel()).ToList();
                }

                Events = merged.OrderBy(o => o.start).ToList();
                if (Events.Count != 0)
                {
                    foreach (var item in Events)
                    {
                        if (item.AccountId != null)
                        {
                            int AccountId = 0;
                            AccountId = (int)item.AccountId;
                            int LeadId = 0;
                            var Accountdata = EmailManager.Getemailnotification(LeadId, AccountId);
                            if (Accountdata != null)
                            {
                                item.IsNotifyemails = Accountdata.IsNotifyemails;
                                item.IsNotifyText = Accountdata.IsNotifyText;
                            }
                        }
                        else if (item.LeadId != null)
                        {
                            int LeadId = (int)item.LeadId;
                            int AccountId = 0;
                            var Leaddate = EmailManager.Getemailnotification(LeadId, AccountId);
                            if (Leaddate != null)
                            {
                                item.IsNotifyemails = Leaddate.IsNotifyemails;
                                item.IsNotifyText = Leaddate.IsNotifyText;
                            }
                        }
                    }
                }
                RecurringMasters = masterRecurring;
                if (RecurringMasters.Count != 0)
                {
                    foreach (var item in RecurringMasters)
                    {
                        if (item.LeadId != null)
                        {
                            int LeadId = (int)item.LeadId;
                            int AccountId = 0;
                            var Accountdata = EmailManager.Getemailnotification(LeadId, AccountId);
                            if (Accountdata != null)
                            {
                                item.IsNotifyemails = Accountdata.IsNotifyemails;
                                item.IsNotifyText = Accountdata.IsNotifyText;
                            }                           
                        }
                        else if (item.AccountId != null)
                        {
                            int AccountId = 0;
                            AccountId = (int)item.AccountId;
                            int LeadId = 0;
                            var Leaddate = EmailManager.Getemailnotification(LeadId, AccountId);
                            if (Leaddate != null)
                            {
                                item.IsNotifyemails = Leaddate.IsNotifyemails;
                                item.IsNotifyText = Leaddate.IsNotifyText;
                            }                           
                        }
                    }
                }
                return IMessageConstants.Success;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public void GetTasks(out List<TaskModel> Tasks, string PersonIds = null)
        {
            Tasks = null;
            var TaskMgr = new TaskManager(SessionManager.CurrentUser);
            try
            {
                int totalRecords = 0;
                List<Task> rawTasks = null;
                rawTasks = TaskMgr.GetTasks(out totalRecords, personIds, dueDateStart: start, dueDateEnd: end, TaskId: CalendarId != null ? (int)CalendarId : 0, PersonIds: PersonIds);


                if (rawTasks != null)
                {
                    Tasks = rawTasks.Select(t => t.ToTaskModel()).ToList();
                }
            }
            catch (Exception ex)
            {

            }
        }


    }
}