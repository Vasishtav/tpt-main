﻿using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Membership;
using HFC.CRM.Data;
using HFC.CRM.Managers;
using System;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.SessionState;

namespace HFC.CRM.MVC
{
    /// <summary>
    /// Summary description for eventstream
    /// </summary>
    public class eventstream : IHttpHandler, IReadOnlySessionState
    {

        public void ProcessRequest(HttpContext context){ }//// COMMENTED OUT, stub kept just in case. 

        //public void ProcessRequest(HttpContext context)
        //{
        //    context.Response.Clear();

        //    if (!context.User.Identity.IsAuthenticated)
        //        return;

        //    context.Response.ContentType = "text/event-stream";
        //    context.Response.Expires = -1;
        //    context.Response.CacheControl = "no-cache";
        //    //IMPORTANT: Clear content-encoding for EventSource to stream, IIS will not flush the response since it will wait for response to finish
        //    //to compress then send the response.  Clearing this will let it flush as expected.
        //    context.Response.AppendHeader("Content-Encoding", "");

        //    var user = SessionManager.CurrentUser;//LocalMembership.GetUser(context.User.Identity.Name);            
        //    if (user == null)
        //        return;
        //    if (!user.FranchiseId.HasValue || user.FranchiseId.Value <= 0)
        //    {
        //        context.Response.StatusCode = 400;
        //        context.Response.StatusDescription = "User must belong to a franchise";
        //        context.Response.End();
        //    }

        //    //starting parameters in case session gets cleared during loop
        //    int personId = user.PersonId;
        //    string domain = user.Domain;
        //    string email = user.Email;
        //    int franchiseId = user.FranchiseId.Value;
        //    bool excludeCounter = Util.CastObject<bool>(context.Request.QueryString["excl_counter"]);
        //    bool isSingleConnect = Util.CastObject<bool>(context.Request.QueryString["isSingleConnect"]);
        //    //bool excludeReminders = Util.CastObject<bool>(context.Request.QueryString["excl_reminders"]);

        //    int interval = Util.CastObject<int>(context.Request.QueryString["interval"]);
        //    if (interval > 0)
        //        interval = interval / 1000;
        //    else
        //        interval = AppConfigManager.GetConfig<int>("CounterInterval", "Time Intervals");
        //    ExchangeManager mseMgr = new ExchangeManager();

        //    using (var db = new CRMContextEx())
        //    {
        //        while (context.Response.IsClientConnected)
        //        {
        //            try
        //            {
        //                context.Response.Flush();
        //                //task, email, and lead count interval settings are changable in app config, while alerts should be checked every minute.
        //                //so we check this only if the loop interval matches the settings interval

        //                SqlParameter taskCountParam = new SqlParameter("TaskCount", System.Data.SqlDbType.Int);
        //                SqlParameter leadCountParam = new SqlParameter("NewLeadCount", System.Data.SqlDbType.Int);
        //                taskCountParam.Direction = System.Data.ParameterDirection.Output;
        //                leadCountParam.Direction = System.Data.ParameterDirection.Output;
        //                var calRem = db.Database.SqlQuery<spReminders_EventStream>(
        //                    string.Format("exec [CRM].[spBatch_Counter_and_Reminder] @PersonId={0},@FranchiseId={1},@RemindMethodEnum={2},@TaskCount=@TaskCount output,@NewLeadCount=@newLeadCount output",
        //                    personId, franchiseId, (Byte)RemindMethodEnum.Popup),
        //                    taskCountParam, leadCountParam).ToList();
        //                //var calRem = db.spBatch_Counter_and_Reminder(personId, franchiseId, (Byte)RemindMethodEnum.Popup, taskCountParam, leadCountParam).ToList();

        //                int? emailCount = null, taskCount = (int?)taskCountParam.Value, newLeadCount = (int?)leadCountParam.Value;

        //                //has AD account so we can check email
        //                if (!string.IsNullOrEmpty(domain) && !excludeCounter)
        //                {
        //                    int totalCount = 0;
        //                    if (mseMgr.GetUnreadEmailCount(email, out totalCount))
        //                        emailCount = totalCount;
        //                }
        //                //exclude counter when we only want reminders and not the counters
        //                if (!excludeCounter)
        //                {
        //                    context.Response.Write(
        //                        string.Format("{1}data: {0}\n\n",
        //                            Util.JSONSerialize(new { EmailCount = emailCount, NewLeadCount = newLeadCount, TaskCount = taskCount }),
        //                            isSingleConnect ? "retry: 60000\n" : "")
        //                    );
        //                    context.Response.Flush();
        //                }
        //                if (calRem != null)
        //                {
        //                    string res = string.Format("{1}event: reminder\ndata: {0}\n\n",
        //                        Newtonsoft.Json.JsonConvert.SerializeObject(calRem, new Newtonsoft.Json.JsonSerializerSettings
        //                        {
        //                            DateTimeZoneHandling = Newtonsoft.Json.DateTimeZoneHandling.RoundtripKind
        //                        }),
        //                        isSingleConnect ? "retry: 60000\n" : "");
        //                    context.Response.Write(res);
        //                    context.Response.Flush();
        //                }
        //                if (!isSingleConnect)
        //                    Thread.Sleep(TimeSpan.FromSeconds(interval)); //sleep for X interval seconds then check everything again
        //                else
        //                    break;
        //                break;
        //            }
        //            catch //(Exception ex)                    
        //            {
        //                //HFC.CRM.Core.Logs.EventLogger.LogEvent(ex);
        //                if (context.Response.IsClientConnected)
        //                {
        //                    context.Response.Write(string.Format("retry: {0}\n\n", 60000));//retry again in 60 sec
        //                    context.Response.Flush();
        //                }
        //                //this is when user leaves the page and causes "The request has been aborted. " warning so we're ignoring it here so it doesnt get logged in system Application log
        //                break;
        //            }
        //        }
        //    }
        //}

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}