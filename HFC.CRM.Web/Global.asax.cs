﻿using System;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using HFC.CRM.Core;
using HFC.CRM.Core.Common;
using HFC.CRM.Core.Logs;
using HFC.CRM.Data.Context;
using HFC.CRM.Managers.AdvancedEmailManager;
using HFC.CRM.Managers.OAuth;
using StackExchange.Profiling;
using System.Net;
using Z.Dapper.Plus;
using System.Configuration;
using HFC.CRM.Managers;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace HFC.CRM.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode,
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : HttpApplication
    {
        public static OAuthTokenManager TokenManager { get; private set; }
        public static NonceStoreManager NonceStore { get; private set; }
        //public AppSettingsReader ConfReader = new AppSettingsReader();

        protected void Application_Start()
        {
            ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

            ContextFactory.Current.Init(new WebContextProvider());

            BundleTable.EnableOptimizations = false;  //// you might forget to turn it back on and that REALLY BAD....

            MvcHandler.DisableMvcResponseHeader = true;

            AreaRegistration.RegisterAllAreas();
            UnityConfig.RegisterComponents();
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();

            TokenManager = new OAuthTokenManager("/OAuth/AccessToken", "/OAuth/RequestToken", "/OAuth/Auth");
            NonceStore = new NonceStoreManager();

            (new EmailTemplateManager(null, null)).Seed();
            (new AppointmentTypeColorManager(null, null)).Seed();

            LoadDictionaries(); // cache warmpup

            //Murugan: https://stackoverflow.com/questions/19467673/entity-framework-self-referencing-loop-detected
            var config = GlobalConfiguration.Configuration;
            config.Formatters.JsonFormatter
                .SerializerSettings
                .ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;

            string licenseName = ConfigurationManager.AppSettings["Z_Dapper_Plus_LicenseName"];//... PRO license name
            string licenseKey = ConfigurationManager.AppSettings["Z_Dapper_Plus_LicenseKey"];//... PRO license key
            DapperPlusManager.AddLicense(licenseName, licenseKey);

            string licenseErrorMessage;
            if (!DapperPlusManager.ValidateLicense(out licenseErrorMessage))
            {
                EventLogger.LogEvent("licenseErrorMessage");
                //throw new Exception(licenseErrorMessage);
            }

            EventLogger.LogEvent("CRM Pool_Started " + SessionManager.CacheTime.ToString());
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            try
            {
                Exception exception = Server.GetLastError();
                EventLogger.LogEvent(exception, source: "Application_Error Global.asax");
            }
            catch (Exception ex)
            {
                EventLogger.WriteToSystemEventLog(ex, ex.Message, LogSeverity.Error, ex.Source, ex.StackTrace, ex.GetType().Name);
            }
        }
        
        protected void Application_BeginRequest()
        {
            var now = DateTime.Now;
            var cacheTime = SessionManager.CacheTime;
            if (now < cacheTime)
            {
                EventLogger.LogEvent("CRM Application_BeginRequest cache lost " + SessionManager.CacheTime.ToString());
            }
            //if (DateTime.Now - SessionManager.CacheLogTime > TimeSpan.FromMinutes(5))
            //{
            //    SessionManager.CacheLogTime = DateTime.Now;
            //    var data = SessionManager.GetCacheSize();

            //    EventLogger.LogEvent("CRM Application_BeginRequest log data: " + String.Join(", ", data.ToArray()));
            //}

            if (AppConfigManager.isMiniProfilerOn)
                MiniProfiler.Start();
            if (Request.Url.Scheme != Uri.UriSchemeHttps && AppConfigManager.IsHttpsEnable)
            {
                Response.Redirect(Request.Url.AbsoluteUri.Replace("http://", "https://"));
            }

            if (!HttpContext.Current.IsDebuggingEnabled && FormsAuthentication.RequireSSL && !Request.IsSecureConnection)
            {
                Response.Redirect(Request.Url.AbsoluteUri.Replace("http://", "https://"));
            }
        }

        protected void Application_EndRequest()
        {
            ContextFactory.Current.Destroy();

            MiniProfiler.Stop();
        }

        #region Required for retrieving Session for api requests

        //protected void Application_PostAuthorizeRequest()
        //{
        //    if (IsWebApiRequest())
        //    {
        //        HttpContext.Current.SetSessionStateBehavior(SessionStateBehavior.Required);
        //    }
        //}

        private bool IsWebApiRequest()
        {
            return HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath.StartsWith(WebApiConfig.UrlPrefixRelative);
        }

        #endregion Required for retrieving Session for api requests

        private static void LoadDictionaries()
        {
            var c1 = CacheManager.SourceCollection;
            var c2 = CacheManager.ManufacturerCollection;
            var c3 = CacheManager.ProductCollection;
            var c4 = CacheManager.LeadStatusTypeCollection;
            var c5 = CacheManager.JobStatusTypeCollection;
            var c6 = CacheManager.OrderStatusTypeCollection;
            var c7 = CacheManager.PermissionTypeCollection;
            var c8 = CacheManager.ProductCollection;
            var c9 = CacheManager.ProductStyleCollection;
            var c10 = CacheManager.ProductTypeCollection;
            var c11 = CacheManager.QuestionCategoryCollection;
            var c12 = CacheManager.RoleCollection;
            var c13 = CacheManager.RoyaltyProfilesCollection;
            var c14 = CacheManager.SourceCollection;
            var c15 = CacheManager.UserCollection;
            var c16 = CacheManager.VendorCollection;
            var c17 = CacheManager.AppointmentTypes;
            var c18 = CacheManager.ColorPalettes;
            var c19 = CacheManager.ConceptCollection;
            var c20 = CacheManager.TimeZoneCollection;
        }
    }
}