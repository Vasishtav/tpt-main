﻿{today}

{name}
{address}
{city}, {state} {zip}

Dear {firstname},

Thank you so much for selecting {franchise_name} as your window treatment specialist.  We sincerely value your business!

We’ve placed your order and are monitoring its progress to ensure it is being processed quickly and accurately.  The moment your custom window coverings arrive, we’ll contact you to set-up a time for the installation that is convenient for your schedule.

Prior to installation please review your part of the Customer Satisfaction Agreement and take some time to prepare your home for installation.

Thanks again,

Your friends at {franchise_name}