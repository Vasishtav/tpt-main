﻿namespace HFC.CRM.Web.Hubs
{
    public class Singleton<T> where T : class, new()
    {
        Singleton() { }

        class SingletonCreator
        {
            static SingletonCreator() { }

            internal static readonly T instance = new T();
        }

        public static T Instance
        {
            get { return SingletonCreator.instance; }
        }
    }
}