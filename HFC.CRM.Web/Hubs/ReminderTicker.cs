﻿using System;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Diagnostics;
using HFC.CRM.Core;
using HFC.CRM.Data;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;

namespace HFC.CRM.Web.Hubs
{

    /// <summary>
    /// This hub periodically sends a message to all connected clients that contains the new lead count, task count and email count.
    /// </summary>
    public class ReminderTicker 
    {
        #region Private Members
        // ReSharper disable once InconsistentNaming reason: this is a private lazy-loaded backing.
        private static readonly Lazy<ReminderTicker> _instance = new Lazy<ReminderTicker>(() => new ReminderTicker(GlobalHost.ConnectionManager.GetHubContext<ReminderHub>(), ReminderHub.Connections));

        // 0 -stopped
        // 1 -active
        private long _state;

        private ReminderTicker(IHubContext connectionContext, ConnectionMapping<string> connectionMapping)
        {
            _hubContext = connectionContext;
            _connectionMapping = connectionMapping;
        }

        private readonly TimeSpan _updateInterval = TimeSpan.FromSeconds(AppConfigManager.GetConfig<int>("CounterInterval", "Time Intervals"));

        private readonly IHubContext _hubContext;

        private readonly ConnectionMapping<string> _connectionMapping;

        private Timer _timer;

        #endregion

        #region Public Properties

        /// <summary>
        /// A static instance of this class.
        /// </summary>
        public static ReminderTicker Instance
        {
            get
            {
                return _instance.Value;
            }
        }

        /// <summary>
        /// A flag indicating if the hub is active. The state of this flag is managed by the Start() and Stop() methods.
        /// </summary>
        public bool IsActive
        {
            get
            {
                return Interlocked.Read(ref _state) == 1;
            }
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Update timer. When this timer elapses, a message is sent to all connected clients.
        /// </summary>
        /// <param name="timerState"></param>
        private void TimerElapsed(object timerState)
        {
            foreach (string userId in _connectionMapping.GetKeys())
                NotifyUser(userId);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Starts the hub. This will reset the notification timer.
        /// </summary>
        public void Start()
        {
            if (IsActive)
                return;

            Interlocked.Exchange(ref _state, 1);
            _timer = new Timer(TimerElapsed, null, _updateInterval, _updateInterval);
        }

        /// <summary>
        /// Stops the hub. No notification events will be sent until the hub is started.
        /// </summary>
        public void Stop()
        {
            if (!IsActive)
                return;

           // _timer?.Dispose();
            Interlocked.Exchange(ref _state, 0);
        }

        /// <summary>
        /// Sends an update message to a single user that contains the new lead count, task count and email count.
        /// </summary>
        /// <param name="userId">A string containined a user ID in the form of a <see cref="System.Guid"/>Guid.</param>
        public void NotifyUser(string userId)
        {
            if (string.IsNullOrEmpty(userId))
                return;

            try
            {
                using (CRMContext db = new CRMContext())
                {
                    Guid guid;

                    // Make sure the userid we received is a valid guid; exit if it is not.
                    if (!Guid.TryParse(userId, out guid))
                        return;

                    // Load the user corresponding to the guid; exit if not found.
                    User user = db.Users.FirstOrDefault(u => u.UserId == guid);

                    if (user == null)
                        return;

                    int personId = user.PersonId;

                    // Make sure the user has a valid franchise id; exit if not.
                    if (user.FranchiseId == null)
                        return;

                    int franchiseId = user.FranchiseId.Value;

                    const string reminderQueryText = "exec [CRM].[spBatch_Counter_and_Reminder] @PersonId={0},@FranchiseId={1},@RemindMethodEnum={2},@TaskCount=@TaskCount output,@NewLeadCount=@newLeadCount output";
                    SqlParameter taskCountParam = new SqlParameter("TaskCount", System.Data.SqlDbType.Int);
                    SqlParameter leadCountParam = new SqlParameter("NewLeadCount", System.Data.SqlDbType.Int);
                    taskCountParam.Direction = System.Data.ParameterDirection.Output;
                    leadCountParam.Direction = System.Data.ParameterDirection.Output;

                    // Run the query to retreive the counts.
                    DbRawSqlQuery<spReminders_EventStream> dbRawSqlQuery = db.Database.SqlQuery<spReminders_EventStream>(string.Format(reminderQueryText, personId, franchiseId, (byte) RemindMethodEnum.Popup), taskCountParam, leadCountParam);

                    if (dbRawSqlQuery == null)
                        return;

                    int? emailCount = null;
                    int? taskCount = (int?) taskCountParam.Value;
                    int? newLeadCount = (int?) leadCountParam.Value;
                    string groupName = ReminderHub.UsernameToGroupName(user.UserId.ToString());

                    // TODO: What is supposed to be placed into Email count? Why is it always null?
                    // ReSharper disable once ExpressionIsAlwaysNull
                    object notificationMessage = new
                    {
                        EmailCount = emailCount,
                        NewLeadCount = newLeadCount,
                        TaskCount = taskCount
                    };

                    _hubContext.Clients.Group(groupName).updateCounters(notificationMessage);

                    JsonSerializerSettings serializerSettings = new JsonSerializerSettings {DateTimeZoneHandling = DateTimeZoneHandling.RoundtripKind};
                    _hubContext.Clients.Group(groupName).updateReminders(JsonConvert.SerializeObject(dbRawSqlQuery, serializerSettings));
                }
            }
            catch (Exception e)
            {
                StackTrace stackTrace = new StackTrace();
                var log = new {Exception = e, Stack = stackTrace.ToString()};
                string logStr = JsonConvert.SerializeObject(log);
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(logStr));
            }
            finally
            {
                GC.Collect();
            }
        }
        #endregion
    }
}