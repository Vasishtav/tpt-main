﻿namespace HFC.CRM.Web.Hubs
{
    using System;
    using System.Linq;

    using Data;

    using Microsoft.AspNet.SignalR;

    using Task = System.Threading.Tasks.Task;

    [Authorize]
    public class PushHub : Hub
    {
        public readonly static ConnectionMapping<string> Connections = Singleton<ConnectionMapping<string>>.Instance;

        public override Task OnConnected()
        {
            string currentUserId = this.GetCurrentUserId();
            int? franchiseId = this.GetCurrenUserFranchiseId(currentUserId);

            if (!string.IsNullOrEmpty(currentUserId))
            {
                Connections.Add(currentUserId, Context.ConnectionId);
                this.Groups.Add(this.Context.ConnectionId, UsernameToGroupName(currentUserId));
                this.Groups.Add(this.Context.ConnectionId, FranchiseToGroupName(franchiseId));
            }

            ReminderTicker.Instance.NotifyUser(currentUserId);

            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            string currentUserId = this.GetCurrentUserId();
            int? franchiseId = this.GetCurrenUserFranchiseId(currentUserId);

            if (!string.IsNullOrEmpty(currentUserId))
            {
                Connections.Remove(currentUserId, Context.ConnectionId);
                this.Groups.Remove(this.Context.ConnectionId, UsernameToGroupName(currentUserId));
                this.Groups.Remove(this.Context.ConnectionId, FranchiseToGroupName(franchiseId));
            }

            return base.OnDisconnected(stopCalled);
        }

        public override Task OnReconnected()
        {
            string currentUserId = this.GetCurrentUserId();
            
            if (!string.IsNullOrEmpty(currentUserId) && !Connections.GetConnections(currentUserId).Contains(Context.ConnectionId))
            {
                Connections.Add(currentUserId, Context.ConnectionId);
            }

            return base.OnReconnected();
        }

        private string GetCurrentUserId()
        {
            var impersonateUserId = this.GetCookie<Guid?>("ImpersonateUserId");
            return impersonateUserId != null ? impersonateUserId.ToString() : null;
        }

        private int? GetCurrenUserFranchiseId(string userId)
        {
            using (var context = new CRMContextEx())
            {
                if (userId == null)
                {
                    return null;
                }

                Guid userGuid = Guid.Parse(userId);
                var user = context.Users.FirstOrDefault(u => u.UserId == userGuid);

                if (user != null)
                {
                    return user.FranchiseId;
                }

                return null;
            }
        }

        private T GetCookie<T>(string name)
        {
            return this.Context.Request.Cookies.Keys.Contains(name) ? Util.CastObject<T>(this.Context.Request.Cookies[name].Value) : default(T);
        }

        public static string UsernameToGroupName(string username)
        {
            return string.Format("group_of_{0}_connections", username);
        }

        public static string FranchiseToGroupName(int? franchiseId)
        {
            return string.Format("group_of_{0}_connections", franchiseId);
        }
    }
}