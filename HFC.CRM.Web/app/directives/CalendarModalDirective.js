﻿app.directive('calendarModalDirective', ['$modal', '$http', 'calendarModalService', 'HFCService', '$window', '$location', 'alertModalService', '$location',
    function ($modal, $http, calendarModalService, HFCService, $window, $location, alertModalService, $location) {
            return {
                restrict: 'E',
                scope: {
                    modalId: '@',
                    title: '@',
                    taskId: '@'
                },
                templateUrl: '/templates/NG/CalendarModalTemplate.html',
                replace: true,
                link: function (scope) {
                    //flag for date validation
                    scope.ValidDate = true;
                    scope.ValidDatelist = [];
                    scope.Invaliddate = false;
                    scope.HFCService = HFCService;
                    // model variables

                    // other variables used in template
                    scope.editFlag = false;
                    scope.dayList = [
                                { checked: false, dayValue: 1, dayString: "Sunday" },
                                { checked: false, dayValue: 2, dayString: "Monday" },
                                { checked: false, dayValue: 4, dayString: "Tuesday" },
                                { checked: false, dayValue: 8, dayString: "Wednesday" },
                                { checked: false, dayValue: 16, dayString: "Thursday" },
                                { checked: false, dayValue: 32, dayString: "Friday" },
                                { checked: false, dayValue: 64, dayString: "Saturday" }
                    ];
                    scope.weekIndexArray = {
                            1: "first",
                            2: "second",
                            3: "third",
                            4: "fourth",
                            5: "fifth"
                    };
                    scope.weekDaysArray = {
                        1: "sunday",
                        2: "monday",
                        4: "tuesday",
                        8: "wednesday",
                        16: "thursday",
                        32: "friday",
                        64: "saturday"
                    }
                    scope.yearArray = {
                        1: "january",
                        2: "february",
                        3: "march",
                        4: "april",
                        5: "may",
                        6: "june",
                        7: "july",
                        8: "august",
                        9: "september",
                        10: "october",
                        11: "november",
                        12: "december"
                    };
                    scope.selectedDayList = [];
                    scope.singleFlag = false;
                    scope.subjectLocationOptionalFlag = false;
                    scope.disableRecurrenceFlag = false;
                    var dropdownSelectDetails = {};
                    var invalidAcceptedFlag = false;
                    

                    var initializeDropdownFields = function () {
                        // Kendo Compose objects
                        scope.leadOptions = {
                            placeholder: "Select",
                            dataTextField: "FullName",
                            dataValueField: "LeadId",
                            valuePrimitive: true,
                            height: 300,
                            autoBind: true,
                            autoBind: true,
                            filter: "contains",
                            filtering: function (e) {
                                var filter = e.filter;
                            },
                            dataSource: {
                                transport: {
                                    read: {
                                        url: '/api/TaskEvents/0/GetLeadsList',
                                        dataType: "json"
                                    }
                                }
                            }
                        };

                        scope.accountOptions = {
                            placeholder: "Select",
                            dataTextField: "FullName",
                            dataValueField: "AccountId",
                            valuePrimitive: true,
                            autoBind: true,
                            filter: "contains",
                            filtering: function (e) {
                                var filter = e.filter;
                            },
                            dataSource: {
                                transport: {
                                    read: {
                                        url: '/api/TaskEvents/0/GetAccountList',
                                        dataType: "json"
                                    }
                                }
                            }
                        };

                        scope.opportunityOptions = {
                            placeholder: "Select",
                            dataTextField: "FullName",
                            dataValueField: "OpportunityId",
                            valuePrimitive: true,
                            autoBind: true,
                            filter: "contains",
                            filtering: function (e) {
                                var filter = e.filter;
                            },
                            dataSource: {
                                transport: {
                                    read: {
                                        url: function (params) {
                                            if (!scope.calendarModelObject.AccountId) { //All opportunities for the FE
                                                var subUrl = '/api/TaskEvents/0/GetOpportunityList';
                                                dataType: "json";
                                                return subUrl;
                                            } else { // All opportunities for the Account
                                                var subUrl = '/api/TaskEvents/' + scope.calendarModelObject.AccountId + '/GetOpportunityListByAccount';
                                                dataType: "json";
                                                return subUrl;
                                            }
                                        }
                                    }
                                }
                            }
                        };

                        scope.orderOptions = {
                            placeholder: "Select",
                            dataTextField: "FullName",
                            dataValueField: "OrderId",
                            valuePrimitive: true,
                            autoBind: true,
                            filter: "contains",
                            filtering: function (e) {
                                var filter = e.filter;
                            },
                            dataSource: {
                                transport: {
                                    read: {
                                        url: function (params) {
                                            
                                            if (scope.calendarModelObject.OpportunityId) { // Order for the Opportunity
                                                var subUrl = '/api/TaskEvents/' + scope.calendarModelObject.OpportunityId + '/GetOrderListByOpportunity';
                                                dataType: "json";
                                                return subUrl;
                                            } else if (scope.calendarModelObject.AccountId) {
                                                var subUrl = '/api/TaskEvents/' + scope.calendarModelObject.AccountId + '/GetOrderListByAccount';
                                                dataType: "json";
                                                return subUrl;
                                            } else { // All Orders for the FE
                                                var subUrl = '/api/TaskEvents/0/GetOrderList';
                                                dataType: "json";
                                                return subUrl;
                                            }
                                        }
                                    }
                                }
                            }
                        };

                        scope.caseOptions = {
                            placeholder: "Select",
                            dataTextField: "CaseNumber",
                            dataValueField: "CaseId",
                            valuePrimitive: true,
                            autoBind: true,
                            filter: "contains",
                            filtering: function (e) {
                                var filter = e.filter;
                            },
                            dataSource: {
                                transport: {
                                    read: {
                                        url: function (params) {
                                            if (scope.calendarModelObject.OrderId) {
                                                var subUrl = '/api/TaskEvents/' + scope.calendarModelObject.OrderId + '/GetCaseList?type=order';
                                                dataType: "json";
                                                return subUrl;
                                            } else if (scope.calendarModelObject.OpportunityId) {
                                                var subUrl = '/api/TaskEvents/' + scope.calendarModelObject.OpportunityId + '/GetCaseList?type=opportunity';
                                                dataType: "json";
                                                return subUrl;
                                            } else if (scope.calendarModelObject.AccountId) {
                                                var subUrl = '/api/TaskEvents/' + scope.calendarModelObject.AccountId + '/GetCaseList?type=account';
                                                dataType: "json";
                                                return subUrl;
                                            } else {
                                                var subUrl = '/api/TaskEvents/0/GetCaseList?type=case';
                                                dataType: "json";
                                                return subUrl;
                                            }
                                        }
                                    }
                                }
                            }
                        };

                        scope.vendorCaseOptions = {
                            placeholder: "Select",
                            dataTextField: "CaseNumber",
                            dataValueField: "CaseId",
                            valuePrimitive: true,
                            autoBind: true,
                            filter: "contains",
                            filtering: function (e) {
                                var filter = e.filter;
                            },
                            dataSource: {
                                transport: {
                                    read: {
                                        url: function (params) {
                                            if (scope.calendarModelObject.OrderId) {
                                                var subUrl = '/api/TaskEvents/' + scope.calendarModelObject.OrderId + '/GetVendorCaseList?type=order';
                                                dataType: "json";
                                                return subUrl;
                                            } else if (scope.calendarModelObject.OpportunityId) {
                                                var subUrl = '/api/TaskEvents/' + scope.calendarModelObject.OpportunityId + '/GetVendorCaseList?type=opportunity';
                                                dataType: "json";
                                                return subUrl;
                                            } else if (scope.calendarModelObject.AccountId) {
                                                var subUrl = '/api/TaskEvents/' + scope.calendarModelObject.AccountId + '/GetVendorCaseList?type=account';
                                                dataType: "json";
                                                return subUrl;
                                            } else if (scope.calendarModelObject.CaseId) {
                                                var subUrl = '/api/TaskEvents/' + scope.calendarModelObject.CaseId + '/GetVendorCaseList?type=case';
                                                dataType: "json";
                                                return subUrl;
                                            } else {
                                                var subUrl = '/api/TaskEvents/0/GetVendorCaseList?type=VendorCase';
                                                dataType: "json";
                                                return subUrl;
                                            }
                                        }
                                    }
                                }
                            }
                        };

                        scope.attendeesOptions = {
                            placeholder: "Select",
                            dataTextField: "FullName",
                            dataValueField: "Email",
                            valuePrimitive: true,
                            height: 300,
                            autoBind: true,
                            filter: "contains",
                            filtering: function (e) {
                                var filter = e.filter;
                            },
                            dataSource: {
                                transport: {
                                    read: {
                                        url: '/api/lookup/1/GetAttendees',
                                        dataType: "json"
                                    }
                                }
                            }
                        };
                    }


                    // save method
                    scope.saveCalendarData = function (returnFlag, convertFlag) {
                        if (scope.frmAppt.$valid) {
                            //restrict saving for invalid date
                            for (i = 0; i < scope.ValidDatelist.length; i++) {
                                if (!scope.ValidDatelist[i].valid && scope.calendarModelObject.IsTask) {
                                    scope.Invaliddate = true;
                                    break;
                                }
                                else if (!scope.ValidDatelist[i].valid && scope.calendarModelObject.allDay && scope.ValidDatelist[i].id != "calendarStartDate" && scope.ValidDatelist[i].id != "calendarEndDate") {
                                    scope.Invaliddate = true; 
                                    break;
                                }
                                else if (!scope.ValidDatelist[i].valid && !scope.calendarModelObject.allDay && scope.ValidDatelist[i].id != "calendarStartDatePicker" && scope.ValidDatelist[i].id != "calendarEndDatePicker") {
                                    scope.Invaliddate = true;
                                    break;
                                }
                            }

                            var currentDateTime = new Date();
                            var currentDate = new Date();
                            currentDate.setHours(0, 0, 0, 0);
                            var dateFormat = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
                            var dateTimeFormat = /^\d{1,2}\/\d{1,2}\/\d{4}\s\d{1,2}:\d{1,2}\s\w{2}$/;
                            var taskDueDate = new Date(scope.calendarModelObject.start);
                            var endsOnDateTime = new Date(scope.calendarModelObject.EventRecurring.EndsOn);
                            endsOnDateTime.setHours(23);
                            endsOnDateTime.setMinutes(59);
                            taskDueDate.setHours(0, 0, 0, 0);
                            var startDateTime = new Date(scope.calendarModelObject.start);
                            var originalStartDateTime = new Date(scope.calendarModelObject.originalStartDate);
                            var endDateTime = new Date(scope.calendarModelObject.end);
                            var originalEndDateTime = new Date(scope.originalEndDate);
                            var startTime;
                            var originalStartTime;
                            var endTime;
                            var originalEndTime;
                            if (startDateTime) {
                                startTime = startDateTime.getTime();
                            }
                            if (originalStartDateTime) {
                                originalStartTime = originalStartDateTime.getTime();
                            }
                            if (endDateTime) {
                                endTime = endDateTime.getTime();
                            }
                            if (originalEndDateTime) {
                                originalEndTime = originalEndDateTime.getTime();
                            }
                            var attendeeChangeFlag = validateAttendeeChange();
                            switch (true) {
                                case ($.inArray(scope.calendarModelObject.AptTypeEnum, [8, 9, 10, 11, 12]) == -1 && (!scope.calendarModelObject.LeadId && !scope.calendarModelObject.AccountId) && !scope.calendarModelObject.IsTask): {
                                    HFC.DisplayAlert("Lead / Account is mandatory");
                                    break;
                                }
                                case (!scope.calendarModelObject.AdditionalPeople || scope.calendarModelObject.AdditionalPeople && scope.calendarModelObject.AdditionalPeople.length == 0): {
                                    HFC.DisplayAlert("Attendee is mandatory");
                                    break;
                                }
                                case (!scope.calendarModelObject.IsTask && !scope.calendarModelObject.AptTypeEnum): {
                                    HFC.DisplayAlert("Appointment Type is mandatory");
                                    break;
                                }
                                case (!(dateTimeFormat.test(scope.calendarModelObject.start) || dateFormat.test(scope.calendarModelObject.start)) &&  !scope.calendarModelObject.IsTask): {
                                    HFC.DisplayAlert("Invalid Start Date");
                                    break;
                                }
                                case (!(dateFormat.test(scope.calendarModelObject.start) || dateTimeFormat.test(scope.calendarModelObject.start)) && scope.calendarModelObject.IsTask): {
                                    HFC.DisplayAlert("Invalid Due Date");
                                    break;
                                }
                                case (!(dateTimeFormat.test(scope.calendarModelObject.end) || dateFormat.test(scope.calendarModelObject.end)) && !scope.calendarModelObject.IsTask): {
                                    HFC.DisplayAlert("Invalid End Date");
                                    break;
                                }
                                case (scope.Invaliddate): {
                                    HFC.DisplayAlert("Invalid Date!");
                                    break;
                                }
                                case (taskDueDate < currentDate && scope.calendarModelObject.IsTask): {
                                    HFC.DisplayAlert("Due Date cannot be in the past.");
                                    break;
                                }
                                case (new Date(scope.calendarModelObject.end) < new Date(scope.calendarModelObject.start) && !scope.calendarModelObject.IsTask): {
                                    HFC.DisplayAlert("To Date must be later than From Date.");
                                    break;
                                }
                                case (scope.calendarModelObject.RecurrenceIsEnabled && !scope.calendarModelObject.EventRecurring.RecursEvery): {
                                    HFC.DisplayAlert("Recurrence Every Day(s) or Week(s) or Month(s) or Year(s) is required");
                                    break;
                                }
                                case (scope.calendarModelObject.RecurrenceIsEnabled && scope.calendarModelObject.EventRecurring.endBy == 2 && (!scope.calendarModelObject.EventRecurring.EndsAfterXOccurrences || scope.calendarModelObject.EventRecurring.EndsAfterXOccurrences == 0) ): {
                                    HFC.DisplayAlert("Ends After Occurrence(s) is required");
                                    break;
                                }
                                case (scope.calendarModelObject.RecurrenceIsEnabled && scope.calendarModelObject.EventRecurring.endBy == 3 && !scope.calendarModelObject.EventRecurring.EndsOn): {
                                    HFC.DisplayAlert("Valid Recurrence End Date is required");
                                    break;
                                }
                                case (scope.calendarModelObject.RecurrenceIsEnabled && scope.calendarModelObject.EventRecurring.endBy == 3 && endsOnDateTime < new Date(scope.calendarModelObject.end)): {
                                    HFC.DisplayAlert("Recurrence End Date must be greater than End Date");
                                    break;
                                }
                                case ((new Date(scope.calendarModelObject.start) < currentDateTime || new Date(scope.calendarModelObject.end) < currentDateTime) && !scope.calendarModelObject.allDay && alertModalService.invalidAcceptedFlag && !scope.calendarModelObject.IsTask): {
                                    alertModalService.setPopUpDetails(true, "Confirmation", "You have chosen the From Date past date. Do you want to continue?", scope.saveCalendarData, [returnFlag, convertFlag]);
                                    break;
                                }
                                case ((new Date(scope.calendarModelObject.start) < currentDate || new Date(scope.calendarModelObject.end) < currentDate) && scope.calendarModelObject.allDay && alertModalService.invalidAcceptedFlag && !scope.calendarModelObject.IsTask): {
                                    alertModalService.setPopUpDetails(true, "Confirmation", "You have chosen the From Date past date. Do you want to continue?", scope.saveCalendarData, [returnFlag, convertFlag]);
                                    break;
                                }
                                case ((((scope.calendarModelObject.Location !== scope.calendarModelObject.locationBackup ||
                                        startTime != originalStartTime ||
                                        endTime != originalEndTime ||
                                        !attendeeChangeFlag || scope.calendarModelObject.allDayBackup !== scope.calendarModelObject.allDay) &&
                                        alertModalService.editchangeEmailFlag) &&
                                        scope.editFlag) && (scope.calendarModelObject.IsNotifyemails || scope.calendarModelObject.IsNotifyText) && !scope.calendarModelObject.IsTask): {
                                    if (!alertModalService.invalidAcceptedFlag) {
                                        setTimeout(function () {
                                            var alertMessage;
                                            if (scope.calendarModelObject.IsNotifyemails && !scope.calendarModelObject.IsNotifyText)
                                                alertMessage = "Do you want to send an appointment confirmation email to the Customer?";
                                            else if (!scope.calendarModelObject.IsNotifyemails && scope.calendarModelObject.IsNotifyText)
                                                alertMessage = "Do you want to send an appointment confirmation text to the Customer?";
                                            else
                                                alertMessage = "Do you want to send an appointment confirmation email/text to the Customer?";
                                            alertModalService.setPopUpDetails(true, "Confirmation",alertMessage , scope.saveCalendarData, [returnFlag, convertFlag], null, null, true);
                                            alertModalService.editchangeEmailFlag = false;
                                        }, 500);
                                    } else {
                                        var alertMessage;
                                        if (scope.calendarModelObject.IsNotifyemails && !scope.calendarModelObject.IsNotifyText)
                                            alertMessage = "Do you want to send an appointment confirmation email to the Customer?";
                                        else if (!scope.calendarModelObject.IsNotifyemails && scope.calendarModelObject.IsNotifyText)
                                            alertMessage = "Do you want to send an appointment confirmation text to the Customer?";
                                        else
                                            alertMessage = "Do you want to send an appointment confirmation email/text to the Customer?";
                                        alertModalService.setPopUpDetails(true, "Confirmation", alertMessage, scope.saveCalendarData, [returnFlag, convertFlag], null, null, true);
                                        alertModalService.editchangeEmailFlag = false;
                                    }
                                    break;
                                }
                                default: {
                                    $("#modalBody")[0].scrollTop = 0;
                                    if (scope.editFlag && !alertModalService.editchangeEmailFlag) {
                                        scope.calendarModelObject.SendEmail = alertModalService.emailSendFlag;
                                    }
                                    alertModalService.editchangeEmailFlag = true;
                                    scope.getAttendesIdList(scope.calendarModelObject.AdditionalPeople);
                                    getReccurenceFieldList();
                                    scope.calendarModelObject.IsPrivate = scope.calendarModelObject.IsPrivateAccess;
                                    var loadingElement = $("#loading");
                                    if (loadingElement) {
                                        loadingElement[0].style.display = "block";
                                    }
                                    alertModalService.invalidAcceptedFlag = true;
                                    var url = scope.calendarModelObject.IsTask ? "api/Tasks/" : "/api/calendar/";
                                    if (scope.calendarModelObject.IsTask) {
                                        scope.calendarModelObject.end = scope.calendarModelObject.start;
                                        scope.calendarModelObject.EventRecurring = null;
                                    }
                                    $http.post(url, JSON.stringify(scope.calendarModelObject)).then(function (response) {
                                        //for reverting the styles of invalid date
                                        scope.ValidDatelist = [];
                                        scope.Invaliddate = false;

                                        if (loadingElement) {
                                            loadingElement[0].style.display = "none";
                                        }
                                        if (convertFlag) {
                                            if (scope.calendarModelObject.LeadId) {
                                                var link = "#!/leadConvert/" + scope.calendarModelObject.LeadId;
                                                window.location.href = link;
                                            }
                                        } else if (returnFlag && calendarModalService.rebindFunction) {
                                            calendarModalService.rebindFunction(false);
                                        } else if (calendarModalService.rebindFunction && !returnFlag) {
                                            calendarModalService.rebindFunction(true);
                                        }
                                        scope.closeCalendar("calendarModal", true);
                                        HFC.DisplaySuccess(response.statusText);
                                    });
                                }
                            }
                        }
                    }

                    // complete the task
                    scope.completeTask = function () {
                        var id = scope.calendarModelObject.id;
                        var loadingElement = $("#loading");
                        if (loadingElement) {
                            loadingElement[0].style.display = "block";
                        }
                        var markurl = '/api/Tasks/' + id + '/Mark?IsComplete=true';
                        $http.get(markurl).then(function (response) {
                            var res = response.data;
                            if (loadingElement) {
                                loadingElement[0].style.display = "none";
                            }
                            if (res) {
                                HFC.DisplaySuccess("Successful");
                            } else {
                                HFC.DisplayAlert("Error");
                            }
                            if (calendarModalService.rebindFunction) {
                                calendarModalService.rebindFunction(true);
                            }
                            scope.closeCalendar("calendarModal");
                            // resetData();
                        });
                    }

                    // incomplete the task
                    scope.incompleteTask = function () {
                        var Id = scope.calendarModelObject.id;
                        var loadingElement = $("#loading");
                        if (loadingElement) {
                            loadingElement[0].style.display = "block";
                        }
                        var markurl = '/api/Tasks/' + Id + '/Mark?IsComplete=false';
                        $http.get(markurl).then(function (response) {
                            var res = response.data;
                            if (loadingElement) {
                                loadingElement[0].style.display = "none";
                            }
                            if (res) {
                                HFC.DisplaySuccess("Successful");
                            } else {
                                HFC.DisplayAlert("Error");
                            }
                            if (calendarModalService.rebindFunction) {
                                calendarModalService.rebindFunction(true);
                            }                            
                            scope.closeCalendar("calendarModal");
                        });
                    }

                    // initiate delete appointment confirmation pop up
                    scope.cancelConfirmation = function (id) {
                        alertModalService.setPopUpDetails(true, "Cancel Confirmation", "Do you want to cancel this event?", scope.cancelAppointment, null, null, [], false, true);
                    }

                    // cancel the task
                    scope.cancelAppointment = function () {
                        var id = scope.calendarModelObject.id;
                        var loadingElement = $("#loading");
                        if (loadingElement) {
                            loadingElement[0].style.display = "block";
                        }
                        var cancelUrl = '/api/tasks/' + id + '/CancelEvent';
                        $http.put(cancelUrl).then(function (response) {
                            var res = response.data;
                            if (loadingElement) {
                                loadingElement[0].style.display = "none";
                            }
                            if (res) {
                                HFC.DisplaySuccess("Successful");
                            } else {
                                HFC.DisplayAlert("Error");
                            }
                            if (calendarModalService.rebindFunction) {
                                calendarModalService.rebindFunction(true);
                            }
                            scope.closeCalendar("calendarModal", true);
                        });
                    }

                    // initiate delete appointment confirmation pop up
                    scope.deleteConfirmation = function (id) {
                        alertModalService.setPopUpDetails(true, "Delete Confirmation", "Do you want to delete this event?", scope.deleteAppointment, null, null, [], false, true);
                    }

                    // delete the task
                    scope.deleteAppointment = function () {
                        var id = scope.calendarModelObject.id;
                        var loadingElement = $("#loading");
                        if (loadingElement) {
                            loadingElement[0].style.display = "block";
                        }
                        var deleteurl;
                        if (scope.calendarModelObject.IsTask) {
                            deleteurl = '/api/Tasks/' + id;
                        } else {
                            deleteurl = '/api/calendar/' + id;
                            if (scope.calendarModelObject.EventType == 1 && scope.singleFlag)
                                deleteurl += '?occurStartDate=' + scope.calendarModelObject.originalStartDate;
                        }
                        $http.delete(deleteurl).then(function (response) {
                            var res = response.data;
                            if (loadingElement) {
                                loadingElement[0].style.display = "none";
                            }
                            if (res) {
                                HFC.DisplaySuccess("Successful");
                            } else {
                                HFC.DisplayAlert("Error");
                            }
                            if (calendarModalService.rebindFunction) {
                                calendarModalService.rebindFunction(true);
                            }
                            scope.closeCalendar("calendarModal", true);
                        });
                    }

                    // close method
                    scope.closeCalendar = function (modalName, saveFlag) {
                        //for reverting the styles of invalid date
                        for (i = 0; i < scope.ValidDatelist.length; i++) {
                            if (!scope.ValidDatelist[i].valid) {
                                $("#" + scope.ValidDatelist[i].id + "").removeClass("invalid_Date");
                                var a = $('#' + scope.ValidDatelist[i].id + '').siblings();
                                $(a[0]).removeClass("icon_Background");
                                $("#" + scope.ValidDatelist[i].id + "").addClass("frm_controllead1");
                            }
                        }
                        scope.ValidDatelist = [];
                        scope.Invaliddate = false;

                        var ClosedModalName = "#" + modalName;
                        $("#modalBody")[0].scrollTop = 0;
                        $(ClosedModalName).modal("hide");
                        if (!saveFlag) {
                            reloadScheduler();
                        }
                        calendarModalService.setPopUpDetails(false);
                        resetData();
                    }

                    // dropdown options selected event handler
                    scope.DropdownOptionSelected = function (id, title, serviceCall) {
                        // add case
                        if (id && id > 0) {
                            // nullify the fields opposite fields
                            var nullifyListName = title + 'NullifyList';
                            var nullifyList = dropdownSelectDetails[nullifyListName];
                            nullifyField(nullifyList, scope);
                            calendarModalService[serviceCall](id, null, null, null, null, null, true);
                        } else {        // remove case
                            // nullify self fields
                            var nullifyListName = title + 'SelfNullifyList';
                            var nullifyList = dropdownSelectDetails[nullifyListName];
                            nullifyField(nullifyList, scope);
                        }

                        var dependentFieldsList = dropdownSelectDetails[title + 'DependencyList'];
                        for (var i = 0; i < dependentFieldsList.length; i++) {
                            var idText = "#" + dependentFieldsList[i];
                            var dropdownSource = $(idText).data("kendoComboBox");
                            dropdownSource.dataSource.read();
                        }
                    }

                    // set the datetime to datepickers for operations
                    /*
                     * param1 - datetime / date string
                     * param2 - create appointment flag, true, only for create flow
                     * param3 - allDay appointment flag, true, only the appointment saved as all day
                     * param4 - date selected from datepicker flag to restrict rounded off
                     * 
                    */
                    scope.setDateTimeFields = function (dateTimeString, createAppointmentFlag, allDayFlag, datePickerSelectionFlag) {
                        var startDate = calendarModalService.getDateTimeString(dateTimeString, null, createAppointmentFlag, allDayFlag, false, datePickerSelectionFlag, scope.calendarModelObject.allDayBackup);
                        // interval calculations
                        var intervalObj = scope.intervalFinder();
                        var endDate = calendarModalService.getDateTimeString(dateTimeString, intervalObj, createAppointmentFlag, allDayFlag, false, datePickerSelectionFlag, scope.calendarModelObject.allDayBackup);
                        console.log("startttttttttttt         ", startDate);
                        console.log("enddddddddddd      ", endDate);
                        scope.calendarModelObject.start = startDate;
                        scope.calendarModelObject.end = endDate;
                        // reccurence sceanrio on create appointment flow
                        if (!scope.calendarModelObject.IsTask && scope.calendarModelObject.EventRecurring && scope.calendarModelObject.EventRecurring.EndsOn && !scope.editFlag) {
                            scope.calendarModelObject.EventRecurring.StartDate = calendarModalService.getDateTimeString(dateTimeString, null, createAppointmentFlag, allDayFlag, true, datePickerSelectionFlag);
                            scope.calendarModelObject.EventRecurring.EndsOn = calendarModalService.getDateTimeString(dateTimeString, intervalObj, createAppointmentFlag, allDayFlag, true, datePickerSelectionFlag);
                        } else if (!scope.calendarModelObject.IsTask && scope.calendarModelObject.EventRecurring && scope.calendarModelObject.EventRecurring.EndsOn && scope.editFlag) {
                            scope.calendarModelObject.EventRecurring.StartDate = calendarModalService.getDateTimeString(dateTimeString, null, createAppointmentFlag, allDayFlag, true, datePickerSelectionFlag);
                            scope.calendarModelObject.EventRecurring.EndsOn = calendarModalService.getDateTimeString(scope.calendarModelObject.EventRecurring.EndsOn, intervalObj, createAppointmentFlag, allDayFlag, true, datePickerSelectionFlag);
                        } else {
                            scope.calendarModelObject.EventRecurring.StartDate = calendarModalService.getDateTimeString(dateTimeString, null, createAppointmentFlag, allDayFlag, true, datePickerSelectionFlag);
                            scope.calendarModelObject.EventRecurring.EndsOn = calendarModalService.getDateTimeString(dateTimeString, intervalObj, createAppointmentFlag, allDayFlag, true, datePickerSelectionFlag);
                        }
                        // set selected date for date pickers
                        if ($("#calendarStartDate").data("kendoDateTimePicker"))
                            $("#calendarStartDate").data("kendoDateTimePicker").value(scope.calendarModelObject.start);
                        if ($("#calendarEndDate").data("kendoDateTimePicker"))
                            $("#calendarEndDate").data("kendoDateTimePicker").value(scope.calendarModelObject.end);

                        if (scope.calendarModelObject.IsTask && $("#calendarTaskStartDatePicker").data("kendoDatePicker")) {
                            scope.ValidateonBlur('calendarTaskStartDatePicker', 'date');
                        }
                        if (!scope.calendarModelObject.IsTask && $("#calendarStartDate").data("kendoDateTimePicker")) {
                            scope.ValidateonBlur('calendarStartDate', 'datetime');
                        }
                        if (!scope.calendarModelObject.IsTask && $("#calendarStartDatePicker").data("kendoDatePicker")) {
                            scope.ValidateonBlur('calendarStartDatePicker', 'date');
                        }
                        if (!scope.calendarModelObject.IsTask && $("#calendarEndDate").data("kendoDateTimePicker")) {
                            scope.ValidateonBlur('calendarEndDate', 'datetime');
                        }
                        if (!scope.calendarModelObject.IsTask && $("#calendarEndDatePicker").data("kendoDatePicker")) {
                            scope.ValidateonBlur('calendarEndDatePicker', 'date');
                        }
                    }

                    // find the interval betwen the dates
                    scope.intervalFinder = function () {
                        var obj = {};
                        if (scope.editFlag && !scope.calendarModelObject.allDayBackup) {
                            var interval = scope.interval / 3600000;
                            var intervalHours = interval / 1;
                            var intervalMinutes = Math.round((interval % 1) * 60);
                            obj['hour'] = intervalHours;
                            obj['minute'] = intervalMinutes;
                        } else if (scope.editFlag && (scope.calendarModelObject.allDayBackup && !scope.calendarModelObject.allDayFlag)) {
                            //var interval = scope.interval / 3600000;
                            //var intervalHours = interval / 1;
                            //var startDateHour = (new Date()).getHours();
                            //var pendingHours;
                            //if (startDateHour > intervalHours || (scope.calendarModelObject.allDayFlag == undefined || scope.calendarModelObject.allDayFlag)) {
                            //    pendingHours = intervalHours - startDateHour;
                            //} else {
                            //    pendingHours = intervalHours;
                            //}
                            // nullify date hour for date manipulation
                            //endDate.setHours(0);
                            //endDate.setMinutes(0);
                            //endDate.setHours(pendingHours);
                            // endDate.setMinutes(endDate.getMinutes() + intervalMinutes);
                            // nullify the hour for all day
                            //endDate.setHours(0);
                            //endDate.setMinutes(0);
                            //endDate.setHours(startDate.getHours() + 1);
                            //endDate.setMinutes(startDate.getMinutes());
                            obj['hour'] = 1;
                            obj['minute'] = 0;
                        } else if (scope.editFlag && (scope.calendarModelObject.allDayBackup && scope.calendarModelObject.allDayFlag)) {
                            obj['hour'] = 0;
                            obj['minute'] = 0;
                        } else {
                            obj['hour'] = 1;
                        }
                        return obj;
                    }

                    // set the start date & end date time
                    scope.setStartEndDateTime = function (currentDateTime, dateSelectedFlag, dateFlag, calendarCreateFlag, event, selectFlag, allDayFlag) {
                        var dateOptimiseFlag = true;
                            if (currentDateTime) {
                                if (typeof (currentDateTime) == "string") {
                                    if (currentDateTime.indexOf("Z") != -1) {
                                        currentDateTime = currentDateTime.substring(0, currentDateTime.length - 1);
                                    }
                                    currentDateTime = new Date(currentDateTime);
                                }

                                if (allDayFlag) {
                                    var currentDate = new Date();
                                    currentDateTime.setHours(currentDate.getHours());
                                    currentDateTime.setMinutes(currentDate.getMinutes());
                                }


                                if ((!scope.editFlag && !dateSelectedFlag && !calendarCreateFlag) || allDayFlag) {
                                    var currentMinute = currentDateTime.getMinutes();
                                    var difference = currentMinute / 15;
                                    var minute;
                                    switch (true) {
                                        case (difference < 1):
                                            {
                                                minute = 15;
                                                break;
                                            }
                                        case (difference < 2):
                                            {
                                                minute = 30;
                                                break;
                                            }
                                        case (difference < 3):
                                            {
                                                minute = 45;
                                                break;
                                            }
                                        case (difference < 4):
                                            {
                                                minute = 0;
                                                break;
                                            }
                                    }
                                    if (minute === 0) {
                                        currentDateTime.setMinutes(minute);
                                        currentDateTime.setHours(currentDateTime.getHours() + 1);
                                    } else {
                                        currentDateTime.setMinutes(minute);
                                    }
                                }

                                // start date manipulation
                                var startDate = angular.copy(currentDateTime);
                                scope.calendarModelObject.start = calendarModalService.getDateStringValue(startDate, false, selectFlag, null, scope.dateStringFlag, allDayFlag);
                                
                                // end date manipulation
                                var endDate = angular.copy(currentDateTime);
                                if (scope.editFlag && !scope.calendarModelObject.allDayBackup) {
                                    var interval = scope.interval / 3600000;
                                    var intervalHours = interval / 1;
                                    var intervalMinutes = Math.round((interval % 1) * 60);
                                    endDate.setHours(endDate.getHours() + intervalHours);
                                    endDate.setMinutes(endDate.getMinutes() + intervalMinutes);
                                } else if (scope.editFlag && (scope.calendarModelObject.allDayBackup || scope.calendarModelObject.allDayFlag)) {
                                    var interval = scope.interval / 3600000;
                                    var intervalHours = interval / 1;
                                    var startDateHour = startDate.getHours();
                                    var pendingHours;
                                    if (startDateHour > intervalHours || (scope.calendarModelObject.allDayFlag == undefined || scope.calendarModelObject.allDayFlag)) {
                                        pendingHours = intervalHours - startDateHour;
                                    } else {
                                        pendingHours = intervalHours;
                                    }
                                    // nullify date hour for date manipulation
                                    endDate.setHours(0);
                                    endDate.setMinutes(0);
                                    endDate.setHours(pendingHours);
                                    // endDate.setMinutes(endDate.getMinutes() + intervalMinutes);
                                    // nullify the hour for all day
                                    endDate.setHours(0);
                                    endDate.setMinutes(0);
                                    endDate.setHours(startDate.getHours() + 1);
                                    endDate.setMinutes(startDate.getMinutes());
                                } else {
                                    endDate.setHours(endDate.getHours() + 1);
                                }
                                scope.calendarModelObject.end = calendarModalService.getDateStringValue(endDate, false, selectFlag, null, scope.dateStringFlag, allDayFlag);

                                // date validation
                                var startTime = new Date(scope.calendarModelObject.start).getHours() + ":" + new Date(scope.calendarModelObject.start).getMinutes();
                                if (startTime == "23:00" || startTime == "23:0") {
                                    var startTimeValue = new Date(scope.calendarModelObject.start).getTime();
                                    if (scope.editFlag && scope.interval) {
                                        endTimeValue = startTimeValue + scope.interval;
                                    } else {
                                        endTimeValue = startTimeValue + 3600000;
                                    }
                                    var updatedEndDate = new Date(endTimeValue);
                                    scope.calendarModelObject.end = (updatedEndDate.getMonth() + 1) + "/" + updatedEndDate.getDate() + "/" + updatedEndDate.getFullYear() + " " + updatedEndDate.getHours() + ":" + updatedEndDate.getMinutes() + " AM";
                                }
                                // recurrence field population
                                if (!scope.calendarModelObject.IsTask && scope.calendarModelObject.EventRecurring && scope.calendarModelObject.EventRecurring.EndsOn && !scope.editFlag) {
                                    scope.calendarModelObject.EventRecurring.StartDate = scope.calendarModelObject.start;
                                    if (scope.calendarModelObject.EventRecurring.EndsOn && scope.editFlag) {
                                        if (typeof (scope.calendarModelObject.EventRecurring.EndsOn) == "string") {
                                            if ((scope.calendarModelObject.EventRecurring.EndsOn).indexOf("Z") != -1) {
                                                scope.calendarModelObject.EventRecurring.EndsOn = (scope.calendarModelObject.EventRecurring.EndsOn).substring(0, (scope.calendarModelObject.EventRecurring.EndsOn).length - 1);
                                            }
                                            if ((scope.calendarModelObject.EventRecurring.EndsOn).indexOf("+") != -1) {
                                                var splitValue = (scope.calendarModelObject.EventRecurring.EndsOn).split("+");
                                                scope.calendarModelObject.EventRecurring.EndsOn = splitValue[0];
                                            }
                                            scope.calendarModelObject.EventRecurring.EndsOn = new Date(scope.calendarModelObject.EventRecurring.EndsOn);
                                        }
                                        // scope.calendarModelObject.EventRecurring.EndDate = calendarModalService.manipulateDateString(new Date(scope.calendarModelObject.EventRecurring.EndDate), dateOptimiseFlag, dateFlag, 'end');
                                        scope.calendarModelObject.EventRecurring.EndsOn = calendarModalService.getDateStringValue(new Date(scope.calendarModelObject.EventRecurring.EndsOn), false, selectFlag, null, scope.dateStringFlag, allDayFlag);
                                    } else {
                                        scope.calendarModelObject.EventRecurring.EndsOn = scope.calendarModelObject.end;
                                    }
                                    scope.setReccuranceValue(scope.calendarModelObject.EventRecurring.PatternEnum, scope.calendarModelObject.EventRecurring.RecursEvery);
                                } else if (scope.editFlag && scope.calendarModelObject.EventRecurring && !scope.calendarModelObject.EventRecurring.EndsOn) {
                                    scope.calendarModelObject.EventRecurring.EndsOn = scope.calendarModelObject.end;
                                    scope.setReccuranceValue(scope.calendarModelObject.EventRecurring.PatternEnum, scope.calendarModelObject.EventRecurring.RecursEvery);
                                }
                                if (!scope.calendarModelObject.IsTask && scope.calendarModelObject.EventRecurring && scope.editFlag) {
                                    scope.calendarModelObject.EventRecurring.StartDate = scope.calendarModelObject.start;
                                    scope.setReccuranceValue(scope.calendarModelObject.EventRecurring.PatternEnum, scope.calendarModelObject.EventRecurring.RecursEvery);
                                }
                            }
                        // set selected date for date pickers
                            //if (scope.calendarModelObject.EventRecurring && scope.editFlag && !scope.singleFlag) {
                                if ($("#calendarStartDate").data("kendoDateTimePicker"))
                                    $("#calendarStartDate").data("kendoDateTimePicker").value(scope.calendarModelObject.start);
                                if ($("#calendarEndDate").data("kendoDateTimePicker"))
                                    $("#calendarEndDate").data("kendoDateTimePicker").value(scope.calendarModelObject.end);
                        //}

                        if (scope.calendarModelObject.IsTask && $("#calendarTaskStartDatePicker").data("kendoDatePicker")) {
                            scope.ValidateonBlur('calendarTaskStartDatePicker', 'date');
                        }
                        if (!scope.calendarModelObject.IsTask && $("#calendarStartDate").data("kendoDateTimePicker")) {
                            scope.ValidateonBlur('calendarStartDate', 'datetime');
                        }
                        if (!scope.calendarModelObject.IsTask && $("#calendarStartDatePicker").data("kendoDatePicker")) {
                            scope.ValidateonBlur('calendarStartDatePicker', 'date');
                        }
                        if (!scope.calendarModelObject.IsTask && $("#calendarEndDate").data("kendoDateTimePicker")) {
                            scope.ValidateonBlur('calendarEndDate', 'datetime');
                        }
                        if (!scope.calendarModelObject.IsTask && $("#calendarEndDatePicker").data("kendoDatePicker")) {
                            scope.ValidateonBlur('calendarEndDatePicker', 'date');
                        }
                    }

                    // get attendess id list
                    scope.getAttendesIdList = function (selectedEmailList) {
                        scope.calendarModelObject.Attendees = selectedEmailList;
                        scope.calendarModelObject.selectedIds = selectedEmailList[0];
                        var list = $("#AttendeesDropdown").data("kendoMultiSelect");
                        if (list) {
                            var selectedItem = JSLINQ(list.dataSource._data).Where(function (item) {
                                    return item["Email"] == selectedEmailList[0]
                            });
                            if (selectedItem["items"] && selectedItem["items"][0]) {
                                scope.calendarModelObject.AssignedPersonId = selectedItem["items"][0]["SalesAgentId"];
                            }
                        }
                    }

                    // removes reccurance fields
                    scope.resetReccuranceFields = function (flag) {
                        if (scope.calendarModelObject.RecurrenceIsEnabled) {
                            setTimeout(function () {
                                var bodyElementHeight = $("#modalBody")[0].scrollHeight;
                                $("#modalBody")[0].scrollTo(0, bodyElementHeight);
                            }, 100);
                        }
                        if (!scope.calendarModelObject.EventRecurring.PatternEnum || flag) {
                            scope.calendarModelObject.EventRecurring.PatternEnum = 1;
                        }
                        scope.calendarModelObject.EventRecurring.RecursEvery = 1;

                        scope.calendarModelObject.EventRecurring.flagMonthly = 1;

                        scope.calendarModelObject.EventRecurring.DayOfMonth = 1; 

                        scope.calendarModelObject.EventRecurring.DayOfWeekIndex = 1;
                        scope.calendarModelObject.EventRecurring.DayOfWeekEnum = 1;

                        scope.calendarModelObject.EventRecurring.MonthEnum = 1;
                        scope.calendarModelObject.EventRecurring.flagYearly = 1;
                        scope.calendarModelObject.EventRecurring.endBy = 1;

                        scope.calendarModelObject.EventRecurring.StartDate = scope.calendarModelObject.start;
                        // scope.calendarModelObject.EventRecurring.EndsOn = scope.calendarModelObject.end;
                        scope.calendarModelObject.EventRecurring.EndsAfterXOccurrences = 1;
                        // scope.calendarModelObject.EventRecurring.EndDate = scope.calendarModelObject.end;
                        if (scope.calendarModelObject.end) {
                            var endDate = new Date(scope.calendarModelObject.end);
                            var endString = (endDate.getMonth() + 1) + "/" + endDate.getDate() + "/" + endDate.getFullYear();
                            scope.calendarModelObject.EventRecurring.EndDate = endString;
                            scope.calendarModelObject.EventRecurring.EndsOn = endString;
                        }
                        for (var i = 0; i < scope.dayList.length; i++) {
                            scope.dayList[i]['checked'] = false;
                        }
                    }

                    // set day list data for weekly reccurance
                    scope.setWeeklyDateSelected = function (selectedDay, flag) {
                        if (!scope.editFlag || flag) {
                            for (var i = 0; i < scope.dayList.length; i++) {
                                if (scope.dayList[i]['dayValue'] == selectedDay && !scope.dayList[i]['checked']) {
                                    scope.selectedDayList[i] = scope.dayList[i]['dayString'];
                                    scope.dayList[i]['checked'] = true;
                                } else if (scope.dayList[i]['dayValue'] == selectedDay && scope.dayList[i]['checked']) {
                                    scope.dayList[i]['checked'] = false;
                                    scope.selectedDayList[i] = null;
                                }
                            }
                        }
                        if (flag) {
                            scope.setReccuranceValue(scope.calendarModelObject.EventRecurring.PatternEnum, scope.calendarModelObject.EventRecurring.RecursEvery);
                        }
                    }

                    // weekly day click event handler
                    scope.weeklyOptionClicked = function (value) {
                        if (scope.editFlag) {
                            scope.setWeeklyDateSelected(value['dayValue'], true);
                        }
                    }

                    // generate occurence text
                    scope.setReccuranceValue = function (pattern, occurence, repeatCount) {                        
                        scope.reccuranceString = "";
                        var recurrenceStartDate;
                        var recurrenceEndDate;
                        if (scope.calendarModelObject.EventRecurring && scope.calendarModelObject.EventRecurring.StartDate) {
                            //recurrenceStartDate = (scope.calendarModelObject.EventRecurring.StartDate).substring(0, 16);
							var strLen = 16;
                            var monthLength = (scope.calendarModelObject.EventRecurring.StartDate).split("/")[0];
                            var dayLength = (scope.calendarModelObject.EventRecurring.StartDate).split("/")[1];
                            if (monthLength.length == 1 && dayLength.length == 1) {
                                strLen = 15;
                            }
                            recurrenceStartDate = (scope.calendarModelObject.EventRecurring.StartDate).substring(0, strLen);
                        
                        }
                        if (scope.calendarModelObject.EventRecurring && scope.calendarModelObject.EventRecurring.EndsOn) {
                            //recurrenceEndDate = (scope.calendarModelObject.EventRecurring.EndsOn).substring(0, 16);
							var strLen = 16;
                            var monthLength = (scope.calendarModelObject.EventRecurring.EndsOn).split("/")[0];
                            var dayLength = (scope.calendarModelObject.EventRecurring.EndsOn).split("/")[1];
                            if (monthLength && dayLength && monthLength.length == 1 && dayLength.length == 1) {
                                strLen = 15;
                            }
                            recurrenceEndDate = (scope.calendarModelObject.EventRecurring.EndsOn).substring(0, strLen);
                        }
                        if (recurrenceStartDate)
                            // var startDate = calendarModalService.getDateStringValue(new Date(recurrenceStartDate), false, true, true);
                            var startDate = calendarModalService.getDateTimeString(recurrenceStartDate, null, false, scope.calendarModelObject.allDayFlag, true, false);
                        // var startDate = (startDateObject.getMonth() + 1) + "/" + startDateObject.getDate() + "/" + startDateObject.getFullYear();
                        if (recurrenceEndDate)
                            // var endDate = calendarModalService.getDateStringValue(new Date(recurrenceEndDate), false, true, true);
                            var endDate = calendarModalService.getDateTimeString(recurrenceEndDate, null, false, scope.calendarModelObject.allDayFlag, true, false);
                        // var endDate = (endDateObject.getMonth() + 1) + "/" + endDateObject.getDate() + "/" + endDateObject.getFullYear();
                        switch (pattern) {
                            case 1: {
                                if (occurence == 1) {
                                    scope.reccuranceString = "Occurs daily effective from " + startDate;
                                } else if (occurence > 1) {
                                    scope.reccuranceString = "Occurs every " + occurence + " days effective from " + startDate;
                                }
                                break;
                            }
                            case 2: {
                                if (occurence == 1) {
                                    scope.reccuranceString = "Occurs weekly ";
                                    scope.reccuranceString += "on ";
                                    for (var i = 0; i < scope.dayList.length; i++) {
                                       if (scope.dayList[i]['checked']) {
                                          scope.reccuranceString += scope.dayList[i]['dayString'] + ', ';
                                       }
                                    }
                                    scope.reccuranceString.slice(0, scope.reccuranceString.length - 2);
                                    scope.reccuranceString += " effective from " + startDate;
                                } else {
                                    scope.reccuranceString = "Occurs every " + occurence + " weeks";
                                    scope.reccuranceString += "on ";
                                    for (var i = 0; i < scope.dayList.length; i++) {
                                        if (scope.dayList[i]['checked']) {
                                           scope.reccuranceString += scope.dayList[i]['dayString'] + ', ';
                                        }
                                    }
                                    scope.reccuranceString.slice(0, scope.reccuranceString.length - 2);
                                    scope.reccuranceString += " effective from " + startDate;
                                }
                                break;
                            }
                            case 3: {
                                if (occurence == 1) {
                                    if (scope.calendarModelObject.EventRecurring.flagMonthly == 1) {
                                        scope.reccuranceString = "Occurs monthly " + scope.calendarModelObject.EventRecurring.DayOfMonth + " day effective from " + startDate;
                                    } else if (scope.calendarModelObject.EventRecurring.flagMonthly == 2) {
                                        scope.reccuranceString = "Occurs monthly " + scope.weekIndexArray[scope.calendarModelObject.EventRecurring.DayOfWeekIndex] + " " + scope.weekDaysArray[scope.calendarModelObject.EventRecurring.DayOfWeekEnum] + " effective from " + startDate;
                                    }
                                } else {
                                    if (scope.calendarModelObject.EventRecurring.flagMonthly == 1) {
                                        scope.reccuranceString = "Occurs every " + occurence + " months " + scope.calendarModelObject.EventRecurring.DayOfMonth + " day effective from " + startDate;
                                    } else if (scope.calendarModelObject.EventRecurring.flagMonthly == 2) {
                                        scope.reccuranceString = "Occurs every " + occurence + " months " + scope.weekIndexArray[scope.calendarModelObject.EventRecurring.DayOfWeekIndex] + " " + scope.weekDaysArray[scope.calendarModelObject.EventRecurring.DayOfWeekEnum] + " effective from " + startDate;
                                    }
                                }
                                break;
                            }
                            case 4: {
                                if (occurence == 1) {
                                    if (scope.calendarModelObject.EventRecurring.flagYearly == 1) {
                                        scope.reccuranceString = "Occurs yearly " + scope.yearArray[scope.calendarModelObject.EventRecurring.MonthEnum] + " " + scope.calendarModelObject.EventRecurring.DayOfMonth + " day effective from " + startDate;
                                    } else {
                                        scope.reccuranceString = "Occurs yearly " + scope.yearArray[scope.calendarModelObject.EventRecurring.MonthEnum] + " " + scope.weekIndexArray[scope.calendarModelObject.EventRecurring.DayOfWeekIndex] + " " + scope.weekDaysArray[scope.calendarModelObject.EventRecurring.DayOfWeekEnum] + " effective from " + startDate;
                                    }
                                }
                            }
                        }

                        switch (scope.calendarModelObject.EventRecurring.endBy) {
                            case 2: {
                                scope.reccuranceString += " " + scope.calendarModelObject.EventRecurring.EndsAfterXOccurrences + " time (s)";
                                break;
                            }
                            case 3: {
                                scope.reccuranceString += " until " + endDate;
                            }
                        }
                        setTimeout(function () {
                            if (($("#recurringEndDate") && $("#recurringEndDate").data("kendoDatePicker")) && (scope.calendarModelObject.EventRecurring.endBy != 3 || scope.disableRecurrenceFlag))
                                $("#recurringEndDate").data("kendoDatePicker").enable(false);
                        }, 100);
                        
                    }

                    // set date picker value as date only not time
                    scope.allDayTapped = function (flag) {                       
                        if (scope.editFlag && scope.calendarModelObject.allDayBackup) {
                            setTimeout(function () {
                                initializeDatePicker();
                                scope.dateStringFlag = false;
                                if (flag) {
                                    scope.setDateTimeFields(scope.calendarModelObject.start, !scope.editFlag, true, false);
                                    // scope.setStartEndDateTime(scope.calendarModelObject.start, true, true, false, false, false, false);
                                } else {
                                    scope.setDateTimeFields(scope.calendarModelObject.start, !scope.editFlag, false, false);
                                    // scope.setStartEndDateTime(scope.calendarModelObject.start, true, false, false, false, false, true);
                                }
                            }, 100);
                        }
                    }

                    // end date tapped event handler
                    scope.endDateTapped = function () {
                        // for all day end date tapped
                        var dateformatflag = true;
                        //for preventing bind for invalid format
                        var dateFormat = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
                        var dateTimeFormat = /^\d{1,2}\/\d{1,2}\/\d{4}\s\d{1,2}:\d{1,2}\s\w{2}$/;
                        if (!dateFormat.test(scope.calendarModelObject.end) || !dateTimeFormat.test(scope.calendarModelObject.end))
                            dateformatflag = false;
                        
                        if (scope.calendarModelObject.allDay && !scope.calendarModelObject.allDayBackup && dateformatflag) {
                            var startTime = (scope.calendarModelObject.start).split(" ")[1];
                            var hour = parseInt((startTime).split(":")[0]);
                            var minute = parseInt((startTime).split(":")[1]);
                            var AMPMString = (scope.calendarModelObject.start).split(" ")[2];
                            if (hour == 12) {
                                hour = 0;
                                AMPMString = "PM";
                            }
                            scope.calendarModelObject.end = scope.calendarModelObject.end + " " + (hour + 1) + ":" + minute + " " + AMPMString;
                        }
                        if (scope.calendarModelObject.EventRecurring && !scope.editFlag) {
                            scope.calendarModelObject.EventRecurring.EndsOn = (scope.calendarModelObject.end).split(" ")[0];
                            scope.setReccuranceValue(scope.calendarModelObject.EventRecurring.PatternEnum, scope.calendarModelObject.EventRecurring.RecursEvery);
                            //for date validation
                            if ($("#recurringEndDate").data("kendoDatePicker")) {
                                scope.ValidateonBlur('recurringEndDate', 'date');
                            }
                        }
                    }

                    // to make the title & location fields optional based on appointment tupe selection
                    scope.appointmentTypeTapped = function () {
                        switch (scope.calendarModelObject.AptTypeEnum) {
                            case 8:
                            case 9:
                            case 10:
                            case 11:
                            case 12: {
                                scope.subjectLocationOptionalFlag = true;
                                break;
                            }
                            default: {
                                scope.subjectLocationOptionalFlag = false;
                            }
                        }
                    }

                    // initilaize the dtae picker for fix the date picker hiding issue
                    scope.initializeRecurrenceDatePicker = function () {
                        setTimeout(function () {
                            $("#recurringEndDate").kendoDatePicker({
                                popup: {
                                    position: "bottom left",
                                    origin: "top left"
                                }
                            });
                            scope.$apply();
                            if ($("#recurringEndDate").data("kendoDatePicker"))
                            $("#recurringEndDate").data("kendoDatePicker").enable(false);
                            if (scope.calendarModelObject.EventRecurring.EndsOn && scope.editFlag) {
                                scope.calendarModelObject.EventRecurring.EndsOn = (scope.calendarModelObject.EventRecurring.EndsOn).split(" ")[0];
                                if (scope.calendarModelObject.EventRecurring.endBy === 3) {
                                    scope.enableDatePicker();
                                }
                            }
                            var datepickerOpenFlag;
                            var datepicker = $("#recurringEndDate").data("kendoDatePicker");
                            datepicker.bind("close", function (e) {
                                if (datepickerOpenFlag) {
                                    e.preventDefault();
                                }
                            });
                            datepicker.bind("open", function (e) {
                                datepickerOpenFlag = true;
                                var timer = setTimeout(function() {
                                    datepickerOpenFlag = false;
                                    clearTimeout(timer);
                                }, 300);
                            });
                            datepicker.bind("change", function () {
                                datepickerOpenFlag = false;
                            });
                        }, 100);
                    }

                    // enable the date picker in recurrence
                    scope.enableDatePicker = function () {
                        $("#recurringEndDate").data("kendoDatePicker").enable(true);
                    }


                    // local methods

                    // attendee change validation
                    var validateAttendeeChange = function () {
                        var count = 0;
                        if (scope.calendarModelObject.AdditionalPeople && scope.calendarModelObject.AdditionalPeople.length != 0 && scope.calendarModelObject.attendeesBackup && scope.calendarModelObject.attendeesBackup.length != 0) {
                            if (scope.calendarModelObject.attendeesBackup.length != scope.calendarModelObject.AdditionalPeople.length) {
                                return false;
                            } else {
                                for (var i = 0; i < scope.calendarModelObject.AdditionalPeople.length; i++) {
                                    for (var j = 0; j < scope.calendarModelObject.attendeesBackup.length; j++) {
                                        if (scope.calendarModelObject.AdditionalPeople[i] == scope.calendarModelObject.attendeesBackup[j]) {
                                            count++;
                                        }
                                    }
                                }
                                if (count == scope.calendarModelObject.AdditionalPeople.length) {
                                    return true;
                                }
                            }
                        }
                        return false;
                    }

                    // get other module data
                    var reloadScheduler = function () {
                        var routePath = $location.path();
                        var routeParamList = routePath.split("/");
                        for (var i = 0; i < routeParamList.length; i++) {
                            if (routeParamList[i] == "Calendar" && calendarModalService.rebindFunction) {
                                calendarModalService.rebindFunction(true);
                            }
                        }
                    }

                    // set reccurence values
                    var setReccurenceField = function () {
                        if (scope.calendarModelObject.RecurrenceIsEnabled) {
                            if (scope.calendarModelObject.EventRecurring.PatternEnum == 2) {
                                if (scope.calendarModelObject.EventRecurring.DayOfWeekEnum) {
                                    var balance = scope.calendarModelObject.EventRecurring.DayOfWeekEnum;
                                    var previousValue;
                                    for (var i = scope.dayList.length - 1; i >= 0 ; i--) {
                                        previousValue = balance;
                                        balance = balance - scope.dayList[i]['dayValue'];
                                        if (balance >= 0) {
                                            scope.dayList[i]['checked'] = true;
                                        } else {
                                            balance = previousValue;
                                        }
                                    }
                                }
                            }
                            // monthly case optimisation
                            if (scope.calendarModelObject.EventRecurring.PatternEnum == 3) {
                                if (scope.calendarModelObject.EventRecurring.DayOfMonth > 0) {
                                    scope.calendarModelObject.EventRecurring.flagMonthly = 1;
                                    scope.calendarModelObject.EventRecurring.DayOfWeekIndex = 1;
                                    scope.calendarModelObject.EventRecurring.DayOfWeek = 1;
                                } else {
                                    scope.calendarModelObject.EventRecurring.flagMonthly = 2;
                                    scope.calendarModelObject.EventRecurring.DayOfMonth = 1;
                                }
                            }
                            // yearly case
                            if (scope.calendarModelObject.EventRecurring.PatternEnum == 4) {
                                if (scope.calendarModelObject.EventRecurring.DayOfMonth) {
                                        scope.calendarModelObject.EventRecurring.flagYearly = 1;
                                        scope.calendarModelObject.EventRecurring.DayOfWeekEnum = 1;
                                        scope.calendarModelObject.EventRecurring.DayOfWeekIndex = 1;
                                    }
                                else {
                                    scope.calendarModelObject.EventRecurring.flagYearly = 2;
                                        scope.calendarModelObject.EventRecurring.DayOfMonth = 1;
                                    }
                            }
                            switch (true) {
                                case (scope.calendarModelObject.EventRecurring.EndsAfterXOccurrences != null): {
                                    scope.calendarModelObject.EventRecurring.endBy = 2;
                                    break;
                                }
                                case (scope.calendarModelObject.EventRecurring.EndsOn != null): {
                                    scope.calendarModelObject.EventRecurring.endBy = 3;
                                    break;
                                }
                                default: {
                                    scope.calendarModelObject.EventRecurring.endBy = 1;
                                }
                            }
                        }
                    }

                    // get reccurence fields based on selection
                    var getReccurenceFieldList = function () {
                            if (scope.calendarModelObject.RecurrenceIsEnabled && scope.calendarModelObject.EventType != 1) {		                 
                                if (scope.calendarModelObject.EventType == 0) {		
                                    scope.calendarModelObject.EventType = 2;		
                                }
                            scope.calendarModelObject.AddRecurrence = true;
                            switch (scope.calendarModelObject.EventRecurring.endBy) {
                                case 1: {
                                    scope.calendarModelObject.EventRecurring.EndsAfterXOccurrences = null;
                                    scope.calendarModelObject.EventRecurring.EndsOn = null;
                                    scope.calendarModelObject.EventRecurring.EndDate = null;
                                    break;
                                }
                                case 2: {
                                    scope.calendarModelObject.EventRecurring.EndsOn = null;
                                    scope.calendarModelObject.EventRecurring.EndDate = null;
                                    break;
                                }
                                case 3: {
                                    scope.calendarModelObject.EventRecurring.EndsAfterXOccurrences = null;
                                    scope.calendarModelObject.EventRecurring.EndDate = scope.calendarModelObject.end;
                                    break;
                                }
                            }
                            switch (scope.calendarModelObject.EventRecurring.PatternEnum) {
                                case 2: {
                                    scope.calendarModelObject.EventRecurring.DayOfWeekEnum = 0;
                                    for (var i = 0; i < scope.dayList.length; i++) {
                                        if (scope.dayList[i]['checked']) {
                                            scope.calendarModelObject.EventRecurring.DayOfWeekEnum += scope.dayList[i]['dayValue'];
                                        }
                                    }
                                    scope.calendarModelObject.EventRecurring.DayOfWeekIndex = null;
                                    scope.calendarModelObject.EventRecurring.DayOfMonth = null;
                                    break;
                                }
                                case 3: {
                                    if (scope.calendarModelObject.EventRecurring.flagMonthly == 1) {
                                        scope.calendarModelObject.EventRecurring.DayOfWeekIndex = null;
                                        scope.calendarModelObject.EventRecurring.DayOfWeekEnum = null;
                                    } else {
                                        scope.calendarModelObject.EventRecurring.DayOfMonth = null;
                                    }
                                    break;
                                }
                                case 4: {
                                    if (scope.calendarModelObject.EventRecurring.flagYearly == 1) {
                                        scope.calendarModelObject.EventRecurring.DayOfWeekIndex = null;
                                        scope.calendarModelObject.EventRecurring.DayOfWeekEnum = null;
                                    } else {
                                        scope.calendarModelObject.EventRecurring.DayOfMonth = null;
                                    }
                                    break;
                                }
                            }
                            } else if (scope.calendarModelObject.EventType == 1) {	
                                scope.calendarModelObject.EventRecurring = null;
                                if (scope.singleFlag)
                                    scope.calendarModelObject.id = 0;
                            } else {
                                scope.calendarModelObject.EventRecurring = null;
                                scope.calendarModelObject.RecurringEventId = null;
                            }
                    }

                    // get interval between start and end date in edit mode
                    var getInterval = function () {
                        scope.interval = new Date(scope.calendarModelObject.end).getTime() - new Date(scope.calendarModelObject.start).getTime();
                    }

                    // setter for date fields min & max values
                    var setDateFields = function (editFlag) {
                        var startDate;
                        if (editFlag) {
                            startDate = new Date(scope.calendarModelObject.start);
                        } else {
                            startDate = new Date();
                        }
                        if (new Date(scope.calendarModelObject.start) > new Date()) {
                            startDate = new Date();
                        }
                        $("#calendarTaskStartDatePicker").kendoDatePicker({
                            min: startDate,
                            value: new Date(scope.calendarModelObject.start),
                            month: {
                                empty: '<span class="k-state-disabled">#= data.value #</span>'
                            }
                        });
                    }

                    // reset data
                    var resetData = function () {
                        scope.calendarModelObject.title = "";
                        scope.calendarModelObject.Location = "";
                        scope.calendarModelObject.Message = "";
                        scope.calendarModelObject.PersonId = 0;
                        scope.calendarModelObject.AssignedPersonId = 0;
                        scope.calendarModelObject.ReminderMinute = 15;
                        scope.calendarModelObject.AptTypeEnum = 0;
                        scope.calendarModelObject.LeadId = null;
                        scope.calendarModelObject.LeadPhoneNumber = "";
                        scope.calendarModelObject.AccountId = null;
                        scope.calendarModelObject.AccountPhoneNumber = "";
                        scope.calendarModelObject.OpportunityId = null;
                        scope.calendarModelObject.OrderId = null;
                        scope.calendarModelObject.CaseId = null;
                        scope.calendarModelObject.vendorCaseId = null;
                        scope.calendarModelObject.start = "";
                        scope.calendarModelObject.end = "";
                        scope.calendarModelObject.IsTask = false;
                        scope.calendarModelObject.IsPrivate = false;
                        scope.calendarModelObject.IsPrivateAccess = false;
                        scope.calendarModelObject.RecurrenceIsEnabled = false;
                        scope.calendarModelObject.allDay = false;
                        scope.calendarModelObject.AdditionalPeople = [];
                        scope.calendarModelObject.EventRecurring = {
                            PatternEnum: 1,
                            RecursEvery: 1,

                            flagMonthly: 1,

                            DayOfMonth: 1,

                            DayOfWeekIndex: 1,
                            DayOfWeekEnum: 1,

                            MonthEnum: 1,
                            flagYearly: 1,

                            StartDate: "",
                            endBy: 1,
                            EndsAfterXOccurrences: "1",
                            EndDate: "",
                            EndsOn: ""
                        };
                        for (var i = 0; i < scope.dayList.length; i++) {
                            scope.dayList[i]['checked'] = false;
                        }
                        scope.subjectLocationOptionalFlag = false;
                    }


                    // nullify fields on select the option
                    var nullifyField = function (list, scope) {
                        for (var i = 0; i < list.length; i++) {
                            if (list[i]['model']) {
                                scope[list[i]['model']][list[i]['key']] = "";
                            } else {
                                scope[list[i]['key']] = "";
                            }
                        }
                    }

                    var getAppointmentTypeEnum = function () {
                        $http.get('/api/lookup/0/Type_Appointments').then(function (data) {
                            scope.AppointmentTypeEnum = data.data.AppointmentTypeEnum;
                        });
                    }


                    var getReminder = function () {
                        $http.get('/api/lookup/0/Reminders').then(function (data) {
                            scope.Reminder = data.data;
                        });
                    }

                    // edit scenario initial data assignments
                    var initialDataLoad = function (recurrenceSelectionFlag) {
                        scope.singleFlag = false;
                        scope.disableRecurrenceFlag = false;
                        dropdownSelectDetails = calendarModalService.getDropdownDetails();
                        initializeDropdownFields();
                        $('.modal-content').resizable({
                            //alsoResize: ".modal-dialog",
                            minHeight: 320,
                            minWidth: 435
                        });
                        $('.modal-dialog').draggable();
                        $("#calendarModal").modal("show");
                        getInterval();
                        scope.calendarModelObject.IsPrivateAccess = scope.calendarModelObject.IsPrivate;
                        var allowPrivateFlag = false;
                        if (scope.calendarModelObject.Attendees) {
                            for (var i = 0; i < scope.calendarModelObject.Attendees.length; i++) {
                                if (scope.calendarModelObject.Attendees[i]['PersonEmail'] == HFCService.CurrentUser['Email']) {
                                    allowPrivateFlag = true;
                                }
                            }
                        }
                        if (allowPrivateFlag && scope.calendarModelObject.IsPrivate) {
                            scope.calendarModelObject.IsPrivate = false;
                        }

                        // for screen blink issue
                        scope.privateFlag = scope.calendarModelObject.IsPrivate;
                        if (scope.calendarModelObject.EventRecurring) {
                            scope.calendarModelObject.RecurrenceIsEnabled = true;
                            scope.initializeRecurrenceDatePicker();
                            setReccurenceField();
                            scope.setReccuranceValue(scope.calendarModelObject.EventRecurring.PatternEnum, scope.calendarModelObject.EventRecurring.RecursEvery);
                        } else {
                            scope.calendarModelObject.EventRecurring = {};
                            scope.resetReccuranceFields();
                        }
                        // recurrence optimisation selection flag
                        if (recurrenceSelectionFlag && recurrenceSelectionFlag == "single") {
                            scope.calendarModelObject.EventType = 1;
                            scope.disableRecurrenceFlag = true;
                            if (typeof (scope.originalStartDate) == 'string') {
                                scope.calendarModelObject.start = scope.originalStartDate;
                            } else {
                                scope.calendarModelObject.start = (scope.originalStartDate).toLocaleString('en-US');
                            }
                            if (typeof (scope.originalEndDate) == 'string') {
                                scope.calendarModelObject.end = scope.originalEndDate;
                            } else {
                                scope.calendarModelObject.end = (scope.originalEndDate).toLocaleString('en-US');
                            }
                            scope.singleFlag = true;
                            alertModalService.invalidAcceptedFlag = true;
                        } else if (recurrenceSelectionFlag && recurrenceSelectionFlag == "series") {
                            scope.calendarModelObject.EventType = 2;
                            scope.disableRecurrenceFlag = false;
                            alertModalService.invalidAcceptedFlag = true;
                        } else if (scope.calendarModelObject.EventType == 1) {
                            scope.disableRecurrenceFlag = true;
                        }
                        setTimeout(function () {
                            scope.$apply();
                        }, 0);
                        if (scope.calendarModelObject.allDay || scope.calendarModelObject.IsTask) {
                            scope.setDateTimeFields(scope.calendarModelObject.start, !scope.editFlag, true);
                            // scope.setStartEndDateTime(scope.calendarModelObject.start, null, true);
                        } else {
                            scope.setDateTimeFields(scope.calendarModelObject.start, !scope.editFlag, false);
                            // scope.setStartEndDateTime(scope.calendarModelObject.start, null, null, null, null, scope.singleFlag);
                        }
                        scope.appointmentTypeTapped();
                        if (scope.calendarModelObject.IsTask)
                        setDateFields(true);
                    }


                    // set dependent data for selecting the option in dropdown
                    var setDependentDetails = function (detailList) {
                        var keyList = Object.keys(detailList);
                        for (var i = 0; i < keyList.length; i++) {
                            scope.calendarModelObject[keyList[i]] = detailList[keyList[i]];
                        }
                    }

                    // initialize kendo date picker data & events
                    var initializeDatePicker = function () {
                        // set interval for date time picker
                        $("#calendarStartDate").kendoDateTimePicker({
                            interval: 15
                        });
                        $("#calendarEndDate").kendoDateTimePicker({
                            interval: 15
                        });

                        // to handle type event in date picker
                        $("#dateTimePickerWrapper").bind("change", function (event) {
                            if (new Date(scope.calendarModelObject.start) != "Invalid Date") {
                                scope.dateStringFlag = false;
                                var startDateValue = $("#calendarStartDate").data("kendoDateTimePicker").value();
                                if (scope.calendarModelObject.allDayFlag) {
                                    scope.setDateTimeFields(startDateValue, !scope.editFlag, true, true);
                                    // scope.setStartEndDateTime(startDateValue, true, null, null, null, true, true);
                                } else {
                                    scope.setDateTimeFields(scope.calendarModelObject.start, !scope.editFlag, false, true);
                                    // scope.setStartEndDateTime(startDateValue, true, null, null, null, true, false);
                                }
                                // scope.setStartEndDateTime(scope.calendarModelObject.start, true, null, null, null, true);
                                if ($("#calendarEndDate").data("kendoDateTimePicker")) {
                                    $("#calendarEndDate").data("kendoDateTimePicker").value(scope.calendarModelObject.end);
                                    scope.$apply();
                                }
                                if ($("#calendarEndDatePicker").data("kendoDatePicker")) {
                                    $("#calendarEndDatePicker").data("kendoDatePicker").value(scope.calendarModelObject.end);
                                    scope.$apply();
                                }
                            }
                        });
                        $("#datePickerWrapper").bind("change", function (event) {
                            if (new Date(scope.calendarModelObject.start) != "Invalid Date") {
                                scope.setDateTimeFields(scope.calendarModelObject.start, !scope.editFlag, true, true);
                                // scope.setStartEndDateTime(scope.calendarModelObject.start, true, true, null, null, true, true);

                                // setting the values to date pickers
                                if ($("#calendarEndDate").data("kendoDateTimePicker")) {
                                    $("#calendarEndDate").data("kendoDateTimePicker").value(scope.calendarModelObject.end);
                                    scope.$apply();
                                }
                                if ($("#calendarEndDatePicker").data("kendoDatePicker")) {
                                    $("#calendarEndDatePicker").data("kendoDatePicker").value(scope.calendarModelObject.end);
                                    scope.$apply();
                                }
                            }
                        });
                        $("#taskDatePicker").bind("change", function (event) {
                            if (new Date(scope.calendarModelObject.start) != "Invalid Date") {
                                scope.setDateTimeFields(scope.calendarModelObject.start, !scope.editFlag, true, true);
                                // scope.setStartEndDateTime(scope.calendarModelObject.start, true, true, null, null, true);
                                // setting the values to date pickers
                                if ($("#calendarEndDate").data("kendoDateTimePicker")) {
                                    $("#calendarEndDate").data("kendoDateTimePicker").value(scope.calendarModelObject.end);
                                    scope.$apply();
                                }
                                if ($("#calendarEndDatePicker").data("kendoDatePicker")) {
                                    $("#calendarEndDatePicker").data("kendoDatePicker").value(scope.calendarModelObject.end);
                                    scope.$apply();
                                }
                            }
                        });
                        
                    }


                    // intial load trigger method
                    var init = function (title, id, returnText, initData, dateSetterFlag, schedulerFlag, originalStartDate, originalEndDate) {
                        // time zone issue
                        scope.schedulerFlag = schedulerFlag;
                        // set init method to get connected with pop up
                        if (!calendarModalService.scopeFunction) {
                            calendarModalService.setInitFunction(init);
                            calendarModalService.setDependentSetterFunction(setDependentDetails);
                        }
                        if (calendarModalService.popUpFlag) {
                            // resetData();
                            if (id) {
                                // edit scenario
                                scope.editFlag = true;
                                var loadingElement = $("#loading");
                                if (loadingElement) {
                                    loadingElement[0].style.display = "block";
                                }
                                var url = "/api/calendar/?CalendarEdit=true&CalendarId=" + id + "&includeLookup=true"
                                $http.get(url).then(function (data) {
                                    if (title != "Task" && data["data"] && data["data"]["Events"]) {
                                        scope.calendarModelObject = data["data"]["Events"][0];
                                        if (scope.calendarModelObject && scope.calendarModelObject.AccountId) {
                                            scope.calendarModelObject.LeadId = null;
                                        }
                                        //dropdownSelectDetails = calendarModalService.getDropdownDetails();
                                        // initializeDropdownFields();
                                    } else if (title == "Task" && data["data"] && data["data"]["Tasks"]) {
                                        var taskList = data["data"]["Tasks"];
                                        var selectedItem = JSLINQ(taskList).Where(function (item) {
                                            return item["id"] == id
                                        });
                                        scope.calendarModelObject = selectedItem['items'][0];
                                        if (scope.calendarModelObject) {
                                            scope.calendarModelObject['IsTask'] = true;
                                            if (scope.calendarModelObject.AccountId) {
                                                scope.calendarModelObject.LeadId = null;
                                            }
                                            // moved to method
                                            //setDateFields(true);
                                        }
                                    }
                                    if (loadingElement) {
                                        loadingElement[0].style.display = "none";
                                    }

                                    // to optimise the isprivate checkbox
                                    if (scope.calendarModelObject) {
                                        // taking back up of attendees, time, location for email confirmation in edit view
                                        scope.calendarModelObject.locationBackup = scope.calendarModelObject.Location;
                                        scope.calendarModelObject.attendeesBackup = scope.calendarModelObject.AdditionalPeople;
                                        scope.calendarModelObject.allDayBackup = scope.calendarModelObject.allDay;
                                        alertModalService.editchangeEmailFlag = true;
                                        scope.dateStringFlag = false;
                                        if (originalStartDate) {
                                            if (typeof (originalStartDate) == "string") {
                                                scope.calendarModelObject.originalStartDate = originalStartDate;
                                                scope.originalStartDate = originalStartDate;
                                                scope.dateStringFlag = true;
                                            } else {
                                                // scope.calendarModelObject.originalStartDate = calendarModalService.getDateStringValue(originalStartDate, false, true);
                                                scope.calendarModelObject.originalStartDate = calendarModalService.getDateTimeString((originalStartDate).toLocaleString('en-US'), null, false, scope.calendarModelObject.allDayFlag, false, false, scope.calendarModelObject.allDayBackup);
                                                scope.originalStartDate = originalStartDate;
                                                scope.dateStringFlag = false;
                                            }
                                        }
                                        //if (originalEndDate) {
                                        //    if (typeof (originalEndDate) == "string") {
                                                
                                        //        scope.originalEndDate = originalEndDate;
                                        //    } else {
                                                
                                        //        scope.originalEndDate = originalEndDate;
                                        //    }
                                        //}
                                        if (originalEndDate) {
                                            scope.originalEndDate = originalEndDate;
                                        }
                                        if (scope.calendarModelObject.EventRecurring && scope.calendarModelObject.EventType == 2) {
                                            // need to raise alert pop up for getting confirmation from user regarding the single occurrence or series optimisation
                                            alertModalService.setPopUpDetails(true, "Edit Recurring Item", "Do you want to edit only this event occurrence or the whole series?", initialDataLoad, null, null, [{ key: 'single', title: 'Edit Current Occurence' }, { key: 'series', title: 'Edit the series' }]);
                                        } else {
                                            initialDataLoad();
                                        }
                                    }
                                });
                            } else {
                                scope.dateStringFlag = false;
                                scope.singleFlag = false;
                                scope.editFlag = false;
                                scope.calendarModelObject = initData;
                                $('.modal-content').resizable({
                                    //alsoResize: ".modal-dialog",
                                    minHeight: 320,
                                    minWidth: 435
                                });
                                $('.modal-dialog').draggable();
                                $("#calendarModal").modal("show");
                                if (dateSetterFlag) {
                                    scope.setDateTimeFields((scope.calendarModelObject.start).toLocaleString('en-US'), true);
                                    // scope.setStartEndDateTime(scope.calendarModelObject.start, null, null, dateSetterFlag, null, true);
                                } else {
                                    scope.setDateTimeFields(new Date().toLocaleString('en-US'), true);
                                    // scope.setStartEndDateTime(new Date(), null, null, null, null, true);
                                }
                                scope.calendarModelObject.RecurrenceIsEnabled = false;
                                scope.disableRecurrenceFlag = false;
                                if (HFCService.CurrentUser && HFCService.CurrentUser.Domain) {
                                    scope.calendarModelObject.AssignedPersonId = HFCService.CurrentUser['PersonId'];
                                    scope.calendarModelObject.PersonId = HFCService.CurrentUser['PersonId'];
                                    scope.calendarModelObject.AdditionalPeople = [HFCService.CurrentUser['Email']];
                                }
                                // for screen blink issue
                                scope.privateFlag = scope.calendarModelObject.IsPrivate;

                                dropdownSelectDetails = calendarModalService.getDropdownDetails();
                                initializeDropdownFields();
                                setDateFields(false);
                            }
                            // set title & task flag
                            scope.returnText = returnText;
                            scope.title = title;
                            // intial calls
                            getAppointmentTypeEnum();
                            getReminder();
                            initializeDatePicker();
                        }
                    }

                    // intial calls
                    init();

                    //for date validation
                    scope.ValidateDate = function (id, type) {
                        var ValidDateobj = {};
                         var date = $("#" + id + "").val();

                        if (date == "")
                            scope.ValidDate = true;
                        else
                            scope.ValidDate = HFCService.ValidateDate(date, type);

                        if (scope.ValidDatelist.find(item => item.id === id)) {
                            var index = scope.ValidDatelist.findIndex(item => item.id === id);
                            scope.ValidDatelist[index].valid = scope.ValidDate;
                        }
                        else {
                            ValidDateobj["id"] = id;
                            ValidDateobj["valid"] = scope.ValidDate;
                            scope.ValidDatelist.push(ValidDateobj);
                        }

                        if (scope.ValidDate) {
                            scope.Invaliddate = false;
                            $("#" + id + "").removeClass("invalid_Date");
                            var a = $('#' + id + '').siblings();
                            $(a[0]).removeClass("icon_Background");
                            $("#" + id + "").addClass("frm_controllead1");
                        }
                        else {
                            $("#" + id + "").addClass("invalid_Date");
                            var a = $('#' + id + '').siblings();
                            $(a[0]).addClass("icon_Background");
                            $("#" + id + "").removeClass("frm_controllead1");
                        }
                    }
                    scope.ValidateonBlur = function (id, type) {
                        setTimeout(function () {
                            scope.ValidateDate(id, type);
                        }, 50);
                    }
                    //date validation end
                }
            }
        }
])