﻿app.directive('hfcCalendarModal', ['cacheBustSuffix', function (cacheBustSuffix) {
    return {
        restrict: 'E',
        replace: true,
        scope: {},
        controller: 'CalendarController',
        templateUrl: '/templates/NG/Calendar/calendar-modal.html?cache-bust=' + cacheBustSuffix
    }
}]);