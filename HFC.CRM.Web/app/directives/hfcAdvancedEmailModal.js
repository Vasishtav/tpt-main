﻿app.directive('hfcAdvancedEmailModal', ['cacheBustSuffix', function (cacheBustSuffix) {
    return {
        restrict: 'E',
        scope: {},
        templateUrl: '/templates/NG/advanced-email-modal.html?cache-bust=' + cacheBustSuffix,
        replace: true,
        controller: 'AdvancedEmailController'
    }
}])

    .directive('hfcAdvancedEmailAdminModal', ['cacheBustSuffix', function (cacheBustSuffix) {
        return {
            restrict: 'E',
            scope: {},
            templateUrl: '/templates/NG/advanced-email-admin-modal.html?cache-bust=' + cacheBustSuffix,
            replace: true,
            controller: 'AdvancedEmailController'
        }
    }])
    .directive('hfcEmailTemplateAdminModal', ['cacheBustSuffix', function (cacheBustSuffix) {
        return {
            restrict: 'E',
            scope: {},
            templateUrl: '/templates/NG/email-template-admin-modal.html?cache-bust=' + cacheBustSuffix,
            replace: true,
            controller: 'AdvancedEmailController'
        }
    }])
    .directive('myMaxlength', ['$compile', function ($compile) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, elem, attrs, ctrl) {
                attrs.$set("ngTrim", "false");
                var maxlength = parseInt(attrs.myMaxlength, 10);
                ctrl.$parsers.push(function (value) {
                    if (value.length > maxlength) {
                        value = value.substr(0, maxlength);
                        ctrl.$setViewValue(value);
                        ctrl.$render();
                    }
                    return value;
                });
            }
        };
    }]);