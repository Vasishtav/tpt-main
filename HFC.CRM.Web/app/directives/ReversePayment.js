﻿/// <reference path="../views/PrintPage.html" />

'use strict';
app.directive('reversePayment', ['$http', '$window', '$filter',
        function ($http, $window, $filter) {
            return {
                restrict: 'E',
                scope: {
                    orderId: '=',
                    paymentId: '=',
                    controll: '=' // this is way to expose the method callable by the parent controller.
                },
                templateUrl: '/app/views/ReversePayment.html',
                replace: true,
                link: function (scope) {
                    scope.Payment = {};
                    //https://stackoverflow.com/questions/16881478/how-to-call-a-method-defined-in-an-angularjs-directive
                    scope.internalControl = scope.controll || {};

                    scope.GetOrderPaymentMethods = function () {
                        $http.get('/api/orders/0/OrderPaymentMethods').then(function (data) {
                            scope.PaymentMethods = data.data;
                        });
                    }

                    scope.GetOrderPaymentMethods();

                    scope.GetPaymentReversalReason = function () {
                        $http.get('/api/orders/0/OrderReversalReason').then(function (data) {
                            scope.ReversalReason = data.data;
                        });
                    }

                    scope.GetPaymentReversalReason();

                    scope.internalControl.showPaymentModal = function (PaymentID, OrderID) {
                        $http.get('/api/orders/' + PaymentID + '/OrderPaymentsByPaymentId/?orderId=' + OrderID).then(function (response) {
                            scope.Payment = response.data;
                            scope.BalanceAmount = scope.Payment.order.BalanceDue;
                            scope.Payment.PayBalance = false;
                            if (scope.Payment.VerificationCheck != "" || scope.Payment.VerificationCheck != null) {
                                scope.Payment.VerificationCheck = "";
                            }
                            else scope.Payment.VerificationCheck = scope.Payment.VerificationCheck;
                            scope.Payment.PaymentMethod = "";
                            if (scope.Payment.Memo != "" || scope.Payment.Memo != null) {
                                scope.Payment.Memo = "";
                            }
                            else scope.Payment.Memo = scope.Payment.Memo;
                            if (scope.Payment.order.OrderTotal > 0) {
                                if (scope.Payment.ReversalAmount == 0) {
                                    $("#rev_Payment").modal("hide");
                                    HFC.DisplayAlert("Reverse payment is not applicable");
                                    return;
                                }
                                else if (scope.Payment.ReversalAmount > scope.Payment.Amount) {
                                    scope.Payment.Amount = scope.Payment.Amount;
                                    scope.Payment.order.BalanceDue = scope.Payment.order.BalanceDue + scope.Payment.Amount;
                                }
                                else {
                                    scope.Payment.Amount = scope.Payment.ReversalAmount;
                                    scope.Payment.order.BalanceDue = scope.Payment.order.BalanceDue + scope.Payment.Amount;
                                }
                            }
                            //for displaying reversed date and time
                            var date = new Date();
                            var options = {
                                year: "numeric",
                                month: "2-digit",
                                day: "2-digit",
                                hour: "2-digit",
                                minute: "2-digit",
                                second: "2-digit"
                            };
                            date = date.toLocaleString("en", options);
                            date = date.replace(/:\d{2}\s/, ' ');
                            scope.Payment.PaymentDate = date.replace(',', '');
                            scope.Payment.TempPaymentDate = $filter('ignoreTimeZone')(scope.Payment.PaymentDate, 'shortDate');
                            scope.Payment.TempAmount = kendo.toString(scope.Payment.Amount, "c")
                            var obj = $("#rev_Payment");
                            obj.modal("show");
                        });
                    }


                    scope.hidePaymentModal = function () {
                        var obj = $("#rev_Payment");
                        obj.modal("hide");
                    }

                    scope.SaveReversePayment = function (modal) {
                        var myDiv = $('.reversepayment_popup');
                        myDiv[0].scrollTop = 0;
                        var dto = scope.Payment;
                        if (dto.ReasonCode == 0 || dto.ReasonCode == null || dto.ReasonCode == undefined || dto.ReasonCode == '') {
                            HFC.DisplayAlert("Reason required");
                            return;
                        }
                        if (dto.PaymentMethod == 0 || dto.PaymentMethod == null || dto.PaymentMethod == undefined || dto.PaymentMethod == '') {
                            HFC.DisplayAlert("Payment Method required");
                            return;
                        }
                        if (dto.PaymentDate == null || dto.PaymentDate == undefined || dto.PaymentDate == '') {
                            HFC.DisplayAlert("Payment Date required");
                            return;
                        }
                        if (dto.Amount == 0 || dto.Amount == null || dto.Amount == undefined) {
                            HFC.DisplayAlert("Payment Amount required");
                            return;
                        }
                        if (dto.Memo == null || dto.Memo == undefined || dto.Memo == '') {
                            HFC.DisplayAlert("Memo required.");
                            return;
                        }
                        if (dto.PaymentMethod == 2) {
                            if (dto.CheckNumber == "" || dto.CheckNumber == undefined || dto.CheckNumber == null) {
                                HFC.DisplayAlert("Check number required");
                                return;
                            }
                            dto.VerificationCheck = dto.CheckNumber;
                        }
                        else if (dto.PaymentMethod == 3 || dto.PaymentMethod == 4 || dto.PaymentMethod == 5 || dto.PaymentMethod == 6 || dto.PaymentMethod == 7 || dto.PaymentMethod == 8 || dto.PaymentMethod == 9) {
                            if (dto.VerificationCheck == null) {
                                HFC.DisplayAlert("Verification required");
                                return;
                            }
                            dto.VerificationCheck = dto.VerificationCheck;
                        }
                        else if (dto.PaymentMethod == 10) {
                            if (dto.Authorization == null) {
                                HFC.DisplayAlert("Authorization required");
                                return;
                            }
                            dto.VerificationCheck = dto.Authorization;
                        }
                        else {
                            dto.VerificationCheck = '';
                        }
                        var paymentId = scope.Payment.paymentId;
                        $http.put('/api/orders/0/AddPayment', dto).then(function (response) {
                            if (response.config.data.OrderID > 0) {
                                HFC.DisplaySuccess("Reverese Payment Created");
                            }
                            $("#gridpaymentSearch").data("kendoGrid").dataSource.read();
                            $("#rev_Payment").modal("hide");
                        }, function (response) {
                            HFC.DisplayAlert(response.statusText);
                        });
                    }

                }
            }
        }

]);

