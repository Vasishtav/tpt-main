﻿
(function (window, document, undefined) {
    'use strict';
    angular.module('hfc.commentsModule', [])
.directive('commentsComponent', [
        'CommentsService', function (CommentsService) {
            return {
                restrict: 'E',
                scope: {
                    fromroot: '=info'
                },
                templateUrl: '/templates/NG/Comments.html',
                link: function (scope) {
                     
                    scope.CommentsService = CommentsService;
                    scope.CommentsService.module = scope.fromroot.module;
                    scope.CommentsService.id = scope.fromroot.id;
                   // scope.CommentsService.RefId = scope.fromroot.RefId; // refid for Vendor view of Franchise will be vendorid
                   // scope.CommentsService.ModuleId = scope.fromroot.ModuleId;
                    scope.CommentsService.loadInitialComments();
                   // scope.PersonService = PersonService;
                    scope.CommentsService.loadUserlist();
                    scope.CommentsService.FindFranchiseTimeZone();
                    scope.CommentsService.initEventHandler("#Comment");
                }
            }
        }
])
.service('CommentsService', ['HFCService', '$http', '$q', '$rootScope', '$sce', '$timeout', function (HFCService, $http, $q, $rootScope, $sce, $timeout) {
   var srv = this;
    srv.isNewComment = false;
    srv.module ="";
    srv.id = "";    
    srv.htmlReplyComment = "";
    srv.htmlMainComment = "";
    srv.comments = [];
    srv.commentIdd = 1;
    srv.CaseId = "";
    srv.ModuleId = "";

    srv.FindFranchiseTimeZone = function () {
        $http.get('/api/comments/' + srv.id + '/FindFranchiseTimeZone').then(function (response) {
            srv.TimeZone = response.data;
        });
    }


    // event handler initialize
    srv.initEventHandler = function (id) {
        var backupPosition;
        $(document).ready(function () {
            setTimeout(function () {
                var editor = $(id).data("kendoEditor");
                srv.initFlag = true;
                editor.bind("select", function (e) {
                    var editor = $(id).data("kendoEditor");
                    var element = $(".k-widget.k-window.k-window-titleless.k-editor-widget");
                    var currentWidgetHeight = $(element).height();
                    var topPosition = element[0].offsetTop;
                    var difference = currentWidgetHeight - srv.initialWidgetHeight;
                    if (difference > 0 && backupPosition != topPosition) {
                        topPosition = topPosition - difference;
                        backupPosition = topPosition;
                        $(element).css("top", topPosition);
                    }
                });
                if (srv.initFlag) {
                    $(".k-i-list-unordered").click(function (e) {
                        var element = $(id);
                        setTimeout(function () {
                            var list = $(element).find("ul");
                            $(list).addClass("unordered-list");
                        }, 10);
                    });
                    srv.initFlag = false;
                }
            }, 500);
            $(id).keyup(function (e) {
                var queryElement = $(".atwho-query");

                if (e.keyCode == 13) {
                    if (queryElement && queryElement.length != 0) {
                        e.preventDefault();
                        var childList = $(id).children();
                        for (var i = 0; i < childList.length; i++) {
                            var textValue = $(childList[i]).text();
                            var duplicateText = textValue.replace(/\s/g, '');
                            if (duplicateText && duplicateText.length != 0) {
                                var flag = textValue.includes("@");
                                if (flag) {
                                    var children = $(childList[i]).children();
                                    var nameList = [];
                                    for (var j = 0; j < children.length; j++) {
                                        if ($(children[j]).hasClass("atwho-inserted")) {
                                            nameList.push($(children[j]).text());
                                        }
                                    }
                                    var currentText = $(childList[i]).text();
                                    var updatedText = currentText.substr(0, currentText.length - 1);
                                    var totalCount = updatedText.length;
                                    var composedText = "";
                                    for (var k = 0; k < nameList.length; k++) {
                                        if (nameList[k] != "@") {
                                            var index = updatedText.indexOf(nameList[k]);
                                            composedText += updatedText.substr(0, index);
                                            composedText += '<span class="atwho-inserted" data-atwho-at-query="@" contenteditable="false">' + nameList[k] + '</span> ';
                                            var updatedCount = index + nameList[k].length + 1;
                                            var endCount = totalCount - updatedCount;
                                            updatedText = updatedText.substr(updatedCount, endCount);
                                        }
                                    }
                                    if (updatedCount != totalCount) {
                                        composedText += updatedText;
                                    }
                                    $(childList[i]).html(composedText);
                                    $(childList[i]).append('<span class="atwho-inserted" data-atwho-at-query="@" contenteditable="false">' + srv.selectedName + '</span>');
                                }
                                var editor = $(id).data("kendoEditor");
                                editor.focus();
                            } else {
                                $(childList[i]).remove();
                            }
                        }
                    }
                }
            });
            $(id).on("inserted.atwho", function (event, data, e) {
                var selectedData = $(data).text();
                var nameLength = selectedData.indexOf(" ");
                srv.selectedName = selectedData.substr(0, nameLength);
            });
        });
    }

    srv.loadUserlist = function () {
        $http.get('/api/comments/' + srv.id + '/loadUserlist?module=' + srv.module).then(function (response) {
            srv.Names = response.data;
        });
    }

    // srv.loadUserlist();

    srv.loadInitialComments = function ()   //ok
    {
        if (srv.id)
            $http.get('/api/Comments/' + srv.id + '/loadInitialComments?module=' + srv.module).then(function (response) {
            
                srv.comments = response.data;
                // console.log(srv.comments);
            });
    }



    srv.bringTextEditor = function () //ok
    {
        srv.isNewComment = true;
        srv.htmlMainComment = "";
        angular.forEach(srv.comments, function (value, key) {
            value.isReply = false;
            value.isEdit = false;
        });


        $('#Comment').atwho({
            at: "@",
            displayTpl: '<li>${name} <small>${content}</small></li>',
            insertTpl: '${name}',
            data: srv.Names
        });

        var div = document.getElementById('Comment');
        setTimeout(function () {
            div.focus();
            var element = $(".k-widget.k-window.k-window-titleless.k-editor-widget");
            srv.initialWidgetHeight = $(element).height();
        }, 0);
    }
    srv.disableMainTextEditor = function ()  //ok
    {

        srv.isNewComment = false;
        srv.htmlMainComment = "";
    }

    srv.enableReply = function (id, index) {    //ok

        angular.forEach(srv.comments, function (value, key) {
            if (value.id != id) {
                value.isReply = false;
                value.isEdit = false;
            }

            if (value.id == id) {
                value.isReply = true;
                return;
            }
        });
        srv.htmlReplyComment = "";
        srv.isNewComment = false;
        srv.htmlMainComment = "";

        var iddd = '#CommentReply' + id;

        $(iddd).atwho({
            at: "@",
            displayTpl: '<li>${name} <small>${content}</small></li>',
            insertTpl: '${name}',
            data: srv.Names
        });
        srv.initEventHandler(iddd);
    }

    srv.cancelReply = function (id, index) {    //ok
        angular.forEach(srv.comments, function (value, key) {
            if (value.id == id) {
                value.isReply = false;
                // value.haschildId = true;               
            }
        });
        srv.htmlReplyComment = "";
    }
    srv.saveReply = function (id, index) {   //ok
        if (srv.htmlReplyComment) {

            var re = $("<div>").html(srv.htmlReplyComment).text();
            re = re.replace(/\s/g, "");
            if (re.length > 0) {
            //srv.currentUserid = HFCService.CurrentUser.PersonId;
            //srv.currrentUserName = HFCService.CurrentUser.FullName;

            srv.qwerty = {
                id: 0,
                module: srv.module,
                moduleId: srv.id,
                userId: srv.currentUserid,
                parentId: id,
                haschildId: false,
                userImageUrl: null,
                userName: srv.currrentUserName,
                // comment: 'Hey! What are you doing buddy...The HollyFrontier Corporation is a Fortune 500 company based in Dallas, TX. HollyFrontier is a petroleum refiner and distributor of petroleum products, from gasoline to petroleum-based lubricants and waxes. George Damaris currently holds the title as CEO',
                htmlComment: srv.htmlReplyComment,
                htmlCurrentComment: null,
                // htmlReplyComment:'',
                isReply: false,
                isEdit: false,
                createdAt: null,
                updatedAt: null,
                deletedAt: null
            }

            //  srv.comments.push(srv.qwerty);

            $("#commmentsHide").html(srv.qwerty.htmlComment);

            var valueArray = $('#commmentsHide .atwho-inserted').map(function () {
                this.attributes["data-atwho-at-query"];
                return this.textContent;
            }).get();
            var unique = [];
            unique = valueArray.filter(onlyUnique);
            var uniquee = [];
            for (var i = 0 ; i < unique.length ; i++) {
                for (var l = 0; l < srv.Names.length ; l++) {
                    if (srv.Names[l].name == unique[i]) {
                        uniquee.push(srv.Names[l].content);
                    }
                }

            }

            $http.post('/api/Comments/0/SaveReplyComment?unique=' + uniquee + '&module=' + srv.module + '&moduleId=' + srv.id, srv.qwerty).then(function (response) {               
                //  srv.comments = response.data;
                srv.comments.push(response.data);
                unique = [];
                uniquee = [];
                //  var res = response;
            });

            // srv.comments.push(srv.qwerty);
            // srv.commentIdd = srv.commentIdd + 1;
            angular.forEach(srv.comments, function (value, key) {
                if (value.id == id) {
                    value.isReply = false;
                    value.haschildId = true;
                    return;
                }
            });
        }

        }
    }

    srv.editComment = function (id, index)  // ok
    {
        angular.forEach(srv.comments, function (value, key) {
            if (value.id != id) {
                value.htmlCurrentComment = value.htmlComment; //.replace('<br class="k-br">', "");
                value.isEdit = false;
                value.isReply = false;
            }
            if (value.id == id) {
                value.htmlCurrentComment = value.htmlComment; //.replace('<br class="k-br">', "");
                value.isEdit = true;
                value.isReply = false;
                return;
            }
        });
        srv.isNewComment = false;
        srv.htmlMainComment = "";
        var idd = '#CommentEdit' + id;

        $(idd).atwho({
            at: "@",
            displayTpl: '<li>${name} <small>${content}</small></li>',
            insertTpl: '${name}',
            data: srv.Names
        });
        srv.initEventHandler(idd);
    }

    srv.cancelUpdate = function (id, index)   //ok
    {
        angular.forEach(srv.comments, function (value, key) {
            if (value.id == id) {
                value.htmlCurrentComment = value.htmlComment;
                value.isEdit = false;
                return;
            }
        });
    }

    srv.updateComment = function (id, index) {  // ok
         

     
        angular.forEach(srv.comments, function (value, key) {
            if (value.id == id) {
                if (value.htmlComment != value.htmlCurrentComment && value.htmlCurrentComment) {
                    var re = $("<div>").html(value.htmlCurrentComment).text();
                    re = re.replace(/\s/g, "");
                    if (re.length > 0)
                     {
                        $("#commmentsHide").html(value.htmlCurrentComment);

                    var valueArray = $('#commmentsHide .atwho-inserted').map(function () {
                        this.attributes["data-atwho-at-query"];
                        return this.textContent;
                    }).get();
                    var unique = [];
                    unique = valueArray.filter(onlyUnique);
                    var uniquee = [];
                    for (var i = 0 ; i < unique.length ; i++) {
                        for (var l = 0; l < srv.Names.length ; l++) {
                            if (srv.Names[l].name == unique[i]) {
                                uniquee.push(srv.Names[l].content);
                            }
                        }

                    }
                    $http.post('/api/Comments/' + id + '/updateParentComment?unique=' + uniquee + '&module=' + srv.module+'&moduleId='+srv.id, value).then(function (response) {
                        value.htmlComment = value.htmlCurrentComment;
                        value.isEdit = false;
                        value.updatedAt = response.data;
                        unique = [];
                        uniquee = [];
                        //srv.comments = srv.comments;
                        return;
                        // srv.comments = response.data;                        
                    });
                }

                }
                else {
                   // value.isEdit = false;
                    return;
                }
            }
        });

    }


    srv.deleteComment = function (id, parentId) {
        $http.post('/api/Comments/' + id + '/deleteComment?parentId=' + parentId).then(function (response) {
            srv.count = 0;
            angular.forEach(srv.comments, function (value) {
                if (value.id == id) {

                    var check = false;
                    angular.forEach(srv.comments, function (value1) {
                        if (value1.parentId == value.parentId && value1.id != id)
                            check = true;
                    });

                    if (check == false) {
                        angular.forEach(srv.comments, function (value2) {
                            if (value2.id == value.parentId) {
                                value2.haschildId = false;
                            }
                        });

                    }
                    srv.comments.splice(srv.count, 1);

                    return;
                }
                srv.count = srv.count + 1;
            });
        });


    }

    function onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
    }

    srv.SaveMainComment = function (belongsTo) {  // ok
        if (srv.htmlMainComment) {

            var re = $("<div>").html(srv.htmlMainComment).text();
            re = re.replace(/\s/g, "");
            if (re.length > 0) {
            srv.qwerty = {
                id: 0,
                module: srv.module,
                moduleId: srv.id,
                belongsTo: belongsTo,
                userId: 0,
                parentId: 0,
                haschildId: false,
                userImageUrl: null,
                userName: null,
                htmlComment: srv.htmlMainComment,
                htmlCurrentComment: null,
                isReply: false,
                isEdit: false,
                createdAt: null,
                updatedAt: null,
                deletedAt: null
            }

            $("#commmentsHide").html(srv.htmlMainComment);

            var valueArray = $('#commmentsHide .atwho-inserted').map(function () {
                this.attributes["data-atwho-at-query"];
                return this.textContent;
            }).get();
            var unique = [];
            unique = valueArray.filter(onlyUnique);
            var uniquee = [];
            for (var i = 0 ; i < unique.length ; i++) {
                for (var l = 0; l < srv.Names.length ; l++) {
                    if (srv.Names[l].name == unique[i]) {
                        uniquee.push(srv.Names[l].content);
                    }
                }

            }

            $http.post('/api/Comments/0/SaveParentComment?unique=' + uniquee + '&module=' + srv.module+'&moduleId='+srv.id, srv.qwerty).then(function (response) {

                srv.comments.push(response.data);
                // srv.comments = response.data;
                //  var res = response;
                unique = [];
                uniquee = [];
            });

            //  $http.post()
            //srv.comments.push(srv.qwerty);
            //srv.commentIdd = srv.commentIdd + 1;
            srv.htmlMainComment = "";
            srv.isNewComment = false;
        }
        }

    }

    srv.currentUserid = 0;
    srv.currrentUserName = '';
    //var promise2 = new Promise(function (resolve, reject) {
    //    resolve({ id: HFCService.CurrentUser.PersonId, name: HFCService.CurrentUser.FullName });

    //});

    //promise2.then(function (value) {
    //    srv.currentUserid = value.id;
    //    srv.currrentUserName = value.name;


    //});

    $http.get('/api/comments/0/getUseridAndName').then(function (response) {
        srv.currentUserid = response.data.id;
        srv.currrentUserName = response.data.name;
    });


}]);
})(window, document);