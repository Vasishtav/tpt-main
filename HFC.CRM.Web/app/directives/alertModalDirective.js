﻿app.directive('alertModalDirective', ['$modal', 'alertModalService', function ($modal, alertModalService) {
    return {
        restrict: 'E',
        scope: {
            modalId: '@'
        },
        templateUrl: '/templates/NG/alertModalTemplate.html',
        replace: true,
        link: function (scope) {
            scope.recurrenceButtonList = [];

            // clsoe method to close pop up
            scope.close = function (id, flag, validateFlag) {
                $("#alertModalBody")[0].scrollTop = 0;
                alertModalService.IsBusy = false;
                $("#" + id).modal("hide");
                $("html").removeClass("scroll-hidden");
                $("body").removeClass("scroll-hidden");
                alertModalService.invalidAcceptedFlag = true;
                if (flag) {
                    alertModalService.editchangeEmailFlag = true;
                }
                var url = window.location.href;
                if (url.includes("/Calendar") && validateFlag != 'noReload' && alertModalService.bindFunctionFlag) {
                    alertModalService.controlFunction(null, null, null, 'triggerReload');
                }
            }

            // continue event handler to trigger the flow

            scope.continue = function (flag, emailFlag) {
                scope.close("alertModal", null, 'noReload');
                alertModalService.invalidAcceptedFlag = false;
                alertModalService.IsBusy = true;
                if (flag && !alertModalService.param) {
                    alertModalService.controlFunction(flag);
                } else if (flag && Array.isArray(alertModalService.param)) {
                    alertModalService.controlFunction(alertModalService.param[0], alertModalService.param[1], flag);
                } else {
                    if (Array.isArray(alertModalService.param)) {
                        alertModalService.controlFunction(alertModalService.param[0], alertModalService.param[1]);
                    } else {
                        alertModalService.controlFunction(alertModalService.param);
                    }
                }
                if (!emailFlag) {
                    //$("html").addClass("scroll-hidden");
                    //$("body").addClass("scroll-hidden");
                } else {
                    $("html").removeClass("scroll-hidden");
                    $("body").removeClass("scroll-hidden");
                }
            }

            var bindFunction = function () {
                if (scope.recurrenceButtonList && scope.recurrenceButtonList.length != 0) {
                    for (var i = 0; i < scope.recurrenceButtonList.length; i++) {
                        $('#' + scope.recurrenceButtonList[i]['key']).click(function (e) {
                            scope.continue(e.target.id);
                        });
                    }
                } else {
                    $('#continue').click(function () {
                        scope.continue();
                    });
                }

                $("#emailContinue").click(function () {
                    alertModalService.emailSendFlag = true;
                    scope.continue(null, true);
                })

                $('#cancel').click(function () {
                    scope.close(scope.modalId);
                });
                $("#emailNo").click(function () {
                    alertModalService.emailSendFlag = false;
                    scope.continue(null, true);
                });
            }

            var setDetails = function (flag, title, body, isBusy, recurrenceButtonList, emailFlag, deleteFlag) {
                if (flag) {
                    scope.title = title;
                    scope.bodyText = body;
                    scope.isBusy = isBusy;
                    scope.recurrenceButtonList = recurrenceButtonList;
                    var id = "#" + scope.modalId;
                    var bodyTemplate = "<h4>" + scope.bodyText + "</h4>";
                    $("#alertModalBody").html(bodyTemplate);
                    var footerTemplate = '';
                    if (scope.recurrenceButtonList && scope.recurrenceButtonList.length != 0) {
                        for (var i = 0; i < scope.recurrenceButtonList.length; i++) {
                            footerTemplate += '<button type="submit" class="btn btn-primary tery_sm1" id="' + scope.recurrenceButtonList[i]['key'] + '">' + scope.recurrenceButtonList[i]['title'] + '</button>';
                        }
                    } else {
                        if (emailFlag || deleteFlag) {
                            footerTemplate += '<button type="button" class="btn btn-primary tery_sm1" id="emailContinue">Yes</button>';
                        } else {
                            footerTemplate += '<button type="button" class="btn btn-primary tery_sm1" id="continue">Ok</button>';
                        }
                    }
                    if (emailFlag) {
                        footerTemplate += '<button type="submit"  class="btn btn-primary tery_sm1" id="emailNo">No</button>';
                    } else if (deleteFlag) {
                        footerTemplate += '<button type="submit"  class="btn btn-primary tery_sm1" id="cancel">No</button>';
                    } else {
                        footerTemplate += '<button type="submit"  class="btn btn-primary tery_sm1" id="cancel">Cancel</button>';
                    }
                    $("#alertModalFooter").html(footerTemplate);
                    $("#alertModal").modal("show");
                    bindFunction();
                   // $("html").addClass("scroll-hidden");
                   // $("body").addClass("scroll-hidden");
                }
            }

            var init = function () {
                alertModalService.setScopeFunction(setDetails);
                alertModalService.IsBusy = false;
            }

            init();
        }
    }
}
])