﻿
'use strict';
app.directive('hfcOpportunityModal', [
       'OpportunityService', function (OpportunityService) {
           return {
               restrict: 'E',
               scope: {
                   modalId: '@'
               },
               templateUrl: '/templates/NG/Opportunity/Opportunity-modal.html',
               replace: true,
               link: function (scope) {
                   scope.OpportunityService = OpportunityService;
               }
           }
       }
])
   .directive('hfcOpportunityForm', [
       function () {
           return {
               restrict: 'E',
               scope: true,
               templateUrl: '/templates/NG/Opportunity/Opportunity-edit-form.html',
               replace: true,
               
           }
       }
   ])