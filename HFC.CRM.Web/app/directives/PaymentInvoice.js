﻿
'use strict';
app.directive('hfcPaymentInvoiceModal', [
       'PaymentInvoiceService', function (PaymentInvoiceService) {
           return {
               restrict: 'E',
               scope: {
                   modalId: '@'
               },
               templateUrl: '/templates/NG/Payment/Payment-Invoice-modal.html',
               replace: true,
               link: function (scope) {
                   scope.PaymentInvoiceService = PaymentInvoiceService;
               }
           }
       }
])
   .directive('hfcPaymentInvoiceForm', [
       function () {
           return {
               restrict: 'E',
               scope: true,
               templateUrl: '/templates/NG/Payment/Payment-Invoice-edit-form.html',
               replace: true,
               
           }
       }
   ])