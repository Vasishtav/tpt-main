﻿app.directive('hfcEmailModal', ['cacheBustSuffix', function (cacheBustSuffix) {
    return {
        restrict: 'E',
        scope: {},
        templateUrl: '/templates/NG/email-modal.html?cache-bust=' + cacheBustSuffix,
        replace: true,
        controller: 'EmailController'
    }
}]);