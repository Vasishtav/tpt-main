﻿'use strict';

app.directive('hfcLeadSource', ['LeadSourceService', 'cacheBustSuffix', function (LeadSourceService, cacheBustSuffix) {
    return {
        restrict: 'EA',
        scope: {
            leadId: '@',
            heading: '@'
        },
        templateUrl: '/templates/NG/Lead/lead-source.html?cache-bust=' + cacheBustSuffix,
        replace: true,
        link: function (scope, element, attrs) {
            scope.LeadSourceService = LeadSourceService;


            // Setup configuration parameters
            scope.heading = scope.heading || "Lead Sources";
            if (attrs.canUpdate)
                scope.canUpdate = attrs.canUpdate === 'true';
            if (attrs.canDelete)
                scope.canDelete = attrs.canDelete === 'true';
            if (attrs.canCreate)
                scope.canCreate = attrs.canCreate === 'true';
        }
    };
}]);