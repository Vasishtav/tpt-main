﻿app
.directive('hfcResurfacingProduct', [
'ResurfacingProductService', function (ResurfacingProduct) {
    return {
        restrict: 'E',
        scope: {
            modalId: '@'
        },
        templateUrl: '/app/views/product/ResurfacingProduct.html',
        replace: true,
        link: function (scope, element) {
            scope.ResurfacingProduct = ResurfacingProduct;
            angular.element(document).ready(function () {
                if (scope.BrandId == 2) {
                    $('#resurfacingProductforFlooring').on('shown.bs.modal', function () {
                    });
                } if (scope.BrandId == 3) {
                    $('#resurfacingProductforConcrete').on('shown.bs.modal', function () {
                    });
                }

            });
        }
    }
}
])
.service('ResurfacingProductService', [
'$http', '$window', '$location', 'HFCService',
'kendoService', 'kendotooltipService',
function ($http, $window, $location, HFCService,
kendoService, kendotooltipService) {
    var scope = this;
    scope.HFCService = HFCService;
    scope.BrandId = scope.HFCService.CurrentBrand;
    scope.Type = '';
    scope.req_validation = false;
    var successCallback = null;
    var digestCallback = null;

    scope.ShowConfigurator = function () {
        if (scope.BrandId == 2) {
            scope.ShowPopup('resurfacingProductforFlooring');
        } if (scope.BrandId == 3) {
            scope.ShowPopup('resurfacingProductforConcrete');
        }
    }

    scope.ShowPopup = function (modalId) {
        $("#" + modalId).modal("show");
    }

    //cancel the pop-up
    scope.CancelPopup = function (modalId) {
        $("#" + modalId).modal("hide");
        scope.Type = '';
        scope.req_validation = false;
    };

    scope.LoadCCPopup = function (type, modal) {
        if (type) {
            scope.req_validation = false;
            scope.CancelPopup(modal);
            if (type == 1) {
                scope.ShowPopup('LoadCCAllProduct');
            } else if (type == 2) {
                scope.ShowPopup('LoadCCNewProduct');
            } else if (type == 3) {
                scope.ShowPopup('LoadCCUpdateProduct');
            }
        } else {
            scope.req_validation = true;
            return false;
        }
    };

    scope.LoadTLPopup = function (type, modal) {
        if (type) {
            scope.req_validation = false;
            scope.CancelPopup(modal);
            if (type == 1) {
                scope.ShowPopup('LoadTLAllProduct');
            } else if (type == 2) {
                scope.ShowPopup('LoadTLNewProduct');
            } else if (type == 3) {
                scope.ShowPopup('LoadTLUpdateProduct');
            }
        } else {
            scope.req_validation = true;
            return false;
        }
    };

    scope.ImportProducts = function (option) {
        if (option == "AllProduct") {
            $http.get('/api/ProductTP/1/RefreshProductsFromHFCList').then(function (response) {
                if (response.data.length) {
                    var data = response.data;
                    var grid = $("#gridProductSearch").data("kendoGrid");
                    var dataSource = grid.dataSource;
                    dataSource._data = data;
                    grid.dataSource.data(data);
                    scope.CancelPopup('LoadTLAllProduct');
                    scope.CancelPopup('LoadCCAllProduct');
                } else {
                    HFC.DisplayAlert("There is no product in the HFC Master list for your vendors.");
                }
            });
        } else if (option == "NewProducts") {
            $http.get('/api/ProductTP/2/RefreshProductsFromHFCList').then(function (response) {
                if (response.data.length) {
                    var data = response.data;
                    var grid = $("#gridProductSearch").data("kendoGrid");
                    var dataSource = grid.dataSource;
                    dataSource._data = data;
                    grid.dataSource.data(data);
                    scope.CancelPopup('LoadCCNewProduct');
                    scope.CancelPopup('LoadTLNewProduct');
                } else {
                    HFC.DisplayAlert("There is no product in the HFC Master list for your vendors.");
                }
            });
        } else if (option == "UpdateVendorCost") {
            $http.get('/api/ProductTP/3/RefreshProductsFromHFCList').then(function (response) {
                if (response.data.length) {
                    var data = response.data;
                    var grid = $("#gridProductSearch").data("kendoGrid");
                    var dataSource = grid.dataSource;
                    dataSource._data = data;
                    grid.dataSource.data(data);
                    scope.CancelPopup('LoadCCUpdateProduct');
                    scope.CancelPopup('LoadTLUpdateProduct');
                } else {
                    HFC.DisplayAlert("There is no product in the HFC Master list for your vendors.");
                }
            });
        }
    };

    scope.SaveFeedbackData = function () {
        var dto = scope.Configurator;
        $http.post('/api/Case', dto).then(function (response) {
            if (response.data != null) {
                var result = response.data.result;
                scope.Configurator.CaseNumber = result.CaseNumber;
                scope.CancelPopup('__configuratorFeedbackModal__');
                scope.ShowPopup('configuratorFeedbackConfirmModal');
            }
        });
    };

}]);