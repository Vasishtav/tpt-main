﻿
'use strict';

app.directive('hfcNewtaskModal', [
       'NewtaskService', 'HFCService', function (NewtaskService, HFCService) {
           return {
               restrict: 'E',
               scope: {
                   modalId: '@'
               },
               templateUrl: '/templates/NG/NewTask/newtask-modal.html',
               replace: true,
               link: function (scope) {
                   scope.NewtaskService = NewtaskService;
                   scope.HFCService = HFCService;
               }
           }
       }
])
   .directive('hfcNewtaskForm', [
       function () {
           return {
               restrict: 'E',
               scope: true,
               templateUrl: '/templates/NG/NewTask/newtask-edit-form.html',
               replace: true,
               
           }
       }
   ])