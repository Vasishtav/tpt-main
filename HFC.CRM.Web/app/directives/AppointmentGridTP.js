﻿
'use strict';
app.directive('hfcAppointmentEventList', [
        'CalendarServiceTP', 'CalendarService', '$http', 'calendarModalService', function (CalendarServiceTP, CalendarService, $http, calendarModalService) {
            return {
                restrict: 'E',
                scope: {
                    modalId: '@',
                    leadId: '@',
                    accountId: '@',
                    opportunityId: '@',
                    associatedWith: '@',
                    isEdit: '@',
                    accessPermission: '=',
                },
                templateUrl: '/templates/NG/LinkedList/Hfc-appointment-event-list.html',
                replace: true,
                link: function (scope) {
                    scope.isEditt = true;
                    scope.calendarModalService = calendarModalService;
                    scope.calendarServiceTP = CalendarServiceTP;
                    scope.CalendarService = CalendarService;
                    scope.eventList = {};
                    scope.errorMessage = "";

                    if (scope.isEdit === "true")
                        scope.isEditt = true;
                    else
                        scope.isEditt = false;



                    // goto calendar page using edit from calendar
                    scope.openCalendar = function (event) {
                        scope.calendarModalService.setCalendarDataBackup(event);
                        // carries the trigger page flow
                        window.location.href = scope['returnPath'];
                    }


                    scope.populateLeadEvents = function () {
                        CalendarServiceTP.getLeadEvents(scope.leadId)
                        .then(function (response) {
                            var tempData = response.data;
                            //for (var i = 0; i < tempData.length; i++) {
                            //    tempData[i]['originalStartDate'] = calendarModalService.getDateStringValue(new Date(tempData[i]['StartDate']), true, true);
                            //    tempData[i]['originalEndDate'] = calendarModalService.getDateStringValue(new Date(tempData[i]['EndDate']), true, true);
                            //}
                            scope['returnPath'] = "/#!/Calendar/leadAppointments/" + scope.leadId;
                            scope.eventList = tempData;
                        },
                        function (responseError) {
                            scope.errorMessage = responseError.statusText;
                        });
                    }

                    scope.populateAccountEvents = function () {
                        CalendarServiceTP.getAccountEvents(scope.accountId)
                        .then(function (response) {
                            var tempData = response.data;
                            //for (var i = 0; i < tempData.length; i++) {
                            //    tempData[i]['originalStartDate'] = calendarModalService.getDateStringValue(new Date(tempData[i]['StartDate']), true, true);
                            //    tempData[i]['originalEndDate'] = calendarModalService.getDateStringValue(new Date(tempData[i]['EndDate']), true, true);
                            //}
                            scope['returnPath'] = "#!/Calendar/accountAppointments/" + scope.accountId;
                            scope.eventList = tempData;
                            //scope.eventList = response.data;
                        },
                        function (responseError) {
                            scope.errorMessage = responseError.statusText;
                        });
                    }

                    scope.populateOpportunityEvents = function () {
                        CalendarServiceTP.getOpportunityEvents(scope.opportunityId)
                        .then(function (response) {
                            var tempData = response.data;
                            //for (var i = 0; i < tempData.length; i++) {
                            //    tempData[i]['originalStartDate'] = calendarModalService.getDateStringValue(new Date(tempData[i]['StartDate']), true, true);
                            //    tempData[i]['originalEndDate'] = calendarModalService.getDateStringValue(new Date(tempData[i]['EndDate']), true, true);
                            //}
                            scope['returnPath'] = "#!/Calendar/opportunityAppointments/" + scope.opportunityId;
                            scope.eventList = tempData;
                            //scope.eventList = response.data;
                        },
                        function (responseError) {
                            scope.errorMessage = responseError.statusText;
                        });
                    }

                    function populateEventGrid() {
                        // While loading the component populate the grid based on the associated component
                        switch (scope.associatedWith) {
                            case "Account":
                                scope.populateAccountEvents();
                                break;
                            case "Opportunity":
                                scope.populateOpportunityEvents();
                                break;
                            default:
                                scope.associatedWith = "Lead";
                                scope.populateLeadEvents();
                        }
                    }

                    populateEventGrid();
                    

                    //CalendarService.SuccessCallback = function (params) {
                    //    populateEventGrid();
                    //}
                    
                    scope.eventSuccessCallback = function (param) {
                        //
                        populateEventGrid();
                    }

                    scope.editEvent = function (modalId, EventId) {
                        $("#_calendarModal_").modal("hide");
                        var editTask = CalendarService.GetEventsFromScheduler(modalId, EventId, scope.eventSuccessCallback);
                        //$scope.$apply();
                    }


                    scope.eventReminderEvent= function (TaskId) {
                        var Id = TaskId;
                        var reminderurl = '/api/Tasks/' + Id + '/EventReminder';
                        $http.put(reminderurl).then(function (response) {
                            var res = response.data;
                            if (res) {
                                HFC.DisplaySuccess("Success");
                            }
                            else {
                                HFC.DisplayAlert("Error");
                            }
                        });

                    }

                    //https://rclayton.silvrback.com/pubsub-controller-communication
                    // Sample subscriber to update the grid after editing a calender
                    // or newly added calendar.
                    scope.$on("ping", function (e, time) {
                        var x = time;
                    });
                }
            }
        }
]);


app.directive('hfcAppointmentTaskList', [
        'CalendarServiceTP', 'CalendarService', '$http', 'calendarModalService', function (CalendarServiceTP, CalendarService, $http, calendarModalService) {
            return {
                restrict: 'E',
                scope: {
                  
                    leadId: '@',
                    accountId: '@',
                    opportunityId: '@',
                    associatedWith: '@',
                    isEdit: '@'
                },
                templateUrl: '/templates/NG/LinkedList/Hfc-appointment-task-list.html',
                replace: true,
                link: function (scope) {
                    scope.isEditt = true;
                    scope.calendarModalService = calendarModalService;
                    scope.calendarServiceTP = CalendarServiceTP;
                    scope.CalendarService = CalendarService;
                    scope.taskList = {};
                    scope.errorMessage = "";
                    if (scope.isEdit === "true")
                        scope.isEditt = true;
                    else
                        scope.isEditt = false;

                    scope.populateLeadTasks = function () {
                        CalendarServiceTP.getLeadTasks(scope.leadId)
                        .then(function (response) {
                            scope.taskList = response.data;
                        },
                        function (responseError) {
                            scope.errorMessage = responseError.statusText;
                        });
                    }

                    scope.populateAccountTasks = function () {
                        CalendarServiceTP.getAccountTasks(scope.accountId)
                        .then(function (response) {
                            scope.taskList = response.data;
                        },
                        function (responseError) {
                            scope.errorMessage = responseError.statusText;
                        });
                    }

                    scope.populateOpportunityTasks = function () {
                        CalendarServiceTP.getOpportunityTasks(scope.opportunityId)
                        .then(function (response) {
                            scope.taskList = response.data;
                        },
                        function (responseError) {
                            scope.errorMessage = responseError.statusText;
                        });
                    }

                    function populateGrid() {
                        // While loading the component populate the grid based on the associated component
                        switch (scope.associatedWith) {
                            case "Account":
                                scope.populateAccountTasks();
                                break;
                            case "Opportunity":
                                scope.populateOpportunityTasks();
                                break;
                            default:
                                scope.associatedWith = "Lead";
                                scope.populateLeadTasks();
                        }
                    }
                    
                    populateGrid();
                    calendarModalService.setTaskGenericRefreshMethod(populateGrid);
                   
                    scope.taskSuccessCallback = function (param) {
                        //
                        populateGrid();
                    }

                    scope.editTask = function (TaskId, TaskModel, value) {
                        
                        //var m = "iuyiuyo";
                        $("#calendarTaskModal").modal("hide");
                        var editTask = CalendarService.EditTaskFromCalendarSceduler('newtask', TaskModel,value ,scope.taskSuccessCallback);
                    }
                   

                    scope.eventReminderTask = function (TaskId) {
                        var Id = TaskId;
                        var reminderurl = '/api/Tasks/' + Id + '/EventReminder';
                        $http.put(reminderurl).then(function (response) {
                            var res = response.data;
                            if (res) {
                                HFC.DisplaySuccess("Success");

                            }
                            else {
                                HFC.DisplayAlert("Error");
                            }
                        });
                      
                    }

                    scope.compeleteTask = function (TaskId) {
                        var loadingElement = document.getElementById("loading");
                        loadingElement.style.display = "block";
                        var Id = TaskId;
                        var markurl = '/api/Tasks/' + Id + '/Mark?IsComplete=true';
                        $http.get(markurl).then(function (response) {
                            var res = response.data;
                            if (res) {
                                HFC.DisplaySuccess("Success");
                                populateGrid();
                            }
                            else {
                                HFC.DisplayAlert("Error");
                            }
                            loadingElement.style.display = "none";
                        }).catch(function (error) {
                        HFC.DisplayAlert(error);
                            loadingElement.style.display = "none";
                        });;
                    }

                    //https://rclayton.silvrback.com/pubsub-controller-communication
                    // Sample subscriber to update the grid after editing a calender
                    // or newly added calendar.
                    scope.$on("message_Task_update", function (e, time) {
                        //
                        var x = time;
                        populateGrid();
                    });
                }
            }
        }
]);

