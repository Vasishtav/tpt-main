﻿    'use strict';

app.directive('hfcPayment', ['cacheBustSuffix', function (cacheBustSuffix) {
        return {
            restrict: 'EA',
            templateUrl: '/templates/NG/invoice/payment-view.html?cache-bust=' + cacheBustSuffix,
            replace: true,            
        };
    }]);
