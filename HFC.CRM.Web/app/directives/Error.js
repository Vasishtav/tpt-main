﻿
'use strict';
app.directive('hfcErrorModal', [
       'ErrorService', function (ErrorService) {
           return {
               restrict: 'E',
               scope: {
                   modalId: '@'
               },
               templateUrl: '/templates/NG/error-modal.html',
               replace: true,
               link: function (scope) {
                   scope.ErrorService = ErrorService;
               }
           }
       }
])
   .directive('hfcErrorForm', [
       function () {
           return {
               restrict: 'E',
               scope: true,
               templateUrl: '/templates/NG/error-edit-form.html',
               replace: true,
               
           }
       }
   ])