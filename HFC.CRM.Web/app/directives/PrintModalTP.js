﻿/// <reference path="../views/PrintPage.html" />

'use strict';
app.directive('hfcPrintModal', ['$http', 'PrintServiceTP', 'HFCService', 'PrintService', '$window','$location',
        function ($http, printServiceTP, HFCService, PrintService, $window,$location) {
            return {
                restrict: 'E',
                scope: {
                    modalId: '@',
                    leadId: '=',
                    accountId: '=',
                    opportunityId: '=',
                    quoteId: '=',
                    orderId: '=',
                    associatedWith: '=',
                    printExistingMeasurements: '=',
                    control: '=' // this is way to expose the method callable by the parent controller.
                    //packetType: '='
                },
                templateUrl: '/templates/NG/Hfc-print-modal.html',
                replace: true,
                link: function (scope) {
                    //
                    //scope = this;
                    scope.printServiceTP = printServiceTP;
                    scope.printservice = PrintService;


                    scope.isValid = false;
                    scope.printOptions = {};
                    scope.mydocumentList = {};


                    function setPrintOptions() {
                        scope.printOptions = {
                            leadDetails: false,
                            internalNotes: false,
                            externalNotes: false,
                            directionNotes: false,
                            googleMap: false,
                            existingMeasurements: false,
                            bbmeasurementForm: {
                                Selected: false,
                                Quantity: 1
                            },
                            ccmeasurementForm: {
                                Selected: false,
                                Quantity: 1
                            },
                            garagemeasurementForm: {
                                Selected: false,
                                Quantity: 1
                            },
                            closetmeasurementForm: {
                                Selected: false,
                                Quantity: 1
                            },
                            officemeasurementForm: {
                                Selected: false,
                                Quantity: 1
                            },
                            floormeasurementForm: {
                                Selected: false,
                                Quantity: 1
                            },
                            warranty: false,
                            installChecklist: false,
                            installProcess: false,
                            orderCustomerInvoice: false,
                            orderInstallerInvoice: false,
                            quote: false
                        };
                        scope.printFormat = {
                            //order: 'CondensedVersion',
                            //quote: 'CondensedVersion'
                        };
                    }

                    function setMydocuments() {
                        scope.printServiceTP.getMydocumentsForSales()
                        .then(function (result) {
                            var obj = result.data;
                            scope.mydocumentList = obj;

                        }
                        , function (resultError) {

                        });
                    }

                    function setMydocumentsForInstall() {
                        scope.printServiceTP.getMydocumentsForInstall()
                        .then(function (result) {
                            var obj = result.data;
                            scope.mydocumentList = obj;

                        }
                        , function (resultError) {

                        });
                    }

                    //https://stackoverflow.com/questions/16881478/how-to-call-a-method-defined-in-an-angularjs-directive
                    scope.internalControl = scope.control || {};

                    scope.internalControl.showPrintModal = function (packetTypee) {
                        //
                        scope.SalesPacketList = [];
                        scope.InstallPacketList = [];

                        scope.brandid = HFCService.CurrentBrand;
                        setPrintOptions();

                        if (packetTypee == "SalesPacket") {

                            $http.get('/api/notes/0/GetsalesPacketsDocList?Id=' + scope.brandid).then(function (result) {

                                scope.SalesPacketList = result.data;
                                scope.InstallPacketList = [];
                            }, function (error) {
                            });

                            scope.packetType = packetTypee;
                            setMydocuments();
                        } else {

                            $http.get('/api/notes/0/GetInstallPacketsDocList?Idd=' + scope.brandid).then(function (result) {

                                scope.SalesPacketList = [];
                                scope.InstallPacketList = result.data;
                            }, function (error) {
                            });

                            scope.packetType = packetTypee;
                            setMydocumentsForInstall();
                        }

                        scope.isValid = false;

                        var obj = $("#__printModalDiv__");
                        obj.modal("show");
                    }

                    // 1 : Budget Blinds
                    // 2 : Tailored Living
                    // 3 : Concrete Craft

                    scope.brandid = HFCService.CurrentBrand;

                    if (!scope.associatedWith) {
                        scope.associatedWith = "Lead";
                    }

                    if (!scope.packetType) {
                        scope.packetType = "SalesPacket";
                    }

                    scope.cancel = function (modl) {
                        //alert("Canceling Process print sales packet");
                        hideMe();
                    }

                    function isvalidInput(printOptions, myDocuments) {
                        var validInput = true;
                        var temp = [];
                        temp.push(printOptions.bbmeasurementForm);
                        temp.push(printOptions.ccmeasurementForm);
                        temp.push(printOptions.garagemeasurementForm);
                        temp.push(printOptions.closetmeasurementForm);
                        temp.push(printOptions.officemeasurementForm);
                        temp.push(printOptions.floormeasurementForm);
                        if (myDocuments) {
                            for (var index = 0; index < myDocuments.length; index++) {
                                temp.push(myDocuments[index]);
                            }
                        }

                        for (var index2 = 0; index2 < temp.length; index2++) {
                            var obj = temp[index2];
                            if (obj.Selected && obj.Quantity < 1) {
                                validInput = false;
                                break;
                            }
                        }

                        return validInput;

                    }

                    scope.printPacket = function () {
                       
                        this.isBusy = true;

                        var obj = this.mydocumentList;
                        var obj2 = this.printOptions;

                        if (!isvalidInput(obj2, obj)) {

                            HFC.DisplayAlert("Minimum 1 quantity is required!");
                            this.isBusy = false;
                            return;
                        }

                        if (this.packetType == "SalesPacket") {
                            var url = this.getUrl();
                            // $('#loading').css('display', 'block');
                            _print(url);
                            // $('#loading').css('display', 'none');
                            this.isBusy = false;
                        } else {
                            // Installation packet
                            //alert("installation packet");
                            //for displaying alert if signature is unavailable
                            if (obj2.orderCustomerInvoice && HFCService.AdminElectronicSign && HFCService.FranciseLevelElectronicSign) {
                                $http.get('/api/Orders/' + this.orderId + '/getSignedOrNot')
                                    .then(function (response) {
                                        if (!response.data) {
                                            HFC.DisplayAlert("The customer and sales rep signatures should be completed before printing the order form.");
                                            $(document.activeElement).bind('click', function () {
                                                scope.printafteralert();
                                            });
                                        }
                                        else
                                            scope.printafteralert();
                                    }).catch(function (error) {
                                        HFC.DisplayAlert($scope.ErrorMessage);
                                    });
                            }
                            else {
                                var md = this.getMydocumentsOptions();
                                var po = {
                                    AddCustomerOpportunity: obj2.leadDetails,
                                    AddGoogleMap: obj2.googleMap,
                                    AddExistingMeasurements: obj2.existingMeasurements,
                                    AddPrintInternalNotes: obj2.internalNotes,
                                    AddPrintExternalNotes: obj2.externalNotes,
                                    AddPrintDirectionNotes: obj2.directionNotes,
                                    AddOrder: true,
                                    AddInstallationChecklist: obj2.installChecklist,
                                    AddWarrantyInformation: obj2.warranty,
                                    AddInstallationProcess: obj2.installProcess,
                                    AddOrderCustomerInvoice: obj2.orderCustomerInvoice,
                                    AddOrderInstallerInvoice: obj2.orderInstallerInvoice,
                                    OrderPrintFormat: this.printFormat.order,
                                    MyDocuments: md
                                };

                                var instalOptions = this.getInstallerPacketOptions();
                                this.printservice.PrintInstallationPacket(this.orderId, po, instalOptions);

                                this.isBusy = false;
                            }
                        }

                        hideMe();
                    }
                    scope.printafteralert = function () {
                        var obj2 = this.printOptions;
                        var md = this.getMydocumentsOptions();
                        var po = {
                            AddCustomerOpportunity: obj2.leadDetails,
                            AddGoogleMap: obj2.googleMap,
                            AddExistingMeasurements: obj2.existingMeasurements,
                            AddPrintInternalNotes: obj2.internalNotes,
                            AddPrintExternalNotes: obj2.externalNotes,
                            AddPrintDirectionNotes: obj2.directionNotes,
                            AddOrder: true,
                            AddInstallationChecklist: obj2.installChecklist,
                            AddWarrantyInformation: obj2.warranty,
                            AddInstallationProcess: obj2.installProcess,
                            AddOrderCustomerInvoice: obj2.orderCustomerInvoice,
                            AddOrderInstallerInvoice: obj2.orderInstallerInvoice,
                            OrderPrintFormat: this.printFormat.order,
                            MyDocuments: md
                        };

                        var instalOptions = this.getInstallerPacketOptions();
                        this.printservice.PrintInstallationPacket(this.orderId, po, instalOptions);

                        this.isBusy = false;
                    }
                    scope.getbaseurl = function () {

                        var id = 0;
                        var ticks = (new Date()).getTime();

                        if (this.associatedWith == "Lead") {
                            id = this.leadId;
                        } else if (this.associatedWith == "Account") {
                            id = this.accountId
                        } else if (this.associatedWith == "Opportunity") {
                            id = this.opportunityId
                        } else if (this.associatedWith == "Quote") {
                            id = this.opportunityId
                        } else {
                            id = this.orderId
                        }

                        var baseurl = '/export/PrintSalesPacket/' + id + '/?type=PDF&inline=true&sourceType=' +
                            this.associatedWith + '&t=' + ticks + '&printoptions='; // + options;
                        return baseurl;
                    }

                    scope.getUrl = function () {
                        var po = this.printOptions;
                        var md = this.mydocumentList;

                        var quoteKey = this.quoteId;
                        var currentURL = $location.$$path;
                        currentURL = currentURL.split('/');
                        if (quoteKey == '' && currentURL[1] == "Editquote")
                            quoteKey = currentURL[3];
                        var options = "";
                        if (po.leadDetails) options += "leadDetails/";
                        if (po.internalNotes) options += "internalNotes/";
                        if (po.externalNotes) options += "externalNotes/";

                        if (po.directionNotes) options += "directionNotes/";

                        if (po.googleMap) options += "googleMap/";
                        if (po.quote) options += "quote/";
                        options += "quotePrintFormat=" + this.printFormat.quote + "/";
                        if (po.existingMeasurements) options += "existingMeasurements/";
                        if (po.bbmeasurementForm.Selected) options += "bbmeasurementForm|"
                            + po.bbmeasurementForm.Quantity + "/";
                        if (po.ccmeasurementForm.Selected) options += "ccmeasurementForm|"
                           + po.ccmeasurementForm.Quantity + "/";
                        if (po.garagemeasurementForm.Selected) options += "garagemeasurementForm|"
                           + po.garagemeasurementForm.Quantity + "/";
                        if (po.closetmeasurementForm.Selected) options += "closetmeasurementForm|"
                           + po.closetmeasurementForm.Quantity + "/";
                        if (po.officemeasurementForm.Selected) options += "officemeasurementForm|"
                           + po.officemeasurementForm.Quantity + "/";
                        if (po.floormeasurementForm.Selected) options += "floormeasurementForm|"
                           + po.floormeasurementForm.Quantity + "/";

                        options += "quoteKey|" + quoteKey + "/";

                        var burl = this.getbaseurl();
                        options += this.getMydocumentsOptions();

                        return burl + options + '&salesPacketOption=' + this.getSalesPacketOptions();
                    }

                    scope.getMydocumentsOptions = function () {
                        var md = this.mydocumentList;
                        var options = "";
                        for (var index = 0; index < md.length; index++) {
                            var obj = md[index];
                            if (obj.Selected)
                                options += obj.PrintItemId + "|" + obj.Quantity + "/";
                        }

                        return options;
                    }

                    scope.getSalesPacketOptions = function () {

                        var salesPacket = "";
                        angular.forEach(scope.SalesPacketList, function (row, key) {
                            if (row.IsEnabled == true) {
                                salesPacket += row.streamId + "|" + row.Quantity + "/";
                            }
                        });

                        return salesPacket;
                    }

                    scope.getInstallerPacketOptions = function () {

                        var salesPacket = "";
                        angular.forEach(scope.InstallPacketList, function (row, key) {
                            if (row.IsEnabled == true) {
                                salesPacket += row.streamId + "|" + row.Quantity + "/";
                            }
                        });

                        return salesPacket;
                    }
                    function _print(src) {
                        window.open(src, '_blank');
                    };
                    //function _print(src) {
                    //    
                    //    if (navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPhone/i)) {
                    //        
                    //        var loadingElement = document.getElementById("loading");
                    //        if (loadingElement)
                    //            loadingElement.style.display = "block";
                    //        $http({
                    //            url: src,
                    //            method: 'GET',
                    //            headers: {
                    //                'Content-type': 'application/pdf'
                    //            },
                    //            responseType: 'arraybuffer'
                    //        }).then(function (data, status, headers, config) {
                    //            var pdfFile = new Blob([data.data], {
                    //                type: 'application/pdf'
                    //            });
                    //            var reader = new FileReader();
                    //            reader.onloadend = function (e) {
                    //                $window.open(reader.result);
                    //            }
                    //            reader.readAsDataURL(pdfFile);
                    //            var loadingElement = document.getElementById("loading");
                    //            if (loadingElement)
                    //                loadingElement.style.display = "none";

                    //        }, function (errorRes) {

                    //            var loadingElement = document.getElementById("loading");
                    //            if (loadingElement)
                    //                loadingElement.style.display = "none";
                    //        });

                    //    }
                    //    else if (navigator.userAgent.match('CriOS')) {
                    //        
                    //        var loadingElement = document.getElementById("loading");
                    //        if (loadingElement)
                    //            loadingElement.style.display = "block";
                    //        $http({
                    //            url: src,
                    //            method: 'GET',
                    //            headers: {
                    //                'Content-type': 'application/pdf'
                    //            },
                    //            responseType: 'arraybuffer'
                    //        }).then(function (data, status, headers, config) {
                    //            var pdfFile = new Blob([data.data], {
                    //                type: 'application/pdf'
                    //            });

                    //            var reader = new FileReader();
                    //            reader.onloadend = function (e) {
                    //                $window.open(reader.result);
                    //            }
                    //            reader.readAsDataURL(pdfFile);

                    //            var loadingElement = document.getElementById("loading");
                    //            if (loadingElement)
                    //                loadingElement.style.display = "none";

                    //        }, function (errorRes) {

                    //            var loadingElement = document.getElementById("loading");
                    //            if (loadingElement)
                    //                loadingElement.style.display = "none";
                    //        });


                    //    } else if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                    //        
                    //        var loadingElement = document.getElementById("loading");
                    //        if (loadingElement)
                    //            loadingElement.style.display = "block";
                    //        $http({
                    //            url: src,
                    //            method: 'GET',
                    //            headers: {
                    //                'Content-type': 'application/pdf'
                    //            },
                    //            responseType: 'blob'
                    //        }).then(function (data, status, headers, config) {
                    //            var pdfFile = new Blob([data.data], {
                    //                type: 'application/pdf'
                    //            });
                    //            var loadingElement = document.getElementById("loading");
                    //            if (loadingElement)
                    //                loadingElement.style.display = "none";
                    //            window.navigator.msSaveOrOpenBlob(pdfFile, /[^/]*$/.exec(URL.createObjectURL(pdfFile))[0] + '.pdf');


                    //        }, function (errorRes) {

                    //            var loadingElement = document.getElementById("loading");
                    //            if (loadingElement)
                    //                loadingElement.style.display = "none";
                    //        });
                    //    }
                    //    else {
                    //        
                    //        var loadingElement = document.getElementById("loading");
                    //        if (loadingElement)
                    //            loadingElement.style.display = "block";

                    //        let newWindow = window.open('app/views/PrintPage.html', '_blank');
                    //        $http({
                    //            url: src,
                    //            method: 'GET',
                    //            headers: {
                    //                'Content-type': 'application/pdf'
                    //            },
                    //            responseType: 'blob'
                    //        }).then(function (data, status, headers, config) {

                    //            if (newWindow.document.readyState === 'complete') {

                    //                var pdfFile = new Blob([data.data], {
                    //                    type: 'application/pdf'
                    //                });
                    //                newWindow.location = URL.createObjectURL(pdfFile);
                    //            } else {
                    //                newWindow.onload = () => {
                    //                    var pdfFile = new Blob([data.data], {
                    //                        type: 'application/pdf'
                    //                    });
                    //                    newWindow.location = URL.createObjectURL(pdfFile);
                    //                };
                    //            }
                    //            var loadingElement = document.getElementById("loading");
                    //            if (loadingElement)
                    //                loadingElement.style.display = "none";

                    //        }, function (errorRes) {

                    //            var loadingElement = document.getElementById("loading");
                    //            if (loadingElement)
                    //                loadingElement.style.display = "none";
                    //        });
                    //    }                    
                    //};

                    function getPDFPlugin() {
                        var userAgent = navigator ? navigator.userAgent.toLowerCase() : "other";

                        if (userAgent.indexOf("msie") > -1) {
                            //
                            // load the activeX control
                            // AcroPDF.PDF is used by version 7 and later
                            // PDF.PdfCtrl is used by version 6 and earlier
                            return getActiveXObject('AcroPDF.PDF') || getActiveXObject('PDF.PdfCtrl');
                        } else {
                            return getNavigatorPlugin('Adobe Acrobat') || getNavigatorPlugin('Chrome PDF Viewer') || getNavigatorPlugin('WebKit built-in PDF');
                        }
                    };

                    function _injectIframe() {
                        if (document.getElementById("print_frame")) {
                            //   document.getElementById("print_frame").remove();
                        }

                        if (document.getElementById("print_frame") == null) {
                            var frame = document.createElement("iframe");
                            frame.id = "print_frame";
                            frame.width = 0;
                            frame.height = 0;
                            frame.frameBorder = 0;
                            $(".wrapper").append(frame);
                        }
                    }

                    function getNavigatorPlugin(name) {
                        for (var key in navigator.plugins) {
                            var plugin = navigator.plugins[key];
                            if (plugin.name == name) return plugin;
                        }
                    };

                    function hideMe() {
                        var obj = $("#__printModalDiv__");
                        obj.modal("hide");
                    }

                    scope.selectChanged = function () {
                        var mydocumentselected = false;
                        if (mydocumentselected != true)
                            for (var index = 0; index < scope.mydocumentList.length; index++) {
                                var obj = scope.mydocumentList[index];
                                mydocumentselected = obj.Selected;

                                if (mydocumentselected) break;
                            }

                        if (mydocumentselected != true)
                            for (var index = 0; index < scope.SalesPacketList.length; index++) {
                                var obj = scope.SalesPacketList[index];
                                mydocumentselected = obj.IsEnabled;

                                if (mydocumentselected) break;
                            }

                        if (mydocumentselected != true)
                            for (var index = 0; index < scope.InstallPacketList.length; index++) {
                                var obj = scope.InstallPacketList[index];
                                mydocumentselected = obj.IsEnabled;

                                if (mydocumentselected) break;
                            }
                        //quote & opportunity
                        if (scope.packetType == 'SalesPacket') {
                            //check if parent quote check box is enable make the 'condenseversion' default.
                            var quoteCheckbox = $('#quote_').is(":checked");
                            if (quoteCheckbox == true && (scope.printFormat.quote == undefined || scope.printFormat.quote == "")) {
                                scope.printFormat = {
                                    quote: 'CondensedVersion'
                                };
                            } else if (quoteCheckbox == false) {
                                scope.printFormat = {
                                    quote: ''
                                };
                            }
                        }
                        //order
                        if (scope.packetType == 'InstallationPacket') {
                            //check if parent order check box is enable make the 'condenseversion' default.
                            var orderCheckbox = $('#order_').is(":checked");
                            if (orderCheckbox == true && (scope.printFormat.order == undefined || scope.printFormat.order == "")) {
                                scope.printFormat = {
                                    order: 'CondensedVersion'
                                };
                            } else if (orderCheckbox == false) {
                                scope.printFormat = {
                                    order: ''
                                };
                            }
                        }
                        scope.isValid = mydocumentselected ||
                            scope.printOptions.bbmeasurementForm.Selected ||
                            scope.printOptions.ccmeasurementForm.Selected ||
                            scope.printOptions.garagemeasurementForm.Selected ||
                            scope.printOptions.closetmeasurementForm.Selected ||
                            scope.printOptions.officemeasurementForm.Selected ||
                            scope.printOptions.floormeasurementForm.Selected ||
                            scope.printOptions.leadDetails ||
                            //scope.printOptions.internalNotes ||
                            //scope.printOptions.externalNotes ||
                            //scope.printOptions.directionNotes ||
                            scope.printOptions.googleMap ||
                            scope.printOptions.existingMeasurements ||
                            scope.printOptions.warranty || // The following are for only install packet type
                            scope.printOptions.installChecklist ||
                            scope.printOptions.installProcess ||
                            scope.printOptions.orderCustomerInvoice ||
                            scope.printOptions.orderInstallerInvoice ||
                            scope.printOptions.quote;
                    }

                   //added the code, when selcting the child radio button ex:- condense,exclude line item discount etc.
                    scope.quoteorderSubOptions = function (value, associated, type) {
                        
                        if (value == true && associated != '' && type == 'SalesPacket') {
                            scope.printOptions.quote = true;
                            scope.isValid = true; // added so that print button is enable.
                        } else if (value == true && associated == '' && type == 'InstallationPacket') {
                            scope.printOptions.orderCustomerInvoice = true;
                            scope.isValid = true;
                        }
                    }
                    //-end.
                }
            }
        }
]);

