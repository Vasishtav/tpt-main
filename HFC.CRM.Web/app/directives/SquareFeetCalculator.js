﻿app
   .directive('hfcSquareFeetCalculator', [
        'SquareFeetCalculatorService', function (SquareFeetCalculator) {
            return {
                restrict: 'E',
                scope: {
                    modalId: '@'
                },
                templateUrl: '/app/views/measurement/SquareFeetCalculator.html',
                replace: true,
                link: function (scope, element) {
                    scope.SquareFeetCalculator = SquareFeetCalculator;
                    angular.element(document).ready(function () {
                        $('#__squareFeetCalculator__').on('shown.bs.modal', function () {
                            SquareFeetCalculator.GetSquareFeetData();
                        });
                    });
                }
            }
        }
   ])
    .service('SquareFeetCalculatorService', [
    '$http', '$window', '$location', 'HFCService',
    'kendoService', 'kendotooltipService',
    function ($http, $window, $location, HFCService,
        kendoService, kendotooltipService) {
        var scope = this;
        scope.HFCService = HFCService;
        scope.OneTimeLoad = true;
        scope.currentEditIdAdd = null;
        scope.currentEditIdSub = null;
        scope.rowNumberrAdd = 0;
        scope.rowNumberrSub = 0;

        //For Kendo tooltip
        kendoService.customTooltip = null;

        scope.saveRedirectCancel = false;

        scope.SquareFeetHeader = {};

        scope.TotalAdditions = 0;
        scope.TotalSubtractions = 0;
        scope.NetSurfaceArea = 0;

        var successCallback = null;
        var digestCallback = null;

        scope.modalState = {
            loaded: true
        }

        scope.ShowCalculator = function (CalculatePopupClosed, receiveDigestCallback) {
            successCallback = CalculatePopupClosed;
            digestCallback = receiveDigestCallback;
            scope.ClearSquareFeetData();

            $("#__squareFeetCalculator__").modal("show");
            scope.SquareFeetGrid();
            $('#gridSquareFeetAdd>.k-grid-content.k-auto-scrollable').height(170);
            $('#gridSquareFeetSub>.k-grid-content.k-auto-scrollable').height(170);

            var menuDivHeight = $("#MenuDiv").height();
            if ((window.innerHeight - menuDivHeight - 30) > 661) {
                $('.modal-body').height(661);
            }
            else {
                if (!window.location.href.includes('/measurementDetails'))
                    $('.modal-body').height(window.innerHeight - menuDivHeight - 30);
                else
                    $('.modal-body').height(window.innerHeight - menuDivHeight - 50);
            }

        }

        function errorHandler(reseponse) {
            scope.modalState.loaded = true;
            HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
        }

        scope.GetSquareFeetData = function () {
            scope.modalState.loaded = false;
            var gridAdd = $('#gridSquareFeetAdd').data("kendoGrid");
            var gridSub = $('#gridSquareFeetSub').data("kendoGrid");

            if (scope.SquareFeetHeader.Add != null && scope.SquareFeetHeader.Add !== '' && scope.SquareFeetHeader.Add != undefined) {
                if (scope.SquareFeetHeader.Add.length > 0) {
                    //console.log(scope.SquareFeetHeader.Add);
                    var dataSourceAdd = new kendo.data.DataSource({ data: scope.SquareFeetHeader.Add, schema: getSquareFeetDataSchema(), pageSize: 25 });
                    dataSourceAdd.read();
                    gridAdd.setDataSource(dataSourceAdd);
                }
            }

            if (scope.SquareFeetHeader.Sub != null && scope.SquareFeetHeader.Sub !== '' && scope.SquareFeetHeader.Sub != undefined) {
                if (scope.SquareFeetHeader.Sub.length > 0) {
                    //console.log(scope.SquareFeetHeader.Sub);
                    var dataSourceSub = new kendo.data.DataSource({ data: scope.SquareFeetHeader.Sub, schema: getSquareFeetDataSchema(), pageSize: 25 });
                    dataSourceSub.read();
                    gridSub.setDataSource(dataSourceSub);
                }
            }

            scope.CalculateNetSurfaceArea(false);

            //Below code to update dataItem changes in grid
            if (digestCallback) digestCallback();

            scope.modalState.loaded = true;

            $(".k-grid").off("mousedown", ".k-grid-header th");
            $(".k-grid").on("mousedown", ".k-grid-header th", function (e) {
                var grid = $(this).closest(".k-grid");
                var editRow = grid.find(".k-grid-edit-row");
                if (editRow.length > 0) {
                    alert("Please complete the editing operation before sorting or filtering");
                    e.preventDefault();
                }
            });
        };

        scope.rowNumberAdd = 0;
        scope.rowNumberSub = 0;

        function renderNumberAdd() {
            scope.rowNumberAdd = scope.rowNumberAdd + 1;
            scope.rowNumberrAdd = scope.rowNumberAdd;
            return scope.rowNumberAdd;
        }

        function renderNumberSub() {
            scope.rowNumberSub = scope.rowNumberSub + 1;
            scope.rowNumberrSub = scope.rowNumberSub;
            return scope.rowNumberSub;
        }

        function resetRowNumberAdd(e) {
            scope.rowNumberAdd = 0;
            var grid = $("#gridSquareFeetAdd").data("kendoGrid");
            var dataSourcee = grid.dataSource;
            if (dataSourcee._data.length === 0) {
                scope.rowNumberrAdd = 0;
            }

            if (kendotooltipService.columnWidth) {
                kendotooltipService.restrictTooltip(kendotooltipService.columnWidth);
            } else if (window.innerWidth < 1280) {
                kendotooltipService.restrictTooltip(null);
            }
            var view = dataSourcee.view();
            scope.rowNumberrAdd = view.length;

        }

        function resetRowNumberSub(e) {
            scope.rowNumberSub = 0;
            var grid = $("#gridSquareFeetSub").data("kendoGrid");
            var dataSourcee = grid.dataSource;
            if (dataSourcee._data.length === 0) {
                scope.rowNumberrSub = 0;
            }

            if (kendotooltipService.columnWidth) {
                kendotooltipService.restrictTooltip(kendotooltipService.columnWidth);
            } else if (window.innerWidth < 1280) {
                kendotooltipService.restrictTooltip(null);
            }
            var view = dataSourcee.view();
            scope.rowNumberrSub = view.length;
        }

        function getSquareFeetDataSchema() {
            return {
                model: {
                    id: "id",
                    fields: {
                        id: { editable: false, nullable: true },
                        Description: { editable: true, validation: { required: true } },
                        Shape: { editable: true, validation: { required: true } },
                        Quantity: { editable: true, type: "number" },
                        Dimension1: { editable: false },
                        Measurement1: { editable: true, type: "number" },
                        Dimension2: { editable: false },
                        Measurement2: { editable: true, type: "number" },
                        Area: { editable: false },
                    }
                },
            }
        }
        scope.SquareFeetGrid = function () {

            scope.EditSquareFeetAdd = {
                dataSource: {
                    error: function (e) {
                        HFC.DisplayAlert(e.errorThrown);
                    },
                    schema: getSquareFeetDataSchema()
                },
                columns: [
                    { title: "#", width: 40, template: renderNumberAdd },
                    {
                        field: "Description",
                        title: "Description",
                        editor: '<input data-type="text" class="k-textbox" id="Description"  name="Description" data-bind="value:Description"/>'
                    },
                    {
                        field: "Shape",
                        title: "Shape",
                        editor: function (container, options) {
                            $('<input id="Shape" required  name="' + options.field + '" data-bind="value:Shape"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                autoBind: true,
                                optionLabel: "Select",
                                valuePrimitive: true,
                                dataTextField: "Value",
                                dataValueField: "Key",
                                change: function (e) {
                                    var item = $('#gridSquareFeetAdd').find('.k-grid-edit-row');
                                    var dataItem = item.data().$scope.dataItem;
                                    DropdownChanged(dataItem, 'Shape', 'Add');
                                },
                                select: function (e) {
                                    if (e.dataItem.Value === "Select") {
                                        scope.SystemPreventClose = true;
                                        e.preventDefault();
                                    }
                                    else {
                                        var a = $("#gridSquareFeetAdd #Shape").parent();
                                        var b = a[0].children[0].children;
                                        $(b[0]).removeClass("requireddropfield");
                                        $(b[0]).addClass("k-input");
                                        scope.SystemPreventClose = false;
                                    }
                                },
                                close: function (e) {
                                    if (scope.SystemPreventClose == true)
                                        e.preventDefault();
                                    scope.SystemPreventClose = false;
                                },
                                dataSource: {
                                    transport: {
                                        read: {
                                            url: '/api/Lookup/0/GetShape',
                                        }
                                    },
                                }
                            });
                            if (scope.NewEditRowAdd) {
                                var a = $("#gridSquareFeetAdd #Shape").parent();
                                var b = a[0].children[0].children;
                                $(b[0]).removeClass("k-input");
                                $(b[0]).addClass("requireddropfield");
                            }
                        }
                    },
                    {
                        field: "Dimension1",
                        title: "Dimension 1",
                        attributes: { class: "text-center" },
                    },
                    {
                        field: "Measurement1",
                        title: "Measurement",
                        editor: numericEditor3Add,
                        attributes: { class: "text-right" },
                    },
                    {
                        field: "Dimension2",
                        title: "Dimension 2",
                        attributes: { class: "text-center" },
                        width: 150,
                    },
                    {
                        field: "Measurement2",
                        title: "Measurement",
                        editor: numericEditor3Add,
                        attributes: { class: "text-right" },
                    },
                    {
                        field: "Quantity",
                        title: "Quantity",
                        editor: numericEditorAdd,
                        width: 100,
                    },
                    {
                        field: "Area",
                        title: "Area",
                        attributes: { class: "text-right" },
                    },
                    {
                        field: "",
                        title: "",
                        width: 60,
                        template: '<div class="dropdown"><button class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> <ul class="dropdown-menu pull-right"><li><a href="javascript:void(0)"  ng-click="SquareFeetCalculator.EditRowDetails(dataItem,\'Add\')">Edit</a></li>  <li><a  href="javascript:void(0)" ng-click="SquareFeetCalculator.DeleteRowDetails(this,\'Add\')">Delete</a></li><li><a href="javascript:void(0)"  ng-click="SquareFeetCalculator.CloneRowDetails(this,\'Add\')">Clone</a></li></ul></div></div>'
                    }
                ],
                noRecords: { template: '<span style="margin-top:20px">No records found</span>' },
                filterable: false,
                resizable: true,
                sortable: false,
                scrollable: true,
                pageable: false,
                dataBound: resetRowNumberAdd,
                editable: "inline",
                edit: function (e) {
                    e.container.find(".k-edit-label:last").hide();
                    e.container.find(".k-edit-field:last").hide();
                },
                toolbar: [
                    {
                        template: kendo.template($("#AddToolbarTemplate").html()),
                    }],
                columnResize: function (e) {
                    getUpdatedColumnList(e);
                }
            };

            //Below code for Subtraction Grid
            scope.EditSquareFeetSub = {
                dataSource: {
                    error: function (e) {
                        HFC.DisplayAlert(e.errorThrown);
                    },
                    schema: getSquareFeetDataSchema()
                },

                columns: [
                    { title: "#", width: 40, template: renderNumberSub },
                    {
                        field: "Description",
                        title: "Description",
                        editor: '<input data-type="text" class="k-textbox " id="Description"  name="Description" data-bind="value:Description"/>'
                    },
                    {
                        field: "Shape",
                        title: "Shape",
                        editor: function (container, options) {
                            $('<input id="Shape" required  name="' + options.field + '" data-bind="value:Shape"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                autoBind: true,
                                optionLabel: "Select",
                                valuePrimitive: true,
                                dataTextField: "Value",
                                dataValueField: "Key",
                                change: function (e) {
                                    var item = $('#gridSquareFeetSub').find('.k-grid-edit-row');
                                    var dataItem = item.data().$scope.dataItem;
                                    DropdownChanged(dataItem, 'Shape', 'Sub');
                                },
                                select: function (e) {
                                    if (e.dataItem.Value === "Select") {
                                        scope.SystemPreventClose = true;
                                        e.preventDefault();
                                    }
                                    else {
                                        var a = $("#gridSquareFeetSub #Shape").parent();
                                        var b = a[0].children[0].children;
                                        $(b[0]).removeClass("requireddropfield");
                                        $(b[0]).addClass("k-input");
                                        scope.SystemPreventClose = false;
                                    }
                                },
                                close: function (e) {
                                    if (scope.SystemPreventClose == true)
                                        e.preventDefault();
                                    scope.SystemPreventClose = false;
                                },
                                dataSource: {
                                    transport: {
                                        read: {
                                            url: '/api/Lookup/0/GetShape',
                                        }
                                    },
                                }
                            });
                            if (scope.NewEditRowSub) {
                                var a = $("#gridSquareFeetSub #Shape").parent();
                                var b = a[0].children[0].children;
                                $(b[0]).removeClass("k-input");
                                $(b[0]).addClass("requireddropfield");
                            }
                        }
                    },
                    {
                        field: "Dimension1",
                        title: "Dimension 1",
                        attributes: { class: "text-center" },
                    },
                    {
                        field: "Measurement1",
                        title: "Measurement",
                        editor: numericEditor3Sub,
                        attributes: { class: "text-right" },
                    },
                    {
                        field: "Dimension2",
                        title: "Dimension 2",
                        attributes: { class: "text-center" },
                        width: 150,
                    },
                    {
                        field: "Measurement2",
                        title: "Measurement",
                        editor: numericEditor3Sub,
                        attributes: { class: "text-right" },
                    },
                    {
                        field: "Quantity",
                        title: "Quantity",
                        editor: numericEditorSub,
                        width: 100,
                    },
                    {
                        field: "Area",
                        title: "Area",
                        attributes: { class: "text-right" },
                    },
                    {
                        field: "",
                        title: "",
                        width: 60,
                        template: '<div class="dropdown"><button class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> <ul class="dropdown-menu pull-right"><li><a href="javascript:void(0)"  ng-click="SquareFeetCalculator.EditRowDetails(dataItem,\'Sub\')">Edit</a></li>  <li><a  href="javascript:void(0)" ng-click="SquareFeetCalculator.DeleteRowDetails(this,\'Sub\')">Delete</a></li><li><a href="javascript:void(0)"  ng-click="SquareFeetCalculator.CloneRowDetails(this,\'Sub\')">Clone</a></div></div>'
                    }
                ],
                noRecords: { template: '<span style="margin-top:20px">No records found</span>' },
                filterable: false,
                resizable: true,
                sortable: false,
                scrollable: true,
                pageable: false,
                dataBound: resetRowNumberSub,
                editable: "inline",
                edit: function (e) {
                    e.container.find(".k-edit-label:last").hide();
                    e.container.find(".k-edit-field:last").hide();
                },
                toolbar: [
                        {
                            template: kendo.template($("#SubToolbarTemplate").html()),
                        }],
                columnResize: function (e) {
                    getUpdatedColumnList(e);
                }
            };
        }

        function numericEditorAdd(container, options) {
            $('<input id="' + options.field + '" name="' + options.field + '" data-bind="value:' + options.field + '"/>')
             .appendTo(container)
             .kendoNumericTextBox({
                 format: "{0:n0}",
                 decimals: 0,
                 min: 1,
                 spinners: false,
                 change: function (e) {
                     var item = $('#gridSquareFeetAdd').find('.k-grid-edit-row');
                     var dataItem = item.data().$scope.dataItem;
                     DropdownChanged(dataItem, 'Shape', 'Add');
                 },
             });
        }

        function numericEditorSub(container, options) {
            $('<input id="' + options.field + '" name="' + options.field + '" data-bind="value:' + options.field + '"/>')
             .appendTo(container)
             .kendoNumericTextBox({
                 format: "{0:n0}",
                 decimals: 0,
                 min: 1,
                 spinners: false,
                 change: function (e) {
                     var item = $('#gridSquareFeetAdd').find('.k-grid-edit-row');
                     var dataItem = item.data().$scope.dataItem;
                     DropdownChanged(dataItem, 'Shape', 'Sub');
                 },
             });
        }

        function numericEditor3Add(container, options) {
            $('<input disabled id="' + options.field + '" placeholder="0.000" name="' + options.field + '" data-bind="value:' + options.field + '"/>')
             .appendTo(container)
             .kendoNumericTextBox({
                 format: "{0:n3}",
                 decimals: 3,
                 min: 0,
                 spinners: false,
                 change: function (e) {
                     var item = $('#gridSquareFeetAdd').find('.k-grid-edit-row');
                     var dataItem = item.data().$scope.dataItem;
                     DropdownChanged(dataItem, 'Shape', 'Add');
                 },
             });
        }

        function numericEditor3Sub(container, options) {
            $('<input disabled id="' + options.field + '" placeholder="0.000" name="' + options.field + '" data-bind="value:' + options.field + '"/>')
             .appendTo(container)
             .kendoNumericTextBox({
                 format: "{0:n3}",
                 decimals: 3,
                 min: 0,
                 spinners: false,
                 change: function (e) {
                     var item = $('#gridSquareFeetSub').find('.k-grid-edit-row');
                     var dataItem = item.data().$scope.dataItem;
                     DropdownChanged(dataItem, 'Shape', 'Sub');
                 },
             });
        }

        // for grid rows manipulation
        var getUpdatedColumnList = function (e) {
            kendotooltipService.columnResizeTooltip(e.newWidth, e.column.headerAttributes.id);
        }

        function DropdownChanged(obj, clicked_id, type) {

            var grid = $("#gridSquareFeet" + type).data("kendoGrid");

            var row = grid.dataSource.get(obj.id);
            var Measurement1 = $("#gridSquareFeet" + type + " #Measurement1").data("kendoNumericTextBox");
            var Measurement2 = $("#gridSquareFeet" + type + " #Measurement2").data("kendoNumericTextBox");
            var Dimension1 = $("#gridSquareFeet" + type + " #Dimension1");
            var Dimension2 = $("#gridSquareFeet" + type + " #Dimension2");

            if (obj.Shape == "Square") {
                row.Dimension1 = "Side";
                row.Dimension2 = "--";
                Measurement1.enable(true);
                Measurement2.enable(false);
                row.Measurement2 = '';
            }
            if (obj.Shape == "Rectangle") {
                row.Dimension1 = 'Length';
                row.Dimension2 = "Width/Depth/Height";
                Measurement1.enable(true);
                Measurement2.enable(true);
            }
            if (obj.Shape == "Circle") {
                row.Dimension1 = "Radius";
                row.Dimension2 = "--";
                Measurement1.enable(true);
                Measurement2.enable(false);
                row.Measurement2 = '';
            }
            if (obj.Shape == "1/2 Circle") {
                row.Dimension1 = "Radius";
                row.Dimension2 = "--";
                Measurement1.enable(true);
                Measurement2.enable(false);
                row.Measurement2 = '';
            }
            if (obj.Shape == "Oval") {
                row.Dimension1 = "Long Radius";
                row.Dimension2 = "Short Radius";
                Measurement1.enable(true);
                Measurement2.enable(true);
            }

            var Area = scope.CalculateRowArea(obj, type);
            if (Area) {
                Area = Area.toFixed(3);
                row.Area = Area;
            }
            else {
                row.Area = null;
            }

            var Measurement2isDisabled = $("#gridSquareFeet" + type + " #Measurement2").prop('disabled');
            if (Measurement2isDisabled) {
                var parent1 = $("#gridSquareFeet" + type + " #Measurement2").parent()[0];
                var parentElement = $(parent1).parent()[0];
                $(parentElement).removeClass("required-error");
            }

            scope.CalculateNetSurfaceArea(true);

            //Below code to update dataItem changes in grid
            if (digestCallback && clicked_id != 'Clone' && clicked_id != 'Edit') digestCallback();
        }

        //Below to calculat the Total Additions, Total Subtractions and Net Surface Area
        scope.CalculateNetSurfaceArea = function (isGridData) {

            var DataAdd = null;
            var DataSub = null;
            if (isGridData) {
                DataAdd = $('#gridSquareFeetAdd').data().kendoGrid.dataSource.data();
                DataSub = $('#gridSquareFeetSub').data().kendoGrid.dataSource.data();
            }
            else {
                DataAdd = scope.SquareFeetHeader.Add;
                DataSub = scope.SquareFeetHeader.Sub;
            }

            scope.TotalAdditions = parseFloat(0);
            scope.TotalSubtractions = parseFloat(0);
            if (DataAdd)
                DataAdd.forEach(function (obj) {
                    if (obj.Area)
                        scope.TotalAdditions = scope.TotalAdditions + parseFloat(obj.Area);
                });
            if (DataSub)
                DataSub.forEach(function (obj) {
                    if (obj.Area)
                        scope.TotalSubtractions = scope.TotalSubtractions + parseFloat(obj.Area);
                });
            scope.TotalAdditions = scope.TotalAdditions.toFixed(3);
            scope.TotalSubtractions = scope.TotalSubtractions.toFixed(3);
            scope.NetSurfaceArea = scope.TotalAdditions - scope.TotalSubtractions;
            scope.NetSurfaceArea = scope.NetSurfaceArea.toFixed(3);
        }

        scope.CalculateRowArea = function (obj, type) {
            var Measurement1 = $("#gridSquareFeet" + type + " #Measurement1").data("kendoNumericTextBox");
            var Measurement2 = $("#gridSquareFeet" + type + " #Measurement2").data("kendoNumericTextBox");
            var Quantity = $("#gridSquareFeet" + type + " #Quantity").data("kendoNumericTextBox");

            var Area = null;
            if (obj.Shape == "Square") {
                if (Measurement1.value())
                    Area = Math.pow(Measurement1.value(), 2);
            }
            if (obj.Shape == "Rectangle") {
                if (Measurement1.value() && Measurement2.value())
                    Area = Measurement1.value() * Measurement2.value();
            }
            if (obj.Shape == "Circle") {
                if (Measurement1.value())
                    Area = Math.PI * Math.pow(Measurement1.value(), 2);
            }
            if (obj.Shape == "1/2 Circle") {
                if (Measurement1.value())
                    Area = (Math.PI * Math.pow(Measurement1.value(), 2)) / 2;
            }
            if (obj.Shape == "Oval") {
                if (Measurement1.value() && Measurement2.value())
                    Area = Math.PI * Measurement1.value() * Measurement1.value();
            }
            if (Quantity.value())
                Area = Quantity.value() * Area;
            else
                Area = null;

            return Area;
        }

        /// Adding new row
        scope.AddRow = function (type) {
            if (type == 'Add')
                scope.NewEditRowAdd = true;
            else if (type == 'Sub')
                scope.NewEditRowSub = true;

            var rowEdit = $('#gridSquareFeet' + type).find('.k-grid-edit-row');
            var validator = scope.kendoValidator("gridSquareFeet" + type);
            if (rowEdit.length) {
                //grid is not in edit mode
                if (!validator.validate() || !scope.validateCurrentRecord(type)) {
                    return false;
                }
            }

            if (type == 'Add')
                scope.AddNewRowAdd = true;
            else if (type == 'Sub')
                scope.AddNewRowSub = true;

            var grid = $("#gridSquareFeet" + type).data("kendoGrid");
            var dataSource = grid.dataSource;
            dataSource.filter({});
            if (type == 'Add')
                scope.currentEditIdAdd = dataSource._data.length + 1;
            else if (type == 'Sub')
                scope.currentEditIdSub = dataSource._data.length + 1;

            var index = dataSource._data.length;
            if (index >= 1) {
                scope.saveRedirectCancel = true;
                scope.SaveSquareFeet_AddRow(type);
            }
            else {
                if (type == 'Add')
                    scope.AddNewRowAdd = false;
                else if (type == 'Sub')
                    scope.AddNewRowSub = false;

                scope.AddSquareFeetData(type);
            }
        };

        scope.AddSquareFeetData = function (type) {
            var grid = $("#gridSquareFeet" + type).data("kendoGrid");
            var dataSource = grid.dataSource;
            var index = dataSource._data.length;
            if (index != 0)
                scope.id = index + 1;
            else
                scope.id = 1;

            var validator = scope.kendoValidator("gridSquareFeet" + type);
            if (validator.validate() && scope.validateCurrentRecord(type)) {

                var newItem = {
                    id: scope.id,
                    Quantity: 1
                }

                var newItem = dataSource.insert(index, newItem);
                var newRow = grid.items().filter("[data-uid='" + newItem.uid + "']");
                grid.editRow(newRow);

                grid.element.find(".k-grid-content").animate({
                    scrollTop: newRow.offset().top
                }, 400);
            } else {
                return false;
            }
        };

        scope.validateCurrentRecord = function (type) {

            var Description = $("#gridSquareFeet" + type + " #Description").val();
            var iQuantity = $("#gridSquareFeet" + type + " #Quantity").data("kendoNumericTextBox");
            if (!iQuantity)
                return true;
            var iMeasurement1 = $("#gridSquareFeet" + type + " #Measurement1").data("kendoNumericTextBox");
            var iMeasurement2 = $("#gridSquareFeet" + type + " #Measurement2").data("kendoNumericTextBox");
            var Measurement1isDisabled = $("#gridSquareFeet" + type + " #Measurement1").prop('disabled');
            var Measurement2isDisabled = $("#gridSquareFeet" + type + " #Measurement2").prop('disabled');

            var returnval = true;

            if (Description) {
                $("#gridSquareFeet" + type + " #Description").removeClass("required-error");
            }
            else {
                $("#gridSquareFeet" + type + " #Description").addClass("required-error");
                returnval = false;
            }

            if (iQuantity) {
                if (iQuantity.value()) {
                    var parent1 = $("#gridSquareFeet" + type + " #Quantity").parent()[0];
                    var parentElement = $(parent1).parent()[0];
                    $(parentElement).removeClass("required-error");
                }
                else {
                    var parent1 = $("#gridSquareFeet" + type + " #Quantity").parent()[0];
                    var parentElement = $(parent1).parent()[0];
                    $(parentElement).addClass("required-error");
                    returnval = false;
                }
            }
            if (iMeasurement1) {
                if (iMeasurement1.value() || Measurement1isDisabled) {
                    var parent1 = $("#gridSquareFeet" + type + " #Measurement1").parent()[0];
                    var parentElement = $(parent1).parent()[0];
                    $(parentElement).removeClass("required-error");
                }
                else {
                    var parent1 = $("#gridSquareFeet" + type + " #Measurement1").parent()[0];
                    var parentElement = $(parent1).parent()[0];
                    $(parentElement).addClass("required-error");
                    returnval = false;
                }
            }
            if (iMeasurement2) {
                if (iMeasurement2.value() || Measurement2isDisabled) {
                    var parent1 = $("#gridSquareFeet" + type + " #Measurement2").parent()[0];
                    var parentElement = $(parent1).parent()[0];
                    $(parentElement).removeClass("required-error");
                }
                else {
                    var parent1 = $("#gridSquareFeet" + type + " #Measurement2").parent()[0];
                    var parentElement = $(parent1).parent()[0];
                    $(parentElement).addClass("required-error");
                    returnval = false;
                }
            }
            return returnval;
        }

        scope.kendoValidator = function (gridId) {
            return $("#" + gridId).kendoValidator({
                validate: function (e) {
                    $("span.k-invalid-msg").hide();
                    var dropDowns = $(".k-dropdown");
                    $.each(dropDowns, function (key, value) {
                        var input = $(value).find("input.k-invalid");
                        var span = $(this).find(".k-widget.k-dropdown.k-header");
                        if (input.size() > 0) {
                            $(this).addClass("dropdown-validation-error");
                        } else {
                            $(this).removeClass("dropdown-validation-error");
                        }
                    });
                }
            }).getKendoValidator();
        }

        scope.ClearSquareFeetData = function () {
            scope.TotalAdditions = 0;
            scope.TotalSubtractions = 0;
            scope.NetSurfaceArea = 0;
            var dataSourceAdd = new kendo.data.DataSource({ data: null, schema: getSquareFeetDataSchema(), pageSize: 25 });
            dataSourceAdd.read();
            var dataSourceSub = new kendo.data.DataSource({ data: null, schema: getSquareFeetDataSchema(), pageSize: 25 });
            dataSourceSub.read();

            if ($('#gridSquareFeetAdd').data("kendoGrid")) {
                $('#gridSquareFeetAdd').data("kendoGrid").cancelChanges();
                $('#gridSquareFeetAdd').data("kendoGrid").setDataSource(dataSourceAdd);
            }
            if ($('#gridSquareFeetSub').data("kendoGrid")) {
                $('#gridSquareFeetSub').data("kendoGrid").cancelChanges();
                $('#gridSquareFeetSub').data("kendoGrid").setDataSource(dataSourceSub);
            }

            document.getElementsByName('SquareFeet_Details_Form')[0].reset();

        }
        scope.CancelSquareFeetEdit = function () {
            if (confirm('You will lose unsaved changes if you reload this page')) {
                scope.ClearSquareFeetData();
                $("#__squareFeetCalculator__").modal("hide");
                scope.CalculatePopupClosed = true;
            }
        }

        scope.SaveSquareFeet_AddRow = function (type) {
            var grid = $('#gridSquareFeet' + type).data().kendoGrid.dataSource.data();
            var validator = scope.kendoValidator("gridSquareFeet" + type);

            if (validator.validate() && scope.validateCurrentRecord(type)) {
                for (var i = 0; i < grid.length; i++) {
                    grid[i].id = i + 1;
                }

                if (scope.saveRedirectCancel == true) {
                    scope.saveRedirectCancel = false;

                    if (type == 'Add') {
                        if (scope.AddNewRowAdd) {
                            scope.AddNewRowAdd = false;
                            scope.AddSquareFeetData(type);
                        }
                    }
                    else if (type == 'Sub') {
                        if (scope.AddNewRowSub) {
                            scope.AddNewRowSub = false;
                            scope.AddSquareFeetData(type);
                        }
                    }
                }
            }
        };


        //Save Square Feet Calculator Data
        scope.SaveSquareFeetData = function () {

            scope.CalculateNetSurfaceArea(true);

            var gridAdd = $('#gridSquareFeetAdd').data().kendoGrid.dataSource.data();
            var gridSub = $('#gridSquareFeetSub').data().kendoGrid.dataSource.data();

            var validatorAdd = scope.kendoValidator("gridSquareFeetAdd");
            var validatorSub = scope.kendoValidator("gridSquareFeetSub");

            if (validatorAdd.validate() && validatorSub.validate() && scope.validateCurrentRecord('Add') && scope.validateCurrentRecord('Sub')) {

                for (var i = 0; i < gridAdd.length; i++) {
                    gridAdd[i].id = i + 1;
                }
                for (var i = 0; i < gridSub.length; i++) {
                    gridSub[i].id = i + 1;
                }
                scope.SquareFeetHeader = {};
                scope.SquareFeetHeader.Add = gridAdd;
                scope.SquareFeetHeader.Sub = gridSub;

                if ($('#gridSquareFeetAdd').data("kendoGrid"))
                    $('#gridSquareFeetAdd').data("kendoGrid").cancelChanges();
                if ($('#gridSquareFeetSub').data("kendoGrid"))
                    $('#gridSquareFeetSub').data("kendoGrid").cancelChanges();
                document.getElementsByName('SquareFeet_Details_Form')[0].reset();

                $("#__squareFeetCalculator__").modal("hide");
                if (successCallback) successCallback();
            }
        };

        scope.DeleteRowDetails = function (e, type) {
            if (type == 'Add')
                scope.currentEditIdAdd = null;
            else if (type == 'Sub')
                scope.currentEditIdSub = null;

            var rowEdit = $('#gridSquareFeet' + type).find('.k-grid-edit-row');
            if (rowEdit.length != 0 && rowEdit[0].dataset.uid != e.dataItem.uid) {
                var validator = scope.kendoValidator('gridSquareFeet' + type);
                if (validator.validate() && scope.validateCurrentRecord(type)) {
                    var dataItem = e.dataItem;
                    var grid = $("#gridSquareFeet" + type).data("kendoGrid");
                    var dataSource = grid.dataSource;
                    dataSource.remove(dataItem);
                } else {
                    return false;
                }
            }
            else {
                var dataItem = e.dataItem;
                var grid = $("#gridSquareFeet" + type).data("kendoGrid");
                var dataSource = grid.dataSource;
                dataSource.remove(dataItem);
            }

            var grid = $("#gridSquareFeet" + type).data("kendoGrid");
            var dataSourcee = grid.dataSource;

            if (dataSourcee._data.length === 0) {
                if (type == 'Add')
                    scope.rowNumberrAdd = 0;
                else if (type = 'Sub')
                    scope.rowNumberrSub = 0;
            }
            scope.CalculateNetSurfaceArea(true);
        }


        scope.EditRowDetails = function (obj, type) {
            if (type == 'Add')
                scope.NewEditRowAdd = false;
            else if (type == 'Sub')
                scope.NewEditRowSub = false;

            var grid = $("#gridSquareFeet" + type).data("kendoGrid");
            var validator = scope.kendoValidator("gridSquareFeet" + type);

            if (validator.validate() && scope.validateCurrentRecord(type)) {
                scope.AddSquareFeetData(type);
                var dataSource = grid.dataSource;
                if (obj) {
                    for (var p = 0; p <= dataSource._data.length ; p++) {
                        if (dataSource._data[p].id == obj.id) {
                            scope.PhotoFilename = dataSource._data[p].fileName;
                            var EditRow = dataSource._data[p];
                            grid.editRow(EditRow);
                            DropdownChanged(obj, 'Edit', type);
                            break;
                        }
                    }
                } else {
                    return false;
                }
            };
        }
        ///Clone row
        scope.CloneRowDetails = function (e, type) {
            scope.clone = true;
            var validator = scope.kendoValidator("gridSquareFeet" + type);

            if (validator.validate() && scope.validateCurrentRecord(type)) {
                var grid = $("#gridSquareFeet" + type).data("kendoGrid");
                var dataSource = grid.dataSource;
                var index = dataSource._data.length;
                if (index !== 0) {
                    var dataItem = e.dataItem;
                    scope.clonedId = e.dataItem.id;

                    index = index + 1;
                    newItem = {
                        id: index,
                        Description: dataItem.Description,
                        Shape: dataItem.Shape,
                        Quantity: dataItem.Quantity,
                        Dimension1: dataItem.Dimension1,
                        Measurement1: dataItem.Measurement1,
                        Dimension2: dataItem.Dimension2,
                        Measurement2: dataItem.Measurement2,
                        Area: dataItem.Area,
                    }

                    var newItem = dataSource.insert(index, newItem);
                    var newRow = grid.items().filter("[data-uid='" + newItem.uid + "']");
                    grid.editRow(newRow);
                    grid.element.find(".k-grid-content").animate({
                        scrollTop: newRow.offset().top
                    }, 400);

                    var a = $("#Shape").parent();
                    var b = a[0].children[0].children;
                    $(b[0]).removeClass("requireddropfield");
                    $(b[0]).addClass("k-input");
                    scope.SystemPreventClose = false;

                    DropdownChanged(newItem, 'Clone', type);
                    scope.clone = false;
                }
                else {
                    HFC.DisplaySuccess("wa! No Data Is Available");
                }
            };
        }
    }
    ]);