﻿
'use strict';
app.directive('hfcCommunicationList',
    ['CommunicationService', '$http', '$sce', 'HFCService', function (CommunicationService, $http, $sce, HFCService) {
            return {
                restrict: 'E',
                scope: {
                    modalId: '@',
                    leadId: '@',
                    accountId: '@',
                    opportunityId: '@',
                    readOnly: '@',
                    associatedWith: '@',
                    notifyEmail: '=',
                    notifyText:'='
                },
                templateUrl: '/templates/NG/LinkedList/Hfc-Communication-list.html',
                replace: true,
                link: function (scope) {
                    scope.communicationService = CommunicationService;
                    scope.HFCService = HFCService;
                    scope.communicationList = {};
                    scope.communicationLogType = {};
                    scope.errorMessage = "";
                    scope.showdata = {};
                    scope.ShowOrder = false;
                    scope.ValidDate = true;
                    scope.ValidDatelist = []; scope.submitted = false; 
                    if (scope.readOnly === "false") {
                        scope.IsEdit = false;
                    }
                    else {
                        scope.IsEdit = true;
                    }
                    

                    scope.trustAsHtml = function (html) {
                        return $sce.trustAsHtml(html);
                    }
                    scope.populateLeadCommunications = function () {
                        CommunicationService.getLeadCommunication(scope.leadId)
                        .then(function (response) {
                            scope.communicationList = response.data;
                        },
                        function (responseError) {
                            scope.errorMessage = responseError.statusText;
                        });
                    }

                    scope.populateAccountCommunications = function () {
                        CommunicationService.getAccountCommunication(scope.accountId)
                        .then(function (response) {
                            scope.communicationList = response.data;
                            scope.ShowOrder = true;
                            
                        },
                        function (responseError) {
                            scope.errorMessage = responseError.statusText;
                        });
                    }

                    scope.populateOpportunityCommunications = function () {
                        CommunicationService.getOpportunityCommunication(scope.opportunityId)
                        .then(function (response) {
                            scope.communicationList = response.data;
                            scope.ShowOrder = true;
                        },
                        function (responseError) {
                            scope.errorMessage = responseError.statusText;
                        });
                    }

                    function populateCommunicationGrid() {
                        // While loading the component populate the grid based on the associated component
                        switch (scope.associatedWith) {
                            case "Account":
                                scope.populateAccountCommunications();
                                break;
                            case "Opportunity":
                                scope.populateOpportunityCommunications();
                                break;
                            default:
                                scope.associatedWith = "Lead";
                                scope.populateLeadCommunications();
                        }
                    }

                    //populateCommunicationGrid();

                   
                   
                    ////CalendarService.SuccessCallback = function (params) {
                    ////    populateEventGrid();
                    ////}
                    
                    //scope.communicationSuccessCallback = function (param) {
                    //    populateCommunicationGrid();
                    //}

                    //scope.editCommunication = function (modalId, CommunicationId) {
                    //    $("#_calendarModal_").modal("hide");
                    //    var editTask = CalendarService.GetCommunicationsFromScheduler(modalId, CommunicationId, scope.CommunicationSuccessCallback);
                    //    //$scope.$apply();
                    //}


                    //scope.CommunicationReminderEvent= function (TaskId) {
                    //    var Id = TaskId;
                    //    var reminderurl = '/api/Tasks/' + Id + '/EventReminder';
                    //    $http.put(reminderurl).then(function (response) {
                    //        var res = response.data;
                    //        if (res) {
                    //            HFC.DisplaySuccess("Success");
                    //        }
                    //        else {
                    //            HFC.DisplayAlert("Error");
                    //        }
                    //    });

                    //}

                    // TODO: need to move the following two function to common library.
                    function maskPhoneNumber(text) {
                        if (text != null && text !== undefined) {
                            return text.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');
                        } else {
                            return '';
                        }
                    }

                    scope.maskedDisplayPhone = function(phone) {
                        //var Div;
                        //if (phone) {
                        //    Div = "<span class='tooltip-wrapper'><div class='tooltip-content'>";
                        //    if (phone != "")
                        //        Div += maskPhoneNumber(phone);
                           
                        //    Div += "</div></span>";
                        //} else {
                        //    Div = "";
                        //}

                        //return Div;
                        return maskPhoneNumber(phone);
                    }

                    scope.reSend = function (emailId) {
                        
                        scope.ResendMailId = emailId;
                        var resendurl = '/api/Communication/' + scope.ResendMailId + '/GetResendEmailData';
                        $http.get(resendurl).then(function (response) {
                            var ResendEmaildata = response.data;
                            scope.ShowEmail = ResendEmaildata;
                            $("#Show_ReSentEmailData").modal("show");
                        })
                    }

                    scope.reSendTexting = function (textingId) {
                        var resendurl = '/api/Communication/' + textingId + '/GetTextingDetails';
                        $http.get(resendurl).then(function (response) {
                            var temp = response.data;
                            scope.ShowTexting = temp;
                            $("#Show_ReSentTexting").modal("show");
                        })
                    }

                    scope.reSendTextingOk = function (textingId) {
                        $("#Show_ReSentTexting").modal("hide");
                        var reminderurl = '/api/Communication/' + textingId + '/ResendTexting';
                        $http.post(reminderurl).then(function (response) {
                            var res = response.data;
                            if (res == true) {
                                HFC.DisplaySuccess("Success");
                                populateCommunicationGrid();
                            }
                            else {
                                HFC.DisplayAlert("Error");
                            }
                        });
                    }

                    scope.ReSentEmailData = function (Emailid) {
                        
                        var Id = Emailid;
                        $("#Show_ReSentEmailData").modal("hide");
                        var reminderurl = '/api/Communication/' + Id + '/ResendEmail';
                        $http.get(reminderurl).then(function (response) {
                            var res = response.data;
                            if (res==true) {
                                HFC.DisplaySuccess("Success");
                                populateCommunicationGrid();
                            }
                            else {
                                HFC.DisplayAlert("Error");
                            }
                        });
                    }

                    scope.Close_ReSentEmailData = function () {
                        
                        $("#Show_ReSentEmailData").modal("hide");
                    }

                    scope.Close_ReSentTexting = function () {

                        $("#Show_ReSentTexting").modal("hide");
                    }

                    scope.displayemaildata = function (emailId) {
                        
                        var id = emailId;
                        var url = '/api/Communication/' + id + '/GetEmaildeatils';
                        $http.get(url).then(function (response) {
                            var data = response.data;
                            scope.showdata = data;
                            $("#Show_EmailSentData").modal("show");
                        });
                    }

                    scope.displayTextingData = function (textingId) {

                        var url = '/api/Communication/' + textingId + '/GetTextingDetails';
                        $http.get(url).then(function (response) {
                            var data = response.data;
                            scope.textingData = data;
                            $("#Show_TextingData").modal("show");
                        });
                    }

                    scope.ClosePop_up_Texting = function () {

                        $("#Show_TextingData").modal("hide");
                    }

                    scope.ClosePop_up = function () {

                        $("#Show_EmailSentData").modal("hide");
                    }

                    scope.SendReviewMail = function (orderid) {
                         
                        scope.orderval = orderid;
                        var resendurl = '/api/Orders/' + scope.orderval + '/SendThankYouReviewEmail';
                        $http.get(resendurl).then(function (response) {
                            var SendedEmaildata = response.data;
                            if (SendedEmaildata == "Success") {
                                 
                                populateCommunicationGrid();
                            }
                            else {
                                HFC.DisplayAlert(response.data);
                            }
                        })
                    }


                    
                  
                    ////https://rclayton.silvrback.com/pubsub-controller-communication
                    //// Sample subscriber to update the grid after editing a calender
                    
                    scope.$on("message_Communication_update", function (e, time) {
                        var x = time;
                        populateCommunicationGrid();
                    });

                    //initialize for the add log
                    function InitializeCommunication() {
                        scope.AddCommunications = {
                            LogType:'',
                            SentDate: '',
                            UserName:'',
                            Description: '',
                            LeadId: '',
                            AccountId: '',
                            OpportunityId:''
                        };
                    }
                    InitializeCommunication();

                    //close the add log pop-up
                    scope.CloseAddCommunication = function () {
                         
                        $("#Show_AddCommunicationLog").modal("hide");
                        scope.submitted = false; 
                        InitializeCommunication();
                    }

                    //save the log
                    scope.SaveCommunication = function () {
                        
                        scope.submitted = true;
                        scope.AddCommunications.LeadId = scope.leadId;
                        scope.AddCommunications.AccountId = scope.accountId;
                        scope.AddCommunications.OpportunityId = scope.opportunityId;
                        var dto = scope.AddCommunications;
                        //validate date
                        for (i = 0; i < scope.ValidDatelist.length; i++) {
                            if (!scope.ValidDatelist[i].valid) {
                                HFC.DisplayAlert("Invalid Date!");
                                return;
                            }
                        }

                        var date = new Date();
                        date.setHours(0);
                        date.setMinutes(0);
                        //var month = date.getMonth() + 1;
                        //var day = date.getDate();
                        //var CurrentDate = (month < 10 ? '0' : '') + month + '/' + (day < 10 ? '0' : '') + day + '/'+date.getFullYear();

                        if (scope.Add_Communication.$invalid) {
                            return;
                        }
                        
                        if (dto.SentDate != null && dto.SentDate != "") {
                            var TempSentDate = new Date(dto.SentDate);
                            var TempCurrentDate = date;
                            if (TempSentDate > TempCurrentDate) {
                                HFC.DisplayAlert("No future date is allowed!");
                                return;
                            }
                        }
                        

                        $http.post("/api/Communication/0/SaveNewCommunicationLog?Model", dto).then(function (response) {
                            var res = response.data;
                            if (res == "Success") {
                                //HFC.DisplaySuccess("Success");
                                $("#Show_AddCommunicationLog").modal("hide");
                                InitializeCommunication();
                                CommunicationService.setControllerValue();
                                scope.submitted = false; 
                            }
                            else {
                                $("#Show_AddCommunicationLog").modal("hide");
                                InitializeCommunication();
                                HFC.DisplayAlert("Error");
                                scope.submitted = false; 
                            }
                        });

                    }

                    //get drop down value for the add log pop-up
                    scope.GetLogOptions = function () {
                        
                        CommunicationService.getCommunicationLogType()
                            .then(function (response) {
                                scope.communicationLogType = response.data;
                                
                            },
                        function (responseError) {
                            scope.errorMessage = responseError.statusText;
                        });

                    }

                    scope.GetLogOptions();

                    //validate date 
                    scope.ValidateDate = function (date, id) {
                        var ValidDateobj = {};
                        
                            scope.ValidDate = scope.HFCService.ValidateDate(date, "date");

                        if (scope.ValidDatelist.find(item => item.id === id)) {
                            var index = scope.ValidDatelist.findIndex(item => item.id === id);
                            scope.ValidDatelist[index].valid = scope.ValidDate;
                        }
                        else {
                            ValidDateobj["id"] = id;
                            ValidDateobj["valid"] = scope.ValidDate;
                            scope.ValidDatelist.push(ValidDateobj);
                        }

                        if (scope.ValidDate) {
                            $("#" + id + "").removeClass("invalid_Date");
                            var a = $('#' + id + '').siblings();
                            $(a[0]).removeClass("icon_Background");
                        }
                        else {
                            $("#" + id + "").addClass("invalid_Date");
                            var a = $('#' + id + '').siblings();
                            $(a[0]).addClass("icon_Background");
                        }
                    }
                    setTimeout(function () {
                        var datepicker = $("#logSentDate").data("kendoDatePicker");

                        datepicker.bind("open", function (e) {

                            if (scope.AddCommunications.SentDate == '') {
                               
                                var datepick = $("#logSentDate").data("kendoDatePicker");
                                datepick.value(new Date());
                                datepick.value(scope.AddCommunications.SentDate);
                            }
                        });
                    }, 5000)
                    
                    
                }
            }
        }
]);




