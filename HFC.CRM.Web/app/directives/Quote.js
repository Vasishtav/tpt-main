﻿/// <reference path="C:\CodeBase\Touchpoint\trunk\HFC.CRM.Web\Templates/NG/Tax-modal-view.html" />
/// <reference path="C:\CodeBase\Touchpoint\trunk\HFC.CRM.Web\Templates/NG/Quote-OnetimeProduct-Modal.html" />
/// <reference path="C:\CodeBase\Touchpoint\trunk\HFC.CRM.Web\Templates/NG/Quote-OnetimeProduct-Modal.html" />
/// <reference path="C:\CodeBase\Touchpoint\trunk\HFC.CRM.Web\Templates/NG/Quote-MarginSummary-modal.html" />
/// <reference path="C:\CodeBase\Touchpoint\trunk\HFC.CRM.Web\Templates/NG/Quote-MarginSummary-modal.html" />
/// <reference path="D:\Ram\HFC\HFC.CRM.Web\Templates/NG/Calendar/calendar-modal.html" />
'use strict';


app.directive('hfcQuoteFormGo', [
    function () {
        return {
            restrict: 'E',
            scope: true,
            templateUrl: '/templates/NG/Quote-Measurement-modal.html',
            replace: true,

        }
    }
])

.directive('hfcQuotePriceFormGo', [
    function () {
        return {
            restrict: 'E',
            scope: true,
            templateUrl: '/templates/NG/Quote-PriceRules-modal.html',
            replace: true,

        }
    }
])
.directive('hfcQuotePrimaryQuoteFormGo', [
    function () {
        return {
            restrict: 'E',
            scope: true,
            templateUrl: '/templates/NG/Quote-Primary-Modal.html',
            replace: true,

        }
    }
])
.directive('hfcQuoteLineMemoFormGo', [
    function () {
        return {
            restrict: 'E',
            scope: true,
            templateUrl: '/templates/NG/QuoteLine-Memo-modal.html',
            replace: true,

        }
    }
])
.directive('hfcMarginSummaryFormGo', [
    function () {
        return {
            restrict: 'E',
            scope: true,
            templateUrl: '/templates/NG/Quote-MarginSummary-modal.html',
            replace: true,

        }
    }
])
.directive('hfcQuoteOnetimeProductFormGo', [
    function () {
        return {
            restrict: 'E',
            scope: true,
            templateUrl: '/templates/NG/Quote-OnetimeProduct-Modal.html',
            replace: true,

        }
    }
])
.directive('hfcTaxDetails', [
function () {
    return {
        restrict: 'E',
        scope: true,
        templateUrl: '/templates/NG/Tax-modal-view.html',
        replace: true,

    }
}
])
.directive('hfcPricingStrategy', [
    function () {
        return {
            restrict: 'E',
            scope: true,
            templateUrl: '/templates/NG/Quote-Pricing-modal.html',
            replace: true,

        }
    }
])
.directive('hfcPricingRule', [
            function () {
                return {
                    restrict: 'E',
                    scope: true,
                    templateUrl: '/templates/NG/pricing-rule-modal.html',
                    replace: true
                }
            }
])
;
