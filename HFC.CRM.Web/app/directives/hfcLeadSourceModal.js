﻿'use strict';

app.directive('hfcLeadSourceModal', ['LeadSourceService', 'cacheBustSuffix', function (LeadSourceService, cacheBustSuffix) {
    return {
        restrict: 'E',
        transclude: 'element',
        scope: {
            modalId: '@',
            leadId: '@'
        },
        templateUrl: '/templates/NG/Lead/leadSource-modal.html?cache-bust=' + cacheBustSuffix,
        replace: true,
        link: function (scope) {
            scope.LeadSourceService = LeadSourceService;
        }
    }
}])