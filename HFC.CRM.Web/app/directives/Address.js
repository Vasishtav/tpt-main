﻿/**
 * hfc.address module  
 * a simple directive for creating an address hover over with google maps 
 * @author Quan Tran
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */
(function (window, document, undefined) {
    'use strict';

    angular.module('hfc.address', ['hfc.hoverintent', 'hfc.core.service'])

    .service('AddressService', ['$http', '$sce', '$rootScope', '$anchorScroll', function ($http, $sce, $rootScope, $anchorScroll) {
        var srv = this;

        srv.Address = null;
        srv.Addresses = [];
        srv.AddressIndex = -1;

        srv.Countries = [];
        srv.States = [];
        srv.IsBusy = false;

        srv.ZipCodeChanged = function () {

            if (srv.Address.ZipCode) {
                srv.IsBusy = true;
                $http.get('/api/lookup/0/zipcodes?q=' + srv.Address.ZipCode + "&country=" + srv.Address.CountryCode2Digits).then(function (response) {
                    srv.IsBusy = false;
                    if (response.data.zipcodes.length > 0) {
                        srv.Address.City = response.data.zipcodes[0].City;
                        srv.Address.State = response.data.zipcodes[0].State;
                    } else {
                        srv.Address.City = null;
                        srv.Address.State = null;
                    }
                }, function (response) {
                    HFC.DisplayAlert(response.statusText);
                    srv.IsBusy = false;
                });
            }

        };

        srv.CountryChanged = function () {
            srv.Address.City = '';
            srv.Address.State = '';
            srv.Address.ZipCode = '';
            srv.Address.CrossStreet = '';
        }

        srv.GetAddress = function (addrId) {
            var addr = $.grep(srv.Addresses, function (a) {
                return a.AddressId == addrId;
            });
            if (addr)
                return addr[0];
            else
                return null;
        };

        srv.GetLabel = function (addrId, singleline) {
            var addr = srv.GetAddress(addrId)
            if (addr) {
                var str = (addr.Address1 || "") + " " + (addr.Address2 || "");
                if (!singleline && (addr.City || addr.State))
                    str += "<br/>";
                else if (singleline || addr.ZipCode)
                    str += ", ";
                str += (addr.City || "");
                if (addr.State)
                    str += ", " + addr.State + " ";
                else if (addr.City)
                    str += " ";
                str += (addr.ZipCode || "");

                if (singleline)
                    return str;
                else
                    return $sce.trustAsHtml(str);
            }
        };

        srv.GetStates = function (countryISO) {
            return $.grep(srv.States, function (s) {
                return s.CountryISO2 == countryISO;
            });
        };

        srv.GetZipCodeMask = function (countryISO) {
            var cc = $.grep(srv.Countries, function (c) {
                return c.ISOCode2Digits == (countryISO || "US");
            });
            if (cc && cc.length)
                return cc[0].ZipCodeMask;
            else
                return "";
        };

        srv.GetGoogleHref = function (addrId) {
            var addr = srv.GetAddress(addrId);
            if (addr && addr.Address1) {

                var srcAddr;
                if (HFC.currentGeoLocation && HFC.Browser.isMobile()) {
                    srcAddr = HFC.currentGeoLocation.coords.latitude + "," + HFC.currentGeoLocation.coords.longitude;
                } else {
                    srcAddr = encodeURIComponent(HFC.CurrentUserAddress);
                }

                if (navigator.userAgent.toLowerCase().indexOf("iphone") == -1 && navigator.userAgent.toLowerCase().indexOf("ipad") == -1) {
                    return '//maps.google.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(srv.GetLabel(addrId, true));
                } else {
                    return '//maps.apple.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(srv.GetLabel(addrId, true));
                }
            } else
                return null;
        };

        srv.Get = function (leadIds, includeStates) {

            if (!srv.IsBusy) {
                srv.IsBusy = true;

                $http({ method: 'GET', url: '/api/address/', params: { leadIds: leadIds, includeStates: includeStates } })
                .then(function (response) {
                    srv.States = response.data.States || [];
                    srv.Countries = response.data.Countries || [];
                    srv.Addresses = response.data.Addresses || [];
                    srv.FranchiseAddress = response.data.FranchiseAddress;

                    srv.IsBusy = false;
                }, function (response) {
                    srv.IsBusy = false;
                });
            }
        }

        srv.Set = function (states, countries, addresses, franchiseAddress) {
            srv.States = states || [];
            srv.Countries = countries || [];
            srv.Addresses = addresses || [];
            srv.FranchiseAddress = franchiseAddress;

        }

        srv.Add = function (modalId, country) {
            srv.Address = {
                IsResidential: true,
                CountryCode2Digits: country || 'US',
                IsInstallationAddress:false,
            };
            srv.AddressIndex = -1;
            
            //var myDiv = $('.modal-body');
            //myDiv[0].scrollTop = 0;
            
            $("#" + modalId).modal("show");
        }

        //srv.Delete = function (leadId, addressId) {
        //    if (!srv.IsBusy && leadId && confirm("Do you wish to continue?")) {
        //        srv.IsBusy = true;

        //        $http({ method: 'DELETE', url: '/api/leads/' + leadId + '/address/', headers: { 'Content-Type': 'application/json' }, data: { AddressId: addressId } })
        //        .then(function (response) {
        //            var address = srv.GetAddress(addressId);
        //            var label = srv.GetLabel(addressId, true);
        //            remove(address);
        //            HFC.DisplaySuccess("Address: " + label + " deleted");
        //            srv.IsBusy = false;
        //        }, function (response) {
        //            HFC.DisplayAlert(response.statusText);
        //            srv.IsBusy = false;
        //        });
        //    }
        //};
        srv.zipCodeIsNotValid = false;


        


        srv.Save = function (leadId, modalId) {
            if (!srv.IsBusy && srv.Address && leadId) {
                srv.IsBusy = true;
                var promise = null;

                if (srv.Address.ZipCode == '' || srv.Address.ZipCode == undefined) {
                    srv.zipCodeIsNotValid = true;
                    srv.IsBusy = false;
                    return;
                }
                else {
                    srv.zipCodeIsNotValid = false;
                }

                if (srv.Address.AddressId)
                    promise = $http.put('/api/leads/' + leadId + '/address/', srv.Address);
                else
                    promise = $http.post('/api/leads/' + leadId + '/address/', srv.Address);

                promise.then(function (res) {
                    if (!srv.Address.AddressId && res.data.AddressId != srv.Address.AddressId)
                        srv.Address.AddressId = res.data.AddressId;

                    if (srv.AddressIndex >= 0)
                        srv.Addresses[srv.AddressIndex] = srv.Address;
                    else
                        srv.Addresses.push(srv.Address);

                    $rootScope.$broadcast('refresh.territory', { zipCode: srv.Address.ZipCode, jobId: null });
                    srv.Address = null;
                    srv.AddressIndex = -1;

                    HFC.DisplaySuccess("Address saved");
                    srv.IsBusy = false;

                    $("#" + modalId).modal("hide");
                }, function (res) {
                    HFC.DisplayAlert(res.statusText);
                    srv.IsBusy = false;
                });
            }
        };

        srv.Edit = function (addressId, modalId, index) {
            var address = srv.GetAddress(addressId);

            srv.Address = angular.copy(address);
            srv.AddressIndex = index;
            
            //var myDiv = $('.modal-body');
            //myDiv[0].scrollTop = 0;
            $("#" + modalId).modal("show");
        };

        srv.Cancel = function (modalId) {
            srv.Address = null;
            srv.AddressIndex = -1;
            $("#" + modalId).modal("hide");
          

        }


        srv.SaveAddressChange = function (jobId, newAddressId, isBilling, success) {
            if (srv.IsBusy)
                return;

            srv.IsBusy = true;
            var data = { BillingAddressId: newAddressId };
            if (!isBilling)
                data = { InstallAddressId: newAddressId };
            $http.put('/api/jobs/' + jobId + '/address', data).then(function () {
                srv.IsBusy = false;
                if (success) {
                    success();
                }
            }, function (response) {
                HFC.DisplayAlert(response.statusText);
                srv.IsBusy = false;
            });
        }

        function remove(item) {
            var index = srv.Addresses.indexOf(item);
            if (index > 0) {
                srv.Addresses.splice(index, 1);
            }
            else if (index === 0) {
                srv.Addresses.shift();
            }
        }
    }])

    .service('AddressValidationService', ['$http', 'HFCService',
        function ($http, HFCService) {

            var srv = this;

            srv.validateAddress = function (model) {
                var url = "/api/AddressValidation/0/ValidateCustomerAddress";
                //return $http.get(url);
                return $http.post(url, model);
            }
        }
    ])

    .service('AdditionalAddressServiceTP', [
    '$http', 'HFCService', 'CacheService', '$rootScope', 'AddressValidationService', '$q',
    function ($http, HFCService, CacheService, $rootScope, addressValidationService, $q) {
        var srv = this;

        srv.showAddAddress = false;
        srv.showEditAddress = false;
        srv.placeHolderText = "Required";
        // Unless otherwise specified the component assume that the parent is Lead.
        srv.ParentName = "Lead";
        //global search
        srv.GlobalSearch = false;
        
        srv.IsBusy = false;

        // On which mode the current *additional contact* service operates. The default is Add mode.
        // When called from the grid row for edit, value will be reset to Edit.
        srv.Mode = "Add";

        srv.submitnte = false;
        // Contain list of additional contacts in the currrent context, example if the current view
        // is Lead, then the list will contain contacts to that specific Lead. 
        srv.AddressList = [];
        srv.CountryList = [];
        srv.StateList = [];

        // Represent the current contact in the context. it may be new contact or existing
        // contact in edit mode

        srv.AddressModal = {
            Index: '',
            AddressId: 0,
            AttentionText: '',
            Address1: '',
            Address2: '',
            City: '',
            State: '',
            ZipCode: '',
            CountryCode2Digits: '',
            IsResidential: '',
            CrossStreet: '',
            AddressType: '',
            LeadId: 0,
            AccountId: 0,
            IsInstallationAddress:'',
        };
        srv.Address = angular.copy(srv.Address);

        // The following ids should be populated from the consumer, for example LeadController
        // AdditionalAddress service required these information based on these only it can associate with either 
        // Lead or Account or Opportunity.
        // TODO: The best place is actually the view page (of Lead) where the Additional contact custom tag is used.
        //srv.LeadId, srv.AccountId, srv.OpportunityId

        srv.AddressIndex = -1;

        // this id is used to contain the controls for the additional contact popup modal.
        srv.divId = "_addtnlAddressModal_";

        // Canel the operation or window.
        srv.Cancel = function () {
            srv.submitnte = false;
            srv.Address = angular.copy(srv.Address);
            $("#" + srv.divId).modal("hide");
           
            //var myDiv = $("#" + srv.divId);
            var myDiv = $('.adres_popup');
            myDiv[0].scrollTop = 0;
            //$anchorScroll();
        };

        function getModelForPost() {
            var model = {
                AddressId: srv.Address.AddressId,
                AttentionText: srv.Address.AttentionText,
                Address1: srv.Address.Address1,
                Address2: srv.Address.Address2,
                City: srv.Address.City,
                State: srv.Address.State,
                ZipCode: srv.Address.ZipCode,
                CountryCode2Digits: srv.Address.CountryCode2Digits,
                IsResidential: !srv.Address.IsBussinessAddress, //srv.Address.IsResidential,
                CrossStreet: srv.Address.CrossStreet,
                Location: srv.Address.Location,
                ContactId: srv.Address.ContactId,
                AddressType: srv.Address.AddressType,
                LeadId: srv.LeadId,
                AccountId: srv.AccountId,
                IsInstallationAddress: srv.Address.IsInstallationAddress,
            };

            switch (srv.ParentName) {
                case 'Lead':
                    model.AssociatedSource = 1;
                    break;
                case 'Account':
                    model.AssociatedSource = 2;
                    break;
                case 'Opportunity':
                    model.AssociatedSource = 3;
                    break;
                default:
                    model.AssociatedSource = 1;

            }

            return model;
        }

        srv.deleteYes = function () {

            var url = '/api/AdditionalAddress/' + deleteAddressId;
            $http.delete(url).then(function (response) {
                 

                HFC.DisplayAlert(response.data);
                populateAddressListFromDatabase();
            }, function (response) {


            });

            $("#" + deleteDiv).modal("hide");
            deleteAddressId = 0
        }

        srv.deleteCancel = function () {
            deleteAddressId = 0
            $("#" + deleteDiv).modal("hide");
        }

        var deleteDiv = "__delete__address";

        var deleteAddressId = 0;
        srv.Delete = function (addressid) {
            deleteAddressId = addressid;
             

            var url = '/api/AdditionalAddress/' + addressid + '/CanbeDeleted';

            $http.get(url).then(function (response) {
                 

                if (response.data == false) {
                    HFC.DisplayAlert("This Address is used on at least one of the Opportunities on this Account. You must remove it from the Opportunities prior to deleting it.");
                    return
                }

                // Display the dialog to get the approval from user before deleting.
                $("#" + deleteDiv).modal("show");

            }, function (response) {
                deleteAddressId = 0;
                srv.IsBusy = false;
            });
        }

        // By default deleted notes are not included.
        var includeDeleted = false;

        srv.RefreshGrid = function (showDeleted) {
            includeDeleted = showDeleted == true ? true : false;
            srv.IsBusy = true;
            populateAddressListFromDatabase();
            srv.IsBusy = false;
        }

        srv.Recover = function (addressid) {
             
            var recoverurl = '/api/AdditionalAddress/' + addressid + '/Recover';
            $http.post(recoverurl).then(function (response) {
                HFC.DisplaySuccess("Recover success");
                populateAddressListFromDatabase();
                srv.IsBusy = false;
            }, function (response) {

                HFC.DisplayAlert(response.statusText);
                srv.IsBusy = false;
            })
        }

        // Save or update the additional contact to the database
        srv.SaveAddressTP = function (model) {
            srv.submitnte = true;

            if (model.Pop_AddtnlAddress.$invalid) {
                return;
            }
            var myDiv = $('.adres_popup');
            myDiv[0].scrollTop = 0;
            //$anchorScroll();

            var data = getModelForPost();

            validateAddress(data)
            .then(function (response) {
                var objaddress = response.data;

                if (objaddress.Status == "error") {
                    //TP-1793: CLONE - The request failed with HTTP status 503: Service Temporarily Unavailable.
                    AfterValidateAddress(data);
                    //HFC.DisplayAlert(objaddress.Message);
                    //srv.IsBusy = false;
                } else if (objaddress.Status == "true") {

                    var validAddress = objaddress.AddressResults[0];
                    data.Address1 = validAddress.address1;
                    data.Address2 = validAddress.address2;
                    data.City = validAddress.City;
                    data.State = validAddress.StateOrProvinceCode;

                    // Update that the address validated to true.
                    data.IsValidated = true;

                    // assign the zipcode back to the model before saving.
                    if (data.CountryCode2Digits == "US") {
                        data.ZipCode = validAddress.PostalCode.substring(0, 5);
                    }
                    else {
                        data.ZipCode = validAddress.PostalCode;
                    }
                    AfterValidateAddress(data);
                } else if (objaddress.Status == "false") {

                    data.IsValidated = false;

                    var errorMessage = "Invalid Address. Do you wish to continue?";
                    //dialogService.confirm(errorMessage, function (result) {
                    //    if (result == "ok") {
                    //        // TODO: need to set address validation flag.

                    //        AfterValidateAddress();
                    //    } else {
                    //        srv.IsBusy = false;
                    //    }
                    //});
                    if (confirm(errorMessage)) {
                        AfterValidateAddress(data);
                    }
                    else {
                        srv.IsBusy = false;
                    }
                } else { //SkipValidation
                    AfterValidateAddress(data);
                }
            },
            function (response) {
                HFC.DisplayAlert(response.statusText);
                srv.IsBusy = false;
            });
           
        }

        function AfterValidateAddress(model) {
            var url = "";
            if (srv.Mode == "Add") {
                url = '/api/additionalAddress/0/AddNewAddress/';
            } else if (srv.Mode == "Edit") {
                url = '/api/additionalAddress/0/UpdateAddress';
            }
             
            //code for spacing in Canada Postal/ZipCode
            if (!model.IsValidated) {
                if (model.CountryCode2Digits == "CA") {
                    if (model.ZipCode) {
                        var Ca_Zipcode = model.ZipCode;
                        Ca_Zipcode = Ca_Zipcode.substr(0, 3) + " " + Ca_Zipcode.substr(3);
                        model.ZipCode = Ca_Zipcode;
                    }
                }
            }
            //for postal code display
            if (model.CountryCode2Digits == "CA") {
                if (model.ZipCode) {
                    if (model.ZipCode.length == 6) {
                        var Ca_Zipcode = model.ZipCode;
                        Ca_Zipcode = Ca_Zipcode.substr(0, 3) + " " + Ca_Zipcode.substr(3);
                        model.ZipCode = Ca_Zipcode;
                    }
                }
            }
            
            $http.post(url, model)
                .then(function (result) {
                    
                    HFC.DisplaySuccess("Address Saved.");
                    srv.IsBusy = false;
                    $("#" + srv.divId).modal("hide");
                    srv.placeHolderText = "Required";

                    // Notify the consumer.
                    if (successCallback) successCallback(result.data.newAddressId);

                    // TODO: is it causing any error while invoked from Opportunity???
                    // Repopulate the collection.
                        populateAddressListFromDatabase();

                }, function (resultError) {
                    HFC.DisplayAlert(resultError.statusText);
                    srv.IsBusy = false;
                });
        }

        function validateAddress(model) {
            var addressModeltp = {
                address1: model.Address1,
                address2: model.Address2,
                City: model.City,
                StateOrProvinceCode: model.State,
                PostalCode: model.ZipCode,
                CountryCode: model.CountryCode2Digits
            };

            if (!addressModeltp.address1 || addressModeltp.address1 == "") {
                var result = {
                    data: {
                        Status: "SkipValidation",
                        Message: "No Address validation required."
                    },
                    status: 200,
                    statusText: "OK"
                }

                return $q.resolve(result);;
            }

            // Call the api to Return the result.
            return addressValidationService.validateAddress(addressModeltp);
        }

        // Pointer to inform the consumer, after saved sucessfully, so that
        // they can do whatever.
        var successCallback = null;

        srv.AddAddressTP = function (callbackPointer) {

            srv.placeHolderText = "Required";
            
            successCallback = callbackPointer;

            srv.Mode = "Add";
            srv.submitnte = false;
            srv.AddressIndex = -1;
            srv.Address = {};
            srv.Address.CountryCode2Digits = HFCService.FranchiseCountryCode; //"US";
            populateContactListFromDatabase();

            //// We need to initialize to empty string so that the previous value will
            //// not be used the special autofill control.
            //$rootScope.$broadcast('angucomplete-alt:clearInput', 'autocomp_');
            setAddress1Value();
             var myDiv = $('.modal-body');
            myDiv[0].scrollTop = 0;

            $("#" + srv.divId).modal("show");
        }

        function setAddress1Value(value) {
            if (!value || value == "") {
                // We need to initialize to empty string so that the previous value will
                // not be used the special autofill control.
                $rootScope.$broadcast('angucomplete-alt:clearInput', 'autocomp_');
                return;
            }

            // Need to set the initial value for the address one, because it is an 
            // autocomplete component needs a spcial handling.
            $rootScope.$broadcast('angucomplete-alt:changeInput', 'autocomp_'
                , value);
        }

        srv.EditAddressTP = function (rowIndex) {
            srv.Mode = "Edit";
            srv.submitnte = false;
            populateContactListFromDatabase();
            //srv.Initialize();
            handleDisplayAndEdit(rowIndex);

            setAddress1Value(srv.Address.Address1);
        }

        //global search
        srv.EditGlobalAddressTP = function (AddressId, LeadId, AccountId) {
            if (LeadId)
            {
                srv.ParentName = "Lead";
                srv.LeadId = LeadId;
                srv.Initialize();
            }
            else
            {
                srv.ParentName = "Account";
                srv.AccountId = AccountId;
                srv.Initialize();
            }
            srv.Address = {};
            srv.Mode = "Edit";
            srv.submitnte = false;
            populateContactListFromDatabase();
            $http.get('/api/AdditionalAddress/' + AddressId + '/GetGlobalAddresss').then(function (responseSuccess) {
                srv.AddressList = responseSuccess.data;               
                handleDisplayAndEdit(0);
                setAddress1Value(srv.Address.Address1);
            }, function (responseError) {
                HFC.DisplayAlert(responseError.statusText);
            });
           
        }

        srv.DisplayAddressTP = function (rowIndex) {
            srv.Mode = "Display";
            srv.submitnte = false;
            handleDisplayAndEdit(rowIndex);
        }

        srv.Initialize = function () {
            includeDeleted = false;
            srv.CountryList = [];
            srv.StateList = [];
            srv.AddressList = [];
            srv.Address = {};
            srv.IsBusy = true;

            //To Load Country list
            populateCountryListFromDatabase();
            //To load State list
            populateStateListFromDatabase();

            //To load the Contact List
            // This is already loaded from Adding and editting, so here it is not necessary.
            // populateContactListFromDatabase();

            srv.IsBusy = false;
        }

        srv.PopulateAddressList = function () {
            populateAddressListFromDatabase();
        }

        function handleDisplayAndEdit(rowIndex) {
            srv.AddressIndex = rowIndex;
            
            srv.Address = {};

            var existingAddress = srv.AddressList[rowIndex];
            srv.Address.AddressId = existingAddress.AddressId;
            srv.Address.AttentionText = existingAddress.AttentionText;
            srv.Address.ContactName = existingAddress.ContactName;
            srv.Address.ContactId = existingAddress.ContactId;
            srv.Address.Address1 = existingAddress.Address1;
            srv.Address.Address2 = existingAddress.Address2;
            srv.Address.City = existingAddress.City;
            srv.Address.State = existingAddress.State;
            srv.Address.ZipCode = existingAddress.ZipCode;
            srv.Address.CountryCode2Digits = existingAddress.CountryCode2Digits;
            srv.Address.IsResidential = existingAddress.IsResidential;
            srv.Address.IsValidated = existingAddress.IsValidated;
            srv.Address.FullAddress = existingAddress.FullAddress;

            // Bussiness address is just opposite of Residential
            srv.Address.IsBussinessAddress = !existingAddress.IsResidential;
            srv.Address.CrossStreet = existingAddress.CrossStreet;
            srv.Address.Location = existingAddress.Location;
            srv.Address.AddressType = existingAddress.AddressType;
            srv.Address.LeadId = existingAddress.LeadId;
            srv.Address.AccountId = existingAddress.AccountId;
            srv.Address.IsInstallationAddress = existingAddress.IsInstallationAddress;
            //required field
            if (srv.Address.ContactId) {
                srv.placeHolderText = " ";
            }
            $("#" + srv.divId).modal("show");
        }

        function populateAddressListFromDatabase() {

            if (srv.GlobalSearch)
            {
                $rootScope.$broadcast("RefreshAddressGrid", "true");
                return;
            }
            
            var geturl = getSourceUrl();
            $http.get(geturl).then(function (responseSuccess) {
                srv.AddressList = responseSuccess.data;
            }, function (responseError) {
                HFC.DisplayAlert(responseError.statusText);
            });
        }

        function populateCountryListFromDatabase() {
            var geturl = '/api/AdditionalAddress/0/GetCountry/';
            $http.get(geturl).then(function (responseSuccess) {
                srv.CountryList = responseSuccess.data;
            }, function (responseError) {
                HFC.DisplayAlert(responseError.statusText);
            });
        }

        function populateContactListFromDatabase() {
            
            var geturl = getSourceUrlForContactList();
            $http.get(geturl).then(function (responseSuccess) {
                srv.ContactList = responseSuccess.data;
            }, function (responseError) {
                HFC.DisplayAlert(responseError.statusText);
            });
        }

        function populateStateListFromDatabase() {
            var geturl = '/api/AdditionalAddress/0/GetState/';
            $http.get(geturl).then(function (responseSuccess) {
                srv.StateList = responseSuccess.data;
            }, function (responseError) {
                HFC.DisplayAlert(responseError.statusText);
            });
        }

        function getSourceUrlForContactList() {
            var temp = includeDeleted == true ? true : false;
            if (srv.ParentName == "Lead") {
                return '/api/AdditionalAddress/' + srv.LeadId + '/GetAllLeadContacts/?showDeleted=false'; //+ includeDeleted;
            }

            if (srv.ParentName == "Account") {
                return '/api/AdditionalAddress/' + srv.AccountId + '/GetAllAccountContacts/?showDeleted=false'; //+ includeDeleted;
            }
            HFC.DisplayAlert("Invalid Entity name : " + srv.ParentName + " set from the called controller");
        }

        function getSourceUrl() {
            if (srv.ParentName == "Lead") {
                return '/api/AdditionalAddress/' + srv.LeadId + '/GetLeadAddress/?showDeleted=' + includeDeleted;
            }

            if (srv.ParentName == "Account") {
                return '/api/AdditionalAddress/' + srv.AccountId + '/GetAccountAddress/?showDeleted=' + includeDeleted;
            }

            //if (srv.ParentName == "Opportunity") {
            //    return '/api/AdditionalAddress/' + srv.OpportunityId + '/GetOpportunityAddresss/';
            //}

            // Display to the user that invalid parameter is set:
            HFC.DisplayAlert("Invalid Entity name : " + srv.ParentName + " set from the called controller");
        }

        //Added for Address - State
        srv.GetStates = function (countryISO) {
            return $.grep(srv.StateList, function (s) {
                return s.CountryISO2 == countryISO;
            });
        };
        //*******************Google Maps**********************
        function addressToString(isSingleLine, model) {
            if (model) {

                var str = "";
                if (model.Address1)
                    str = model.Address1 + (model.Address2 ? (", " + model.Address2) : "") + (isSingleLine ? ", " : "<br/>");

                if (model.City)
                    str += model.City + ", " + (model.State || "") + " " + (model.ZipCode || "");
                else
                    str += (model.ZipCode || "");

                return str;
            }
        };
        srv.GetGoogleAddressHref = function (model) {
            var dest = addressToString(true, model);
            if (model = undefined) { return; }
            if (model && model.Address1) {
                var srcAddr;
                if (HFC.currentGeoLocation && HFC.Browser.isMobile()) {
                    srcAddr = HFC.currentGeoLocation.coords.latitude + "," + HFC.currentGeoLocation.coords.longitude;
                } else {
                    srcAddr = encodeURIComponent(HFC.CurrentUserAddress);
                }

                if (navigator.userAgent.toLowerCase().indexOf("iphone") == -1 && navigator.userAgent.toLowerCase().indexOf("ipad") == -1) {
                    return '//maps.google.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
                } else {
                    return '//maps.apple.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
                }
            } else
                return null;
        }

        srv.AddressToString = function (model) { return addressToString(true, model) };
        //*******************End Google Maps******************
        //CountryChanged
        srv.CountryChanged = function () {
            if (srv.Address != null) {
                srv.Address.City = '';
                srv.Address.State = '';
                srv.Address.ZipCode = '';
                srv.Address.CrossStreet = '';
            }
        }
        srv.changePlaceholder = function (id) {
            if (id)
                srv.placeHolderText = " ";
            else
                srv.placeHolderText = "Required";
        }

        //GetZipCodeMask 
        srv.GetZipCodeMask = function (countryISO) {
            var cc = $.grep(srv.CountryList, function (c) {
                return c.ISOCode2Digits == (countryISO || "US");
            });
            if (cc && cc.length)
                return cc[0].ZipCodeMask;
            else
                return "";
        };

        //ZipCodeChanged 
        srv.ZipCodeChanged = function () {
            if (srv.Address == null) { return; }
            if (srv.Address.ZipCode) {
                srv.IsBusy = true;
                $http.get('/api/lookup/0/zipcodes?q=' + srv.Address.ZipCode + "&country=" + srv.Address.CountryCode2Digits).then(function (response) {
                    srv.IsBusy = false;
                    if (response.data.zipcodes.length > 0) {
                        srv.Address.City = response.data.zipcodes[0].City;
                        srv.Address.State = response.data.zipcodes[0].State;
                    } else {
                        srv.Address.City = null;
                        srv.Address.State = null;
                    }
                }, function (response) {
                    HFC.DisplayAlert(response.statusText);
                    srv.IsBusy = false;
                });
            }

        };

        // TODO: is this used in Touchpoint??? -murugan
        srv.OnContactHover = function (id) {
            var geturl = '/api/AdditionalAddress/' + id + '/GetContact/';
            $http.get(geturl).then(function (responseSuccess) {
                srv.contactDetails = responseSuccess.data;
                $("#_contactModal_").modal("show");
            }, function (responseError) {
                HFC.DisplayAlert(responseError.statusText);
            });
        }

        srv.CancelContactPopUp = function () {

            $("#_contactModal_").modal("hide");
        }

        srv.IsBusy = false;
    }

    ])

    .controller('AddressController', ['$scope', '$http', 'AddressService', function ($scope, $http, AddressService) {

        $scope.AddressService = AddressService;
        $scope.inChangeMode = false;

        $scope.OnMouseEnter = function ($event) {
            var $element = $($event.currentTarget),
                canvas = $element.find(".mapCanvas"),
                address = AddressService.GetAddress($scope.ngModel);
            if (address && address.Address1) {
                var dest = AddressService.GetLabel(address.AddressId, true);

                var request = {
                    origin: AddressService.FranchiseAddress,
                    destination: dest,
                    travelMode: google.maps.TravelMode.DRIVING
                };
                var mapOptions = {
                    zoom: 7,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                }
                var map = new google.maps.Map(canvas.get()[0], mapOptions);
                var directionsService = new google.maps.DirectionsService(),
                    directionsDisplay = new google.maps.DirectionsRenderer();
                directionsService.route(request, function (result, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        directionsDisplay.setDirections(result);
                        directionsDisplay.setMap(map);
                        $element.data("maploaded", true);
                    }
                });

                google.maps.event.addListener(map, "click", function () {
                    if (navigator.userAgent.toLowerCase().indexOf("iphone") == -1 && navigator.userAgent.toLowerCase().indexOf("ipad") == -1) {
                        window.open('//maps.google.com?saddr=' + encodeURIComponent(AddressService.FranchiseAddress) + '&daddr=' + encodeURIComponent(dest), '_blank');
                        //return '//maps.google.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
                    } else {
                        window.open('//maps.apple.com?saddr=' + encodeURIComponent(AddressService.FranchiseAddress) + '&daddr=' + encodeURIComponent(dest), '_blank');
                        //return '//maps.apple.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
                    }
                });

                //}
                var viewHeight = window.innerHeight,
                    pos = $element.offset();
                if (pos.top + canvas.height() - window.scrollY > viewHeight)
                    canvas.css("top", -canvas.height());
                else
                    canvas.css("top", "");
                if (pos.left < canvas.width() + 20)
                    canvas.css({ right: "", left: 0 });
                else
                    canvas.css({ right: 0, left: "" });
                canvas.show();
            }
        }

        $scope.OnMouseLeave = function ($event) {
            var canvas = $($event.currentTarget).find(".mapCanvas");
            if (canvas)
                canvas.hide();
        }

        $scope.ChangeAddress = function ($event) {
            if ($scope.jobId) {
                var isBilling = $scope.heading.toLowerCase().indexOf('billing') >= 0;
                $scope.AddressService.SaveAddressChange($scope.jobId, $scope.ngModel, isBilling, function () {
                    $scope.inChangeMode = false;
                    if (isBilling === false) {
                        var adr = $scope.AddressService.GetAddress($scope.ngModel);
                        $scope.$root.$broadcast('refresh.territory', { zipCode: adr.ZipCode, jobId: $scope.jobId });
                        return;
                    }
                    //$scope.$root.$broadcast('reload.jobs');
                });
            }
        };
    }])

    // TODO: are we using this anymore in Touchpoint???? -murugan
    .directive('hfcAddress', [function () {
        return {
            restrict: 'E',
            scope: {
                ngModel: '=', //addressId                
                heading: '@',
                leadId: '@',
                jobId: '@',
                modalId: '@'
            },
            controller: 'AddressController',
            controllerAs: 'addrCtrl',
            templateUrl: '/templates/NG/address-view.html?cache-bust=' + HFC.Version,
            replace: true,
            link: function (scope, element, attrs) {
                // Setup configuration parameters                
                if (attrs.editable)
                    scope.editable = attrs.editable === 'true';
                if (attrs.deletable)
                    scope.deletable = attrs.deletable === 'true';
                if (attrs.changeable)
                    scope.changeable = attrs.changeable === 'true';
                if (attrs.index)
                    scope.index = parseInt(attrs.index);
            }
        };
    }])

    .directive('hfcAddresstp', ['$http', 'AddressValidationService','$rootScope',
        function ($http, AddressValidationService,$rootScope) {
            return {
                restrict: 'E',
                scope: {
                    addressModel: '=',
                    parentFormSubmitStatus: '=',
                    autocompleteid: '@',
                    notify: '@'
                },
                templateUrl: '/templates/NG/Hfc-addresstp.html',
                replace: true,
                link: function (scope) {
                    if (!scope.notify) scope.notify = false;
                    var geturl = '/api/AdditionalAddress/0/GetState/';
                    $http.get(geturl).then(function (response) {
                        scope.StateList = response.data;
                        scope.Get_States();
                    }, function (responseError) {
                        HFC.DisplayAlert(responseError.statusText);
                    });

                    //Added for Address - State
                    //TODO: Can we move this common service??
                    scope.getStatesTP = function (countryISO) {
                        if (!scope.StateList) return null;
                        return $.grep(scope.StateList, function (s) {
                            return s.CountryISO2 == countryISO;
                        });
                    };

                    scope.$watch('addressModel.ZipCode', function () {
                         
                        if (scope.addressModel)
                            if (scope.addressModel.ZipCode) {

                                $http.get('/api/lookup/0/zipcodes?q='
                                    + scope.addressModel.ZipCode
                                    + "&country=" + scope.addressModel.CountryCode2Digits)
                                    .then(function (response) {
                                        scope.IsBusy = false;
                                        if (response.data.zipcodes.length > 0) {
                                            scope.addressModel.City = response.data.zipcodes[0].City;
                                            scope.addressModel.State = response.data.zipcodes[0].State;
                                        };
                                    }, function (response) {
                                        HFC.DisplayAlert(response.statusText);
                                        scope.IsBusy = false;
                                    });

                                $http.get('/api/Franchise/0/GetTerritory?Zipcode='
                                   + scope.addressModel.ZipCode
                                   + "&Country=" + scope.addressModel.CountryCode2Digits)
                                   .then(function (response) {
                                       scope.IsBusy = false;
                                       scope.addressModel.Territory = response.data;

                                   });
                            }
                    });


                    scope.isValidAddress = true;
                    scope.isUserSelected = false;

                    scope.remoteUrlRequestFn = function (str) {
                        var temp = scope.addressModel.City + ", " + scope.addressModel.State;
                        return { prefix: str, prefer: temp, country: scope.addressModel.CountryCode2Digits };
                    };

                    scope.address1Changed = function (str) {
                        //$scope.console10 = str;
                        scope.addressModel.Address1 = str;
                        if (str == "") {
                            scope.isValidAddress = true;
                            return;
                        }

                        if (!scope.selectedAddress) {
                            scope.isValidAddress = false;
                            return;
                        }
                        
                    }

                    scope.notifyAddress1blur = function (str) {
                        //$scope.console10 = str;
                        
                        //alert("fired while loading");
                        if (scope.notify) $rootScope.$broadcast("AddressComponent.Blur");
                        scope.requireAddress1Focus = false;
                         //$('#autocomp__value').focus();
                    }
                    scope.requireAddress1Focus = false;
                    scope.notifyAddress1in = function (str) {
                        
                        scope.requireAddress1Focus = true;
                    }

                    scope.$watch('selectedAddress', function (newvalue, oldvalue) {
                        var obj = newvalue;
                        scope.isUserSelected = true;
                        // When the new value is undefined, exit.
                        if (!newvalue) return;

                        // Set the value based on the originalObject maintained by the
                        // autocompleting component.
                        scope.addressModel.City = newvalue.originalObject.city;
                        scope.addressModel.State = newvalue.originalObject.state;
                        scope.addressModel.Address1 = newvalue.originalObject.street_line;
                       
                        //$rootScope.$broadcast("AddressComponent.Blur");
                        if (scope.requireAddress1Focus) {
                            scope.requireAddress1Focus = false;
                            $('#autocomp__value').focus();
                        }
                        
                    });
                    scope.Get_States = function (e) {
                        scope.States_Name_Option = {
                            optionLabel: {
                                StateName: "Select",
                                StateAbv: ""
                            },
                            dataTextField: "StateName",
                            dataValueField: "StateAbv",
                            valueTemplate: '#: StateAbv# - #: StateName#',
                            template:'#: StateAbv# - #: StateName#',
                            filter: "contains",
                            valuePrimitive: true,
                            autoBind: true,
                            dataSource: {
                                transport: {
                                    read: function (e) {
                                        var data = scope.getStatesTP(scope.addressModel.CountryCode2Digits);
                                        e.success(data);
                                    }
                                }
                            }
                        };
                    }
                    scope.onSelect = function (e) {

                    }

                    scope.countries = [
                        { "name": "Afghanistan", "code": "AF" },
                        { "name": "Aland Islands", "code": "AX" },
                        { "name": "Albania", "code": "AL" },
                        { "name": "Algeria", "code": "DZ" },
                        { "name": "American Samoa", "code": "AS" },
                        { "name": "AndorrA", "code": "AD" },
                        { "name": "Angola", "code": "AO" },
                        { "name": "Anguilla", "code": "AI" },
                        { "name": "Antarctica", "code": "AQ" },
                        { "name": "Antigua and Barbuda", "code": "AG" },
                        { "name": "Argentina", "code": "AR" },
                        { "name": "Armenia", "code": "AM" },
                        { "name": "Aruba", "code": "AW" },
                        { "name": "Australia", "code": "AU" },
                        { "name": "Austria", "code": "AT" },
                        { "name": "Azerbaijan", "code": "AZ" },
                        { "name": "Bahamas", "code": "BS" },
                        { "name": "Bahrain", "code": "BH" },
                        { "name": "Bangladesh", "code": "BD" },
                        { "name": "Barbados", "code": "BB" },
                        { "name": "Belarus", "code": "BY" },
                        { "name": "Belgium", "code": "BE" },
                        { "name": "Belize", "code": "BZ" },
                        { "name": "Benin", "code": "BJ" },
                        { "name": "Bermuda", "code": "BM" },
                        { "name": "Bhutan", "code": "BT" },
                        { "name": "Bolivia", "code": "BO" },
                        { "name": "Bosnia and Herzegovina", "code": "BA" },
                        { "name": "Botswana", "code": "BW" },
                        { "name": "Bouvet Island", "code": "BV" },
                        { "name": "Brazil", "code": "BR" },
                        { "name": "British Indian Ocean Territory", "code": "IO" },
                        { "name": "Brunei Darussalam", "code": "BN" },
                        { "name": "Bulgaria", "code": "BG" },
                        { "name": "Burkina Faso", "code": "BF" },
                        { "name": "Burundi", "code": "BI" },
                        { "name": "Cambodia", "code": "KH" },
                        { "name": "Cameroon", "code": "CM" },
                        { "name": "Canada", "code": "CA" },
                        { "name": "Cape Verde", "code": "CV" },
                        { "name": "Cayman Islands", "code": "KY" },
                        { "name": "Central African Republic", "code": "CF" },
                        { "name": "Chad", "code": "TD" },
                        { "name": "Chile", "code": "CL" },
                        { "name": "China", "code": "CN" },
                        { "name": "Christmas Island", "code": "CX" },
                        { "name": "Cocos (Keeling) Islands", "code": "CC" },
                        { "name": "Colombia", "code": "CO" },
                        { "name": "Comoros", "code": "KM" },
                        { "name": "Congo", "code": "CG" },
                        { "name": "Congo, The Democratic Republic of the", "code": "CD" },
                        { "name": "Cook Islands", "code": "CK" },
                        { "name": "Costa Rica", "code": "CR" },
                        { "name": "Cote D\"Ivoire", "code": "CI" },
                        { "name": "Croatia", "code": "HR" },
                        { "name": "Cuba", "code": "CU" },
                        { "name": "Cyprus", "code": "CY" },
                        { "name": "Czech Republic", "code": "CZ" },
                        { "name": "Denmark", "code": "DK" },
                        { "name": "Djibouti", "code": "DJ" },
                        { "name": "Dominica", "code": "DM" },
                        { "name": "Dominican Republic", "code": "DO" },
                        { "name": "Ecuador", "code": "EC" },
                        { "name": "Egypt", "code": "EG" },
                        { "name": "El Salvador", "code": "SV" },
                        { "name": "Equatorial Guinea", "code": "GQ" },
                        { "name": "Eritrea", "code": "ER" },
                        { "name": "Estonia", "code": "EE" },
                        { "name": "Ethiopia", "code": "ET" },
                        { "name": "Falkland Islands (Malvinas)", "code": "FK" },
                        { "name": "Faroe Islands", "code": "FO" },
                        { "name": "Fiji", "code": "FJ" },
                        { "name": "Finland", "code": "FI" },
                        { "name": "France", "code": "FR" },
                        { "name": "French Guiana", "code": "GF" },
                        { "name": "French Polynesia", "code": "PF" },
                        { "name": "French Southern Territories", "code": "TF" },
                        { "name": "Gabon", "code": "GA" },
                        { "name": "Gambia", "code": "GM" },
                        { "name": "Georgia", "code": "GE" },
                        { "name": "Germany", "code": "DE" },
                        { "name": "Ghana", "code": "GH" },
                        { "name": "Gibraltar", "code": "GI" },
                        { "name": "Greece", "code": "GR" },
                        { "name": "Greenland", "code": "GL" },
                        { "name": "Grenada", "code": "GD" },
                        { "name": "Guadeloupe", "code": "GP" },
                        { "name": "Guam", "code": "GU" },
                        { "name": "Guatemala", "code": "GT" },
                        { "name": "Guernsey", "code": "GG" },
                        { "name": "Guinea", "code": "GN" },
                        { "name": "Guinea-Bissau", "code": "GW" },
                        { "name": "Guyana", "code": "GY" },
                        { "name": "Haiti", "code": "HT" },
                        { "name": "Heard Island and Mcdonald Islands", "code": "HM" },
                        { "name": "Holy See (Vatican City State)", "code": "VA" },
                        { "name": "Honduras", "code": "HN" },
                        { "name": "Hong Kong", "code": "HK" },
                        { "name": "Hungary", "code": "HU" },
                        { "name": "Iceland", "code": "IS" },
                        { "name": "India", "code": "IN" },
                        { "name": "Indonesia", "code": "ID" },
                        { "name": "Iran, Islamic Republic Of", "code": "IR" },
                        { "name": "Iraq", "code": "IQ" },
                        { "name": "Ireland", "code": "IE" },
                        { "name": "Isle of Man", "code": "IM" },
                        { "name": "Israel", "code": "IL" },
                        { "name": "Italy", "code": "IT" },
                        { "name": "Jamaica", "code": "JM" },
                        { "name": "Japan", "code": "JP" },
                        { "name": "Jersey", "code": "JE" },
                        { "name": "Jordan", "code": "JO" },
                        { "name": "Kazakhstan", "code": "KZ" },
                        { "name": "Kenya", "code": "KE" },
                        { "name": "Kiribati", "code": "KI" },
                        { "name": "Korea, Democratic People\"S Republic of", "code": "KP" },
                        { "name": "Korea, Republic of", "code": "KR" },
                        { "name": "Kuwait", "code": "KW" },
                        { "name": "Kyrgyzstan", "code": "KG" },
                        { "name": "Lao People\"S Democratic Republic", "code": "LA" },
                        { "name": "Latvia", "code": "LV" },
                        { "name": "Lebanon", "code": "LB" },
                        { "name": "Lesotho", "code": "LS" },
                        { "name": "Liberia", "code": "LR" },
                        { "name": "Libyan Arab Jamahiriya", "code": "LY" },
                        { "name": "Liechtenstein", "code": "LI" },
                        { "name": "Lithuania", "code": "LT" },
                        { "name": "Luxembourg", "code": "LU" },
                        { "name": "Macao", "code": "MO" },
                        { "name": "Macedonia, The Former Yugoslav Republic of", "code": "MK" },
                        { "name": "Madagascar", "code": "MG" },
                        { "name": "Malawi", "code": "MW" },
                        { "name": "Malaysia", "code": "MY" },
                        { "name": "Maldives", "code": "MV" },
                        { "name": "Mali", "code": "ML" },
                        { "name": "Malta", "code": "MT" },
                        { "name": "Marshall Islands", "code": "MH" },
                        { "name": "Martinique", "code": "MQ" },
                        { "name": "Mauritania", "code": "MR" },
                        { "name": "Mauritius", "code": "MU" },
                        { "name": "Mayotte", "code": "YT" },
                        { "name": "Mexico", "code": "MX" },
                        { "name": "Micronesia, Federated States of", "code": "FM" },
                        { "name": "Moldova, Republic of", "code": "MD" },
                        { "name": "Monaco", "code": "MC" },
                        { "name": "Mongolia", "code": "MN" },
                        { "name": "Montserrat", "code": "MS" },
                        { "name": "Morocco", "code": "MA" },
                        { "name": "Mozambique", "code": "MZ" },
                        { "name": "Myanmar", "code": "MM" },
                        { "name": "Namibia", "code": "NA" },
                        { "name": "Nauru", "code": "NR" },
                        { "name": "Nepal", "code": "NP" },
                        { "name": "Netherlands", "code": "NL" },
                        { "name": "Netherlands Antilles", "code": "AN" },
                        { "name": "New Caledonia", "code": "NC" },
                        { "name": "New Zealand", "code": "NZ" },
                        { "name": "Nicaragua", "code": "NI" },
                        { "name": "Niger", "code": "NE" },
                        { "name": "Nigeria", "code": "NG" },
                        { "name": "Niue", "code": "NU" },
                        { "name": "Norfolk Island", "code": "NF" },
                        { "name": "Northern Mariana Islands", "code": "MP" },
                        { "name": "Norway", "code": "NO" },
                        { "name": "Oman", "code": "OM" },
                        { "name": "Pakistan", "code": "PK" },
                        { "name": "Palau", "code": "PW" },
                        { "name": "Palestinian Territory, Occupied", "code": "PS" },
                        { "name": "Panama", "code": "PA" },
                        { "name": "Papua New Guinea", "code": "PG" },
                        { "name": "Paraguay", "code": "PY" },
                        { "name": "Peru", "code": "PE" },
                        { "name": "Philippines", "code": "PH" },
                        { "name": "Pitcairn", "code": "PN" },
                        { "name": "Poland", "code": "PL" },
                        { "name": "Portugal", "code": "PT" },
                        { "name": "Puerto Rico", "code": "PR" },
                        { "name": "Qatar", "code": "QA" },
                        { "name": "Reunion", "code": "RE" },
                        { "name": "Romania", "code": "RO" },
                        { "name": "Russian Federation", "code": "RU" },
                        { "name": "RWANDA", "code": "RW" },
                        { "name": "Saint Helena", "code": "SH" },
                        { "name": "Saint Kitts and Nevis", "code": "KN" },
                        { "name": "Saint Lucia", "code": "LC" },
                        { "name": "Saint Pierre and Miquelon", "code": "PM" },
                        { "name": "Saint Vincent and the Grenadines", "code": "VC" },
                        { "name": "Samoa", "code": "WS" },
                        { "name": "San Marino", "code": "SM" },
                        { "name": "Sao Tome and Principe", "code": "ST" },
                        { "name": "Saudi Arabia", "code": "SA" },
                        { "name": "Senegal", "code": "SN" },
                        { "name": "Serbia and Montenegro", "code": "CS" },
                        { "name": "Seychelles", "code": "SC" },
                        { "name": "Sierra Leone", "code": "SL" },
                        { "name": "Singapore", "code": "SG" },
                        { "name": "Slovakia", "code": "SK" },
                        { "name": "Slovenia", "code": "SI" },
                        { "name": "Solomon Islands", "code": "SB" },
                        { "name": "Somalia", "code": "SO" },
                        { "name": "South Africa", "code": "ZA" },
                        { "name": "South Georgia and the South Sandwich Islands", "code": "GS" },
                        { "name": "Spain", "code": "ES" },
                        { "name": "Sri Lanka", "code": "LK" },
                        { "name": "Sudan", "code": "SD" },
                        { "name": "Suriname", "code": "SR" },
                        { "name": "Svalbard and Jan Mayen", "code": "SJ" },
                        { "name": "Swaziland", "code": "SZ" },
                        { "name": "Sweden", "code": "SE" },
                        { "name": "Switzerland", "code": "CH" },
                        { "name": "Syrian Arab Republic", "code": "SY" },
                        { "name": "Taiwan, Province of China", "code": "TW" },
                        { "name": "Tajikistan", "code": "TJ" },
                        { "name": "Tanzania, United Republic of", "code": "TZ" },
                        { "name": "Thailand", "code": "TH" },
                        { "name": "Timor-Leste", "code": "TL" },
                        { "name": "Togo", "code": "TG" },
                        { "name": "Tokelau", "code": "TK" },
                        { "name": "Tonga", "code": "TO" },
                        { "name": "Trinidad and Tobago", "code": "TT" },
                        { "name": "Tunisia", "code": "TN" },
                        { "name": "Turkey", "code": "TR" },
                        { "name": "Turkmenistan", "code": "TM" },
                        { "name": "Turks and Caicos Islands", "code": "TC" },
                        { "name": "Tuvalu", "code": "TV" },
                        { "name": "Uganda", "code": "UG" },
                        { "name": "Ukraine", "code": "UA" },
                        { "name": "United Arab Emirates", "code": "AE" },
                        { "name": "United Kingdom", "code": "GB" },
                        { "name": "United States", "code": "US" },
                        { "name": "United States Minor Outlying Islands", "code": "UM" },
                        { "name": "Uruguay", "code": "UY" },
                        { "name": "Uzbekistan", "code": "UZ" },
                        { "name": "Vanuatu", "code": "VU" },
                        { "name": "Venezuela", "code": "VE" },
                        { "name": "Vietnam", "code": "VN" },
                        { "name": "Virgin Islands, British", "code": "VG" },
                        { "name": "Virgin Islands, U.S.", "code": "VI" },
                        { "name": "Wallis and Futuna", "code": "WF" },
                        { "name": "Western Sahara", "code": "EH" },
                        { "name": "Yemen", "code": "YE" },
                        { "name": "Zambia", "code": "ZM" },
                        { "name": "Zimbabwe", "code": "ZW" }
                    ];
                }
            }
        }
    ])

    .directive('hfcAdditionalAddressModal', [
        'AdditionalAddressServiceTP', function (AdditionalAddressServiceTP) {
            return {
                restrict: 'E',
                scope: {
                    modalId: '@'
                },
                templateUrl: '/templates/NG/LinkedList/Hfc-additional-address-modal.html',
                replace: true,
                link: function (scope) {
                    scope.AdditionalAddressServiceTP = AdditionalAddressServiceTP;
                }
            }
        }
    ])

        .directive('hfcAdditionalAddress', [
        'AdditionalAddressServiceTP', function (AdditionalAddressServiceTP) {
            return {
                restrict: 'E',
                scope: {
                    modalId: '@',
                    isEdit:'@'
                },
                templateUrl: '/templates/NG/LinkedList/Hfc-additional-address.html',
                replace: true,
                link: function (scope) {
                    scope.IsEdit = true;
                    scope.AdditionalAddressServiceTP = AdditionalAddressServiceTP;
                    if (scope.isEdit === "true")
                        scope.IsEdit = true;
                    else
                        scope.IsEdit = false;
                }
            }
        }
        ])

        .directive('hfcContactOverlay', ['AdditionalAddressServiceTP', '$http'
                , function (AdditionalAddressServiceTP,$http) {
                    return {
                        restrict: 'A',
                        scope: {
                            con: '='
                        },
                        //templateUrl: '/templates/NG/LinkedList/Hfc-additional-contact-display.html',
                        replace: true,
                        link: function (scope, element, attrs) {
                            scope.AdditionalAddressServiceTP = AdditionalAddressServiceTP;
                            $(element).hoverIntent({
                                over: function () {
                                    //
                                    var contact = scope.con;
                                    var canvas = $(element).find(".contactCanvas");

                                    // TODO: We need to find how this works in Stage
                                    if (!contact.ContactId) return

                                    var geturl = '/api/AdditionalAddress/' + contact.ContactId + '/GetContact/';
                                    $http.get(geturl).then(function (responseSuccess) {
                                        var contactDetails = responseSuccess.data;
                                        var text = $("#_contactModal_").html();
                                       
                                        text = text.replace('First_Name', contactDetails.FirstName);
                                        text = text.replace('Last_Name', contactDetails.LastName);
                                        text = text.replace('Company_Name', contactDetails.CompanyName);
                                        text = text.replace('Title_', contactDetails.WorkTitle);
                                        text = text.replace('Cell_Phone', contactDetails.CellPhone);
                                        text = text.replace('Home_Phone', contactDetails.HomePhone);
                                        text = text.replace('Work_Phone', contactDetails.WorkPhone);
                                        text = text.replace('Work_Phone_Ext', contactDetails.WorkPhoneExt);
                                        text = text.replace('Primary_Email', contactDetails.PrimaryEmail);
                                        text = text.replace('Secondary_Email', contactDetails.SecondaryEmail);
                                        //text = text.replace('IsReceiveEmails', contactDetails.IsReceiveEmails);
                                       
                                        canvas.html(text);
                                        if (contactDetails.IsReceiveEmails) {
                                            $("#receive_promotion_emails").attr('checked', 'checked');
                                        }

                                        if (contactDetails.PreferredTFN == 'H') {
                                            $("#Home_Phone_Pre").attr('checked', 'checked');
                                        }
                                        else if (contactDetails.PreferredTFN == 'W') {
                                            $("#Work_Phone_Pre").attr('checked', 'checked');
                                        }
                                        else if (contactDetails.PreferredTFN == 'C') {
                                            $("#Cell_Phone_Pre").attr('checked', 'checked');
                                        }
                                        $(".phone_Format").text(function (i, text) {
                                            text = text.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
                                            return text;
                                        });
                                    }, function (responseError) {
                                        HFC.DisplayAlert(responseError.statusText);
                                    });
                                   
                                    
                                   
                                   
                                    var viewHeight = window.innerHeight,
                                                                    pos = $(element).offset();
                                    if (pos.top + canvas.height() - window.scrollY > viewHeight)
                                        canvas.css("top", -canvas.height());
                                    else
                                        canvas.css("top", "");
                                    if (pos.left < canvas.width() + 20)
                                        canvas.css({ right: "", left: 0 });
                                    else
                                        canvas.css({ right: 0, left: "" });

                                    canvas.show();
                                    //$("#_contactModal_").modal("show");
                                },
                                out: function () {
                                    var canvas = $(element).find(".contactCanvas");
                                    canvas.html('<div></div>');
                                    canvas.hide();
                                    //$(element).find(".contactCanvas2").hide();
                                    //$("#_contactModal_").modal("hide");
                                },
                                interval: 300
                            });
                        }
                    }
                }
        ])

        .directive('hfcStatusesOverlay', ['$http'
                , function ($http) {
                    return {
                        restrict: 'A',
                        scope: {
                            con: '='
                        },
                        replace: true,
                        link: function (scope, element, attrs) {
                          
						  $(element).on('touchstart', function(){
								var canvas = $(element).find(".statusCanvas");
								var text = $("#_statusModal_").html();
								canvas.html(text);
								var viewHeight = window.innerHeight,pos = $(element).offset();
								if (pos.top + canvas.height() - window.scrollY > viewHeight)
									canvas.css("top", -canvas.height());
								else
									canvas.css("top", "");
								if (pos.left < canvas.width() + 20)
									canvas.css({ right: "", left: 0 });
								else
									canvas.css({ right: 0, left: "" });

								$('#ipadtooltipclosebutton').on('touchstart', function(){
									canvas = $(element).find(".statusCanvas");
									canvas.hide();
								});
							});
							
                            $(element).hoverIntent({
                                over: function () {
                                  
                                    //var contact = scope.con;
                                    var canvas = $(element).find(".statusCanvas");
                                    var text = $("#_statusModal_").html();
                                    canvas.html(text);
                                    var viewHeight = window.innerHeight,pos = $(element).offset();
                                    if (pos.top + canvas.height() - window.scrollY > viewHeight)
                                        canvas.css("top", -canvas.height());
                                    else
                                        canvas.css("top", "");
                                    if (pos.left < canvas.width() + 20)
                                        canvas.css({ right: "", left: 0 });
                                    else
                                        canvas.css({ right: 0, left: "" });

                                    canvas.show();
                                    //$("#_contactModal_").modal("show");
                                },
                                out: function () {
                                    $(element).find(".statusCanvas").hide();
                                    //$("#_contactModal_").modal("hide");
                                },
                                interval: 300
                            });
                        }
                    }
                }
        ])

        .directive('hfcStatusesOverlay1', ['$http'
                , function ($http) {
                    return {
                        restrict: 'A',
                        scope: {
                            con: '='
                        },
                        replace: true,
                        link: function (scope, element, attrs) {

							$(element).on('touchstart', function(){
								var canvas = $(element).find(".statusCanvas");
                                    var text = $("#_statusModal_").html();
                                    canvas.html(text);
                                    var viewHeight = window.innerHeight, pos = $(element).offset();
                                   
                                    if (pos.left < canvas.width() + 20)
                                        canvas.css({ right: "", left: 0 });
                                    else
                                        canvas.css({ right: 0, left: "" });

								$('#ipadtooltipclosebutton').on('touchstart', function(){
									canvas = $(element).find(".statusCanvas");
									canvas.hide();
								});
							});
							
                            $(element).hoverIntent({
                                over: function () {

                                    //var contact = scope.con;
                                    var canvas = $(element).find(".statusCanvas");
                                    var text = $("#_statusModal_").html();
                                    canvas.html(text);
                                    var viewHeight = window.innerHeight, pos = $(element).offset();
                                   
                                    if (pos.left < canvas.width() + 20)
                                        canvas.css({ right: "", left: 0 });
                                    else
                                        canvas.css({ right: 0, left: "" });

                                    canvas.show();
                                    //$("#_contactModal_").modal("show");
                                },
                                out: function () {
                                    $(element).find(".statusCanvas").hide();
                                    //$("#_contactModal_").modal("hide");
                                },
                                interval: 300
                            });
                        }
                    }
                }
        ])

        .directive('hfcStatusesOverlay2', ['$http'
                , function ($http) {
                    return {
                        restrict: 'A',
                        scope: {
                            con: '='
                        },
                        replace: true,
                        link: function (scope, element, attrs) {

							$(element).on('touchstart', function(){
								var canvas = $(element).find(".statusCanvas");
                                    var text = $("#_statusModal_").html();
                                    canvas.html(text);
                                    var viewHeight = window.innerHeight, pos = $(element).offset();
                                   
                                    if (pos.left < canvas.width() + 20)
                                        canvas.css({ right: "", left: 0 });
                                    else
                                        canvas.css({ right: 0, left: "" });

								$('#ipadtooltipclosebutton').on('touchstart', function(){
									canvas = $(element).find(".statusCanvas");
									canvas.hide();
								});
							});
							
                            $(element).hoverIntent({
                                over: function () {

                                    //var contact = scope.con;
                                    var canvas = $(element).find(".statusCanvas");
                                    var text = $("#_statusModal_").html();
                                    canvas.html(text);
                                    var viewHeight = window.innerHeight, pos = $(element).offset();
                                   
                                    if (pos.left < canvas.width() + 20)
                                        canvas.css({ right: "", left: 0 });
                                    else
                                        canvas.css({ right: 0, left: "" });

                                    canvas.show();
                                    //$("#_contactModal_").modal("show");
                                },
                                out: function () {
                                    $(element).find(".statusCanvas").hide();
                                    //$("#_contactModal_").modal("hide");
                                },
                                interval: 300
                            });
                        }
                    }
                }
        ])

    .directive('hfcAddressModal', ['AddressService', 'cacheBustSuffix', function (AddressService, cacheBustSuffix) {
        return {
            restrict: 'E',
            scope: {
                modalId: '@',
                leadId: '@'
            },
            templateUrl: '/templates/NG/address-modal.html?cache-bust=' + cacheBustSuffix,
            replace: true,
            link: function (scope) {

                scope.AddressService = AddressService;
            }
        }
    }])

    .directive('hfcGooglemaps', [function () {
        return {
            restrict: 'A',
            scope: { addr: '=' },
            replace: true,
            link: function (scope, element, attrs) {
				
				$(element).on('touchstart', function(){
								var addr = scope.addr;
                        var dest = "";
                        if (typeof (addr) == "string")
                            dest = addr;
                        else if (addr && addr.Address1)
                            dest = addr.Address1 + ", " + addr.City + " " + addr.State + " " + addr.ZipCode;
                        else if (addr)
                            dest = addr.City + " " + addr.State + " " + addr.ZipCode;
                        if (HFC.CurrentUserAddress && HFC.CurrentUserAddress.length > 0 && dest != null && dest.length > 0) {
                            var canvas = $(element).find(".mapCanvas");
                            // if ($(element).data("maploaded") == null)
                            {

                                var request = {
                                    origin: HFC.FranchiseAddress,
                                    destination: dest,
                                    travelMode: google.maps.TravelMode.DRIVING
                                };

                                var mapOptions = {
                                    zoom: 7,
                                    mapTypeId: google.maps.MapTypeId.ROADMAP
                                }

                                var map = new google.maps.Map(canvas.get()[0], mapOptions);

                                var directionsService = new google.maps.DirectionsService(),
                                    directionsDisplay = new google.maps.DirectionsRenderer();
                                directionsService.route(request, function (result, status) {
                                    if (status == google.maps.DirectionsStatus.OK) {
                                        directionsDisplay.setDirections(result);
                                        directionsDisplay.setMap(map);
                                        $(element).data("maploaded", true);
                                    }
                                });
                                google.maps.event.addListener(map, "click", function () {
                                    if (navigator.userAgent.toLowerCase().indexOf("iphone") == -1 && navigator.userAgent.toLowerCase().indexOf("ipad") == -1) {
                                        window.open('//maps.google.com?saddr=' + encodeURIComponent(HFC.FranchiseAddress) + '&daddr=' + encodeURIComponent(dest), '_blank');
                                    } else {
                                        window.open('//maps.apple.com?saddr=' + encodeURIComponent(HFC.FranchiseAddress) + '&daddr=' + encodeURIComponent(dest), '_blank');
                                    }
                                });
                                //google.maps.event.addListener(map, "click", function () {
                                //    window.open('//maps.apple.com?saddr=' + HFC.FranchiseAddress + '&daddr=' + encodeURIComponent(dest), '_blank');
                                //});
                            }
                            var viewHeight = window.innerHeight,
                                pos = $(element).offset();
                            if (pos.top + canvas.height() - window.scrollY > viewHeight)
                                canvas.css("top", -canvas.height());
                            else
                                canvas.css("top", "");
                            if (pos.left < canvas.width() + 20)
                                canvas.css({ right: "", left: 0 });
                            else
                                canvas.css({ right: 0, left: "" });
                        }

								$('#ipadtooltipclosebutton').on('touchstart', function(){
									canvas = $(element).find(".mapCanvas");
									canvas.hide();
								});
				});
				
                $(element).hoverIntent({
                    over: function () {
                        
                        var addr = scope.addr;
                        var dest = "";
                        if (typeof (addr) == "string")
                            dest = addr;
                        else if (addr && addr.Address1)
                            dest = addr.Address1 + ", " + addr.City + " " + addr.State + " " + addr.ZipCode;
                        else if (addr)
                            dest = addr.City + " " + addr.State + " " + addr.ZipCode;
                        if (HFC.CurrentUserAddress && HFC.CurrentUserAddress.length > 0 && dest != null && dest.length > 0) {
                            var canvas = $(element).find(".mapCanvas");
                            // if ($(element).data("maploaded") == null)
                            {

                                var request = {
                                    origin: HFC.FranchiseAddress,
                                    destination: dest,
                                    travelMode: google.maps.TravelMode.DRIVING
                                };

                                var mapOptions = {
                                    zoom: 7,
                                    mapTypeId: google.maps.MapTypeId.ROADMAP
                                }

                                var map = new google.maps.Map(canvas.get()[0], mapOptions);

                                var directionsService = new google.maps.DirectionsService(),
                                    directionsDisplay = new google.maps.DirectionsRenderer();
                                directionsService.route(request, function (result, status) {
                                    if (status == google.maps.DirectionsStatus.OK) {
                                        directionsDisplay.setDirections(result);
                                        directionsDisplay.setMap(map);
                                        $(element).data("maploaded", true);
                                    }
                                });
                                google.maps.event.addListener(map, "click", function () {
                                    if (navigator.userAgent.toLowerCase().indexOf("iphone") == -1 && navigator.userAgent.toLowerCase().indexOf("ipad") == -1) {
                                        window.open('//maps.google.com?saddr=' + encodeURIComponent(HFC.FranchiseAddress) + '&daddr=' + encodeURIComponent(dest), '_blank');
                                    } else {
                                        window.open('//maps.apple.com?saddr=' + encodeURIComponent(HFC.FranchiseAddress) + '&daddr=' + encodeURIComponent(dest), '_blank');
                                    }
                                });
                                //google.maps.event.addListener(map, "click", function () {
                                //    window.open('//maps.apple.com?saddr=' + HFC.FranchiseAddress + '&daddr=' + encodeURIComponent(dest), '_blank');
                                //});
                            }
                            var viewHeight = window.innerHeight,
                                pos = $(element).offset();
                            if (pos.top + canvas.height() - window.scrollY > viewHeight)
                                canvas.css("top", -canvas.height());
                            else
                                canvas.css("top", "");
                            if (pos.left < canvas.width() + 20)
                                canvas.css({ right: "", left: 0 });
                            else
                                canvas.css({ right: 0, left: "" });
                            canvas.show();
                        }
                    },
                    out: function () {
                        $(element).find(".mapCanvas").hide();
                    },
                    interval: 300
                });
            }
        };
    }])

    .directive('hfcAddressForm', ['cacheBustSuffix', function (cacheBustSuffix) {
        return {
            restrict: 'E',
            scope: true,
            templateUrl: '/templates/NG/address-edit-form.html?cache-bust=' + cacheBustSuffix,
            replace: true
        }
    }]);

})(window, document);