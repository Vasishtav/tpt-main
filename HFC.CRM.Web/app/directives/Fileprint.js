﻿
'use strict';
app.directive('hfcFileprintModal', [
       'FileprintService', function (FileprintService) {
           return {
               restrict: 'E',
               scope: {
                   modalId: '@'
               },
               templateUrl: '/templates/NG/Fileprint/Fileprint-modal.html',
               replace: true,
               link: function (scope) {
                   scope.FileprintService = FileprintService;
               }
           }
       }
])
   .directive('hfcFileprintForm', [
       function () {
           return {
               restrict: 'E',
               scope: true,
               templateUrl: '/templates/NG/Fileprint/Fileprint-edit-form.html',
               replace: true,
               
           }
       }
   ])