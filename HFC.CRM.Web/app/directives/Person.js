﻿/// <reference path="D:\Ram\HFC\HFC.CRM.Web\Templates/NG/Calendar/calendar-modal.html" />
'use strict';

app.directive('hfcPerson', [
        function() {
            return {
                restrict: 'E',
                scope: {
                    ngModel: '=',
                    heading: '@',
                    modalId: '@',
                    index: '@',
                    leadId: '@',
                    jobId: '@',
                    secondary: '@'
                },
                controller: 'PersonController',
                templateUrl: '/templates/NG/person-view.html',
                replace: true,
                link: function(scope, element, attrs) {
                    // Setup configuration parameters
                    scope.heading = scope.heading || personConfig.heading;
                    if (attrs.editable)
                        scope.editable = attrs.editable === 'true';
                    if (attrs.changeable)
                        scope.changeable = attrs.changeable === 'true';

                    if (attrs.secondary) {
                        scope.secondary = attrs.secondary === 'true';
                    }
                }
            };
        }
    ])
    .directive('hfcPersonBase', [
        'PersonService', function(PersonService) {
            return {
                restrict: 'E',
                scope: true,
                templateUrl: '/templates/NG/person-base.html?cache-bust=' + HFC.Version,
                replace: true,
                link: function(scope) {
                    scope.PersonService = PersonService;
                }
            }
        }
    ])
    .directive('hfcPersonModal', [
        'PersonService', function(PersonService) {
            return {
                restrict: 'E',
                scope: {
                    modalId: '@'
                },
                templateUrl: '/templates/NG/person-modal.html',
                replace: true,
                link: function(scope) {
                    scope.PersonService = PersonService;
                }
            }
        }
    ])
    .directive('hfcPersonForm', [
        function() {
            return {
                restrict: 'E',
                scope: true,
                templateUrl: '/templates/NG/person-edit-form.html',
                replace: true
            }
        }
    ])
    .directive('hfcPersonFormNew', [
        function () {
            return {
                restrict: 'E',
                scope: true,
                templateUrl: '/templates/NG/person-edit-form-new.html',
                replace: true
            }
        }
    ])
    .directive('hfcPersonFormFile', [
        function () {
            return {
                restrict: 'E',
                scope: true,
                templateUrl: '/templates/NG/person-edit-form-new-file.html',
                replace: true,
               
            }
        }
    ])
     .directive('hfcPersonModalGo', [
        'PersonService', 'HFCService', function (PersonService, HFCService) {
            return {
                restrict: 'E',
                scope: {
                    modalId: '@'
                },
                templateUrl: '/templates/NG/person-modal-new-go.html',
                replace: true,
                link: function (scope) {
                    scope.PersonService = PersonService;
                    scope.HFCService = HFCService;
                }
            }
        }
     ])
    .directive('hfcPersonFormGo', [
        function () {
            return {
                restrict: 'E',
                scope: true,
                templateUrl: '/templates/NG/person-edit-form-new-go.html',
                replace: true,

            }
        }
    ])
 .directive('hfcPersonModalSource', [
        'PersonService', function (PersonService) {
            return {
                restrict: 'E',
                scope: {
                    modalId: '@'
                },
                templateUrl: '/templates/NG/person-modal-new-source.html',
                replace: true,
                link: function (scope) {
                    scope.PersonService = PersonService;
                }
            }
        }
 ])
    .directive('hfcPersonFormSource', [
        function () {
            return {
                restrict: 'E',
                scope: true,
                templateUrl: '/templates/NG/person-edit-form-new-source.html',
                replace: true,

            }
        }
    ])
;
