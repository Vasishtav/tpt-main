﻿
'use strict';
//app.directive('hfcAccount', [
//        function () {
//            return {
//                restrict: 'E',
//                scope: {
//                    ngModel: '=',
//                    heading: '@',
//                    modalId: '@',
//                    index: '@',
//                    leadId: '@',
//                    jobId: '@',
//                    secondary: '@'
//                },
//                controller: 'AccountController',
//                templateUrl: '/templates/NG/account-view.html',
//                replace: true,
//                link: function (scope, element, attrs) {
//                    // Setup configuration parameters
//                    scope.heading = scope.heading || AccountConfig.heading;
//                    if (attrs.editable)
//                        scope.editable = attrs.editable === 'true';
//                    if (attrs.changeable)
//                        scope.changeable = attrs.changeable === 'true';

//                    if (attrs.secondary) {
//                        scope.secondary = attrs.secondary === 'true';
//                    }
//                }
//            };
//        }
//])
//    .directive('hfcAccountBase', [
//        'AccountService', function (AccountService) {
//            return {
//                restrict: 'E',
//                scope: true,
//                templateUrl: '/templates/NG/account-base.html?cache-bust=' + HFC.Version,
//                replace: true,
//                link: function (scope) {
//                    scope.AccountService = AccountService;
//                }
//            }
//        }
//    ])
app.directive('hfcAccountModal', [
       'AccountService', function (AccountService) {
           return {
               restrict: 'E',
               scope: {
                   modalId: '@'
               },
               templateUrl: '/templates/NG/account-modal.html',
               replace: true,
               link: function (scope) {
                   scope.AccountService = AccountService;
               }
           }
       }
])
   .directive('hfcAccountForm', [
       function () {
           return {
               restrict: 'E',
               scope: true,
               templateUrl: '/templates/NG/account-edit-form.html',
               replace: true,
               
           }
       }
   ])