﻿ 

app.directive('hfcQuotePanel', [
    'QuoteService', 'QuoteItemService', 'QuoteSaleAdjService', 'PrintService', 'EmailService', 'InvoiceService', 'OrderService', 'PaymentService', 'cacheBustSuffix', 'JobService', 'HFCService',
    function(QuoteService, QuoteItemService, QuoteSaleAdjService, PrintService, EmailService, InvoiceService, OrderService, PaymentService, cacheBustSuffix, JobService, HFCService) {
        return {
            restrict: 'E',
            scope: true,
            require: '^hfcJobPanel',
            templateUrl: '/templates/NG/Job/Brand/BB/job-quote-pane.html?cache-bust=' + cacheBustSuffix,
            replace: true,
            link: function(scope) {
                scope.QuoteService = QuoteService;
                scope.QuoteItemService = QuoteItemService;
                scope.QuoteSaleAdjService = QuoteSaleAdjService;
                scope.PrintService = PrintService;
                scope.EmailService = EmailService;
                //scope.ESignatureService = ESignatureService;
                scope.InvoiceService = InvoiceService;
                scope.OrderService = OrderService;
                scope.PaymentService = PaymentService;
                scope.JobService = JobService;

                scope.dragControlListeners = {
                    orderChanged: function(event) {
                        scope.QuoteItemService.UpdateSortOrder(scope.quote);
                    }
                }

                scope.isSolatechEnabled = function() {
                    return HFCService.FranchiseIsSolaTechEnabled;
                }

                scope.OrderBtnTitle = function() {
                    if (!scope.isSolatechEnabled()) return "You have not been setup to place orders through MyCRM. Please call Tech Support for more information 800-578-7058"
                    if (!scope.IsOrderValid()) return "Order now";
                    return "";
                    //return "Order is not valid";
                }

                scope.IsOrderValid = function() {
                    if (scope.quote.IsPrimary) {
                        var customItems = $.grep(scope.quote.JobItems, function(item) {
                            return item.isCustom == false && item.LastUpdated
                        });

                        var notOrderedItems = $.grep(scope.quote.JobItems, function(item) {
                            return item.isCustom == false && scope.JobService.OrderedJobItemIds.indexOf(item.JobItemId) == -1 && item.LastUpdated;
                        });

                        return customItems.length == 0 || notOrderedItems.length == 0;
                    } else {
                        return true;
                    }
                }
            }
        };
    }
]).directive('quoteItemRow', [
    'QuoteItemService', 'QuoteSaleAdjService', 'cacheBustSuffix', function(QuoteItemService, QuoteSaleAdjService, cacheBustSuffix) {
        return {
            restrict: 'E',
            controller: 'QuoteLineController',
            templateUrl: '/templates/NG/Job' + HFC.Util.BrandedPath + '/quote-item-row.html?cache-bust=' + cacheBustSuffix,
            replace: true,
            link: function(scope, element, attrs) {
                if (attrs.canUpdate)
                    scope.canUpdate = attrs.canUpdate === 'true';
                scope.DisplayDiscount = scope.item.DiscountAmount > 0;
                scope.QuoteItemService = QuoteItemService;
                scope.QuoteSaleAdjService = QuoteSaleAdjService;

                function remove(item) {
                    var index = scope.quote.JobItems.indexOf(item);
                    if (index > 0) {
                        scope.quote.JobItems.splice(index, 1);
                    } else if (index === 0) {
                        scope.quote.JobItems.shift();
                    }
                };

                function init() {
                    if (scope.InEditMode) {
                        if (scope.IsCustom) {
                            QuoteItemService.initCustomData(function() {
                            });
                        } else {
                            QuoteItemService.initService(0, 0, function() {
                            });
                        }
                    }
                    if (QuoteSaleAdjService.SaleAdjCategories.length == 0) {
                        QuoteSaleAdjService.GetLookups();
                    }
                }

                scope.RoomLocation = function() {
                    if (!scope.item || !scope.item.Options) {
                        return;
                    }

                    for (var i = 0; i < scope.item.Options.length; i++) {

                        if (scope.item.Options[i].Name === "Room Location") {
                            return scope.item.Options[i].Value;
                        }
                    }
                }

                scope.CalculateSubtotal = function() {
                    var qty = parseInt(scope.item.Quantity) || 0,
                        sale = parseFloat(scope.item.SalePrice) || 0,
                        lineTotal = parseFloat(scope.item.Subtotal) || 0,
                        discount = parseFloat(scope.item.DiscountAmount) || 0;

                    /*    percent = parseFloat(scope.item.DiscountPercent) / 100.0,
                    discount = 0;

                // force recalculate discount
                if (scope.item.DiscountType == "Percent") {
                    if (qty > 0 && sale > 0)
                        discount = qty * sale * percent;

                    scope.item.DiscountAmount = HFC.evenRound(discount, 4);
                }

                discount = parseFloat(scope.item.DiscountAmount) || 0;*/
                    var cost = parseFloat(scope.item.UnitCost) || 0,
                        subtotal = lineTotal,
                        costSubtotal = qty * cost,
                        margin = ((sale - cost) / sale) * 100.0;

                    scope.item.Subtotal = HFC.evenRound(subtotal, 4);
                    scope.item.CostSubtotal = HFC.evenRound(costSubtotal, 4);
                    scope.item.MarginPercent = HFC.evenRound(margin, 4);

                    scope.CalculateQuoteTotal();
                };

                scope.View = function($event, orderHasBeenSent) {
                    if (!QuoteItemService.InEditMode) {
                        scope.OriginalItem = angular.copy(scope.item);
                        scope.InEditMode = QuoteItemService.InEditMode = true;
                        scope.InViewMode = QuoteItemService.InViewMode = true;
                        scope.OrderHasBeenSent = QuoteItemService.OrderHasBeenSent = true;

                        QuoteItemService.Edit(scope.OriginalItem, false);

                        scope.QtyChanged();
                    } else {
                        alert('Can only view 1 line item.');
                    }

                    $event.preventDefault();
                };

                scope.Edit = function($event) {
                    if (!QuoteItemService.InEditMode) {
                        scope.OriginalItem = angular.copy(scope.item);
                        scope.InEditMode = QuoteItemService.InEditMode = true;
                        QuoteItemService.Edit(scope.OriginalItem, false);

                        scope.QtyChanged();
                    } else {
                        alert('Can only edit 1 line item.');
                    }

                    $event.preventDefault();
                };

                scope.Clone = function($event) {
                    $event.preventDefault();

                    if (!QuoteItemService.InEditMode) {
                        if (scope.quote) {
                            var clone = angular.copy(scope.item);
                            clone.JobItemId = 0;
                            clone.InEditMode = QuoteItemService.InEditMode = true;

                            scope.quote.JobItems.push(clone);
                            QuoteItemService.Edit(scope.item, true);
                        }
                    } else {
                        alert('Can only edit 1 line item.');
                    }
                };

                scope.Cancel = function() {
                    if (scope.item.JobItemId)
                        angular.copy(scope.OriginalItem, scope.item);
                    else
                        remove(scope.item);

                    scope.CalculateQuoteTotal();
                    scope.OriginalItem = null;
                    scope.InEditMode = QuoteItemService.InEditMode = false;
                };

                scope.Delete = function($event, isOrderSent, isOrdered) {
                    $event.preventDefault();

                    var doDelete = function() {
                        QuoteItemService.Delete(scope.item, scope.quote, function(data) {
                            scope.quote.LastUpdated = data.LastUpdated;
                            remove(scope.item);
                            scope.CalculateQuoteTotal();
                        });
                    };

                    var ask = function(message) {
                        bootbox.dialog({
                            message: message,
                            title: "Warning",
                            buttons: {
                                danger: {
                                    label: "Yes, delete",
                                    className: "btn-danger",
                                    callback: function() {
                                        doDelete();
                                    }
                                },
                                main: {
                                    label: "Cancel",
                                    className: "btn-default",
                                    callback: function() {

                                    }
                                }
                            }
                        });
                    };

                    if (isOrderSent) {
                        ask(window.localization.POSentDeleteConfirmation);
                    } else if (isOrdered) {
                        ask(window.localization.POOrderedDeleteConfirmation);
                    } else if (confirm("Do you want to continue?")) {
                        QuoteItemService.Delete(scope.item, scope.quote, function(data) {
                            scope.quote.LastUpdated = data.LastUpdated;
                            remove(scope.item);
                            scope.CalculateQuoteTotal();
                        });
                    }
                };

                scope.Save = function(addNew, form) {

                    

                    if (form && !form.$valid) {
                        return;
                    }

                    var doSave = function () {
                        
                        QuoteItemService.Save(scope.item, scope.quote, QuoteItemService.ProductOptions.Messages, function(data) {


                            if (!scope.item.JobItemId && scope.item.JobItemId != data.JobItemId) {
                                scope.item.JobItemId = data.JobItemId;
                                scope.item.LastUpdated = data.LastUpdated;
                            }

                            if (data.quoteDate) {
                                scope.job.QuoteDateUTC = data.quoteDate;
                            }

                            scope.quote.LastUpdated = data.LastUpdated;
                            scope.InEditMode = QuoteItemService.InEditMode = false;
                            scope.OriginalItem = null;

                            if (addNew)
                                QuoteItemService.AddCustom(scope.quote);

                            scope.CalculateQuoteTotal();
                        });
                    };


                    var messages = [];
                    for (var i = 0; i < QuoteItemService.ProductOptions.Options.length; i++) {
                        var option = QuoteItemService.ProductOptions.Options[i];

                        if (option.HasError()) {
                            messages.push("The " + option.Name + " for this product must be between " + option.MinValue + " and " + option.MaxValue + ".");
                        }
                    }

                   doSave();
                };

                scope.GetCategory = function(categoryId) {
                    var cat = $.grep(QuoteItemService.ProductCategories, function(c) {
                        return c.Id == categoryId;
                    });

                    if (cat.length)
                        return cat[0].Category;
                    else
                        return "";
                };

                scope.GetOption = function(optname) {
                    var opt = $.grep(scope.item.Options, function(o) {
                        return o.Name === optname;
                    });
                    if (opt.length > 0)
                        return opt[0];
                    else
                        return {};
                };

                scope.UpdateManfacturers = function(productTypeId) {
                    QuoteItemService.GetManufacturers(productTypeId);
                };

                scope.UpdateProductList = function(manufacturerid) {
                    QuoteItemService.GetProductList(manufacturerid);
                };

                scope.UpdateProductStyles = function(productGuid) {
                    QuoteItemService.GetProductStyles(productGuid);
                };

                scope.DiscountPercentBlur = function() {
                    if (!scope.item.DiscountPercent) {
                        scope.item.DiscountPercent = 0;
                        scope.DiscountPercentChanged();
                    }
                }

                scope.DiscountPercentChanged = function(reset) {
                    if (!reset) {
                        if (!scope.item.DiscountPercent) {
                            scope.item.DiscountType = null;
                        } else {
                            scope.item.DiscountType = 'Percent';
                        }
                    }


                    var qty = parseInt(scope.item.Quantity || 0),
                        jobberPrice = parseFloat(scope.item.JobberPrice),
                        unitDiscount = jobberPrice * parseFloat(scope.item.DiscountPercent || 0) / 100,
                        salePrice = jobberPrice - unitDiscount;

                    scope.item.SalePrice = HFC.evenRound(salePrice, 2);
                    scope.item.UnitDiscountAmount = HFC.evenRound(unitDiscount, 2);
                    scope.item.Subtotal = HFC.evenRound(salePrice * qty, 2);
                    scope.item.DiscountAmount = HFC.evenRound(unitDiscount * qty, 2);

                    scope.CalculateSubtotal();
                };

                scope.ResetDiscountPercent = function() {
                    scope.item.DiscountType = null;
                    scope.item.DiscountPercent = 0;
                    scope.DiscountPercentChanged(true);
                }

                scope.LineDiscountBlur = function() {
                    if (!scope.item.DiscountAmount) {
                        scope.item.DiscountAmount = 0;
                        scope.LineDiscountChanged();
                    }
                }

                scope.LineDiscountChanged = function(reset) {
                    if (!reset) {
                        if (!scope.item.DiscountAmount) {
                            scope.item.DiscountType = null;
                        } else {
                            scope.item.DiscountType = 'Amount';
                        }
                    }

                    var qty = parseInt(scope.item.Quantity),
                        jobberPrice = parseFloat(scope.item.JobberPrice),
                        unitDiscount = scope.item.DiscountAmount / qty,
                        salePrice = parseFloat(scope.item.JobberPrice) - unitDiscount;


                    scope.item.UnitDiscountAmount = HFC.evenRound(unitDiscount, 2);
                    scope.item.SalePrice = HFC.evenRound(salePrice, 2);
                    scope.item.Subtotal = HFC.evenRound(salePrice * qty, 2);
                    scope.item.DiscountPercent = HFC.evenRound(100 - (salePrice / jobberPrice * 100), 2);

                    if (scope.item.DiscountPercent < 0) {
                        scope.item.DiscountPercent = 0;
                    }

                    scope.CalculateSubtotal();
                };

                scope.ResetLineDiscount = function() {
                    scope.item.DiscountType = null;
                    scope.item.DiscountAmount = 0;
                    scope.LineDiscountChanged(true);
                }

                scope.SalePriceBlur = function() {
                    if (!scope.item.SalePrice) {
                        scope.item.SalePrice = 0;
                        scope.SalePriceChanged();
                    }
                }

                scope.SalePriceChanged = function(reset) {
                    if (!reset) {
                        scope.item.DiscountType = 'SalePrice';
                    }

                    var qty = parseInt(scope.item.Quantity),
                        salePrice = parseFloat(scope.item.SalePrice || 0),
                        jobberPrice = parseFloat(scope.item.JobberPrice),
                        unitDiscount = jobberPrice - salePrice,
                        lineDiscount = qty * unitDiscount,
                        discountPercent = 100 - (salePrice / jobberPrice * 100);

                    scope.item.UnitDiscountAmount = HFC.evenRound(unitDiscount, 2);
                    if (scope.item.UnitDiscountAmount < 0) {
                        scope.item.UnitDiscountAmount = 0;
                    }

                    scope.item.Subtotal = HFC.evenRound(salePrice * qty, 2);

                    scope.item.DiscountAmount = HFC.evenRound(lineDiscount, 2);

                    if (scope.item.DiscountAmount < 0) {
                        scope.item.DiscountAmount = 0;
                    }

                    scope.item.DiscountPercent = HFC.evenRound(discountPercent, 2);
                    if (scope.item.DiscountPercent < 0) {
                        scope.item.DiscountPercent = 0;
                    }

                    scope.CalculateSubtotal();
                };

                scope.ResetSalePrice = function() {
                    scope.item.DiscountType = null;
                    scope.item.SalePrice = parseFloat(scope.item.JobberPrice);
                    scope.SalePriceChanged(true);
                }

                scope.UnitCostCodeChanged = function(reset) {
                    scope.ResetSalePrice();
                    scope.CalculateSubtotal();
                };

                scope.QtyBlur = function() {
                    if (!scope.item.Quantity || parseFloat(scope.item.Quantity) == 0) {
                        scope.item.Quantity = 1;
                        scope.QtyChanged();
                    }
                }

                scope.QtyChanged = function() {
                    var qty = parseInt(scope.item.Quantity || 0),
                        salePrice = parseFloat(scope.item.SalePrice),
                        jobberPrice = parseFloat(scope.item.JobberPrice),
                        lineDiscount = parseFloat(scope.item.DiscountAmount),
                        unitDiscount;

                    if (scope.item.DiscountType == "Amount") {
                        unitDiscount = lineDiscount / qty;
                        scope.item.UnitDiscountAmount = HFC.evenRound(unitDiscount, 2);

                        if (scope.item.UnitDiscountAmount < 0) {
                            scope.item.UnitDiscountAmount = 0;
                        }

                        scope.item.SalePrice = HFC.evenRound(jobberPrice - unitDiscount, 2);
                        scope.item.DiscountPercent = HFC.evenRound(100 - (salePrice / jobberPrice * 100), 2);

                    } else if (scope.item.DiscountType == "SalePrice") {
                        unitDiscount = jobberPrice - salePrice;
                        scope.item.DiscountAmount = HFC.evenRound(unitDiscount * qty, 2);

                        if (scope.item.DiscountAmount < 0) {
                            scope.item.DiscountAmount = 0;
                        }

                        scope.item.UnitDiscountAmount = HFC.evenRound(unitDiscount);

                        if (scope.item.UnitDiscountAmount < 0) {
                            scope.item.UnitDiscountAmount = 0;
                        }


                    } else if (scope.item.DiscountType == "Percent") {

                        scope.DiscountPercentChanged();
                    }


                    scope.item.Subtotal = HFC.evenRound(parseFloat(scope.item.SalePrice) * qty, 2);

                    scope.CalculateSubtotal();
                };

                scope.DiscountAmountChanged = function() {
                    if (!scope.item.DiscountAmount) {
                        scope.item.DiscountType = null;
                    } else {
                        scope.item.DiscountType = 'Amount';
                    }

                    var qty = parseInt(scope.item.Quantity),
                        sale = parseFloat(scope.item.SalePrice),
                        discount = parseFloat(scope.item.DiscountAmount),
                        percent = 0;
                    if (qty > 0 && sale > 0)
                        percent = HFC.evenRound((discount / (qty * sale)) * 100.0, 4);

                    scope.item.DiscountPercent = HFC.evenRound(percent, 2);
                    scope.CalculateSubtotal();
                };
                init(); //retrieve all the necessary lookup objects
            }
        }
    }
]).directive('quoteSaleadjRow', [
    'QuoteSaleAdjService', 'cacheBustSuffix', function(QuoteSaleAdjService, cacheBustSuffix) {
        return {
            restrict: 'E',
            controller: 'QuoteLineController',
            templateUrl: '/templates/NG/Job/quote-saleadj-row.html?cache-bust=' + cacheBustSuffix,
            replace: true,
            link: function(scope, element, attrs) {
                if (attrs.canUpdate)
                    scope.canUpdate = attrs.canUpdate === 'true';
                scope.QuoteSaleAdjService = QuoteSaleAdjService;

                function remove(item) {
                    var index = scope.quote.SaleAdjustments.indexOf(item);
                    if (index > 0) {
                        scope.quote.SaleAdjustments.splice(index, 1);
                    } else if (index === 0) {
                        scope.quote.SaleAdjustments.shift();
                    }
                };

                scope.Edit = function($event) {

                    scope.OriginalItem = angular.copy(scope.item);
                    scope.InEditMode = true;
                    $event.preventDefault();
                };

                scope.Clone = function($event) {
                    $event.preventDefault();

                    if (scope.quote) {
                        var clone = angular.copy(scope.item);
                        clone.SaleAdjustmentId = 0;
                        clone.InEditMode = true;
                        scope.quote.SaleAdjustments.push(clone);
                    }
                };

                scope.Cancel = function() {
                    if (scope.item.SaleAdjustmentId)
                        angular.copy(scope.OriginalItem, scope.item);
                    else
                        remove(scope.item);

                    scope.CalculateQuoteTotal();
                    scope.OriginalItem = null;
                    scope.InEditMode = false;
                };

                scope.Delete = function($event) {
                    $event.preventDefault();

                    if (confirm("Do you want to continue?"))
                        scope.QuoteSaleAdjService.Delete(scope.item, scope.quote, function(data) {
                            scope.quote.LastUpdated = data.LastUpdated;
                            remove(scope.item);
                            scope.CalculateQuoteTotal();
                        });
                };

                scope.Save = function(form) {
                    if (form && !form.$valid) {
                        return;
                    }

                    scope.QuoteSaleAdjService.Save(scope.item, scope.quote, function(data) {
                        if (!scope.item.SaleAdjustmentId && scope.item.SaleAdjustmentId != data.SaleAdjustmentId) {
                            scope.item.SaleAdjustmentId = data.SaleAdjustmentId;
                        }
                        scope.quote.LastUpdated = data.LastUpdated;
                        scope.item.LastUpdated = data.LastUpdated;
                        scope.InEditMode = false;
                        scope.OriginalItem = null;

                        scope.CalculateQuoteTotal();
                    });
                };

                scope.CategoryChanged = function() {

                    var cat = $.grep(scope.QuoteSaleAdjService.SaleAdjCategories, function(c) {
                        return c.Category === scope.item.Category;
                    });
                    if (cat && cat.length) {
                        scope.item.TypeEnum == scope.QuoteSaleAdjService.SaleAdjTypeEnum[cat[0].Label];
                        scope.item.Percent = parseFloat(cat[0].Value);
                    }
                }
            }
        }
    }
]);




    

   