﻿



app.directive('hfcQuotePanel', [
        'QuoteService', 'QuoteItemService', 'QuoteSaleAdjService', 'PrintService', 'EmailService', 'InvoiceService', 'PaymentService', 'cacheBustSuffix',
        function(QuoteService, QuoteItemService, QuoteSaleAdjService, PrintService, EmailService, InvoiceService, PaymentService, cacheBustSuffix) {
            return {
                restrict: 'E',
                scope: true,
                require: '^hfcJobPanel',
                templateUrl: '/templates/NG/Job/Brand/CC/job-quote-pane.html?cache-bust=' + cacheBustSuffix,
                replace: true,
                link: function(scope) {
                    scope.QuoteService = QuoteService;
                    scope.QuoteItemService = QuoteItemService;
                    scope.QuoteSaleAdjService = QuoteSaleAdjService;
                    scope.PrintService = PrintService;
                    scope.EmailService = EmailService;
                    scope.InvoiceService = InvoiceService;
                    scope.PaymentService = PaymentService;

                    scope.dragControlListeners = {
                        orderChanged: function(event) {
                            scope.QuoteItemService.UpdateSortOrder(scope.quote);
                        }
                    }
                }
            };
        }
    ])
    
    .directive('quoteItemRow', [
        'QuoteItemService', 'QuoteSaleAdjService', 'cacheBustSuffix', function(QuoteItemService, QuoteSaleAdjService, cacheBustSuffix) {
            return {
                restrict: 'E',
                controller: 'QuoteLineController',
                templateUrl: '/templates/NG/Job' + HFC.Util.BrandedPath + '/quote-item-row.html?cache-bust=' + cacheBustSuffix,
                replace: true,
                link: function(scope, element, attrs) {
                    if (attrs.canUpdate)
                        scope.canUpdate = attrs.canUpdate === 'true';
                    scope.DisplayDiscount = scope.item.DiscountAmount > 0;
                    scope.QuoteItemService = QuoteItemService;
                    scope.QuoteSaleAdjService = QuoteSaleAdjService;

                    function remove(item) {
                        var index = scope.quote.JobItems.indexOf(item);
                        if (index > 0) {
                            scope.quote.JobItems.splice(index, 1);
                        } else if (index === 0) {
                            scope.quote.JobItems.shift();
                        }
                    };

                    function init() {
                        if (scope.InEditMode && QuoteItemService.ProductCategories.length == 0) {
                            QuoteItemService.GetCategories();
                            //gets initial product list by current category id
                            QuoteItemService.GetProductList(scope.item.CategoryId);
                        }
                        if (QuoteSaleAdjService.SaleAdjCategories.length == 0) {
                            QuoteSaleAdjService.GetLookups();
                        }
                    }

                    scope.CalculateSubtotal = function() {
                        var qty = parseInt(scope.item.Quantity) || 0,
                            sale = parseFloat(scope.item.SalePrice) || 0,
                            percent = parseFloat(scope.item.DiscountPercent) / 100.0,
                            discount = 0;

                        // force recalculate discount
                        if (scope.item.DiscountType == "Percent") {
                            if (qty > 0 && sale > 0)
                                discount = qty * sale * percent;

                            scope.item.DiscountAmount = HFC.evenRound(discount, 4);
                        }

                        discount = parseFloat(scope.item.DiscountAmount) || 0;
                        var cost = parseFloat(scope.item.UnitCost) || 0,
                            subtotal = (qty * sale) - discount,
                            costSubtotal = qty * cost,
                            margin = ((sale - cost) / sale) * 100.0;

                        scope.item.Subtotal = HFC.evenRound(subtotal, 4);
                        scope.item.CostSubtotal = HFC.evenRound(costSubtotal, 4);
                        scope.item.MarginPercent = HFC.evenRound(margin, 4);

                        scope.CalculateQuoteTotal();
                    };

                    scope.Edit = function($event) {

                        scope.OriginalItem = angular.copy(scope.item);
                        scope.InEditMode = true;
                        init();
                        QuoteItemService.Edit(scope.OriginalItem, false);

                        $event.preventDefault();
                    };

                    scope.Clone = function($event) {
                        $event.preventDefault();
                        if (scope.quote) {
                            var clone = angular.copy(scope.item);
                            clone.JobItemId = 0;
                            clone.InEditMode = true;
                            scope.quote.JobItems.push(clone);
                        }
                    };

                    scope.Cancel = function() {
                        if (scope.item.JobItemId)
                            angular.copy(scope.OriginalItem, scope.item);
                        else
                            remove(scope.item);

                        scope.CalculateQuoteTotal();
                        scope.OriginalItem = null;
                        scope.InEditMode = false;
                    };

                    scope.Delete = function($event) {
                        $event.preventDefault();

                        if (confirm("Do you want to continue?"))
                            QuoteItemService.Delete(scope.item, scope.quote, function(data) {
                                scope.quote.LastUpdated = data.LastUpdated;
                                remove(scope.item);
                                scope.CalculateQuoteTotal();
                            });
                    };

                    scope.Save = function(addNew) {
                        QuoteItemService.Save(scope.item, scope.quote, function(data) {
                            if (!scope.item.JobItemId && scope.item.JobItemId != data.JobItemId) {
                                scope.item.JobItemId = data.JobItemId;
                                scope.item.LastUpdated = data.LastUpdated;
                            }

                            if (data.quoteDate) {
                                scope.job.QuoteDateUTC = data.quoteDate;
                            }
                            if (scope)
                                scope.quote.LastUpdated = data.LastUpdated;
                            scope.InEditMode = false;
                            scope.OriginalItem = null;

                            if (addNew)
                                QuoteItemService.Add(scope.quote);

                            scope.CalculateQuoteTotal();
                        });
                    };

                    scope.GetCategory = function(categoryId) {
                        var cat = $.grep(QuoteItemService.ProductCategories, function(c) {
                            return c.Id == categoryId;
                        })
                        if (cat.length)
                            return cat[0].Category;
                        else
                            return "";
                    };

                    scope.GetOption = function(optname) {
                        var opt = $.grep(scope.item.Options, function(o) {
                            return o.Name === optname;
                        });
                        if (opt.length > 0)
                            return opt[0];
                        else
                            return {};
                    };

                    scope.DiscountPercentChanged = function() {
                        if (!scope.item.DiscountPercent) {
                            scope.item.DiscountType = null;
                        } else {
                            scope.item.DiscountType = 'Percent';
                        }

                        var qty = parseInt(scope.item.Quantity),
                            sale = parseFloat(scope.item.SalePrice),
                            percent = parseFloat(scope.item.DiscountPercent) / 100.0,
                            discount = 0;
                        if (qty > 0 && sale > 0)
                            discount = qty * sale * percent

                        scope.item.DiscountAmount = HFC.evenRound(discount, 4);
                        scope.CalculateSubtotal();
                    };

                    scope.DiscountAmountChanged = function() {
                        if (!scope.item.DiscountAmount) {
                            scope.item.DiscountType = null;
                        } else {
                            scope.item.DiscountType = 'Amount';
                        }

                        var qty = parseInt(scope.item.Quantity),
                            sale = parseFloat(scope.item.SalePrice),
                            discount = parseFloat(scope.item.DiscountAmount),
                            percent = 0;
                        if (qty > 0 && sale > 0)
                            percent = HFC.evenRound((discount / (qty * sale)) * 100.0, 4);

                        scope.item.DiscountPercent = HFC.evenRound(percent, 2);
                        scope.CalculateSubtotal();
                    };

                    init(); //retrieve all the necessary lookup objects
                }
            }
        }
    ])
    .directive('quoteSaleadjRow', [
        'QuoteSaleAdjService', 'cacheBustSuffix', function(QuoteSaleAdjService, cacheBustSuffix) {
            return {
                restrict: 'E',
                controller: 'QuoteLineController',
                templateUrl: '/templates/NG/Job/quote-saleadj-row.html?cache-bust=' + cacheBustSuffix,
                replace: true,
                link: function(scope, element, attrs) {
                    if (attrs.canUpdate)
                        scope.canUpdate = attrs.canUpdate === 'true';
                    scope.QuoteSaleAdjService = QuoteSaleAdjService;

                    function remove(item) {
                        var index = scope.quote.SaleAdjustments.indexOf(item);
                        if (index > 0) {
                            scope.quote.SaleAdjustments.splice(index, 1);
                        } else if (index === 0) {
                            scope.quote.SaleAdjustments.shift();
                        }
                    };

                    scope.Edit = function($event) {
                        scope.OriginalItem = angular.copy(scope.item);
                        scope.InEditMode = true;
                        $event.preventDefault();
                    };

                    scope.Clone = function($event) {
                        $event.preventDefault();

                        if (scope.quote) {
                            var clone = angular.copy(scope.item);
                            clone.SaleAdjustmentId = 0;
                            clone.InEditMode = true;
                            scope.quote.SaleAdjustments.push(clone);
                        }
                    };

                    scope.Cancel = function() {
                        if (scope.item.SaleAdjustmentId)
                            angular.copy(scope.OriginalItem, scope.item);
                        else
                            remove(scope.item);

                        scope.CalculateQuoteTotal();
                        scope.OriginalItem = null;
                        scope.InEditMode = false;
                    };

                    scope.Delete = function($event) {
                        $event.preventDefault();

                        if (confirm("Do you want to continue?"))
                            scope.QuoteSaleAdjService.Delete(scope.item, scope.quote, function(data) {
                                scope.quote.LastUpdated = data.LastUpdated;
                                remove(scope.item);
                                scope.CalculateQuoteTotal();
                            });
                    };

                    scope.Save = function(form) {
                        if (form && !form.$valid) {
                            return;
                        }
                        scope.QuoteSaleAdjService.Save(scope.item, scope.quote, function(data) {
                            if (!scope.item.SaleAdjustmentId && scope.item.SaleAdjustmentId != data.SaleAdjustmentId) {
                                scope.item.SaleAdjustmentId = data.SaleAdjustmentId;
                            }

                            scope.item.LastUpdated = data.LastUpdated;
                            scope.quote.LastUpdated = data.LastUpdated;
                            scope.InEditMode = false;
                            scope.OriginalItem = null;

                            scope.CalculateQuoteTotal();
                        });
                    };

                    scope.CategoryChanged = function() {
                        var cat = $.grep(scope.QuoteSaleAdjService.SaleAdjCategories, function(c) {
                            return c.Category === scope.item.Category;
                        });
                        if (cat && cat.length) {
                            scope.item.TypeEnum == scope.QuoteSaleAdjService.SaleAdjTypeEnum[cat[0].Label];
                            scope.item.Percent = parseFloat(cat[0].Value);
                        }
                    }
                }
            }
        }
    ]);
