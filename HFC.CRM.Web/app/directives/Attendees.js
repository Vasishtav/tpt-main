﻿
'use strict';
app.directive('hfcAttendeeModal', [
       'AttendeeService', function (AttendeeService) {
           return {
               restrict: 'E',
               scope: {
                   modalId: '@'
               },
               templateUrl: '/templates/NG/Attendee/Attendees-modal.html',
               replace: true,
               link: function (scope) {
                   scope.AttendeeService = AttendeeService;
               }
           }
       }
])
   .directive('hfcAttendeeForm', [
       function () {
           return {
               restrict: 'E',
               scope: true,
               templateUrl: '/templates/NG/Attendee/Attendees-edit-form.html',
               replace: true,
               
           }
       }
   ])