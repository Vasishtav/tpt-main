﻿
(function (window, document, undefined) {
    'use strict';
    angular.module('hfc.reportModule', [])
        .directive('reportheaderComponent', [
            'ReportHeaderService', function (ReportHeaderService) {
                return {
                    restrict: 'E',
                    templateUrl: '/templates/NG/ReportHeader.html',
                    link: function (scope) {
                        scope.ReportHeaderService = ReportHeaderService;
                    }
                }
            }
        ])
        .service('ReportHeaderService', ['HFCService', '$http', '$q', '$rootScope', '$sce', '$timeout', '$window', function (HFCService, $http, $q, $rootScope, $sce, $timeout, $window) {
            var srv = this;
            srv.BrandId = HFCService.CurrentBrand;
            srv.ShowRawdata = true;
            if (/iPad|iPhone/.test(navigator.userAgent)) {
                srv.ShowRawdata = false;
            }

            srv.gotohome = function () {
                $window.location.href = "#!/Reports";
            }

            //srv.dropdownFunction = function (element) {
            //    var dropdowns = document.getElementsByClassName("dropdown-content");
            //    for (var i = 0; i < dropdowns.length; i++) {
            //        if(dropdowns[i].id!=element)
            //        dropdowns[i].classList.remove('show');
            //    }
            //    document.getElementById(element).classList.toggle("show");
            //}
            
            //window.onclick = function (event) {
            //    if (!event.target.matches('.dropbtn')) {
            //        var dropdowns = document.getElementsByClassName("dropdown-content");
            //        var i;
            //        for (i = 0; i < dropdowns.length; i++) {
            //            var openDropdown = dropdowns[i];
            //            if (openDropdown.classList.contains('show')) {
            //                openDropdown.classList.remove('show');
            //            }
            //        }
            //    }
            //}
        }]);
})(window, document);