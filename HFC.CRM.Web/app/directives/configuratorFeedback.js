﻿app
    .directive('hfcConfiguratorFeedback', [
        'ConfiguratorFeedbackService', function (ConfiguratorFeedback) {
            return {
                restrict: 'E',
                scope: {
                    modalId: '@'
                },
                templateUrl: '/app/views/configurator/configuratorFeedback.html',
                replace: true,
                link: function (scope, element) {
                    scope.ConfiguratorFeedback = ConfiguratorFeedback;
                    angular.element(document).ready(function () {
                        $('#__configuratorFeedbackModal__').on('shown.bs.modal', function () {
                            // scope.GetConfiguratorData();                           
                        });
                    });
                }
            }
        }
    ])
    .service('ConfiguratorFeedbackService', [
        '$http', '$window', '$location', 'HFCService',
        'kendoService', 'kendotooltipService',
        function ($http, $window, $location, HFCService,
            kendoService, kendotooltipService) {
            var scope = this;
            scope.HFCService = HFCService;
            //scope.BrandId = $scope.HFCService.CurrentBrand;

            //scope.modalState = {
            //    loaded: true
            //}

            //scope.$watch('modalState.loaded', function () {
            //    var loadingElement = document.getElementById("loading");

            //    if (!scope.modalState.loaded) {
            //        loadingElement.style.display = "block";
            //    } else {
            //        loadingElement.style.display = "none";
            //    }
            //});

            scope.Configurator = {
                ErrorTypeId: '',
                PICVendor: '',
                ProductPICJson: '',
                VendorId: '',
                ProductGroupId: '',
                Vendor: '',
                ProductCategory: '',
                RecordTypeId: '',
                Details: '',
                CurrentUser: '',
                CurrentDateTime: '',
                TouchpointURL: '',
                TouchpointVersion: '',
                BrowserDetails: '',
                PICJson: '',
                StatusId: 12000,
                CaseType: 'Feedback',
                CaseNumber: '',
                SelectedModel: '',
                FieldErrors: ''
            };
            var successCallback = null;
            var digestCallback = null;
            scope.SaveClicked = false;

            scope.ShowConfigurator = function (CalculatePopupClosed, receiveDigestCallback) {
                successCallback = CalculatePopupClosed;
                digestCallback = receiveDigestCallback;
                scope.ClearConfiguratorData();
                scope.GetConfiguratorData();
                scope.ShowPopup('__configuratorFeedbackModal__');
                //$("#configuratorFeedbackModal").modal("show");            
            }

            scope.ClearConfiguratorData = function () {
                scope.Configurator.RecordTypeId = 0;
                scope.Configurator.Details = "";
                $('textarea[name="Details"]').removeClass("required-error");
            };

            scope.ErrorTypeChanged = function () {
                if (scope.Configurator.SelectedModel == undefined || scope.Configurator.SelectedModel == "") {
                    scope.Message = "Please select Product and Model in Configurator";
                }
            }

            scope.GetConfiguratorData = function () {
                //if(scope.Configurator!=null &&  scope.Configurator!==''&& scope.Configurator!=undefined)
                //{
                scope.Configurator.CurrentUser = HFCService.CurrentUser.FullName;
                scope.Configurator.CurrentDateTime = HFCService.KendoDateTimeFormat(new Date());
                scope.Configurator.BrowserDetails = navigator.userAgent;
                scope.Configurator.TouchpointURL = window.location.href;
                //}   
                scope.GetTPVersion();
                scope.GetErrorTypes();
            };

            scope.GetTPVersion = function () {
                $http.post('/api/Case/0/GetTPVersion').then(function (res) {
                    if (res.data != null) {
                        scope.Configurator.TouchpointVersion = res.data;
                    }
                });
            }

            scope.GetErrorTypes = function () {
                //if (scope.Configurator.ErrorTypeId != 0) {
                $http.get("/api/Case/0/GetFeedbackChangeLookup?TableName=ErrorType").then(function (res) {
                    //console.log(res);
                    scope.ErrorTypes = res.data;
                });
                //}
            };

            scope.ShowPopup = function (modalId) {

                $("#" + modalId).modal("show");
            }

            //cancel the pop-up
            scope.CancelPopup = function (modalId) {
                $("#" + modalId).modal("hide");
                scope.SaveClicked = false;
                if (successCallback) successCallback();
            };

            scope.GetValue = function () {
                var ErrorTypeId = scope.Configurator.RecordTypeId;
                for (var i = 0; i < scope.ErrorTypes.length; i++) {
                    if (scope.ErrorTypes[i].Id == ErrorTypeId) {
                        scope.ErrorName = scope.ErrorTypes[i].Name;
                        break;
                    }
                }
            };

            scope.SaveConfiguratorFeedback = function () {
                scope.SaveClicked = true;
                if (scope.ValidateControls()) {

                    //scope.modalState.loaded = true;
                    scope.Configurator.CaseNumber = "PER-" + scope.Configurator.PICVendor;
                    scope.GetValue();
                    if (scope.ErrorName == "Missing product") {
                        scope.Configurator.PICJson = scope.Configurator.ProductPICJson;
                    } else if (scope.Configurator.SelectedModel == 0) {
                        HFC.DisplayAlert("Please select Product and Model in configurator");
                        return false;
                    };
                    scope.SaveFeedbackData();
                    scope.SaveClicked = false;
                }

            };

            scope.ValidateControls = function () {
                if (scope.Configurator.RecordTypeId == 0) {
                    return false;
                }
                if (scope.Configurator.Details == null || scope.Configurator.Details == "") {
                    $('textarea[name="Details"]').addClass("required-error");
                    return false;
                } else {
                    $('textarea[name="Details"]').removeClass("required-error");
                    return true;
                }
                return true;
            };

            scope.RemoveRequiredClass = function (controlId) {
                var cId = controlId.currentTarget.name;
                $("textarea[name=" + cId + "]").removeClass("required-error");
            }

            scope.SaveFeedbackData = function () {
                //scope.modalState.loaded = false;
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "block";
                var dto = scope.Configurator;
                $http.post('/api/Case', dto).then(function (response) {
                    //console.log(response.data);
                    if (response.data != null) {
                        var result = response.data.result;
                        scope.Configurator.CaseNumber = result.CaseNumber;
                        scope.CancelPopup('__configuratorFeedbackModal__');
                        scope.ShowPopup('configuratorFeedbackConfirmModal');
                        // scope.modalState.loaded = true;
                    }
                    loadingElement.style.display = 'none';
                }).catch(function (error) { //scope.modalState.loaded = true; 
                    loadingElement.style.display = 'none';
                });
            };

        }]);