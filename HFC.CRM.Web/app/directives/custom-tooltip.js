﻿app.directive('customTooltipDirective', [
    function () {
        return {
            restrict: 'E',
            scope: {
                tooltipEmail: '=',
                tooltipPhone: '=',
                displayFlag: '='
            },
            templateUrl: '/templates/NG/customTooltipTemplate.html',
            replace: true,
            link: function (scope) {
            }
        };
    }
]);