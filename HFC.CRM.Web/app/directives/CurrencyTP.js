﻿'use strict'
var blurToCurrency = angular.module('blurToCurrencyModule', [])
blurToCurrency.directive('blurToCurrency', function ($filter) {
    return {
        scope: {
            control: '=',
            amount: '='

        },
        replace: true,
        link: function (scope, el, attrs) {
            //
            el.val($filter('currency')(scope.amount));

            el.bind('focus', function () {

                el.val(scope.amount);
            });

            el.bind('input', function () {

                scope.amount = el.val();
                scope.$apply();
            });

            el.bind('blur', function () {

                el.val($filter('currency')(scope.amount));
            });
            scope.internalControl = scope.control || {};
            scope.internalControl.CurrenctFormatControl = function (value) {

                scope.amount = value;
                el.val($filter('currency')(scope.amount));
            }

        }
    }
});
