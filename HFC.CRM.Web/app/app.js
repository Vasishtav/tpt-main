﻿/// <reference path="views/FranchiseCaseMgmt/FranchiseCaseVendorView.html" />
/// <reference path="views/FranchiseCaseMgmt/FranchiseCaseVendorView.html" />
/// <reference path="views/FranchiseCaseMgmt/FranchiseCaseVendorView.html" />
/// <reference path="views/Quickbook/QuickbookSetup.html" />
/// <reference path="views/Quickbook/QuickbookSetup.html" />
/// <reference path="views/Quickbook/QuickbookSetup.html" />
/// <reference path="views/FranchiseCaseMgmt/FranchiseCaseList.html" />
/// <reference path="views/FranchiseCaseMgmt/FranchiseCaseList.html" />
/// <reference path="views/FranchiseCaseMgmt/FranchiseCaseList.html" />
/***************************************************************************\
Module Name:  app.js
Project: HFC-Tochpoint
Created on: 15 Oct 2017
Created By:
Copyright:
Description: Global routing providers
Change History:
Date  By  Description

\***************************************************************************/

var app = angular.module('HFC_NGApp', ['commonConfiguratorService', 'unsavedChanges', 'ngRoute', 'ui.mask', 'ngMessages', 'ui.format'
    , 'hfc.address', 'ui.select2', 'summernote', 'ui.mask'
    , 'ui.helpers', 'treeControl', 'angular-cache', 'ui.dropzone'
    , 'mgcrea.ngStrap.modal', 'mgcrea.ngStrap.select', 'ngSanitize'
    , 'ui.sortable', 'globalMod',
    , 'hfc.note.service', 'mgcrea.ngStrap.aside', 'kendo.directives'
    , 'ui.bootstrap.pagination', 'mgcrea.ngStrap', 'ui.bootstrap'
    , 'ngMask', 'hfc.core.service', "kendo.directives"
    , "angucomplete-alt", 'hfc.newsandupdates', 'hfc.Notemodule'
    , 'hfc.Cachemodule', 'jqwidgets', 'HFC_NGApp_Auth'
    , 'hfc.accountModule', 'hfc.leadModule', 'hfc.leadaccountModule', 'hfc.reportModule'
    , 'hfc.calendarModule', 'hfc.personModule', 'hfc.fileUploadModule', 'hfc.fileUploadModuleVg', 'hfc.commentsModule', 'hfc.caseCommentsModule', 'hfc.kendotooltipModule', 'HFC_UserApp'
    , 'feedbackModule', 'overviewModule', 'HFC_NGAppVendor', 'HFC_NGAppPIC', 'HFC_NGAppCP', 'blurToCurrencyModule']);
app.config(function ($routeProvider, unsavedWarningsConfigProvider) {
    var resolve = {
        source: [
            "CacheService", "$q", function (CacheService, $q) {
                var deferred = $q.defer();

                CacheService.Get('Source', function (items) {
                    deferred.resolve();
                });

                return deferred.promise;
            }
        ],

        leadStatus: [
                "CacheService", "$q", function (CacheService, $q) {
                    var deferred = $q.defer();

                    CacheService.Get('LeadStatus', function (items) {
                        deferred.resolve();
                    });

                    return deferred.promise;
                }
        ],

        jobStatus: [
                "CacheService", "$q", function (CacheService, $q) {
                    var deferred = $q.defer();

                    CacheService.Get('JobStatus', function (items) {
                        deferred.resolve();
                    });

                    return deferred.promise;
                }
        ],

        concept: [
                    "CacheService", "$q", function (CacheService, $q) {
                        var deferred = $q.defer();

                        CacheService.Get('Concept', function (items) {
                            deferred.resolve();
                        });

                        return deferred.promise;
                    }
        ]
    }

    $routeProvider.when("/news/:newsId", {
        controller: "newsController",
        templateUrl: "/app/views/settings/General/News/NewsDetailsUser.html",
        resolve: resolve,
        title: "Manage News Updates",
    });

    // Moved to app_CP.
    //$routeProvider.when("/franchises", {
    //    controller: "FranchiseListingController",
    //    templateUrl: "/app/views/admin/franchise/franchises.html"
    //});

    //$routeProvider.when("/franchise/:FranchiseId", {
    //    controller: "FranchiseController",
    //    templateUrl: "/app/views/admin/franchise/franchise.html"
    //});

    //$routeProvider.when("/franchiseView/:FranchiseId", {
    //    controller: "FranchiseController",
    //    templateUrl: "/app/views/admin/franchise/franchiseDetail.html"
    //});

    $routeProvider.when("/leadsSearch", {
        controller: "leadSearchController",
        templateUrl: "/app/views/search/leadSearch.html",
        resolve: resolve,
        title: "Lead Search",
    });

    //moved to app-cp.js
    //// Manage sources and channel
    //$routeProvider.when("/editsourcelist/", {
    //    controller: "sourceListController",
    //    templateUrl: "/app/views/sources/editSourceList.html",
    //    title: "Sources Management"
    //});

    //$routeProvider.when("/sourcelist/", {
    //    controller: "sourceListController",
    //    templateUrl: "/app/views/sources/sourceList.html",
    //    title: "Sources Management"
    //});

    //$routeProvider.when("/channellist/", {
    //    controller: "channelListController",
    //    templateUrl: "/app/views/sources/channelList.html",
    //    title: "Channel Management"
    //});

    //$routeProvider.when("/editchannellist/", {
    //    controller: "channelListController",
    //    templateUrl: "/app/views/sources/editChannelList.html",
    //    title: "Channel Management"
    //});

    //$routeProvider.when("/ownerlist/", {
    //    controller: "ownerListController",
    //    templateUrl: "/app/views/sources/ownerList.html",
    //    title: "Owner Management"
    //});

    //$routeProvider.when("/editownerlist/", {
    //    controller: "ownerListController",
    //    templateUrl: "/app/views/sources/editOwnerList.html",
    //    title: "Owner Management"
    //});

    //$routeProvider.when("/tpsources/", {
    //    controller: "tpSourceController",
    //    templateUrl: "/app/views/sources/tpSources.html",
    //    title: "TPSources Management"
    //});

    //$routeProvider.when("/edittpsources/", {
    //    controller: "tpSourceController",
    //    templateUrl: "/app/views/sources/editTpSources.html",
    //    title: "TPSources Management"
    //});

    //$routeProvider.when("/hfcsources/", {
    //    controller: "hfcSourceController",
    //    templateUrl: "/app/views/sources/hfcSources.html",
    //    title: "HFCSources Management"
    //});

    //$routeProvider.when("/edithfcsources/", {
    //    controller: "hfcSourceController",
    //    templateUrl: "/app/views/sources/editHfcSources.html",
    //    title: "HFCSources Management"
    //});

    //$routeProvider.when("/hfcleadsourcemap/", {
    //    controller: "hfcLeadsourcemapController",
    //    templateUrl: "/app/views/sources/leadsourcemap.html",
    //    title: "HFCSources Management"
    //});
    //End of sources and channels

    //global search
    $routeProvider.when("/globalSearch/:type/:value", {
        controller: "globalSearchController",
        templateUrl: "/app/views/globalsearch/globalSearch.html",
        resolve: resolve,
        title: "Global Search"
    });
    $routeProvider.when("/globalSearch/:type/", {
        controller: "globalSearchController",
        templateUrl: "/app/views/globalsearch/globalSearch.html",
        resolve: resolve,
        title: "Global Search"
    });

    $routeProvider.when("/globalSearch", {
        controller: "globalSearchController",
        templateUrl: "/app/views/globalsearch/globalSearch.html",
        resolve: resolve,
        title: "Global Search"
    });

    $routeProvider.when("/quoteSearch/:OpportunityId", {
        controller: "quoteController",
        templateUrl: "/app/views/search/quoteSearch.html",
        resolve: resolve,
        title: "Quote Search"
    });

    $routeProvider.when("/accountSearch", {
        controller: "accountSearchController",
        templateUrl: "/app/views/search/accountSearch.html",
        resolve: resolve,
        title: "Account Search"
    });

    $routeProvider.when("/orderSearch", {
        controller: "orderSearchController",
        templateUrl: "/app/views/search/orderSearch.html",
        resolve: resolve,
        title: "Order Search"
    });

    $routeProvider.when("/quotesSearch", {
        controller: "quotesSearchController",
        templateUrl: "/app/views/search/quotesSearch.html",
        resolve: resolve,
        title: "Quote Search"
    });

    //------ Pricing Management-----------//
    //---------Start-------------//

    $routeProvider.when("/pricingSettings", {
        controller: "pricingController",
        templateUrl: "/app/views/settings/operations/pricing/basicpricing.html",
        resolve: resolve,
        title: "Basic Pricing Settings"
    });

    $routeProvider.when("/advancedpricingSettings", {
        controller: "advancedpricingController",
        templateUrl: "/app/views/settings/operations/pricing/advancedpricing.html",
        resolve: resolve,
        title: "Advanced Pricing Settings"
    });

    //---------End-------------//

    //------ Manage Documents-----------//
    //---------Start-------------//

    $routeProvider.when("/listDocuments", {
        controller: "manageDocumentsController",
        templateUrl: "/app/views/settings/General/ManageDocuments/ListDocuments.html",
        resolve: resolve,
        title: "Manage Documents"
    });

    // Moved to app-cp.js

    //$routeProvider.when("/listNews", {
    //    controller: "newsListingController",
    //    templateUrl: "/app/views/settings/General/News/ListNews.html",
    //    title: "Manage News Updates"
    //});

    //$routeProvider.when("/news", {
    //    controller: "newsController",
    //    templateUrl: "/app/views/settings/General/News/News.html",
    //    title: "Manage News Updates"
    //});

    //$routeProvider.when("/news/:newsId", {
    //    controller: "newsController",
    //    templateUrl: "/app/views/settings/General/News/NewsDetails.html",
    //    title: "Manage News Updates"
    //});

    //$routeProvider.when("/newsedit/:newsId", {
    //    controller: "newsController",
    //    templateUrl: "/app/views/settings/General/News/News.html",
    //    title: "Manage News Updates"
    //});

    //---------End-------------//

    //------ Security-----------//
    //---------Start-------------//

    $routeProvider.when("/users", {
        controller: "userController",
        templateUrl: "/app/views/settings/Security/users.html",
        resolve: resolve,
        title: "Users List"
    });
    // Roles And Permission

    $routeProvider.when("/rolesSearch", {
        controller: "RoleSearchController",
        templateUrl: "/app/views/settings/Security/rolesSearchView.html",
        resolve: resolve,
        title: "Roles Search"
    });

    $routeProvider.when("/rolesView/:RoleId", {
        controller: "RolesController",
        templateUrl: "/app/views/settings/Security/rolesview.html",
        resolve: resolve,
        title: "Roles View"
    });

    $routeProvider.when("/permissionSetsSearch", {
        controller: "PermissionSetSearchController",
        templateUrl: "/app/views/settings/Security/permissionSetSearch.html",
        resolve: resolve,
        title: "Permission Sets Search"
    });

    $routeProvider.when("/permissionSetsView/:PermissionSetId", {
        controller: "PermissionSetsController",
        templateUrl: "/app/views/settings/Security/permissionsets.html",
        resolve: resolve,
        title: "Permission Sets"
    });
    $routeProvider.when("/permissionSet", {
        controller: "PermissionSetsController",
        templateUrl: "/app/views/settings/Security/permissionsets.html",
        resolve: resolve,
        title: "Permission Set"
    });
    $routeProvider.when("/permissionSets/:PermissionSetId", {
        controller: "PermissionSetsController",
        templateUrl: "/app/views/settings/Security/permissionsets.html",
        resolve: resolve,
        title: "Permission Sets"
    });

    $routeProvider.when("/ViewPermission", {
        controller: "PermissionSetsController",
        templateUrl: "/app/views/settings/Security/permissionView.html",
        resolve: resolve,
        title: "Permission Sets"
    });

    $routeProvider.when("/newuser", {
        controller: "userController",
        templateUrl: "/app/views/settings/Security/newuser.html",
        resolve: resolve,
        title: "New User"
    });

    $routeProvider.when("/manageprofile/:userid", {
        controller: "userController",
        templateUrl: "/app/views/settings/Security/edituser.html",
        resolve: resolve,
        title: "edit User"
    });

    $routeProvider.when("/edituser/:userid", {
        controller: "userController",
        templateUrl: "/app/views/settings/Security/edituser.html",
        resolve: resolve,
        title: "New User"
    });

    $routeProvider.when("/permissions", {
        controller: "permissionController",
        templateUrl: "/app/views/settings/Security/permissionlist.html",
        resolve: resolve,
        title: "Permissions List"
    });
    //---------End-------------//

    //------ Appointment Types-----------//
    //---------Start-------------//

    $routeProvider.when("/appointmentTypes", {
        controller: "appTypeController",
        templateUrl: "/app/views/settings/General/AppointmentTypes/AppTypeView.html",
        resolve: resolve,
        title: "Manage Documents"
    });

    $routeProvider.when("/editAppointmentTypes", {
        controller: "appTypeController",
        templateUrl: "/app/views/settings/General/AppointmentTypes/AppTypeEdit.html",
        resolve: resolve,
        title: "Manage Documents"
    });

    // batch purchasing
    $routeProvider.when("/bulkPurchasing", {
        controller: "BatchPurchasingController",
        templateUrl: "/app/views/batchPurchasing.html",
        resolve: resolve,
        title: "Batch Purchasing"
    });

    // move Quote
    $routeProvider.when("/moveQuote/:opportunityId/:quoteKey", {
        controller: "quoteController",
        templateUrl: "/app/views/opportunity/MoveQuoteToOppurtunity.html",
        resolve: resolve,
        title: "Move Quote"
    });
    

    //---------End-------------//

    //----MSR Repots(Settings Module)

    $routeProvider.when("/MsrReporting", {
        controller: "SettingsMsrReportController",
        templateUrl: "/app/views/settings/General/MsrReporting/MsrReporting.html",
        resolve: resolve,
        title: "Msr Reports"
    });

    //------END------------------//

    //-----Maintain my vendor
    $routeProvider.when("/vendors", {
        controller: "vendorController",
        templateUrl: "/app/views/settings/vendors.html",
        resolve: resolve,
        //title: "Vendors List"
    });

    $routeProvider.when("/vendor", {
        controller: "vendorController",
        templateUrl: "/app/views/settings/vendor.html",
        resolve: resolve
    });
    $routeProvider.when("/vendor/:VendorId", {
        controller: "vendorController",
        templateUrl: "/app/views/settings/vendor.html",
        resolve: resolve
    });
    $routeProvider.when("/vendorProduct/:Guid", {
        controller: "vendorController",
        templateUrl: "/app/views/settings/vendor.html",
        resolve: resolve
    });

    $routeProvider.when("/vendorview/:VendorId", {
        controller: "vendorController",
        templateUrl: "/app/views/settings/vendorview.html",
        resolve: resolve
    });
    //--------------------
    //Start Measurement
    $routeProvider.when("/measurementEdit/:OpportunityId/:InstallAddressId", {
        controller: "MeasurementController",
        templateUrl: "/app/views/measurement/MeasurementsEdit.html",
        resolve: resolve,
        title: "Measurement Edit"
    });

    $routeProvider.when("/measurementEdits/:AccountId/:InstallAddressId", {
        controller: "MeasurementController",
        templateUrl: "/app/views/measurement/MeasurementsEdit.html",
        resolve: resolve,
        title: "Measurement Edit"
    });

    $routeProvider.when("/measurementDetail/:OpportunityId/:AddressId", {
        controller: "MeasurementController",
        templateUrl: "/app/views/measurement/MeasurementsDetail.html",
        resolve: resolve,
        title: "Measurement Details"
    });

    $routeProvider.when("/measurementDetails/:AccountId/:InstallAddressId", {
        controller: "MeasurementController",
        templateUrl: "/app/views/measurement/MeasurementsDetail.html",
        resolve: resolve,
        title: "Measurement Details"
    });
    $routeProvider.when("/measurementDetails/:AccountId", {
        controller: "MeasurementController",
        templateUrl: "/app/views/measurement/MeasurementsDetail.html",
        resolve: resolve,
        title: "Measurement Details"
    });
    $routeProvider.when("/Measurments/:AccountId", {
        controller: "MeasurementController",
        templateUrl: "/app/views/measurement/MeasurementsDetail.html",
        resolve: resolve,
        title: "Measurement Details"
    });
    $routeProvider.when("/Measurments/:AccountId/:InstallAddressId", {
        controller: "MeasurementController",
        templateUrl: "/app/views/measurement/MeasurementsDetail.html",
        resolve: resolve,
        title: "Measurement Details"
    });

    //End Measurement
    //Emailsettings start
    $routeProvider.when("/EditEmail/", {
        controller: "emailsettingsController",
        templateUrl: "/app/views/settings/Email/EditEmailSetting.html",
        resolve: resolve,
        title: " Edit"
    });

    $routeProvider.when("/ViewEmail/", {
        controller: "emailsettingsController",
        templateUrl: "/app/views/settings/Email/EmailSettingList.html",
        resolve: resolve,
        title: " Details"
    });
    //Procurement Settings
    $routeProvider.when("/procurement/", {
        controller: "ProcurementController",
        templateUrl: "/app/views/settings/General/ProcurementSettings/ShipToLocation.html",
        resolve: resolve,
        title: " Details"
    });
    $routeProvider.when("/TaxSettings/", {
        controller: "TaxSettingsController",
        templateUrl: "/app/views/settings/General/TaxSettings/TaxSettings.html",
        resolve: resolve,
        title: " Tax Settings"
    });
    //emailsettings end

    //Start QuoteEditquote

    //Start Quote

    //$routeProvider.when("/quote/:OpportunityId/:quoteKey", {
    //    controller: "quoteController",
    //    templateUrl: "/app/views/Quote/Quote.html",
    //    resolve: resolve
    //});
    $routeProvider.when("/Editquote/:OpportunityId/:quoteKey", {
        controller: "quoteController",
        templateUrl: "/app/views/Quote/Quote.html",
        resolve: resolve,
        title: "Edit Quote"
    });
    $routeProvider.when("/quote/:OpportunityId", {
        controller: "quoteController",
        templateUrl: "/app/views/Quote/Quote.html",
        resolve: resolve
    });

    $routeProvider.when("/quote/:OpportunityId/:quoteKey", {
        controller: "quoteController",
        templateUrl: "/app/views/Quote/Quote.html",
        resolve: resolve
    });

    $routeProvider.when("/quoteReview/:quoteKey", {
        controller: "marginController",
        templateUrl: "/app/views/Quote/QuoteMarginReview.html",
        resolve: resolve
    });
    $routeProvider.when("/SOquoteReview/:quoteKey/:orderId/:orderNumber", {
        controller: "marginController",
        templateUrl: "/app/views/Quote/QuoteMarginReview.html",
        resolve: resolve
    });
    $routeProvider.when("/quoteReviewEdit", {
        controller: "marginController",
        templateUrl: "/app/views/Quote/QuoteMarginEdit.html",
        resolve: resolve
    });

    // TODO: is it used anymore??? -murugan
    $routeProvider.when("/quoteDetails/:quoteKey/:OpportunityId", {
        controller: "quoteController",
        templateUrl: "/app/views/Quote/QuoteDetails.html",
        resolve: resolve
    });
    //End Quote

    // Sample crud to test.
    $routeProvider.when("/sampleCrud", {
        controller: "SampleCrudController",
        templateUrl: "/app/views/sampleCrud/sampleCrud.html",
        resolve: resolve
    });

    $routeProvider.when("/sampleCrud/:sampleId", {
        controller: "SampleCrudController",
        templateUrl: "/app/views/sampleCrud/sampleCrud.html",
        resolve: resolve
    });

    $routeProvider.when("/leads", {
        controller: "leadController",
        templateUrl: "/app/views/lead/lead.html",
        resolve: resolve
    });

    $routeProvider.when("/leads/:leadId", {
        controller: "leadController",
        templateUrl: "/app/views/lead/leadDetails.html",
        resolve: resolve
    });

    $routeProvider.when("/leadConvert/:leadId", {
        //controller: "leadController",
        controller:"leadConvertController",
        templateUrl: "/app/views/lead/ConvertLead.html",
        resolve: resolve
    });
    $routeProvider.when("/account/", {
        controller: "accountDetailsController",
        templateUrl: "/app/views/Account/account.html",
        resolve: resolve
    });

    // Appointments view for Lead, Account Opportunity
    $routeProvider.when("/leadAppointments/:leadId", {
        controller: "AppointmentsListingController",
        controllerAs: "aptmtCtrl",
        templateUrl: "/app/views/lead/Appointments.html",
        resolve: resolve
    });

    $routeProvider.when("/accountAppointments/:accountId", {
        controller: "AppointmentsListingController",
        controllerAs: "aptmtCtrl",
        templateUrl: "/app/views/account/Appointments.html",
        resolve: resolve
    });

    $routeProvider.when("/opportunityAppointments/:opportunityId", {
        controller: "AppointmentsListingController",
        controllerAs: "aptmtCtrl",
        templateUrl: "/app/views/opportunity/Appointments.html",
        resolve: resolve
    });

    //new communicationlog
    $routeProvider.when("/leadCommunicationLog/:leadId", {
        controller: "LeadCommunicationListingController",
        controllerAs: "cmctnCtrl",
        templateUrl: "/app/views/lead/LeadCommunication.html",
        resolve: resolve
    });

    $routeProvider.when("/accountCommunicationLog/:accountId", {
        controller: "AccountCommunicationListingController",
        controllerAs: "cmctnCtrl",
        templateUrl: "/app/views/account/AccountCommunication.html",
        resolve: resolve
    });
    
    $routeProvider.when("/opportunityCommunicationLog/:opportunityId/:accountId", {
        controller: "OpportunityCommunicationListingController",
        controllerAs: "cmctnCtrl",
        templateUrl: "/app/views/opportunity/OpportunityCommunication.html",
        resolve: resolve
    });
    //end


    // Email Communication log for Customer journey point
    $routeProvider.when("/leadCommunication/:leadId", {
        controller: "CommunicationListingController",
        controllerAs: "cmctnCtrl",
        templateUrl: "/app/views/lead/Communication.html",
        resolve: resolve
    });

    $routeProvider.when("/accountCommunication/:accountId", {
        controller: "CommunicationListingController",
        controllerAs: "cmctnCtrl",
        templateUrl: "/app/views/account/Communication.html",
        resolve: resolve
    });

    $routeProvider.when("/opportunityCommunication/:opportunityId", {
        controller: "CommunicationListingController",
        controllerAs: "cmctnCtrl",
        templateUrl: "/app/views/opportunity/Communication.html",
        resolve: resolve
    });

    $routeProvider.when("/leadsQualify/:leadId", {
        controller: "questionController",
        templateUrl: "/app/views/question/QuestionDetails.html",
        resolve: resolve
    });

    $routeProvider.when("/leadsEditQualify/:leadId", {
        controller: "questionController",
        templateUrl: "/app/views/question/QuestionDetails.html",
        resolve: resolve
    });

    $routeProvider.when("/accountsQualify/:accountId", {
        controller: "accountDetailsController",
        templateUrl: "/app/views/account/accountDetailsQualify.html",
        resolve: resolve
    });

    $routeProvider.when("/accountsEditQualify/:accountId", {
        controller: "accountDetailsController",
        templateUrl: "/app/views/account/EditAccountQualify.html",
        resolve: resolve
    });

    //TODO: this should point to the Lead.html
    $routeProvider.when("/leadsEdit/:leadId", {
        controller: "leadController",
        templateUrl: "/app/views/lead/Lead.html",
        resolve: resolve
    });

    //For the Disclaimer
    $routeProvider.when("/disclaimerEdit/", {
        controller: "disclaimerController",
        templateUrl: "/app/views/settings/General/Disclaimer/DisclaimerEdit.html",
        resolve: resolve
    });

    $routeProvider.when("/disclaimerView/", {
        controller: "disclaimerController",
        templateUrl: "/app/views/settings/General/Disclaimer/DisclaimerView.html",
        resolve: resolve
    });
    $routeProvider.when("/roles/", {
        controller: "rolesController",
        templateUrl: "/app/views/settings/Roles/RoleList.html",
        resolve: resolve
    });
    //End of Disclaimer

    //Adding Product
    $routeProvider.when("/productSearch/:searchTerm?/:searchFilter?/:salesPersonIds?/:jobStatusIds?/:leadStatusIds?/:invoiceStatuses?/:sourceIds?/:selectedDateText?/:createdOnUtcStart?/:createdOnUtcEnd?/:commercialType?/:isReportSearch?", {
        controller: "productController",
        templateUrl: "/app/views/search/productSearch.html",
        resolve: resolve
    });
    $routeProvider.when("/productCreate/", {
        controller: "productController",
        templateUrl: "/app/views/product/productadd.html",
        resolve: resolve
    });
    $routeProvider.when("/productCreate/:VendorId", {
        controller: "productController",
        templateUrl: "/app/views/product/productadd.html",
        resolve: resolve
    });
    $routeProvider.when("/productVendor/:Guid", {
        controller: "productController",
        templateUrl: "/app/views/product/productadd.html",
        resolve: resolve
    });

    $routeProvider.when("/productEdit/:Productkey/:FranchiseId", {
        controller: "productController",
        templateUrl: "/app/views/product/productadd.html",
        resolve: resolve
    });
    $routeProvider.when("/productDetail/:Productkey/:FranchiseId", {
        controller: "productController",
        templateUrl: "/app/views/product/productview.html",
        resolve: resolve
    });
    //End of Product changes

    //opportunity List,view and Edit
    //-------------------------------------------------------------------
    $routeProvider.when("/opportunitySearch/", {
        controller: "opportunitySearchController",
        templateUrl: "/app/views/search/opportunitySearch.html",
        resolve: resolve,
        title: "Opportunity Search"
    });

    $routeProvider.when("/accountopportunitySearch/:AccountId", {
        controller: "opportunitySearchController",
        templateUrl: "/app/views/account/opportunitiesList.html",
        resolve: resolve,
        title: "Opportunity List"
    });
    $routeProvider.when("/accountordersSearch/:AccountId", {
        controller: "orderSearchController",
        templateUrl: "/app/views/search/orderSearch.html",
        resolve: resolve,
        title: "orders List"
    });

    $routeProvider.when("/accountPaymentSearch/:AccountId", {
        controller: "paymentSearchController",
        templateUrl: "/app/views/account/paymentList.html",
        resolve: resolve,
        title: "orders List"
    });

    $routeProvider.when("/accountInvoiceSearch/:AccountId", {
        controller: "invoiceSearchController",
        templateUrl: "/app/views/account/invoiceList.html",
        resolve: resolve,
        title: "Invoices List"
    });
    // TODO: no more required -murugan mar 15, 2019
    //$routeProvider.when("/accountappointments/:AccountId", {
    //    controller: "opportunitySearchController",
    //    templateUrl: "/app/views/account/appointmentsList.html",
    //    resolve: resolve,
    //    title: "Appointments List"
    //});

    $routeProvider.when("/Opportunitys/", {
        controller: "opportunityController",
        templateUrl: "/app/views/opportunity/opportunity.html",
        resolve: resolve
    });

    $routeProvider.when("/opportunityEdit/:OpportunityId", {
        controller: "opportunityController",
        // replaced with common opportunity.html - murugan/Mar 05, 2019.
        //templateUrl: "/app/views/opportunity/opportunityEdit.html",
        templateUrl: "/app/views/opportunity/opportunity.html",
        resolve: resolve
    });

    $routeProvider.when("/Opportunitys/:AccountIdReferred", {
        controller: "opportunityController",
        templateUrl: "/app/views/opportunity/opportunity.html",
        resolve: resolve
    });
    $routeProvider.when("/OpportunityDetail/:OpportunityId", {
        controller: "opportunityController",
        templateUrl: "/app/views/opportunity/opportunityDetails.html",
        resolve: resolve
    });

    //--------------------------------------------------------------------
    //Case Manage-Add/Edit
    $routeProvider.when("/caseAddEdit/", {
        controller: "CaseManagementController",
        templateUrl: "/app/views/CaseManagement/Caseaddedit.html",
        title: " Details"
    });
    $routeProvider.when("/createCaseMPO/:MPO/:LineNo", {
        controller: "CaseManagementController",
        templateUrl: "/app/views/CaseManagement/Caseaddedit.html",
        title: " Details"
    });
    $routeProvider.when("/createCasexMPO/:MPO/:LineNo", {
        controller: "CaseManagementController",
        templateUrl: "/app/views/CaseManagement/Caseaddedit.html",
        title: " Details"
    });
    $routeProvider.when("/createCaseVPO/:MPO/:LineNo/:SOMPO", {
        controller: "CaseManagementController",
        templateUrl: "/app/views/CaseManagement/Caseaddedit.html",
        title: " Details"
    });
    $routeProvider.when("/createCasexVPO/:MPO/:LineNo/:SOMPO", {
        controller: "CaseManagementController",
        templateUrl: "/app/views/CaseManagement/Caseaddedit.html",
        title: " Details"
    });
    $routeProvider.when("/createCaseOrder/:SO/:MPO/:LineNo", {
        controller: "CaseManagementController",
        templateUrl: "/app/views/CaseManagement/Caseaddedit.html",
        title: " Details"
    });

    $routeProvider.when("/Case/:CaseId", {
        controller: "CaseManagementController",
        templateUrl: "/app/views/CaseManagement/Caseaddedit.html",
        title: " Details"
    });

    //-----------------------------------------------------------------------------
    //Vendor caseManagement
    //$routeProvider.when("/VendorcaseEdit/:CaseId/:Id", {
    //$routeProvider.when("/VendorcaseEdit/:VendorCaseId", {
    //    controller: "VendorcaseManagementController",
    //    templateUrl: "/app/views/VendorCaseManagement/vendorEdit.html",
    //    title: " Details"
    //});

    //$routeProvider.when("/VendorCaseList", {
    //    controller: "VendorcaseManagementListController",
    //    templateUrl: "/app/views/VendorCaseManagement/VendorCaseList.html",
    //    title: " Details"
    //});

    //$routeProvider.when("/VendorcaseView/:VendorCaseId", {
    //    controller: "VendorcaseManagementViewController",
    //    templateUrl: "/app/views/VendorCaseManagement/vendorCaseView.html",
    //    title: " Details"
    //});
    //------------------------------------------------------------------------------

    //account details
    $routeProvider.when("/Accounts/:accountId", {
        controller: "accountDetailsController",
        templateUrl: "/app/views/account/accountDetails.html",
        resolve: resolve
    });

    //$routeProvider.when("/Orders/:orderId", {
    //    controller: "OrderController",
    //    templateUrl: "/app/views/order/orderDetails.html",
    //    resolve: resolve
    //});
    

    $routeProvider.when("/OrderPayment/:orderId", {
        controller: "paymentController",
        templateUrl: "/app/views/order/Payment.html",
        resolve: resolve
    });
    $routeProvider.when("/ReverseOrderPayment/:orderId", {
        controller: "paymentController",
        templateUrl: "/app/views/order/Payment.html",
        resolve: resolve
    });
    $routeProvider.when("/accountEdit/:accountId", {
        controller: "accountDetailsController",
        templateUrl: "/app/views/account/account.html",
        resolve: resolve
    });

    // Order details
    $routeProvider.when("/orderEdit/:orderId", {
        controller: "OrderController",
        templateUrl: "/app/views/order/orderEdit.html",
        resolve: resolve
    });

    $routeProvider.when("/Orders/:orderId", {
        controller: "OrderController",
        //templateUrl: "/app/views/order/orderDetails.html",
        templateUrl: "/app/views/order/new-order.html",
        resolve: resolve
    });

    $routeProvider.when("/customerView/:orderId", {
        controller: "OrderController",
        templateUrl: "/app/views/order/customerView.html",
        resolve: resolve
    });

    //account details
    $routeProvider.when("/accounts/:accountId/:jobId", {
        controller: "accountDetailsController",
        templateUrl: "/app/views/account/accountDetails.html",
        resolve: resolve
    });

    //$routeProvider.when("/invoices/detail/:invoiceId", {
    //    controller: "invoiceDetailsController",
    //    templateUrl: "/app/views/invoice/invoiceDetails.html"
    //});

    $routeProvider.when("/dashboard", {
        controller: "dashboardController",
        templateUrl: "/app/views/dashboard/dashboard.html",
        resolve: resolve,
    });

    //--- for the navigation created----//
    //Calendar Controller
    $routeProvider.when("/Calendar", {
        controller: "CalendarControllerTP",
        templateUrl: "/app/views/dashboard/calendar.html",
        resolve: resolve,
    });
    $routeProvider.when("/Calendar/:Section/:Id", {
        controller: "CalendarControllerTP",
        templateUrl: "/app/views/dashboard/calendar.html",
        resolve: resolve,
    });
    $routeProvider.when("/Calendar/:Section/:Id/:Id2", {
        controller: "CalendarControllerTP",
        templateUrl: "/app/views/dashboard/calendar.html",
        resolve: resolve,
    });
    $routeProvider.when("/Calendar/:tabselection", {
        controller: "CalendarControllerTP",
        templateUrl: "/app/views/dashboard/calendar.html",
        resolve: resolve,
    });
    //Save Calendar
    $routeProvider.when("/SaveCalendar/:Id", {
        controller: "SaveCalendarControllerTP",
        templateUrl: "/app/views/dashboard/Savecalendar.html",
        resolve: resolve,
    });
    $routeProvider.when("/SaveCalendar/:Id/:tabselection", {
        controller: "SaveCalendarControllerTP",
        templateUrl: "/app/views/SaveCalendarControllerTP/Savecalendar.html",
        resolve: resolve,
    });
    //shipping notices
    $routeProvider.when("/shipnotices/:ShipNoticeId", {
        controller: "ShippingNoticesController",
        templateUrl: "/app/views/shippingNotices/shipnotices.html",
        resolve: resolve,
    });
    //shipping notice Dashboard
    $routeProvider.when("/ShippingNoticeDashboard", {
        controller: "ShippingNoticeDashboardController",
        templateUrl: "/app/views/shippingNotices/ShippingNoticeDashboard.html",
        resolve: resolve,
    });

    //procurement dashboard
    $routeProvider.when("/procurementboard", {
        controller: "ProcurementDashboardController",
        templateUrl: "/app/views/procurementDashboard/procurementboard.html",
        resolve: resolve
    });

    //Vendor Acknowledgement
    $routeProvider.when("/vendorAck/:POId/:vendorReference", {
        controller: "VendorAcknowledgementController",
        templateUrl: "/app/views/VendorAcknowledgement/VendorAcknowledgement.html",
        resolve: resolve
    });

    //vendorinvoice
    $routeProvider.when("/vendorinvoice/:invoiceNumber", {
        controller: "VendorInvoiceController",
        templateUrl: "/app/views/vendorInvoice/vendorinvoice.html",
        resolve: resolve,
    });
    $routeProvider.when("/revisedinvoice/:invoiceNumber", {
        controller: "VendorInvoiceController",
        templateUrl: "/app/views/vendorInvoice/Revisedinvoice.html",
        resolve: resolve,
    });
    $routeProvider.when("/revisedinvoiceview/:invoiceNumber", {
        controller: "VendorInvoiceController",
        templateUrl: "/app/views/vendorInvoice/Revisedinvoiceview.html",
        resolve: resolve,
    });
    //vendorinvoice-PPV List
    $routeProvider.when("/vendorinvoicePPVlist", {
        controller: "VendorInvoiceController",
        templateUrl: "/app/views/vendorInvoice/VendorInvoicePPVList.html",
        resolve: resolve,
    });
    //buyer remorse
    $routeProvider.when("/buyerremorse", {
        controller: "buyerremorseController",
        templateUrl: "/app/views/settings/BuyerRemorse/buyerremorse.html",
        resolve: resolve,
    });

    //purchase Controller
    $routeProvider.when("/purchase", {
        controller: "purchaseOrderController",
        templateUrl: "/app/views/purchaseOrder/purchase.html",
        resolve: resolve,
    });
    $routeProvider.when("/purchasemasterview/:masterPO", {
        controller: "purchaseOrderController",
        templateUrl: "/app/views/purchaseOrder/purchaseviewmaster.html",
        resolve: resolve,
    });
    $routeProvider.when("/MasterPurchaseOrdeList", {
        controller: "purchaseOrderController",
        templateUrl: "/app/views/purchaseOrder/purchaseviewmaster.html",
        resolve: resolve,
    });
    // Xvpo page
    $routeProvider.when("/xPurchaseMasterView/:PurchaseOrderId", {
        controller: "xVendorPurchaseOrderController",
        templateUrl: "/app/views/purchaseOrder/xVendorPurchaseOrdersview.html",
        resolve: resolve,
    });
    //vendor governance
    $routeProvider.when("/Feedback", {
        controller: "FeedbackController",
        templateUrl: "/app/views/VendorGovernance/Feedback.html",
        resolve: resolve,
    });
    $routeProvider.when("/FeedbackKanbanView", {
        controller: "FeedbackController",
        templateUrl: "/app/views/VendorGovernance/FeedbackKanbanView.html",
        resolve: resolve,
    });
    $routeProvider.when("/Overview", {
        controller: "OverviewController",
        templateUrl: "/app/views/VendorGovernance/Overview.html",
        resolve: resolve,
    });
    $routeProvider.when("/OverviewKanbanView", {
        controller: "OverviewController",
        templateUrl: "/app/views/VendorGovernance/OverviewKanbanView.html",
        resolve: resolve,
    });
    //Reports Controller
    $routeProvider.when("/Reports", {
        controller: "ReportsControllerTP",
        templateUrl: "/app/views/dashboard/reports.html",
        resolve: resolve,
    });
    //Marketing Controller
    //$routeProvider.when("/Marketing", {
    //    controller: "MarketingControllerTP",
    //    templateUrl: "/app/views/dashboard/marketing.html"
    //});
    $routeProvider.when("/Marketing", {
        controller: "sourceController",
        templateUrl: "/app/views/settings/source.html",
        resolve: resolve,
    });
    //-------------------------------------------//
    $routeProvider.when("/sources", {
        controller: "sourceController",
        templateUrl: "/app/views/settings/source.html",
        resolve: resolve,
    });
    //$routeProvider.when("/PICconfigurator/:quoteLineId", {
    //    controller: "PICconfiguratorController",
    //    templateUrl: "/app/views/configurator/PICconfigurator.html",
    //    resolve: resolve,
    //});
    $routeProvider.when("/configurator/:quoteKey/:quoteLineId/", {
        controller: "configuratorController",
        templateUrl: "/app/views/configurator/configurator.html",
        resolve: resolve,
    });
    $routeProvider.when("/configurator/:quoteLineId", {
        controller: "configuratorController",
        templateUrl: "/app/views/configurator/configurator.html",
        resolve: resolve,
    });

    $routeProvider.when("/CoreProductconfigurator/:OpportunityId/:quoteKey", {
        controller: "batchBBCoreProductConfigurator",
        templateUrl: "/app/views/configurator/batchBBCoreProductConfigurator.html",
        resolve: resolve,
    });

    $routeProvider.when("/MPOCoreProductconfigurator/:OpportunityId/:quoteKey/:purchaseOrderId", {
        controller: "batchBBCoreProductConfigurator",
        templateUrl: "/app/views/configurator/batchBBCoreProductConfigurator.html",
        resolve: resolve,
    });
    $routeProvider.when("/xVPOCoreProductconfigurator/:OpportunityId/:quoteKey/:purchaseOrderId", {
        controller: "batchBBCoreProductConfigurator",
        templateUrl: "/app/views/configurator/batchBBCoreProductConfigurator.html",
        resolve: resolve,
    });

    $routeProvider.when("/myVendorConfigurator/:Opportunity/:quoteLineId", {
        controller: "myVendorconfiguratorController",
        templateUrl: "/app/views/configurator/myVendorConfigurator.html",
        resolve: resolve,
    });

    $routeProvider.when("/surfacingJobEstimator/:OpportunityId/:quoteKey", {
        controller: "surfacingJobEstimatorController",
        templateUrl: "/app/views/configurator/surfacingJobEstimator.html",
        resolve: resolve,
    });
    //Add Product configurator
    $routeProvider.when("/myProductConfigurator/:Opportunity/:quoteId", {
        controller: "myProductConfiguratorController",
        templateUrl: "/app/views/configurator/myProductConfigurator.html",
        resolve: resolve,
    });
    // Service Configurator
    $routeProvider.when("/MyServiceConfigurator/:Opportunity/:quoteId", {
        controller: "myServiceConfiguratorController",
        templateUrl: "/app/views/configurator/myServiceConfigurator.html",
        resolve: resolve,
    });
    $routeProvider.when("/myVendorConfigurator/:quoteLineId", {
        controller: "myVendorconfiguratorController",
        templateUrl: "/app/views/configurator/myVendorConfigurator.html",
        resolve: resolve,
    });
    $routeProvider.when("/mergeTool", {
        controller: "mergeToolController",
        templateUrl: "/app/views/settings/mergeTool.html",
        resolve: resolve,
    });

    // Franchise Case Management
    $routeProvider.when("/franchiseCase", {
        controller: "FranchiseCaseListController",
        templateUrl: "/app/views/FranchiseCaseMgmt/FranchiseCaseList.html",
    });

    $routeProvider.when("/CaseView/:CaseId", {
        controller: "FranchiseCaseViewController",
        templateUrl: "/app/views/FranchiseCaseMgmt/caseView.html",
    });

    $routeProvider.when("/FranchiseVendorView/:CaseId/:VendorCaseId/:VendorId", {
        controller: "FranchiseCaseVendorViewController",
        templateUrl: "/app/views/FranchiseCaseMgmt/FranchiseCaseVendorView.html",
    });

    //$routeProvider.when("/FranchiseCaseVendorView/:CaseId/:VendorCaseId/:VendorId", {
    //    controller: "FranchiseCaseVendorViewController",
    //    templateUrl: "/app/views/FranchiseCaseMgmt/FranchiseCaseVendorView.html",

    //});

    $routeProvider.when("/SalesAgentReport", {
        controller: "ReportsController",
        templateUrl: "/app/views/Reports/SalesAgentReport.html",
    });

    $routeProvider.when("/SalesandPurchasingDetailReport", {
        controller: "ReportsController",
        templateUrl: "/app/views/Reports/SalesandPurchasingDetail.html",
    });

    $routeProvider.when("/FranchisePerformanceReport", {
        controller: "ReportsController",
        templateUrl: "/app/views/Reports/FranchisePerformanceReport.html",
    });

    $routeProvider.when("/SalesSummaryByMonth", {
        controller: "ReportsController",
        templateUrl: "/app/views/Reports/SalesSummary.html",
    });

    $routeProvider.when("/UseTax", {
        controller: "ReportsController",
        templateUrl: "/app/views/Reports/UseTax.html",
    });

    $routeProvider.when("/VendorPerformance", {
        controller: "ReportsController",
        templateUrl: "/app/views/Reports/VendorSalesPerformance.html",
    });

    $routeProvider.when("/MarketingPerformance", {
        controller: "ReportsController",
        templateUrl: "/app/views/Reports/MarketingPerformance.html",
    });

    $routeProvider.when("/SalesTax", {
        controller: "ReportsController",
        templateUrl: "/app/views/Reports/SalesTax.html",
    });

    $routeProvider.when("/SalesTaxSummary", {
        controller: "ReportsController",
        templateUrl: "/app/views/Reports/SalesTaxSummary.html",
    });

    $routeProvider.when("/Reports", {
        controller: "ReportsController",
        templateUrl: "/app/views/Reports/MainPageReports.html",
    });
    $routeProvider.when("/SalesSummaryReport", {
        controller: "ReportsController",
        templateUrl: "/app/views/Reports/SalesSummaryReport.html",
    });

    $routeProvider.when("/RawDataReport", {
        controller: "ReportsController",
        templateUrl: "/app/views/Reports/RawData.html",
    });

    $routeProvider.when("/OpenBalanceReport", {
        controller: "ReportsController",
        templateUrl: "/app/views/Reports/OpenARReport.html",
    });
    $routeProvider.when("/SalesJournalReport", {
        controller: "ReportsController",
        templateUrl: "/app/views/Reports/SalesJournalReport.html",
    });
    $routeProvider.when("/OrdersPaidInFull", {
        controller: "ReportsController",
        templateUrl: "/app/views/Reports/OrdersPaidInFull.html",
    });
    $routeProvider.when("/MsrReport", {
        controller: "ReportsController",
        templateUrl: "/app/views/Reports/MsrReport.html",
    });

    $routeProvider.when("/PPCMatchbackReport", {
        controller: "ReportsController",
        templateUrl: "/app/views/Reports/PPCMatchBackReport.html",
    });

    $routeProvider.when("/PaymentReport", {
        controller: "ReportsController",
        templateUrl: "/app/views/Reports/PaymentReport.html",
    });

    $routeProvider.when("/AgingReport", {
        controller: "ReportsController",
        templateUrl: "/app/views/Reports/AgingReport.html",
    });

    $routeProvider.when("/IncomeSummaryReport", {
        controller: "ReportsController",
        templateUrl: "/app/views/Reports/IncomeSummaryReport.html",
    });

    //Case Management Kanban
    $routeProvider.when("/KanbanView", {
        controller: "KanbanController",
        templateUrl: "/app/views/Kanban/Kanban.html",
        title: "Vendor Case Config"
    });

    $routeProvider.when("/QuickbookSetup", {
        controller: "QuickbookController",
        templateUrl: "/app/views/Quickbook/QuickbookSetup.html",
        title: "Quickbook Setup"
    });

    $routeProvider.when("/qbo", {
        controller: "QuickbookController",
        templateUrl: "/app/views/Quickbook/qbo.html",
        title: "Quickbook Setup"
    });

    $routeProvider.when("/QBOPreSyncCheck", {
        controller: "QuickbookController",
        templateUrl: "/app/views/Quickbook/QBOPreSyncCheck.html",
        title: "Quickbook Setup"
    });

    $routeProvider.when("/SyncLogs", {
        controller: "QuickbookController",
        templateUrl: "/app/views/Quickbook/SyncLogs.html",
        title: "Quickbook Setup"
    });

    $routeProvider.when("/qboconnectclose", {
        controller: "QuickbookController",
        templateUrl: "/app/views/Quickbook/qbo.html",
        title: "Quickbook Setup"
    });

    //source migration
    $routeProvider.when("/migrateMyCrmSource", {
        controller: "myCrmDataConversionController",
        templateUrl: "/app/views/settings/MigrationUI/myCrmDataConversion.html",
        title: "Quickbook Setup"
    });

    //xmpo url--------------
    $routeProvider.when("/xSalesOrderView/:PurchaseOrderId", {
        controller: "xSalesOrderview",
        templateUrl: "/app/views/procurementDashboard/xSalesOrderView.html",
        title:"xSalesOrderView"
    });
    //End------------

    $routeProvider.otherwise({ redirectTo: "/dashboard" });

    unsavedWarningsConfigProvider.useTranslateService = false;
});

app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptorService');
});

app.constant('CONFIG', {
    apiServiceBaseUri: HFCNamespaceUrls.baseEndPoint,
});

app.run(function ($rootScope) {
    $rootScope.$on("$locationChangeStart", function (event, next, current) {
        if (next.indexOf('search') > 0) {
            $rootScope.enableSaveSearch = enableSaveSearch;
        } else {
            $rootScope.enableSaveSearch = false;
        }
    });

    $rootScope.$on('$viewContentLoaded', function () {
        var interval = setInterval(function () {
            if (document.readyState == "complete") {
                window.scrollTo(0, 0);
                clearInterval(interval);
            }
        }, 200);
    });

    $rootScope.$on("$routeChangeSuccess", function (event, currentRoute, previousRoute) {
        window.scrollTo(0, 0);
        setTimeout(function () {
            $(".tooltip-bottom").click(function (e) {
                var parent = $(e.target).parent()[0];
                $(parent).blur();
            });
            $('select').on('change', function (e) {
                $(this).blur();
            });
        }, 5000);
    });
});

app.run(function (HFCService) {
    HFCService.CurrentUserPermissions = permissionsList;
    HFCService.FranciseLevelTexting = FranciseLevelTexting;
    HFCService.FranciseLevelCase = FranciseLevelCase;
    HFCService.FranciseLevelVendorMng = FranciseLevelVendorMng;
    HFCService.AdminElectronicSign = AdminElectronicSign;
    HFCService.FranciseLevelElectronicSign = FranciseLevelElectronicSign;
    HFCService.Roles = roles;
    //console.log("**    ", HFCService.CurrentUserPermissions);
});

var permissionsList = [];
var FranciseLevelTexting;
var FranciseLevelCase;
var FranciseLevelVendorMng;
var AdminElectronicSign;
var FranciseLevelElectronicSign;
var roles = [];

angular.element(document).ready(function () {

    if (!angular.element(document).scope())
    {
        $.ajax({
            url: '/api/Permission/0/GetUserRolesPermission',
            async: false,
            success: function (data) {
                //console.log("manual bootstraping");
                permissionsList = data;
                $.ajax({
                    url: '/api/users/',
                    async: false,
                    success: function (data) {
                        //console.log("manual bootstraping");
                        if (data != "") {
                            roles = "Franchise";
                            FranciseLevelTexting = data.FranciseLevelTexting;
                            FranciseLevelCase = data.FranciseLevelCase;
                            FranciseLevelVendorMng = data.FranciseLevelVendorMng;
                            AdminElectronicSign = data.AdminElectronicSign;
                            FranciseLevelElectronicSign = data.FranciseLevelElectronicSign;
                        }
                        var path = window.location.href;
                        if (!path.includes("controlpanel"))
                            angular.bootstrap(document, ['HFC_NGApp']);
                    },
                    error: function (data) {
                        angular.bootstrap(document, ['HFC_NGApp']);
                    }
                });

            },
            error: function (data) {
                angular.bootstrap(document, ['HFC_NGApp']);
            }
        });
    }

});


var cacheBustSuffix = HFC.Version;

app.constant("cacheBustSuffix", cacheBustSuffix);

app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('preventTemplateCache');
    $httpProvider.interceptors.push('authInterceptorService');
});

//added this since we made usercreation module common for admin and fe side
//the top navbar are getting disable/not selected when page get refesh
//added this since the service value needed for selection.
app.service('NavbarServiceCP', [function () {
    //var srv = this;
}]);

app.constant('ngAuthSettings', {
    apiServiceBaseUri: HFCNamespaceUrls.baseEndPoint,
    clientId: 'ngAuthApp'
});

app.directive("csDateToIso", function () {
    //Directive to set the correct date - added for global date settting
    var linkFunction = function (scope, element, attrs, ngModelCtrl) {
        ngModelCtrl.$parsers.push(function (datepickerValue) {
            return moment(datepickerValue).format("YYYY-MM-DD");
        });
    };

    return {
        restrict: "A",
        require: "ngModel",
        link: linkFunction
    };
});

app.directive('datepicker', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            element.datetimepicker({
                mask: true,
                timepicker: false,
                format: 'MM/dd/yyyy',
                onSelect: function (dateText, inst) {
                    ngModelCtrl.$setViewValue(dateText);
                    scope.$apply();
                }
            }).on('blur', function () {
                $(this).valida();
            });
        }
    }
});

app.directive("formatDate", function () {
    return {
        require: 'ngModel',
        link: function (scope, elem, attr, modelCtrl) {
            modelCtrl.$formatters.push(function (modelValue) {
                if (modelValue) {
                    return new Date(modelValue);
                }
                else {
                    return null;
                }
            });
        }
    };
});

app.run(['$route', '$rootScope', '$location', function ($route, $rootScope, $location) {
    var original = $location.path;
    $location.path = function (path, reload) {
        if (reload === false) {
            var lastRoute = $route.current;
            var un = $rootScope.$on('$locationChangeSuccess', function () {
                $route.current = lastRoute;
                un();
            });
        }
        return original.apply($location, [path]);
    };
}]);

app.filter('ignoreTimeZone', function ($filter, calendarModalService) {
    return function (val, flag) {
        if (val) {
            let finalDate = '';
            if (typeof (val) == 'string') {
                finalDate = val.substr(0, 16);
                var newDate = calendarModalService.getDateStringValue(new Date(finalDate), true, false, false);
            }
            else
                var newDate = val;

            if (flag == "short")
                var formattedDate = $filter('date')(newDate, 'MM/dd/yyyy hh:mm a');
            else
                var formattedDate = $filter('date')(newDate, 'MM/dd/yyyy');

            return formattedDate;
        } else {
            return "";
        }
    };
});

// Remove template cache on app load
app.run(function ($rootScope, $templateCache) {
    $templateCache.removeAll();
});
// For global unhandled exception handling
app.factory('$exceptionHandler', ['$log', function ($log, $location) {
    return function myExceptionHandler(exception, cause) {
        $log.error(exception, cause);
        var loadingElement = document.getElementById("loading");
        if (loadingElement)
            loadingElement.style.display = "none";

        if (window.location.href.includes('localhost'))
            var loadingElement1 = document.getElementById("errorLoadingg");
        if (loadingElement1)
            loadingElement1.style.display = "block";
    };
}]);
//Prevent Template Cache by adding Version to html
app.factory('preventTemplateCache',
    function ($injector) {
        var ENV = HFC.Version;
        var Version = ENV.substring(0, ENV.indexOf(' '));
        return {
            'request': function (config) {
                if (config.url.indexOf('views') !== -1) {
                    config.url = config.url + '?t=' + Version;
                }
                else if (config.url.indexOf('/templates/') !== -1) {
                    config.url = config.url + '?t=' + Version;
                }

                return config;
            }
        }
    });

// For global unhandled exception handling
app.run(function ($rootScope) {
    $rootScope.$on("$locationChangeStart", function (event, next, current) {
        var loadingElement1 = document.getElementById("errorLoadingg");
        if (loadingElement1)
            loadingElement1.style.display = "none";
        if (window.location.href.includes('/news/'))
            $rootScope.newslogo = 'disableddiv';
        else
            $rootScope.newslogo = '';
    });
});
// for Deprecation Synchronous XMLHttpRequest Error Message on console
$.ajaxPrefilter(function (options, original_Options, jqXHR) {
    options.async = true;
});

var appAuth = angular.module('HFC_NGApp_Auth', []);
appAuth.factory('authInterceptorService', ['$q', '$injector', '$location', '$rootScope', function ($q, $injector, $location, $rootScope) {
    var authInterceptorServiceFactory = {};
    var $http;
    var numLoadings = 0;

    var _request = function (config) {
        numLoadings++;
        //show loader
        $rootScope.$broadcast("loader_show");

        config.headers = config.headers || {};
        if (config.type != "GET") {
            config.headers["__RequestVerificationToken"] = HFC.getVerificationToken(true);
        }
        return config;
        // return "GPWBNoy_LERTMvREswbN3EYSsOgpVtm4rEh1a2kXlVYjz-5TFm8Mjeb_aB2QNp9yY509ev6ueMTM3zpvDtHdMN5W4SHO71RQbE_GTE3xxdw1";
    }

    var _response = function (response) {
        if ((--numLoadings) === 0) {
            // Hide loader
            $rootScope.$broadcast("loader_hide");
        }
        return response || $q.when(response);
    }

    var _responseError = function (rejection) {
        if (!(--numLoadings)) {
            // Hide loader
        }

        $rootScope.$broadcast("loader_hide");

        if (rejection && rejection.data && rejection.data.modelState) {
            var errors = [];
            for (var key in response.data.modelState) {
                for (var i = 0; i < response.data.modelState[key].length; i++) {
                    errors.push(response.data.modelState[key][i]);
                }
            }
            var message = errors.join(' ');
        }

        return $q.reject(rejection);
    }

    var _retryHttpRequest = function (config, deferred) {
        $http = $http || $injector.get('$http');
        $http(config).then(function (response) {
            deferred.resolve(response);
        }, function (response) {
            deferred.reject(response);
        });
    }

    authInterceptorServiceFactory.request = _request;
    authInterceptorServiceFactory.response = _response;
    authInterceptorServiceFactory.responseError = _responseError;

    return authInterceptorServiceFactory;
}]);

//display time without using ignore timezone
app.filter('GlobalTime', function ($filter, calendarModalService) {
    return function (val, flag) {
        if (val) {
            let finalDate = '';
            if (typeof (val) == 'string') {
                finalDate = val.substr(0, 16);
                var newDate = calendarModalService.getDateStringValue(new Date(finalDate), true, false, false);
            }
            else
                var newDate = val;

            if (flag == "short")
                var formattedDate = $filter('date')(newDate, 'MM/dd/yyyy hh:mm a');
            else
                var formattedDate = $filter('date')(newDate, 'MM/dd/yyyy');

            return formattedDate;
        } else {
            return "";
        }
    };
});

app.filter('GlobalCurrency', function ($filter) {
    return function (val) {
        if (val == null)
            val = 0;
        var value = $filter('currency')(val, "$");
        return value;
    };
});



app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push('errorHttpInterceptor');
}]);

app.factory('errorHttpInterceptor', ['$q', '$rootScope', function ($q, $rootScope) {
        return {
            responseError: function responseError(rejection) {
                
                if (rejection.status === 401) {
                    $rootScope.hideConfirm = true;
                    var orginPath = location.origin + '/Account/Login?SessionExpired=True';
                    location.href = orginPath;
                    return;
                }
                $rootScope.hideConfirm = false;
                return $q.reject(rejection);
            }
        };
    }])
  

localStorage.clear();


