﻿var appCP = angular.module('HFC_NGAppCP', ['commonConfiguratorService',
    'unsavedChanges', 'ngRoute', 'ui.mask', 'ngMessages', 'ui.format'
    , 'hfc.address', 'ui.select2', 'summernote', 'ui.mask'
    , 'ui.helpers', 'treeControl', 'angular-cache', 'ui.dropzone'
    , 'mgcrea.ngStrap.modal', 'mgcrea.ngStrap.select', 'ngSanitize'
    , 'ui.sortable', 'hfc.note.service', 'mgcrea.ngStrap.aside'
    , 'kendo.directives', 'ui.bootstrap.pagination', 'mgcrea.ngStrap'
    , 'ui.bootstrap', 'ngMask', 'hfc.core.service', "globalMod",
    , "kendo.directives", "angucomplete-alt", 'hfc.newsandupdates'
    , 'hfc.Notemodule', 'hfc.Cachemodule', 'ui.tinymce', 'HFC_NGApp_Auth'
    , 'hfc.accountModule', 'hfc.leadModule', 'hfc.leadaccountModule'
    , 'hfc.calendarModule', 'hfc.personModule', 'hfc.fileUploadModule', 'hfc.fileUploadModuleVg', 'hfc.commentsModule', 'hfc.caseCommentsModule', 'jqwidgets', 'hfc.kendotooltipModule', 'HFC_UserApp'
    , 'changeRequestModule', 'feedbackModule', 'overviewModule', 'HFC_NGAppVendor', 'HFC_NGAppPIC', 'blurToCurrencyModule']);
appCP.config(function ($routeProvider, unsavedWarningsConfigProvider) {
    //$routeProvider.when("/sampleAngularJS", {
    //    controller: "sampleAngularJSController",
    //    templateUrl: "/app/views/admin/franchise/SampleFranchise.html"
    //});

    $routeProvider.when("/franchises", {
        controller: "FranchiseListingController",
        templateUrl: "/app/views/admin/franchise/franchises.html"
    });

    $routeProvider.when("/franchise/:FranchiseId", {
        controller: "FranchiseController",
        templateUrl: "/app/views/admin/franchise/franchise.html"
    });

    $routeProvider.when("/franchiseView/:FranchiseId", {
        controller: "FranchiseController",
        templateUrl: "/app/views/admin/franchise/franchiseDetail.html"
    });
    $routeProvider.when("/HFCconfigurator", {
        controller: "HomeOfficeconfiguratorController",
        templateUrl: "/app/views/configurator/HomeOfficeConfigurator.html"
    });
    // Home Office Case Management
    $routeProvider.when("/homeofficeCase", {
        controller: "HomeOfficeCaseListController",
        templateUrl: "/app/views/HomeOfficeCaseManagement/HomeOfficeCaseList.html",
    });

    //Job Crew Size
    $routeProvider.when("/jobcrewsize", {
        controller: "JobCrewSizeController",
        templateUrl: "/app/views/settings/operations/JobCrewSize/JobCrewSize.html",
    });

    $routeProvider.when("/CaseView/:CaseId/:FranchiseID", {
        controller: "HomeOfficeCaseViewController",
        templateUrl: "/app/views/HomeOfficeCaseManagement/CaseView.html",
    });

    $routeProvider.when("/HomeOfficeVendorView/:CaseId/:VendorCaseId/:VendorId", {
        controller: "HomeOfficeCaseVendorViewController",
        templateUrl: "/app/views/HomeOfficeCaseManagement/HomeOfficeCaseVendorView.html",
    });

    $routeProvider.when("/CaseViewFullDetails/:CaseId/:FranchiseID", {
        controller: "HomeOfficeCaseViewController",
        templateUrl: "/app/views/HomeOfficeCaseManagement/CaseViewFullDetails.html",
    });

    $routeProvider.when("/PurchaseMasterView/:PurchaseOrderId", {
        controller: "HomeOfficePurchaseOrderController",
        templateUrl: "/app/views/HomeOfficeCaseManagement/HomeOfficePurchaseView.html",
    });

    $routeProvider.when("/VendorDetails/:VendorId/:FranchiseID", {
        controller: "HomeOfficeVendorController",
        templateUrl: "/app/views/HomeOfficeCaseManagement/HomeOfficeVendorDetails.html",
    });


     //Fe Admin-Alliance Vendor
    $routeProvider.when("/alliancevendor/", {
        controller: "AdminAlliancevendorController",
        templateUrl: "/app/views/AdminOperations/AdminAlliancevendor.html",

        title: " Details"
    });

    //Core Product Model
    $routeProvider.when("/CoreProductModel/", {
        controller: "CoreProductController",
        templateUrl: "/app/views/AdminOperations/CoreProduct.html",
        title: " Core Product Model"
    });

    //Fe Admin-Currecncyconversion
    $routeProvider.when("/currencyconversion/", {
        controller: "AdminCurrencyConversionController",
        templateUrl: "/app/views/AdminFinance/AdminCurrencyConversion.html",

        title: " Details"
    });

    //Adding categories
    $routeProvider.when("/productCategory/", {
        controller: "categoryController",
        templateUrl: "/app/views/category/categoryView.html",

        title: "Category Management"
    });

    // Manage sources and channel
    $routeProvider.when("/editsourcelist/:Id", {
        controller: "sourceListController",
        templateUrl: "/app/views/sources/editSourceList.html",

        title: "Sources Management"
    });

    $routeProvider.when("/sourcelist/", {
        controller: "sourceListController",
        templateUrl: "/app/views/sources/sourceList.html",

        title: "Sources Management"
    });

    $routeProvider.when("/channellist/", {
        controller: "channelListController",
        templateUrl: "/app/views/sources/channelList.html",

        title: "Channel Management"
    });

    $routeProvider.when("/editchannellist/:Id", {
        controller: "channelListController",
        templateUrl: "/app/views/sources/editChannelList.html",

        title: "Channel Management"
    });

    $routeProvider.when("/ownerlist/", {
        controller: "ownerListController",
        templateUrl: "/app/views/sources/ownerList.html",

        title: "Owner Management"
    });

    $routeProvider.when("/editownerlist/", {
        controller: "ownerListController",
        templateUrl: "/app/views/sources/editOwnerList.html",

        title: "Owner Management"
    });

    //$routeProvider.when("/tpsources/", {
    //    controller: "tpSourceController",
    //    templateUrl: "/app/views/sources/tpSources.html",
    //    title: "TPSources Management"
    //});

    $routeProvider.when("/edittpsources/", {
        controller: "tpSourceController",
        templateUrl: "/app/views/sources/editTpSources.html",

        title: "TPSources Management"
    });

    //$routeProvider.when("/hfcsources/", {
    //    controller: "hfcSourceController",
    //    templateUrl: "/app/views/sources/hfcSources.html",
    //    title: "HFCSources Management"
    //});

    $routeProvider.when("/edithfcsources/", {
        controller: "hfcSourceController",
        templateUrl: "/app/views/sources/editHfcSources.html",

        title: "HFCSources Management"
    });

    $routeProvider.when("/hfcleadsourcemap/", {
        controller: "hfcLeadsourcemapController",
        templateUrl: "/app/views/sources/leadsourcemap.html",

        title: "HFCSources Management"
    });

    //---End--

    //master procurement board
    $routeProvider.when("/masterprocurementboard", {
        controller: "MasterProcurementDashboardController",
        templateUrl: "/app/views/masterprocurementDashboard/masterprocurementboard.html"
    });

    //Region : news & updates
    $routeProvider.when("/listNews", {
        controller: "newsListingController",
        templateUrl: "/app/views/settings/General/News/ListNews.html",

        title: "Manage News Updates"
    });

    $routeProvider.when("/news", {
        controller: "newsControllerCP",
        templateUrl: "/app/views/settings/General/News/News.html",

        title: "Manage News Updates"
    });

    $routeProvider.when("/news/:newsId", {
        controller: "newsControllerCP",
        templateUrl: "/app/views/settings/General/News/NewsDetails.html",

        title: "Manage News Updates"
    });

    $routeProvider.when("/newsedit/:newsId", {
        controller: "newsControllerCP",
        templateUrl: "/app/views/settings/General/News/News.html",

        title: "Manage News Updates"
    });

    //HFC Permission Set
    $routeProvider.when("/hfcrolesSearch", {
        controller: "HFCRoleSearchController",
        templateUrl: "/app/views/HFCRole/HFCRolesSearchView.html",
        title: "Roles Search"
    });

    $routeProvider.when("/hfcrolesView/:RoleId", {
        controller: "HFCRoleController",
        templateUrl: "/app/views/HFCRole/HFCRolesView.html",
        title: "Roles View"
    });
    $routeProvider.when("/hfcrolesSet/:RoleId", {
        controller: "HFCRoleController",
        templateUrl: "/app/views/HFCRole/HFCRolesView.html",
        title: "Roles View"
    });
    $routeProvider.when("/hfcPermissionSetsSearch", {
        controller: "HFCPermissionSetSearchController",
        templateUrl: "/app/views/HFCPermission/HFCPermissionSetSearch.html",

        title: "Permission Sets Search"
    });

    $routeProvider.when("/hfcPermissionSetsView/:PermissionSetId", {
        controller: "HFCPermissionSetController",
        templateUrl: "/app/views/HFCPermission/HFCPermissionSet.html",

        title: "Permission Sets"
    });
    $routeProvider.when("/hfcPermissionSet", {
        controller: "HFCPermissionSetController",
        templateUrl: "/app/views/HFCPermission/HFCPermissionSet.html",

        title: "Permission Set"
    });

    $routeProvider.when("/hfcPermissionSets/:PermissionSetId", {
        controller: "HFCPermissionSetController",
        templateUrl: "/app/views/HFCPermission/HFCPermissionSet.html",

        title: "Permission Sets"
    });

    // global attachments
    $routeProvider.when("/attachments", {
        controller: "globalSystemWideAttachmentscontroller",
        templateUrl: "/app/views/attachments/globalSystemWideAttachments.html",

        title: "Global Attachments"
    });
    $routeProvider.when("/VendorSetup", {
        controller: "VendorCaseController",
        templateUrl: "/app/views/VendorCaseConfig/VendorCaseConfig.html",

        title: "Vendor Case Config"
    });
    //End Region : news & updates

    //---Vendor Case Management---
    //$routeProvider.when("/VendorcaseEdit/:VendorCaseId", {
    //    controller: "VendorcaseManagementController",
    //    templateUrl: "/app/views/VendorCaseManagement/vendorEdit.html",
    //    title: " Details"
    //});

    //$routeProvider.when("/VendorCaseList", {
    //    controller: "VendorcaseManagementListController",
    //    templateUrl: "/app/views/VendorCaseManagement/VendorCaseList.html",
    //    title: " Details"
    //});

    //$routeProvider.when("/VendorcaseView/:VendorCaseId", {
    //    controller: "VendorcaseManagementViewController",
    //    templateUrl: "/app/views/VendorCaseManagement/vendorCaseView.html",
    //    title: " Details"
    //});
    //--End---

    //-----USER---
    $routeProvider.when("/users", {
        controller: "userController",
        templateUrl: "/app/views/settings/Security/users.html",
        //resolve: resolve,
        title: "Users List"
    });

     $routeProvider.when("/newuser", {
        controller: "userController",
        templateUrl: "/app/views/settings/Security/newuser.html",
        //resolve: resolve,
        title: "New User"
     });

     $routeProvider.when("/edituser/:userid", {
         controller: "userController",
         templateUrl: "/app/views/settings/Security/edituser.html",
         //resolve: resolve,
         title: "New User"
     });
    //Adding Product
     $routeProvider.when("/productSearch/:searchTerm?/:searchFilter?/:salesPersonIds?/:jobStatusIds?/:leadStatusIds?/:invoiceStatuses?/:sourceIds?/:selectedDateText?/:createdOnUtcStart?/:createdOnUtcEnd?/:commercialType?/:isReportSearch?", {
         controller: "SurfacingProductController",
         templateUrl: "/app/views/settings/operations/Products/productSearch.html",
     });
     $routeProvider.when("/productCreate/", {
         controller: "SurfacingProductController",
         templateUrl: "/app/views/settings/operations/Products/productadd.html",
     });
     $routeProvider.when("/productCreate/:VendorId", {
         controller: "SurfacingProductController",
         templateUrl: "/app/views/settings/operations/Products/productadd.html",
     });
     $routeProvider.when("/productVendor/:Guid", {
         controller: "SurfacingProductController",
         templateUrl: "/app/views/settings/operations/Products/productadd.html",

     });

     $routeProvider.when("/productEdit/:Productkey", {
         controller: "SurfacingProductController",
         templateUrl: "/app/views/settings/operations/Products/productadd.html",
     });
     $routeProvider.when("/productDetail/:Productkey", {
         controller: "SurfacingProductController",
         templateUrl: "/app/views/settings/operations/Products/productview.html",
     });
    //End of Product changes

    //vendor governance
     $routeProvider.when("/ChangeRequest", {
         controller: "ChangeRequestController",
         templateUrl: "/app/views/VendorGovernance/ChangeRequest.html",
     });
     $routeProvider.when("/ChangeRequestKanbanView", {
         controller: "ChangeRequestController",
         templateUrl: "/app/views/VendorGovernance/ChangeRequestKanbanView.html",
     });

     $routeProvider.when("/Feedback", {
         controller: "FeedbackController",
         templateUrl: "/app/views/VendorGovernance/Feedback.html",
     });

     $routeProvider.when("/FeedbackKanbanView", {
         controller: "FeedbackController",
         templateUrl: "/app/views/VendorGovernance/FeedbackKanbanView.html",
     });

     $routeProvider.when("/Overview", {
         controller: "OverviewController",
         templateUrl: "/app/views/VendorGovernance/Overview.html",
     });

     $routeProvider.when("/OverviewKanbanView", {
         controller: "OverviewController",
         templateUrl: "/app/views/VendorGovernance/OverviewKanbanView.html",
     });
    //----End---


    $routeProvider.otherwise({ redirectTo: "/franchises" });

    unsavedWarningsConfigProvider.useTranslateService = false;
});

appCP.constant('CONFIG', {
    apiServiceBaseUri: HFCNamespaceUrls.baseEndPoint,
});

appCP.run(function ($rootScope) {
    $rootScope.$on("$locationChangeStart", function (event, next, current) {
        // TODO: do we really need this???
        //////if (next.indexOf('search') > 0) {
        //////    $rootScope.enableSaveSearch = enableSaveSearch;
        //////} else {
        //////    $rootScope.enableSaveSearch = false;
        //////}
    });
});

var cacheBustSuffixFESettings = HFC.Version;

appCP.constant("cacheBustSuffixFESettings", cacheBustSuffixFESettings);

appCP.config(function ($httpProvider) {
    $httpProvider.interceptors.push('preventTemplateCache');
    $httpProvider.interceptors.push('authInterceptorService');
});


appCP.filter('ignoreTimeZone', function ($filter, calendarModalService) {
    return function (val, flag) {
        if (val) {
            let finalDate = '';
            if (typeof (val) == 'string') {
                finalDate = val.substr(0, 16);
                var newDate = calendarModalService.getDateStringValue(new Date(finalDate), true, false, false);
            }
            else
                var newDate = val;

            if (flag == "short")
                var formattedDate = $filter('date')(newDate, 'MM/dd/yyyy hh:mm a');
            else
                var formattedDate = $filter('date')(newDate, 'MM/dd/yyyy');

            return formattedDate;
        } else {
            return "";
        }
    };
});




appCP.run(function (HFCService) {
    HFCService.CurrentUserPermissions = permissionsList;
    HFCService.Roles = roles;
   });

var permissionsList = [];
var roles = [];

angular.element(document).ready(function () {

    if (!angular.element(document).scope())
    {
        $.ajax({
            url: '/api/Permission/0/GetUserRolesPermission',
            async: false,
            success: function (data) {
                //console.log("manual bootstraping");
                permissionsList = data;
                $.ajax({
                    url: '/api/users/',
                    async: false,
                    success: function (data) {
                        if (data != "") {
                            roles = data;
                            if (data[0] && data[0].RoleName && data[0].RoleName == "HFC Product Strategy Mgt (PSM)") {
                                var url = '/ControlPanel#!/ChangeRequest';
                                window.location.href = url;
                            }
                        }
                        var path = window.location.href;
                        if (path.includes("ControlPanel"))
                            angular.bootstrap(document, ['HFC_NGAppCP']);
                        else if(path.includes("controlpanel"))
                            angular.bootstrap(document, ['HFC_NGAppCP']);
                    },
                    error: function (data) {
                        angular.bootstrap(document, ['HFC_NGAppCP']);
                    }
                });
                
                },
            error: function (data) {
                angular.bootstrap(document, ['HFC_NGAppCP']);
            }
        });
    }

});

appCP.run(['$route', '$rootScope', '$location', function ($route, $rootScope, $location) {
    var original = $location.path;
    $location.path = function (path, reload) {
        if (reload === false) {
            var lastRoute = $route.current;
            var un = $rootScope.$on('$locationChangeSuccess', function () {
                $route.current = lastRoute;
                un();
            });
        }
        return original.apply($location, [path]);
    };

    $rootScope.$on('$viewContentLoaded', function () {
        var interval = setInterval(function () {
            if (document.readyState == "complete") {
                window.scrollTo(0, 0);
                clearInterval(interval);
            }
        }, 200);
    });

    //ipad calendar issue fix--

    var navigatorMonthFlag = false;
    var initDatepickerFlag = false;
    var eventBackup;
    var initFlag = true;
    var closeFlag = false;
    var globalElementId;
    var pickerFlag;
    var abc; var timer = 600;
    // for navigate months closes pop up issue fix
    var initNavigationEvent = function (e) {

        abc = e;
        var id = "#" + abc.sender.element[0]["id"] + "_dateview";
        var iddate = "#" + abc.sender.element[0]["id"] + "_timeview li";
        var inputid = "#" + abc.sender.element[0]["id"];
        globalElementId = inputid;
        pickerFlag = "dateTime";
        var element = id + ".k-calendar-container .k-calendar .k-header a";
        var elementcal = ".kendo-date-picker-wrapper .k-select .k-link-date";

        $(".k-calendar-container .k-calendar .k-header a").attr("href", "javascript:void(0)");
        $(element).click(function (event) {
           
            navigatorMonthFlag = true;
        });

        $(".k-calendar-container").click(function (event) {
            navigatorMonthFlag = false;
           
            var a = ((((($(event.target).parent()).parent()).parent()).parent()).parent()).parent();
            var id = "#" + a[0]["id"];
            if (id.indexOf("_") != -1) {
                var realid = id.split("_");
                var datepicker = $(realid[0]).data("kendoDateTimePicker");
                datepicker.close();
            }
            else {
                var datepicker = $(id).data("kendoDateTimePicker");
                datepicker.close();
            }

        });

        $(elementcal).click(function (event) {
           
            if (navigatorMonthFlag) {
                navigatorMonthFlag = false;
                var parentElement = $(event.target).parent();
                var baseParent = $($(parentElement).parent()).parent();
                var inputElement = $(baseParent).children()[0];
                var id = "#" + inputElement["id"];
                var datepicker = $(id).data("kendoDateTimePicker");
                datepicker.close();
            }

        });
        $(iddate).click(function (event) {
           
            closeFlag = true;
            navigatorMonthFlag = false;
            var a = $(event.target).parent();
            var b = a[0]["id"];
            var c = b.split("_");
            var id = "#" + c[0];

            var datepicker = $(id).data("kendoDateTimePicker");
            datepicker.close();

        });
    }
    //for date picker
    var initNavigationDatePickerEvent = function (e) {
        abc = e;
        var idvalue;
        var id;
        var url = window.location.href;
        if (url.includes("/leadsEditQualify") || url.includes("/purchasemasterview") || url.includes("/opportunityEdit")) {
            idvalue = ""; id = "";
        }
        else {
            idvalue = "#" + abc.sender.element[0]["id"];
            id = idvalue + "_dateview";
        }


        var inputid = "#" + abc.sender.element[0]["id"];
        globalElementId = inputid;
        pickerFlag = "date";
        var element = id + ".k-calendar-container .k-calendar .k-header a";
        // var elementcal = ".kendo-date-picker-wrapper .k-select .k-link-date";
        var elementcal = ".kendo-date-picker-date .k-select .k-i-calendar";


        $(".k-calendar-container .k-calendar .k-header a").attr("href", "javascript:void(0)");
        $(element).click(function (event) {

            pickerOpenFlag = false;
            
            navigatorMonthFlag = true;
        });

        $(".k-calendar-container").click(function (event) {
            navigatorMonthFlag = false;
           
            pickerOpenFlag = false;
            var a = ((((($(event.target).parent()).parent()).parent()).parent()).parent()).parent();
            var id = "#" + a[0]["id"];
            if (id.indexOf("{") != -1) {
                var datepicker = $(globalElementId).data("kendoDatePicker");
                datepicker.close();
            } else {
                if (id.indexOf("_") != -1) {
                    var realid = id.split("_");
                    var datepicker = $(realid[0]).data("kendoDatePicker");
                    datepicker.close();
                }
                else {
                    var datepicker = $(id).data("kendoDatePicker");
                    datepicker.close();
                }
            }
        });

        $(elementcal).click(function (event) {
            pickerOpenFlag = false;
           
            if (navigatorMonthFlag) {

                navigatorMonthFlag = false;
                var parentElement = $(event.target).parent();
                var baseParent = $($(parentElement).parent()).parent();
                var inputElement = $(baseParent).children()[0];
                var idElement = $(inputElement).children()[0];
                var id = "#" + idElement["id"];
                var datepicker = $(id).data("kendoDatePicker");
                datepicker.close();
            }

        });

    }
    //End

    var pickerOpenFlag;
    $rootScope.$on("$routeChangeSuccess", function (event, currentRoute, previousRoute) {
        initFlag = true;
        window.scrollTo(0, 0);
        setTimeout(function () {
            $(".tooltip-bottom").click(function (e) {
                var parent = $(e.target).parent()[0];
                $(parent).blur();
            });
        }, 5000);



        if (window.navigator.platform == "Mac68K" || window.navigator.platform == "MacPPC" || window.navigator.platform == "MacIntel") {

            timer = 600;

            $(document).click(function (e) {

                e.stopPropagation();

                // subscribe all date pickers

                setTimeout(function () {


                    var pickerList = $(".kendo-date-picker-wrapper");
                    console.log("&&&&&&&&    ", pickerList);
                    for (var i = 0; i < pickerList.length; i++) {
                        var id = pickerList[i]['id'];
                        if (id && id != "") {
                            id = "#" + id;
                            var dateTimePicker = $(id).data("kendoDateTimePicker");


                            dateTimePicker.bind("open", function (e) {

                                closeFlag = false;
                                navigatorMonthFlag = false;
                                initDatepickerFlag = true;
                                initNavigationEvent(e);


                            });
                            dateTimePicker.bind("close", function (e) {
                                eventBackup = e;

                                if (initDatepickerFlag) {
                                    if (closeFlag == false) { e.preventDefault(); closeFlag = true; }
                                    setTimeout(function () {
                                        initDatepickerFlag = false;
                                        var id = "#" + eventBackup.sender.element[0]["id"];
                                        var datepicker = $(id).data("kendoDateTimePicker");

                                        datepicker.close();
                                    }, 200);
                                }

                                else {
                                    if (navigatorMonthFlag) {
                                        eventBackup.preventDefault();
                                    }
                                }
                            });
                        }
                    }
                    //End date time picker

                    //date picker

                    var picker = $(".kendo-date-picker-date");
                    console.log("&&&&&&&&    ", picker);
                    for (var i = 0; i < picker.length; i++) {
                        var ids = picker[i]['id'];
                        if (ids && ids != "") {
                            ids = "#" + ids;
                            var datesPicker = $(ids).data("kendoDatePicker");

                            datesPicker.bind("open", function (e) {

                                closeFlag = false;
                                navigatorMonthFlag = false;
                                initDatepickerFlag = true;
                                initNavigationDatePickerEvent(e);
                                var url = window.location.href;
                                if (url.includes("purchasemasterview") || url.includes("opportunityEdit")) {
                                    pickerOpenFlag = true;
                                }

                            });
                            datesPicker.bind("close", function (e) {
                                eventBackup = e;

                                if (initDatepickerFlag) {
                                    if (closeFlag == false) { e.preventDefault(); closeFlag = true; }
                                    setTimeout(function () {

                                        initDatepickerFlag = false;
                                        var ids = "#" + eventBackup.sender.element[0]["id"];
                                        var datesPicker = $(ids).data("kendoDatePicker");

                                        datesPicker.close();
                                    }, 200);
                                }

                                else {
                                    if (navigatorMonthFlag) {
                                        eventBackup.preventDefault();
                                    }
                                    var url = window.location.href;
                                    if ((url.includes("purchasemasterview") && pickerOpenFlag) || (url.includes("opportunityEdit") && pickerOpenFlag)) {

                                        eventBackup.preventDefault();
                                    }
                                }
                            });
                        }
                    }



                    //End Date picker
                }, timer);

                if (navigatorMonthFlag) {
                    navigatorMonthFlag = false;
                    var picker;
                    if (pickerFlag == "dateTime") {
                        picker = $(globalElementId).data("kendoDateTimePicker");
                    } else if (pickerFlag == "date") {
                        picker = $(globalElementId).data("kendoDatePicker");

                    }
                    if (picker) {
                        picker.close();
                    }
                }
                initFlag = false;
            });
            $(document).click();
        }

    });

    //End----
}]);

appCP.run(function ($rootScope, $templateCache) {
    $templateCache.removeAll();
});

// TODO: ???
//////localStorage.clear();

//
//'use strict';
//appCP.factory('authInterceptorService', ['$q', '$injector', '$location', '$rootScope', function ($q, $injector, $location, $rootScope) {
//    var authInterceptorServiceFactory = {};
//    var $http;
//    var numLoadings = 0;

//    var _request = function (config) {
//        numLoadings++;
//        //show loader
//        $rootScope.$broadcast("loader_show");

//        config.headers = config.headers || {};
//        if (config.type != "GET") {
//            config.headers["__RequestVerificationToken"] = HFC.getVerificationToken(true);
//        }

//        return config;
//    }

//    var _response = function (response) {
//        if ((--numLoadings) === 0) {
//            // Hide loader
//            $rootScope.$broadcast("loader_hide");
//        }
//        return response || $q.when(response);
//    }

//    var _responseError = function (rejection) {
//        if (!(--numLoadings)) {
//            // Hide loader
//        }

//        $rootScope.$broadcast("loader_hide");

//        if (rejection.data.modelState) {
//            var errors = [];
//            for (var key in response.data.modelState) {
//                for (var i = 0; i < response.data.modelState[key].length; i++) {
//                    errors.push(response.data.modelState[key][i]);
//                }
//            }
//            var message = errors.join(' ');

//        }

//        return $q.reject(rejection);
//    }

//    var _retryHttpRequest = function (config, deferred) {
//        $http = $http || $injector.get('$http');
//        $http(config).then(function (response) {
//            deferred.resolve(response);
//        }, function (response) {
//            deferred.reject(response);
//        });
//    }

//    authInterceptorServiceFactory.request = _request;
//    authInterceptorServiceFactory.response = _response;
//    authInterceptorServiceFactory.responseError = _responseError;

//    return authInterceptorServiceFactory;
//}]);

appCP.controller('NavbarControllerCPTP', [
        '$http', '$scope', '$window', '$location', '$rootScope', 'NavbarServiceCP', 'FranchiseService'
        , function ($http, $scope, $window, $location, $rootScope, NavbarServiceCP, FranchiseService) {
            $("#navvcp").css({ "display": "block" });
            $("#navvcp_side").css({ "display": "block" });

            $scope.NavbarServiceCP = NavbarServiceCP;
            $scope.FranchiseService = FranchiseService;
        }
])
  .service('NavbarServiceCP', [
        '$http', '$rootScope', function ($http, $rootScope) {
            var srv = this;
            srv.hidemenusCP = true;

            function DeslectAll() {
                srv.FranchisesSelected = "";
                srv.ReportsSelected = "";
                srv.SettingsSelected = "";
                srv.AdministrationSelected = "";
                srv.OperationsSelected = "";
                srv.MarketingSelected = "";
            }

            $.ajax({
                url: '/api/Color/0/Headercolor',
                async: false,
                success: function (data) {
                    srv.headercolor = data;
                }
            });

            //Franchise
            srv.SelectFranchises = function () {
                DeslectAll();
                srv.FranchisesSelected = "selected";
            }
            //Admin
            srv.Administration = function () {
                DeslectAll();
                srv.AdministrationSelected = "selected"
            }
            //Operation
            srv.Operations = function () {
                DeslectAll();
                srv.OperationsSelected = "selected"
            }

            //Market
            srv.Market = function () {
                DeslectAll();
                srv.MarketingSelected = "selected"
            }
            //Finance
            srv.SelectReports = function () {
                DeslectAll();
                srv.ReportsSelected = "selected";
            }

            //srv.SelectSettings = function () {
            //    DeslectAll();
            //    srv.SettingsSelected = "selected";
            //}

            // Refining Top Level Navigation

            function DeselectAllTop() {
                srv.franchiseTab = "";
                srv.currencyTab = "";
                srv.operationsTab = "";
                srv.vendorsTab = "";
                srv.productsTab = "";
                srv.configuratorTab = "";
                srv.news_updatesTab = "";
                srv.caseManagementTab = "";
                srv.vendorGovernanceTab = "";
                srv.jobCrewSizeTab = "";

                srv.SetupFranchiseInfo = "";
                srv.SetupAdministrationInfo = "";
                srv.SetupOperationsInfo = "";
                srv.SetupMarketInfo = "";

                srv.Marketing = "";
                srv.CurrencyCurrrency = "";
                srv.OperationsSourceList = "";
                //srv.OperationsChannelList = "";
                //srv.OperationsOwnerList = "";
                srv.OperationsTPSources = "";
                srv.OperationsHFCSource = "";
                srv.OperationsUTMCampaign = "";
                srv.VendorsVendors = "";
                srv.productsCategoryManagement = "";
                srv.surfacingProductManagement = "";
                srv.news_updatesNews_updates = "";
                srv.masterprocurementboard = "";
                srv.globalAttachments = "";
                srv.CaseManagement = "";


                srv.hidemenusCP = false;

                srv.HFCPermissionSetSelected = "";
            }

            srv.SetupFranchiseInfoTab = function () {
                srv.SelectFranchises();
                DeselectAllTop();
                srv.franchiseTab = "active";
                srv.SetupFranchiseInfo = "active";
            }
            //new Admin -- START --
            srv.SetupAdministrationInfoTab = function () {
                srv.Administration();
                DeselectAllTop();
                srv.news_updatesTab = "active";
                srv.SetupAdministrationInfo = "";
            }

            srv.selectGlobalSystemwideAttachments = function () {
                srv.Administration();
                DeselectAllTop();
                srv.globalAttachments = "active";
            }

            // Permission and sets

            srv.GlobalPermissionSets = function () {
                srv.Administration();
                DeselectAllTop();
                srv.HFCPermissionSetSelected = "active";
            }

            //--END--

            //new Operations -- START--
            srv.SetupOperationsInfoTab = function () {
                srv.Operations();
                DeselectAllTop();
                srv.masterprocurementboard = "active";
                srv.vendorsTab = "";
                srv.productsTab = "";
                srv.configuratorTab = "";
                srv.SetupOperationsInfo = "active";
            }

            srv.VendorsVendorsTab = function () {
                srv.Operations();
                DeselectAllTop();
                srv.vendorsTab = "active";
                srv.VendorsVendors = "active";
            }

            srv.productsCategoryManagementTab = function () {
                srv.Operations();
                DeselectAllTop();
                srv.productsTab = "active";
                srv.productsCategoryManagement = "active";
                srv.configuratorTab = "";
            }
            srv.surfacingProductManagementTab = function () {
                srv.Operations();
                DeselectAllTop();
                srv.productsTab = "active";
                srv.surfaceProductsManagement = "active";
                srv.productsCategoryManagement = "";
                srv.configuratorTab = "";
            }           

            srv.PICConfiguratorTab = function () {
                srv.Operations();
                DeselectAllTop();
                srv.productsTab = "";
                srv.productsCategoryManagement = "";
                srv.configuratorTab = "active";
            }

            srv.CaseMangementTab = function () {
                srv.Operations();
                DeselectAllTop();
                srv.caseManagementTab = "active";
                srv.CaseMangement = "active";
            }

            //vendor governance
            srv.VendorGovernanceTab = function () {
                srv.Operations();
                DeselectAllTop();
                srv.vendorGovernanceTab = "active";
                srv.VendorGovernance = "active";
            }

            srv.SetupOperationsCrewSizeTab = function () {
                srv.Operations();
                DeselectAllTop();
                srv.jobCrewSizeTab = "active";
            }

            //--END--

            //Marketting -- START--
            srv.SetupMarketInfoTab = function () {
                srv.Market();
                DeselectAllTop();
                srv.Marketing = "active";
                srv.SetupMarketInfo = "active";
            }

            //--END--

            //Finance -- START --
            srv.CurrencyCurrrencyTab = function () {
                srv.SelectReports();
                DeselectAllTop();
                srv.currencyTab = "active";
                srv.CurrencyCurrrency = "active";
            }
            //-- END --
            srv.OperationsSourceListTab = function () {
                srv.SelectSettings();
                DeselectAllTop();
                srv.operationsTab = "active";
                srv.OperationsSourceList = "active";
            }
            //srv.OperationsChannelListTab = function () {
            //    srv.SelectSettings();
            //    DeselectAllTop();
            //    srv.operationsTab = "active";
            //    srv.OperationsChannelList = "active";
            //}
            //srv.OperationsOwnerListTab = function () {
            //    srv.SelectSettings();
            //    DeselectAllTop();
            //    srv.operationsTab = "active";
            //    srv.OperationsOwnerList = "active";
            //}
            //srv.OperationsTPSourcesTab = function () {
            //    srv.SelectSettings();
            //    DeselectAllTop();
            //    srv.operationsTab = "active";
            //    srv.OperationsTPSources = "active";
            //}
            //srv.OperationsHFCSourceTab = function () {
            //    srv.SelectSettings();
            //    DeselectAllTop();
            //    srv.operationsTab = "active";
            //    srv.OperationsHFCSource = "active";
            //}
            //srv.OperationsUTMCampaignTab = function () {
            //    srv.SelectSettings();
            //    DeselectAllTop();
            //    srv.operationsTab = "active";
            //    srv.OperationsUTMCampaign = "active";
            //}

            //srv.newsAndUpdatesTab = function () {
            //    srv.SelectSettings();
            //    DeselectAllTop();
            //    srv.news_updatesTab = "active";
            //    srv.news_updatesNews_updates = "active";
            //}

            //srv.masterprocurementboardTab = function () {
            //    srv.SelectSettings();
            //    DeselectAllTop();
            //    srv.masterprocurementboard = "active";
            //    //srv.news_updatesNews_updates = "active";
            //}
        }
  ]);
appCP.factory('$exceptionHandler', ['$log', function ($log, $location) {
    return function myExceptionHandler(exception, cause) {
        $log.error(exception, cause);
        var loadingElement = document.getElementById("loading");
        if (loadingElement)
            loadingElement.style.display = "none";

        if (window.location.href.includes('localhost'))
            var loadingElement1 = document.getElementById("errorLoadingg");
        if (loadingElement1)
            loadingElement1.style.display = "block";
    };
}]);
// For global unhandled exception handling
appCP.run(function ($rootScope) {
    $rootScope.$on("$locationChangeStart", function (event, next, current) {
        var loadingElement1 = document.getElementById("errorLoadingg");
        if (loadingElement1)
            loadingElement1.style.display = "none";
    });
});
//Prevent Template Cache by adding Version to html
appCP.factory('preventTemplateCache',
    function ($injector) {
        var ENV = HFC.Version;
        var Version = ENV.substring(0, ENV.indexOf(' '));
        return {
            'request': function (config) {
                if (config.url.indexOf('views') !== -1) {
                    config.url = config.url + '?t=' + Version;
                }
                else if (config.url.indexOf('/templates/') !== -1) {
                    config.url = config.url + '?t=' + Version;
                }

                return config;
            }
        }
    });

//display time without using ignore timezone
appCP.filter('GlobalTime', function ($filter, calendarModalService) {
    return function (val, flag) {
        if (val) {
            let finalDate = '';
            if (typeof (val) == 'string') {
                finalDate = val.substr(0, 16);
                var newDate = calendarModalService.getDateStringValue(new Date(finalDate), true, false, false);
            }
            else
                var newDate = val;

            if (flag == "short")
                var formattedDate = $filter('date')(newDate, 'MM/dd/yyyy hh:mm a');
            else
                var formattedDate = $filter('date')(newDate, 'MM/dd/yyyy');

            return formattedDate;
        } else {
            return "";
        }
    };
});

//added this since we made usercreation module common for admin and fe side
//the top navbar are getting disable/not selected when page get refesh
//added this since the service value needed for selection.
appCP.service('NavbarService', [function () {
    //var srv = this;
}]);

appCP.filter('GlobalCurrency', function ($filter) {
    return function (val) {
        var value = $filter('currency')(val, "$");
        return value;
    };
});

appCP.directive('validDomainName', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attributes, control) {
            control.$validators.validDomainName = function (modelValue, viewValue) {
                var EXP = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;
                if (viewValue != undefined && viewValue.includes('@') && (viewValue.split('@')[1].includes('_') || viewValue.split('@')[1].includes('..')) && EXP.test(viewValue)) // correct value
                {
                    return false;
                }
                else
                    return true; // wrong value
            };
        }
    };
});
appCP.directive('validEmailRegx', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attributes, control) {
            control.$validators.validEmailRegx = function (modelValue, viewValue) {
                var EXP = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;
                if (control.$isEmpty(modelValue)) // if empty, correct value
                {
                    return true;
                }
                if (EXP.test(viewValue)) // correct value
                {
                    return true;
                }
                return false; // wrong value
            };
        }
    };
});

appCP.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push('errorHttpInterceptor');
}]);

appCP.factory('errorHttpInterceptor', ['$q', '$rootScope', function ($q, $rootScope) {
    return {
        responseError: function responseError(rejection) {

            if (rejection.status === 401) {
                $rootScope.hideConfirm = true;
                location.reload();
                return;
            }
            $rootScope.hideConfirm = false;
            return $q.reject(rejection);
        }
    };
}])

