﻿
'use strict';
app.filter('htmlToPlaintext', function () {
    return function (text, length, end) {
        var htmlToPlaintextOld;
        if (isNaN(length))
            length = 10;

        if (end === undefined)
            end = "...";

        htmlToPlaintextOld = String(text).replace(/<[^>]+>/gm, '');

        if (htmlToPlaintextOld.length <= length ) {
            return htmlToPlaintextOld;
        }
        else {
           
            return String(htmlToPlaintextOld).substring(0, length - end.length) + end;
        }

    }
});

