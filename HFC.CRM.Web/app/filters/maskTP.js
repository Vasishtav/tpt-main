﻿
'use strict';
(function () {
    'use strict';
    angular.module('globalMod').filter('mask', ['MaskService', function (MaskService) {
        return function (text, mask) {
            if (!text)
                text = "";
            var maskService = MaskService.create();
            maskService.generateRegex({ mask: mask });

            return maskService.getViewValue(text).withDivisors();
        };
    }]);
})();
