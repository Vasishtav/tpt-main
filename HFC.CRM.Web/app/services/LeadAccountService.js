﻿
(function (window, document, undefined) {
    'use strict';
    angular.module('hfc.leadaccountModule', [])

    .service('LeadAccountService', ['$http', function ($http) {
        var srv = this;

        srv.IsBusy = false;
        srv.Accounts = [];
        srv.AccountStatuses = [];
        srv.PurchaseOrdersCount = 0;

        srv.GetStatus = function (statId) {
            var stat = $.grep(srv.AccountStatuses, function (stat) {
                return stat.Id == statId;
            });
            if (stat)
                return stat[0];
            else
                return null;
        };

        srv.GetAccountStatuses = function (account) {

            var arr = [];
            var parents = $.grep(srv.AccountStatuses, function (stat) {
                return stat.ParentId == null;
            });
            angular.forEach(parents, function (stat) {
                arr.push(stat);
                var child = $.grep(srv.AccountStatuses, function (child) {
                    return child.ParentId == stat.Id;
                });
                if (child)
                    arr = arr.concat(child);
            });


            var selectedStatus = $.grep(arr, function (e) {
                return e.Id == account.AccountStatusId;
            });

            if (!selectedStatus || selectedStatus == 0) {
                var missedStatus = $.grep(srv.AccountStatuses, function (e) {
                    return e.Id == account.AccountStatusId;
                });

                if (missedStatus && missedStatus.length > 0) {
                    arr = arr.concat(missedStatus[0]);
                }
            }

            return arr;
        };

        srv.GetStatusLabel = function (stat) {
            var parent = null;
            if (stat.ParentId)
                parent = srv.GetStatus(stat.ParentId);
            if (parent)
                return '<span class="left-margin">' + stat.Name + ' <span class="text-muted">' + parent.Name + '</span></span>';
            else
                return stat.Name;
        };

        //utility to return a class name linked to a status id
        srv.AccountStatusCss = function (statusId) {
            var css = null;
            if (statusId && srv.AccountStatuses) {
                angular.forEach(srv.AccountStatuses, function (stat) {
                    if (stat.Id == statusId) {
                        if (stat.ParentId)
                            css = srv.AccountStatusCss(stat.ParentId);
                        else
                            css = stat.ClassName;
                        return;
                    }
                });
            }
            return css || "btn-default";
        };

        //gets a single account and store it in this service as well as any pass in reference
        srv.Get = function (accountId, success, onError) {
            if (!isNaN(accountId) && accountId > 0) {
                $http.get('/api/accounts/' + accountId).then(function (response) {

                    srv.AccountStatuses = response.data.AccountStatuses || [];
                    var accountServiceData = response.data;
                    //var primaryNotes = response.data.AccountPrimaryNotes;
                     
                    srv.PurchaseOrdersCount = response.data.PurchaseOrdersCount;

                    //if (primaryNotes) {
                    //    AccountServiceData.Account.PrimaryNotes = primaryNotes.Message;
                    //}

                    //change dates to XDate for easier manipulation
                    //accountServiceData.Account.CreatedOnUtc = new XDate(accountServiceData.Account.CreatedOnUtc, true);

                    srv.Accounts.push(accountServiceData);

                    if (success)
                        success(accountServiceData);
                }, function (response) {
                    if (onError) {
                        onError(response);
                        return;
                    }

                    HFC.DisplayAlert(response.statusText);
                });
            }
        };


        srv.AccountDispositionByAccountStatus = function (id, onError) {
            if (id == undefined || id == null) {
                id = 0;
            }
            $http.get('/api/lookup/' + id + '/Dispositions')
                .then(function (data) {
                    srv.AccountDispositionlist = data.data;
                }, function (resultError) {
                    if (onError) {
                        onError(resultError);
                    }
                });
        }



        srv.SaveStatus = function (accountId, statId) {
            if (!srv.IsBusy) {
                srv.IsBusy = true;
                $http.put('/api/accounts/' + accountId + '/status', { Id: statId }).then(function () {
                    HFC.DisplaySuccess("Account status successfully changed");
                    srv.IsBusy = false;
                }, function (response) {
                    HFC.DisplayAlert(response.statusText);
                    srv.IsBusy = false;
                });
            }
        }

    }]);

})(window, document);
   
