﻿appCP.service('AdminSourceChannelServiceTP',
['$http',
function ($http) {
    var adminSourceChannelServiceTP = {};

    // set editable row setter
    adminSourceChannelServiceTP.setEditableRow = function (row) {
        adminSourceChannelServiceTP.editableRow = row;
        console.log("%^%^%^%^%^%     ", adminSourceChannelServiceTP.editableRow);
    }
    
    // set channel value after save
    adminSourceChannelServiceTP.setValuetorow = function (rowval) {
        adminSourceChannelServiceTP.valueRow = rowval;
        console.log("%^%^%^%^%^%     ", adminSourceChannelServiceTP.valueRow);
    }

    //set the sourcevalue after save
    adminSourceChannelServiceTP.setSourceValtorow = function (rowSourceval) {
        adminSourceChannelServiceTP.sourceValRow = rowSourceval;
        console.log("%^%^%^%^%^%     ", adminSourceChannelServiceTP.sourceValRow);
    }

    //pagenumber
    adminSourceChannelServiceTP.setPageNumber = function (pagenum) {
        adminSourceChannelServiceTP.pagenumber = pagenum;
        console.log("%^%^%^%^%^%     ", adminSourceChannelServiceTP.pagenumber);
    }

    //get or assign the row edited.
    adminSourceChannelServiceTP.getEditableRow = function () {
        return adminSourceChannelServiceTP.editableRow;
    }

    //get or assign the channel value added newly
    adminSourceChannelServiceTP.getChannelValue = function () {
        return adminSourceChannelServiceTP.valueRow;
    }

    //get or assign the source value added newly
    adminSourceChannelServiceTP.getSourceValue = function () {
        return adminSourceChannelServiceTP.sourceValRow;
    }

    //pagenumber
    adminSourceChannelServiceTP.getPageNumberValue = function () {
        return adminSourceChannelServiceTP.pagenumber;
    }

    adminSourceChannelServiceTP.clearValue = function () {
        adminSourceChannelServiceTP.valueRow=null;
        adminSourceChannelServiceTP.sourceValRow = null;
        adminSourceChannelServiceTP.editableRow = null;
        adminSourceChannelServiceTP.pagenumber = null;
    }

    adminSourceChannelServiceTP.setIcon = function (val) {
        adminSourceChannelServiceTP.val_Icon = val;
     }

    adminSourceChannelServiceTP.getIcon = function () {
        return adminSourceChannelServiceTP.val_Icon;
    }
   
    return adminSourceChannelServiceTP;
}
]);