﻿
(function (window, document, undefined) {
    'use strict';
    angular.module('hfc.fileUploadModuleVg', []) // ['hfc.core.service', 'hfc.accountModule', 'hfc.leadModule', 'hfc.leadaccountModule'])
        .directive('fileUploadVg', ['$http', 'FileUploadServiceVg', 'FranchiseService', function ($http, FileUploadServiceVg, FranchiseService
        ) {
            return {
                templateUrl: 'app/views/VendorGovernance/attachmentVg.html',
                link: function (scope, element) {
                    scope.filesizee = '';
                    scope.ftype = '';
                    scope.FileUploadServiceVg = FileUploadServiceVg;
                    scope.FranchiseService = FranchiseService;

                    element.bind('change', function () {
                        scope.$apply(function () {
                            scope.files = document.getElementById('uploadFileInput').files;

                            scope.data1 = [];
                            if (scope.files) {
                                for (var ii = 0; ii < scope.files.length; ii++) {
                                    if (scope.files[ii].size !== 0 && scope.files[ii].size < (1024 * 1024)) { // file should be less than 5MB
                                        var ext = scope.files[ii].name.split('.').pop();
                                        var size = bytesToSize(scope.files[ii].size);

                                        var todayy = new Date();
                                        //var today = todayy.getUTCDate()
                                        var dd = todayy.getUTCDate();
                                        var mm = todayy.getUTCMonth() + 1; //January is 0!
                                        var yyyy = todayy.getUTCFullYear();

                                        if (dd < 10) {
                                            dd = '0' + dd
                                        }

                                        if (mm < 10) {
                                            mm = '0' + mm
                                        }

                                        var today = mm + '/' + dd + '/' + yyyy;

                                        var clr = '';

                                        if (ext.toLowerCase() == 'pdf') { clr = 'red'; scope.ftype = 'far fa-file-pdf'; }
                                        else if (ext.toLowerCase() == 'doc' || ext.toLowerCase() == 'docx' || ext.toLowerCase() == 'txt') { clr = 'blue'; scope.ftype = 'far fa-file-alt'; }
                                        else if (ext.toLowerCase() == 'xls' || ext.toLowerCase() == 'xlsx') { clr = 'green'; scope.ftype = 'far fa-file-excel'; }
                                        else if (ext.toLowerCase() == 'png' || ext.toLowerCase() == 'jpg' || ext.toLowerCase() == 'jpeg' || ext.toLowerCase() == 'gif') { clr = 'violet'; scope.ftype = 'far fa-image'; }
                                        else { clr = ''; scope.ftype = 'far fa-file-alt'; }

                                        scope.filesizee = today + ' | ' + size + ' | ' + ext;
                                        scope.data1.push({ streamId: '', FileIconType: scope.ftype, FileDetails: scope.filesizee, FileName: scope.files[ii].name });
                                    }
                                }
                                if (scope.data1) {
                                    //FileUploadServiceVg.UploadFile(scope.files, scope.data1);
                                    if (FileUploadServiceVg.CaseId == 0) {
                                        if (FileUploadServiceVg.files != null && FileUploadServiceVg.files.length > 0) {
                                            FileUploadServiceVg.files = toArray(FileUploadServiceVg.files).concat(toArray(scope.files));
                                        }
                                        else
                                            FileUploadServiceVg.files = scope.files

                                        if (FileUploadServiceVg.filesDescription != null && FileUploadServiceVg.filesDescription.length > 0)
                                            FileUploadServiceVg.filesDescription = FileUploadServiceVg.filesDescription.concat(scope.data1);
                                        else
                                            FileUploadServiceVg.filesDescription = scope.data1;
                                    } else {
                                        FileUploadServiceVg.files = scope.files;
                                        FileUploadServiceVg.filesDescription = scope.data1;
                                    }
                                    FileUploadServiceVg.UploadFile(false);
                                }
                                else {
                                    if (scope.files !== undefined)
                                        alert("File is not valid or corrupted");
                                }
                                document.getElementById("uploadFileInput").innerHTML = null;
                                document.getElementById("uploadFileInput").files = null;
                            }
                        });
                    });

                    function bytesToSize(bytes) {
                        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
                        if (bytes == 0) return '0 Byte';
                        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
                        return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
                    };

                    function toArray(fileList) {
                        return Array.prototype.slice.call(fileList);
                    }
                }
            };
        }])
        .service('FileUploadServiceVg', function ($http, $q, FranchiseService) {
            var srv = this;
            srv.CaseId = 0;
            srv.Module = '';
            srv.VendorId = 0;
            srv.showattachment = false;
            srv.Showtimezone = false;

            srv.files = [];
            srv.filesDescription = [];
            //srv.files = [];
            srv.css1 = "";
            srv.css2 = "";
            srv.css3 = "";
            srv.css4 = "";
            srv.disableLineDropDown = false;

            srv.SetCaseId = function (value, module) {
                srv.CaseId = value;
                srv.showattachment = true;
                srv.Module = module;
                if (srv.CaseId != 0)
                    this.getCaseDocList();
                else {
                    srv.files = [];
                    srv.filesDescription = [];
                    srv.AttachmentList = [];
                }

                //$http.get('/api/CaseManageAddEdit/' + this.CaseId + '/getFranchiseTimeZone?CaseId=' + srv.CaseId).then(function (data) {
                //    srv.FranchiseTimeZone = data.data;
                //}, function (error) {

                //});
            }

            srv.ShowFranchisetimezone = function (flag) {
                srv.Showtimezone = flag;
            }

            srv.clearCaseId = function () {
                srv.CaseId = 0;
                srv.Module = '';
                srv.AttachmentList = [];
                srv.lineIdList = [];
                srv.showattachment = false;
            }
            // this.SetCaseId(1000);
            srv.AttachmentList = [];

            srv.getCaseDocList = function () {
                $http.get('/api/Case/' + this.CaseId + '/getCaseAttachmentsList?flag=' + srv.Showtimezone).then(function (data) {
                    srv.AttachmentList = data.data;
                    srv.bindToList();
                }, function (error) {

                });
            }

            srv.bindToList = function () {
                if (srv.AttachmentList != null) {
                    if (srv.CaseId == 0) {
                        for (var i = 0; i < srv.AttachmentList.length; i++) {
                            var FileDetails = srv.AttachmentList[i].FileDetails.split("|");
                            srv.AttachmentList[i].Id = i + 1;
                            srv.AttachmentList[i].CreatedOn = FileDetails[0];
                            srv.AttachmentList[i].Size = FileDetails[1];
                        }
                    }
                    else {
                        for (var i = 0; i < srv.AttachmentList.length; i++) {
                            var FileDetails = srv.AttachmentList[i].FileDetails.split("|");                            
                            srv.AttachmentList[i].CreatedOn = FileDetails[0];
                            srv.AttachmentList[i].Size = FileDetails[1];
                        }
                    }
                    //srv.AttachmentList.CreatedOn = FileDetails[0];
                    var dataSource = new kendo.data.DataSource({ data: srv.AttachmentList, pageSize: 25 });
                    var grid = $('#VendorAttachmentGrid1').data("kendoGrid");
                    dataSource.read();
                    grid.setDataSource(dataSource);
                    if (window.location.href.toUpperCase().includes('CHANGEREQUESTVIEW')) {
                        grid.hideColumn(3);
                    }
                }
            };

            srv.VendorAttachment = {
                dataSource: {
                    error: function (e) {
                        HFC.DisplayAlert(e.errorThrown);
                    },
                    schema: {
                        model: {
                            Id: "Id",
                            fields: {
                                Id: { editable: false, nullable: true },
                                StreamId: { editable: false, type: "string" },
                                FileName: { editable: false, type: "string" },
                                CreatedOn: { editable: false, type: "string" },
                                Size: { editable: false, type: "string" }
                            }
                        }
                    }
                },
                columns: [
                    {
                        field: "FileName",
                        title: "Attachment",
                        template: function (dataItem) {
                            if (window.location.href.toUpperCase().includes('CHANGEREQUESTCREATE'))
                                return "<span>" + dataItem.FileName + "</span>";
                            else
                                return "<a href='/api/Download?streamid=" + dataItem.StreamId + "' target='_blank' style='color:#2a6496 !important;'> " + dataItem.FileName + "</a>";
                        },
                    },
                    {
                        field: "CreatedOn",
                        title: "Created On",
                    },
                    {
                        field: "Size",
                        title: ""
                    },
                    {
                        field: "",
                        title: "",
                        width: "50px",
                        template: function (dataItem) {
                            return "<a ng-hide='FileUploadServiceVg.CheckCreatedBy(dataItem)' ng-click='FileUploadServiceVg.removeCaseAttachmentsDetail(dataItem.Id)' style='color:#3e7d8a;'> <i class='far fa-trash-alt'> </i> </a>";
                        }
                    }
                ],
                noRecords: { template: '<span style="margin-top:20px">No records found</span>' },
                filterable: false,
                resizable: false,
                sortable: false,
                scrollable: true,
                pageable: true,
                //toolbar: [
                //        {
                //            template: kendo.template($("#headToolbarTemplate").html())
                //        }]
            };

            // this.getfranchiseCaseList();

            //srv.UploadFile = function (file, description) {
            srv.UploadFile = function (flag) {
                if (srv.CaseId == 0) {
                    //srv.files = file;
                    srv.AttachmentList = srv.filesDescription;
                    srv.bindToList();
                    return false;
                };

                var file = srv.files;
                var description = srv.filesDescription;
                var formData = new FormData();
                for (var ee = 0; ee < file.length; ee++) {
                    formData.append("file", file[ee]);
                }
                // formData.append("file", file);
                formData.append("description", angular.toJson(description));

                var defer = $q.defer();

                //  $http.post("api/File/" + srv.CaseId + "/UploadAttachment/?VendorCaseId=" + 0 + "&Module=" + srv.Module + "&fileIconType=" + description.fileIconType + "&fileSize=" + description.fileSize + "&fileName=" + description.fileName + "&color=" + description.color, formData,
                //{
                $http.post("api/File/" + srv.CaseId + "/UploadAttachmentVendorGovernance/?flag=" + srv.Showtimezone, formData,
                    {
                        withCredentials: true,
                        headers: { 'Content-Type': undefined },
                        transformRequest: angular.identity
                    })
                    .then(function (data) {
                        angular.element("input[type='file']").val(null);
                        if (data.data == "Maximum request length exceeded.") {
                            alert("Maximum file size exceeded")
                        }
                        else if (data.data == "Error Occurred") {

                        }
                        else if (data.data == "File Not found") {

                        }
                        else {
                            if (flag) {
                                srv.files = [];
                                srv.filesDescription = [];
                                srv.AttachmentList = [];
                            }

                            for (var r = 0; r < data.data.length; r++) {
                                srv.AttachmentList.push(data.data[r]);
                            }
                            srv.bindToList();
                        }
                    });
            }

            srv.removeCaseAttachmentsDetail = function (Id) {
                if (srv.CaseId == 0) {
                    //srv.AttachmentList.splice(index, 1);
                    for (var i = 0; i < srv.AttachmentList.length; i++) {
                        if (srv.AttachmentList[i].Id == Id) {
                            srv.AttachmentList.splice(i, 1);
                            var files = Array.prototype.slice.call(srv.files);
                            files.splice(i, 1);
                            srv.files = files;
                        }
                    }
                    srv.bindToList();
                    return;
                }
                $http.post('/api/Case/' + Id + '/removeCaseAttachmentsDetail').then(function (data) {
                    for (var i = 0; i < srv.AttachmentList.length; i++) {
                        if (srv.AttachmentList[i].Id == Id) {
                            srv.AttachmentList.splice(i, 1);
                        }
                    }                    
                    srv.bindToList();
                }, function (error) {

                });
            }

            srv.CheckCreatedBy = function (dataItem) {
                if (FranchiseService.UserData.PersonId == dataItem.CreatedBy || window.location.href.toUpperCase().includes('CHANGEREQUESTCREATE')) {
                    return false;
                } else {
                    return true;
                }
            }

            srv.removeCaseAttachmentsDetails = function (Id, CaseId) {
                $http.post('/api/Case/' + Id + '/removeCaseAttachmentsDetail?CaseId=' + CaseId).then(function (data) {
                    //srv.getlineIdList();
                    srv.getCaseDocList();
                }, function (error) {

                });
            }
        });

})(window, document);
