﻿'use strict';
app.service('NewtaskService', [
    '$http',  'CalendarService',
    function ($http,  CalendarService) {
        var srv = this;
        srv.CalendarService = CalendarService;
        srv.IsBusy = false;
        srv.AddOrEdit = function (modalId, index) {
            $("#" + modalId).modal("show");
        };
    }
]);


