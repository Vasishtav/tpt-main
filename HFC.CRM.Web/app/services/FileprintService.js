﻿'use strict';

app.service('FileprintService', [
    '$http', 'CacheService', 'PrintService',
    function ($http, CacheService, PrintService) {

        var srv = this;

        srv.PrintService = PrintService;
        srv.IsBusy = false;
        srv.SelectedSources = [];
        srv.Sources = [];
        srv.Accounts = [];
        srv.EditorSelection = [];
        srv.LeadId = false;

        srv.AddOrEdit = function (modalId, index, page) {
            if (modalId = 'fileprint') {
                srv.PrintService.PrintOptions.AddleadCustomer = false;
                srv.PrintService.PrintOptions.AddGoogleMap = false;
                srv.PrintService.PrintOptions.AddMeasurementForm = false;
                srv.PrintService.PrintOptions.AddGarageMeasurementForm = false;
                srv.PrintService.PrintOptions.AddClosetMeasurementForm = false;
                srv.PrintService.PrintOptions.AddOfficeMeasurementForm = false;
                srv.PrintService.PrintOptions.AddFloorMeasurementForm = false;
                srv.PrintService.PrintOptions.AddFCCMeasurementForm = false;
                srv.PrintService.PrintOptions.AddPrintExistingMeasurements = false;
                srv.PrintService.PrintOptions.AddPrintInternalNotes = false;
                srv.PrintService.PrintOptions.AddPrintExternalNotes = false;
                srv.PrintService.PrintOptions.AddPrintDirectionNotes = false;
                srv.PrintService.PrintOptions.AddPrintQuote = false;

                // by default the all are 1 qty.
                srv.PrintService.PrintOptions.GarageMeasurementFormQuntity = 1;
                srv.PrintService.PrintOptions.ClosetMeasurementFormQuntity = 1;
                srv.PrintService.PrintOptions.OfficeMeasurementFormQuntity = 1;
                srv.PrintService.PrintOptions.FloorMeasurementFormQuntity = 1;
                srv.PrintService.PrintOptions.ConcreteCraftMeasurementFormQuntity = 1;
            }

            if (index != undefined) {
                if (page != null && page != undefined) {
                    if (page == 'AccountSearch') {
                        srv.PrintService.accountId = index;
                        srv.PrintService.leadId = null;
                        srv.PrintService.opportunityId = null;
                        srv.PrintService.measurementId = null;
                    }
                    if (page == 'LeadSearch') {
                        srv.PrintService.leadId = index;
                        srv.PrintService.accountId = null;
                        srv.PrintService.opportunityId = null;
                        srv.PrintService.measurementId = null;
                    }
                    if (page == 'OpportunitySearch') {
                        srv.PrintService.opportunityId = index;
                        srv.PrintService.leadId = null;
                        srv.PrintService.accountId = null;
                        srv.PrintService.measurementId = null;
                    }
                    if (page == 'AccountDetails') {
                        srv.PrintService.accountId = index;
                        srv.PrintService.leadId = null;
                        srv.PrintService.opportunityId = null;
                        srv.PrintService.measurementId = null;
                    }
                    if (page == 'LeadDetails') {
                        srv.PrintService.leadId = index;
                        srv.PrintService.accountId = null;
                        srv.PrintService.opportunityId = null;
                        srv.PrintService.measurementId = null;
                    }
                    if (page == 'OpportunityDetails') {
                        srv.PrintService.opportunityId = index;
                        srv.PrintService.leadId = null;
                        srv.PrintService.accountId = null;
                        srv.PrintService.measurementId = null;
                    }
                    if (page == 'MeasurementDetails') {
                        srv.PrintService.opportunityId = index;
                        srv.PrintService.measurementId = index;
                        srv.PrintService.leadId = null;
                        srv.PrintService.accountId = null;
                    }
                }
            }

            $("#" + modalId).modal("show");
        };
        srv.CancelPrintEditor = function (modalId) {
            $("#" + modalId).modal("hide");

        };

        srv.GetAccounts = function (jobCount) {

            var result = [];

            if (srv.Accounts.length > 1) {
                result.push({ SourceId: 9999 });
            }

            if (jobCount > 1) {
                var RepeatID = 0;
                RepeatID = (HFC.Util.BrandedPath != "/brand/tl") ? 22 : 21; /// The Repeat SourceID on TL is 21, on BB is 22;
                if (srv.Accounts.map(function (e) { return e.SourceId; }).indexOf(RepeatID) == -1) {
                    result.push({ SourceId: RepeatID });
                }
            }

            for (var i = 0; i < srv.Accounts.length; i++) {
                result.push(srv.Accounts[i]);
            }

            return result;
        };

        srv.GetSource = function (sourceId, sources, path) {
            if (!sources || sources.length == 0) {
                return false;
            }

            for (var i = 0; i < sources.length; i++) {
                if (sources[i].SourceId == sourceId) {
                    sources[i].path = path;
                    return sources[i];
                } else {

                    if (sources[i].Children && sources[i].Children.length > 0) {
                        var result = srv.GetSource(sourceId, sources[i].Children, path + sources[i].Name + " > ");
                        if (result) {
                            return result;
                        }
                    }
                }
            }

            return false;
        };

        srv.GetLabel = function (srcId, excludeMargin) {
            var path = "";
            var src = srv.GetSource(srcId, srv.Sources, path);
            return src;
        };

        srv.AddSource = function (leadId) {
            srv.Accounts.push({
                LeadId: leadId,
                SourceId: null,
                IsManuallyAdded: true
            });
        };

        srv.Delete = function (src) {
            if (!srv.LeadId) {
                remove(src);
                return;
            };

            if (srv.IsBusy)
                return false;
            var exists = srv.CheckIfSourceUsed(src.SourceId);
            if (exists) {
                HFC.DisplayAlert("Source selected to remove is already used in a job for this lead");
                return false;
            }

            if (src.AccountId) {
                srv.IsBusy = true;
                //validate if used
                $http.delete('/api/leads/' + src.AccountId + '/source/').then(function (response) {
                    remove(src);
                    srv.IsBusy = false;
                }, function (response) {
                    HFC.DisplayAlert(response.statusText);
                    srv.IsBusy = false;
                });
            } else
                remove(src);
        };

        srv.Update = function (sources) {
            if (!srv.LeadId) return;

            if (srv.IsBusy)
                return false;

            srv.IsBusy = true;

            $http.post('/api/leads/' + srv.LeadId + '/source/', sources).then(function (response) {
                HFC.DisplaySuccess("Lead Source(s) saved");
                srv.IsBusy = false;
            }, function (response) {
                HFC.DisplayAlert(response.statusText);
                srv.IsBusy = false;
            });
        }

        srv.SelectSource = function (node) {
            if (node.Selected) { // select
                srv.EditorSelection.push(node.SourceId);
            } else {

                for (var i = 0; i < srv.EditorSelection.length; i++) {
                    if (srv.EditorSelection[i] == node.SourceId) {
                        srv.EditorSelection.splice(i, 1);
                    }
                }
            }

            console.log(srv.EditorSelection);
        }

        srv.GetEditorData = function (data) {
            var filter = function (nodes) {
                var result = [];
                if (!nodes || nodes.length == 0) return [];
                for (var i = 0; i < nodes.length; i++) {
                    if (!nodes[i].isDeleted) {
                        var n = nodes[i];
                        if (n.Children) {
                            n.Children = filter(n.Children);
                        }

                        result.push(n);
                    }
                }

                return result;
            }

            return filter(data);
        }

        srv.Get = function (leadId) {
            srv.LeadId = leadId;
            srv.IsBusy = true;
            $http.get('/api/leads/' + leadId + '/source/').then(function (response) {
                
                srv.Sources = [];
                srv.EditorData = false;
                CacheService.Get('Source', function (items) {
                    srv.Sources = items;
                });

                srv.Accounts = response.data.Accounts;
                srv.GetSelectedSources(leadId);

            }, function (response) {
                HFC.DisplayAlert(response.statusText);
            });

            srv.IsBusy = false;
        }
        srv.GetSelectedSources = function (leadId) {
            //Selected Sources 
            srv.IsBusy = true;
            $http.get('/api/leads/' + leadId + '/SelectedSources/').then(function (response) {
                srv.SelectedSources = [];
                srv.EditorData = false;
                CacheService.Get('SelectedSources', function (items) {
                    srv.SelectedSources = items;
                });

                srv.SelectedSources = response.data.SelectedSources;
            }, function (response) {
                HFC.DisplayAlert(response.statusText);
            });
            srv.IsBusy = false;
        }
        srv.CheckIfSourceUsed = function (sourceId) {

            if (srv.SelectedSources.length > 0) {
                var found = false;
                for (var i = 0; i <= srv.SelectedSources.length - 1; i++) {
                    if (srv.SelectedSources[i] == sourceId) {
                        found = true;
                    }
                }
                return found;
            }
            else {
                return false;
            }
        }

        srv.ShowEditor = function (modalId, index, source, $event, AccountService) {
            if ($event) $event.preventDefault();
            //$http.get('/api/leads/' + srv.LeadId + '/source/').then(function (response) {

            //srv.Sources = null;


            //load selected sources while the dialog to select a new source
            // AccountService.GetSelectedSources(srv.LeadId);
            console.log("hello Account Service");

            CacheService.Invalidate('source');
            if (source) {
                //sourceId = sourceId.target.form[23].selectedIndex;
                var selectedsourceId = source.$parent.AccountsList.Id.SourceId;
                $http.get('/api/lookup/' + selectedsourceId + '/Type_Source').then(function (data) {
                    srv.Sources = data.data;
                });


                srv.EditorSelection = [];

                for (var i = 0; i < srv.Accounts.length; i++) {
                    srv.EditorSelection.push(srv.Accounts[i].SourceId);
                }

                // if (!srv.EditorData) {
                srv.EditorData = srv.GetEditorData(srv.Sources);
                //}

                srv.EditorDataExpandedNodes = [];

                // restoreSelection(srv.EditorData);

                $("#" + modalId).modal("show");
            }
            else {
                CacheService.Get('Source', function (items) {
                    
                    srv.Sources = items;

                    srv.EditorSelection = [];

                    for (var i = 0; i < srv.Accounts.length; i++) {
                        srv.EditorSelection.push(srv.Accounts[i].SourceId);
                    }

                    // if (!srv.EditorData) {
                    srv.EditorData = srv.GetEditorData(srv.Sources);
                    //}

                    srv.EditorDataExpandedNodes = [];

                    restoreSelection(srv.EditorData);

                    $("#" + modalId).modal("show");

                });
            }
            //}, function (response) {
            //    HFC.DisplayAlert(response.statusText);

            //});

        };

        srv.CancelAccountEditor = function (modalId) {
            $("#" + modalId).modal("hide");
        }

        var restoreSelection = function (nodes) {
            if (!nodes || nodes.length == 0) {
                return;
            }
            for (var i = 0; i < nodes.length; i++) {
                restoreSelection(nodes[i].Children);
                if (srv.EditorSelection.indexOf(nodes[i].SourceId) > -1) {
                    nodes[i].Selected = true;
                    expandPath(nodes[i]);
                } else {
                    nodes[i].Selected = false;
                }
            }
        };

        var expandPath = function (node) {
            srv.EditorDataExpandedNodes.push(node);

            if (node.ParentId) {
                var parent = srv.GetSource(node.ParentId, srv.Sources);
                expandPath(parent);
            }
        }

        var resetSelection = function (node) {
            node.Selected = false;
            if (!node.Children) {
                return;
            }

            for (var i = 0; i < node.Children.length; i++) {
                resetSelection(node.Children[i]);
            }
        }

        srv.SaveEditor = function (modalId) {


            for (var i = 0; i < srv.EditorData.length; i++) {
                resetSelection(srv.EditorData[i]);
            };

            srv.Accounts = [];

            var selectedSources = [];

            for (var j = 0; j < srv.EditorSelection.length; j++) {
                selectedSources.push(srv.GetSource(srv.EditorSelection[j], srv.Sources));
            }

            var sourcesToUnselect = [];

            for (var j = 0; j < selectedSources.length; j++) {
                if (selectedSources[j].ParentId > 0) {
                    var selectedParent = $.grep(selectedSources, function (em) {
                        return em.SourceId == selectedSources[j].ParentId;
                    });

                    if (selectedParent && selectedParent.length > 0) {
                        sourcesToUnselect.push(selectedParent[0].SourceId);
                    }
                }
            }

            if (sourcesToUnselect.length > 0) {
                for (var k = 0; k < sourcesToUnselect.length; k++) {
                    var index = srv.EditorSelection.indexOf(sourcesToUnselect[k]);
                    if (index >= 0) srv.EditorSelection.splice(index, 1);
                }
            }

            srv.Update(srv.EditorSelection);

            for (var j = 0; j < srv.EditorSelection.length; j++) {
                srv.Accounts.push({ LeadId: srv.LeadId, SourceId: srv.EditorSelection[j] });
            }

            $("#" + modalId).modal("hide");
        }

        function remove(item) {
            var index = srv.Accounts.indexOf(item);
            if (index > 0) {
                srv.Accounts.splice(index, 1);
            } else if (index === 0) {
                srv.Accounts.shift();
            }
        }

    }
]);


