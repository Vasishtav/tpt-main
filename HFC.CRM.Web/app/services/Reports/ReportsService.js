﻿// Reference LeadSearchService.js

'use strict';
app.service('ReportsService', [
            '$http', 'HFCService', function ($http, HFCService) {
                var srv = this;

                srv.IsBusy = false;
                srv.Pagination = { page: 1, size: 50, pageTotal: 1 };
                srv.PageTotal = srv.Pagination.pageTotal;
                srv.PageSize = srv.Pagination.size;
                srv.PageNumber = srv.Pagination.page;
                srv.TotalRecords = 0;
                srv.ChangePageSize = function () {
                }
                srv.PageSummary = '';


                //srv.TotalRecords = 0;
                srv.SearchTerm = "";
                srv.SearchFilter = "";
                srv.LeadStatusIds = [];
                srv.DateRangeText = "";
                srv.StartDate = null;
                srv.EndDate = null;
                //srv.predicate = '-createdonutc';
                srv.DateRangeArray = ["Last 7 Days", "Last 30 Days", "Last Week", "Last Month", "Last Year", "This Week", "This Month", "This Year", "1st Quarter", "2nd Quarter", "3rd Quarter", "4th Quarter", "1st Half", "2nd Half"];
                srv.SearchEnums = {};
                srv.Leads = [];


                srv.getSalesAgent = function (data) {

                    srv.SalesAgent_options = {

                        dataSource: {
                            transport: {
                                read: {
                                    url: '/api/Reports/0/getSalesAgent',
                                    dataType: "json",
                                }
                            },
                            error: function (e) {
                                HFC.DisplayAlert(e.errorThrown);
                            }
                        },
                        dataBound: function (e) {
                            if (this.dataSource.view().length == 0) {
                                $('.k-pager-nav').hide();
                                $('.k-pager-numbers').hide();
                                $('.k-pager-sizes').hide();
                            } else {
                                $('.k-pager-nav').show();
                                $('.k-pager-numbers').show();
                                $('.k-pager-sizes').show();
                            }
                        },
                        resizable: true,
                        columns: [
                                   {
                                       field: "SalesAgent",
                                       title: "Sales Agent",
                                       filterable: { multi: true, search: true },
                                   },
                                   {
                                       field: "opportunities",
                                       title: "# Opportunities",
                                       filterable: { multi: true, search: true },
                                   },
                                   {
                                       field: "Quote",
                                       title: "# Quotes",
                                       filterable: { multi: true, search: true },
                                   },
                                   {
                                       field: "OpenQuote",
                                       title: "# Open Quote",
                                       filterable: { multi: true, search: true },
                                   },
                                   {

                                       field: "Lostsales",
                                       title: "# Lost Sales",
                                       filterable: { multi: true, search: true }
                                   },
                                   {

                                       field: "Orders",
                                       title: "# Orders",
                                       filterable: { multi: true, search: true }

                                   },
                                   {

                                       field: "ClosingRate",
                                       title: "Closing Rate",
                                       filterable: { multi: true, search: true }
                                   },
                                   {
                                       field: "QuotetoClose",
                                       title: "Quote to Close %",
                                       filterable: { multi: true, search: true }
                                   },
                                   {
                                       field: "TotalOrders",
                                       title: "Total Orders",
                                       filterable: { multi: true, search: true }
                                   },
                                   {
                                       field: "AvgOrder",
                                       title: "Avg Order",
                                       filterable: { multi: true, search: true }
                                   },
                        ],
                        noRecords: { template: "No records found" },
                        filterable: {
                            extra: true,
                            operators: {
                                string: {
                                    contains: "Contains",
                                    eq: "Is equal to",
                                    neq: "Is not equal to"
                                },
                                date: {
                                    gt: "After (Excludes)",
                                    lt: "Before (Includes)"
                                },
                            }
                        },
                        filterMenuInit: function (e) {
                            $(e.container).find('.k-check-all').click()
                        },
                        pageable: {
                            refresh: true,
                            pageSize: 25,
                            pageSizes: [25, 50, 100, 'All'],
                            buttonCount: 5
                        },
                    };
                };

                srv.SelectDateRange = function (dateRange) {
                    srv.DateRangeText = dateRange;
                    var dates = HFCService.GetDateRange(srv.DateRangeText);
                    return dates;
                }

                //to get active vendor to drop down
                srv.Lookupvendor = function () {
                    return $http.get('/api/lookup/0/VendorNameList');
                };

                //Get the Industries
                srv.LookupIndustries = function () {
                    return $http.get('/api/lookup/1/Type_LookUpValues');
                };

                //Get the Commercial Type
                srv.LookupCommercialType = function () {
                    return $http.get('/api/lookup/2/Type_LookUpValues');
                };

                //Get FE salesPerson based on roles
                srv.LookupGetFESalesPerson = function () {
                    return $http.get('/api/lookup/0/GetFESalesPerson');
                };

                // Get Sales Summary report record
                srv.SalesSummaryByMonth = function (data) {
                    return $http.post('api/Reports/0/getSalesSummaryByMonth', data);
                };

                //Get the FE Source Lookup
                srv.LookupFESource = function () {
                    return $http.get('/api/lookup/0/GetFESource');
                };

                //Get the FE Campaign Lookup
                srv.LookupFECampaign = function () {
                    return $http.get('/api/lookup/0/GetFECampaign');
                };

                //Get the FE Zip Lookup
                srv.LookupFEZip = function () {
                    return $http.get('/api/lookup/0/GetFEZip');
                };

                //Get the FE Territory Lookup
                srv.LookupFETerritory = function () {
                    return $http.get('/api/lookup/0/GetFETerritory');
                };

            }
])








