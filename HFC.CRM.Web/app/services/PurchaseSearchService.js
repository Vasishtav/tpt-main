﻿'use strict';
app.service('PurchaseSearchService', [
            '$http', 'HFCService', function ($http, HFCService) {
                var srv = this;

               

                srv.IsBusy = false;
                srv.Pagination = { page: 1, size: 50, pageTotal: 1 };
                srv.PageTotal = srv.Pagination.pageTotal;
                srv.PageSize = srv.Pagination.size;
                srv.PageNumber = srv.Pagination.page;
                srv.TotalRecords = 0;
                srv.ChangePageSize = function () {

                }


                srv.PageSummary = '';




                //srv.TotalRecords = 0;
                srv.SearchTerm = "";
                srv.SearchFilter = "";
                srv.AccountStatusIds = [];
                srv.DateRangeText = "";
                srv.StartDate = null;
                srv.EndDate = null;
                //srv.predicate = '-createdonutc';
                srv.DateRangeArray = ["All Date & Time", "Last 7 Days", "Last 30 Days", "This Week", "This Month", "This Year", "Last Week", "Last Month", "Last Year"];
                srv.SearchEnums = {};
                srv.Purchase = [];


                srv.Get = function (data) {
                    var filters = {
                        pageIndex: srv.Pagination.page,
                        pageSize: srv.Pagination.size
                    };
                    srv.Purchase = [];
                    //$http.get('/api/Vendor/0/GetVendors').then(
                        //function (result) {
                            srv.Purchase_options = {
                                cache: false,
                                dataSource: {
                                    transport: {
                                        read: {
                                            url: '/api/Vendor/0/GetVendors',
                                            dataType: "json",
                                            data: filters
                                        }

                                    },
                                    error: function (e) {

                                        HFC.DisplayAlert(e.errorThrown);
                                    }
                                },
                                // https://www.telerik.com/forums/remove-page-if-no-records
                                dataBound: function (e) {
                                    //console.log("dataBound");
                                    if (this.dataSource.view().length == 0) {
                                        // The Grid contains No recrods so hide the footer.
                                        $('.k-pager-nav').hide();
                                        $('.k-pager-numbers').hide();
                                        $('.k-pager-sizes').hide();
                                    } else {
                                        // The Grid contains recrods so show the footer.
                                        $('.k-pager-nav').show();
                                        $('.k-pager-numbers').show();
                                        $('.k-pager-sizes').show();
                                    }
                                },
                                noRecords: { template: "No records found" },
                                columns: [
                                         {
                                             field: "VendorId",
                                             title: "Submitted Date",
                                             filterable: { multi: true, search: true }
                                         },
                                    {
                                        field: "Name",
                                        title: "Master PO",
                                        template: "<a href='\\\\#!/purchasemasterview/#= VendorId #' style='color: rgb(61,125,139);'><span style='padding: 10px;'>#= Name #</span></a>"
                                    },
                                    {
                                        field: "VendorType",
                                        title: "Account Name",
                                        //template: "\#= VendorType ? 'Alliance Vendors' : 'Non-Alliance Vendors'\#"
                                        //template: "# switch (VendorType) { case 1: #Alliance Vendor# break; ## case 0: #Non-Vendor Alliance# break; ## case 3: #My Vendor# break; ##}#"
                                    },


                                        {
                                            field: "Location",
                                            title: "Opportunity",
                                            filterable: { multi: true, search: true },
                                            //Width:"500px"

                                        },

                                         {
                                             field: "AccountRep",
                                             title: "Sales Order",
                                             filterable: { multi: true, search: true }
                                         },
                                         //{
                                         //    field: "AccountRepPhone",
                                         //    title: "Rep Phone",
                                         //    filterable: { multi: true, search: true }
                                         //},

                                         {
                                             field: "VendorStatus",
                                             title: "Status",
                                             //template: "\#= IsActive ? 'Active' : 'Inactive' \#"

                                         },
                                         {
                                             field: "",
                                             title: "",
                                             template: '<ul> <li class="dropdown note1 ad_user"><div class="btn-group"><button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> <ul class="dropdown-menu pull-right"> <li><a href="\\\\#!/leadsEdit/#= VendorId #">Edit</a> </li> <li><a href="javascript:void(0)" ng-click="Fileprint(\'PrintPopup\', #= VendorId #)">Print</a></li> </ul> </li> </ul>'
                                         },

                                ],
                                filterable: true,
                                resizable: true,
                                pageable: {
                                    refresh: true,
                                    pageSize: 25,
                                    pageSizes: [25, 50, 100],
                                    buttonCount: 5
                                },
                                sortable: ({ field: "CreatedOnUtc", dir: "desc" }),

                                //toolbar: kendo.template($(' <script id="template" type="text/x-kendo-template"><input ng-keyup="AccountgridSearch()" type="search" id="searchBox" placeholder="Search any column" class="k-textbox form-control frm_controlAccount" style="width: 250px !important;margin-left: 0px !important;"><input type="button" id="btnReset" ng-click="VendorgridSearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></script>').html()),
                                toolbar: kendo.template($(' <script id="template" type="text/x-kendo-template"><div class="vendor_search"><input ng-keyup="PurchasegridSearch()" type="search" id="searchBox" placeholder="Search Purchase" class="k-textbox frm_controllead" style="width: 250px !important;margin-left: 0px !important;"><input type="button" id="btnReset" ng-click="PurchasegridSearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div></script>').html()),
                            };
                        //});
                };


                srv.SelectDateRange = function (dateRange) {
                    srv.DateRangeText = dateRange;
                    var dates = HFCService.GetDateRange(srv.DateRangeText);
                    srv.StartDate = dates.StartDate;
                    srv.EndDate = dates.EndDate;
                }

                this.orderBy = function () {
                    return this.predicate.substr(1);
                };

                this.orderByDesc = function () {
                    return this.predicate.substr(0, 1) == '-';
                };
            }
])



