﻿    'use strict';

    app.service('PaymentService', [
        '$http', '$rootScope', function($http, $rootScope) {
            var srv = this;

            srv.IsBusy = null;

            srv.payment = null;
            srv.paymentIndex = -1;

            srv.Payments = [];

            srv.Get = function(invoiceId) {
                $http.get('/api/payments/' + invoiceId).then(function(response) {
                    angular.forEach(response.data, function(item) {
                        item.CreatedOn = new XDate(item.CreatedOn, true);
                        var xdate = new XDate(item.PaymentDate, true);
                        item.PaymentDateRaw = new XDate(xdate.getUTCFullYear(), xdate.getUTCMonth(), xdate.getUTCDate())[0];
                        item.PaymentDate = moment(item.PaymentDate).utc().format('MMMM Do YYYY');
                    });
                    srv.Payments = response.data || [];
                }, function(response) {
                    HFC.DisplayAlert(response.statusText);
                });
            };

            srv.EditPayment = function(payment, index) {
                srv.payment = angular.copy(payment);
                srv.payment.PaymentDate = srv.payment.PaymentDateRaw;
                srv.paymentIndex = index;
                $("#paymentEditModal").modal("show");
            };

            srv.GetInvoiceTotalPayments = function(invoice) {
                var result = 0;
                if (invoice && invoice.Payments) {
                    for (var i = 0; i < invoice.Payments.length; i++) {
                        result = result + invoice.Payments[i].Amount;
                    }
                }

                return result;
            }

            srv.AddPayment = function(invoiceId, jobId, quoteId) {
                srv.JobId = jobId;
                srv.QuoteId = quoteId;

                srv.payment = {
                    PaymentId: 0,
                    InvoiceId: invoiceId,
                    PaymentMethod: 'Cash',
                    PaymentDate: new Date(),
                    JobId: jobId,
                };
                srv.paymentIndex = -1;
                $("#paymentEditModal").modal("show");
            };

            //saves payment, but requires Invoice to update invoice status
            srv.SavePayment = function(Invoice) {
                if (srv.IsBusy)
                    return false;

                srv.IsBusy = true;
                var promise = null;
                srv.payment.PaymentDate = moment(srv.payment.PaymentDate).format('YYYY-MM-DD');

                if (srv.payment.PaymentId) {
                    promise = $http.put('/api/payments/' + srv.payment.PaymentId + '/', srv.payment);
                } else {
                    promise = $http.post('/api/payments?jobId=' + srv.JobId + '&quoteId=' + srv.QuoteId, srv.payment);
                }

                promise.then(function(response) {

                    if (!srv.payment.PaymentId && srv.payment.PaymentId != response.data.PaymentId) {
                        srv.payment.PaymentId = response.data.PaymentId;

                        srv.payment.CreatedOn = new XDate(response.data.CreatedOn, true);


                        srv.Payments.push(srv.payment);
                    } else if (srv.paymentIndex >= 0)
                        srv.Payments[srv.paymentIndex] = srv.payment;

                    var xdate = new XDate(response.data.PaymentDate, true);
                    srv.payment.PaymentDateRaw = new XDate(xdate.getUTCFullYear(), xdate.getUTCMonth(), xdate.getUTCDate())[0];
                    srv.payment.PaymentDate = moment(response.data.PaymentDate).utc().format('MMMM Do YYYY');


                    if (Invoice) {
                        Invoice.LastUpdated = response.data.LastUpdated;
                        Invoice.StatusEnum = response.data.StatusEnum;
                        Invoice.PaidInFullOn = response.data.PaidInFullOn ? new XDate(response.data.PaidInFullOn, true) : null;
                    }
                    srv.payment = null;
                    srv.paymentIndex = -1;

                    $("#paymentEditModal").modal("hide");
                    HFC.DisplaySuccess("Payment saved");
                    srv.IsBusy = null;

                    $rootScope.$broadcast('invoiceUpdated', { JobId: srv.JobId });

                }, function(response) {
                    HFC.DisplayAlert(response.statusText);
                    srv.IsBusy = null;
                });
            };

            srv.ResetPayment = function() {
                srv.payment = null;
                srv.paymentIndex = -1;
            };

            srv.DeletePayment = function(payment, Invoice) {
                if (!srv.IsBusy && confirm("Do you want to continue?")) {
                    srv.IsBusy = true;
                    $http.delete('/api/payments/' + payment.PaymentId).then(function(response) {
                        if (Invoice) {
                            Invoice.LastUpdated = response.data.LastUpdated;
                            Invoice.StatusEnum = response.data.StatusEnum;
                            Invoice.PaidInFullOn = response.data.PaidInFullOn ? new XDate(response.data.PaidInFullOn, true) : null;
                        }
                        removePayment(payment);
                        HFC.DisplaySuccess("Payment deleted");
                        srv.IsBusy = null;
                    }, function(response) {
                        HFC.DisplayAlert(response.statusText);
                        srv.IsBusy = null;
                    });
                }
            };

            function removePayment(payment) {
                var index = srv.Payments.indexOf(payment);
                if (index > 0) {
                    srv.Payments.splice(index, 1);
                } else if (index === 0) {
                    srv.Payments.shift();
                }
            }

        }
    ]);