﻿/***************************************************************************\
Module Name:  EmailService.js - AngularJS file
Project: Tochpoint
Created on: 07 November 2017
Created By:
Copyright:
Description: Sending Email - AngularJS
Change History:
Date  By  Description

\***************************************************************************/
'use strict';
app.service('EmailService', ['$http', 'HFCService', 'LeadService', 'PersonService',
    function ($http, HFCService, LeadService, PersonService) {
        var srv = this;

        srv.IsBusy = false;
        srv.SendUrl = "";
        srv.ToAddresses = [];
        srv.BccAddresses = [];
        srv.Subject = "";
        srv.Body = "";
        srv.LeadService = LeadService;
        srv.PersonService = PersonService;
        srv.AttachmentLink = "";
        srv.AttachmentText = "";
        srv.PersonPrimaryEmail = "";

        srv.SendThanksMail = function () {
            if (!srv.IsBusy) {
                srv.IsBusy = true;
                var LeadsId = srv.LeadService.Leads[0].Lead.LeadId;
                srv.SendUrl = "/email/SendEmail/" + LeadsId;
                var data = {
                    EmailType : 1,
                    fromAddresses: null,
                    toAddresses: null,
                    bccAddresses: null,
                    subject: null,
                    body: null,
                    attachment: null,
                };
                $http.post(srv.SendUrl, data).then(function (res) {
                    HFC.DisplaySuccess("Email successfully sent");
                    srv.IsBusy = false;
                }, function (res) {
                    srv.IsBusy = false;
                    HFC.DisplayAlert(res.statusText);
                });
            }
        }

        srv.GetPersonPrimaryEmail = function (personId) {
            $http.get('/api/users/' + personId + "/getPerson/").then(function (response) {
                var person = response.data.Person;
                if (person) {
                    srv.PersonPrimaryEmail = person.PrimaryEmail;
                } else {
                    if (HFCService.CurrentUser) {
                        srv.PersonPrimaryEmail = HFCService.CurrentUser.Email;
                    }
                }
            });
        }

        srv.Send = function () {
            if (!srv.IsBusy && srv.SendUrl) {
                srv.IsBusy = true;
                var data = {
                    toAddresses: srv.ToAddresses,
                    bccAddresses: srv.BccAddresses,
                    subject: HFC.htmlEncode(srv.Subject),
                    body: HFC.htmlEncode(srv.Body),
                    attachment: srv.AttachmentLink
                };
                $http.post(srv.SendUrl, data).then(function (res) {
                    HFC.DisplaySuccess("Email successfully sent");
                    srv.IsBusy = false;
                    $("#emailModal").modal("hide");
                }, function (res) {
                    srv.IsBusy = false;
                    HFC.DisplayAlert(res.statusText);
                });
            }
        }


        //Previous code
        srv.EmailInvoice = function (invoice, customer) {

            if (customer) {
                if (HFCService.CurrentUser) {
                    srv.GetPersonPrimaryEmail(HFCService.CurrentUser.PersonId);
                }

                srv.SendUrl = "/email/invoice/" + invoice.InvoiceId;
                var job = invoice.Job;
                if (job) {
                    var leadId = job.LeadId;

                    srv.LeadService.Get(leadId, function (leadServiceData) {
                        var lead = leadServiceData.Lead;
                        if (lead) {
                            var person = srv.PersonService.GetPerson(lead.PersonId);
                            if (person) {
                                var email = person.PrimaryEmail
                                srv.PersonId = lead.PersonId;
                                if (email != '') {
                                    srv.ToAddresses = [];
                                    srv.ToAddresses.push(email);
                                }
                            }
                        }
                    });

                }
                //if (customer.PrimaryEmail) {
                //    srv.ToAddresses = [customer.PrimaryEmail];
                //}
                srv.BccAddresses = [];
                srv.Subject = "Invoice for " + customer.FullName;
                srv.AttachmentLink = "/export/invoice/" + invoice.InvoiceId;
                srv.AttachmentText = "Invoice " + invoice.InvoiceNumber + ".pdf";
                if (HFCService.CurrentUser) {
                    if (HFCService.CurrentUser.EmailSignature)
                        srv.Body = "<br/>" + HFCService.CurrentUser.EmailSignature;
                }
                $("#emailModal").modal("show");
            }
        }

        srv.EmailLeadSheet = function (job) {
            if (job.SalesPersonId > 0) {
                if (HFCService.CurrentUser) {
                    srv.GetPersonPrimaryEmail(HFCService.CurrentUser.PersonId);
                }

                srv.SendUrl = "/email/leadsheet/" + job.JobId;
                if (job) {
                    var leadId = job.LeadId;

                    srv.LeadService.Get(leadId, function (leadServiceData) {
                        var lead = leadServiceData.Lead;
                        if (lead) {
                            var person = srv.PersonService.GetPerson(lead.PersonId);
                            if (person) {
                                var email = person.PrimaryEmail
                                srv.PersonId = lead.PersonId;
                                if (email != '') {
                                    srv.ToAddresses = [];
                                    //srv.ToAddresses.push(email);
                                }
                            }
                        }
                    });

                }
                //if (job.SalesPerson.PrimaryEmail) {
                //    srv.ToAddresses = [job.SalesPerson.PrimaryEmail];
                //}
                srv.BccAddresses = [];
                srv.Subject = "Lead Sheet for Job Number #" + job.JobNumber;
                srv.AttachmentLink = "/export/LeadSheet/" + job.JobId;
                srv.AttachmentText = "LeadSheet for Job Number #" + job.JobNumber + ".pdf";
                $("#emailModal").modal("show");
            }
        }

        srv.EmailQuote = function (quote, job, customer) {
            if (customer) {
                if (job) {
                    var leadId = job.LeadId;

                    srv.LeadService.Get(leadId, function (leadServiceData) {
                        var lead = leadServiceData.Lead;
                        if (lead) {
                            var person = srv.PersonService.GetPerson(lead.PersonId);
                            if (person) {
                                var email = person.PrimaryEmail
                                srv.PersonId = lead.PersonId;
                                if (email != '') {
                                    srv.ToAddresses = [];
                                    srv.ToAddresses.push(email);
                                }
                            }
                        }
                    });

                }

                if (HFCService.CurrentUser) {
                    srv.GetPersonPrimaryEmail(HFCService.CurrentUser.PersonId);
                }

                srv.SendUrl = "/email/quote/" + quote.QuoteId;
                //if (customer.PrimaryEmail) {
                //    srv.ToAddresses = [customer.PrimaryEmail];
                //}
                srv.BccAddresses = [];
                srv.Subject = "Quote for " + customer.FullName;
                srv.AttachmentLink = "/export/quote/" + quote.QuoteId;
                srv.AttachmentText = "Quote " + job.JobNumber + "-" + quote.QuoteNumber + ".pdf";
                if (HFCService.CurrentUser) {
                    if (HFCService.CurrentUser.EmailSignature)
                        srv.Body = "<br/>" + HFCService.CurrentUser.EmailSignature;
                }
                $("#emailModal").modal("show");
            }
        }

    }
]);