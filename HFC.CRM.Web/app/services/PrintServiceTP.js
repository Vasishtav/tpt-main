﻿/***************************************************************************\
Module Name:  PrintService Service
Project: HFC-Tochpoint
Created on: 14 Dec 2017
Created By: 
Copyright:
Description: Print service api helper
Change History:
Date  By  Description

\***************************************************************************/

app.service('PrintServiceTP', [
    '$http', 'HFCService', 
    function ($http, HFCService) {

        var srv = this;

        srv.getMydocumentsForSales = function () {
            var url = '/api/print/0/GetMydocumentsForSale';
            return $http.get(url);
        }

        srv.getMydocumentsForInstall = function () {
            var url = '/api/print/0/GetMydocumentsForInstall';
            return $http.get(url);
        }
    }
]);
