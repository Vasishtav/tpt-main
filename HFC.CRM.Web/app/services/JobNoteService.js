﻿'use strict';
app.service('JobNoteService', ['$http', '$sce', 'HFCService', function ($http, $sce, HFCService) {
    var srv = this;

    srv.JobNotes = [];
    srv.NoteTypes = window.NoteTypeEnum; 

    srv.GetType = function (value) {
        for (var prop in srv.NoteTypes) {
            if (srv.NoteTypes[prop] === value)
                return prop;
        }
        return null;
    }

    srv.MessageInHtml = function (message) {
        return $sce.trustAsHtml(message);
    };

    srv.Get = function (jobIds, includeHistory) {

         if (jobIds && jobIds.length) {
             $http.get('/api/notes/' + jobIds[0]+ '/job/', { params: { includeHistory: includeHistory, jobIds: jobIds } }).then(function (response) {
                srv.JobNotes = response.data.JobNotes;
                srv.NoteTypes = response.data.NoteTypeEnum;

                angular.forEach(srv.JobNotes, function (note) {
                    note.CreatedOn = new XDate(note.CreatedOn, true);
                });
            }, function (response) {
                HFC.DisplayAlert("Job Notes service returned an error, " + response.statusText);
            });
        }
    };

    srv.Load = function(job) {

        $http.get('/api/notes/' + job.JobId + '/job/').then(function(response) {
            srv.JobNotes = response.data.JobNotes;
            srv.NoteTypes = response.data.NoteTypeEnum;

            angular.forEach(srv.JobNotes, function(note) {
                note.CreatedOn = new XDate(note.CreatedOn, true);
            });

        }, function(response) {
            HFC.DisplayAlert("Job Notes service returned an error, " + response.statusText);
        });

    };

    srv.Add = function (jobId, msg, noteType) {
        var trimmed_msg = $.trim(msg);
        if (trimmed_msg && jobId) {
            $http.post('/api/notes/'+jobId+'/job/', {
                TypeEnum: noteType,
                Message: HFC.htmlEncode(trimmed_msg)
            }).then(
                function (response) {
                    srv.JobNotes.splice(0, 0, { NoteId: response.data.NoteId || 0, JobId: jobId, AvatarSrc: HFCService.CurrentUser.AvatarSrc, TypeEnum: noteType, Message: msg, CreatedByPersonId: HFCService.CurrentUser.PersonId, CreatorFullName: HFCService.CurrentUser.FullName, CreatedOn: new XDate() });
                    $(".aside .note-msg-box").val("");
                },
                function (response) {
                    HFC.DisplayAlert(response.statusText);
                }
            );
        }
    };

    srv.ChangeType = function (note, noteType) {
        $http.put('/api/notes/' + note.NoteId + '/job', { TypeEnum: noteType }).then(function(response) {
            note.TypeEnum = noteType;
        }, function(response) {
            HFC.DisplayAlert(response.statusText);
        });
    };
}])