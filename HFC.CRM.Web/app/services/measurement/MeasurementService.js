﻿'use strict';
app.service('MeasurementService', [
     '$http', '$routeParams', 'HFCService', function ($http, $routeParams,HFCService) {
         var srv = this;
         srv.Sources = [];
         srv.id = $routeParams.OpportunityId;
         srv.GetMeasurements = [];
         //srv.GetSources = function () {
         //    return $http.get('/api/leadsources/0/getsourses');
         //}


         srv.GetMeasurements = function () {
             srv.Measurements = {
                 dataSource: {
                     transport: {
                         read: {
                             url: '/api/Measurement/'+srv.id+'/Get',
                                 dataType: "json"
                         },

                     },
                     error: function (e) {

                         HFC.DisplayAlert(e.errorThrown);
                     },

                     schema: {
                         model: {
                             id: "RoomId",
                             fields: {
                                 RoomId: { editable: false, nullable: true },
                                 RoomLocation: { validation: { required: true } },
                                 RoomName: { validation: { required: true, min: 1 } },
                                 WindowLocation: { validation: { required: true, min: 1 } },
                                 MountTypeName: { validation: { required: true } },
                                 Width: { type: "int" },

                                 Height: { type: "int" },
                                 Comments: {editable:true}
                                 


                             }
                         }
                     }
                 },
                 columns: [
                     {

                         field: "RoomLocation",
                         title: "Room Location",
                         //template: "<a class='test k-grid-edit' href=''><span>#= RoomLocation #</span></a>",
                         filterable: { multi: true, search: true },
                         hidden: false,
                         //editor: LocationDropDownEditor

                     },
                             {
                                 field: "RoomName",
                                 title: "Room Name",
                                 filterable: { multi: true, search: true }


                             },
                              {
                                  field: "WindowLocation",
                                  title: "Window Location",
                                  hidden: false,
                                  //editor: SourceDropDownEditor


                              },
                             {
                                 field: "MountTypeName",
                                 title: "Mount Type",
                                 hidden: false,
                                 //editor: MountTypeDropDownEditor


                             },
                               {
                                   field: "Width",
                                   title: "Width",
                                   // attributes:{ style: "display: none;" },
                                   hidden: true,

                                   template: ""

                               },
                               {
                                   field: "FranctionalValueWidth",
                                   title: "Franctional Value Width",
                                   //attributes: { style: "display: none;" },
                                   //hidden: true,
                                   template: "",
                                   editor: FractionDropDownEditor,

                                   hidden: true
                               },
                               {
                                   title: "Width",
                                   //attributes: { colspan: 2 },
                                   template: "#= Width # #= FranctionalValueWidth #"

                               },

                                {
                                    field: "Height",
                                    title: "Height",
                                    //attributes: { style: "display: none;" },
                                    hidden: true

                                },
                                {
                                    field: "FranctionalValueHeight",
                                    title: "Franctional Value Height",
                                    //attributes: { style: "display: none;" },
                                    editor: FractionDropDownEditor,
                                    hidden: true


                                },
                                {
                                    title: "Height",
                                    //field: "Height",
                                    //headerAttributes: { style: "display:none;" },
                                    //attributes: { colspan: 2 },
                                    template: "#= Height # #= FranctionalValueHeight #"

                                },
                                {
                                    field: "Comments",
                                    title: "Comments",
                                    hidden: false,
                                    //editor: AreaCodesEditor


                                },
                                
                                 
                 ],
                 editable: false,
                 filterable: true,
                 //pageable: {
                 //    refresh: true,
                 //    pageSize: 25,
                 //    pageSizes: [25, 50, 100, 'All'],
                 //    buttonCount: 5
                 //},
                 scrollable : true,
                 toolbar: [{ template: kendo.template($(' <script id="template" type="text/x-kendo-template"><input ng-keyup="MeasurementgridSearch()" type="search" id="searchBox" placeholder="Search any column" class="k-textbox form-control frm_controllead" style="width: 250px !important;margin-left: 0px !important;"><input type="button" id="btnReset" ng-click="CampaigngridSearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></script>').html()) }],

             };

         };


     }
]);