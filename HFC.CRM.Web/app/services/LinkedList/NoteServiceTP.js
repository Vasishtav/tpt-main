﻿(function (window, document, undefined) {
    'use strict';
    angular.module('hfc.Notemodule', ['hfc.Notemodule', ]).
        service('NoteServiceTP', ['$http', 'HFCService', 'CacheService', '$timeout', '$rootScope', 'kendotooltipService', 'kendoService', '$window',
    function ($http, HFCService, CacheService, $timeout, $rootScope, kendotooltipService, kendoService, $window) {
        var srv = this;

        srv.showAddNotes = false;
        srv.showEditNotes = false;
        srv.sysWideDocumentEdit = false;

        srv.Permission = {};
        var GlobalDoc = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'SystemDocuments')
        if (GlobalDoc != undefined) {
            srv.Permission.PermissionDeleteDoc = GlobalDoc.CanDelete;
            srv.Permission.PermissionEditDoc = GlobalDoc.CanUpdate;

        }



        // Unless otherwise specified the component assume that the parent is Lead.
        srv.ParentName = "Lead";
        srv.SourceParam = {};
        srv.IsBusy = false;
        srv.FranchiseSettings = false;
        srv.FranchiseSettingsAdd = false;
        srv.FranchiseSettingsEdit = false;
        srv.Id = 0;
        srv.Id1 = 0;
        srv.Document = "";
        srv.Brand = "";
        // On which mode the current note service operates. The default is Add mode.
        // When called from the grid row for edit, value will be reset to Edit.
        srv.Mode = "Add";
        srv.submitnte = false;
        // Contain list of notes in the currrent context, example if the current view
        // is Lead, then the list will contain notes to that specific Lead.
        srv.NoteList = [];

        // TODO: this can be come from the webapi, to avoid the mismatch of the category
        // id and name between client and the middle tier.
        //srv.CategoryList = [
        //    { id: 0, name: 'Internal' },
        //    { id: 1, name: 'External' }, // Earlier it was Customer.
        //    { id: 2, name: 'Direction' }
        //];

        // Represent the current note in the context. it may be new note or existing
        // note in edit mode
        srv.Note = {
            Index: '',
            Title: '',
            Description: '',
            //SelectedCategory: { id: 0 },
            NoteId: 0,
            LeadId: 0,
            AccountId: 0,
            OpportunityId: 0,
            OrderId: 0
        };

        // The following here ids should be populated from the consumer, for example LeadController
        // Note service required these information based on these only it can associate with either
        // Lead or Account or Opportunity.
        // TODO: The best place is actually the view page (of Lead) where the notes custom tag is used.
        //srv.LeadId, srv.AccountId, srv.OpportunityId

        srv.NoteIndex = -1;

        //global search
        srv.NoteId = 0;

        // this id is used to contain the controls for the notes popup modal.
        srv.divId = "_noteModal_";
        srv.Delete = "delete";

        // A referrance to the Dropzone object. So that we can call some of its methods like ProcesQueue.
        srv.DropzoneObject = null;
        srv.DropzoneOption = {
            'options': {
                //   passed into the Dropzone constructor
                // url: '/ControlPanelll/Leads/UploadFileTP/0', //+ srv.LeadId, //+ "/mydescription" , //The webapi to be called when a file uploaded...???
                url: 'api/File/0/UploadFileTP',
                paramName: "files", // The name that will be used to transfer the file
                maxFilesize: 10, // MB
                parallelUploads: 5,
                addRemoveLinks: true,
                autoProcessQueue: false,
                init: function () {
                    //
                    //this.on("processing", function (file) {
                    //    if (srv.Document == "SYS-DOC")
                    //        this.options.url = '/ControlPanel/Leads/UploadFileTP/0';
                    //    else
                    //        this.options.url = '/Leads/UploadFileTP/0';
                    //});
                    //
                    var submitButton = document.querySelector("#act-on-upload")
                    var myDropzonexyz = this;
                    srv.DropzoneObject = myDropzonexyz;

                    if (srv.Document == "SYS-DOC") // to keep only one file in the dropzone
                    {
                        this.on("addedfile", function () {
                            if (this.files[1] != null) {
                                this.removeFile(this.files[0]);
                            }
                            if (this.files[0].type != "application/pdf")
                                this.removeFile(this.files[0]);
                        });
                    }
                },
                drop: function (event, err) {
                    if (event.dataTransfer.files.length > 1) {
                        alert('One file at time');
                        event.stopPropagation();
                        event.preventDefault();
                        event.dataTransfer.files = [];
                    }
                },
                thumbnailWidth: 120,
                thumbnailHeight: 120,
                addedFiles: 0,
                dictRemoveFileConfirmation: "This will permanently remove the file, do you want to continue?",
                acceptedFiles: "image/*,application/pdf,.eps,.tiff,.doc,.docx,.html,.htm,.txt,.csv,.xls,.xlsx",
                //acceptedFiles: srv.allowedfiles,
                previewTemplate: '<div class="dz-preview dz-file-preview"><a target="_blank" class="dz-details" style="display:block"><div class="dz-filename"><span data-dz-name></span></div><div class="dz-size" data-dz-size></div><img data-dz-thumbnail /></a><div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div><div class="dz-success-mark"><span>✔</span></div><div class="dz-error-mark"><span>✘</span></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div>'
            },
            'eventHandlers': {
                'addedfile': function (file) {
                },

                'sending': function (file, xhr, formData) {
                    if (srv.Note.Title)
                        formData.append('title', srv.Note.Title);
                    else
                        formData.append('title', "title");

                    // The categories are multiple options, so should be send as comma separated strings
                    var selectedCategories = '';
                    if (srv.FranchiseDocuments) {
                        if (srv.Note.IsSales == true) {
                            selectedCategories = '10';
                        }
                        if (srv.Note.IsInstallation == true) {
                            selectedCategories += (selectedCategories != '') ? ',12' : '12';
                        }
                        if (srv.Note.IsProductGuide == true) {
                            selectedCategories += (selectedCategories != '') ? ',11' : '11';
                        }

                        //var categories = srv.Note.SelectedCategory.id;
                        // TODO: need to append the selected categories.
                        formData.append('categories', selectedCategories); //categories);
                    } else {
                        if (srv.Note != undefined && srv.Note != null && srv.Note.SelectedCategory != undefined && srv.Note.SelectedCategory != null) {
                            selectedCategories = '' + srv.Note.SelectedCategory.id + '';
                            formData.append('categories', selectedCategories);
                        }

                        //if (srv.Note.SelectedCategory)
                        //    formData.append('categories', srv.Note.SelectedCategory.id);
                        //else
                        //    formData.append('categories', 0);
                    }

                    if (srv.ParentName == "Lead" && srv.LeadId)
                        formData.append('leadId', srv.LeadId);

                    if (srv.ParentName == "Account" && srv.AccountId)
                        formData.append('accountId', srv.AccountId);

                    if (srv.ParentName == "Opportunity" && srv.OpportunityId)
                        formData.append('opportunityId', srv.OpportunityId);

                    if (srv.ParentName == "Order" && srv.OrderId)
                        formData.append('orderId', srv.OrderId);

                    // for global documents

                    if (srv.Id1)
                        formData.append('Id1', srv.Id1);

                    if (srv.Document != '') {
                        formData.append('sysWideIsEnabled', srv.sysWideIsEnabled);
                        formData.append('sysWideCategory', srv.sysWideCategory);
                        formData.append('sysWideConcept', srv.sysWideConcept);
                        formData.append('sysWideTitle', srv.sysWideTitle);
                        formData.append('Document', srv.Document);
                    }
                    else {
                        formData.append('sysWideTitle', "");
                        formData.append('sysWideTitle', "");
                        formData.append('Document', "");
                    }

                    if (srv.FranchiseSettings)
                        formData.append('IsFranchiseSettings', srv.FranchiseSettings);
                    else
                        formData.append('IsFranchiseSettings', false);

                    if (srv.Note.IsEnabled)
                        formData.append('IsEnabled', srv.Note.IsEnabled);
                    else
                        formData.append('IsEnabled', false);

                    //if (srv.Document == "SYS-DOC") {
                    //    formData.append('title', "title");
                    //    formData.append('category', 0);
                    //    formData.append('IsFranchiseSettings', false);
                    //    formData.append('IsEnabled', srv.Note.IsEnabled);

                    //    formData.append('Id1', srv.Id1);
                    //    formData.append('sysWideIsEnabled', srv.sysWideIsEnabled);
                    //    formData.append('sysWideCategory', srv.sysWideCategory);
                    //    formData.append('sysWideConcept', srv.sysWideConcept);
                    //    formData.append('sysWideTitle', srv.sysWideTitle);
                    //    formData.append('Document', srv.Document);

                    //} else {
                    //
                    //    formData.append('title', srv.Note.Title);
                    //    formData.append('category', srv.Note.SelectedCategory.id);
                    //    if (srv.ParentName == "Lead") {
                    //        formData.append('leadId', srv.LeadId);
                    //    }
                    //    if (srv.ParentName == "Account") {
                    //        formData.append('accountId', srv.AccountId);
                    //    }
                    //    if (srv.ParentName == "Opportunity") {
                    //        formData.append('opportunityId', srv.OpportunityId);
                    //    }
                    //    if (srv.ParentName == "Order") {
                    //        formData.append('orderId', srv.OrderId);
                    //    }

                    //    formData.append('IsFranchiseSettings', srv.FranchiseSettings)
                    //    formData.append('IsEnabled', srv.Note.IsEnabled);

                    //    ///
                    //    formData.append('Id1', 0);
                    //    formData.append('sysWideTitle', "");
                    //    formData.append('sysWideTitle', "");
                    //    formData.append('Document', "");

                    //}
                },
                'success': function (file, response) {
                    file.fileId = response.FileId;
                    file.description = srv.fileDescription;
                    file.category = "photo" //srv.fileCategory;
                    file.createdOn = new Date();
                    if (HFCService.CurrentUser)
                        file.creatorFullName = HFCService.CurrentUser.FullName;
                    file.Url = response.Url;
                    file.ThumbUrl = response.ThumbUrl;

                    srv.FilesCount = srv.FilesCount + 1;
                    //$scope.$apply();

                    srv.fileDescription = "";
                    $(file.previewElement).find(".dz-details").attr("href", file.Url);
                    HFC.DisplaySuccess(file.name + " uploaded");
                    $(file.previewElement).popover({
                        trigger: 'hover',
                        title: file.creatorFullName + ' - ' + new XDate(file.createdOn, true).toString("M/d/yy h:mm tt"),
                        content: (file.description || "No description") + " (" + (file.category || "No Catgory") + ")",
                        placement: 'auto',
                        animation: !HFC.Browser.isMobile()
                    });
                },
                'queuecomplete': function () {
                    //alert('All Files uploaded!');
                    $("#" + srv.divId).modal("hide");
                    //changes for franchise settings - manage documents
                    if (srv.Document == undefined || srv.Document == "" || srv.Document == null) {
                        if (srv.FranchiseSettings) {
                            $('#gridDocuments').data('kendoGrid').dataSource.read();
                            $('#gridDocuments').data('kendoGrid').refresh();
                            GetFranchiseGrid();
                        }
                        else {
                            populateNoteListFromDatabase();
                        }
                    }
                    else {
                        $('#gridDocumentsCP').data('kendoGrid').dataSource.read();
                        $('#gridDocumentsCP').data('kendoGrid').refresh();
                    }
                },
                'error': function (file, response) {
                    //var dropzoneFilesCopy = dropzone.files.slice(0);
                    //dropzone.removeAllFiles();
                    //$.each(dropzoneFilesCopy, function (_, file) {
                    //    if (file.status === Dropzone.ERROR) {
                    //        file.status = undefined;
                    //        file.accepted = undefined;
                    //    }
                    //    dropzone.addFile(file);
                    //});
                }
            }
        };

        // Canel the operation or window.
        srv.Cancel = function () {
            srv.submitnte = false;
            $("#" + srv.divId).modal("hide");
            srv.Id1 = 0;
            // srv.Document = "";
            srv.Brand = "";
            srv.sysWideTitle = "";
            srv.sysWideIsEnabled = false;
            srv.sysWideCategory = "";
            srv.sysWideConcept = "";
            srv.filename = "";
            srv.streamId = "";
            srv.Document = "";
        };

        // SAve Global attachment
        srv.SaveGlobalNoteTP = function (modalId, modal) {
            srv.submitnte = true;
            if (modal.Pop_Note.$invalid) {
                return;
            }

            if (this.DropzoneObject.files.length == 0 && srv.Id1 > 0) {
                var formData = new FormData();
                formData.append('Id1', srv.Id1);
                formData.append('sysWideTitle', srv.sysWideTitle);
                formData.append('sysWideCategory', srv.sysWideCategory);
                formData.append('sysWideConcept', srv.sysWideConcept);
                formData.append('Document', srv.Document);
                formData.append('sysWideIsEnabled', srv.sysWideIsEnabled);
                // var src = "Leads/UpdateGlobalDocDetails?Id1=" + srv.Id1 + "&sysWideTitle=" + srv.sysWideTitle + "&sysWideCategory=" + srv.sysWideCategory + "&sysWideConcept=" + srv.sysWideConcept + "&sysWideIsEnabled=" + srv.sysWideIsEnabled;

                $http.post('/api/File/0/UpdateGlobalDocDetails', formData, //?Id1=" + srv.Id1 + "&sysWideTitle=" + srv.sysWideTitle + "&sysWideCategory=" + srv.sysWideCategory + "&sysWideConcept=" + srv.sysWideConcept + "&sysWideIsEnabled=" + srv.sysWideIsEnabled,formData,
                {
                    withCredentials: true,
                    headers: { 'Content-Type': undefined },
                    transformRequest: angular.identity
                })
            .then(function (result) {
                $('#gridDocumentsCP').data('kendoGrid').dataSource.read();
                $('#gridDocumentsCP').data('kendoGrid').refresh();
            }, function (error) {
            });
                srv.submitnte = false;
                srv.IsBusy = false;
                $("#" + srv.divId).modal("hide");
                return;
            }
            var promise = null;
            if (srv.Mode == "Add") {
                if (!srv.Note.IsNote) {
                    srv.DropzoneObject.processQueue();
                    srv.submitnte = false;
                    return;
                }
            }

            srv.submitnte = false;
            srv.IsBusy = false;
            $("#" + srv.divId).modal("hide");
        }

        // Save or update the notes/attachment to the database
        srv.SaveNoteTP = function (modalId, modal) {
            $rootScope.$broadcast("RefreshNoteGrid", "true");
            if (srv.Document == 'SYS-DOC') {
                this.SaveGlobalNoteTP(modalId, modal);
                return;
            }

            srv.submitnte = true;
            // TODO: validation yet to be done.
            if (modal.Pop_Note.$invalid) {
                // HFC.DisplaySuccess("Please fill all the required fields! and valid data in optional fields if provided.");

                return;
            }

            if (srv.FranchiseDocuments && !(srv.Note.IsSales ||
                                            srv.Note.IsProductGuide ||
                                            srv.Note.IsInstallation)) {
                $(".ui-dialog-title").text("Alert");
                HFC.DisplayAlert('One or more Category is required');
                return;
            }

            if (srv.Mode == "Add" && !srv.Note.IsNote) {
                srv.DropzoneObject.processQueue();
                srv.submitnte = false;
                return;
            }

            var promise = null;

            if (srv.ParentName == "Lead") {
                srv.AccountId = 0;
                srv.OpportunityId = 0;
                srv.OrderId = 0;
            }
            if (srv.ParentName == "Account") {
                srv.LeadId = 0;
                srv.OpportunityId = 0;
                srv.OrderId = 0;
            }

            if (srv.ParentName == "Opportunity") {
                srv.LeadId = 0;
                srv.AccountId = 0;
                srv.OrderId = 0;
            }
            if (srv.ParentName == "Order") {
                srv.LeadId = 0;
                srv.AccountId = 0;
                srv.OpportunityId = 0;
            }

            var selectedCategories = '';
            if (srv.FranchiseDocuments) {
                if (srv.Note.IsSales == true) {
                    selectedCategories = '10';
                }
                if (srv.Note.IsInstallation == true) {
                    selectedCategories += (selectedCategories != '') ? ',12' : '12';
                }
                if (srv.Note.IsProductGuide == true) {
                    selectedCategories += (selectedCategories != '') ? ',11' : '11';
                }
            } else {
                selectedCategories = '' + srv.Note.SelectedCategory.id + '';
            }

            // TODO: move to a common helper method.
            var notemodel = {
                Title: srv.Note.Title,
                Notes: srv.Note.Description,
                TypeEnum: srv.Note.SelectedCategory.id,
                NoteId: srv.Note.NoteId,
                LeadId: srv.LeadId,
                AccountId: srv.AccountId,
                OpportunityId: srv.OpportunityId,
                OrderId: srv.OrderId,
                IsEnabled: srv.Note.IsEnabled,
                // As attachments in manage documents is now supports multiple
                // categoreis to select, even there is one we need to send it via tis.
                Categories: selectedCategories //'' + srv.Note.SelectedCategory.id + ''
            }

            if (srv.Mode == "Add") {
                // Add to the Database.
                promise = $http.post('/api/notes/0/AddNewNote/', notemodel);
                srv.submitnte = false;
            } else if (srv.Mode == "Edit") {
                // update the notes database.
                promise = $http.post('/api/notes/0/UpdateNote', notemodel);
                srv.submitnte = false;
            }

            promise.then(function (response) {
                if (srv.Note.IsNote) {
                    HFC.DisplaySuccess("Note udpated.");
                } else {
                    HFC.DisplaySuccess("Attachment udpated.");
                }

                srv.IsBusy = false;
                $("#" + srv.divId).modal("hide");

                // Repopulate the collection.
                if (srv.Id == 0)
                    if (srv.FranchiseSettings) {
                        $('#gridDocuments').data('kendoGrid').dataSource.read();
                        $('#gridDocuments').data('kendoGrid').refresh();
                        GetFranchiseGrid();
                    }
                    else {
                        populateNoteListFromDatabase();
                    }
            }, function (response) {
                HFC.DisplayAlert(response.statusText);
                srv.IsBusy = false;
            });
        }

        // Show the popup for new Note.
        srv.AddNoteTP = function () {
            srv.FranchiseDocuments = false;

            srv.Id = 0;
            srv.Document = "";
            srv.Brand = "";

            srv.Mode = "Add";
            srv.NoteIndex = -1;
            srv.Note = {};
            //https://docs.angularjs.org/api/ng/directive/select
            //srv.Note.SelectedCategory = {};
            //srv.Note.SelectedCategory.id = 0;
            srv.Note.IsNote = true;

            populateCategoryList(true, false);

            $("#" + srv.divId).modal("show");
        }

        // Show the popup for new Attachment
        srv.AddAttachment = function () {
            srv.FranchiseDocuments = false;

            srv.Id = 0;
            srv.Document = "";
            srv.Brand = "";
            // alert("Its working!");

            srv.Mode = "Add";
            srv.NoteIndex = -1;
            srv.Note = {};

            //https://docs.angularjs.org/api/ng/directive/select
            //srv.Note.SelectedCategory = {};
            //srv.Note.SelectedCategory.id = 6;
            srv.Note.IsNote = false;

            populateCategoryList(false, false);

            // We have remove all the existing files before we show the fresh options..
            //https://github.com/enyo/dropzone/wiki/Remove-all-files-with-one-button
            if (srv.DropzoneObject) {
                srv.DropzoneObject.removeAllFiles();
            }

            $("#" + srv.divId).modal("show");
        }
        srv.addGlobalDocuments = function (Id) {
            this.Cancel();

            srv.Id1 = Id;
            srv.Document = 'SYS-DOC';
            srv.Mode = "Add";
            srv.NoteIndex = -1;
            srv.Note = {};
            srv.Note.IsNote = false;

            if (srv.DropzoneObject) {
                srv.DropzoneObject.removeAllFiles();
            }

            $("#" + srv.divId).modal("show");
        }

        srv.DisplayGlobalDoc = function (Title, ConceptTitle, CategoryTitle, Name, streamId) {
            this.Cancel();
            srv.Note = {};
            srv.Document = "SYS-DOC";
            srv.Mode = "Display";
            srv.Note.IsNote = false;
            srv.sysWideTitle = Title;
            srv.sysWideCategoryTitle = CategoryTitle.toString();
            srv.sysWideConceptTitle = ConceptTitle.toString();
            srv.filename = Name;
            srv.streamId = streamId;

            $("#" + srv.divId).modal("show");
        }


        srv.updateGlobalDocuments = function (dataValue) {
            this.Cancel();

            srv.NoteIndex = -1;
            srv.Note = {};
            srv.Id1 = dataValue.Id;
            srv.sysWideTitle = dataValue.Title;
            srv.sysWideIsEnabled = dataValue.IsEnabled;
            srv.sysWideCategory = dataValue.Category.toString();
            srv.sysWideConcept = dataValue.Concept.toString();
            srv.filename = dataValue.Name;
            srv.streamId = dataValue.streamId;
            srv.Document = 'SYS-DOC';
            srv.Mode = "Add";
            srv.Note.IsNote = false;
            if (srv.DropzoneObject) {
                srv.DropzoneObject.removeAllFiles();
            }

            $("#" + srv.divId).modal("show");
        }

        // Show the popup for new Attachment in Franchise Settings - changes for franchise settings - manage documents
        srv.AddFranchiseDocuments = function () {
            srv.FranchiseDocuments = true;
            srv.Id = 0;
            srv.Document = "";
            srv.Brand = "";
            srv.Mode = "Add";
            srv.NoteIndex = -1;
            srv.Note = {};
            srv.Note.SelectedCategory = {};
            srv.Note.SelectedCategory.id = 10;
            srv.Note.IsNote = false;
            populateCategoryList(false, true);

            // We have remove all the existing files before we show the fresh options..
            //https://github.com/enyo/dropzone/wiki/Remove-all-files-with-one-button
            if (srv.DropzoneObject) {
                srv.DropzoneObject.removeAllFiles();
            }

            $("#" + srv.divId).modal("show");
        }

        // Show the popup for editing notes/attachment
        srv.EditNoteTP = function (rowIndex) {
            srv.Mode = "Edit";
            handleDisplayAndEdit(rowIndex, 0);
        }
        var id;

        srv.deleteNoteTP = function (NoteId) {
            srv.Id = 0;
            srv.Document = "";
            srv.Brand = "";
            id = NoteId
            $("#" + srv.Delete).modal("show");
        }

        srv.Delete2 = "__delete__"
        var delete2NoteId = 0;
        srv.DeleteNoteTP2 = function (noteId) {
            delete2NoteId = noteId;
            $("#" + srv.Delete2).modal("show");
        }

        srv.DeleteCancel2 = function () {
            $("#" + srv.Delete2).modal("hide");
            delete2NoteId = 0;
        };

        srv.DeleteYes2 = function () {
            var deleteurl = '/api/notes/' + delete2NoteId;
            $http.delete(deleteurl).then(function (response) {
                HFC.DisplaySuccess("Delete success");
                populateNoteListFromDatabase();
                srv.IsBusy = false;
                $("#" + srv.Delete2).modal("hide");
            }, function (response) {
                HFC.DisplayAlert(response.statusText);
                srv.IsBusy = false;
                $("#" + srv.Delete2).modal("hide");
            })
        }

        srv.RecoverNote = function (noteId) {
            var recoverurl = '/api/notes/' + noteId + '/RecoverNote';
            $http.post(recoverurl).then(function (response) {
                HFC.DisplaySuccess("Recover success");
                populateNoteListFromDatabase();
                srv.IsBusy = false;
            }, function (response) {
                HFC.DisplayAlert(response.statusText);
                srv.IsBusy = false;
            })
        }

        srv.Deleteyes = function () {
            var promise = null;
            // promise = $http.post('/api/notes/Delete/', id);
            var notes = {
                id: id
            }
            $http.post('/api/notes/delete', notes)
            var deleteurl = '/api/notes/' + id;
            $http.delete(deleteurl).then(function (response) {
                var res = response.data;
                if (res == true) {
                    $("#" + srv.Delete).modal("hide");
                    $("#" + srv.divId).modal("hide");
                    $http.get('/api/notes/' + srv.LeadId + '/GetLeadNotes/?showDeleted=' + includeDeleted);
                    HFC.DisplaySuccess("Delete success");
                    populateNoteListFromDatabase();
                }
                else {
                }
            });
        };

        srv.DeleteCancel = function () {
            $("#" + srv.Delete).modal("hide");
            srv.Id = 0;
            srv.Brand = "";
            srv.Document = "";
        };

        // Show the popup for displaying notes/attachment
        srv.DisplayNoteTP = function (rowIndex) {
            srv.Mode = "Display";
            handleDisplayAndEdit(rowIndex, 0);
        }

        //global search
        srv.DisplayGlobalNoteTP = function (NoteId) {
            srv.Mode = "Display";
            var result = $http.get('/api/notes/' + srv.NoteId + '/GetGlobalNotess').then(function (responseSuccess) {
                srv.NoteList = responseSuccess.data;
                srv.IsBusy = false;
                handleDisplayAndEdit(0, NoteId);
            }, function (responseError) {
                srv.IsBusy = false;
                HFC.DisplayAlert(responseError.statusText);
            });
        }
        srv.EditGlobalNoteTP = function (NoteId) {
            srv.Mode = "Edit";
            var result = $http.get('/api/notes/' + srv.NoteId + '/GetGlobalNotess').then(function (responseSuccess) {
                srv.NoteList = responseSuccess.data;
                handleDisplayAndEdit(0, NoteId);
            }, function (responseError) {
                HFC.DisplayAlert(responseError.statusText);
            });
        }
        // Show the popup for editing notes/attachment - changes for franchise settings - manage documents
        srv.EditFranchiseDocument = function (NoteId) {
            srv.Mode = "Edit";
            handleDisplayAndEdit(0, NoteId);
        }

        // Show the popup for displaying notes/attachment - changes for franchise settings - manage documents
        srv.DisplayFranchiseDocument = function (NoteId) {
            srv.Mode = "Display";
            handleDisplayAndEdit(0, NoteId);
        }

        // Initialize the service and gets the list of Notes/Attachments for
        // the associated Lead, account, opportunity.
        srv.Initialize = function () {
            includeDeleted = false;
            srv.NoteList = [];
            srv.Note = {};

            srv.IsBusy = true;

            populateNoteListFromDatabase();

            srv.IsBusy = false;
        }

        //srv.AddAttachmentTP = function () {
        //    HFC.DisplayAlert("Coming Soon...");
        //}

        // The category list should be populated based on whethere we are
        // working on Attachment or Note, as the options differ
        function populateCategoryList(isNote, isFranchiseSettings) {
            if (isNote) {
                srv.CategoryList = [
                    { id: 0, name: 'Internal' },
                    { id: 5, name: 'External' }, // Earlier it was Customer.
                    { id: 2, name: 'Direction' }
                ];
            }
            else if (isFranchiseSettings) { //changes for franchise settings - manage documents
                srv.CategoryList = [
                   { id: 10, name: 'Sales' },
                   { id: 11, name: 'Product Guide' },
                   { id: 12, name: 'Installation' }
                ];
            }
            else {
                srv.CategoryList = [
                    { id: 6, name: 'Photo' },
                    { id: 7, name: 'Plans' },
                    { id: 8, name: 'Document' },
                    { id: 9, name: 'Other' },
                    { id: 10, name: 'Sales' },
                    { id: 11, name: 'Product Guide' },
                    { id: 12, name: 'Installation' }
                ];
            }
        }

        // The angular model is same for both Add and Edit, the only difference
        // is add should set some default value whereas for edit we should fetch
        // the value from collection and assign to the current note.

        function handleDisplayAndEdit(rowIndex, NoteId) {
            srv.NoteIndex = rowIndex;

            srv.Note = {};
            srv.Note.CategoryId = 0;
            var exstingNote = null;
            if (NoteId && parseInt(NoteId) > 0) {
                exstingNote = $.grep(srv.NoteList, function (e) { return e.NoteId == NoteId; })[0];
            }
            else {
                exstingNote = srv.NoteList[rowIndex];
            }
            srv.Note.Title = exstingNote.Title
            //https://docs.angularjs.org/api/ng/directive/select
            //https://docs.angularjs.org/api/ng/directive/select#using-ngvalue-to-bind-the-model-to-an-array-of-objects
            srv.Note.SelectedCategory = {};
            srv.Note.SelectedCategory.id = exstingNote.TypeEnum;

            srv.Note.CategoryName = exstingNote.CategoryName;
            srv.Note.Description = exstingNote.Notes;
            srv.Note.NoteId = exstingNote.NoteId;
            srv.Note.IsNote = exstingNote.IsNote;
            srv.Note.FileName = exstingNote.FileName;
            srv.Note.AttachmentStreamId = exstingNote.AttachmentStreamId;
            srv.Note.index = rowIndex;
            srv.Note.LeadId = exstingNote.LeadId;
            srv.Note.AccountId = exstingNote.AccountId;
            srv.Note.OpportunityId = exstingNote.OpportunityId;
            srv.Note.OrderId = exstingNote.OrderId;
            srv.Note.IsEnabled = exstingNote.IsEnabled;

            //changes for franchise settings - manage documents
            if (exstingNote.FranchiseId && exstingNote.FranchiseId != null) {
                srv.FranchiseDocuments = true;
                populateCategoryList(exstingNote.IsNote, true);
            }
            else {
                srv.FranchiseDocuments = false;
                populateCategoryList(exstingNote.IsNote, false);
            }

            if (srv.FranchiseDocuments) {
                if (!srv.LeadId) {
                    srv.LeadId = exstingNote.LeadId;
                }
                if (!srv.AccountId) {
                    srv.AccountId = exstingNote.AccountId;
                }
                if (!srv.OpportunityId) {
                    srv.OpportunityId = exstingNote.OpportunityId;
                }
                if (!srv.OrderId) {
                    srv.OrderId = exstingNote.OrderId;
                }

                var categoriesarray = exstingNote.Categories.split(',');
                if (categoriesarray) {
                    var length = categoriesarray.length;
                    for (var i = 0; i < length; i++) {
                        var temp = categoriesarray[i];
                        if (temp == 10) srv.Note.IsSales = true;
                        if (temp == 12) srv.Note.IsInstallation = true;
                        if (temp == 11) srv.Note.IsProductGuide = true;
                    }
                }
            }

            srv.Id = 0;
            srv.Document = "";
            srv.Brand = "";

            $("#" + srv.divId).modal("show");
        }

        // The list should be refresshed for each time, whether we create a new
        // Note/Attachment or edit.
        function populateNoteListFromDatabase() {
            var geturl = getSourceUrl();
            $http.get(geturl).then(function (responseSuccess) {
                srv.NoteList = responseSuccess.data;
            }, function (responseError) {
                HFC.DisplayAlert(responseError.statusText);
            });
        }
        var offsetvalue = 0;
        // By default deleted notes are not included.
        var includeDeleted = false;

        srv.RefreshNotesGrid = function (showDeleted) {
            includeDeleted = showDeleted == true ? true : false;
            srv.IsBusy = true;
            populateNoteListFromDatabase();
            srv.IsBusy = false;
        }

        // The source Url for each element is differ and this is the only way we
        // differentiate elements like Lead, Account, etc.
        function getSourceUrl() {
            if (srv.ParentName == "Lead") {
                if (srv.LeadId == undefined) srv.LeadId = 0;
                return '/api/notes/' + srv.LeadId + '/GetLeadNotes/?showDeleted=' + includeDeleted;
            }

            if (srv.ParentName == "Account") {
                return '/api/notes/' + srv.AccountId + '/GetAccountNotes/?showDeleted=' + includeDeleted;
            }

            if (srv.ParentName == "Opportunity") {
                return '/api/notes/' + srv.OpportunityId + '/GetOpportunityNotes/?showDeleted=' + includeDeleted;
            }
            if (srv.ParentName == "Order") {
                return '/api/notes/' + srv.OrderId + '/GetOrderNotes/?showDeleted=' + includeDeleted;
            }

            // Display to the user that invalid parameter is set:
            HFC.DisplayAlert("Invalid Entity name : " + " set from the called controller");
        }

        // TODO: do we really need this???
        function chkNumeric(inputtxt) {
            if (isNaN(inputtxt)) {
                return false;
            }
            return true;
        }

        // TODO: do we really need this???
        function allLetter(inputtxt) {
            var letters = /^[A-Za-z]+$/;
            if (inputtxt.match(letters)) {
                return true;
            }
            else {
                return false;
            }
        }

        srv.IsBusy = false;

        function AccountLinkTemplate(dataItem) {
            var Div = '';
            if (dataItem.AccountId && dataItem.AccountId != null) {
                Div = "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.AccountName + "</div><a href='#!/Accounts/" + dataItem.AccountId + "' style='color: rgb(61,125,139);'>" + dataItem.AccountName + "</a></span>";
            }
            return Div;
        }

        function OpportunityTemplate(dataItem) {
            var Div = '';
            if (dataItem.OpportunityId && dataItem.OpportunityId != null) {
                Div = "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.OpportunityName + "</div><a href='#!/OpportunityDetail/" + dataItem.OpportunityId + "' style='color: rgb(61,125,139);'>" + dataItem.OpportunityName + "</a></span>";
            }
            return Div;
        }

        function OrderTemplate(dataItem) {
            var Div = '';
            if (dataItem.OrderId && dataItem.OrderId != null) {
                Div = "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.OrderName + "</div><a href='#!/Orders/" + dataItem.OrderId + "' style='color: rgb(61,125,139);'>" + dataItem.OrderName + "</a></span>";
                //Div = "<a href='#!/Orders/" + dataItem.OrderId + "' style='color: rgb(61,125,139);'>" + dataItem.OrderName + "</a>";
            }
            return Div;
        }
        function CheckImageFile(filename) {
            if (filename != null) {
                var validFormats = ['jpg', 'jpeg', 'png'];
                var ext = filename.substr(filename.lastIndexOf('.') + 1);
                if (ext == '') return false;
                if (validFormats.indexOf(ext) < 0) {
                    return false;
                }
                return true;
            }
            return false;
        }
        function AttachmentTemplate(dataItem) {
            var Div = '';
            if (dataItem.AttachmentStreamId && dataItem.AttachmentStreamId != null) {
                Div = "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.FileName + "</div><a href='/api/Download?streamid=" + dataItem.AttachmentStreamId + "' style='color: rgb(61,125,139);'>" + dataItem.FileName + "</a></span>";
                //Div = "<a href='/api/Download?streamid=" + dataItem.AttachmentStreamId + "' target=\"_blank\" style='color: rgb(61,125,139);'>" + dataItem.FileName + "</a> ";
            }
            return Div;
        }

        //Commented the prview code
        //function FranchiseGridPreviewTemplate(dataItem) {
        //    var Div = '';
        //    if (CheckImageFile(dataItem.FileName)) {
        //        Div = '<img src="/api/Download?streamid=' + dataItem.AttachmentStreamId + '" style="height: 95px;"/>';
        //    }
        //    else {
        //        Div = '<img src="../Images/Document-icon.png" style="height: 100px;"/>';
        //    }
        //    return Div;
        //}

        function FranchiseGridDropdownTemplate(dataItem) {
            if (srv.FranchiseSettingsEdit) {
                var PermissionEditPermission = srv.FranchiseSettingsEdit;

                var Edithtml = "";

                if (PermissionEditPermission == true) {
                    Edithtml = '<li><a class="deactivateLink" ng-click="NoteServiceTP.EditFranchiseDocument(' + dataItem.NoteId + ')">Edit</a></li>';
                }
                //return '<ul> <li class="dropdown note1 ad_user"><div class="btn-group"><button class="btn btn-default dropdown-toggle" data-toggle="dropdown">' +
                //   '<i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i></button><ul class="dropdown-menu pull-right" style="z-index: 1;">' +
                //   '<li><a class="deactivateLink" ng-click="NoteServiceTP.DisplayFranchiseDocument(' + dataItem.NoteId + ')">View</a><li>' + Edithtml +
                //    '<li><a class="deactivateLink" ng-click="NoteServiceTP.DeleteFranchiseDocument(' + dataItem.NoteId + ')">Delete</a></li>';

                return '<ul><li class="dropdown note1 ad_user"><div class="dropdown">' +
                    '<button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> ' +
                    '<i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button>' +
                    ' <ul class="dropdown-menu pull-right">' +
                    '<li><a class="deactivateLink" ng-click="NoteServiceTP.DisplayFranchiseDocument(' + dataItem.NoteId + ')">View</a><li>' + Edithtml +
                    '<li><a class="deactivateLink" ng-click="NoteServiceTP.DeleteFranchiseDocument(' + dataItem.NoteId + ')">Delete</a></li>' +
                    '</ul></li></ul>';
            }
            else {
                return '';
            }
        };

        srv.DeleteFranchiseDocument = function (NoteId) {
            if (confirm('Do you want to delete this document?')) {
                $http.get('/api/notes/' + NoteId + '/DeleteDocuments/').then(function (responseSuccess) {
                    //
                    if (responseSuccess.data != 'Success')
                        alert('Not Able To Delete');
                    else {
                        $("#gridDocuments").data("kendoGrid").dataSource.read();
                        $('#gridDocuments').data('kendoGrid').refresh();

                        alert('Deleted Successfully');
                    }
                });
            }
        }

        srv.FranchiseSettingsGridOptions = {

            //dataSource: ds,
            dataSource: {
                transport: {
                    read: {
                        url: '/api/notes/0/GetFranchiseDocuments',
                        dataType: "json"
                    }
                },
                error: function (e) {
                    HFC.DisplayAlert(e.errorThrown);
                },
                pageSize: 25,
                schema: {
                    model: {
                        fields: {
                            Title: { editable: false },
                            CategoriesName: { editable: false },
                            AccountName: { editable: false },
                            IsEnabled: { type: "boolean", editable: false },
                            CreatedOn: { type: "date", editable: false },
                            FileType: { editable: false },
                            FileName: { editable: false },
                           
                            
                        }
                    }
                }
            },
            // https://www.telerik.com/forums/remove-page-if-no-records
            cache: false,
            batch: true,
            sortable: true,
            Records: { template: "No records found" },
            dataBound: function (e) {
                if (this.dataSource.view().length == 0) {
                    // The Grid contains No recrods so hide the footer.
                    $('.k-pager-nav').hide();
                    $('.k-pager-numbers').hide();
                    $('.k-pager-sizes').hide();
                } else {
                    // The Grid contains recrods so show the footer.
                    $('.k-pager-nav').show();
                    $('.k-pager-numbers').show();
                    $('.k-pager-sizes').show();
                }
                //if (kendotooltipService.columnWidth) {
                //    kendotooltipService.restrictTooltip(kendotooltipService.columnWidth);
                //} else if (window.innerWidth < 1280) {
                //    kendotooltipService.restrictTooltip(null);
                //}
                if (kendotooltipService.columnWidth) {
                    kendotooltipService.restrictTooltip(kendotooltipService.columnWidth);
                } else if (window.innerWidth < 1280) {
                    kendotooltipService.restrictTooltip(null);
                }

            },
            resizable: true,
            columns: [
                      {
                          field: "Title",
                          title: "Title",
                          width: "150px",
                          template: "<span class='tooltip-wrapper'><div class='tooltip-content' >#= Title #</div><a ng-click='NoteServiceTP.DisplayFranchiseDocument(#= NoteId #)' style='color: rgb(61,125,139);'>#= Title #</a></span> ",
                          //filterable: { multi: true, search: true, dataSource: ds }
                          filterable: { search: true }
                      },
                      {
                          field: "CategoriesName", //"CategoryName",
                          title: "Category",
                          width: "150px",
                          template: function CategoriesName(dataItem) {
                              var template = "";
                              if (dataItem.CategoriesName) {
                                  template += "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.CategoriesName + "</div><span>" + dataItem.CategoriesName + "</span></span>";
                                  return template;
                              }
                              else {
                                  return '';
                              }

                          },
                          filterable: { multi: true, search: true }
                          //filterable: { multi: true, search: true, dataSource: ds }

                      },
                      {
                          field: "AccountName",
                          title: "Account",
                          width: "150px",
                          template: function (dataItem) {
                              return AccountLinkTemplate(dataItem);
                          },
                          filterable: {  search: true }
                          //filterable: { multi: true, search: true, dataSource: ds }
                      },
                      {
                          field: "OpportunityName",
                          title: "Opportunity",
                          width: "150px",
                          template: function (dataItem) {
                              return OpportunityTemplate(dataItem);
                          },
                          filterable: {  search: true }
                          //filterable: { multi: true, search: true, dataSource: ds }
                      },
                      {
                          field: "FileType",
                          title: "Order",
                          width: "100px",
                          filterable: {  search: true }
                          //filterable: { multi: true, search: true, dataSource: ds }
                      },
                      {
                          field: "FileName",
                          title: "View",
                          width: "200px",
                          template: function (dataItem) {
                              return AttachmentTemplate(dataItem);
                          },
                          filterable: { search: true }
                          //filterable: { multi: true, search: true, dataSource: ds }
                          //
                      },
                        //commented the Preview Code
                      //{
                      //    title: "Preview",
                      //    template: function (dataItem) {
                      //        return FranchiseGridPreviewTemplate(dataItem);
                      //    }
                      //},
                      {
                          field: "CreatedOn",
                          title: "Created",
                          width: "150px",
                          // template: "#= CreatedOn #",
                          template: function (dataItem) {
                              var html = "<span class='tooltip-wrapper'><div class='tooltip-content'>" + HFCService.KendoDateFormat(dataItem.CreatedOn) + "</div><span>" + HFCService.KendoDateFormat(dataItem.CreatedOn) + "</span></span>"
                              //var html = '<div><span>' + HFCService.KendoDateFormat(dataItem.CreatedOn) + ' </span></div>'
                              return html;
                          },
                          type: "date",
                          filterable: HFCService.gridfilterableDate("date", "NotesFilterCalendar", offsetvalue),
                      },
                      {
                          field: "IsEnabled",
                          title: "Enabled",
                          width: "125px",
                          template: function (dataItem) {
                              // https://stackoverflow.com/questions/805107/creating-multiline-strings-in-javascript
                              var checkedStatus = dataItem.IsEnabled == true ? "checked" : "";
                              var html = '<input type="checkbox" class="option-input checkbox" '
                              html += checkedStatus;
                              html += ' disabled>';

                              return html;
                          },
                          filterable: {
                              multi: true,
                              //dataSource: [{ IsEnabled: true }, { IsEnabled: false }]
                          }
                      },
                      {
                          title: "",
                          width: "100px",
                          template: function (dataItem) {
                              return FranchiseGridDropdownTemplate(dataItem);
                          }
                      }
            ],
            noRecords: { template: "No records found" },

            //filterable : true,

            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        isempty: "Empty",
                        isnotempty: "Not empty"
                    },
                    date: {
                        gt: "After (Excludes)",
                        lt: "Before (Includes)"
                    },
                }
            },
            filterMenuInit: function (e) {
                $(e.container).find('.k-check-all').click()
            },
            pageable: {
                refresh: true,
                pageSize: 25,
                resizable: true,
                pageSizes: [25, 50, 100],
                buttonCount: 5,
                change: function (e) {
                    var myDiv = $('.k-grid-content.k-auto-scrollable');
                    myDiv[0].scrollTop = 0;
                }
            },

            toolbar: kendo.template($('<script id="template" type="text/x-kendo-template"><div class="row vendor_search no-margin" ><div class="leadsearch_topleft row no-margin col-sm-10 col-md-10 col-xs-10 no-padding"><div class="col-xs-2 col-sm-2 no-padding hfc_txtbox"><input ng-keyup="NoteServiceTP.gridDocumentsSearch()" type="search" id="searchBox" placeholder="Search Document" class="k-textbox leadsearch_tbox"></div><div class="col-xs-2 col-sm-2 measurement_search no-padding"><input type="button" id="btnReset" ng-click="NoteServiceTP.gridDocumentsSearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div></div><div class="col-sm-2 col-xs-2 no-padding"><div class ="leadsearch_icon"><div class ="plus_but tooltip-bottom" data-tooltip="Add Document" ng-if="NoteServiceTP.FranchiseSettingsAdd" ng-click="NoteServiceTP.AddFranchiseDocuments()"><i class="far fa-plus-square"></i></div></div></script>').html()),

            columnResize: function (e) {
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 238);
                getUpdatedColumnList(e);
            }
        };

   

      


        var getUpdatedColumnList = function (e) {
            kendotooltipService.columnResizeTooltip(e.newWidth, e.column.headerAttributes.id);
        }



        



        //will be called by the consumber - murugan
        //GetFranchiseGrid();

        //};

        srv.populateFranchiseGrid = function () {
            var loadingElement = document.getElementById("loading");
            loadingElement.style.display = "block";
            GetFranchiseGrid();
        }
        function GetFranchiseGrid() {             
            $http.get("/api/notes/0/GetFranchiseDocuments").then(function (responseSuccess) {
                srv.NoteList = responseSuccess.data;
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";
            }, function (responseError) {
                 var loadingElement = document.getElementById("loading");
                        loadingElement.style.display = "none";
                HFC.DisplayAlert(responseError.statusText);
                
            });

        }

        function GetDefaultFilter() {
            try {
                if (ds.options.data.some(function (el) { if (el.CategoryName === "Sales") { return 63; } })) {
                    return "Sales";
                }
            } catch (ex) {
                return "";
            }
        }

        srv.DeleteGlobalDocument = function (Id) {
            if (confirm('Are you sure to delete this document?')) {
                $http.post('/api/notes/' + Id + '/DeleteGlobalDocument').then(function (responseSuccess) {
                    $('#gridDocumentsCP').data('kendoGrid').dataSource.read();
                    $('#gridDocumentsCP').data('kendoGrid').refresh();
                }, function (responseError) {
                    HFC.DisplayAlert(responseError.statusText);
                });
            }
        }

        srv.gridDocumentsSearch = function () {
            var searchValue = $('#searchBox').val();
            $("#gridDocuments").data("kendoGrid").dataSource.filter({
                logic: "or",
                filters: [
                  {
                      field: "Title",
                      operator: "contains",
                      value: searchValue
                  },
                  {
                      field: "CategoriesName",
                      operator: "contains",
                      value: searchValue
                  },
                  {
                      //field: "AccountId",
                      field: "AccountName",
                      operator: "contains",
                      value: searchValue
                  },
                  {
                      //field: "OpportunityId",
                      field: "OpportunityName",
                      operator: "contains",
                      value: searchValue
                  },
                  {
                      //field: "CategoryName",
                      field: "FileType",
                      operator: "contains",
                      value: searchValue
                  },
                  {
                      field: "FileName",
                      operator: "contains",
                      value: searchValue
                  },
                  {
                      field: "IsEnabled",
                      operator: (value) => (value + "").indexOf(searchValue) >= 0,
                      value: searchValue
                  },
                ]
            });
        }

        srv.gridDocumentsSearchClear = function () {
            $('#searchBox').val('');
            $("#gridDocuments").data('kendoGrid').dataSource.filter({
            });
        }

        // FOr GLOBAL DOCUMENTS

        srv.CPSettingsDocumentGridOptions = {
            cache: false,
            dataSource: {
                transport: {
                    read: {
                        url: '/api/Source/0/GetGlobaldocList',
                        dataType: "json"
                    }
                }
                ,
                error: function (e) {
                    HFC.DisplayAlert(e.errorThrown);
                },
                pageSize: 25
            },
            dataBound: function (e) {
                if (this.dataSource.view().length == 0) {
                    $('.k-pager-nav').hide();
                    $('.k-pager-numbers').hide();
                    $('.k-pager-sizes').hide();
                } else {
                    $('.k-pager-nav').show();
                    $('.k-pager-numbers').show();
                    $('.k-pager-sizes').show();
                }
            },
            columns: [
                {
                    field: "Id",
                    title: "Id",
                    hidden: true
                },
            {
                field: "streamId",
                title: "streamId",
                hidden: true
            },
             {
                 title: "Title",
                 field: "Title",
                 filterable: {
                     //multi: true,
                     search: true
                 },
                 template: function (dataItem) {
                     return '<div><a href="javascript:void(0)" ng-click="NoteServiceTP.DisplayGlobalDoc(dataItem.Title, dataItem.ConceptTitle, dataItem.CategoryTitle, dataItem.Name, dataItem.streamId)"><span>' + dataItem.Title + '</span></a></div>'
                 }
             },
                {
                    field: "ConceptTitle",
                    title: "Concept",
                    filterable: { multi: true, search: true }
                    //template: function (dataItem) {
                    //    return '<div><span>' + dataItem.Concept_Title + '</span></div>'
                    //    }
                },
                {
                    title: "Category",
                    field: "CategoryTitle",
                    filterable: { multi: true, search: true }
                    //template: function (dataItem) {
                    //    return '<div><span>' + dataItem.Category_Title + '</span></div>'
                    //}
                },
                  {
                      title: "View",
                      field: "Name",
                      filterable: {
                          //multi: true,
                          search: true
                      },
                      template: function (dataItem) {
                          var html = '<div><a href="/api/Download?streamid=' + dataItem.streamId + '" target="_blank"><span>' + dataItem.Name + ' </span></a></div>'
                          return html;
                      }
                  },
                      {
                          title: "Created",
                          field: "CreatedOn",
                          type: "date",
                          filterable: HFCService.gridfilterableDate("date", "ManageDocFilterCalendar", offsetvalue),
                          template: function (dataItem) {
                              var html = '<div><span>' + HFCService.KendoDateFormat(dataItem.CreatedOn) + ' </span></div>'
                              return html;
                          }
                      },
                         {
                             title: "Enabled",
                             filterable: { multi: true, search: true },
                             template: function (dataItem) {
                                 if (dataItem.IsEnabled == true)
                                     return '<div><input disabled="disabled" checked type="checkbox" class="option-input checkbox "></div>'
                                 else
                                     return '<div><input disabled="disabled" type="checkbox" class="option-input checkbox "></div>'
                             }
                         },
                           {
                               field: "",
                               title: "",
                               template: function (dataItem) {
                                   return NotesTemplate(dataItem);
                                   //return '<ul><li class="dropdown note1 ad_user"><div class="btn-group"><button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> <ul class="dropdown-menu pull-right"><li><a href="javascript:void(0)"  ng-click="NoteServiceTP.updateGlobalDocuments(dataItem.Id,dataItem.Title,dataItem.Concept,dataItem.Category,dataItem.Name,dataItem.IsEnabled)">Edit</a></li><li><a href="javascript:void(0)" ng-click="NoteServiceTP.DeleteGlobalDocument(dataItem.Id)">Delete2222</a></li></ul></li></ul>';
                               }
                           }
                      //{
                      //    title: "Attach",
                      //    template: function (dataItem) {
                      //        var checkedStatus = dataItem.IsEnabled == true ? "checked" : "";
                      //        var html = '<div ><div><u  style="cursor: pointer;" ng-click="NoteServiceTP.addGlobalDocuments(dataItem.Id,dataItem.Brand,dataItem.Document)">attach</u></div></div>';
                      //        return html;
                      //    }
                      //}
            ],
            noRecords: { template: "No records found" },
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        isnotempty: "Not empty",
                        isempty: "Empty",
                    },
                    date: {
                        gt: "After (Excludes)",
                        lt: "Before (Includes)"
                    },
                }
            },
            pageable: {
                refresh: true,
                pageSize: 25,
                pageSizes: [25, 50, 100],
                buttonCount: 5
            },
            toolbar: kendo.template($(' <script id="template" type="text/x-kendo-template"><div class="vendor_search row no-margin" ><div  class="leadsearch_topleft row no-margin col-sm-12 col-md-12 col-xs-12 no-padding"><div class="col-xs-2 col-sm-2 no-padding hfc_txtbox"><input ng-keyup="NoteServiceTP.gridDocumentsSearchh()" type="search" id="searchBox" placeholder="Search Document" class="k-textbox leadsearch_tbox"></div><div class="col-xs-2 col-sm-2 measurement_search no-padding"><input type="button" id="btnReset" ng-click="NoteServiceTP.gridDocumentsSearchClearr()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div></div></div></script>').html())
        };

        function NotesTemplate(dataItem) {

            var div = "";
            var edit = srv.Permission.PermissionEditDoc;
            var Delete = srv.Permission.PermissionDeleteDoc;
            //var view = srv.Permission.PermissionViewDoc;
            var value1 = "";
            var value2 = "";
            if (edit == false && Delete == false) {
                div = "";
            }

            else {
                if (edit == true) {
                    value1 = '<li><a ng-click="NoteServiceTP.updateGlobalDocuments(dataItem)">Edit</a></li>';

                }
                if (Delete == true) {
                    value2 = '<li><a ng-click="NoteServiceTP.DeleteGlobalDocument(' + dataItem.Id + ')">Delete</a></li>';
                }

                div = '<ul><li class="dropdown note1 ad_user"><div class="btn-group">' +
                    '<button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> ' +
                    '<i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button>' +
                    ' <ul class="dropdown-menu pull-right">' + value1 + value2 + '</ul></li></ul>';

            }
            return div;
        }

        // searchbox filter
        srv.gridDocumentsSearchh = function () {
            var searchValue = $('#searchBox').val();
            $("#gridDocumentsCP").data("kendoGrid").dataSource.filter({
                logic: "or",
                filters: [
                  {
                      field: "Title",
                      operator: "contains",
                      value: searchValue
                  },
                  {
                      field: "ConceptTitle",
                      //  operator:  (value) => (value + "").indexOf(searchValue) >= 0,
                      operator: "contains",
                      value: searchValue
                  },
                     {
                         field: "CategoryTitle",
                         // operator:  (value) => (value + "").indexOf(searchValue) >= 0,
                         operator: "contains",
                         value: searchValue
                     },
                  {
                      field: "Name",
                      operator: "contains",
                      value: searchValue
                  },                
                ]
            });
        }
        // clear grid filter
        srv.gridDocumentsSearchClearr = function () {
            $('#searchBox').val('');
            $("#gridDocumentsCP").data('kendoGrid').dataSource.filter({
            });
        }

        function AttachmentTemplateGlobal(dataItem) {
            var Div = '';
            if (dataItem.Stream_id) {
                Div = "<a href='/api/Download?streamid=" + dataItem.Stream_id + "' target=\"_blank\" style='color: rgb(61,125,139);'>" + dataItem.Document + "</a> ";
            }
            else {
                Div = " + dataItem.Document + ";
            }
            return Div;
        }

        srv.GetMenu = function (note) {
            var x = note;
            return `<div><ul>
                        <li class ="dropdown note1 ad_user">
                            <div class ="btn-group">
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <i class ="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i>
                                </button>
                                <ul class ="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0)"  ng-click="NoteServiceTP.EditNoteTP($index)">Print</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul></div>`;
        }
    }
        ])
    // This directive is specific to Note/attachment dropzone configuration
    // and event binding and tightly bound with NoteServiceTP.
    // WARNING: It will not work with in any other situation!!!!!!
    .directive('hfcDropzoneNote', ['DropzoneService', 'NoteServiceTP'
    , function (DropzoneService, NoteServiceTP) {
        return function (scope, element, attrs) {
            //var config;

            //config = scope[attrs.dropzone];
            //confignew = scope[attrs.dropzonetouchpoint];
            var confignew = NoteServiceTP.DropzoneOption

            // create a Dropzone for the element with the given options
            //var dropzone = new Dropzone(element[0], config.options);
            var dropzonenew = new Dropzone(element[0], confignew.options)

            // bind the given event handlers
            //for (var event in config.eventHandlers) {
            //    dropzone.on(event, config.eventHandlers[event]);
            //};

            for (var event in confignew.eventHandlers) {
                dropzonenew.on(event, confignew.eventHandlers[event]);
            };

            //we will require an id for each dropzone so we can retrieve it later to insert existing files
            //DropzoneService.Dropzones.push({ id: attrs.id || "", dropzone: dropzone });
            DropzoneService.Dropzones.push({ id: attrs.id || "", dropzone: dropzonenew });
        };
    }])
    .directive('hfcNoteList', [
        'NoteServiceTP', function (NoteServiceTP) {
            return {
                restrict: 'E',
                scope: {
                    modalId: '@',
                    isEdit:'@'
                },
                templateUrl: '/templates/NG/LinkedList/Hfc-note-list.html',
                replace: true,
                link: function (scope) {
                    scope.IsEdit = true;
                    if (scope.isEdit === "true") {
                        scope.IsEdit = true;
                    }
                    else {
                        scope.IsEdit = false;
                    }
                    scope.NoteServiceTP = NoteServiceTP;
                }
            }
        }
    ])
    .directive('hfcNoteModal', [
            'NoteServiceTP', function (NoteServiceTP) {
                return {
                    restrict: 'E',
                    scope: {
                        modalId: '@'
                    },
                    templateUrl: '/templates/NG/LinkedList/Hfc-note-modal.html',
                    replace: true,
                    link: function (scope) {
                        scope.NoteServiceTP = NoteServiceTP;
                    }
                }
            }
    ]);
})(window, document);