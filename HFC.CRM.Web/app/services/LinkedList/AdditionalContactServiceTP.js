﻿'use strict';
app.service('AdditionalContactServiceTP', ['$http', 'HFCService', 'CacheService', '$rootScope', '$anchorScroll',
    function ($http, HFCService, CacheService, $rootScope, $anchorScroll) {
        var srv = this;

        srv.showAddoption = false;
        srv.showEditoption = false;

        // Unless otherwise specified the component assume that the parent is Lead.
        srv.ParentName = "Lead";
        srv.IsBusy = false;

        srv.submitnte = false;
        // On which mode the current *additional contact* service operates. The default is Add mode.
        // When called from the grid row for edit, value will be reset to Edit.
        srv.Mode = "Add";

        // Contain list of additional contacts in the currrent context, example if the current view
        // is Lead, then the list will contain contacts to that specific Lead. 
        srv.ContactList = [];

        // Represent the current contact in the context. it may be new contact or existing
        // contact in edit mode

        srv.ContactModal = {
            Index: '',
            //SelectedCategory: {id:0},
            ContactId: 0,
            FirstName: '',
            LastName: '',
            CompanyName: '',
            Title: '', // WorkTitle in DB.
            HomePhone: '',
            CellPhone: '',
            WorkPhone: '',
            WorkPhoneExt: '',
            Email: '', // PrimaryEmail in DB.
            SecondaryEmail: '',
            PreferredPhone: 0, // PreferredTFN in DB
            RecieveCorporateEmailBlast: 0 //, //IsReceiveEmails
        };
        srv.Contact = angular.copy(srv.ContactModal);

        // The following ids should be populated from the consumer, for example LeadController
        // AdditionalContact service required these information based on these only it can associate with either 
        // Lead or Account or Opportunity.
        // TODO: The best place is actually the view page (of Lead) where the Additional contact custom tag is used.
        //srv.LeadId, srv.AccountId, srv.OpportunityId

        srv.ContactIndex = -1;

        // this id is used to contain the controls for the additional contact popup modal.
        srv.divId = "_addtnlContactModal_";

        // Canel the operation or window.
        srv.Cancel = function () {
            srv.submitnte = false;
            srv.Contact = angular.copy(srv.ContactModal);
            $("#" + srv.divId).modal("hide");
            var myDiv = $('.modal-body');
            myDiv[0].scrollTop = 0;
        };

        function getModelForPost() {
            var model = {
                CustomerId: srv.Contact.ContactId,
                FirstName: srv.Contact.FirstName,
                LastName: srv.Contact.LastName,
                CompanyName: srv.Contact.CompanyName,
                WorkTitle: srv.Contact.Title,
                HomePhone: srv.Contact.HomePhone,
                CellPhone: srv.Contact.CellPhone,
                WorkPhone: srv.Contact.WorkPhone,
                WorkPhoneExt: srv.Contact.WorkPhoneExt,
                PrimaryEmail: srv.Contact.Email,
                SecondaryEmail: srv.Contact.SecondaryEmail,
                PreferredTFN: srv.Contact.PreferredPhone,
                IsReceiveEmails: srv.Contact.RecieveCorporateEmailBlast,
                LeadId: srv.LeadId,
                AccountId: srv.AccountId,
                OpportunityId: srv.OpportunityId,
                IsDeleted: srv.IsDeleted
                
            };

            switch (srv.ParentName) {
                case 'Lead':
                    model.AssociatedSource = 1;
                    break;
                case 'Account':
                    model.AssociatedSource = 2;
                    break;
                case 'Opportunicty':
                    model.AssociatedSource = 3;
                    break;
                default:
                    model.AssociatedSource = 1;

            }

            return model;
        }

        
        srv.deleteYes = function () {

            var url = '/api/additionalContact/' + deleteContactId;
            $http.delete(url).then(function (response) {
                 

                HFC.DisplayAlert(response.data);
                populateContactListFromDatabase();
            }, function (response) {

                
            });

            $("#" + deleteDiv).modal("hide");
            deleteContactId = 0
        }

        srv.deleteCancel = function () {
            deleteContactId = 0
            $("#" + deleteDiv).modal("hide");
        }

        var deleteDiv = "__delete__contact";

        var deleteContactId = 0;
        srv.Delete = function (contactid) {
            deleteContactId = contactid;
             

            var url = '/api/additionalContact/' + contactid + '/CanbeDeleted';

            $http.get(url).then(function (response) {
                 

                if (response.data == false) {
                    HFC.DisplayAlert("This Contact is related to one of the Additional Addresses. You must remove it from the Address prior to deleting it.");
                    return
                }

                // Display the dialog to get the approval from user before deleting.
                $("#" + deleteDiv).modal("show");
                

                //populateContactListFromDatabase();
            }, function (response) {
                deleteContactId = 0;
                srv.IsBusy = false;
            });
        }

        // By default deleted notes are not included.
        var includeDeleted = false;

        srv.RefreshGrid = function (showDeleted) {
            includeDeleted = showDeleted == true ? true : false;
            srv.IsBusy = true;
            populateContactListFromDatabase();
            srv.IsBusy = false;
        }

        srv.Recover = function (contactid) {
             
            var recoverurl = '/api/additionalContact/' + contactid + '/Recover';
            $http.post(recoverurl).then(function (response) {
                HFC.DisplaySuccess("Recover success");
                populateContactListFromDatabase();
                srv.IsBusy = false;
            }, function (response) {

                HFC.DisplayAlert(response.statusText);
                srv.IsBusy = false;
            })
        }

        // Save or update the additional contact to the database
        srv.SaveContactTP = function (model) {
            srv.submitnte = true;
            // TODO: validation yet to be done.
            if (model.Pop_AddtnlContact.$invalid) {
                return;
            }

            var promise = null;

            var data = getModelForPost();
            //warning for duplicate email
            if (!(data.PrimaryEmail == "" && data.SecondaryEmail == "") && !(data.PrimaryEmail == undefined && data.SecondaryEmail == undefined) && (data.PrimaryEmail === data.SecondaryEmail))
            {
                HFC.DisplayAlert("Secondary Email should not be same as Primary Email");
                return;
            }

            var tfn = data.PreferredTFN;

            // All the three phone is not entered do not display any message, just return.
            //if (!data.WorkPhone && !data.HomePhone && !data.CellPhone) {
            //    HFC.DisplayAlert("One of the Phone is Required");
            //    return;
            //}

            //if (!(tfn == "C" || tfn == "H" || tfn == "W")) {
            //    HFC.DisplayAlert("Please select one phone as Preferred");
            //    return;
            //}

            //if (tfn == "W" && !data.WorkPhone) {
            //    HFC.DisplayAlert("Work Phone can't be selected as Preferred phone");
            //    return;
            //}

            //if (tfn == "H" && !data.HomePhone) {
            //    HFC.DisplayAlert("Home Phone can't be selected as Preferred phone");
            //    return;
            //}

            //if (tfn == "C" && !data.CellPhone) {
            //    HFC.DisplayAlert("Cell Phone can't be selected as Preferred phone");
            //    return;
            //}

            var myDiv = $('.modal-body');
            myDiv[0].scrollTop = 0;
           

            if (srv.Mode == "Add") {
                // Add to the Database.
                promise = $http.post('/api/additionalContact/0/AddNewContact/', data);

            } else if (srv.Mode == "Edit") {
                if (!data.CustomerId || data.CustomerId <= 0) {
                    HFC.DisplayAlert("Customer ID is missing. Update failed.");
                    return;
                }

                // update to the database.
                promise = $http.post('/api/additionalContact/0/UpdateContact', data);
            }

            promise.then(function (response) {

                HFC.DisplaySuccess("Contact Saved.");
                srv.IsBusy = false;
                $("#" + srv.divId).modal("hide");
                
                // Repopulate the collection.
                    populateContactListFromDatabase();

            }, function (response) {

                HFC.DisplayAlert(response.statusText);
                srv.IsBusy = false;

            });
            
        }

        srv.AddContactTP = function () {
            
            srv.submitnte = false;
            srv.Mode = "Add";
            srv.ContactIndex = -1;
            srv.Contact = {};
            $("#" + srv.divId).modal("show");
        }

        srv.EditContactTP = function (rowIndex) {
            
            srv.submitnte = false;
            srv.Mode = "Edit";
            handleDisplayAndEdit(rowIndex);
        }
        //global search
        srv.EditGlobalContactTP = function (CustomerId) {             
            srv.ParentName = "GlobalSearch";
            srv.submitnte = false;
            srv.Mode = "Edit";
            var result = $http.get('/api/AdditionalContact/' + CustomerId + '/GetGlobalContactss').then(function (responseSuccess) {
                srv.ContactList = responseSuccess.data;
                handleDisplayAndEdit(0);
                //$rootScope.$broadcast("RefreshContactsGrid", "true");
            }, function (responseError) {
                HFC.DisplayAlert(responseError.statusText);
            });
        }

        srv.DisplayContactTP = function (rowIndex) {
            
            srv.submitnte = false;
            srv.Mode = "Display";
            handleDisplayAndEdit(rowIndex);
        }

        srv.Initialize = function () {
            includeDeleted = false;
            srv.ContactList = [];
            srv.Contact = {};

            srv.IsBusy = true;

            populateContactListFromDatabase();

            srv.IsBusy = false;

        }


        function handleDisplayAndEdit(rowIndex) {             
            srv.ContactIndex = rowIndex;
            srv.Contact = {};
            var existingContact = srv.ContactList[rowIndex];
            srv.Contact.ContactId = existingContact.CustomerId;
            srv.Contact.FirstName = existingContact.FirstName;
            srv.Contact.LastName = existingContact.LastName;
            srv.Contact.CompanyName = existingContact.CompanyName;
            srv.Contact.Title = existingContact.WorkTitle;
            srv.Contact.HomePhone = existingContact.HomePhone;
            srv.Contact.CellPhone = existingContact.CellPhone;
            srv.Contact.WorkPhone = existingContact.WorkPhone;
            srv.Contact.WorkPhoneExt = existingContact.WorkPhoneExt;
            srv.Contact.Email = existingContact.PrimaryEmail;
            srv.Contact.SecondaryEmail = existingContact.SecondaryEmail;
            srv.Contact.PreferredPhone = existingContact.PreferredTFN;
            srv.Contact.RecieveCorporateEmailBlast = existingContact.IsReceiveEmails;
            //srv.Contact.IsDeleted = existingContact.IsDeleted;
             
            $("#" + srv.divId).modal("show");
        }

        function populateContactListFromDatabase() {

            if (srv.ParentName == "GlobalSearch") {
                $rootScope.$broadcast("RefreshContactsGrid", "true");
                return;
            }
            var geturl = getSourceUrl();
            $http.get(geturl).then(function (responseSuccess) {
                 
                srv.ContactList = responseSuccess.data;
            }, function (responseError) {
                HFC.DisplayAlert(responseError.statusText);
            });
        }

        function getSourceUrl() {

            var temp = includeDeleted == true ? true : false;
            if (srv.ParentName == "Lead") {
                return '/api/AdditionalContact/' + srv.LeadId + '/GetLeadContacts/?showDeleted=' + includeDeleted;
            }

            if (srv.ParentName == "Account") {
                return '/api/AdditionalContact/' + srv.AccountId + '/GetAccountContacts/?showDeleted=' + includeDeleted;
            }

            //if (srv.ParentName == "Opportunity") {
            //    return '/api/AdditionalContact/' + srv.OpportunityId + '/GetOpportunityContacts/';
            //}

            // Display to the user that invalid parameter is set:
            HFC.DisplayAlert("Invalid Entity name : " + srv.ParentName + " set from the called controller");
        }

        srv.IsBusy = false;
    }
])
    .directive('hfcAdditionalContact', [
        'AdditionalContactServiceTP', function (AdditionalContactServiceTP) {
            return {
                restrict: 'E',
                scope: {
                    modalId: '@',
                    globalSearch: '@',
                    isEdit:'@'
                },
                templateUrl: '/templates/NG/LinkedList/Hfc-additional-contact.html',
                replace: true,
                link: function (scope) {
                    scope.IsEdit = true;
                    scope.AdditionalContactServiceTP = AdditionalContactServiceTP;
                    if (scope.isEdit === "true") {
                        scope.IsEdit = true;
                    }
                    else {
                        scope.IsEdit = false;
                    }
                }
            }
        }
    ]);

