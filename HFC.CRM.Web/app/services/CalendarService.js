﻿
(function (window, document, undefined) {
    'use strict';
    angular.module('hfc.calendarModule', []) // ['hfc.core.service', 'hfc.accountModule', 'hfc.leadModule', 'hfc.leadaccountModule'])
        .service('CalendarService', [
    '$http', '$window', 'HFCService', 'AddressService', 'AccountService', 'LeadService', 'LeadAccountService', '$anchorScroll',
    function ($http, $window, HFCService, AddressService, AccountService, LeadService, LeadAccountService, $anchorScroll) {
        // 
        var srv = this,



            numbers = [];

        srv.AccountService = AccountService;
        srv.AddressService = AddressService;
        srv.leadService = LeadService;
        srv.leadAccountService = LeadAccountService;


        for (var i = 1; i <= 31; i++)
            numbers.push(i);

        srv.IsBusy = false;
        srv.NumbersArray = numbers;
        srv.RelativeGroup = 'Day';
        srv.EndSelection = 'Never';
        srv.IncludeJobLead = false;
        srv.RecurrenceIsDisabled = false; //we may want to disable recurrence for modified split occurrence in series
        srv.CachedJobIds = [];
        srv.CalendarEvents = [];
        srv.Tasks = [];
        srv.AccountPhoneNumber = null;
        srv.LeadPhoneNumber = null;
        srv.EventIndex = -1;
        srv.AppointmentTypeEnum = null;
        srv.RecurringPatternEnum = null;
        srv.DayOfWeekEnum = null; //this is a special day of week enum, since it can be used as flag the values are not sequential
        srv.DayOfWeekIndexEnum = null;
        srv.EventTypeEnum = null;
        srv.RecurrsEvery = null;
        srv.TypeAppointments = [];
        srv.IsPrivate = null;
        srv.AccountNumber = null;
        //Added To display the Save And Convert Lead Button
        srv.IsConvertLead = false;
        srv.minDate = new Date();
        srv.maxDate = new Date('2099/12/31');
        srv.ViewOpportunity = null;
        srv.ViewAccountLead = null;
        function error(res) {
            srv.IsBusy = false;
            HFC.DisplayAlert(res.statusText);
        };


        srv.viewvendorcase = false;
        srv.setVendorcasevisible = function () {
            srv.viewvendorcase = true;
        }

        srv.setVendorcaseinvisible = function () {
            srv.viewvendorcase = false;
        }

        // The following are provided by Kannan, in order to fix the calendar issues.
        srv.locationfromAddress = function (address) {

            var location = "";
            if (typeof address == "string") { location = address }
            else if (address != null) {
                location = address.Address1 != '' && address.Address1 != null ? address.Address1 : "";
                location += address.Address2 != '' && address.Address2 != null ? ' ' + address.Address2 : "";
                location += address.City != '' && address.City != null ? ' ' + address.City : "";
                location += address.State != '' && address.State != null ? ' ' + address.State : "";
                location += address.ZipCode != '' && address.ZipCode != null ? ' ' + address.ZipCode : "";
            } else {
                location = '';
            }

            return location;
        }

        srv.EditableEvent = {};
        // To reset all the dropdown
        srv.resetLOV = function () {
            srv.selectedLeadId = null;
            //if(srv.EditableEvent)
            srv.EditableEvent = {};
            srv.AccountPhoneNumber = null;
            srv.LeadPhoneNumber = null;
            srv.EditableEvent.RecurrenceIsEnabled = false;
            srv.EditableRecurringEvent.recurrancepattern = 1;
            srv.EditableEvent.start = new Date();
            srv.EditableRecurringEvent.endBy = 1;
            srv.EditableEvent.end = new Date();




            srv.EditableEvent.id = null;
            srv.selectedAccountId = null;
            srv.selectedOpportunityId = null;
            srv.selectedOrderId = null;
            srv.selectedCaseId = null;
            srv.selectedVendorCaseId = null;
            //srv.EditableRecurringEvent = {};
            srv.recurrencesummary = null;



            var opportunityDrpDown = $("#Opportunity").data("kendoComboBox");
            opportunityDrpDown.dataSource.read();
            // opportunityDrpDown.text('Select');
            var orderDrpDown = $("#Order").data("kendoComboBox");
            orderDrpDown.dataSource.read();
            // orderDrpDown.text('Select');
        }

        //This is a new function - Generic New Appointment function
        srv.CreateApt = function (modalId, Subject, address, leadId, accountId, opportunityId, orderId, caseId, start, isTaskOrEvent, vendorcaseId) {
            
            srv.resetLOV();
            srv.IsTaskCheck = false;
            if (isTaskOrEvent === 'task') {
                srv.IsTaskCheck = true;
                srv.EditableEvent.AptTypeEnum = '';

            }
            //Initialize controls
            srv.EditableEvent.title = Subject;
            srv.EditableEvent.Location = srv.locationfromAddress(address);
            if (leadId && leadId > 0) {
                var leadList = $("#Lead").data("kendoComboBox");
                if (leadList) {
                    var lead = JSLINQ(leadList.dataSource._data).Where(function (item) { return item.LeadId == leadId });
                    if (lead) {
                        srv.LeadPhoneNumber = lead.items[0].PhoneNumber;
                    }
                }

                srv.selectedLeadId = leadId;
            }
            else {
                srv.selectedLeadId = 0;

            }
            
            if (accountId && accountId > 0) {
                srv.selectedAccountId = accountId;
                var accountList = $("#Account").data("kendoComboBox");
                if (accountList) {
                    var account = JSLINQ(accountList.dataSource._data).Where(function (item) { return item.AccountId == accountId });
                    if (account) {
                        srv.AccountPhoneNumber = account.items[0].PhoneNumber;
                    }
                }

                var opportunityDrpDown = $("#Opportunity").data("kendoComboBox");

                opportunityDrpDown.dataSource.read();
                var orderDrpDown = $("#Order").data("kendoComboBox");
                orderDrpDown.dataSource.read();

                var Casedd = $("#Case").data("kendoComboBox");
                Casedd.dataSource.read();

            }
            else {
                //$("#Account").data("kendoComboBox").text('Select');
                srv.selectedAccountId = 0;
            }
            
            if (opportunityId && opportunityId > 0) {
                srv.selectedOpportunityId = opportunityId;
                var orderDrpDown = $("#Order").data("kendoComboBox");
                orderDrpDown.dataSource.read();

                var Casedd = $("#Case").data("kendoComboBox");
                Casedd.dataSource.read();
            }
            else {
                srv.selectedOpportunityId = 0;

            }
            if (orderId && orderId > 0) {
                srv.selectedOrderId = orderId;
                var Casedd = $("#Case").data("kendoComboBox");
                Casedd.dataSource.read();
            }
            else {
                srv.selectedOrderId = 0;

            }
            if (caseId && caseId > 0) {

                srv.selectedOpportunityId = opportunityId;
                srv.selectedOrderId = orderId;
                srv.selectedCaseId = caseId;
            }
            else {
                srv.selectedCaseId = 0;

            }
            //added for vendor case
            if (vendorcaseId && vendorcaseId > 0) {

                srv.selectedOpportunityId = opportunityId;
                srv.selectedOrderId = orderId;
                srv.selectedCaseId = caseId;
                srv.selectedVendorCaseId = vendorcaseId;
            }
            else {
                srv.selectedVendorCaseId = 0;

            }
            srv.selectedIds = [HFCService.CurrentUser.Email];
            srv.EditableEvent.Message = '';

            srv.getAppointmentTypeEnum();
            srv.getReminder();

            srv.EditableEvent.allDay = false;
            //srv.EditableEvent.end = srv.EditableEvent.start; // Must add 1 hour
            srv.SetStartAndEndDateInAppointment(start);

            srv.EditableEvent.ReminderMinute = 15; //Is this how we make the control blank      
            
            $("#kendoComboboxLead").data("kendoComboBox").dataSource.read();
            $("#kendoComboboxAccount").data("kendoComboBox").dataSource.read();

            $("#persongo").modal("show");

            

            $anchorScroll();
            srv.ShowEventRecurringPanel(false);
        };

        // For use from locations without a context such as lead, account, opportunity, or order
        srv.NewApt = function (modalId, startDate, type, path, id, id2) {
            //srv.EditableRecurringEvent = {};
            srv.returntoSectionDisplay = null;
            if (!(type == undefined || type == "Lead" || type == "Qualify" || type == "Lead-Appointment" || type == "Lead-Communication")) {
                srv.returntoSectionDisplay = 'Save And Return To' + ' ' + type;
                srv.pathToReturn = path;
                srv.sectionId = id;
                if (id2) {
                    srv.sectionId2 = id2;
                }
            }
            if (srv.EditableEvent)
                srv.EditableEvent.AptTypeEnum = '';
            srv.IsRecur = false;
            // Lead
            if (type == "Lead" || type == "Qualify" || type == "Lead-Appointment" || type == "Lead-Communication" || (type == "Leads-Search" && id != null && id != "" && id != undefined)) {
                srv.NewLeadApt(modalId, id, 'event', startDate);
            }
                // Account
            else if ((type == "Account-Search" && id != null && id != "" && id != undefined) || type == "Account" || type == "Account-Opportunity List" || type == "Account-Order List" || type == "Account-Payment" || type == "Account-Appointments" || type == "Account-Communication" || type == "Account-Measurements") {
                srv.NewAccountApt(modalId, id, 'event', startDate);
            }
                // Opportunity
            else if ((type == "Opportunity-Search" && id != null && id != "" && id != undefined) || type == "Opportunity-Measurement" || type == "Opportunity-Appointments" || type == "Opportunity-Communication" || type == "Opportunity" || type == "Quote") {
                srv.NewOpportunityApt(modalId, id, 'event', startDate);
            }
                // Order
            else if (type == "Order") {
                srv.NewOrderApt(modalId, id, 'event', startDate);
            }
            else if (type == "CaseView") {
                srv.NewCaseApt(modalId, id, 'event', startDate);
            }
            else if (type == "VendorcaseView") {
                
                srv.NewVendorCaseApt(modalId, id, 'event', startDate);
            }
            else {
                srv.CreateApt(modalId, '', null, null, null, null, null, null, startDate, 'event', null);
            }
        }



        function errorHandler(reseponse) {


            // Inform that the modal is ready to consume:
            $scope.modalState.loaded = true;
            HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
        }

        srv.NewLeadApt = function (modalId, leadId, isTaskEvent, startDate) { //person, address, leadid) {
            // 
            //Added To display the Save And Convert Lead Button
            srv.IsConvertLead = true;
            srv.leadService.Get(leadId, function (result) {
                var lead = result.Lead;
                var person = lead.PrimCustomer;
                var address = lead.Addresses[0];
                if (lead.IsCommercial)
                    var fullName = person ? (person.CompanyName) : "";
                else
                    var fullName = person ? (person.FullName + (person.CompanyName != null && person.CompanyName != "" ? " / " + person.CompanyName : "")) : "";

                if (srv.EditableEvent)
                    srv.EditableEvent.AptTypeEnum = 1;

                if (lead.LeadStatusId === 13) leadId = 0;
                srv.CreateApt(modalId, fullName, address, leadId, null, null, null, null, startDate, isTaskEvent, null);
            }, errorHandler);
        }



        srv.NewAccountApt = function (modalId, accountId, isTaskEvent, startDate) {


            srv.leadAccountService.Get(accountId, function (result) {
                var account = result.Account;
                var person = account.PrimCustomer;
                var address = account.Addresses[0];
                var fullName = "";
                if (account.IsCommercial)
                    var fullName = person ? (person.CompanyName) : "";
                else
                    var fullName = person ? (person.FullName + (person.CompanyName != null && person.CompanyName != "" ? " / " + person.CompanyName : "")) : "";
                if (srv.EditableEvent)
                    srv.EditableEvent.AptTypeEnum = 6;
                srv.CreateApt(modalId, fullName, address, null, accountId, null, null, null, startDate, isTaskEvent, null);
            }, errorHandler);
        }


        srv.NewOpportunityApt = function (modalId, opportunityId, isTaskEvent, startDate) {


            $http.get('/api/Opportunities/' + opportunityId)
                .then(function (result) {
                    var opportunity = result.data.Opportunity;
                    var person = opportunity.PrimCustomer;
                    var address = opportunity.Addresses[0];
                    if (opportunity.IsCommercial)
                        var fullName = person ? (person.CompanyName) : "";
                    else
                        var fullName = person ? (person.FullName + (person.CompanyName != null && person.CompanyName != "" ? " / " + person.CompanyName : "")) : "";

                    if (srv.EditableEvent)
                        srv.EditableEvent.AptTypeEnum = 5;
                    srv.CreateApt(modalId, fullName, address, null, opportunity.AccountId, opportunity.OpportunityId, null, null, startDate, isTaskEvent, null);
                }, errorHandler);
        }




        srv.NewOrderApt = function (modalId, orderId, isTaskEvent, startDate) {
            
            $http.get('/api/Orders/' + orderId)
               .then(function (result) {
                   var order = result.data;
                   // var person = order.PrimCustomer;
                   //var address = order.InstallationAddress;
                   var address = '';
                   if (order.InstallAddress) {
                       if (order.InstallAddress.Address1) address += order.InstallAddress.Address1 + ' ';
                       if (order.InstallAddress.Address2) address += order.InstallAddress.Address2 + ' ';
                       if (order.InstallAddress.City) address += order.InstallAddress.City + ' ';
                       if (order.InstallAddress.State) address += order.InstallAddress.State + ' ';
                       if (order.InstallAddress.ZipCode) address += order.InstallAddress.ZipCode;
                   }
                   var fullName = order ? (order.OrderName) : "";
                   if (srv.EditableEvent)
                       srv.EditableEvent.AptTypeEnum = 5;
                   srv.CreateApt(modalId, fullName, address, null, order.AccountId, order.OpportunityId, order.OrderID, null, startDate, isTaskEvent, null);
               }, errorHandler);
        }
        srv.NewCaseApt = function (modalId, caseId, isTaskEvent, startDate) {
            //
             
            $http.get('/api/CaseManageAddEdit/' + caseId + '/getAccOrderOpportunity')
             .then(function (result) {
                 if (result.data) {
                     var MPO = result.data;
                     

                     srv.CreateApt(modalId, "", "", null, MPO.AccountId, MPO.OpportunityId, MPO.OrderId, caseId, startDate, isTaskEvent, null);
                 }
             });
        }

        //added for vendor case
        srv.NewVendorCaseApt = function (modalId, vendorcaseId, isTaskEvent, startDate) {
             
            $http.get('/api/CaseManageAddEdit/' + vendorcaseId + '/getCompleteDetails')
             .then(function (result) {
                 if (result.data) {
                     var Vendorcase = result.data;
                     

                     srv.CreateApt(modalId, "", "", null, Vendorcase.AccountId, Vendorcase.OpportunityId, Vendorcase.OrderId, null, startDate, isTaskEvent, vendorcaseId);
                 }
             });
        }

        //Leads 
        //srv.selectedLeadIds = [];
        // 
        srv.selectLeadOptions = null;

        srv.selectLeadOptions = {
            placeholder: "Select",
            dataTextField: "FullName",
            dataValueField: "LeadId",
            valuePrimitive: true,
            height: 300,
            autoBind: true,
            dataSource: {
                transport: {
                    read: {
                        url: '/api/TaskEvents/0/GetLeadsList',
                        dataType: "json"

                    }
                }
            }
        };

        //Attendees
        srv.selectedIds = [];
        srv.selectOptions = null;
        srv.selectOptions = {
            placeholder: "Select",
            dataTextField: "FullName",
            dataValueField: "Email",
            valuePrimitive: true,
            height: 300,
            autoBind: true,
            dataSource: {
                transport: {
                    read: {
                        url: '/api/lookup/1/GetAttendees',
                        dataType: "json"
                    }
                }
            }
        };

        //Account List
        srv.selectAccountOptions = {
            placeholder: "Select",
            dataTextField: "FullName",
            dataValueField: "AccountId",
            valuePrimitive: true,
            autoBind: true,
            dataSource: {
                transport: {
                    read: {
                        url: '/api/TaskEvents/0/GetAccountList',
                        dataType: "json"
                    }
                }
            }
        };

        //Opportunity List
        //srv.selectedOpportunityIds = [];
        srv.selectOpportunityOptions = null;
        srv.selectOpportunityOptions = {

            placeholder: "Select",
            dataTextField: "FullName",
            dataValueField: "OpportunityId",
            valuePrimitive: true,
            autoBind: true,
            dataSource: {
                transport: {
                    read: {
                        url: function (params) {
                            if (!srv.selectedAccountId) { //All opportunities for the FE
                                var url1 = '/api/TaskEvents/0/GetOpportunityList';
                                dataType: "json";
                                return url1;
                            }
                            else { // All opportunities for the Account
                                var url1 = '/api/TaskEvents/' + srv.selectedAccountId + '/GetOpportunityListByAccount';
                                dataType: "json";
                                return url1;
                            }
                        }
                    }
                }
            }
        };


        //Order List
        //srv.selectedOrderIds = [];
        srv.selectOrderOptions = null;
        srv.selectOrderOptions = {
            placeholder: "Select",
            dataTextField: "FullName",
            dataValueField: "OrderId",
            valuePrimitive: true,
            autoBind: true,
            dataSource: {
                transport: {
                    read: {
                        url: function (params) {
                            
                            if (srv.selectedOpportunityId) { // Order for the Opportunity
                                var url1 = '/api/TaskEvents/' + srv.selectedOpportunityId + '/GetOrderListByOpportunity';
                                dataType: "json";
                                return url1;
                            } else if (srv.selectedAccountId) {
                                var url1 = '/api/TaskEvents/' + srv.selectedAccountId + '/GetOrderListByAccount';
                                dataType: "json";
                                return url1;
                            } else { // All Orders for the FE
                                var url1 = '/api/TaskEvents/0/GetOrderList';
                                dataType: "json";
                                return url1;
                            }
                        }
                    }
                }
            }
        };


        //Order List
        //srv.selectedOrderIds = [];
        srv.selectCaseOptions = null;
        srv.selectCaseOptions = {
            placeholder: "Select",
            dataTextField: "CaseNumber",
            dataValueField: "CaseId",
            valuePrimitive: true,
            autoBind: true,
            dataSource: {
                transport: {
                    read: {
                        url: function (params) {
                            
                            if (srv.selectedOrderId) {
                                var url1 = '/api/TaskEvents/' + srv.selectedOrderId + '/GetCaseList?type=order';
                                dataType: "json";
                                return url1;
                            }
                            else if (srv.selectedOpportunityId) {
                                var url1 = '/api/TaskEvents/' + srv.selectedOpportunityId + '/GetCaseList?type=opportunity';
                                dataType: "json";
                                return url1;
                            } else if (srv.selectedAccountId) {
                                var url1 = '/api/TaskEvents/' + srv.selectedAccountId + '/GetCaseList?type=account';
                                dataType: "json";
                                return url1;
                            }
                            else {
                                var url1 = '/api/TaskEvents/0/GetCaseList?type=case';
                                dataType: "json";
                                return url1;
                            }
                        }
                    }
                }
            }
        };



        //added for vendor case
        srv.selectedVendorCaseoptions = null;
        srv.selectedVendorCaseoptions = {
            placeholder: "Select",
            dataTextField: "CaseNumber",
            dataValueField: "CaseId",
            valuePrimitive: true,
            autoBind: true,
            dataSource: {
                transport: {
                    read: {
                        url: function (params) {
                            
                            if (srv.selectedOrderId) {
                                var url1 = '/api/TaskEvents/' + srv.selectedOrderId + '/GetVendorCaseList?type=order';
                                dataType: "json";
                                return url1;
                            }
                            else if (srv.selectedOpportunityId) {
                                var url1 = '/api/TaskEvents/' + srv.selectedOpportunityId + '/GetVendorCaseList?type=opportunity';
                                dataType: "json";
                                return url1;
                            } else if (srv.selectedAccountId) {
                                var url1 = '/api/TaskEvents/' + srv.selectedAccountId + '/GetVendorCaseList?type=account';
                                dataType: "json";
                                return url1;
                            }
                            else if (srv.selectedCaseId) {
                                var url1 = '/api/TaskEvents/' + srv.selectedCaseId + '/GetVendorCaseList?type=case';
                                dataType: "json";
                                return url1;
                            }
                            else {
                                var url1 = '/api/TaskEvents/0/GetVendorCaseList?type=VendorCase';
                                dataType: "json";
                                return url1;
                            }
                        }
                    }
                }
            }
        };


        //Need to replace SetLeadAccountOpportunityOrder by the following functions that are called when the respective dropdowns are changed
        srv.LeadChanged = function (id) {
            var leadList = $("#Lead").data("kendoComboBox");

            srv.selectedAccountId = null;
            srv.selectedOpportunityId = null;
            srv.selectedOrderId = null;
            srv.selectedCaseId = null;
            srv.selectedVendorCaseId = null;
            srv.AccountPhoneNumber = null;

            if (leadList) {
                var lead = JSLINQ(leadList.dataSource._data).Where(function (item) { return item.LeadId == id });
                if (lead) {
                    srv.LeadPhoneNumber = lead.items[0].PhoneNumber;
                }
            }

            var VendorCasedd = $("#VendorCase").data("kendoComboBox");
            VendorCasedd.dataSource.read();

            var Casedd = $("#Case").data("kendoComboBox");
            Casedd.dataSource.read();


            var orderDrpDown = $("#Order").data("kendoComboBox");
            orderDrpDown.dataSource.read();

            var opportunityDrpDown = $("#Opportunity").data("kendoComboBox");
            opportunityDrpDown.dataSource.read();


        }
        srv.AccountChanged = function (id) {
            if (srv.selectedAccountId == 0 || !srv.selectedAccountId) {

                srv.selectedAccountId = null;
                srv.selectedOpportunityId = null;
                srv.selectedOrderId = null;
                srv.selectedCaseId = null;
                srv.selectedVendorCaseId = null;
                srv.AccountPhoneNumber = null;

                var VendorCasedd = $("#VendorCase").data("kendoComboBox");
                VendorCasedd.dataSource.read();

                var Casedd = $("#Case").data("kendoComboBox");
                Casedd.dataSource.read();

                var orderDrpDown = $("#Order").data("kendoComboBox");
                orderDrpDown.dataSource.read();

                var opportunityDrpDown = $("#Opportunity").data("kendoComboBox");
                opportunityDrpDown.dataSource.read();
            }
            else {
                console.log(srv.selectedAccountId);
                
                srv.selectedLeadId = null;
                srv.LeadPhoneNumber = null;
                var accountList = $("#Account").data("kendoComboBox");
                if (accountList) {
                    var account = JSLINQ(accountList.dataSource._data).Where(function (item) { return item.AccountId == id });
                    if (account) {
                        srv.AccountPhoneNumber = account.items[0].PhoneNumber;
                    }
                }

                srv.selectedOpportunityId = null;

                var opportunityDrpDown = $("#Opportunity").data("kendoComboBox");
                opportunityDrpDown.dataSource.read();

                srv.selectedOrderId = null;
                var orderDrpDown = $("#Order").data("kendoComboBox");
                orderDrpDown.dataSource.read();

                srv.selectedCaseId = null;
                var Casedd = $("#Case").data("kendoComboBox");
                Casedd.dataSource.read();

                srv.selectedVendorCaseId = null;
                var VendorCasedd = $("#VendorCase").data("kendoComboBox");
                VendorCasedd.dataSource.read();
            }

            //How do you force a rebind of order dropdown
        }

        srv.OpportunityChanged = function () {
            if (srv.selectedOpportunityId == 0 || !srv.selectedOpportunityId) {

                srv.selectedOpportunityId = null;
                srv.selectedOrderId = null;
                srv.selectedCaseId = null;
                srv.selectedVendorCaseId = null;

                var Casedd = $("#Case").data("kendoComboBox");
                Casedd.dataSource.read();


                var orderDrpDown = $("#Order").data("kendoComboBox");
                orderDrpDown.dataSource.read();

                var VendorCase = $("#VendorCase").data("kendoComboBox");
                VendorCase.dataSource.read();

            } else {

                srv.selectedLeadId = null;
                srv.LeadPhoneNumber = null;
                srv.selectedOrderId = null;
                srv.selectedCaseId = null;
                srv.selectedVendorCaseId = null;
                srv.selectedAccountId = null;

                var orderDrpDown = $("#Order").data("kendoComboBox");
                orderDrpDown.dataSource.read();

                var Casedd = $("#Case").data("kendoComboBox");
                Casedd.dataSource.read();

                var VendorCase = $("#VendorCase").data("kendoComboBox");
                VendorCase.dataSource.read();

                //reset the selected value of account
                $http.get('/api/Opportunities/' + srv.selectedOpportunityId)
                    .then(function (result) {
                        var opportunity = result.data.Opportunity
                        if (!srv.selectedAccountId)
                            srv.selectedAccountId = opportunity.AccountId;
                        if (!opportunity.PrimCustomer.PreferredTFN) {
                            if (opportunity.PrimCustomer.HomePhone != null) {
                                srv.AccountPhoneNumber = opportunity.PrimCustomer.HomePhone;
                            }
                            else if (opportunity.PrimCustomer.CellPhone != null) {
                                srv.AccountPhoneNumber = opportunity.PrimCustomer.CellPhone;
                            }
                            else if (opportunity.PrimCustomer.WorkPhone != null) {
                                srv.AccountPhoneNumber = opportunity.PrimCustomer.WorkPhone;
                            }

                        }
                        else {
                            if (opportunity.PrimCustomer.PreferredTFN == "H") {
                                srv.AccountPhoneNumber = opportunity.PrimCustomer.HomePhone;
                            }
                            else if (opportunity.PrimCustomer.PreferredTFN == "C") {
                                srv.AccountPhoneNumber = opportunity.PrimCustomer.CellPhone;
                            }
                            else if (opportunity.PrimCustomer.PreferredTFN == "W") {
                                srv.AccountPhoneNumber = opportunity.PrimCustomer.WorkPhone;
                            }
                        }
                    });

            }
        }

        srv.OrderChanged = function () {
            
            if (srv.selectedOrderId == 0 || !srv.selectedOrderId) {

                srv.selectedOrderId = null;
                srv.selectedCaseId = null;
                srv.selectedVendorCaseId = null;

                var Casedd = $("#Case").data("kendoComboBox");
                Casedd.dataSource.read();

                var VendorCase = $("#VendorCase").data("kendoComboBox");
                VendorCase.dataSource.read();
            } else {
                srv.selectedLeadId = null;
                srv.LeadPhoneNumber = null;
                //reset the selected value of opportunity and account
                srv.selectedCaseId = null;
                srv.selectedVendorCaseId = null;
                srv.selectedAccountId = null;
                srv.selectedOpportunityId = null;

                var Casedd = $("#Case").data("kendoComboBox");
                Casedd.dataSource.read();

                var VendorCase = $("#VendorCase").data("kendoComboBox");
                VendorCase.dataSource.read();

                $http.get('/api/Orders/' + srv.selectedOrderId)
                  .then(function (result) {
                      var order = result.data;

                      if (!srv.selectedAccountId)
                          srv.selectedAccountId = order.AccountId;

                      srv.AccountPhoneNumber = order.AccountPhone
                      if (!srv.selectedOpportunityId)
                          srv.selectedOpportunityId = order.OpportunityId;
                  });
            }

        }

        srv.CaseChanged = function () {
            srv.selectedLeadId = null;
            srv.LeadPhoneNumber = null;
            srv.selectedVendorCaseId = null;

            if (srv.selectedCaseId == 0 || !srv.selectedCaseId) {
                var Casedd = $("#Case").data("kendoComboBox");
                Casedd.dataSource.read();

                var VendorCase = $("#VendorCase").data("kendoComboBox");
                VendorCase.dataSource.read();
            } else {
                
                srv.selectedVendorCaseId = null;

                //reset the selected value of opportunity and account and order
                $http.get('/api/CaseManageAddEdit/' + srv.selectedCaseId + '/getAccOrderOpportunity')
                  .then(function (result) {
                      if (result.data) {
                          var MPO = result.data;
                          if (!srv.selectedAccountId)
                              srv.selectedAccountId = MPO.AccountId;

                          var opportunityDrpDown = $("#Opportunity").data("kendoComboBox");
                          opportunityDrpDown.dataSource.read();

                          var VendorCase = $("#VendorCase").data("kendoComboBox");
                          VendorCase.dataSource.read();

                          if (!srv.selectedOpportunityId)
                              srv.selectedOpportunityId = MPO.OpportunityId;

                          var orderDrpDown = $("#Order").data("kendoComboBox");
                          orderDrpDown.dataSource.read();

                          if (!srv.selectedOrderId)
                              srv.selectedOrderId = MPO.OrderId;

                          var accountList = $("#kendoComboboxAccount").data("kendoComboBox");
                          if (accountList) {
                              var account = JSLINQ(accountList.dataSource._data).Where(function (item) { return item.AccountId == srv.selectedAccountId });
                              
                              if (account) {
                                  srv.AccountPhoneNumber = account.items[0].PhoneNumber;
                              }
                          }
                      }
                  });




            }

        }


        //Vendor Case drop down changes
        srv.VendorCaseChanged = function () {
            srv.selectedLeadId = null;
            srv.LeadPhoneNumber = null;

            if (srv.selectedVendorCaseId == 0 || !srv.selectedVendorCaseId) {
                var VendorCasedd = $("#VendorCase").data("kendoComboBox");
                VendorCasedd.dataSource.read();
            } else {
                //reset the selected value of opportunity and account and order, case
                $http.get('/api/CaseManageAddEdit/' + srv.selectedVendorCaseId + '/getCompleteDetails')
                  .then(function (result) {
                      if (result.data) {
                          
                          var Vendata = result.data;
                          if (!srv.selectedAccountId)
                              srv.selectedAccountId = Vendata.AccountId;

                          var opportunityDrpDown = $("#Opportunity").data("kendoComboBox");
                          opportunityDrpDown.dataSource.read();

                          if (!srv.selectedOpportunityId)
                              srv.selectedOpportunityId = Vendata.OpportunityId;

                          var orderDrpDown = $("#Order").data("kendoComboBox");
                          orderDrpDown.dataSource.read();

                          if (!srv.selectedOrderId)
                              srv.selectedOrderId = Vendata.OrderId;

                          var Casedd = $("#Case").data("kendoComboBox");
                          Casedd.dataSource.read();

                          if (!srv.selectedCaseId)
                              srv.selectedCaseId = Vendata.CaseId;


                          var accountList = $("#kendoComboboxAccount").data("kendoComboBox");
                          if (accountList) {
                              var account = JSLINQ(accountList.dataSource._data).Where(function (item) { return item.AccountId == srv.selectedAccountId });
                              
                              if (account) {
                                  srv.AccountPhoneNumber = account.items[0].PhoneNumber;
                              }
                          }
                      }
                  });
            }
        }

        srv.getAppointmentTypeEnum = function () {
            $http.get('/api/lookup/0/Type_Appointments').then(function (data) {
                srv.AppointmentTypeEnum = data.data.AppointmentTypeEnum;

            });
        };

        srv.getReminder = function () {
            $http.get('/api/lookup/0/Reminders').then(function (data) {
                srv.Reminder = data.data;

            });
        };
        srv.dispositionId = null;
        srv.leadstatusid = null;
        srv.accountid = null;
        srv.opportunityid = null;

        srv.AccountId = null;
        srv.OpportunityId = null;
        srv.OrderId = null;

        srv.UpdateToDate = function () {

            var mydate = new Date(srv.EditableEvent.start);
            mydate.setMinutes(mydate.getMinutes() + 60);
            mydate = mydate.toLocaleString('en-US');
            mydate = mydate.replace(',', '')
            mydate = mydate.replace(/:\d{2}\s/, ' ');

            srv.EditableEvent.end = mydate;
            if (srv.EditableEvent.RecurrenceIsEnabled)
                srv.SetStartEndTime();
        }
        srv.UpdateToDateTP = function (datetimestr) {

            var datestr = datetimestr.split("T");
            var timestrarr = datestr[1].split(":");
            var datecombine = datestr[0] + 'T' + timestrarr[0] + ':' + timestrarr[1];
            return datecombine;
        }


        srv.IsModalLoad = false;
        srv.callbackForEvent = null;
        srv.callbackForTask = null;

        srv.GetEventsFromScheduler = function (modalId, CalendarId, callback) {
            // Store the callback to call after successful save.
            
            srv.callbackForEvent = callback;
            srv.IsTaskCheck = false;
            $("#" + modalId).modal("show");


            GetEvents(modalId, CalendarId);
        };

        function GetEvents(modalId, CalendarId) {

            if (CalendarId == 0)
                return;
            var data = {
                includeLookup: true,
                CalendarId: CalendarId,
                CalendarEdit: true
            };

            srv.RemoveRecurrenceEnable = false;
            var loadingElement = document.getElementById("loading");
            loadingElement.style.display = "block";
            $http.get('/api/calendar/', { params: data }).then(function (res) {

                srv.IsBusy = false;
                srv.IsRecur = false;
                srv.AppointmentTypeEnum = res.data.AppointmentTypeEnum;
                srv.RecurringPatternEnum = res.data.RecurringPatternEnum;
                srv.DayOfWeekEnum = res.data.DayOfWeekEnum;
                srv.DayOfWeekIndexEnum = res.data.DayOfWeekIndexEnum;
                srv.EventTypeEnum = res.data.EventTypeEnum;
                srv.Reminder = res.data.Reminder;
                srv.Recurrancepattern = res.data.Recurrancepattern;
                srv.DayoftheMonth = res.data.DayoftheMonth;
                srv.WeekOfTheMonth = res.data.WeekOfTheMonth;
                srv.DayOfWeek = res.data.DayOfWeek;
                srv.yearlyMontRepeatsOn = res.data.yearlyMontRepeatsOn;

                var evt = res.data.Events[0];

                
                srv.CalendarEvents.push(evt);
                srv.EditableEvent = evt;
                if (srv.EditableEvent) {
                    srv.AccountPhoneNumber = srv.EditableEvent.AccountPhone;
                    srv.LeadPhoneNumber = srv.EditableEvent.PhoneNumber;
                }
                loadingElement.style.display = "none";
                if (evt.EventRecurring != null) {
                    srv.EditableEvent.RecurrenceIsEnabled = true;
                    srv.RemoveRecurrenceEnable = true;
                    srv.IsRecur = true;
                    srv.CreateRecurringEventEdit(srv.EditableEvent.EventRecurring, evt.RRRule);
                    srv.RecurrenceSummary(srv.EditableRecurringEvent.recurrancepattern)
                }
                else {
                    srv.ShowEventRecurringPanel(false);
                }
                //srv.SetStartAndEndDateInAppointment();
                srv.InitializeDropDown();
            }, error);
        }

        srv.InitializeDropDown = function () {
            //srv.resetLOV();
            srv.selectedAccountId = null;
            srv.selectedOpportunityId = null;
            srv.selectedOrderId = null;
            srv.SelectedCaseId = null;
            srv.SelectedVendorCaseId = null;
            var opportunityDrpDown = $("#Opportunity").data("kendoComboBox");
            var orderDrpDown = $("#Order").data("kendoComboBox");
            var leadDrpDown = $("#Leads").data("kendoComboBox");
            var Casedd = $("#Case").data("kendoComboBox");
            var VendorCasedd = $("#VendorCase").data("kendoComboBox");

            if (leadDrpDown)
                var isLeadConverted = JSLINQ(leadDrpDown.dataSource._data)
                              .Where(function (item) { return item.LeadId == srv.EditableEvent.LeadId; });

            srv.selectedIds = srv.EditableEvent.AdditionalPeople;

            if (!srv.EditableEvent.LeadId || (isLeadConverted && isLeadConverted.items.length == 0)) {
                srv.selectedLeadId = 0;
            }
            else
                srv.selectedLeadId = srv.EditableEvent.LeadId;
            if (!srv.EditableEvent.AccountId) {
                srv.selectedAccountId = 0;
            }
            else
                srv.selectedAccountId = srv.EditableEvent.AccountId;

            opportunityDrpDown.dataSource.read();
            if (!srv.EditableEvent.OpportunityId) {
                srv.selectedOpportunityId = 0;
            }
            else
                srv.selectedOpportunityId = srv.EditableEvent.OpportunityId;

            orderDrpDown.dataSource.read();
            if (!srv.EditableEvent.OrderId) {
                srv.selectedOrderId = 0;
            }
            else
                srv.selectedOrderId = srv.EditableEvent.OrderId;

            
            Casedd.dataSource.read();
            if (!srv.EditableEvent.CaseId) {
                srv.selectedCaseId = 0;
            }
            else
                srv.selectedCaseId = srv.EditableEvent.CaseId;

            VendorCasedd.dataSource.read();
            if (!srv.EditableEvent.VendorCaseId) {
                srv.selectedVendorCaseId = 0;
            }
            else
                srv.selectedVendorCaseId = srv.EditableEvent.VendorCaseId;
        }

        //touchpoint_recur
        srv.CreateRecurringEventEdit = function (EventRecurring, RecurringRule) {
            var pattern = EventRecurring.PatternEnum;
            if (pattern == 1) {

                //Pattern
                srv.EditableRecurringEvent.recurrancepattern = EventRecurring.PatternEnum;
                srv.EditableRecurringEvent.occurs = EventRecurring.RecursEvery;


            }

            if (pattern == 2) {

                //Weekly Pattern
                srv.EditableRecurringEvent.recurrancepattern = EventRecurring.PatternEnum;
                srv.EditableRecurringEvent.occurs = EventRecurring.RecursEvery;

                if (RecurringRule != null && RecurringRule != undefined) {
                    //RRULE:FREQ=WEEKLY;COUNT=15;INTERVAL=2;BYDAY=MO,WE
                    var rlist = RecurringRule.toString().split(';');
                    var index;
                    for (index = 0; index < rlist.length; ++index) {
                        console.log(rlist[index]);
                        if (rlist[index].indexOf("INTERVAL") !== -1) {
                            //reccur evey

                            srv.EditableRecurringEvent.occurs = EventRecurring.RecursEvery;
                        }
                        if (rlist[index].indexOf("BYDAY") !== -1) {
                            var daylist = rlist[index].toString().split(',');
                            var dayIndex = 0;
                            for (dayIndex = 0; dayIndex < daylist.length; ++dayIndex) {
                                if (daylist[dayIndex].indexOf("SU") !== -1) {
                                    srv.EditableRecurringEvent.days["Sunday"] = true;
                                }
                                if (daylist[dayIndex].indexOf("MO") !== -1) {
                                    srv.EditableRecurringEvent.days["Monday"] = true;
                                }
                                if (daylist[dayIndex].indexOf("TU") !== -1) {
                                    srv.EditableRecurringEvent.days["Tuesday"] = true;
                                }
                                if (daylist[dayIndex].indexOf("WE") !== -1) {
                                    srv.EditableRecurringEvent.days["Wednesday"] = true;
                                }
                                if (daylist[dayIndex].indexOf("TH") !== -1) {
                                    srv.EditableRecurringEvent.days["Thursday"] = true;
                                }
                                if (daylist[dayIndex].indexOf("FR") !== -1) {
                                    srv.EditableRecurringEvent.days["Friday"] = true;
                                }
                                if (daylist[dayIndex].indexOf("SA") !== -1) {
                                    srv.EditableRecurringEvent.days["Saturday"] = true;
                                }
                            }

                        }
                    }
                }
            }
            if (pattern == 3) {

                //Monthly Pattern
                srv.EditableRecurringEvent.recurrancepattern = EventRecurring.PatternEnum;
                srv.EditableRecurringEvent.occurs = EventRecurring.RecursEvery

                srv.EditableRecurringEvent.dayoftheMonth = EventRecurring.DayOfMonth;
                var monthlyflag = false;
                if (EventRecurring.DayOfMonth > 0) {
                    monthlyflag = true;
                    srv.EditableRecurringEvent.flagMonthly = 1;
                }
                var weekOfTheMonth = 0;
                weekOfTheMonth = EventRecurring.DayOfWeekIndex;
                if (weekOfTheMonth > 0) {
                    if (weekOfTheMonth == 1) { weekOfTheMonth = 'First'; }
                    if (weekOfTheMonth == 2) { weekOfTheMonth = 'Second'; }
                    if (weekOfTheMonth == 3) { weekOfTheMonth = 'Third'; }
                    if (weekOfTheMonth == 4) { weekOfTheMonth = 'Fourth'; }
                    if (weekOfTheMonth == 5) { weekOfTheMonth = 'Last'; }
                    srv.EditableRecurringEvent.weekOfTheMonth = weekOfTheMonth;
                    srv.EditableRecurringEvent.WeekOfTheMonth = weekOfTheMonth;
                    monthlyflag = false;
                }
                var DayOfWeek = 0;
                DayOfWeek = EventRecurring.DayOfWeekEnum;
                if (DayOfWeek > 0) {
                    if (DayOfWeek == 1) { DayOfWeek = 'Sunday'; }
                    if (DayOfWeek == 2) { DayOfWeek = 'Monday'; }
                    if (DayOfWeek == 3) { DayOfWeek = 'Tuesday'; }
                    if (DayOfWeek == 4) { DayOfWeek = 'Wednesday'; }
                    if (DayOfWeek == 5) { DayOfWeek = 'Thursday'; }
                    if (DayOfWeek == 6) { DayOfWeek = 'Friday'; }
                    if (DayOfWeek == 7) { DayOfWeek = 'Saturday'; }
                    srv.EditableRecurringEvent.DayOfWeek = DayOfWeek;
                    monthlyflag = false;
                }
                if ((srv.EditableRecurringEvent.weekOfTheMonth != null && srv.EditableRecurringEvent.weekOfTheMonth != undefined) && monthlyflag == false) {
                    srv.EditableRecurringEvent.flagMonthly = 2;
                }
            }
            if (pattern == 4) {

                //Yearly Pattern
                srv.EditableRecurringEvent.recurrancepattern = EventRecurring.PatternEnum;
                srv.EditableRecurringEvent.occurs = EventRecurring.RecursEvery;
                srv.EditableRecurringEventmonthOfTheYear = {};
                var yearlyMontRepeatsOn = 0;
                yearlyMontRepeatsOn = EventRecurring.MonthEnum;
                if (yearlyMontRepeatsOn > 0) {
                    if (yearlyMontRepeatsOn == 1) { yearlyMontRepeatsOn = 'January'; }
                    if (yearlyMontRepeatsOn == 2) { yearlyMontRepeatsOn = 'February'; }
                    if (yearlyMontRepeatsOn == 3) { yearlyMontRepeatsOn = 'March'; }
                    if (yearlyMontRepeatsOn == 4) { yearlyMontRepeatsOn = 'April'; }
                    if (yearlyMontRepeatsOn == 5) { yearlyMontRepeatsOn = 'May'; }
                    if (yearlyMontRepeatsOn == 6) { yearlyMontRepeatsOn = 'June'; }
                    if (yearlyMontRepeatsOn == 7) { yearlyMontRepeatsOn = 'July'; }
                    if (yearlyMontRepeatsOn == 8) { yearlyMontRepeatsOn = 'August'; }
                    if (yearlyMontRepeatsOn == 9) { yearlyMontRepeatsOn = 'September'; }
                    if (yearlyMontRepeatsOn == 10) { yearlyMontRepeatsOn = 'October'; }
                    if (yearlyMontRepeatsOn == 11) { yearlyMontRepeatsOn = 'November'; }
                    if (yearlyMontRepeatsOn == 11) { yearlyMontRepeatsOn = 'December'; }
                    srv.EditableRecurringEventmonthOfTheYear.yearlyMontRepeatsOn = yearlyMontRepeatsOn;
                    srv.EditableRecurringEvent.yearlyMontRepeatsOn = yearlyMontRepeatsOn;
                }

                //Year flag 1
                //  alert(EventRecurring.DayOfMonth);
                srv.EditableRecurringEvent.dayoftheMonth = EventRecurring.DayOfMonth;
                var yearlyflag = false;
                if (EventRecurring.DayOfMonth > 0) {
                    yearlyflag = true;
                    srv.EditableRecurringEvent.flagYearly = 1;
                }

                //Year flag 2
                var weekOfTheMonth = 0;
                weekOfTheMonth = EventRecurring.DayOfWeekIndex;
                if (weekOfTheMonth > 0) {
                    if (weekOfTheMonth == 1) { weekOfTheMonth = 'First'; }
                    if (weekOfTheMonth == 2) { weekOfTheMonth = 'Second'; }
                    if (weekOfTheMonth == 3) { weekOfTheMonth = 'Third'; }
                    if (weekOfTheMonth == 4) { weekOfTheMonth = 'Fourth'; }
                    if (weekOfTheMonth == 5) { weekOfTheMonth = 'Last'; }

                    srv.EditableRecurringEvent.weekOfTheMonth = weekOfTheMonth;
                }
                var DayOfWeek = 0;
                DayOfWeek = EventRecurring.DayOfWeekEnum;
                if (DayOfWeek > 0) {
                    if (DayOfWeek == 1) { DayOfWeek = 'Sunday'; }
                    if (DayOfWeek == 2) { DayOfWeek = 'Monday'; }
                    if (DayOfWeek == 3) { DayOfWeek = 'Tuesday'; }
                    if (DayOfWeek == 4) { DayOfWeek = 'Wednesday'; }
                    if (DayOfWeek == 5) { DayOfWeek = 'Thursday'; }
                    if (DayOfWeek == 6) { DayOfWeek = 'Friday'; }
                    if (DayOfWeek == 7) { DayOfWeek = 'Saturday'; }
                    srv.EditableRecurringEvent.DayOfWeek = DayOfWeek;
                }
                if ((srv.EditableRecurringEvent.DayOfWeek != null && srv.EditableRecurringEvent.DayOfWeek != undefined && srv.EditableRecurringEvent.weekOfTheMonth != null
                    && srv.EditableRecurringEvent.weekOfTheMonth != undefined) && yearlyflag == false) {
                    srv.EditableRecurringEvent.flagYearly = 2;
                }


            }


            //#region Range
            srv.EditableRecurringEvent.endsAfterOccurances = EventRecurring.EndsAfterXOccurrences;
            //srv.EditableRecurringEvent.endBy = EventRecurring.endBy;
            var isdailyflag = false;

            //var sdate = EventRecurring.StartDate.toString().substring(0, 19)
            //sdate = new Date(sdate);
            //sdate = sdate.toLocaleDateString();
            //EventRecurring.StartDate = sdate;

            //var edate = EventRecurring.EndDate.toString().substring(0, 19)
            //edate = new Date(edate);
            //edate = edate.toLocaleDateString();
            //EventRecurring.EndDate = edate;
            if (EventRecurring.StartDate) {
                srv.EditableRecurringEvent.startOn = EventRecurring.StartDate;
            }
            if (EventRecurring.EndDate) {
                srv.EditableRecurringEvent.endByDate = EventRecurring.EndDate;
            }
            if (EventRecurring.EndDate > EventRecurring.StartDate) {
                srv.EditableRecurringEvent.endBy = 3
                srv.EditableRecurringEvent.endByDate = EventRecurring.EndDate;
            }
            if (EventRecurring.EndsAfterXOccurrences == undefined && EventRecurring.EndsAfterXOccurrences == null) {
                srv.EditableRecurringEvent.endBy = 1;
                isdailyflag = true;
                $("#reoccuranceEnd").prop("checked", true);
            }
            if (EventRecurring.EndsAfterXOccurrences > 0) {
                srv.EditableRecurringEvent.endBy = 2;
                isdailyflag = true;
                $("#reoccuranceEndBy").prop("checked", true);
            }
            if ((srv.EditableRecurringEvent.endByDate != null || srv.EditableRecurringEvent.endByDate != undefined)
                && ((isdailyflag == false || EventRecurring.EndDate > EventRecurring.StartDate) && EventRecurring.EndsOn != null)) {
                srv.EditableRecurringEvent.endBy = 3
                $("#reoccuranceEndDate").prop("checked", true);
                $("#reoccuranceEnd").prop("checked", false);
                $("#reoccuranceEndBy").prop("checked", false);
            }
            //#endregion

            //Add class for enlarge the recurrence view 
            srv.ShowEventRecurringPanel(true);
        }
        srv.recurrencesummary = '';
        srv.RecurrenceSummary = function (recurrenceId, isNew) {


            srv.recurrencesummary = '';
            var repeatText = 'Occurs  ';
            if (isNew === 'NewRecur' && srv.IsRecur != true) {
                srv.EditableRecurringEvent.recurrancepattern = 1;
                srv.EditableRecurringEvent.occurs = 1;
                srv.EditableRecurringEvent.endBy = 1;
                srv.EditableRecurringEvent.endsAfterOccurances = 1;
                srv.EditableRecurringEvent.days["Sunday"] = false;
                srv.EditableRecurringEvent.days["Monday"] = false;
                srv.EditableRecurringEvent.days["Tuesday"] = false;
                srv.EditableRecurringEvent.days["Wednesday"] = false;
                srv.EditableRecurringEvent.days["Thursday"] = false;
                srv.EditableRecurringEvent.days["Friday"] = false;
                srv.EditableRecurringEvent.days["Saturday"] = false;
                srv.SetStartEndTime();
            }
            if (recurrenceId == 1) {
                if (srv.EditableRecurringEvent.occurs > 1) {
                    repeatText = repeatText + 'every ' + srv.EditableRecurringEvent.occurs + ' days';
                }
                else {
                    repeatText = repeatText + 'daily';
                }

            }
            if (recurrenceId == 2) {
                if (srv.EditableRecurringEvent.occurs > 1) {
                    repeatText = repeatText + 'every ' + srv.EditableRecurringEvent.occurs + ' weeks';
                }
                else {
                    repeatText = repeatText + 'weekly';
                }

                var sunday = srv.EditableRecurringEvent.days.Sunday;
                var monday = srv.EditableRecurringEvent.days.Monday;
                var tuesday = srv.EditableRecurringEvent.days.Tuesday;
                var wednesday = srv.EditableRecurringEvent.days.Wednesday;
                var thursday = srv.EditableRecurringEvent.days.Thursday;
                var friday = srv.EditableRecurringEvent.days.Friday;
                var saturday = srv.EditableRecurringEvent.days.Saturday;
                if (sunday == true || monday == true || tuesday == true || wednesday == true || thursday == true || friday == true || saturday == true) {
                    repeatText = repeatText + ' On ';
                }
                var day = '';
                if (sunday == true) {
                    day = day + 'Sunday';
                }
                if (monday == true) {
                    day = day + ',Monday';
                }
                if (tuesday == true) {
                    day = day + ',Tuesday';
                }
                if (wednesday == true) {
                    day = day + ',Wednesday';
                }
                if (thursday == true) {
                    day = day + ',Thursday';
                }
                if (friday == true) {
                    day = day + ',Friday';
                }
                if (saturday == true) {
                    day = day + ',Saturday';
                }


                repeatText = repeatText + day;
            }
            if (recurrenceId == 3) {
                if (srv.EditableRecurringEvent.occurs > 1) {
                    repeatText = repeatText + 'every ' + srv.EditableRecurringEvent.occurs + ' months ';
                }
                else {
                    repeatText = repeatText + 'monthly ';
                }
                var flagMonthly = srv.EditableRecurringEvent.flagMonthly;
                //monthly flag 2
                if (flagMonthly == 1) {
                    var dayofmonth = srv.EditableRecurringEvent.dayoftheMonth;
                    if (dayofmonth > 0) {
                        repeatText = repeatText + dayofmonth + ' day ';
                    }
                }
                //monthly flag 3
                if (flagMonthly == 2) {
                    var weekOfTheMonth = 0;
                    weekOfTheMonth = srv.EditableRecurringEvent.WeekOfTheMonth;
                    if (weekOfTheMonth > 0) {
                        if (weekOfTheMonth == 1) { weekOfTheMonth = 'First '; }
                        if (weekOfTheMonth == 2) { weekOfTheMonth = 'Second '; }
                        if (weekOfTheMonth == 3) { weekOfTheMonth = 'Third '; }
                        if (weekOfTheMonth == 4) { weekOfTheMonth = 'Fourth '; }
                        if (weekOfTheMonth == 5) { weekOfTheMonth = 'Last '; }
                    }
                    if (weekOfTheMonth == undefined) { weekOfTheMonth = 'First ' }
                    repeatText = repeatText + weekOfTheMonth;
                    var DayOfWeek = 0;
                    DayOfWeek = srv.EditableRecurringEvent.DayOfWeek;
                    if (DayOfWeek > 0) {
                        if (DayOfWeek == 1) { DayOfWeek = 'Sunday '; }
                        if (DayOfWeek == 2) { DayOfWeek = 'Monday '; }
                        if (DayOfWeek == 3) { DayOfWeek = 'Tuesday '; }
                        if (DayOfWeek == 4) { DayOfWeek = 'Wednesday '; }
                        if (DayOfWeek == 5) { DayOfWeek = 'Thursday '; }
                        if (DayOfWeek == 6) { DayOfWeek = 'Friday '; }
                        if (DayOfWeek == 7) { DayOfWeek = 'Saturday '; }

                    }
                    repeatText = repeatText + " " + DayOfWeek;
                }

            }
            if (recurrenceId == 4) {
                if (srv.EditableRecurringEvent.occurs > 1) {
                    repeatText = repeatText + 'every ' + srv.EditableRecurringEvent.occurs + ' year ';
                }
                else {
                    repeatText = repeatText + 'yearly ';
                }

                //common repeat on
                var yearlyMontRepeatsOn = srv.EditableRecurringEvent.yearlyMontRepeatsOn;
                if (yearlyMontRepeatsOn != null) {
                    if (yearlyMontRepeatsOn == 1) { yearlyMontRepeatsOn = 'January '; }
                    if (yearlyMontRepeatsOn == 2) { yearlyMontRepeatsOn = 'February '; }
                    if (yearlyMontRepeatsOn == 3) { yearlyMontRepeatsOn = 'March '; }
                    if (yearlyMontRepeatsOn == 4) { yearlyMontRepeatsOn = 'April '; }
                    if (yearlyMontRepeatsOn == 5) { yearlyMontRepeatsOn = 'May '; }
                    if (yearlyMontRepeatsOn == 6) { yearlyMontRepeatsOn = 'June '; }
                    if (yearlyMontRepeatsOn == 7) { yearlyMontRepeatsOn = 'July '; }
                    if (yearlyMontRepeatsOn == 8) { yearlyMontRepeatsOn = 'August '; }
                    if (yearlyMontRepeatsOn == 9) { yearlyMontRepeatsOn = 'September '; }
                    if (yearlyMontRepeatsOn == 10) { yearlyMontRepeatsOn = 'October '; }
                    if (yearlyMontRepeatsOn == 11) { yearlyMontRepeatsOn = 'November '; }
                    if (yearlyMontRepeatsOn == 11) { yearlyMontRepeatsOn = 'December '; }

                    repeatText = repeatText + yearlyMontRepeatsOn;
                }

                //year flags
                var flagYearly = srv.EditableRecurringEvent.flagYearly;
                //yealy flag 1
                if (flagYearly == 1) {
                    var dayofmonth = srv.EditableRecurringEvent.dayoftheMonth;
                    if (dayofmonth > 0) {
                        repeatText = repeatText + dayofmonth + ' day ';
                    }
                }
                //yealy flag 2
                if (flagYearly == 2) {
                    var weekOfTheMonth = 0;
                    weekOfTheMonth = srv.EditableRecurringEvent.WeekOfTheMonth;
                    if (weekOfTheMonth > 0) {
                        if (weekOfTheMonth == 1) { weekOfTheMonth = ' First '; }
                        if (weekOfTheMonth == 2) { weekOfTheMonth = ' Second '; }
                        if (weekOfTheMonth == 3) { weekOfTheMonth = ' Third '; }
                        if (weekOfTheMonth == 4) { weekOfTheMonth = ' Fourth '; }
                        if (weekOfTheMonth == 5) { weekOfTheMonth = ' Last '; }
                    }
                    if (weekOfTheMonth == undefined) { weekOfTheMonth = ' First ' }
                    repeatText = repeatText + weekOfTheMonth;
                    var DayOfWeek = 0;
                    DayOfWeek = srv.EditableRecurringEvent.DayOfWeek;
                    if (DayOfWeek > 0) {
                        if (DayOfWeek == 1) { DayOfWeek = ' Sunday '; }
                        if (DayOfWeek == 2) { DayOfWeek = ' Monday '; }
                        if (DayOfWeek == 3) { DayOfWeek = ' Tuesday '; }
                        if (DayOfWeek == 4) { DayOfWeek = ' Wednesday '; }
                        if (DayOfWeek == 5) { DayOfWeek = ' Thursday '; }
                        if (DayOfWeek == 6) { DayOfWeek = ' Friday '; }
                        if (DayOfWeek == 7) { DayOfWeek = ' Saturday '; }

                    }
                    repeatText = repeatText + " " + DayOfWeek;
                }

            }


            //Range is common
            var recurrenceCount;
            var rangeselection = srv.EditableRecurringEvent.endBy;

            _setRecurrenceDate(srv.EditableRecurringEvent.startOn, 'start');
            if (srv.EditableRecurringEvent.startOn != null) {
                //startDate = new Date(srv.EditableRecurringEvent.startOn);
                //startDate = startDate.toLocaleDateString();
                repeatText = repeatText + ' effective ' + srv.EditableRecurringEvent.startOn + " ";
            }
            if (rangeselection == 2) {
                if (srv.EditableRecurringEvent.endsAfterOccurances == null) {
                    srv.EditableRecurringEvent.endsAfterOccurances = 1;
                }
                recurrenceCount = srv.EditableRecurringEvent.endsAfterOccurances;
                repeatText = repeatText + recurrenceCount + ' time(s)';
            }
            if (rangeselection == 3) {

                _setRecurrenceDate(srv.EditableRecurringEvent.endByDate, 'end');
                if (srv.EditableRecurringEvent.endByDate)
                    repeatText = repeatText + ' untill ' + srv.EditableRecurringEvent.endByDate;
            }

            srv.RemoveRecurrenceEnable = false;
            srv.recurrencesummary = repeatText;
        }
        srv.ShowEventRecurringPanel = function (view) {

            if (view == true) {
                //Add class for enlarge the recurrence view
                document.getElementById('evt_recurring_panel').style.visibility = 'visible';

                $("#evt_recurring_panel").removeClass("evt-recurring-panel collapse");
                $("#evt_recurring_panel").addClass("evt-recurring-panel");
            }
            else {
                //Add class for hide the recurrence view
                $("#evt_recurring_panel").removeClass("evt-recurring-panel");
                $("#evt_recurring_panel").removeClass("ng-scope collapse in evt-recurring-panel");
                $("#evt_recurring_panel").addClass("evt-recurring-panel collapse");
            }

        }

        srv.SetStartAndEndDateInAppointment = function (startDate) {
            //var ddate = srv.EditableEvent.start.toString().substring(0, 19)
            var date = new Date();
            if (startDate) {
                date = startDate;
            }
            if (startDate == undefined) {
                if (date.getMinutes() > 0) {
                    date.setMinutes(0);
                    date.setHours((date.getHours()) + 1);

                }
            }
            var startDate = date;
            srv.EditableEvent.start = startDate.toLocaleString('en-US');
            srv.EditableEvent.start = srv.EditableEvent.start.replace(/:\d{2}\s/, ' ');
            //date.setHours((date.getHours()) + Math.round(date.getMinutes() / 60));
            //date.setMinutes(0);
            //var mydate = new Date(date);
            //date = date.toLocaleString('en-US');
            //date = date.replace(/:\d{2}\s/, ' ');
            
            srv.EditableEvent.start = srv.EditableEvent.start.replace(',', '')
            //srv.EditableEvent.start = date;
            date.setHours((date.getHours()) + 1);
            srv.EditableEvent.end = date.toLocaleString('en-US');
            srv.EditableEvent.end = srv.EditableEvent.end.replace(',', '')
            srv.EditableEvent.end = srv.EditableEvent.end.replace(/:\d{2}\s/, ' ');
            //date = date.toLocaleDateString() + ' ' + date.toLocaleTimeString();
            //date = date.replace(/:\d{2}\s/, ' ');

            //mydate = mydate.replace(',', '')
            //srv.EditableEvent.end = date;
            //srv.EditableEvent.start = date;
        }
        srv.SetRecurrenceEventStartAndEndDate = function (startDate) {

            var date = new Date();
            if (startDate) {
                if (srv.EditableEvent.start.indexOf("T") != -1) {
                    var splitDateTime = srv.EditableEvent.start.split("T");
                    var splitTime = splitDateTime[1];
                    startDate = startDate + " " + splitTime;
                }
                else {
                    var splitDateTime = srv.EditableEvent.start.split(" ");
                    var splitTime = splitDateTime[1];
                }
                date = new Date(startDate);
            }

            var startDate = date;
            srv.EditableEvent.start = startDate.toLocaleString('en-US');
            srv.EditableEvent.start = srv.EditableEvent.start.replace(/:\d{2}\s/, ' ');

            srv.EditableEvent.start = srv.EditableEvent.start.replace(',', '')
            //srv.EditableEvent.start = date;
            
            date.setHours((date.getHours()) + 1);
            srv.EditableEvent.end = date.toLocaleString('en-US');
            srv.EditableEvent.end = srv.EditableEvent.end.replace(',', '')
            srv.EditableEvent.end = srv.EditableEvent.end.replace(/:\d{2}\s/, ' ');
            srv.SetStartEndTime();
        }


        srv.NewTask = function (modalId) {
            var evt = {
                title: title,
                OrganizerPersonId: HFCService.CurrentUser.PersonId,
                ReminderMinute: 30,
                editable: true,
                IsDeletable: true,
                IsPrivate: false,
            }

            _setDefaults(evt, -1);
            $("#" + modalId).modal("show");
        };




        srv.EditTaskFromCalendarSceduler = function (modalId, TaskModel, value, callback) {
            
            srv.callbackForTask = callback;
            var title = "";
            var OrganizerPersonId = 0;
            var Message = '';
            var TaskID = 0;
            var start = '';
            var IsPrivate = false;
            srv.IsTaskCheck = true;
            srv.getReminder();
            if (value == true) {
                var evt = {
                    id: TaskModel.TaskId,
                    title: TaskModel.Subject,
                    Message: TaskModel.Message,
                    start: TaskModel.DueDate,
                    IsPrivate: TaskModel.IsPrivate,
                    selectedIds: TaskModel.AssignedName,
                    ReminderMinute: TaskModel.ReminderMinute,
                    IsPrivateAccess: TaskModel.IsPrivateAccess
                }
                var id = TaskModel.TaskId;
                $http.get('/api/lookup/' + id + '/GetAttendeesByTaskId').then(function (data) {

                    var attendees = data.data;
                    srv.selectedIds = attendees;
                });

            }
            else {
                var evt = {
                    id: TaskModel.Id,
                    title: TaskModel.subject,
                    Message: TaskModel.note,
                    start: TaskModel.end,
                    IsPrivate: TaskModel.IsPrivate,
                    selectedIds: TaskModel.Attendee,
                    ReminderMinute: TaskModel.ReminderId,
                    IsPrivateAccess: TaskModel.IsPrivateAccess
                }
                srv.selectedIds = TaskModel.OwnerID2;
            }
            srv.selectedLeadId = null;
            srv.selectedAccountId = null;
            srv.selectedOpportunityId = null;
            srv.selectedOrderId = null;
            srv.SelectedCaseId = null;
            srv.SelectedVendorCaseId = null;
            var opportunityDrpDown = $("#Opportunity").data("kendoComboBox");
            var orderDrpDown = $("#Order").data("kendoComboBox");
            var accountDrpDown = $("#Account").data("kendoComboBox");
            var leadDrpDown = $("#Lead").data("kendoComboBox");
            var casedd = $("#Case").data("kendoComboBox");
            var Vendorcasedd = $("#VendorCase").data("kendoComboBox");

            srv.selectedLeadId = TaskModel.LeadId;
            if (!srv.selectedLeadId)
                srv.selectedLeadId = 0;
            srv.selectedAccountId = TaskModel.AccountId;
            if (!srv.selectedAccountId)
                srv.selectedAccountId = 0;

            opportunityDrpDown.dataSource.read();

            srv.selectedOpportunityId = TaskModel.OpportunityId;
            if (!srv.selectedOpportunityId)
                srv.selectedOpportunityId = 0;

            orderDrpDown.dataSource.read();
            srv.selectedOrderId = TaskModel.OrderId;
            if (!srv.selectedOrderId)
                srv.selectedOrderId = 0;

            casedd.dataSource.read();
            srv.selectedCaseId = TaskModel.CaseId;
            if (!srv.selectedCaseId)
                srv.selectedCaseId = 0;


            Vendorcasedd.dataSource.read();
            srv.selectedVendorCaseId = TaskModel.VendorCaseId;
            if (!srv.selectedVendorCaseId)
                srv.selectedVendorCaseId = 0;

            srv.EditableEvent = evt;



            var date = new Date(srv.EditableEvent.start);
            date = date.toLocaleString('en-US');
            date = date.replace(/:\d{2}\s/, ' ');
            date = date.replace(',', '')
            srv.EditableEvent.start = date;

            $("#persongo").modal("show");
            if (document.getElementById('ReminderControl'))
                document.getElementById('ReminderControl').value = TaskModel.ReminderId;
        };
        //Recur date
        function _setRecurrenceDate(dateString, type) {
            //var date = new Date(rdate);
            //date = date.toLocaleString();
            //date = date.replace(/:\d{2}\s/, ' ');
            //date = date.replace(',', '')
            if (dateString.indexOf("T") != -1) {
                dateString = srv.UpdateToDateTP(dateString);
            }

            var today = new Date(dateString);
            var dd = today.getDate();

            var mm = today.getMonth() + 1;
            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }

            if (mm < 10) {
                mm = '0' + mm;
            }


            if (type == 'start') {
                srv.EditableRecurringEvent.startOn = mm + '/' + dd + '/' + yyyy;;
            }
            else {
                srv.EditableRecurringEvent.endByDate = mm + '/' + dd + '/' + yyyy;;
            }


        }


        function _setDefaults(clone, index) {
            if (srv.EditableRecurringEvent == undefined) {
                srv.EditableRecurringEvent = {};
            }

            srv.EditableRecurringEvent.recurrancepattern = 1;
            srv.EndSelection = 'Never';
            srv.RelativeGroup = 'Day';
            srv.RecurrenceIsEnabled = false;
            srv.RecurrenceIsDisabled = false;

            if (clone.EventRecurring) {

                if (clone.EventTypeEnum == srv.EventTypeEnum.Occurrence)
                    srv.RecurrenceIsDisabled = true;
                if (srv.EventRecurring.EndDate)
                    srv.EndSelection = 'EndsOn';
                else if (srv.EventRecurring.EndsAfterXOccurrences)
                    srv.EndSelection = 'After';

                if (clone.EventRecurring.DayOfWeekIndex)
                    srv.RelativeGroup = 'Relative';
            }

            srv.EditableEvent = clone;
            srv.EventIndex = index;
            srv.PatternEnum = "1";

            srv.SetStartEndTime();
            srv.RemoveRecurrenceEnable = false;
            $("#crm-evt-container").modal("show");
            setTimeout(function () {
                $("#subjectIpt").focus();
            }, 300)
        }
        srv.SetStartEndTime = function () {
            srv.EditableRecurringEvent.startOn = srv.EditableEvent.start;
            srv.EditableRecurringEvent.endByDate = srv.EditableEvent.end;

        }




        srv.Edit = function (calEvent, index) {
            var clone = angular.copy(calEvent);
            _setDefaults(clone, index);
        };


        srv.ApptCancelled = function () {
            srv.IsBusy = true;
            $http.post('/api/calendar/' + srv.EditableEvent.id + '/ApptCancelled').then(function (res) {
                srv.IsBusy = false;
                $("#crm-evt-container").modal("hide");
                for (var i = 0; i < srv.CalendarEvents.length; i++) {
                    if (srv.EditableEvent.id == srv.CalendarEvents[i].id) {
                        srv.CalendarEvents[i].IsCancelled = true;
                    }
                }
            }, error);
        };



        srv.close = function (modelId) {
            $("#" + modalId).modal("hide");
        }
        srv.GetFranchiseCurrentTime = function () {
            var CurrentDateTime = new Date();
            var cdatetime = CurrentDateTime.toDateString() + ' ' + CurrentDateTime.toLocaleTimeString();
            var date = new Date(cdatetime);
            date = date.toLocaleString();
            date = date.replace(/:\d{2}\s/, ' ');
            date = date.replace(',', '');
            return date;
        }



        srv.VendorCaseTaskCallback;
        srv.saveTask = function (modelId, modal, TaskId) {

            var temp = [];
            temp = srv.selectedIds;
            //*****************************************************************
            var FormatStartDate = new Date();
            FormatStartDate.setHours(0, 0, 0, 0);
            FormatStartDate = new Date(FormatStartDate);
            var FormatTotDate = new Date(modal.frmAppt.fromDate.$viewValue);
            var d = FormatTotDate;
            if ((d.getFullYear() == 0 || d.getMonth() + 1 == 0 || d.getDate() == 0) || (isNaN(d.getFullYear()) || isNaN(d.getMonth()) || isNaN(d.getDate()))) {
                HFC.DisplayAlert("Invalid Due Date");
                return;
            }
            if (FormatStartDate > FormatTotDate) {
                HFC.DisplayAlert("Due Date cannot be in the past.");
                return false;
            }
            if (modal.frmAppt.subject.$viewValue === null) {
                return false;
            }
            //*****************************************************************

            srv.EditableEvent.id = TaskId;
            srv.EditableEvent.AdditionalPeople = temp;
            //If save from Lead - Account not required
            if (modal.$parent.AccountService != undefined) {
                srv.EditableEvent.Accounts = modal.$parent.AccountService.Accounts;
            }
            //If save from Lead - Opportunitys not required
            if (modal.$parent.OpportunityService != undefined) {
                srv.EditableEvent.Opportunitys = modal.$parent.OpportunityService.Opportunitys;
            }
            srv.EditableEvent.Attendees = srv.selectedIds;


            //LeadId,Account,Opportunity and OrderId
            if (srv.selectedLeadId > 0) {
                srv.EditableEvent.LeadId = srv.selectedLeadId;
            }
            if (srv.selectedAccountId > 0) {
                srv.EditableEvent.AccountId = srv.selectedAccountId;
            }
            if (srv.selectedOpportunityId > 0) {

                srv.EditableEvent.OpportunityId = srv.selectedOpportunityId;
            }
            if (srv.selectedOrderId > 0) {

                srv.EditableEvent.OrderId = srv.selectedOrderId;
            }

            if (srv.selectedCaseId > 0) {
                srv.EditableEvent.CaseId = srv.selectedCaseId;
            }

            if (srv.selectedVendorCaseId > 0) {
                srv.EditableEvent.VendorCaseId = srv.selectedVendorCaseId;
            }

            var notMatched = [];
            for (var a = 0; a < srv.EditableEvent.AdditionalPeople.length; a++) {
                var isExists = false;
                for (var i = 0; i < HFCService.AllUsers.length; i++) {
                    if (HFCService.AllUsers[i].Email == srv.EditableEvent.AdditionalPeople[a]) {
                        isExists = true;
                        break;
                    }
                }

                if (isExists == false) {
                    notMatched.push(srv.EditableEvent.AdditionalPeople[a]);
                }
            }

            var isAllExists = true;
            for (var i = 0; i < notMatched.length; i++) {
                var index = srv.EditableEvent.AdditionalPeople.indexOf(notMatched[i]);
                // srv.EditableEvent.AdditionalPeople.splice(index, 1);
                isAllExists = false;
            } var assignedpersonId;
            if (temp.length > 0) {
                for (var i = 0; i < HFCService.AllUsers.length; i++) {
                    if (HFCService.AllUsers[i].Email == temp[0]) {
                        assignedpersonId = HFCService.AllUsers[i].PersonId;
                        break;
                    }
                }
            }

            var payload = angular.copy(srv.EditableEvent);
            payload.start = new Date();//StartDate Current Date
            payload.end = new Date(srv.EditableEvent.start);//Due Date

            payload.start = moment(payload.start.length > 0 ? payload.start[0] : payload.start).format('YYYY-MM-DD');
            payload.end = moment(payload.end && payload.end.length > 0 ? payload.end[0] : payload.end).format('YYYY-MM-DD');


            //payload.LeadId = srv.EditableEvent.LeadId;
            if (assignedpersonId == null || assignedpersonId == undefined) {
                assignedpersonId = modal.PersonId;
            }
            payload.AssignedPersonId = assignedpersonId;
            payload.AdditionalPeople = srv.EditableEvent.Attendees;

            // Moved to the bottom as this blocks the warning message below.
            //var loadingElement = document.getElementById("loading");
            //loadingElement.style.display = "block";

            if (isAllExists == false) {
                var myDiv = $('.taskevent_popup');
                myDiv[0].scrollTop = 0;
                $("#crm-evt-container").modal("hide");
                bootbox.dialog({
                    closeButton: false,
                    message: HFC.AttendeeWarning,
                    title: "Warning",
                    buttons: {
                        main: {
                            label: "Ok",
                            className: "btn-default",
                            callback: function () {

                                var loadingElement = document.getElementById("loading");
                                loadingElement.style.display = "block";

                                var res = srv.PostTask(payload);
                                srv.EditableEvent = {};
                                $("#" + modelId).modal("hide");
                                
                                if (srv.SuccessCallback) {
                                    srv.SuccessCallback();
                                }

                                return (res);
                            }
                        }
                    }
                });
            } else {
                $("#crm-evt-container").modal("hide");

                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "block";

                var res = srv.PostTask(payload);

                srv.EditableEvent = {};
                var myDiv = $('.taskevent_popup');
                myDiv[0].scrollTop = 0;
                $("#" + modelId).modal("hide");

                //if (srv.SuccessCallback) {
                //    srv.SuccessCallback();
                //}

                //if (srv.callbackForTask) {
                //    srv.callbackForTask();
                //}

                return (res);

            }
            //}

        }

        srv.FormatDate = function (date) {
            var month = 0;
            if (date.getMonth() != null) {
                if (date.getMonth().toString().length == 1) {
                    month = "0" + (date.getMonth() + 1);
                }
                else {
                    date = date.getMonth();
                }
            }
            date = month + "/" + date.getDate() + "/" + date.getFullYear();
            return date
        }

        srv.SaveCalendar = function (modelId, modal, type, removerecurence) {
            
            var event;
            var myDiv = $('.taskevent_popup');
            myDiv[0].scrollTop = 0;
            srv.returntoSectionDisplay = null;
            //Remove Recurrence
            if (removerecurence || (srv.EditableEvent.RecurrenceIsEnabled == false && srv.EditableEvent.EventRecurring != undefined)) {
                var payload = angular.copy(srv.EditableEvent);
                srv.recurrencesummary = null;
                if (srv.EditableRecurringEvent == undefined) {
                    srv.EditableRecurringEvent = {};
                }

                srv.EditableRecurringEvent.recurrancepattern = 1;
                srv.EditableRecurringEvent.occurs = 1;
                srv.EditableRecurringEvent.endBy = 1;
                srv.EditableRecurringEvent.endsAfterOccurances = 1;
                srv.EndSelection = 'Never';
                srv.RelativeGroup = 'Day';
                srv.RecurrenceIsEnabled = false;
                srv.RecurrenceIsDisabled = false;

                srv.PatternEnum = "1";

                srv.EditableRecurringEvent.days["Sunday"] = false;
                srv.EditableRecurringEvent.days["Monday"] = false;
                srv.EditableRecurringEvent.days["Tuesday"] = false;
                srv.EditableRecurringEvent.days["Wednesday"] = false;
                srv.EditableRecurringEvent.days["Thursday"] = false;
                srv.EditableRecurringEvent.days["Friday"] = false;
                srv.EditableRecurringEvent.days["Saturday"] = false;
                srv.SetStartEndTime();
                srv.RemoveRecurrenceEnable = false;

                payload.AddRecurrence = false;

                var res = srv.PostCalendar(payload, modelId);

                return (res);
            }

            if (srv.EditableEvent.title === null || srv.EditableEvent.title === '' || srv.EditableEvent.title === undefined) {
                HFC.DisplayAlert("Subject is required.");
                return false;
            }
            //3/23/2018
            if (srv.EditableEvent.Location === null || srv.EditableEvent.Location === '' || srv.EditableEvent.Location === undefined) {
                HFC.DisplayAlert("Location is required.");
                return false;
            }
            if (srv.EditableEvent.ReminderMinute === '') {
                return;
            }
            if (srv.EditableEvent.AptTypeEnum == undefined || srv.EditableEvent.AptTypeEnum == '') {
                HFC.DisplayAlert('Appointment Type is required');
                return false;
            }

            if (srv.EditableEvent.start === null || srv.EditableEvent.start === '' || srv.EditableEvent.start === undefined) {
                HFC.DisplayAlert("From Date is required.");
                return false;
            }

            if (srv.EditableEvent.end === null || srv.EditableEvent.end === '' || srv.EditableEvent.end === undefined) {
                HFC.DisplayAlert("End Date is required.");
                return false;
            }
            if (srv.EditableEvent.start.indexOf("T") != -1) {
                srv.EditableEvent.start = srv.UpdateToDateTP(srv.EditableEvent.start);
            }


            if (srv.EditableEvent.end.indexOf("T") != -1) {
                srv.EditableEvent.end = srv.UpdateToDateTP(srv.EditableEvent.end);  // temporary solution
            }



            var FormatStartDate = new Date(srv.EditableEvent.start);
            var FormatTotDate = new Date(srv.EditableEvent.end);
            if (FormatStartDate > FormatTotDate) {
                HFC.DisplayAlert("To Date must be later than From Date.");
                return false;
            }
            if (FormatStartDate < new Date()) {
                if (!confirm("You have chosen the From Date past date. Do you want to continue?")) return selectedAccountId
            }

            // The following is not necessary as the model itself contain the selected values.
            //////srv.selectedLeadIds = srv.LeadId = $("#Lead").val();
            ////srv.selectedAccountId = srv.AccountId = $("#Account").val();
            ////srv.selectedOrderIds = srv.OrderId = $("#Order").val();
            ////srv.selectedOpportunityIds = srv.OpportunityId = $("#Opportunity").val();

            //If appointment type is sales lead is required field
            if (srv.EditableEvent.AptTypeEnum == 1 && srv.selectedLeadId == 0 && srv.selectedAccountId == 0) {

                HFC.DisplayAlert("Lead or Account is required for selected appointment type.");
                return false;

            }
            if (srv.EditableEvent.AptTypeEnum == 5 && (srv.selectedAccountId == 0 && srv.selectedOpportunityId == 0)) {
                HFC.DisplayAlert("Account or Opportunity is required for selected appointment type.");
                return false;
            }
            if (srv.EditableEvent.AptTypeEnum == 6 && (srv.selectedAccountId == 0)) {
                HFC.DisplayAlert("Account  is required for selected appointment type.");
                return false;
            }

            //Recurrence Validation
            if (srv.EditableEvent != null && srv.EditableEvent.RecurrenceIsEnabled) {

                if (srv.EditableRecurringEvent.occurs == null || srv.EditableRecurringEvent.occurs == undefined || srv.EditableRecurringEvent.occurs == "" || srv.EditableRecurringEvent.occurs == 0) {
                    HFC.DisplayAlert("Recurrence Every Day(s) or Week(s) or Month(s) or Year(s) is required");
                    return false;
                }
                if (srv.EditableRecurringEvent.recurrancepattern == 2) {
                    //if(srv.EditableRecurringEvent.days==null)
                    {
                        var sunday = srv.EditableRecurringEvent.days.Sunday;
                        var monday = srv.EditableRecurringEvent.days.Monday;
                        var tuesday = srv.EditableRecurringEvent.days.Tuesday;
                        var wednesday = srv.EditableRecurringEvent.days.Wednesday;
                        var thursday = srv.EditableRecurringEvent.days.Thursday;
                        var friday = srv.EditableRecurringEvent.days.Friday;
                        var saturday = srv.EditableRecurringEvent.days.Saturday;
                        if (!sunday && !monday && !tuesday && !wednesday && !thursday && !friday && !saturday) {
                            HFC.DisplayAlert("Recurrence Week Day(s) required");
                            return false;
                        }
                    }
                }
                if (srv.EditableRecurringEvent.recurrancepattern == 3) {
                    if (srv.EditableRecurringEvent.flagMonthly == 1) {
                        if (srv.EditableRecurringEvent.dayoftheMonth == null || srv.EditableRecurringEvent.dayoftheMonth == undefined ||
                            srv.EditableRecurringEvent.dayoftheMonth == "" || srv.EditableRecurringEvent.dayoftheMonth == 0) {
                            HFC.DisplayAlert("Recurrence Day of the Month is required");
                            return false;
                        }
                    }
                    if (srv.EditableRecurringEvent.flagMonthly == 2) {
                        if (srv.EditableRecurringEvent.WeekOfTheMonth == null || srv.EditableRecurringEvent.WeekOfTheMonth == undefined
                            || srv.EditableRecurringEvent.WeekOfTheMonth == "" || srv.EditableRecurringEvent.WeekOfTheMonth == 0) {
                            HFC.DisplayAlert("Recurrence Week of the Month is required");
                            return false;
                        }
                        if (srv.EditableRecurringEvent.DayOfWeek == null || srv.EditableRecurringEvent.DayOfWeek == undefined
                            || srv.EditableRecurringEvent.DayOfWeek == "" || srv.EditableRecurringEvent.DayOfWeek == 0 || typeof srv.EditableRecurringEvent.DayOfWeek == 'number') {
                            HFC.DisplayAlert("Recurrence Day of Week is required");
                            return false;
                        }
                    }
                }
                if (srv.EditableRecurringEvent.recurrancepattern == 4) {
                    //alert(srv.EditableRecurringEvent.yearlyMontRepeatsOn);
                    if (srv.EditableRecurringEvent.yearlyMontRepeatsOn == null || srv.EditableRecurringEvent.yearlyMontRepeatsOn == undefined
                        || srv.EditableRecurringEvent.yearlyMontRepeatsOn == "" || srv.EditableRecurringEvent.yearlyMontRepeatsOn == 0 || typeof srv.EditableRecurringEvent.yearlyMontRepeatsOn == 'number') {
                        HFC.DisplayAlert("Recurrence Yearly Month RepeatsOn required");
                        return false;
                    }

                    if (srv.EditableRecurringEvent.flagYearly == 1) {
                        if (srv.EditableRecurringEvent.dayoftheMonth == null || srv.EditableRecurringEvent.dayoftheMonth == undefined
                            || srv.EditableRecurringEvent.dayoftheMonth == "" || srv.EditableRecurringEvent.dayoftheMonth == 0) {
                            HFC.DisplayAlert("Recurrence Day of the Month is required");
                            return false;
                        }
                        if (srv.EditableRecurringEvent.flagYearly == 2) {
                            if (srv.EditableRecurringEvent.WeekOfTheMonth == null || srv.EditableRecurringEvent.WeekOfTheMonth == undefined
                                || srv.EditableRecurringEvent.WeekOfTheMonth == "" || srv.EditableRecurringEvent.WeekOfTheMonth == 0) {
                                HFC.DisplayAlert("Recurrence Week of the Month is required");
                                return false;
                            }
                            if (srv.EditableRecurringEvent.DayOfWeek == null || srv.EditableRecurringEvent.DayOfWeek == undefined
                                || srv.EditableRecurringEvent.DayOfWeek == "" || srv.EditableRecurringEvent.DayOfWeek == 0 || typeof srv.EditableRecurringEvent.DayOfWeek == 'number') {
                                HFC.DisplayAlert("Recurrence Day of Week is required");
                                return false;
                            }
                        }
                    }
                }

                //Range Of Recurrance
                var startdate = new Date(srv.EditableRecurringEvent.startOn);

                if (startdate == "Invalid Date") {
                    HFC.DisplayAlert('Valid Recurrence Start Date is required');
                    return false;
                }
                else {
                    startdate = new Date(startdate);
                }

                if (srv.EditableRecurringEvent.endBy == 2) {
                    if (srv.EditableRecurringEvent.endsAfterOccurances == "" || srv.EditableRecurringEvent.endsAfterOccurances == null) {
                        HFC.DisplayAlert('Ends After Occurance(s) is required');
                        return false;
                    }
                }
                if (srv.EditableRecurringEvent.endBy == 3) {
                    var enddate = new Date(srv.EditableRecurringEvent.endByDate);
                    if (enddate == "Invalid Date") {
                        HFC.DisplayAlert('Valid Recurrence End Date is required');
                        return false;
                    }
                    else {
                        enddate = srv.FormatDate(enddate);
                        if (startdate > enddate) {
                            HFC.DisplayAlert('Recurrence Start Date Should be less than End Date');
                            return false;
                        }
                    }
                }
            }

            var temp = [];
            srv.IsConvertLead = false;
            temp = srv.selectedIds;
            if (srv.selectedLeadId != undefined && srv.selectedLeadId > 0) {
                srv.EditableEvent.LeadId = srv.selectedLeadId;
            }
            else
                srv.EditableEvent.LeadId = null;


            if (srv.selectedAccountId > 0 && srv.selectedAccountId != undefined) {
                srv.EditableEvent.AccountId = srv.selectedAccountId;

            }
            else
                srv.EditableEvent.AccountId = 0;


            if (srv.selectedOpportunityId > 0 && srv.selectedOpportunityId != undefined) {

                srv.EditableEvent.OpportunityId = srv.selectedOpportunityId;
            }
            else {
                srv.selectedOpportunityId = null;
                srv.EditableEvent.OpportunityId = 0;
            }


            if (srv.selectedOrderId > 0 && srv.selectedOrderId != undefined) {
                srv.EditableEvent.OrderId = srv.selectedOrderId;
            }
            else {
                srv.selectedOrderId = null;
                srv.EditableEvent.OrderId = 0;
            }

            
            if (srv.selectedCaseId > 0 && srv.selectedCaseId) {
                srv.EditableEvent.CaseId = srv.selectedCaseId;
            }
            else {
                srv.EditableEvent.CaseId = 0;
                srv.selectedCaseId = null;
            }
            //vendor case
            if (srv.selectedVendorCaseId > 0 && srv.selectedVendorCaseId) {
                srv.EditableEvent.VendorCaseId = srv.selectedVendorCaseId;
            }
            else {
                srv.EditableEvent.VendorCaseId = 0;
                srv.selectedVendorCaseId = null;
            }


            srv.EditableEvent.AdditionalPeople = srv.selectedId;
            srv.EditableEvent.Accounts = srv.selectedAccountId;
            srv.EditableEvent.Opportunitys = srv.selectedOpportunityId;

            if (srv.selectedIds) {
                srv.EditableEvent.Attendees = srv.selectedIds;
            }

            var isAllExists = false;

            var assignedpersonId;
            if (temp.length > 0) {
                for (var i = 0; i < HFCService.AllUsers.length; i++) {
                    if (HFCService.AllUsers[i].Email == temp[0]) {
                        assignedpersonId = HFCService.AllUsers[i].PersonId;
                        break;
                    }
                }
            }

            srv.EditableEvent.start = FormatStartDate;
            srv.EditableEvent.end = FormatTotDate;
            var payload = angular.copy(srv.EditableEvent);

            payload.start = moment(payload.start.length > 0 ? payload.start[0] : payload.start).format('YYYY-MM-DD HH:mm:ss');
            payload.end = moment(payload.end && payload.end.length > 0 ? payload.end[0] : payload.end).format('YYYY-MM-DD HH:mm:ss');
            if (srv.EditableEvent.AccountId != null && srv.EditableEvent.AccountId != undefined) {
                payload.AccountId = srv.EditableEvent.AccountId;
            }
            if (srv.EditableEvent.OpportunityId != null && srv.EditableEvent.OpportunityId != undefined) {
                payload.OpportunityId = srv.EditableEvent.OpportunityId;
            }

            if (srv.EditableEvent.OrderId != null && srv.EditableEvent.OrderId != undefined) {
                payload.OrderId = srv.EditableEvent.OrderId;
            }
            if (srv.EditableEvent.CaseId != null && srv.EditableEvent.CaseId != undefined) {
                payload.CaseId = srv.EditableEvent.CaseId;
            }
            //vendor case
            if (srv.EditableEvent.VendorCaseId != null && srv.EditableEvent.VendorCaseId != undefined) {
                payload.VendorCaseId = srv.EditableEvent.VendorCaseId;
            }

            payload.AssignedPersonId = assignedpersonId;
            payload.AdditionalPeople = srv.EditableEvent.Attendees;
            //setting up the recurring object
            if (srv.EditableEvent != null && srv.EditableEvent.RecurrenceIsEnabled) {
                var EventRecurring = {};
                EventRecurring.StartDate = payload.start;
                EventRecurring.EndDate = payload.end;
                if (srv.EditableRecurringEvent.endBy == 2) {
                    EventRecurring.EndsAfterXOccurrences = srv.EditableRecurringEvent.endsAfterOccurances;
                }
                if (srv.EditableRecurringEvent.endBy == 3) {
                    EventRecurring.EndsOn = srv.EditableRecurringEvent.endByDate;
                    EventRecurring.EndDate = srv.EditableRecurringEvent.endByDate;
                }
                if (srv.EditableRecurringEvent.recurrancepattern == 1) {
                    EventRecurring.PatternEnum = 'Daily';
                }
                if (srv.EditableRecurringEvent.recurrancepattern == 2) {
                    EventRecurring.PatternEnum = 'Weekly';
                    var arr = [];
                    EventRecurring.DayOfWeekEnum = 0;
                    if (srv.EditableRecurringEvent.days.Sunday) {
                        arr.push('Sunday');
                        EventRecurring.DayOfWeekEnum = EventRecurring.DayOfWeekEnum + 1
                    }
                    if (srv.EditableRecurringEvent.days.Monday) {
                        arr.push('Monday');
                        EventRecurring.DayOfWeekEnum = EventRecurring.DayOfWeekEnum + 2
                    }
                    if (srv.EditableRecurringEvent.days.Tuesday) {
                        arr.push('Tuesday');
                        EventRecurring.DayOfWeekEnum = EventRecurring.DayOfWeekEnum + 4
                    }
                    if (srv.EditableRecurringEvent.days.Wednesday) {
                        arr.push('Wednesday');
                        EventRecurring.DayOfWeekEnum = EventRecurring.DayOfWeekEnum + 8
                    }
                    if (srv.EditableRecurringEvent.days.Thursday) {
                        arr.push('Thursday');
                        EventRecurring.DayOfWeekEnum = EventRecurring.DayOfWeekEnum + 16
                    }
                    if (srv.EditableRecurringEvent.days.Friday) {
                        arr.push('Friday');
                        EventRecurring.DayOfWeekEnum = EventRecurring.DayOfWeekEnum + 32
                    }
                    if (srv.EditableRecurringEvent.days.Saturday) {
                        arr.push('Saturday');
                        EventRecurring.DayOfWeekEnum = EventRecurring.DayOfWeekEnum + 64
                    }
                    //EventRecurring.DayOfWeekEnum = arr.join('|');
                }
                if (srv.EditableRecurringEvent.recurrancepattern == 3) {
                    EventRecurring.PatternEnum = 'Monthly';

                    if (srv.EditableRecurringEvent.flagMonthly == 1) {
                        EventRecurring.DayOfMonth = srv.EditableRecurringEvent.dayoftheMonth;
                    }
                    if (srv.EditableRecurringEvent.flagMonthly == 2) {
                        EventRecurring.DayOfWeekIndex = srv.EditableRecurringEvent.WeekOfTheMonth;
                        EventRecurring.DayOfWeekEnum = srv.EditableRecurringEvent.DayOfWeek;
                    }
                }
                if (srv.EditableRecurringEvent.recurrancepattern == 4) {
                    EventRecurring.PatternEnum = 'Yearly';
                    EventRecurring.MonthEnum = srv.EditableRecurringEvent.yearlyMontRepeatsOn;

                    if (srv.EditableRecurringEvent.flagYearly == 1) {
                        EventRecurring.DayOfMonth = srv.EditableRecurringEvent.dayoftheMonth;
                    }
                    if (srv.EditableRecurringEvent.flagYearly == 2) {
                        EventRecurring.DayOfWeekIndex = srv.EditableRecurringEvent.WeekOfTheMonth;
                        EventRecurring.DayOfWeekEnum = srv.EditableRecurringEvent.DayOfWeek;
                    }

                }

                EventRecurring.RecursEvery = srv.EditableRecurringEvent.occurs;
                payload.EventRecurring = EventRecurring;
            }
            payload.AddRecurrence = true;



            // Leadid is not assigned for some reason.
            payload.LeadId = srv.selectedLeadId;

            if (isAllExists == false) {
                
                var res = srv.PostCalendar(payload, modelId, type);
                return (res);
            } else {

                var res = srv.PostCalendar(payload, modelId, type);
                return (res);

            }

        };

        // Callback function to notify the caller on sucess,
        srv.SuccessCallback;
        srv.CancelCallback;

        //srv.RemoveRecurrence=function(modelId, modal)

        srv.PostCalendar = function (payload, modelId, type) {

            var loadingElement = document.getElementById("loading");
            loadingElement.style.display = "block";
            var IsOrderAppointment = true;
            $http.post('/api/calendar/', JSON.stringify(payload)).then(function (response) {


                loadingElement.style.display = "none";
                $("#persongo").modal("hide");
                HFC.DisplaySuccess(response.statusText);
                //IsConvertLead : it is defined to check whether do we need to convert the lead to account or not
                if (type === 'ConvertLead') {

                    srv.CovertToAccountAndOpportunity();
                }
                else if (type === 'returnToPage' && srv.sectionId2 == undefined) {
                    window.location = '#!/' + srv.pathToReturn + '/' + srv.sectionId;
                }
                else if (type === 'returnToPage' && srv.sectionId2) {
                    window.location = '#!/' + srv.pathToReturn + '/' + srv.sectionId + '/' + srv.sectionId2;
                }
                if (srv.SuccessCallback) {
                    srv.SuccessCallback();
                }

                if (srv.callbackForEvent) {
                    srv.callbackForEvent();


                    return (response.statusText);
                }


            }, function (response) {

                loadingElement.style.display = "none";
                HFC.DisplayAlert(response.statusText);
                //Commented due to ReferenceError: $scope is not defined
                //$scope.IsBusy = false;
                srv.IsBusy = false;
                return (response.statusText);

            });
        }

        srv.PostTask = function (payload) {
            
            var loadingElement = document.getElementById("loading");
            loadingElement.style.display = "block";
            
            $http.post('/api/Tasks/', payload).then(function (response) {
                loadingElement.style.display = "none";
                $("#persongo").modal("hide");
                HFC.DisplaySuccess(response.statusText);

                if (payload.VendorCaseId > 0 && payload.VendorCaseId) {
                    srv.VendorCaseTaskCallback();
                }
                else {
                    if (srv.SuccessCallback) {
                        srv.SuccessCallback();
                    }
                    if (srv.callbackForTask) {
                        srv.callbackForTask();
                    }
                }


                return (response.statusText);

            }, function (response) {
                loadingElement.style.display = "none";
                HFC.DisplayAlert(response.statusText);
                return (response.statusText);
                //Commented due to ReferenceError: $scope is not defined
                //$scope.IsBusy = false;
                srv.IsBusy = false;
            });
        }

        srv.CancelCalendar = function (modalid) {
            srv.returntoSectionDisplay = null;
            srv.IsConvertLead = false;
            
          
            $("#persongo").modal("hide");
            if (srv.CancelCallback) {
                srv.CancelCallback();
            }
            var myDiv = $('.taskevent_popup');
            myDiv[0].scrollTop = 0;
        }

        srv.SaveSendToServer = function (promise, payload) {

            promise.then(function (res) {
                srv.IsBusy = false;
                if (dispositionId != null) {

                }
                // lookup AssignedName
                var assignedPerson = $.grep(HFCService.Users, function (u) {
                    return u.PersonId == srv.EditableEvent.AssignedPersonId;
                });

                if (assignedPerson && assignedPerson.length > 0) {
                    srv.EditableEvent.AssignedName = assignedPerson[0].FullName;
                }

                if (!srv.EditableEvent.id && res.data.id != srv.EditableEvent.id)
                    srv.EditableEvent.id = res.data.id;

                if (res.data.calendar) {
                    srv.EditableEvent.AdditionalPeople = [];
                    for (var l = 0; l < res.data.calendar.Attendees.length; l++) {
                        srv.EditableEvent.AdditionalPeople.push(res.data.calendar.Attendees[l].PersonEmail);
                    }

                    srv.EditableEvent.RevisionSequence = res.data.calendar.RevisionSequence;

                } else if (res.data.task) {
                    srv.EditableEvent.AdditionalPeople = [];
                    for (var l = 0; l < res.data.task.Attendees.length; l++) {
                        srv.EditableEvent.AdditionalPeople.push(res.data.task.Attendees[l].PersonEmail);
                    }

                    srv.EditableEvent.RevisionSequence = res.data.task.RevisionSequence;
                }

                if (srv.EditableEvent.AptTypeEnum) {
                    if (srv.EventIndex >= 0) {
                        for (var j = 0; j < srv.CalendarEvents.length; j++) {
                            if (srv.CalendarEvents[j].id == srv.EditableEvent.id) {

                                srv.EditableEvent.start = res.data.calendar.StartDate;
                                srv.EditableEvent.end = res.data.calendar.EndDate;

                                srv.CalendarEvents[j] = srv.EditableEvent;
                            }
                        }
                    } else
                        srv.CalendarEvents.push(srv.EditableEvent);
                } else {
                    if (srv.EventIndex >= 0) {
                        for (var k = 0; k < srv.Tasks.length; k++) {
                            if (srv.Tasks[k].id == srv.EditableEvent.id) {
                                srv.Tasks[k] = srv.EditableEvent;
                            }
                        }
                    } else
                        srv.Tasks.push(srv.EditableEvent);
                }
            }, error);
        }

        srv.CovertToAccountAndOpportunity = function (modalid, modal) {
            srv.IsConvertLead = false;
            srv.selectedSalesAgent = [];

            $http.get('/api/Lookup/' + srv.selectedLeadId + '/GetLeadByLeadId').then(function (data) {
                //
                // srv.leadData = data.data;
                srv.LeadName = data.data.LastName;

            });
            $http.get('/api/opportunities/0/GetSalesAgents').then(function (data) {
                srv.salesAgentlst = data.data;
                srv.selectedSalesAgent = [];
                srv.selectedIds;
                for (var i = 0; i < srv.selectedIds.length; i++) {
                    for (var j = 0; j < srv.salesAgentlst.length ; j++) {
                        if (srv.selectedIds[i] == srv.salesAgentlst[j].Email) {
                            srv.selectedSalesAgent.push(srv.salesAgentlst[j].SalesAgentId);
                        }
                    }
                }
                srv.GetMatchedAccount();
            });



        }
        srv.GetMatchedAccount = function () {
            $http.get('/api/search/' + srv.selectedLeadId + '/GetLeadMatchingAccounts')
              .then(function (data) {

                  srv.matchedAccount = data.data;
                  srv.LeadConvertToAccountAndOpportunity();
              });
        }
        srv.LeadConvertToAccountAndOpportunity = function () {
            //


            var leadId = srv.selectedLeadId;
            var selectedaccountid = 0;
            var addnewaccount = 0;
            var addopportunity = 0;
            if (srv.selectedSalesAgent.length == 1) {
                var currentDate = new Date();
                currentDate = currentDate.toLocaleDateString();
                var data = { leadId: leadId, selectedaccountid: selectedaccountid, addnewaccount: addnewaccount, addopportunity: addopportunity }
                data.opportunityName = srv.LeadName + ' ' + currentDate;
                data.SalesAgentId = srv.selectedSalesAgent[0];
                if (srv.matchedAccount.length == 0) {
                    data.addnewaccount = 1;
                    data.addopportunity = 1;
                    $http.put('/api/leads/1/ConvertSelectLead', data).then(function (response) {
                        HFC.DisplaySuccess("Lead converted");
                        var newaccountId = response.data.newaccountId;

                        if (newaccountId > 0) {
                            var goToAccountsPage = '#!/Accounts/' + newaccountId;
                            $window.open(goToAccountsPage, '_self');
                        }
                        //Commented due to ReferenceError: $scope is not defined
                        //$scope.IsBusy = false;
                        srv.IsBusy = false;
                    });
                }
                    //else if (srv.matchedAccount.length == 1) {
                    //    data.addnewaccount = 0;
                    //    data.addopportunity = 1;
                    //    $http.put('/api/leads/1/ConvertSelectLead', data).then(function (response) {
                    //        HFC.DisplaySuccess("Lead converted");
                    //        var newaccountId = response.data.newaccountId;

                    //        if (newaccountId > 0) {
                    //            var goToAccountsPage = '#!/Accounts/' + newaccountId;
                    //            $window.open(goToAccountsPage, '_self');
                    //        }
                    //        $scope.IsBusy = false;
                    //    });
                    //}
                else {
                    window.location = '#!/leadConvert/' + srv.selectedLeadId;
                }

            }
            else {
                window.location = '#!/leadConvert/' + srv.selectedLeadId;
            }
        }








        srv.SetCompletedDate = function () {
            srv.EditableEvent.CompletedDate = srv.EditableEvent.CompletedDate ? null : new Date();
        }


        srv.CompleteTask = function (moaldId, TaskID, callback) {
            

            // Only assign if the callback points to a valid pointer
            // otherwise keep the already registered callback alive. --murugan
            if (callback) srv.callbackForTask = callback;

            var Id = TaskID;
            var markurl = '/api/Tasks/' + Id + '/Mark?IsComplete=true';
            $http.get(markurl).then(function (response) {
                var res = response.data;
                
                if (res) {
                    
                    var saveStatus = "Successful";
                    HFC.DisplaySuccess(saveStatus);
                    if (srv.SuccessCallback) {
                        srv.SuccessCallback();
                    }

                    if (srv.callbackForTask) {
                        srv.callbackForTask();
                    }
                }
                else {
                    HFC.DisplayAlert("Error");
                }
            });
            $("#" + moaldId).modal("hide");
        }

        srv.CompleteTaskVendor = function (moaldId, TaskID, callback) {
            
            srv.callbackForTask = callback;
            var Id = TaskID;
            var markurl = '/api/Tasks/' + Id + '/Mark?IsComplete=true';
            $http.get(markurl).then(function (response) {
                var res = response.data;
                
                if (res) {

                    if (srv.callbackForTask) {
                        srv.callbackForTask();
                    }
                }
                else {
                    HFC.DisplayAlert("Error");
                }
            });
            $("#" + moaldId).modal("hide");
        }

        //srv.SetCompletedDate = function () {
        //    srv.EditableEvent.CompletedDate = srv.EditableEvent.CompletedDate ? null : new Date();
        //}

        srv.InComplete = function (moaldId, TaskID, taskSuccessCallback) {
            
            srv.callbackForTask = taskSuccessCallback;
            var Id = TaskID;
            var markurl = '/api/Tasks/' + Id + '/Mark?IsComplete=false';
            $http.get(markurl).then(function (response) {
                var res = response.data;
                if (res) {
                    
                    var saveStatus = "Successful";
                    HFC.DisplaySuccess(saveStatus);
                    if (srv.SuccessCallback) {
                        srv.SuccessCallback();
                    }

                    if (srv.callbackForTask) {
                        srv.callbackForTask();
                    }
                }
                else {
                    HFC.DisplayAlert("Error");
                }
            });
            $("#" + moaldId).modal("hide");
        }



        srv.InCompleteTaskVendor = function (moaldId, TaskID, taskSuccessCallback) {
            
            srv.callbackForTask = taskSuccessCallback;
            var Id = TaskID;
            var markurl = '/api/Tasks/' + Id + '/Mark?IsComplete=false';
            $http.get(markurl).then(function (response) {
                var res = response.data;
                if (res) {
                    
                    var saveStatus = "Successful";
                    HFC.DisplaySuccess(saveStatus);

                    if (srv.callbackForTask) {
                        srv.callbackForTask();
                    }
                }
                else {
                    HFC.DisplayAlert("Error");
                }
            });
            $("#" + moaldId).modal("hide");
        }





    }
        ]) //;
        .service('AttendeeService', [
    '$http', 'CacheService', function ($http, CacheService) {
        var srv = this;

        srv.IsBusy = false;

        srv.SelectedSources = [];

        srv.Sources = [];

        srv.Attendees = [];

        srv.EditorSelection = [];

        srv.LeadId = false;

        srv.AddOrEdit = function (modalId, index) {


            $("#" + modalId).modal("show");
        };

        srv.GetAttendees = function (jobCount) {

            var result = [];

            if (srv.Attendees.length > 1) {
                result.push({ SourceId: 9999 });
            }

            if (jobCount > 1) {
                var RepeatID = 0;
                RepeatID = (HFC.Util.BrandedPath != "/brand/tl") ? 22 : 21; /// The Repeat SourceID on TL is 21, on BB is 22;
                if (srv.Attendees.map(function (e) { return e.SourceId; }).indexOf(RepeatID) == -1) {
                    result.push({ SourceId: RepeatID });
                }
            }

            for (var i = 0; i < srv.Attendees.length; i++) {
                result.push(srv.Attendees[i]);
            }

            return result;
        };

        srv.GetSource = function (sourceId, sources, path) {
            if (!sources || sources.length == 0) {
                return false;
            }

            for (var i = 0; i < sources.length; i++) {
                if (sources[i].SourceId == sourceId) {
                    sources[i].path = path;
                    return sources[i];
                } else {

                    if (sources[i].Children && sources[i].Children.length > 0) {
                        var result = srv.GetSource(sourceId, sources[i].Children, path + sources[i].Name + " > ");
                        if (result) {
                            return result;
                        }
                    }
                }
            }

            return false;
        };

        srv.GetLabel = function (srcId, excludeMargin) {
            var path = "";
            var src = srv.GetSource(srcId, srv.Sources, path);
            return src;
        };

        srv.AddSource = function (leadId) {
            srv.Attendees.push({
                LeadId: leadId,
                SourceId: null,
                IsManuallyAdded: true
            });
        };

        srv.Delete = function (src) {
            if (!srv.LeadId) {
                remove(src);
                return;
            };

            if (srv.IsBusy)
                return false;
            var exists = srv.CheckIfSourceUsed(src.SourceId);
            if (exists) {
                HFC.DisplayAlert("Source selected to remove is already used in a job for this lead");
                return false;
            }

            if (src.AttendeeId) {
                srv.IsBusy = true;
                //validate if used
                $http.delete('/api/leads/' + src.AttendeeId + '/source/').then(function (response) {
                    remove(src);
                    srv.IsBusy = false;
                }, function (response) {
                    HFC.DisplayAlert(response.statusText);
                    srv.IsBusy = false;
                });
            } else
                remove(src);
        };

        srv.Update = function (sources) {
            if (!srv.LeadId) return;

            if (srv.IsBusy)
                return false;

            srv.IsBusy = true;

            $http.post('/api/leads/' + srv.LeadId + '/source/', sources).then(function (response) {
                HFC.DisplaySuccess("Lead Source(s) saved");
                srv.IsBusy = false;
            }, function (response) {
                HFC.DisplayAlert(response.statusText);
                srv.IsBusy = false;
            });
        }

        srv.SelectSource = function (node) {
            if (node.Selected) { // select
                srv.EditorSelection.push(node.SourceId);
            } else {

                for (var i = 0; i < srv.EditorSelection.length; i++) {
                    if (srv.EditorSelection[i] == node.SourceId) {
                        srv.EditorSelection.splice(i, 1);
                    }
                }
            }

            console.log(srv.EditorSelection);
        }

        srv.GetEditorData = function (data) {
            var filter = function (nodes) {
                var result = [];
                if (!nodes || nodes.length == 0) return [];
                for (var i = 0; i < nodes.length; i++) {
                    if (!nodes[i].isDeleted) {
                        var n = nodes[i];
                        if (n.Children) {
                            n.Children = filter(n.Children);
                        }

                        result.push(n);
                    }
                }

                return result;
            }

            return filter(data);
        }

        srv.Get = function (leadId) {
            srv.LeadId = leadId;
            srv.IsBusy = true;
            $http.get('/api/leads/' + leadId + '/source/').then(function (response) {

                srv.Sources = [];
                srv.EditorData = false;
                CacheService.Get('Source', function (items) {
                    srv.Sources = items;
                });

                srv.Attendees = response.data.Attendees;
                srv.GetSelectedSources(leadId);

            }, function (response) {
                HFC.DisplayAlert(response.statusText);
            });

            srv.IsBusy = false;
        }
        srv.GetSelectedSources = function (leadId) {
            //Selected Sources 
            srv.IsBusy = true;
            $http.get('/api/leads/' + leadId + '/SelectedSources/').then(function (response) {
                srv.SelectedSources = [];
                srv.EditorData = false;
                CacheService.Get('SelectedSources', function (items) {
                    srv.SelectedSources = items;
                });

                srv.SelectedSources = response.data.SelectedSources;
            }, function (response) {
                HFC.DisplayAlert(response.statusText);
            });
            srv.IsBusy = false;
        }
        srv.CheckIfSourceUsed = function (sourceId) {

            if (srv.SelectedSources.length > 0) {
                var found = false;
                for (var i = 0; i <= srv.SelectedSources.length - 1; i++) {
                    if (srv.SelectedSources[i] == sourceId) {
                        found = true;
                    }
                }
                return found;
            }
            else {
                return false;
            }
        }

        srv.ShowEditor = function (modalId, index, source, $event, AttendeeService) {
            if ($event) $event.preventDefault();
            //$http.get('/api/leads/' + srv.LeadId + '/source/').then(function (response) {

            //srv.Sources = null;


            //load selected sources while the dialog to select a new source
            // AttendeeService.GetSelectedSources(srv.LeadId);
            console.log("hello Attendee Service");

            CacheService.Invalidate('source');
            if (source) {
                //sourceId = sourceId.target.form[23].selectedIndex;
                var selectedsourceId = source.$parent.AttendeesList.Id.SourceId;
                $http.get('/api/lookup/' + selectedsourceId + '/Type_Source').then(function (data) {
                    srv.Sources = data.data;
                });


                srv.EditorSelection = [];

                for (var i = 0; i < srv.Attendees.length; i++) {
                    srv.EditorSelection.push(srv.Attendees[i].SourceId);
                }

                // if (!srv.EditorData) {
                srv.EditorData = srv.GetEditorData(srv.Sources);
                //}

                srv.EditorDataExpandedNodes = [];

                // restoreSelection(srv.EditorData);

                $("#" + modalId).modal("show");
            }
            else {
                CacheService.Get('Source', function (items) {

                    srv.Sources = items;

                    srv.EditorSelection = [];

                    for (var i = 0; i < srv.Attendees.length; i++) {
                        srv.EditorSelection.push(srv.Attendees[i].SourceId);
                    }

                    // if (!srv.EditorData) {
                    srv.EditorData = srv.GetEditorData(srv.Sources);
                    //}

                    srv.EditorDataExpandedNodes = [];

                    restoreSelection(srv.EditorData);

                    $("#" + modalId).modal("show");

                });
            }
            //}, function (response) {
            //    HFC.DisplayAlert(response.statusText);

            //});

        };

        srv.CancelAttendeeEditor = function (modalId) {
            $("#" + modalId).modal("hide");
        }

        var restoreSelection = function (nodes) {
            if (!nodes || nodes.length == 0) {
                return;
            }
            for (var i = 0; i < nodes.length; i++) {
                restoreSelection(nodes[i].Children);
                if (srv.EditorSelection.indexOf(nodes[i].SourceId) > -1) {
                    nodes[i].Selected = true;
                    expandPath(nodes[i]);
                } else {
                    nodes[i].Selected = false;
                }
            }
        };

        var expandPath = function (node) {
            srv.EditorDataExpandedNodes.push(node);

            if (node.ParentId) {
                var parent = srv.GetSource(node.ParentId, srv.Sources);
                expandPath(parent);
            }
        }

        var resetSelection = function (node) {
            node.Selected = false;
            if (!node.Children) {
                return;
            }

            for (var i = 0; i < node.Children.length; i++) {
                resetSelection(node.Children[i]);
            }
        }

        srv.SaveEditor = function (modalId) {


            for (var i = 0; i < srv.EditorData.length; i++) {
                resetSelection(srv.EditorData[i]);
            };

            srv.Attendees = [];

            var selectedSources = [];

            for (var j = 0; j < srv.EditorSelection.length; j++) {
                selectedSources.push(srv.GetSource(srv.EditorSelection[j], srv.Sources));
            }

            var sourcesToUnselect = [];

            for (var j = 0; j < selectedSources.length; j++) {
                if (selectedSources[j].ParentId > 0) {
                    var selectedParent = $.grep(selectedSources, function (em) {
                        return em.SourceId == selectedSources[j].ParentId;
                    });

                    if (selectedParent && selectedParent.length > 0) {
                        sourcesToUnselect.push(selectedParent[0].SourceId);
                    }
                }
            }

            if (sourcesToUnselect.length > 0) {
                for (var k = 0; k < sourcesToUnselect.length; k++) {
                    var index = srv.EditorSelection.indexOf(sourcesToUnselect[k]);
                    if (index >= 0) srv.EditorSelection.splice(index, 1);
                }
            }

            srv.Update(srv.EditorSelection);

            for (var j = 0; j < srv.EditorSelection.length; j++) {
                srv.Attendees.push({ LeadId: srv.LeadId, SourceId: srv.EditorSelection[j] });
            }

            $("#" + modalId).modal("hide");
        }

        function remove(item) {
            var index = srv.Attendees.indexOf(item);
            if (index > 0) {
                srv.Attendees.splice(index, 1);
            } else if (index === 0) {
                srv.Attendees.shift();
            }
        }

    }
        ]);

})(window, document);
