﻿'use strict';

app.service('PaymentInvoiceService', [
    '$http', 'CacheService',  'HFCService',
    function ($http, CacheService, HFCService) {

        var srv = this;

        
        srv.IsBusy = false;

        srv.Payment = null;
        srv.submitAction = null;
        srv.OrderId = null;
        srv.Sendemail = null;
        srv.InvoiceOption = null;
        srv.InvoiceFormatOptions = 0;
        srv.CancelEditor = function (modalId) {
            $("#" + modalId).modal("hide");
            
        };
        srv.ShowInvoice = function (modalId) {
            $("#" + modalId).modal("show");

        };


        //email pop-up
        srv.SummayEmail = function (value) {
            
            srv.Sendemail = value;
            $("#Accept_ordersummary").modal("hide");
            srv.SavePayment();
        }
        srv.NoSummaryEmail = function (value) {
            
            srv.Sendemail = value;
            $("#Accept_ordersummary").modal("hide");
            srv.SavePayment();
        }
        srv.Close = function (emailmodelid) {
            var modalId = "PaymentInvoice";
            $("#" + modalId).modal("hide");
            $("#Accept_ordersummary").modal("hide");
        }
        ///

        srv.CreateInvoice = function (modalId) {
            $("#" + modalId).modal("hide");
            var dto = srv.Payment; 
            if (dto.IsNotifyemails == false) {
                srv.SavePayment();
            }
            else
            $("#Accept_ordersummary").modal("show");
         }


        srv.SavePayment = function () {
            var dto = srv.Payment;
            dto.Sendemail = srv.Sendemail;
            if (srv.InvoiceFormatOptions == null) {
                HFC.DisplayAlert("Required Invoice Format Option ");
                return;
            }
            if (srv.InvoiceFormatOptions == 1) {
                dto.IncludeDiscount = true;
            }
            else {
                dto.IncludeDiscount = false;
            }
            var loadingElement = document.getElementById("loading");
            loadingElement.style.display = "block";
            $http.post('/api/Payments', dto).then(function (response) {
                srv.IsBusy = false;

                if (srv.OrderId > 0) {
                    HFC.DisplaySuccess("Payment Created");
                }

                if (srv.submitAction == 1) {
                    srv.Payment = null;
                    window.location = '#!/Orders/' + srv.OrderId;
                }
                else if (srv.submitAction == 3) {
                    srv.Payment = null;
                    window.location = '#!/orderSearch/';
                }
                loadingElement.style.display = "none";
            }, function (response) {
                // Fix for : TP-1553	Adding Payment = spiny wheel
                loadingElement.style.display = "none";
                HFC.DisplayAlert(response.statusText);
                srv.IsBusy = false;
            });
            //$("#" + modalId).modal("hide");
        }
		srv.GetInvoicePrintOptions=function()
		{
		    //Get Invoice Options from the Franchise
		    if (srv.OrderId == null)
		    {
		        var Id = 0;
		    }
		    else
		    {
		        var Id = srv.OrderId;
		    }
		    $http.get('/api/Payments/'+ Id +'/FranchiseInvoiceOptions').then(function (response) {
		        srv.InvoiceFormatOptions = response.data;
		        if(srv.InvoiceFormatOptions==false)
		        {
		            srv.InvoiceFormatOptions = 1;
		        }
		        else
		        {
		            srv.InvoiceFormatOptions = 2;
		        }
		    });
		}
		srv.GetInvoicePrintOptions();
	}
]);


