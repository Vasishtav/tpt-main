﻿
'use strict';

app.service('PrintService', [
    '$http', '$routeParams', '$window', function ($http, $routeParams, $window) {
        var srv = this;

        srv.IsBusy = false;
        srv.MPOPrintOptions = {
            MPODetails: false,
            VPODetails: false
        };
        srv.PrintOptions = {
            AddleadCustomer: false
        };
        srv.PrintOrderOptions = {
        };


        function getActiveXObject(name) {
            try {
                return new ActiveXObject(name);
            } catch (e) {
            }
        };

        function getNavigatorPlugin(name) {
            for (var key in navigator.plugins) {
                var plugin = navigator.plugins[key];
                if (plugin.name == name) return plugin;
            }
        };

        function getPDFPlugin() {
            var userAgent = navigator ? navigator.userAgent.toLowerCase() : "other";

            if (userAgent.indexOf("msie") > -1) {
                //
                // load the activeX control
                // AcroPDF.PDF is used by version 7 and later
                // PDF.PdfCtrl is used by version 6 and earlier
                return getActiveXObject('AcroPDF.PDF') || getActiveXObject('PDF.PdfCtrl');
            } else {
                return getNavigatorPlugin('Adobe Acrobat') || getNavigatorPlugin('Chrome PDF Viewer') || getNavigatorPlugin('WebKit built-in PDF');
            }
        };

        function _injectIframe() {
            if (document.getElementById("print_frame")) {
                //   document.getElementById("print_frame").remove();
            }

            if (document.getElementById("print_frame") == null) {
                var frame = document.createElement("iframe");
                frame.id = "print_frame";
                frame.width = 0;
                frame.height = 0;
                frame.frameBorder = 0;
                $(".wrapper").append(frame);
            }
        }

        function _Print(src) {
            window.open(src, '_blank');
        };


        //var pdfPlugin = getPDFPlugin();
        //if (pdfPlugin) {
        //    _injectIframe();

        srv.PrintQuotee = function (QuoteKey) {

            var url = '/export/PrintQuote/' + QuoteKey + '/?printOptions=IncludeDiscount'; // + '/?type=PDF&inline=true&t=' + ticks;
            _Print(url);

        };



        srv.PrintQuotee_short = function (QuoteKey) {

            //var url = '/export/PrintQuote_short/' + QuoteKey; // + '/?type=PDF&inline=true&t=' + ticks;
            var url = '/export/PrintQuote/' + QuoteKey + '/?printOptions=ExcludeDiscount';
            _Print(url);

        };

        srv.PrintQuotee_condensed = function (QuoteKey) {

            //var url = '/export/PrintQuote_short/' + QuoteKey; // + '/?type=PDF&inline=true&t=' + ticks;
            var url = '/export/PrintQuote/' + QuoteKey + '/?printOptions=CondensedVersion';
            _Print(url);

        };

        srv.PrintQuote = function (quoteId) {

            var ticks = (new Date()).getTime();
            var url = '/export/quote/' + quoteId + '/?type=PDF&inline=true&t=' + ticks;

            _Print(url);

        },
            srv.PrintInvoice = function (invoiceId) {

                var ticks = (new Date()).getTime();
                var url = '/export/invoice/' + invoiceId + '/?type=PDF&inline=true&t=' + ticks;

                //var url = '/export/invoice/' + invoiceId + '/?type=PDF&inline=true&t=' + ticks+'LeadCustomer='+;

                _Print(url);

            }



        srv.PrintOrderInvoice = function (OrderId, invoiceId) {

            var ticks = (new Date()).getTime();
            var url = '/export/OrderInvoice/' + invoiceId + '/?type=PDF&inline=true&t=' + ticks;

            _Print(url);

        }
        //added code for the purchase order print
        srv.PrintPurchaseOrder = function (data, data2) {

            var ticks = (new Date()).getTime();
            var url = '/export/PurchasePrint/0/?type=PDF&inline=true&t=' + ticks;

            _Print(url);

        }

        srv.PrintMPO = function (orderId) {

            var ticks = (new Date()).getTime();
            var url = '/export/PrintMPO/' + orderId + '?printVPO=true&type=PDF';

            _Print(url);

        }
        //for invoice print
        srv.PrintInvoice = function (invoiceId) {

            var ticks = (new Date()).getTime();
            var url = '/export/PrintVendorInvoice?invoiceNumber=' + invoiceId;

            _Print(url);

        }

        srv.PrintShipnotice = function (ShipId) {

            var ticks = (new Date()).getTime();
            var url = '/export/PrintShipnotice/' + ShipId + '?printship=true&type=PDF';

            _Print(url);

        }

        srv.DownloadOrderInvoice = function (OrderId, invoiceId) {

            var ticks = (new Date()).getTime();
            var url = '/export/OrderInvoice/' + invoiceId + '/?type=PDF&inline=false&t=' + ticks;

            _Print(url);

        }

        srv.PrintInstallationPacket = function (OrderId, PrintOrderOptions, instalOptions) {

            var ticks = (new Date()).getTime();

            var options = '';

            options = options + 'AddleadCustomer=' + PrintOrderOptions.AddCustomerOpportunity;
            options = options + '/AddGoogleMap=' + PrintOrderOptions.AddGoogleMap;
            options = options + '/AddPrintExistingMeasurements=' + PrintOrderOptions.AddExistingMeasurements;
            options = options + '/AddPrintInternalNotes=' + PrintOrderOptions.AddPrintInternalNotes;
            options = options + '/AddPrintExternalNotes=' + PrintOrderOptions.AddPrintExternalNotes;
            options = options + '/AddPrintDirectionNotes=' + PrintOrderOptions.AddPrintDirectionNotes;
            //options = options + '/AddOrder=' + PrintOrderOptions.AddOrder;
            options = options + '/AddInstallationChecklist=' + PrintOrderOptions.AddInstallationChecklist;
            options = options + '/AddWarrantyInformation=' + PrintOrderOptions.AddWarrantyInformation;
            //options = options + '/PrintOrder=true';
            options += '/AddInstallationProcess=' + PrintOrderOptions.AddInstallationProcess;
            options += '/AddOrderCustomerInvoice=' + PrintOrderOptions.AddOrderCustomerInvoice;
            options += '/AddOrderInstallerInvoice=' + PrintOrderOptions.AddOrderInstallerInvoice;
            options += '/OrderPrintFormat=' + PrintOrderOptions.OrderPrintFormat;
            options += '/PrintOrder=true';
            // mydocuments
            options += '/' + PrintOrderOptions.MyDocuments;

            var url = '/export/InstallationPacket/' + OrderId + '/?type=PDF&inline=true&t=' + ticks + '&printoptions=' + options + '&installPacketOptions=' + instalOptions;

            _Print(url);

        }


        srv.PrintLeadSheet = function (leadId, accountId, opportunityId) {

            var accountId;
            var leadId;
            var opportunityId;
            var measurementId;
            var printFlag = '';

            //Lead Id take from Route Param (url parameter)
            //if ($routeParams.leadId != null && $routeParams.leadId != undefined) {
            //    leadId = $routeParams.leadId;
            //    printFlag = 'Lead';
            //}
            //else if (leadId == undefined && srv.leadId != undefined && srv.leadId > 0) {
            //    leadId = srv.leadId;
            //    printFlag = 'Lead';
            //}
            //else if (leadId == undefined && srv.leadId != undefined && srv.leadId > 0) {
            //    leadId = $routeParams.OpportunityId;
            //    printFlag = 'Measurement';
            //}
            //if ($routeParams.accountId != null && $routeParams.accountId != undefined) {
            //    accountId = $routeParams.accountId;
            //    printFlag = 'Account';
            //}

            //Lead Search
            if (srv.leadId != null && srv.leadId != 0) {
                leadId = srv.leadId;
                printFlag = 'Lead';
            }
            //AccountSearch
            if (srv.accountId != null && srv.accountId != 0) {
                accountId = srv.accountId;
                printFlag = 'Account';
            }
            if (srv.opportunityId != null && srv.opportunityId != 0) {
                opportunityId = srv.opportunityId;
                if (srv.measurementId == null) {
                    printFlag = 'Opportunity';
                }
                else {
                    printFlag = 'Measurement';
                }
            }
            if (srv.measurementId != null && srv.measurementId != 0) {
                measurementId = srv.opportunityId;
                printFlag = 'Measurement';
            }

            if (printFlag == 'Lead' || printFlag == 'Account' || printFlag == 'Opportunity' || printFlag == 'Measurement') {
                if (srv.PrintOptions.AddleadCustomer == false &&
                srv.PrintOptions.AddGoogleMap == false &&
                srv.PrintOptions.AddMeasurementForm == false &&
                srv.PrintOptions.AddGarageMeasurementForm == false &&
                srv.PrintOptions.AddClosetMeasurementForm == false &&
                srv.PrintOptions.AddOfficeMeasurementForm == false &&
                srv.PrintOptions.AddFloorMeasurementForm == false &&
                srv.PrintOptions.AddFCCMeasurementForm == false &&
                srv.PrintOptions.AddPrintExistingMeasurements == false &&
                srv.PrintOptions.AddPrintInternalNotes == false &&
                srv.PrintOptions.AddPrintExternalNotes == false &&
                srv.PrintOptions.AddPrintDirectionNotes == false) {
                    return;
                }
            }

            var options = '';

            options = options + 'AddleadCustomer=' + srv.PrintOptions.AddleadCustomer;
            options = options + '/AddGoogleMap=' + srv.PrintOptions.AddGoogleMap;
            options = options + '/AddMeasurementForm=' + srv.PrintOptions.AddMeasurementForm;
            options = options + '/AddGarageMeasurementForm=' + srv.PrintOptions.AddGarageMeasurementForm;
            options = options + '/AddClosetMeasurementForm=' + srv.PrintOptions.AddClosetMeasurementForm;
            options = options + '/AddFloorMeasurementForm=' + srv.PrintOptions.AddFloorMeasurementForm;
            options = options + '/AddFCCMeasurementForm=' + srv.PrintOptions.AddFCCMeasurementForm;
            options = options + '/AddPrintExistingMeasurements=' + srv.PrintOptions.AddPrintExistingMeasurements;
            options = options + '/AddPrintInternalNotes=' + srv.PrintOptions.AddPrintInternalNotes;
            options = options + '/AddPrintExternalNotes=' + srv.PrintOptions.AddPrintExternalNotes;
            options = options + '/AddPrintDirectionNotes=' + srv.PrintOptions.AddPrintDirectionNotes;
            options = options + '/GarageMeasurementFormQuntity=' + srv.PrintOptions.GarageMeasurementFormQuntity;
            options = options + '/ClosetMeasurementFormQuntity=' + srv.PrintOptions.ClosetMeasurementFormQuntity;
            options = options + '/OfficeMeasurementFormQuntity=' + srv.PrintOptions.OfficeMeasurementFormQuntity;
            options = options + '/FloorMeasurementFormQuntity=' + srv.PrintOptions.FloorMeasurementFormQuntity;
            options = options + '/ConcreteCraftMeasurementFormQuntity=' + srv.PrintOptions.ConcreteCraftMeasurementFormQuntity;
            options = options + '/PrintOrder=fasle';

            if (printFlag == 'Lead') {
                var ticks = (new Date()).getTime();
                var url = '/export/LeadSheet/' + leadId + '/?type=PDF&inline=true&t=' + ticks + '&printoptions=' + options;
                _Print(url);
            }
            else if (printFlag == 'Account') {
                var ticks = (new Date()).getTime();
                var url = '/export/AccountSheet/' + accountId + '/?type=PDF&inline=true&t=' + ticks + '&printoptions=' + options;
                _Print(url);
            }
            else if (printFlag == 'Measurement') {
                var url = '/export/MeasurementSheet/' + measurementId + '/?type=PDF&inline=true&t=' + ticks + '&printoptions=' + options;
                _Print(url);
            }
            else if (printFlag == 'Opportunity') {
                var ticks = (new Date()).getTime();
                var url = '/export/OpportunitySheet/' + opportunityId + '/?type=PDF&inline=true&t=' + ticks + '&printoptions=' + options;
                _Print(url);
            }

        }

        srv.PrintMeasurements = function (measurementId, Garage, Closet, Office, FloorMeasurement, ConcreteCraft) {

            var ticks = (new Date()).getTime();
            if (Garage == undefined) { Garage = false }
            if (Closet == undefined) { Closet = false }
            if (Office == undefined) { Office = false }
            if (FloorMeasurement == undefined) { FloorMeasurement = false }
            if (ConcreteCraft == undefined) { ConcreteCraft = false }

            var url = '/export/MeasurementSheet/' + measurementId + '/?Garage=' + Garage + '&Closet=' + Closet + '&Office=' + Office + '&FloorMeasurement=' + FloorMeasurement + '&ConcreteCraft=' + ConcreteCraft + '&type=PDF&inline=true&t=' + ticks;

            _Print(url);

        }

        srv.PrintSalesAppointmentPacket = function (leadid) {

            leadid = 1;
            var ticks = (new Date()).getTime();
            var url = '/export/LeadSheet/' + leadid + '/?type=PDF&inline=true&t=' + ticks;

            _Print(url);
        }

    }
]);
