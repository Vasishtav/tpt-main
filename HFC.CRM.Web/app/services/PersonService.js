﻿(function (window, document, undefined) {
    'use strict';
    angular.module('hfc.personModule', [])
    .service('PersonService', [
    '$http', 'HFCService', 'AddressService', 'CacheService', 'CalendarService'
    , 'AccountService', 'DropzoneService',
function ($http, HFCService, AddressService, CacheService, CalendarService
    , AccountService, DropzoneService) {
    var srv = this;

    srv.CalendarService = CalendarService;
    srv.AccountService = AccountService;

    // NOTE: These are not used anymore so removing the referrence.-- murugan
    // srv.OpportunityService = OpportunityService;
    // srv.AttendeeService = AttendeeService;
    // srv.NewtaskService = NewtaskService;

    srv.eventdata = null;
    srv.setCalendarValue = function (data) {
        srv.eventdata = data;
    }
    //Callbacks for various options.
    // TODO: it should be moved to corresponding services later.
    srv.QuickDispositionCallBack;

    srv.minDate = new Date();
    srv.maxDate = new Date('2099/12/31');
    srv.goPageIndex = 0;

    srv.People = [];
    srv.Person = null;
    srv.PersonIndex = -1;
    srv.LeadId = null;
    srv.IsBusy = false;
    srv.IsNew = false;

    srv.Noteslist = [];
    srv.Notes = {
        NotesIndex: '',
        Title: '',
        Note: '',
        Category: ''
    };
    srv.NotesIndex = -1;
    srv.IsNewNote = false;

    srv.Attachmentlist = [];
    srv.Attachment = {
        AttachmentIndex: '',
        Title: '',
        FileName: '',
        Category: '',
        FullFileName: ''
    };
    srv.AttachmentIndex = -1;
    srv.IsNewAttachment = false;

    srv.GetPerson = function (personId) {
        var per = $.grep(srv.People, function (p) {
            return p.PersonId == personId;
        });
        if (per)
            return per[0];
        else
            return null;
    };

    srv.Edit = function (personId, modalId, leadId, index) {
        var person = srv.GetPerson(personId);
        var copy = angular.copy(person);
        copy.IsSubscribed = copy.UnsubscribedOnUtc == null;
        srv.Person = copy;
        srv.PersonIndex = parseInt(index);
        srv.LeadId = leadId;
        $("#" + modalId).modal("show");
    };

    srv.updateDisposition = function (leadId, modelcustomer) {
        if (leadId != null) {
            $http.post('/api/leads/' + leadId + '/CustomerLeadUpdate/', modelcustomer).then(function (response) {
                HFC.DisplaySuccess(response.statusText);
                return response.statusText;
                $scope.IsBusy = false;
            }, function (response) {
                HFC.DisplayAlert(response.statusText);
                $scope.IsBusy = false;
                return response.statusText;
            });
        }

    }

    srv.CallendarSuccessCallback;
    srv.CalendarCancelCallback;
    srv.leadredirectCallBack;
    srv.OppertunityYesNoCallBack;

    function CallenderCallback() {

        //If no reference then add default values
        var currentLeadId = srv.Lead.LeadId;
        var customer = srv.Lead.Customer;
        if (currentLeadId == undefined || currentLeadId == null) {
            currentLeadId = srv.CalendarService.leadid;
            srv.Lead.LeadId = srv.CalendarService.leadid;
        }
        if (customer == undefined || srv.Lead.Customer == null) {
            //If Quick disposition value not there then substitute
            var dispositionValue = "7|0|1";
            var modelCustomer = {};
            modelCustomer.Value = dispositionValue;
            customer = modelCustomer;
        }
        //Update the Lead status based on disposition value
        var temp = srv.updateDisposition(currentLeadId, customer);

        // Notify the caller so that they can do whatever you want.
        if (srv.CallendarSuccessCallback) {
            srv.CallendarSuccessCallback();
        }
    }
    function CalendarCallBack2() {
        if (srv.CalendarCancelCallback) {
            srv.CalendarCancelCallback();
        }
    }

    srv.Lead = {};
    srv.Account = {};
    CalendarService.SuccessCallback = CallenderCallback;
    CalendarService.CancelCallback = CalendarCallBack2;
    srv.setDisposition = function (modalId, leadId, dispositionValue) {


        if (!dispositionValue) {
            HFC.DisplayAlert("Please select Disposition.");
            return;
        }

        var fields = dispositionValue.split("|");
        var leadStatusId = fields[0];
        var dispositionId = fields[1];
        var modelIndex = fields[2];

        if (modelIndex == 0) {
            var modelCustomer = {};
            modelCustomer.Value = dispositionValue;
            srv.updateDisposition(leadId, modelCustomer);
            window.location.reload();
        }
        else if (modelIndex == 1) {
            //Set Appointment and go to set values for Disposition and Lead Status - Qualified
            srv.CalendarService.NewLeadApt(modalId, leadId, 'event');
        }
        else {
            modalId = 'newtask';
            //Create Task and go to set values for Disposition and Lead Status - Nurture
            var res = srv.CalendarService.NewLeadApt(modalId, leadId, 'task');
            srv.Lead.LeadId = leadId;

            //var temp = srv.updateDisposition(leadId, modelCustomer);
        }
    }

    srv.AddLeadAppointment = function (modalId, leadId, isTaskEvent) {
        //
        //var address = lead.Addresses[0];
        srv.CalendarService.NewLeadApt(modalId, leadId, isTaskEvent);//lead.PrimCustomer,address, lead.LeadId)
    }
    srv.AddAccountAppointment = function (modalId, accountId, isTaskEvent) {

        //var address = lead.Addresses[0];
        srv.CalendarService.NewAccountApt(modalId, accountId, isTaskEvent);//lead.PrimCustomer,address, lead.LeadId)
    }
    srv.AddOpportunityAppointment = function (modalId, opportunityId, isTaskEvent) {
        srv.CalendarService.NewOpportunityApt(modalId, opportunityId, isTaskEvent);
    }
    srv.AddOrderAppointment = function (modalId, orderId, isTaskEvent) {
        //
        srv.CalendarService.NewOrderApt(modalId, orderId, isTaskEvent);
    }

    srv.AddOrderAppointmentt = function (modalId, orderId, isTaskEvent) {
        //
        srv.CalendarService.NewOrderApt(modalId, orderId, isTaskEvent);
    }
    srv.AddCaseAppointment = function (modalId, orderId, isTaskEvent) {
        //
        
        srv.CalendarService.NewCaseApt(modalId, orderId, isTaskEvent);
    }

    srv.AddVendorCaseAppointment = function (modalId, orderId, isTaskEvent) {
        //
        
        srv.CalendarService.NewVendorCaseApt(modalId, orderId, isTaskEvent);
    }

    function validateEmail(inputEmail) {
        var atpos = inputEmail.indexOf("@");
        var dotpos = inputEmail.lastIndexOf(".");
        if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= inputEmail.length) {
            return false;
        }
        else {
            return true;
        }
    }

    srv.Save = function (modalId, index) {
        srv.Personsubmit = true;
        if (srv.Person && srv.LeadId >= 0 && !srv.IsBusy) {
            srv.IsBusy = true;

            if (srv.Person.FirstName == null || srv.Person.FirstName == "" || srv.Person.FirstName == undefined) {
                HFC.DisplayAlert("Secondary Person First Name Required");
                srv.IsBusy = false;
                return;
            }

            if (srv.Person.LastName == null || srv.Person.LastName == "" || srv.Person.LastName == undefined) {
                HFC.DisplayAlert("Secondary Person Last Name Required");
                srv.IsBusy = false;
                return;
            }

            if (srv.Person.HomePhone != "" && srv.Person.HomePhone.length != 10) {
                HFC.DisplayAlert("Home Phone invalid");
                srv.IsBusy = false;
                return;
            }
            if (srv.Person.CellPhone != "" && srv.Person.CellPhone.length != 10) {
                HFC.DisplayAlert("Cell Phone invalid");
                srv.IsBusy = false;
                return;
            }

            if (srv.Person.WorkPhone != "" && srv.Person.WorkPhone.length < 10) {
                HFC.DisplayAlert("Work Phone invalid");
                srv.IsBusy = false;
                return;
            }

            var SecPersonEmail = document.getElementById('SecpersonEmail').value;
            if (SecPersonEmail == null || SecPersonEmail == "" || SecPersonEmail == undefined) {

            }
            else {
                if (!validateEmail(SecPersonEmail)) {
                    HFC.DisplayAlert("Secondary Person Primary Email not valid");
                    srv.IsBusy = false;
                    return;
                }
            }

            var SecPersonSecondaryEmail = document.getElementById('SecPersonSecondaryEmail').value;
            if (SecPersonSecondaryEmail == null || SecPersonSecondaryEmail == "" || SecPersonSecondaryEmail == undefined) {

            }
            else {
                if (!validateEmail(SecPersonSecondaryEmail)) {
                    HFC.DisplayAlert("Secondary Person Secondary Email not valid");
                    srv.IsBusy = false;
                    return;
                }
            }

            if (!srv.Person.IsSubscribed)
                srv.Person.UnsubscribedOnUtc = new Date().toISOString();
            else
                srv.Person.UnsubscribedOnUtc = null;

            if (srv.LeadId == 0) {
                srv.IsBusy = false;
                $("#" + modalId).modal("hide");
                if (srv.IsNew == true) {
                    srv.PersonIndex = srv.People.length;
                    srv.People.push({
                        PersonIndex: srv.PersonIndex + 1,
                        FirstName: srv.Person.FirstName,
                        LastName: srv.Person.LastName,
                        CompanyName: srv.Person.CompanyName,
                        WorkTitle: srv.Person.WorkTitle,
                        HomePhone: srv.Person.HomePhone,
                        CellPhone: srv.Person.CellPhone,
                        FaxPhone: srv.Person.FaxPhone,
                        WorkPhone: srv.Person.WorkPhone,
                        WorkPhoneExt: srv.Person.WorkPhoneExt,
                        PrimaryEmail: srv.Person.PrimaryEmail,
                        SecondaryEmail: srv.Person.SecondaryEmail,
                        PreferredTFN: srv.Person.PreferredTFN,
                        Unsubscribed: srv.Person.Unsubscribed,
                        UnsubscribedOnUtc: srv.Person.UnsubscribedOnUtc
                    })
                }
                else {
                    srv.People[srv.PersonIndex] = srv.Person;
                }


                srv.Person = null;
                srv.IsNew = false;
                return;
            }

            var promise = null;
            if (!srv.Person.PersonId)
                promise = $http.post('/api/leads/' + srv.LeadId + '/person/', srv.Person);
            else
                promise = $http.put('/api/leads/' + srv.LeadId + '/person/', srv.Person);

            promise.then(function (response) {
                if (!srv.Person.PersonId && response.data.PersonId != srv.Person.PersonId) {
                    srv.Person.PersonId = response.data.PersonId;
                }

                srv.Person.FullName = $.trim($.trim(srv.Person.FirstName) + " " + $.trim(srv.Person.LastName));
                srv.People[srv.PersonIndex] = srv.Person;

                $("#" + modalId).modal("hide");
                HFC.DisplaySuccess("Person saved");
                srv.Person = null;
                srv.IsBusy = false;
            }, function (response) {
                HFC.DisplayAlert(response.statusText);
                srv.IsBusy = false;
            });
        }
    };

    srv.Cancel = function (modalId) {
        $("#" + modalId).modal("hide");

    };



    srv.parsePhone = function (phone) {
        return phone.match(/(\d{3})(\d{3})(\d{4})/);
    };

    srv.EditorData = [];

    srv.IsBusy = false;



    srv.LeadId = false;

}
    ]);


})(window, document);


