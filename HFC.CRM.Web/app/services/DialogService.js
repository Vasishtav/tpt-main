﻿/***************************************************************************\
Module Name:  DialogService
Project: HFC-Tochpoint
Created on: 17 mar 2018
Created By: 
Copyright:
Description: A common service to expose confirm and other javascript replacement
Change History:
Date  By  Description

\***************************************************************************/

app.service('DialogService', [
    '$http', 'HFCService', 'PersonService',
function ($http, HFCService,PersonService) {

        var srv = this;

        //srv.validateAddress = function (model) {
        //    var url = "/api/AddressValidation/0/GetFEDExAddressValidation";
        //    //return $http.get(url);
        //    return $http.post(url, model);
        //}
        srv.divId = "_dialogModal_";
        srv.confirmDialog = function () {
            $("#" + srv.divId).modal("show");
        };

        // Canel the operation or window.
        srv.SendCancel = function () {
            $("#" + srv.divId).modal("hide");
            PersonService.clicked = 'cancel';
            return PersonService.leadredirectCallBack();

           // if (srv.myCallback) srv.myCallback("Cancel");
           //return;
        };

        srv.SendNo = function () {
            $("#" + srv.divId).modal("hide");
            PersonService.clicked = 'no';
           return PersonService.leadredirectCallBack();
           // if (srv.myCallback) srv.myCallback("No");
         // return;
        };
        

        // Save or update the notes/attachment to the database
        srv.acceptConfirm = function (modal) {
            
            srv.submitnte = true;
            
            $("#" + srv.divId).modal("hide");
            PersonService.clicked = 'yes';
            return PersonService.leadredirectCallBack();
           // if (srv.myCallback) srv.myCallback("Yes");
          // return true;
       }

    }
]).directive('hfcDialog', [
            'DialogService', function (DialogService) {
                return {
                    restrict: 'E',
                    scope: {
                        modalId: '@'
                    },
                    templateUrl: '/templates/NG/Hfc-dialog.html',
                    replace: true,
                    link: function (scope) {
                        scope.dialogService = DialogService;
                    }
                }
            }
]);

app.service('DialogYesNoService', [
    '$http', 'HFCService', 'PersonService',
function ($http, HFCService, PersonService) {

    var srv = this;
    srv.message = '';

    //srv.validateAddress = function (model) {
    //    var url = "/api/AddressValidation/0/GetFEDExAddressValidation";
    //    //return $http.get(url);
    //    return $http.post(url, model);
    //}
    srv.divId = "_dialogModalYesNo_";
    srv.confirmDialog = function (message,option) {

        srv.message = message;
        srv.option = option;
        $("#" + srv.divId).modal("show");
    };

    // Canel the operation or window.
    //srv.SendCancel = function () {
    //    $("#" + srv.divId).modal("hide");
    //    PersonService.clicked = 'cancel';
    //    srv.message = '';
    //    return PersonService.leadredirectCallBack();

    //    // if (srv.myCallback) srv.myCallback("Cancel");
    //    //return;
    //};

    srv.SendNo = function () {
        $("#" + srv.divId).modal("hide");
        PersonService.clicked = 'no';
        srv.message = '';
        return PersonService.OppertunityYesNoCallBack(srv.option);
        // if (srv.myCallback) srv.myCallback("No");
        // return;
    };


    
    srv.acceptConfirm = function (modal) {

        srv.submitnte = true;

        $("#" + srv.divId).modal("hide");
        PersonService.clicked = 'yes';
        return PersonService.OppertunityYesNoCallBack(srv.option);
        // if (srv.myCallback) srv.myCallback("Yes");
        // return true;
    }

}
]).directive('hfcYesNoDialog', [
            'DialogYesNoService', function (DialogYesNoService) {
                return {
                    restrict: 'E',
                    scope: {
                        modalId: '@'
                    },
                    templateUrl: '/templates/NG/Hfc-dialog-Yes-No.html',
                    replace: true,
                    link: function (scope) {
                        scope.DialogYesNoService = DialogYesNoService;
                    }
                }
            }
]);

