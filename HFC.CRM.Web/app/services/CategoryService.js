﻿'use strict';
app.service('categoryService', [
    '$http', function ($http) {
        var categoryServiceFactory = {};
         
        categoryServiceFactory.getActiveCategories = function () {
            return $http.get('api/ProductCategoryTP/0?Inactive=false').then(function (results) {
                return results;
                 
            });
        }
        return categoryServiceFactory;
    }
]);