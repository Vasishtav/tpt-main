﻿(function () {
    'use strict';
    angular.module('globalMod').factory("calendarModalService", ['$location', '$http', function ($location, $http) {
    var calendarModalService = {};

    // url for get the dependency data

    calendarModalService.OpportunityUrl = "/api/Opportunities/";
    calendarModalService.OrderUrl = "/api/Orders/";
    calendarModalService.CaseUrl = "/api/CaseManageAddEdit/";
    calendarModalService.CaseUrlSub = "/getAccOrderOpportunity";
    calendarModalService.VendorCaseUrl = "/api/CaseManageAddEdit/";
    calendarModalService.VendorCaseUrlSub = "/getCompleteDetails";

    calendarModalService.isModalOpen = false;

    // DependencyList   ->  fields that are reloaded the dropdown data
    // NullifyList      ->  fields that are need to be nullified, when the option selects
    // SelfNullifyList  ->  fields that are need to be nullified, when the option deselects

    var dropdownSelectDetails = {};
    // Lead
    dropdownSelectDetails.LeadDependencyList = ["OpportunityDropdown", "OrderDropdown", "CaseDropdown", "VendorCaseDropdown"];
    dropdownSelectDetails.LeadNullifyList = [{ model: "calendarModelObject", key: "AccountId" }, { model: "calendarModelObject", key: "OpportunityId" }, { model: "calendarModelObject", key: "OrderId" }, { model: "calendarModelObject", key: "CaseId" }, { model: "calendarModelObject", key: "vendorCaseId" }, { model: "calendarModelObject", key: "AccountPhoneNumber" }];
    dropdownSelectDetails.LeadSelfNullifyList = [{ model: "calendarModelObject", key: "LeadId" }, { model: "calendarModelObject", key: "LeadPhoneNumber" }];
    // Account
    dropdownSelectDetails.AccountDependencyList = ["OpportunityDropdown", "OrderDropdown", "CaseDropdown", "VendorCaseDropdown"];
    dropdownSelectDetails.AccountNullifyList = [{ model: "calendarModelObject", key: "LeadId" }, { model: "calendarModelObject", key: "LeadPhoneNumber" }, { model: "calendarModelObject", key: "OpportunityId" }, { model: "calendarModelObject", key: "OrderId" }, { model: "calendarModelObject", key: "CaseId" }, { model: "calendarModelObject", key: "vendorCaseId" }];
    dropdownSelectDetails.AccountSelfNullifyList = [{ model: "calendarModelObject", key: "AccountId" }, { model: "calendarModelObject", key: "OpportunityId" }, { model: "calendarModelObject", key: "OrderId" }, { model: "calendarModelObject", key: "CaseId" }, { model: "calendarModelObject", key: "vendorCaseId" }, { model: "calendarModelObject", key: "AccountPhoneNumber" }];
    // Opportunity
    dropdownSelectDetails.OpportunityDependencyList = ["OrderDropdown", "CaseDropdown", "VendorCaseDropdown"];
    dropdownSelectDetails.OpportunityNullifyList = [{ model: "calendarModelObject", key: "LeadId" }, { model: "calendarModelObject", key: "LeadPhoneNumber" }, { model: "calendarModelObject", key: "AccountId" }, { model: "calendarModelObject", key: "OrderId" }, { model: "calendarModelObject", key: "CaseId" }, { model: "calendarModelObject", key: "vendorCaseId" }, { model: "calendarModelObject", key: "AccountPhoneNumber" }];
    dropdownSelectDetails.OpportunitySelfNullifyList = [{ model: "calendarModelObject", key: "OpportunityId" }, { model: "calendarModelObject", key: "OrderId" }, { model: "calendarModelObject", key: "CaseId" }, { model: "calendarModelObject", key: "vendorCaseId" }];
    // Order
    dropdownSelectDetails.OrderDependencyList = ["CaseDropdown", "VendorCaseDropdown"];
    dropdownSelectDetails.OrderNullifyList = [{ model: "calendarModelObject", key: "LeadId" }, { model: "calendarModelObject", key: "LeadPhoneNumber" }, { model: "calendarModelObject", key: "AccountId" }, { model: "calendarModelObject", key: "OpportunityId" }, { model: "calendarModelObject", key: "CaseId" }, { model: "calendarModelObject", key: "vendorCaseId" }, { model: "calendarModelObject", key: "AccountPhoneNumber" }];
    dropdownSelectDetails.OrderSelfNullifyList = [{ model: "calendarModelObject", key: "OrderId" }, { model: "calendarModelObject", key: "CaseId" }, { model: "calendarModelObject", key: "vendorCaseId" }];
    // Case
    dropdownSelectDetails.CaseDependencyList = ["VendorCaseDropdown"];
    dropdownSelectDetails.CaseNullifyList = [{ model: "calendarModelObject", key: "LeadId" }, { model: "calendarModelObject", key: "LeadPhoneNumber" }, { model: "calendarModelObject", key: "AccountId" }, { model: "calendarModelObject", key: "OpportunityId" }, { model: "calendarModelObject", key: "OrderId" }, { model: "calendarModelObject", key: "vendorCaseId" }, { model: "calendarModelObject", key: "AccountPhoneNumber" }];
    dropdownSelectDetails.CaseSelfNullifyList = [{ model: "calendarModelObject", key: "CaseId" }, { model: "calendarModelObject", key: "vendorCaseId" }];
    // VendorCase
    dropdownSelectDetails.VendorCaseDependencyList = [];
    dropdownSelectDetails.VendorCaseNullifyList = [{ model: "calendarModelObject", key: "LeadId" }, { model: "calendarModelObject", key: "LeadPhoneNumber" }, { model: "calendarModelObject", key: "AccountId" }, { model: "calendarModelObject", key: "OpportunityId" }, { model: "calendarModelObject", key: "OrderId" }, { model: "calendarModelObject", key: "CaseId" }, { model: "calendarModelObject", key: "AccountPhoneNumber" }];
    dropdownSelectDetails.VendorCaseSelfNullifyList = [{ model: "calendarModelObject", key: "vendorCaseId" }];

    // setter / getter flag for pop up open / close functionality
    calendarModalService.popUpFlag = false;
    calendarModalService.scopeFunction = null;

    calendarModalService.Quote_exp_draft = false;

    // intialize object for task / event
    var initialObject = {
        PersonId: 0,

        title: "",
        Location: "",
        Message: "",
        LeadId: null,
        AccountId: null,
        OpportunityId: null,
        OrderId: null,
        CaseId: null,
        vendorCaseId: null,
        LeadPhoneNumber: "",
        AccountPhoneNumber: "",
        start: "",
        end: "",
        AptTypeEnum: 0,
        ReminderMinute: 15,
        CompletedDateUtc: null,
        EventType: 0,
        SendEmail: true,
        originalStartDate: null,
        RecurringEventId: null,
        IsTask: false,
        IsPrivateAccess: false,
        IsPrivate: false,
        RecurrenceIsEnabled: false,
        allDay: false,
        AdditionalPeople: [],
        AssignedPersonId: 0,
        IsNotifyemails: false,
        IsNotifyText:false,
        EventRecurring: {
            RecurringEventId: 0,
            PatternEnum: 1,
            RecursEvery: 1,

            flagMonthly: 1,

            DayOfMonth: 1,

            DayOfWeekIndex: 1,
            DayOfWeekEnum: 1,

            MonthEnum: 1,
            flagYearly: 1,

            StartDate: "",
            EndsOn: "",
            endBy: 1,
            EndsAfterXOccurrences: "1",
            EndDate: ""
        }
    };

    //for displaying task popup
    calendarModalService.TaskPathId = 0;

    // get dropDown Details for all fields
    calendarModalService.getDropdownDetails = function () {
        return dropdownSelectDetails;
    }

    // traverse the oppurtunity related data and fetch thier dependencies
    calendarModalService.traverseOpportunity = function (result, parentScope) {                 // need to optimise these methods
        var opportunity = result.data.Opportunity;
        if (!parentScope.calendarModelObject.AccountId)
            parentScope.calendarModelObject.AccountId = opportunity.AccountId;
        if (!opportunity.PrimCustomer.PreferredTFN) {
            if (opportunity.PrimCustomer.HomePhone != null) {
                parentScope.AccountPhoneNumber = opportunity.PrimCustomer.HomePhone;
            } else if (opportunity.PrimCustomer.CellPhone != null) {
                parentScope.AccountPhoneNumber = opportunity.PrimCustomer.CellPhone;
            } else if (opportunity.PrimCustomer.WorkPhone != null) {
                parentScope.AccountPhoneNumber = opportunity.PrimCustomer.WorkPhone;
            }

        } else {
            if (opportunity.PrimCustomer.PreferredTFN == "H") {
                parentScope.AccountPhoneNumber = opportunity.PrimCustomer.HomePhone;
            } else if (opportunity.PrimCustomer.PreferredTFN == "C") {
                parentScope.AccountPhoneNumber = opportunity.PrimCustomer.CellPhone;
            } else if (opportunity.PrimCustomer.PreferredTFN == "W") {
                parentScope.AccountPhoneNumber = opportunity.PrimCustomer.WorkPhone;
            }
        }
    }

    // traverse the order related data and fetch their dependencies
    calendarModalService.traverseOrder = function (result, parentScope) {
        var order = result.data;

        if (!parentScope.calendarModelObject.AccountId)
            parentScope.calendarModelObject.AccountId = order.AccountId;

        parentScope.AccountPhoneNumber = order.AccountPhone;
        if (!parentScope.calendarModelObject.OpportunityId)
            parentScope.calendarModelObject.OpportunityId = order.OpportunityId;
    }

    // traverse the case related data and fetch their dependencies
    calendarModalService.traverseCase = function (result, parentScope) {
        if (result.data) {
            var MPO = result.data;
            if (!parentScope.calendarModelObject.AccountId)
                parentScope.calendarModelObject.AccountId = MPO.AccountId;

            if (!parentScope.calendarModelObject.OpportunityId)
                parentScope.calendarModelObject.OpportunityId = MPO.OpportunityId;

            if (!parentScope.calendarModelObject.OrderId)
                parentScope.calendarModelObject.OrderId = MPO.OrderId;

            var accountList = $("#kendoComboboxAccount").data("kendoComboBox");
            if (accountList) {
                var account = JSLINQ(accountList.dataSource._data).Where(function (item) {
                    return item.AccountId == parentScope.calendarModelObject.AccountId
                });
                if (account) {
                    parentScope.AccountPhoneNumber = account.items[0].PhoneNumber;
                }
            }
        }
    }

    // manipulate hour and minute in date fields
    var getHourMinuteString = function (selectedDate, currentDateflag) {
        var hourString = selectedDate.getHours();
        var minuteString = selectedDate.getMinutes();
        if (currentDateflag) {
            var minuteCheck = minuteString - 30;
            if (minuteCheck >= 0) {
                minuteString = "00";
                hourString += 1;
            } else {
                minuteString = "30";
            }
        }
        var ampmString = hourString >= 12 ? 'PM' : 'AM';
        hourString = hourString % 12;
        hourString = hourString ? hourString : 12;
        var resultStr = " " + hourString + ":" + minuteString + " " + ampmString;
        return resultStr;
    }

    // manipulate date time to convert as string
    calendarModalService.manipulateDateString = function (dateTime, currentDateflag, dateFlag, operationFlag) {
        var monthString = dateTime.getMonth() + 1;
        var dateString = dateTime.getDate();
        var yearString = dateTime.getFullYear();
        var timeStr = '';
        if (dateFlag == false) {
            if (operationFlag == 'start') {
                timeStr = getHourMinuteString(new Date(), true);
            } else {
                var currentDate = new Date();
                var hour = currentDate.getHours() + 1;
                currentDate.setHours(hour);
                timeStr = getHourMinuteString(currentDate, true);
            }
        } else if (!dateFlag) {
            timeStr = getHourMinuteString(dateTime, currentDateflag);
        }

        dateTimeString = monthString + "/" + dateString + "/" + yearString + timeStr;
        return dateTimeString;
    }

    // set generic method for reload purpose for task
    calendarModalService.setTaskGenericRefreshMethod = function (func) {
        calendarModalService.genericTaskMethod = func;
        }

        // demo

    // set generic method for reload purpose for event
    calendarModalService.setEventGenericRefreshMethod = function (func) {
        calendarModalService.genericEventMethod = func;
    }


        // to set the calendar id from appointment pages from sales flow
        calendarModalService.setCalendarDataBackup = function (dataObject) {
            calendarModalService.storedCalendarData = dataObject
        }
    // manipulate the date string		
    calendarModalService.getDateStringValue = function (dateObject, flag, selectFlag, dateFlag, globalUTCFlag, allDayFlag) {
        var utcFormat = (dateObject.getUTCMonth() + 1) + "/" + dateObject.getUTCDate() + "/" + dateObject.getUTCFullYear() + " " + dateObject.getUTCHours() + ":" + dateObject.getUTCMinutes();
        var dateFormat = (dateObject.getMonth() + 1) + "/" + dateObject.getDate() + "/" + dateObject.getFullYear();
        var utcDate = new Date(utcFormat);
        // updated validation		
        var timezoneOffset = new Date(dateFormat).getTimezoneOffset();
        var initialDate = utcDate;
        var dateOffset = initialDate.getTime();
        var actualDate = new Date(dateOffset - (timezoneOffset * 60 * 1000));
        var checkHourString = actualDate.toLocaleTimeString(navigator.language, {
            hour: '2-digit'
        });
        var dateHourString;
        var hour = actualDate.getHours() % 12;
        var dateMinuteString;
        var dateString;
        var hourString = parseInt(checkHourString) % 12;
        var compatability = false;
        var userAgent = navigator.userAgent || navigator.vendor || window.opera;
        if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
            compatability = true;
        }
        if (window.navigator.platform == "Mac68K" || window.navigator.platform == "MacPPC" || window.navigator.platform == "MacIntel") {
           // compatability = true;
            // mac os finder
            var versionString = userAgent.split("Mac OS X")[1];
            var version = versionString.split(")")[0];
            var subVersion = parseInt(version.split("_")[2]);
            if (subVersion > 2) {
                compatability = true;
            }
        }
        if (compatability && globalUTCFlag && selectFlag) {
            dateHourString = actualDate.getUTCHours();
            dateMinuteString = actualDate.getUTCMinutes();
            dateString = (actualDate.getUTCMonth() + 1) + "/" + actualDate.getUTCDate() + "/" + actualDate.getUTCFullYear();
        } else {
            if ((!compatability) || selectFlag || allDayFlag) {
                dateHourString = actualDate.getHours();
                dateMinuteString = actualDate.getMinutes();
                // dateString = actualDate.toLocaleDateString();
                dateString = (actualDate.getMonth() + 1) + "/" + actualDate.getDate() + "/" + actualDate.getFullYear();
            } else {
                dateHourString = actualDate.getUTCHours();
                dateMinuteString = actualDate.getUTCMinutes();
                dateString = (actualDate.getUTCMonth() + 1) + "/" + actualDate.getUTCDate() + "/" + actualDate.getUTCFullYear();
            }
        }
        if (dateFlag) {
            return dateString;
        }
        if (flag) {
            var createdDateObject = new Date(dateString);
            createdDateObject.setHours(dateHourString);
            createdDateObject.setMinutes(dateMinuteString);
            return createdDateObject;
        }
        var AmpmString = dateHourString >= 12 ? 'PM' : 'AM';
        dateHourString = dateHourString % 12;
        dateHourString = dateHourString ? dateHourString : 12;
        if (dateHourString < 10) {
            dateHourString = "0" + dateHourString;
        }
        if (dateMinuteString < 10) {
            dateMinuteString = "0" + dateMinuteString;
        }
        var updatedDate = dateString + " " + dateHourString + ":" + dateMinuteString + " " + AmpmString;
        return updatedDate;
        }

        // round off time setter
        calendarModalService.roundedTime = function (dateTimeObject) {
            var currentMinute = dateTimeObject.get("minute");
            var difference = currentMinute / 15;
            var minute;
            switch (true) {
                case (difference < 1):
                    {
                        minute = 15;
                        break;
                    }
                case (difference < 2):
                    {
                        minute = 30;
                        break;
                    }
                case (difference < 3):
                    {
                        minute = 45;
                        break;
                    }
                case (difference < 4):
                    {
                        minute = 0;
                        break;
                    }
            }
            if (minute === 0) {
                dateTimeObject.set({ hour: dateTimeObject.get("hour") + 1, minute: minute });
            } else {
                dateTimeObject.set({ minute: minute });
            }
            return dateTimeObject;
        }

        // updated timezone handler
        /*
                      * param1 - datetime / date string
                      * param2 - hour value to be added, especially for enddate alone
                      * param3 - create appointment flag
                      * param4 - allDay appointment flag, true, only the appointment saved as all day
                      * param5 - date flag to return date format
                      * param6 - date selected from date picker flag to restrict rounded off
        */
        calendarModalService.getDateTimeString = function (dateTimeString, intervalObj, createAppointmentFlag, allDayFlag, dateFlag, pickerSelectionFlag, allDayBackup) {
            var dateTimeUTCObject;
            var Zflag = dateTimeString.indexOf("Z");
            var Tflag = dateTimeString.indexOf("T");
            // validate the occurenec of Z & add Z, If missed
            if (dateTimeString.indexOf("+") !== -1) {
                dateTimeString = dateTimeString.split("+")[0];
            }
            if (Zflag === -1) {
                dateTimeString = dateTimeString.concat(" Z");
            }
            if (Tflag === -1) {
                dateTimeString = dateTimeString.replace(", ", "T");
                dateTimeUTCObject = moment(dateTimeString, "MM/DD/YYYY[T]HH:mm:ss A Z");
            } else {
                // convert the dateTimeString into dateObject in UTC form.
                dateTimeUTCObject = moment(dateTimeString);
            }
            
            var dateTimeObject = (dateTimeUTCObject).utcOffset(0);
            // for all day appointments, datetime is set to local time to handle the all day uncheck event
            if (allDayFlag || (allDayBackup && !allDayFlag && !pickerSelectionFlag)) {
                var currentDate = new Date();
                dateTimeObject.set({ hour: currentDate.getHours(), minute: currentDate.getMinutes() });
            }

            // round off the minute in create flow
            if ((createAppointmentFlag && !calendarModalService.schedulerCreateFlag && !pickerSelectionFlag) || (!allDayFlag && allDayBackup && !pickerSelectionFlag)) {
                dateTimeObject = calendarModalService.roundedTime(dateTimeObject);
            }
            // check for time addition flag, only for end date manipulation
            if (intervalObj) {
                if (intervalObj['minute'])
                    dateTimeObject.set({ hour: dateTimeObject.get("hour") + intervalObj['hour'], minute: dateTimeObject.get("minute") + intervalObj['minute'] });
                else
                    dateTimeObject.set({ hour: dateTimeObject.get("hour") + intervalObj['hour'] });
            }
            if (dateFlag) {
                var updatedDateString = dateTimeObject.format('MM/DD/YYYY');
                return updatedDateString;
            }
            var updatedDateTimeString = dateTimeObject.format('MM/DD/YYYY hh:mm A');
            return updatedDateTimeString;
        }


    // get appointment data for view pop up
    calendarModalService.getAppointmentData = function (id, flag, startDate, endDate) {
        if (id) {
            var loadingElement = $("#loading");
            if (loadingElement) {
                loadingElement[0].style.display = "block";
            }
            var url = "/api/calendar/?CalendarEdit=true&CalendarId=" + id + "&includeLookup=true"
            $http.get(url).then(function (data) {
                if (!flag && data["data"] && data["data"]["Events"]) {
                    calendarModalService.eventview = data["data"]["Events"][0];
                    if (calendarModalService.eventview.start) {
                        calendarModalService.eventview.startDate = startDate;
                        calendarModalService.eventview.start = startDate;
                    }
                    if (calendarModalService.eventview.end) {
                        calendarModalService.eventview.endDate = endDate;
                        calendarModalService.eventview.end = endDate;
                    }
                    $("#_calendarModal_").modal("show");
                } else if (flag && data["data"] && data["data"]["Tasks"]) {
                    var taskList = data["data"]["Tasks"];
                    var selectedItem = JSLINQ(taskList).Where(function (item) {
                        return item["id"] == id
                    });
                    calendarModalService.eventview = selectedItem['items'][0];
                    if (calendarModalService.eventview) {
                        calendarModalService.eventview['IsTask'] = true;
                    }
                    $("#calendarTaskModal").modal("show");
                }
                if (loadingElement) {
                    loadingElement[0].style.display = "none";
                }
            });
        }
    }

    // set pop up open / close flag
    calendarModalService.setPopUpDetails = function (operationFlag, title, id, functionMethod, returnText, data, schedulerFlag, originalStartDate, originalEndDate,globalFlag, schedulerCreateFlag) {
        calendarModalService.popUpFlag = operationFlag;
        var dateSetterFlag = false;
        calendarModalService.schedulerFlag = schedulerFlag;
        calendarModalService.schedulerCreateFlag = schedulerCreateFlag;
        calendarModalService.originalStartDate = originalStartDate;
        calendarModalService.originalEndDate = originalEndDate;
        if (operationFlag) {
            if (title == "Task") {
                initialObject.IsTask = true;
            } else {
                initialObject.IsTask = false;
            }
            if (functionMethod) {
                calendarModalService.rebindFunction = functionMethod;
            } else {
                if (title == "Task") {
                    calendarModalService.rebindFunction = calendarModalService.genericTaskMethod;
                } else {
                    calendarModalService.rebindFunction = calendarModalService.genericEventMethod;
                }

            }
            if (!id) {
                if (data && data['event']) {
                    initialObject['start'] = data['event'].start;
                    initialObject['end'] = data['event'].end;
                    dateSetterFlag = true;
                }
                if (title == "Task") {
                    calendarModalService.getTaskDetails(title, id, returnText, initialObject, dateSetterFlag);
                } else {
                    calendarModalService.getInitModuleData(null, returnText, title, id, returnText, initialObject, dateSetterFlag);
                }
            } else {
                calendarModalService.scopeFunction(title, id, returnText, initialObject, dateSetterFlag, calendarModalService.schedulerFlag, originalStartDate, originalEndDate);
            }

            //calendarModalService.isModalOpen = true;
            //$("html").addClass("scroll-hidden");
            //$("body").addClass("scroll-hidden");
        } else {
            //calendarModalService.isModalOpen = false;
            //calendarModalService.schedulerFlag = false;
            //$("html").removeClass("scroll-hidden");
            //$("body").removeClass("scroll-hidden");
        }
        if (globalFlag) {
            setTimeout(function () {
                {
                    $(document).click();
                }
            }, 500);
        }
    }

    // setter for init method to intiate the pop up flow
    calendarModalService.setInitFunction = function (initFunction) {
        calendarModalService.scopeFunction = initFunction;
    }

    // set route details
    calendarModalService.setDetails = function (dataList, nameList) {
        for (var i = 0; i < nameList.length; i++) {
            initialObject[nameList[i]] = dataList[nameList[i]];
        }
    }

    // view only method binding for reload the calendar
    calendarModalService.setViewFunction = function (func, returnText) {
        calendarModalService.viewFunction = func;
        calendarModalService.viewReturnText = returnText;
    }

    // setter function for set the dependent information on sleecting the dropdown option in calendar
    calendarModalService.setDependentSetterFunction = function (func) {
        calendarModalService.dependentSetterPreserveFunction = func;
    }


    // for pre populate data
    calendarModalService.getInitModuleData = function (id, type, title, editId, returnText, initialObject, dateSetterFlag) {

        var routePath = $location.path();
        var routeParamList = routePath.split("/");
        if (!id) {
            id = routeParamList[3];
        }

        // Lead
        if (type == "Lead" || type == "Qualify" || type == "Lead-Appts" || type == "Lead-Communication" || (type == "Leads-Search" && id != null && id != "" && id != undefined)) {
            calendarModalService.NewLeadApt(id, title, editId, returnText, initialObject, dateSetterFlag);
        }
            // Account
        else if ((type == "Acct-Search" && id != null && id != "" && id != undefined) || type == "Acct" || type == "Acct-Opp List" || type == "Acct-Order List" || type == "Acct-Payment" || type == "Acct-Appts" || type == "Acct-Communication" || type == "Acct-Measurements") {
            calendarModalService.NewAccountApt(id, title, editId, returnText, initialObject, dateSetterFlag);
        }
            // Opportunity
        else if ((type == "Opp-Search" && id != null && id != "" && id != undefined) || type == "Opp-Measurement" || type == "Opp-Appts" || type == "Opp-Communication" || type == "Opp" || type == "Quote" || type == "Move Quote") {
            calendarModalService.NewOpportunityApt(id, title, editId, returnText, initialObject, dateSetterFlag);
        }
            // Order
        else if (type == "Order") {
            calendarModalService.NewOrderApt(id, title, editId, returnText, initialObject, dateSetterFlag);
        } else if (type == "CaseView" || type == "Case") {
            calendarModalService.NewCaseApt(id, title, editId, returnText, initialObject, dateSetterFlag);
        } else if (type == "VendorcaseView") {
            calendarModalService.NewVendorCaseApt(id, title, editId, returnText, initialObject, dateSetterFlag);
        } else {
            calendarModalService.scopeFunction(title, editId, returnText, initialObject, dateSetterFlag, calendarModalService.schedulerFlag, calendarModalService.originalStartDate, calendarModalService.originalEndDate);
        }
    }

    // get address
    var getAddress = function (address) {
        var location = "";
        if (address) {
            if (address.Address1) {
                location = address.Address1;
            }
            if (address.Address2) {
                if (location.length != 0) {
                    location += ", " + address.Address2;
                } else {
                    location = address.Address2;
                }
            }
            if (address.City) {
                if (location.length != 0) {
                    location += ", " + address.City;
                } else {
                    location = address.City;
                }
            }
            if (address.State) {
                if (location.length != 0) {
                    location += ", " + address.State;
                } else {
                    location = address.State;
                }
            }
            if (address.ZipCode) {
                if (location.length != 0) {
                    location += " " + address.ZipCode;
                } else {
                    location = address.ZipCode;
                }
            }
        }
        return location;
    }

    // pre population from other modules

    calendarModalService.NewLeadApt = function (leadId, title, editId, returnText, initialObject, dateSetterFlag, dependentSetterFlag) {
        var detailsObject = {};
        //Added To display the Save And Convert Lead Button
        if (leadId) {
            $http.get('/api/leads/' + leadId).then(function (result) {
                var lead = result.data.Lead;
                var person = lead.PrimCustomer;
                var address = lead.Addresses[0];
                var leadPhoneNumber;
                var leadEmail;
                if (lead.IsCommercial)
                    var fullName = person ? (person.CompanyName) : "";
                else
                    var fullName = person ? (person.FullName + (person.CompanyName != null && person.CompanyName != "" ? " / " + person.CompanyName : "")) : "";

                /* if (srv.EditableEvent)
                     srv.EditableEvent.AptTypeEnum = 1; */

                if (lead.PrimCustomer) {
                    if (!lead.PrimCustomer.PreferredTFN) {
                        if (lead.PrimCustomer.HomePhone != null) {
                            leadPhoneNumber = lead.PrimCustomer.HomePhone;
                        } else if (lead.PrimCustomer.CellPhone != null) {
                            leadPhoneNumber = lead.PrimCustomer.CellPhone;
                        } else if (lead.PrimCustomer.WorkPhone != null) {
                            leadPhoneNumber = lead.PrimCustomer.WorkPhone;
                        }

                    } else {
                        if (lead.PrimCustomer.PreferredTFN == "H") {
                            leadPhoneNumber = lead.PrimCustomer.HomePhone;
                        } else if (lead.PrimCustomer.PreferredTFN == "C") {
                            leadPhoneNumber = lead.PrimCustomer.CellPhone;
                        } else if (lead.PrimCustomer.PreferredTFN == "W") {
                            leadPhoneNumber = lead.PrimCustomer.WorkPhone;
                        } else if (lead.PrimCustomer.CellPhone !== "" && lead.PrimCustomer.CellPhone != null) {
                            leadPhoneNumber = lead.PrimCustomer.CellPhone;
                        }
                        else if (lead.PrimCustomer.HomePhone !== "" && lead.PrimCustomer.HomePhone != null) {
                            leadPhoneNumber = lead.PrimCustomer.HomePhone;
                        }
                        else if (lead.PrimCustomer.WorkPhone !== "" && lead.PrimCustomer.WorkPhone != null) {
                            leadPhoneNumber = lead.PrimCustomer.WorkPhone;
                        }
                    }
                    leadEmail = lead.PrimCustomer.PrimaryEmail;
                }
                var msgTxt = "";
                if (leadPhoneNumber) {
                    msgTxt = "Phone: " + leadPhoneNumber + "\n"
                }
                if (leadEmail) {
                    msgTxt += "Email: " + leadEmail;
                }

                if (lead.LeadStatusId === 13) leadId = 0;

                var location = getAddress(address);
                
                detailsObject['title'] = fullName;
                detailsObject['Location'] = location;
                detailsObject['LeadId'] = leadId;
                detailsObject['LeadPhoneNumber'] = leadPhoneNumber;
                detailsObject['Message'] = msgTxt;
                detailsObject['IsNotifyemails'] = lead.IsNotifyemails;
                detailsObject['IsNotifyText'] = lead.IsNotifyText;
                if (dependentSetterFlag) {
                    calendarModalService.dependentSetterPreserveFunction(detailsObject);
                } else {
                    calendarModalService.setModuleDetails(detailsObject, title, editId, returnText, initialObject, dateSetterFlag);
                }
            });
        } else {
            calendarModalService.setModuleDetails(detailsObject, title, editId, returnText, initialObject, dateSetterFlag);
        }
    }



    calendarModalService.NewAccountApt = function (accountId, title, editId, returnText, initialObject, dateSetterFlag, dependentSetterFlag) {
        var detailsObject = {};
        if (accountId) {
        $http.get('/api/accounts/' + accountId).then(function(result) {
            var account = result.data.Account;
            var person = account.PrimCustomer;
            var address = account.Addresses[0];
            var fullName = "";
            var accountEmail;
            var accountPhoneNumber;
            if (account.IsCommercial)
                var fullName = person ? (person.CompanyName) : "";
            else
                var fullName = person ? (person.FullName + (person.CompanyName != null && person.CompanyName != "" ? " / " + person.CompanyName : "")) : "";
           /* if (srv.EditableEvent)
                srv.EditableEvent.AptTypeEnum = 6; */

            var location = getAddress(address);
            if (account.PrimCustomer) {
                if (!account.PrimCustomer.PreferredTFN) {
                    if (account.PrimCustomer.HomePhone != null) {
                        accountPhoneNumber = account.PrimCustomer.HomePhone;
                    } else if (account.PrimCustomer.CellPhone != null) {
                        accountPhoneNumber = account.PrimCustomer.CellPhone;
                    } else if (account.PrimCustomer.WorkPhone != null) {
                        accountPhoneNumber = account.PrimCustomer.WorkPhone;
                    }

                } else {
                    if (account.PrimCustomer.PreferredTFN == "H") {
                        accountPhoneNumber = account.PrimCustomer.HomePhone;
                    } else if (account.PrimCustomer.PreferredTFN == "C") {
                        accountPhoneNumber = account.PrimCustomer.CellPhone;
                    } else if (account.PrimCustomer.PreferredTFN == "W") {
                        accountPhoneNumber = account.PrimCustomer.WorkPhone;
                    } else if (account.PrimCustomer.CellPhone !== "" && account.PrimCustomer.CellPhone != null) {
                        accountPhoneNumber = account.PrimCustomer.CellPhone;
                    }
                    else if (account.PrimCustomer.HomePhone !== "" && account.PrimCustomer.HomePhone != null) {
                        accountPhoneNumber = account.PrimCustomer.HomePhone;
                    }
                    else if (account.PrimCustomer.WorkPhone !== "" && account.PrimCustomer.WorkPhone != null) {
                        accountPhoneNumber = account.PrimCustomer.WorkPhone;
                    }
                }
                accountEmail = account.PrimCustomer.PrimaryEmail;
            }

            var msgTxt = "";
            if (accountPhoneNumber) {
                msgTxt = "Phone: " + accountPhoneNumber + "\n";
            }
            if (accountEmail) {
                msgTxt += "Email: " + accountEmail;
            }

            detailsObject['title'] = fullName;
            detailsObject['Location'] = location;
            detailsObject['AccountId'] = accountId;
            detailsObject['AccountPhoneNumber'] = accountPhoneNumber;
            detailsObject['Message'] =  msgTxt;
            detailsObject['IsNotifyemails'] = account.IsNotifyemails;
            detailsObject['IsNotifyText'] = account.IsNotifyText;
            if (dependentSetterFlag) {
                calendarModalService.dependentSetterPreserveFunction(detailsObject);
            } else {
                calendarModalService.setModuleDetails(detailsObject, title, editId, returnText, initialObject, dateSetterFlag);
            }
        });
        } else {
            calendarModalService.setModuleDetails(detailsObject, title, editId, returnText, initialObject, dateSetterFlag);
        }
    }


    calendarModalService.NewOpportunityApt = function (opportunityId, title, editId, returnText, initialObject, dateSetterFlag, dependentSetterFlag) {
        var detailsObject = {};
        if (opportunityId) {
        $http.get('/api/Opportunities/' + opportunityId)
            .then(function (result) {
                var opportunity = result.data.Opportunity;
                var person = opportunity.PrimCustomer;
                var address = opportunity.Addresses[0];
                var accountPhoneNumber;
                var accountEmail;
                if (opportunity.IsCommercial)
                    var fullName = person ? (person.CompanyName) : "";
                else
                    var fullName = person ? (person.FullName + (person.CompanyName != null && person.CompanyName != "" ? " / " + person.CompanyName : "")) : "";

                var location = getAddress(address);
                if (opportunity.PrimCustomer) {
                    if (!opportunity.PrimCustomer.PreferredTFN) {
                        if (opportunity.PrimCustomer.HomePhone != null) {
                            accountPhoneNumber = opportunity.PrimCustomer.HomePhone;
                        } else if (opportunity.PrimCustomer.CellPhone != null) {
                            accountPhoneNumber = opportunity.PrimCustomer.CellPhone;
                        } else if (opportunity.PrimCustomer.WorkPhone != null) {
                            accountPhoneNumber = opportunity.PrimCustomer.WorkPhone;
                        }

                    } else {
                        if (opportunity.PrimCustomer.PreferredTFN == "H") {
                            accountPhoneNumber = opportunity.PrimCustomer.HomePhone;
                        } else if (opportunity.PrimCustomer.PreferredTFN == "C") {
                            accountPhoneNumber = opportunity.PrimCustomer.CellPhone;
                        } else if (opportunity.PrimCustomer.PreferredTFN == "W") {
                            accountPhoneNumber = opportunity.PrimCustomer.WorkPhone;
                        } else if (opportunity.PrimCustomer.CellPhone !== "" && opportunity.PrimCustomer.CellPhone != null) {
                            accountPhoneNumber = opportunity.PrimCustomer.CellPhone;
                        }
                        else if (opportunity.PrimCustomer.HomePhone !== "" && opportunity.PrimCustomer.HomePhone != null) {
                            accountPhoneNumber = opportunity.PrimCustomer.HomePhone;
                        }
                        else if (opportunity.PrimCustomer.WorkPhone !== "" && opportunity.PrimCustomer.WorkPhone != null) {
                            accountPhoneNumber = opportunity.PrimCustomer.WorkPhone;
                        }
                    }

                    accountEmail = opportunity.PrimCustomer.PrimaryEmail;
                }

                var msgTxt = "";
                if (accountPhoneNumber) {
                    msgTxt = "Phone: " + accountPhoneNumber + "\n";
                }
                if (accountEmail) {
                    msgTxt += "Email: " + accountEmail;
                }

                detailsObject['title'] = fullName;
                detailsObject['Location'] = location;
                detailsObject['AccountId'] = opportunity.AccountId;
                detailsObject['OpportunityId'] = opportunity.OpportunityId;
                detailsObject['AccountPhoneNumber'] = accountPhoneNumber;
                detailsObject['Message'] = msgTxt;
                detailsObject['IsNotifyemails'] = opportunity.AccountTP.IsNotifyemails;
                detailsObject['IsNotifyText'] = opportunity.AccountTP.IsNotifyText;
                if (dependentSetterFlag) {
                    calendarModalService.dependentSetterPreserveFunction(detailsObject);
                } else {
                    calendarModalService.setModuleDetails(detailsObject, title, editId, returnText, initialObject, dateSetterFlag);
                }
            });
        } else {
            calendarModalService.setModuleDetails(detailsObject, title, editId, returnText, initialObject, dateSetterFlag);
        }
    }




    calendarModalService.NewOrderApt = function (orderId, title, editId, returnText, initialObject, dateSetterFlag, dependentSetterFlag) {
        var detailsObject = {};
        if (orderId) {
        $http.get('/api/Orders/' + orderId)
            .then(function (result) {
                var order = result.data;
                // var person = order.PrimCustomer;
                //var address = order.InstallationAddress;
                var address = order.InstallAddress;
               /* if (order.InstallAddress) {
                    if (order.InstallAddress.Address1) address += order.InstallAddress.Address1 + ' ';
                    if (order.InstallAddress.Address2) address += order.InstallAddress.Address2 + ' ';
                    if (order.InstallAddress.City) address += order.InstallAddress.City + ' ';
                    if (order.InstallAddress.State) address += order.InstallAddress.State + ' ';
                    if (order.InstallAddress.ZipCode) address += order.InstallAddress.ZipCode;
                } */
                var fullName = order ? (order.OrderName) : "";
                /* if (srv.EditableEvent)
                    srv.EditableEvent.AptTypeEnum = 5; */

                var location = getAddress(address);
                var accountPhoneNumber = order.AccountPhone;
                var accountEmail = order.PrimaryEmail;
                var msgTxt = "";
                if (accountPhoneNumber) {
                    msgTxt = "Phone: " + accountPhoneNumber + "\n";
                }
                if (accountEmail) {
                    msgTxt += "Email: " + accountEmail;
                }
                detailsObject['title'] = fullName;
                detailsObject['Location'] = location;
                detailsObject['AccountId'] = order.AccountId;
                detailsObject['OpportunityId'] = order.OpportunityId;
                detailsObject['OrderId'] = order.OrderID;
                detailsObject['AccountPhoneNumber'] = accountPhoneNumber;
                detailsObject['Message'] = msgTxt;
                detailsObject['IsNotifyemails'] = order.IsNotifyemails;
                detailsObject['IsNotifyText'] = order.IsNotifyText;
                if (dependentSetterFlag) {
                    calendarModalService.dependentSetterPreserveFunction(detailsObject);
                } else {
                    calendarModalService.setModuleDetails(detailsObject, title, editId, returnText, initialObject, dateSetterFlag);
                }
            });
        } else {
            calendarModalService.setModuleDetails(detailsObject, title, editId, returnText, initialObject, dateSetterFlag);
        }
    }
    calendarModalService.NewCaseApt = function (caseId, title, editId, returnText, initialObject, dateSetterFlag, dependentSetterFlag) {
        var detailsObject = {};
        if (caseId) {
        $http.get('/api/CaseManageAddEdit/' + caseId + '/getAccOrderOpportunity')
            .then(function (result) {
                if (result.data) {
                    var MPO = result.data;
                    var detailsObject = {};
                    var accountPhoneNumber;
                    var accountEmail;
                    var address = MPO.InstallAddress;

                    if (!MPO.PreferredTFN) {
                        if (MPO.HomePhone != null) {
                            accountPhoneNumber = MPO.HomePhone;
                        } else if (MPO.CellPhone != null) {
                            accountPhoneNumber = MPO.CellPhone;
                        } else if (MPO.WorkPhone != null) {
                            accountPhoneNumber = MPO.WorkPhone;
                        }
                    } else {
                        if (MPO.PreferredTFN == "H") {
                            accountPhoneNumber = MPO.HomePhone;
                        } else if (MPO.PreferredTFN == "C") {
                            accountPhoneNumber = MPO.CellPhone;
                        } else if (MPO.PreferredTFN == "W") {
                            accountPhoneNumber = MPO.WorkPhone;
                            }
                        }
                    var fullName = MPO ? (MPO.OrderName) : "";
                    var location = getAddress(address);
                    accountEmail = MPO.PrimaryEmail;
                    var msgTxt = "";
                    if (accountPhoneNumber) {
                        msgTxt = "Phone: " + accountPhoneNumber + "\n";
                    }
                    if (accountEmail) {
                        msgTxt += "Email: " + accountEmail;
                    }
                    detailsObject['title'] = fullName;
                    detailsObject['Location'] = location;
                    detailsObject['AccountId'] = MPO.AccountId;
                    detailsObject['OpportunityId'] = MPO.OpportunityId;
                    detailsObject['OrderId'] = MPO.OrderId;
                    detailsObject['CaseId'] = caseId;
                    detailsObject['AccountPhoneNumber'] = accountPhoneNumber;
                    detailsObject['Message'] = msgTxt;
                    detailsObject['IsNotifyemails'] = MPO.IsNotifyemails;
                    detailsObject['IsNotifyText'] = MPO.IsNotifyText;
                    if (dependentSetterFlag) {
                        calendarModalService.dependentSetterPreserveFunction(detailsObject);
                    } else {
                        calendarModalService.setModuleDetails(detailsObject, title, editId, returnText, initialObject, dateSetterFlag);
                    }
                }
            });
        } else {
            calendarModalService.setModuleDetails(detailsObject, title, editId, returnText, initialObject, dateSetterFlag);
        }
    }

    //added for vendor case
    calendarModalService.NewVendorCaseApt = function (vendorcaseId, title, editId, returnText, initialObject, dateSetterFlag, dependentSetterFlag) {
        var detailsObject = {};
        if (vendorcaseId) {
        $http.get('/api/CaseManageAddEdit/' + vendorcaseId + '/getCompleteDetails')
            .then(function (result) {
                if (result.data) {
                    var Vendorcase = result.data;
                    var accountPhoneNumber;
                    var accountEmail;

                    var msgTxt = "";
                    if (accountPhoneNumber) {
                        msgTxt = "Phone: " + accountPhoneNumber + "\n";
                    }
                    if (accountEmail) {
                        msgTxt += "Email: " + accountEmail;
                    }

                    var detailsObject = {};
                    detailsObject['title'] = "";
                    detailsObject['Location'] = "";
                    detailsObject['AccountId'] = Vendorcase.AccountId;
                    detailsObject['OpportunityId'] = Vendorcase.OpportunityId;
                    detailsObject['OrderId'] = Vendorcase.OrderId;
                    detailsObject['vendorCaseId'] = vendorcaseId;
                    detailsObject['AccountPhoneNumber'] = accountPhoneNumber;
                    detailsObject['Message'] = msgTxt;
                    detailsObject['IsNotifyemails'] = Vendorcase.IsNotifyemails;
                    detailsObject['IsNotifyText'] = Vendorcase.IsNotifyText;
                    if (dependentSetterFlag) {
                        calendarModalService.dependentSetterPreserveFunction(detailsObject);
                    } else {
                        calendarModalService.setModuleDetails(detailsObject, title, editId, returnText, initialObject, dateSetterFlag);
                    }
                }
            });
        } else {
            calendarModalService.setModuleDetails(detailsObject, title, editId, returnText, initialObject, dateSetterFlag);
        }
    }

    // set details
    calendarModalService.setModuleDetails = function (detailList, title, editId, returnText, initialObject, dateSetterFlag) {
        var keyList = Object.keys(detailList);
        for (var i = 0; i < keyList.length; i++) {
            initialObject[keyList[i]] = detailList[keyList[i]];
        }
        calendarModalService.scopeFunction(title, editId, returnText, initialObject, dateSetterFlag, calendarModalService.schedulerFlag, calendarModalService.originalStartDate, calendarModalService.originalEndDate);
    }


    // for task data setter
    calendarModalService.getTaskDetails = function (title, id, returnText, initialObject, dateSetterFlag) {
        var routePath = $location.path();
        var routeParamList = routePath.split("/");

        var pathType = routeParamList[1];
        var pathId = routeParamList[2];
        var sectionType;

        if (pathType.includes("opportunitySearch") && (pathId == null || pathId == "" || pathId == undefined)) {
            sectionType = "Opp-Search";
        }
        else if (pathType.includes("leadsSearch") && (pathId == null || pathId == "" || pathId == undefined)) {
            sectionType = "Leads-Search";
        }
        else if (pathType.includes("accountSearch") && (pathId == null || pathId == "" || pathId == undefined)) {
            sectionType = "Acct-Search";
        }
        else if (pathType.includes("quotesSearch") && (pathId == null || pathId == "" || pathId == undefined)) {
            sectionType = "Quote-Search";
        }
            //Lead
        else if (pathType.includes("leadAppointments")) {
            sectionType = "Lead-Appts";
        }
        else if (pathType.includes("Qualify")) {
            sectionType = "Qualify";
        }
        else if (pathType.includes("leadCommunication")) {
            sectionType = "Lead-Communication";
        }
        else if (pathType.includes("lead")) {
            sectionType = "Lead";
        }
            //Account
        else if (pathType.includes("accountCommunication")) {
            sectionType = "Acct-Communication";
        }
        else if (pathType.includes("accountAppointments")) {
            sectionType = "Acct-Appts";
        }
        else if (pathType.includes("accountPaymentSearch")) {
            sectionType = "Acct-Payment";
        }
        else if (pathType.includes("accountordersSearch")) {
            sectionType = "Acct-Order List";
        }
        else if (pathType.includes("accountopportunitySearch")) {
            sectionType = "Acct-Opp List";
        }
        else if (pathType.includes("account")) {
            sectionType = "Acct";
        }
        else if (pathType.includes("Accounts")) {
            sectionType = "Acct";
        }
        else if (pathType.includes("measurementDetails")) {
            sectionType = "Acct-Measurements";
        }
            //Opportunity
        else if (pathType.includes("measurementDetail")) {
            sectionType = "Opp-Measurement";
        }
        else if (pathType.includes("opportunityAppointments")) {
            sectionType = "Opp-Appts";
        }
        else if (pathType.includes("opportunityCommunication")) {
            sectionType = "Opp-Communication";
        }
        else if (pathType.includes("OpportunityDetail")) {
            sectionType = "Opp";
        }
        else if (pathType.includes("opportunityEdit")) {
            sectionType = "Opp";
        }
        else if (pathType.includes("SOquoteReview")) {
            sectionType = "Order";
            pathId = routeParamList[3];
        }
        else if (pathType.includes("quoteReview")) {
            sectionType = "Quote";
            pathId = calendarModalService.TaskPathId;
        }
        else if (pathType.includes("Editquote")) {
            sectionType = "Quote";
        }
        else if (pathType.includes("quote")) {
            sectionType = "Quote";
        }
        else if (pathType.includes("moveQuote")) {
            sectionType = "Move Quote";
        }
            //Order
        else if (pathType.includes("orderSearch")) {
            sectionType = "Orders";
        }
        else if (pathType.includes("orderEdit")) {
            sectionType = "Order";
        }
        else if (pathType.includes("Orders")) {
            sectionType = "Order";
        }
        else if (pathType.includes("OrderPayment")) {
            sectionType = "Order";
        }    
        else if (pathType.includes("CaseView")) {
            sectionType = "CaseView";
            //  $scope.AddEvent('persongo');
        }
        else if (pathType.includes("Case")) {
            sectionType = "Case";
        }
        else if (pathType.includes("VendorcaseView")) {
            sectionType = "VendorcaseView";
        }

        calendarModalService.getInitModuleData(pathId, sectionType, title, id, returnText, initialObject, dateSetterFlag);
    }

    return calendarModalService;
    }]);
})();