﻿'use strict';
app.service('OrderSearchService', [
            '$http', 'HFCService', function ($http, HFCService) {
                var srv = this;

                srv.IsBusy = false;
                srv.Pagination = { page: 1, size: 50, pageTotal: 1 };
                srv.PageTotal = srv.Pagination.pageTotal;
                srv.PageSize = srv.Pagination.size;
                srv.PageNumber = srv.Pagination.page;
                srv.TotalRecords = 0;
                srv.ChangePageSize = function () {

                }
                srv.PageSummary = '';

                var offsetvalue = 0;
                //srv.TotalRecords = 0;
                srv.SearchTerm = "";
                srv.SearchFilter = "";
                srv.OrderStatusIds = [];
                srv.DateRangeText = "";
                srv.StartDate = null;
                srv.EndDate = null;
                //srv.predicate = '-createdonutc';
                srv.DateRangeArray = ["All Date & Time", "Last 7 Days", "Last 30 Days", "This Week", "This Month", "This Year", "Last Week", "Last Month", "Last Year"];
                srv.SearchEnums = {};
                srv.Orders = [];

                
                var SalesOrderpermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'SalesOrder')

                srv.PermissionEditOrder = SalesOrderpermission.CanUpdate; //HFCService.GetPermissionTP('Edit Order').CanAccess;

                function DropdownTemplate(dataitem) {
                    var PermissionEditOrder = srv.PermissionEditOrder;

                    var EditOrder = "";
                    if (PermissionEditOrder == true) {
                        EditOrder = '<li><a href="#!/orderEdit/'+dataitem.OrderId+'">Edit</a></li> ';
                        
                    }
                    return '<ul> <li class="dropdown note1 ad_user"> <div class="dropdown">' +
                        ' <button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> ' +
                        ' <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> <ul class="dropdown-menu">' +
                        EditOrder +
                        //' <li><a href="\\\\#!/orderEdit/#= OrderId #">Edit</a></li>' +
                        '<li><a  ng-click="ShowInstallationPrint2('+dataitem.OrderId+')">Print</a></li>' +
                        '</ul></div> </li>  </ul>'

                }

                function OrderNameTemplate(dataitem) {
                    
                    if (dataitem.ReversalAmount && (dataitem.Status=='Cancelled' || dataitem.Status=='Void'))
                    {
                        
                        return '<a href="#!/Orders/' + dataitem.OrderId + '"><span style="color:red !important">' + dataitem.OrderName + '</span></a>'
                        
                    }
                    else
                    {
                        return '<a href="#!/Orders/' + dataitem.OrderId + '" style = "color: rgb(61,125,139);"><span >' + dataitem.OrderName + '</span></a>';
                    }

                }
                srv.Get = function (data) {
                    
                    srv.Orders = [];
                    var filters = {
                        pageIndex: srv.Pagination.page,
                        pageSize: srv.Pagination.size
                    };
                    var Id = 0;

                    if (data.AccountId != null) { Id = data.AccountId; } else { Id = 0; }
                    if (Id > 0) {
                        //Accounts - Orders
                        var s = '<script id="template" type="text/x-kendo-template"><div class="row vendor_search no-margin" ><div class="leadsearch_topleft row no-margin col-sm-12 col-md-12 col-xs-12 no-padding"><div class="col-xs-2 col-sm-2 no-padding hfc_txtbox"><input ng-keyup="OrdergridSearch()" type="search" id="searchBox" placeholder="Search Order" class="k-textbox leadsearch_tbox"></div><div class="col-xs-2 col-sm-2 measurement_search no-padding"><input type="button" id="btnReset" ng-click="OrdergridSearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div></div></div></script>';
                    }
                    else {
                        //Orders
                        var s = '<script id="template" type="text/x-kendo-template"><div class="tp_leadsearch row no-margin"><div class="leadsearch_topleft row no-margin col-sm-10 col-md-10 col-xs-10 no-padding"><div class="col-xs-2 col-sm-2 no-padding hfc_txtbox"><input ng-keyup="OrdergridSearch()" type="search" id="searchBox" placeholder="Search Order" class="k-textbox leadsearch_tbox"></div><div class="col-xs-2 col-sm-2 no-padding hfc_leadbut"><input type="button" id="btnReset" ng-click="OrdergridSearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div><div id="btnReset" ng-click="NewOrderpage()"></div></div><div class="col-sm-2 col-xs-2 col-md-2 no-padding"><div class="leadsearch_icon"><button type="button" class="plus_but tooltip-bottom" data-tooltip="Download Sales Order" ng-click="DownloadSalesOrder_Excel()"><i class="far fa-cloud-download"></i></button>&nbsp;</div></div></script>';
                    }
                    srv.Orders_options = {
                        cache: false,
                        excel: {
                            fileName: "SalesOrders_Export.xlsx",
                            allPages: true

                        },
                        dataSource: {
                            transport: {
                                read: {
                                    url: '/api/search/' + Id + '/GetSearchOrders',
                                    dataType: "json",
                                    data: filters
                                }

                            },
                            error: function (e) {

                                HFC.DisplayAlert(e.errorThrown);
                            }
                        },
                        // https://www.telerik.com/forums/remove-page-if-no-records
                        dataBound: function (e) {
                            //console.log("dataBound");
                            if (this.dataSource.view().length == 0) {
                                // The Grid contains No recrods so hide the footer.
                                $('.k-pager-nav').hide();
                                $('.k-pager-numbers').hide();
                                $('.k-pager-sizes').hide();
                            } else {
                                // The Grid contains recrods so show the footer.
                                $('.k-pager-nav').show();
                                $('.k-pager-numbers').show();
                                $('.k-pager-sizes').show();
                            }
                        },
                        resizable: true,
                        columns: [
                            {
                                field: "OrderNumber",
                                title: "ID",
                                //template: "<a href='\\\\#!/Orders/#= OrderId #' style='color: rgb(61,125,139);'><span>#= OrderNumber #</span></a> ",
                                filterable: { multi: true, search: true }
                            },
                            {
                                field: "OrderName",
                                title: "Order Name",
                                template: function (dataitem) {
                                    return OrderNameTemplate(dataitem);
                                },
                                //template: "<a href='\\\\#!/Orders/#= OrderId #' ><span style='color:red !important'>#= OrderName #</span></a> ",
                                filterable: { multi: true, search: true }
                            },
                            {
                                field: "AccountName",
                                title: "Account Name",
                                template: "<a href='\\\\#!/Accounts/#= AccountId #' style='color: rgb(61,125,139);'><span>#= AccountName #</span></a> ",
                                filterable: { multi: true, search: true }
                            },
                                 {

                                     field: "OpportunityName",
                                     title: "Opportunity Name",
                                     template: "<a href='\\\\#!/OpportunityDetail/#= OpportunityId #' style='color: rgb(61,125,139);'><span>#= OpportunityName #</span></a> ",
                                     filterable: { multi: true, search: true }
                                 },
                                 {
                                     field: "Status",
                                     title: "Status",
                                     filterable: { multi: true, search: true }
                                 },
                                 {
                                     field: "SalesAgentName",
                                     title: "Sales Agent",
                                     filterable: { multi: true, search: true }
                                 },
                                 {
                                     
                                     field: "TotalAmount",
                                     format: "{0:c2}",
                                     attributes: { class: "text-right" },
                                     title: "Total Amount",
                                     filterable: { multi: true, search: true },
                                    
                                 },
                                 {
                                     field: "ContractDate",
                                     title: "Contract Date",
                                     type: "date",
                                     filterable: HFCService.gridfilterableDate("date", "OrderSearchContractDateContractFilterCalender", offsetvalue),
                                     template: function (dataitem) {
                                         return HFCService.KendoDateFormat(dataitem.ContractDate);
                                     }
                                         //"#= kendo.toString(kendo.parseDate(ContractDate, 'yyyy-MM-dd'), 'M/d/yyyy') #"

                                 },
                                  {
                                      field: "CreatedDate",
                                      title: "Created Date",
                                      type: "date",
                                      filterable: HFCService.gridfilterableDate("date", "OrderSearchCreatedFilterCalender", offsetvalue),
                                      template: function (dataitem) {
                                          return HFCService.KendoDateFormat(dataitem.CreatedDate);
                                      }
                                          //"#= kendo.toString(kendo.parseDate(CreatedDate, 'yyyy-MM-dd'), 'M/d/yyyy')#"

                                  },
                                   {
                                       field: "ModifiedDate",
                                       title: "Modified Date",
                                       type: "date",
                                       filterable: HFCService.gridfilterableDate("date", "OrderSearchModifiedFilterCalender", offsetvalue),
                                       template: function (dataitem) {
                                           return HFCService.KendoDateFormat(dataitem.ModifiedDate);
                                       }
                                           //"#= kendo.toString(kendo.parseDate(ModifiedDate, 'yyyy-MM-dd'), 'M/d/yyyy') #"

                                   },
                                 {
                                     field: "",
                                     title: "",
                                     template: function (dataitem) {
                                         return DropdownTemplate(dataitem);
                                     },
                                    // attributes: { style: "vertical-align: middle;" },
                                     //template: '<ul ng-if="srv.PermissionEditOrder"> <li class="dropdown note1 ad_user"> <div class="btn-group"> <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">  <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> <ul class="dropdown-menu"> <li><a href="\\\\#!/orderEdit/#= OrderId #">Edit</a></li>   <li><a  ng-click="ShowInstallationPrint2(#= OrderId #)">Print</a></li></ul> </li>  </ul>'
                                 }

                        ],
                        filterable: {
                            extra: true,
                            operators: {
                                string: {
                                    contains: "Contains",
                                    eq: "Is equal to",
                                    neq: "Is not equal to"
                                },
                                date: {
                                    gt: "After (Excludes)",
                                    lt: "Before (Includes)"
                                },
                            }
                        },
                        noRecords: { template: "No records found" },
                        pageable: {
                            refresh: true,
                            pageSize: 25,
                            pageSizes: [25, 50, 100],
                            buttonCount: 5
                        },
                        sortable: ({ field: "CreatedOnUtc", dir: "desc" }),
                        
                        toolbar: kendo.template($(s).html()),
                    };
                };
               

                srv.Jobs = [];


                srv.LoadJobs = function (OrderId) {
                    srv.IsBusy = true;
                    return $http.get('/api/Orders/' + OrderId + '/GetJobsByOrderId').then(function (response) {

                        for (var i = 0; i < response.data.JobCollections.length; i++) {
                            srv.Jobs.push(response.data.JobCollections[i]);
                        }

                        srv.IsBusy = false;
                        // return response.data.JobDetails;
                    }, function (response) {
                        HFC.DisplayAlert(response.statusText);
                        srv.IsBusy = false;
                    });
                }

                srv.SelectDateRange = function (dateRange) {
                    srv.DateRangeText = dateRange;
                    var dates = HFCService.GetDateRange(srv.DateRangeText);
                    srv.StartDate = dates.StartDate;
                    srv.EndDate = dates.EndDate;
                }

                this.orderBy = function () {
                    return this.predicate.substr(1);
                };

                this.orderByDesc = function () {
                    return this.predicate.substr(0, 1) == '-';
                };
            }
])