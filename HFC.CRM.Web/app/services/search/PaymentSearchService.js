﻿'use strict';
app.service('PaymentSearchService', [
            '$http', 'HFCService', 'kendotooltipService', function ($http, HFCService, kendotooltipService) {
                var srv = this;

                srv.IsBusy = false;
                srv.Pagination = { page: 1, size: 50, pageTotal: 1 };
                srv.PageTotal = srv.Pagination.pageTotal;
                srv.PageSize = srv.Pagination.size;
                srv.PageNumber = srv.Pagination.page;
                srv.TotalRecords = 0; var offsetvalue = 0;
                srv.ChangePageSize = function () {

                }
                srv.PageSummary = '';


                //srv.TotalRecords = 0;
                srv.SearchTerm = "";
                srv.SearchFilter = "";
                srv.PaymentstatusIds = [];
                srv.DateRangeText = "";
                srv.StartDate = null;
                srv.EndDate = null;
                //srv.predicate = '-createdonutc';
                srv.DateRangeArray = ["All Date & Time", "Last 7 Days", "Last 30 Days", "This Week", "This Month", "This Year", "Last Week", "Last Month", "Last Year"];
                srv.SearchEnums = {};
                srv.Payments = [];
                srv.HFCService = HFCService;
                srv.Permission = {};
                var Paymentpermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Payment')
                var Reversepaypermission = Paymentpermission.SpecialPermission.find(x=>x.PermissionCode == 'ReversePayment ').CanAccess;
                srv.Permission.ReversePayment = Reversepaypermission;


                srv.Get = function (data) {
                    
                    var filters = {
                        pageIndex: srv.Pagination.page,
                        pageSize: srv.Pagination.size
                    };
                    srv.Payments = [];
                    var Id = 0;

                    if (data.AccountId != null) { Id = data.AccountId; } else { Id = 0; }
                    srv.Payments_options = {
                        cache: false,
                        dataSource: {
                            transport: {
                                read: {
                                    url: '/api/search/' + Id + '/GetSearchAccountPayments',
                                    dataType: "json",
                                    data: filters
                                }

                            },
                            error: function (e) {

                                HFC.DisplayAlert(e.errorThrown);
                            }
                        },
                        dataBound: function (e) {
                            if (!srv.Permission.ReversePayment) {
                                var grid = $("#gridpaymentSearch").data("kendoGrid");
                                grid.hideColumn("");
                            }
                            if (kendotooltipService.columnWidth) {
                                kendotooltipService.restrictTooltip(kendotooltipService.columnWidth);
                            } else if (window.innerWidth < 1280) {
                                kendotooltipService.restrictTooltip(null);
                            }
                        },
                        resizable: true,
                        noRecords: { template: "No records found" },
                        columns: [
                                                 {
                                                     field: "PaymentID",
                                                     title: "Payment ID",
                                                     type:"number",
                                                     filterable: { search: true },
                                                     template: function (dataItem) {
                                                         return "<span class='Grid_Textalign'>" + dataItem.PaymentID + "</span>";
                                                     },
                                                 },
                                            {
                                                field: "OpportunityName",
                                                title: "Opportunity Name",
                                                template: function (dataItem) {
                                                    if (dataItem.OpportunityName) {
                                                        return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.OpportunityName + "</div><a href='#!/OpportunityDetail/" + dataItem.OpportunityID + "' style='color: rgb(61,125,139);'><span>" + dataItem.OpportunityName + "</span></span>";
                                                    } else {
                                                        return "";
                                                    }
                                            }

                                            },
                                            {
                                                field: "Method",
                                                title: "Order Method",
                                                filterable: { multi: true, search: true },
                                                template: function (dataItem) {
                                                    if (dataItem.Method) {
                                                        return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.Method + "</div><span>" + dataItem.Method + "</span></span>";
                                                    } else {
                                                        return "";
                                                    }
                                                },
                                            },
                                            {
                                                field: "VerificationCheck",
                                                title: "Authorization",
                                                filterable: {search: true },
                                                template: function (dataItem) {
                                                    if (dataItem.VerificationCheck) {
                                                        return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.VerificationCheck + "</div><span>" + dataItem.VerificationCheck + "</span></span>";
                                                    } else {
                                                        return "";
                                                    }
                                                },
                                            },
                                                 {
                                                     field: "PaymentDate",
                                                     title: "Payment Date",
                                                     type: "date",
                                                     filterable: HFCService.gridfilterableDate("date", "PaymentFilterCalendar", offsetvalue),
                                                     template: function (dataItem) {
                                                         if (dataItem.PaymentDate) {
                                                             return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + HFCService.KendoDateFormat(dataItem.PaymentDate) + "</div><span>" + HFCService.KendoDateFormat(dataItem.PaymentDate) + "</span></span>";
                                                         } else {
                                                             return "";
                                                         }
                                                     }
                                                 },
                                                 {
                                                     field: "Amount",
                                                     title: "Amount",
                                                     type:"number",
                                                     attributes: { class: "text-right" },
                                                     //format: "{0:c}",
                                                     // template: "#= kendo.toString(Amount,'c', 'en-US') #",
                                                     //template: "#=(Amount < 0 ? '-$' : '$') + Math.abs(Amount) + (Amount.toString().indexOf('.') < 0 ? '.00' : '.00' ) #",
                                                     filterable: { search: true },
                                                     template: function (dataItem) {
                                                            // return "<span class='Grid_Textalign'><span class='tooltip-wrapper'><div class='tooltip-content'>" + HFCService.CurrencyFormat(dataItem.Amount) + "</div><span>" + HFCService.CurrencyFormat(dataItem.Amount) + "</span></span></span>";
                                                             if (dataItem.Reversal)
                                                                 return "<span class='Grid_Textalign'><span class='tooltip-wrapper'><div class='tooltip-content'>" + " - " + HFCService.CurrencyFormat(dataItem.Amount) + "</div><span>" + " - " + HFCService.CurrencyFormat(dataItem.Amount) + "</span></span></span>";
                                                             else
                                                                return "<span class='Grid_Textalign'><span class='tooltip-wrapper'><div class='tooltip-content'>" + HFCService.CurrencyFormat(dataItem.Amount) + "</div><span>" + HFCService.CurrencyFormat(dataItem.Amount) + "</span></span></span>";
                                                     }
                                                 },
                                                 {
                                                     field: "Memo",
                                                     title: "Memo",
                                                     filterable: {
                                                         //multi: true,
                                                         search: true
                                                     },
                                                     template: function (dataItem) {
                                                         if (dataItem.Memo) {
                                                             return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.Memo + "</div><span>" + dataItem.Memo + "</span></span>";
                                                         } else {
                                                             return "";
                                                         }
                                                     }
                                                 },
                                                 {
                                                     field: "",
                                                     title: "",
                                                     width: "60px",
                                                     template: function (dataitem) {
                                                         var ReversePayment = '<li><a ng-click="bringReversePaymentModal(' + dataitem.PaymentID + ', ' + dataitem.OrderID + ')">Reverse Payment</a></li>';
                                                         if (dataitem.Reversal || dataitem.OrderStatus == 6 || dataitem.OrderStatus == 9) {
                                                             var hcontent = "";
                                                         }
                                                         else {
                                                             var hcontent = `<ul>
                                                                            <li class ="dropdown note1 ad_user">
                                                                            <div class ="btn-group">
                                                                            <button  class ="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                                                <i class ="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i>
                                                                            </button>
                                                                            <ul class ="dropdown-menu pull-right">`
                                                                                                     + ReversePayment +
                                                                               `</ul>
                                                                            </div>
                                                                            </li>
                                                                            </ul>`;
                                                         }
                                                         return hcontent;
                                                     }
                                                 },
                                                 //{
                                                 //    field: "",
                                                 //    title: "",
                                                 //    template: '<ul> <li class="dropdown note1 ad_user"> <div class="btn-group"> <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">  <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> <ul class="dropdown-menu"> <li><a href="\\\\#!/PaymentsEdit/#= PaymentID #">Reverse Payment</a></li> </ul> </li>  </ul>'
                                                 //}

                        ],
                        filterable: {
                            extra: true,
                            operators: {
                                string: {
                                    contains: "Contains",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    isempty: "Empty",

                                },
                                date: {
                                    gt: "After (Excludes)",
                                    lt: "Before (Includes)"
                                },
                            }
                        },
                        pageable: {
                            refresh: true,
                            pageSize: 25,
                            pageSizes: [25, 50, 100],
                            buttonCount: 5,
                            change: function (e) {
                                var myDiv = $('.k-grid-content.k-auto-scrollable');
                                myDiv[0].scrollTop = 0;
                            }
                        },
                        sortable: ({ field: "PaymentDate", dir: "desc" }),
                        toolbar: kendo.template($(' <script id="template" type="text/x-kendo-template"><div class="row vendor_search no-margin" ><div class="leadsearch_topleft row no-margin col-sm-12 col-md-12 col-xs-12 no-padding"><div class="col-xs-2 col-sm-2 no-padding hfc_txtbox"><input ng-keyup="PaymentgridSearch()" type="search" id="searchBox" placeholder="Search Payment" class="k-textbox leadsearch_tbox"></div><div class="col-xs-2 col-sm-2 measurement_search no-padding"><input type="button" id="btnReset" ng-click="PaymentgridSearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div></div></div></script>').html()),
                        columnResize: function (e) {
                            $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 238);
                            getUpdatedColumnList(e);
                        }

                    };
                    srv.Payments_options = kendotooltipService.setColumnWidth(srv.Payments_options);
                };

                var getUpdatedColumnList = function (e) {
                    kendotooltipService.columnResizeTooltip(e.newWidth, e.column.headerAttributes.id);
                }

                function test() {
                    return 'test';
                }
                srv.SelectDateRange = function (dateRange) {
                    srv.DateRangeText = dateRange;
                    var dates = HFCService.GetDateRange(srv.DateRangeText);
                    srv.StartDate = dates.StartDate;
                    srv.EndDate = dates.EndDate;
                }
                srv.FormatAmount=function(amount)
                {
                    amount.toLocaleString('en-US', { style: 'currency', currency: 'USD' });
                }
                this.orderBy = function () {
                    return this.predicate.substr(1);
                };

                this.orderByDesc = function () {
                    return this.predicate.substr(0, 1) == '-';
                };
            }
])