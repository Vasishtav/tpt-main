﻿'use strict';
app.service('OpportunitySearchService', [
          '$http', 'HFCService', 'kendotooltipService',
          function ($http, HFCService, kendotooltipService) {
              var srv = this;

              srv.IsBusy = false;
              srv.Pagination = { page: 1, size: 50, pageTotal: 1 };
              srv.PageTotal = srv.Pagination.pageTotal;
              srv.PageSize = srv.Pagination.size;
              srv.PageNumber = srv.Pagination.page;
              srv.TotalRecords = 0;
              srv.ChangePageSize = function () {
              }
              srv.PageSummary = '';
              //srv.CalendarService = CalendarService;

              var Permission = {};
              var offsetvalue = 0;
              var Opportunitypermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Opportunity')

              Permission.EditOpportunity = Opportunitypermission.CanUpdate; 
              srv.SearchTerm = "";
              srv.SearchFilter = "";
              srv.OpportunityStatusIds = [];
              srv.DateRangeText = "";
              srv.StartDate = null;
              srv.EndDate = null;
              srv.DateRangeArray = ["All Date & Time", "Last 7 Days", "Last 30 Days", "This Week", "This Month", "This Year", "Last Week", "Last Month", "Last Year"];
              srv.SearchEnums = {};
              srv.Opportunitys = [];

              function DropdownTemplate(dataItem) {
                  var Div;
                  if (dataItem.OpportunityID) {
                      Div = '';
                      Div += '<ul> <li class="dropdown note1 ad_user"><div class="dropdown"><button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> <ul class="dropdown-menu pull-right"> ';
                      if (Permission.EditOpportunity)
                          Div += '<li><a href="#!/opportunityEdit/' + dataItem.OpportunityID + '">Edit</a> </li> ';
                      Div += '<li> <a href="#!/Calendar/opportunitySearch/' + dataItem.OpportunityID + '">Appointment</a></li> ';
                  } else {
                      Div = "";
                  }

                  return Div;
              }
              srv.Get = function (data) {
                  srv.Opportunitys = [];
                  var Id = 0;

                  if (data.AccountId != null) { Id = data.AccountId; } else { Id = 0; }
                  var filters = {
                      pageIndex: srv.Pagination.page,
                      pageSize: srv.Pagination.size
                  };
                  if (Id > 0) {
                      var s = ' <script id="template" type="text/x-kendo-template"><div class="vendor_search row no-margin" ><div class="leadsearch_topleft row no-margin col-sm-12 col-md-12 col-xs-12 no-padding"><div class="col-xs-2 col-sm-2 no-padding hfc_txtbox"><input ng-keyup="OpportunitygridSearch()" type="search" id="oppsearchBox" placeholder="Search Opportunity" class="k-textbox leadsearch_tbox"></div><div class="col-xs-2 col-sm-2 measurement_search no-padding"><input type="button" id="btnReset" ng-click="OpportunitygridSearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div></div></script>';
                  }
                  else {
                      var s = '<script id="template" type="text/x-kendo-template"><div class="tp_leadsearch row no-margin"><div class="leadsearch_topleft row no-margin col-sm-10 col-md-10 col-xs-10 no-padding"><div class="col-xs-2 col-sm-2 no-padding hfc_txtbox"><input ng-keyup="OpportunitygridSearch()" type="search" id="oppsearchBox" placeholder="Search Opportunity" class="k-textbox leadsearch_tbox"></div><div class="col-xs-2 col-sm-2 no-padding hfc_leadbut"><input type="button" id="btnReset" ng-click="OpportunitygridSearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div></div><div class="col-sm-2 col-xs-2 col-md-2 no-padding"><div class="leadsearch_icon"><button type="button" class="plus_but download_but tooltip-bottom" data-tooltip="Download Opportunity" ng-click="DownloadOpportunity_Excel()"><i class="far fa-cloud-download"></i></button><button ng-show="PermissionOpportunity.CanUpdate" type="button" class="plus_but tooltip-bottom" data-tooltip="New Opportunity" ng-click="NewOpportunitypage()"><i class="far fa-plus-square"></i></button></div></div></div></script>';
                  }
                  var ds = new kendo.data.DataSource({
                      transport: {
                          read: {
                              url: '/api/search/' + Id + '/GetSearchOpportunitys',
                              dataType: "json",
                              data: filters
                          }
                      },
                      error: function (e) {
                          HFC.DisplayAlert(e.errorThrown);
                      },
                      pageSize: 25,
                      schema: {
                          model: {
                              fields: {
                                  ContractDate: { type: "date" },
                                  OpportunityNumber: { type: "number", editable: false, nullable: true },
                                  OrderAmount: { type: "number" }
                              }
                          }
                      }
                  })

                  srv.Opportunitys_options = {
                      cache: false,
                      excel: {
                          fileName: "Opportunities_Export.xlsx",
                          allPages: true
                      },
                      dataSource: ds,
                      // https://www.telerik.com/forums/remove-page-if-no-records
                      dataBound: function (e) {
                          //console.log("dataBound");
                          if (this.dataSource.view().length == 0) {
                              // The Grid contains No recrods so hide the footer.
                              $('.k-pager-nav').hide();
                              $('.k-pager-numbers').hide();
                              $('.k-pager-sizes').hide();
                          } else {
                              // The Grid contains recrods so show the footer.
                              $('.k-pager-nav').show();
                              $('.k-pager-numbers').show();
                              $('.k-pager-sizes').show();
                          }
                          if (kendotooltipService.columnWidth) {
                             kendotooltipService.restrictTooltip(kendotooltipService.columnWidth);
                          } else if (window.innerWidth < 1280) {
                              kendotooltipService.restrictTooltip(null);
                          }
                      },
                      resizable: true,
                      columns: [
                           {
                               field: "Territory",
                               title: "Territory",
                               template: function (dataItem) {
                                   if (dataItem.Territory) {
                                       return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.Territory + "</div><span>" + dataItem.Territory + "</span></span>";
                                   } else {
                                       return "";
                                   }
                               },
                               width: "100px",
                               filterable: { multi: true, search: true }
                           },
                                   {
                                       field: "OpportunityNumber",
                                       title: "ID",
                                       template: function (dataItem) {
                                           if (dataItem.OpportunityNumber) {
                                               return "<span class='Grid_Textalign'><span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.OpportunityNumber + "</div><span>" + dataItem.OpportunityNumber + "</span></span></span>";
                                           } else {
                                               return "";
                                           }
                                       },
                                       width: "100px",
                                       filterable: {
                                           //multi: true,
                                           search: true,
                                           //dataSource: ds
                                       }
                                   },
                          {
                              field: "OpportunityFullName",
                              title: "Opportunity Name",
                              width: "100px",
                              template: function (dataItem) {
                                  if (dataItem.OpportunityFullName) {
                                      return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.OpportunityFullName + "</div><a href='/#!/OpportunityDetail/" + dataItem.OpportunityID + "' style='color: rgb(61,125,139);'>" + dataItem.OpportunityFullName + "</a></span>";
                                  } else {
                                      return "";
                                  }
                              }
                              //template: "<a href='\\\\#!/OpportunityDetail/#= OpportunityID #' style='color: rgb(61,125,139);'><span >#= OpportunityFullName #</span></a> "
                          },
                          {
                              field: "AccountName",
                              title: "Account Name",
                              width: "100px",
                              template: function (dataItem) {
                                  if (dataItem.AccountName) {
                                      return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.AccountName + "</div><a href='/#!/Accounts/" + dataItem.AccountID + "' style='color: rgb(61,125,139);'>" + dataItem.AccountName + "</a></span>";
                                  } else {
                                      return "";
                                  }
                              }
                              //template: "<a href='\\\\#!/Accounts/#= AccountID #' style='color: rgb(61,125,139);'><span >#= AccountName #</span></a> "
                          },
                          {
                              field: "OpportunityStatus",
                              title: "Status",
                              template: function (dataItem) {
                                  if (dataItem.OpportunityStatus) {
                                      return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.OpportunityStatus + "</div><span>" + dataItem.OpportunityStatus + "</span></span>";
                                  } else {
                                      return "";
                                  }
                              },
                              width: "100px",
                              filterable: { multi: true, search: true }
                          },
                           {
                               field: "SalesAgentName",
                               title: "Sales Agent",
                               template: function (dataItem) {
                                   if (dataItem.SalesAgentName) {
                                       return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.SalesAgentName + "</div><span>" + dataItem.SalesAgentName + "</span></span>";
                                   } else {
                                       return "";
                                   }
                               },
                               width: "120px",
                               //filterable: { multi: true, search: true }
                           },
                               {
                                   field: "OrderAmount",
                                   title: "Order Amount",
                                   template: function (dataItem) {
                                           return "<span class='Grid_Textalign'><span class='tooltip-wrapper'><div class='tooltip-content'>" + HFCService.CurrencyFormat(dataItem.OrderAmount) + "</div><span>" + HFCService.CurrencyFormat(dataItem.OrderAmount) + "</span></span></span>";
                                   },
                                   width: "120px",
                                  // format: "{0:c2}",
                                   attributes: { class: 'text-right' },
                                   //filterable: { multi: true, search: true }
                               },

                               {
                                   field: "ContractDate",
                                   title: "Contract Date",
                                   width: "100px",
                                   type: "date",
                                   filterable: HFCService.gridfilterableDate("date", "OpportunityFilterCalendar", offsetvalue),
                                   template: function (dataItem) {
                                       if (dataItem.ContractDate) {
                                           return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + HFCService.KendoDateFormat(dataItem.ContractDate) + "</div><span>" + HFCService.KendoDateFormat(dataItem.ContractDate) + "</span></span>";
                                       }
                                       else {
                                           return "";
                                       }
                                   }
                               }
                              ,
                              {
                                  field: "",
                                  title: "",
                                  width: "100px",
                                  attributes: { style: "vertical-align: middle;" },
                                  template: function (dataItem) {
                                      return DropdownTemplate(dataItem);
                                  }
                              }

                      ],
                      filterable: {
                          extra: true,
                          operators: {
                              string: {
                                  contains: "Contains",
                                  eq: "Is equal to",
                                  neq: "Is not equal to",
                                  isempty: "Empty",
                                  isnotempty: "Not empty"


                              },
                              number: {
                                  eq: "Equal to",
                                  neq: "Not equal to",
                                  gte: "Greater than or equal to",
                                  lte: "Less than or equal to"
                              },
                              date: {
                                  gt: "After (Excludes)",
                                  lt: "Before (Includes)"
                              },
                          }
                      },
                      filterMenuInit: function (e) {
                          $(e.container).find('.k-check-all').click()
                      },
                      noRecords: { template: "No records found" },
                      pageable: {
                          refresh: true,
                          pageSize: 25,
                          resizable: true,
                          pageSizes: [25, 50, 100],
                          buttonCount: 5, change: function (e) {
                              var myDiv = $('.k-grid-content.k-auto-scrollable');
                              myDiv[0].scrollTop = 0;
                          }
                      },
                      sortable: ({ field: "ContractDate", dir: "desc" }),
                      //toolbar: kendo.template($(' <script id="template" type="text/x-kendo-template"><input ng-keyup="OpportunitygridSearch()" type="search" id="searchBox" placeholder="Search any column" class="k-textbox form-control frm_controlOpportunity" style="width: 250px !important;margin-left: 0px !important;"><input type="button" id="btnReset" ng-click="OpportunitygridSearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"><input type="button" id="btnReset" ng-click="NewOpportunitypage()" class="k-button btn btn-primary cancel_but" value="New Opportunity"></script>').html()),
                      toolbar: kendo.template($(s).html()),
                      columnResize: function (e) {
                          $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 238);
                          getUpdatedColumnList(e);
                      }
                  };
              };

              var getUpdatedColumnList = function (e) {
                  kendotooltipService.columnResizeTooltip(e.newWidth, e.column.headerAttributes.id);
              }

              // Need this???
              //this.orderBy = function () {
              //    return this.predicate.substr(1);
              //};

              //this.orderByDesc = function () {
              //    return this.predicate.substr(0, 1) == '-';
              //};
          }
])