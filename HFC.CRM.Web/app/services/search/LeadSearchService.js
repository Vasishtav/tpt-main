﻿'use strict';
app.service('LeadSearchService', [
 '$http', 'HFCService', 'CalendarService','kendotooltipService',
 function ($http, HFCService, CalendarService, kendotooltipService) {
     var srv = this;

     srv.IsBusy = false;
     srv.Pagination = {
         page: 1,
         size: 50,
         pageTotal: 1
     };
     srv.PageTotal = srv.Pagination.pageTotal;
     srv.PageSize = srv.Pagination.size;
     srv.PageNumber = srv.Pagination.page;
     srv.TotalRecords = 0;
     srv.CalendarService = CalendarService;
     srv.ChangePageSize = function () {

     }
     srv.PageSummary = '';

     srv.SearchTerm = "";
     srv.SearchFilter = "";
     srv.LeadStatusIds = [];
     srv.DateRangeText = "";
     srv.StartDate = null;
     srv.EndDate = null;
     srv.DateRangeArray = ["All Date & Time", "Last 7 Days", "Last 30 Days", "This Week", "This Month", "This Year", "Last Week", "Last Month", "Last Year"];
     srv.SearchEnums = {};
     srv.Leads = [];

     var Leadpermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'Lead')

     srv.PermissionAddLead = Leadpermission.CanCreate;
     srv.PermissionEditLead = Leadpermission.CanUpdate; 
     srv.PermissionQualifyLeadEdit = Leadpermission.CanUpdate; 

     var PermissionAddLead = srv.PermissionAddLead;
     var offsetvalue = 0;

     function DropdownTemplate(dataitem) {
         var PermissionSetAppointment = srv.PermissionSetAppointment;
         var PermissionEditLead = srv.PermissionEditLead;
         var PermissionQualifyLeadEdit = srv.PermissionQualifyLeadEdit;

         var Edithtml = "";
         var appointmenthtml = "";
         var EditQualifyhtml = "";

         appointmenthtml = '<li><a href="#!/Calendar/leadsSearch/' + dataitem.LeadId + '">Appointment</a></li>';

         if (PermissionEditLead == true) {
             Edithtml = '<li><a href="#!/leadsEdit/' + dataitem.LeadId + '">Edit</a> </li>';
         }
         if (PermissionQualifyLeadEdit == true) {
             EditQualifyhtml = '<li><a href="#!/leadsEditQualify/' + dataitem.LeadId + '" >Qualify</a></li>';
         }

         return '<ul> <li class="dropdown note1 ad_user"><div class="dropdown">' +
          '<button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> ' +
          '<i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> ' +
          '<ul class="dropdown-menu pull-right">' + Edithtml + EditQualifyhtml + appointmenthtml +
          ' <li><a href="javascript:void(0)" ' +
          'ng-click="printSalesPacket2(\'modal_print\', ' + dataitem.LeadId +
          ')">Print</a></li> ' +
          '</ul> </li> </ul>';
     }

     var searchHeaderTemplate = $("#leadSearchHeaderTemplate").html();

              function maskPhoneNumber(text) {
         if (text != null && text !== undefined) {
             return text.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');
         } else {
             return '';
         }
     }

     function maskedDisplayPhone(phone) {
         var Div;
         if (phone) {
             Div = "<span class='tooltip-wrapper'><div class='tooltip-content'>";
             if (phone && phone != "")
                 Div += maskPhoneNumber(phone) + "</div><span>";
             if (phone && phone != "")
                 Div += maskPhoneNumber(phone)
             Div += "</span></span>";
         } else {
             Div = "";
         }


         return Div;
     }

     function tooltipDescription(dataitem, key) {
         var templateString = "";
         // template += '<label class="tooltipcase_viewhover" title=' + dataitem.Description + '> ' + dataitem.Description + '</label>';
         if (key && dataitem[key]) {
             if (typeof (dataitem[key]) == "string")
                 dataitem[key] = dataitem[key].toString();
             var erer = dataitem[key].replace('"', "");
             templateString += '<p>' + dataitem[key] + '</p>'
         }
         return templateString;
     }

     //for address data hiding issue on search
     function addressToString(isSingleLine, model) {
         if (model) {

             var str = "";
             if (model.Address1)
                 str = model.Address1 + (model.Address2 ? (", " + model.Address2) : "") + (isSingleLine ? ", " : "<br/>");

             return str;
         }
     };

     function MapDestinationaddress(model) {
         if (model) {

             var str = "";
             if (model.Address1)
                 str = model.Address1 + (model.Address2 ? (", " + model.Address2 + " ") : " ");

             if (model.City)
                 str += model.City + ", " + (model.State || "") + " " + (model.ZipCode || "");
             else
                 str += (model.ZipCode || "");

             return str;
         }
     };


     srv.GetGoogleAddressHref = function (model) {
         var dest = MapDestinationaddress(model);
         if (model.Address1) {

             var srcAddr;

             if (HFC.currentGeoLocation && HFC.Browser.isMobile()) {
                 srcAddr = HFC.currentGeoLocation.coords.latitude + "," + HFC.currentGeoLocation.coords.longitude;
             } else {
                 srcAddr = encodeURIComponent(HFC.CurrentUserAddress);
             }

             if (navigator.userAgent.toLowerCase().indexOf("iphone") == -1 && navigator.userAgent.toLowerCase().indexOf("ipad") == -1) {
                 return '//maps.google.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
             } else {
                 return '//maps.apple.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
             }
         } else
             return null;
     }

     srv.AddressToString = function (model) { return addressToString(true, model) };

     srv.Get = function (data) {
         srv.Leads = [];
         var filters = {
             pageIndex: srv.Pagination.page,
             pageSize: srv.Pagination.size
         };
         srv.Leads_options = {
             cache: false,
             excel: {
                 fileName: "Leads_Export.xlsx",
                 allPages: true
             },
             dataSource: {
                 transport: {
                     read: {
                         url: '/api/search/0/GetSearchLeads',
                         dataType: "json",
                         
                     }

                 },
                 error: function (e) {

                     HFC.DisplayAlert(e.errorThrown);
                 }
             },
             // https://www.telerik.com/forums/remove-page-if-no-records
             dataBound: function (e) {
                 // auto fit column
                 //for (var i = 0; i < this.columns.length; i++) {
                 //    this.autoFitColumn(i);
                 //}
                 // end auto fit column
                 //console.log("dataBound");
                 if (this.dataSource.view().length == 0) {
                     // The Grid contains No recrods so hide the footer.
                     $('.k-pager-nav').hide();
                     $('.k-pager-numbers').hide();
                     $('.k-pager-sizes').hide();
                 } else {
                     // The Grid contains recrods so show the footer.
                     $('.k-pager-nav').show();
                     $('.k-pager-numbers').show();
                     $('.k-pager-sizes').show();
                 }
                 if (kendotooltipService.columnWidth) {
                     kendotooltipService.restrictTooltip(kendotooltipService.columnWidth);
                 } else if (window.innerWidth < 1280) {
                     kendotooltipService.restrictTooltip(null);
                 }
             },
             resizable: true,
             filterable: {
                 extra: true,
                 operators: {
                     string: {
                         contains: "Contains",
                         eq: "Is equal to",
                         neq: "Is not equal to",
                         isempty: "Empty",
                         isnotempty: "Not empty"


                     },
                     number: {
                         eq: "Equal to",
                         neq: "Not equal to",
                         gte: "Greater than or equal to",
                         lte: "Less than or equal to"
                     },
                     date: {
                         gt: "After (Excludes)",
                         lt: "Before (Includes)"
                     },
                 }
             },
             columns: [{
                 field: "Territory",
                 title: "Territory",
                 //minScreenWidth: 1280,
                 //minResizableWidth: 100,
                 //maxResizableWidth: 100,

                 template: function (dataItem) {
                     if (dataItem.Territory) {
                         return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.Territory + "</div><span>" + dataItem.Territory + "</span></span>";
                     } else {
                         return "";
                     }
                 },
                 width: "150px",
                 filterable: {
                     multi: true,
                     search: true
                 },
             },
              {
                  field: "LeadFullName",
                  title: "Name",
                  width: "150px",
                  template: function (dataItem) {
                      // , "<a href='\\\\#!/leads/#= LeadId #' style='color: rgb(61,125,139);'><span>#= LeadFullName #</span></a>"
                      //return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.LeadFullName + "</div>" + <"a href='\\\\#!/leads/#= LeadId #' style='color: rgb(61,125,139);'"> + dataItem.LeadFullName + "</a></a></span>";
                      if (dataItem.LeadFullName) {
                          return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.LeadFullName + "</div><a href='/#!/leads/" + dataItem.LeadId + "' style='color: rgb(61,125,139);'>" + dataItem.LeadFullName + "</a></span>";
                      } else {
                          return "";
                      }
                  }
              },
              {
                  field: "Status",
                  title: "Status",
                  width: "100px",
                  filterable: {
                      multi: true,
                      search: true
                  },
                  template: function (dataItem) {
                      if (dataItem.Status) {
                          return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.Status + "</div><span>" + dataItem.Status + "</span></span>";
                      } else {
                          return "";
                      }
                      //return "<span class='tooltip-wrapper' data-tooltip='" + dataItem.Status + "'>" + dataItem.Status + "</span>";
                      //return tooltipDescription(dataItem, 'Status');;
                  }
              },
              {
                  field: "Channel",
                  title: "Channel",
                  width: "120px",
                  filterable: {
                      //multi: true,
                      search: true
                  },
                  template: function (dataItem) {
                      if (dataItem.Channel) {
                          return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.Channel + "</div><span>" + dataItem.Channel + "</span></span>";
                      } else {
                          return "";
                      }
                      //return "<span class='tooltip' data-tooltip='" + dataItem.Channel + "'>" + dataItem.Channel + "</span></a></span>";
                      //return tooltipDescription(dataItem, 'Channel');
                  }
              },
              {
                  field: "source",
                  title: "Source",
                  width: "120px",
                  filterable: {
                      //multi: true,
                      search: true
                  },
                  template: function (dataItem) {
                      if (dataItem.source) {
                          return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.source + "</div><span>" + dataItem.source + "</span></span>";
                      } else {
                          return "";
                      }
                      //return "<span class='tooltip' data-tooltip='" + dataItem.source + "'>" + dataItem.source + "</span></a></span>";
                      //return tooltipDescription(dataItem, 'source');;
                  }
              },
              {
                  field: "Campaign",
                  title: "Campaign",
                  width: "120px",
                  filterable: {
                      //multi: true,
                      search: true
                  },
                  template: function (dataItem) {

                      if (dataItem.Campaign)
                          return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.Campaign + "</div><span>" + dataItem.Campaign + "</span></span>";
                      else
                          return '';
                      //return "<span class='tooltip' data-tooltip='" + dataItem.Campaign + "'>" + dataItem.Campaign + "</span></a></span>";
                      //return tooltipDescription(dataItem, 'Campaign');;
                  }
              },
              {

                  field: "Address1",
                  title: "Address",
                  width: "150px",
                  //filterable: {  search: true }
                  template: function (dataItem) {                 
                      var address = srv.AddressToString(dataItem);
                      var googleaddress = srv.GetGoogleAddressHref(dataItem);
                      var templateString = "<address class=' inline-relative' hfc-googlemaps='' addr='dataItem'><span class='tooltip-wrapper'><div class='tooltip-content'></div> <a style='color: rgb(61,125,139);' target='_blank' href="+googleaddress+" ng-cloak >" + address + "</a></span> <div  class='mapCanvas'></div> </address>";
                      return templateString;
                      //ng-href='{{ GetGoogleAddressHref(dataItem) }}' ng-bind-html='AddressToString(dataItem) | unsafe'
                  }

              },
              {

                  field: "City",
                  title: "City",
                  width: "100px",
                  filterable: {
                      //multi: true,
                      search: true
                  },
                  template: function (dataItem) {
                      if (dataItem.City) {
                          return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.City + "</div><span>" + dataItem.City + "</span></span>";
                      } else {
                          return "";
                      }
                      //return "<span class='tooltip' data-tooltip='" + dataItem.City + "'>" + dataItem.City + "</span></a></span>";
                      //return tooltipDescription(dataItem, 'City');;
                  }
              },
              {

                  field: "State",
                  title: "State",
                  width: "100px",
                  filterable: {
                      //multi: true,
                      search: true
                  },
                  template: function (dataItem) {
                      if (dataItem.State) {
                          return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.State + "</div><span>" + dataItem.State + "</span></span>";
                      } else {
                          return "";
                      }
                      //return "<span class='tooltip' data-tooltip='" + dataItem.State + "'>" + dataItem.State + "</span></a></span>";
                      //return tooltipDescription(dataItem, 'State');;
                  }
              },
              {

                  field: "ZipCode",
                  title: "Postal Code",
                  width: "100px",
                  filterable: {
                      //multi: true,
                      search: true
                  },
                  template: function (dataItem) {
                      if (dataItem.ZipCode) {
                          return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.ZipCode + "</div><span>" + dataItem.ZipCode + "</span></span>";
                      } else {
                          return "";
                      }
                      //return "<span class='tooltip' data-tooltip='" + dataItem.ZipCode + "'>" + dataItem.ZipCode + "</span></a></span>";
                      //return tooltipDescription(dataItem, 'ZipCode');;
                  }
              },
              {
                  field: "DisplayPhone",
                  title: "Phone",
                 
                  width: "150px",
                  filterable: {
                      //multi: true,
                      search: true
                  },
                  template: function (dataItem) {
                      return maskedDisplayPhone(dataItem.DisplayPhone);
                  } // "#= formatData(foo) #"
              },
                   {

                       field: "PrimaryEmail",
                       title: "Email",
                       width: "150px",
                       filterable: {
                           //multi: true,
                           search: true
                       },
                       template: function (dataItem) {
                           if (dataItem.PrimaryEmail) {
                               return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.PrimaryEmail + "</div><span>" + dataItem.PrimaryEmail + "</span></span>";
                           } else {
                               return "";
                           }
                           //return "<span class='tooltip' data-tooltip='" + dataItem.PrimaryEmail + "'>" + dataItem.PrimaryEmail + "</span>";
                           //return tooltipDescription(dataItem, 'PrimaryEmail');;
                       }
                   },
              {
                  field: "CreatedOnUtc",
                  title: "Created On",
                  width: "100px",
                  type: "date",
                  filterable: HFCService.gridfilterableDate("date", "LeadFilterCalendar", offsetvalue),
                  template: function (dataItem) {
                      if (dataItem.CreatedOnUtc) {  ``
                          return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + HFCService.KendoDateFormat(dataItem.CreatedOnUtc) + "</div><span>" + HFCService.KendoDateFormat(dataItem.CreatedOnUtc) + "</span></span>";
                           //return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + kendo.toString(kendo.parseDate(dataItem.CreatedOnUtc, 'yyyy-MM-dd'), 'M/d/yyyy') + "</div><span>" + kendo.toString(kendo.parseDate(dataItem.CreatedOnUtc, 'yyyy-MM-dd'), 'M/d/yyyy') + "</span></span>";
                      } else {
                          return "";
                      }
                      //return "<span class='tooltip' data-tooltip='" + kendo.toString(kendo.parseDate(dataItem.CreatedOnUtc, 'yyyy-MM-dd'), 'M/d/yyyy') + "'>" + kendo.toString(kendo.parseDate(dataItem.CreatedOnUtc, 'yyyy-MM-dd'), 'M/d/yyyy') + "</span>";
                  }
              }, {
                  field: "",
                  title: "",
                  width: "100px",
                  template: function (dataItem) {
                      var templateString = DropdownTemplate(dataItem);
                      var tooltipTemplate = templateString;
                      return tooltipTemplate;
                  },
                  width: 62
              }
             ],
             noRecords: {
                 template: "No records found"
             },
           
         
             pageable: {
                 refresh: true,
                 pageSize: 25,
                 resizable: true,
                 pageSizes: [25, 50, 100],
                 buttonCount: 5,
                 change: function (e) {
                     var myDiv = $('.k-grid-content.k-auto-scrollable');
                     myDiv[0].scrollTop = 0;
                 }
             },
             sortable: ({
                 field: "CreatedOnUtc",
                 dir: "desc"
             }),
             toolbar: kendo.template(searchHeaderTemplate),
             columnResize: function (e) {
                 $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 238);
                 getUpdatedColumnList(e);
             }
         };

     };

     var getUpdatedColumnList = function (e) {
         kendotooltipService.columnResizeTooltip(e.newWidth, e.column.headerAttributes.id);
     }

     $(function () {
         $("span.tooltip").tooltip({
             position: {
                 my: "left center",
                 at: "right center"
             }
         });
     });
     this.orderBy = function () {
         return this.predicate.substr(1);
     };

     this.orderByDesc = function () {
         return this.predicate.substr(0, 1) == '-';
     };
 }
])