﻿'use strict';
app.service('JobSearchService', [
            '$http', 'HFCService', function ($http, HFCService) {
                var srv = this;
                srv.IsBusy = false;
                srv.Pagination = { page: 1, size: 50, pageTotal: 1 };
                srv.PageTotal = srv.Pagination.pageTotal;
                srv.PageSize = srv.Pagination.size;
                srv.PageNumber = srv.Pagination.page;
                srv.TotalRecords = 0;
                srv.ChangePageSize = function () {

                }
                srv.PageSummary = '';


                //srv.TotalRecords = 0;
                srv.SearchTerm = "";
                srv.SearchFilter = "";
                srv.LeadStatusIds = [];
                srv.DateRangeText = "";
                srv.StartDate = null;
                srv.EndDate = null;
                //srv.predicate = '-createdonutc';
                srv.DateRangeArray = ["All Date & Time", "Last 7 Days", "Last 30 Days", "This Week", "This Month", "This Year", "Last Week", "Last Month", "Last Year"];
                srv.SearchEnums = {};
                srv.Jobs = [];


                srv.Get = function (data) {
                    srv.Jobs = [];
                    $http.get('/api/search/0/getJobs?' + jQuery.param(data)).then(
                        function (result) {

     //                       var data = [
     //[
     //   { Row: 0, Col: 0 },
     //   { Row: 0, Col: 1 },
     //   { Row: 0, Col: 2 },
     //   { Row: 0, Col: 3 },
     //   { Row: 0, Col: 4 },
     //   { Row: 0, Col: 5 },
     //   { Row: 0, Col: 6 }
     //],
     //[
     //   { Row: 1, Col: 0 },
     //   { Row: 1, Col: 1 },
     //   { Row: 1, Col: 2 },
     //   { Row: 1, Col: 3 },
     //   { Row: 1, Col: 4 },
     //   { Row: 1, Col: 5 },
     //   { Row: 1, Col: 6 }
     //]
                            //                       ];
                    //        <td>
                    //    <a class="text-truncate" href="{{ '#/leads/'+ job.LeadId+'/'+ job.JobId }}">
                    //        <span class="glyphicon glyphicon-new-window"></span> <span>{{ ::job.JobNumber }}</span>
                    //    </a>
                    //</td>
                    //<td>
                    //    <a class="text-truncate" href="{{  '#/leads/'+ job.LeadId+'/'+ job.JobId }}">
                    //        <b>{{::job.CompanyName ? job.CompanyName : job.FullName }}</b>
                    //    </a>
                    //    <div ng-bind-html="::job.PreferredTFNHtml | unsafe"></div>
                    //</td>

                            
                            //$('#trTemplate').tmpl(result.data.JobCollection).appendTo('#containerTable');
                            srv.Jobs = result.data.JobCollection;
                            srv.TotalRecords = result.data.TotalRecords;
                            srv.Pagination.pageTotal = srv.TotalRecords ? Math.ceil(srv.TotalRecords / srv.Pagination.size) : 1;

                            if (srv.TotalRecords) {
                                var end = srv.Pagination.size * (srv.Pagination.page);
                                if (end > srv.TotalRecords)
                                    end = srv.TotalRecords;
                                if (srv.Pagination.pageTotal > 1)
                                    srv.PageSummary = ((srv.Pagination.page * srv.Pagination.size) + 1 - srv.Pagination.size) + " - " + end + " out of " + srv.TotalRecords;
                                else
                                    srv.PageSummary = srv.TotalRecords + " total records";
                            }
                        });
                }

                srv.qoutes = [];

                srv.GetPrimeQuote = function (jobId) {
                    for (var i = 0; i < srv.qoutes.length; i++) {
                        if (srv.qoutes[i].JobId == jobId) {
                            return srv.qoutes[i];
                        }
                    }
                }

                srv.GetPrimeQuoteLoad = function (jobId) {
                    return $http.get('/api/jobs/' + jobId + '/QoutePrimeDetailsByJobId').then(function (response) {
                        srv.qoutes.push(response.data.JobDetails);
                        srv.IsBusy = false;
                        return response.data.JobDetails;
                    }, function (response) {
                        HFC.DisplayAlert(response.statusText);
                        srv.IsBusy = false;
                    });
                }

                srv.SelectDateRange = function (dateRange) {
                    srv.DateRangeText = dateRange;
                    var dates = HFCService.GetDateRange(srv.DateRangeText);
                    srv.StartDate = dates.StartDate;
                    srv.EndDate = dates.EndDate;
                }

                this.orderBy = function () {
                    return this.predicate.substr(1);
                };

                this.orderByDesc = function () {
                    return this.predicate.substr(0, 1) == '-';
                };
            }
])