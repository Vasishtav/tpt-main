﻿'use strict';
app.service('InvoiceSearchService', [
            '$http', 'HFCService', function ($http, HFCService) {
                var srv = this;

                srv.IsBusy = false;
                srv.Pagination = { page: 1, size: 50, pageTotal: 1 };
                srv.PageTotal = srv.Pagination.pageTotal;
                srv.PageSize = srv.Pagination.size;
                srv.PageNumber = srv.Pagination.page;
                srv.TotalRecords = 0;
                srv.ChangePageSize = function () {

                }
                srv.PageSummary = '';

                var offsetvalue = 0;
                //srv.TotalRecords = 0;
                srv.SearchTerm = "";
                srv.SearchFilter = "";
                srv.InvoicestatusIds = [];
                srv.DateRangeText = "";
                srv.StartDate = null;
                srv.EndDate = null;
                //srv.predicate = '-createdonutc';
                srv.DateRangeArray = ["All Date & Time", "Last 7 Days", "Last 30 Days", "This Week", "This Month", "This Year", "Last Week", "Last Month", "Last Year"];
                srv.SearchEnums = {};
                srv.Invoices = [];


                srv.Get = function (data) {
                    
                    var filters = {
                        pageIndex: srv.Pagination.page,
                        pageSize: srv.Pagination.size
                    };
                    srv.Invoices = [];
                    var Id = 0;

                    if (data.AccountId != null) { Id = data.AccountId; } else { Id = 0; }
                    srv.Invoices_options = {
                        cache: false,
                        dataSource: {
                            transport: {
                                read: {
                                    url: '/api/search/' + Id + '/GetSearchAccountInvoices',
                                    dataType: "json",
                                    data: filters
                                }

                            },
                            error: function (e) {

                                HFC.DisplayAlert(e.errorThrown);
                            }
                        },
                        resizable: true,
                        noRecords: { template: "No records found" },
                        columns: [
                                                 {
                                                     field: "InvoiceId",
                                                     title: "Invoice ID",
                                                     filterable: { multi: true, search: true }
                                                 },
                                            {
                                                field: "OpportunityName",
                                                title: "Opportunity Name",
                                                template: "<a href='\\\\#!/OpportunityDetail/#= OpportunityID #' style='color: rgb(61,125,139);'><span style='padding: 10px;'>#= OpportunityName #</span></a> "

                                            },
                                            {
                                                field: "InvoiceType",
                                                title: "Invoice Type",
                                                filterable: { multi: true, search: true }
                                            },
                                          
                                                 {
                                                     field: "InvoiceDate",
                                                     title: "Invoice Date",
                                                     type: "date",
                                                     filterable: HFCService.gridfilterableDate("date", "InvoiceSearchDateFilterCalender",offsetvalue),
                                                     template: function (dataitem) {
                                                         return HFCService.KendoDateFormat(dataitem.InvoiceDate);
                                                     }
                                                         //"#= kendo.toString(kendo.parseDate(InvoiceDate, 'yyyy-MM-dd'), 'M/d/yyyy')#"
                                                 },
                                                 {
                                                     field: "TotalAmount",
                                                     title: "Total Amount",
                                                     format: "{0:c2}",
                                                     attributes: { class: "text-right" },
                                                     //template: "#=(TotalAmount < 0 ? '-$' : '$') + Math.abs(TotalAmount) + (TotalAmount.toString().indexOf('.') < 0 ? '.00' : '.00' ) #",
                                                     filterable: { multi: true, search: true }
                                                 },
                                                 {
                                                     field: "Balance",
                                                     title: "Balance",
                                                     format: "{0:c2}",
                                                     attributes: { class: "text-right" },
                                                     //template: "#=(Balance < 0 ? '-$' : '$') + Math.abs(Balance) + (Balance.toString().indexOf('.') < 0 ? '.00' : '.00' ) #",
                                                     filterable: { multi: true, search: true }
                                                 },
                                                 //{
                                                 //    field: "",
                                                 //    title: "",
                                                 //    template: '<ul> <li class="dropdown note1 ad_user"> <div class="btn-group"> <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">  <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> <ul class="dropdown-menu"> <li><a href="\\\\#!/InvoicesEdit/#= InvoiceId #">Reverse Invoice</a></li> </ul> </li>  </ul>'
                                                 //}

                        ],
                        filterable: {
                            extra: true,
                            operators: {
                                string: {
                                    contains: "Contains",
                                    eq: "Is equal to",
                                    neq: "Is not equal to"
                                },
                                date: {
                                    gt: "After (Excludes)",
                                    lt: "Before (Includes)"
                                },
                            }
                        },
                        pageable: {
                            refresh: true,
                            pageSize: 25,
                            pageSizes: [25, 50, 100, 'All'],
                            buttonCount: 5
                        },
                        sortable: ({ field: "CreatedOnUtc", dir: "desc" }),
                        toolbar: kendo.template($(' <script id="template" type="text/x-kendo-template"><div class="vendor_search"><input ng-keyup="InvoicegridSearch()" type="search" id="searchBox" placeholder="Search Account Invoice" class="k-textbox leadsearch_tbox"><input type="button" id="btnReset" ng-click="InvoicegridSearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div></script>').html()),

                    };
                };


                srv.SelectDateRange = function (dateRange) {
                    srv.DateRangeText = dateRange;
                    var dates = HFCService.GetDateRange(srv.DateRangeText);
                    srv.StartDate = dates.StartDate;
                    srv.EndDate = dates.EndDate;
                }

                this.orderBy = function () {
                    return this.predicate.substr(1);
                };

                this.orderByDesc = function () {
                    return this.predicate.substr(0, 1) == '-';
                };
            }
])