﻿'use strict';
app.service('AccountSearchService', [
 '$http', 'HFCService', 'CalendarService', 'kendotooltipService',
function ($http, HFCService, CalendarService, kendotooltipService) {
     var srv = this;

     srv.IsBusy = false;
     srv.Pagination = {
         page: 1,
         size: 50,
         pageTotal: 1
     };
     srv.PageTotal = srv.Pagination.pageTotal;
     srv.PageSize = srv.Pagination.size;
     srv.PageNumber = srv.Pagination.page;
     srv.TotalRecords = 0;
     srv.ChangePageSize = function () {

     }

     srv.BrandId = HFCService.CurrentBrand;
     var brandid = srv.BrandId;

     srv.PageSummary = '';
     srv.CalendarService = CalendarService;
     var offsetvalue = 0;
     var Permission = {};
     var Accountpermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'Account')
     var Measurementpermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'Measurement')

     Permission.EditAccount = Accountpermission.CanUpdate; //HFCService.GetPermissionTP('Edit Account').CanAccess;
     //Permission.QualifyAccount = HFCService.GetPermissionTP('Qaulify Account').CanAccess;
     Permission.MeasurementAccount = Measurementpermission.CanRead; //HFCService.GetPermissionTP('List Measurements').CanAccess;
     //Permission.AppointmentAccount = HFCService.GetPermissionTP('Add Appointment').CanAccess;
     //Permission.PrintAccount = HFCService.GetPermissionTP('Print Page').CanAccess;

     //srv.TotalRecords = 0;
     srv.SearchTerm = "";
     srv.SearchFilter = "";
     srv.AccountStatusIds = [];
     srv.DateRangeText = "";
     srv.StartDate = null;
     srv.EndDate = null;
     srv.SearchEnums = {};
     srv.Accounts = [];

     

     function DropdownTemplate(dataitem) {
         var Div = '';
         if (dataitem.AccountId) {
             Div += '<ul> <li class="dropdown note1 ad_user"> <div class="dropdown"> <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">  <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> <ul class="dropdown-menu pull-right ">';
             if (Permission.EditAccount)
                 Div += '<li><a href="#!/accountEdit/' + dataitem.AccountId + '">Edit</a></li> ';
             if (Permission.MeasurementAccount && brandid != 3)
                 Div += '<li><a href="javascript:void(0)" ng-click="ShowEditMeasurements(' + dataitem.AccountId + ',' + dataitem.MeasurementCount + ')" >Measurements</a></li> ';
             //if (PermissionSetAppointment==true)
             Div += '<li><a href="#!/Calendar/accountSearch/' + dataitem.AccountId + '" >Appointment</a></li> ';

             //  Div += '<li><a href="javascript:void(0)" ng-click="SetAppointment(\'persongo\', ' + dataitem.AccountId + ')" >Appointment</a></li> ';

             // We are no loner supporting print in Account pages.
             //if (Permission.PrintAccount)
             //   Div += '<li><a ng-click="">Print</a></li> ';
             Div += '</ul> </div></li>  </ul>';
         } else {
             Div = "";
         }
         return Div;
     }

              function maskPhoneNumber(text) {
         if (text != null && text !== undefined) {
             return text.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');
         } else {
             return '';
         }
     }


     function maskedDisplayPhone(phone) {
         var Div;
         if (phone) {
             Div = "<span class='tooltip-wrapper'><div class='tooltip-content'>";
             if (phone && phone != "")
                 Div += maskPhoneNumber(phone) + "</div><span>";
             Div += maskPhoneNumber(phone) + "</span></span>";
         } else {
             Div = "";
         }

         return Div;
     }

    //for address data hiding issue on search
     function addressToString(isSingleLine, model) {
         if (model) {

             var str = "";
             if (model.Address1)
                 str = model.Address1 + (model.Address2 ? (", " + model.Address2) : "") + (isSingleLine ? ", " : "<br/>");

             if (model.City)
                 str += model.City + ", " + (model.State || "") + " " + (model.ZipCode || "");
             else
                 str += (model.ZipCode || "");

             return str;
         }
     };

     srv.GetGoogAccountdressHrefAccounts = function (dest) {
         //return "maps.google.com?saddr= '1927 n glassell st orange ca'&daddr='21273 beechwood way lake forest ca'";
         var srcAddr = '1927 n glassell st orange ca';
         var dest = '21273 beechwood way lake forest ca';
         return '//maps.google.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);

     }

     srv.GetGoogAccountdressHref = function (model) {
         var dest = addressToString(true, model);
         if (model.Address1) {

             var srcAddr;

             if (HFC.currentGeoLocation && HFC.Browser.isMobile()) {
                 srcAddr = HFC.currentGeoLocation.coords.latitude + "," + HFC.currentGeoLocation.coords.longitude;
             } else {
                 srcAddr = encodeURIComponent(HFC.CurrentUserAddress);
             }

             if (navigator.userAgent.toLowerCase().indexOf("iphone") == -1 && navigator.userAgent.toLowerCase().indexOf("ipad") == -1) {
                 return '//maps.google.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
             } else {
                 return '//maps.apple.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
             }
         } else
             return null;
     }

     srv.AddressToString = function (model) { return addressToString(true, model) };

     srv.GetRecord = function () {
          
         var searchHeaderTemplate = $("#accountSearchHeaderTemplate").html();

         var filters = {
             pageIndex: srv.Pagination.page,
             pageSize: srv.Pagination.size
         };
         srv.Accounts = [];
         srv.Accounts_options = {
             cache: false,
             excel: {
                 fileName: "Accounts_Export.xlsx",
                 allPages: true
             },
             dataSource: {
                 transport: {
                     read: {
                         url: '/api/search/0/GetSearchAccounts',
                         dataType: "json",
                         data: filters
                     }

                 },
                 error: function (e) {

                     HFC.DisplayAlert(e.errorThrown);
                 }
             },
             dataBound: function (e) {
                 //console.log("dataBound");
                 if (this.dataSource.view().length == 0) {
                     // The Grid contains No recrods so hide the footer.
                     $('.k-pager-nav').hide();
                     $('.k-pager-numbers').hide();
                     $('.k-pager-sizes').hide();
                 } else {
                     // The Grid contains recrods so show the footer.
                     $('.k-pager-nav').show();
                     $('.k-pager-numbers').show();
                     $('.k-pager-sizes').show();
                 }
                 if (kendotooltipService.columnWidth) {
                     kendotooltipService.restrictTooltip(kendotooltipService.columnWidth);
                 } else if (window.innerWidth < 1280) {
                     kendotooltipService.restrictTooltip(null);
                 }
             },
             resizable: true,
             columns: [{
                 field: "Territory",
                 title: "Territory",
                 width: "120px",
                 template: function (dataItem) {
                     if (dataItem.Territory) {
                         return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.Territory + "</div><span>" + dataItem.Territory + "</span></span>";
                     } else {
                         return "";
                     }
                 },
                 
                 filterable: {
                     multi: true,
                     search: true
                 }
             },
              {
                  field: "CompanyName",
                  title: "Company",
                 
                  width: "120px",
                  template: function (dataItem) {
                      if (dataItem.CompanyName) {
                          return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.CompanyName + "</div><span>" + dataItem.CompanyName + "</span></span>";
                      } else {
                          return "";
                      }
                  },
                  
                  filterable: {
                      //multi: true,
                      search: true
                  }
              },
              {
                  field: "AccountFullName",
                  title: "Name",
                  width: "120px",
                  template: function (dataItem) {
                      if (dataItem.AccountFullName) {
                          return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.AccountFullName + "</div><a href='/#!/Accounts/" + dataItem.AccountId + "' style='color: rgb(61,125,139);'>" + dataItem.AccountFullName + "</a></span>";
                      } else {
                          return "";
                      }
                  }
                  //template: "<a href='\\\\#!/Accounts/#= AccountId #' style='color: rgb(61,125,139);'><span>#= AccountFullName #</span></a> "

              },
              {
                  field: "AccountType", //in db it is AccountTypeId
                  title: "Type",
                  template: function (dataItem) {
                      if (dataItem.AccountType) {
                          return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.AccountType + "</div><span>" + dataItem.AccountType + "</span></span>";
                      } else {
                          return "";
                      }
                  },
                  width: "100px",
                  filterable: {
                      //multi: true,
                      search: true
                  }
              },
              {
                  field: "Status",
                  title: "Status",
                  template: function (dataItem) {
                      if (dataItem.Status) {
                          return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.Status + "</div><span>" + dataItem.Status + "</span></span>";
                      } else {
                          return "";
                      }
                  },
                  width: "100px",
                  filterable: {
                      multi: true,
                      search: true
                  }
              },
              {

                  field: "Address1",
                  title: "Address",
                  width: "250px",
                  template: function (dataItem) {
                      var address = srv.AddressToString(dataItem);
                      var googleaddress = srv.GetGoogAccountdressHref(dataItem);
                      var templateString = "<address class=' inline-relative' hfc-googlemaps='' addr='dataItem'><span class='tooltip-wrapper'><div class='tooltip-content'></div> <a style='color: rgb(61,125,139);' target='_blank' href="+googleaddress+" ng-cloak >"+ address +"</a></span> <div  class='mapCanvas'></div> </address>";
                      //var templateString = "<address class=' inline-relative' hfc-googlemaps='' addr='dataItem'><span class='tooltip-wrapper'><div class='tooltip-content'></div> <a style='color: rgb(61,125,139);' target='_blank' href='' ng-cloak ng-href='{{ GetGoogleAddressHref(dataItem) }}' ng-bind-html='AddressToString(dataItem) | unsafe'></a></span> <div  class='mapCanvas'></div> </address>";
                      return templateString;
                  }
                  //template: " #= (Address1 == null || Address1 == 'undefined' || Address1 == '') ? ' ' : 		Address1 #  #= (Address2 == null || Address2 == 'undefined' || Address2 == '') ? ' ' :   		Address2 #  #= (City 	 == null || City 	 == 'undefined' || City 	== '') ? ' ' :   		City #     #= (State 	 == null || State 	 == 'undefined' || State 	== '') ? ' ' :     		State #    #= (ZipCode  == null || ZipCode  == 'undefined' || ZipCode 	== '') ? ' ' :  	ZipCode # "
                  //template: "<address class='inline-relative' hfc-googlemaps='' addr='dataItem'> <a style='color: rgb(61,125,139);' target='_blank' href='' ng-cloak ng-href='{{ GetGoogleAddressHref(dataItem) }}' ng-bind-html='AddressToString(dataItem) | unsafe'></a> <div class='mapCanvas'></div> </address>",

              },
              {
                  field: "City",
                  title: "City",
                  template: function (dataItem) {
                      if (dataItem.City) {
                          return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.City + "</div><span>" + dataItem.City + "</span></span>";
                      } else {
                          return "";
                      }
                  },
                  width: "100px",
                  filterable: {
                      //multi: true,
                      search: true
                  }
              },
              {
                  field: "State",
                  title: "State",
                  template: function (dataItem) {
                      if (dataItem.State) {
                          return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.State + "</div><span>" + dataItem.State + "</span></span>";
                      } else {
                          return "";
                      }
                  },
                  width: "100px",
                  filterable: {
                      //multi: true,
                      search: true
                  }
              },
              {
                  field: "ZipCode",
                  title: "Postal Code",
                  template: function (dataItem) {
                      if (dataItem.ZipCode) {
                          return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.ZipCode + "</div><span>" + dataItem.ZipCode + "</span></span>";
                      } else {
                          return "";
                      }
                  },
                  width: "100px",
                  filterable: {
                      //multi: true,
                      search: true
                  }
              },
              {
                  field: "DisplayPhone",
                  title: "Phone",
                 
                  width: "170px",
                  filterable: {
                      //multi: true,
                      search: true
                  } 
                  , template: function (dataItem) {
                      return maskedDisplayPhone(dataItem.DisplayPhone);
                  } // "#= formatData(foo) #"
              },
                {

                    field: "PrimaryEmail",
                    title: "Email",
                    template: function (dataItem) {
                        if (dataItem.PrimaryEmail) {
                            return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + dataItem.PrimaryEmail + "</div><span>" + dataItem.PrimaryEmail + "</span></span>";
                        } else {
                            return "";
                        }
                    },
                    width: "100px",
                    filterable: {
                        //multi: true,
                        search: true
                    }

                },
              {
                  field: "CreatedOnUtc",
                  title: "Created On",
                  width: "100px",
                  type: "date",
                  filterable: HFCService.gridfilterableDate("date", "AccountFilterCalendar", offsetvalue),
                  // template: "#= kendo.toString(kendo.parseDate(CreatedOnUtc, 'yyyy-MM-dd'), 'F') #"
                  template: function (dataItem) {
                      if (dataItem.CreatedOnUtc) {
                          return "<span class='tooltip-wrapper'><div class='tooltip-content'>" + HFCService.KendoDateFormat(dataItem.CreatedOnUtc) + "</div><span>" + HFCService.KendoDateFormat(dataItem.CreatedOnUtc) + "</span></span>";
                      } else {
                          return "";
                      }
                  }
              },
              {
                  field: "",
                  title: "",
                  width: "100px",
                  //attributes: { style: "vertical-align: middle;" },
                  template: function (dataitem) {
                      return DropdownTemplate(dataitem);
                  }
              }

             ],
             noRecords: {
                 template: "No records found"
             },
             filterable: {
                 extra: true,
                 operators: {
                     string: {
                         contains: "Contains",
                         eq: "Is equal to",
                         neq: "Is not equal to",
                         isempty: "Empty",
                         isnotempty: "Not empty"


                     },
                     number: {
                         eq: "Equal to",
                         neq: "Not equal to",
                         gte: "Greater than or equal to",
                         lte: "Less than or equal to"
                     },
                     date: {
                         gt: "After (Excludes)",
                         lt: "Before (Includes)"
                     },
                 }
             },
             pageable: {
                 refresh: true,
                 pageSize: 25,
                 resizable: true,
                 pageSizes: [25, 50, 100, 250],
                 buttonCount: 5,
                 change: function (e) {
                     
                     var myDiv = $('.k-grid-content.k-auto-scrollable');
                     myDiv[0].scrollTop = 0;
                 }

             },
             sortable: ({
                 field: "CreatedOnUtc",
                 dir: "desc"
             }),
             toolbar: kendo.template(searchHeaderTemplate),
             columnResize: function (e) {
                 $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 238);
                 getUpdatedColumnList(e);
             }
         };

         var getUpdatedColumnList = function (e) {
             kendotooltipService.columnResizeTooltip(e.newWidth, e.column.headerAttributes.id);
         }

         
     };

      var filters = {
             pageIndex: srv.Pagination.page,
             pageSize: srv.Pagination.size
         };
     srv.ConvertLeadAccount_options = {
         cache: false,
         dataSource: {
             transport: {
                 read: {
                     url: '/api/search/0/GetSearchAccounts',
                     dataType: "json",
                     data: filters
                 }

             },
             error: function (e) {

                 HFC.DisplayAlert(e.errorThrown);
             }
         },
         resizable: true,
         columns: [{
             field: "",
             title: "",
             width: 30,
             template: "<input type='radio' class='option-input radio' id='optSelectAccount' ng-click='SelectedSearchAccount(#= AccountId #)' name='optSelectAccount'  value='SelectAccount'>"
         },
          {
              field: "CompanyName",
              title: "Company",
              filterable: {
                  multi: true,
                  search: true
              }
          },
          {
              field: "AccountFullName",
              title: "Name",

          },
          {
              field: "AccountType", //in db it is AccountTypeId
              title: "Account Type",
              filterable: {
                  multi: true,
                  search: true
              }
          },
          {
              field: "Status",
              title: "Account Status",
              filterable: {
                  multi: true,
                  search: true
              }
          },
          {

              field: "Address1",
              title: "Address",
              template: " #= (Address1 == null || Address1 == 'undefined' || Address1 == '') ? ' ' : 		Address1 #  #= (Address2 == null || Address2 == 'undefined' || Address2 == '') ? ' ' :   		Address2 #  #= (City 	 == null || City 	 == 'undefined' || City 	== '') ? ' ' :   		City #     #= (State 	 == null || State 	 == 'undefined' || State 	== '') ? ' ' :     		State #    #= (ZipCode  == null || ZipCode  == 'undefined' || ZipCode 	== '') ? ' ' :  	ZipCode # "

          },
          {
              field: "City",
              title: "City",
              filterable: {
                  multi: true,
                  search: true
              }
          },
          {
              field: "State",
              title: "State",
              filterable: {
                  multi: true,
                  search: true
              }
          },
          {
              field: "ZipCode",
              title: "Postal Code",
              filterable: {
                  multi: true,
                  search: true
              }
          },
          {
              field: "CreatedOnUtc",
              title: "Created On",
              type: "date",
              filterable: HFCService.gridfilterableDate("date", "AccountCreatedOnUtcFilterCalender", offsetvalue),
              template: function (dataitem) {
                  if (dataitem.CreatedOnUtc)
                      return HFCService.KendoDateFormat(dataitem.CreatedOnUtc);
                  else
                      return "";
              }
                  //"#= kendo.toString(kendo.parseDate(CreatedOnUtc, 'yyyy-MM-dd'), 'F') #"

          }



         ],
         filterable: {
             extra: true,
             operators: {
                 string: {
                     contains: "Contains",
                     eq: "Is equal to",
                     neq: "Is not equal to",
                     isnull: "Null",
                     isempty: "Empty"
                 },
                 date: {
                     gt: "After (Excludes)",
                     lt: "Before (Includes)"
                 },
             }
         },
         noRecords: {
             template: "No records found"
         },
         pageable: {
             refresh: true,
             pageSize: 25,
             pageSizes: [25, 50, 100],
             buttonCount: 5,
             change: function (e) {
                 var myDiv = $('.k-grid-content.k-auto-scrollable');
                 myDiv[0].scrollTop = 0;
                 var myDiv = $('.convert_popup');
                 myDiv[0].scrollTop = 0;
             }
         },
         sortable: ({
             field: "CreatedOnUtc",
             dir: "desc"
         }),
         toolbar: kendo.template($(' <script id="template" type="text/x-kendo-template"><div class="acount_search row" style="margin:5px;"><div class="col-sm-10 col-xs-10 no-padding"><input ng-keyup="AccountgridSearch()" type="search" id="searchBox" placeholder="Search Account" class="k-textbox leadsearch_tbox"></div><div class="col-xs-2 col-sm-2 no-padding"><input type="button" id="btnReset" ng-click="AccountgridSearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters" style="width:100%;"></div></div></script>').html()),

     };

     this.orderBy = function () {
         return this.predicate.substr(1);
     };

     this.orderByDesc = function () {
         return this.predicate.substr(0, 1) == '-';
     };


 }
])