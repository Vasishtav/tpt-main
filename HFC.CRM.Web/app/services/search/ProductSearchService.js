﻿/***************************************************************************\
Module Name:  Product Search Service
Project: HFC
Created on: 02 November 2017 Thursday
Created By:
Copyright:
Description: Product Search based on Product entities
Change History:
Date  By  Description

\***************************************************************************/
'use strict';
angular.module('globalMod')
    .service('ProductSearchService',
    ['$http', 'HFCService', function ($http, HFCService) {

        var srv = this;

        srv.IsBusy = false;
        srv.Pagination = { page: 1, size: 50, pageTotal: 1 };
        srv.PageTotal = srv.Pagination.pageTotal;
        srv.PageSize = srv.Pagination.size;
        srv.PageNumber = srv.Pagination.page;
        srv.TotalRecords = 0;
        srv.ChangePageSize = function () {
            //
        }
        srv.PageSummary = '';
        srv.SearchTerm = "";
        srv.SearchFilter = "";
        srv.DateRangeArray = ["All Date & Time", "Last 7 Days", "Last 30 Days", "This Week", "This Month", "This Year", "Last Week", "Last Month", "Last Year"];
        srv.SearchEnums = {};
        srv.Products = [];

        srv.Get = function () {
            srv.Products = [];
            var ds = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: '/api/ProductTP/0/GetSearchProducts',
                        dataType: "json"
                    }

                },
                schema: {
                    model: {
                        fields: {

                            ProductID: { type: "number" },

                        }
                    }
                },
                error: function (e) {

                    HFC.DisplayAlert(e.errorThrown);
                },
                pageSize: 25
            });
            var gridColumns = [];
            if (window.location.href.toLowerCase().includes('/controlpanel#!/productsearch')) {
                //For Admin product view page
                gridColumns = [
                        {
                            field: "ProductID",
                            title: "ID",
                            width: "100px",
                            //template: "<a href='\\\\#!/productDetail/#= ProductKey #/#= FranchiseId #' style='color: rgb(61,125,139);'><span style='padding: 10px;'>#= ProductID #</span></a> "
                            filterable: {
                                search: true,
                                //dataSource: ds
                            },
                            template: function (dataItem) {
                                return "<span class='Grid_Textalign'>" + dataItem.ProductID + "</span>";
                            },
                        },
                        {
                            field: "ProductKey",
                            title: "Product Key",
                            width: "150px",
                            hidden: true
                        },
                        {
                            field: "FranchiseId",
                            title: "Franchise Id",
                            width: "150px",
                            hidden: true
                        },
                        {
                            field: "VendorId",
                            title: "Vendor Id",
                            width: "100px",
                            hidden: true
                        },
                        {
                            field: "ProductName",
                            title: "Name",
                            width: "150px",
                            filterable: {  search: true, dataSource: ds },
                            template: "<a href='\\\\#!/productDetail/#= ProductKey #' style='color: rgb(61,125,139);'><span>#= ProductName #</span></a> "
                        },
                        {
                            field: "VendorName",
                            title: "Vendor Name",
                            width: "150px",
                            filterable: {  search: true },
                            //filterable: { multi: true, search: true, dataSource: ds },
                            template: function (dataItem) {
                                if (HFCService.CurrentBrand == undefined) {
                                    if (dataItem.VendorName == 'Multiple') return '<span>Multiple</span>'
                                    else return '<span>' + dataItem.VendorName + '</span>'
                                }
                                return '';
                            }
                        },
                        {
                            field: "VendorProductSKU",
                            title: "SKU",
                            width: "100px",
                            filterable: {  search: true },
                            //filterable: { multi: true, search: true, dataSource: ds }
                            template: function (dataItem) {
                                return "<span class='Grid_Textalign'>" + dataItem.VendorProductSKU + "</span>";
                            },
                        },
                        {
                            field: "ProductCategory",
                            title: "Category",
                            width: "120px",
                            filterable: {  search: true }
                            //filterable: { multi: true, search: true, dataSource: ds }
                        },
                        {
                            field: "ProductSubCategory",
                            title: "Sub Category",
                            width: "150px",
                            filterable: {  search: true }
                            //filterable: { multi: true, search: true, dataSource: ds }
                        },
                        {
                            field: "Description",
                            title: "Description",
                            width: "150px",
                            filterable: { search: true }
                            //filterable: { multi: true, search: true, dataSource: ds }
                        },
                        {
                            field: "SalePrice",
                            width: "150px",
                            //format: "{0:c}",
                            //format: "{0:c2}",
                            attributes: { class: "text-right" },
                            title: "Sales Price",
                            filterable: {  search: true },
                            //filterable: { multi: true, search: true, dataSource: ds }
                            template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.SalePrice) + "</span>";
                            },
                        },
                        {
                            field: "ProductStatus",
                            width: "150px",
                            title: "Status",
                            //filterable: { multi: true, search: true, dataSource: ds }
                            filterable: { multi: true, search: true }
                        }
                ];
            }
            else {
                gridColumns = [
                        {
                            field: "ProductID",
                            title: "ID",
                            width: "100px",
                            //template: "<a href='\\\\#!/productDetail/#= ProductKey #/#= FranchiseId #' style='color: rgb(61,125,139);'><span style='padding: 10px;'>#= ProductID #</span></a> "
                            filterable: {
                                //multi: true,
                                search: true,
                                dataSource: ds
                            },
                            template: function (dataItem) {
                                return "<span class='Grid_Textalign'>" + dataItem.ProductID + "</span>";
                            },
                        },
                        {
                            field: "ProductKey",
                            title: "Product Key",
                            width: "150px",
                            hidden: true
                        },
                        {
                            field: "FranchiseId",
                            title: "Franchise Id",
                            width: "150px",
                            hidden: true
                        },
                        {
                            field: "VendorId",
                            title: "Vendor Id",
                            width: "100px",
                            hidden: true
                        },
                        {
                            field: "ProductName",
                            title: "Name",
                            width: "150px",
                            filterable: {
                                //multi: true,
                                search: true,
                                dataSource: ds
                            },
                            template: "<a href='\\\\#!/productDetail/#= ProductKey #/#= FranchiseId #' style='color: rgb(61,125,139);'><span>#= ProductName #</span></a> "
                        },
                        {
                            field: "VendorName",
                            title: "Vendor Name",
                            width: "150px",
                            filterable: {
                                //multi: true,
                                search: true
                            },
                            //filterable: { multi: true, search: true, dataSource: ds },
                            template: function (dataItem) {
                                if (HFCService.CurrentBrand == 2 || HFCService.CurrentBrand == 3) {
                                    if (dataItem.VendorName == 'Multiple') return "<span style='color: rgb(61,125,139);'>Multiple</span>"
                                    else if (dataItem.VendorName == null) return "<span style='color: rgb(61,125,139);'></span>"
                                    else return '<a href="#!/vendorview/' + dataItem.VendorId + '" style="color: rgb(61,125,139);"><span>' + dataItem.VendorName + '</span></a> '
                                } else if (HFCService.CurrentBrand == 1) {
                                    if (dataItem.VendorName == null) return "<span style='color: rgb(61,125,139);'></span>"
                                    else return '<a href="#!/vendorview/' + dataItem.VendorId + '" style="color: rgb(61,125,139);"><span>' + dataItem.VendorName + '</span></a> '
                                }
                                return '';
                            }
                        },
                        {
                            field: "VendorProductSKU",
                            title: "SKU",
                            width: "100px",
                            filterable: {
                                //multi: true,
                                search: true
                            },
                            //filterable: { multi: true, search: true, dataSource: ds }
                            template: function (dataItem) {
                                if (dataItem.VendorProductSKU)
                                    return "<span class='Grid_Textalign'>" + dataItem.VendorProductSKU + "</span>";
                                else
                                    return "";
                            },
                        },
                        {
                            field: "ProductCategory",
                            title: "Category",
                            width: "120px",
                            filterable: {
                                //multi: true,
                                search: true
                            }
                            //filterable: { multi: true, search: true, dataSource: ds }
                        },
                        {
                            field: "ProductSubCategory",
                            title: "Sub Category",
                            width: "150px",
                            filterable: {
                                //multi: true,
                                search: true
                            }
                            //filterable: { multi: true, search: true, dataSource: ds }
                        },
                        {
                            field: "Description",
                            title: "Description",
                            width: "150px",
                            filterable: {
                                //multi: true,
                                search: true
                            }
                            //filterable: { multi: true, search: true, dataSource: ds }
                        },
                        {
                            field: "SalePrice",
                            width: "150px",
                            //format: "{0:c}",
                            //format: "{0:c2}",
                            attributes: { class: "text-right" },
                            title: "Sales Price",
                            filterable: {
                                //multi: true,
                                search: true
                            },
                            //filterable: { multi: true, search: true, dataSource: ds }
                            template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.SalePrice) + "</span>";
                            },
                        },
                        {
                            field: "ProductStatus",
                            width: "150px",
                            title: "Status",
                            //filterable: { multi: true, search: true, dataSource: ds }
                            filterable: { multi: true, search: true }
                        }
                ];
            }

            srv.Products_options = {
                //dataSource: {
                //    data: result.data,
                //},
                cache: false,
                dataSource: ds,
                // https://www.telerik.com/forums/remove-page-if-no-records
                dataBound: function (e) {
                    //console.log("dataBound");
                    if (this.dataSource.view().length == 0) {
                        // The Grid contains No recrods so hide the footer.
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        // The Grid contains recrods so show the footer.
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }
                },
                resizable: true,

                columns: gridColumns,
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            isempty: "Empty",
                            isnotempty: "Not empty"


                        },
                        number: {
                            eq: "Equal to",
                            neq: "Not equal to",
                            gte: "Greater than or equal to",
                            lte: "Less than or equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                noRecords: { template: "No records found" },
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [25, 50, 100],
                    buttonCount: 5,
                    change: function (e) {
                        var myDiv = $('.k-grid-content.k-auto-scrollable');
                        myDiv[0].scrollTop = 0;
                    }
                },
                sortable: ({ field: "CreatedOnUtc", dir: "desc" }),
                toolbar: kendo.template($(' <script id="template" type="text/x-kendo-template"><div class="row vendor_search no-margin" ><div class="leadsearch_topleft row no-margin col-sm-9 col-md-9 col-xs-9 no-padding"><div class="col-xs-2 col-sm-2 no-padding hfc_txtbox"><input ng-keyup="ProductgridSearch()" type="search" id="searchBox" placeholder="Search Product" class="k-textbox leadsearch_tbox"></div><div class="col-xs-2 col-sm-2 measurement_search no-padding"><input type="button" id="btnReset" ng-click="ProductgridSearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div></div> <div class="col-sm-3 col-xs-3"><div class="float-right mt-2"><input class="option-input" name="ProductCheck" type="checkbox" ng-click="GetallPrdtDetails()"><label class="ldet_label">Show Inactive</label></div></div></div></div></script>').html()),
            };
            //});
        };

        srv.SelectDateRange = function (dateRange) {
            srv.DateRangeText = dateRange;
            var dates = HFCService.GetDateRange(srv.DateRangeText);
            srv.StartDate = dates.StartDate;
            srv.EndDate = dates.EndDate;
        }

        this.orderBy = function () {
            return this.predicate.substr(1);
        };

        this.orderByDesc = function () {
            return this.predicate.substr(0, 1) == '-';
        };
    }
    ])