﻿/***************************************************************************\
Module Name:  Product Service .js - AngularJS file
Project: HFC
Created on: 02 November 2017 Thursday
Created By:
Copyright:
Description: Product Service
Change History:
Date  By  Description

\***************************************************************************/
'use strict';
angular.module('globalMod')
    .service('ProductService', [
    '$http', 'CacheService', function ($http, CacheService) {
        var srv = this;

        srv.IsBusy = false;
        srv.Products = [];
      
    }
]);


