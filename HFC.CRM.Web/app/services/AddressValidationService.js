﻿/***************************************************************************\
Module Name:  Address Validation Service
Project: HFC-Tochpoint
Created on: 14 Dec 2017
Created By: 
Copyright:
Description: AddressValidationService
Change History:
Date  By  Description

\***************************************************************************/

// TODO: Note: moved to address service module.

//app.service('AddressValidationService', [
//    '$http', 'HFCService', 
//    function ($http, HFCService) {

//        var srv = this;

//        srv.validateAddress = function (model) {
//            var url = "/api/AddressValidation/0/GetFEDExAddressValidation";
//            //return $http.get(url);
//            return $http.post(url, model);
//        }
//    }
//]);
