﻿(function (window, document, undefined) {
    'use strict';
    angular.module('hfc.Cachemodule', ['hfc.Cachemodule']).
service('CacheService', [
            '$http', 'CacheFactory', function ($http, CacheFactory) {
                var srv = this;
                var cache = null;

                srv.InitCache = function () {
                    if (!CacheFactory.get('lookupCache')) {
                        cache = CacheFactory('lookupCache', {
                            maxAge: 15 * 60 * 1000, // Items added to this cache expire after 15 minutes.
                            cacheFlushInterval: 60 * 60 * 1000, // This cache will clear itself every hour.
                            deleteOnExpire: 'aggressive', // Items will be deleted from this cache right when they expire.
                            storageMode: 'localStorage', // This cache will use `localStorage`.
                            onExpire: function (key, value) {   
                                $http.get('/api/lookup/0/get' + key + 'lookup').then(function (data) {
                                    cache.put(key, data.Added);
                                });
                            }

                        });
                    };
                }

                srv.Load = function (key, fnNext) {

                    if (!cache.get(key)) {
                       // console.log(key + ' not in cache');
                    
                        $http.get('/api/lookup/0/get' + key + 'lookup').then(function (data) {
                            cache.put(key, data.data.Added);
                            fnNext(data.data.Added);
                        });
                    } else {
                      //  console.log(key + ' exists in cache');
                    }
                };

                srv.Invalidate = function (key) {
                    cache.remove(key);
                };

                srv.Get = function (key, fnNext) {
                    
                    key = key.toLowerCase();
                    //console.log("getting from cache: " + key);
                    var val = cache.get(key);
                    if (!val) {
                        srv.Load(key, fnNext);
                    } else {
                        fnNext(val);
                    }
                }

                srv.InitCache();

            }
]);

})(window, document);