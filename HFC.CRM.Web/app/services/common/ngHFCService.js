﻿/**
 * hfc.core.service module  
 * a simple module for handling common core utility services
 * @author Quan Tran
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */
(function (window, document, undefined) {
    'use strict';


    var cacheBustSuffix = HFC.Version;// Date.now();

    angular.module('hfc.core.service', ['hfc.core.service'])
        .constant("cacheBustSuffix", cacheBustSuffix)
        .service('HFCService', ['$http', '$q', function ($http, $q) {
            //
            var srv = this;

            srv.Users = [];
            srv.DisabledUsers = [];
            srv.AllUsers = [];
            srv.CurrentUser = null;
            srv.PathType = null;
            srv.PathId = null;
            srv.CurrentUserPermissions = [];
            srv.FranchiseCountryCode = [];
            srv.FranchiseIsSolaTechEnabled = false;

            // all the available roles in Touchpoint
            srv.RolesTP = [];

            srv.setHeaderTitle = function (title) {
                $('head title').text(title);
            }

            //$.ajax({
            //    //url: '/api/permissions/',
            //    url: '/api/Permission/0/GetUserRolesPermission',
            //    async: false,
            //    success: function (data) {

            //        srv.CurrentUserPermissions = data;
            //        // 
            //    }
            //});

            srv.CurrencyFormat = function (data) {
                if (data == null)
                    data = 0;

                var result = kendo.toString(data, "c")
                return result;
            }
            srv.NumberFormat = function (data, format) {
                if (data == null || data == 0)
                    return "";
                else {
                    if (format == "number")
                        return kendo.toString(data, "n0")
                    else if (format == "decimal")
                        return kendo.toString(data, "n")
                    else if (format == "3decimals")
                        return kendo.toString(Number(data), "n3")
                }

            }
            srv.NewKendoDateFormat = function (data) {
                var result = kendo.toString(new Date(data), 'MM/dd/yyyy');
                return result;
            }
            srv.KendoDateFormat = function (data) {
                var result = kendo.toString(kendo.parseDate(data, 'yyyy-MM-dd'), 'MM/dd/yyyy');
                return result;
            }
            srv.StaticKendoDateFormat = function () {
                var result = 'MM/dd/yyyy'
                return result;
            }
            srv.KendoDateTimeFormat = function (data) {
                var result = kendo.toString(kendo.parseDate(data), 'MM/dd/yyyy hh:mm tt');
                //, 'yyyy-MM-dd hh:mm tt'
                return result;
            }
            srv.StaticKendoDateTimeFormat = function () {
                var result = 'MM/dd/yyyy hh:mm tt'
                return result;
            }
            //for validating date and datetime
            srv.ValidateDate = function (date, type) {
                if (!date)
                    return false;

                if (type == "date") {
                    var dateFormat = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
                    if (!dateFormat.test(date))
                        return false;

                    var Kdate = kendo.parseDate(date);
                    var vals = date.split('/');
                    var year = Number(vals[2]);
                    if (Kdate && year >= 1900 && year <= 2099) {
                        return true;
                    }
                }
                else if (type == "datetime") {
                    var dateTimeFormat = /^\d{1,2}\/\d{1,2}\/\d{4}\s\d{1,2}:\d{1,2}\s\w{2}$/;
                    if (!dateTimeFormat.test(date))
                        return false;

                    var Kdate = kendo.parseDate(date);
                    var vals = date.split(' ');
                    var date = vals[0].split('/');
                    var year = Number(date[2]);
                    var time = vals[1].split(':');
                    var hour = Number(time[0]);
                    var minute = Number(time[1]);
                    if (vals[2].toLowerCase() == "am" || vals[2].toLowerCase() == "pm")
                        var Ampm = true;
                    else
                        var Ampm = false;

                    if (Kdate && year >= 1900 && year <= 2099 && hour <= 12 && minute <= 60 && Ampm) {
                        return true;
                    }
                }
                return false;
            }
            //globalized the grid date filter
            srv.gridfilterableDate = function (type, id, offsetvalue) {
                if (type == "date" && id != "") {
                    return {
                        ui: function (element) {
                            $(element).addClass("kendo-date-picker-date");
                            var idvalue = id + offsetvalue;
                            $(element).attr("id", idvalue);
                            offsetvalue++;
                            $(document).click();
                            element.kendoDatePicker({
                                format: 'MM/dd/yyyy',
                                change: function (e) {
                                    var value = this.value();
                                    // update the selected date in date filter
                                    var divElement = e.sender.dateView.div[0];
                                    var divChildren = $(divElement).children()[0];
                                    var dateInput = $(divChildren).children()[2];
                                    value = kendo.toString(kendo.parseDate(value, 'yyyy-MM-dd'), "dddd, MMMM dd, yyyy");
                                    $(dateInput).text(value);
                                    //value is the selected date in the datepicker
                                },
                                open: function (e) {
                                    var calendar = this.dateView.calendar;
                                    if ($("input[id^='" + id + "']").length > 1) {
                                        var datePickerId = $("input[id^='" + id + "']");
                                        for (var i = 0; i < datePickerId.length; i++) {
                                            var ctrlId = datePickerId[i].id;
                                            if (ctrlId != e.sender.element[0].id)
                                                $("#" + ctrlId).data("kendoDatePicker").close();
                                        }
                                    }
                                    if (!this.element.val()) {
                                        //navigate if input is empty
                                        calendar.navigate(new Date());
                                        var value = new Date();
                                        var divElement = e.sender.dateView.div[0];
                                        var divChildren = $(divElement).children()[0];
                                        var dateInput = $(divChildren).children()[2];
                                        value = kendo.toString(kendo.parseDate(value, 'yyyy-MM-dd'), "dddd, MMMM dd, yyyy");
                                        $(dateInput).text(value);
                                    }
                                }
                            }); // initialize a Kendo UI DatePicker
                            //for disabling entering of date
                            $(element).attr("disabled", "disabled");
                        }
                    }
                }
                else if (type == "date" && id == "") {
                    return {
                        ui: function (element) {
                            element.kendoDatePicker({
                                format: 'MM/dd/yyyy',
                                change: function (e) {
                                    var value = this.value();
                                    // update the selected date in date filter
                                    var divElement = e.sender.dateView.div[0];
                                    var divChildren = $(divElement).children()[0];
                                    var dateInput = $(divChildren).children()[2];
                                    value = kendo.toString(kendo.parseDate(value, 'yyyy-MM-dd'), "dddd, MMMM dd, yyyy");
                                    $(dateInput).text(value);
                                    //value is the selected date in the datepicker
                                },
                                open: function (e) {
                                    var calendar = this.dateView.calendar;
                                    if (!this.element.val()) {
                                        //navigate if input is empty
                                        calendar.navigate(new Date());
                                        var value = new Date();
                                        var divElement = e.sender.dateView.div[0];
                                        var divChildren = $(divElement).children()[0];
                                        var dateInput = $(divChildren).children()[2];
                                        value = kendo.toString(kendo.parseDate(value, 'yyyy-MM-dd'), "dddd, MMMM dd, yyyy");
                                        $(dateInput).text(value);
                                    }
                                }
                            }); // initialize a Kendo UI DateTimePicker
                            //for disabling entering of date
                            $(element).attr("disabled", "disabled");
                        }
                    }
                }
                else if (type == "datetime") {
                    return {
                        ui: function (element) {
                            $(element).addClass("kendo-date-picker-wrapper");
                            var idvalue = id + offsetvalue;
                            $(element).attr("id", idvalue);
                            offsetvalue++;
                            $(document).click();
                            element.kendoDateTimePicker({
                                format: 'MM/dd/yyyy hh:mm tt',
                                change: function (e) {
                                    var value = this.value();
                                    // update the selected date in date filter
                                    var divElement = e.sender.dateView.div[0];
                                    var divChildren = $(divElement).children()[0];
                                    var dateInput = $(divChildren).children()[2];
                                    value = kendo.toString(kendo.parseDate(value, 'yyyy-MM-dd'), "dddd, MMMM dd, yyyy");
                                    $(dateInput).text(value);
                                    //value is the selected date in the datepicker
                                },
                                open: function (e) {
                                    var calendar = this.dateView.calendar;
                                    if ($("input[id^='" + id + "']").length > 1) {
                                        var datePickerId = $("input[id^='" + id + "']");
                                        for (var i = 0; i < datePickerId.length; i++) {
                                            var ctrlId = datePickerId[i].id;
                                            if (ctrlId != e.sender.element[0].id) {
                                                $("#" + ctrlId).data("kendoDateTimePicker").close('date');
                                                $("#" + ctrlId).data("kendoDateTimePicker").close('time');
                                            }
                                        }
                                    }
                                    if (!this.element.val()) {
                                        //navigate if input is empty
                                        calendar.navigate(new Date());
                                        var value = new Date();
                                        var divElement = e.sender.dateView.div[0];
                                        var divChildren = $(divElement).children()[0];
                                        var dateInput = $(divChildren).children()[2];
                                        value = kendo.toString(kendo.parseDate(value, 'yyyy-MM-dd'), "dddd, MMMM dd, yyyy");
                                        $(dateInput).text(value);
                                    }
                                }
                            }); // initialize a Kendo UI DateTimePicker
                            //for disabling entering of date
                            $(element).attr("disabled", "disabled");
                        }
                    }
                }
            }
            srv.getPermissionn = function () {

                if (srv.CurrentUserPermissions.length == 0) {
                    var deferred = $q.defer();

                    $.ajax({
                        url: '/api/Permission/0/GetUserRolesPermission',
                        async: false,
                        success: function (data) {

                            srv.CurrentUserPermissions = data;
                            deferred.resolve(srv.CurrentUserPermissions);
                        }
                    });

                    srv.CurrentUserPermissions = deferred.promise;
                }
                return $q.when(srv.CurrentUserPermissions);

            }




            $.ajax({
                url: '/api/Timezones/0/GetTimezones',
                async: false,
                success: function (data) {
                    srv.CurrentUserTimezone = data;
                }
            });

            $.ajax({
                url: '/api/Users/0/GetRoles',
                async: false,
                success: function (response) {
                    //  
                    srv.RolesTP = response;
                }
            });

            srv.GetUserInfo = function () {
                //turn off async, we want to make sure we have list of users first, as other angular services will reference users list
                $.ajax({
                    url: '/api/users/',
                    async: false,
                    success: function (data) {

                        if (data != "") {
                            srv.Users = data.Users || [];
                            srv.DisabledUsers = data.DisabledUsers || [];
                            srv.AllUsers = srv.Users.concat(srv.DisabledUsers);

                            if (data.CurrentUser) {
                                srv.CurrentUser = srv.GetUser(data.CurrentUser.PersonId);
                                if (srv.CurrentUser == null) {
                                    srv.CurrentUser = data.CurrentUser.user;
                                }
                            }

                            //    
                            srv.FranchiseCountryCode = data.FranchiseCountryCode;
                            srv.CurrentBrand = data.BrandId;
                            srv.FranchiseIsSolaTechEnabled = data.FranchiseIsSolaTechEnabled;
                            srv.FranchiseName = data.FranchiseName;
                            srv.IsQBEnabled = data.IsQBEnabled;
                            srv.FranciseLevelTexting = data.FranciseLevelTexting;
                            srv.FranciseLevelCase = data.FranciseLevelCase;
                            srv.FranciseLevelVendorMng = data.FranciseLevelVendorMng;
                            srv.AdminElectronicSign = data.AdminElectronicSign;
                            srv.FranciseLevelElectronicSign = data.FranciseLevelElectronicSign;
                            srv.EnableEmailSignature = data.EnableEmailSignature;
                            srv.AddEmailSignAllEmails = data.AddEmailSignAllEmails;
                        }
                    }
                });
            }

            srv.GetUserInfo();


            srv.getUsers = function (id) {

                var result = srv.Users.slice();

                var found = $.grep(srv.Users, function (emp, i) {
                    return emp.UserId == id || emp.PersonId == id || emp.Email == id;
                });


                if (found && found.length > 0) {
                    return result;
                }

                found = $.grep(srv.DisabledUsers, function (emp, i) {
                    return emp.UserId == id || emp.PersonId == id || emp.Email == id;
                });

                if (found && found.length > 0) {
                    result.push(found[0]);
                }

                return result;
            }

            //for now we will only check user agent, but we can also add a check for the media break points as well
            var browser = {
                isAndroid: function () {
                    return navigator.userAgent.match(/Android/i);
                },
                isBlackBerry: function () {
                    return navigator.userAgent.match(/BlackBerry/i);
                },
                isIOS: function () {
                    return navigator.userAgent.match(/iPhone|iPad|iPod/i);
                },
                isOpera: function () {
                    return navigator.userAgent.match(/Opera Mini/i);
                },
                isWindows: function () {
                    return navigator.userAgent.match(/IEMobile/i);
                },
                isMobile: function () {
                    return (browser.isAndroid() || browser.isBlackBerry() || browser.isIOS() || browser.isOpera() || browser.isWindows());
                }
            };
            srv.IsMobile = browser.isMobile;

            srv.GetUser = function (personId) {
                var user = $.grep(srv.Users, function (u) {
                    return u.PersonId == personId;
                });

                if (user)
                    return user[0];
                else
                    return null;
            };

            /*srv.SalesUsers = function () {
                return $.grep(srv.Users, function (u) {
                    return u.InSales;
                });
            };*/

            srv.SalesUsers = function (id) {
                var users = srv.Users.slice();

                var result = $.grep(users, function (u) {
                    return u.InSales;
                });

                var found = $.grep(result, function (emp, i) { // if result contains selected element - nothing to do
                    return (emp.UserId == id || emp.PersonId == id || emp.Email == id);
                });

                if (found && found.length > 0) {
                    return result;
                }

                found = $.grep(srv.DisabledUsers, function (emp, i) { // try to find missed selected element in disabled users
                    return (emp.UserId == id || emp.PersonId == id || emp.Email == id) && emp.InSales;
                });

                if (found && found.length > 0) {
                    result.push(found[0]);
                }

                return result;
            };

            srv.InstallerUsers = function (id) {
                var users = srv.Users.slice();

                var result = $.grep(users, function (u) {
                    return u.InInstall;
                });

                var found = $.grep(result, function (emp, i) { // if result contains selected element - nothing to do
                    return (emp.UserId == id || emp.PersonId == id || emp.Email == id);
                });

                if (found && found.length > 0) {
                    return result;
                }

                found = $.grep(srv.DisabledUsers, function (emp, i) { // try to find missed selected element in disabled users
                    return (emp.UserId == id || emp.PersonId == id || emp.Email == id) && emp.InInstall;
                });

                if (found && found.length > 0) {
                    result.push(found[0]);
                }

                return result;
            };

            srv.GetUserLabel = function (user) {
                return '<span class="color-box color-' + (user.ColorId || "none") + '"></span> ' + (user.FullName || "");
            };

            srv.GetUserAvatar = function (personId) {
                var user = srv.GetUser(personId);
                if (user)
                    return user.AvatarSrc;
                else
                    return "/images/avatar/generic.png";
            }

            srv.GetPermission = function (name, subsection) {

                if (srv.CurrentUserPermissions.length == 0) {

                    var permission = $.grep(srv.CurrentUserPermissions, function (p) {
                        if (!subsection) {
                            return p.Name == name;
                        } else {
                            return p.Name == name && p.Subsection == subsection;
                        }
                    });


                    if (permission) {
                        return permission[0];
                    }
                }
                else {

                    srv.getPermissionn()
                        .then(function (posts) {

                            var permission = $.grep(posts, function (p) {
                                if (!subsection) {
                                    return p.Name == name;
                                } else {
                                    return p.Name == name && p.Subsection == subsection;
                                }
                            });


                            if (permission) {
                                return permission[0];
                            }

                        });

                }
            };



            srv.GetPermissionTP = function (name) {


                var Thread = {
                    sleep: function (ms) {
                        var start = Date.now();

                        while (true) {
                            var clock = (Date.now() - start);
                            if (clock >= ms) break;
                        }

                    }
                };

                for (var i = 0; i <= 100 && srv.CurrentUserPermissions.length < 1; i++) {
                    console.log(srv.CurrentUserPermissions.length + " " + i);
                    Thread.sleep(1000);
                }

                if (srv.CurrentUserPermissions.length > 1) {
                    var permission = $.grep(srv.CurrentUserPermissions, function (p) {
                        return p.Name == name;
                    });
                }
                else {
                    return { CanAccess: false };
                }

                return permission[0];

            };

            srv.GetQueryString = function (key) {
                var qstring = location.search.replace('?', '').split('&');
                //var QS = {}
                for (var i = 0; i < qstring.length; i++) {
                    var keyval = qstring[i].split('=');
                    if (keyval[0].toLowerCase() == key.toLowerCase())
                        return decodeURIComponent(keyval[1]);
                    //QS[keyval[0]] = decodeURIComponent(keyval[1]);
                }
                return null;
            }


            srv.GetDateRange = function (range) {
                var endDate = new XDate(),
                    startDate = new XDate();
                switch (range.toLowerCase()) {
                    case "this week":
                        startDate.addDays(-startDate.getDay());
                        endDate = startDate.clone().addDays(6);
                        break;
                    case "this month":
                        startDate.setDate(1);
                        endDate = startDate.clone().addMonths(1).addDays(-1);
                        break;
                    case "this year":
                        startDate.setDate(1).setMonth(0);
                        endDate = startDate.clone().addYears(1).addDays(-1);
                        break;
                    case "last week":
                        startDate.addDays(-startDate.getDay() - 7);
                        endDate = startDate.clone().addDays(6);
                        break;
                    case "last month":
                        startDate.setDate(1).addMonths(-1);
                        endDate = startDate.clone().addMonths(1).addDays(-1);
                        break;
                    case "last year":
                        startDate.setDate(1).setMonth(0).addYears(-1);
                        endDate.setDate(1).setMonth(0).addDays(-1);
                        break;
                    case "last 7 days":
                        startDate.addDays(-7); break;
                    case "last 30 days":
                        startDate.addDays(-30); break;
                    case "1st quarter":
                        startDate.setDate(1).setMonth(0);
                        endDate = startDate.clone().addMonths(3).addDays(-1); break;
                    case "2nd quarter":
                        startDate.setDate(1).setMonth(3);
                        endDate = startDate.clone().addMonths(3).addDays(-1); break;
                    case "3rd quarter":
                        startDate.setDate(1).setMonth(6);
                        endDate = startDate.clone().addMonths(3).addDays(-1); break;
                    case "4th quarter":
                        startDate.setDate(1).setMonth(9);
                        endDate = startDate.clone().addMonths(3).addDays(-1); break;
                    case "1st half":
                        startDate.setDate(1).setMonth(0);
                        endDate = startDate.clone().addMonths(6).addDays(-1); break;
                    case "2nd half":
                        startDate.setDate(1).setMonth(6);
                        endDate = startDate.clone().addMonths(6).addDays(-1); break;
                    default:
                        startDate = null; endDate = null; break;
                }
                return { StartDate: startDate, EndDate: endDate };
            }
            srv.GetUrl = function () {
                //  
                srv.PathId = null;
                srv.PathId2 = null;
                var x = window.location.href;
                if (x.includes('settings/qbintrnl') || (x.includes('Settings/QBO')) && !(x.includes('#!'))) {
                    srv.CurrentURL = window.location.href.split('/');
                    srv.PathType = srv.CurrentURL[3];
                    srv.PathId = srv.CurrentURL[4];
                }
                else {
                    srv.CurrentURL = window.location.href.split('#!');
                    srv.CurrentURL = srv.CurrentURL[1].split('/');
                    srv.PathType = srv.CurrentURL[1];
                    srv.PathId = srv.CurrentURL[2];
                    if (srv.CurrentURL[3] != undefined) {
                        srv.PathId2 = srv.CurrentURL[3];
                    }
                }

            }








        }])

        .service('FranchiseService', [
            '$http', 'HFCService',
            function ($http, HFCService) {

                var srv = this;

                srv.Impersonate = function (userId) {
                    var url = '/api/users/' + userId + '/Impersonate';
                    return $http.post(url);
                }

                srv.ChangeBrand = function (brandId) {
                    var url = '/api/users/' + brandId + '/PostBrandChange';
                    return $http.post(url);
                }

                $http.get('/api/lookup/0/LoginUserDetails').then(function (data) {

                    srv.UserData = data.data;

                });
                srv.SelectedBrand;
            }
        ])

        .directive('charLimit', function () {
            return {
                restrict: 'A',
                link: function ($scope, $element, $attributes) {
                    var limit = $attributes.maxlength;

                    $element.bind('keyup', function (event) {
                        var span = $element.parent().children('.text-muted');
                        span.html(limit - $element.val().length + ' left');
                    });
                }
            };
        });

})(window, document);

