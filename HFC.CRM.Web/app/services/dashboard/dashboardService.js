﻿'use strict';
app.service('dashboardService', [
    '$http', function ($http) {
        var dashboardServiceFactory = {};

        dashboardServiceFactory.getVisibleWidgets = function () {
            return $http.get('api/dashboard/0/getVisibleWidget/').then(function (results) {
                return results;
            });
        }
        return dashboardServiceFactory;
    }
]);