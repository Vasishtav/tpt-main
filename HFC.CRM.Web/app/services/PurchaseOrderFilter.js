﻿'use strict';
app.service('PurchaseOrderFilter', ['$http', 'HFCService', function($http, HFCService) {

        this.page_info = { page: 1, size: 5 };
        this.total_items = 0;

        this.predicate = '-CreatedOn';

        this.orderBy = function() {
            return this.predicate.substr(1);
        };
        this.orderByDesc = function() {
            return this.predicate.substr(0, 1) == '-';
        };

        this.get = function(leadId) {
            var filters = {
                leadId: leadId,
                pageNum: this.page_info.page,
                pageSize: this.page_info.size,
                statusEnum: this.statusEnum,
                orderBy: this.predicate.substr(1),
                orderDir: this.predicate.substr(0, 1) == '-' ? 1 : 0, //0 | + == ascending, 1 | - == descending
                startDate: null,
                endDate: null
            }
            return $http.get('/api/purchaseOrders/', { params: filters });
        }

    }
]);



