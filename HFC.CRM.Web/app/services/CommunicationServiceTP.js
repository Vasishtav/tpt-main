﻿/***************************************************************************\
Module Name:  CommunicationServiceTP Service
Project: HFC-Tochpoint
Created on: 14 Dec 2017
Created By: 
Copyright:
Description: email communication service, for customer journey point
Change History:
Date  By  Description

\***************************************************************************/

app.service('CommunicationService', [
    '$http', 'HFCService', 
    function ($http, HFCService) {

        var srv = this;

        srv.getLeadCommunication = function (leadId,filteroptions) {
            var url = '/api/Communication/' + leadId + '/GetLeadCommunication?Filteroptions=' + filteroptions;
            return $http.get(url);
        }
        
        srv.getAccountCommunication = function (accountId, filteroptions) {
            var url = '/api/Communication/' + accountId + '/GetAccountCommunication?Filteroptions=' + filteroptions;
            return $http.get(url);
        }

        srv.getOpportunityCommunication = function (opportunityId, filteroptions) {
            var url = '/api/Communication/' + opportunityId + '/GetOpportunityCommunication?Filteroptions=' + filteroptions;
            return $http.get(url);
        }

        srv.getCommunicationLogType = function () {
            var url = '/api/Communication/0/GetCommunicationLogType';
            return $http.get(url);
        }

        srv.setControllerMethod = function (value) {
            srv.setControllerValue = value;
            
        }

       
    }
]);
