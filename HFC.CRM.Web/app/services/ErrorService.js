﻿'use strict';
app.service('ErrorService', [
    '$http', 'CacheService', function ($http, CacheService) {
        var srv = this;

        srv.IsBusy = false;

        srv.SelectedSources = [];

        srv.Sources = [];

        srv.Opportunitys = [];

        srv.EditorSelection = [];

        srv.LeadId = false;
        srv.Error = null;
        srv.AddOrEdit = function (modalId, index) {


            $("#" + modalId).modal("show");
        };

        srv.Cancel = function (modalId) {

            $("#" + modalId).modal("hide");
        };

        srv.Samepage = function (modalId) {

            $("#" + modalId).modal("hide");

        };
       

    }
]);


