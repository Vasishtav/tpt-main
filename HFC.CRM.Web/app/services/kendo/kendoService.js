﻿'use strict';

(function () {
    'use strict';
    angular.module('globalMod')
      .service('kendoService', [
    '$http', function ($http) {
        var srv = this;
        //
        srv.kendoToolTipOptions = {
            filter: "th,td",
            position: "top",
            hide: function (e) {
                this.content.parent().css('visibility', 'hidden');
            },
            show: function (e) {
                if (this.content.text().length > 1) {
                    this.content.parent().css('visibility', 'visible');
                } else {
                    this.content.parent().css('visibility', 'hidden');
                }
            },
            content: function (e) {
                var target = e.target.data().title; // element for which the tooltip is shown

                // Invoke Custom tooltip for kendo grid.
                var temp = null;
                if (srv.customTooltip) {
                    temp = srv.customTooltip(e.target.data().field);
                }
                if (temp != null) return temp;

                return target;//$(target).text();
            }
        }

        srv.customTooltip = null;



    }
      ]);
})();