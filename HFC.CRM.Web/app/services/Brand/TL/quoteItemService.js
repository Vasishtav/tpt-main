﻿    app.service('QuoteItemService', ['$http', 'QuoteSaleAdjService', function ($http, QuoteSaleAdjService) {
        var srv = this;

        srv.IsBusy = false;
        srv.ProductCategories = [];
        srv.ProductList = [];

        srv.CheckedCategoryIds = [];
        
        function error(res) {
            srv.IsBusy = false;
            HFC.DisplayAlert(res.statusText);
        }

        srv.AddColor = function (item, color) {
            $http.post('/api/color', { colorName: color, categoryId: item.CategoryId }).then(function (response) {
                var tmpCategory = item.CategoryId;
                item.CategoryId = 0;
                $http.get('/api/lookup/0/category/').success(function (res) {
                    srv.ProductCategories = res;
                    item.CategoryId = tmpCategory;
                    
                    var opt = $.grep(item.Options, function (o) {
                        return o.Name === 'Color';
                    });
                    if (opt.length > 0)
                        opt[0].Value = color;
                });
            });
        }

        srv.GetCategoryColors = function (categoryId) {
            var category = $.grep(srv.ProductCategories, function (c) {
                return c.Id === categoryId;
            });

            if (category.length == 0) {
                return [];
            }

            var result = [];

            for (var i = 0; i < category[0].Category_Color.length; i++) {
                result.push(category[0].Category_Color[i].Type_NamedColor.NamedColor);
            }

            return result;

        };

        srv.GetCategories = function () {
            srv.IsBusy = true;
            $http.get('/api/lookup/0/category/').success(function (res) {
                srv.ProductCategories = res;
                srv.IsBusy = false;
            });            
        };

        srv.GetProductList = function(categoryId) {
                srv.ProductList = [];
                if (categoryId) {
                    $http.get('/api/lookup/' + categoryId + '/products/').success(function(res) {
                        srv.ProductList = res;
                    });
                }
        }

        srv.Delete = function (item, quote, success) {
            if (!srv.IsBusy) {
                srv.IsBusy = true;

                item.QuoteLastUpdated = quote.LastUpdated;

                $http({ method: 'DELETE', url: '/api/quotes/' + item.QuoteId + '/jobitems/', data: item, headers: {'Content-Type': 'application/json'} })
                .then(function (res) {                    
                    if (success)
                        success(res.data);
                    HFC.DisplaySuccess("Quote item deleted");
                    srv.IsBusy = false;
                }, error);
            }
        }

        srv.Save = function (item, quote, success) {
            if (!srv.IsBusy) {
                srv.IsBusy = true;
                
                item.QuoteLastUpdated = quote.LastUpdated;
                //retrieve the product and category names from Ids
                var cat = $.grep(srv.ProductCategories, function (c) {
                    return c.Id == item.CategoryId;
                });
                if (cat && cat.length) //only set if its anything but Other
                    item.CategoryName = cat[0].Category;
                var prodType = $.grep(srv.ProductList, function (p) {
                    return p.ProductTypeId == item.ProductTypeId;
                });
                if (prodType && prodType.length && cat && cat[0].Category != "Other")
                    item.ProductType = prodType[0].ProductType;

                var promise;
                if (item.JobItemId)
                    promise = $http.put('/api/quotes/' + item.QuoteId + '/jobitems/', item);
                else
                    promise = $http.post('/api/quotes/' + item.QuoteId + '/jobitems/', item);

                promise.then(function (res) {
                    if (success)
                        success(res.data);
                    HFC.DisplaySuccess("Quote item saved");
                    srv.IsBusy = false;

                    item.InEditMode = false;
                }, error);
            }
        }

        srv.Edit = function (jobItem) {
            srv.GetProductList(jobItem.CategoryId);
        };


        srv.Add = function (quote) {
            
            quote.JobItems.push({
                QuoteId: quote.QuoteId,
                InEditMode: true,
                Quantity: 1,
                IsTaxable: true,
                SalePrice: 0,
                JobberPrice: 0,
                Subtotal: 0,
                DiscountPercent: 0,
                DiscountAmount: 0,
                MarginPercent: 0,
                CostSubtotal: 0,
                UnitCost: 0,
                Options: [{Name: 'Color', Value: ''}]
            });
        }

        srv.UpdateSortOrder = function (quote) {
            if (!srv.IsBusy) {
                srv.IsBusy = true;

                var jobItems = [];

                for (var i = 0; i < quote.JobItems.length; i++) {

                    quote.JobItems[i].InEditMode = false;
                    jobItems.push(quote.JobItems[i].JobItemId);
                }

                $http({ method: 'POST', url: '/api/quotes/' + quote.QuoteId + '/sortorder/', data: jobItems, headers: { 'Content-Type': 'application/json' } })
                .then(function (res) {
                    srv.IsBusy = false;
                }, error);
            }
        }
              
    }]);



