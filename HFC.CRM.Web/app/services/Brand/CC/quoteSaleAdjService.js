﻿

    app.directive('ngEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if (event.which === 13) {
                    scope.$apply(function() {
                        scope.newColor = element.val();
                        scope.$eval(attrs.ngEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    })
    .service('QuoteSaleAdjService', ['$http', function ($http) {
        var srv = this;

        srv.IsBusy = false;
        srv.SaleAdjTypeEnum = {};
        srv.SaleAdjCategories = [];
        srv.LookupsLoaded = false;

        srv.GetSaleAdjCategories = function (filter) {
            var result = [];
            for (var i = 0; i < srv.SaleAdjCategories.length; i++) {
                if (srv.SaleAdjCategories[i].Label == filter.Group) {
                    result.push(srv.SaleAdjCategories[i]);
                }
            }
            return result;
        };

        srv.GetSaleAdjByType = function (filter) {
            var filterCreteria = '';
            if (filter.Group) {
                filterCreteria = filter.Group;
            }
            else {

                if (filter.TypeEnum == srv.SaleAdjTypeEnum.Discount) {
                    filterCreteria = 'Discount'
                }

                if (filter.TypeEnum == srv.SaleAdjTypeEnum.Surcharge) {
                    filterCreteria = 'Surcharge'
                }
                if (filter.TypeEnum == srv.SaleAdjTypeEnum.Tax) {
                    filterCreteria = 'Tax'
                }
            }
            var result = [];
            for (var i = 0; i < srv.SaleAdjCategories.length; i++) {
                if (srv.SaleAdjCategories[i].Label == filterCreteria) {
                    result.push(srv.SaleAdjCategories[i]);
                }
            }
            return result;
        };

        function error(res) {
            srv.IsBusy = false;
            HFC.DisplayAlert(res.statusText);
        }

        srv.GetLookups = function () {
            if (srv.LookupsLoaded) {
                return;
            }
           
            srv.LookupsLoaded = true;

            $http.get('/api/lookup/0/saleadj/').success(function (res) {
                srv.SaleAdjTypeEnum = res.SaleAdjTypeEnum;
                srv.SaleAdjCategories = res.SaleAdjCategories;
                srv.LookupsLoaded = true;
            });
        }
        srv.GetLookups();
        srv.Delete = function (item, quote, success) {
            if (!srv.IsBusy) {
                srv.IsBusy = true;

                item.QuoteLastUpdated = quote.LastUpdated;

                $http({ method: 'DELETE', url: '/api/quotes/' + item.QuoteId + '/SaleAdjustment/', data: item, headers: { 'Content-Type': 'application/json' } })
                .then(function (res) {
                    if (success)
                        success(res.data);
                    HFC.DisplaySuccess("Quote sale adjustment deleted");
                    srv.IsBusy = false;
                }, error);
            }
        }

        srv.Save = function (item, quote, success) {
            if (!srv.IsBusy) {
                srv.IsBusy = true;

                item.QuoteLastUpdated = quote.LastUpdated;
                                
                var promise;
                if (item.SaleAdjustmentId)
                    promise = $http.put('/api/quotes/' + item.QuoteId + '/saleadjustment/', item);
                else
                    promise = $http.post('/api/quotes/' + item.QuoteId + '/saleadjustment/', item);

                promise.then(function (res) {
                    if (success)
                        success(res.data);
                    HFC.DisplaySuccess("Quote sale adjustment saved");
                    srv.IsBusy = false;
                }, error);
            }
        }

        srv.AddSurcharge = function (quote) {
            quote.SaleAdjustments.push({
                QuoteId: quote.QuoteId,
                TypeEnum: srv.SaleAdjTypeEnum.Surcharge,
                Category: 'Installation',
                InEditMode: true,                
                IsTaxable: true,
                Amount: 0,
                Percent: 0,
                Group: 'Surcharge'
            });
        }

        srv.AddDiscount = function (quote) {
            quote.SaleAdjustments.push({
                QuoteId: quote.QuoteId,
                TypeEnum: srv.SaleAdjTypeEnum.Discount,
                Category: 'Discount',
                InEditMode: true,
                IsTaxable: false,
                Amount: 0,
                Percent: 0,
                Group: 'Discount'
            });
        }

        srv.AddTax = function (quote) {
            quote.SaleAdjustments.push({
                QuoteId: quote.QuoteId,
                TypeEnum: srv.SaleAdjTypeEnum.Tax,
                Category: 'Base Tax',
                InEditMode: true,
                Percent: 0,
                Group: 'Tax'
            });
        }

    }]);



