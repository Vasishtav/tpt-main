﻿   app.service('QuoteService', ['$http', function ($http) {
        var srv = this;

        srv.IsBusy = false;

        function error(res) {
            srv.IsBusy = false;
            HFC.DisplayAlert(res.statusText);
        }

        function removeQuote(arr, item) {
            var index = arr.indexOf(item);
            if (index > 0) {
                arr.splice(index, 1);
            }
            else if (index === 0) {
                arr.shift();
            }
        }

        srv.AddQuote = function (job, quotes) {
            if (!srv.IsBusy && confirm("Do you want to continue?")) {
                srv.IsBusy = true;
                $http.post('/api/quotes/', { JobId: job.JobId }).then(function (res) {
                    quotes.push(res.data);
                    job.JobQuotes.push(res.data);
                    srv.IsBusy = false;
                }, error);
            }
        };

        srv.CloneQuote = function (quote, job) {
            if (!srv.IsBusy) {
                srv.IsBusy = true;
                $http.post('/api/quotes/' + quote.QuoteId + '/clone/').then(function (res) {
                    job.JobQuotes.push(res.data);
                    srv.IsBusy = false;
                }, error);
            }
        };

        srv.DeleteQuote = function (quote, job) {
            if (!srv.IsBusy) {
                srv.IsBusy = true;
                $http.delete('/api/quotes/' + quote.QuoteId).then(function (res) {
                    removeQuote(job.JobQuotes, quote);
                    var prim = $.grep(job.JobQuotes, function (q) {
                        return q.IsPrimary;
                    });
                    if (prim && prim.length) 
                        $("a[href='#quote" + prim[0].JobId + "_" + prim[0].QuoteId+"']").tab("show");
                    
                    srv.IsBusy = false;
                    HFC.DisplaySuccess("Quote deleted");
                }, error);
            }
        };

        srv.SetPrimaryQuote = function (quote, job, quotes) {
            if (!srv.IsBusy) {
                srv.IsBusy = true;
                $http.put('/api/quotes/' + quote.QuoteId + '/primary/').then(function (res) {
                    $.each(quotes, function (i, q) {                        
                        if (q.QuoteId == quote.QuoteId)
                            q.IsPrimary = true;
                        else
                            q.IsPrimary = false;
                    });
                    quote.LastUpdated = res.data.LastUpdated;
                    srv.IsBusy = false;
                }, error);
            }
        };

    }]);



