﻿// DO we really need this??? - murugan

//app.service('AddressService', ['$http', '$sce', '$rootScope', function($http, $sce, $rootScope) {
//        var srv = this;

//        srv.Address = null;
//        srv.Addresses = [];
//        srv.AddressIndex = -1;

//        srv.Countries = [];
//        srv.States = [];
//        srv.IsBusy = false;
//        srv.IsNew = false;
//        srv.MeasurementId = null;
//        srv.ZipCodeChanged = function() {
//            if (srv.Address == null) { return;}
//            if (srv.Address.ZipCode) {
//                srv.IsBusy = true;
//                $http.get('/api/lookup/0/zipcodes?q=' + srv.Address.ZipCode + "&country=" + srv.Address.CountryCode2Digits).then(function(response) {
//                    srv.IsBusy = false;
//                    if (response.data.zipcodes.length > 0) {
//                        srv.Address.City = response.data.zipcodes[0].City;
//                        srv.Address.State = response.data.zipcodes[0].State;
//                    } else {
//                        srv.Address.City = null;
//                        srv.Address.State = null;
//                    }
//                }, function(response) {
//                    HFC.DisplayAlert(response.statusText);
//                    srv.IsBusy = false;
//                });
//            }

//        };

//        srv.CountryChanged = function () {
//            if (srv.Address != null) {
//                srv.Address.City = '';
//                srv.Address.State = '';
//                srv.Address.ZipCode = '';
//                srv.Address.CrossStreet = '';
//            }
//        }

//        srv.GetAddress = function(addrId) {
//            var addr = $.grep(srv.Addresses, function(a) {
//                return a.AddressId == addrId;
//            });
//            if (addr)
//                return addr[0];
//            else
//                return null;
//        };

//        srv.GetLabel = function(addrId, singleline) {
//            var addr = srv.GetAddress(addrId)
//            if (addr) {
//                var str = (addr.Address1 || "") + " " + (addr.Address2 || "");
//                if (!singleline && (addr.City || addr.State))
//                    str += "<br/>";
//                else if (singleline || addr.ZipCode)
//                    str += ", ";
//                str += (addr.City || "");
//                if (addr.State)
//                    str += ", " + addr.State + " ";
//                else if (addr.City)
//                    str += " ";
//                str += (addr.ZipCode || "");

//                if (singleline)
//                    return str;
//                else
//                    return $sce.trustAsHtml(str);
//            }
//        };

//        srv.GetStates = function(countryISO) {
//            return $.grep(srv.States, function(s) {
//                return s.CountryISO2 == countryISO;
//            });
//        };

//        srv.GetZipCodeMask = function(countryISO) {
//            var cc = $.grep(srv.Countries, function(c) {
//                return c.ISOCode2Digits == (countryISO || "US");
//            });
//            if (cc && cc.length)
//                return cc[0].ZipCodeMask;
//            else
//                return "";
//        };

//        srv.GetGoogleHref = function(addrId) {
//            var addr = srv.GetAddress(addrId);
//            if (addr && addr.Address1) {

//                var srcAddr;
//                if (HFC.currentGeoLocation && HFC.Browser.isMobile()) {
//                    srcAddr = HFC.currentGeoLocation.coords.latitude + "," + HFC.currentGeoLocation.coords.longitude;
//                } else {
//                    srcAddr = encodeURIComponent(HFC.CurrentUserAddress);
//                }

//                if (navigator.userAgent.toLowerCase().indexOf("iphone") == -1 && navigator.userAgent.toLowerCase().indexOf("ipad") == -1) {
//                    return '//maps.google.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(srv.GetLabel(addrId, true));
//                } else {
//                    return '//maps.apple.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(srv.GetLabel(addrId, true));
//                }
//            } else
//                return null;
//        };

//        this.getResponse = function () {
//            var response = $http.get("/api/TimeZones/0/GetFranchiseDateTime");
//            return response;
//        };

//        srv.Get = function(leadIds, includeStates) {

//            if (!srv.IsBusy) {
//                srv.IsBusy = true;

//                $http({ method: 'GET', url: '/api/address/', params: { leadIds: leadIds, includeStates: includeStates } })
//                    .then(function(response) {
//                        srv.States = response.data.States || [];
//                        srv.Countries = response.data.Countries || [];
//                        srv.Addresses = response.data.Addresses || [];
//                        srv.FranchiseAddress = response.data.FranchiseAddress;

//                        srv.IsBusy = false;
//                    }, function(response) {
//                        srv.IsBusy = false;
//                    });
//            }
//        }

//        srv.Set = function(states, countries, addresses, franchiseAddress) {
//            srv.States = states || [];
//            srv.Countries = countries || [];
//            srv.Addresses = addresses || [];
//            srv.FranchiseAddress = franchiseAddress;

//        }

//        srv.Add = function (modalId, country) {
//            //todo: we need to get country from current franchise
//            country = 'US';
//            srv.Address = {
//                IsResidential: true,
//                CountryCode2Digits: country || 'US'
//            };
//            srv.AddressIndex = -1;
//            srv.IsNew = true;
//            $("#" + modalId).modal("show");
//        }

//        srv.Delete = function(leadId, addressId) {
//            if (!srv.IsBusy && leadId && confirm("Do you wish to continue?")) {
//                srv.IsBusy = true;

//                $http({ method: 'DELETE', url: '/api/leads/' + leadId + '/address/', headers: { 'Content-Type': 'application/json' }, data: { AddressId: addressId } })
//                    .then(function(response) {
//                        var address = srv.GetAddress(addressId);
//                        var label = srv.GetLabel(addressId, true);
//                        remove(address);
//                        HFC.DisplaySuccess("Address: " + label + " deleted");
//                        srv.IsBusy = false;
//                    }, function(response) {
//                        HFC.DisplayAlert(response.statusText);
//                        srv.IsBusy = false;
//                    });
//            }
//        };
//        srv.zipCodeIsNotValid = false;
//        function chkNumeric(inputtxt) {
//            if (isNaN(inputtxt)) {
//                return false;
//            }
//            return true;
//        }
//        srv.Save = function(leadId, modalId) {
//            srv.Addrsubmit = true;
//                if (!srv.IsBusy && srv.Address && leadId) {
//                srv.IsBusy = true;
//                var promise = null;

//                if (srv.Address.AddressType == undefined)
//                {
//                    HFC.DisplayAlert("Address Type Required");
//                    srv.zipCodeIsNotValid = true;
//                    srv.IsBusy = false;
//                    return;
//                }
//                if (srv.Address.Address1 == undefined) {
//                    HFC.DisplayAlert("Address1  Required");
//                    srv.zipCodeIsNotValid = true;
//                    srv.IsBusy = false;
//                    return;
//                }
//                if (srv.Address.ZipCode == '' || srv.Address.ZipCode == undefined) {
//                    HFC.DisplayAlert("Additional Address or Zip Code Required");
//                    srv.zipCodeIsNotValid = true;
//                    srv.IsBusy = false;
//                    return;
//                } else {
                    
//                    if (chkNumeric(srv.Address.ZipCode)) {
//                        srv.zipCodeIsNotValid = false
//                    }
//                    else {
//                        HFC.DisplayAlert("Zipcode must be numeric");
//                        return;
//                    }
//                }

//                if (srv.Address.AddressId)
//                    promise = $http.put('/api/leads/' + leadId + '/address/', srv.Address);
//                else
//                    promise = $http.post('/api/leads/' + leadId + '/address/', srv.Address);

//                promise.then(function(res) {
//                    if (!srv.Address.AddressId && res.data.AddressId != srv.Address.AddressId)
//                        srv.Address.AddressId = res.data.AddressId;

//                    if (srv.AddressIndex >= 0)
//                        srv.Addresses[srv.AddressIndex] = srv.Address;
//                    else
//                        srv.Addresses.push(srv.Address);

//                    $rootScope.$broadcast('refresh.territory', { zipCode: srv.Address.ZipCode, jobId: null });
//                    srv.Address = null;
//                    srv.AddressIndex = -1;

//                    HFC.DisplaySuccess("Address saved");
//                    srv.IsBusy = false;

//                    $("#" + modalId).modal("hide");
//                }, function(res) {
//                    HFC.DisplayAlert(res.statusText);
//                    srv.IsBusy = false;
//                });
//            }
//            else if(leadId=="")
//            {
//                srv.IsBusy = true;
//                var promise = null;

//                if (srv.Address.AddressType == undefined || srv.Address.AddressType == null || srv.Address.AddressType=="") {
//                    HFC.DisplayAlert("Address Type Required");
//                    srv.zipCodeIsNotValid = true;
//                    srv.IsBusy = false;
//                    return;
//                }
//                if (srv.Address.Address1 == undefined || srv.Address.Address1 ==null || srv.Address.Address1 =="") {
//                    HFC.DisplayAlert("Address1  Required");
//                    srv.zipCodeIsNotValid = true;
//                    srv.IsBusy = false;
//                    return;
//                }

//                if (srv.Address.ZipCode == '' || srv.Address.ZipCode == undefined) {
//                    HFC.DisplayAlert("Additional Address or Zip Code Required");
//                    srv.zipCodeIsNotValid = true;
//                    srv.IsBusy = false;
//                    return;
//                } else {

//                    //if (chkNumeric(srv.Address.ZipCode)) {
//                    //    srv.zipCodeIsNotValid = false
//                    //}
//                    //else {
//                    //    HFC.DisplayAlert("Additional Address  Zip Code must be numbers");
//                    //    return;
//                    //}
//                }

//                if (srv.IsNew == true)
//                {
//                    srv.Addresses.push({
//                        IsResidential: srv.Address.IsResidential,
//                        IsCommercial: srv.Address.IsCommercial,
//                        AddressType: srv.Address.AddressType,
//                        AttentionText: srv.Address.AttentionText,
//                        Address1: srv.Address.Address1,
//                        Address2: srv.Address.Address2,
//                        City: srv.Address.City,
//                        Country: srv.Address.Country,
//                        State: srv.Address.State,
//                        ZipCode: srv.Address.ZipCode,
//                        CountryCode2Digits: srv.Address.CountryCode2Digits,
//                        CrossStreet: srv.Address.CrossStreet,
//                        Location: srv.Address.Location
//                    })
//                }
//                else
//                {
//                    srv.Addresses[srv.AddressIndex] = srv.Address;
//                }
//                srv.Address = null;
//                srv.IsNew = false;
                
//                $("#" + modalId).modal("hide");

//            }
//        };

//        srv.Edit = function(addressId, modalId, index) {
//            //var address = srv.GetAddress(addressId);

//            //if (addressId != null) {
//            //    srv.Address = angular.copy(address);
//            //    srv.AddressIndex = index;
//            //}
//            //To Edit Additional Address which starts with index value 1
//            // because index 0 is for Primary Address
//            srv.Address = srv.Addresses[index];
//            srv.AddressIndex = index;
//            srv.IsNew = false;
//            $("#" + modalId).modal("show");
//        };

//        srv.Cancel = function(modalId) {
//            srv.Address = null;
//            srv.AddressIndex = -1;
//            srv.Addrsubmit = false;
//            $("#" + modalId).modal("hide");
//        }

//        srv.SaveAddressChange = function(jobId, newAddressId, isBilling, success) {
//            if (srv.IsBusy)
//                return;

//            srv.IsBusy = true;
//            var data = { BillingAddressId: newAddressId };
//            if (!isBilling)
//                data = { InstallAddressId: newAddressId };
//            $http.put('/api/jobs/' + jobId + '/address', data).then(function() {
//                srv.IsBusy = false;
//                if (success) {
//                    success();
//                }
//            }, function(response) {
//                HFC.DisplayAlert(response.statusText);
//                srv.IsBusy = false;
//            });
//        }

//        function remove(item) {
//            var index = srv.Addresses.indexOf(item);
//            if (index > 0) {
//                srv.Addresses.splice(index, 1);
//            } else if (index === 0) {
//                srv.Addresses.shift();
//            }
//        }
//    }
//]);
