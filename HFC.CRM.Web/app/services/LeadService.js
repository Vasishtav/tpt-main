﻿
(function (window, document, undefined) {
    'use strict';
    angular.module('hfc.leadModule', [])
    .service('LeadService', ['$http', function ($http) {
        var srv = this;

        srv.IsBusy = false;
        srv.Leads = [];
        srv.LeadStatuses = [];
        srv.PurchaseOrdersCount = 0;

        srv.GetStatus = function (statId) {
            var stat = $.grep(srv.LeadStatuses, function (stat) {
                return stat.Id == statId;
            });
            if (stat)
                return stat[0];
            else
                return null;
        };

        srv.GetLeadStatuses = function (lead) {

            var arr = [];
            var parents = $.grep(srv.LeadStatuses, function (stat) {
                return stat.ParentId == null;
            });
            angular.forEach(parents, function (stat) {
                arr.push(stat);
                var child = $.grep(srv.LeadStatuses, function (child) {
                    return child.ParentId == stat.Id;
                });
                if (child)
                    arr = arr.concat(child);
            });


            var selectedStatus = $.grep(arr, function (e) {
                return e.Id == lead.LeadStatusId;
            });

            if (!selectedStatus || selectedStatus == 0) {
                var missedStatus = $.grep(srv.LeadStatuses, function (e) {
                    return e.Id == lead.LeadStatusId;
                });

                if (missedStatus && missedStatus.length > 0) {
                    arr = arr.concat(missedStatus[0]);
                }
            }

            return arr;
        };

        srv.GetStatusLabel = function (stat) {
            var parent = null;
            if (stat.ParentId)
                parent = srv.GetStatus(stat.ParentId);
            if (parent)
                return '<span class="left-margin">' + stat.Name + ' <span class="text-muted">' + parent.Name + '</span></span>';
            else
                return stat.Name;
        };

        //utility to return a class name linked to a status id
        srv.LeadStatusCss = function (statusId) {
            var css = null;
            if (statusId && srv.LeadStatuses) {
                angular.forEach(srv.LeadStatuses, function (stat) {
                    if (stat.Id == statusId) {
                        if (stat.ParentId)
                            css = srv.LeadStatusCss(stat.ParentId);
                        else
                            css = stat.ClassName;
                        return;
                    }
                });
            }
            return css || "btn-default";
        };

        //gets a single lead and store it in this service as well as any pass in reference
        srv.Get = function (leadId, success, onError) {
            if (!isNaN(leadId) && leadId > 0) {
                $http.get('/api/leads/' + leadId).then(function (response) {

                    srv.LeadStatuses = response.data.LeadStatuses || [];
                    var leadServiceData = response.data;
                    //var primaryNotes = response.data.LeadPrimaryNotes;

                    srv.PurchaseOrdersCount = response.data.PurchaseOrdersCount;

                    //if (primaryNotes) {
                    //    leadServiceData.Lead.PrimaryNotes = primaryNotes.Message;
                    //}

                    //change dates to XDate for easier manipulation
                    //leadServiceData.Lead.CreatedOnUtc = new XDate(leadServiceData.Lead.CreatedOnUtc, true);
                    //leadServiceData.Lead.LastUpdatedOnUtc = new XDate(leadServiceData.Lead.LastUpdatedOnUtc, true);
                    srv.Leads.push(leadServiceData);

                    if (success)
                        success(leadServiceData);
                }, function (response) {
                    if (onError) {
                        onError(response);
                    } else {
                        HFC.DisplayAlert(response.statusText);
                    }
                });
            }
        };





        // Load list of dispositions from the db, which is not changing very frequently



        srv.SaveStatus = function (leadId, statId) {
            if (!srv.IsBusy) {
                srv.IsBusy = true;
                $http.put('/api/leads/' + leadId + '/status', { Id: statId }).then(function () {
                    HFC.DisplaySuccess("Lead status successfully changed");
                    srv.IsBusy = false;
                }, function (response) {
                    HFC.DisplayAlert(response.statusText);
                    srv.IsBusy = false;
                });
            }
        }

    }]);

})(window, document);
 