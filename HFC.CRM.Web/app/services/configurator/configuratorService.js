﻿'use strict';
angular.module('commonConfiguratorService', []).service('configuratorService', ['$http', function ($http) {
    var srv = this;

    srv.IsBusy = false;

    function httpGet(theUrl) {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open("GET", theUrl, false); // false for synchronous request
        xmlHttp.send(null);
        return xmlHttp.responseText;
    }

    srv.GetDataSourceAndPromptAnswers = function (productId, modelId, success) {
        //GetDataSourceAndPromptAnswers(string productid,string modelId)
        //if (!isNaN(accountId) && accountId > 0) {
        $http.get("/api/PIC/0/GetDataSourceAndPromptAnswers?productid=" + productId + "&modelId=" + modelId).then(function (response) {
            if (success) {
                var respo = httpGet(response.data.dsurl);
                response.data.dataSource = respo;
                success(response);
            }
        }, function (response) {
            HFC.DisplayAlert(response.statusText);
        });
    };
        //////////Datasource By franchise Code
        srv.GetDataSourceAndPromptAnswersByFranchiseId = function (productId, modelId, FranchiseCode,success) {
            $http.get("/api/PIC/0/GetDataSourceAndPromptAnswersByFranchiseId?productid=" + productId + "&modelId=" + modelId+"&franchiseCode="+FranchiseCode).then(function (response) {
                if (success) {
                    var respo = httpGet(response.data.dsurl);
                    response.data.dataSource = respo;
                    success(response);
                }
            }, function (response) {
                HFC.DisplayAlert(response.statusText);
            });
        };

        srv.GetDatsource = function (productId) {
            this.findMiqaat = function (productId) {
                var dat = productId;
                console.log(dat);
                var url = "/api/PIC/0/GetDataSource?productid=" + productId;
                return $http({
                    url: url,
                    method: 'GET',
                });
            }

            //$http.get("/api/PIC/0/GetDataSource?productid=" + productId).then(function (response) {
            //    return response.Data;
            //});
            srv.IsBusy = false;
        }
        srv.GetProductPromptAnswers = function (productId, modelId) {
            $http.get("/api/PIC/0/PICProductPromptAnswers?productId=" + productId + "&modelId=" + modelId).then(function (response) {
                return response.Data;
            });
            srv.IsBusy = false;
        }
    }])