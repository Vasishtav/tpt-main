﻿'use strict';
app.service('myVendorconfiguratorService', ['$http', function ($http) {
    var srv = this;

    srv.IsBusy = false;


    srv.GetPICToken = function () {
        return $http.get('/api/lookup/0/GetPICToken');
    };

    //srv.GetProductProductGroup = function (token) {

    //    return $http.get('https://hfc.picbusiness.com/ProductGroup?access_token=' + token);
    //};


    //srv.GetProductProductGroup = function () {
    //    return $http.get('/api/leadsources/0/GetPICProductGroup');
    //}


    //function getMetaData(selection) {
    //    $http.get('http://localhost:61475/api/SxTableInfo/', { params: { tablename: selection } }
    //    }

    //return { getMetaData: getMetaData }

    srv.GetDatsource = function (productId) {

        $http.get('/api/PIC/0/PICDatasourceByProduct/' + productId).then(function (response) {
            return response.Data;
        });
        srv.IsBusy = false;
    }
    srv.GetProductPromptAnswers = function (productId) {
        $http.get('/api/PIC/0/PICDatasourceByProduct?productId=' + productId).then(function (response) {
            return response.Data;
        });
        srv.IsBusy = false;
    }
}])