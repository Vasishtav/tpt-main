﻿
(function (window, document, undefined) {
    'use strict';
    angular.module('hfc.fileUploadModule', []) // ['hfc.core.service', 'hfc.accountModule', 'hfc.leadModule', 'hfc.leadaccountModule'])
        .directive('fileUpload', ['$http', 'FileUploadService', function ($http, FileUploadService
        ) {
            return {
                templateUrl: 'app/views/CaseManagement/attachment.html',
                link: function (scope, element) {
                    scope.filesizee = '';
                    scope.ftype = '';
                    scope.FileUploadService = FileUploadService;

                    element.bind('change', function () {
                        scope.$apply(function () {
                            scope.files = document.getElementById('uploadFileInput').files ;

                            scope.data1 = [];
                            if (scope.files) {
                                for (var ii = 0; ii < scope.files.length ; ii++) {
                                    if (scope.files[ii].size !== 0) {
                                        var ext = scope.files[ii].name.split('.').pop();
                                        var size = bytesToSize(scope.files[ii].size);
                                        var todayy = new Date();
                                        //var today = todayy.getUTCDate()
                                        var dd = todayy.getUTCDate();
                                        var mm = todayy.getUTCMonth() + 1; //January is 0!
                                        var yyyy = todayy.getUTCFullYear();

                                        if (dd < 10) {
                                            dd = '0' + dd
                                        }

                                        if (mm < 10) {
                                            mm = '0' + mm
                                        }

                                       var today = mm + '/' + dd + '/' + yyyy;

                                        var clr = '';

                                        if (ext.toLowerCase() == 'pdf') { clr = 'red'; scope.ftype = 'far fa-file-pdf'; }
                                        else if (ext.toLowerCase() == 'doc' || ext.toLowerCase() == 'docx' || ext.toLowerCase() == 'txt') { clr = 'blue'; scope.ftype = 'far fa-file-alt'; }
                                        else if (ext.toLowerCase() == 'xls' || ext.toLowerCase() == 'xlsx') { clr = 'green'; scope.ftype = 'far fa-file-excel'; }
                                        else if (ext.toLowerCase() == 'png' || ext.toLowerCase() == 'jpg' || ext.toLowerCase() == 'jpeg' || ext.toLowerCase() == 'gif') { clr = 'violet'; scope.ftype = 'far fa-image'; }
                                        else { clr = ''; scope.ftype = 'far fa-file-alt'; }

                                        scope.filesizee = today + ' | ' + size + ' | ' + ext;
                                        scope.data1.push({ streamId: '', fileIconType: scope.ftype, fileSize: scope.filesizee, fileName: scope.files[ii].name, color: clr });
                                    }
                                }
                                FileUploadService.UploadFile(scope.files, scope.data1);
                            }
                            else {
                                if (scope.files !== undefined)
                                alert("File is not valid or corrupted");
                            }
                        });
                    });

                    function bytesToSize(bytes) {
                        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
                        if (bytes == 0) return '0 Byte';
                        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
                        return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
                    };
                }
            };
        }])
       .service('FileUploadService', function ($http, $q) {
           var srv = this;
           srv.CaseId = 0;
           srv.Module = '';
           srv.VendorId = 0;
           srv.showattachment = false;
           srv.Showtimezone = false;

           srv.css1 = "";
           srv.css2 = "";
           srv.css3 = "";
           srv.css4 = "";
           srv.disableLineDropDown = false;
          
           srv.SetCaseId = function (value, module) {
               srv.CaseId = value;
               srv.showattachment = true;
               srv.Module = module;
               this.getlineIdList();
               this.getfranchiseCaseDocList();

               //$http.get('/api/CaseManageAddEdit/' + this.CaseId + '/getFranchiseTimeZone?CaseId=' + srv.CaseId).then(function (data) {
               //    srv.FranchiseTimeZone = data.data;
               //}, function (error) {

               //});

           }

           srv.ShowFranchisetimezone = function (flag) {
               srv.Showtimezone = flag;           

           }

           srv.clearCaseId = function () {
               srv.CaseId = 0;
               srv.Module = '';
               srv.AttachmentList = [];
               srv.lineIdList = [];
               srv.showattachment = false;
           }
           // this.SetCaseId(1000);
           srv.AttachmentList = [];
           srv.getlineIdList = function () {
               $http.get('/api/CaseManageAddEdit/' + this.CaseId + '/getlineIdList?Module=' + srv.Module).then(function (data) {
                   srv.lineIdList = data.data;

                   // srv.lineIdList.splice(0, 0, { lineId: srv.CaseId, lineText: srv.CaseId });
                   
                   angular.forEach(srv.lineIdList, function (value, key) {
                       // value.lineId = value.lineId.toString();
                       value.lineId = Number(value.lineId);
                   });
               }, function (error) {

               });


           }

           srv.getfranchiseCaseDocList = function () {
               $http.get('/api/CaseManageAddEdit/' + this.CaseId + '/getfranchiseCaseDocList?Module=' + srv.Module +'&flag='+srv.Showtimezone).then(function (data) {
                   srv.AttachmentList = data.data;
               }, function (error) {

               });
           }


           // this.getfranchiseCaseList();

           srv.UploadFile = function (file, description) {

               var formData = new FormData();
               for (var ee = 0; ee < file.length; ee++)
               {
                   formData.append("file", file[ee]);
               }
              // formData.append("file", file);
               formData.append("description", angular.toJson(description));
               
               var defer = $q.defer();

             //  $http.post("api/File/" + srv.CaseId + "/UploadAttachment/?VendorCaseId=" + 0 + "&Module=" + srv.Module + "&fileIconType=" + description.fileIconType + "&fileSize=" + description.fileSize + "&fileName=" + description.fileName + "&color=" + description.color, formData,
               //{
               $http.post("api/File/" + srv.CaseId + "/UploadAttachment/?VendorCaseId=" + 0 + "&Module=" + srv.Module+ "&flag="+  srv.Showtimezone,formData,
            {
                 withCredentials: true,
                 headers: { 'Content-Type': undefined },
                 transformRequest: angular.identity
             })
         .then(function (data) {
             angular.element("input[type='file']").val(null);
             if (data.data == "Maximum request length exceeded.")
             {
                // alert("Maximum file size exceeded")
             }
             else if(data.data == "Error Occurred")
             {

             }
             else if (data.data == "File Not found")
             {

             }
             else
             {
             for (var r = 0; r < data.data.length ; r++)
             {
                 srv.AttachmentList.push(data.data[r]);
             }
             }            
         });
           }

           srv.removeFranchiseCaseDetail = function (Id, index) {
               $http.post('/api/CaseManageAddEdit/' + Id + '/removeFranchiseCaseDetail').then(function (data) {
                   srv.AttachmentList.splice(index, 1);
               }, function (error) {

               });
           }

           srv.removeFranchiseCaseDetails = function (Id, CaseId) {
               $http.post('/api/CaseManageAddEdit/' + Id + '/removeFranchiseCaseDetails?CaseId=' + CaseId).then(function (data) {
                   srv.getlineIdList();
                   srv.getfranchiseCaseDocList();
               }, function (error) {

               });
           }

           srv.lineChanged = function (Id, VendorId) {
               if (VendorId)
                   $http.post('/api/CaseManageAddEdit/' + Id + '/changeLineId?VendorId=' + VendorId).then(function (data) {
                   }, function (error) {

                   });
           };
       });

})(window, document);
