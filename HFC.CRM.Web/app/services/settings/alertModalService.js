﻿app.factory('alertModalService', [function () {
    var alertModelObject = {};

    alertModelObject.invalidAcceptedFlag = true;

    // to set the scope function to initiate the directive flow
    alertModelObject.setScopeFunction = function (func) {
        alertModelObject.scopeFunction = func;
    }

    // to initiate the directive flow from controllers
    alertModelObject.setPopUpDetails = function (flag, title, body, futureFunc, data, isBusy, recurrenceButtonList, emailFlag, deleteFlag) {
        if (flag) {
            alertModelObject.controlFunction = futureFunc;
            alertModelObject.param = data;
            alertModelObject.scopeFunction(flag, title, body, isBusy, recurrenceButtonList, emailFlag, deleteFlag);
        }
    }

    return alertModelObject;
}]);