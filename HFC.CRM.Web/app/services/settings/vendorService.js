﻿'use strict';

(function () {
    'use strict';
    angular.module('globalMod').service('vendorService', [
            '$http', 'HFCService', function ($http, HFCService) {
                var srv = this;

                srv.IsBusy = false;
                srv.Pagination = { page: 1, size: 50, pageTotal: 1 };
                srv.PageTotal = srv.Pagination.pageTotal;
                srv.PageSize = srv.Pagination.size;
                srv.PageNumber = srv.Pagination.page;
                srv.TotalRecords = 0;
                srv.ChangePageSize = function () {

                }


                srv.PageSummary = '';


                function FormatContactNumber(text) {
                    return text.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');
                }
                
                function formatNumber(dataItem) {
                    var Div = '<address>';
                    if (dataItem.AccountRepPhone && dataItem.AccountRepPhone != "")
                        Div += FormatContactNumber(dataItem.AccountRepPhone) + '</div>';
                    Div += '<text></text>' + '</div>';
                    Div += '</address>';


                    return Div;
                }

                //srv.TotalRecords = 0;
                srv.SearchTerm = "";
                srv.SearchFilter = "";
                srv.AccountStatusIds = [];
                srv.DateRangeText = "";
                srv.StartDate = null;
                srv.EndDate = null;
                //srv.predicate = '-createdonutc';
                srv.DateRangeArray = ["All Date & Time", "Last 7 Days", "Last 30 Days", "This Week", "This Month", "This Year", "Last Week", "Last Month", "Last Year"];
                srv.SearchEnums = {};
                srv.vendor = [];


                srv.Get = function () {
                    var filters = {
                        pageIndex: srv.Pagination.page,
                        pageSize: srv.Pagination.size
                    };
                    srv.vendor = [];
                    var ds = new kendo.data.DataSource({
                        transport: {
                            read: {
                                url: '/api/Vendor/0/GetVendors',
                               // url: '/api/Vendor/0/VendorOption',
                                dataType: "json"
                            }

                        },
                        schema: {
                            model: {
                                fields: {

                                    VendorId: { type: "number" },

                                }
                            }
                        },
                        error: function (e) {

                            HFC.DisplayAlert(e.errorThrown);
                        },
                        pageSize: 25
                    })
                    //$http.get('/api/Vendor/0/GetVendors').then(
                    //    function (result) {
                    srv.Vendors_details = {
                        //dataSource: {
                        //    data: result.data,

                        //},
                        cache: false,
                        dataSource: ds,
                        
                        // https://www.telerik.com/forums/remove-page-if-no-records
                        dataBound: function (e) {
                            //console.log("dataBound");
                            if (this.dataSource.view().length == 0) {
                                // The Grid contains No recrods so hide the footer.
                                $('.k-pager-nav').hide();
                                $('.k-pager-numbers').hide();
                                $('.k-pager-sizes').hide();
                            } else {
                                // The Grid contains recrods so show the footer.
                                $('.k-pager-nav').show();
                                $('.k-pager-numbers').show();
                                $('.k-pager-sizes').show();
                            }
                        },
                      

                        resizable: true,
                        columns: [
                                 {
                                     field: "VendorId",
                                     title: "ID",
                                     type: "number",
                                     width: "100px",
                                     //filterable: {  search: true, dataSource: ds },
                                     filterable: { search: true },
                                     template: function (dataItem) {
                                         return "<span class='Grid_Textalign'>" + dataItem.VendorId + "</span>";
                                     },
                                 },
                            {
                                field: "Name",
                                title: "Name",
                                width: "150px",
                                template: "<a href='\\\\#!/vendorview/#= VendorId #' style='color: rgb(61,125,139);'><span >#= Name #</span></a>",
                                //filterable: { multi: true, search: true, dataSource: ds }
                                filterable: { search: true }
                            },
                            {
                                field: "VendorType",
                                title: "Type",
                                width: "150px",
                                //template: "\#= VendorType ? 'Alliance Vendors' : 'Non-Alliance Vendors'\#"
                                //template: "# switch (VendorType) { case 1: #Alliance Vendor# break; ## case 0: #Non-Vendor Alliance# break; ## case 3: #My Vendor# break; ##}#"
                                //filterable: { multi: true, search: true, dataSource: ds },
                                filterable: { multi: true, search: true }
                            },


                                {
                                    field: "Location",
                                    title: "Location",
                                    width: "300px",
                                    //filterable: { multi: true, search: true, dataSource: ds },
                                    filterable: {  search: true },

                                },

                                 {
                                     field: "AccountRep",
                                     title: "Account Rep",
                                     width: "150px",
                                     //filterable: { multi: true, search: true, dataSource: ds }
                                     filterable: { search: true }
                                 },
                                 {
                                     field: "AccountRepPhone", 
                                     //template: "\#= kendo.toString(AccountRepPhone,'(###) ##-####') \#",
                                     title: "Rep Phone",
                                     width: "150px",
                                     template: function (dataItem) {
                                         return formatNumber(dataItem);
                                     },
                                     //filterable: { multi: true, search: true, dataSource: ds }
                                     filterable: {  search: true }
                                 },
                                 

                                 {
                                     field: "VendorStatus",
                                     title: "Status",
                                     width: "150px",
                                     //template: "\#= IsActive ? 'Active' : 'Inactive' \#"
                                     //filterable: { multi: true, search: true, dataSource: ds }
                                     filterable: { multi: true, search: true }

                                 },
                                 {
                                     field: "OrderingMethodName",
                                     title: "Ordering Method",
                                     width: "150px",
                                     filterable: { multi: true, search: true }
                                 },

                        ],
                        filterable: {
                            extra: true,
                            operators: {
                                string: {
                                    contains: "Contains",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    isempty: "Empty",
                                    isnotempty: "Not empty"


                                },
                                number: {
                                    eq: "Equal to",
                                    neq: "Not equal to",
                                    gte: "Greater than or equal to",
                                    lte: "Less than or equal to"
                                },
                                date: {
                                    gt: "After (Excludes)",
                                    lt: "Before (Includes)"
                                },
                            }
                        },
                        noRecords: { template: "No records found" },
                      
                        filterMenuInit: function (e) {
                            $(e.container).find('.k-check-all').click()
                        },



                        pageable: {
                            refresh: true,
                            pageSize: 25,
                            pageSizes: [25, 50, 100],
                            buttonCount: 5,
                            change: function (e) {
                                var myDiv = $('.k-grid-content.k-auto-scrollable');
                                myDiv[0].scrollTop = 0;
                            }
                        },
                      
                        sortable: ({ field: "CreatedOnUtc", dir: "desc" }),
                       
                        //toolbar: kendo.template($(' <script id="template" type="text/x-kendo-template"><input ng-keyup="AccountgridSearch()" type="search" id="searchBox" placeholder="Search any column" class="k-textbox form-control frm_controlAccount" style="width: 250px !important;margin-left: 0px !important;"><input type="button" id="btnReset" ng-click="VendorgridSearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></script>').html()),
                        toolbar: kendo.template($('<script id="template" type="text/x-kendo-template"><div class="row vendor_search no-margin" ><div class="leadsearch_topleft row no-margin col-sm-9 col-md-9 col-xs-9 no-padding"><div class="col-xs-2 col-sm-2 no-padding hfc_txtbox"><input ng-keyup="VendorgridSearch()" type="search" id="searchBox" placeholder="Search Vendor" class="k-textbox leadsearch_tbox"></div><div class="col-xs-2 col-sm-2 measurement_search no-padding"><input type="button" id="btnReset" ng-click="VendorgridSearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div></div><div class="col-sm-3 col-xs-3"><div class="float-right mt-2"><input class="option-input" name="VendorCheck" style="margin-right:5px;" type="checkbox" ng-click="GetallPrdtDetails()"><label class="ldet_label">Show Inactive</label></div></div></div></div></script>').html()),
                    };
                    //});
                };


                    srv.SelectDateRange = function (dateRange) {
                    srv.DateRangeText = dateRange;
                    var dates = HFCService.GetDateRange(srv.DateRangeText);
                    srv.StartDate = dates.StartDate;
                    srv.EndDate = dates.EndDate;
                }

                this.orderBy = function () {
                    return this.predicate.substr(1);
                };

                this.orderByDesc = function () {
                    return this.predicate.substr(0, 1) == '-';
                };
            }
])
})();