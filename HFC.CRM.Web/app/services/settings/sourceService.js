﻿'use strict';
app.service('sourceService', [
     '$http', 'HFCService', 'NavbarService', function ($http, HFCService, NavbarService) {
        var srv = this;
        srv.Sources = [];
        srv.GetCampaignsByFranchise = [];
        srv.GetSources = function() {
            return $http.get('/api/leadsources/0/getsourses');
        }
        srv.NavbarService = NavbarService;
        srv.NavbarService.SelectSettings();

        srv.GetCampaignsByFranchise = function () {
            srv.SourcesAndCampaigns = {
                dataSource :{
                    transport: {
                        read: {
                       
                            url: '/api/lookup/0/GetCampaignByFranchise',
                        },
                        update: {
                            url: '/api/leadsources/0/UpdateCampaign',
                            type: "POST",
                            complete: function (e) {
                                $("#gridCampaigns").data("kendoGrid").dataSource.read();
                                HFC.DisplaySuccess("Campaign Updated Successfully.");
                            }
                            
                        },
                        create: {
                            url: '/api/leadsources/0/AddNewCampaign',
                            type: "POST",
                            complete: function (e) {
                                $("#gridCampaigns").data("kendoGrid").dataSource.read();
                                HFC.DisplaySuccess("New Campaign added sucessfully.");
                            }
                           
                        },
                        parameterMap: function (options, operation) {
                            if (operation === "create") {
                               
                                return {
                                    CampaignId:options.CampaignId,
                                    Name: options.Campaign,
                                    StartDate: options.CampaignStartDate.toLocaleString(),
                                    EndDate: options.CampaignEndDate.toLocaleString(),
                                    Amount:options.Amount,
                                    Memo:options.Memo,
                                    IsActive:options.CampaignIsActive,
                                    SourcesTPId: options.SourcesTPId.SourcesTPId
                                };
                            }
                            else if (operation === "update") {
                                return {
                                    CampaignId: options.CampaignId,
                                    Name: options.Campaign,
                                    StartDate: options.CampaignStartDate.toLocaleString(),
                                    EndDate: options.CampaignEndDate.toLocaleString(),
                                    Amount: options.Amount,
                                    Memo: options.Memo,
                                    IsActive: options.CampaignIsActive,
                                    SourcesTPId: options.SourcesTPId
                                };
                            }
                        }
                    },
                    error: function (e) {
                        HFC.DisplayAlert(e.errorThrown);
                    },
                    
                    schema: {
                        model: {
                            id: "CampaignId",
                            fields: {
                                CampaignId: { editable: false, nullable: true },
                                Campaign: { validation: { required: true } },
                                CampaignStartDate: { type: "date", validation: { required: true, min: 1 } },
                                //CampaignEndDate: { type: "date", validation: { required: true, min: 1 } },
                                CampaignIsDeleted: { type: "boolean" },
                                CampaignIsActive: { type: "boolean" },
                                Memo: { type: "string" },
                                Amount: { type: "number" },
                                Channel: { editable: false, visable: false },
                                Source: { editable: false, visable: false },
                                CampaignEndDate: {
                                    type: "date", validation: {
                                        validationMessage: "End date cannot be before start date.",
                                        required: true, min: 1, campaignValidation: function (input) {
                                            if ((input.is("[name='CampaignStartDate']") || input.is("[name='CampaignEndDate']")) && input.val() !== "") {
                                                if (input[0].kendoBindingTarget.source.CampaignEndDate <
                                                    input[0].kendoBindingTarget.source.CampaignStartDate) {
                                                    return false;
                                                }
                                            }

                                            return true;
                                        }
                                    }
                                },

        
                            }
                        }
                    }
                   

                },
                // https://www.telerik.com/forums/remove-page-if-no-records
                dataBound: function (e) {
                    //console.log("dataBound");
                    if (this.dataSource.view().length == 0) {
                        // The Grid contains No recrods so hide the footer.
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        // The Grid contains recrods so show the footer.
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }
                },
                resizable: true,
                columns: [
                    {

                        field: "Campaign",
                        title: "Campaign",
                        template: "<a class='test k-grid-edit' href=''><span>#= Campaign #</span></a>",
                        filterable: { multi: true, search: true }

                    },
                            {
                                field: "Source",
                                title: "Source",
                                filterable: { multi: true, search: true }


                            },
                             {
                                 field: "SourcesTPId",
                                 title: "Source",
                                 hidden:true,
                                 editor: SourceDropDownEditor


                             },
                            {
                                field: "CampaignStartDate",
                                title: "Start Date",
                                hidden: true,
                                type: "date",
                                filterable: HFCService.gridfilterableDate("date", "", ""),
                            },
                              {
                                  field: "CampaignEndDate",
                                  title: "End Date",
                                  hidden: true,
                                  type: "date",
                                  filterable: HFCService.gridfilterableDate("date", "", ""),
                              },
                               {
                                   field: "Amount",
                                   title: "Amount",
                                   hidden: true


                               },
                                {
                                    field: "Memo",
                                    title: "Memo",
                                    hidden: true


                                },
                             {
                                 field: "Channel",
                                 title: "Channel",
                                 filterable: { multi: true, search: true }
                             },

                               {
                                   field: "CampaignIsActive",
                                   title: "Active",
                                   template: " #if(CampaignIsActive){# Active #}else{# In Active#}# "
                                   

                               }

                ],
                editable: "popup",
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                noRecords: { template: "No records found" },
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [25, 50, 100],
                    buttonCount: 5
                },
                toolbar: [{ template: kendo.template($(' <script id="template" type="text/x-kendo-template"><div style="padding:5px;"><input ng-keyup="CampaigngridSearch()" type="search" id="searchBox" placeholder="Search any column" class="k-textbox leadsearch_tbox"><input type="button" id="btnReset" ng-click="CampaigngridSearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div></script>').html()) }, { name: "create", text: "Add New Campaign" }],

            };
      
        };

      
    }
]);