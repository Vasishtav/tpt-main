﻿'use strict';
app.service('QuoteService', [
    '$http', 'HFCService', function ($http, HFCService) {
        var srv = this;

        srv.GetPICToken = function () {
            return $http.get('/api/lookup/0/GetPICToken');
        };

        srv.GetProductProductGroup = function (token) {

            return $http.get('https://hfc.picbusiness.com/ProductGroup?access_token='+srv.GetPICToken());
        };

    }
]
    )