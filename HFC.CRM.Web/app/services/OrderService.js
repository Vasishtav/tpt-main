﻿'use strict';
app.service('OrderService', ['$http', '$rootScope', 'HFCService', function($http, $rootScope, HFCService) {
        var srv = this;

        srv.IsBusy = false;
        srv.OrderReview = {};
        srv.redirectFlag = false;

        srv.AddOrder = function(quoteId) {
            if (quoteId && !srv.IsBusy && HFCService.FranchiseIsSolaTechEnabled) {

                srv.IsBusy = true;
                $http.get('/api/purchaseOrders/' + quoteId + '/addorder/').success(function(res) {
                    HFC.DisplaySuccess("Quote added to order");
                    $rootScope.$broadcast('reload.purchaseOrders');
                    $rootScope.$broadcast('reload.orderedJobItems');
                });
                srv.IsBusy = false;
            }
        };

        srv.getOrderReview = function(orderId, success) {
            if (orderId && !srv.IsBusy) {
                srv.IsBusy = true;
                $http.get('/api/purchaseOrders/' + orderId + '/orderreview/').success(function(res) {
                    srv.OrderReview = res;

                    angular.forEach(srv.OrderReview.OrderItems, function(val, index) {
                        val.visible = false;
                    });

                    if (success)
                        success(srv.OrderReview);
                });
                srv.IsBusy = false;
            }
        };
        
    // withhold the order status update method and order data in payment page
        srv.setOrderStatusChangerMethod = function (func, data, flag, mpoFlag) {
            srv.orderStatusUpdateMethod = func;
            srv.orderDTOBackup = data;
            srv.redirectFlag = flag;
            srv.mpoFlag = mpoFlag;
        }

    }
]);
 


