﻿/***************************************************************************\
Module Name:  NewsUpdatesService
Project: HFC-Tochpoint
Created on: 14 Dec 2017
Created By: 
Copyright:
Description: NewsUpdatesService
Change History:
Date  By  Description

\***************************************************************************/

(function (window, document, undefined) {
    'use strict';

    angular.module('hfc.newsandupdates', [])
        .service('NewsService', [
            '$http',
            function ($http) {

                var srv = this;

                srv.getNews = function (newsId) {

                    var url = '/api/NewsUpdates/' + newsId + '/GetNews/';
                    return $http.get(url);
                }

                srv.getAllNews = function () {
                    var url = '/api/NewsUpdates/0/GetNews/';
                    return $http.get(url);
                }

                srv.getCurrentNews = function () {
                    var url = '/api/NewsUpdates/0/GetCurrentNews/';
                    return $http.get(url);
                }

                srv.addNews = function (model) {
                    var url = '/api/NewsUpdates/0/AddNews/';
                    return $http.post(url, model);
                }

                srv.updateNews = function (model) {
                    var url = '/api/NewsUpdates/0/UpdateNews/';
                    return $http.post(url, model);
                }

                srv.activateNews = function (newsId) {
                    var url = '/api/NewsUpdates/' + newsId + '/ActivateNews/';
                    return $http.post(url, newsId);
                }

                srv.deactivateNews = function (newsId) {
                    var url = '/api/NewsUpdates/' + newsId + '/DeactivateNews/';
                    return $http.post(url, newsId);
                }

                srv.moveUp = function (newsId) {
                    var url = '/api/NewsUpdates/' + newsId + '/MoveUp/';
                    return $http.post(url, newsId);
                }

                srv.moveDown = function (newsId) {
                    var url = '/api/NewsUpdates/' + newsId + '/MoveDown/';
                    return $http.post(url, newsId);
                }
            }
        ]);

})(window, document);


//appCP.service('NewsService', [
//    '$http', 'HFCService', 
//    function ($http, HFCService) {

//        var srv = this;

//        srv.getNews = function (newsId) {
//            var url = '/api/NewsUpdates/' + newsId + '/GetNews/';
//            return $http.get(url);
//        }

//        srv.getAllNews = function () {
//            var url = '/api/NewsUpdates/0/GetNews/';
//            return $http.get(url);
//        }

//        srv.getCurrentNews = function () {
//            var url = '/api/NewsUpdates/0/GetCurrentNews/';
//            return $http.get(url);
//        }
        
//        srv.addNews = function (model) {
//            var url = '/api/NewsUpdates/0/AddNews/';
//            return $http.post(url, model);
//        }

//        srv.updateNews = function (model) {
//            var url = '/api/NewsUpdates/0/UpdateNews/';
//            return $http.post(url, model);
//        }

//        srv.activateNews = function (newsId) {
//            var url = '/api/NewsUpdates/' + newsId + '/ActivateNews/';
//            return $http.post(url, newsId);
//        }

//        srv.deactivateNews = function (newsId) {
//            var url = '/api/NewsUpdates/' + newsId + '/DeactivateNews/';
//            return $http.post(url, newsId);
//        }
//    }
//]);
