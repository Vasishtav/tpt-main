﻿/***************************************************************************\
Module Name:  CalendarService Service
Project: HFC-Tochpoint
Created on: 14 Dec 2017
Created By: 
Copyright:
Description: CalendarService
Change History:
Date  By  Description

\***************************************************************************/

app.service('CalendarServiceTP', [
    '$http', 'HFCService', 
    function ($http, HFCService) {

        var srv = this;

        srv.getLeadEvents = function (leadId) {
            var url = '/api/Calendar/' + leadId + '/GetLeadEvents/';
            return $http.get(url);
        }
        
        srv.getAccountEvents = function (accountId) {
            var url = '/api/Calendar/' + accountId + '/GetAccountEvents/';
            return $http.get(url);
        }

        srv.getOpportunityEvents = function (accountId) {
            var url = '/api/Calendar/' + accountId + '/GetOpportunityEvents/';
            return $http.get(url);
        }


        //srv.getAllEvents = function () {
        //    var url = '/api/Calendar/0/GetAllEvents/';
        //    return $http.get(url);
        //}

        //srv.getMyEvents = function () {

        //}

        //srv.getAllTasks = function(){
        //    var url = '/api/Calendar/0/GetAllTasks/';
        //    return $http.get(url);
        //}



        //For the task options
        srv.getLeadTasks = function (leadId) {
            var url = '/api/Calendar/' + leadId + '/GetLeadTasks/';
            return $http.get(url);
        }

        srv.getAccountTasks = function (accountId) {
            var url = '/api/Calendar/' + accountId + '/GetAccountTasks/';
            return $http.get(url);
        }

        srv.getOpportunityTasks = function (accountId) {
            var url = '/api/Calendar/' + accountId + '/GetOpportunityTasks/';
            return $http.get(url);
        }


    }
]);
