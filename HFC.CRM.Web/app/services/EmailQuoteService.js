﻿/***************************************************************************\
Module Name:  EmailService.js - AngularJS file
Project: Tochpoint
Created on: 07 November 2017
Created By:
Copyright:
Description: Sending Email - AngularJS
Change History:
Date  By  Description

\***************************************************************************/
app.directive('emailQuoteModal', ['$http', 'PrintServiceTP', 'PrintService', '$window', 'HFCService', 'EmailQuoteService', 'PersonService', 'LeadService', 'NavbarService',
    function ($http, printServiceTP, PrintService, $window, HFCService, EmailQuoteService, PersonService, LeadService, NavbarService) {
        return {
            restrict: 'E',
            scope: {

                leadId: '=',
                accountId: '=',
                opportunityId: '=',
                quoteId: '=',
                orderId: '=',
                associatedWith: '=',
                printExistingMeasurements: '=',
                control: '=' // this is way to expose the method callable by the parent controller.
            },
            templateUrl: '/templates/NG/EmailQuote-modal.html',
            replace: true,
            link: function (scope) {
                scope.EmailQuoteService = EmailQuoteService;
                scope.HFCService = HFCService;
                scope.PersonService = PersonService;
                scope.printServiceTP = printServiceTP;
                scope.printservice = PrintService;
                scope.LeadService = LeadService;
                scope.NavbarService = NavbarService;


                scope.isValid = false;
                scope.primaryEmail = "";
                scope.printOptions = {};
                scope.mydocumentList = {};


                function setPrintOptions() {
                    scope.printOptions = {
                        leadDetails: false,
                        internalNotes: false,
                        externalNotes: false,
                        directionNotes: false,
                        googleMap: false,
                        existingMeasurements: false,
                        bbmeasurementForm: {
                            Selected: false,
                            Quantity: 1
                        },
                        ccmeasurementForm: {
                            Selected: false,
                            Quantity: 1
                        },
                        garagemeasurementForm: {
                            Selected: false,
                            Quantity: 1
                        },
                        closetmeasurementForm: {
                            Selected: false,
                            Quantity: 1
                        },
                        officemeasurementForm: {
                            Selected: false,
                            Quantity: 1
                        },
                        floormeasurementForm: {
                            Selected: false,
                            Quantity: 1
                        },
                        warranty: false,
                        installChecklist: false,
                        installProcess: false,
                        orderCustomerInvoice: false,
                        orderInstallerInvoice: false,
                        quote: false
                    };
                    scope.printFormat = {
                        //order: 'CondensedVersion',
                        //quote: 'CondensedVersion',
                    };
                }

                function setMydocuments() {
                    scope.printServiceTP.getMydocumentsForSales()
                        .then(function (result) {
                            var obj = result.data;
                            scope.mydocumentList = obj;

                        }
                            , function (resultError) {

                            });
                }

                function setMydocumentsForInstall() {
                    scope.printServiceTP.getMydocumentsForInstall()
                        .then(function (result) {
                            var obj = result.data;
                            scope.mydocumentList = obj;

                        }
                            , function (resultError) {

                            });
                }

                //https://stackoverflow.com/questions/16881478/how-to-call-a-method-defined-in-an-angularjs-directive
                scope.internalControl = scope.control || {};
                function errorHandler(reseponse) {
                    HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
                }
                function successHandler(leadServiceData) {

                    if (LeadService != null) {
                        var lead = leadServiceData.Lead;
                        scope.primaryEmail = lead.PrimCustomer.PrimaryEmail;
                        scope.EmailQuoteService.ToAddresses = scope.primaryEmail;
                    }


                }



                scope.internalControl.showPrintModaltt = function (packetTypee) {
                    //
                    scope.ShowOtherEmailData = true;

                    scope.SalesPacketList = [];
                    scope.InstallPacketList = [];
                    scope.brandid = HFCService.CurrentBrand;
                    setPrintOptions();

                    if (packetTypee == "SalesPacket") {

                        $http.get('/api/notes/0/GetsalesPacketsDocList?Id=' + scope.brandid).then(function (result) {
                            scope.SalesPacketList = result.data;
                            scope.InstallPacketList = [];
                        }, function (error) {
                        });

                        scope.packetType = packetTypee;
                        setMydocuments();
                    } else {

                        $http.get('/api/notes/0/GetInstallPacketsDocList?Idd=' + scope.brandid).then(function (result) {
                            scope.SalesPacketList = [];
                            scope.InstallPacketList = result.data;
                        }, function (error) {
                        });

                        scope.packetType = packetTypee;
                        setMydocumentsForInstall();
                    }

                    scope.isValid = false;
                    scope.EmailQuoteService.ToAddresses = "";
                    scope.EmailQuoteService.PersonPrimaryEmail = "";
                    scope.EmailQuoteService.PersonPrimaryEmail = HFCService.CurrentUser.Email;
                    scope.EmailQuoteService.Errortext = "";

                    scope.EmailQuoteService.EnableEmailSignature = HFCService.EnableEmailSignature;
                    scope.EmailQuoteService.AddEmailSignAllEmails = HFCService.AddEmailSignAllEmails;

                    // Emailsignature checkbox unchecked default
                    scope.EmailQuoteService.Emailsignature = false;

                    //Show Emailsignature checkbox
                    scope.EmailQuoteService.displayEmailsignature = true;

                    // Emailsignature checkbox disable when Add Email Signature to All Emails true
                    scope.EmailQuoteService.cboxDisableEmailsignature = false;

                    //Emailsignature checkbox checked and Locked
                    if (HFCService.EnableEmailSignature && HFCService.AddEmailSignAllEmails) {
                        scope.EmailQuoteService.Emailsignature = true;
                        scope.EmailQuoteService.cboxDisableEmailsignature = true;
                    }

                    //scope.EmailQuoteService.baseurl = this.getUrl();

                    if (scope.NavbarService.AssociatedWith == "Lead") {
                        scope.EmailQuoteService.Associatedwith = scope.NavbarService.AssociatedWith;
                        scope.EmailQuoteService.id = scope.NavbarService.leadIdPrint;
                        id = scope.NavbarService.leadIdPrint;
                        scope.LeadService.Get(id, successHandler, errorHandler);
                        scope.EmailQuoteService.setAttachments(id, scope.NavbarService.AssociatedWith);

                    } else if (scope.NavbarService.AssociatedWith == "Opportunity") {
                        scope.EmailQuoteService.Associatedwith = scope.NavbarService.AssociatedWith;
                        scope.EmailQuoteService.id = scope.NavbarService.opportunityIdPrint;
                        id = scope.NavbarService.opportunityIdPrint;
                        var url = "/api/lookup/" + id + "/GetPrimaryEmail?type=" + scope.NavbarService.AssociatedWith;
                        $http.get(url).then(function (response) {
                            var data = response.data;
                            if (data.length > 0) {
                                scope.EmailQuoteService.ToAddresses = data[0].PrimaryEmail;
                            }
                            scope.EmailQuoteService.setAttachments(id, scope.NavbarService.AssociatedWith);

                        });
                    } else if (scope.NavbarService.AssociatedWith == "Order") {
                        scope.EmailQuoteService.Associatedwith = scope.NavbarService.AssociatedWith;
                        scope.EmailQuoteService.id = scope.NavbarService.orderIdPrint;
                        id = scope.NavbarService.orderIdPrint
                        var url = "/api/lookup/" + id + "/GetPrimaryEmail?type=" + scope.NavbarService.AssociatedWith;
                        $http.get(url).then(function (response) {
                            var data = response.data;
                            if (data.length > 0) {
                                scope.EmailQuoteService.ToAddresses = data[0].PrimaryEmail;
                            }
                            scope.EmailQuoteService.setAttachments(id, scope.NavbarService.AssociatedWith);
                        });
                    }
                    else {
                        var obj = $("#_quoteEmailModal_");
                        obj.modal("show");
                        var myDiv = $('.emailquote_popup');
                        myDiv[0].scrollTop = 0;
                    }



                }

                //Separate method for msr report e-mail (passing data)
                scope.internalControl.showReportPrintModaltt = function (ReportDatas) {

                    scope.brandid = HFCService.CurrentBrand;
                    scope.isValid = false;
                    scope.EmailQuoteService.ToAddresses = "accountinghfc@homefranchiseconcepts.com";
                    scope.EmailQuoteService.ccAddresses = "";
                    scope.EmailQuoteService.BccAddresses = "";
                    scope.EmailQuoteService.PersonPrimaryEmail = "";
                    scope.EmailQuoteService.PersonPrimaryEmail = HFCService.CurrentUser.Email;
                    scope.ShowOtherEmailData = false;
                    scope.EmailQuoteService.Subject = "MSR Report - " + HFCService.FranchiseName;
                    scope.EmailQuoteService.Errortext = "";
                    //if (scope.NavbarService.AssociatedWith == "Report") {
                    //    scope.EmailQuoteService.setReportAttachments(ReportDatas);//Call
                    //}
                    scope.EmailQuoteService.Body = "";
                    scope.EmailQuoteService.RepotTerritoryid = ReportDatas.TerritoryId;
                    scope.EmailQuoteService.ReportStartDate = ReportDatas.startdate;
                    scope.EmailQuoteService.ReportAttachmentName = ReportDatas.TerritoryCode + "_" + ReportDatas.MonthName + ReportDatas.Year + "_" + "MSR.pdf";
                    var obj = $("#_quoteEmailModal_");
                    obj.modal("show");
                    var myDiv = $('.emailquote_popup');
                    myDiv[0].scrollTop = 0;

                    //Hide Emailsignature checkbox
                    scope.EmailQuoteService.displayEmailsignature = false;

                }

                // 1 : Budget Blinds
                // 2 : Tailored Living
                // 3 : Concrete Craft

                scope.brandid = HFCService.CurrentBrand;


                if (!scope.associatedWith) {
                    scope.associatedWith = "Lead";
                }

                if (!scope.packetType) {
                    scope.packetType = "SalesPacket";
                }

                scope.cancel = function (modl) {
                    //alert("Canceling Process print sales packet");
                    hideMe();
                    var myDiv = $('.emailquote_popup');
                    myDiv[0].scrollTop = 0;
                    alert('f');
                }

                //function isvalidInput(printOptions, myDocuments) {
                //    var validInput = true;
                //    var temp = [];
                //    temp.push(printOptions.bbmeasurementForm);
                //    temp.push(printOptions.ccmeasurementForm);
                //    temp.push(printOptions.garagemeasurementForm);
                //    temp.push(printOptions.closetmeasurementForm);
                //    temp.push(printOptions.officemeasurementForm);
                //    temp.push(printOptions.floormeasurementForm);
                //    if (myDocuments) {
                //        for (var index = 0; index < myDocuments.length; index++) {
                //            temp.push(myDocuments[index]);
                //        }
                //    }
                //    for (var index2 = 0; index2 < temp.length; index2++) {
                //        var obj = temp[index2];
                //        if (obj.Selected && obj.Quantity < 1) {
                //            validInput = false;
                //            break;
                //        }
                //    }
                //    return validInput;
                //}


                //scope.getbaseurl = function () {
                //    var id = 0;
                //    var ticks = (new Date()).getTime();
                //    if (this.associatedWith == "Lead") {
                //        id = this.leadId;
                //    } else if (this.associatedWith == "Account") {
                //        id = this.accountId
                //    } else if (this.associatedWith == "Opportunity") {
                //        id = this.opportunityId
                //    } else {
                //        id = this.orderId
                //    }
                //    var baseurl = '/export/PrintSalesPacket/' + id + '/?type=PDF&inline=true&sourceType=' +
                //        this.associatedWith + '&t=' + ticks + '&printoptions='; // + options;
                //    return baseurl;
                //}

                scope.getUrl = function () {
                    var po = this.printOptions;
                    var md = this.mydocumentList;

                    var quoteKey = this.quoteId;

                    var options = "";
                    if (po.leadDetails) options += "leadDetails/";
                    if (po.internalNotes) options += "internalNotes/";
                    if (po.externalNotes) options += "externalNotes/";

                    if (po.directionNotes) options += "directionNotes/";

                    if (po.googleMap) options += "googleMap/";
                    if (po.quote) options += "quote/";
                    options += "quotePrintFormat=" + this.printFormat.quote + "/";
                    if (po.existingMeasurements) options += "existingMeasurements/";
                    if (po.bbmeasurementForm.Selected) options += "bbmeasurementForm|"
                        + po.bbmeasurementForm.Quantity + "/";
                    if (po.ccmeasurementForm.Selected) options += "ccmeasurementForm|"
                        + po.ccmeasurementForm.Quantity + "/";
                    if (po.garagemeasurementForm.Selected) options += "garagemeasurementForm|"
                        + po.garagemeasurementForm.Quantity + "/";
                    if (po.closetmeasurementForm.Selected) options += "closetmeasurementForm|"
                        + po.closetmeasurementForm.Quantity + "/";
                    if (po.officemeasurementForm.Selected) options += "officemeasurementForm|"
                        + po.officemeasurementForm.Quantity + "/";
                    if (po.floormeasurementForm.Selected) options += "floormeasurementForm|"
                        + po.floormeasurementForm.Quantity + "/";

                    options += "quoteKey|" + quoteKey + "/";

                    //var burl = this.getbaseurl();
                    options += this.getMydocumentsOptions();

                    var obj = {};
                    obj.printoptions = options;
                    obj.salesPacketOption = this.getSalesPacketOptions();
                    return obj;//'printoptions =' +options + '&salesPacketOption=' + this.getSalesPacketOptions();
                }

                scope.getMydocumentsOptions = function () {
                    var md = this.mydocumentList;
                    var options = "";
                    for (var index = 0; index < md.length; index++) {
                        var obj = md[index];
                        if (obj.Selected)
                            options += obj.PrintItemId + "|" + obj.Quantity + "/";
                    }

                    return options;
                }

                scope.getSalesPacketOptions = function () {

                    var salesPacket = "";
                    angular.forEach(scope.SalesPacketList, function (row, key) {
                        if (row.IsEnabled == true) {
                            salesPacket += row.streamId + "|" + row.Quantity + "/";
                        }
                    });

                    return salesPacket;
                }

                scope.getInstallerPacketOptions = function () {

                    var salesPacket = "";
                    angular.forEach(scope.InstallPacketList, function (row, key) {
                        if (row.IsEnabled == true) {
                            salesPacket += row.streamId + "|" + row.Quantity + "/";
                        }
                    });

                    return salesPacket;
                }

                function hideMe() {
                    var obj = $("#_quoteEmailModal_");
                    obj.modal("hide");
                }
                var streamid = [];
                var pdftitle = [];
                scope.selectChanged = function (TypeMail) {

                    if (TypeMail) {
                        var mydocumentselected = false;
                        if (mydocumentselected != true)
                            for (var index = 0; index < scope.mydocumentList.length; index++) {
                                var obj = scope.mydocumentList[index];
                                mydocumentselected = obj.Selected;
                                if (obj.Selected == true) {
                                    streamid.push(obj.streamId);
                                    pdftitle.push(obj.title);


                                }

                                if (mydocumentselected) break;
                            }

                        if (mydocumentselected != true)
                            for (var index = 0; index < scope.SalesPacketList.length; index++) {
                                var obj = scope.SalesPacketList[index];
                                mydocumentselected = obj.IsEnabled;
                                if (obj.IsEnabled == true) {
                                    streamid.push(obj.streamId);
                                    pdftitle.push(obj.title);


                                }
                                if (mydocumentselected) break;
                            }

                        if (mydocumentselected != true)
                            for (var index = 0; index < scope.InstallPacketList.length; index++) {
                                var obj = scope.InstallPacketList[index];
                                mydocumentselected = obj.IsEnabled;
                                if (obj.IsEnabled == true) {
                                    streamid.push(obj.streamId);
                                    pdftitle.push(obj.title);

                                }
                                if (mydocumentselected) break;
                            }


                        scope.isValid = mydocumentselected ||
                            scope.printOptions.bbmeasurementForm.Selected ||
                            scope.printOptions.ccmeasurementForm.Selected ||
                            scope.printOptions.garagemeasurementForm.Selected ||
                            scope.printOptions.closetmeasurementForm.Selected ||
                            scope.printOptions.officemeasurementForm.Selected ||
                            scope.printOptions.floormeasurementForm.Selected ||
                            scope.printOptions.leadDetails ||
                            scope.printOptions.internalNotes ||
                            scope.printOptions.externalNotes ||
                            scope.printOptions.directionNotes ||
                            scope.printOptions.googleMap ||
                            scope.printOptions.existingMeasurements ||
                            scope.printOptions.warranty || // The following are for only install packet type
                            scope.printOptions.installChecklist ||
                            scope.printOptions.installProcess ||
                            scope.printOptions.orderCustomerInvoice ||
                            scope.printOptions.orderInstallerInvoice ||
                            scope.printOptions.quote

                        if (this.packetType == "SalesPacket") {
                            scope.EmailQuoteService.packetType = "SalesPacket";
                            var data = this.getUrl();
                            scope.EmailQuoteService.printOptions = data.printoptions;
                            scope.EmailQuoteService.salesPacketOption = data.salesPacketOption;
                        }
                        else {
                            var obj2 = this.printOptions;

                            var md = this.getMydocumentsOptions();
                            var po = {
                                AddCustomerOpportunity: obj2.leadDetails,
                                AddGoogleMap: obj2.googleMap,
                                AddExistingMeasurements: obj2.existingMeasurements,
                                AddPrintInternalNotes: obj2.internalNotes,
                                AddPrintExternalNotes: obj2.externalNotes,
                                AddPrintDirectionNotes: obj2.directionNotes,
                                AddOrder: true,
                                AddInstallationChecklist: obj2.installChecklist,
                                AddWarrantyInformation: obj2.warranty,
                                AddInstallationProcess: obj2.installProcess,
                                AddOrderCustomerInvoice: obj2.orderCustomerInvoice,
                                AddOrderInstallerInvoice: obj2.orderInstallerInvoice,
                                OrderPrintFormat: this.printFormat.order,
                                MyDocuments: md
                            };
                            var instalOptions = this.getInstallerPacketOptions();
                            var options = '';

                            options = options + 'AddleadCustomer=' + po.AddCustomerOpportunity;
                            options = options + '/AddGoogleMap=' + po.AddGoogleMap;
                            options = options + '/AddPrintExistingMeasurements=' + po.AddExistingMeasurements;
                            options = options + '/AddPrintInternalNotes=' + po.AddPrintInternalNotes;
                            options = options + '/AddPrintExternalNotes=' + po.AddPrintExternalNotes;
                            options = options + '/AddPrintDirectionNotes=' + po.AddPrintDirectionNotes;
                            //options = options + '/AddOrder=' + PrintOrderOptions.AddOrder;
                            options = options + '/AddInstallationChecklist=' + po.AddInstallationChecklist;
                            options = options + '/AddWarrantyInformation=' + po.AddWarrantyInformation;
                            //options = options + '/PrintOrder=true';
                            options += '/AddInstallationProcess=' + po.AddInstallationProcess;
                            options += '/AddOrderCustomerInvoice=' + po.AddOrderCustomerInvoice;
                            options += '/AddOrderInstallerInvoice=' + po.AddOrderInstallerInvoice;
                            options += '/OrderPrintFormat=' + po.OrderPrintFormat;
                            options += '/PrintOrder=true';
                            // mydocuments
                            options += '/' + po.MyDocuments;

                            scope.EmailQuoteService.packetType = "InstallationPacket";
                            scope.EmailQuoteService.printOptions = options;
                            scope.EmailQuoteService.installPacketOptions = instalOptions;

                        }

                        scope.EmailQuoteService.Send(TypeMail);
                    }
                    else {
                        //msr report
                        scope.EmailQuoteService.Send(TypeMail);
                    }
                }

                //added , when clicking the quote or order parent check box from mail pop-up.
                scope.selectionChanged = function () {

                    if (scope.packetType == 'SalesPacket') {
                        //check if parent quote check box is enable make the 'condenseversion' default.
                        var quoteCheckbox = $('#quotecheck').is(":checked");
                        if (quoteCheckbox == true) {
                            scope.printFormat = {
                                quote: 'CondensedVersion'
                            };
                        } else {
                            scope.printFormat = {
                                quote: ''
                            };
                        }
                    }
                    //order
                    if (scope.packetType == 'InstallationPacket') {
                        //check if parent order check box is enable make the 'condenseversion' default.
                        var orderCheckbox = $('#ordercheck').is(":checked");
                        if (orderCheckbox == true) {
                            scope.printFormat = {
                                order: 'CondensedVersion'
                            };
                        } else {
                            scope.printFormat = {
                                order: ''
                            };
                        }
                    }

                }
                //-end

                //added the code, when selcting the child radio button ex:- condense,exclude line item discount etc.
                scope.quoteorderSubOptions = function (value, associated, type) {
                    if (value == true && associated != '' && type == 'SalesPacket') {
                        scope.printOptions.quote = true;

                    } else if (value == true && associated == '' && type == 'InstallationPacket') {
                        scope.printOptions.orderCustomerInvoice = true;

                    }
                }
                //-end

                //called when click on attachment from e-mail(msr)
                scope.OpenReportAttchment = function () {

                    window.open('/api/Reports/0/getMSRReport_PDF?TerritoryID=' + scope.EmailQuoteService.RepotTerritoryid + '&Mth=' + scope.EmailQuoteService.ReportStartDate + '', '_blank');
                }
            }
            // controller: 'AdvancedEmailController'
        }
    }]);
app.service('EmailQuoteService', ['$http', '$filter', 'HFCService', '$routeParams', '$window', 'PersonService', 'LeadService',
    function ($http, $filter, HFCService, $routeParams, $window, PersonService, LeadService) {
        var srv = this;
        srv.divId = "_quoteEmailModal_";
        srv.IsBusy = false;
        srv.SendUrl = "";
        srv.ToAddresses = [];
        srv.ccAddresses = [];
        srv.BccAddresses = [];
        srv.Subject = "";
        srv.Body = "";
        srv.PersonService = PersonService;
        srv.LeadService = LeadService;
        srv.PersonPrimaryEmail = "";
        srv.selectedIds = [];
        srv.selectOptions = null;
        srv.printOptions = "";
        srv.installPacketOptions = "";
        srv.salesPacketOption = "";
        srv.packetType = "";
        srv.Associatedwith = "";
        srv.id = "";
        srv.attachmentsize = "";
        srv.Errortext = "";
        srv.valid = true;
        srv.indFile = true;
        srv.ReportAttachmentName = "";
        srv.RepotTerritoryid = "";
        srv.ReportStartDate = "";
        srv.EmailSuccessCallback;
        srv.Emailsignature = false;
        srv.EnableEmailSignature = false;
        srv.AddEmailSignAllEmails = false;

        srv.selectItem = function () {
            var selectedData = [];
            selectedData = $("#Attachments1").data("kendoMultiSelect").value();
            if (selectedData.length > 0) {
                $http.get('/api/lookup/0/GetSizeAttachment?streamId=' + selectedData.join("','")).then(function (data) {
                    var sum = 0.00;
                    srv.valid = true;
                    srv.indFile = true;
                    srv.Errortext = "";
                    if (data.data.length > 0) {

                        for (var i = 0; i < data.data.length; i++) {
                            sum = sum + data.data[i].cached_file_size;
                            if (data.data[i].cached_file_size > 10.00) {
                                srv.valid = false;
                                srv.indFile = false;
                                srv.Errortext = "Attached file should not be greater than  10 MB!";
                            }


                        }
                    }
                    else
                        sum = 0;
                    srv.attachmentsize = parseFloat(sum).toFixed(2);
                });
            }
            else
                srv.attachmentsize = "";
        }
        srv.selectOptions = {
            placeholder: "Select",
            dataTextField: "title",
            dataValueField: "AttachmentStreamId",
            height: 300,
            autoBind: true
        };

        srv.setAttachments = function (id, type) {
            srv.attachmentsize = "";
            //$("#Attachments1").data("kendoMultiSelect").value([]);
            srv.Body = "";
            srv.Subject = "";
            srv.ccAddresses = "";
            srv.BccAddresses = "";
            var result = null;
            var url = "/api/lookup/" + id + "/GetAttachmentList?type=" + type;
            $http.get(url).then(function (response) {
                result = response.data;
                if (result.length > 0) {
                    $("#Attachments1").data("kendoMultiSelect").setDataSource(result);
                    $("#Attachments1").data("kendoMultiSelect").options.maxSelectedItems = 10;
                }

                //else
                //$("#Attachments1").data("kendoMultiSelect").setDataSource([]);


                var obj = $("#_quoteEmailModal_");
                obj.modal("show");
                var myDiv = $('.emailquote_popup');
                myDiv[0].scrollTop = 0;
            });


        };

        srv.Cancel = function () {

            var obj = $("#_quoteEmailModal_");
            obj.modal("hide");
            var myDiv = $('.emailquote_popup');
            myDiv[0].scrollTop = 0;
        };


        srv.Send = function (TypeMail) {

            srv.valid = true;
            if (srv.IsBusy) return;
            // var filter = /\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*([,]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*$/;
            var filter = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            var emailVerified = true;
            srv.ToAddresses = srv.ToAddresses.replace(/\s/g, '');
            if (srv.ccAddresses.length > 0)
                srv.ccAddresses = srv.ccAddresses.replace(/\s/g, '');
            else srv.ccAddresses = "";
            if (srv.BccAddresses.length > 0)
                srv.BccAddresses = srv.BccAddresses.replace(/\s/g, '');
            else srv.BccAddresses = "";
            if (srv.ToAddresses == "" || srv.ToAddresses == null)
                srv.valid = false;
            else {
                var email_split = srv.ToAddresses.split(',');
                for (var i = 0; i < email_split.length; i++) {
                    if (email_split[i] != '') {
                        if (!filter.test(email_split[i])) {
                            srv.valid = false;
                            emailVerified = false;
                        }
                    }
                }
            }
            if (srv.ccAddresses != "") {
                var email_split = srv.ccAddresses.split(',');
                for (var i = 0; i < email_split.length; i++) {
                    if (email_split[i] != '') {
                        if (!filter.test(email_split[i])) {
                            srv.valid = false;
                            emailVerified = false;
                        }

                    }
                }
            }


            if (srv.BccAddresses != "") {
                var email_split = srv.BccAddresses.split(',');
                for (var i = 0; i < email_split.length; i++) {
                    if (email_split[i] != '') {
                        if (!filter.test(email_split[i])) {
                            srv.valid = false;
                            emailVerified = false;
                        }
                    }
                }
            }

            if (srv.Body == "" || srv.Body == null) {
                srv.valid = false;
            }
            if (srv.Subject == "" || srv.Subject == null) {
                srv.valid = false;
            }
            if (parseFloat(srv.attachmentsize) > 23.00)
                srv.valid = false;

            if (!srv.EnableEmailSignature && srv.Emailsignature)
                srv.valid = false;

            if (srv.valid && TypeMail == true) {
                srv.Errortext = "";
                var selectedData = [];
                selectedData = $("#Attachments1").data("kendoMultiSelect").value();
                var Emaildata = {
                    toAddresses: srv.ToAddresses,
                    bccAddresses: srv.BccAddresses,
                    ccAddresses: srv.ccAddresses,
                    subject: HFC.htmlEncode(srv.Subject),
                    body: HFC.htmlEncode(srv.Body),
                    streamid: selectedData,
                    printOptions: srv.printOptions,
                    salesPacketOption: srv.salesPacketOption,
                    installPacketOptions: srv.installPacketOptions,
                    packetType: srv.packetType,
                    Associatedwith: srv.Associatedwith,
                    id: srv.id,
                    Emailsignature: srv.Emailsignature
                };

                srv.IsBusy = true;
                $http.post('api/Email/0/SendEmail', Emaildata).then(function (response) {
                    srv.IsBusy = false;
                    if (response.data == true) {
                        var obj = $("#_quoteEmailModal_");
                        obj.modal("hide");
                        if (srv.EmailSuccessCallback) {
                            srv.EmailSuccessCallback();
                        }
                        var myDiv = $('.emailquote_popup');
                        myDiv[0].scrollTop = 0;
                    }
                });

            }
            //for msr report email
            else if (srv.valid && TypeMail == false) {
                srv.Errortext = "";
                //var selectedData = [];
                //selectedData = $("#Attachments1").data("kendoMultiSelect").value();
                var Emaildata = {
                    toAddresses: srv.ToAddresses,
                    bccAddresses: srv.BccAddresses,
                    ccAddresses: srv.ccAddresses,
                    subject: HFC.htmlEncode(srv.Subject),
                    body: HFC.htmlEncode(srv.Body),
                    TerritoryID: srv.RepotTerritoryid,
                    Mth: srv.ReportStartDate,
                    //streamid: selectedData,
                    //printOptions: srv.printOptions,
                    //salesPacketOption: srv.salesPacketOption,
                    //installPacketOptions: srv.installPacketOptions,
                    //packetType: srv.packetType,
                    //Associatedwith: srv.Associatedwith,
                    //id: srv.id,
                };

                srv.IsBusy = true;
                $http.post('api/Reports/0/getMSRReport_Email', Emaildata).then(function (response) {
                    srv.IsBusy = false;
                    if (response.data == true) {
                        var obj = $("#_quoteEmailModal_");
                        obj.modal("hide");
                        if (srv.EmailSuccessCallback) {
                            srv.EmailSuccessCallback();
                        }
                        var myDiv = $('.emailquote_popup');
                        myDiv[0].scrollTop = 0;
                    }
                });
            }
            else {


                if (parseFloat(srv.attachmentsize) > 23.00)
                    srv.Errortext = "Total Attachment size should not be greater than 23 MB!";
                else if (emailVerified == false)
                    srv.Errortext = "Not able to recognize Email id. Please enter a valid email address!";
                else if (srv.Body == "")
                    srv.Errortext = "Email content should not be empty.";
                else if (srv.indFile == false)
                    srv.Errortext = "Attached file should not be greater than of size 10 MB!";
                else if (!srv.EnableEmailSignature && srv.Emailsignature)
                    srv.Errortext = "Enable Email Signature on a user's profile must be selected to add a signature to an email.";
                else
                    srv.Errortext = "";

            }
        }
    }
]);