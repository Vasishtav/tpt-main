﻿'use strict';
app.service('PurchaseOrderService', ['$http', '$window', '$location', 'LeadNoteService', function($http, $window, $location, LeadNoteService) {

        var srv = this;

        srv.Get = function(invoiceId, success) {
            if (!isNaN(invoiceId) && invoiceId > 0 && success != null) {
                $http.get('/api/invoices/' + invoiceId).then(success, function(response) {
                    HFC.DisplayAlert(response.statusText);
                });
            }
        }
        srv.InvoiceId = 0;

        srv.Payments = function(quote, job) {
            if (job.Invoices.length == 0) {
                if (!isNaN(quote.QuoteId) && quote.QuoteId > 0 && !isNaN(job.JobId) && job.JobId > 0) {
                    $http.post('/api/invoices/Post', JSON.stringify({ 'quoteId': quote.QuoteId, 'jobId': job.JobId }))
                        .then(function(response) {
                            srv.InvoiceId = response.data.InvoiceId;
                            HFC.DisplayAlert(response.statusText);
                        });
                    job.InvoiceButtonText = 'Invoice Created';
                }
            }
            if (job.Invoices.length > 0 || job.InvoiceButtonText == 'Apply Payment') {
                var _InvoiceId = (typeof job.Invoices[0] !== 'undefined' && typeof job.Invoices[0].InvoiceId !== 'undefined')
                    ? job.Invoices[0].InvoiceId : srv.InvoiceId;
                window.location.href = HFC.Util.BaseURL() + '#/invoices/detail/' + _InvoiceId + '/';
            }
        }
    }
]);



