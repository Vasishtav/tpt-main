﻿/***************************************************************************\
Module Name:  QuestionService.js - QuestionService AngularJS
Project: HFC
Created on: 15 Oct 2017
Created By:
Copyright:
Description: Adding Question for Qualification details
Change History:
Date  By  Description

\***************************************************************************/

'use strict';
app.service('QuestionService', ['$http', function ($http) {

        var srv = this;

        srv.IsBusy = false;
        srv.Questions = [];
        srv.LeadStatusId = 0;
        srv.DispositionId = 0;

        //gets a single lead and store it in this service as well as any pass in reference
        srv.GetQuestion = function (leadId, success, onError) {
            if (!isNaN(leadId) && leadId > 0)
            {
                $http.get('/api/questions/' + leadId).then(function (response) {
                    
                    var questionServiceData = response.data;

                    srv.Questions.push(questionServiceData);

                    if (success) 
                        success(questionServiceData);
                }, function (response) {

                    if (onError) {
                        onError(response);
                        return;
                    }

                    HFC.DisplayAlert(response.statusText);
                });
            }
        };
        
        srv.LeadDispositionByLeadStatus = function (id)
        {
            if (id == undefined || id == null)
            {
                id = 0;
            }
            $http.get('/api/lookup/' + id + '/Dispositions').then(function (data) {
                srv.LeadDispositionlist = data.data;
            });
        }

    }]);
