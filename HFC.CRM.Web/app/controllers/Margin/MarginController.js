﻿app.controller('marginController', [
    '$http', '$scope', '$routeParams', '$window', '$location', '$rootScope', 'HFCService', 'calendarModalService', function ($http, $scope, $routeParams, $window, $location, $rootScope, HFCService, calendarModalService) {
        $scope.OpportunityId = $routeParams.OpportunityId;
        $scope.quoteKey = $routeParams.quoteKey;
        $scope.quoteLineId = $routeParams.QuoteLineId;
        $scope.OrderId = $routeParams.orderId;
        $scope.OrderNumber = $routeParams.orderNumber;

        $scope.HFCService = HFCService;
        $scope.BrandId = $scope.HFCService.CurrentBrand;
        HFCService.setHeaderTitle("Margin Review #");
        $scope.Permission = {};

        var Quotepermission = $scope.HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Quote')
        var MarginReviewLineDetailpermission = Quotepermission.SpecialPermission.find(x=>x.PermissionCode == 'MarginReviewLineDetail ').CanAccess;
        $scope.Permission.MarginLinedetails = MarginReviewLineDetailpermission; //$scope.HFCService.GetPermissionTP('Margin Review Line Detail').CanAccess;

        $scope.QuoteHeader = {
            QuoteKey: 0,
            QuoteID: 0,
            OpportunityId: 0,
            MeasurementId: '',
            OpportunityName: '',
            Address1: '',
            Address2: '',
            City: '',
            State: '',
            ZipCode: '',
            AccountName: '',
            QuoteName: '',
            PrimaryQuote: false,
            SalesAgent: '',
            QuoteStatus: '',
            QuoteStatusId: 1,
            TotalCharges: 0,
            Discount: 0,
            DiscountType: '',
            TotalDiscounts: 0,
            NetCharges: 0,
            CreatedOn: 0,
            CreatedBy: '',
            LastUpdatedOn: 0,
            LastUpdatedBy: '',
            ExpiryDate: 0,
            QuoteLinesList: {},
            MeausurementList: {},
            TotalMargin: 0,
            QuoteSubTotal: 0,
            ProductSubTotal: 0,
            DiscountId: 0,
            Discount: 0,
            DiscountType: '',
            AdditionalCharges: 0,
            BillingAddress: '',
            OrderExist: false,
            PrimaryQuoteName: '',
            PrimaryQuoteId: 0,
            CoreProductMargin: 0,
            CoreProductMarginPercent: 0,
            MyProductMargin: 0,
            MyProductMarginPercent: 0,
            ServiceMargin: 0,
            ServiceMarginPercent: 0,
            ServicesList: {},
            DiscountsList: {}
        }
        var Quoteline = {
            QuoteLineId: 0
            , QuoteKey: 0
            , MeasurementsetId: 0
            , VendorId: 0
            , ProductId: 0
            , SuggestedResale: 0
            , Discount: 0
            , CreatedOn: 0
            , CreatedBy: 0
            , LastUpdatedOn: 0
            , LastUpdatedBy: 0
            , IsActive: true
            , Width: 0
            , Height: 0
            , UnitPrice: 0
            , Quantity: 0
            , Description: ''
            , MountType: ''
            , ExtendedPrice: 0
            , Memo: ''
            , PICJson: ''
            , ProductName: ''
            , VendorName: ''
            , ProductTypeId: ''
             , PricingManagementId: 0
             , RuleNumber: 0
             , PricingCode: ''
             , PricingRule: ''
             , Markup: ''
             , RoundingRule: ''
        }

        $scope.RuleNumber = '1';
        $scope.PricingCode = '1';
        $scope.PricingRule = '1';
        $scope.PricingMarkup = '1';
        $scope.MarkupFactor = '1';
        $scope.RoundingRule = '1';

        //Initialize the Quote Object

        $scope.MarginView = {
            QuoteLineId: 0,
            Quantity: 0,
            Product: '',
            Vendor: '',
            //Start Pricing Strategy
            PricingStrategyId: 0
            , RuleNumber: 0
            , PricingCode: ''
            , PricingRule: ''
            , Markup: ''
            , RoundingRule: '',
            //End Pricing Strategy
            BaseCost: 0,
            SuggestedResaleCheck: false,
            BaseResalePrice: 0,
            BaseResalePriceCheck: false,
            BaseResalePriceWOPromos: 0,
            BaseResalePriceWoPromoscheck: false,

            SuggestedResale: 0,
            Discount: '',
            UnitPrice: 0,
            DiscountAmount: 0,
            ExtendedPrice: 0,
            MarginPercentage: 0,
            VendorPromo: '',
            SelectedPrice: ''
        }

        $scope.Discount = {
            DiscountId: '',
            Description: '',
            DiscountValue: 0,
            DiscountAmountPassed: 0,
            DiscountPercentPassed: 0,
            PromoStartDate: '',
            PromoEndDate: ''
        }

        $scope.SelectedgridRow = {};
        $scope.EditRowDetail = function (e) {
            var grid = $("#QuotelinesGrid").data("kendoGrid");
            grid.editRow(e);
        };

        $scope.tooltipOptions = {
            filter: "th",
            position: "top",
            hide: function (e) {
                this.content.parent().css('visibility', 'hidden');
            },
            show: function (e) {
                if (this.content.text().length > 1) {
                    this.content.parent().css('visibility', 'visible');
                } else {
                    this.content.parent().css('visibility', 'hidden');
                }
            },
            content: function (e) {
                var target = e.target.data().title; // element for which the tooltip is shown
                return target;//$(target).text();
            }
        };
        function GetQuoteReview() {
            var loadingElement = document.getElementById("loading");
            loadingElement.style.display = "block";
            $http.get('/api/Quotes/' + $scope.quoteKey + '/GetMarginDetails/').then(function (response) {
                if (response.data.error == '') {
                    $scope.QuoteHeader = response.data.data.quote;
                    $scope.QuotelineDatasource = response.data.data.quoteLines;
                    $scope.DiscountAndPromo = response.data.data.DiscountAndPromo;
                    $scope.costAudit = response.data.data.costAudit;
                    //for displaying task popup
                    calendarModalService.TaskPathId = $scope.QuoteHeader.OpportunityId;
                    var element = $("#QuotelinesGrid").kendoGrid({
                        //$scope.quoteLinesOptions = {
                        dataSource: {
                            type: "json",
                            transport: {
                                read: function (e) {
                                    e.success($scope.QuotelineDatasource);
                                },
                                //update: { url: '/api/Quotes/' + $scope.quoteKey + '/UpdatequoteLineMargin', dataType: "json" }
                                update: function (options) {
                                    var loadingElement = document.getElementById("loading");
                                    loadingElement.style.display = "block";
                                    $http.post('/api/Quotes/' + $scope.quoteKey + '/UpdatequoteLineMargin',
                                        options.data).then(function (response) {
                                            if (response.data.error == '') {
                                                $scope.QuoteHeader = response.data.data.quote;
                                                $scope.QuotelineDatasource = response.data.data.quoteLines;
                                                $scope.DiscountAndPromo = response.data.data.DiscountAndPromo;;
                                                $scope.costAudit = response.data.data.costAudit;
                                                //e.success($scope.QuotelineDatasource);
                                                $('#QuotelinesGrid').data('kendoGrid').dataSource
                                                    .data($scope.QuotelineDatasource);
                                                $('#QuotelinesGrid').data('kendoGrid').refresh();

                                                var mod = response.data.data.quoteLines;
                                                for (var i = 0; i < mod.length; i++) {
                                                    $('#discount_' + mod[i].QuoteLineId).data('kendoGrid').dataSource
                                                        .data($scope.DiscountAndPromo);
                                                    $('#discount_' + mod[i].QuoteLineId).data('kendoGrid').refresh();
                                                }

                                                //var grid = $("#QuotelinesGrid").getKendoGrid();
                                                //grid.dataSource.data(response.data.data.quoteLines);

                                                HFC.DisplaySuccess("Quote Modified");
                                            } else {
                                                HFC.DisplaySuccess(response.data.error);
                                            }
                                            loadingElement.style.display = "none";
                                        }).catch(function (error) {
                                            var loadingElement = document.getElementById("loading");
                                            loadingElement.style.display = "none";
                                        });
                                    //
                                },
                            },
                            schema: {
                                model: {
                                    id: "QuoteLineId",
                                    fields: {
                                        QuoteLineId: { editable: false },
                                        Quantity: { editable: false },
                                        Product: { editable: false },
                                        Vendor: { editable: false },
                                        PricingStrategyId: { editable: true },

                                        RuleNumber: { editable: false },
                                        PricingCode: { editable: false },
                                        PricingRule: { editable: false },
                                        PricingMarkup: { editable: false },
                                        MarkupFactor: { editable: false },

                                        Markup: { editable: false },
                                        RoundingRule: { editable: false },
                                        BaseCost: { editable: true },
                                        SuggestedResaleCheck: { editable: true, type: "boolean" },
                                        BaseResalePrice: { editable: false },
                                        BaseResalePriceCheck: { editable: true, type: "boolean" },
                                        BaseResalePriceWOPromos: { editable: false },
                                        BaseResalePriceWoPromoscheck: { editable: true, type: "boolean" },
                                        SuggestedResale: { editable: true },
                                        Discount: { editable: false },
                                        UnitPrice: { editable: false },
                                        DiscountAmount: { editable: false },
                                        ExtendedPrice: { editable: false },
                                        MarginPercentage: { editable: false },
                                        VendorPromo: { editable: false },
                                        SelectedPrice: { editable: true },
                                    }
                                }
                            }
                        },
                        autoSync: true,
                        batch: true,
                        resizable: true,
                        toolbar: [
                            {
                                template: $("#quotelineMargin").html()
                            }
                        ],
                        sortable: true,
                        filterable: true,
                        editable:true,// "incell",
                        detailInit: detailInit,
                        dataBound: function (e) {
                            this.expandRow(this.tbody.find("tr.k-master-row").first());
                        },
                        noRecords: { template: "No records found" },
                        columns: [
                            { field: "SelectedPrice", title: "Line", hidden: true },
                            {
                                field: "QuoteLineId", title: "Line ID",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + dataItem.QuoteLineNumber + "</span>";
                                },
                                //template: "#= QuoteLineNumber #",
                                width: "80px"
                            },
                            //{ field: "QuoteLineId", title: "Line Id" },
                            {
                                field: "Quantity", title: "Qty",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + dataItem.Quantity + "</span>";
                                }
                            },
                            { field: "Product", title: "Product" },
                            { field: "Vendor", title: "Vendor" },//pricingRuleShow
                            {
                                field: "PricingStrategyId", title: "Pricing Mgt Strategy",
                                template: ' <span class="Grid_Textalign"> <a type="button" class="linkrule" name="#=QuoteLineId#" value="#=PricingStrategyId#">#=PricingStrategyId#</a></span>'
                            },
                            {
                                field: "BaseCost",
                                title: "Base Cost",
                                //headerAttributes: { style: "white-space: normal" },
                                //headerTemplate: '<label title="Base cost includes any vendor promotions" class="report_tooltip">Base Cost</label>',
                                //format: "{0:c}",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.BaseCost) + "</span>";
                                },
                                editor: function (element, options) {
                                    if (options.model.ProductTypeId === 1 || options.model.ProductTypeId === 4) {
                                        $('<input disabled type="number" class="k-input k-textbox" name="BaseCost" data-bind="value:BaseCost">').appendTo(element);
                                    }
                                    else {
                                        $('<input type="number" class="k-input k-textbox" name="BaseCost" data-bind="value:BaseCost">').appendTo(element);
                                    }
                                }
                            },
                            {
                                field: "BaseResalePriceWOPromos",
                                title: "Base Resale Price",
                                format: "{0:c}",
                                template: function (e) {
                                    var str = '<input data-quotelineId="' + e.QuoteLineId + '" data-bind="checked:SelectedPrice" value="BaseResalePriceWOPromos" name="PriceCheck_' + e.QuoteLineId + '" ng-click="setDirtyFlag(' + e + 'a)"  class="option-input radio radio_space" type="radio" ';

                                    if (e.BaseResalePriceWoPromoscheck) {
                                        str = str + ' checked="checked" ';
                                    }

                                    if ($scope.QuoteHeader.QuoteOrder) {
                                        str = str + ' disabled ';
                                    }
                                    else {
                                    }
                                    str = str + '>' + '</input>' + '$' + e.BaseResalePriceWOPromos;
                                    return str;
                                },
                            },
                            //{
                            //    field: "BaseResalePrice",
                            //    title: "Base Resale Price",
                            //    format: "{0:c}",
                            //    //template: BaseResalePriceCheck,
                            //    template:
                            //        '<input  data-quotelineId="#=QuoteLineId#" data-bind="checked:SelectedPrice" value="BaseResalePrice" name="PriceCheck_#=QuoteLineId#" ng-click="setDirtyFlag(data)"  class="option-input radio radio_space" type="radio" #= BaseResalePriceCheck ? "checked=checked" : "" # >#=kendo.format("{0:c}", BaseResalePrice)#</input>',
                            //    editor: '<input data-quotelineId="#=QuoteLineId#" data-bind="checked:SelectedPrice" value="BaseResalePrice" name="PriceCheck_#=QuoteLineId#" ng-click="setDirtyFlag(data)"  class="option-input radio radio_space" type="radio" #= BaseResalePriceCheck ? "checked=checked" : "" # >#=kendo.format("{0:c}", BaseResalePrice)#</input>'
                            //},
                            {
                                field: "SuggestedResale",
                                title: "Suggested Resale",
                                format: "{0:c}",
                                template: function (e) {
                                    var str = '<input  data-quotelineId="' + e.QuoteLineId + '" data-bind="checked:SelectedPrice" value="SuggestedResale" name="PriceCheck_' + e.QuoteLineId + '"   ng-click="setDirtyFlag(' + e + 'a)" class="option-input radio radio_space" type="radio" ';
                                    if (e.SuggestedResaleCheck) {
                                        str = str + ' checked="checked" ';
                                    }
                                    if ($scope.QuoteHeader.QuoteOrder) {
                                        str = str + ' disabled ';
                                    }
                                    str = str + ' >' + '$' + e.SuggestedResale + '</input>';
                                    return str;
                                },
                            },

                            {
                                field: "Discount",
                                title: "Discount",
                                template: function (dataItem) {
                                    if (dataItem.ProductTypeId == 4) {
                                        if (dataItem.DiscountType === "$") {
                                            return "$" + dataItem.Discount;;
                                        } else if (dataItem.DiscountType === "%") {
                                            return dataItem.Discount + "%";
                                        }
                                        else if (dataItem.DiscountType == null) {
                                            if (dataItem.Discount != null && dataItem.Discount != undefined) {
                                                return dataItem.Discount + "%";
                                            }
                                        } else {
                                            return '';
                                        }
                                    }
                                    else {

                                        if (dataItem.Discount > 0 && dataItem.IsGroupDiscountApplied)
                                            return '<span>' + HFCService.NumberFormat(dataItem.Discount, 'number') + ' %</span>' + '<div class="tooltip-bottom pull-right" style="display:inline;" data-tooltip="Discount by Product Category Applied"><i class="fas fa-tags" style="padding-top:5px"></i></div>';
                                        else if (dataItem.Discount > 0)
                                            return '<span>' + HFCService.NumberFormat(dataItem.Discount, 'number') + ' %</span>'
                                        else
                                            return '0.00 %'
                                    }
                                },
                            },
                            {
                                field: "UnitPrice", title: "Unit Price",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.UnitPrice) + "</span>";
                                },
                                //format: "{0:c}"
                            },
                            {
                                field: "DiscountAmount", title: "Discount Amt", attributes: { class: 'text-right' },
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.DiscountAmount) + "</span>";
                                },
                                //format: "{0:c}"
                            },
                            {
                                field: "ExtendedPrice", title: "Extended Price",
                                template: function (dataItem) {
                                    return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.ExtendedPrice) + "</span>";
                                },
                                //format: "{0:c}"
                            },
                            {
                                field: "MarginPercentage",
                                title: "Margin",
                                template: '<span class="Grid_Textalign">#=kendo.format("{0:p}", MarginPercentage/100)#</span>'
                            },
                            { field: "VendorPromo", title: "Vendor Promo", format: "{0:c}" },

                        ]
                    });
                } else {
                    HFC.DisplaySuccess(response.data.error);
                }
                loadingElement.style.display = "none";
            }).catch(function (error) {
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";
            });
        }

        function detailInit(e) {
            var disc = $scope.DiscountAndPromo;
            var exists = false;

            if (disc != undefined && disc != '' && disc.length > 0) {
                for (var i = 0; disc.length > i; i++) {
                    if (e.data.QuoteLineId == disc[i].QuoteLineId) {
                        exists = true;
                    }
                }
            }
            if (exists) {
                $("<div style='width: 50%;float: right;'id='discount_" + e.data.QuoteLineId + "'/>")
                    .appendTo(e.detailCell).kendoGrid({
                        dataSource: {
                            type: "json",
                            //transport: {
                            //    read: '/api/Quotes/' + $scope.quoteKey + '/GetDiscountDetails/'
                            //},
                            transport: {
                                read: function (ei) {
                                    ei.success($scope.DiscountAndPromo);
                                },
                                update: function (options) {
                                    var loadingElement = document.getElementById("loading");
                                    loadingElement.style.display = "block";
                                    $scope.updateModels = options.data.models;
                                    $http.post('/api/Quotes/' + $scope.quoteKey + '/UpdateDiscountAndPromos',
                                        options.data.models).then(function (response) {
                                            //
                                            if (response.data.error == '') {
                                                $scope.QuoteHeader = response.data.data.quote;
                                                $scope.QuotelineDatasource = response.data.data.quoteLines;
                                                $scope.DiscountAndPromo = response.data.data.DiscountAndPromo;;
                                                $scope.costAudit = response.data.data.costAudit;
                                                var mod = $scope.updateModels;
                                                for (var i = 0; i < mod.length; i++) {
                                                    $('#discount_' + mod[i].QuoteLineId).data('kendoGrid').dataSource
                                                        .read();
                                                    $('#discount_' + mod[i].QuoteLineId).data('kendoGrid').refresh();
                                                }
                                                $('#QuotelinesGrid').data('kendoGrid').dataSource.read();
                                                $('#QuotelinesGrid').data('kendoGrid').refresh();

                                                HFC.DisplaySuccess("Quote Modified");
                                            } else {
                                                HFC.DisplaySuccess(response.data.error);
                                            }
                                            loadingElement.style.display = "none";
                                        }).catch(function (error) {
                                            var loadingElement = document.getElementById("loading");
                                            loadingElement.style.display = "none";
                                        });
                                    //
                                }
                            },
                            serverPaging: false,
                            serverSorting: false,
                            serverFiltering: false,

                            pageSize: 10,
                            batch: true,
                            filter: { field: "QuoteLineId", operator: "eq", value: e.data.QuoteLineId },
                            schema: {
                                model: {
                                    id: "Id",
                                    fields: {
                                        QuoteLineId: { editable: false },
                                        Description: { editable: false },
                                        Cost: { editable: false },
                                        Discount: { editable: false },
                                        DiscountPercentPassed: { editable: true },
                                        DiscountAmountPassed: { editable: false }
                                    }
                                }
                            }
                        },
                        toolbar: [
                            {
                                template: $("#toolbarTemplate1").html()
                            }
                        ],
                        scrollable: false,
                        sortable: true,
                        pageable: false,
                        resizable: true,
                        editable: true,
                        columns: [
                            { field: "QuoteLineId", title: "Line" },
                            { field: "Description", title: "Description" },
                             {
                                 field: "Cost",
                                 title: "Cost",
                                 //headerAttributes: { style: "white-space: normal" },
                                 //headerTemplate: '<label title="Cost without applied vendor promotions" class="report_tooltip">Cost</label>',
                                 format: "{0:c}"
                             },
                            { field: "Discount", title: "Discount Value", format: "{0:c}" },
                            //{ field: "DiscountPercentPassed", title: "Extended Discount", template: '#=kendo.format("{0:p}", DiscountPercentPassed/100)#', width: "150px" },
                            {
                                field: "DiscountPercentPassed",
                                title: "Extended Discount",
                                editor: extendedDiscount,
                                template: '#=kendo.format("{0:p}", DiscountPercentPassed/100)#'
                            },
                            { field: "DiscountAmountPassed", title: "Extended Value", format: "{0:c}" },
                            { field: "", title: "Promo Start Date" },
                            { field: "", title: "Promo End Date" }
                        ]
                    });
            }
        }

        $scope.PricingRuleDetails = function () {
            $scope.PricingRules = {
                resizable: true,
                columns: [
                    {
                        field: "PricingStrategyId",
                        title: "Rule Number"
                    },
                    {
                        field: "PricingCode",
                        title: "Pricing Code"
                    },
                    {
                        field: "PricingRule",
                        title: "Pricing Rule"
                    },
                    {
                        field: "PricingMarkup",
                        title: "MarkUp",
                        //template:
                        //    "#if(MarkupFactor === '$') {#=MarkupFactor# #=MarkUp#} else{#=MarkUp# #=MarkupFactor#}#"
                    },
                    {
                        field: "RoundingRule",
                        title: "Rounding"
                    }
                ],
                noRecords: { template: "No records found" },
                filterable: false,
                scrollable: true,
                sortable: true,
            };
        };
        $scope.PricingRuleDetails();

        $("#QuotelinesGrid").on("click", ".linkrule", function () {
            var SelectedgridRow;
            var quotelineId = Number($(this).attr('name'));
            //var grid = $("#QuotelinesGrid").data("kendoGrid").dataSource.data();
            var grid = $scope.QuotelineDatasource;
            for (var i = 0; i < grid.length; i++) {
                if (grid[i].QuoteLineId === quotelineId) {
                    $scope.SelectedgridRow = grid[i];
                    SelectedgridRow = grid[i];

                    $scope.RuleNumber = grid[i].RuleNumber;
                    $scope.PricingCode = grid[i].PricingCode;
                    $scope.PricingRule = grid[i].PricingRule;
                    $scope.PricingMarkup = grid[i].PricingMarkup;
                    $scope.MarkupFactor = grid[i].MarkupFactor;
                    $scope.RoundingRule = grid[i].RoundingRule;
                    $scope.$apply();
                }
            }
            //$scope.PricingRuleDetails();
            //$('#pricingmgmtGrid').data('kendoGrid').dataSource.read($scope.SelectedgridRow);
            //$('#pricingmgmtGrid').data('kendoGrid').dataSource.read();
            //$('#pricingmgmtGrid').data('kendoGrid').refresh();
            $("#pricingRuleModal").modal("show");
            //$scope.PricingRuleDetails();
        });
        $("#QuotelinesGrid").on("click", ":radio", function () {
            var grid = $("#QuotelinesGrid").data("kendoGrid");
            var radio = this;
            var qlid = radio.dataset.quotelineid;
            var val = radio.value;
            for (var i = 0; i < grid.dataItems().length; i++) {
                if (grid.dataItems()[i].QuoteLineId === Number(qlid)) {
                    grid.dataItems()[i].SelectedPrice = val;
                    if (val === "BaseResalePrice") {
                        grid.dataItems()[i].BaseResalePriceCheck = true;
                        grid.dataItems()[i].BaseResalePriceWoPromoscheck = false;
                        grid.dataItems()[i].SuggestedResaleCheck = false;
                    }
                    if (val === "SuggestedResale") {
                        grid.dataItems()[i].BaseResalePriceCheck = false;
                        grid.dataItems()[i].BaseResalePriceWoPromoscheck = false;
                        grid.dataItems()[i].SuggestedResaleCheck = true;
                    }
                    if (val === "BaseResalePriceWoPromos") {
                        grid.dataItems()[i].BaseResalePriceCheck = false;
                        grid.dataItems()[i].BaseResalePriceWoPromoscheck = true;
                        grid.dataItems()[i].SuggestedResaleCheck = false;
                    }
                    grid.dataItems()[i].dirty = true;
                    break;
                }
            }

            //var item = grid.dataItem(radio.closest("tr"));
        });

        function extendedDiscount(container, options) {
            var reg = [
                { text: "None", value: 0 },
                { text: "25%", value: 25 },
                { text: "50%", value: 50 },
                { text: "75%", value: 75 },
                { text: "100%", value: 100 }
            ];

            if (!$scope.QuoteHeader.QuoteOrder) {
                $('<input required  name="' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                    autoBind: false,
                    valuePrimitive: true,
                    dataTextField: "text",
                    dataValueField: "value",
                    dataSource: {
                        type: "json",
                        transport: {
                            read: function (ei) {
                                ei.success(reg);
                            },
                        },
                    }
                });
            }
        }

        $scope.CustomerView = function () {
            window.location.href = '/#!/quote/' + $scope.QuoteHeader.OpportunityId + '/' + $scope.QuoteHeader.QuoteKey;
        }
        //quote/{{QuoteHeader.OpportunityId}}/{{QuoteHeader.QuoteKey}}

        function BaseResalePriceWoPromoscheck(dataItem) {
            if (dataItem.BaseResalePriceWoPromoscheck) {
                return "<input type='radio' class='option-input radio radio_space' id='BaseResalePriceWOPromos' checked=checked  name='PriceCheck_" + dataItem.QuoteLineId + "' value='" + dataItem.SelectedPrice + "'>$" + dataItem.BaseResalePriceWOPromos.toFixed(2) + "</input>";
            } else {
                return "<input type='radio' class='option-input radio radio_space' id='BaseResalePriceWOPromos'  name='PriceCheck_" + dataItem.QuoteLineId + "'  value='" + dataItem.SelectedPrice + "'>$" + dataItem.BaseResalePriceWOPromos.toFixed(2) + "</input>";
            }
        }

        function BaseResalePriceCheck(dataItem) {
            if (dataItem.BaseResalePriceCheck) {
                return "<input type='radio' class='option-input radio radio_space' id='BaseResalePriceCheck' checked=checked  name='PriceCheck_" + dataItem.QuoteLineId + "'  value='" + dataItem.SelectedPrice + "'>$" + dataItem.BaseResalePrice.toFixed(2) + "</input>";
            } else {
                return "<input type='radio' class='option-input radio radio_space' id='BaseResalePriceCheck'  name='PriceCheck_" + dataItem.QuoteLineId + "' value='" + dataItem.SelectedPrice + "'>$" + dataItem.BaseResalePrice.toFixed(2) + "</input>";
            }
        }

        function SuggestedResaleCheck(dataItem) {
            if (dataItem.SuggestedResaleCheck) {
                return "<input type='radio' class='option-input radio radio_space'  id='SuggestedResaleCheck' checked=checked  name='PriceCheck_" + dataItem.QuoteLineId + "'  value='" + dataItem.SelectedPrice + "'>$" + dataItem.SuggestedResale.toFixed(2) + "</input>";
            } else {
                return "<input type='radio' class='option-input radio radio_space' id='SuggestedResaleCheck' name='PriceCheck_" + dataItem.QuoteLineId + "'  value='" + dataItem.SelectedPrice + "'>$" + dataItem.SuggestedResale.toFixed(2) + "</input>";
            }
        }

        //$scope.quoteReviewCancel = function() {
        //    document.getElementById("discountCancel").click();
        //    document.getElementById("headerCancel").click();

        //}

        GetQuoteReview();
        $scope.ApplyQuoteReviewChanges = function () {
            //document.getElementById("discountsave").click();
            //document.getElementById("headersave").click();
            $("#discountsave").click();
            $("#headersave").click();
        }
        $scope.showPopUp = function (modalId, quoteLineDetailId) {
            $http.get('/api/Quotes/' + quoteLineDetailId + '/GetVendorPromoDetails').then(function (data) {
                $scope.VendorsPromoList = data.data;
            });
            $("#" + modalId).modal("show");
        }
        $scope.Cancel = function (modalId) {
            $("#" + modalId).modal("hide");
        };

        // function pricingRuleShow () {
        //    //$scope.pricingRule = dataItem;
        //    $("#taxDetailsSummary").modal("show");
        //}

        function backToQuoteLineList() {
            history.go(-1);
        };
    }
]);