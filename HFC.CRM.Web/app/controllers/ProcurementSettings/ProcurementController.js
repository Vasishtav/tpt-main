﻿/***************************************************************************\
Module Name:  Product Controller .js - AngularJS file
Project: HFC
Created on: 02 November 2017 Thursday
Created By:
Copyright:
Description: Product Controller
Change History:
Date  By  Description

\***************************************************************************/
'use strict';
app.controller('ProcurementController', ['$scope', '$routeParams', '$rootScope', '$window', '$location', '$http','$interval',
                    'HFCService', 'AdditionalAddressServiceTP', 'AddressService', 'kendoService','NavbarService','AddressValidationService','$q',
    function ($scope, $routeParams, $rootScope, $window, $location, $http,$interval,
        HFCService, AdditionalAddressServiceTP, AddressService, kendoService, NavbarService, AddressValidationService, $q) {

        var ctrl = this;
        // Additional Address
        $scope.AdditionalAddressServiceTP = AdditionalAddressServiceTP;

        HFCService.setHeaderTitle("Procurement Settings #");

        $scope.NavbarService = NavbarService;
        $scope.NavbarService.SelectSettings();
        $scope.NavbarService.EnableSettingsOperationsTab();

        $scope.AddressService = AddressService;
        $scope.HFCService = HFCService;
        $scope.IsQBEnabled = $scope.HFCService.IsQBEnabled;
        $scope.Permission = {};
        var AddEditShipToLocationspermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'AddEditShipToLocations')

        $scope.Permission.AddProcurement = AddEditShipToLocationspermission.CanCreate; //$scope.HFCService.GetPermissionTP('Add ShiptoLocation').CanAccess;
        $scope.Permission.EditProcurement = AddEditShipToLocationspermission.CanUpdate; //$scope.HFCService.GetPermissionTP('Edit ShiptoLocation').CanAccess;
        $scope.Permission.ListProcurement = AddEditShipToLocationspermission.CanRead; //$scope.HFCService.GetPermissionTP('List ShiptoLocation').CanAccess;


        // for kendo tooltip
        $scope.kendotooltipOptions = kendoService.kendoToolTipOptions;
        kendoService.customTooltip = null;



        //Custom Template
        function ActivateDeActivateTemplate(dataitem) {

            var ActDeActhtml = "";
            if ($scope.Permission.EditProcurement) {
                if (dataitem.Active == true) {
                    return ActDeActhtml = '<ul> <li class="dropdown note1 ad_user"><div class="btn-group"><button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> <ul class="dropdown-menu pull-right"> <li><a href="javascript:void(0)" ng-click="DeActivateShipping(this)">Deactivate Ship To Location</a></li> </ul> </li> </ul>';
                }
                else if (dataitem.Active == false) {
                    return ActDeActhtml = '<ul> <li class="dropdown note1 ad_user"><div class="btn-group"><button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> <ul class="dropdown-menu pull-right"><li><a href="javascript:void(0)"  ng-click="ActivateShipping(this)">Activate Ship To Location</a></li> </ul> </li> </ul>';
                }
            }
            else { return ''}
        }


            $scope.GetShipToLocation = function () {
                $scope.GridEditable = false;
                $scope.ShipToLocation = {
                    dataSource: {
                        transport: {
                            read: {
                                url: function (params) {

                                    var url1 = '/api/Shipping/0/Get';
                                    return url1;
                                },
                                dataType: "json"
                            }

                        },
                        error: function (e) {

                            HFC.DisplayAlert(e.errorThrown);
                        },

                        schema: {
                            model: {
                                id: "id",
                                fields: {
                                    ShippingId: { editable: false},
                                    ShipToLocationName: {type:"string", editable: false, nullable: true },
                                    ShipToName: { type: "string", validation: { required: true } },
                                    AddressId: { type: "string", validation: { required: true } },
                                    Address1: { type: "string", type: "string" },
                                    Address2: { type: "string", type: "string" },
                                    State: { type: "string" },
                                    ZipCode: { type: "string" },
                                    Country: { type: "string" },
                                    TerritoriesName: { type: "string" },
                                    PhoneNumber:{editable:false},

                                }
                            }
                        }
                    },
                    columns: [
                        {


                            field: "ShippingId",
                            title: "Shipping Id",
                            width: "150px",
                            filterable: {
                                //multi: true,
                                search: true
                            },
                            hidden: true,


                        },
                        {


                            field: "ShipToLocationName",
                            title: "Ship To Location",
                            width: "150px",
                            filterable: {
                                //multi: true,
                                search: true
                            },
                            hidden: false,


                        },
                                {
                                    field: "ShipToName",
                                    title: "Ship To Name",
                                    width: "150px",
                                    filterable: { search: true },
                                    hidden:false,
                                    template: "# if(Active==true) {#<a class='test k-grid-edit' ng-click='EditShipping(this)'><span>#= ShipToName #</span></a>#} else {#<span>#= ShipToName #</span>#}#"

                                },
                                 {
                                     field: "Active",
                                     title: "Active",
                                     width: "100px",
                                     hidden: true,

                                 },
                                 {
                                     field: "Address1",
                                     title: "Address 1",
                                     width: "150px",
                                     hidden: false,
                                },
                                {
                                    field: "Address2",
                                    title: "Address 2",
                                    width: "150px",
                                    hidden: false,



                                },


                                  {
                                      field: "State",
                                      title: "State/Province",
                                      width: "150px",
                                      //attributes: { style: "display: none;" },
                                      //hidden: true,
                                     // template: "",
                                      //editor: FractionDropDownEditor,

                                     // hidden: true
                                  },
                                  {
                                      field: "ZipCode",
                                      title: "Zip Code",
                                      width: "100px",
                                      //attributes: { colspan: 2 },
                                      //template: "#= Width # #= FranctionalValueWidth #"

                                  },

                                   {
                                       field: "Country",
                                       title: "Country",
                                       width: "150px",
                                       //attributes: { style: "display: none;" },
                                       //hidden: true

                                   },
                                    {
                                        field: "PhoneNumber",
                                        title: "PhoneNumber",
                                        width: "150px",
                                        template: function (dataItem) {
                                            return formatNumber(dataItem);
                                        },

                                    },
                                   {
                                       field: "TerritoriesName",
                                       title: "Territories",
                                       width: "200px",
                                       //attributes: { style: "display: none;" },
                                       //editor: FractionDropDownEditor,
                                     //  hidden: true


                                   },
                                   {
                                       field: "",
                                       title: "",
                                       width: "100px",
                                       template: function(dataitem){
                                           return ActivateDeActivateTemplate(dataitem);
                                       }
                                   },


                    ],

                    filterable: {
                        extra: true,
                        operators: {
                            string: {
                                contains: "Contains",
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                isempty: "Empty",
                                isnotempty: "Not empty"


                            },
                            number: {
                                eq: "Equal to",
                                neq: "Not equal to",
                                gte: "Greater than or equal to",
                                lte: "Less than or equal to"
                            },
                            date: {
                                gt: "After (Excludes)",
                                lt: "Before (Includes)"
                            },
                        }
                    },
                    resizable: true,
                    autoSync: true,
                    noRecords: { template: "No records found" },
                    scrollable: true,
                    toolbar: [{ template: kendo.template($(' <script id="template" type="text/x-kendo-template"><div class="row vendor_search no-margin" ><div class="leadsearch_topleft row no-margin col-sm-9 col-md-9 col-xs-9 no-padding"><div class="col-xs-2 col-sm-2 no-padding hfc_txtbox"><input ng-keyup="ShipgridSearchEdit()" type="search" id="searchBox" placeholder="Search" class="k-textbox leadsearch_tbox"></div><div class="col-xs-2 col-sm-2 measurement_search no-padding"><input type="button" id="btnReset" ng-click="ShipgridSearchClear()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div></div><div class="col-sm-3 col-xs-3 no-padding"><div class="leadsearch_icon" ng-if="Permission.AddProcurement"><div class="plus_but tooltip-bottom procurement_tooltip" data-tooltip="Add New Ship To Location"  onclick="return false;" ng-click="AddShipping()"><i class="far fa-plus-square"></i></div></div></div></div></div></script>').html()) }],

                };

            };
            $scope.GetShipToLocation();

            function FormatContactNumber(text) {
                return text.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');
            }

            function formatNumber(dataItem) {
                var Div = '<address>';
                if (dataItem.PhoneNumber)
                    Div += FormatContactNumber(dataItem.PhoneNumber) + '</div>';
                Div += '<text></text>' + '</div>';
                Div += '</address>';


                return Div;
            }

        // Kendo Resizing
            $scope.$on("kendoWidgetCreated", function (e) {
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 318);
                window.onresize = function () {
                    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 318);
                };
            });
        // End Kendo Resizing
        //Pop Calling Function
            $scope.AddShipping =function()
            {
                $scope.InitializeDropdown();
                $scope.NullifyObject();
                $scope.Show('shiplocation');
            }
            $scope.EditShipping = function (e) {
                $scope.NullifyObject();
                var currentData = e.dataItem;
                $scope.Ship.Id = currentData.Id;
                $scope.Ship.AddressId = currentData.AddressId
                $scope.Ship.ShipToLocation = currentData.ShipToLocation;
                $scope.Ship.ShipToName = currentData.ShipToName;
                $scope.Ship.SelectedRelatedTerritoriesId = currentData.TerritoriesId;
                $scope.Ship.CreatedOn = currentData.CreatedOn;
                $scope.Ship.CreatedBy = currentData.CreatedBy;
                $scope.Ship.Active = currentData.Active;
                $scope.Ship.LastUpdatedBy = currentData.LastUpdatedBy;
                $scope.Ship.LastUpdatedOn = currentData.LastUpdatedOn;
                $scope.Address.Address1 = currentData.Address1;
                $scope.$broadcast('angucomplete-alt:changeInput', 'autocomp_', $scope.Address.Address1);
                $scope.Address.AddressId = currentData.AddressId;
                $scope.Address.Address2 = currentData.Address2;
                $scope.Address.City = currentData.City;
                $scope.Address.ZipCode = currentData.ZipCode;
                $scope.Address.State = currentData.State;
                $scope.Address.CountryCode2Digits = currentData.Country;
                $scope.Address.CreatedBy = currentData.CreatedBy;
                $scope.Address.CreatedOn = currentData.CreatedOn;
                $scope.Address.LastUpdatedBy = currentData.LastUpdatedBy;
                $scope.Address.LastUpdatedOn = currentData.LastUpdatedOn;
                $scope.Address.IsValidated = currentData.IsValidated;
                $scope.Ship.PhoneNumber = currentData.PhoneNumber;

                $scope.InitializeDropdown();

                $scope.Show('shiplocation');
            }
            $scope.ActivateShipping = function(e)
            {
                $scope.NullifyObject();
                var currentData = e.dataItem;
                $scope.Ship.Id = currentData.Id;
                $scope.Ship.Active = true;
                $("#Activeshiplocation").modal("show");
            }
            $scope.DeActivateShipping = function (e) {
                $scope.NullifyObject();
                var currentData = e.dataItem;
                $scope.Ship.Id = currentData.Id;
                $scope.Ship.Active = false;
                $("#DeActiveshiplocation").modal("show");
            }
            $scope.Show = function (modalId) {

                $("#" + modalId).modal("show");

            }
            $scope.Cancel = function (modalId) {
                $scope.NullifyObject();
                //$scope.SelectedRelatedTerritoriesId = [];
                $("#" + modalId).modal("hide");
                var myDiv = $('.modal-body');
                myDiv[0].scrollTop = 0;
            };
        //Get the Ship To Location Type
            $scope.InitializeDropdown = function () {

                $http.get('/api/lookup/0/Type_ShipToLocation').then(function (data) {
                    $scope.shipTolocationList = data.data;
                });
                $scope.RelatedTerritory = {
                    placeholder: "Select",
                    dataTextField: "Name",
                    dataValueField: "Id",
                    filter: "contains",
                    valuePrimitive: true,
                    autoBind: true,
                    dataSource: {
                        transport: {
                            read: {
                                url: "/api/lookup/0/Territory",
                            }
                        }
                    },
                };

            }
            $scope.NullifyObject = function () {
                $scope.Address =
              {
                  IsResidential: false,
                  IsCommercial: false,
                  AddressType: 'false',
                  AttentionText: '',
                  AddressId:'',
                  Address1: '',
                  Address2: '',
                  City: '',
                  Country: 'US',
                  State: '',
                  ZipCode: '',
                  CountryCode2Digits: HFCService.FranchiseCountryCode,
                  CrossStreet: '',
                  Location: '',
                  CreatedOn: '',
                  CreatedBy: '',
                  LastUpdatedOn: '',
                  LastUpdatedBy: ''

              };
                $scope.Ship =
                  {
                      Active:'',
                      Id:0,
                      ShipToLocation: '',
                      ShipToName: null,
                      //TerritoriesId: '',
                      CreatedOn: '',
                      CreatedBy: '',
                      LastUpdatedOn: '',
                      LastUpdatedBy:'',
                      SelectedRelatedTerritoriesId: [],
                      PhoneNumber:'',
                  };
                setAddress1Value();

            }
            $scope.NullifyObject();
            function setAddress1Value() {
                    // We need to initialize to empty string so that the previous value will
                    // not be used the special autofill control.
                    $rootScope.$broadcast('angucomplete-alt:clearInput', 'autocomp_');
                    return;
            }


        //Save the Details
            $scope.SaveShippingLocation = function (modalId) {
                $scope.IsDuplicateCheck = false;

                if (modalId == 'Activeshiplocation' || modalId == 'DeActiveshiplocation') {
                    $("#" + modalId).modal("hide");
                    var dto = $scope.Ship;

                    $http.post('/api/Shipping/0/ActivateDeactivateShip', dto).then(function (response) {
                        var status = response;
                        $scope.NullifyObject();
                        //$interval(function () { $scope.GetShipToLocation() }, 5000);
                        $("#gridShipLocation").data("kendoGrid").dataSource.read();
                        var myDiv = $('.modal-body');
                        myDiv[0].scrollTop = 0;
                    });
                }

                else {
                    $scope.submitted = true;
                    $scope.form_ship.formAddressValidation.zipCode.$commitViewValue(true);

                    // Check whether the forms contain value for all the required fields.
                    // when any one of the filed is not given a value the following will return true.
                    if ($scope.form_ship.$invalid) {
                        // HFC.DisplaySuccess("Please fill all the required fields! and valid data in optional fields if provided.");
                        return;
                    }

                    var value = $scope.Ship.ShipToName;

                    $http.get('/api/Shipping/' + $scope.Ship.Id + '/DuplicateShippingName/?value=' + value).then(function (response) {
                        var Status = response.data.status;
                        $scope.IsDuplicateCheck = true;
                        if (Status) {
                            HFC.DisplayAlert("This Ship Name is Already Exist!");
                            return;
                        }
                        else
                        {
                            var dto = $scope.Ship;
                            $scope.Addresses = {};
                            dto.Addresses = $scope.Address;

                            saveAddressHandler(dto, modalId);

                            //$http.post('/api/Shipping/0/SaveShipToLocation', dto).then(function (response) {
                            //    var status = response;
                            //    $("#" + modalId).modal("hide");
                            //    $("#gridShipLocation").data("kendoGrid").dataSource.read();
                            //});
                        }
                    });




                }
            }

            function saveAddressHandler(dto, modalId) {
                validateAddress(dto.Addresses)
                    .then(function (response) {
                        var objaddress = response.data;
                        if (objaddress.Status == "error") {
                            //TP-1793: CLONE - The request failed with HTTP status 503: Service Temporarily Unavailable.
                            AfterValidateAddress(dto, modalId);
                            //HFC.DisplayAlert(objaddress.Message);
                            //$scope.IsBusy = false;
                        } else if (objaddress.Status == "true") {
                            var validAddress = objaddress.AddressResults[0];
                            dto.Addresses.Address1 = validAddress.address1;
                            $scope.Addressvalues = dto.Addresses.Address1;
                            dto.Addresses.Address2 = validAddress.address2;
                            dto.Addresses.City = validAddress.City;
                            dto.Addresses.State = validAddress.StateOrProvinceCode;

                            // Update that the address validated to true.
                            dto.Addresses.IsValidated = true;

                            // assign the zipcode back to the model before saving.
                            if (dto.CountryCode2Digits == "US") {
                                dto.Addresses.ZipCode = validAddress.PostalCode.substring(0, 5);
                            }
                            else {
                                dto.Addresses.ZipCode = validAddress.PostalCode;
                            }
                            $scope.$broadcast('angucomplete-alt:changeInput', 'autocomp_', $scope.Addressvalues);
                            AfterValidateAddress(dto, modalId);
                        } else if (objaddress.Status == "false") {

                            dto.Addresses.IsValidated = false;

                            var errorMessage = "Invalid Address. Do you wish to continue?";

                            $scope.Addressvalues = dto.Addresses.Address1;
                            //address get empty so assiging the value here.
                            $scope.$broadcast('angucomplete-alt:changeInput', 'autocomp_', $scope.Addressvalues);

                            if (confirm(errorMessage)) {
                                AfterValidateAddress(dto, modalId);
                            }
                            else {
                                $scope.IsBusy = false;
                            }
                        } else { //SkipValidation
                            AfterValidateAddress(dto, modalId);
                        }
                    },
                    function (response) {
                        HFC.DisplayAlert(response.statusText);
                        $scope.IsBusy = false;
                    });
            };


            function validateAddress(obj) {
                var addressModeltp = {
                    address1: obj.Address1,
                    address2: obj.Address2,
                    City: obj.City,
                    StateOrProvinceCode: obj.State,
                    PostalCode: obj.ZipCode,
                    CountryCode: obj.CountryCode2Digits
                };

                if (!addressModeltp.address1 || addressModeltp.address1 == "") {
                    var result = {
                        data: {
                            Status: "SkipValidation",
                            Message: "No Address validation required."
                        },
                        status: 200,
                        statusText: "OK"
                    }

                    return $q.resolve(result);;
                }

                // Call the api to Return the result.
                return AddressValidationService.validateAddress(addressModeltp);
            }

            function AfterValidateAddress(dto, modalId) {

                //code for spacing in Canada Postal/ZipCodes
                if (!dto.Addresses.IsValidated) {
                    if (dto.Addresses.CountryCode2Digits == "CA") {
                        if (dto.Addresses.ZipCode) {
                            var Ca_Zipcode = dto.Addresses.ZipCode;
                            Ca_Zipcode = Ca_Zipcode.substr(0, 3) + " " + Ca_Zipcode.substr(3);
                            dto.Addresses.ZipCode = Ca_Zipcode;
                        }
                    }
                }

                $http.post('/api/Shipping/0/SaveShipToLocation', dto).then(function (response) {
                    var status = response;
                    $scope.NullifyObject();
                    $("#" + modalId).modal("hide");
                    $("#gridShipLocation").data("kendoGrid").dataSource.read();
                    var myDiv = $('.modal-body');
                    myDiv[0].scrollTop = 0;
                });
            }


            //$scope.tooltipOptions = {
            //    filter: "td,th",
            //    position: "top",
            //    hide: function (e) {
            //        this.content.parent().css('visibility', 'hidden');
            //    },
            //    show: function (e) {
            //        if (this.content.text().length > 1) {
            //            if (this.content.text().trim() === "Edit QualifyAppointment Print") {
            //                this.content.parent().css('visibility', 'hidden');
            //            } else {
            //                this.content.parent().css('visibility', 'visible');
            //            }

            //        }
            //        else {
            //            this.content.parent().css('visibility', 'hidden');
            //        }
            //    },
            //    content: function (e) {

            //        return e.target.context.textContent;
            //    }
            //};


        //Address get method
            $http({ method: 'GET', url: '/api/address/', params: { leadIds: 0, includeStates: true } })
                .then(function (response) {
          $scope.AddressService.States = response.data.States || [];
          $scope.AddressService.Countries = response.data.Countries || [];
          $scope.AddressService.FranchiseAddress = response.data.FranchiseAddress;
      }, function (response) {
      });


        //Search BB Grid
            $scope.ShipgridSearchEdit = function () {
                var searchValue = $('#searchBox').val();

                $("#gridShipLocation").data("kendoGrid").dataSource.filter({

                    logic: "or",
                    filters: [
                        {
                            field: "ShipToLocationName",
                            operator: "contains",
                            value: searchValue
                        },
                        {
                            field: "ShipToName",
                            operator: "contains",
                            value: searchValue
                        },
                        {
                            field: "Address1",
                            operator: "contains",
                            value: searchValue
                        },
                        {
                            field: "Address2",
                            operator: "contains",
                            value: searchValue

                        },

                        {
                            field: "State",
                            operator: "contains",
                            value: searchValue
                        },
                         {
                             field: "ZipCode",
                             operator: "contains",
                            value: searchValue
                         },
                         {
                             field: "Country",
                             operator: "contains",
                             value: searchValue
                         },



                    ]
                });


            };

        //Clear Search and Filters
            $scope.ShipgridSearchClear = function () {
                $('#searchBox').val('');
                $("#gridShipLocation").data('kendoGrid').dataSource.filter({
                });
            };

    }
]);

