﻿'use strict';

jQuery.scrollTo = function (target, offset, speed, container) {
    if (isNaN(target)) {
        if (!(target instanceof jQuery))
            target = $(target);

        target = parseInt(target.offset().top);
    }

    container = container || "html, body";
    if (!(container instanceof jQuery))
        container = $(container);

    speed = speed || 500;
    offset = offset || 0;

    container.animate({
        scrollTop: target + offset
    }, speed);
};

app.controller('OrderController', ['$scope', '$routeParams', '$rootScope', '$window', '$location', '$http',
    'HFCService', 'LeadSourceService', 'JobService', 'AddressService', 'PersonService',
    'JobNoteService', 'InvoiceService', 'PurchaseOrderFilter', 'PaymentService', 'AdvancedEmailService',
    'AttendeeService', 'OrderService', 'NewtaskService', 'NavbarService', 'EmailService', 'PrintService', 'CalendarService', 'NoteServiceTP', 'kendoService', 'AdditionalAddressServiceTP', '$filter', '$anchorScroll',
    function ($scope, $routeParams, $rootScope, $window, $location, $http
        , HFCService, LeadSourceService, JobService, AddressService, PersonService,
        JobNoteService, PurchaseOrderFilter, PaymentService, AdvancedEmailService,
        LeadOrderService, AttendeeService, OrderService, NewtaskService, NavbarService, EmailService, PrintService, CalendarService, NoteServiceTP, kendoService, AdditionalAddressServiceTP, $filter, $anchorScroll
    ) {
        //  we will use local ctrl var so we don't clutter up $scope that doesn't need to be shared with nested controllers and directives
        var ctrl = this;
        var OrderId = $routeParams.OrderId;
        var jobId = parseInt($routeParams.jobId) || 0;
        $scope.selectedIds = [];
        $scope.OrderStatuseslist = null;
        $scope.accountlist = null;
        $scope.Order = null;
        $scope.franchiseAddressObject = null;
        $scope.isSubmitted = false;
        $scope.SubmitEmail = false;
        $scope.QuoteNoteLineModel = false;

        $scope.showeditbtn = false;
        $scope.viewCDate = true;
        $scope.BindCursor = true;

        // tooltip purpose
        $scope.tooltipFlag = false;

        $scope.NavbarService = NavbarService;

        $scope.NavbarService.SelectSales();
        $scope.NavbarService.EnableSalesOrdersTab();

        $scope.AdditionalAddressServiceTP = AdditionalAddressServiceTP;
        $scope.brandid = HFCService.CurrentBrand;

        $scope.AdvancedEmailService = AdvancedEmailService;
        $scope.JobNoteService = JobNoteService;
        $scope.JobService = JobService;
        $scope.LeadSourceService = LeadSourceService;
        $scope.AttendeeService = AttendeeService;
        $scope.OrderStuseslist = [];
        $scope.OrderSourcesList = [];
        $scope.OrderCampaignslist = [];
        $scope.HFCService = HFCService;
        $scope.PersonService = PersonService;
        $scope.AddressService = AddressService;
        $scope.LeadSourceService = LeadSourceService;
        $scope.PaymentService = PaymentService;
        $scope.OrderService = OrderService;
        $scope.NewtaskService = NewtaskService;
        $scope.purchaseOrders = [];
        $scope.orderStatuses = [];
        $scope.filter = ctrl.filter = PurchaseOrderFilter;
        $scope.OrderPopAddress = [];

        $scope.PrintService = PrintService;
        $scope.EmailService = EmailService;

        $scope.CalendarService = CalendarService;
        // $anchorScroll();
        // for kendo tooltip
        $scope.kendotooltipOptions = kendoService.kendoToolTipOptions;
        kendoService.customTooltip = null;

        // Notes Integration.
        $scope.NoteServiceTP = NoteServiceTP;
        $scope.NoteServiceTP.OrderId = $routeParams.orderId;
        $scope.NoteServiceTP.ParentName = "Order";
        $scope.ShowCreateCaseIcon = false;

        $scope.BindCursor = true;

        // Fix for TP-1588	Attachments - duplicating
        $scope.NoteServiceTP.FranchiseSettings = false;

        //enable case
        $scope.FranciseLevelCase = $scope.HFCService.FranciseLevelCase;

        // tooltip emulator
        $scope.showTooltip = function (flag) {
            $scope.tooltipFlag = flag;
        };
        //contract date
        $scope.FirstTimeCdView = false;
        //disable create case for service
        $scope.IsDiscountCheck = false;

        //amount format
        $scope.CostCurrencyControl = {};

        //flag for date validation
        $scope.ValidDate = true;
        //permission for customer view
        $scope.AdminElectronicSign = $scope.HFCService.AdminElectronicSign;
        $scope.FranciseLevelElectronicSign = $scope.HFCService.FranciseLevelElectronicSign;
        $scope.CVD = [];    //Customer View Data
        $scope.isCustomerView = false;

        if (window.location.href.includes("customerView"))
            $scope.isCustomerView = true;

        if ($routeParams.orderId > 0 && !$scope.isCustomerView) {
            $http.get('/api/Orders/' + $routeParams.orderId + '/getPhoneEmailOrder')
                .then(function (response) {
                    $scope.PrimaryEmailPhone = response.data;

                    //self.Opportunity = opportunity;
                });
        }

        ///////////

        var SalesOrderpermission = [];
        var BuyerRemorse = false;
        var CustomerView = false;
        var BuyerRemorseOverride = false;
        var Quotepermission = [];

        $scope.PermissionEditOrder = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'SalesOrder').CanUpdate;

        var promise1 = new Promise(function (resolve, reject) {
            resolve(HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'SalesOrder'));
        });

        promise1.then(function (value) {
            SalesOrderpermission = value;
            CustomerView = SalesOrderpermission.SpecialPermission.find(x => x.PermissionCode == 'CustomerView').CanAccess;
            BuyerRemorse = SalesOrderpermission.SpecialPermission.find(x => x.PermissionCode == 'BuyerRemorseConvert').CanAccess;
            BuyerRemorseOverride = SalesOrderpermission.SpecialPermission.find(x => x.PermissionCode == 'BuyerRemorseOverride').CanAccess;
            Quotepermission = $scope.HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'Quote')
        });

        var promise2 = new Promise(function (resolve, reject) {
            resolve($scope.HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'Payment'));
        });

        promise2.then(function (value) {
            var Paymentpermission = value;
            var Reversepaypermission = Paymentpermission.SpecialPermission.find(x => x.PermissionCode == 'ReversePayment ').CanAccess;
            var ConvertToMpopermission = SalesOrderpermission.SpecialPermission.find(x => x.PermissionCode == 'ConvertToMpo').CanAccess;
            var MarginReviewSummarypermission = Quotepermission.SpecialPermission.find(x => x.PermissionCode == 'MarginReviewSummary ').CanAccess;
            //$scope.NoteServiceTP.showAddNotes = SalesOrderpermission.CanRead; //$scope.HFCService.GetPermissionTP('Add Notes&Attachments-Order').CanAccess;
            //$scope.NoteServiceTP.showEditNotes = SalesOrderpermission.CanUpdate; //$scope.HFCService.GetPermissionTP('Edit Notes&Attachments-Order').CanAccess;

            $scope.Permission = {};
            $scope.Permission.AddPayment = Paymentpermission.CanCreate; //$scope.HFCService.GetPermissionTP('Add Payment').CanAccess;
            $scope.Permission.ReversePayment = Reversepaypermission; //$scope.HFCService.GetPermissionTP('Reverse Payment').CanAccess;
            $scope.Permission.ViewOrder = SalesOrderpermission.CanRead; //$scope.HFCService.GetPermissionTP('Display Order').CanAccess;
            $scope.Permission.EditOrder = SalesOrderpermission.CanUpdate; //$scope.HFCService.GetPermissionTP('Edit Order').CanAccess;
            $scope.Permission.ConvertOrder = ConvertToMpopermission; //$scope.HFCService.GetPermissionTP('Convert SO to MPO-Order').CanAccess;
            $scope.Permission.ConvertBuyerRemorse = BuyerRemorse; //$scope.HFCService.GetPermissionTP('Buyer Remorse Convert').CanAccess;
            $scope.Permission.ConvertBuyerOverride = BuyerRemorseOverride;
            $scope.Permission.MarginReview = MarginReviewSummarypermission;
            $scope.Permission.ViewCustomer = CustomerView;
        });

        // TP-2959: CLONE - Suggestion: link to navigate from Order to MPO
        var procurementPermission = $scope.HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'ProcurementDashboard')
        $scope.PermissionMpo = {};
        $scope.PermissionMpo.CanReadMpoPage = false;
        if (procurementPermission) $scope.PermissionMpo.CanReadMpoPage = procurementPermission.CanRead;

        //var SalesOrderpermission = HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'SalesOrder')
        //var BuyerRemorse = SalesOrderpermission.SpecialPermission.find(x=>x.tooltipFlag == 'BuyerRemorseConvert').CanAccess;
        //var BuyerRemorseOverride = SalesOrderpermission.SpecialPermission.find(x=>x.PermissionCode == 'BuyerRemorseOverride').CanAccess;

        //var Paymentpermission = $scope.HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Payment')
        //var Reversepaypermission = Paymentpermission.SpecialPermission.find(x=>x.PermissionCode == 'ReversePayment ').CanAccess;
        //var ConvertToMpopermission = SalesOrderpermission.SpecialPermission.find(x=>x.PermissionCode == 'ConvertToMpo').CanAccess;

        //$scope.NoteServiceTP.showAddNotes = SalesOrderpermission.CanRead; //$scope.HFCService.GetPermissionTP('Add Notes&Attachments-Order').CanAccess;
        //$scope.NoteServiceTP.showEditNotes = SalesOrderpermission.CanUpdate; //$scope.HFCService.GetPermissionTP('Edit Notes&Attachments-Order').CanAccess;

        //$scope.Permission = {};
        //$scope.Permission.AddPayment = Paymentpermission.CanCreate; //$scope.HFCService.GetPermissionTP('Add Payment').CanAccess;
        //$scope.Permission.ReversePayment = Reversepaypermission; //$scope.HFCService.GetPermissionTP('Reverse Payment').CanAccess;
        //$scope.Permission.ViewOrder = SalesOrderpermission.CanRead; //$scope.HFCService.GetPermissionTP('Display Order').CanAccess;
        //$scope.Permission.EditOrder = SalesOrderpermission.CanUpdate; //$scope.HFCService.GetPermissionTP('Edit Order').CanAccess;
        //$scope.Permission.ConvertOrder = ConvertToMpopermission; //$scope.HFCService.GetPermissionTP('Convert SO to MPO-Order').CanAccess;
        //$scope.Permission.ConvertBuyerRemorse = BuyerRemorse; //$scope.HFCService.GetPermissionTP('Buyer Remorse Convert').CanAccess;
        //$scope.Permission.ConvertBuyerOverride = BuyerRemorseOverride; //$scope.HFCService.GetPermissionTP('Buyer Remorse Override').CanAccess;

        //For Appointment
        HFCService.GetUrl();
        // $anchorScroll();

        //to do
        //$scope.Permission.ConvertOrder = $scope.HFCService.GetPermissionTP('Convert Order').CanAccess;
        //$scope.Permission.ListPaymentOrder = $scope.HFCService.GetPermissionTP('List Payments').CanAccess;
        //$scope.Permission.AddPaymentOrder = $scope.HFCService.GetPermissionTP('Add Payment').CanAccess;
        //$scope.Permission.AddInvoiceOrder = $scope.HFCService.GetPermissionTP('Add Invoice').CanAccess;
        //$scope.Permission.ListNotesOrder = $scope.HFCService.GetPermissionTP('List Notes').CanAccess;
        //$scope.Permission.AddNotesOrder = $scope.HFCService.GetPermissionTP('Add Notes').CanAccess;
        //$scope.Permission.EmailInvoiceOrder = $scope.HFCService.GetPermissionTP('Email Invoice').CanAccess;
        //$scope.Permission.DownloadInvoiceOrder = $scope.HFCService.GetPermissionTP('Download Inovice').CanAccess;

        //******************* init Order data

        if ($routeParams.orderId > 0 && !$scope.isCustomerView) {
            $http.get('/api/Opportunities/' + $routeParams.orderId + '/getPhoneEmailforOrder')
                .then(function (response) {
                    $scope.emailPhone = response.data;

                    //angular.forEach($scope.emailPhone, function (value, key) {
                    //    if (value.PrimaryEmail)
                    //        value.PrimaryEmail = 'Email: ' + value.PrimaryEmail;

                    //    if (value.AccountPhone)
                    //        value.AccountPhone = 'Phone: ' + value.AccountPhone;

                    //})

                    //  $scope.opportunityvalue = response.data.Opportunity;

                    //self.Opportunity = opportunity;
                });
        }

        if ($routeParams.orderId > 0 && !$scope.isCustomerView) {
            $http.get('/api/CaseManageAddEdit/' + $routeParams.orderId + '/getMPOforOrder').then(function (success) {
                if (success.data) {
                    $scope.PurchaseOrderIdToCase = success.data;
                    $scope.ShowCreateCaseIcon = true;
                }
                else
                    $scope.ShowCreateCaseIcon = false;
            });
        }

        $scope.enableUpdateContractDate = function () {
            $scope.viewCDate = false;
            $scope.ValidDate = true;
        }

        $scope.SaveContractedDate = function () {
            var value = document.getElementById("ContractedDate").value;

            if (!Date.parse(value)) {
                HFC.DisplayAlert("Invalid Date!");
            }
            else {
                var dateee = new Date(value);
                var cuDate = new Date();
                var PreviousMonthDate = new Date(cuDate.setMonth(cuDate.getMonth() - 1));
                PreviousMonthDate = PreviousMonthDate.setDate(9);
                var opprec = ($scope.Order.OpportunityReceivedDate).substr(0, (($scope.Order.OpportunityReceivedDate).length - 6))
                var oppCre = new Date(opprec);
                var rr = $filter('date')(opprec, HFCService.StaticKendoDateFormat());
                if (dateee > new Date()) {
                    HFC.DisplayAlert("Contracted date should not exceed the current date");
                }
                else if (dateee < oppCre.setDate(oppCre.getDate() - 1) || dateee < PreviousMonthDate) {
                    if (new Date(rr) < new Date($filter('date')((new Date(new Date().setMonth(new Date().getMonth() - 1))).setDate(10), HFCService.StaticKendoDateFormat())))
                        HFC.DisplayAlert("Contracted date should not be before " + $filter('date')((new Date(new Date().setMonth(new Date().getMonth() - 1))).setDate(10), HFCService.StaticKendoDateFormat()));
                    else
                        HFC.DisplayAlert("Contracted date should not be before the opportunity received date - " + rr);
                }
                else if (dateee <= oppCre.setDate(oppCre.getDate()) && dateee >= PreviousMonthDate) {
                    HFC.DisplayAlert("Contracted date should not be before the opportunity received date - " + rr);
                }
                //else if (dateee < PreviousMonthDate)
                //{
                //    HFC.DisplayAlert("Contracted date should not before the 10th of the previous month");
                //}

                else {
                    var datee = document.getElementById("ContractedDate").value;
                    $scope.Order.ContractedDate = datee;
                    $http.post('/api/orders/' + $routeParams.orderId + '/updateContractedDate?date=' + $scope.Order.ContractedDate + '&quotekey=' + $scope.Order.QuoteKey).then(function (res) {
                        if (res.data != null) {
                            $scope.FirstTimeCdView = true;
                            $scope.Order.ContractedDateUpdatedBy = res.data.ContractedDateUpdatedBy;
                            $scope.Order.ContractedDateUpdatedBy_Name = res.data.ContractedDateUpdatedBy_Name;
                            $scope.Order.ContractedDateUpdatedOn = res.data.ContractedDateUpdatedOn;
                            $scope.Order.PreviousContractedDate = res.data.PreviousContractedDate;
                            $scope.Order.ContractedDate = res.data.ContractedDate;
                            $scope.NewContractedDate = res.data.ContractedDate;
                        }
                    });
                    $scope.viewCDate = true;
                }
            }
            $("#ContractedDateWarning").modal("hide");
        }

        $scope.UpdateContractDate = function () {
            //restrict saving for invalid date
            if (!$scope.ValidDate) {
                HFC.DisplayAlert("Invalid Date!");
                return;
            }
            $("#ContractedDateWarning").modal("show");
        }

        $scope.CancelPopup = function () {
            $("#ContractedDateWarning").modal("hide");
            $scope.viewCDate = true;

            $scope.NewContractedDate = $scope.Order.ContractedDate;
            //  document.getElementById("ContractedDate").value = $scope.Order.ContractedDate;
        }

        $scope.onMouseDownOrder = function (action) {
            $scope.submitAction = action;
            $scope.SaveOrder();
        }
        $scope.setOrderAction = function (action) {
            $scope.submitAction = action;
        }
        $scope.onOrderMouseDown = function (action) {
            $scope.submitAction = action;
            $scope.SaveOrder();
        }
        $scope.returnToOrderList = function () {
            window.location = "#!/orderSearch/";
        }
        $scope.CreateNewOrder = function () {
            window.location = "#!/Orders";
        }
        $scope.AddOrder = function () {
            $scope.Order = null;
            window.location = '#!/Orders';
        }
        $scope.CancelOrder = function () {
            // disabled for keep data in form when user doesn't want to redirect
            // $scope.Order = null;
            window.location.href = '#!/Orders/' + $routeParams.orderId;
        }

        $scope.showPopUp = function (modalId) {
            $("#" + modalId).modal("show");
        }
        $scope.Cancel = function (modalId) {
            $("#" + modalId).modal("hide");
        };

        $scope.AddOrEditNotesAttachment = function (modalID) {
            $("#" + modalID).modal("show");
        }

        $scope.GetSalesAgent = function () {
            $http.get('/api/orders/0/GetSalesAgents').then(function (data) {
                $scope.SalesAgentlist = data.data;
            });
        }

        $scope.GetInstallationAddress = function () {
            $http.get('/api/orders/0/InstallationAddresses').then(function (data) {
                $scope.InstallationAddresslist = data.data;
            });
        }

        $scope.GetOrderStatus = function () {
            $http.get('/api/orders/0/OrderStatus').then(function (data) {
                $scope.OrderStatuseslist = data.data;


            });
        }
        //added to get reverse payment type-methods in pop-up
        $scope.GetOrderPaymentMethods = function () {
            $http.get('/api/orders/0/OrderPaymentMethods').then(function (data) {
                $scope.OrderPaymentMethods = data.data;
            });
        }

        $scope.ResetInstallationAddress = function (Account) {
            var accountId = Account.accountId
            $http.get('/api/orders/' + accountId + '/InstallationAddresses').then(function (data) {
                $scope.InstallationAddresslist = data.data;
            });
        }
        $scope.ResetInstallationAddressEdit = function (AccountId) {
            $http.get('/api/orders/' + AccountId + '/InstallationAddresses').then(function (data) {
                $scope.InstallationAddresslist = data.data;
            });
        }

        //$scope.GetSalesAgent();
        //$scope.GetInstallationAddress();
        if (!$scope.isCustomerView) {
            $scope.GetOrderStatus();
            $scope.GetOrderPaymentMethods();
        }

        $scope.Order = {};

        $scope.findBySpecField = function (data, reqField, value, resField) {
            if (data == null) {
                return;
            }
            var container = data;
            for (var i = 0; i < container.length; i++) {
                if (container[i][Object.keys(container[i])[0]] == value) {
                    return (container[i]);
                }
            }
            return '';
        }

        $scope.getAccountName = function (Accountid) {
            $http.get('/api/orders/' + Accountid + '/getAccountName').then(function (data) {
                $scope.SelectedAccount = data.data.Name;
            });
        }
        $scope.getSalesAgentName = function (SalesAgentId) {
            $http.get('/api/orders/' + SalesAgentId + '/getSalesAgentName').then(function (data) {
                $scope.SelectedAgent = data.data.Name;
            });
        }
        $scope.getInstalltionAddressName = function (Id) {
            if (Id != undefined && Id != null) {
                $http.get('/api/orders/' + Id + '/getInstalltionAddressName').then(function (data) {
                    $scope.SelectedInstallationAddress = data.data.Name;
                });
            }
        }
        if (OrderId == undefined || OrderId == null || OrderId == "") {
            $scope.Order = null;

            $scope.OrderStatusID = 0;
            $scope.SalesAgentId = 0;
            $scope.InstallerId = 0;
            $scope.InstallationAddressId = 0;
        }
        if (document.location.href.toString().indexOf("/Orders/") > 0) {
            $scope.isDisabled = true;
        }

        $scope.modalState = {
            loaded: true
        }

        $scope.$watch('modalState.loaded', function () {
            var loadingElement = document.getElementById("loading");

            if (!$scope.modalState.loaded) {
                loadingElement.style.display = "block";
            } else {
                loadingElement.style.display = "none";
            }
        });

        if ($routeParams.orderId > 0 && !$scope.isCustomerView) {
            // Inform that the modal is not yet ready
            $scope.modalState.loaded = false;
            //
            $http.get('/api/Orders/' + $routeParams.orderId)
                .then(function (orderServiceData) {
                    $scope.Order = orderServiceData.data;

                    if (!$scope.Order.ContractedDateUpdatedBy_Name)
                        $scope.showeditbtn = true;

                    $scope.NewContractedDate = $scope.Order.ContractedDate;

                    if ($scope.Order.PrimaryEmail)
                        $scope.OrderPrimaryemail = "Email: " + $scope.Order.PrimaryEmail;
                    else
                        $scope.OrderPrimaryemail = '';

                    if ($scope.Order.AccountPhone)
                        $scope.OrderAccountPhone = "Phone: " + $scope.Order.AccountPhone;
                    else
                        $scope.OrderAccountPhone = '';

                    //if ($scope.OrderAccountPhone != '' && $scope.OrderPrimaryemail)
                    //    $scope.tooltipOpportunity = true;
                    //else
                    //    $scope.tooltipOpportunity = false;

                    // for header notes
                    $scope.Quotation = {};
                    $scope.Quotation.OrderNumber = $scope.Order.OrderNumber;// view
                    $scope.Quotation.OrderID = $scope.Order.OrderID;

                    $scope.Quotation.QuoteID = $scope.Order.Quote.QuoteID;
                    $scope.Quotation.QuoteKey = $scope.Order.QuoteKey;
                    $scope.Quotation.QuoteName = $scope.Order.Quote.QuoteName;
                    $scope.Quotation.OpportunityId = $scope.Order.OpportunityId;

                    $scope.Quotation.SideMark = $scope.Order.SideMark;
                    $scope.Quotation.OpportunityName = $scope.Order.Opportunity;

                    $scope.Quotation.QuoteLevelNotes = $scope.Order.Quote.QuoteLevelNotes;
                    $scope.Quotation.OrderInvoiceLevelNotes = $scope.Order.Quote.OrderInvoiceLevelNotes;
                    $scope.Quotation.InstallerInvoiceNotes = $scope.Order.Quote.InstallerInvoiceNotes;
                    $scope.Quotation.IsOrderpage = true;

                    $rootScope.HearderNoteQuoteKey = $scope.Order.QuoteKey;
                    // var gg = $scope.Quotation;
                    $scope.tempquotation = angular.copy($scope.Quotation);

                    //$scope.Quotation.QuoteID = '';
                    //$scope.Quotation.QuoteName = '';
                    //$scope.Quotation.OpportunityId = '';
                    //$scope.Quotation.SideMark = '';
                    //$scope.Quotation.OpportunityName = '';
                    //$scope.Quotation.OrderNumber = '';
                    //$scope.Quotation.OrderID = '';

                    // end of header notes

                    $scope.TaxDetails();
                    $scope.getQuotelines(orderServiceData.data.QuoteLinesVM);

                    if ($scope.Order != null) {
                        $scope.PreviousOrderStatusId = $scope.Order.OrderStatus;
                    }
                    //Initialize the NoteServiceTP, so that it will get the necessary data from the DB.
                    $scope.NoteServiceTP.Initialize();

                    if (document.location.href.toString().indexOf("/Orders/") > 0) {
                        $scope.GetOrderLines($scope.Order.OrderLines)
                    }
                    if (!window.location.href.includes("customerView"))
                        HFCService.setHeaderTitle("Order #" + $scope.Order.OrderNumber + " - " + $scope.Order.OrderName);
                    // Inform that the modal is ready to consume:
                    $scope.modalState.loaded = true;

                    var panelBar = $("#panelbar1").data("kendoPanelBar");
                    if (panelBar !== undefined && panelBar !== null) {
                        panelBar.expand($("#panelOpportunity"));
                    }


                }, function (response) {
                    // Inform that the modal is ready to consume:
                    $scope.modalState.loaded = true;

                    HFC.DisplayAlert(response.statusText);
                });
        }
        $scope.OrderStatusManagement = function (oldstatusid, NewStatusId) {
            $scope.IsEditable = false;
            //if (oldstatusid == 1) {
            //    if (NewStatusId == 6) {
            //        $scope.IsEditable = true;
            //    }

            //}
            if (oldstatusid == 2) {
                if (NewStatusId == 1) {
                    $scope.IsEditable = true;
                }
            }
            if (oldstatusid == 3) {
                if (NewStatusId == 2) {
                    $scope.IsEditable = true;
                }
            }
            if (oldstatusid == 4) {
                if (NewStatusId == 2 || NewStatusId == 3) {
                    $scope.IsEditable = true;
                }
            }
            if (oldstatusid == 5) {
                if (NewStatusId == 1 || NewStatusId == 2 || NewStatusId == 3 || NewStatusId == 4) {
                    $scope.IsEditable = true;
                }
            }
            if (oldstatusid == 6) {
                if (NewStatusId == 1 || NewStatusId == 2 || NewStatusId == 3 || NewStatusId == 4 || NewStatusId == 5 || NewStatusId == 9) {
                    $scope.IsEditable = true;
                }
            }
            if (oldstatusid == 9) {
                if (NewStatusId == 1 || NewStatusId == 2 || NewStatusId == 3 || NewStatusId == 4 || NewStatusId == 5 || NewStatusId == 6) {
                    $scope.IsEditable = true;
                }
            }
            return $scope.IsEditable;
        }
        $scope.SaveOrder = function (operation) {
            $scope.submitted = true;
            if ($scope.Order.IsTaxExempt == false || ($scope.Order.IsTaxExempt == true && $scope.Order.TaxExemptID != '' && $scope.Order.TaxExemptID != null && $scope.Order.TaxExemptID != undefined)) {
                $scope.validate = 0;
                if ($scope.form_order.$invalid) {
                    // HFC.DisplaySuccess("Please fill all the required fields! and valid data in optional fields if provided.");

                    return;
                }
                $scope.IsValidStatus = false;
                $scope.submitAction = operation;

                var modalId = 'error';

                var dto = $scope.Order;

                if (dto == null || dto == undefined) {
                    $("#" + modalId).modal("show");
                    return;
                }
                if (dto.OrderName.length == 0) {
                    $("#" + modalId).modal("show");
                    return;
                }

                if (dto.OrderStatus == 0 || dto.OrderStatus == null || dto.OrderStatus == undefined) {
                    HFC.DisplayAlert("Order Status Required");
                    $("#" + modalId).modal("show");
                    return;
                }

                if ($scope.OrderStatusManagement($scope.PreviousOrderStatusId, dto.OrderStatus)) {
                    HFC.DisplayAlert("Order status is invalid");
                    //$("#" + modalId).modal("show");
                    return;
                }

                if (dto.OrderStatus == 5 && (dto.IsNotifyemails == true || dto.IsNotifyText == true)) {
                    if ($scope.Order.SendReviewEmail == false) {
                        $("#SendThanks_EmailReview").modal("show");
                        return;
                    }
                    //else {
                    //    $scope.SaveOrderDatas(dto);
                    //}
                }

                //$scope.Sendmail = false;
                $scope.SaveOrderDatas(dto);
            }
        }

        $scope.ClickMeToRedirectEditOrder = function (OrderId) {
            location.href = '/#!/orderEdit/' + OrderId
        }

        $scope.CloseMailoption = function () {
            $("#SendThanks_EmailReview").modal("hide");
            $scope.IsBusy = false;
            return;
        }

        $scope.SendReviewMail = function () {
            var dto = $scope.Order;
            $("#SendThanks_EmailReview").modal("hide");
            $scope.Sendmail = true;
            $scope.SaveOrderDatas(dto);
        }

        $scope.NoReviewEmail = function () {
            var dto = $scope.Order;
            $("#SendThanks_EmailReview").modal("hide");
            $scope.Sendmail = false;
            $scope.SaveOrderDatas(dto);
        }

        $scope.SaveOrderDatas = function (dto) {
            if ($scope.Sendmail) {
                dto.SendReviewEmail = $scope.Sendmail;
            }

            if ($scope.Order.OrderStatus == 6 || $scope.Order.OrderStatus == 9) {
                var reversePayment = $scope.Order.OrderTotal - $scope.Order.BalanceDue;
                if (reversePayment > 0) {
                    hfcConfirm("A Payment in the amount of " + $filter('currency')(reversePayment) + " has been applied to this sales Order. Please reverse this transaction.");
                    $scope.OrderService.setOrderStatusChangerMethod($scope.updateOrderInfo, dto, true, false);
                    //hfcConfirm("A Payment in the amount of $" + reversePayment + " has been applied to this sales Order. Please reverse this transaction.");
                } else {
                    $scope.OrderService.setOrderStatusChangerMethod(null, null, false, false);
                    $scope.updateOrderInfo(dto);
                }
            }
            else {
                $scope.updateOrderInfo(dto);
            }
        }

        $scope.updateOrderInfo = function (dto) {
            $http.post('/api/orders', dto).then(function (response) {
                var orderId = response.config.data.OrderID;
                if ($scope.OrderService.redirectFlag) {
                    window.location = '#!/Orders/' + orderId;

                } else {
                    $scope.IsBusy = false;
                    $scope.OrderId = response.config.data.OrderID;
                    if ($routeParams.OrderId > 0) {
                        HFC.DisplaySuccess("Order Modified");
                    }
                    if ($scope.submitAction == 1) {
                        $scope.form_order.$setPristine();
                        window.location.href = '#!/Orders/' + $scope.OrderId;
                    }
                    else if ($scope.submitAction == 3) {
                        $scope.Order = null;
                        $scope.form_order.$setPristine();
                        window.location.href = '#!/orderSearch/';
                    }
                }
            }, function (response) {
                HFC.DisplayAlert(response.statusText);
                $scope.IsBusy = false;
            });
        }

        $scope.TaxDetails = function () {
            $scope.TaxDetailsLines = {
                dataSource: {
                    data: $scope.Order.TaxDetails,
                },
                resizable: true,
                columns: [
                    //{
                    //    field: "Jurisdiction",
                    //    title: "Jurisdiction",
                    //    filterable: false
                    //},
                    {
                        field: "JurisType",
                        title: "Tax Type",
                        filterable: false
                    },
                    {
                        field: "TaxName",
                        title: "Tax Name",
                        //filterable: false
                        template: function (item) {
                            if (item.TaxName != undefined && item.TaxName != null && item.TaxName != '') {
                                return item.TaxName;
                            } else {
                                return item.Jurisdiction;
                            }
                        }
                    },
                    {
                        field: "Rate",
                        title: "Rate",
                        //width: "100px",
                        template: '<span class="Grid_Textalign">#=kendo.format("{0:p}", Rate / 100)#</span>'
                    },
                    {
                        field: "Amount",
                        title: "Amount",
                        //width: "150px",
                        // format: "{0:c}"
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.Amount) + "</span>";
                        },
                    }
                ],
                editable: $scope.GridEditable,
                noRecords: { template: "No records found" },
                filterable: false,
                scrollable: true,
                sortable: true,
            };
        };

        //Warning for the Cancelled order and reverse payment

        function hfcConfirm(message, successCallback) {
            bootbox.dialog({
                message: message,
                title: "Warning",
                buttons: {
                    ok: {
                        label: "ok",
                        className: "btn-warning",
                        callback: function (params) {
                            $scope.ReverseAllPayment($scope.Order.OrderID);
                        }
                    },
                }
            });
        }

        $scope.GetOrderLines = function (orderlines) {
            $scope.GridEditable = false;
            var ds = new kendo.data.DataSource({
                data: orderlines,
                schema: {
                    model: {
                        fields: {
                            QuoteLineId: { type: "number" },
                            VendorId: { type: "number" },
                            ProductId: { type: "number" },
                            SuggestedResale: { type: "number" },
                            Discount: { type: "number" },
                            Width: { type: "number" },
                            Height: { type: "number" },
                            UnitPrice: { type: "number" },
                            Quantity: { type: "number" },
                            ExtendedPrice: { type: "number" },
                            ProductTypeId: { type: "number" },
                        }
                    }
                },
            });
            $scope.OrderLines = {
                dataSource: ds, //{
                //data: orderlines
                //},
                resizable: true,
                dataBound: function (e) {
                    //console.log("dataBound");

                    if (this.dataSource.view().length == 0) {
                        // The Grid contains No recrods so hide the footer.
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        // The Grid contains recrods so show the footer.
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }
                    if ($scope.brandid === 2 || $scope.brandid === 3) {
                        var grid = $("#QuotelinesGrid").data("kendoGrid");
                        grid.hideColumn("RoomLocation");
                        grid.hideColumn("RoomName");
                        grid.hideColumn("WindowName");
                    }
                },
                noRecords: { template: "No records found" },
                columns: [
                    {
                        field: "OrderLineId",
                        title: "Line ID",
                        filterable: false,
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.OrderLineId + "</span>";
                        },
                    },
                    {
                        field: "Quantity",
                        title: "QTY",
                        filterable: false,
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.NumberFormat(dataItem.Quantity, 'number') + "</span>";
                        },
                    },

                    {
                        field: "RoomLocation",
                        title: "Room",
                        filterable: false
                    },
                    {
                        field: "RoomName",
                        title: "Window Name",
                        filterable: false
                    },
                    {
                        field: "WindowName",
                        title: "Window Location",
                        filterable: false
                    },

                    {
                        field: "VendorId",
                        title: "Vendor",
                        filterable: false
                    },
                    {
                        field: "ProductId",
                        title: "Product",
                        filterable: false
                    },
                    {
                        field: "Description",
                        title: "Description",
                        filterable: false
                    },
                    {
                        field: "Width",
                        title: "Width",
                        filterable: false
                    },
                    {
                        field: "Height",
                        title: "Height",
                        filterable: false
                    },
                    {
                        field: "MountType",
                        title: "Mount Type",
                        filterable: false
                    },
                    {
                        field: "SuggestedResale",
                        title: "Suggested Resale",
                        filterable: false
                    },
                    {
                        field: "UnitPrice",
                        title: "Unit Price",
                        filterable: false
                    },
                    {
                        field: "Discount",
                        title: "Discount",
                        filterable: false
                    },
                    {
                        field: "Tax",
                        title: "Tax",
                        filterable: false
                    },
                    {
                        field: "ExtendedTotal",
                        title: "Extended Total",
                        filterable: false,
                        template: function (dataItem) {
                            if (dataItem.TaxExempt) {
                                return "<span class='Grid_Textalign'><img style='float: left;' src='../../../Content/images/Tax-Exempt.png'>" + HFCService.CurrencyFormat(dataItem.ExtendedPrice) + "</span>";
                            } else {
                                return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.ExtendedPrice) + "</span>";
                            }
                        },
                    },

                ],
                editable: $scope.GridEditable,
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                scrollable: true,
                sortable: true,
            };
        };
        $scope.AddNewPayment = function (OrderId) {
            if (OrderId == undefined) {
                OrderId = $routeParams.orderId;
            }
            location.href = '/#!/OrderPayment/' + OrderId;
            // $scope.form_order.$setDirty();
        }
        $scope.ReverseAllPayment = function (OrderId) {
            if (OrderId == undefined) {
                OrderId = $routeParams.orderId;
            }
            $scope.form_order.$setPristine();
            location.href = '/#!/ReverseOrderPayment/' + OrderId;
            // $scope.form_order.$setDirty();
        }

        $scope.GetOrderPaymentMethods = function () {
            $http.get('/api/orders/0/OrderPaymentMethods').then(function (data) {
                $scope.PaymentMethods = data.data;
            });
        }

        if (!$scope.isCustomerView)
            $scope.GetOrderPaymentMethods();

        $scope.GetPaymentReversalReason = function () {
            $http.get('/api/orders/0/OrderReversalReason').then(function (data) {
                $scope.ReversalReason = data.data;
            });
        }

        if (!$scope.isCustomerView)
            $scope.GetPaymentReversalReason();

        //code modified for reversal pop-up
        $scope.AddReversePayment = function (modal, paymentId) {
            $scope.Payment = [];
            $http.get('/api/orders/' + paymentId + '/OrderPaymentsByPaymentId/?orderId=' + $routeParams.orderId).then(function (response) {
                $scope.Payment = response.data;
                $scope.BalanceAmount = $scope.Payment.order.BalanceDue;
                $scope.Payment.PayBalance = false;
                //$scope.Payment.chkReversal = true;
                if ($scope.Payment.VerificationCheck != "" || $scope.Payment.VerificationCheck != null) {
                    $scope.Payment.VerificationCheck = "";
                }
                else $scope.Payment.VerificationCheck = $scope.Payment.VerificationCheck;
                $scope.Payment.PaymentMethod = "";
                if ($scope.Payment.Memo != "" || $scope.Payment.Memo != null) {
                    $scope.Payment.Memo = "";
                }
                else $scope.Payment.Memo = $scope.Payment.Memo;
                if ($scope.Payment.order.OrderTotal > 0) {
                    if ($scope.Payment.ReversalAmount == 0) {
                        $("#" + modal).modal("hide");
                        HFC.DisplayAlert("Reverse payment is not applicable");
                        return;
                    }
                    else if ($scope.Payment.ReversalAmount > $scope.Payment.Amount) {
                        $scope.Payment.Amount = $scope.Payment.Amount;
                        $scope.Payment.order.BalanceDue = $scope.Payment.order.BalanceDue + $scope.Payment.Amount;
                    }
                    else {
                        $scope.Payment.Amount = $scope.Payment.ReversalAmount;
                        $scope.Payment.order.BalanceDue = $scope.Payment.order.BalanceDue + $scope.Payment.Amount;
                    }
                    //$scope.Payment.Amount = $scope.Payment.Amount;
                    //$scope.Payment.Amount = (($scope.Payment.order.OrderTotal / $scope.Payment.order.OrderTotal) - 1) - $scope.Payment.order.OrderTotal;
                }
                //else {
                //    $scope.Payment.Amount = 0;
                //}
                //amount format
                $scope.CostCurrencyControl.CurrenctFormatControl($scope.Payment.Amount);
                //for displaying reversal date and time
                var date = new Date();
                //var options = {
                //    year: "numeric",
                //    month: "2-digit",
                //    day: "2-digit",
                //    hour: "2-digit",
                //    minute: "2-digit",
                //    second: "2-digit"
                //};
                //date = date.toLocaleString("en", options);
                //date = date.replace(/:\d{2}\s/, ' ');
                //$scope.Payment.PaymentDate = date.replace(',', '');
                $scope.Payment.PaymentDate = date;
                $("#" + modal).modal("show");
            });

            //$("#" + modal).modal("show");
        }
        $scope.SaveReversePayment = function (modal) {
            var myDiv = $('.reversepayment_popup');
            myDiv[0].scrollTop = 0;
            var dto = $scope.Payment;

            if (dto.ReasonCode == 0 || dto.ReasonCode == null || dto.ReasonCode == undefined || dto.ReasonCode == '') {
                HFC.DisplayAlert("Reason required");
                return;
            }
            if (dto.PaymentMethod == 0 || dto.PaymentMethod == null || dto.PaymentMethod == undefined || dto.PaymentMethod == '') {
                HFC.DisplayAlert("Payment Method required");
                return;
            }
            if (dto.PaymentDate == null || dto.PaymentDate == undefined || dto.PaymentDate == '') {
                HFC.DisplayAlert("Payment Date required");
                return;
            }
            if (dto.Amount == 0 || dto.Amount == null || dto.Amount == undefined) {
                HFC.DisplayAlert("Payment Amount required");
                return;
            }
            if (dto.Memo == null || dto.Memo == undefined || dto.Memo == '') {
                HFC.DisplayAlert("Memo required.");
                return;
            }
            if (dto.PaymentMethod == 2) {
                //if (dto.CheckNumber == null || dto.CheckNumber == undefined) {
                if (dto.CheckNumber == "" || dto.CheckNumber == undefined || dto.CheckNumber == null) {
                    HFC.DisplayAlert("Check number required");
                    return;
                }
                dto.VerificationCheck = dto.CheckNumber;
            }
            else if (dto.PaymentMethod == 3 || dto.PaymentMethod == 4 || dto.PaymentMethod == 5 || dto.PaymentMethod == 6 || dto.PaymentMethod == 7 || dto.PaymentMethod == 8 || dto.PaymentMethod == 9) {
                if (dto.VerificationCheck == null) {
                    HFC.DisplayAlert("Verification required");
                    return;
                }
                dto.VerificationCheck = dto.VerificationCheck;
            }
            else if (dto.PaymentMethod == 10) {
                if (dto.Authorization == null) {
                    HFC.DisplayAlert("Authorization required");
                    return;
                }
                dto.VerificationCheck = dto.Authorization;
            }
            else {
                dto.VerificationCheck = '';
            }
            //dto.Amount = dto.Amount * -1;
            var paymentId = $scope.Payment.paymentId;
            $http.put('/api/orders/0/AddPayment', dto).then(function (response) {
                $scope.IsBusy = false;

                if ($routeParams.orderId > 0) {
                    HFC.DisplaySuccess("Reverese Payment Created");
                }

                $("#" + modal).modal("hide");
                $scope.GetOrderData();
                window.location = '#!/Orders/' + $routeParams.orderId;
            }, function (response) {
                HFC.DisplayAlert(response.statusText);
                $scope.IsBusy = false;
            });
        }
        $scope.CreateInvoice = function () {
            var dto = $scope.Order;
            $http.put('/api/orders/0/CreateInvoice', dto).then(function (response) {
                $scope.IsBusy = false;

                if ($routeParams.orderId > 0) {
                    HFC.DisplaySuccess("Invoice created");
                }

                $scope.GetInvoiceData();
                window.location = '#!/Orders/' + $routeParams.orderId;
            }, function (response) {
                HFC.DisplayAlert(response.statusText);
                $scope.IsBusy = false;
            });
        }
        $scope.GetInvoiceData = function () {
            $http.get('/api/Orders/' + $routeParams.orderId).then(function (orderServiceData) {
                //
                $scope.Order = orderServiceData.data;
                $scope.NewContractedDate = $scope.Order.ContractedDate;
                if (!$scope.Order.ContractedDateUpdatedBy_Name)
                    $scope.showeditbtn = true;

                if (document.location.href.toString().indexOf("/Orders/") > 0) {
                    $scope.GetOrderLines($scope.Order.OrderLines);
                }
            });
        }
        $scope.ConvertTheOrderToMasterPO = function () {
            var dto = $scope.Order;
            var loadingElement = document.getElementById("loading");
            loadingElement.style.display = "block";
            dto.OrderStatus = 2;//Procurement Complete
            $http.get('/api/Orders/' + dto.OrderID + '/OrderToMPO').then(function (modRes) {
                loadingElement.style.display = "none";
                if (modRes.data.error === "") {
                    $window.location.href = '#!/purchasemasterview/' + modRes.data.data;
                }
                else {
                    HFC.DisplayAlert(modRes.data.error);
                }
            }, function (response) {
                loadingElement.style.display = "none";
                console.log('Error Response : ' + response);
            }
            );
            //    .catch(function (response) {
            //
            //    loadingElement.style.display = "none";
            //    console.log('Error Response : ' + response)
            //    console.log('Error Response.data : ' + response.data)
            //    console.log('Error Response.status : ' + response.status)
            //});
            //$http.post('/api/orders/', dto).then(function (response) {
            //
            //    $scope.IsBusy = false;

            //    if ($routeParams.orderId > 0) {
            //        HFC.DisplaySuccess("The Order Converted to a Master PO");
            //    }

            //    $scope.OrderId = response.config.data.OrderID;

            //    $scope.Order = null;
            //    window.location = '#!/orderSearch/';

            //}, function (response) {
            //    HFC.DisplayAlert(response.statusText);
            //    $scope.IsBusy = false;
            //});
        }
        $scope.GetOrderData = function () {
            $http.get('/api/Orders/' + $routeParams.orderId).then(function (orderServiceData) {
                $scope.Order = orderServiceData.data;
                $scope.NewContractedDate = $scope.Order.ContractedDate;
                if (!$scope.Order.ContractedDateUpdatedBy_Name)
                    $scope.showeditbtn = true;
                $scope.getQuotelines(orderServiceData.data.QuoteLinesVM);

                //if (document.location.href.toString().indexOf("/Orders/") > 0) {
                //    $scope.GetOrderLines($scope.Order.OrderLines)

                //}
            });
        }
        $scope.CancelPayment = function (modal) {
            $("#" + modal).modal("hide");
            var myDiv = $('.reversepayment_popup');
            myDiv[0].scrollTop = 0;
        }

        $scope.PrintInvoice = function (OrderId, InvoiceId) {
            $scope.PrintService.PrintOrderInvoice(OrderId, InvoiceId);
        }

        $scope.DownloadInvoice = function (OrderId, InvoiceId) {
            $scope.PrintService.DownloadOrderInvoice(OrderId, InvoiceId);
        }
        $scope.Attachment = null;
        $scope.PrintInvoiceforEmail = function (modal, InvoiceId) {
            var ticks = (new Date()).getTime();
            var url = '/export/EmailOrderInvoice/' + InvoiceId + '/?type=PDF&inline=false&t=' + ticks;
            if (url != null) {
                $http.get(url).then(function (response) {
                    $scope.IsBusy = false;

                    if (response != null) {
                        HFC.DisplaySuccess("Attachment created");
                    }

                    var FileName = response.data.substring(response.data.lastIndexOf('\\'));
                    FileName = FileName.replace(/^.*(\\|\/|\:)/, '');
                    $scope.EmailService.AttachmentLink = response.data;
                    $scope.EmailService.AttachmentText = FileName;

                    $("#" + modal).modal("show");
                }, function (response) {
                    HFC.DisplayAlert(response.statusText);
                    $scope.IsBusy = false;
                });
            }
        }
        $scope.EmailInvoice = function (modal, InvoiceId) {
            var currentuser = HFCService.CurrentUser;
            var personId = null;
            if (currentuser != null) {
                personId = currentuser.PersonId;
            }
            //fromAddresses: null,
            //toAddresses: null,
            //bccAddresses: null,
            //subject: null,
            //body: null,
            //attachment: null,
            $scope.EmailService.ToAddresses = '';
            $scope.EmailService.BccAddresses = '';
            $scope.EmailService.AttachmentText = '';
            $scope.EmailService.AttachmentLink = '';
            $scope.EmailService.Body = '';
            $scope.EmailService.attachment = '';
            $scope.EmailService.Subject = '';

            $scope.EmailService.GetPersonPrimaryEmail(personId);
            $scope.PrintInvoiceforEmail(modal, InvoiceId);
            $scope.EmailService.Attachment = $scope.Attachment;
            $scope.InvoiceId = InvoiceId;
        }
        $scope.CancelEmailInvoice = function (modal) {
            $("#" + modal).modal("hide");
        }
        $scope.SendInvoiceEmail = function (modal, InvoiceId) {
            $scope.SubmitEmail = true;

            if ($scope.frmEmailInvoice.$invalid) {
                return;
            }

            var dto = $scope.EmailService;

            if (dto.ToAddresses == undefined || dto.ToAddresses == null || dto.ToAddresses == '') {
                HFC.DisplayAlert("To Address required");
                return;
            }
            if (dto.Subject == undefined || dto.Subject == null || dto.Subject == '') {
                HFC.DisplayAlert("Subject required");
                return;
            }
            if (dto.Body == undefined || dto.Body == null || dto.Body == '') {
                HFC.DisplayAlert("Message required");
                return;
            }
            dto.Body = HFC.htmlEncode($scope.EmailService.Body);
            $http.put('/api/orders/' + $scope.InvoiceId + '/SendInvoiceEmail', dto).then(function (response) {
                $scope.IsBusy = false;

                if ($routeParams.orderId > 0) {
                    HFC.DisplaySuccess("Email sent");
                }

                $("#" + modal).modal("hide");
                $scope.GetOrderData();
                window.location = '#!/Orders/' + $routeParams.orderId;
            }, function (response) {
                HFC.DisplayAlert(response.statusText);
                $scope.IsBusy = false;
            });

            $("#" + modal).modal("hide");
        }

        //Print Installation packet
        $scope.PrintOptions = {};
        $scope.PrintOrderOptions = {
            AddCustomerOpportunity: false,
            AddGoogleMap: false,
            AddExistingMeasurements: false,
            AddPrintInternalNotes: false,
            AddPrintExternalNotes: false,
            AddPrintDirectionNotes: false,
            AddOrder: false,
            AddInstallationChecklist: false,
            AddWarrantyInformation: false,
        };
        $scope.ShowInstallationPrint = function (modalId) {
            $("#" + modalId).modal("show");
        }
        $scope.CancelPrintEditor = function (modalId) {
            $("#" + modalId).modal("hide");
        }
        $scope.PrintInstallationPacket = function (OrderId) {
            if (OrderId == null || OrderId == undefined) {
                OrderId = $routeParams.orderId;
            }
            $scope.PrintService.PrintInstallationPacket(OrderId, $scope.PrintOrderOptions);
        }

        // new version of print:
        // print integration -- murugan
        $scope.printControl = {};
        $scope.printInstallPacket = function (modalid) {
            if ($routeParams.orderId) $scope.orderIdPrint = $routeParams.orderId;

            // $("#" + modalid).modal("show");
            $scope.printControl.showPrintModal();
        }

        //Appointment
        $scope.calendarModalId = 0;
        //gets a single lead and store it in this service as well as any pass in reference
        $scope.GetAppointmentData = function (orderId, success) {
            if (!isNaN(orderId) && orderId > 0) {
                $scope.OrderServiceData = $scope.Order;

                if (success) {
                    success($scope.OrderServiceData);
                }
            }
        }
        $scope.SetAppointmentCallback = function (data) {
            // $scope.Order = data.Order;
            CalendarService.NewOrderApt($scope.calendarModalId, $scope.OrderServiceData, 0, 0, null, 0);
        }

        $scope.SetInstallationAppointment = function (modalId, orderId) {
            //call appointment popup
            $scope.calendarModalId = modalId;
            var result = $scope.GetAppointmentData(orderId, $scope.SetAppointmentCallback);
        }

        $scope.AddMultipleNotes = function (e) {
            $scope.QuoteNoteLineModel = !$scope.QuoteNoteLineModel;
            $scope.Quotation = angular.copy($scope.tempquotation);
            $scope.form_order.$setPristine();
            //   $anchorScroll();
        }

        $scope.confirmTaxExempt = function () {
            //
            //if ($scope.Order.IsTaxExempt == true)
            //    if (!confirm('Would you like to update the Tax Exempt Status on Account?')) $scope.Order.IsTaxExempt = false;
        };

        $scope.getQuotelines = function (data) {
            $scope.quoteLinesOptions = {
                dataSource: {
                    data: data
                },
                //filterable: true,
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            eq: "Is equal to",
                            neq: "Is not equal to"
                        },
                        date: {
                            gt: "After (Excludes)",
                            lt: "Before (Includes)"
                        },
                    }
                },
                resizable: true,
                noRecords: { template: "No records found" },
                detailInit: detailinitParent,
                dataBound: function (e) {
                    //console.log("dataBound");

                    $scope.orderLineLength = $("#QuotelinesGrid").data("kendoGrid").dataSource.data().length;
                    if (this.dataSource.view().length == 0) {
                        // The Grid contains No recrods so hide the footer.
                        $('.k-pager-nav').hide();
                        $('.k-pager-numbers').hide();
                        $('.k-pager-sizes').hide();
                    } else {
                        // The Grid contains recrods so show the footer.
                        $('.k-pager-nav').show();
                        $('.k-pager-numbers').show();
                        $('.k-pager-sizes').show();
                    }
                    if ($scope.brandid === 2 || $scope.brandid === 3) {
                        var grid = $("#QuotelinesGrid").data("kendoGrid");
                        grid.hideColumn("WindowLocation");
                        grid.hideColumn("RoomName");
                        grid.hideColumn("Width");
                        grid.hideColumn("Height");
                        grid.hideColumn("MountType");
                        grid.hideColumn("Color");
                        grid.hideColumn("Fabric");
                    }

                    $scope.quoteLineQtyTotal = 0;
                    $scope.quoteLineLength = $("#QuotelinesGrid").data("kendoGrid").dataSource._data.length;
                    var datasourcee = $("#QuotelinesGrid").data("kendoGrid").dataSource._data;
                    if (datasourcee.length > 0)
                        for (var count = 0; count < $scope.quoteLineLength; count++) {
                            if (datasourcee[count].Quantity) $scope.quoteLineQtyTotal = $scope.quoteLineQtyTotal + datasourcee[count].Quantity;
                        }

                    //if ($scope.BindCursor) {
                    //    $("#QuotelinesGrid .k-grid-header thead tr th:first-child").html('<button id="btnCaret" class="k-icon k-i-expand" title="Selecting Caret will expand all quote lines" style="display:block !important;float:left;margin: 8px 5px 0 0;" aria-label="Expand" ></button> <button id="btnCaret1" class="k-icon k-i-collapse" title="Selecting Caret will collapse all quote lines" style="display:block !important;float:left;margin: 8px 5px 0 0;" aria-label="Collapse" ></button>');
                    //    $scope.BindCursor = false;
                    //    document.getElementById("btnCaret1").style.display = "none";
                    //    $("#btnCaret").click(function (e) {
                    //        var grid = $("#QuotelinesGrid").data("kendoGrid");
                    //        $(".k-master-row").each(function (index) {
                    //            grid.expandRow(this);
                    //        });
                    //        document.getElementById("btnCaret").style.display = "none";
                    //        document.getElementById("btnCaret1").style.display = "block";
                    //        //}

                    //        e.preventDefault();
                    //        e.stopPropagation();
                    //    });

                    //    $("#btnCaret1").click(function (e) {
                    //        var grid = $("#QuotelinesGrid").data("kendoGrid");
                    //        $(".k-master-row").each(function (index) {
                    //            grid.collapseRow(this);
                    //        });
                    //        document.getElementById("btnCaret").style.display = "block";
                    //        document.getElementById("btnCaret1").style.display = "none";

                    //        e.preventDefault();
                    //        e.stopPropagation();
                    //    });

                    //}

                },
                toolbar: [
                    {
                        template: $("#toolbarTemplatee").html()
                    }
                ],

                columns: [
                    { field: "QuoteLineId", hidden: true },
                    { field: "QuoteKey", hidden: true },
                    { field: "MeasurementsetId", hidden: true },
                    { field: "MeasurementId", hidden: true },
                    { field: "VendorId", hidden: true },
                    { field: "ProductId", hidden: true },
                    { field: "Memo", hidden: true },
                    { field: "PICJson", hidden: true },
                    { field: "ProductTypeId", hidden: true },

                    {
                        field: "QuoteLineNumber", title: "Line ID",
                        //template: "#= QuoteLineNumber #",
                        template: function (dataItem) {
                            var template = '';
                            if (dataItem.ProductTypeId == 1 && dataItem.ValidConfiguration !== null) {
                                if (dataItem.ValidConfiguration) {
                                    template = template + '<div class="revalidate-wrapper"><i  class="fas fa-check-circle validConfig"></i></div>';
                                }
                                else {
                                    template = template + '<div class="revalidate-wrapper"><i class="fas fa-exclamation-triangle invalidConfig"></i></div>';
                                }
                                return template + "<div class='revalidate-wrapper'><span class='Grid_Textalign revalidate-align'>" + dataItem.QuoteLineNumber + "</span></div>";
                            } else {
                                return template + "<span class='Grid_Textalign'>" + dataItem.QuoteLineNumber + "</span>";
                            }
                        },
                        width: "80px"
                    },
                    {
                        field: "Quantity", title: "Qty", width: "150px", template: function (dataItem) {
                            if (dataItem.ProductTypeId === 1 ||
                                dataItem.ProductTypeId === 2 ||
                                dataItem.ProductTypeId === 3 ||
                                dataItem.ProductTypeId === 5) {
                                if (dataItem.Quantity === 0) {
                                    return "Cancelled";
                                } else {
                                    return '<span class="Grid_Textalign">' + HFCService.NumberFormat(dataItem.Quantity, 'number') + '</span>';
                                }
                            } else {
                                return '<span class="Grid_Textalign">' + HFCService.NumberFormat(dataItem.Quantity, 'number') + '</span>';
                            }
                        }
                    },

                    { field: "RoomName", title: "Window  Name" },
                    { field: "WindowLocation", title: "Window Location" },
                    //{
                    //    field: "ProductTypeId", title: "Item",
                    //    template: "# if(ProductTypeId == null){ #" + "<span></span>" + "#} else if(ProductTypeId == 4){ #" + "<span>#=Description#</span>" + "#} else {#" + "<span>#=ProductName#</span>" + "# } #"
                    //},
                    {
                        field: "ProductName", title: "Item",
                        //template: "# if(ProductTypeId == null){ #" + "<span></span>" + "#} else if(ProductTypeId == 4){ #" + "<span>#=Description#</span>" + "#} else {#" + "<span>#=ProductName#</span>" + "# } #"
                    },
                    { field: "Color", title: "Color" },
                    { field: "Fabric", title: "Fabric" },
                    {
                        field: "Width",
                        title: "Width",
                        template: "#= Width # #= FranctionalValueWidth #",
                        filterable: false
                    },
                    {
                        field: "Height",
                        title: "Height",
                        filterable: false,
                        template: "#= Height # #= FranctionalValueHeight #"
                    },
                    { field: "MountType", title: "Mount Type" },

                    {
                        field: "ExtendedPrice", width: 96, title: "Extended Total",
                        //format: "{0:c}"
                        template: function (dataItem) {
                            if (dataItem.TaxExempt) {
                                return "<span class='Grid_Textalign' style='padding-right:0 !important'><img style='float: left;' src='../../../Content/images/Tax-Exempt.png'>" + HFCService.CurrencyFormat(dataItem.ExtendedPrice) + "</span>";
                            } else {
                                return "<span class='Grid_Textalign' style='float:right; padding-right:0 !important'>" + HFCService.CurrencyFormat(dataItem.ExtendedPrice) + "</span>";
                            }
                        },
                    },
                    {
                        field: "",
                        title: "",
                        filterable: false,
                        width: "30px",
                        template: function (dataItem) {
                            if (dataItem.ProductTypeId != 4 && dataItem.ProductTypeId != 0) {
                                $scope.IsDiscountCheck = true;
                            }
                            return '<a href="" ng-show="ShowCreateCaseIcon && dataItem.vpoStatus && FranciseLevelCase && dataItem.ProductTypeId !=4 && dataItem.ProductTypeId !=0 && Order.OrderStatus != 9 && Permission.EditOrder" class="tooltip-bottom createcase_ttip" data-tooltip="Create Case" ng-click="gotoCaseAddEditwithVPO(dataItem.QuoteLineNumber)"><i class="far fa-radiation-alt" style="font-size:18px;"></i></a>'
                        }//"#= Height # #= FranctionalValueHeight #"
                    }

                ]
            };
        }

        if (!$scope.isCustomerView) {
            $http.get('/api/lookup/0/GetQuoteStatus').then(function (data) {
                $scope.QuoteStatusList = data.data;
            });
        }

        $scope.CancelReasonList = [
            { ID: 1, Name: 'Customer Cancellation' },
            { ID: 2, Name: 'Entered  Incorrectly' },
            { ID: 3, Name: 'Incorrect Measurement / Product Configuration' },
            { ID: 4, Name: 'Other' }

        ]

        function detailinitParent(e) {
            var grdId = "grd" + e.data.QuoteLineId;
            $("<div id = '" + grdId + "'/>").appendTo(e.detailCell).kendoGrid({
                dataSource: [e.data],
                columns: [
                    { field: "VendorName", title: "Vendor" },
                    { field: "Description", title: "Item Description", template: '#=Description#' },
                    { field: "SuggestedResale", title: "Suggested Resale", format: "{0:c}" },
                    { field: "UnitPrice", title: "Unit Price", format: "{0:c}" },
                    { field: "Discount", title: "Discount", template: '#=kendo.format("{0:p}", Discount / 100)#' },
                ],
                dataBound: function (e) {
                    //Below code added to show the group discount tags icon if group discount applied
                    var rows = this.tbody.children();
                    var dataItems = this.dataSource.view();
                    for (var i = 0; i < dataItems.length; i++) {
                        var temp = dataItems[i];

                        if (temp["IsGroupDiscountApplied"] == true) {
                            $("#" + this.element[0].id + " th[data-field=Discount]").html("Discount" + '<i class="fas fa-tags pull-right" style="padding-top:5px"></i>');
                        }
                    }
                }
            });
        }
        $scope.External = {};

        $scope.refreshNotesGrid = function () {
            var result = $scope.External.Notes;
            $scope.NoteServiceTP.RefreshNotesGrid(result);
        }

        $scope.refreshAdditionalContactsGrid = function () {
            var result = $scope.External.AdditionalContacts;
            $scope.AdditionalContactServiceTP.RefreshGrid(result);
        }

        $scope.refreshAdditionalAddressGrid = function () {
            var result = $scope.External.AdditionalAddress;
            $scope.AdditionalAddressServiceTP.RefreshGrid(result);
        }
        $scope.gotoCaseAddEditwithMPO = function () {
            //  window.location.href = '/#!/caseAddEdit/' + $routeParams.orderId + '/' + $scope.PurchaseOrderIdToCase + '/0';
            window.location.href = '/#!/createCaseOrder/' + $routeParams.orderId + '/0/0';
        }

        $scope.gotoCaseAddEditwithVPO = function (id) {
            //  window.location.href = '/#!/caseAddEdit/' + $routeParams.orderId + '/' + $scope.PurchaseOrderIdToCase + '/' + id;
            window.location.href = '/#!/createCaseOrder/' + $routeParams.orderId + '/0/' + id;
        }

        $scope.SaveHeaderLevelNote = function () {
            var formData = new FormData();
            formData.append('QuoteLevelNotes', $scope.Quotation.QuoteLevelNotes);
            formData.append('OrderInvoiceLevelNotes', $scope.Quotation.OrderInvoiceLevelNotes);
            formData.append('InstallerInvoiceNotes', $scope.Quotation.InstallerInvoiceNotes);
            $http.post('/api/Quotes/' + $scope.Quotation.QuoteKey + '/SaveQuoteNotes', formData,
                {
                    withCredentials: true,
                    headers: { 'Content-Type': undefined },
                    transformRequest: angular.identity
                }).then(function (response) {
                    if (response.data == true) {
                        //$scope.Quotation = response.data.data;
                        //$scope.QuotationCopy = angular.copy($scope.Quotation);
                        //$scope.Quotation.QuoteKey = response.data.quoteKey;

                        // $scope.QuoteinEditMode = false;
                        $scope.form_order.$setPristine();
                        $scope.tempquotation = angular.copy($scope.Quotation);
                        $scope.QuoteNoteLineModel = false;
                        $anchorScroll();
                    } else {
                        HFC.DisplaySuccess(response.data.error);
                    }
                    //$scope.$apply();
                    $scope.loadingElement.style.display = "none";
                }).catch(function (error) {
                    var loadingElement = document.getElementById("loading");
                    loadingElement.style.display = "none";
                });
        }

        var setCaretIconBehaviour = function () {
            //  if ($scope.brandid === 1) {
            if ($scope.BindCursor) {
                if (document.getElementById("btnCaret1") && document.getElementById("btnCaret")) {
                    $scope.BindCursor = false;
                    document.getElementById("btnCaret1").style.display = "none";
                    document.getElementById("btnCaret").style.display = "block";

                    $("#btnCaret").click(function (e) {
                        var grid = $("#QuotelinesGrid").data("kendoGrid");
                        $(".k-master-row").each(function (index) {
                            grid.expandRow(this);
                        });

                        document.getElementById("btnCaret").style.display = "none";
                        document.getElementById("btnCaret1").style.display = "block";

                        var panelBody = $("#panelbarQuoteLine");
                        var panelBar = $("#panelbar").data("kendoPanelBar");
                        panelBar.expand(panelBody);
                        e.preventDefault();
                        e.stopPropagation();
                    });

                    $("#btnCaret1").click(function (e) {
                        //var panelBody = $("#panelbar .k-item.k-state-default");
                        //var panelBar = $("#panelbar").data("kendoPanelBar");
                        //panelBar.collapse(panelBody);

                        var grid = $("#QuotelinesGrid").data("kendoGrid");
                        $(".k-master-row").each(function (index) {
                            grid.collapseRow(this);
                        });
                        document.getElementById("btnCaret").style.display = "block";
                        document.getElementById("btnCaret1").style.display = "none";

                        e.preventDefault();
                        e.stopPropagation();
                    });
                }
            }
            //}
            //else {
            //    if (document.getElementById("btnCaret"))
            //        document.getElementById("btnCaret").style.display = "none";
            //    if (document.getElementById("btnCaret1"))
            //        document.getElementById("btnCaret1").style.display = "none";

            //}
        };

        // Kendo Resizing
        $scope.$on("kendoWidgetCreated", function (e) {
            //$('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 332);
            //window.onresize = function () {
            //    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 332);
            //};
            setCaretIconBehaviour();
        });
        // End Kendo Resizing

        //for date validation
        $scope.ValidateDate
            = function (date, id) {
                date = $("#" + id + "").val();

                $scope.ValidDate = HFCService.ValidateDate(date, "date");

                if ($scope.ValidDate) {
                    $("#" + id + "").removeClass("invalid_Date");
                    var a = $('#' + id + '').siblings();
                    $(a[0]).removeClass("icon_Background");
                }
                else {
                    $("#" + id + "").addClass("invalid_Date");
                    var a = $('#' + id + '').siblings();
                    $(a[0]).addClass("icon_Background");
                }
            }


        $scope.ErrorMessage = "We're Sorry this feature is currently unavailable. Please refresh your screen or try again in a few minutes";

        //for navigating to customer view
        $scope.CustomerView = function () {
            window.location.href = '#!/customerView/' + $routeParams.orderId;

        }

        if (window.location.href.includes("customerView")) {
            if ($routeParams.orderId > 0) {
                $scope.modalState.loaded = false;
                $http.get('/api/Orders/' + $routeParams.orderId + '/GetCustomerViewData')
                    .then(function (response) {
                        $scope.CVD = response.data;
                        HFCService.setHeaderTitle("Customerview #" + $scope.CVD.cVHeader.InvoiceNumber);
                        if ($scope.CVD.TermsAcceptedDate) {
                            $('#btnAgree').prop('disabled', true);
                            $('#btnCancel').prop('disabled', true);
                            if ($scope.CVD.CustomerSignJson == null) {
                                $('#CustSign .sigPad').signaturePad({ drawOnly: true, lineTop: 0, displayOnly: false });
                                $('#btnAcceptcustsign').prop('disabled', false);
                                $('#btnCancelcustsign').prop('disabled', false);
                            }
                            if ($scope.CVD.SalesRepSignJson == null) {
                                $('#SalerepSign .sigPad').signaturePad({ drawOnly: true, lineTop: 0, displayOnly: false });
                                $('#btnAcceptsalerepsign').prop('disabled', false);
                                $('#btnCancelsalerepsign').prop('disabled', false);
                            }
                        }
                        else {
                            $('#CustSign').css("pointer-events", "none");
                            $('#SalerepSign').css("pointer-events", "none");
                        }
                        if ($scope.CVD.CustomerSignJson != null) {
                            $('#CustSign .sigPad').signaturePad({ drawOnly: true, lineTop: 0, displayOnly: true }).regenerate(JSON.parse($scope.CVD.CustomerSignJson));
                            $('#CustSign').css("pointer-events", "none");
                        }
                        if ($scope.CVD.SalesRepSignJson != null) {
                            $('#SalerepSign .sigPad').signaturePad({ drawOnly: true, lineTop: 0, displayOnly: true }).regenerate(JSON.parse($scope.CVD.SalesRepSignJson));
                            $('#SalerepSign').css("pointer-events", "none");
                        }
                        $scope.modalState.loaded = true;
                    }).catch(function (error) {
                        HFC.DisplayAlert($scope.ErrorMessage);
                        $scope.modalState.loaded = true;
                    });

                //below api is to check whethe customer sign and sales rep sign available(only when admin and franchise ds enabled)
                //$http.get('/api/Orders/' + $routeParams.orderId + '/getSignedOrNot')
                //    .then(function (response) {
                //    }).catch(function (error) {
                //        HFC.DisplayAlert($scope.ErrorMessage);
                //    });
            }
        }

        //for saving terms and condition accept or cancel
        $scope.TermsCondition = function (value) {
            $('#btnAgree').prop('disabled', true);
            $('#btnCancel').prop('disabled', true);
            if (value == 1) {
                $http.post('/api/Orders/' + $routeParams.orderId + '/AcceptTerms?flag=' + (value == 1 ? "true" : "false"))
                    .then(function (response) {
                        $scope.CVD.TermsAccepted = response.data.TermsAccepted;
                        $scope.CVD.TermsAcceptedDate = response.data.TermsAcceptedDate;
                    }).catch(function (error) {
                        HFC.DisplayAlert($scope.ErrorMessage);
                    });

                $('#CustSign .sigPad').signaturePad({ drawOnly: true, lineTop: 0 });
                $('#CustSign').css("pointer-events", "auto");
                $('#btnAcceptcustsign').prop('disabled', false);
                $('#btnCancelcustsign').prop('disabled', false);
                $('#SalerepSign .sigPad').signaturePad({ drawOnly: true, lineTop: 0, displayOnly: false });
                $('#SalerepSign').css("pointer-events", "auto");
                $('#btnAcceptsalerepsign').prop('disabled', false);
                $('#btnCancelsalerepsign').prop('disabled', false);
            }
            else {
                $("#Warning").modal("hide");
            }
        }
        //for saving customer signature
        $scope.SaveCustSign = function () {
            var custsign = $('#CustSign .sigPad').signaturePad({ drawOnly: true, lineTop: 0 });
            if (custsign.getSignature().length > 0) {
                $scope.CVD.CustomerSignJson = JSON.stringify(custsign.getSignature());
                $http.post('/api/Orders/' + $routeParams.orderId + '/UpdateSign?type=Customer'.toString(), $scope.CVD)
                    .then(function (response) {
                        $scope.CVD.CustomerSignedDate = response.data.CustomerSignedDate;
                    }).catch(function (error) {
                        HFC.DisplayAlert($scope.ErrorMessage);
                    });

                $('#CustSign').css("pointer-events", "none");
                $('#btnAcceptcustsign').prop('disabled', true);
                $('#btnCancelcustsign').prop('disabled', true);                
            }
            else
                HFC.DisplayAlert("Please enter your signature before selecting Accept.");
        }
        //for saving salesrep signature
        $scope.SaveSalerepSign = function () {
            var SalerepSign = $('#SalerepSign .sigPad').signaturePad({ drawOnly: true, lineTop: 0 });
            if (SalerepSign.getSignature().length > 0) {
                if (!$scope.CVD.CustomerSignedDate)
                    HFC.DisplayAlert("The customer's signature is required to complete the order.");
                $scope.CVD.SalesRepSignJson = JSON.stringify(SalerepSign.getSignature());
                $http.post('/api/Orders/' + $routeParams.orderId + '/UpdateSign?type=SalesRep', $scope.CVD)
                    .then(function (response) {
                        $scope.CVD.SalesRepSignedDate = response.data.SalesRepSignedDate;
                    }).catch(function (error) {
                        HFC.DisplayAlert($scope.ErrorMessage);
                    });

                $('#SalerepSign').css("pointer-events", "none");
                $('#btnAcceptsalerepsign').prop('disabled', true);
                $('#btnCancelsalerepsign').prop('disabled', true);
            }
            else
                HFC.DisplayAlert("Please enter your signature before selecting Accept.");
        }
        //for canceling customer signature
        $scope.CancelSign = function (id) {
            $('#' + id + ' .sigPad').signaturePad({ displayOnly: false }).regenerate([]);
        }
    }
]);
//app.filter('ignoreTimeZone', function () {
//    return function (val) {
//        var newDate = new Date(val.replace('T', ' ').slice(0, -6));
//        return newDate;
//    };
//});