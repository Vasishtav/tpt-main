﻿'use strict';

jQuery.scrollTo = function (target, offset, speed, container) {

    if (isNaN(target)) {

        if (!(target instanceof jQuery))
            target = $(target);

        target = parseInt(target.offset().top);
    }

    container = container || "html, body";
    if (!(container instanceof jQuery))
        container = $(container);

    speed = speed || 500;
    offset = offset || 0;

    container.animate({
        scrollTop: target + offset
    }, speed);
};

app.controller('paymentController', ['$scope', '$routeParams', '$rootScope', '$window', '$location', '$http',
                    'HFCService', 'LeadSourceService', 'JobService', 'AddressService', 'PersonService',
                    'JobNoteService', 'InvoiceService', 'PurchaseOrderFilter', 'PaymentService', 'AdvancedEmailService',
                    'AttendeeService', 'OrderService', 'NewtaskService', 'NavbarService', 'PaymentInvoiceService',
    function ($scope, $routeParams, $rootScope, $window, $location, $http,
        HFCService, LeadSourceService, JobService, AddressService, PersonService,
        JobNoteService, PurchaseOrderFilter, PaymentService, AdvancedEmailService,
        LeadOrderService, AttendeeService, OrderService, NewtaskService, NavbarService, PaymentInvoiceService) {

        //we will use local ctrl var so we don't clutter up $scope that doesn't need to be shared with nested controllers and directives
        var ctrl = this;
        var OrderId = $routeParams.OrderId;

        var reversalPayment = false;
        if (window.location.hash.includes("ReverseOrderPayment")) {
             
            reversalPayment = true;
        }
        var jobId = parseInt($routeParams.jobId) || 0;
        $scope.selectedIds = [];
        $scope.OrderStatuseslist = null;
        $scope.accountlist = null;
        $scope.Order = null;
        $scope.franchiseAddressObject = null;
        $scope.isSubmitted = false;


        $scope.NavbarService = NavbarService;
        $scope.NavbarService.SelectSales();
        $scope.NavbarService.EnableSalesOrdersTab();
        $scope.AdvancedEmailService = AdvancedEmailService;
        $scope.JobNoteService = JobNoteService;
        $scope.JobService = JobService;
        $scope.LeadSourceService = LeadSourceService;
        $scope.AttendeeService = AttendeeService;
        $scope.OrderStuseslist = [];
        $scope.OrderSourcesList = [];
        $scope.OrderCampaignslist = [];
        $scope.HFCService = HFCService;
        $scope.PersonService = PersonService;
        $scope.AddressService = AddressService;
        $scope.LeadSourceService = LeadSourceService;
        $scope.PaymentService = PaymentService;
        $scope.OrderService = OrderService;
        $scope.NewtaskService = NewtaskService;
        $scope.purchaseOrders = [];
        $scope.orderStatuses = [];
        $scope.filter = ctrl.filter = PurchaseOrderFilter;
        $scope.OrderPopAddress = [];
        $scope.PaymentInvoiceService = PaymentInvoiceService;
        $scope.Showerror = false

        $scope.Permission = {};
        var Paymentpermission = $scope.HFCService.CurrentUserPermissions.find(x=>x.ModuleCode == 'Payment')
        var Reversepaypermission = Paymentpermission.SpecialPermission.find(x=>x.PermissionCode == 'ReversePayment ').CanAccess;

        $scope.Permission.ReversePayment = Reversepaypermission; //$scope.HFCService.GetPermissionTP('Reverse Payment').CanAccess;
        //amount format
        $scope.CostCurrencyControl = {};
        //flag for date validation
        $scope.ValidDate = true;
        //added to make amount field as kendo-numeric
        $scope.options = {
            format: "c", decimals: 2, spinners: false, min: 0, placeholder: "$0.00",
            change: function () {
                $scope.PayBalanceAmount();
            }
            
        }
        //end..

        $scope.BalanceAmount = null;
        $scope.ResetPaymentForNew = function () {
            $scope.Payment.PaymentID = 0;
            //OpportunityID
            //OrderID
            //Sidemark
            $scope.Payment.Total = 0;
            //BalanceDue
            //PayBalance
            $scope.Payment.PaymentMethod = 0;
            //VerificationCheck
            $scope.Payment.PaymentDate = $scope.SetNewPaymentDate();
            $scope.Payment.Amount = '';
            $scope.Payment.Memo = '';
            $scope.Payment.CreatedOn = '';
            //CreatedBy
            //LastUpdatedOn
            //LastUpdatedBy


        }
        $scope.SetNewPaymentDate = function () {
            var date = new Date();
            date = date.toLocaleString();
            date = date.replace(/:\d{2}\s/, ' ');
            date = date.replace(',', '')
            return date;
        }
        $scope.PayBalanceAmount = function () {
            $scope.Showerror = false;
            var reversalAmount = document.getElementById('chkReversal').checked;
           
            var Amount = $scope.Payment.Amount;
            Amount = parseFloat(Amount);
            var CurrentBalanceAmount = $scope.BalanceAmount;
            if (reversalAmount == true) {
                var PayBalanceAmount = CurrentBalanceAmount + Amount;
            }
            else {
                if (CurrentBalanceAmount < Amount) {
                    //var PayBalanceAmount = 0;
                    var PayBalanceAmount = CurrentBalanceAmount - Amount;
                }
                else
                    var PayBalanceAmount = CurrentBalanceAmount - Amount;
                
                if (Amount > CurrentBalanceAmount) {
                    $scope.OverPayment = Amount - CurrentBalanceAmount;
                }
                else {
                    $scope.OverPayment=0;
                } 
                }
              
            if (isNaN(PayBalanceAmount)) {
                PayBalanceAmount = $scope.BalanceAmount;
            }

            if (reversalAmount == true) {
                //$scope.Payment.order.OrderTotal
                
                if (PayBalanceAmount > $scope.CorrectBalanceDueAmount) {
                    $scope.OverPayment = $scope.OrginalOverPayment;
                    if ($scope.DueAmount) {
                        $scope.Payment.order.BalanceDue = $scope.DueAmount;
                    }
                    else
                        $scope.Payment.order.BalanceDue = $scope.PaidAmount - $scope.OrginalOverPayment;
                    //$scope.Payment.Amount = $scope.PaidAmount;
                    // HFC.DisplayAlert("Balance due amount exceed from total");
                    $scope.Showerror = true;
                    return;
                }
                else {
                    if (PayBalanceAmount == 0) {
                        $scope.OverPayment = $scope.OrginalOverPayment;
                        $scope.Payment.order.BalanceDue = 0;
                    }
                    else {
                        
                        if ($scope.OrginalOverPayment)
                        {
                            $scope.OverPayment = $scope.OrginalOverPayment - PayBalanceAmount;
                        if ($scope.OverPayment < 0)
                        {
                            //$scope.Payment.order.BalanceDue = $scope.Payment.order.OrderTotal + ($scope.OverPayment);
                            $scope.Payment.order.BalanceDue = -($scope.OverPayment);
                            $scope.OverPayment = 0;
                        }
                        else $scope.Payment.order.BalanceDue = 0;
                        }
                        else {
                            //$scope.Payment.order.BalanceDue = $scope.CorrectBalanceDueAmount - PayBalanceAmount;
                            if (Amount)
                            {
                                var paidamnt = $scope.PaidAmount - Amount;
                                //var bal = $scope.CorrectBalanceDueAmount - paidamnt;
                                $scope.Payment.order.BalanceDue = paidamnt;
                                if ($scope.Payment.order.BalanceDue < 0) {
                                    $scope.OverPayment = -($scope.Payment.order.BalanceDue);
                                }
                                else $scope.OverPayment = 0;
                                //$scope.Payment.order.BalanceDue = $scope.CorrectBalanceDueAmount - Amount;
                            }
                            else $scope.Payment.order.BalanceDue = $scope.DueAmount; // will be of the due amount itself
                        }
                        
                    }
                    //$scope.OverPayment = $scope.OverPayment - PayBalanceAmount;
                    //$scope.Payment.order.BalanceDue = $scope.CorrectBalanceDueAmount - PayBalanceAmount;
                }
            }
            else $scope.Payment.order.BalanceDue = PayBalanceAmount;
            $scope.$apply(); //added since after intro kendo numeric txt box balnce is not updated in ui.
            //$scope.Payment.order.OrderTotal;
        }
        
        $scope.GetPaymentReversalReason = function () {

            $http.get('/api/orders/0/OrderReversalReason').then(function (data) {
                $scope.ReversalReason = data.data;
            });
        }

        $scope.GetPaymentReversalReason();

        $scope.GetOrderPaymentMethods = function () {

            $http.get('/api/orders/0/OrderPaymentMethods').then(function (data) {
                $scope.PaymentMethods = data.data;
            });
        }
        $scope.Payment = [];
        $scope.GetOrderPaymentMethods();
        $scope.PopulateAmount = function () {
            if ($scope.Payment.PayBalance == false) {
                $scope.Payment.Amount = '';
                $scope.Payment.order.BalanceDue = $scope.BalanceAmount;
            }
            else {
                $scope.Payment.chkReversal = false;
                if ($scope.BalanceAmount < 0) {
                    $scope.Payment.Amount = '';
                    $scope.Payment.order.BalanceDue = $scope.BalanceAmount;
                }
                else {
                    $scope.Payment.Amount = $scope.BalanceAmount;
                    $scope.Payment.order.BalanceDue = 0;
                }
                    
            }

        }
        if ($routeParams.orderId > 0) {
            
            var loadingElement = document.getElementById("loading");
            loadingElement.style.display = "block";
            $http.get('/api/orders/' + $routeParams.orderId + '/OrderPayments').then(function (response) {
                $scope.Payment = response.data;
                $scope.ResetPaymentForNew();
                $scope.BalanceAmount = $scope.Payment.order.BalanceDue;
                $scope.OverPayment = $scope.Payment.OverPaymentAmount;
                if (reversalPayment == true) {
                    $scope.Payment.chkReversal = true;
                    $scope.PayReversalAmount();
                }
                if (($scope.Payment.order.BalanceDue == $scope.Payment.order.OrderTotal) && ($scope.Payment.order.OverPaymentAmount != 0 || $scope.Payment.order.OverPaymentAmount == 0))
                {
                    $scope.ShowReversalBox = true;
                }
                else {
                    $scope.ShowReversalBox = false;
                }
                var loadingElement = document.getElementById("loading");
                loadingElement.style.display = "none";
            });

        }

        $scope.showPopUp = function (modalId) {
            $("#" + modalId).modal("show");
            return;
        }

        $scope.CancelOverPayment = function (modalId) {
            $("#" + modalId).modal("hide");
            //$scope.IsBusy = false;
        }

        $scope.SaveOrderpayment = function (operation) {

            $scope.isSubmitted = true;
            var reversalAmount = document.getElementById('chkReversal').checked;

            var dto = $scope.Payment;

            //earlier we are using after intoducing amount as kendo numeric this not needed.
            //if (isNaN(dto.Amount)) {
            //    HFC.DisplayAlert("Amount should be an numeric value");
            //    return;               
            //}

            

            if (reversalAmount == true) {
                if (dto.ReasonCode == 0 || dto.ReasonCode == null || dto.ReasonCode == undefined || dto.ReasonCode == '') {
                    HFC.DisplayAlert("Reason required");
                    return;
                }
            }

            if (dto.PaymentMethod == 0) {
                HFC.DisplayAlert("Payment method required");
                return;
            }
            //added the validation from below method.
            if (dto.Amount == 0 || dto.Amount == null || dto.Amount == undefined || dto.Amount == "") {
                HFC.DisplayAlert("Payment amount required");
                return;
            }

            if (reversalAmount == false) {
                var BalanceAmount = $scope.BalanceAmount;
                var Amount = parseFloat(dto.Amount);

                if (BalanceAmount < Amount) {
                    //HFC.DisplayAlert("Payment should not exceed balance due.");
                    $scope.showPopUp("overPaymentAmount");
                    return;
                }
            }

            $scope.MakePayment(operation);
        }

        $scope.MakePayment = function (operation) {

            $scope.CancelOverPayment('overPaymentAmount');
            $scope.isSubmitted = true;
            var reversalAmount = document.getElementById('chkReversal').checked;

            var dto = $scope.Payment;
            //if (dto.Amount == 0 || dto.Amount == null || dto.Amount == undefined || dto.Amount == "") {
            //    HFC.DisplayAlert("Payment amount required");
            //    return;
            //}
            if (reversalAmount == true) {
                dto.Amount = parseFloat(dto.Amount);
                if (dto.Amount > $scope.datavalue) {
                    HFC.DisplayAlert("Amount should not exceed actual amount of " + $scope.HFCService.CurrencyFormat($scope.datavalue));
                    return;
                }
            }

            if (reversalAmount == true) {
                if (dto.Memo == null || dto.Memo == undefined || dto.Memo == '') {
                    HFC.DisplayAlert("Memo is required");
                    return;
                }
            }
            if (dto.PaymentDate == null || dto.PaymentDate == undefined || dto.PaymentDate == '') {
                HFC.DisplayAlert("Payment date required");
                return;
            }
            //restrict saving for invalid date
            if (!$scope.ValidDate) {
                HFC.DisplayAlert("Invalid Date!");
                return;
            }
                       
            //if (dto.Memo == null || dto.Memo == undefined || dto.Memo == '') {
            //    HFC.DisplayAlert("Memo required");
            //    return;
            //}
            if (dto.PaymentMethod == 2) {
                if (dto.CheckNumber == null) {
                    HFC.DisplayAlert("Check number required");
                    return;
                }
                dto.VerificationCheck = dto.CheckNumber;
            }
            else if (dto.PaymentMethod == 3 || dto.PaymentMethod == 4 || dto.PaymentMethod == 5 ||
                dto.PaymentMethod == 6 || dto.PaymentMethod == 7 || dto.PaymentMethod == 8 || dto.PaymentMethod == 9) {
                if (dto.VerificationCheck == null) {
                    HFC.DisplayAlert("Verification required");
                    return;
                }
                dto.VerificationCheck = dto.VerificationCheck;
            }
            else if (dto.PaymentMethod == 10) {
                if (dto.Authorization == null) {
                    HFC.DisplayAlert("Authorization required");
                    return;
                }
                dto.VerificationCheck = dto.Authorization;
            }
            else {
                dto.VerificationCheck = '';
            }
            $scope.Payment_addint.$setPristine();
            $scope.submitAction = operation;
            $scope.IsBusy = true;
            if (reversalAmount == true) {


                $http.put('/api/orders/0/AddPayment', dto).then(function (response) {
                    $scope.IsBusy = false;

                    if ($routeParams.orderId > 0) {
                        HFC.DisplaySuccess("Reverese Payment Created");
                    }
                    $scope.CheckBalanceAmount = $scope.Payment.order.OrderTotal - $scope.Payment.order.BalanceDue;
                    if (operation == 1) {
                        //srv.Payment = null;
                        if ($scope.OrderService.redirectFlag && $scope.CheckBalanceAmount == 0) {
                            $scope.OrderService.orderStatusUpdateMethod($scope.OrderService.orderDTOBackup);
                            if ($scope.OrderService.mpoFlag)
                                window.location = '#!/Orders/' + $routeParams.orderId;
                        }
                        else if (!$scope.OrderService.redirectFlag) {
                            window.location = '#!/Orders/' + $routeParams.orderId;
                        }
                        else {
                            //$scope.Payment.order.BalanceDue
                            if ($scope.CheckBalanceAmount != 0)
                                alert("The Reverse payment is not completely done. So We are unabe to cancel / void the order");
                            window.location = '#!/Orders/' + $routeParams.orderId;
                        }
                    }
                });
            }
            else {
                $scope.PaymentInvoiceService.Payment = $scope.Payment;
                $scope.PaymentInvoiceService.submitAction = $scope.submitAction;
                $scope.PaymentInvoiceService.OrderId = $routeParams.orderId;
                $scope.PaymentInvoiceService.ShowInvoice('PaymentInvoice');
                $scope.IsBusy = false;
            }
            //$scope.PaymentInvoiceService.Payment = $scope.Payment;
            //$scope.PaymentInvoiceService.submitAction = $scope.submitAction;
            //$scope.PaymentInvoiceService.OrderId = $routeParams.orderId;
            //$scope.PaymentInvoiceService.ShowInvoice('PaymentInvoice');
           
        }

        $scope.RedirectToOrderDetails = function () {
            window.location = '#!/Orders/' + $routeParams.orderId;
        }

        //$scope.PayReversalAmount = function () {
        //    if ($scope.Payment.chkReversal == false) {
        //        $scope.Payment.Amount = '';
        //    }
        //    else {
        //        $scope.Id = $routeParams.orderId;
        //        $http.get('/api/Payments/0/ReversePaymentsAmount?OrderId=' + $scope.Id).then(function (response) {
        //            var response;
        //            $scope.datavalue = response.data.RevesalAmount;
        //            $scope.Payment.Amount = response.data.RevesalAmount;
        //            $scope.Payment.PayBalance = false;

        //        });
        //        //$scope.Payment.Amount = $scope.Payment.order.BalanceDue;
        //    }
        //}

        $scope.PayReversalAmount = function () {
            $scope.Showerror = false;
            if ($scope.Payment.chkReversal == false) {
                 //$scope.Payment.order.BalanceDue = $scope.Payment.order.BalanceDue - $scope.Payment.Amount;
                        $scope.Payment.order.BalanceDue = $scope.Payment.order.OrderTotal - $scope.PaidAmount;
                        if ($scope.Payment.order.BalanceDue < 0) {
                            $scope.OverPayment = -($scope.Payment.order.BalanceDue);
                            //$scope.Payment.order.BalanceDue = 0; commented since balnc to be in -ve value
                        }
                    $scope.Payment.Amount = '';
            }
            else {
                $scope.Id = $routeParams.orderId;
                $http.get('/api/Payments/0/ReversePaymentsAmount?OrderId=' + $scope.Id).then(function (response) {
                    var response;
                    $scope.datavalue = response.data.RevesalAmount;
                    $scope.Payment.Amount = response.data.RevesalAmount;
                    $scope.Payment.PayBalance = false;
                    //if over payed and balance due amount is 0.
                    if ($scope.Payment.order.BalanceDue == 0) {
                        //$scope.Payment.order.BalanceDue = 0;
                        $scope.PaidAmount = $scope.Payment.Amount;
                        $scope.OverPayment = $scope.Payment.Amount - $scope.Payment.order.OrderTotal; //temp varialbe used for cal
                        $scope.OrginalOverPayment = $scope.OverPayment; //temp varialbe used for cal
                       
                        $scope.Payment.order.BalanceDue = $scope.Payment.Amount - $scope.OverPayment;
                        if ($scope.OrginalOverPayment > 0) {
                            $scope.OverPayment = 0;
                            //$scope.Payment.OverPaymentAmount //data from db. 
                        }
                        //$scope.CorrectBalanceDueAmount = $scope.Payment.order.BalanceDue + $scope.Payment.Amount; 
                        $scope.CorrectBalanceDueAmount = $scope.Payment.Amount;//temp varialbe used for cal
                    }
                        //not paid full but have balance amount to be paid.
                    else {
                        $scope.DueAmount = $scope.Payment.order.BalanceDue; //temp varialbe used for cal
                        $scope.Payment.order.BalanceDue = $scope.Payment.order.BalanceDue + $scope.Payment.Amount;
                        $scope.CorrectBalanceDueAmount = $scope.Payment.order.BalanceDue; //temp varialbe used for cal

                        $scope.OverPayment = 0; $scope.OrginalOverPayment = $scope.OverPayment;
                        $scope.PaidAmount = $scope.Payment.Amount;
                    }
                                           
                    setTimeout(function () {
                        //amount format
                        $scope.CostCurrencyControl.CurrenctFormatControl($scope.Payment.Amount);
                    }, 0);
                    //$scope.CorrectBalanceDueAmount = $scope.Payment.order.BalanceDue;
                });
                //$scope.Payment.Amount = $scope.Payment.order.BalanceDue;
            }
        }

        //for date validation
        $scope.ValidateDate = function (date, id) {
            $scope.ValidDate = HFCService.ValidateDate(date, "date");

            if ($scope.ValidDate) {
                $("#" + id + "").removeClass("invalid_Date");
                var a = $('#' + id + '').siblings();
                $(a[0]).removeClass("icon_Background");
                $("#" + id + "").addClass("frm_controllead1");
            }
            else {
                $("#" + id + "").addClass("invalid_Date");
                var a = $('#' + id + '').siblings();
                $(a[0]).addClass("icon_Background");
                $("#" + id + "").removeClass("frm_controllead1");
            }
        }

    }
]);

