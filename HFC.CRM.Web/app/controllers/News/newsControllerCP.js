﻿appCP.controller('newsControllerCP', ['$http', '$scope', '$window'
   , '$rootScope'
   , 'HFCService', 'NewsService'
   , '$routeParams', '$sce', 'NavbarServiceCP'
   , function ($http, $scope, $window
       , $rootScope
       , HFCService, NewsService
       , $routeParams, $sce, NavbarServiceCP) {

       $scope.NavbarServiceCP = NavbarServiceCP;
       //$scope.NavbarServiceCP.newsAndUpdatesTab();
       $scope.NavbarServiceCP.SetupAdministrationInfoTab();      

       $scope.HFCService = HFCService;

       $scope.Permission = {};
       var NewsUpdatepermission = $scope.HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'NewsUpdates')

       $scope.Permission.ViewNews = NewsUpdatepermission.CanRead;
       $scope.Permission.AddNews =  NewsUpdatepermission.CanCreate;
       $scope.Permission.EditNews = NewsUpdatepermission.CanUpdate;

       $scope.newsService = NewsService;

       var newsId = $routeParams.newsId;

       HFCService.setHeaderTitle("News & Updates #");
       $scope.News = {};
       $scope.News.NewsUpdatesId = 0;
       //flag for date validation
       $scope.ValidStartDate = true;
       $scope.ValidEndDate = true;

       $scope.addNews = function () {

           var url = '#!/news';
           $window.open(url, '_self');
       }

       // TODO: THe following method hleps to avoid pasting an image in the Kendo
       // html editor.
       //////https://www.telerik.com/forums/disable-image-paste-functionality-in-kendo-editor
       ////$scope.onPaste = function (e) {
       ////    
       ////    if ((/^<img src="data:image/).test(e.html)) {
       ////        e.html = "";
       ////    }
       ////}

       $scope.redirecttoEditNews = function () {
            
           //var url = "http://" + $window.location.host + "/#!/leadsEdit/" + $scope.Lead.LeadId;
           $window.location.href = "#!/newsedit/" + $scope.News.NewsUpdatesId;
           // window.location.href = url;
       };

       if (newsId > 0) {
           $scope.newsService.getNews(newsId).then(function (response) {
                
               var result = response.data;
               if (result) {
                   $scope.News = result[0];

                   //https://stackoverflow.com/questions/18340872/how-do-you-use-sce-trustashtmlstring-to-replicate-ng-bind-html-unsafe-in-angu
                   $scope.News.trustedMessage = $sce.trustAsHtml($scope.News.Message);
                   $scope.News.trustedShortMessage = $sce.trustAsHtml($scope.News.ShortMessage);

                   // TP-2311: News & Updates entries need to be concept specific
                   
                   $scope.News.appliesToBB = $scope.appliesTo($scope.News.Brands, 1);
                   $scope.News.appliesToTL = $scope.appliesTo($scope.News.Brands, 2);
                   $scope.News.appliesToCC = $scope.appliesTo($scope.News.Brands, 3);

                   setTimeout(function () {
                       $scope.DispalytoChange1();
                       $scope.DispalyFromChange1();
                   }, 3000);
                   
               }
           },
           function (response) {
               HFC.DisplayAlert(response.statusText);
               $scope.IsBusy = false;
           });
       }

       $scope.appliesTo = function (source, brandid) {
           var result = $.grep(source, function (s) {
               return s.Id == brandid;
           });

           return result[0].IsApplicable;
       }

       $scope.cancel = function () {
           $scope.showValidate = false;
           var newsId = $scope.News.NewsUpdatesId;
           if (newsId > 0) {
               window.location.href = '#!/news/' + newsId;
           }
           else {
               window.location.href = '#!/listNews';
           }
       }

       $scope.DispalyFromChange = function() {
           if (new Date($scope.News.StartDate) != "Invalid Date") {
               var minDate = $("#NewsThroughDate").data("kendoDatePicker");
               minDate.min(new Date($scope.News.StartDate));
           }
           //for date validation
           if ($scope.News.StartDate == "")
               $scope.ValidStartDate = true;
           else
               $scope.ValidStartDate = HFCService.ValidateDate($scope.News.StartDate, "date");

           if ($scope.ValidStartDate) {
               $("#NewsFromDate").removeClass("gridinvalid_Date");
               var a = $('#NewsFromDate').siblings();
               $(a[0]).removeClass("icon_Background");
           }
           else {
               $("#NewsFromDate").addClass("gridinvalid_Date");
               var a = $('#NewsFromDate').siblings();
               $(a[0]).addClass("icon_Background");
           }
       }

       $scope.DispalytoChange = function () {
           if (new Date($scope.News.EndDate) != "Invalid Date") {
               var minDatee = $("#NewsFromDate").data("kendoDatePicker");
               minDatee.max(new Date($scope.News.EndDate));
           }
           //for date validation
           if ($scope.News.EndDate == "")
               $scope.ValidEndDate = true;
           else
               $scope.ValidEndDate = HFCService.ValidateDate($scope.News.EndDate, "date");

           if ($scope.ValidEndDate) {
               $("#NewsThroughDate").removeClass("gridinvalid_Date");
               var a = $('#NewsThroughDate').siblings();
               $(a[0]).removeClass("icon_Background");
           }
           else {
               $("#NewsThroughDate").addClass("gridinvalid_Date");
               var a = $('#NewsThroughDate').siblings();
               $(a[0]).addClass("icon_Background");
           }
       }

       $scope.DispalyFromChange1 = function () {
           if ($scope.News.StartDate) {
           var minDate = $("#NewsThroughDate").data("kendoDatePicker");
           minDate.min(new Date($scope.News.StartDate));
           }
       }

       $scope.DispalytoChange1 = function () {
           if ($scope.News.EndDate) {
               var minDatee = $("#NewsFromDate").data("kendoDatePicker");
               minDatee.max(new Date($scope.News.EndDate));
           }
       }

      
       $scope.saveNews = function () {
            
           if ($scope.IsBusy) return;
           $scope.IsBusy = true;

           $scope.submitted = true;

           // Check whether the forms contain value for all the required fields.
           // when any one of the filed is not given a value the following will return true.
           if ($scope.formnews.$invalid) {
               // HFC.DisplaySuccess("Please fill all the required fields! and valid data in optional fields if provided.");
               $scope.IsBusy = false;
               return;
           }

           //TP-3411: Date Select Validation issue in News and Updates
           var from = document.getElementById("NewsFromDate").value;
           var through = document.getElementById("NewsThroughDate").value;
           //var dateFormat = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
           //if ((from && !dateFormat.test(from)) || (through && !dateFormat.test(through)))
           //{
           //    HFC.DisplayAlert("Invalid Date Format");
           //    $scope.IsBusy = false;
           //    return;
           //}

           //restrict saving for invalid date
           if (!$scope.ValidStartDate && !$scope.ValidEndDate ) {
               HFC.DisplayAlert("Invalid Date!");
               $scope.IsBusy = false;
               return;
           }
           else if (!$scope.ValidStartDate) {
               HFC.DisplayAlert("Invalid Display From Date!");
               $scope.IsBusy = false;
               return;
           }
           else if (!$scope.ValidEndDate) {
               HFC.DisplayAlert("Invalid Display Through Date!");
               $scope.IsBusy = false;
               return;
           }

           if ($scope.News.StartDate && $scope.News.EndDate)
           {               
               if (new Date($scope.News.StartDate) > new Date($scope.News.EndDate)) {
                     $scope.IsBusy = false;
                     HFC.DisplayAlert("Display From Date should be less than Display Through Date");
                     return;
               }
           }

           
           // TP-2311: News & Updates entries need to be concept specific
           var brands = [];
           var brand = {};
           brand.Id = 1;
           brand.IsApplicable = $scope.News.appliesToBB;
           brands.push(brand);

           
           brand = angular.copy(brand);
           brand.Id = 2;
           brand.IsApplicable = $scope.News.appliesToTL;
           brands.push(brand);

           brand = angular.copy(brand);
           brand.Id = 3;
           brand.IsApplicable = $scope.News.appliesToCC;
           brands.push(brand);
           $scope.News.Brands = brands;

           var promize;
           // This is an existing  news.
           if ($scope.News.NewsUpdatesId) {
               promize = $scope.newsService.updateNews($scope.News);
           } else {
               promize = $scope.newsService.addNews($scope.News);
           }

           promize.then(function (response) {
               $scope.formnews.$setPristine();
               window.location.href = '#!/news/' + response.data.NewsUpdatesId;
           },
           function (response) {
               HFC.DisplayAlert(response.statusText);
               $scope.IsBusy = false;
           })
       };
   }]);