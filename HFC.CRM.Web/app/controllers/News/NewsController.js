﻿app.controller('newsController', ['$http', '$scope', '$window'
   , '$rootScope'
   , 'HFCService', 'NewsService'
   , '$routeParams', '$sce',
    function ($http, $scope, $window
       , $rootScope
       , HFCService, NewsService
       , $routeParams, $sce) {

       $scope.HfcService = HFCService;

       $scope.newsService = NewsService;

       var newsId = $routeParams.newsId;

       $scope.News = {};
       $scope.News.NewsUpdatesId = 0;

       //$scope.addNews = function () {

       //    var url = '#!/news';
       //    $window.open(url, '_self');
       //}

       //$scope.redirecttoEditNews = function () {
       //  $window.location.href = "/#!/newsedit/" + $scope.News.NewsUpdatesId;
       //};

       if (newsId > 0) {
           $scope.newsService.getNews(newsId).then(function (response) {
                
               var result = response.data;
               if (result) {
                   $scope.News = result[0];

                   //https://stackoverflow.com/questions/18340872/how-do-you-use-sce-trustashtmlstring-to-replicate-ng-bind-html-unsafe-in-angu
                   $scope.News.trustedMessage = $sce.trustAsHtml($scope.News.Message);
                   $scope.News.trustedShortMessage = $sce.trustAsHtml($scope.News.ShortMessage);

                   // TP-2311: News & Updates entries need to be concept specific
                   
                   $scope.News.appliesToBB = $scope.appliesTo($scope.News.Brands, 1);
                   $scope.News.appliesToTL = $scope.appliesTo($scope.News.Brands, 2);
                   $scope.News.appliesToCC = $scope.appliesTo($scope.News.Brands, 3);
               }
           },
           function (response) {
               HFC.DisplayAlert(response.statusText);
               $scope.IsBusy = false;
           });
       }

       $scope.appliesTo = function (source, brandid) {
              var result = $.grep(source, function (s) {
                   return s.Id == brandid;
              });

              return result[0].IsApplicable;
       }

       //$scope.cancel = function () {
       //    $scope.showValidate = false;
       //    var newsId = $scope.News.NewsUpdatesId;
       //    if (newsId > 0) {
       //        window.location.href = '#!/news/' + newsId;
       //    }
       //    else {
       //        window.location.href = '#!/listNews';
       //    }
       //}

       //$scope.saveNews = function () {
            
       //    if ($scope.IsBusy) return;
       //    $scope.IsBusy = true;

       //    $scope.submitted = true;
       //    if ($scope.formnews.$invalid) {
       //       $scope.IsBusy = false;
       //        return;
       //    }

       //    var promize;
       //    // This is an existing  news.
       //    if ($scope.News.NewsUpdatesId) {
       //        promize = $scope.newsService.updateNews($scope.News);
       //    } else {
       //        promize = $scope.newsService.addNews($scope.News);
       //    }

       //    promize.then(function (response) {
       //        $scope.formnews.$setPristine();
       //        window.location.href = '#!/news/' + response.data.NewsUpdatesId;
       //    },
       //    function (response) {
       //        HFC.DisplayAlert(response.statusText);
       //        $scope.IsBusy = false;
       //    })
       //};
   }]);
