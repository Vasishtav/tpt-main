﻿appCP.controller('newsListingController', ['$http', '$scope', '$window'
    , '$rootScope'
    //, 'NavbarService'
    , 'HFCService'
    , 'NewsService', 'NavbarServiceCP'
    , function ($http, $scope, $window
        , $rootScope
        //, NavbarService
        , HFCService
        , NewsService, NavbarServiceCP
        ) {
        
        $scope.NavbarServiceCP = NavbarServiceCP;
        //$scope.NavbarServiceCP.newsAndUpdatesTab();
        $scope.NavbarServiceCP.SetupAdministrationInfoTab();

        $scope.HfcService = HFCService;
        var offsetvalue = 0;
        $scope.Permission = {};
        var NewsUpdatepermission = $scope.HfcService.CurrentUserPermissions.find(x => x.ModuleCode == 'NewsUpdates')
        if (NewsUpdatepermission){
        $scope.Permission.ViewNews = NewsUpdatepermission.CanRead;
        $scope.Permission.AddNews =  NewsUpdatepermission.CanCreate;
        $scope.Permission.EditNews = NewsUpdatepermission.CanUpdate;
        }

        HFCService.setHeaderTitle("News & Updates #");
        //
        $scope.newsService = NewsService;

        $scope.NewsGridOptions = {
            dataSource: {
                type: "jsonp",
                transport: {
                    cache: false,
                    read: function (e) {
                        //
                        var url = "api/newsupdates/0/GetNews";
                        //if ($scope.companyView == true) {
                        //    url = "api/calendar/0/GetAllEvents";
                        //}

                        $http.get(url, e).then(function (data) {
                            //
                            var result = data.data;

                            // When the user don't wan tot see the inactive records, apply filter
                            // to show only active news.
                            
                            // if (!$scope.showInactiveNews || $scope.showInactiveNews == false) {
                            if ($("#checkboxInactive").prop("checked") !== true){
                              result =   $.grep(result, function (s) {
                                    return s.IsActive == true;
                                });
                            }

                            e.success(result);
                        }).catch(function (error) {
                            var loadingElement = document.getElementById("loading");
                            loadingElement.style.display = "none";
                        });
                    },
                    parameterMap: function (options, operation) {
                        if (operation === "update") {
                            return options;
                        }
                    }
                },
                schema: {
                    model: {
                        //id: "CalendarId",
                        fields: {
                            IsActive: { type: "boolean" },
                            BB: { type: "boolean" },
                            TL: { type: "boolean" },
                            CC: { type: "boolean" },
                            StartDate: { type: "date" },
                            EndDate: { type: "date" },
                        }
                    }
                },
            },
            batch: true,
            cache: false,
            pageable: {
                refresh: true,
                pageSize: 25,
                pageSizes: [25, 50, 100],
                buttonCount: 5
            },
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        eq: "Is equal to",
                        neq: "Is not equal to", isempty: "Empty",
                        isnotempty: "Not empty"
                    },
                    date: {
                        gt: "After (Excludes)",
                        lt: "Before (Includes)"
                    },
                }
            },
            toolbar: kendo.template($(' <script id="template" type="text/x-kendo-template"><div class="tp-measurementsearch row no-margin"><div class="leadsearch_topleft col-sm-9 col-md-9 col-xs-9 no-padding row no-margin"><div class="col-xs-3 col-sm-3 no-padding hfc_txtbox"><input ng-keyup="filterNewsGrid()" type="search" id="searchBox" placeholder="Search News Updates" class="k-textbox leadsearch_tbox"></div><div class="col-xs-3 col-sm-3 measurement_search no-padding"><input type="button" id="btnReset" ng-click="clearFilter()" class="k-button btn btn-primary cancel_but" value="Clear All Filters"></div></div><div class="col-sm-3 col-md-3 col-xs-3 no-padding"><div class="float-right mt-2"><input class="option-input" id="checkboxInactive" ng-model="showInactiveNews" type="checkbox" ng-click="ShowAllNews()"><label class="ldet_label">Show Inactive</label></div></div></script>').html()),
            noRecords: { template: "No records found" },
            columns: [
                {
                    field: "",
                    title: "",
               
                width:60,
                    template: function (dataitem) {
                        return NewsUpDownTemplate(dataitem);
                    }//,
                    
                },
                {
                    field: "Headline"
                    , title: "Headline"
                    , template: "<a href='\\\\#!/news/#= NewsUpdatesId #' style='color: rgb(61,125,139);'><span>#= Headline #</span></a> ",
                },
                {
                    field: "StartDate"
                    , title: "Start",
                    type: "date",
                    filterable: HFCService.gridfilterableDate("date", "NewsListStartDateFilterCalendar", offsetvalue),
                     template: function (dataitem) {
                        if (dataitem.StartDate == null || dataitem.StartDate == '')
                            return ''
                        else
                            return HFCService.KendoDateFormat(dataitem.StartDate);
                    },
                        //"#= (StartDate == null|| StartDate == '')? '' : kendo.toString(kendo.parseDate(StartDate, 'yyyy-MM-dd'), 'M/d/yyyy')#",
                },
                {
                    field: "EndDate"
                    , title: "End"
                    , type: "date",
                    filterable: HFCService.gridfilterableDate("date", "NewsListEndDateFilterCalendar", offsetvalue),
                    template: function (dataitem) {
                        if (dataitem.EndDate == null || dataitem.EndDate == '')
                            return ''
                        else
                            return HFCService.KendoDateFormat(dataitem.EndDate);
                    },
                        //"#= (EndDate == null|| EndDate == '')? '' : kendo.toString(kendo.parseDate(EndDate, 'yyyy-MM-dd'), 'M/d/yyyy')#",
                },
                {
                    field: "IsActive"
                    , title: "Status"
                    , template: "\#= IsActive ? 'Active' : 'Inactive' \#",
                    filterable: { messages: { isTrue: "Active", isFalse: "Inactive" } }
                },
                {
                    field: "BB"
                    , title: "Budget Blinds"
                    , filterable: { messages: { isFalse: "False", isTrue: "True" } },
                    //filterable: { multi: true, search: true },
                    template: function (dataItem) {
                        if (dataItem.BB == true)
                            return '<div style="text-align:center;"><input disabled="disabled" checked type="checkbox" class="option-input checkbox center_checkbox"></div>'
                        else
                            return '<div style="text-align:center;"><input disabled="disabled" type="checkbox" class="option-input checkbox "></div>'
                    }
                },
                {
                    field: "TL"
                    , title: "Tailored Living"
                    , filterable: { messages: { isFalse: "False", isTrue: "True" } },
                    //filterable: { multi: true, search: true },
                    template: function (dataItem) {
                        if(dataItem.TL == true)
                            return '<div style="text-align:center;"><input disabled="disabled" checked type="checkbox" class="option-input checkbox "></div>'
                        else
                            return '<div style="text-align:center;"><input disabled="disabled" type="checkbox" class="option-input checkbox "></div>'
                    }
                },
                {
                    field: "CC"
                    , title: "Concrete Craft"
                    , filterable: { messages: { isFalse: "False", isTrue: "True" } },
                    //filterable: { multi: true, search: true },
                    template: function (dataItem) {
                        if (dataItem.CC == true)
                            return '<div style="text-align:center;"><input disabled="disabled" checked type="checkbox" class="option-input checkbox "></div>'
                        else
                            return '<div style="text-align:center;"><input disabled="disabled" type="checkbox" class="option-input checkbox "></div>'
                    }
                },
                {
                    field: "",
                    title: "",
                    template: function (dataitem) {
                        return NewsMenuTemplate(dataitem);
                    }//,
                    //width: "5%"
                }
                
            ]

        };

        $scope.ShowAllNews = function () {
            $("#NewsGrid").data("kendoGrid").dataSource.read();
        }

        $scope.addNews = function () {
            //
            var url = '#!/news';
            $window.open(url, '_self');
        }

        $scope.moveUp = function (newsId) {
            
            $scope.newsService.moveUp(newsId)
                .then(function (response) {
                    var result = response.data;
                    if (result) {
                        //$scope.News = result[0];
                        $("#NewsGrid").data("kendoGrid").dataSource.read();
                    }
                },
                function (response) {
                    HFC.DisplayAlert(response.statusText);
                    $scope.IsBusy = false;
                });
        }

        $scope.moveDown = function (newsId) {
            
            $scope.newsService.moveDown(newsId)
                .then(function (response) {
                    var result = response.data;
                    if (result) {
                        //$scope.News = result[0];
                        $("#NewsGrid").data("kendoGrid").dataSource.read();
                    }
                },
                function (response) {
                    HFC.DisplayAlert(response.statusText);
                    $scope.IsBusy = false;
                });
        }

        $scope.deactivate = function (newsId) {
            //
            $scope.newsService.deactivateNews(newsId).then(function (response) {
                var result = response.data;
                if (result) {
                    //$scope.News = result[0];
                    $("#NewsGrid").data("kendoGrid").dataSource.read();
                }
            },
            function (response) {
                HFC.DisplayAlert(response.statusText);
                $scope.IsBusy = false;
            });
        }

        $scope.activate = function (newsId) {
            //
            $scope.newsService.activateNews(newsId).then(function (response) {
                var result = response.data;
                if (result) {
                    //$scope.News = result[0];
                    $("#NewsGrid").data("kendoGrid").dataSource.read();
                }
            },
            function (response) {
                HFC.DisplayAlert(response.statusText);
                $scope.IsBusy = false;
            });
        }

        $scope.clearFilter = function () {
            //
            $('#searchBox').val('');
            $("#NewsGrid").data('kendoGrid').dataSource.filter({
            });
        }

        $scope.filterNewsGrid = function () {
            //
            var searchValue = $('#searchBox').val();
            $("#NewsGrid").data("kendoGrid").dataSource.filter({
                logic: "or",
                filters: [
                    {
                        field: "Headline",
                        operator: "contains",
                        value: searchValue
                    },
                  {
                      field: "AccountFullName",
                      operator: "contains",
                      value: searchValue
                  },
                 // {
                 //    field: "StartDate",
                 //    operator: "gt",
                 //  value: searchValue
                 // },
                 // {
                 //   field: "EndDate",
                 //   operator: "gt",
                 //    value: searchValue
                 //},
                 //{
                 //     field: "IsActive",
                 //     operator: "contains",
                 //   value: searchValue
                 // }
                   
                ]
            });
        }

        function NewsUpDownTemplate(dataitem) {
            
            // we no need to support up or down for inactive records.
            if (dataitem.IsActive == 0) return "";

            var template = "<div>";
            template += '<button type="button" class="plus_but" style="margin-right:10px; font-size:16px;" id="btnReset2" ng-click="moveDown(' + dataitem.NewsUpdatesId + ')"  value="down"><i class="fas fa-arrow-down"></i></button>';
            template += '<button type="button" class="plus_but" style="font-size:16px;" id="btnReset" ng-click="moveUp(' + dataitem.NewsUpdatesId + ')"  value="up++"><i class="fas fa-arrow-up"></i></button>';
            template += '</div>';
            return template;
        }


        function NewsMenuTemplate(dataitem) {
            //
            var template = "";
            if ($scope.Permission.EditNews) {


                //template += '<li ng-show="Permission.EditEvent==true"><a href="#!/newsEdit/' + dataitem.NewsUpdatesId + '">Edit</a></li>';
                template += '<li ><a href="#!/newsedit/' + dataitem.NewsUpdatesId + '">Edit</a></li>';

                if (dataitem.IsActive) {
                    template += '<li ><a href="javascript:void(0)" ng-click="deactivate(' + dataitem.NewsUpdatesId + ') ">Deactivate</a></li>';
                } else {
                    template += '<li ><a href="javascript:void(0)" ng-click="activate(' + dataitem.NewsUpdatesId + ') ">Activate</a></li>';
                }

                return '<ul> <li class="dropdown note1 ad_user"><div class="btn-group">' +
                            '<button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> ' +
                            '<i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i> </button> ' +
                            '<ul class="dropdown-menu pull-right">' + template + '</ul> </li></ul>'
            }
            else return template = "";
        }

}]);

