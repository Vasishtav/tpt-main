﻿/***************************************************************************\
Module Name:  Settings Roles Controller .js - AngularJS file
Project: HFC - TouchPoint
Created on: 08 January 2018 Monday
Created By:
Modified on:
Modified By:
Description: Roles Controller
\***************************************************************************/
'use strict';
app.controller('rolesController', ['$scope', '$routeParams', '$rootScope', '$window', '$location', '$http',
   
    function ($scope, $routeParams, $rootScope, $window, $location, $http
        ) {
        $http.get('api/lookup/0/GetRoles').then(function (response) {
            $scope.RolesList = response.data;
        });

        $scope.ReturnSettings = function () {
            history.go(-1);
        }

    }
]);

