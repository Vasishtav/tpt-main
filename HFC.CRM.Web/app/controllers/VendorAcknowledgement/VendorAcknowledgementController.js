﻿/***************************************************************************\
Module Name:  Disclaimer Controller  .js - AngularJS file
Project: HFC
Created on: 06 April 2020 Monday
Created By: Rajasekaran K
Copyright:
Description: Disclaimer Controller
Change History:
Date  By  Description

\***************************************************************************/
'use strict';
app.controller('VendorAcknowledgementController', ['$scope', '$routeParams', '$rootScope', '$window', '$location', '$http', 'NavbarService',
    'HFCService', 'kendotooltipService',
    function ($scope, $routeParams, $rootScope, $window, $location, $http, NavbarService,
        HFCService, kendotooltipService) {

        $scope.masterPO = $routeParams.POId;
        $scope.vendorReference = $routeParams.vendorReference;
        $scope.NavbarService = NavbarService;
        $scope.NavbarService.SelectOperation();
        $scope.NavbarService.EnableOperationsProcurementDashboardTab();

        HFCService.setHeaderTitle("Vendor Acknowledgement #" + $scope.vendorReference);

        $scope.MPORelatedIds = {
            VendorAckIds: [],
            ShipmentIds: [],
            VendorInvoiceIds: [],
            PurchaseOrderId: '',
        };
        $scope.GetMPORelatedIds = function () {
            $http.get('/api/PurchaseOrders/' + $scope.masterPO + '/GetMPORelatedIds').then(function (data) {
                var MPORelatedIds = data.data.data;
                if (MPORelatedIds.length > 0) {
                    for (var i = 0; i < MPORelatedIds.length; i++) {
                        if (MPORelatedIds[i].vendorReference) {
                            if (jQuery.isEmptyObject($scope.MPORelatedIds.VendorAckIds))
                                $scope.MPORelatedIds.VendorAckIds.push({ vendorReference: MPORelatedIds[i].vendorReference, PurchaseOrderId: MPORelatedIds[i].PurchaseOrderId });
                            else if ($scope.MPORelatedIds.VendorAckIds.length > 0) {
                                var exists = $scope.MPORelatedIds.VendorAckIds.some(el => el.vendorReference === MPORelatedIds[i].vendorReference);
                                if (!exists)
                                    $scope.MPORelatedIds.VendorAckIds.push({ vendorReference: MPORelatedIds[i].vendorReference, PurchaseOrderId: MPORelatedIds[i].PurchaseOrderId });
                            }
                        }
                        if (MPORelatedIds[i].ShipmentId) {
                            if (jQuery.isEmptyObject($scope.MPORelatedIds.ShipmentIds))
                                $scope.MPORelatedIds.ShipmentIds.push({ ShipmentId: MPORelatedIds[i].ShipmentId, ShipNoticeId: MPORelatedIds[i].ShipNoticeId });
                            else if ($scope.MPORelatedIds.ShipmentIds.length > 0) {
                                var exists = $scope.MPORelatedIds.ShipmentIds.some(el => el.ShipmentId === MPORelatedIds[i].ShipmentId);
                                if (!exists)
                                    $scope.MPORelatedIds.ShipmentIds.push({ ShipmentId: MPORelatedIds[i].ShipmentId, ShipNoticeId: MPORelatedIds[i].ShipNoticeId });
                            }
                        }

                        if (MPORelatedIds[i].VendorInvoiceId) {
                            if (jQuery.isEmptyObject($scope.MPORelatedIds.VendorInvoiceIds))
                                $scope.MPORelatedIds.VendorInvoiceIds.push({ VendorInvoiceId: MPORelatedIds[i].VendorInvoiceId, InvoiceNumber: MPORelatedIds[i].InvoiceNumber });
                            else if ($scope.MPORelatedIds.VendorInvoiceIds.length > 0) {
                                var exists = $scope.MPORelatedIds.VendorInvoiceIds.some(el => el.InvoiceNumber === MPORelatedIds[i].InvoiceNumber);
                                if (!exists)
                                    $scope.MPORelatedIds.VendorInvoiceIds.push({ VendorInvoiceId: MPORelatedIds[i].VendorInvoiceId, InvoiceNumber: MPORelatedIds[i].InvoiceNumber });
                            }
                        }
                        //$scope.mporelatedids.vendorackids = MPORelatedIds.find(mpo => mpo.vendorreference);
                    }
                    setTimeout(function () {
                        if ($scope.MPORelatedIds.ShipmentIds) {
                            $("#submenu-dropdown li").find('a:contains(' + $scope.vendorReference + ')').addClass('activeLi');
                        }
                    }, 100);
                }
            });
        }
        $scope.VendorAcknowledgementHeader = {};
        $scope.VendorAcknowledgementLineDetails = {};

        if ($scope.masterPO && $scope.vendorReference) {
            var loadingElement = document.getElementById("loading");
            loadingElement.style.display = "block";
            $scope.GetMPORelatedIds();
            $http.get("/api/VendorAcknowledgement/" + $scope.masterPO +"/GetDetails?vendorReferenceNumber=" + $scope.vendorReference).then(function (response) {
                $scope.VendorAcknowledgementHeader = response.data.VendorAcknowledgementHeader;
                $scope.VendorAcknowledgementLineDetails = response.data.VendorAcknowledgementLineDetails;
                $scope.GetVendorAcknowledgementView();
                loadingElement.style.display = "none";
            }, function (error) {
                HFC.DisplayAlert(error);
                loadingElement.style.display = "none";
            });
        }

        $scope.GetVendorAcknowledgementView = function () {
            $scope.GridEditable = false;
            $scope.VendorAcknowledgementView = {
                dataSource: {
                    data: $scope.VendorAcknowledgementLineDetails,
                    error: function (e) {
                        HFC.DisplayAlert(e.errorThrown);
                    },
                    batch: true,
                    schema: {
                        model: {
                            id: "Id",
                            fields: {
                                POLine: { type: 'number', editable: false },
                                QTY: { type: 'number', editable: false },
                                UoM: { type: 'string', editable: false },
                                Item: { type: 'number', editable: false },
                                Description: { type: 'string', editable: false },
                                QuotePrice: { type: 'number', editable: false },
                                Cost: { type: 'number', editable: false },
                                Discount: { type: 'number', editable: false },
                                Status: { type: 'string', editable: false },
                                EstShipDate: { type: 'Date', editable: false },
                                OrderNumber: { type: 'string', editable: false },
                                OrderLineNumber: { type: 'string', editable: false },
                            }
                        }
                    }
                },
                columns: [
                    {
                        field: "POLine",
                        title: "PO Line #",
                        width: "80px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.POLine + "</span>";
                        },
                    },
                    {
                        field: "QTY",
                        title: "QTY",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.NumberFormat(dataItem.QTY, 'number') + "</span>";
                        },
                    },
                    {
                        field: "UoM",
                        title: "UoM",
                        width: "50px",
                    },
                    {
                        field: "PartNumber",
                        title: "PartNumber",
                        width: "50px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.Item + "</span>";
                        },
                    },
                    {
                        field: "Description",
                        title: "Description",
                        width: "250px",
                        template: function (dataItem) {
                            return "<p>" + dataItem.Description + "</p>";
                        },
                    },
                    {
                        field: "QuotePrice",
                        title: "Quoted Price",
                        width: "100px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.QuotePrice) + "</span>";
                        },
                    },
                    {
                        field: "Discount",
                        title: "Cost",
                        width: "100px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.Discount) + "</span>";
                        },
                    },
                    //{
                    //    field: "Cost",
                    //    title: "Cost",
                    //    width: "100px",
                    //    template: function (dataItem) {
                    //        return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.Cost) + "</a>";
                    //    },
                    //},
                    {
                        field: "Status",
                        title: "Status",
                        width: "150px",
                    },
                    {
                        field: "EstShipDate",
                        title: "Est Ship Date",
                        width: "100px",
                        type: "date",
                        template: function (dataitem) {
                            if (dataitem.EstShipDate == null || dataitem.EstShipDate == '')
                                return "";
                            else
                                return HFCService.KendoDateFormat(dataitem.EstShipDate);
                        }
                    },
                    {
                        field: "OrderNumber",
                        title: "Order #",
                        width: "80px",
                        template: function (dataItem) {
                            return "<a class='hquote_hover ng-binding' href='#!/Orders/" + dataItem.OrderId + "'>" + dataItem.OrderNumber + ' - ' + dataItem.OrderSideMark + "</span>";
                        },
                    },
                    {
                        field: "OrderLineNumber",
                        title: "Order Line #",
                        width: "80px",
                        template: function (dataItem) {
                            return "<span class='Grid_Textalign'>" + dataItem.OrderLineNumber + "</span>";
                        },
                    }
                ],
                editable: false,
                resizable: true,
                noRecords: { template: "No records found" },
                scrollable: true,
            };
        };

        //$scope.GetVendorAcknowledgementView();

        var getUpdatedColumnList = function (e) {
            kendotooltipService.columnResizeTooltip(e.newWidth, e.column.headerAttributes.id);
        }
        // Kendo Resizing
        $scope.$on("kendoWidgetCreated", function (e) {
            //if ($window.location.href.toUpperCase().includes('VENDORINVOICEPPVLIST'))
            $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 295);
            window.onresize = function () {
                //if ($window.location.href.toUpperCase().includes('VENDORINVOICEPPVLIST')) {
                $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 295);
                setTimeout(function () {
                    $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 295);
                }, 100);
                //}
            };
        });
        // End Kendo Resizing
    }
]);

