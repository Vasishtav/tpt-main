﻿appCP.controller('FranchiseController', [
    '$scope', '$routeParams', '$rootScope', '$window', '$location', '$http', '$interval','$q'
    , 'HFCService', 'FranchiseService', 'AddressService', 'NavbarServiceCP', 'AddressValidationService'
    //, 'NavbarService'
    ,
    function (
        $scope, $routeParams, $rootScope, $window, $location, $http, $interval,$q
        , HFCService, FranchiseService, AddressService, NavbarServiceCP, AddressValidationService
        //, NavbarService//
    ) {

        $scope.NavbarServiceCP = NavbarServiceCP;
        $scope.NavbarServiceCP.SetupFranchiseInfoTab();

        $scope.HFCService = HFCService;
        $scope.AddressService = AddressService;
        // $scope.NavbarService = NavbarService;
        $scope.FranchiseService = FranchiseService;
        $scope.FrachiseId = $routeParams.FranchiseId;
        $scope.Viewmode = false;
        if ($window.location.href.indexOf("franchiseView")) $scope.Viewmode = true;

        $scope.BusinessAddress = {};
        $scope.OwnerNameData = [];
        $scope.submitted = false;
        HFCService.setHeaderTitle("Franchise #");
        $scope.Permission = {};
        var Franchisepermission = $scope.HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'Franchises')
        var Impersonatepermission = Franchisepermission.SpecialPermission.find(x => x.PermissionCode == 'Impersonation').CanAccess;

        $scope.Permission.ViewFe = Franchisepermission.CanRead;
        $scope.Permission.AddFe = Franchisepermission.CanCreate;
        $scope.Permission.EditFe = Franchisepermission.CanUpdate;
        $scope.Permission.Impersonatepermission = Impersonatepermission;
        //flag for date validation
        $scope.ValidDate = true;
        $scope.ValidDatelist = [];

        //QB Button text toggle
        $scope.qbbtntext = 'Enable QB Sync';
        $scope.QBText = function () {
            if ($scope.Franchise.IsQBEnabled == undefined || $scope.Franchise.IsQBEnabled == null || $scope.Franchise.IsQBEnabled == false) {
                $scope.qbbtntext = 'Enable QB Sync';
            }
            if ($scope.Franchise.IsQBEnabled == true) {
                $scope.qbbtntext = 'QB added';
            }
        };


        if ($scope.FrachiseId > 0) {

            $http.get('/api/franchise/' + $scope.FrachiseId).then(function (response) {

                $scope.Franchise = response.data.Franchise;

                $scope.OwnerNameData = $scope.Franchise.lstOwnerName;
                $scope.TimezoneValue = $scope.Franchise.TimezoneValue;

                $scope.GetFranchiseSetUpOwner();

                $scope.BusinessAddress.CountryCode2Digits = $scope.Franchise.CountryCode;
                $scope.BusinessAddress = $scope.Franchise.Address;

                $scope.BusinessAddress.Address1 = $scope.Franchise.Address.Address1;
                $scope.$broadcast('angucomplete-alt:changeInput', 'autocomp_', $scope.BusinessAddress.Address1);
                if ($scope.Viewmode == true) $scope.GetFranchiseViewOwner();
                HFCService.setHeaderTitle("Franchise #" + $scope.Franchise.FranchiseId + "-" + $scope.Franchise.Name);
                $scope.QBText();
            });

        }

        function validateAddress(address) {
            var addressModeltp = {
                address1: address.Address1,
                address2: address.Address2,
                City: address.City,
                StateOrProvinceCode: address.State,
                PostalCode: address.ZipCode,
                CountryCode: address.CountryCode2Digits
            };

            if (!addressModeltp.address1 || addressModeltp.address1 == "") {
                var result = {
                    data: {
                        Status: "SkipValidation",
                        Message: "No Address validation required."
                    },
                    status: 200,
                    statusText: "OK"
                }

                return $q.resolve(result);;
            }

            // Call the api to Return the result.
            return AddressValidationService.validateAddress(addressModeltp);
        }



        //This grid for territory and owner view
        if ($scope.Viewmode = true) {
            // Territory
            $scope.FranchiseViewTerritory = {
                dataSource: {
                    transport: {
                        read: {

                            url: '/api/lookup/' + $scope.FrachiseId + '/GetTerritoryByFranchiseId',
                        },

                    },
                },
                columns: [
                    {
                        field: "TerritoryId",
                        title: "Territory ID",
                        filterable: { multi: true, search: true },
                        hidden: true,
                    },
                    {
                        field: "Code",
                        title: "Territory Code",
                        filterable: { multi: true, search: true },
                        hidden: false,
                    },
                    {
                        field: "Name",
                        title: "Territory Name",
                        filterable: { multi: true, search: true },
                        hidden: false,
                    },
                    {
                        field: "Minion",
                        title: "Minion ID",
                        filterable: { multi: true, search: true },
                        hidden: false,
                    },
                    {
                        field: "FranchiseId",
                        title: "Franchise ID",
                        hidden: true,
                    },
                    {
                        field: "WebSiteURL",
                        title: "Website URL",
                    },
                ],

            };

            //Owner name
            $scope.GetFranchiseViewOwner = function () {
                $scope.FranchiseViewOwner = {
                    dataSource: {
                        transport: {
                            read: function (e) {
                                if ($scope.OwnerNameData != null)
                                    e.success($scope.OwnerNameData);
                            },
                        },
                    },

                    columns: [
                        { field: "FirstName", title: "First Name" },
                        { field: "LastName", title: "Last Name" },
                    ],
                };
            }
        }
        //This Grid is for the Territory Add and Edit 
        $scope.GetFranchiseSetUpTerritory = function () {
            $scope.FranchiseSetupTerritory = {
                dataSource: {
                    transport: {
                        read: {

                            url: '/api/lookup/' + $scope.FrachiseId + '/GetTerritoryByFranchiseId',
                        },
                        update: {
                            url: '/api/Franchise/0/saveTerritory',
                            type: "POST",
                            complete: function (e) {
                                if (e.responseJSON.Result == "Success")
                                    HFC.DisplaySuccess("Territory Updated sucessfully.");
                                else
                                    HFC.DisplayAlert(e.responseJSON.Result);
                                $("#gridFranchiseSetupTerritory").data("kendoGrid").dataSource.read();
                            }

                        },
                        create: {
                            url: '/api/Franchise/0/saveTerritory',
                            type: "POST",
                            complete: function (e) {

                                if (e.responseJSON.Result == "Success")
                                    HFC.DisplaySuccess("New Territory added sucessfully.");
                                else
                                    HFC.DisplayAlert(e.responseJSON.Result);

                                $("#gridFranchiseSetupTerritory").data("kendoGrid").dataSource.read();
                            }

                        },
                        destroy: {
                            url: '/api/Franchise/0/DeleteTerritory',
                            type: "POST",
                            complete: function (e) {
                                $("#gridFranchiseSetupTerritory").data("kendoGrid").dataSource.read();
                                HFC.DisplaySuccess("New Campaign added sucessfully.");
                            }
                        },
                        parameterMap: function (options, operation) {
                            if (operation === "create") {

                                return {
                                    TerritoryId: options.TerritoryId,
                                    Code: options.Code,
                                    Name: options.Name,
                                    Minion: options.Minion,
                                    WebSiteURL: options.WebSiteURL,
                                    FranchiseId: $scope.FrachiseId,
                                };
                            }
                            else if (operation === "update") {

                                return {

                                    TerritoryId: options.TerritoryId,
                                    Code: options.Code,
                                    Name: options.Name,
                                    Minion: options.Minion,
                                    WebSiteURL: options.WebSiteURL,
                                    FranchiseId: $scope.FrachiseId,
                                };
                            }
                            else if (operation === "destroy") {

                                return {

                                    TerritoryId: options.TerritoryId,
                                    Code: options.Code,
                                    Name: options.Name,
                                    Minion: options.Minion,
                                    WebSiteURL: options.WebSiteURL,
                                    FranchiseId: $scope.FrachiseId,
                                };
                            }

                        }
                    },
                    error: function (e) {
                        HFC.DisplayAlert(e.errorThrown);
                    },

                    schema: {
                        model: {
                            id: "TerritoryId",
                            fields: {
                                TerritoryId: { editable: false, nullable: true },
                                Code: { editable: true, nullable: true },
                                Name: { validation: { required: true } },
                                Minion: { editable: true, validation: { required: true } },
                                WebSiteURL: { editable: true, validation: { required: true } },
                                FranchiseId: { editable: false },


                            }
                        }
                    }


                },

                resizable: true,
                toolbar: ["create"],
                columns: [
                    {
                        field: "TerritoryId",
                        title: "Territory ID",
                        filterable: { multi: true, search: true },
                        hidden: true,
                    },
                    {
                        field: "Code",
                        title: "Territory Code",
                        filterable: { multi: true, search: true },
                        hidden: false,
                    },
                    {
                        field: "Name",
                        title: "Territory Name",
                        filterable: { multi: true, search: true },
                        hidden: false,
                    },
                    {
                        field: "Minion",
                        title: "Minion ID",
                        filterable: { multi: true, search: true },
                        hidden: false,
                    },
                    {
                        field: "FranchiseId",
                        title: "Franchise ID",
                        hidden: true,
                    },
                    {
                        field: "WebSiteURL",
                        title: "Website URL",

                    },

                    { command: ["edit", "destroy"], title: "&nbsp;", width: "200px" }

                ],
                editable: "inline",
                filterable: true,
                noRecords: { template: "No records found" },


                //toolbar: [{ template: kendo.template($(' <script id="template" type="text/x-kendo-template"><div class="vendor_search" style="padding:0 5px !important;"></script>').html()) }, { name: "create", text: "Add Territory" }],

            };

        };

        $scope.GetFranchiseSetUpTerritory();


        if ($scope.Franchise) $scope.OwnerNameData = $scope.Franchise.lstOwnerName;
        var ownerDataNextID = $scope.OwnerNameData.length + 1;
        function getIndexById(id) {
            var idx,
                l = $scope.OwnerNameData.length;

            for (var j = 0; j < l; j++) {
                if ($scope.OwnerNameData[j].OwnerNameId == id) {
                    return j;
                }
            }
            return null;
        }

        // This Grid is for the Additional Owner
        $scope.GetFranchiseSetUpOwner = function () {

            $scope.FranchiseSetupOwner = {
                dataSource: {
                    transport: {
                        read: function (e) {
                            if ($scope.OwnerNameData != null)
                                e.success($scope.OwnerNameData);

                        },
                        create: function (e) {
                            $scope.OwnerNameData = e.data;
                            e.data.OwnerNameId = ownerDataNextID++;
                            e.success(e.data);
                        },
                        update: function (e) {
                            $scope.OwnerNameData[getIndexById(e.data.OwnerNameId)] = e.data;
                            e.success();

                        },
                        destroy: function (e) {

                            // $scope.OwnerNameData.splice(getIndexById(e.data.OwnerNameId));

                            e.success();

                        }

                    },
                    error: function (e) {

                        alert("Status: " + e.status + "; Error message: " + e.errorThrown);
                    },

                    schema: {
                        model: {
                            id: "OwnerNameId",
                            fields: {
                                OwnerNameId: { editable: false, nullable: true },
                                FirstName: { validation: { required: true } },
                                LastName: { validation: { required: true } },
                            }
                        }
                    }


                },
                toolbar: ["create"],
                columns: [
                    { field: "FirstName", title: "First Name" },
                    { field: "LastName", title: "Last Name" },
                    { command: ["edit", "destroy"], title: "&nbsp;", width: "200px" }
                ],
                editable: {

                    mode: "inline",
                    createAt: "bottom"
                },



            };
        };
        if ($scope.FrachiseId == 0) $scope.GetFranchiseSetUpOwner();

        //initialize the Franchise Object
        $scope.Initialize = function () {
            $http.get('../api/Timezones/0/Get').then(function (response) {
                $scope.TimeZoneCodeList = response.data;
            }, function (response) {
                $scope.TimeZoneCodeList = {};
            });

            $scope.Franchise =
            {
                Code: '',
                CreatedOnUtc: '',
                IsSuspended: false,
                Name: '',
                DBAName: '',
                Website: '',
                TimezoneCode: '',
                DMA: '',
                Region: '',
                OwnerEmail: '',
                LocalPhoneNumber: '',
                OwnerName: '',
                AddressId: '',
                AdminName: '',
                AdminEmail: '',
                IsAddressSame: false,
                Currency: 'US',
                BusinessPhone: '',
                MailingAddressId: '',
                OwnerId: '',
                Website: '',
                CoachName: '',
                TPTGoLiveDate: '',
                EnableTexting: false,
                EnableCase: false,
                EnableVendorManagement: false,
                EnableElectronicSignAdmin: false,
            }


            $scope.Territory =
            {

                TerritoryId: '',
                Code: '',
                TerritoryName: '',
                MinionId: '',
                FEWebsiteUrl: ''
            }



            $scope.BusinessAddress =
            {
                IsResidential: false,
                IsCommercial: false,
                AddressType: 'false',
                AttentionText: '',
                AddressId: '',
                //Address1: $rootScope.$broadcast('angucomplete-alt:clearInput', 'autocomp_'),
                Address1: '',
                Address2: '',
                City: '',
                Country: 'US',
                State: '',
                ZipCode: '',
                CountryCode2Digits: 'US',
                CrossStreet: '',
                Location: '',
                CreatedOn: '',
                CreatedBy: '',
                LastUpdatedOn: '',
                LastUpdatedBy: '',
                IsValidated: false
            };
        }

        function setAddress1Value() {
            // We need to initialize to empty string so that the previous value will
            // not be used the special autofill control.
            $rootScope.$broadcast('angucomplete-alt:clearInput', 'autocomp_');
            return;
        }

        //Address get method
        $http({ method: 'GET', url: '/api/address/', params: { includeStates: true } })
            .then(function (response) {

                $scope.AddressService.States = response.data.States || [];
                $scope.AddressService.Countries = response.data.Countries || [];
                $scope.AddressService.FranchiseAddress = response.data.FranchiseAddress;
            }, function (response) {

            });

        $scope.CancelChanges = function () {

            if ($scope.FrachiseId > 0)
                var url = '/controlpanel#!/franchiseView/' + $scope.Franchise.FranchiseId;
            else
                var url = '/controlpanel#!/franchises';
            $window.location.href = url;
        }

        // Brand Dropdown Changed event


        if ($scope.FrachiseId == 0) {
            $scope.BrandId = 1;
            $scope.FranchiseService.SelectedBrand = $scope.BrandId;
        }
        else
            $scope.FranchiseService.SelectedBrand = $scope.BrandId;
        $scope.BrandSelect = function () {

            $scope.FranchiseService.SelectedBrand = $scope.BrandId;
            //$scope.FranchiseService.ChangeBrand($scope.BrandId).then(function (response) {               
            //});           
            return false;
        }
        // $scope.BrandSelect();



        $scope.CountryChanged = function () {

            if ($scope.Franchise.Address != null) {
                $scope.Franchise.Address.City = '';
                $scope.Franchise.Address.State = '';
                $scope.Franchise.Address.ZipCode = '';
                $scope.Franchise.Address.CrossStreet = '';
            }

            $scope.Franchise.Currency = $scope.BusinessAddress.CountryCode2Digits;

        }

        //ToolTips
        $scope.tooltipOptions = {
            filter: "th",
            position: "top",
            hide: function (e) {
                this.content.parent().css('visibility', 'hidden');
            },
            show: function (e) {
                if (this.content.text().length > 1) {
                    this.content.parent().css('visibility', 'visible');
                } else {
                    this.content.parent().css('visibility', 'hidden');
                }
            },
            content: function (e) {
                var target = e.target.data().title; // element for which the tooltip is shown
                return target;//$(target).text();
            }
        };
        $scope.GetFranchiseName = function (id) {
            var brandName;
            if (id == 1)
                brandName = "Budget Blind(BB)";
            else if (id == 3)
                brandName = "Concrete Craft(CC)";
            else if (id == 2)
                brandName = "Tailored Living(TL)";
            return brandName;
        }
        $scope.SaveFranchise = function () {

            $scope.submitted = true;
            var grid = $("#gridFranchiseOwner").getKendoGrid();
            var dataSource = grid.dataSource;
            var lstOwnerName = [];
            $scope.Franchise.lstOwnerName = dataSource._data;
            $scope.formFranchise.formAddressValidation.zipCode.$commitViewValue(true);
            //restrict saving for invalid date
            for (i = 0; i < $scope.ValidDatelist.length; i++) {
                if (!$scope.ValidDatelist[i].valid) {
                    HFC.DisplayAlert("Invalid Date!");
                    return;
                }
            }
            if ($scope.formFranchise.$invalid) {
                var a = $('#FeStartDate').siblings();
                $(a[0]).addClass("icon_Background");
                return;
            }
            // Stetp: validate address.
            validateAddress($scope.BusinessAddress)
                .then(function (response) {
                    var objaddress = response.data;

                    if (objaddress.Status == "error") {
                        //
                        //TP-1793: CLONE - The request failed with HTTP status 503: Service Temporarily Unavailable.
                        AfterValidateAddress();                       
                    } else if (objaddress.Status == "true") {
                        var validAddress = objaddress.AddressResults[0];
                        $scope.BusinessAddress.Address1 = validAddress.address1;
                        $scope.Addressvalues = $scope.BusinessAddress.Address1;
                        $scope.BusinessAddress.address2 = validAddress.address2;
                        $scope.BusinessAddress.City = validAddress.City;
                        $scope.BusinessAddress.State = validAddress.StateOrProvinceCode;

                        // Update that the address validated to true.
                        $scope.BusinessAddress.IsValidated = true;


                        // assign the zipcode back to the model before saving.
                        if ($scope.BusinessAddress.CountryCode2Digits == "US") {
                            $scope.BusinessAddress.ZipCode = validAddress.PostalCode.substring(0, 5);
                        }
                        else {
                            $scope.BusinessAddress.ZipCode = validAddress.PostalCode;
                        }
                        $http.get('/api/Franchise/0/GetTerritory?Zipcode=' + $scope.BusinessAddress.ZipCode + "&Country=" + $scope.BusinessAddress.CountryCode2Digits)
                            .then(function (response) {
                                $scope.BusinessAddress.Territory = response.data;
                                // $scope.Lead.TeritoryType = response.data;

                                AfterValidateAddress();
                            });
                        //address get empty so assiging the value here.
                        $scope.$broadcast('angucomplete-alt:changeInput', 'autocomp_', $scope.Addressvalues);
                    } else if (objaddress.Status == "false") {                       
                        $scope.BusinessAddress.IsValidated = false;

                        var errorMessage = "Invalid Address. Do you wish to continue?";
                       
                        $scope.Addressvalues = $scope.BusinessAddress.Address1;
                        //address get empty so assiging the value here.
                        $scope.$broadcast('angucomplete-alt:changeInput', 'autocomp_', $scope.Addressvalues);
                        if (confirm(errorMessage)) {
                            AfterValidateAddress();
                        } else {
                            $scope.IsBusy = false;
                        }
                    } else { //SkipValidation
                        AfterValidateAddress();
                    }
                });
        }
        function AfterValidateAddress() {
            $scope.Franchise.Address = $scope.BusinessAddress;
            if ($scope.FrachiseId == 0)
                $scope.Franchise.BrandId = $scope.FranchiseService.SelectedBrand;

            var dto = $scope.Franchise;

            //code for spacing in Canada Postal/ZipCode
            if (dto.Address.CountryCode2Digits == "CA") {
                if (dto.Address.ZipCode) {
                    var Ca_Zipcode = dto.Address.ZipCode;
                    Ca_Zipcode = Ca_Zipcode.substr(0, 3) + " " + Ca_Zipcode.substr(3);
                    dto.Address.ZipCode = Ca_Zipcode;
                }
            }
            //API call to save or update the franchise

            $http.post('/api/Franchise/0/SaveFranchise', dto).then(function (response) {

                var saveStatus = response.statusText;
                var franchiseId = 0;
                if ($scope.Franchise.FranchiseId == undefined) {
                    franchiseId = response.data.FranchiseId;
                }
                else
                    franchiseId = $scope.Franchise.FranchiseId;
                if (response.data.Result != "Account Code already exists") {
                    HFC.DisplaySuccess("Franchise Saved Successfully!");
                    var url = '/controlpanel#!/franchiseView/' + franchiseId;
                    $window.location.href = url;
                }
                else {
                    HFC.DisplayAlert(response.data.Result);

                }
            }, function (errorResponse) {

                var error = errorResponse;
                HFC.DisplayAlert(error.statusText);
            });
        }

        $scope.EditFranchise = function () {
            $scope.EditMode = true;
            var url = '/controlpanel#!/franchise/' + $routeParams.FranchiseId;
            $window.location.href = url;
        }

        $scope.Initialize();

        //Impersonate as function
        $scope.impersonate = function (userId) {
            if (userId) {

                $scope.FranchiseService.Impersonate(userId).then(function (response) {
                    window.location = '/';
                });
            }
            else
                HFC.DisplayAlert("Invalid user");
        }

        //Enabled the QB App
        $scope.addToQB = function () {
            $http.post('/api/franchise/' + $scope.Franchise.FranchiseId + '/AddToQB').then(function (response) {
                $scope.Franchise.IsQBEnabled = true;
                $scope.qbbtntext = 'QB added';
                HFC.DisplaySuccess("QB Added Successfully");
            }, function (response) {
                HFC.DisplayAlert(response.statusText);
            });
        }

        //for date validation
        $scope.ValidateDate = function (date, id) {
            var ValidDateobj = {};
            if (id == "FeLiveDate" && date == "")
                $scope.ValidDate = true;
            else
                $scope.ValidDate = HFCService.ValidateDate(date, "date");

            if ($scope.ValidDatelist.find(item => item.id === id)) {
                var index = $scope.ValidDatelist.findIndex(item => item.id === id);
                $scope.ValidDatelist[index].valid = $scope.ValidDate;
            }
            else {
                ValidDateobj["id"] = id;
                ValidDateobj["valid"] = $scope.ValidDate;
                $scope.ValidDatelist.push(ValidDateobj);
            }

            if ($scope.ValidDate) {
                $("#" + id + "").removeClass("invalid_Date");
                var a = $('#' + id + '').siblings();
                $(a[0]).removeClass("icon_Background");
            }
            else {
                $("#" + id + "").addClass("invalid_Date");
                var a = $('#' + id + '').siblings();
                $(a[0]).addClass("icon_Background");
            }
        }

    }])
