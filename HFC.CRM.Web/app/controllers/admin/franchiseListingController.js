﻿    
'use strict';

appCP.controller('FranchiseListingController', [
    '$scope', '$routeParams', '$rootScope', '$http', '$window',
    '$location', '$modal', 'HFCService','FranchiseService','NavbarServiceCP',
function (
    $scope, $routeParams, $rootScope, $http, $window,
    $location, $modal, HFCService, FranchiseService,NavbarServiceCP) {

    HFCService.setHeaderTitle("Franchise #" );

    var self = this;

    $scope.NavbarServiceCP = NavbarServiceCP;
    $scope.NavbarServiceCP.SetupFranchiseInfoTab();
    
    $scope.Permission = {};
    var Franchisepermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'Franchises')
    if (Franchisepermission) {
        var Impersonatepermission = Franchisepermission.SpecialPermission.find(x => x.PermissionCode == 'Impersonation').CanAccess;
        var ReadOnlyViewPermission = Franchisepermission.SpecialPermission.find(x => x.PermissionCode == 'Read Only View').CanAccess;
    }
    if (Franchisepermission) { //&& Impersonatepermission
        $scope.Permission.ListFe = Franchisepermission.CanRead;
        $scope.Permission.AddFe = Franchisepermission.CanCreate;
        $scope.Permission.Impersonatepermission = Impersonatepermission;
        $scope.Permission.ReadOnlyViewPermission = ReadOnlyViewPermission;
    }

    $scope.FranchiseService = FranchiseService;
    function MainGridDropdownTemplate(dataItem) {

        var Div = '';
         if ($scope.Permission.Impersonatepermission) {

            Div += '<ul>';
            Div += '<li class="dropdown note1 ad_user">';
            Div += '<div class="dropdown">';
            Div += '<button class="btn btn-default dropbtn">';
            Div += '<i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i>';
            Div += '</button>';
            Div += ' <ul class="dropdown-content">';
            if (dataItem.IsSuspended) {
                Div += '<li>';
                Div += '<a href="javascript:void(0)" ng-if="' + dataItem.IsSuspended + '" class="removeSuspended" data-toggle="modal" data-target="#myModalRenew" ng-click="RenewFranchise(this.dataItem)">Renew Franchise</a>';
                Div += '</li>';
            }
            else {
                Div += '<li>';
                Div += '<a href="javascript:void(0)" class="deactivateLink" data-toggle="modal" data-target="#myModal" ng-click="editFranchise(this.dataItem)">Deactivate</a>';
                Div += '</li>';
            }


            if (dataItem.Owner != null && !dataItem.IsSuspended) {
                Div += '<li class="dropdown-submenu">';

                Div += '<a href="javascript:void(0)">Impersonate as:</a>';
                Div += '<ul class="dropdown-menu" style="height: auto; top:3px; margin-left:-130px !important; overflow-x: hidden;" role="menu">';
                Div += '<li>';
                Div += '<a href="javascript:void(0)" ng-click="impersonate(\'' + dataItem.Owner.User.UserId + '\',' + dataItem.BrandId + ')">' + dataItem.Owner.User.Person.FullName + " / " + dataItem.Owner.User.Person.PrimaryEmail.split("@")[0] + " (Root User) " + '</a>';
                Div += '</li>';
                for (var i = 0; i < dataItem.Users.length; i++) {
                    Div += '<li>';
                    Div += '<a href="javascript:void(0)" ng-click="impersonate(\'' + dataItem.Users[i].UserId + '\',' + dataItem.BrandId + ')" >' + dataItem.Users[i].UserName + '</a>';
                    Div += '</li>';
                }
                Div += '</ul>';
                Div += '</li>';
            }
            Div += '</ul>';
            Div += '</div>';
            Div += '</li>';

            Div += '</ul>';
        } else if($scope.Permission.ReadOnlyViewPermission)
        {
            Div += '<ul>';
            Div += '<li class="dropdown note1 ad_user">';
            Div += '<div class="dropdown">';
            Div += '<button class="btn btn-default dropbtn">';
            Div += '<i class="fa fa-ellipsis-h dot_icon" aria-hidden="true"></i>';
            Div += '</button>';
            Div += ' <ul class="dropdown-content">';

            if (dataItem.Owner != null && !dataItem.IsSuspended) {
                Div += '<li class="dropdown-submenu">';

                Div += '<a href="javascript:void(0)">Read Only View as:</a>';
                Div += '<ul class="dropdown-menu" style="height: auto; top:3px; margin-left:-130px !important; overflow-x: hidden;" role="menu">';
                Div += '<li>';
                Div += '<a href="javascript:void(0)" ng-click="impersonate(\'' + dataItem.Owner.User.UserId + '\',' + dataItem.BrandId + ')">' + dataItem.Owner.User.Person.FullName + " / " + dataItem.Owner.User.Person.PrimaryEmail.split("@")[0] + " (Root User) " + '</a>';
                Div += '</li>';
                Div += '</ul>';
                Div += '</li>';
            }

            Div += '</ul>';
            Div += '</div>';
            Div += '</li>';

            Div += '</ul>';

        }
        else {
            if (Div === '')
            Div = "";
        }
        return Div;
    };
    function MainGridStatusTemplate(dataItem) {
        var Div = '';
        Div += '<span ng-if="' + dataItem.IsSuspended + '" class="fieldInActive index_cbox">';
        Div += '<input type="checkbox" value="suspended Franchise" id="lstchksuspendedFranchise" disabled="disabled" checked="' + dataItem.IsSuspended + '" style="border:0px solid #b2bfca">';
        Div += '<label class="checkbox" for="lstchksuspendedFranchise" style="border:0px solid #b2bfca"></label>';
        Div += '</span>';

        Div += '<span class="field index_cbox" ng-hide="' + dataItem.IsSuspended + '">';
        Div += '<input type="checkbox" value="suspended Franchise" id="lstchksuspendedFranchise" disabled="disabled" checked="' + dataItem.IsSuspended + '" style="border:0px solid #b2bfca">';
        Div += '<label class="checkbox" for="lstchksuspendedFranchise" style="border:0px solid #b2bfca"></label>';
        Div += '</span>';
        return Div;
    }

    $scope.editFranchise = function (obj) {
        
        $scope.FranchiseId = obj.FranchiseId;
        $http.get('/api/Franchise/0/DeactivationReasons').then(function (response) {
            $scope.DeactivateReasons = response.data;
            $("#myModal").modal("show");
          
        });
    }

    // Kendo Resizing
    $scope.$on("kendoWidgetCreated", function (e) {
        $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 233);
        window.onresize = function () {
            $('.k-grid-content.k-auto-scrollable').height(window.innerHeight - $("#MenuDiv").height() - 233);
        };
    });

    $scope.RenewFranchise = function (obj) {
        
        var id = obj.FranchiseId;
        $http.get('/api/Franchise/' + id + '/getUnexpiredRoyalties').then(function (response) {
        
            $scope.RoyaltiesCollection1 = response.data
        });
        $scope.FranchiseID = obj.FranchiseId;
    }

    $scope.NewFranchisepage = function () {
        window.location.href = '/controlpanel#!/franchise/0';
       
        //var url = "http://" + $window.location.host + "/#!/leads/";
        //$window.location.href = url;
    };

    //Deactivate Franchise
    $scope.DeactivateFranchise = function (obj) {
        
        $scope.Deacsubmit = true;

        var dto = $scope.Deactivatefran;
        if (!$scope.Deactivatefran.cbodeactivate)
            return;
        //if (modal.DeacReson.$invalid) {
        //    // HFC.DisplaySuccess("Please fill all the required fields! and valid data in optional fields if provided.");
        //    return;
        //}

        var model = {};
        model.DeactivationReasonId = dto.cbodeactivate.DeactivationReasonId;
        model.FranchiseId = $scope.FranchiseId;
        model.DeactivationAdditionalReason = dto.Message;

        if (model.DeactivationReasonId != "" && model.DeactivationReasonId != "0" && model.DeactivationAdditionalReason != "" && model.DeactivationAdditionalReason != "null") {
            $http.post('/api/Franchise/0/deactivateTP/', model).then(
                  function () {
                      HFC.DisplaySuccess("Deactivated Sucessfully");
                      $('#txtDeactivateMessage').val('');
                      $('#myModal').modal('hide');
                      $('#gridFranchise').data('kendoGrid').dataSource.read();
                      //$('#gridFranchise').data('kendoGrid').refresh();

                  });

        }
        //else if ((model.DeactivationReasonId == "" || model.DeactivationReasonId == "0") && (model.DeactivationAdditionalReason == "" || model.DeactivationAdditionalReason == "null")) {
        //    HFC.DisplayAlert("Reason & Comment is required");
        //}
        //else if (model.DeactivationReasonId == "" || model.DeactivationReasonId == "0") {
        //    HFC.DisplayAlert("Reason is required");
        //}
        //else if (model.DeactivationAdditionalReason == "" || model.DeactivationAdditionalReason == "null") {
        //    HFC.DisplayAlert("Comment is required");
        //}
    }

    $scope.ClearDeactiveValues = function () {
        $('#cbodeactivate').val("");
        $('#txtDeactivateMessage').val("");
        $scope.Deacsubmit = false;
        $("#myModal").modal("hide");
    }
    //Reactivate Franchise
    $scope.ReactivateFranchise = function (Royalties, modal) {
        
        $scope.RenewSubmitted = true;

        //if (modal.form_RenewFranchisee.$invalid) {
        //    return;
        //}

        if (Royalties.length == 0) {
            var id = modal.FranchiseID;
            //$http.post('/controlpanel/franchises/0/ReActivateFranchise')

            $http.post('/api/Franchise/' + id + '/ReActivateFranchise/').then(function () {

                $('#myModalRenew').modal('hide');
                $('#gridFranchise').data('kendoGrid').dataSource.read();
                $('#gridFranchise').data('kendoGrid').refresh();
            });
        }
        else {
            var FranchiseroyaltieRenewurl = '/api/Franchise/0/removeSuspended/';
            var data = Royalties;
            $http.post(FranchiseroyaltieRenewurl, data).then(function (response) {
                $('#myModalRenew').modal('hide');
                $('#gridFranchise').data('kendoGrid').dataSource.read();
                $('#gridFranchise').data('kendoGrid').refresh();
            });
        }

    };
    //filter with show inactive
    $scope.IsSuspendSelect = function () {
        $("#gridFranchise").data("kendoGrid").dataSource.read();
    }


    $scope.filters = {
        pageIndex: 0,
        pageSize: 0,    
        ShowInActive: false,
        searchTerm: null,       
    };
   
   
    // Brand Dropdown Changed event
    //if ($scope.FranchiseService.SelectedBrand == null || $scope.FranchiseService.SelectedBrand > 3) {
    //    $scope.BrandId = 1;
    //    $scope.FranchiseService.SelectedBrand = $scope.BrandId;
    //}
    //else
    //    $scope.BrandId = $scope.FranchiseService.SelectedBrand;      
    //    $scope.BrandSelect = function () {        
    //        $scope.FranchiseService.SelectedBrand = $scope.BrandId;
            
    //    $scope.FranchiseService.ChangeBrand($scope.BrandId).then(function (response) {
    //        $("#gridFranchise").data("kendoGrid").dataSource.read();
           
    //    });
    //    return false;
    //}
    //    $scope.BrandSelect();


    //Impersonate As Function
    $scope.impersonateOriginal = function (userId) {
        if (userId) {            
            $scope.FranchiseService.Impersonate(userId).then(function (response) {
                window.location = '/';
            });
        }
    else
        HFC.DisplayAlert("Invalid user");
    }

    $scope.impersonate = function (UserId, brandid) {
        
        $scope.BrandId = brandid;
        $scope.FranchiseService.SelectedBrand = $scope.BrandId;
        $scope.FranchiseService.ChangeBrand($scope.BrandId).then(function (response) {
        });
        HFC.CP.Impersonate(UserId);
    }

    $scope.GetFranchiseList = function () {
        $scope.FranchiseList = {
            dataSource: {
                transport: {
                    read: {
                        url: '/api/franchise/0/list',
                        dataType: "json",
                        data: $scope.filters
                    }
                },
                schema: {
                    model: {
                        fields: {
                            IsSuspended: { type: "boolean" },
                        }
                    }
                }
            },

            columns: [
                         {
                             field: "Brand",
                             title: "Concept",
                             filterable: { multi: true, search: true },
                         },
                
                           {
                               field: "Name",
                               title: "Franchise",
                               filterable: {
                                   //multi: true,
                                   search: true
                               },
                               template: "<a href='\\\\#!/franchiseView/#= FranchiseId #' style='color: rgb(61,125,139);'><span >#= Name #</span></a> ",
                               
                           },
                           {
                               field: "ownername",
                               title: "Owner Name",
                               filterable: {
                                   //multi: true,
                                   search: true
                               },
                           },
                           {
                               field: "Code",
                               title: "Account Code",
                               filterable: {
                                   //multi: true,
                                   search: true
                               },
                               //filterable: { multi: true, search: true, dataSource: ds }
                           },
                           {
                               field: "Address",
                               title: "Location",
                               filterable: {
                                   //multi: true,
                                   search: true
                               },
                               //filterable: { multi: true, search: true, dataSource: ds }
                           },
                           {
                               field: "IsSuspended",
                               title: "Status",
                               filterable: { messages: { isFalse: "Active", isTrue: "Inactive" } },
                               template: function (e) {                                   
                                   if (!e.IsSuspended) {
                                       return '<input disabled="disabled" checked type="checkbox" class="option-input checkbox " value="">';
                                   } else {
                                       return '<input disabled="disabled"  type="checkbox" class="option-input checkbox " value="">';
                                   }
                               }
                           },
                           {
                               field: "FranchiseId",
                               title: "Franchise ID",
                               hidden:true
                               
                           },
                           {
                               field: "BrandId",
                               title: "Brand ID",
                               hidden: true

                           },
                           {
                               field: "",
                               title: "",
                               template: function (dataItem) {
                                   return MainGridDropdownTemplate(dataItem);
                               }
                           }
            ],
            //sort: {
            //    field: "FranchiseId",
            //    dir: "desc"
            //},
            editable: false,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        isempty: "Empty",
                        isnotempty: "Not empty"

                    },
                    date: {
                        gt: "After (Excludes)",
                        lt: "Before (Includes)"
                    },
                }
            },
            dataBound: function (e) {
                // get the index of the UnitsInStock cell
                var columns = e.sender.columns;
                var columnIndex = this.wrapper.find(".k-grid-header [data-field=" + "IsSuspended" + "]").index();

                // iterate the data items and apply row styles where necessary
                var dataItems = e.sender.dataSource.view();
                for (var j = 0; j < dataItems.length; j++) {
                    var discontinued = dataItems[j].get("IsSuspended");

                    var row = e.sender.tbody.find("[data-uid='" + dataItems[j].uid + "']");
                    if (discontinued) {
                        row.addClass("LightGray");
                    }
                }
            },
            resizable: true,
            autoSync: true,
            pageable: {
                refresh: true,
                pageSize: 25,
                pageSizes: [25, 50, 100],
                buttonCount: 5,
                change: function (e) {
                    var myDiv = $('.k-grid-content.k-auto-scrollable');
                    myDiv[0].scrollTop = 0;
                }
            },
            sortable: true,
            noRecords: { template: "No records found" },
            scrollable: true,
            //toolbar: [{ template: kendo.template($(' <script id="template" type="text/x-kendo-template"><div class="vendor_search"><div class="pull-right" ><button id="btnprocurement" type="button" onclick="return false;" ng-click="AddOwner()"><img src="/Content/images/add.png"><br>Add Owner</button></div></div></div></script>').html()) }],

        };
    };
    $scope.GetFranchiseList();
   
    //Search function
    $scope.FranchisegridSearch = function () {
        var searchValue = $('#searchBox').val();
        $("#gridFranchise").data("kendoGrid").dataSource.filter({
            logic: "or",
            filters: [
              
                {
                    field: "Brand",
                    operator: "contains",
                    value: searchValue
                },
              {
                  field: "Name",
                  operator: "contains",
                  value: searchValue
              },
              {
                  field: "Code",
                  operator: "contains",
                  value: searchValue
              },
              {
                  field: "Address",
                  operator: "contains",
                  value: searchValue
              },
              {
                  field: "ownername",
                  operator: "contains",
                  value: searchValue
              }
            ]
        });

    }
    //Clear the text from search box
    $scope.FranchisegridSearchClear = function () {
        $('#searchBox').val('');
        $("#gridFranchise").data('kendoGrid').dataSource.filter({
        });
    }

    //Sign Off function
    // no more required--murugan
    //$scope.SignOff = function () {
    //    $window.location.assign("/ControlPanel/LogOff");
    //};

   
    

}
]);




