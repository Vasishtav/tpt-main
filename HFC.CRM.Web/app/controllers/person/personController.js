﻿app.controller('PersonController', ['$scope','PersonService', function ($scope, PersonService) {
    $scope.inChangeMode = false;

    $scope.PersonService = PersonService;
    $scope.ChangePerson = function () {
        if($scope.jobId)
            PersonService.SaveChangePerson($scope.ngModel, $scope.jobId, function () {
                $scope.inChangeMode = false;
            });
    }

    $scope.$watch('ngModel', function (newval, oldval) {
        if (newval != oldval || !$scope.person) {
            $scope.person = PersonService.GetPerson(newval);
        }
    });        
}])