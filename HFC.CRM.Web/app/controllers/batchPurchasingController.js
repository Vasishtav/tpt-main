﻿app.controller('BatchPurchasingController', ['$scope', '$http','NavbarService','HFCService',
    function ($scope, $http, NavbarService, HFCService) {

        $scope.NavbarService = NavbarService;
        $scope.NavbarService.SelectOperation();
        $scope.NavbarService.EnableBulkPurchaseTab();
        $scope.HFCService = HFCService;

        HFCService.setHeaderTitle("Bulk Purchasing #");
        // header block grids configurations
        $scope.summaryDetailList = [
            {
                title: "Total Counts",
                headerList: ["Sales Order Count", "Vendor Count", "Total QTY", "Total Sales $", "Total Cost"],
                dataList: [],
                keyList: ['salesCount', 'vendorCount', 'totalQty', 'totalSales', 'totalCost']
            }, {
                title: "Selected Counts",
                headerList: ["Sales Order Count", "Vendor Count", "Total QTY", "Total Sales $", "Total Cost"],
                dataList: [],
                keyList: ['salesCount', 'vendorCount', 'totalQty', 'totalSales', 'totalCost']
            }
        ];

        // data list definition
        $scope.orderIdList = [];
        $scope.selectFlag = false;
        $scope.selectAllFlag = false;
        $scope.selectedOrders = [];
        $scope.selectedorderData = [];

        $scope.PurchaseOrderpermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'PurchaseOrder')

        // panel bar grid configurations
        var getGridConfiguration = function (dataList) {
            if (dataList) {
                for (var i = 0; i < dataList.length; i++) {
                    $scope.orderIdList.push(dataList[i]['OrderID']);
                    var gridConfigName = "gridConfig" + i;
                    $scope[gridConfigName] =  {
                        dataSource: dataList[i].QuoteLines,
                        resizable: true,
                        columns: [{
                            field: "QuoteLineNumber", //"QuoteLineId",
                            title: "Line ID",
                            template: function (dataItem) {
                                if (dataItem.QuoteLineNumber)
                                    return "<span class='Grid_Textalign'>" + dataItem.QuoteLineNumber + "</span>";
                                else
                                    return "";
                            },
                            width: "50px"
                        },
                         {
                             field: "Quantity",
                             title: "QTY",
                             template: function (dataItem) {
                                 if (dataItem.Quantity)
                                     return "<span class='Grid_Textalign'>" + HFCService.NumberFormat(dataItem.Quantity, 'number') + "</span>";
                                 else
                                     return "";
                             },
                             width: "50px"
                         },
                         {
                             field: "RoomName",
                             title: "Room Name",
                             width: "150px"
                         },
                         {
                             field: "VendorName",
                             title: "Vendor",
                             width: "200px"
                         },
                         {
                             field: "ProductName",
                             title: "Product / Model",
                             width: "120px"
                         },
                         {
                             field: "Width",
                             title: "W",
                             width: "70px",
                             template: function (dataItem) {
                                 return "<span>" + dataItem.Width + " " + dataItem.FranctionalValueWidth + "</span>";
                             }
                         },
                         {
                             field: "Height",
                             title: "H",
                             width: "50px",
                             template: function (dataItem) {
                                 return "<span>" + dataItem.Height + " " + dataItem.FranctionalValueHeight + "</span>";
                             }
                         },
                         {
                             field: "MountType",
                             title: "M",
                             width: "100px"
                         },
                         {

                             field: "Color",
                             title: "Color",
                             width: "200px"
                         },
                         {

                             field: "Fabric",
                             title: "Fabric",
                             width: "100px"
                         },
                         {
                             field: "ExtendedPrice",
                             title: "Extended Total",
                             template: function (dataItem) {
                                     return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.ExtendedPrice) + "</span>";
                             },
                             width: "120px"
                         },
                         {
                             field: "Cost",
                             title: "Cost",
                             template: function (dataItem) {
                                     return "<span class='Grid_Textalign'>" + HFCService.CurrencyFormat(dataItem.Cost) + "</span>";
                             },
                             width: "100px"
                         }, {
                             field: "QuoteId",
                             title: "Quote ID",
                             template: function (dataItem) {
                                 if (dataItem.QuoteId)
                                     return "<span class='Grid_Textalign'>" + dataItem.QuoteId + "</span>";
                                 else
                                     return "";
                             },
                             width: "100px"
                         }
                        ],
                        noRecords: {
                            template: "No records found"
                        }
                    };
                }
                setTimeout(function () {
                    bindClickEvent();
                }, 2000);
            }
        }

        // for selecting particular order
        $scope.orderSelectClicked = function (orderId, offset, orderData) {
            var elementList = $(".order-select-box");
            var element = elementList[offset];
            if (!$(element).hasClass("selected-order-checkbox")) {
                $(element).addClass("selected-order-checkbox");
                $(element).prop("checked", true);
                $scope.selectedOrders.push(orderId);
                $scope.selectedorderData.push(orderData);
                if ($scope.selectedorderData.length === $scope.orderDetails.length) {
                    $("#selectAllInput").prop("checked", true);
                }
            } else {
                $(element).removeClass("selected-order-checkbox");
                $(element).prop("checked", false);
                var index = ($scope.selectedOrders).indexOf(orderId);
                ($scope.selectedOrders).splice(index, 1);
                if ((($scope.selectedorderData.length !== $scope.orderDetails.length) && ($("#selectAllInput")[0].checked)) || $("#selectAllInput")[0].checked) {
                    $("#selectAllInput").prop("checked", false);
                }
                var tempArray = [];
                for (var i = 0; i < $scope.selectedorderData.length; i++) {
                    if ($scope.selectedorderData[i]['OrderID'] != orderId) {
                        tempArray.push($scope.selectedorderData[i]);
                    }
                }
                $scope.selectedorderData = tempArray;
            }
            $(element).trigger('blur');
            manipulateSelectedData();
        }

        // for select all order
        $scope.selectAllOrders = function () {
            var element = $("#selectAllInput");
            element.trigger('blur');
            if ($(element)[0].checked) {
                $scope.selectedOrders = $scope.orderIdList;
                $scope.selectedorderData = $scope.orderDetails;
                $(".order-select-box").prop("checked", true);
                $(".order-select-box").addClass("selected-order-checkbox");
                manipulateSelectedData(true);
            } else {
                $scope.selectedOrders = [];
                $scope.selectedorderData = [];
                $(".order-select-box").prop("checked", false);
                $(".order-select-box").removeClass("selected-order-checkbox");
                manipulateSelectedData(false);
            }
        }

        // click event binding
        var bindClickEvent = function () {
            $(".order-select-box").click(function (e) {
                e.stopPropagation();
            });
        }

        // manipulate selected data
        var manipulateSelectedData = function (flag) {
            if (flag) {
                getHeaderDetails($scope.orderDetails, 1);
            } else {
                getHeaderDetails($scope.selectedorderData, 1);
            }
        }

        // get header details
        var getHeaderDetails = function (dataItem, offset) {
            var lineItemCount = 0;
            var totalSales = 0;
            var totalCost = 0;
            var vendorListArray = [];
            var vendorCount = 0;
            if (dataItem && dataItem.length !== 0) {
                for (var i = 0; i < dataItem.length; i++) {
                    if (dataItem[i]['QuoteLines']) {
                        lineItemCount += dataItem[i]['TotalQuantity'];
                    }
                    totalSales += dataItem[i]['TotalSales'];
                    totalCost += dataItem[i]['TotalCost'];
                    var existFlag;
                    if (dataItem[i]['QuoteLines'] && dataItem[i]['QuoteLines'].length !== 0) {
                        for (var j = 0; j < dataItem[i]['QuoteLines'].length; j++) {
                            existFlag = false;
                            if (vendorListArray && vendorListArray.length !== 0) {
                                for (var k = 0; k < vendorListArray.length; k++) {
                                    if (dataItem[i]['QuoteLines'][j]['VendorId'] === vendorListArray[k]) {
                                        existFlag = true;
                                    }
                                }
                            }
                            if (!existFlag) {
                                vendorListArray.push(dataItem[i]['QuoteLines'][j]['VendorId']);
                            }
                        }
                    }
                }
                vendorCount = vendorListArray.length;
                // setting the header data
                var tempArray = [];

                tempArray.push({'salesCount': dataItem.length});
                tempArray.push({'vendorCount': vendorCount});
                tempArray.push({'totalQty': lineItemCount});
                tempArray.push({'totalSales': '$' + (totalSales).toFixed(2)});
                tempArray.push({'totalCost': '$' + (totalCost).toFixed(2) });

                $scope.summaryDetailList[offset]['dataList'] = tempArray;
            } else if (dataItem && dataItem.length === 0) {
                // settinbg the header data
                $scope.summaryDetailList[offset]['dataList'] = [];
            }
        }

        // convert to xvpo process
        $scope.convertoXVPO = function () {
            var loading = $("#loading");
            $(loading).css("display", "block");
            $http.post("/api/BulkPurchase/0/PostConvertSalesOrderToxVpo", $scope.selectedOrders).then(function (data) {
                var loading = $("#loading");
                $(loading).css("display", "none");
                window.location.href = "#!/procurementboard";
            });
        }

        var getOrderDetails = function () {
            var loading = $("#loading");
            $(loading).css("display", "block");
            $http.get("/api/BulkPurchase/0/getorders").then(function (data) {
                var loading = $("#loading");
                $(loading).css("display", "none");
                $scope.orderDetails = data.data;
                getGridConfiguration($scope.orderDetails);
                getHeaderDetails($scope.orderDetails, 0);
            });
        }

        getOrderDetails();
    }]);