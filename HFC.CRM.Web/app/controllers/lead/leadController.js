﻿/***************************************************************************\
Module Name:  leadController.js - leadController AngularJS
Project: HFC
Created on: 15 Oct 2017
Created By:
Copyright:
Description: Adding Editing and save the lead information
Change History:
Date  By  Description

\***************************************************************************/

'use strict';

app.controller('leadController', [
 '$scope', '$routeParams', '$rootScope', '$http', '$window',
 '$location', '$modal', 'LeadSourceService', 'PersonService',
 'LeadService', 'AddressService', 'HFCService',
 'FileprintService',
 'NavbarService', 'NoteServiceTP', 'AdditionalContactServiceTP',
 'AdditionalAddressServiceTP', 'PrintService', 'EmailService',
 '$log', 'AddressValidationService', '$q', 'DialogService', 'kendoService', 'AccountSearchService',
 function (
  $scope, $routeParams, $rootScope, $http, $window,
  $location, $modal, LeadSourceService, PersonService,
  LeadService, AddressService, HFCService,
  FileprintService,
  NavbarService, NoteServiceTP, AdditionalContactServiceTP,
  AdditionalAddressServiceTP, PrintService, EmailService, $log,
  AddressValidationService, $q, dialogService, kendoService, AccountSearchService) {

     $scope.AccountSearchService = AccountSearchService;
     console.log($scope.AccountSearchService);
     $scope.LeadSourceService = LeadSourceService;
     $scope.LeadSourceService.LeadSources = [];
     $scope.AddressService = AddressService;
     $scope.PersonService = PersonService;

     $scope.LeadDispositionlist = [];
     $scope.LeadSalesSteplist = [];
     $scope.LeadStatuseslist = [];

     $scope.TerritoryDisplayId = 0;

     // for kendo tooltip
     $scope.kendotooltipOptions = kendoService.kendoToolTipOptions;
     kendoService.customTooltip = null;

     // Represent Access Error message for the whole view. If this contains value
     // the required view is not accessibel to the user.
     $scope.NoAccessMessage = null;
     //
     HFCService.setHeaderTitle("Lead  #");
     $scope.FileprintService = FileprintService;

     $scope.NavbarService = NavbarService;
     $scope.NavbarService.SelectSales();
     $scope.NavbarService.EnableSalesLeadsTab();

     $scope.showValidate = false;

     $scope.Permission = {};
     var Leadpermission = HFCService.CurrentUserPermissions.find(x => x.ModuleCode == 'Lead')
     var ConvertLeadpermission = Leadpermission.SpecialPermission.find(x => x.PermissionCode == 'ConvertLead').CanAccess;

     $scope.Permission.ListLead = Leadpermission.CanRead; //HFCService.GetPermissionTP('List Lead').CanAccess;
     $scope.Permission.AddLead = Leadpermission.CanCreate; //HFCService.GetPermissionTP('Add Lead').CanAccess
     $scope.Permission.EditLead = Leadpermission.CanUpdate; //HFCService.GetPermissionTP('Edit Lead').CanAccess
     $scope.Permission.DisplayLead = Leadpermission.CanRead //HFCService.GetPermissionTP('Display Lead').CanAccess
     $scope.Permission.ConvertLead = ConvertLeadpermission; //HFCService.GetPermissionTP('Convert Lead').CanAccess
     $scope.Permission.QualifyLead = Leadpermission.CanRead //HFCService.GetPermissionTP('Qualify Lead').CanAccess

     //$scope.Permission.AddNotes = Leadpermission.CanRead; //HFCService.GetPermissionTP('Add Notes').CanAccess
     //$scope.Permission.EditNotes = Leadpermission.CanUpdate; //HFCService.GetPermissionTP('Edit Notes').CanAccess
     //$scope.Permission.AddEvent = HFCService.GetPermissionTP('Add Event').CanAccess

     HFCService.GetUrl();
     //LeadDetails Start
     var ctrl = this;

     //$scope.leadURL = window.location.href.split('#!');
     //$scope.leadURL = $scope.leadURL[1].split('/');
     //$scope.leadPathType = $scope.leadURL[1];
     //$scope.leadPathId = $scope.leadURL[2];

     $scope.NotesParams = {};
     $scope.Lead = {};
     $scope.Lead.LeadId = $routeParams.leadId;

     // We need to set these following properties in each controller.
     // TODO: the litteral prameter is good candidate to move to the custom directive.
     //http://blogs.quovantis.com/scope-in-custom-directives-angularjs/
     //http://jsfiddle.net/QrCXh/1776/
     $scope.NoteServiceTP = NoteServiceTP;
     $scope.NoteServiceTP.LeadId = $scope.Lead.LeadId;
     $scope.NoteServiceTP.ParentName = "Lead";

     // Fix for TP-1588	Attachments - duplicating
     $scope.NoteServiceTP.FranchiseSettings = false;

     //$scope.NoteServiceTP.showAddNotes =  Leadpermission.CanCreate; //HFCService.GetPermissionTP('Add Notes').CanAccess;
     //$scope.NoteServiceTP.showEditNotes = Leadpermission.CanUpdate; // HFCService.GetPermissionTP('Edit Notes').CanAccess;

     // Additional Contact Integration:
     $scope.AdditionalContactServiceTP = AdditionalContactServiceTP;
     $scope.AdditionalContactServiceTP.ParentName = "Lead";
     //$scope.AdditionalContactServiceTP.showAddoption =  Leadpermission.CanCreate; //HFCService.GetPermissionTP('Add Additional Contact').CanAccess;
     //$scope.AdditionalContactServiceTP.showEditoption = Leadpermission.CanUpdate; //HFCService.GetPermissionTP('Edit Additional Contact').CanAccess;
     $scope.AdditionalContactServiceTP.LeadId = $scope.Lead.LeadId;

     // Additional Address Integration:
     $scope.AdditionalAddressServiceTP = AdditionalAddressServiceTP;
     $scope.AdditionalAddressServiceTP.ParentName = "Lead";
     //$scope.AdditionalAddressServiceTP.showAddAddress =  Leadpermission.CanCreate; //HFCService.GetPermissionTP('Add Additional Address').CanAccess;
     //$scope.AdditionalAddressServiceTP.showEditAddress = Leadpermission.CanUpdate; //HFCService.GetPermissionTP('Edit Additional Address').CanAccess;
     $scope.AdditionalAddressServiceTP.LeadId = $scope.Lead.LeadId;




     $scope.LeadService = LeadService;

     $scope.franchiseAddressObject = null;


     $scope.HFCService = HFCService;
     //texting
     $scope.BrandId = $scope.HFCService.CurrentBrand;
     $scope.FranciseLevelTexting = $scope.HFCService.FranciseLevelTexting;
     $scope.disablenotifytext = true;
     $scope.Required = true;


     $scope.LeadPopAddress = [];

     // This is very much required for Angular to track otherwise the current implementation won't work.
     $scope.StateList = [];
     $scope.PrintService = PrintService;

     $scope.submitted = false;
     $scope.showValidate = false;
     $scope.MatchLeadAccount = [];
     $scope.FileprintService = FileprintService;
     $scope.TerritoryDisData = {};

     $scope.PersonService.CallendarSuccessCallback = function () {
         //if (PersonService.goPageIndex == 5) {
         //    var goLeadQualify = '#!/leadsQualify/' + $scope.Lead.LeadId;
         //    window.open(goLeadQualify, '_self');
         //}
         //else {
         //    window.location.reload();
         //}
     }

     $http.get('/api/FranchiseSettings/0/GetDisplayTerritoryId').then(function (data) {
         if (data.data) {
             $scope.TerritoryDisData = data.data;
             $scope.Lead.TerritoryDisplayId = data.data.TerritoryId;
             $scope.TerritoryDisplayId = data.data.TerritoryId;


             //$scope.DisplayTerritoryId = data.data.TerritoryId;
             //$scope.DisplayTerritoryName = data.data.TerritoryName;

         } else {
             $scope.Lead.TerritoryDisplayId = 0;
             $scope.TerritoryDisplayId = 0;

         }

         $http.get('/api/Accounts/0/getTerritoryForFranchise').then(function (response) {
             if (response.data != null) {

                 //$scope.Lead.TerritoryDisplayId = 0;
                 //$scope.TerritoryDisplayId = 0;

                 if (response.data.length > 0) {
                     $scope.DisplayTerritorySource = response.data;
                     //angular.forEach($scope.DisplayTerritorySource, function (value, key) {

                     //    if (value.Name.toUpperCase().includes('GRAY AREA')) {

                     //        $scope.TerritoryDisplayId = value.Id;
                     //        $scope.Lead.TerritoryDisplayId = value.Id;
                     //    }
                     //});

                     //if (!($scope.TerritoryDisplayId > 0)) {   //  if (!($scope.TerritoryDisplayId > 0) || !($scope.TerritoryDisplayId)) 
                     //    $scope.DisplayTerritorySource.splice(0, 0, {
                     //        Id: 0,
                     //        Name: 'Select'
                     //    });
                     //}


                 } else {
                     $scope.DisplayTerritorySource = [];
                 }

             }

             $scope.DisplayTerritorySource.splice(0, 0, {
                 Id: 0,
                 Name: 'Select'
             });

         });


     });




     $scope.$watch('Address.Territory', function () {

         //if ($scope.Address.Territory)
         //    if ($scope.TerritoryDisData) {



         //    }
         //    else {
         //        $scope.Lead.TerritoryDisplayId = 0;
         //        $scope.TerritoryDisplayId = 0;
         //        //if ($scope.DisplayTerritorySource.length > 0)
         //        //    angular.forEach($scope.DisplayTerritorySource, function (value, key) {
         //        //        if (value.Name.toUpperCase().includes('GRAY AREA')) {

         //        //            $scope.TerritoryDisplayId = value.Id;
         //        //            $scope.Lead.TerritoryDisplayId = value.Id;
         //        //        }
         //        //    });
         //    }

         //if ($scope.Lead.TerritoryType != $scope.Address.Territory) {
         //    $scope.TerritoryDisplayId = 0;
         //    $scope.Lead.TerritoryDisplayId = 0;
         //}
         $scope.Lead.TerritoryType = $scope.Address.Territory;
     });


     $scope.$on('AddressComponent.Blur', function (v) {

         //alert("test vale");
         var Addr_Value = $scope.Address;
         if (Addr_Value.Address1) {
             $http.get('/api/Accounts/0/GetAcctAddrDetails?Addrvalue=' + Addr_Value.Address1).then(function (response) {
                 if (response.data != null) {

                     if (response.data.length == 0) {
                         return;
                     } else {
                         $scope.Acct_details = response.data;
                         $("#lead_AccntDetails").modal("show");
                         return
                     }
                 }
             })
         }

     });


     $scope.errorMessages = {};
     $scope.errorMessages.ConvertPage = "Please select a matching Account or Create New Account";
     $scope.errorMessages.AccountSearchPage = "Please choose an account for the lead";
     $scope.convertErrorMessage = "";

     $scope.minDate = new Date();
     $scope.maxDate = new Date('2099/12/31');

     //Added for Convert Lead functionality
     $scope.SelectedAccountFromMatchinglist = null
     $scope.AccountSearchService.SelectedAccountFromSearch = null
     $scope.IsCreateNewAccount = false;
     $scope.IsAddOpportunity = true;
     $scope.SearchAccounts = function (modalId) {
         $("#" + modalId).modal("show");
     }
     $scope.CancelConvertLead = function (modalId) {
         $("#" + modalId).modal("hide");
         var myDiv = $('.convert_popup');
         myDiv[0].scrollTop = 0;
     }
     $scope.SaveSelectedAccounts = function (modalId) {
         var myDiv = $('.convert_popup');
         myDiv[0].scrollTop = 0;
         var errmodalId = 'convertleadmessagepopup';
         if ($scope.AccountSearchService.SelectedAccountFromSearch == null) {
             $scope.convertErrorMessage = $scope.errorMessages.AccountSearchPage;
             $("#" + errmodalId).modal("show");
             return;
         } else {
             var leadId = $scope.AccountSearchService.SelectedAccountFromSearch;
             //$http.get('/api/search/' + leadId + '/ApplyLeadMatchingAccount')
             $http.get('/api/search/' + leadId + '/GetLeadMatchingAccountsTP')
              .then(function (data) {

                  $scope.MatchLeadAccount = data.data;
                  $scope.SelectMatchingAccount = data.data[0].AccountId;

                  $scope.SelectedAccount($scope.SelectMatchingAccount);

              });
         }
         $("#" + modalId).modal("hide");

     }

     $scope.ClickMeToRedirectQualify = function () {
         // var url = "http://" + $window.location.host + "/#!/leadsEditQualify/" + $scope.Lead.LeadId;
         $window.location.href = "#!/leadsEditQualify/" + $scope.Lead.LeadId;
     };


     $scope.SelectedSearchAccount = function (accountid) {
         $scope.AccountSearchService.SelectedAccountFromSearch = accountid;

     }

     $scope.AccountgridSearch = function () {
         var searchValue = $('#searchBox').val();
         $("#gridAccountSearch").data("kendoGrid").dataSource.filter({
             logic: "or",
             filters: [
                 {
                     field: "CompanyName",
                     operator: "contains",
                     width: "150px",
                     value: searchValue
                 },
               {
                   field: "AccountFullName",
                   operator: "contains",
                   width: "150px",
                   value: searchValue
               },
               {
                   field: "AccountType", //in db it is AccountTypeId
                   operator: "contains",
                   width: "100px",
                   value: searchValue
               },
               {
                   field: "Status",
                   operator: "contains",
                   width: "100px",
                   value: searchValue
               },
               {
                   field: "Address1",
                   operator: "contains",
                   width: "200px",
                   value: searchValue
               },
                {
                    field: "City",
                    operator: "contains",
                    width: "100px",
                    value: searchValue
                },
                 {
                     field: "State",
                     operator: "contains",
                     width: "100px",
                     value: searchValue
                 },
               {
                   field: "ZipCode",
                   operator: "contains",
                   width: "100px",
                   value: searchValue
               },
               {
                   field: "DisplayPhone",
                   operator: "contains",
                   value: searchValue
               },
                {
                    field: "PrimaryEmail",
                    operator: "contains",
                    value: searchValue
                }
             ]
         });

     }

     $scope.AccountgridSearchClear = function () {
         $('#searchBox').val('');
         $("#gridAccountSearch").data('kendoGrid').dataSource.filter({
         });
         // AccountSearchService.Accounts_options = kendotooltipService.setColumnWidth(AccountSearchService.Accounts_options);
     }

     $scope.SelectedAccount = function (accountId) {
         $scope.ResetCreateNewAccount();
         $scope.SelectedAccountFromMatchinglist = accountId;

         //reset that new account is false;
         $scope.IsCreateNewAccount = false;
     }
     $scope.ConvertLeadFromSelectedAccount = function () {
         if ($scope.IsBusy) return;

         $scope.submitted = true;
         $scope.IsBusy = true;

         var leadId = 0;
         var selectedaccountid = 0;
         var addnewaccount = 0;
         var addopportunity = 0;

         if ($scope.formConvertLead.$invalid) {
             $scope.IsBusy = false;
             return;
         }

         //if ($scope.IsBusy) return;
         //$scope.IsBusy = true;

         var modalId = 'convertleadmessagepopup';

         if ($scope.Lead.LeadId != null) {
             leadId = $scope.Lead.LeadId;
         }
         if ($scope.SelectedAccountFromMatchinglist != null && $scope.IsCreateNewAccount == false) {
             selectedaccountid = $scope.SelectedAccountFromMatchinglist;
         }
         if ($scope.IsCreateNewAccount != null) {
             if ($scope.IsCreateNewAccount == true) {
                 addnewaccount = 1;
             }
         }
         if ($scope.IsAddOpportunity != null) {
             if ($scope.IsAddOpportunity == true) {
                 addopportunity = 1;
             }

         }
         if (selectedaccountid == 0 && addnewaccount == 0) {
             $scope.ConvertLeadValidation == true;
             $scope.convertErrorMessage = $scope.errorMessages.ConvertPage;
             $("#" + modalId).modal("show");

             $scope.IsBusy = false;
             return;
         }

         //$scope.IsBusy = true;
         var newaccountId = 0;
         if ($scope.Lead.LeadId) {
             var data = {
                 leadId: leadId,
                 selectedaccountid: selectedaccountid,
                 addnewaccount: addnewaccount,
                 addopportunity: addopportunity
             }
             data.opportunityName = $scope.OpportunityName;
             data.SalesAgentId = $scope.SelectedAgent;
             var loadingElement = $("#loading");
             if (loadingElement) {
                 loadingElement[0].style.display = "block";
             }
             $http.put('/api/leads/1/ConvertSelectLead', data).then(function (response) {
                 HFC.DisplaySuccess("Lead converted");
                 if (loadingElement) {
                     loadingElement[0].style.display = "none";
                 }
                 if (response.data.newaccountId != null) {
                     newaccountId = response.data.newaccountId;
                 }
                 if (newaccountId > 0) {
                     var goToAccountsPage = '#!/Accounts/' + newaccountId;
                     $window.open(goToAccountsPage, '_self');
                 }
                 $scope.IsBusy = false;
             });

         }

         //$scope.IsBusy = false;

         if (selectedaccountid != 0) {
             var goToAccountsPage = '#!/Accounts/' + selectedaccountid;
             $window.open(goToAccountsPage, '_self');
         }
     }

     $scope.ResetCreateNewAccount = function () {
         $("#optNewAccount").prop("checked", false);
         $scope.AccountSearchService.SelectedAccountFromSearch = null
     }
     $scope.CheckForMatchingAccounts = function () {
         $('input:radio[name="optSelectAccount"]').prop('checked', false);
     }
     $scope.ClickMeToRedirectLeadDetails = function () {

         //  var url = "http://" + $window.location.host + "/#!/leads/" + $scope.Lead.LeadId;
         $window.location.href = "#!/leads/" + $scope.Lead.LeadId;
     };
     $scope.ConvertLead = function () {


         $http.get('/api/search/' + $scope.Lead.LeadId + '/GetLeadMatchingAccounts').then(function (data) {

             $scope.MatchLeadAccount = data.data;
         });
         window.location = '#!/leadConvert/' + $scope.Lead.LeadId;
     }

     $scope.CountryChanged = function () {
         $scope.Address.City = '';
         $scope.Address.State = '';
         $scope.Address.ZipCode = '';
         $scope.Address.CrossStreet = '';

     }

     $scope.EmailService = EmailService; //Added for email service to send email

     $scope.PrintOptions = {
         AddleadCustomer: true,
         AddGoogleMap: true,
         AddMeasurementForm: true,
         AddGarageMeasurementForm: false,
         AddClosetMeasurementForm: false,
         AddOfficeMeasurementForm: false,
         AddFloorMeasurementForm: false,
         AddFCCMeasurementForm: false,
         AddPrintExistingMeasurements: false,
         AddPrintInternalNotes: false,
         AddPrintExternalNotes: false,
         AddPrintDirectionNotes: false,
     }
     $scope.PrintService.PrintOptions = $scope.PrintOptions;
     $scope.IsPurchaseOrderdsVisible = HFC.Util.HostDomain != 'tailoredliving';

     function getParameterByName(name) {
         name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
         var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
          results = regex.exec(location.search);
         return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
     }

     //*******************Google Maps**********************
     function addressToString(isSingleLine, model) {
         if (model) {

             var str = "";
             if (model.Address1)
                 str = model.Address1 + (model.Address2 ? (", " + model.Address2) : "") + (isSingleLine ? ", " : "<br/>");

             if (model.City)
                 str += model.City + ", " + (model.State || "") + " " + (model.ZipCode || "");
             else
                 str += (model.ZipCode || "");

             return str;
         }
     };
     $scope.GetGoogleAddressHref = function (model) {
         var dest = addressToString(true, model);
         if (model = undefined) {
             return;
         }
         if (model && model.Address1) {
             var srcAddr;
             if (HFC.currentGeoLocation && HFC.Browser.isMobile()) {
                 srcAddr = HFC.currentGeoLocation.coords.latitude + "," + HFC.currentGeoLocation.coords.longitude;
             } else {
                 srcAddr = encodeURIComponent(HFC.CurrentUserAddress);
             }

             if (navigator.userAgent.toLowerCase().indexOf("iphone") == -1 && navigator.userAgent.toLowerCase().indexOf("ipad") == -1) {
                 return '//maps.google.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
             } else {
                 return '//maps.apple.com?saddr=' + srcAddr + '&daddr=' + encodeURIComponent(dest);
             }
         } else
             return null;
     }

     $scope.AddressToString = function (model) {
         return addressToString(true, model)
     };
     //*******************End Google Maps******************
     //************************************************** init lead data


     var leadId = $routeParams.leadId;

     //Currently we not using Quick Disposition, it will come later with updated field

     //function getDispositionValue(leadStatusId, DispositionId) {
     //    var dispositionid;
     //    if (leadStatusId === 7) {
     //        dispositionid = '7|0|1';
     //    }
     //    else if (leadStatusId === 2) {
     //        dispositionid = '2|' + DispositionId + '|0';
     //    }
     //    else if (leadStatusId === 3) {
     //        dispositionid = '5|' + DispositionId + '|2';
     //    }
     //    else {
     //        dispositionid = '';
     //    }
     //    var quickdisp = $scope.QuickDispositionList;
     //    for (var i = 0 ; i < quickdisp.length; i++) {
     //        if (quickdisp[i].Value === dispositionid) {
     //            return quickdisp[i];
     //        }
     //    }
     //    return '';
     //}
     $scope.GetZillowinfo = function (address, citystatezip) {
         //var address = "15018 Rixeyville Rd";
         //var citystatezip = "Culpeper, VA 22701";

         $http.get('/api/zillow/?address=' + address + '&citystatezip=' + citystatezip).then(function (data) {
             $scope.zillowdata = data.data.ZilloResponce;
         },
          function (error) {
              console.log("Error: " + error);
          });

     }

     // This function is no more required, it was replaced by errorHandler - murugan
     //function permissionErrorHandler(response) {
     //    $scope.NoAccessMessage = response.statusText;

     //     Inform that the modal is ready to consume:
     //    $scope.modalState.loaded = true;
     //}

     function errorHandler(reseponse) {


         // Inform that the modal is ready to consume:
         $scope.modalState.loaded = true;
         HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
     }

     $scope.modalState = {
         loaded: true
     }

     $scope.$watch('modalState.loaded', function () {
         var loadingElement = document.getElementById("loading");

         if (!$scope.modalState.loaded) {
             loadingElement.style.display = "block";
         } else {
             loadingElement.style.display = "none";
         }
     });

     function successHandler(leadServiceData) {

         var lead = leadServiceData.Lead;
         $scope.Lead = lead;
         $scope.FranciseLevelTexting = leadServiceData.FranciseLevelTexting;
         $scope.Lead.TextStatus = leadServiceData.TextStatus;
         $scope.IsNotifyTextCopy = $scope.Lead.IsNotifyText;
         $scope.CellPhoneCopy = $scope.Lead.PrimCustomer.CellPhone;
         if (!$scope.Lead.IsNotifyemails && document.getElementsByName('user_email')[0] != undefined) {
             document.getElementsByName('user_email')[0].placeholder = '';
             $scope.Required = false;
         }

         if ($scope.Lead.PrimCustomer.CellPhone == null) {
             $scope.disablenotifytext = true;
         }
         else {
             if ($scope.Lead.TextStatus == null) {
                 $scope.Lead.NotifyTextStatus = 'new';
                 $scope.disablenotifytext = false;
             }
             else if ($scope.Lead.TextStatus.Optout == true) {
                 $scope.Lead.NotifyTextStatus = 'Optout';
                 $scope.disablenotifytext = false;
             }
             else if ($scope.Lead.TextStatus.Optin == true) {
                 $scope.Lead.NotifyTextStatus = 'Optin';
                 $scope.disablenotifytext = false;
             }
             else if ($scope.Lead.TextStatus.IsOptinmessagesent == true) {
                 $scope.Lead.NotifyTextStatus = 'waiting';
                 $scope.disablenotifytext = false;
             }
             else if ($scope.Lead.TextStatus.Optout == null && $scope.Lead.TextStatus.Optin == null && $scope.Lead.TextStatus.IsOptinmessagesent == false) {
                 $scope.Lead.NotifyTextStatus = 'error';
                 $scope.disablenotifytext = false;
             }
         }

         if ($scope.Lead.TerritoryDisplayId && $scope.Lead.TerritoryDisplayId > 0)
             $scope.TerritoryDisplayId = $scope.Lead.TerritoryDisplayId;
         else {
             $scope.TerritoryDisplayId = 0;
             $scope.Lead.TerritoryDisplayId = 0;
             //angular.forEach($scope.DisplayTerritorySource, function (value, key) {
             //    if (value.Name.toUpperCase().includes('GRAY AREA')) {
             //        $scope.TerritoryDisplayId = value.Id;
             //        $scope.Lead.TerritoryDisplayId = value.Id;
             //    }
             //});
         }

         if ($scope.Lead.LeadId) {
             var id = 0;
             if ($scope.Lead.LeadStatusId)
                 id = $scope.Lead.LeadStatusId;

             $http.get('/api/lookup/' + id + '/LeadStatus')
              .then(function (data) {

                  $scope.LeadStatuseslist = data.data;
                  if (!$scope.Lead.LeadId) {
                      $scope.Lead.LeadStatusId = 1;
                  }
              }); //, errorHandler);
         }
         $scope.currentDate = new Date();
         $scope.currentDate = $scope.currentDate.toLocaleDateString();

         if ($scope.Lead.IsCommercial && $scope.Lead.PrimCustomer.CompanyName != "")
             $scope.OpportunityName = $scope.Lead.PrimCustomer.CompanyName + " " + $scope.currentDate;
         else
             $scope.OpportunityName = $scope.Lead.PrimCustomer.LastName + " " + $scope.currentDate;

         // The db stores RecieveCorporateEmailBlast value in IsReceiveEmails field.
         if ($scope.Lead.PrimCustomer) {
             $scope.Lead.PrimCustomer.RecieveCorporateEmailBlast = $scope.Lead.PrimCustomer.IsReceiveEmails;
         }
         if (lead.SourcesTPId && lead.SourcesTPId > 0) {
             $scope.LeadChannelList = lead.SourcesList;
             $scope.SelectedSourceIds = [lead.SourcesList[0].SourceId];

             $scope.selectedSourceName = lead.SourcesList[0].name;
         }

         if (lead.RelatedAccountId != undefined) {
             $scope.SelectedRelatedAccountIds[0] = lead.RelatedAccountId;
         }

         $scope.Address = $scope.Lead.Addresses[0];
         var franchiseAddressObject = leadServiceData.FranchiseAddressObject;
         //this is temporary until we change leads to use an array of customer 
         // so we can have more than 2 people per lead

         $scope.Address.NotifyAddress1blur = function () {
             var add1 = "";
         }

         $scope.PersonSecondaryPeople = [];
         $scope.SelectedLeadStatus = leadServiceData.SelectedLeadStatus;
         $scope.selectedCampaign = leadServiceData.selectedCampaign;
         $scope.PrimarySource = leadServiceData.PrimarySource;
         $scope.SecondarySource = leadServiceData.SecondarySource;
         $scope.LeadSourceService.LeadSources = leadServiceData.SecondarySource;
         $scope.LeadSourceService.Sources = leadServiceData.SecondarySource;

         $scope.AddressService.Set(leadServiceData.States, leadServiceData.Countries, leadServiceData.Addresses, leadServiceData.FranchiseAddress);
         $scope.leadAddresses = leadServiceData.Addresses;
         $scope.AddressService.Addresses = [];

         //Added to fetch the matching accounts for selected lead
         $http.get('/api/search/' + leadId + '/GetLeadMatchingAccounts')
          .then(function (data) {
              $scope.MatchLeadAccount = data.data;
              if ($scope.MatchLeadAccount.length < 1)
                  $scope.IsCreateNewAccount = true;
          }); //, errorHandler);

         // TODO: who do we need this???
         var leadAddresslength = leadServiceData.Addresses.length;
         for (i = 1; i < leadAddresslength; i++) {
             $scope.AddressService.Addresses.push(leadServiceData.Addresses[i]);
         }

         //Culpeper, VA 22701
         //var addressZillow = leadServiceData.Addresses[0];
         //var citystatezip = addressZillow.City + ', ' + addressZillow.State + ' ' + addressZillow.ZipCode;
         //var address = addressZillow.Address1;
         //$scope.checkZillowinfo = addressZillow.CountryCode2Digits == "US" ? true : false;
         //if (addressZillow.CountryCode2Digits == "US") {
         //    $scope.GetZillowinfo(address, citystatezip);
         //}

         //https://docs.angularjs.org/api/ng/input/input%5Bradio%5D
         //to get the selected value in IsResidential property
         if (leadServiceData.Addresses[0].IsResidential) {
             $scope.Lead.IsBusinessAddress = false;
         } else {
             $scope.Lead.IsBusinessAddress = true;
         }

         $scope.FranchiseAddressObject = franchiseAddressObject;

         // TODO: do we need this???
         var activeTabQueryParameter = getParameterByName("tab");
         if (activeTabQueryParameter) {
             $('a[href="#' + activeTabQueryParameter + '"]').tab("show");
         }

         $scope.showValidate = true;

         // TODO: do we need this in every controller???
         //set window title
         //$('head title').text("Lead #" + $scope.Lead.LeadNumber + " - " + $scope.Lead.PrimCustomer.FullName);
         HFCService.setHeaderTitle("Lead #" + $scope.Lead.LeadId + " - " + $scope.Lead.PrimCustomer.FullName);
         // Need to set the initial value for the address one, because it is an 
         // autocomplete component needs a spcial handling.
         $scope.$broadcast('angucomplete-alt:changeInput', 'autocomp_', $scope.Address.Address1);

         // Inform that the modal is ready to consume:
         $scope.modalState.loaded = true;
     }

     // The controller is called for Edit more.
     if (leadId > 0) {

         // Inform that the modal is not yet ready
         $scope.modalState.loaded = false;

         $scope.LeadService.Get(leadId, successHandler, errorHandler);

         //Initialize the NoteServiceTP, so that it will get the necessary data from the DB.
         $scope.NoteServiceTP.Initialize();

         //Initialize the AdditionalContactServiceTP, so that it will get the necessary data from the DB.
         $scope.AdditionalContactServiceTP.Initialize();

         //Initialize the AdditionalAddressServiceTP, so that it will get the necessary data from the DB.
         $scope.AdditionalAddressServiceTP.Initialize();
         $scope.AdditionalAddressServiceTP.PopulateAddressList();
        // $scope.AdditionalAddressServiceTP.ShowAddIcon = $scope.Permission.EditLead;
        // $scope.AdditionalContactServiceTP.ShowAddIcon = $scope.Permission.EditLead;
     }

     $scope.cancelQualify = function () {
         $scope.showValidate = false;
         var goLeadQualify = '#!/leadsQualify/' + $scope.Lead.LeadId;
         $window.open(goLeadQualify, '_self');
     }

     var isZillowApiCalled = false;
     $scope.handleKendoPanelBarExpand = function (ev) {

         //var element = ev.srcElement ? ev.srcElement : ev.target;
         //console.log(element, angular.element(element));

         if (isZillowApiCalled) return;

         isZillowApiCalled = true;
         var addressZillow = $scope.leadAddresses[0]; //leadServiceData.Addresses[0];
         var citystatezip = addressZillow.City + ', ' + addressZillow.State + ' ' + addressZillow.ZipCode;
         var address = addressZillow.Address1;
         $scope.checkZillowinfo = addressZillow.CountryCode2Digits == "US" ? true : false;
         if (addressZillow.CountryCode2Digits == "US") {
             $scope.GetZillowinfo(address, citystatezip);
         }
     }

     //referred in ZipCode in lead.html
     $scope.ResetStateCountry = function (mode) {

     }

     //Redirect to Create Lead Page
     $scope.Newleadpage = function () {
         window.location.href = '#!/leads';
         //var url = "http://" + $window.location.host + "/#!/leads/";
         //$window.location.href = url;
     };

     $scope.ClickMeToRedirectEditLead = function () {

         //var url = "http://" + $window.location.host + "/#!/leadsEdit/" + $scope.Lead.LeadId;
         $window.location.href = "#!/leadsEdit/" + $scope.Lead.LeadId;
         // window.location.href = url;
     };

     //Get Disposition Based on Lead Status
     $scope.ChangeLeadStatus = function () {
         // Murugan: This is unnecessary???
         //$scope.LeadService.LeadDispositionlist = null
         //$scope.LeadService.LeadDispositionByLeadStatus($scope.Lead.LeadStatusId);
     }

     function PopulateChannelAndSource(campaignId) {
         if (!campaignId || campaignId <= 0) {
             $scope.Lead.SourcesTPId = 0;
             $scope.Lead.SourceNameFromCampaign = "";
             $scope.Lead.SourcesTPIdFromCampaign = 0;

             // no more further execution required.
             return
         }

         $http.get('/api/lookup/' + campaignId + '/SourceTp').then(function (data) {
             $scope.LeadSingleSourceList = [];
             $scope.LeadSingleSourceList = data.data;
             if ($scope.Lead.LeadId != undefined) {
                 $scope.franchiseFlag = $scope.Lead.AllowFranchisetoReassign;
             } else
                 $scope.franchiseFlag = true;

             $scope.SelectedSourceIds = [$scope.LeadSingleSourceList[0].SourceId];
             $scope.selectedSourceName = $scope.LeadSingleSourceList[0].name;
             $scope.ChangeSource();

         });
     }

     //get source and channel based on campaign selection
     $scope.ChangeCampaign = function () {
         PopulateChannelAndSource($scope.Lead.CampaignId);
     }

     $scope.ChangeDispositionDesc = function (id) {
         $scope.DispositionDesc = "";
         if ($scope.LeadDispositionlist.length > 0 && id) {
             var source = $.grep($scope.LeadDispositionlist, function (d) {
                 return d.DispositionId == id;
             });
             if (source) $scope.DispositionDesc = source[0].Description;
         }
     }

     $scope.ChangeSource = function () {
         if ($scope.SelectedSourceIds.length != 0) {
             $scope.LeadChannelList = {};
             var id = $scope.SelectedSourceIds[0];
             $http.get('/api/lookup/' + id + '/ChannelBySource').then(function (data) {
                 $scope.LeadChannelList = data.data;
             });
         }
     }

     $scope.UpdateCommercialDependField = function () {
         if ($scope.Lead.IsCommercial == false) {
             $scope.Lead.CommercialTypeId = null;
             $scope.Lead.RelationshipTypeId = null;
             $scope.Lead.IndustryId = null;
             $scope.Lead.OtherIndustry = null;
         } else if ($scope.Lead.IsCommercial == true) {
             $scope.Lead.RelationshipTypeId = 3001;
         }
     }

     $scope.UpdateOtherIndustries = function () {
         $scope.Lead.OtherIndustry = null;
     }

     $scope.LeadService = LeadService;

     // This is no more required
     $http({
         method: 'GET',
         url: '/api/address/',
         params: {
             leadIds: 0,
             includeStates: true
         }
     })
      .then(function (response) {
          $scope.AddressService.States = response.data.States || [];
          $scope.AddressService.Countries = response.data.Countries || [];
          $scope.AddressService.FranchiseAddress = response.data.FranchiseAddress;
      }, function (response) { });

     $scope.setAction = function (action) {
         $(".save_tpbut").blur();
         // $scope.CloseNotifyTextpopup();
         $scope.submitAction = action;
         $scope.SaveLead();
     }

     $scope.DisplayMore = false;

     $scope.IsBusy = false;

     $scope.$watch('Address.IsResidential', function (v) {
         if ($scope.Address.IsResidential) {
             $scope.Address.IsCommercial = false;
         }
     });

     $scope.$watch('Address.IsCommercial', function (v) {
         if ($scope.Address.IsCommercial) {
             $scope.Address.IsResidential = false;
         }
     });

     $scope.EditLeadAddressChanges = function () {
         //if ($scope.Address.ZipCode) {

         //    $http.get('/api/lookup/0/zipcodes?q=' + $scope.Address.ZipCode + "&country=" + $scope.Address.CountryCode2Digits).then(function (response) {
         //        $scope.IsBusy = false;
         //        if (response.data.zipcodes.length > 0) {
         //            $scope.Address.City = response.data.zipcodes[0].City;
         //            $scope.Address.State = response.data.zipcodes[0].State;
         //            $scope.Address.Country = response.data.zipcodes[0].CountryCode;
         //        };
         //    }, function (response) {
         //        HFC.DisplayAlert(response.statusText);
         //        $scope.IsBusy = false;
         //    });
         //}

     }

     $scope.addressTypeIsNotValid = false;
     $scope.leadSourceIsNotValid = false;
     $scope.zipCodeIsNotValid = false;
     $scope.firstNameIsNotValid = false;
     $scope.lastNameIsNotValid = false;

     function hfcConfirm(message, successCallback) {
         bootbox.dialog({
             message: message,
             title: "Warning",
             buttons: {
                 ok: {
                     label: "Yes, Continue",
                     className: "btn-warning",
                     callback: function (params) {
                         successCallback("ok");
                     }
                 },
                 cancel: {
                     label: "Cancel",
                     className: "btn-default",
                     callback: function () {

                     }
                 }
             }
         });
     }

     $scope.SaveLead = function (operation) {
         // $scope.checkk = true;
         if ($scope.IsBusy) return;

         $scope.IsBusy = true;

         $scope.validate = 0;
         $scope.submitted = true;
         var modalId = 'error';
         var dto = $scope.Lead;
         // This is a hack to fix "Canada zipcode not allowing to save without editing" issue.
         // http://plnkr.co/edit/wCWTk9My0LFqEpKa4r3a?p=preview
         // https://github.com/angular/angular.js/issues/8527
         $scope.formlead.formAddressValidation.zipCode.$commitViewValue(true);


         dto.AllowFranchisetoReassign = true;
         if ($scope.SelectedRelatedAccountIds != undefined) {
             dto.RelatedAccountId = $scope.SelectedRelatedAccountIds[0];
         }

         if (dto.LeadId == 0 || dto.LeadId == undefined) {
             dto.Addresses = [];
             dto.Addresses.push($scope.Address);
         }

         // RecieveCorporateEmailBlast strored in the DB as IsReceiveEmails in crm.customer tabel
         if (dto.PrimCustomer) {
             dto.PrimCustomer.IsReceiveEmails = dto.PrimCustomer.RecieveCorporateEmailBlast;
         }
         if ($scope.SelectedSourceIds.length == 0) {
             $scope.IsBusy = false;
             return;

         }

         // Check whether the forms contain value for all the required fields.
         // when any one of the filed is not given a value the following will return true.
         if ($scope.formlead.$invalid) {
             // HFC.DisplaySuccess("Please fill all the required fields! and valid data in optional fields if provided.");
             $scope.IsBusy = false;
             return;
         }

         //if ($scope.Lead.LeadStatusId == 2 || $scope.Lead.LeadStatusId == 5) {
         //    if (LeadService.LeadDispositionlist.length > 0 && ($scope.Lead.DispositionId == null || $scope.Lead.DispositionId == 0)) {
         //        return;
         //    }
         //}

         //warning for duplicate email
         if (!(dto.PrimCustomer.PrimaryEmail == "" && dto.PrimCustomer.SecondaryEmail == "") && !(dto.PrimCustomer.PrimaryEmail == null && dto.PrimCustomer.SecondaryEmail == null) && !(dto.PrimCustomer.PrimaryEmail == undefined && dto.PrimCustomer.SecondaryEmail == undefined) && (dto.PrimCustomer.PrimaryEmail === dto.PrimCustomer.SecondaryEmail)) {
             HFC.DisplayAlert("Secondary Email should not be same as Primary Email");
             $scope.IsBusy = false;
             return;
         }

         if ($scope.SelectedSourceIds.length == 0) {
             dto.SourcesTPId = 0;
         } else {
             dto.SourcesTPId = $scope.SelectedSourceIds[0];
         }

         if (dto.IsBusinessAddress) {
             dto.Addresses[0].IsResidential = false;
             dto.Addresses[0].AddressType = "false"
         } else {
             dto.Addresses[0].IsResidential = true;
             dto.Addresses[0].AddressType = "true"
         }

         dto.LeadSources = LeadSourceService.LeadSources;

         if (!$scope.Lead.PrimCustomer.RecieveCorporateEmailBlast) {
             dto.PrimCustomer.UnsubscribedOn = new Date().toUTCString();
         }

         if ($scope.LeadStatuseslist.Id) {
             dto.LeadStatusId = $scope.LeadStatuseslist.Id.Id; //lead status
             dto.DispositionId = $scope.LeadDispositionlist.Name.DispositionId; //lead status
         }

         if ($scope.isClosedDuplicateSave) {
             dto.SkipDuplicateCheck = true;
         }


         if (dto.LeadId != undefined) {
             $scope.IsEdit = true;
         }




         // Lead save process is not straight forward, so we are splitting into multiple
         // functions call from one another if we can continue, otherwise we come out of
         // the process and give the control to the user.

         // Stetp: validate address.
         validateAddress()
          .then(function (response) {
              var objaddress = response.data;

              if (objaddress.Status == "error") {
                  //
                  //TP-1793: CLONE - The request failed with HTTP status 503: Service Temporarily Unavailable.
                  AfterValidateAddress();
                  //HFC.DisplayAlert(objaddress.Message);
                  //$scope.IsBusy = false;
              } else if (objaddress.Status == "true") {
                  var validAddress = objaddress.AddressResults[0];
                  dto.Addresses[0].Address1 = validAddress.address1;
                  $scope.Addressvalues = dto.Addresses[0].Address1;
                  dto.Addresses[0].address2 = validAddress.address2;
                  dto.Addresses[0].City = validAddress.City;
                  dto.Addresses[0].State = validAddress.StateOrProvinceCode;

                  // Update that the address validated to true.
                  dto.Addresses[0].IsValidated = true;


                  // assign the zipcode back to the model before saving.
                  if (dto.Addresses[0].CountryCode2Digits == "US") {
                      dto.Addresses[0].ZipCode = validAddress.PostalCode.substring(0, 5);
                  }
                  else {
                      dto.Addresses[0].ZipCode = validAddress.PostalCode;
                  }
                  $http.get('/api/Franchise/0/GetTerritory?Zipcode=' + dto.Addresses[0].ZipCode + "&Country=" + dto.Addresses[0].CountryCode2Digits)
                   .then(function (response) {
                       $scope.Address.Territory = response.data;
                       // $scope.Lead.TeritoryType = response.data;

                       AfterValidateAddress();
                   });
                  //address get empty so assiging the value here.
                  $scope.$broadcast('angucomplete-alt:changeInput', 'autocomp_', $scope.Addressvalues);
              } else if (objaddress.Status == "false") {

                  dto.Addresses[0].IsValidated = false;

                  var errorMessage = "Invalid Address. Do you wish to continue?";

                  //dialogService.confirm(errorMessage, function (result) {
                  //    if (result == "ok") {
                  //        // TODO: need to set address validation flag.

                  //        AfterValidateAddress();
                  //    } else {
                  //        $scope.IsBusy = false;
                  //    }
                  //});
                  $scope.Addressvalues = dto.Addresses[0].Address1;
                  //address get empty so assiging the value here.
                  $scope.$broadcast('angucomplete-alt:changeInput', 'autocomp_', $scope.Addressvalues);
                  if (confirm(errorMessage)) {
                      AfterValidateAddress();
                  } else {
                      $scope.IsBusy = false;
                  }
              } else { //SkipValidation
                  AfterValidateAddress();
              }
          },
           function (response) {
               HFC.DisplayAlert(response.statusText);
               $scope.IsBusy = false;
           });
     };

     function AfterValidateAddress() {

         CheckLeadDuplicate().
         then(function (response) {
             var obj = response.data;

             // Validation Error, display a warning message whether the
             // user want to continue or not.
             if (obj.Result == "Error") {
                 // This is causing the application to freeze, we need to find solution. 
                 //dialogService.confirm(obj.ErrorMessage, function (result) {
                 //    if (result == "ok") {
                 //        afterCheckLeadDuplicate();
                 //    } else {
                 //        $scope.IsBusy = false;
                 //    }
                 //});
                 if (confirm(obj.ErrorMessage)) {
                     afterCheckLeadDuplicate();
                 } else {
                     $scope.IsBusy = false;
                 }
             } else {
                 // No duplication address found, so we need to continue the saving process.
                 afterCheckLeadDuplicate();
             }
         },
          function (response) {
              HFC.DisplayAlert(response.statusText);
              $scope.IsBusy = false;
          });
     }

     function afterCheckLeadDuplicate() {

         //This will check the Territory condition
         if ($scope.Address.Territory == "Contracted to a DIFFERENT Franchisee") {
             var errorMessage =
              'This Postal Code is Contracted to another Franchise, do you wish to continue?';
             if (confirm(errorMessage)) {

             } else {
                 $scope.IsBusy = false;
                 return;
             }
         }

         var dto = $scope.Lead;
         //code for spacing in Canada Postal/ZipCode
         if (!dto.Addresses[0].IsValidated) {
             if (dto.Addresses[0].CountryCode2Digits == "CA") {
                 if (dto.Addresses[0].ZipCode) {
                     var Ca_Zipcode = dto.Addresses[0].ZipCode;
                     Ca_Zipcode = Ca_Zipcode.substr(0, 3) + " " + Ca_Zipcode.substr(3);
                     dto.Addresses[0].ZipCode = Ca_Zipcode;
                 }
             }
         }
         //for postal code display
         if (dto.Addresses[0].CountryCode2Digits == "CA") {
             if (dto.Addresses[0].ZipCode) {
                 if (dto.Addresses[0].ZipCode.length == 6) {
                     var Ca_Zipcode = dto.Addresses[0].ZipCode;
                     Ca_Zipcode = Ca_Zipcode.substr(0, 3) + " " + Ca_Zipcode.substr(3);
                     dto.Addresses[0].ZipCode = Ca_Zipcode;
                 }
             }
         }

         // Either there is no duplication or the user want to continue with the duplication.
         //API call to save or update the lead
         $http.post('/api/leads', dto).then(function (response) {
             if ($scope.submitAction == 5) $window.location.href = $scope.redirectto;

             $scope.IsBusy = false;
             if ($scope.IsEdit) {
                 HFC.DisplaySuccess("Lead Modified");
             } else {
                 HFC.DisplaySuccess("Lead created");
             }
             $scope.CheckDuplicatesSave = true;
             if ($scope.isClosedDuplicateSave == false || $scope.isClosedDuplicateSave == undefined) {
                 if (response.data.lstDuplicates) {
                     $scope.lstDuplicates = response.data.lstDuplicates;
                     if ($scope.lstDuplicates.length > 0) {
                         $("#lead-duplicates").modal("show");
                         return;
                     }
                 }
             }

             if ($scope.submitAction == 3) {
                 LeadSourceService.LeadSources = [];

                 $scope.formlead.$setPristine();
                 window.location.href = '#!/leads/' + response.data.LeadId;
             }
             if ($scope.submitAction == 4) {
                 LeadSourceService.LeadSources = [];
                 $scope.formlead.$setPristine();
                 window.location = '#!/leadsEditQualify/' + response.data.LeadId;
             }
             $scope.LeadDispositionlist = null;
             $scope.leadCampaignslist = null;
             $scope.LeadStatuseslist = null;
             $scope.LeadSingleSourceList = null;
             $scope.LeadChannelList = null;

         }, function (response) {
             HFC.DisplayAlert(response.statusText);
             $scope.IsBusy = false;
         });
     }

     $scope.return = function () {
         $scope.showValidate = false;
         window.location.href = '#!/leadsSearch/';
     }

     $scope.cancel = function () {

         $scope.showValidate = false;
         var LeadId = $scope.Lead.LeadId;
         if ($scope.Lead.LeadId > 0) {
             window.location.href = '#!/leads/' + LeadId;
         } else {
             //if ($scope.formlead.$dirty)
             //{
             //    if(confirm('You will lose unsaved changes if you reload this page'))
             //    {
             //        window.location.href = '#!/leadsSearch';
             //    }

             //}               
             //else
             window.location.href = '#!/leadsSearch';

         }
     }

     function CheckLeadDuplicate() {
         var dto = $scope.Lead;
         var model = {
             //LeadId: dto.LeadId,
             FirstName: dto.PrimCustomer.FirstName,
             LastName: dto.PrimCustomer.LastName,
             HomePhone: dto.PrimCustomer.HomePhone,
             CellPhone: dto.PrimCustomer.CellPhone,
             WorkPhone: dto.PrimCustomer.WorkPhone,
             PreferredTFN: dto.PrimCustomer.PreferredTFN,
             Email: dto.PrimCustomer.PrimaryEmail,
             Address1: $scope.Address.Address1,
             Address2: $scope.Address.Address2,
             City: $scope.Address.City,
             State: $scope.Address.State,
             ZipCode: $scope.Address.ZipCode,
         }
         if (dto.LeadId) {
             model.LeadId = dto.LeadId;
         }

         //https://stackoverflow.com/questions/31509452/synchronous-http-call-in-angularjs
         return $http.post('/api/leads/0/ValidateDuplicateLeadTP/', model);
     }

     function validateAddress() {
         var addressModeltp = {
             address1: $scope.Address.Address1,
             address2: $scope.Address.Address2,
             City: $scope.Address.City,
             StateOrProvinceCode: $scope.Address.State,
             PostalCode: $scope.Address.ZipCode,
             CountryCode: $scope.Address.CountryCode2Digits
         };

         if (!addressModeltp.address1 || addressModeltp.address1 == "") {
             var result = {
                 data: {
                     Status: "SkipValidation",
                     Message: "No Address validation required."
                 },
                 status: 200,
                 statusText: "OK"
             }

             return $q.resolve(result);;
         }

         // Call the api to Return the result.
         return AddressValidationService.validateAddress(addressModeltp);
     }

     function InitializeAddress() {
         $scope.Address = {
             IsResidential: false,
             IsCommercial: false,
             AddressType: 'false',
             AttentionText: '',
             Address1: '',
             Address2: '',
             City: '',
             State: '',
             ZipCode: '',
             CountryCode2Digits: HFCService.FranchiseCountryCode,
             CrossStreet: '',
             Location: ''
         };
     }

     function InitializeLead() {
         $scope.Lead = {
             //PrimaryNotes: '',
             PrimCustomer: {
                 FirstName: '',
                 LastName: '',
                 CompanyName: '',
                 WorkTitle: '',
                 HomePhone: '',
                 CellPhone: '',
                 FaxPhone: '',
                 WorkPhone: '',
                 WorkPhoneExt: '',
                 PrimaryEmail: '',
                 SecondaryEmail: '',
                 PreferredTFN: '',
                 RecieveCorporateEmailBlast: false,
                 UnsubscribedOn: '',
                 Origination: '',
                 ActionTaken: '',
                 CrossStreet: '',
                 Disposition: '',
                 SalesStep: '',
                 LocationInfo: '',
                 LegacyAccount: false,
                 CommercialTypeId: ''

             }
         };
     }

     //TODO:Do we really need this?
     $scope.DuplicatesModalClose = function () {
         if ($scope.CheckDuplicatesSave) {
             $scope.isClosedDuplicateSave = true;
         }

         $("#lead-duplicates").modal("hide");
     }

     $scope.getDuplicateAddress = function () {
         var addressDuplicate = $scope.Address;
         if (addressDuplicate.Address1 || addressDuplicate.Address2) {
             $http.post('/api/AdditionalAddress/0/CheckDuplicateAddress', addressDuplicate).then(function (response) {
                 if (response.data) {
                     $window.alert("Lead with " + addressDuplicate.Address1 + " " + addressDuplicate.Address2 + " " + addressDuplicate.City + " " + addressDuplicate.State + " is already exist")
                     //HFC.DisplaySuccess("Lead with "+type+" is already exist");
                 }
             });
         }
     }

     $scope.External = {};

     $scope.refreshNotesGrid = function () {

         var result = $scope.External.Notes;
         $scope.NoteServiceTP.RefreshNotesGrid(result);
     }

     $scope.refreshAdditionalContactsGrid = function () {

         var result = $scope.External.AdditionalContacts;
         $scope.AdditionalContactServiceTP.RefreshGrid(result);
     }

     $scope.refreshAdditionalAddressGrid = function () {

         var result = $scope.External.AdditionalAddress;
         $scope.AdditionalAddressServiceTP.RefreshGrid(result);
     }

     //Added for Address - State
     //TODO: Can we move this common service??
     $scope.getStates = function (countryISO) {
         if ($scope.StateList.length > 0 && countryISO) {
             var sourcee = $.grep($scope.StateList, function (s) {
                 return s.CountryISO2 == countryISO;
             });

             if (sourcee) return source;
         }
         return null;
     };

     $scope.tooltipOptions = {
         filter: "th",
         position: "top",
         hide: function (e) {
             this.content.parent().css('visibility', 'hidden');
         },
         show: function (e) {
             if (this.content.text().length > 1) {
                 this.content.parent().css('visibility', 'visible');
             } else {
                 this.content.parent().css('visibility', 'hidden');
             }
         },
         content: function (e) {
             var target = e.target.data().title; // element for which the tooltip is shown
             return target; //$(target).text();
         }
     };

     $scope.CountryChanged = function () {
         if ($scope.Address != null) {
             $scope.Address.State = '';
         }
     }

     $scope.GetSalesAgent = function () {
         $http.get('/api/opportunities/0/GetSalesAgents').then(function (data) {
             $scope.SalesAgentlist = data.data;
             var selectedSales = null;
             selectedSales = $scope.PersonService.CalendarService.selectedSalesAgent;

             if (selectedSales && selectedSales.length == 1) {
                 $scope.SelectedAgent = selectedSales[0];
                 $scope.PersonService.CalendarService.selectedSalesAgent = [];
             }
         });

     }
     $scope.location = $location.$$path;
     if ($scope.location.match("leadConvert")) {
         $scope.GetSalesAgent();
     }

     $scope.initialize = function () {
         if (!$scope.Lead.LeadId) {
             var id = 0;
             if ($scope.Lead.LeadStatusId)
                 id = $scope.Lead.LeadStatusId;

             $http.get('/api/lookup/' + id + '/LeadStatus').then(function (data) {
                 $scope.LeadStatuseslist = data.data;

                 if (!$scope.Lead.LeadId) {
                     $scope.Lead.LeadStatusId = 1;
                 }
             });
         }

         //Get the Industries

         $http.get('/api/lookup/1/Type_LookUpValues').then(function (data) {
             $scope.LeadIndustries = data.data;
         });
         //Get the Commercial Type
         $http.get('/api/lookup/2/Type_LookUpValues').then(function (data) {
             $scope.LeadCommercialType = data.data;

         });
         //Get the RelationshipType
         $http.get('/api/lookup/3/Type_LookUpValues').then(function (data) {
             $scope.LeadRelationshipType = data.data;
             //$scope.Lead.RelationshipTypeId = 3001;
         });

         $http.get('/api/lookup/0/GetDispositionsTP').then(function (data) {
             $scope.LeadDispositionlist = data.data;
         });
         $http.get('/api/lookup/0/Campaigns').then(function (data) {
             $scope.leadCampaignslist = data.data;

         });

         $http.get('/api/lookup/0/SourceTP').then(function (data) {
             $scope.LeadSourcesList = data.data;
         });
         $scope.StatusNameByLeadStatus = function (id) {

             // Filter the required disposition from the exisiting list.
             if ($scope.LeadStatuseslist && $scope.LeadStatuseslist.length > 0 && id) {
                 var sourcee = $.grep($scope.LeadStatuseslist, function (d) {
                     return d.Id == id;
                 });
                 if (sourcee)
                     return sourcee[0].Name;
             }
             return "";
         }
         $http.get('/api/lookup/0/ReferralType').then(function (data) {
             $scope.referralTypeList = data.data;
         });
         $scope.Lead = {
             //SelectedSource: {},

             //SourcesTPId:0,
             IsNotifyText: false,
             IsNotifyemails: true,
             CampaignId: 0,
             //PrimaryNotes: '',
             PrimCustomer: {
                 FirstName: '',
                 LastName: '',
                 CompanyName: '',
                 WorkTitle: '',
                 HomePhone: '',
                 CellPhone: '',
                 FaxPhone: '',
                 WorkPhone: '',
                 WorkPhoneExt: '',
                 PrimaryEmail: '',
                 SecondaryEmail: '',
                 PreferredTFN: '',
                 RecieveCorporateEmailBlast: false, // IsReceiveEmails
                 UnsubscribedOn: '',
                 Origination: '',
                 ActionTaken: '',
                 CrossStreet: '',
                 SalesStep: '',
                 LocationInfo: '',


             }
         };



         $scope.Address = {
             IsResidential: false,
             IsCommercial: false,
             AddressType: 'false',
             AttentionText: '',
             Address1: '',
             Address2: '',
             City: '',
             Country: 'US',
             State: '',
             ZipCode: '',
             CountryCode2Digits: HFCService.FranchiseCountryCode,
             CrossStreet: '',
             Location: ''
         };


         $scope.showterritoryvalue = function () {

             if ($scope.TerritoryDisplayId > 0) {
                 $scope.Lead.TerritoryDisplayId = $scope.TerritoryDisplayId;
             } else
                 $scope.Lead.TerritoryDisplayId = 0;

         }

         $scope.RelatedAccounts = function () {
             $scope.RelatedAccounts = {
                 placeholder: "Select",
                 dataTextField: "FullName",
                 dataValueField: "AccountId",
                 filter: "contains",
                 valuePrimitive: true,
                 autoBind: true,
                 dataSource: {
                     transport: {
                         read: {
                             url: "/api/lookup/0/GetRelatedAccounts",
                         }
                     }
                 },
             };
         }

         $scope.lead_campaign = function () {
             $scope.lead_campaign = {
                 optionLabel: {
                     Name: "Select",
                     CampaignId: ""
                 },
                 dataTextField: "Name",
                 dataValueField: "CampaignId",
                 filter: "contains",
                 valuePrimitive: true,
                 autoBind: true,
                 change: function (data) {
                     var val = this.value();
                     if (val.length > 0) {
                         $scope.ChangeCampaign()
                     }
                 },
                 dataSource: {
                     transport: {
                         read: {
                             url: "/api/lookup/0/Campaigns",
                         }
                     }
                 },
             };
         }
         $scope.SelectedRelatedAccountIds = [];
         $scope.RelatedSource = function () {
             $scope.RelatedSource = {
                 placeholder: "Select",
                 dataTextField: "name",
                 dataValueField: "SourceId",
                 filter: "contains",
                 valuePrimitive: true,
                 autoBind: true,
                 dataSource: {
                     transport: {
                         read: {
                             url: "/api/lookup/0/SourceTP",
                         }
                     }
                 },
             };
         }
         $scope.SelectedSourceIds = [];
         // Populate the list of State
         // TODO: Can we move this to commonservice.
         var geturl = '/api/AdditionalAddress/0/GetState/';
         $http.get(geturl).then(function (responseSuccess) {
             $scope.StateList = responseSuccess.data;
             //$scope.StateListForCountry = $scope.getStates($scope.Address.CountryCode2Digits);
         }, function (responseError) {
             HFC.DisplayAlert(responseError.statusText);
         });
     }

     $scope.LeadDispositionByLeadStatus = function (id) {
         if ($scope.LeadDispositionlist && $scope.LeadDispositionlist.length > 0 && id) {
             var sourcee = $.grep($scope.LeadDispositionlist, function (d) {
                 return d.LeadStatusId == id;
             });

             if (sourcee) return sourcee;
         }
         return null;
     }

     $scope.DescriptionByDisposition = function (id) {
         // Filter the required disposition from the exisiting list.
         if ($scope.LeadDispositionlist && $scope.LeadDispositionlist.length > 0 && id) {
             var source = $.grep($scope.LeadDispositionlist, function (d) {
                 return d.DispositionId == id;
             });
             if (source) return source[0].Description;
         }
         return "";
     }

     // print integration -- murugan
     $scope.printControl = {};
     $scope.printSalesPacket = function (modalid) {
         if ($routeParams.leadId) $scope.leadIdPrint = $routeParams.leadId;

         // $("#" + modalid).modal("show");
         $scope.printControl.showPrintModal();
     }

     //check with account email 
     $scope.Acct_EmailData = function (Val) {

         if (Val == null || Val == "" || Val == undefined) {
             return;
         } else {
             var Check_email = Val;
             $http.get('/api/Accounts/0/GetAcctDetails?value=' + Check_email).then(function (response) {
                 if (response.data != null) {

                     if (response.data.length == 0) {
                         return;
                     } else {
                         $scope.Acct_details = response.data;
                         $("#lead_AccntDetails").modal("show");
                         return
                     }

                 }
             })
         }
     }

     //code for sending email when email button clicked.
     $scope.SendLeadEmail = function () {
         $scope.EmailNotify = false;
         var leadid = $scope.Lead.LeadId;
         $scope.modalState.loaded = false;
         if ($scope.Lead.IsNotifyemails == true) {


             $http.get('/api/Leads/0/SendEmailToLead?leadid=' + leadid).then(function (response) {

                 if (response.data == "True") {
                     $scope.Lead.EmailSent = true;
                     $scope.MailSent = true;

                     if ($scope.Lead.PrimCustomer.SecondaryEmail && $scope.Lead.PrimCustomer.PrimaryEmail)
                         $scope.Mailid = $scope.Lead.PrimCustomer.PrimaryEmail + ',' + $scope.Lead.PrimCustomer.SecondaryEmail
                     else if ($scope.Lead.PrimCustomer.PrimaryEmail)
                         $scope.Mailid = $scope.Lead.PrimCustomer.PrimaryEmail;
                     else if ($scope.Lead.PrimCustomer.SecondaryEmail)
                         $scope.Mailid = $scope.Lead.PrimCustomer.SecondaryEmail;

                     $scope.modalState.loaded = true;

                     $("#AfetrThanks_EmailSend").modal("show");
                     return;
                 } else if (response.data == "Success") {
                     $scope.MailSent = false;
                     $scope.Lead.EmailSent = false;
                     $scope.modalState.loaded = true;
                     $("#AfetrThanks_EmailSend").modal("show");
                     return;
                 }
                 else {
                     $scope.EmailNotify = true;
                     $scope.modalState.loaded = true;
                     $("#AfetrThanks_EmailSend").modal("show");
                     return;
                 }

             });
         }
         else {
             $scope.EmailNotify = true;
             $scope.modalState.loaded = true;
             $("#AfetrThanks_EmailSend").modal("show");
             return;
         }
     }

     $scope.EmailMailClose = function () {

         $("#AfetrThanks_EmailSend").modal("hide");
         return;
     }

     //check with account (phone numbers)
     $scope.Check_AcctNum = function (ValPhne) {

         if (ValPhne == null || ValPhne == "" || ValPhne == undefined) {
             return;
         } else {
             var Check_Phne = ValPhne;

             $http.get('/api/Accounts/0/GetAcctPhneDetails?value=' + Check_Phne).then(function (response) {
                 if (response.data != null) {

                     if (response.data.length == 0) {
                         return;
                     } else {
                         $scope.Acct_details = response.data;
                         $("#lead_AccntDetails").modal("show");                         
                         return
                     }

                 }
             })
         }
     }

     $scope.selectedRow = null;
     $scope.setClickedRow = function (index, AcntId) {

         $scope.selectedRow = index;
         $scope.AcntId = AcntId;
     }

     $scope.cancel_Pop = function () {
         
         $("#lead_AccntDetails").modal("hide");
         $scope.selectedRow = null; //made the selection as empty when matching list pop-up is closed
         $scope.setClickedRow(); //called the function so the values are not retained 
         return
     }

     $scope.View_newtab = function (tab_val) {

         if (tab_val == true) {
             var actId = $scope.AcntId;
             if (actId) {
                 $("#lead_AccntDetails").modal("hide");
                 $scope.selectedRow = null;
                 var urls_val = '#!/Accounts/' + actId;
                 var win = window.open(urls_val, '_blank');
                 win.focus();
                 $scope.setClickedRow();
             } else {
                 HFC.DisplayAlert("Select Any One Matching Record To View");
             }
         } else return;
     }

     $scope.Account_view = function (view_val) {

         if (view_val == true) {
             var actId = $scope.AcntId;
             if (actId) {
                 $("#lead_AccntDetails").modal("hide");
                 $scope.selectedRow = null;
                 $scope.formlead.$setPristine();
                 window.location.href = '#!/Accounts/' + actId;
                 $scope.setClickedRow();
             } else {
                 HFC.DisplayAlert("Select Any One Matching Record To View");
             }
         } else return;
     }

     // Initialization code.
     $scope.initialize();

     $scope.RelatedAccounts();
     $scope.lead_campaign();
     $scope.RelatedSource();
     $scope.ShowHelpPopup = function () {
         $("#helppopup").modal("show");
         return;
     }

     //Update the RelationShip type on Commercial Type Change
     $scope.UpdateRelationShipType = function () {
         if ($scope.Lead.CommercialTypeId != 2000) {
             $scope.Lead.RelationshipTypeId = 3001;
         } else
             $scope.Lead.RelationshipTypeId = null;
     }

     $scope.checkk = true;

     $scope.$on('$locationChangeStart', function ($event, next, current) {
         if ($scope.formlead)
             if ($scope.formlead.$dirty)
                 if ($scope.checkk == true)
                     if (current.includes('/leads') && !current.includes('/leads/')) {
                         $scope.checkk = false;
                         $event.preventDefault();
                         $scope.redirectto = next.substring(next.indexOf('#!'), next.length);
                         dialogService.confirmDialog();

                     }

     });

     $scope.PersonService.leadredirectCallBack = function () {

         if ($scope.PersonService.clicked == 'yes') {
             $scope.submitAction = 5;
             $scope.SaveLead(5); // $scope.SaveLead function needs arg and no meaning behind this arg from this place
         } else if ($scope.PersonService.clicked == 'no') {
             $window.location.href = $scope.redirectto;
         } else {
             $scope.checkk = true;
         }
     }

     //texting
     $scope.EnableNotifyText = function (CellPhone) {
         if (CellPhone != undefined) {
             //if (CellPhone == $scope.CellPhoneCopy)
             //    $scope.Lead.IsNotifyText = $scope.IsNotifyTextCopy;

             if (CellPhone.length == 10) {
                 $http.get("/api/Communication/0/GetTextingStatus?cellPhone=" + CellPhone).then(function (data) {

                     if (data.data == null) {
                         $scope.Lead.NotifyTextStatus = 'new';
                         $scope.disablenotifytext = false;
                     }
                     else if (data.data.optOut == true) {
                         $scope.Lead.NotifyTextStatus = 'Optout';
                         $scope.disablenotifytext = false;
                     }
                     else if (data.data.optIn == true) {
                         $scope.Lead.NotifyTextStatus = 'Optin';
                         $scope.disablenotifytext = false;
                     }
                     else if (data.data.msgSent == true) {
                         $scope.Lead.NotifyTextStatus = 'waiting';
                         $scope.disablenotifytext = false;
                     }
                     else if (data.data.optOut == null && data.data.optIn == null && data.data.msgSent == false) {
                         $scope.Lead.NotifyTextStatus = 'error';
                         $scope.disablenotifytext = false;
                     }

                 })
             }
             else {
                 $scope.Lead.NotifyTextStatus = 'new';
                 $scope.disablenotifytext = true;
                 $scope.Lead.IsNotifyText = false;
             }
         }
         else {
             $scope.Lead.NotifyTextStatus = 'new';
             $scope.disablenotifytext = true;
             $scope.Lead.IsNotifyText = false;
         }
     }

     $scope.NotifyText = function () {
         if ($scope.FranciseLevelTexting && $scope.Lead.IsNotifyText && ($scope.Lead.NotifyTextStatus == 'new' || $scope.Lead.NotifyTextStatus == 'error')) {
             if ($scope.CellPhoneCopy == $scope.Lead.PrimCustomer.CellPhone && $scope.Lead.IsNotifyText == $scope.IsNotifyTextCopy) {
                 $scope.Lead.SendText = false;
                 $scope.setAction(3);
             }
             else {
                 //if ($scope.formlead.$invalid) {
                 //   $scope.IsBusy = false;
                 //    return;
                 //}
                 //if ($scope.SelectedSourceIds.length == 0) {
                 //    $scope.IsBusy = false;
                 //    return;
                 //}
                 $scope.Lead.SendText = true;
                 //$("#Textnotification").modal("show");       
                 $scope.setAction(3);
             }
         }
         else {
             $scope.Lead.SendText = false;
             $scope.setAction(3);
         }

     }
     $scope.CloseNotifyTextpopup = function () {
         $("#Textnotification").modal("hide");
     }

     $scope.EmailCheck = function (value) {
         if (value == false) {
             document.getElementsByName('user_email')[0].placeholder = '';
             $scope.Required = false;
         }
         else {
             document.getElementsByName('user_email')[0].placeholder = 'Required';
             $scope.Required = true;
         }

     }
 }
]);

app.directive('validDomainName', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attributes, control) {
            control.$validators.validDomainName = function (modelValue, viewValue) {
                var EXP = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;                
                if (viewValue != undefined && viewValue.includes('@') && (viewValue.split('@')[1].includes('_') || viewValue.split('@')[1].includes('..')) && EXP.test(viewValue)) // correct value
                {
                    return false;
                }
                else
                return true; // wrong value
            };
        }
    };
});
app.directive('validEmailRegx', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attributes, control) {
            control.$validators.validEmailRegx = function (modelValue, viewValue) {
                var EXP = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;
                if (control.$isEmpty(modelValue)) // if empty, correct value
                {
                    return true;
                }                
                if (EXP.test(viewValue)) // correct value
                {
                    return true;
                }
                return false; // wrong value
            };
        }
    };
});

app.filter('dateFormat1', function ($filter) {
    return function (input) {
        if (input == null) {
            return "";
        }

        var _date = $filter('date')(new Date(input), 'M/d/yyyy');

        return _date.toUpperCase();

    };
});
