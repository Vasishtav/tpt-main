﻿/***************************************************************************\
Controller Name:  leadConvertController.js - leadConvertController AngularJS
Project: HFC
Created on: 09/04/2020
Created By: Thiyaghu / Ramasamy
Copyright:
Description: seperate controller to handle the convert lead flow ( break up from lead controller )
Change History:
Date  By  Description

\***************************************************************************/

'use strict';

app.controller('leadConvertController', [
    '$scope', '$routeParams', '$rootScope', '$http', '$window',
    '$location', '$modal', 'LeadSourceService', 'PersonService',
    'LeadService', 'AddressService', 'HFCService', 'kendoService', 'AccountSearchService', 'NavbarService',
    function (
        $scope, $routeParams, $rootScope, $http, $window,
        $location, $modal, LeadSourceService, PersonService,
        LeadService, AddressService, HFCService, kendoService, AccountSearchService, NavbarService) {

        $scope.AccountSearchService = AccountSearchService;
        HFCService.setHeaderTitle("Lead  #");

        $scope.NavbarService = NavbarService;
        $scope.NavbarService.SelectSales();
        $scope.NavbarService.EnableSalesLeadsTab();


        HFCService.GetUrl();

        var ctrl = this;


        $scope.NotesParams = {};
        $scope.Lead = {};
        $scope.Lead.LeadId = $routeParams.leadId;

        $scope.LeadService = LeadService;

        $scope.HFCService = HFCService;
        //texting
        $scope.BrandId = $scope.HFCService.CurrentBrand;
        $scope.FranciseLevelTexting = $scope.HFCService.FranciseLevelTexting;
        $scope.disablenotifytext = true;
        $scope.Required = true;

        $scope.MatchLeadAccount = [];

        //--needed to show error message when no Account is being selected.
        $scope.errorMessages = {};
        $scope.errorMessages.ConvertPage = "Please select a matching Account or Create New Account";
        $scope.errorMessages.AccountSearchPage = "Please choose an account for the lead"; //-- end.
        $scope.convertErrorMessage = "";

        //Added for Convert Lead functionality
        $scope.SelectedAccountFromMatchinglist = null
        $scope.AccountSearchService.SelectedAccountFromSearch = null
        $scope.IsCreateNewAccount = false;
        $scope.IsAddOpportunity = true;

        // -- needed
        $scope.SearchAccounts = function (modalId) {
            $("#" + modalId).modal("show");
        }

        // -- needed
        $scope.CancelConvertLead = function (modalId) {
            $("#" + modalId).modal("hide");
            var myDiv = $('.convert_popup');
            myDiv[0].scrollTop = 0;
        }

        // -- needed
        $scope.SaveSelectedAccounts = function (modalId) {

            var myDiv = $('.convert_popup');
            myDiv[0].scrollTop = 0;
            var errmodalId = 'convertleadmessagepopup';
            if ($scope.AccountSearchService.SelectedAccountFromSearch == null) {
                $scope.convertErrorMessage = $scope.errorMessages.AccountSearchPage;
                $("#" + errmodalId).modal("show");
                return;
            } else {
                var leadId = $scope.AccountSearchService.SelectedAccountFromSearch;
                //$http.get('/api/search/' + leadId + '/ApplyLeadMatchingAccount')
                $http.get('/api/search/' + leadId + '/GetLeadMatchingAccountsTP')
                    .then(function (data) {

                        $scope.MatchLeadAccount = data.data;
                        $scope.SelectMatchingAccount = data.data[0].AccountId;

                        $scope.SelectedAccount($scope.SelectMatchingAccount);

                    });
            }
            $("#" + modalId).modal("hide");

        }



        //-- needed when user select an account from serch grid pop-up this is assiging the value.
        $scope.SelectedSearchAccount = function (accountid) {
            $scope.AccountSearchService.SelectedAccountFromSearch = accountid;

        }
        //--end.

        //--needed for seraching account in when accout search pop-up appears.
        $scope.AccountgridSearch = function () {
            var searchValue = $('#searchBox').val();
            $("#gridAccountSearch").data("kendoGrid").dataSource.filter({
                logic: "or",
                filters: [
                    {
                        field: "CompanyName",
                        operator: "contains",
                        width: "150px",
                        value: searchValue
                    },
                    {
                        field: "AccountFullName",
                        operator: "contains",
                        width: "150px",
                        value: searchValue
                    },
                    {
                        field: "AccountType", //in db it is AccountTypeId
                        operator: "contains",
                        width: "100px",
                        value: searchValue
                    },
                    {
                        field: "Status",
                        operator: "contains",
                        width: "100px",
                        value: searchValue
                    },
                    {
                        field: "Address1",
                        operator: "contains",
                        width: "200px",
                        value: searchValue
                    },
                    {
                        field: "City",
                        operator: "contains",
                        width: "100px",
                        value: searchValue
                    },
                    {
                        field: "State",
                        operator: "contains",
                        width: "100px",
                        value: searchValue
                    },
                    {
                        field: "ZipCode",
                        operator: "contains",
                        width: "100px",
                        value: searchValue
                    },
                    {
                        field: "DisplayPhone",
                        operator: "contains",
                        value: searchValue
                    },
                    {
                        field: "PrimaryEmail",
                        operator: "contains",
                        value: searchValue
                    }
                ]
            });

        }
        //-- end.

        //-- needed when clear search button is clicked.
        $scope.AccountgridSearchClear = function () {
            $('#searchBox').val('');
            $("#gridAccountSearch").data('kendoGrid').dataSource.filter({
            });
            // AccountSearchService.Accounts_options = kendotooltipService.setColumnWidth(AccountSearchService.Accounts_options);
        }
        //-- end.

        // -- needed
        $scope.SelectedAccount = function (accountId) {
            $scope.ResetCreateNewAccount();
            $scope.SelectedAccountFromMatchinglist = accountId;

            //reset that new account is false;
            $scope.IsCreateNewAccount = false;
        }

        // -- needed
        $scope.ConvertLeadFromSelectedAccount = function () {
            if ($scope.IsBusy) return;

            $scope.submitted = true;
            $scope.IsBusy = true;

            var leadId = 0;
            var selectedaccountid = 0;
            var addnewaccount = 0;
            var addopportunity = 0;

            var salesAgentdropdown = $("#salesAgent").data("kendoDropDownList").value();
            if (salesAgentdropdown == null || salesAgentdropdown == "") {
                $("#salesAgent").addClass("required-error");
                $scope.IsBusy = false;
                return;
            }

            if ($scope.formConvertLead.$invalid) {
                $scope.IsBusy = false;
                return;
            }

            //if ($scope.IsBusy) return;
            //$scope.IsBusy = true;

            var modalId = 'convertleadmessagepopup';

            if ($scope.Lead.LeadId != null) {
                leadId = $scope.Lead.LeadId;
            }
            if ($scope.SelectedAccountFromMatchinglist != null && $scope.IsCreateNewAccount == false) {
                selectedaccountid = $scope.SelectedAccountFromMatchinglist;
            }
            if ($scope.IsCreateNewAccount != null) {
                if ($scope.IsCreateNewAccount == true) {
                    addnewaccount = 1;
                }
            }
            if ($scope.IsAddOpportunity != null) {
                if ($scope.IsAddOpportunity == true) {
                    addopportunity = 1;
                }

            }
            if (selectedaccountid == 0 && addnewaccount == 0) {
                $scope.ConvertLeadValidation == true;
                $scope.convertErrorMessage = $scope.errorMessages.ConvertPage;
                $("#" + modalId).modal("show");

                $scope.IsBusy = false;
                return;
            }

            //$scope.IsBusy = true;
            var newaccountId = 0;
            if ($scope.Lead.LeadId) {
                var data = {
                    leadId: leadId,
                    selectedaccountid: selectedaccountid,
                    addnewaccount: addnewaccount,
                    addopportunity: addopportunity
                }
                data.opportunityName = $scope.OpportunityName;
                data.SalesAgentId = $scope.SelectedAgent;
                var loadingElement = $("#loading");
                if (loadingElement) {
                    loadingElement[0].style.display = "block";
                }
                $http.put('/api/leads/1/ConvertSelectLead', data).then(function (response) {
                    HFC.DisplaySuccess("Lead converted");
                    if (loadingElement) {
                        loadingElement[0].style.display = "none";
                    }
                    if (response.data.newaccountId != null) {
                        newaccountId = response.data.newaccountId;
                    }
                    if (newaccountId > 0) {
                        var goToAccountsPage = '#!/Accounts/' + newaccountId;
                        $window.open(goToAccountsPage, '_self');
                    }
                    $scope.IsBusy = false;
                });

            }

            //$scope.IsBusy = false;

            if (selectedaccountid != 0) {
                var goToAccountsPage = '#!/Accounts/' + selectedaccountid;
                $window.open(goToAccountsPage, '_self');
            }
        }

        $scope.ResetCreateNewAccount = function () {
            $("#optNewAccount").prop("checked", false);
            $scope.AccountSearchService.SelectedAccountFromSearch = null
        }
        // -- needed
        $scope.CheckForMatchingAccounts = function () {
            $('input:radio[name="optSelectAccount"]').prop('checked', false);
        }
        // -- needed
        $scope.ClickMeToRedirectLeadDetails = function () {

            //  var url = "http://" + $window.location.host + "/#!/leads/" + $scope.Lead.LeadId;
            $window.location.href = "#!/leads/" + $scope.Lead.LeadId;
        };

        function errorHandler(reseponse) {
            // Inform that the modal is ready to consume:
            $scope.modalState.loaded = true;
            HFC.DisplayAlert("Unknown error occured. Please contact site Administrator.");
        }


        function successHandler(leadServiceData) {

            var lead = leadServiceData.Lead;
            $scope.Lead = lead;
            //--needed to show the opportunity name by default.
            $scope.currentDate = new Date();
            $scope.currentDate = $scope.currentDate.toLocaleDateString();

            if ($scope.Lead.IsCommercial && $scope.Lead.PrimCustomer.CompanyName != "")
                $scope.OpportunityName = $scope.Lead.PrimCustomer.CompanyName + " " + $scope.currentDate;
            else
                $scope.OpportunityName = $scope.Lead.PrimCustomer.LastName + " " + $scope.currentDate;
            //--end.


            //Added to fetch the matching accounts for selected lead   -- needed
            var loading = document.getElementById("loading");
            loading.style.display = "block";
            $http.get('/api/search/' + $scope.Lead.LeadId + '/GetLeadMatchingAccounts')
                .then(function (data) {
                    $scope.MatchLeadAccount = data.data;

                    // trigger depenedent call
                    //$scope.GetSalesAgent();
                    $scope.salesAgentdrop();
                    if ($scope.MatchLeadAccount.length < 1)
                        $scope.IsCreateNewAccount = true;

                    var loading = document.getElementById("loading");
                    loading.style.display = "none";
                }); //, errorHandler);

        }

        //     // The controller is called for Edit more.
        if ($scope.Lead.LeadId > 0) {//--needed since successhandler gives the opportunity name data.
            $scope.LeadService.Get($scope.Lead.LeadId, successHandler, errorHandler);
        }

        //-- needed Redirect to Create Lead Page.
        $scope.Newleadpage = function () {
            window.location.href = '#!/leads';
            //var url = "http://" + $window.location.host + "/#!/leads/";
            //$window.location.href = url;
        };
        //--end

        // -- needed
        $scope.ClickMeToRedirectEditLead = function () {

            //var url = "http://" + $window.location.host + "/#!/leadsEdit/" + $scope.Lead.LeadId;
            $window.location.href = "#!/leadsEdit/" + $scope.Lead.LeadId;
            // window.location.href = url;
        };

        // -- needed
        //$scope.GetSalesAgent = function () {
        //    $http.get('/api/opportunities/0/GetSalesAgents').then(function (data) {
        //        $scope.SalesAgentlist = data.data;
        //        var loading = document.getElementById("loading");
        //        loading.style.display = "none";
        //        //-- commented out as CalendarService is not needed it seems. --
        //        //var selectedSales = null;
        //        //selectedSales = $scope.PersonService.CalendarService.selectedSalesAgent;

        //        //if (selectedSales && selectedSales.length == 1) {
        //        //    $scope.SelectedAgent = selectedSales[0];
        //        //    $scope.PersonService.CalendarService.selectedSalesAgent = [];
        //        //}
        //        //-- end --
        //    });

        //}
        $scope.salesAgentdrop = function () {
            $scope.salesAgent_option = {
                optionLabel: {
                    Name: "Select",
                    SalesAgentId: ""
                },
                dataTextField: "Name",
                dataValueField: "SalesAgentId",
                filter: "contains",
                valuePrimitive: true,
                autoBind: true,
                change: function (data) {
                    var val = this.value();
                    if (val.length > 0) {
                        $("#salesAgent").removeClass("required-error");
                    }
                },
                dataSource: {
                    transport: {
                        read: function (e) {
                            $http.get("/api/opportunities/0/GetSalesAgents").then(function (data) {
                                e.success(data.data);
                                var loading = document.getElementById("loading");
                                loading.style.display = "none";
                            });
                        }
                    }
                }
            }
        }

    }
]);
